module alauda.io/devops-apiserver

go 1.12

replace (
	alauda.io/app-core => bitbucket.org/mathildetech/app-core v1.3.11
	github.com/Sirupsen/logrus v1.1.1 => github.com/sirupsen/logrus v1.1.1
	github.com/appscode/jsonpatch => gomodules.xyz/jsonpatch/v2 v2.0.1 // indirect
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b => k8s.io/klog v0.3.2
	github.com/prometheus/client_golang => github.com/prometheus/client_golang v0.9.3
	golang.org/x/text => github.com/golang/text v0.3.0
	// replacing dependencies updated by alauda-backend/cyborg
	k8s.io/api v0.0.0-20190313235455-40a48860b5ab => k8s.io/api v0.0.0-20181213150558-05914d821849
	k8s.io/apimachinery v0.0.0-20190313205120-d7deff9243b1 => github.com/alauda/apimachinery v0.0.0-20200319033618-f3d08b62a30d
	k8s.io/apiserver v0.0.0-20181126153457-92fdef3a232a => k8s.io/apiserver v0.0.0-20181213151703-3ccfe8365421
	k8s.io/client-go v11.0.0+incompatible => k8s.io/client-go v0.0.0-20181213151034-8d9ed539ba31
	k8s.io/kube-openapi v0.0.0-20190709113604-33be087ad058 => k8s.io/kube-openapi v0.0.0-20181021203552-90b54e673cf4
	sigs.k8s.io/controller-runtime v0.1.12 => sigs.k8s.io/controller-runtime v0.1.10
)

require (
	alauda.io/app-core v1.3.11
	bitbucket.org/mathildetech/alauda-backend v0.1.27
	bitbucket.org/mathildetech/app v1.0.1
	bitbucket.org/mathildetech/auth-controller2 v0.0.0-20190716071038-5e0dc2c2e6b9
	bitbucket.org/mathildetech/courier v0.0.0-20191223091114-c9ac53b1d197
	bitbucket.org/mathildetech/devops-client v1.0.41
	bitbucket.org/mathildetech/jenkinsfilext/v2 v2.5.2
	bitbucket.org/mathildetech/log v1.0.5
	bitbucket.org/ww/goautoneg v0.0.0-20120707110453-75cd24fc2f2c // indirect
	github.com/Azure/go-ansiterm v0.0.0-20170929234023-d6e3b3328b78 // indirect
	github.com/Jeffail/gabs v1.4.0 // indirect
	github.com/Sirupsen/logrus v1.1.1 // indirect
	github.com/alauda/cyborg v0.5.1 // indirect
	github.com/alcortesm/tgz v0.0.0-20161220082320-9c5fe88206d7 // indirect
	github.com/anmitsu/go-shlex v0.0.0-20161002113705-648efa622239 // indirect
	github.com/appscode/jsonpatch v0.0.0-20190108182946-7c0e3b262f30
	github.com/blang/semver v3.5.1+incompatible
	github.com/coreos/go-semver v0.3.0 // indirect
	github.com/docker/docker v1.13.1 // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/elazarl/goproxy/ext v0.0.0-20190711103511-473e67f1d7d2 // indirect
	github.com/emicklei/go-restful v2.9.6+incompatible
	github.com/emicklei/go-restful-swagger12 v0.0.0-20170208215640-dcef7f557305 // indirect
	github.com/emirpasic/gods v1.9.0 // indirect
	github.com/fatih/color v1.7.0
	github.com/flynn/go-shlex v0.0.0-20150515145356-3f9db97f8568 // indirect
	github.com/ghodss/yaml v1.0.0
	github.com/gliderlabs/ssh v0.1.4 // indirect
	github.com/go-logr/logr v0.1.0
	github.com/go-openapi/errors v0.19.0
	github.com/go-openapi/runtime v0.19.15
	github.com/go-openapi/spec v0.19.3
	github.com/go-openapi/strfmt v0.19.0
	github.com/go-openapi/swag v0.19.2
	github.com/go-openapi/validate v0.19.0
	github.com/go-swagger/go-swagger v0.19.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/golang/mock v1.3.1
	github.com/google/go-github v15.0.0+incompatible
	github.com/google/go-querystring v0.0.0-20170111101155-53e6ce116135
	github.com/google/gofuzz v1.0.0
	github.com/igm/sockjs-go v2.0.1+incompatible // indirect
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99 // indirect
	github.com/jinzhu/now v1.0.1
	github.com/juju/loggo v0.0.0-20190526231331-6e530bcce5d8 // indirect
	github.com/kevinburke/ssh_config v0.0.0-20180317175531-9fc7bb800b55 // indirect
	github.com/liggitt/tabwriter v0.0.0-20181228230101-89fcab3d43de // indirect
	github.com/mitchellh/mapstructure v1.1.2
	github.com/moul/http2curl v1.0.0
	github.com/natefinch/lumberjack v2.0.0+incompatible // indirect
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.5.0
	github.com/pelletier/go-buffruneio v0.2.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/prometheus/client_golang v1.0.0
	github.com/satori/go.uuid v1.2.0
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/spf13/cast v1.3.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.4.0
	github.com/src-d/gcfg v1.3.0 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/ugorji/go v1.1.5-pre // indirect
	github.com/xanzy/go-gitlab v0.10.6
	github.com/xanzy/ssh-agent v0.2.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
	gopkg.in/h2non/gock.v1 v1.0.14
	gopkg.in/igm/sockjs-go.v2 v2.0.1
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0-20150622162204-20b71e5b60d7 // indirect
	gopkg.in/src-d/go-billy.v4 v4.2.0
	gopkg.in/src-d/go-git-fixtures.v3 v3.5.0 // indirect
	gopkg.in/src-d/go-git.v4 v4.4.1
	gopkg.in/warnings.v0 v0.1.2 // indirect
	gopkg.in/yaml.v1 v1.0.0-20140924161607-9f9df34309c0 // indirect
	gopkg.in/yaml.v2 v2.2.4
	k8s.io/api v0.0.0-20190313235455-40a48860b5ab
	k8s.io/apimachinery v0.0.0-20190313205120-d7deff9243b1
	k8s.io/apiserver v0.0.0-20181213151703-3ccfe8365421
	k8s.io/client-go v11.0.0+incompatible
	k8s.io/cluster-registry v0.0.6
	k8s.io/klog v1.0.0
	k8s.io/kube-openapi v0.0.0-20190709113604-33be087ad058
	k8s.io/kubernetes v1.14.2
	sigs.k8s.io/controller-runtime v0.1.12
)
