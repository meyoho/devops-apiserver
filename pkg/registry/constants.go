package registry

const (
	// ObjectMetaNameKey key for name
	ObjectMetaNameKey = "name"
	// ObjectMetaName table header name for Name
	ObjectMetaName = "Name"
	// TableTypeString type of column string
	TableTypeString = "string"
	// ObjectMetaCreationTimestampAge table header for Age
	ObjectMetaCreationTimestampAge = "Age"
	// ObjectMetaCreationTimestampKey key to find swagger doc for creationTimestamp
	ObjectMetaCreationTimestampKey = "creationTimestamp"

	// ResourceStatus table header name for Status
	ResourceStatus = "Status"
	// ResourceType table header name for Status
	ResourceType = "Type"
	// ResourceHost table header name for Host
	ResourceHost = "Host"
	// ResourceNamespaces table header name for Namespaces
	ResourceNamespaces = "Namespaces"
	// ResourceSecret table header name for Secret
	ResourceSecret = "Secret"
	// RepositoryRepository table header name for Repository
	RepositoryRepository = "Repository"
	// RepositoryEngine table header name for Engine
	RepositoryEngine = "Engine"
	// RepositoryCategory table header name for Category
	RepositoryCategory = "Category"
	// RepositoryVersion table header name for Version
	RepositoryVersion = "Version"
	// RepositoryAgent table header name for Agent
	RepositoryAgent = "Agent"
	// RepositoryDisplayName table header name for Display Name
	RepositoryDisplayName = "Display Name"
	// ToolName name of tool
	ToolName = "ToolName"
)
