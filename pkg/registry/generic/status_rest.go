package generic

import (
	"context"
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericregistry "k8s.io/apiserver/pkg/registry/generic/registry"
	"k8s.io/apiserver/pkg/registry/rest"
)

// NewStatusREST constructor for
func NewStatusREST(store *genericregistry.Store, newResourceFunc func() runtime.Object) (storage rest.StandardStorage, err error) {
	if store == nil {
		err = fmt.Errorf("StatusREST needs a Store")
		return
	}
	if newResourceFunc == nil {
		err = fmt.Errorf("StatusREST needs a resource constructor function")
		return
	}
	storage = &StatusREST{Store: store, newFunc: newResourceFunc}
	return
}

// StatusREST implements the REST endpoint for changing the status of a ToolBindingReplica
type StatusREST struct {
	*genericregistry.Store
	newFunc func() runtime.Object
}

// New constructs new return
func (r *StatusREST) New() runtime.Object {
	return r.newFunc()
}

// Get gets data
func (r *StatusREST) Get(ctx context.Context, name string, options *metav1.GetOptions) (runtime.Object, error) {
	return r.Store.Get(ctx, name, options)
}

// Update alters the status subset of an object.
func (r *StatusREST) Update(ctx context.Context, name string, objInfo rest.UpdatedObjectInfo, createValidation rest.ValidateObjectFunc, updateValidation rest.ValidateObjectUpdateFunc, forceAllowCreate bool, options *metav1.UpdateOptions) (runtime.Object, bool, error) {
	// We are explicitly setting forceAllowCreate to false in the call to the underlying storage because
	// subresources should never allow create on update.
	return r.Store.Update(ctx, name, objInfo, createValidation, updateValidation, false, options)
}
