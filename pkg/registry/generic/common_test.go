package generic_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/registry/generic"
	"github.com/stretchr/testify/assert"
)

func TestRemoveTrailingDash(t *testing.T) {
	result := generic.RemoveTrailingDash("http://aaaa/")
	assert.EqualValues(t, "http://aaaa", result, "remove trailing slash")

	result = generic.RemoveTrailingDash("http://aaaa/   ")
	assert.EqualValues(t, "http://aaaa", result, "remove trailing slash and spaces")

	result = generic.RemoveTrailingDash("http://xxxx   ")
	assert.EqualValues(t, "http://xxxx", result, "remove trailing spaces")
}
