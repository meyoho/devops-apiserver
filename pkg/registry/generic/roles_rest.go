package generic

import (
	"context"
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"k8s.io/apimachinery/pkg/api/errors"
	glog "k8s.io/klog"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
)

// GetRolesMappingFunc function to get a specific tool role mapping provided a dependency getter, context, instance name, and a list of options
type GetRolesMappingFunc func(dependency.Manager, context.Context, string, *devops.RoleMappingListOptions) (*devops.RoleMapping, error)

// ApplyRolesMappingFunc function to apply a role mapping to a specific tool provided a dependency getter, context, instance name and the target role mapping
type ApplyRolesMappingFunc func(dependency.Manager, context.Context, string, *devops.RoleMapping) (*devops.RoleMapping, error)

// RolesREST implements the roles endpoints for DevOps toolchain
type RolesREST struct {
	Name   string
	Getter dependency.Manager

	GetRolesMapping   GetRolesMappingFunc
	ApplyRolesMapping ApplyRolesMappingFunc
}

// ProjectREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&RolesREST{})

// New creates a new RoleMapping object
func (r *RolesREST) New() runtime.Object {
	return &devops.RoleMapping{}
}

// NewRolesREST starts a new project store
func NewRolesREST(
	name string,
	getter dependency.Manager,
	getFunc GetRolesMappingFunc,
	applyFunc ApplyRolesMappingFunc,
) rest.Storage {
	return &RolesREST{
		Name:              name,
		Getter:            getter,
		GetRolesMapping:   getFunc,
		ApplyRolesMapping: applyFunc,
	}
}

// NewGetOptions creates a new options object
func (r *RolesREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.RoleMappingListOptions{}, false, ""
}

// Get retrieves a runtime.Object that will stream the contents of projectmanagement
func (r *RolesREST) Get(ctx context.Context, name string, opts runtime.Object) (result runtime.Object, err error) {
	glog.V(7).Infof("RoleREST[%s] Get roles request for \"%s\"...", r.Name, name)
	defer func() {
		if err != nil {
			glog.Errorf("RoleREST[%s] error getting roles mapping for \"%s\". err: %#v", r.Name, name, err)
		}
	}()
	// validating get function
	if r.GetRolesMapping == nil {
		err = errors.NewInternalError(fmt.Errorf("Get Roles Mapping function not provided"))
		return
	}
	var (
		typedOpts = &devops.RoleMappingListOptions{}
		ok        bool
	)
	// sanity type check, should not happen in real cases
	typedOpts, ok = opts.(*devops.RoleMappingListOptions)
	if !ok {
		err = errors.NewBadRequest("Request options is of invalid type")
		return
	}
	result, err = r.GetRolesMapping(r.Getter, ctx, name, typedOpts)
	return
}

var _ = rest.NamedCreater(&RolesREST{})

// Create used to assign a role mapping to a platform
func (r *RolesREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (result runtime.Object, err error) {
	glog.V(7).Infof("RoleREST[%s] Apply roles request for \"%s\"...", r.Name, name)
	defer func() {
		if err != nil {
			glog.Errorf("RoleREST[%s] error applying roles mapping for \"%s\". err: %s", r.Name, name, err.Error())
		}
	}()
	if r.ApplyRolesMapping == nil {
		err = errors.NewInternalError(fmt.Errorf("Apply Roles Mapping function not provided"))
		return
	}
	var (
		roleMapping = &devops.RoleMapping{}
		ok          bool
	)
	// sanity type checking, should not happen in real cases
	roleMapping, ok = obj.(*devops.RoleMapping)
	if !ok {
		err = errors.NewBadRequest("Request payload is of invalid type")
		return
	}

	result, err = r.ApplyRolesMapping(r.Getter, ctx, name, roleMapping)
	return
}
