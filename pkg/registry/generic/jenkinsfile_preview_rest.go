package generic

import (
	"context"
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/util"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
	glog "k8s.io/klog"
)

// PreviewREST is jenkinsfile preview rest
type PreviewREST struct {
	Name                   string // name of previewrest instance
	Manager                dependency.Manager
	PipelineTemplateGetter func(ctx context.Context, getter dependency.Manager, name string) (*devops.PipelineTemplateRef, error)
	HandlePreviewOptions   func(ctx context.Context, getter dependency.Manager, name string, opts *devops.JenkinsfilePreviewOptions) error
}

var _ = rest.NamedCreater(&PreviewREST{})

// New will construct a JenkinsfilePreviewOptions instance
func (r *PreviewREST) New() runtime.Object {
	return &devops.JenkinsfilePreviewOptions{}
}

// NewPreviewREST will construct a previewREST instance
func NewPreviewREST(
	name string,
	getter dependency.Manager,
	pipelineTemplateGetter func(ctx context.Context, getter dependency.Manager, name string) (*devops.PipelineTemplateRef, error),
) *PreviewREST {
	return &PreviewREST{
		Name:                   name,
		Manager:                getter,
		PipelineTemplateGetter: pipelineTemplateGetter,
	}
}

func (p *PreviewREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (runtime.Object, error) {
	previewOpt, ok := obj.(*devops.JenkinsfilePreviewOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", previewOpt)
	}

	if p.HandlePreviewOptions != nil {
		err := p.HandlePreviewOptions(ctx, p.Manager, name, previewOpt)
		if err != nil {
			return nil, err
		}
	}

	if p.PipelineTemplateGetter == nil {
		glog.Errorf("[%s] not implement PipelineTemplateGetter", p.Name)
		return nil, fmt.Errorf("Not Implement PipelineTemplateGetter")
	}

	templateRef, err := p.PipelineTemplateGetter(ctx, p.Manager, name)
	if err != nil {
		return nil, err
	}
	pipelineTemplate, err := p.getPipelineTemplate(ctx, templateRef.Kind, templateRef.Name)
	if err != nil {
		return nil, err
	}

	taskTemplates := []devops.PipelineTaskTemplateInterface{}
	for _, stages := range pipelineTemplate.GetPiplineTempateSpec().Stages {
		for _, task := range stages.Tasks {
			taskTemplate, err := p.getPipelineTaskTemplate(ctx, task.Kind, task.Name)
			if err != nil {
				glog.Errorf("[%s] Get PipelineTaskTemplate error, kind:%s , name:%s, context:%#v", p.Name, task.Kind, task.Name, ctx)
				return nil, err
			}
			taskTemplates = append(taskTemplates, taskTemplate)
		}
	}

	for _, tasks := range pipelineTemplate.GetPiplineTempateSpec().Post {
		for _, task := range tasks {
			taskTemplate, err := p.getPipelineTaskTemplate(ctx, task.Kind, task.Name)
			if err != nil {
				glog.Errorf("[%s] Get PipelineTaskTemplate error, kind:%s , name:%s, context:%#v", p.Name, task.Kind, task.Name, ctx)
				return nil, err
			}
			taskTemplates = append(taskTemplates, taskTemplate)
		}
	}

	glog.V(3).Infof("Render Jenkinsfile by template: %s, values:%#v", pipelineTemplate, previewOpt.Values)

	jenkinsfile, err := util.PreviewJenkinsfile(pipelineTemplate, taskTemplates, previewOpt)
	if err != nil {
		err = fmt.Errorf("error happened in the process of genereate jenkinsfile: %s", err.Error())
		glog.Error(err.Error())
		return nil, err
	}

	return &devops.JenkinsfilePreview{
		Jenkinsfile: jenkinsfile,
	}, err
}

func (p *PreviewREST) getPipelineTemplate(ctx context.Context, kind, name string) (devops.PipelineTemplateInterface, error) {

	if kind == devops.TypeClusterPipelineTemplate {
		// when resource is cluster resource, no need to pass namespace
		ctx = genericapirequest.NewContext()
	}

	dp := p.Manager.Get(ctx, kind, name)
	err := dp.Validate()
	if err != nil {
		glog.Errorf("[%s] get pipelinetemplate %s/%s error, context:%#v, err:%s", p.Name, kind, name, ctx, err.Error())
		return nil, err
	}

	var template = devops.NewPipelineTemplate(kind)
	dpList := dp.GetInto(kind, template.(runtime.Object))
	err = dpList.Validate()
	if err != nil {
		glog.Errorf("[%s] get pipelinetemplate %s error, context:%#v, err:%s", p.Name, name, ctx, err.Error())
		return nil, err
	}

	return template, nil
}

// getPipelineTaskTemplate get pipelineTaskTemplate according name
func (p *PreviewREST) getPipelineTaskTemplate(ctx context.Context, kind, name string) (taskTemplate devops.PipelineTaskTemplateInterface, err error) {
	if kind == devops.TypeClusterPipelineTaskTemplate {
		// when resource is cluster resource, no need to pass namespace
		ctx = genericapirequest.NewContext()
	}

	depItem := p.Manager.Get(ctx, kind, name)
	err = depItem.Validate()
	if err != nil {
		return nil, err
	}

	taskTemplate = devops.NewPipelineTaskTemplate(kind)
	depItemList := depItem.GetInto(kind, taskTemplate.(runtime.Object))
	err = depItemList.Validate()
	return
}
