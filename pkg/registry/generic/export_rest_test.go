package generic_test

import (
	"context"
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mdependency "alauda.io/devops-apiserver/pkg/mock/dependency"
	"alauda.io/devops-apiserver/pkg/registry/generic"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("ExportREST.Get", func() {

	var (
		ctrl                *gomock.Controller
		getter              *mdependency.MockManager
		rest                *generic.ExportREST
		clusterpipelinename string
		clonetaskname       string
		buildtaskname       string
		exportshowoption    *devops.ExportShowOptions
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		getter = mdependency.NewMockManager(ctrl)
		clusterpipelinename = "test"
		clonetaskname = "clone"
		buildtaskname = "build-docker"
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	It("Type ClusterPipelinetemplate  should return the exports that we wants", func() {
		rest = generic.NewExportREST(clusterpipelinename, getter, devops.TypeClusterPipelineTemplate).(*generic.ExportREST)

		cloneparamer := devops.GlobalParameter{
			Name: "clone",
			Description: &devops.I18nName{
				Zh: "克隆代码",
				En: "clone",
			},
		}
		buildparamer := devops.GlobalParameter{
			Name: "build-docker",
			Description: &devops.I18nName{
				Zh: "构建镜像",
				En: "build-docker",
			},
		}
		clonetasktemplate := &devops.ClusterPipelineTaskTemplate{
			Spec: devops.PipelineTaskTemplateSpec{
				Exports: []devops.GlobalParameter{
					cloneparamer,
				},
			},
		}
		buildasktemplate := &devops.ClusterPipelineTaskTemplate{
			Spec: devops.PipelineTaskTemplateSpec{
				Exports: []devops.GlobalParameter{
					buildparamer,
				},
			},
		}
		clusterpipelinetemplate := &devops.ClusterPipelineTemplate{
			Spec: devops.PipelineTemplateSpec{
				Stages: []devops.PipelineStage{
					devops.PipelineStage{
						Tasks: []devops.PipelineTemplateTask{
							devops.PipelineTemplateTask{
								Kind: devops.TypeClusterPipelineTaskTemplate,
								Name: clonetaskname,
							},
							devops.PipelineTemplateTask{
								Kind: devops.TypeClusterPipelineTaskTemplate,
								Name: buildtaskname,
							},
						},
					},
				},
			},
		}

		ctx := context.TODO()
		getter.EXPECT().
			Get(ctx, devops.TypeClusterPipelineTemplate, clusterpipelinename).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypeClusterPipelineTemplate,
					Object:   clusterpipelinetemplate,
					IntoFunc: dependency.ClusterPipelineTemplateCopyInto,
				},
			})

		getter.EXPECT().
			Get(ctx, devops.TypeClusterPipelineTaskTemplate, clonetaskname).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypeClusterPipelineTaskTemplate,
					Object:   clonetasktemplate,
					IntoFunc: dependency.ClusterPipelineTaskTemplateCopyInto,
				},
			})
		getter.EXPECT().
			Get(ctx, devops.TypeClusterPipelineTaskTemplate, buildtaskname).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypeClusterPipelineTaskTemplate,
					Object:   buildasktemplate,
					IntoFunc: dependency.ClusterPipelineTaskTemplateCopyInto,
				},
			})

		exportshowoption = &devops.ExportShowOptions{
			TaskName: clonetaskname,
		}
		PipelineExportedVariables, err := rest.GetTemplateExport(getter, context.TODO(), clusterpipelinename, exportshowoption)
		Expect(err).To(BeNil())
		Expect(PipelineExportedVariables).ToNot(BeNil())
		Expect(PipelineExportedVariables.Values).To(Equal([]devops.GlobalParameter{
			cloneparamer,
		}))
	})

	It("Type Pipelinetemplate should return the exports that we wants", func() {
		rest = generic.NewExportREST(clusterpipelinename, getter, devops.TypePipelineTemplate).(*generic.ExportREST)

		cloneparamer := devops.GlobalParameter{
			Name: "clone",
			Description: &devops.I18nName{
				Zh: "克隆代码",
				En: "clone",
			},
		}
		formatcloneparamerresult := devops.GlobalParameter{
			Name: "${clone}",
			Description: &devops.I18nName{
				Zh: "克隆代码",
				En: "clone",
			},
		}
		buildparamer := devops.GlobalParameter{
			Name: "build-docker",
			Description: &devops.I18nName{
				Zh: "构建镜像",
				En: "build-docker",
			},
		}
		clonetasktemplate := &devops.PipelineTaskTemplate{
			Spec: devops.PipelineTaskTemplateSpec{
				Exports: []devops.GlobalParameter{
					cloneparamer,
				},
			},
		}
		buildasktemplate := &devops.PipelineTaskTemplate{
			Spec: devops.PipelineTaskTemplateSpec{
				Exports: []devops.GlobalParameter{
					buildparamer,
				},
			},
		}
		pipelinetemplate := &devops.PipelineTemplate{
			Spec: devops.PipelineTemplateSpec{
				Stages: []devops.PipelineStage{
					devops.PipelineStage{
						Tasks: []devops.PipelineTemplateTask{
							devops.PipelineTemplateTask{
								Kind: devops.TypePipelineTaskTemplate,
								Name: clonetaskname,
							},
							devops.PipelineTemplateTask{
								Kind: devops.TypePipelineTaskTemplate,
								Name: buildtaskname,
							},
						},
					},
				},
			},
		}

		ctx := context.TODO()
		getter.EXPECT().
			Get(ctx, devops.TypePipelineTemplate, clusterpipelinename).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypePipelineTemplate,
					Object:   pipelinetemplate,
					IntoFunc: dependency.PipelineTemplateCopyInto,
				},
			})

		getter.EXPECT().
			Get(ctx, devops.TypePipelineTaskTemplate, clonetaskname).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypePipelineTaskTemplate,
					Object:   clonetasktemplate,
					IntoFunc: dependency.PipelineTaskTemplateCopyInto,
				},
			})
		getter.EXPECT().
			Get(ctx, devops.TypePipelineTaskTemplate, buildtaskname).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypePipelineTaskTemplate,
					Object:   buildasktemplate,
					IntoFunc: dependency.PipelineTaskTemplateCopyInto,
				},
			})

		exportshowoption = &devops.ExportShowOptions{
			TaskName:    clonetaskname,
			FormatValue: "${%s}",
		}
		PipelineExportedVariables, err := rest.GetTemplateExport(getter, context.TODO(), clusterpipelinename, exportshowoption)
		Expect(err).To(BeNil())
		fmt.Println(err)
		Expect(PipelineExportedVariables).ToNot(BeNil())
		Expect(PipelineExportedVariables.Values).To(Equal([]devops.GlobalParameter{
			formatcloneparamerresult,
		}))
	})

})
