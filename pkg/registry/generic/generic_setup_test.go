package generic

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestGenericTests(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("generic.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/registry/generic", []Reporter{junitReporter})
}
