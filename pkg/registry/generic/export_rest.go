package generic

import (
	"context"

	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	glog "k8s.io/klog"
)

type ExportREST struct {
	// Name is name of ExportREST
	Name   string
	Getter dependency.Manager
	// TemplateKind is kind of pipeline template
	TemplateKind string
}

var _ = rest.GetterWithOptions(&ExportREST{})

func (r *ExportREST) New() runtime.Object {
	return &devops.ExportShowOptions{}
}

func NewExportREST(
	name string,
	getter dependency.Manager,
	templateKind string,
) rest.Storage {
	return &ExportREST{
		Name:         name,
		Getter:       getter,
		TemplateKind: templateKind,
	}
}

func (r *ExportREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ExportShowOptions{}, false, ""
}

func (r *ExportREST) Get(ctx context.Context, name string, opts runtime.Object) (result runtime.Object, err error) {
	glog.V(7).Infof("ExportREST[%s] Get export request for \"%s\"...", r.Name, name)
	defer func() {
		if err != nil {
			glog.Errorf("ExportREST[%s] error getting export values for \"%s\". err: %#v", r.Name, name, err)
		}
	}()
	var (
		typedOpts = &devops.ExportShowOptions{}
		ok        bool
	)
	// sanity type check, should not happen in real cases
	typedOpts, ok = opts.(*devops.ExportShowOptions)
	if !ok {
		err = errors.NewBadRequest("Request options is of invalid type")
		return
	}
	result, err = r.GetTemplateExport(r.Getter, ctx, name, typedOpts)
	return
}

func (r *ExportREST) GetTemplateExport(getter dependency.Manager, context context.Context, name string, exportshowoption *devops.ExportShowOptions) (*devops.PipelineExportedVariables, error) {

	exports := devops.PipelineExportedVariables{}
	depItemList := getter.Get(context, r.TemplateKind, name)
	if err := depItemList.Validate(); err != nil {
		return nil, err
	}

	template := devops.NewPipelineTemplate(r.TemplateKind)
	depItemList.GetInto(r.TemplateKind, template)
	Stages := template.GetPiplineTempateSpec().Stages

	format := exportshowoption.FormatValue
	if format == "" {
		format = "%s"
	}
	tasknamewithexportval := map[string][]devops.GlobalParameter{}
	for _, stage := range Stages {
		for _, task := range stage.Tasks {

			taskTemplate := devops.NewPipelineTaskTemplate(task.Kind)
			depItemList := getter.Get(context, task.Kind, task.Name)
			depItemList.GetInto(task.Kind, taskTemplate)
			tasktemplatexports := taskTemplate.GetPiplineTaskTempateSpec().Exports

			valueexports := []devops.GlobalParameter{}
			for _, taskexportvalue := range tasktemplatexports {
				exportvalue := devops.GlobalParameter{
					Name:        fmt.Sprintf(format, taskexportvalue.Name),
					Description: taskexportvalue.Description,
				}
				valueexports = append(valueexports, exportvalue)

			}
			tasknamewithexportval[task.Name] = valueexports

		}
	}

	exportvalues := []devops.GlobalParameter{}
	taskname := exportshowoption.TaskName

	getalltask := true
	if taskname != "" {
		getalltask = false
	}

	if !getalltask {
		exportvalues = append(exportvalues, tasknamewithexportval[taskname]...)
	} else {
		for _, namewithval := range tasknamewithexportval {
			exportvalues = append(exportvalues, namewithval...)
		}
	}
	exports.Values = exportvalues

	return &exports, nil
}
