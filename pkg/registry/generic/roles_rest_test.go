package generic_test

import (
	"context"
	"fmt"

	"github.com/golang/mock/gomock"
	"k8s.io/apimachinery/pkg/api/errors"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mdependency "alauda.io/devops-apiserver/pkg/mock/dependency"
	"alauda.io/devops-apiserver/pkg/registry/generic"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("RolesREST.Get", func() {

	var (
		ctrl      *gomock.Controller
		getter    *mdependency.MockManager
		name      string
		getFunc   generic.GetRolesMappingFunc
		applyFunc generic.ApplyRolesMappingFunc
		rest      *generic.RolesREST
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		getter = mdependency.NewMockManager(ctrl)
		name = "test"
		getFunc = func(getter dependency.Manager, ctx context.Context, resName string, opts *devops.RoleMappingListOptions) (*devops.RoleMapping, error) {
			return &devops.RoleMapping{}, nil
		}
	})

	JustBeforeEach(func() {
		rest = generic.NewRolesREST(name, getter, getFunc, applyFunc).(*generic.RolesREST)
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	It("should run the whole method and return functions result", func() {

		opts, _, _ := rest.NewGetOptions()
		result, err := rest.Get(context.TODO(), "somename", opts)

		Expect(err).To(BeNil())
		Expect(result).ToNot(BeNil())
		Expect(result).To(Equal(&devops.RoleMapping{}))
	})

	It("should return error on type check", func() {

		result, err := rest.Get(context.TODO(), "somename", &devops.RoleMapping{})

		Expect(err).ToNot(BeNil())
		Expect(err).To(Equal(errors.NewBadRequest("Request options is of invalid type")), "should return BadRequest error")
		Expect(result).To(BeNil())
	})
	Context("getFunc is nil", func() {
		BeforeEach(func() {
			getFunc = nil
		})
		It("should return error when getFunc is nil", func() {
			result, err := rest.Get(context.TODO(), "somename", &devops.RoleMappingListOptions{})

			Expect(err).ToNot(BeNil())
			Expect(err).To(Equal(errors.NewInternalError(fmt.Errorf("Get Roles Mapping function not provided"))), "should return InternalServerError")
			Expect(result).To(BeNil(), "result should be nil on error")
		})
	})

})

var _ = Describe("RolesREST.Create", func() {

	var (
		ctrl      *gomock.Controller
		getter    *mdependency.MockManager
		name      string
		getFunc   generic.GetRolesMappingFunc
		applyFunc generic.ApplyRolesMappingFunc
		rest      *generic.RolesREST
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		getter = mdependency.NewMockManager(ctrl)
		name = "test"

		applyFunc = func(getter dependency.Manager, ctx context.Context, resName string, data *devops.RoleMapping) (*devops.RoleMapping, error) {
			return &devops.RoleMapping{}, nil
		}
	})

	JustBeforeEach(func() {
		rest = generic.NewRolesREST(name, getter, getFunc, applyFunc).(*generic.RolesREST)
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	It("should run the whole method and return functions result", func() {

		payload := rest.New()
		result, err := rest.Create(context.TODO(), "somename", payload, nil, nil)

		Expect(err).To(BeNil())
		Expect(result).ToNot(BeNil())
		Expect(result).To(Equal(&devops.RoleMapping{}))
	})

	It("should return error on type check", func() {

		result, err := rest.Create(context.TODO(), "somename", &devops.RoleMappingListOptions{}, nil, nil)

		Expect(err).ToNot(BeNil())
		Expect(err).To(Equal(errors.NewBadRequest("Request payload is of invalid type")), "should return BadRequest error")
		Expect(result).To(BeNil())
	})
	Context("applyFunc is nil", func() {
		BeforeEach(func() {
			applyFunc = nil
		})
		It("should return error when applyFunc is nil", func() {
			result, err := rest.Create(context.TODO(), "somename", nil, nil, nil)

			Expect(err).ToNot(BeNil())
			Expect(err).To(Equal(errors.NewInternalError(fmt.Errorf("Apply Roles Mapping function not provided"))), "should return InternalServerError")
			Expect(result).To(BeNil(), "result should be nil on error")
		})
	})

})
