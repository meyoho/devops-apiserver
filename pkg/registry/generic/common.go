package generic

import (
	"context"
	"strings"
	"time"

	"gopkg.in/src-d/go-git.v4/plumbing/object"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/generic"
	genericregistry "k8s.io/apiserver/pkg/registry/generic/registry"
	"k8s.io/apiserver/pkg/registry/rest"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/kubernetes/pkg/printers"
	printerstorage "k8s.io/kubernetes/pkg/printers/storage"
)

// ResourceGetter is an interface for retrieving resources by ResourceLocation.
type ResourceGetter interface {
	Get(context.Context, string, *metav1.GetOptions) (runtime.Object, error)
}

// RemoveTrailingDash removes a trailling / and spaces if present in URL
func RemoveTrailingDash(url string) string {
	return strings.TrimRight(strings.TrimRight(url, " "), "/")
}

// InitObject function signature to initiate a runtime.Object
type InitObject func() runtime.Object

// PredicateFunc function signature for predicate function
type PredicateFunc func(label labels.Selector, field fields.Selector) storage.SelectionPredicate

// PrinterAddHandler function signature to add printer handler
type PrinterAddHandler func(h printers.PrintHandler)

// StoreFactory function signature to create a store
type StoreFactory func(
	optsGetter generic.RESTOptionsGetter,
	resourceName string,
	newObjFunc InitObject,
	newListFunc InitObject,
	predicateFunc PredicateFunc,
	getAttrsFunc storage.AttrFunc,
	createStrategy rest.RESTCreateStrategy,
	updateStrategy rest.RESTUpdateStrategy,
	deleteStrategy rest.RESTDeleteStrategy,
	printerAddHandler PrinterAddHandler) (*genericregistry.Store, error)

// NewStandardStore initiates a new store
func NewStandardStore(
	optsGetter generic.RESTOptionsGetter,
	resourceName string,
	newObjFunc InitObject,
	newListFunc InitObject,
	predicateFunc PredicateFunc,
	getAttrsFunc storage.AttrFunc,
	createStrategy rest.RESTCreateStrategy,
	updateStrategy rest.RESTUpdateStrategy,
	deleteStrategy rest.RESTDeleteStrategy,
	printerAddHandler PrinterAddHandler) (*genericregistry.Store, error) {

	store := &genericregistry.Store{
		NewFunc:                  newObjFunc,
		NewListFunc:              newListFunc,
		PredicateFunc:            predicateFunc,
		DefaultQualifiedResource: devops.Resource(resourceName),

		CreateStrategy: createStrategy,
		UpdateStrategy: updateStrategy,
		DeleteStrategy: deleteStrategy,
	}
	if printerAddHandler != nil {
		store.TableConvertor = printerstorage.TableConvertor{TablePrinter: printers.NewTablePrinter().With(printerAddHandler)}
	}
	options := &generic.StoreOptions{RESTOptions: optsGetter, AttrFunc: getAttrsFunc}
	if err := store.CompleteWithOptions(options); err != nil {
		return nil, err
	}
	return store, nil
}

// NewFakeStore return an uncompleted store.
// FOR TESTS ONLY! do not use this on production code
func NewFakeStore(optsGetter generic.RESTOptionsGetter,
	resourceName string,
	newObjFunc InitObject,
	newListFunc InitObject,
	predicateFunc PredicateFunc,
	getAttrsFunc storage.AttrFunc,
	createStrategy rest.RESTCreateStrategy,
	updateStrategy rest.RESTUpdateStrategy,
	deleteStrategy rest.RESTDeleteStrategy,
	printerAddHandler PrinterAddHandler) (*genericregistry.Store, error) {
	store := &genericregistry.Store{
		NewFunc:       newObjFunc,
		NewListFunc:   newListFunc,
		PredicateFunc: predicateFunc,
	}
	return store, nil
}

// GetNowAsString useful for all components
func GetNowAsString() string {
	return metav1.Now().Format(time.RFC3339)
}

func SetUpdatedAtAsNow(obj object.Object) {

}
