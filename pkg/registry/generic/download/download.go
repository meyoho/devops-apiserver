package download

import (
	"context"
	klog "github.com/golang/glog"
	"io"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apiserver/pkg/registry/rest"
)

var _ rest.ResourceStreamer = &ArtifactFile{}

func (obj *ArtifactFile) GetObjectKind() schema.ObjectKind {
	return schema.EmptyObjectKind
}
func (obj *ArtifactFile) DeepCopyObject() runtime.Object {
	panic("generic.ArtifactFile does not implement DeepCopyObject")
}

//ArtifactFile represent the response info for download file

type ArtifactFile struct {
	metav1.TypeMeta
	Project     string
	Name        string
	ID          string
	FileName    string
	DownloadAll bool
	Downloader  Downloader
}

type Downloader interface {
	DownloadArtifact(context context.Context, project string, name string, id string, filename string) (body io.ReadCloser, contenttype string, err error)
	DownloadArtifacts(context context.Context, project string, name string, id string) (body io.ReadCloser, contenttype string, err error)
}

func (Aritfact *ArtifactFile) InputStream(ctx context.Context, apiVersion, acceptHeader string) (stream io.ReadCloser, flush bool, contentType string, err error) {

	if Aritfact.DownloadAll {
		stream, contentType, err = Aritfact.Downloader.DownloadArtifacts(context.Background(), Aritfact.Project, Aritfact.Name, Aritfact.ID)
	} else {
		stream, contentType, err = Aritfact.Downloader.DownloadArtifact(context.Background(), Aritfact.Project, Aritfact.Name, Aritfact.ID, Aritfact.FileName)
	}

	if err != nil {
		klog.Errorf("err happend when InputStream get stream err is %v", err)

		return nil, false, contentType, err
	}
	return stream, false, contentType, nil
}
