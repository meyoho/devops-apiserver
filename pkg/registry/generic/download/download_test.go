package download_test

import (
	mocks "alauda.io/devops-apiserver/pkg/mock/download"
	"alauda.io/devops-apiserver/pkg/registry/generic/download"
	"context"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
)

type readclose struct {
	data string
}

func (readclose) Read(p []byte) (n int, err error) {
	return 0, nil
}
func (readclose) Close() error {
	return nil
}

var _ = Describe("Download", func() {

	var mockCtrl = gomock.NewController(GinkgoT())
	var atrifact = mocks.NewMockDownloader(mockCtrl)

	It("test for download file", func() {

		atrifact.EXPECT().DownloadArtifact(context.Background(), "project", "name", "id", "filename").Return(readclose{}, "application/json", nil)
		atrifact.EXPECT().DownloadArtifacts(context.Background(), "project", "name", "id").Return(readclose{}, "application/text", nil)

		artifactfile := download.ArtifactFile{
			Project:     "project",
			Name:        "name",
			ID:          "id",
			FileName:    "filename",
			DownloadAll: false,
			Downloader:  atrifact,
		}
		stream, _, contenttype, _ := artifactfile.InputStream(context.Background(), "", "")
		gomega.Expect(stream).To(gomega.Equal(readclose{}))
		gomega.Expect(contenttype).To(gomega.Equal("application/json"))

	})

	It("test for download all archive", func() {
		atrifact.EXPECT().DownloadArtifact(context.Background(), "project", "name", "id", "filename").Return(readclose{}, "application/json", nil)
		atrifact.EXPECT().DownloadArtifacts(context.Background(), "project", "name", "id").Return(readclose{}, "application/text", nil)

		artifactfile := download.ArtifactFile{
			Project:     "project",
			Name:        "name",
			ID:          "id",
			FileName:    "",
			DownloadAll: true,
			Downloader:  atrifact,
		}
		stream, _, contenttype, _ := artifactfile.InputStream(context.Background(), "", "")
		gomega.Expect(stream).To(gomega.Equal(readclose{}))
		gomega.Expect(contenttype).To(gomega.Equal("application/text"))
	})

})
