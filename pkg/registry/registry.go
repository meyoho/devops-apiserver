/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package registry

import (
	"fmt"

	genericregistry "k8s.io/apiserver/pkg/registry/generic/registry"
	"k8s.io/apiserver/pkg/registry/rest"
)

var _ rest.ShortNamesProvider = &REST{}

// REST implements a RESTStorage for API services against etcd
type REST struct {
	*genericregistry.Store
	Short           []string
	AddedCategories []string
}

// certifies that it implements the interface
var _ rest.ShortNamesProvider = &REST{}

// ShortNames return short names used for the resource
func (r *REST) ShortNames() []string {
	return r.Short
}

// certifies that it implements the interface
var _ rest.CategoriesProvider = &REST{}

// Categories return categories for the resource
func (r *REST) Categories() []string {
	return r.AddedCategories
}

// RESTInPeace is just a simple function that panics on error.
// Otherwise returns the given storage object. It is meant to be
// a wrapper for devops registries.
func RESTInPeace(storage rest.StandardStorage, err error) rest.StandardStorage {
	if err != nil {
		err = fmt.Errorf("unable to create REST storage for a resource due to %v, will die", err)
		panic(err)
	}
	return storage
}

// RESTInPeaceStorage is just a simple function that panics on error.
// Otherwise returns the given storage object. It is meant to be
// a wrapper for devops registries.
func RESTInPeaceStorage(storage rest.Storage, err error) rest.Storage {
	if err != nil {
		err = fmt.Errorf("unable to create REST storage for a resource due to %v, will die", err)
		panic(err)
	}
	return storage
}
