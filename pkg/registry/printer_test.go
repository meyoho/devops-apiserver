package registry

import (
	"testing"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestTranslateTimestamp(t *testing.T) {
	assert := assert.New(t)

	now := metav1.NewTime(time.Now().Add(-time.Hour))
	result := TranslateTimestamp(now)
	assert.EqualValues("60m", result)

	zero := metav1.NewTime(time.Time{})
	result = TranslateTimestamp(zero)
	assert.EqualValues("<unknown>", result)
}

func TestPrintObjectMeta(t *testing.T) {
	assert := assert.New(t)

	now := metav1.NewTime(time.Now().Add(-time.Hour))
	pipeline := &devops.Pipeline{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "pipeline",
			CreationTimestamp: now,
		},
	}
	result, err := PrintObjectMeta(pipeline, printers.PrintOptions{Wide: true})
	assert.EqualValues(nil, err)
	assert.EqualValues([]metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: pipeline},
			Cells:  []interface{}{pipeline.Name, TranslateTimestamp(pipeline.CreationTimestamp)},
		},
	}, result)
}

func TestGetPipelineNames(t *testing.T) {
	assert := assert.New(t)

	obj := metav1.ObjectMeta{
		Annotations: map[string]string{
			"displayName.en": "english-name",
			"version":        "version",
		},
		Labels: map[string]string{
			"category": "cat",
		},
	}
	display, version, category := GetPipelineNames(obj)
	assert.EqualValues("english-name", display)
	assert.EqualValues("version", version)
	assert.EqualValues("cat", category)

	display, version, category = GetPipelineNames(metav1.ObjectMeta{
		Annotations: map[string]string{
			"displayName.zh-CN": "chinese-name",
		},
	})
	assert.EqualValues("chinese-name", display)
	assert.EqualValues("-", version)
	assert.EqualValues("-", category)

	display, version, category = GetPipelineNames(metav1.ObjectMeta{
		Name: "name",
	})
	assert.EqualValues("name", display)
	assert.EqualValues("-", version)
	assert.EqualValues("-", category)
}
