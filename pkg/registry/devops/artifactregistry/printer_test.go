package artifactregistry

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestArtifactRegistryPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	arObj := &devops.ArtifactRegistry{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "ar-1",
			CreationTimestamp: now,
		},
		Spec: devops.ArtifactRegistrySpec{
			Type:                 "maven2",
			Public:               true,
			ArtifactRegistryName: "test-ar-maven",
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "nexus.io",
				},
			},

			ArtifactRegistryArgs: map[string]string{
				"type": "hosted",
			},
		},
		Status: devops.ServiceStatus{
			Phase: devops.ServiceStatusPhaseReady,
		},
	}
	list := &devops.ArtifactRegistryList{
		Items: []devops.ArtifactRegistry{*arObj},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: arObj},
			Cells: []interface{}{
				arObj.Name, arObj.Spec.Type, arObj.Spec.ArtifactRegistryName, arObj.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printArtifactRegistryList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing registry: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
