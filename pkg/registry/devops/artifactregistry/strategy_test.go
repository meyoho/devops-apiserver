package artifactregistry_test

import (
	"alauda.io/devops-apiserver/pkg/registry/devops/artifactregistry"
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestStrategy(t *testing.T) {
	Schema := runtime.NewScheme()
	strategy := artifactregistry.NewStrategy(Schema)

	if strategy.NamespaceScoped() {
		t.Errorf("Artifactregistry strategy should not be namespace scoped")
	}

	if strategy.AllowCreateOnUpdate() {
		t.Errorf("Artifactregistry strategy should not allow create on update")
	}

	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("Artifactregistry strategy should not allow unconditional update")
	}

	type Table struct {
		name     string
		new      *devopsv1alpha1.ArtifactRegistry
		old      *devopsv1alpha1.ArtifactRegistry
		expected *devopsv1alpha1.ArtifactRegistry
		method   func(*Table)
	}

	table := []Table{
		{
			name: "simple",
			new: &devopsv1alpha1.ArtifactRegistry{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.ArtifactRegistrySpec{
					ArtifactRegistryName: "test-maven",
				},
			},
			old: nil,
			expected: &devopsv1alpha1.ArtifactRegistry{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.ArtifactRegistrySpec{},
			},
			method: func(tb *Table) {
				strategy.PrepareForCreate(nil, tb.new)

				err := strategy.Validate(nil, tb.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input did not generate error: %v",
						tb.name, err,
					)
				}
			},
		},
	}

	for _, tb := range table {
		tb.method(&tb)
	}
}
