package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
	glog "k8s.io/klog"
)

var secretFilename = "arsecret"

// SecretREST implements the secret operation for a service
type SecretREST struct {
	Getter          dependency.Manager
	SecretLister    corev1listers.SecretLister
	RoundTripper    http.RoundTripper
	systemNamespace string
}

// NewRepositoryREST starts a new log store
func NewSecretREST(
	getter dependency.Manager,
	secretLister corev1listers.SecretLister,
	roundTripper http.RoundTripper,
	systemNamespace string,
) rest.Storage {
	return &SecretREST{
		Getter:          getter,
		SecretLister:    secretLister,
		RoundTripper:    roundTripper,
		systemNamespace: systemNamespace,
	}
}

// SecretREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&SecretREST{})

// New creates a new secret options object
func (r *SecretREST) New() runtime.Object {
	return &devops.CodeRepoServiceAuthorizeResponse{}
}

// ProducesObject SecretREST implements StorageMetadata, return CodeRepoServiceAuthorizeResponse as the generating object
func (r *SecretREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.CodeRepoServiceAuthorizeResponse{}
}

// Get retrieves a runtime.Object that will stream the contents of jenkins
func (r *SecretREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {

	glog.V(9).Infof("%s opt is %#v", secretFilename, opts)

	validateOptions, ok := opts.(*devops.CodeRepoServiceAuthorizeOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}

	var err error

	dependencies := r.Getter.Get(ctx, devops.TypeArtifactRegistry, name)
	if err = dependencies.Validate(); err != nil {
		return nil, err
	}

	glog.V(9).Infof("%s get dependency successful", secretFilename)

	ar := &devops.ArtifactRegistry{}
	secret := &v1.Secret{}
	dependencies.GetInto(devops.TypeArtifactRegistry, ar).GetInto(devops.TypeSecret, secret)

	glog.V(9).Infof("%s ar is %#v", secretFilename, ar)
	glog.V(9).Infof("%s ar`s secret is %#v", secretFilename, secret)

	if validateOptions.SecretName != "" {
		optSecret, err := r.SecretLister.Secrets(validateOptions.Namespace).Get(validateOptions.SecretName)
		if err != nil {
			return nil, err
		}

		if optSecret != nil {
			secret = optSecret
		}
		glog.V(9).Infof("%s sectet with opts is %#v", secretFilename, secret)
	}

	ownerRfrList := ar.OwnerReferences

	artifactType := ar.Spec.Type

	if len(ownerRfrList) > 0 {
		managerName := ownerRfrList[0].Name
		dependencies := r.Getter.Get(ctx, devops.TypeArtifactRegistryManager, managerName)
		if err = dependencies.Validate(); err != nil {
			return nil, err
		}
		manager := &devops.ArtifactRegistryManager{}
		secret := &v1.Secret{}
		dependencies.GetInto(devops.TypeArtifactRegistryManager, manager).GetInto(devops.TypeSecret, secret)
		glog.V(9).Infof("%s manager type is %s and host is %s", secretFilename, manager.Spec.Type.String(), manager.Spec.HTTP.Host)
		artifactType = manager.Spec.Type.String()

	}

	basicAuth := k8s.GetDataBasicAuthFromSecret(secret)
	glog.V(9).Infof("%s user is %s and password is %s", secretFilename, basicAuth.Username, basicAuth.Password)
	//secret end

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(ar.Spec.HTTP.Host),
		devopsclient.NewBasicAuth(basicAuth.Username, basicAuth.Password),
		devopsclient.NewTransport(r.RoundTripper),
	)

	serviceClient, err := factory.NewClient(
		artifactType,
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}
	status, err := serviceClient.Authenticate(ctx)

	glog.V(9).Infof("%s status is %#v", secretFilename, status)

	if err != nil {
		return nil, errors.NewBadRequest(fmt.Errorf("authorize failed: %v", err).Error())
	}
	if status != nil && status.StatusCode >= 400 {
		return nil, errors.NewBadRequest(fmt.Errorf("username or password in secret '%s' is incorrect", secret.GetName()).Error())
	}

	var validateResponse = &devops.CodeRepoServiceAuthorizeResponse{}
	return validateResponse, err

}

// NewGetOptions creates a new options object
func (r *SecretREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoServiceAuthorizeOptions{}, false, ""
}
