package rest

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	internalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/registry/generic"
	"alauda.io/devops-apiserver/pkg/role"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	k8srequest "k8s.io/apiserver/pkg/endpoints/request"
	glog "k8s.io/klog"
)

var roleFilename = "arrole"

type ArtifactRegistryRolesOperation struct {
	generic.GetRolesMappingFunc
	generic.ApplyRolesMappingFunc
	Factory         internalthirdparty.ArtifactRegistryClientFactory
	systemNamespace string
}

func NewArtifactRegistryRolesOperation(transport http.RoundTripper, systemNamespace string) ArtifactRegistryRolesOperation {
	factory := internalthirdparty.NewArtifactRegistryClientFactory()
	factory.SetTransport(transport)
	arroleoperation := ArtifactRegistryRolesOperation{}
	arroleoperation.Factory = factory
	arroleoperation.GetRolesMappingFunc = arroleoperation.ArtifactRegistryGetRolesMappingFunc
	arroleoperation.ApplyRolesMappingFunc = arroleoperation.ArtifactRegistryApplyRolesMappingFunc
	arroleoperation.systemNamespace = systemNamespace
	return arroleoperation
}
func (r *ArtifactRegistryRolesOperation) getARClient(getter dependency.Manager, context context.Context, name string) (client internalthirdparty.ArtifactRegistryClientInterface, err error) {

	depItemList := getter.Get(context, devops.TypeArtifactRegistry, name)
	if err := depItemList.Validate(); err != nil {
		return nil, err
	}

	ar := &devops.ArtifactRegistry{}
	secret := &v1.Secret{}
	depItemList.GetInto(devops.TypeArtifactRegistry, ar).GetInto(devops.TypeSecret, secret)

	basicAuth := k8s.GetDataBasicAuthFromSecret(secret)

	owners := ar.OwnerReferences

	artifactType := ar.Spec.Type

	if len(owners) > 0 {
		managerName := owners[0].Name
		dependencies := getter.Get(context, devops.TypeArtifactRegistryManager, managerName)
		if err = dependencies.Validate(); err != nil {
			return nil, err
		}
		manager := &devops.ArtifactRegistryManager{}
		secret := &v1.Secret{}
		dependencies.GetInto(devops.TypeArtifactRegistryManager, manager).GetInto(devops.TypeSecret, secret)
		glog.V(9).Infof("%s manager type is %s and host is %s", roleFilename, manager.Spec.Type.String(), manager.Spec.HTTP.Host)
		artifactType = manager.Spec.Type.String()

	}

	client = r.Factory.GetArtifactRegistryClient(artifactType, ar.Spec.HTTP.Host, basicAuth, 15, r.systemNamespace)
	return
}

func (r *ArtifactRegistryRolesOperation) ArtifactRegistryGetRolesMappingFunc(getter dependency.Manager, context context.Context, name string, rolemappinglist *devops.RoleMappingListOptions) (*devops.RoleMapping, error) {

	methodName := "ArtifactRegistryGetRolesMappingFunc"

	glog.V(9).Infof("%s %s parameter name is %s and rolemappinglist is %#v", roleFilename, methodName, name, rolemappinglist)

	serviceClient, err := r.getARClient(getter, context, name)
	if err != nil {
		return nil, err
	}

	rolemapping, err := serviceClient.GetRoleMapping(name, rolemappinglist)
	if err != nil {
		return nil, err
	}

	glog.V(9).Infof("%s %s result is %#v", roleFilename, methodName, rolemapping)

	return rolemapping, nil
}

func (r *ArtifactRegistryRolesOperation) ArtifactRegistryApplyRolesMappingFunc(getter dependency.Manager, context context.Context, name string, rolemapping *devops.RoleMapping) (rolemappinglist *devops.RoleMapping, err error) {

	methodName := "ArtifactRegistryApplyRolesMappingFunc"

	glog.V(9).Infof("%s %s parameter name is %s and rolemapping is %#v", roleFilename, methodName, name, rolemapping)

	serviceClient, err := r.getARClient(getter, context, name)
	if err != nil {
		return nil, err
	}

	dependencies := getter.Get(context, devops.TypeArtifactRegistry, name)
	if err = dependencies.Validate(); err != nil {
		return nil, err
	}

	glog.V(9).Infof("%s %s get dependency successful", roleFilename, methodName)

	ar := &devops.ArtifactRegistry{}

	dependencies.GetInto(devops.TypeArtifactRegistry, ar)

	glog.V(9).Infof("%s %s ar is %#v", roleFilename, methodName, ar)

	operations := &rolemapping.Spec

	for _, operation := range *operations {
		//range for different project
		for _, userandoperation := range operation.UserRoleOperations {
			//range for different user and operation in one project
			switch userandoperation.Operation {
			case "add":
				{
					err = serviceClient.AddUserRoleWithActions(operation.Project.Name, name, []string{userandoperation.User.Username}, userandoperation.Role.Name, strings.ToLower(ar.Spec.ArtifactRegistryArgs["artifactType"]), getter)
				}
			case "remove":
				{
					err = serviceClient.RemoveUserRole(operation.Project.Name, name, []string{userandoperation.User.Name}, userandoperation.Role.Name)
				}
			default:
				glog.V(9).Infof("%s %s Not found operation", roleFilename, methodName)
				return rolemappinglist, errors.NewBadRequest("Not found operation")
			}
		}
	}
	rolemappinglist = &devops.RoleMapping{}
	rolemappinglist.Spec = *operations

	return rolemappinglist, err
}

func (r ArtifactRegistryRolesOperation) getDevOpsConfigMap(getter dependency.Manager) (*v1.ConfigMap, error) {
	context := k8srequest.NewContext()
	context = k8srequest.WithNamespace(context, r.systemNamespace)
	depList := getter.Get(context, devops.TypeConfigMap, devops.SettingsConfigMapName)
	if err := depList.Validate(); err != nil {
		return nil, err
	}
	configMap := v1.ConfigMap{}
	depList.GetInto(devops.TypeConfigMap, &configMap)
	// GetInto will not return error if error happened, trick it here
	if configMap.Name == "" {
		return nil, fmt.Errorf("Error to get config map '%s/%s'", r.systemNamespace, devops.SettingsConfigMapName)
	}

	return &configMap, nil
}

// NewFromConfigMap starts a scheme from devops-config configmap
func newPlatformFromConfigMap(configMap *v1.ConfigMap) (s *role.Scheme, err error) {

	//get role_mapping
	roleSyncSchemaString, ok := configMap.Data[devops.SettingsKeyRoleMapping]
	if !ok || len(roleSyncSchemaString) == 0 {
		err = fmt.Errorf("Key of '%s' in ConfigMap '%s' is not found or content is empty", devops.SettingsKeyRoleMapping, devops.SettingsConfigMapName)
		glog.Errorf(err.Error())
		return nil, err
	}

	//
	s, err = role.NewFromString(roleSyncSchemaString)
	if err != nil {
		glog.Errorf("Cannot init RoleSyncSchema from roleSyncSchemaString: %s, error:%s", roleSyncSchemaString, err)
		return nil, err
	}
	return
}
