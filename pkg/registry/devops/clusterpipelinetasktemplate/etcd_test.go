package clusterpipelinetasktemplate_test

import (
	"reflect"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apiserver"
	"alauda.io/devops-apiserver/pkg/registry/devops/clusterpipelinetasktemplate"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	genericregistry "k8s.io/apiserver/pkg/registry/generic"
)

const defaultEtcdPathPrefix = "/registry/devops.alauda.io"

func TestNewRest(t *testing.T) {
	registry, _, err := clusterpipelinetasktemplate.NewREST(apiserver.Scheme, genericregistry.RESTOptions{}, devopsgeneric.NewFakeStore)
	if err != nil {
		t.Errorf("Should not error while creating: %v", err)
	}
	expected := []string{"devops", "templates"}
	if !reflect.DeepEqual(expected, registry.Categories()) {
		t.Errorf("Categories are different: %v != %v", expected, registry.Categories())
	}
	expected = []string{"cpipetaskt"}
	if !reflect.DeepEqual(expected, registry.ShortNames()) {
		t.Errorf("Short names are different: %v != %v", expected, registry.ShortNames())
	}
	obj := registry.NewFunc()
	if cptt, ok := obj.(*devops.ClusterPipelineTaskTemplate); cptt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.ClusterPipelineTaskTemplate", reflect.TypeOf(obj))
	}
	objList := registry.NewListFunc()
	if cptt, ok := objList.(*devops.ClusterPipelineTaskTemplateList); cptt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.ClusterPipelineTaskTemplateList", reflect.TypeOf(objList))
	}
	_, _, err = clusterpipelinetasktemplate.NewREST(apiserver.Scheme, genericregistry.RESTOptions{}, devopsgeneric.NewStandardStore)
	if err == nil {
		t.Errorf("Empty rest options did not return error")
	}
}
