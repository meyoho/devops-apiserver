package clusterpipelinetasktemplate

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestPipelineTaskPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	template1 := &devops.ClusterPipelineTaskTemplate{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "pipeline-task-template",
			CreationTimestamp: now,
			Annotations: map[string]string{
				"displayName.en": "TestTemplate",
				"version":        "v0.1",
			},
			Labels: map[string]string{
				"category": "CICD",
			},
		},
		Spec: devops.PipelineTaskTemplateSpec{
			Agent: &devops.JenkinsAgent{
				Label: "mylabel",
			},
			Engine: devops.PipelineTaskTemplateEngineGoTemplate,
		},
		Status: devops.TemplateStatus{Phase: devops.TemplateReady},
	}
	template2 := &devops.ClusterPipelineTaskTemplate{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "pipeline-task-2",
			CreationTimestamp: now,
			Annotations: map[string]string{
				"displayName.zh-CN": "Task2",
			},
			Labels: map[string]string{},
		},
		Spec: devops.PipelineTaskTemplateSpec{
			Engine: devops.PipelineTaskTemplateEngineGoTemplate,
		},
	}
	list := &devops.ClusterPipelineTaskTemplateList{
		Items: []devops.ClusterPipelineTaskTemplate{*template1, *template2},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: &list.Items[0]},
			Cells: []interface{}{
				// name - displayName.en  - version - category label - timestamp - agent label - engine
				"pipeline-task-template", "TestTemplate", "v0.1", "CICD", "Ready", registry.TranslateTimestamp(now), "mylabel", string(devops.PipelineTaskTemplateEngineGoTemplate),
			},
		},
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: &list.Items[1]},
			Cells: []interface{}{
				// name - displayName.zh-CN  - no version - no category - timestamp - no agent - engine
				"pipeline-task-2", "Task2", "-", "-", "-", registry.TranslateTimestamp(now), "-", string(devops.PipelineTaskTemplateEngineGoTemplate),
			},
		},
	}

	result, err := printPipelineList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing pipeline task template: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
