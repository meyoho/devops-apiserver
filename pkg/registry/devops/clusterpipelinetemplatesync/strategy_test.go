package clusterpipelinetemplatesync_test

import (
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/clusterpipelinetemplatesync"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestStrategy(t *testing.T) {
	Schema := runtime.NewScheme()
	strategy := clusterpipelinetemplatesync.NewStrategy(Schema)

	if strategy.NamespaceScoped() {
		t.Errorf("ClusterPipelineTemplateSync strategy should not be namespace scoped")
	}

	if strategy.AllowCreateOnUpdate() {
		t.Errorf("ClusterPipelineTemplateSync strategy should not allow create on update")
	}

	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("ClusterPipelineTemplateSync strategy should not allow unconditional update")
	}

	type Table struct {
		name     string
		new      *devopsv1alpha1.ClusterPipelineTemplateSync
		old      *devopsv1alpha1.ClusterPipelineTemplateSync
		expected *devopsv1alpha1.ClusterPipelineTemplateSync
		method   func(*Table)
	}

	table := []Table{
		{
			name: "simpe-ClusterPipelineTemplateSync",
			new: &devopsv1alpha1.ClusterPipelineTemplateSync{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.PipelineTemplateSyncSpec{
					Source: devopsv1alpha1.PipelineSource{
						Git:            nil,
						CodeRepository: nil,
					},
				},
			},
			old: nil,
			expected: &devopsv1alpha1.ClusterPipelineTemplateSync{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineTemplateSyncSpec{},
			},
			method: func(tb *Table) {
				strategy.PrepareForCreate(nil, tb.new)

				err := strategy.Validate(nil, tb.new)
				if len(err) != 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input generate error: %v",
						tb.name, err,
					)
				}
			},
		},
		{
			name: "lack phase",
			new: &devopsv1alpha1.ClusterPipelineTemplateSync{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.PipelineTemplateSyncSpec{
					Source: devopsv1alpha1.PipelineSource{
						Git:            nil,
						CodeRepository: nil,
					},
				},
				Status: &devopsv1alpha1.PipelineTemplateSyncStatus{},
			},
			old: nil,
			expected: &devopsv1alpha1.ClusterPipelineTemplateSync{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineTemplateSyncSpec{},
			},
			method: func(tb *Table) {
				strategy.PrepareForCreate(nil, tb.new)

				err := strategy.Validate(nil, tb.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input generate error: %v",
						tb.name, err,
					)
				}
			},
		},
	}

	for _, tb := range table {
		tb.method(&tb)
	}
}
