package artifactregistrybinding

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestArtifactRegistryBindingPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	arObj := &devops.ArtifactRegistryBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "arb-1",
			CreationTimestamp: now,
		},
		Spec: devops.ArtifactRegistryBindingSpec{
			ArtifactRegistry: devops.LocalObjectReference{
				Name: "artifactRegistryBinding",
			},
		},
		Status: devops.ServiceStatus{
			Phase: devops.ServiceStatusPhaseReady,
		},
	}
	list := &devops.ArtifactRegistryBindingList{
		Items: []devops.ArtifactRegistryBinding{*arObj},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: arObj},
			Cells: []interface{}{
				arObj.Name, arObj.Spec.ArtifactRegistry, arObj.Spec.Secret, arObj.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printArtifactRegistryBindingList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing registry: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
