package artifactregistrybinding_test

import (
	"alauda.io/devops-apiserver/pkg/registry/devops/artifactregistrybinding"
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestStrategy(t *testing.T) {
	Schema := runtime.NewScheme()
	strategy := artifactregistrybinding.NewStrategy(Schema)

	if !strategy.NamespaceScoped() {
		t.Errorf("Artifactregistrybinding strategy should not be namespace scoped")
	}

	if strategy.AllowCreateOnUpdate() {
		t.Errorf("Artifactregistrybinding strategy should not allow create on update")
	}

	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("Artifactregistrybinding strategy should not allow unconditional update")
	}

	type Table struct {
		name     string
		new      *devopsv1alpha1.ArtifactRegistryBinding
		old      *devopsv1alpha1.ArtifactRegistryBinding
		expected *devopsv1alpha1.ArtifactRegistryBinding
		method   func(*Table)
	}

	table := []Table{
		{
			name: "simple",
			new: &devopsv1alpha1.ArtifactRegistryBinding{
				Spec: devopsv1alpha1.ArtifactRegistryBindingSpec{},
			},
			old: nil,
			expected: &devopsv1alpha1.ArtifactRegistryBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.ArtifactRegistryBindingSpec{},
			},
			method: func(tb *Table) {
				strategy.PrepareForCreate(nil, tb.new)

				err := strategy.Validate(nil, tb.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input did not generate error: %v",
						tb.name, err,
					)
				}
			},
		},
	}

	for _, tb := range table {
		tb.method(&tb)
	}
}
