package artifactregistrybinding

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// ColumnDefinitions column definitions for PipelineConfig
var columnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
	{Name: registry.ResourceType, Type: registry.TableTypeString, Description: "Type of ArtifactRegistryBinding"},
	{Name: registry.ResourceHost, Type: registry.TableTypeString, Description: "Host address of the ArtifactRegistryBinding instance"},
	{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "Status of the connectivity to the ArtifactRegistryBinding instance"},
	{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
}

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {

	h.TableHandler(columnDefinitions, printArtifactRegistryBindingList)
	h.TableHandler(columnDefinitions, printerArtifactRegistryBinding)

	registry.AddDefaultHandlers(h)

}

// printArtifactRegistryBindingList prints a ArtifactRegistryBinding list
func printArtifactRegistryBindingList(ArtifactRegistryBindingList *devops.ArtifactRegistryBindingList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(ArtifactRegistryBindingList.Items))
	for i := range ArtifactRegistryBindingList.Items {
		r, err := printerArtifactRegistryBinding(&ArtifactRegistryBindingList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

// printerArtifactRegistry prints a ArtifactRegistryBinding
func printerArtifactRegistryBinding(ar *devops.ArtifactRegistryBinding, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: ar},
	}

	// basic rows
	row.Cells = append(row.Cells, ar.Name, ar.Spec.ArtifactRegistry, ar.Spec.Secret, ar.Status.Phase, registry.TranslateTimestamp(ar.CreationTimestamp))

	if options.Wide {
		// for later use
	}

	return []metav1alpha1.TableRow{row}, nil
}
