/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package documentmanagementbinding

import (
	"context"
	"fmt"
	"time"

	devopsregistrygeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	apiequality "k8s.io/apimachinery/pkg/api/equality"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"
	glog "k8s.io/klog"
)

// NewStrategy creates a new Strategy instance for documentManagementbinding
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// Strategy documentManagementbinding strategy
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// PrepareForCreate prepares an object for create request
func (Strategy) PrepareForCreate(ctx context.Context, new runtime.Object) {
	newObj, ok := new.(*devops.DocumentManagementBinding)
	if ok {
		newObj.Status.Phase = devops.ServiceStatusPhaseCreating
		newObj.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate prepares an object for update request
func (Strategy) PrepareForUpdate(ctx context.Context, new, old runtime.Object) {
	var (
		newObj, oldObj *devops.DocumentManagementBinding
	)
	newObj, _ = new.(*devops.DocumentManagementBinding)
	if old != nil {
		oldObj, _ = old.(*devops.DocumentManagementBinding)
	}

	if newObj != nil && newObj.Status.Phase.IsInvalid() {
		newObj.Status.Phase = devops.StatusCreating
	}

	if isChanged(oldObj, newObj) {
		glog.Infof("documentManagementBinding %s/%s was changed", newObj.GetNamespace(), newObj.GetName())
		newObj.Status.HTTPStatus = nil
		newObj.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	}
}

// Validate validates a request object
func (Strategy) Validate(ctx context.Context, obj runtime.Object) (errs field.ErrorList) {
	newObj, ok := obj.(*devops.DocumentManagementBinding)
	if ok {
		return validation.ValidateDocumentManagementBinding(newObj)
	}
	return
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx context.Context, new, old runtime.Object) (errs field.ErrorList) {
	newObj, newOk := new.(*devops.DocumentManagementBinding)
	oldObj, oldOk := old.(*devops.DocumentManagementBinding)
	if newOk && oldOk {
		return validation.ValidateDocumentManagementBindingUpdate(newObj, oldObj)
	}
	return
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
	// Often implemented as a type check or empty method
}

func isChanged(old, new *devops.DocumentManagementBinding) bool {
	if old == nil || new == nil {
		return true
	}
	return !apiequality.Semantic.DeepEqual(old.Spec, new.Spec)
}

// MatchDocumentManagementBinding is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchDocumentManagementBinding(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	apiserver, ok := obj.(*devops.DocumentManagementBinding)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a DocumentManagementBinding")
	}
	return labels.Set(apiserver.ObjectMeta.Labels), toSelectableFields(apiserver), apiserver.Initializers != nil, nil
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.DocumentManagementBinding) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

//
func GetDocumentManagement(getter devopsregistrygeneric.ResourceGetter, ctx context.Context, name string) (documentmanagement *devops.DocumentManagement, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	documentmanagement = obj.(*devops.DocumentManagement)
	if documentmanagement == nil {
		err = fmt.Errorf("unexpected object type: %v", obj)
	}
	return
}

func GetDocumentManagementBinding(getter devopsregistrygeneric.ResourceGetter, ctx context.Context, name string) (documentManagementBinding *devops.DocumentManagementBinding, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	documentManagementBinding = obj.(*devops.DocumentManagementBinding)
	if documentManagementBinding == nil {
		err = fmt.Errorf("unexpected object type: %v", obj)
	}
	return
}
