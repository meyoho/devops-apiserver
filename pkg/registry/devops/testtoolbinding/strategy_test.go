package testtoolbinding_test

import (
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/testtoolbinding"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

type Table struct {
	name     string
	new      *devopsv1alpha1.TestToolBinding
	old      *devopsv1alpha1.TestToolBinding
	expected *devopsv1alpha1.TestToolBinding
	method   func(*Table)
}

func TestStrategy(t *testing.T) {
	scheme := runtime.NewScheme()
	strategy := testtoolbinding.NewStrategy(scheme)

	if !strategy.NamespaceScoped() {
		t.Errorf("TestToolBinding strategy should be namespace scoped")
	}
	if strategy.AllowCreateOnUpdate() {
		t.Errorf("TestToolBinding strategy should not allow create on update")
	}
	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("TestToolBinding strategy should not allow unconditional update")
	}

	table := []Table{
		{
			name: "create: invalid input",
			new: &devopsv1alpha1.TestToolBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.TestToolBindingSpec{},
				Status:     devopsv1alpha1.ServiceStatus{},
			},
			expected: &devopsv1alpha1.TestToolBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.TestToolBindingSpec{},
				Status: devopsv1alpha1.ServiceStatus{
					Phase: devopsv1alpha1.StatusCreating,
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input did not generate error: %v",
						tst.name, err,
					)
				}

				validGetAttrs(t, tst)
			},
		},
		{
			name: "create: valid input",
			new: &devopsv1alpha1.TestToolBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.TestToolBindingSpec{
					TestTool: devopsv1alpha1.LocalObjectReference{
						Name: "xxx",
					},
				},
				Status: devopsv1alpha1.ServiceStatus{},
			},
			expected: &devopsv1alpha1.TestToolBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.TestToolBindingSpec{
					TestTool: devopsv1alpha1.LocalObjectReference{
						Name: "xxx",
					},
				},
				Status: devopsv1alpha1.ServiceStatus{
					Phase: devopsv1alpha1.StatusCreating,
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) != 0 {
					t.Errorf(
						"Test: \"%s\" - Validate valid input should not generate error: %v",
						tst.name, err,
					)
				}

				validGetAttrs(t, tst)
			},
		},
	}

	for i, tst := range table {
		tst.method(&tst)

		if tst.new.Status.Phase != tst.expected.Status.Phase {
			t.Errorf(
				"Test %d: \"%v\" - status.phase are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
	}

}

func validGetAttrs(t *testing.T, tst *Table) {
	_, _, _, getErr := testtoolbinding.GetAttrs(tst.new)
	if getErr != nil {
		t.Errorf(
			"Test: \"%s\" - GetAttr return err for object: %v",
			tst.name, getErr,
		)
	}
}
