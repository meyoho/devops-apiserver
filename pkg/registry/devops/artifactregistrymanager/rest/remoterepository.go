package rest

import (
	"context"
	"net/http"
	"strings"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	glog "k8s.io/klog"

	"k8s.io/apiserver/pkg/registry/rest"

	"alauda.io/devops-apiserver/pkg/dependency"
	metainternalversion "k8s.io/apimachinery/pkg/apis/meta/internalversion"
)

var repositoryFilename = "armrepository"

// RepositoryREST implements the log endpoint for a Pod
type RepositoryREST struct {
	Getter          dependency.Manager
	RoundTripper    http.RoundTripper
	systemNamespace string
}

// NewRepositoryREST starts a new log store
func NewRepositoryREST(
	getter dependency.Manager,
	roundTripper http.RoundTripper,
	systemNamespace string,
) rest.Storage {
	return &RepositoryREST{
		Getter:          getter,
		RoundTripper:    roundTripper,
		systemNamespace: systemNamespace,
	}
}

// RepositoryREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&RepositoryREST{})

// New creates a new Pod log options object
func (r *RepositoryREST) New() runtime.Object {
	return &devops.ArtifactRegistry{}
}

// Get retrieves a runtime.Object that will stream the contents of codeRepoService
func (r *RepositoryREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		err error
	)

	artifactType := opts.(*devops.ArtifactRegistryOption).ArtifactType

	isFilterAR := opts.(*devops.ArtifactRegistryOption).IsFilterAR

	//get artifact registry manager by name
	dependencies := r.Getter.Get(ctx, devops.TypeArtifactRegistryManager, name)
	if err = dependencies.Validate(); err != nil {
		return nil, err
	}

	manager := &devops.ArtifactRegistryManager{}
	secret := &v1.Secret{}
	dependencies.GetInto(devops.TypeArtifactRegistryManager, manager).GetInto(devops.TypeSecret, secret)

	glog.V(9).Infof("%s manager is %#v", repositoryFilename, manager)
	glog.V(9).Infof("%s manager`s secret is %#v", repositoryFilename, secret)
	//end

	//sercet begin

	basicAuth := k8s.GetDataBasicAuthFromSecret(secret)
	glog.V(9).Infof("%s user is %s and password is %s", repositoryFilename, basicAuth.Username, basicAuth.Password)
	//secret end

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(manager.Spec.HTTP.Host),
		devopsclient.NewBasicAuth(basicAuth.Username, basicAuth.Password),
		devopsclient.NewTransport(r.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		manager.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	arList, err := serviceClient.ListRegistry(ctx)
	glog.V(9).Infof("%s arList is %#v", repositoryFilename, arList)
	if err != nil {
		return &arList, err
	}

	glog.V(9).Infof("%s getter is %#v", repositoryFilename, dependencies)

	if isFilterAR == "true" {
		var resultList []devops.ArtifactRegistry

		options := metainternalversion.ListOptions{ResourceVersion: "0"}

		pArList, err := r.Getter.List(ctx, devops.TypeArtifactRegistry, &options)
		pArListl := pArList.(*devops.ArtifactRegistryList)
		if err != nil {
			return nil, err
		}

		glog.V(9).Infof("%s pArList is %#v", repositoryFilename, pArList)

		for _, n := range arList.Items {
			var bov = false
			for _, m := range pArListl.Items {
				if n.Name == m.Name {
					bov = true
					break
				}
			}
			if !bov {
				resultList = append(resultList, n)
			}

		}
		glog.V(9).Infof("%s resultList is %#v", repositoryFilename, resultList)
		arList.Items = resultList

	}

	if artifactType != "" {
		var resultListWithType []devops.ArtifactRegistry
		for _, h := range arList.Items {
			if strings.ToLower(h.Spec.Type) == strings.ToLower(artifactType) {
				resultListWithType = append(resultListWithType, h)
			}
		}

		arList.Items = resultListWithType
	}

	return &arList, nil
}

// NewGetOptions creates a new options object
func (r *RepositoryREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ArtifactRegistryOption{}, false, ""
}
