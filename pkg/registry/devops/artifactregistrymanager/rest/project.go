package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
	glog "k8s.io/klog"
)

var projectFilename = "armproject"

// RepositoryREST implements the log endpoint for a Pod
type ArtifactRegistryREST struct {
	arStore         rest.StandardStorage
	armStore        rest.StandardStorage
	devopsClient    clientset.Interface
	SecretLister    corev1listers.SecretLister
	RoundTripper    http.RoundTripper
	systemNamespace string
	provider        devops.AnnotationProvider
}

// New creates a new Pod log options object
func (r *ArtifactRegistryREST) New() runtime.Object {
	return &devops.CreateProjectOptions{}
}

// NewProjectREST starts a new project store
func NewCreateREST(
	arStore rest.StandardStorage,
	armStore rest.StandardStorage,
	devopsClient clientset.Interface,
	SecretLister corev1listers.SecretLister,
	roundTripper http.RoundTripper,
	systemNamespace string,
	provider devops.AnnotationProvider,

) rest.Storage {
	api := &ArtifactRegistryREST{
		arStore:         arStore.(*devopsregistry.REST),
		armStore:        armStore.(*devopsregistry.REST),
		devopsClient:    devopsClient,
		SecretLister:    SecretLister,
		RoundTripper:    roundTripper,
		systemNamespace: systemNamespace,
		provider:        provider,
	}

	return api
}

var _ = rest.NamedCreater(&ArtifactRegistryREST{})

func (r *ArtifactRegistryREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (runtime.Object, error) {

	artifactRegistryCreateOption, ok := obj.(*devops.CreateProjectOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", obj)
	}
	glog.V(9).Infof("%s artifactRegistryCreateOption is %#v and opts is %#v", projectFilename, obj, opts)

	//get artifact registry manager by name
	managerName := name

	options := metav1.GetOptions{ResourceVersion: "0"}

	managerOrgi, err := r.armStore.Get(ctx, managerName, &options)
	if err != nil {
		return nil, err
	}

	manager := managerOrgi.(*devops.ArtifactRegistryManager)
	glog.V(9).Infof("%s manager is %#v", projectFilename, manager)
	//end

	//artifactType like maven2 npm
	artifactType := artifactRegistryCreateOption.Data["artifactType"]

	glog.V(9).Infof("%s artifactType is %s", projectFilename, artifactType)

	//sercet begin
	secretKeySetRef := manager.Spec.Secret

	if artifactRegistryCreateOption.SecretName != "" {
		secretKeySetRef.Name = artifactRegistryCreateOption.SecretName
		secretKeySetRef.Namespace = artifactRegistryCreateOption.Namespace
	}

	secret, err := r.SecretLister.Secrets(secretKeySetRef.Namespace).Get(secretKeySetRef.Name)
	if err != nil {
		glog.Errorf("Error get secret '%s/%s', error:%#v", secretKeySetRef.Namespace, secretKeySetRef.Name, err)
		return nil, errors.NewBadRequest(err.Error())
	}
	if secret == nil {
		glog.Errorf("Not found secret: %s", secretKeySetRef.Name)
		return nil, errors.NewNotFound(devops.Resource(devops.TypeArtifactRegistryManager), secretKeySetRef.Name)
	}
	basicAuth := k8s.GetDataBasicAuthFromSecret(secret)
	glog.V(9).Infof("%s user is %s and password is %s", projectFilename, basicAuth.Username, basicAuth.Password)
	//secret end

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(manager.Spec.HTTP.Host),
		devopsclient.NewBasicAuth(basicAuth.Username, basicAuth.Password),
		devopsclient.NewTransport(r.RoundTripper),
	)

	serviceClient, err := factory.NewClient(
		manager.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	authStatus, err := serviceClient.Authenticate(ctx)
	glog.V(9).Infof("%s authStatus is %#v and err is %#v", projectFilename, authStatus, err)
	if err != nil {
		glog.Errorf("%s Error check secret error:%#v", projectFilename, err)
		return nil, errors.NewBadRequest(err.Error())
	}

	if authStatus != nil && authStatus.StatusCode >= 400 {
		return nil, errors.NewBadRequest(fmt.Errorf("username or password in secret '%s' is incorrect", secret.GetName()).Error())
	}

	arName := artifactRegistryCreateOption.Name
	if arName == "" && artifactRegistryCreateOption.Data["artifactRegistryName"] == "" {
		return nil, errors.NewBadRequest("Name or artifactRegistryName must set.")
	} else if artifactRegistryCreateOption.Data["artifactRegistryName"] == "" {
		artifactRegistryCreateOption.Data["artifactRegistryName"] = arName
	} else if arName == "" {
		arName = artifactRegistryCreateOption.Data["artifactRegistryName"]
	}

	err = serviceClient.CreateRegistry(ctx, artifactType, artifactRegistryCreateOption.Data)

	if err != nil {
		glog.V(9).Infof("%s create nexus repository err is %#v", projectFilename, err)
		return nil, err
	}

	isPublic := false
	if artifactRegistryCreateOption.Data["public"] == "true" {
		isPublic = true
	}

	//When create manager repository successful
	artifactRegistryObj := devops.ArtifactRegistry{
		ObjectMeta: metav1.ObjectMeta{
			Name: arName,
			OwnerReferences: []metav1.OwnerReference{
				{
					APIVersion: "devops.alauda.io/v1alpha1",
					Kind:       "ArtifactRegistryManager",
					Name:       manager.Name,
					UID:        manager.UID,
				},
			},
			Labels: map[string]string{
				r.provider.ArtifactRegistryManagerLabel(): name,
			},
			Annotations: artifactRegistryCreateOption.Annotations,
		},
		Spec: devops.ArtifactRegistrySpec{

			ArtifactRegistryName: artifactRegistryCreateOption.Data["artifactRegistryName"],
			Type:                 artifactType,
			Public:               isPublic,
			ToolSpec: devops.ToolSpec{
				HTTP:   manager.Spec.HTTP,
				Secret: secretKeySetRef,
			},
			ArtifactRegistryArgs: artifactRegistryCreateOption.Data,
		},
	}

	devopsClient := r.devopsClient
	createResult, err := devopsClient.Devops().ArtifactRegistries().Create(&artifactRegistryObj)
	if err != nil {
		return createResult, err
	}

	return createResult, nil
}
