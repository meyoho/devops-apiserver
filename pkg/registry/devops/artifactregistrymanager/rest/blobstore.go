package rest

import (
	"context"
	"net/http"

	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	glog "k8s.io/klog"

	"k8s.io/apiserver/pkg/registry/rest"

	"alauda.io/devops-apiserver/pkg/dependency"
)

var blobStoreFilename = "armblob"

// RepositoryREST implements the log endpoint for a Pod
type BlobStoreREST struct {
	Getter          dependency.Manager
	RoundTripper    http.RoundTripper
	systemNamespace string
}

// NewRepositoryREST starts a new log store
func NewBlobStoreREST(
	getter dependency.Manager,
	roundTripper http.RoundTripper,
	systemNamespace string,
) rest.Storage {
	return &BlobStoreREST{
		Getter:          getter,
		RoundTripper:    roundTripper,
		systemNamespace: systemNamespace,
	}
}

// RepositoryREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&BlobStoreREST{})

// New creates a new Pod log options object
func (r *BlobStoreREST) New() runtime.Object {
	return &devops.ArtifactRegistry{}
}

// Get retrieves a runtime.Object that will stream the contents of codeRepoService
func (r *BlobStoreREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		err error
	)

	//get artifact registry manager by name
	dependencies := r.Getter.Get(ctx, devops.TypeArtifactRegistryManager, name)
	if err = dependencies.Validate(); err != nil {
		return nil, err
	}

	manager := &devops.ArtifactRegistryManager{}
	secret := &v1.Secret{}
	dependencies.GetInto(devops.TypeArtifactRegistryManager, manager).GetInto(devops.TypeSecret, secret)

	glog.V(9).Infof("%s manager is %#v", blobStoreFilename, manager)
	glog.V(9).Infof("%s manager`s secret is %#v", blobStoreFilename, secret)
	//end

	//sercet begin

	basicAuth := k8s.GetDataBasicAuthFromSecret(secret)
	glog.V(9).Infof("%s user is %s and password is %s", blobStoreFilename, basicAuth.Username, basicAuth.Password)
	//secret end
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(manager.Spec.HTTP.Host),
		devopsclient.NewBasicAuth(basicAuth.Username, basicAuth.Password),
		devopsclient.NewTransport(r.RoundTripper),
	)

	serviceClient, err := factory.NewClient(
		manager.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	blobList, err := serviceClient.ListBlobStore(ctx)
	if err != nil {
		return nil, err
	}

	return &blobList, nil
}

// NewGetOptions creates a new options object
func (r *BlobStoreREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoBindingRepositoryOptions{}, false, ""
}
