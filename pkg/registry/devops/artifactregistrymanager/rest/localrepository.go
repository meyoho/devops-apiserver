package rest

import (
	"context"
	"net/http"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	glog "k8s.io/klog"

	"k8s.io/apiserver/pkg/registry/rest"

	"alauda.io/devops-apiserver/pkg/dependency"
	metainternalversion "k8s.io/apimachinery/pkg/apis/meta/internalversion"
)

var localRepositoryFilename = "armlocalrepository"

// RepositoryREST implements the log endpoint for a Pod
type LocalRepositoryREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

// NewRepositoryREST starts a new log store
func NewLocalRepositoryREST(
	getter dependency.Manager,
	roundTripper http.RoundTripper,
) rest.Storage {
	return &LocalRepositoryREST{
		Getter:       getter,
		RoundTripper: roundTripper,
	}
}

// RepositoryREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&LocalRepositoryREST{})

// New creates a new Pod log options object
func (r *LocalRepositoryREST) New() runtime.Object {
	return &devops.ArtifactRegistry{}
}

// Get retrieves a runtime.Object that will stream the contents of codeRepoService
func (r *LocalRepositoryREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		err error
	)

	artifactType := opts.(*devops.ArtifactRegistryOption).ArtifactType

	//get artifact registry manager by name
	dependencies := r.Getter.Get(ctx, devops.TypeArtifactRegistryManager, name)
	if err = dependencies.Validate(); err != nil {
		return nil, err
	}

	manager := &devops.ArtifactRegistryManager{}
	secret := &v1.Secret{}
	dependencies.GetInto(devops.TypeArtifactRegistryManager, manager).GetInto(devops.TypeSecret, secret)

	glog.V(9).Infof("%s manager is %#v", localRepositoryFilename, manager)
	glog.V(9).Infof("%s manager`s secret is %#v", localRepositoryFilename, secret)
	//end

	options := metainternalversion.ListOptions{ResourceVersion: "0"}

	pArList, err := r.Getter.List(ctx, devops.TypeArtifactRegistry, &options)
	pArListl := pArList.(*devops.ArtifactRegistryList)
	if err != nil {
		return nil, err
	}

	glog.V(9).Infof("%s pArListl is %#v", localRepositoryFilename, pArListl)

	var resultList []devops.ArtifactRegistry
	for _, v := range pArListl.Items {
		if len(v.OwnerReferences) > 0 && v.OwnerReferences[0].Name == name {
			if artifactType != "" {
				if strings.ToLower(v.Spec.Type) != strings.ToLower(artifactType) {
					continue
				}
			}
			resultList = append(resultList, v)

		}
	}

	pArListl.Items = resultList

	return pArListl, nil
}

// NewGetOptions creates a new options object
func (r *LocalRepositoryREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ArtifactRegistryOption{}, false, ""
}
