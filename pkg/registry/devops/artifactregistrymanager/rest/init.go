package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/util"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"

	"alauda.io/devops-apiserver/pkg/dependency"
	"k8s.io/apiserver/pkg/registry/rest"
)

type InitREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

func NewInitREST(getter dependency.Manager, roundTripper http.RoundTripper) rest.Storage {
	return &InitREST{
		Getter:       getter,
		RoundTripper: roundTripper,
	}
}

var _ = rest.NamedCreater(&InitREST{})

func (r *InitREST) New() runtime.Object {
	return &devops.ArtifactRegistryManagerOptions{}
}

func (r *InitREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, options *metav1.CreateOptions) (runtime.Object, error) {
	//opts, ok := obj.(*devops.ArtifactRegistryManagerOptions)
	//if !ok {
	//	return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opts))
	//}
	dependencies := r.Getter.Get(ctx, devops.TypeArtifactRegistryManager, name)
	if err := dependencies.Validate(); err != nil {
		return nil, err
	}
	artifactRegistryManager := &devops.ArtifactRegistryManager{}
	secret := &corev1.Secret{}
	dependencies.GetInto(devops.TypeArtifactRegistryManager, artifactRegistryManager).GetInto(devops.TypeSecret, secret)

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(artifactRegistryManager.GetEndpoint()),
		devopsclient.NewBasicAuth(k8s.GetDataBasicAuthFromSecret(secret).Username, k8s.GetDataBasicAuthFromSecret(secret).Password),
		devopsclient.NewTransport(r.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		artifactRegistryManager.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get artifactRegistryManager %s client: %v", artifactRegistryManager.Spec.Type.String(), err))
	}

	errs := serviceClient.Init(ctx)

	var multiErrs util.MultiErrors
	if errs != nil {
		for _, e := range errs {
			multiErrs = append(multiErrs, e)
		}
	}
	return nil, &multiErrs
}
