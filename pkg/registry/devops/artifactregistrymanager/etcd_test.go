package artifactregistrymanager_test

import (
	"alauda.io/devops-apiserver/pkg/registry/devops/artifactregistrymanager"
	"reflect"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apiserver"

	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	genericregistry "k8s.io/apiserver/pkg/registry/generic"
)

const defaultEtcdPathPrefix = "/registry/devops.alauda.io"

func TestNewRest(t *testing.T) {
	registry, err := artifactregistrymanager.NewREST(apiserver.Scheme, genericregistry.RESTOptions{}, devopsgeneric.NewFakeStore)
	if err != nil {
		t.Errorf("Should not error while creating: %v", err)
	}
	expected := []string{"integrations", "toolchain", "devops"}
	if !reflect.DeepEqual(expected, registry.Categories()) {
		t.Errorf("Categories are different: %v != %v", expected, registry.Categories())
	}
	expected = []string{"arm"}
	if !reflect.DeepEqual(expected, registry.ShortNames()) {
		t.Errorf("Short names are different: %v != %v", expected, registry.ShortNames())
	}
	obj := registry.NewFunc()
	if cpt, ok := obj.(*devops.ArtifactRegistryManager); cpt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.ArtifactRegistryManager", reflect.TypeOf(obj))
	}
	objList := registry.NewListFunc()
	if cpt, ok := objList.(*devops.ArtifactRegistryManagerList); cpt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.ArtifactRegistryManagerList", reflect.TypeOf(objList))
	}
}
