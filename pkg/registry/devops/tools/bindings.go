package tools

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/toolchain"
	corev1 "k8s.io/api/core/v1"
	metainternalversion "k8s.io/apimachinery/pkg/apis/meta/internalversion"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
	glog "k8s.io/klog"
)

// NewBindingREST constructor for fetching multiple tool bindings
func NewBindingREST(dependencyGetter dependency.Manager, systemNamespace string) (storage rest.Storage, err error) {
	if dependencyGetter == nil {
		err = fmt.Errorf("BindingREST needs a DependencyGetter")
		return
	}
	storage = &BindingREST{Getter: dependencyGetter, systemNamespace: systemNamespace}
	return
}

// BindingREST implements the REST endpoint for changing the status of a ToolBindingReplica
type BindingREST struct {
	Getter          dependency.Manager
	systemNamespace string
}

// New constructs new return
func (r *BindingREST) New() runtime.Object {
	return &devops.ToolBinding{}
}

// NewList returns a list
func (r *BindingREST) NewList() runtime.Object {
	return &devops.ToolBindingList{}
}

// NamespaceScoped defines that the API should request a namespace
func (r *BindingREST) NamespaceScoped() bool {
	return true
}

func (r *BindingREST) getConfigContext() context.Context {
	return genericapirequest.WithNamespace(context.TODO(), r.systemNamespace)
}

// List list items
func (r *BindingREST) List(ctx context.Context, options *metainternalversion.ListOptions) (obj runtime.Object, err error) {
	namespace := genericapirequest.NamespaceValue(ctx)
	if options == nil {
		options = &metainternalversion.ListOptions{}
	}
	options.ResourceVersion = "0"
	glog.V(7).Infof("List bindings on namespace %s started", namespace)
	defer func() {
		glog.V(7).Infof("List bindings on namespace %s ended. err? %s", namespace, err)
	}()
	bindingsList := &devops.ToolBindingList{Items: []devops.ToolBindingMap{}}
	items := map[string][]devops.ToolBinding{}
	for _, kind := range r.Getter.GetBindingKinds() {
		list, err := r.Getter.List(ctx, kind, options)
		if err != nil {
			glog.Errorf("List bindings on namespace %s kind %s list err %s", namespace, kind, err)
			continue
		}
		bindingLister, ok := list.(devops.ToolBindingLister)
		if ok && bindingLister != nil {
			items[kind] = bindingLister.GetItems()
		}
	}
	var scheme *toolchain.Scheme
	scheme, err = r.getScheme()
	bindingsList.Items = r.getMap(namespace, scheme, items)
	obj = bindingsList
	return
}

// getSchemePanel retrieves the Panel from the whole Toolchain scheme
func (r *BindingREST) getScheme() (scheme *toolchain.Scheme, err error) {
	configMap := &corev1.ConfigMap{}
	alaudaSystemContext := r.getConfigContext()
	list := r.Getter.Get(alaudaSystemContext, devops.TypeConfigMap, devops.SettingsConfigMapName)
	if err = list.Validate(); err != nil {
		return
	}
	list.GetInto(devops.TypeConfigMap, configMap)
	if configMap != nil && configMap.Data != nil && configMap.Data[devops.SettingsKeyDomain] != "" {
		scheme, err = toolchain.NewFromString(configMap.Data[devops.SettingsKeyDomain])
	}
	return
}

func (r *BindingREST) getMap(namespace string, scheme *toolchain.Scheme, index map[string][]devops.ToolBinding) (result []devops.ToolBindingMap) {
	result = make([]devops.ToolBindingMap, 0, len(index))
	if scheme != nil && scheme.Panel != nil && scheme.Panel.Type == toolchain.PanelTypeThreeByThree && len(scheme.Panel.Panels) > 0 {
		for _, p := range scheme.Panel.Panels {
			if len(p.Categories) == 0 {
				continue
			}
			item := r.newToolBindingMap(namespace, p.Name.English, p.Name.Chinese, p.Index)
			for _, cat := range p.Categories {
				if list, ok := index[cat]; ok && list != nil {
					item.Items = append(item.Items, list...)
				}
			}
			result = append(result, item)
		}
	} else {
		item := r.newToolBindingMap(namespace, "DevOps Toolchain", "DevOps工具链", 0)
		for _, list := range index {
			if list != nil {
				item.Items = append(item.Items, list...)
			}
		}
		result = append(result, item)
	}
	return
}

func (r *BindingREST) newToolBindingMap(namespace, english, chinese string, index int) (result devops.ToolBindingMap) {
	result = devops.ToolBindingMap{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.TypeToolBindingMap,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      strings.Replace(strings.ToLower(english), " ", "-", -1),
			Namespace: namespace,
			Annotations: map[string]string{
				"en":    english,
				"zh":    chinese,
				"index": strconv.Itoa(index),
			},
		},
		Items: make([]devops.ToolBinding, 0, 10),
	}
	return
}
