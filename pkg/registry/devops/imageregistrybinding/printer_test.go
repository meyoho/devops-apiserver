package imageregistrybinding

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestImageRegistryBindingPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	binding := &devops.ImageRegistryBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "registry-binding-1",
			CreationTimestamp: now,
		},
		Spec: devops.ImageRegistryBindingSpec{
			ImageRegistry: devops.LocalObjectReference{
				Name: "registry",
			},
			Secret: devops.SecretKeySetRef{
				SecretReference: corev1.SecretReference{
					Name: "secret",
				},
			},
			RepoInfo: devops.ImageRegistryBindingRepo{
				Repositories: []string{"/"},
			},
		},
		Status: devops.ServiceStatus{
			Phase: devops.ServiceStatusPhaseError,
		},
	}
	list := &devops.ImageRegistryBindingList{
		Items: []devops.ImageRegistryBinding{*binding},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: binding},
			Cells: []interface{}{
				binding.Name, binding.Spec.ImageRegistry.Name, binding.Spec.Secret.Name, len(binding.Spec.RepoInfo.Repositories), binding.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printImageRegistryBindingList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing binding: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
