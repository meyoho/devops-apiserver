package rest

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
	glog "k8s.io/klog"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	bindingregistry "alauda.io/devops-apiserver/pkg/registry/devops/imageregistrybinding"
	"alauda.io/devops-apiserver/pkg/util/k8s"
)

const TTLSession = 3 * time.Minute

var (
	sessionStore map[string]*ImageRegistrySession
)

func GetSessionStore() map[string]*ImageRegistrySession {
	if sessionStore == nil {
		sessionStore = map[string]*ImageRegistrySession{}
	}
	return sessionStore
}

type ImageRegistrySession struct {
	LastModifiedAt                   time.Time                                `json:"lastModifiedAt"`
	ImageRegistryBindingRepositories *devops.ImageRegistryBindingRepositories `json:"imageRegistryBindingRepositories"`
}

func (s *ImageRegistrySession) IsExpired() bool {
	return s.LastModifiedAt.Add(TTLSession).Before(time.Now())
}

// RepositoryREST implements the binding repository endpoint for a pod
type RepositoryREST struct {
	ImageRegistryStore        *devopsregistry.REST
	ImageRegistryBindingStore *devopsregistry.REST
	SecretLister              corev1listers.SecretLister
	Transport                 http.RoundTripper
}

// NewRepositoryREST starts a new repository store
func NewRepositoryREST(
	imageRegistryStore rest.StandardStorage,
	imageRegistryBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	transport http.RoundTripper,
) rest.Storage {
	api := &RepositoryREST{
		ImageRegistryStore:        imageRegistryStore.(*devopsregistry.REST),
		ImageRegistryBindingStore: imageRegistryBindingStore.(*devopsregistry.REST),
		SecretLister:              secretLister,
		Transport:                 transport,
	}
	return api
}

// New creates a new pod imageregistrybinding repository options object
func (r *RepositoryREST) New() runtime.Object {
	return &devops.ImageRegistryBindingRepositories{}
}

// ProducesObject binding RepositoryREST implements StorageMetadata, return string as the generating object
func (r *RepositoryREST) ProducesObject(verb string) interface{} {
	return ""
}

// Get retrieves a runtime.Object that will stream the contents of ImageRegistry
func (r *RepositoryREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	logName := "imageregistrybindingrepository"
	var (
		secretRef             devops.SecretKeySetRef
		serviceType           devops.ImageRegistryType
		serviceName, endpoint string
		err                   error
		secret                *corev1.Secret
	)
	bindingOpts, ok := opts.(*devops.ImageRegistryBindingRepositoryOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}

	glog.V(9).Infof("%s %s bindingOpts is: %#v", logName, name, bindingOpts)

	binding, err := bindingregistry.GetImageRegistryBinding(r.ImageRegistryBindingStore, ctx, name)
	if err != nil {
		return nil, fmt.Errorf("error get binding %s, err: %v", name, err)
	}

	glog.V(9).Infof("%s %s binding is: %#v", logName, name, binding)

	service, err := bindingregistry.GetImageRegistry(r.ImageRegistryStore, ctx, binding.Spec.ImageRegistry.Name)
	if err != nil {
		return nil, fmt.Errorf("error get service from binding %s, err: %v", name, err)
	}
	glog.V(9).Infof("%s %s service is: %#v", logName, name, service)
	serviceName = service.Name
	serviceType = service.Spec.Type
	serviceUrl, err := bindingregistry.BuildLogURL(service, bindingOpts)
	if err != nil {
		return nil, fmt.Errorf("error get service url from service %s, err: %v", serviceName, err)
	}
	glog.V(9).Infof("%s %s serviceUrl is: %#v", logName, name, serviceUrl)
	endpoint = serviceUrl.String()

	secretRef = binding.Spec.Secret
	if secretRef.Name != "" {
		secret, err = r.SecretLister.Secrets(binding.GetSecretNamespace()).Get(binding.GetSecretName())
		if err != nil {
			return nil, fmt.Errorf("error get secret from service %s, secretName %s err: %v", serviceName, secretRef.Name, err)
		}
	}
	glog.V(9).Infof("%s %s secretRef is: %#v", logName, name, secretRef)
	username := k8s.GetValueInSecret(secret, corev1.BasicAuthUsernameKey)
	credential := k8s.GetValueInSecret(secret, corev1.BasicAuthPasswordKey)

	// reduce frequently request
	//sessionKey := fmt.Sprintf("%s-%s-%s", serviceName, serviceType, username)
	//ss := GetSessionStore()
	//if session, ok := ss[sessionKey]; ok && !session.IsExpired() {
	//	glog.V(3).Infof("Get info from session, key: %s", sessionKey)
	//	return session.ImageRegistryBindingRepositories, nil
	//}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(endpoint),
		devopsclient.NewBasicAuth(username, credential),
		devopsclient.NewTransport(r.Transport),
	)
	serviceClient, err := factory.NewClient(
		serviceType.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	glog.V(9).Infof("%s %s serviceClient is: %#v", logName, name, serviceClient)
	repos, err := serviceClient.GetImageRepos(ctx)
	if err != nil {
		glog.Errorf("%s %s GetImageRepos errer is %s", logName, name, err.Error())
		return nil, err
	}
	glog.Infof("%s %s Get remote repo list from registry: %s, result is: %s", logName, name, service.Name, repos.Items)
	//ss[sessionKey] = &ImageRegistrySession{
	//	ImageRegistryBindingRepositories: repos,
	//	LastModifiedAt:                   time.Now(),
	//}
	return repos, nil
}

func (r *RepositoryREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ImageRegistryBindingRepositoryOptions{}, false, ""
}
