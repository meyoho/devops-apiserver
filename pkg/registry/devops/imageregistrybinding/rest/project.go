package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
	glog "k8s.io/klog"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	bindingregistry "alauda.io/devops-apiserver/pkg/registry/devops/imageregistrybinding"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
)

// ProjectREST implements platform manage the image project
type ProjectREST struct {
	ImageRegistryStore        *devopsregistry.REST
	ImageRegistryBindingStore *devopsregistry.REST
	SecretLister              corev1listers.SecretLister
	Transport                 http.RoundTripper
}

// NewProjectREST starts a new project store
func NewProjectREST(
	imageRegistryStore rest.StandardStorage,
	imageRegistryBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	transport http.RoundTripper,
) rest.Storage {
	api := &ProjectREST{
		ImageRegistryStore:        imageRegistryStore.(*devopsregistry.REST),
		ImageRegistryBindingStore: imageRegistryBindingStore.(*devopsregistry.REST),
		SecretLister:              secretLister,
		Transport:                 transport,
	}
	return api
}

func (r *ProjectREST) ProducesObject(verb string) interface{} {
	return ""
}

func (r *ProjectREST) Get(ctx context.Context, name string, obj runtime.Object) (runtime.Object, error) {
	var repositories []string
	bindingOpts, ok := obj.(*devops.ListProjectOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", bindingOpts)
	}
	isRemote := bindingOpts.IsRemote
	serviceClient, err := r.GetClient(ctx, name)
	if err != nil {
		glog.Errorf("get service client error: %#v", err)
		return nil, err
	}
	projects, err := serviceClient.GetImageProjects(ctx)
	if err != nil {
		return projects, err
	}
	// if isRemote is false, represent this get request return the projects of the binding, else return all projects in registry
	if !isRemote {
		binding, _ := bindingregistry.GetImageRegistryBinding(r.ImageRegistryBindingStore, ctx, name)
		repositories = binding.Spec.RepoInfo.Repositories
	}
	if repositories != nil || len(repositories) != 0 {
		projectItems := projects.Items
		bindingProjects := devops.ProjectDataList{}
		for _, project := range projectItems {
			for _, repo := range repositories {
				if repo == project.Name {
					bindingProjects.Items = append(bindingProjects.Items, project)
				}
			}
		}
		projects.Items = bindingProjects.Items
	}
	return projects, nil
}

func (r *ProjectREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ListProjectOptions{}, false, ""
}

func (r *ProjectREST) New() runtime.Object {
	return &devops.CreateProjectOptions{}
}

var _ = rest.NamedCreater(&ProjectREST{})

func (r *ProjectREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (runtime.Object, error) {
	projectOpt, ok := obj.(*devops.CreateProjectOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", projectOpt)
	}
	serviceClient, err := r.GetClient(ctx, name)
	if err != nil {
		return nil, fmt.Errorf("get service client error: %#v", err)
	}
	err = validation.ValidateImageCreateProject(projectOpt.Name)
	if err != nil {
		return nil, fmt.Errorf("validate project name error: %s", err)
	}
	result, err := serviceClient.CreateImageProject(ctx, projectOpt.Name)

	return result, nil
}

func (r *ProjectREST) GetClient(ctx context.Context, name string) (devopsclient.Interface, error) {
	glog.V(6).Infof("Get registry client with binding: %s", name)
	var (
		secretRef                      devops.SecretKeySetRef
		serviceType                    devops.ImageRegistryType
		endpoint, username, credential string
		err                            error
		namespace                      = genericapirequest.NamespaceValue(ctx)
	)
	binding, err := bindingregistry.GetImageRegistryBinding(r.ImageRegistryBindingStore, ctx, name)
	if err != nil {
		return nil, fmt.Errorf("error get binding %s, err: %v", name, err)
	}

	service, err := bindingregistry.GetImageRegistry(r.ImageRegistryStore, ctx, binding.Spec.ImageRegistry.Name)
	if err != nil {
		return nil, fmt.Errorf("error get service url from service %s, err: %v", service.Name, err)
	}
	secretRef = binding.Spec.Secret
	secretNamespace := namespace
	if secretRef.Namespace != "" {
		secretNamespace = secretRef.Namespace
	}
	username, credential, err = k8s.GetUsernameAndPasswordFromSecret(r.SecretLister, secretRef.Name, corev1.BasicAuthUsernameKey, corev1.BasicAuthPasswordKey, secretNamespace)
	if err != nil {
		return nil, fmt.Errorf("error get user login info from secretRef: %s, err: %v", secretRef.Name, err)
	}

	endpoint = service.GetEndpoint()
	serviceType = service.Spec.Type

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(endpoint),
		devopsclient.NewBasicAuth(username, credential),
		devopsclient.NewTransport(r.Transport),
	)
	serviceClient, err := factory.NewClient(
		serviceType.String(),
		"",
		clientOpts,
	)
	return serviceClient, err
}
