package rest

//import (
//	"context"
//	"strings"
//	"testing"
//
//	"alauda.io/devops-apiserver/pkg/apis/devops"
//	mockregistry "alauda.io/devops-apiserver/pkg/mock/apiserver/registry"
//	"github.com/golang/mock/gomock"
//	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
//	"k8s.io/apimachinery/pkg/runtime"
//	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
//	"k8s.io/apiserver/pkg/registry/rest"
//)
//
//func TestProject(t *testing.T) {
//	ctrl := gomock.NewController(t)
//	defer ctrl.Finish()
//
//	imageRegistryStore := mockregistry.NewMockStandardStorage(ctrl)
//	imageRegistryBindingStore := mockregistry.NewMockStandardStorage(ctrl)
//
//	projectREST := &ProjectREST{
//		ImageRegistryStore: imageRegistryStore,
//		ImageRegistryBindingStore: imageRegistryBindingStore,
//	}
//
//	type TestCase struct {
//		Name string
//		ImageRegistry func() (*devops.ImageRegistry, error)
//		ImageRegistryBinding func() (*devops.ImageRegistryBinding, error)
//		Prepare func() (ctx context.Context, name string, opts runtime.Object)
//		Run func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error)
//		Expect *devops.ImageResult
//		Err error
//	}
//
//	table := []TestCase{
//		{
//			Name: "case 0: all ok create registry project with name",
//			ImageRegistry: func() (registry *devops.ImageRegistry, err error) {
//				registry = &devops.ImageRegistry{
//					Spec: devops.ImageRegistrySpec{
//						Type: devops.RegistryTypeHarbor,
//						ToolSpec: devops.ToolSpec{
//							HTTP: devops.HostPort{
//								AccessURL:  "https://harbor.harbor.sparrow.host",
//								Host: "https://harbor.harbor.sparrow.host",
//							},
//						},
//					},
//				}
//				return
//			},
//			ImageRegistryBinding: func() (binding *devops.ImageRegistryBinding, err error) {
//				binding = &devops.ImageRegistryBinding{
//					Spec: devops.ImageRegistryBindingSpec{
//						Secret: devops.SecretKeySetRef{
//							UsernameKey: "admin",
//							APITokenKey: "Harbor12345",
//						},
//					},
//				}
//				return
//			},
//			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
//				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
//				name = "test"
//				opts = &devops.ImageRegistryProjectOptions{
//					Name: "test-project",
//				}
//				return
//			},
//			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
//				return projectREST.Create(ctx, name, obj, createValidation, nil)
//			},
//			Err: nil,
//			Expect: &devops.ImageResult{
//				StatusCode: 201,
//			},
//		},
//	}
//	for i, test := range table {
//		ctx, name, opts := test.Prepare()
//
//		options := metav1.GetOptions{ResourceVersion: "0"}
//		options.Kind = "ImageRegistryBinding"
//		imageRegistryStore.EXPECT().
//			Get(ctx, name, &options).
//			Return(test.ImageRegistry())
//		imageRegistryBindingStore.EXPECT().
//			Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"}).
//			Return(test.ImageRegistryBinding())
//		result, err := test.Run(ctx, name, opts, func(obj runtime.Object) error { return nil }, false)
//
//		if err == nil && test.Err != nil {
//			t.Errorf("[%d] %s: Should fail but didn't: %v", i, test.Name, test.Err)
//		} else if err != nil && test.Err == nil {
//			t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
//		}
//
//		project, ok := result.(*devops.ImageResult)
//		if ok {
//			if project.StatusCode == test.Expect.StatusCode {
//				t.Log("test successful")
//			} else {
//				t.Errorf("create project failed: %v", project)
//			}
//		} else {
//			t.Errorf("[%d] %s invalid type, should be devops.ImageResult", i, test.Name)
//		}
//	}
//}
