package imageregistrybinding

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// ColumnDefinitions column definitions for PipelineConfig
var columnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
	{Name: "ImageRegistry", Type: registry.TableTypeString, Description: "ImageRegistry instance name"},
	{Name: registry.ResourceSecret, Type: registry.TableTypeString, Description: "Secret name used for accessing"},
	{Name: "Synced Paths", Type: "number", Description: "Number of paths synced"},
	{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "Status of the connectivity to the ImageRegistryBinding instance"},
	{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
}

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {

	h.TableHandler(columnDefinitions, printImageRegistryBindingList)
	h.TableHandler(columnDefinitions, printImageRegistryBinding)

	registry.AddDefaultHandlers(h)

}

// printImageRegistryBindingList prints a imageRegistryBinding list
func printImageRegistryBindingList(imageRegistryBindingList *devops.ImageRegistryBindingList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(imageRegistryBindingList.Items))
	for i := range imageRegistryBindingList.Items {
		r, err := printImageRegistryBinding(&imageRegistryBindingList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

// printImageRegistryBinding prints a imageRegistryBinding
func printImageRegistryBinding(imageRegistryBinding *devops.ImageRegistryBinding, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: imageRegistryBinding},
	}

	// basic rows
	row.Cells = append(row.Cells, imageRegistryBinding.Name, imageRegistryBinding.Spec.ImageRegistry.Name, imageRegistryBinding.Spec.Secret.Name, len(imageRegistryBinding.Spec.RepoInfo.Repositories), imageRegistryBinding.Status.Phase, registry.TranslateTimestamp(imageRegistryBinding.CreationTimestamp))
	if options.Wide {
		// for later use
	}

	return []metav1alpha1.TableRow{row}, nil
}
