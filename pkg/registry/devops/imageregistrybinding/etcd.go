package imageregistrybinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/registry/rest"
)

// NewREST returns a RESTStorage object that will work against API services.
func NewREST(scheme *runtime.Scheme, optsGetter generic.RESTOptionsGetter, factory devopsgeneric.StoreFactory) (*registry.REST, rest.StandardStorage, error) {
	strategy := NewStrategy(scheme)

	store, err := factory(
		optsGetter,
		"imageregistrybindings",
		func() runtime.Object { return &devops.ImageRegistryBinding{} },
		func() runtime.Object { return &devops.ImageRegistryBindingList{} },
		MatchImageRegistryBinding,
		GetAttrs,
		strategy, strategy, strategy,
		AddHandlers,
	)
	if err != nil {
		return nil, nil, err
	}

	statusStore := *store
	statusStore.UpdateStrategy = NewStatusStrategy(strategy)

	statusStorage, err := devopsgeneric.NewStatusREST(&statusStore, func() runtime.Object {
		return &devops.ImageRegistryBinding{}
	})
	if err != nil {
		return nil, nil, err
	}

	return &registry.REST{
		Store:           store,
		Short:           []string{"irb"},
		AddedCategories: []string{"toolbindings", "toolchain", "devops"},
	}, statusStorage, nil
}
