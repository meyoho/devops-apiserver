package pipelineconfig

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestPipelineTaskPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	config1 := &devops.PipelineConfig{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "pipeline-config",
			CreationTimestamp: now,
		},
		Spec: devops.PipelineConfigSpec{
			JenkinsBinding: devops.LocalObjectReference{
				Name: "jenkinsbinding1",
			},
			Triggers: []devops.PipelineTrigger{
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCron,
				},
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCodeChange,
				},
			},
			Source: devops.PipelineSource{
				Git: &devops.PipelineSourceGit{
					URI: "https://github.com/alauda/alauda",
					Ref: "master",
				},
			},
			Strategy: devops.PipelineStrategy{
				Jenkins: devops.PipelineStrategyJenkins{
					JenkinsfilePath: "./Jenkinsfile",
				},
			},
		},
		Status: devops.PipelineConfigStatus{
			Phase: devops.PipelineConfigPhaseReady,
		},
	}
	config2 := &devops.PipelineConfig{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "pipeline-config2",
			CreationTimestamp: now,
		},
		Spec: devops.PipelineConfigSpec{
			JenkinsBinding: devops.LocalObjectReference{
				Name: "jenkinsbinding2",
			},
			Triggers: []devops.PipelineTrigger{},
			Source: devops.PipelineSource{
				Git: nil,
			},
			Template: devops.PipelineTemplateWithValue{
				PipelineTemplateSource: devops.PipelineTemplateSource{
					PipelineTemplateRef: devops.PipelineTemplateRef{
						Name: "MyTemplate",
						Kind: devops.TypeClusterPipelineTemplate,
					},
				},
			},
		},
		Status: devops.PipelineConfigStatus{
			Phase: devops.PipelineConfigPhaseCreating,
		},
	}
	list := &devops.PipelineConfigList{
		Items: []devops.PipelineConfig{*config1, *config2},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: &list.Items[0]},
			Cells: []interface{}{
				// name - displayName.en  - version - category label - timestamp - agent label - engine
				"pipeline-config", "Jenkinsfile", devops.PipelineConfigPhaseReady, registry.TranslateTimestamp(now),
				string(devops.PipelineTriggerTypeCron) + " " + string(devops.PipelineTriggerTypeCodeChange),
				"https://github.com/alauda/alauda (master)", "jenkinsbinding1",
			},
		},
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: &list.Items[1]},
			Cells: []interface{}{
				// name - displayName.en  - version - category label - timestamp - agent label - engine
				"pipeline-config2", "Template (ClusterPipelineTemplate/MyTemplate)", devops.PipelineConfigPhaseCreating, registry.TranslateTimestamp(now),
				"-", "-", "jenkinsbinding2",
			},
		},
	}

	result, err := printPipelineList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing pipeline config: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
