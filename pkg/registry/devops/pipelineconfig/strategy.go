/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package pipelineconfig

import (
	"context"
	"fmt"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper, provider devops.AnnotationProvider) Strategy {
	return Strategy{typer, names.SimpleNameGenerator, provider}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	apiserver, ok := obj.(*devops.PipelineConfig)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a PipelineConfig")
	}
	return labels.Set(apiserver.ObjectMeta.Labels), toSelectableFields(apiserver), apiserver.Initializers != nil, nil
}

// MatchPipelineConfig is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchPipelineConfig(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.PipelineConfig) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// Strategy strategy for PipelineConfig
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
	provider devops.AnnotationProvider
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// PrepareForCreate adds basic fields for creation
func (s Strategy) PrepareForCreate(ctx context.Context, obj runtime.Object) {
	config := obj.(*devops.PipelineConfig)

	conditions := initializeConditions(config)
	config.Status = calculateStatus(conditions)
}

// PrepareForUpdate Verify if any of the following parts were changed and
// cleanup status if any change happened
// to make sure the controller will check the data again
func (s Strategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) {
	newConfig := obj.(*devops.PipelineConfig)
	oldConfig := old.(*devops.PipelineConfig)

	oldChecksum := oldConfig.Annotations[s.provider.AnnotationsKeyChecksum()]
	newChecksum := newConfig.Annotations[s.provider.AnnotationsKeyChecksum()]

	// if spec changed, we will re-initialize conditions
	var conditions []devops.Condition
	if oldChecksum != newChecksum {
		conditions = initializeConditions(newConfig)
	} else {
		conditions = newConfig.Status.Conditions
		// re-initialize conditions if it is nil
		if len(conditions) == 0 {
			conditions = initializeConditions(newConfig)
		}
	}

	newConfig.Status = calculateStatus(conditions)
}

func initializeConditions(pipelineConfig *devops.PipelineConfig) []devops.Condition {
	metaTime := metav1.NewTime(time.Now())
	conditions := []devops.Condition{
		{
			Type:        devops.PipelineConfigConditionTypeInitialized,
			LastAttempt: &metaTime,
			Status:      devops.ConditionStatusUnknown,
		},
		{
			Type:        devops.PipelineConfigConditionTypeSynced,
			LastAttempt: &metaTime,
			Status:      devops.ConditionStatusUnknown,
		},
	}
	if isMultiBranch(pipelineConfig) {
		conditions = append(conditions, devops.Condition{
			Type:        devops.PipelineConfigConditionTypeSupportPRDiscovery,
			LastAttempt: &metaTime,
			Status:      devops.ConditionStatusUnknown,
		})
	}
	return conditions
}

func calculateStatus(conditions []devops.Condition) devops.PipelineConfigStatus {
	metaTime := metav1.NewTime(time.Now())
	status := devops.PipelineConfigStatus{
		Phase:      devops.PipelineConfigPhaseCreating,
		Conditions: conditions,
		LastUpdate: &metaTime,
	}

	for _, cond := range conditions {
		// SupportPRDiscovery condition should not affect status
		if cond.Type == devops.PipelineConfigConditionTypeSupportPRDiscovery {
			continue
		}

		if cond.Status == devops.ConditionStatusFalse {
			status.Phase = devops.PipelineConfigPhaseError
			status.Reason = cond.Reason
			status.Message = cond.Message
			break
		}

		if cond.Type == devops.PipelineConfigConditionTypeInitialized {
			if cond.Status == devops.ConditionStatusUnknown {
				status.Phase = devops.PipelineConfigPhaseCreating
				break
			}
		}

		if cond.Type == devops.PipelineConfigConditionTypeSynced {
			if cond.Status == devops.ConditionStatusUnknown {
				status.Phase = devops.PipelineConfigPhaseSyncing
				break
			} else if cond.Status == devops.ConditionStatusTrue {
				status.Phase = devops.PipelineConfigPhaseReady
			}
		}
	}

	return status
}

func isMultiBranch(pipelineConfig *devops.PipelineConfig) bool {
	return pipelineConfig.Labels[devops.LabelPipelineKind] == devops.LabelPipelineKindMultiBranch
}

// Validate validates a request object
func (Strategy) Validate(ctx context.Context, obj runtime.Object) (errs field.ErrorList) {
	config := obj.(*devops.PipelineConfig)
	return validation.ValidatePipelineConfig(config)
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) (errs field.ErrorList) {
	errs = validation.ValidatePipelineConfig(obj.(*devops.PipelineConfig))
	return append(errs, validation.ValidatePipelineConfigUpdate(obj.(*devops.PipelineConfig), old.(*devops.PipelineConfig))...)
}
