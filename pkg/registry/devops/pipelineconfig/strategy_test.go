package pipelineconfig_test

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/pipelineconfig"
	"context"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestPipelineConfigStrategy(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("pipelineconfig.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/registry/devops/pipelineconfig", []Reporter{junitReporter})
}

var _ = Describe("PipelineConfig.Strategy", func() {
	var (
		scheme             *runtime.Scheme
		annotationProvider devopsv1alpha1.AnnotationProvider
		strategy           pipelineconfig.Strategy

		newPipelineConfig *devopsv1alpha1.PipelineConfig
		oldPipelineConfig *devopsv1alpha1.PipelineConfig
	)

	BeforeEach(func() {
		scheme = runtime.NewScheme()
		annotationProvider = devopsv1alpha1.AnnotationProvider{BaseDomain: "alauda.io"}
		strategy = pipelineconfig.NewStrategy(scheme, annotationProvider)
	})

	Context("Create a new pipelineconfig", func() {
		BeforeEach(func() {
			newPipelineConfig = &devopsv1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineConfigSpec{},
				Status:     devopsv1alpha1.PipelineConfigStatus{
					// content is not important, it should be kept the same
				},
			}

			strategy.PrepareForCreate(context.TODO(), newPipelineConfig)
		})

		It("should initialize all conditions and set phase to creating", func() {
			Expect(newPipelineConfig.Status.Phase).To(Equal(devopsv1alpha1.PipelineConfigPhaseCreating))
			Expect(len(newPipelineConfig.Status.Conditions)).To(Equal(2))
			Expect(newPipelineConfig.Status.Conditions[0].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeInitialized))
			Expect(newPipelineConfig.Status.Conditions[0].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
			Expect(newPipelineConfig.Status.Conditions[1].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeSynced))
			Expect(newPipelineConfig.Status.Conditions[1].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
		})
	})

	Context("PipelineConfig initialized", func() {
		BeforeEach(func() {
			newPipelineConfig = &devopsv1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineConfigSpec{},
				Status: devopsv1alpha1.PipelineConfigStatus{
					Phase: devopsv1alpha1.PipelineConfigPhaseCreating,
					// content is not important, it should be kept the same
					Conditions: []devopsv1alpha1.Condition{
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeInitialized,
							Status: devopsv1alpha1.ConditionStatusTrue,
						},
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeSynced,
							Status: devopsv1alpha1.ConditionStatusUnknown,
						},
					},
				},
			}

			strategy.PrepareForUpdate(context.TODO(), newPipelineConfig, newPipelineConfig)
		})

		It("should set phase to Syncing", func() {
			Expect(newPipelineConfig.Status.Phase).To(Equal(devopsv1alpha1.PipelineConfigPhaseSyncing))
			Expect(len(newPipelineConfig.Status.Conditions)).To(Equal(2))
			Expect(newPipelineConfig.Status.Conditions[0].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeInitialized))
			Expect(newPipelineConfig.Status.Conditions[0].Status).To(Equal(devopsv1alpha1.ConditionStatusTrue))
			Expect(newPipelineConfig.Status.Conditions[1].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeSynced))
			Expect(newPipelineConfig.Status.Conditions[1].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
		})
	})

	Context("PipelineConfig sycned", func() {
		BeforeEach(func() {
			newPipelineConfig = &devopsv1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineConfigSpec{},
				Status: devopsv1alpha1.PipelineConfigStatus{
					Phase: devopsv1alpha1.PipelineConfigPhaseSyncing,
					// content is not important, it should be kept the same
					Conditions: []devopsv1alpha1.Condition{
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeInitialized,
							Status: devopsv1alpha1.ConditionStatusTrue,
						},
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeSynced,
							Status: devopsv1alpha1.ConditionStatusTrue,
						},
					},
				},
			}

			strategy.PrepareForUpdate(context.TODO(), newPipelineConfig, newPipelineConfig)
		})

		It("should set phase to Ready", func() {
			Expect(newPipelineConfig.Status.Phase).To(Equal(devopsv1alpha1.PipelineConfigPhaseReady))
			Expect(len(newPipelineConfig.Status.Conditions)).To(Equal(2))
			Expect(newPipelineConfig.Status.Conditions[0].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeInitialized))
			Expect(newPipelineConfig.Status.Conditions[0].Status).To(Equal(devopsv1alpha1.ConditionStatusTrue))
			Expect(newPipelineConfig.Status.Conditions[1].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeSynced))
			Expect(newPipelineConfig.Status.Conditions[1].Status).To(Equal(devopsv1alpha1.ConditionStatusTrue))
		})
	})

	Context("PipelineConfig initializing failed", func() {
		BeforeEach(func() {
			newPipelineConfig = &devopsv1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineConfigSpec{},
				Status: devopsv1alpha1.PipelineConfigStatus{
					Phase: devopsv1alpha1.PipelineConfigPhaseCreating,
					// content is not important, it should be kept the same
					Conditions: []devopsv1alpha1.Condition{
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeInitialized,
							Status: devopsv1alpha1.ConditionStatusFalse,
						},
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeSynced,
							Status: devopsv1alpha1.ConditionStatusUnknown,
						},
					},
				},
			}

			strategy.PrepareForUpdate(context.TODO(), newPipelineConfig, newPipelineConfig)
		})

		It("should set phase to Error", func() {
			Expect(newPipelineConfig.Status.Phase).To(Equal(devopsv1alpha1.PipelineConfigPhaseError))
			Expect(len(newPipelineConfig.Status.Conditions)).To(Equal(2))
			Expect(newPipelineConfig.Status.Conditions[0].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeInitialized))
			Expect(newPipelineConfig.Status.Conditions[0].Status).To(Equal(devopsv1alpha1.ConditionStatusFalse))
			Expect(newPipelineConfig.Status.Conditions[1].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeSynced))
			Expect(newPipelineConfig.Status.Conditions[1].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
		})
	})

	Context("PipelineConfig syncing failed", func() {
		BeforeEach(func() {
			newPipelineConfig = &devopsv1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineConfigSpec{},
				Status: devopsv1alpha1.PipelineConfigStatus{
					Phase: devopsv1alpha1.PipelineConfigPhaseCreating,
					// content is not important, it should be kept the same
					Conditions: []devopsv1alpha1.Condition{
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeInitialized,
							Status: devopsv1alpha1.ConditionStatusTrue,
						},
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeSynced,
							Status: devopsv1alpha1.ConditionStatusFalse,
						},
					},
				},
			}

			strategy.PrepareForUpdate(context.TODO(), newPipelineConfig, newPipelineConfig)
		})

		It("should set phase to Error", func() {
			Expect(newPipelineConfig.Status.Phase).To(Equal(devopsv1alpha1.PipelineConfigPhaseError))
			Expect(len(newPipelineConfig.Status.Conditions)).To(Equal(2))
			Expect(newPipelineConfig.Status.Conditions[0].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeInitialized))
			Expect(newPipelineConfig.Status.Conditions[0].Status).To(Equal(devopsv1alpha1.ConditionStatusTrue))
			Expect(newPipelineConfig.Status.Conditions[1].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeSynced))
			Expect(newPipelineConfig.Status.Conditions[1].Status).To(Equal(devopsv1alpha1.ConditionStatusFalse))
		})
	})

	Context("PipelineConfig spec changed", func() {
		BeforeEach(func() {
			newPipelineConfig = &devopsv1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						"alauda.io/checksum": "1",
					},
				},
				Spec: devopsv1alpha1.PipelineConfigSpec{},
				Status: devopsv1alpha1.PipelineConfigStatus{
					Phase: devopsv1alpha1.PipelineConfigPhaseError,
					// content is not important, it should be kept the same
					Conditions: []devopsv1alpha1.Condition{
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeInitialized,
							Status: devopsv1alpha1.ConditionStatusTrue,
						},
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeSynced,
							Status: devopsv1alpha1.ConditionStatusFalse,
						},
					},
				},
			}

			oldPipelineConfig = &devopsv1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						"alauda.io/checksum": "0",
					},
				},
				Spec: devopsv1alpha1.PipelineConfigSpec{},
				Status: devopsv1alpha1.PipelineConfigStatus{
					Phase: devopsv1alpha1.PipelineConfigPhaseError,
					// content is not important, it should be kept the same
					Conditions: []devopsv1alpha1.Condition{
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeInitialized,
							Status: devopsv1alpha1.ConditionStatusTrue,
						},
						{
							Type:   devopsv1alpha1.PipelineConfigConditionTypeSynced,
							Status: devopsv1alpha1.ConditionStatusFalse,
						},
					},
				},
			}

			strategy.PrepareForUpdate(context.TODO(), newPipelineConfig, oldPipelineConfig)
		})

		It("should initializing conditions and set phase to creating", func() {
			Expect(newPipelineConfig.Status.Phase).To(Equal(devopsv1alpha1.PipelineConfigPhaseCreating))
			Expect(len(newPipelineConfig.Status.Conditions)).To(Equal(2))
			Expect(newPipelineConfig.Status.Conditions[0].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeInitialized))
			Expect(newPipelineConfig.Status.Conditions[0].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
			Expect(newPipelineConfig.Status.Conditions[1].Type).To(Equal(devopsv1alpha1.PipelineConfigConditionTypeSynced))
			Expect(newPipelineConfig.Status.Conditions[1].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
		})
	})
})
