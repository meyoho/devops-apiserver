package pipelineconfig

import (
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {
	pipelineConfigColumnDefinitions := []metav1alpha1.TableColumnDefinition{
		{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
		{Name: registry.ResourceType, Type: registry.TableTypeString, Description: "Pipeline Type"},
		{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "The State of the pipeline."},
		{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
		{Name: "Triggers", Type: registry.TableTypeString, Priority: 1, Description: "Triggers used in PipelineConfig."},
		{Name: "Code Repository", Type: registry.TableTypeString, Priority: 1, Description: "Code Repository used"},
		{Name: "JenkinsBinding", Type: registry.TableTypeString, Priority: 1, Description: "JenkinsBinding which it relates to."},

		// {Name: "Stages", Type: registry.TableTypeString,  Priority: 1, Description: "Pipeline stages."},
	}

	h.TableHandler(pipelineConfigColumnDefinitions, printPipelineList)
	h.TableHandler(pipelineConfigColumnDefinitions, printPipeline)

	registry.AddDefaultHandlers(h)

}

func printPipelineList(pipelineList *devops.PipelineConfigList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(pipelineList.Items))
	for i := range pipelineList.Items {
		r, err := printPipeline(&pipelineList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

func printPipeline(pipeline *devops.PipelineConfig, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: pipeline},
	}

	configType := "Jenkinsfile"
	if pipeline.Spec.Template.PipelineTemplateRef.Name != "" {
		configType = fmt.Sprintf("Template (%s/%s)", pipeline.Spec.Template.PipelineTemplateRef.Kind, pipeline.Spec.Template.PipelineTemplateRef.Name)
	} else if pipeline.Spec.Template.PipelineTemplate != nil {
		configType = fmt.Sprintf("Graph Template (%s/%s)", pipeline.Labels[devops.LabelTemplateKind], pipeline.Labels[devops.LabelTemplateName])
	}

	// basic rows
	row.Cells = append(row.Cells, pipeline.Name, configType, pipeline.Status.Phase, registry.TranslateTimestamp(pipeline.CreationTimestamp))
	if options.Wide {

		triggers := "-"
		if len(pipeline.Spec.Triggers) > 0 {
			triggers = ""
			space := ""
			for i, t := range pipeline.Spec.Triggers {
				if i > 0 {
					space = " "
				}
				triggers = fmt.Sprintf("%s%s%s", triggers, space, t.Type)
			}
		}
		codeRepo := "-"
		if pipeline.Spec.Source.Git != nil {
			codeRepo = fmt.Sprintf("%s (%s)", pipeline.Spec.Source.Git.URI, pipeline.Spec.Source.Git.Ref)
		}

		// adding to rows
		row.Cells = append(row.Cells, triggers, codeRepo, pipeline.Spec.JenkinsBinding.Name)
	}
	return []metav1alpha1.TableRow{row}, nil
}
