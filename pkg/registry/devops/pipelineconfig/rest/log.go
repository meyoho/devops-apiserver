package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	glog "k8s.io/klog"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/dependency"
	openapiruntime "github.com/go-openapi/runtime"
	"k8s.io/apimachinery/pkg/api/errors"

	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

// LogREST implements the log endpoint for a Pod
type LogREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

// NewLogREST starts a new log store
func NewLogREST(dependencyGetter dependency.Manager, roundTripper http.RoundTripper) *LogREST {
	return &LogREST{
		Getter:       dependencyGetter,
		RoundTripper: roundTripper,
	}
}

// LogREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&LogREST{})

// New creates a new PipelineLogOptions object
func (r *LogREST) New() runtime.Object {
	return &devops.PipelineConfigLogOptions{}
}

// ProducesMIMETypes LogREST implements StorageMetadata
func (r *LogREST) ProducesMIMETypes(verb string) []string {
	// the "produces" section for pipelines/{name}/log
	return []string{
		"application/json",
	}
}

// ProducesObject LogREST implements StorageMetadata, return PipelineLog as the generating object
func (r *LogREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.PipelineConfigLog{}
}

// Get retrieves a runtime.Object that will stream the contents of the pod log
func (r *LogREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var ok bool
	interOpts, ok := opts.(*devops.PipelineConfigLogOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opts))
	}
	namespace := genericapirequest.NamespaceValue(ctx)
	glog.V(3).Infof(
		"got log options: start: %d namespace: %v",
		interOpts.Start, namespace,
	)

	pipelineConfig, _, jenkins, secret, err := getDependency(ctx, name, r.Getter)
	if err != nil {
		return nil, err
	}

	// only support for multi-branch pipeline
	if pipelineConfig.Labels == nil {
		return nil, errors.NewBadRequest("Not support to scan, except multi-branch pipeline")
	}
	if kind, ok := pipelineConfig.Labels[devops.LabelPipelineKind]; !ok || kind != devops.LabelPipelineKindMultiBranch {
		return nil, errors.NewBadRequest("Not support to scan, except multi-branch pipeline")
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(jenkins.GetHostPort().Host),
		devopsclient.NewBasicAuth(k8s.GetDataBasicAuthFromSecret(secret).Username, k8s.GetDataBasicAuthFromSecret(secret).Password),
		devopsclient.NewTransport(r.RoundTripper),
	)
	jenkinsClient, err := factory.NewClient(
		jenkins.GetKindType(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get ContinuousIntegrationClient from Jenkins: %s, err: %v", jenkins.GetName(), err))
	}

	log, err := jenkinsClient.GetBranchProgressiveScanLog(ctx, namespace, name, interOpts.Start)

	if err != nil {
		if apiErr, ok := err.(*openapiruntime.APIError); ok && apiErr.Code == 404 {
			return &devops.PipelineLog{
				HasMore: false,
				Text:    "",
			}, nil
		} else {
			return nil, errors.NewInternalError(fmt.Errorf("error get logs from Pipeline: %s/%s, err: %v", namespace, name, err))

		}
	}

	return log, nil

}

// NewGetOptions creates a new options object
func (r *LogREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.PipelineConfigLogOptions{}, false, ""
}
