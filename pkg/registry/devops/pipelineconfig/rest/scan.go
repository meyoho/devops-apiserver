package rest

import (
	"context"
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	k8sutil "alauda.io/devops-apiserver/pkg/util/k8s"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"

	"net/http"

	"k8s.io/apiserver/pkg/registry/rest"
)

// ScanREST implements the preview endpoint
type ScanREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

// NewScanREST create rest for preview api
func NewScanREST(dependencyGetter dependency.Manager, roundTripper http.RoundTripper) *ScanREST {
	return &ScanREST{
		Getter:       dependencyGetter,
		RoundTripper: roundTripper,
	}
}

// ScanREST implements NamedCreater
var _ = rest.NamedCreater(&ScanREST{})

// New create a new scan option object
func (r *ScanREST) New() runtime.Object {
	return &devops.PipelineConfigScanOptions{}
}

// Create genereate jenkinsfile from PipelineTemplate
func (r *ScanREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (runtime.Object, error) {
	pipelineConfig, _, jenkins, secret, err := getDependency(ctx, name, r.Getter)
	namespace := genericapirequest.NamespaceValue(ctx)

	if err != nil {
		return &devops.PipelineConfigScanResult{
			Code:    -3,
			Success: false,
			Message: fmt.Sprintf("Get dependency error: %v", err),
		}, err
	}

	// do not allow to scan if it's disabled
	if pipelineConfig.Spec.Disabled {
		return &devops.PipelineConfigScanResult{
			Code:    -4,
			Success: false,
			Message: fmt.Sprintf("Cannot scan %s because it's disabled", pipelineConfig.Name),
		}, err
	}

	// only support for multi-branch pipeline
	if !k8sutil.HasMultibranchLabel(pipelineConfig.Labels) {
		const NotSupportScan = "Not support to scan, except multi-branch pipeline"
		return &devops.PipelineConfigScanResult{
			Code:    -2,
			Success: false,
			Message: fmt.Sprintf(NotSupportScan),
		}, errors.NewBadRequest(NotSupportScan)
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(jenkins.GetHostPort().Host),
		devopsclient.NewBasicAuth(k8s.GetDataBasicAuthFromSecret(secret).Username, k8s.GetDataBasicAuthFromSecret(secret).Password),
		devopsclient.NewTransport(r.RoundTripper),
	)
	jenkinsClient, err := factory.NewClient(
		jenkins.GetKindType(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get ContinuousIntegrationClient from Jenkins: %s, err: %v", jenkins.GetName(), err))
	}

	err = jenkinsClient.ScanMultiBranchJob(ctx, namespace, name)
	if err != nil {
		return &devops.PipelineConfigScanResult{
			Code:    -1,
			Success: false,
			Message: fmt.Sprintf("error getting response, error: %v", err),
		}, errors.NewBadRequest(fmt.Sprintf("error getting response, error: %v", err))
	}

	return &devops.PipelineConfigScanResult{
		Code:    0,
		Success: true,
	}, nil
}
