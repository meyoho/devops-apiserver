package rest

import (
	"context"
	"encoding/json"
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/registry/generic"
	jenkinsfilext "bitbucket.org/mathildetech/jenkinsfilext/v2/domain"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	glog "k8s.io/klog"
)

const PipelineConfigPreviewREST = "PipelineConfigPreviewREST"

func NewPreviewREST(getter dependency.Manager) *generic.PreviewREST {
	p := generic.NewPreviewREST(
		"PipelineConfigPreviewREST",
		getter,
		getPipelineTemplate,
	)
	p.HandlePreviewOptions = usePipelineConfigPreviewOptions
	return p
}

func usePipelineConfigPreviewOptions(ctx context.Context, getter dependency.Manager, name string, opts *devops.JenkinsfilePreviewOptions) error {
	// if setted opts.Values by client, just use it
	if opts != nil && len(opts.Values) > 0 {
		return nil
	}

	config, err := getPipelineConfig(ctx, getter, name)
	if err != nil {
		return err
	}

	var values map[string]string
	//created by pipeline template ref // use default config values
	if config.Spec.Template.PipelineTemplateRef.Name != "" {
		values = config.Spec.Template.Values
	}
	// created by pipeline template
	if config.Spec.Template.PipelineTemplate != nil {
		var err error
		values, err = unfoldGraphValues(config.Spec.Template.GraphValues)
		if err != nil {
			glog.Errorf("Unfold graph values error:%s, pipelineconfig:%s/%s", err.Error(), config.Namespace, config.Name)
			return err
		}
	}

	opts.Environments = config.Spec.Environments
	opts.Values = values
	glog.V(7).Infof("[%s] get the previewoption is %v", PipelineConfigPreviewREST, opts)

	return nil
}

func unfoldGraphValues(graphValues map[string]devops.KeyValueSet) (map[string]string, error) {

	var values = map[string]string{}

	for stageName, keyValues := range graphValues {
		for _, keyValue := range keyValues {
			name := jenkinsfilext.BuildArgName(stageName, keyValue.Name)
			switch keyValue.Value.Type {
			case devops.String:
				values[name] = keyValue.Value.StringVal
			case devops.Int:
				values[name] = fmt.Sprint(keyValue.Value.IntVal)
			case devops.Bool:
				values[name] = fmt.Sprint(keyValue.Value.BoolVal)
			default:
				jsonByts, err := json.Marshal(keyValue.Value)
				if err != nil {
					glog.Errorf("cannot marshal %#v to json, error:%s", keyValue, err.Error())
					return values, err
				}
				values[name] = string(jsonByts)
			}
		}
	}

	return values, nil
}

func getPipelineConfig(ctx context.Context, getter dependency.Manager, name string) (*devops.PipelineConfig, error) {
	dp := getter.Get(ctx, devops.TypePipelineConfig, name)
	err := dp.Validate()
	if err != nil {
		glog.Errorf("[%s] get PipelineConfig %s error, context:%#v, err:%s", PipelineConfigPreviewREST, name, ctx, err.Error())
		return nil, err
	}

	config := &devops.PipelineConfig{}

	dpList := dp.GetInto(devops.TypePipelineConfig, config)
	err = dpList.Validate()
	if err != nil {
		glog.Errorf("[%s] get PipelineConfig %s error, context:%#v, err:%s", PipelineConfigPreviewREST, name, ctx, err.Error())
		return nil, err
	}
	return config, nil
}

func getPipelineTemplate(ctx context.Context, getter dependency.Manager, name string) (*devops.PipelineTemplateRef, error) {

	config, err := getPipelineConfig(ctx, getter, name)
	if err != nil {
		return nil, err
	}

	if config.Spec.Template.PipelineTemplateRef.Name != "" {
		return &config.Spec.Template.PipelineTemplateRef, nil
	}

	// Created by graph
	if config.Spec.Template.PipelineTemplate != nil {

		templateName, ok := config.ObjectMeta.Labels[devops.LabelTemplateName]
		if !ok {
			glog.V(7).Infof("[%s] no templateName in config labels , pipelineconfig=%s/%s", PipelineConfigPreviewREST, config.Namespace, config.Name)
			return nil, nil
		}

		return &devops.PipelineTemplateRef{
			Kind:      devops.TypePipelineTemplate,
			Name:      templateName,
			Namespace: config.Namespace,
		}, nil
	}

	glog.Errorf("[%s] cannot get pipeline template name for pipelineconfig '%s/%s', maybe it was not created by template.",
		PipelineConfigPreviewREST, genericapirequest.NamespaceValue(ctx), name)
	return nil, fmt.Errorf("pipelineconfig %s is not created by template", name)
}
