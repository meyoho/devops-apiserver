package rest

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"k8s.io/api/core/v1"
	"net/http"
	"net/url"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mdependency "alauda.io/devops-apiserver/pkg/mock/dependency"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

func TestScan(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	roundTripper := mhttp.NewMockRoundTripper(ctrl)
	dependencyGetter := mdependency.NewMockManager(ctrl)
	scanREST := NewScanREST(dependencyGetter, roundTripper)

	type TestCase struct {
		Name           string
		PipelineConfig func() (*devops.PipelineConfig, error)
		JenkinsBinding func() (*devops.JenkinsBinding, error)
		Jenkins        func() (*devops.Jenkins, error)
		Secret         func() (*v1.Secret, error)
		Prepare        func() (ctx context.Context, name string, opts runtime.Object)
		Run            func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error)
		Err            error
	}

	table := []TestCase{
		{
			Name: "case 0, all ok",
			PipelineConfig: func() (pc *devops.PipelineConfig, err error) {
				return &devops.PipelineConfig{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							devops.LabelPipelineKind: devops.LabelPipelineKindMultiBranch,
						},
						Namespace: "test",
					},
					Spec: devops.PipelineConfigSpec{
						JenkinsBinding: devops.LocalObjectReference{
							Name: "JenkinsBinding",
						},
					},
				}, nil
			},
			JenkinsBinding: func() (binding *devops.JenkinsBinding, err error) {
				return &devops.JenkinsBinding{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "JenkinsBinding",
						Namespace: "test",
					},
					Spec: devops.JenkinsBindingSpec{
						Jenkins: devops.JenkinsInstance{
							Name: "Jenkins",
						},
					},
				}, nil
			},
			Jenkins: func() (jenkins *devops.Jenkins, err error) {
				return &devops.Jenkins{
					ObjectMeta: metav1.ObjectMeta{
						Name: "Jenkins",
					},
					Spec: devops.JenkinsSpec{
						ToolSpec: devops.ToolSpec{
							HTTP: devops.HostPort{
								Host: "http://localhost",
							},
						},
					},
				}, nil
			},
			Secret: func() (*v1.Secret, error) {
				return &v1.Secret{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "secret",
						Namespace: "test",
					},
					Type: v1.SecretTypeBasicAuth,
					Data: map[string][]byte{
						"username": []byte("admin"),
						"password": []byte("123456"),
					},
				}, nil
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"

				crumbURL := &url.URL{
					Host:   "localhost",
					Scheme: "http",
					Path:   "/crumbIssuer/api/json",
				}
				crumbRequest, _ := http.NewRequest(http.MethodGet,
					crumbURL.String(), nil)
				crumbRequest.Header.Add("Accept", "application/json")
				crumbRequest.SetBasicAuth("admin", "123456")
				crumbRequest.Host = ""
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewRequestMatcher(crumbRequest)).
					Return(&http.Response{
						StatusCode: http.StatusOK,
						Body:       ioutil.NopCloser(bytes.NewReader([]byte(`{"_class":"hudson.security.csrf.DefaultCrumbIssuer","crumb":"6e78c5725ea0c522bdcb787d548465aa","crumbRequestField":"Jenkins-Crumb"}`))),
					}, nil)

				requestURL := &url.URL{
					Scheme: "http",
					Host:   "localhost",
					Path:   "/job/test/job/test-test/build",
				}
				request, _ := http.NewRequest(http.MethodPost,
					requestURL.RequestURI(), nil)
				request.URL = requestURL
				request.SetBasicAuth("admin", "123456")
				request.Header.Add("Accept", "*/*")
				request.Header.Add("Jenkins-Crumb", "6e78c5725ea0c522bdcb787d548465aa")
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewRequestMatcher(request)).
					Return(&http.Response{
						StatusCode: http.StatusOK,
						Body:       ioutil.NopCloser(bytes.NewBuffer([]byte(""))),
					}, nil)

				return
			},
			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return scanREST.Create(ctx, name, obj, createValidation, nil)
			},
			Err: nil,
		},
		{
			Name: "case 1, don't contains multi-branch label",
			PipelineConfig: func() (pc *devops.PipelineConfig, err error) {
				return &devops.PipelineConfig{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							// devops.LabelPipelineKind: devops.LabelPipelineKindMultiBranch,
						},
						Namespace: "test",
					},
					Spec: devops.PipelineConfigSpec{
						JenkinsBinding: devops.LocalObjectReference{
							Name: "JenkinsBinding",
						},
					},
				}, nil
			},
			JenkinsBinding: func() (binding *devops.JenkinsBinding, err error) {
				return &devops.JenkinsBinding{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "JenkinsBinding",
						Namespace: "test",
					},
					Spec: devops.JenkinsBindingSpec{
						Jenkins: devops.JenkinsInstance{
							Name: "Jenkins",
						},
					},
				}, nil
			},
			Jenkins: func() (jenkins *devops.Jenkins, err error) {
				return &devops.Jenkins{
					ObjectMeta: metav1.ObjectMeta{
						Name: "Jenkins",
					},
					Spec: devops.JenkinsSpec{
						ToolSpec: devops.ToolSpec{
							HTTP: devops.HostPort{
								Host: "http://localhost",
							},
						},
					},
				}, nil
			},
			Secret: func() (*v1.Secret, error) {
				return &v1.Secret{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "secret",
						Namespace: "test",
					},
					Type: v1.SecretTypeBasicAuth,
					Data: map[string][]byte{
						"username": []byte("admin"),
						"password": []byte("123456"),
					},
				}, nil
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				return
			},
			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return scanREST.Create(ctx, name, obj, createValidation, nil)
			},
			Err: fmt.Errorf("Not support to scan, except multi-branch pipeline"),
		},
	}

	for i, test := range table {
		ctx, name, opts := test.Prepare()

		pc, _ := test.PipelineConfig()
		jenkinsBinding, _ := test.JenkinsBinding()
		jenkins, _ := test.Jenkins()
		secret, _ := test.Secret()

		dependencyGetter.EXPECT().
			Get(ctx, devops.TypePipelineConfig, name).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypePipelineConfig,
					Object:   pc,
					IntoFunc: dependency.PipelineConfigCopyInto,
				},
				&dependency.Item{
					Kind:     devops.TypeJenkinsBinding,
					Object:   jenkinsBinding,
					IntoFunc: dependency.JenkinsBindingCopyInto,
				},
				&dependency.Item{
					Kind:     devops.TypeJenkins,
					Object:   jenkins,
					IntoFunc: dependency.JenkinsCopyInto,
				}, &dependency.Item{
					Kind:     devops.TypeSecret,
					Object:   secret,
					IntoFunc: dependency.CoreV1SecretCopyInto,
				},
			})

		_, err := test.Run(ctx, name, opts, func(obj runtime.Object) error { return nil }, false)

		if err == nil && test.Err != nil {
			t.Errorf("[%d] %s: Should fail but didn't: %v", i, test.Name, test.Err)
		} else if err != nil && test.Err == nil {
			t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
		}
	}
}
