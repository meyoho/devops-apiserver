package rest

import (
	"context"

	"alauda.io/devops-apiserver/pkg/dependency"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	corev1 "k8s.io/api/core/v1"
)

func getDependency(ctx context.Context, name string, getter dependency.Manager) (pipelineConfig *devops.PipelineConfig,
	jenkinsBinding *devops.JenkinsBinding, jenkins *devops.Jenkins, secret *corev1.Secret, err error) {
	depItemList := getter.Get(ctx, devops.TypePipelineConfig, name)
	if err = depItemList.Validate(); err != nil {
		return
	}

	pipelineConfig = &devops.PipelineConfig{}
	jenkinsBinding = &devops.JenkinsBinding{}
	jenkins = &devops.Jenkins{}
	secret = &corev1.Secret{}

	depItemList.GetInto(devops.TypePipelineConfig, pipelineConfig).
		GetInto(devops.TypeJenkinsBinding, jenkinsBinding).
		GetInto(devops.TypeJenkins, jenkins).
		GetInto(devops.TypeSecret, secret)
	return
}
