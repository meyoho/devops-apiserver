package rest_test

import (
	"context"
	"strings"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mdependency "alauda.io/devops-apiserver/pkg/mock/dependency"
	devopsrest "alauda.io/devops-apiserver/pkg/registry/devops/pipelineconfig/rest"
	"alauda.io/devops-apiserver/pkg/registry/generic"
	"github.com/golang/mock/gomock"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

func TestPreview(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	type TestCase struct {
		Name           string
		Template       *devops.ClusterPipelineTemplate
		TaskTemplate   *devops.ClusterPipelineTaskTemplate
		PipelineConfig func() (*devops.PipelineConfig, error)
		Prepare        func() (ctx context.Context, name string, opts runtime.Object)
		Run            func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error)
		Expect         string
		Err            error
	}
	var previewREST *generic.PreviewREST

	table := []TestCase{
		{
			Name: "case 0, all ok simplest jenkinsfile",
			Template: &devops.ClusterPipelineTemplate{
				Spec: devops.PipelineTemplateSpec{
					Stages: []devops.PipelineStage{
						{
							Name: "test",
							Tasks: []devops.PipelineTemplateTask{
								devops.PipelineTemplateTask{
									Name: "test",
									Type: "test",
									Kind: "ClusterPipelineTaskTemplate",
								},
							},
						},
					},
				},
			},
			TaskTemplate: &devops.ClusterPipelineTaskTemplate{
				ObjectMeta: metav1.ObjectMeta{Name: "test"},
				TypeMeta:   metav1.TypeMeta{Kind: "ClusterPipelineTaskTemplate"},
				Spec: devops.PipelineTaskTemplateSpec{
					Body: "echo '1'",
				},
			},
			PipelineConfig: func() (pipelineConfig *devops.PipelineConfig, err error) {

				pipelineConfig = &devops.PipelineConfig{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "test",
						Namespace: "test",
					},
					Spec: devops.PipelineConfigSpec{
						Template: devops.PipelineTemplateWithValue{
							PipelineTemplateSource: devops.PipelineTemplateSource{
								PipelineTemplateRef: devops.PipelineTemplateRef{
									Name: "test",
									Kind: devops.TypeClusterPipelineTemplate,
								},
							},
						},
					},
				}
				return
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{}
				return
			},
			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, nil)
			},
			Expect: "echo '1'",
			Err:    nil,
		},
		{
			Name: "case 1, with arguments",
			Template: &devops.ClusterPipelineTemplate{
				Spec: devops.PipelineTemplateSpec{
					Stages: []devops.PipelineStage{
						{
							Name: "test",
							Tasks: []devops.PipelineTemplateTask{
								devops.PipelineTemplateTask{
									Name: "test",
									Kind: "ClusterPipelineTaskTemplate",
									Type: "test",
								},
							},
						},
					},
					Arguments: []devops.PipelineTemplateArgumentGroup{
						{
							Items: []devops.PipelineTemplateArgumentValue{
								{
									PipelineTemplateArgument: devops.PipelineTemplateArgument{
										Name:    "Param",
										Binding: []string{"test.args.Param"},
										Schema: devops.PipelineTaskArgumentSchema{
											Type: "string",
										},
										Display: devops.PipelineTaskArgumentDisplay{
											Type: "string",
											Name: devops.I18nName{
												Zh: "param",
												En: "param",
											},
										},
									},
								},
							},
						},
					},
				},
			},
			TaskTemplate: &devops.ClusterPipelineTaskTemplate{
				TypeMeta: metav1.TypeMeta{
					Kind: "ClusterPipelineTaskTemplate",
				},
				ObjectMeta: metav1.ObjectMeta{Name: "test"},
				Spec: devops.PipelineTaskTemplateSpec{
					Body: "echo '{{.Param}}'",
					Arguments: []devops.PipelineTaskArgument{
						{
							Name: "Param",
							Schema: devops.PipelineTaskArgumentSchema{
								Type: "string",
							},
							Display: devops.PipelineTaskArgumentDisplay{
								Type: "string",
								Name: devops.I18nName{
									En: "param",
									Zh: "param",
								},
							},
						},
					},
				},
			},
			PipelineConfig: func() (pipelineConfig *devops.PipelineConfig, err error) {

				pipelineConfig = &devops.PipelineConfig{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "test",
						Namespace: "test",
					},
					Spec: devops.PipelineConfigSpec{
						Template: devops.PipelineTemplateWithValue{
							Values: map[string]string{
								"Param": "good",
							},
							PipelineTemplateSource: devops.PipelineTemplateSource{
								PipelineTemplateRef: devops.PipelineTemplateRef{
									Name: "test",
									Kind: devops.TypeClusterPipelineTemplate,
								},
							},
						},
					},
				}
				return
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{}
				return
			},
			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, nil)
			},
			Expect: "echo 'good'",
			Err:    nil,
		},
		{
			Name: "case 2, with environments",
			TaskTemplate: &devops.ClusterPipelineTaskTemplate{
				TypeMeta: metav1.TypeMeta{
					Kind: "ClusterPipelineTaskTemplate",
				},
				ObjectMeta: metav1.ObjectMeta{Name: "test"},
				Spec: devops.PipelineTaskTemplateSpec{
					Body: "echo '{{.Param}}'",
					Arguments: []devops.PipelineTaskArgument{
						{
							Name: "Param",
							Schema: devops.PipelineTaskArgumentSchema{
								Type: "string",
							},
							Display: devops.PipelineTaskArgumentDisplay{
								Type: "string",
								Name: devops.I18nName{
									En: "param",
									Zh: "param",
								},
							},
						},
					},
				},
			},
			Template: &devops.ClusterPipelineTemplate{
				Spec: devops.PipelineTemplateSpec{
					Stages: []devops.PipelineStage{
						{
							Name: "test",
							Tasks: []devops.PipelineTemplateTask{
								devops.PipelineTemplateTask{
									Name: "test",
									Kind: "ClusterPipelineTaskTemplate",
									Type: "test",
								},
							},
						},
					},
					Arguments: []devops.PipelineTemplateArgumentGroup{
						{
							Items: []devops.PipelineTemplateArgumentValue{
								{
									PipelineTemplateArgument: devops.PipelineTemplateArgument{
										Name:    "Param",
										Binding: []string{"test.args.Param"},
										Schema: devops.PipelineTaskArgumentSchema{
											Type: "string",
										},
										Display: devops.PipelineTaskArgumentDisplay{
											Type: "string",
											Name: devops.I18nName{
												Zh: "param",
												En: "param",
											},
										},
									},
								},
							},
						},
					},
					Environments: []devops.PipelineEnvironment{
						{
							Name:  "project",
							Value: "demo",
						},
					},
				},
			},
			PipelineConfig: func() (pipelineConfig *devops.PipelineConfig, err error) {

				pipelineConfig = &devops.PipelineConfig{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "test",
						Namespace: "test",
					},
					Spec: devops.PipelineConfigSpec{
						Template: devops.PipelineTemplateWithValue{
							Values: map[string]string{
								"Param": "good",
							},
							PipelineTemplateSource: devops.PipelineTemplateSource{
								PipelineTemplateRef: devops.PipelineTemplateRef{
									Name: "test",
									Kind: devops.TypeClusterPipelineTemplate,
								},
							},
						},
					},
				}
				return
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{}
				return
			},
			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, nil)
			},
			Expect: "environment",
			Err:    nil,
		},
	}

	for i, test := range table {
		ctx, name, opts := test.Prepare()
		mockDependency := mdependency.NewMockManager(ctrl)
		previewREST = devopsrest.NewPreviewREST(mockDependency)

		config, _ := test.PipelineConfig()
		mockDependency.EXPECT().
			Get(ctx, devops.TypePipelineConfig, name).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypePipelineConfig,
					Object:   config,
					IntoFunc: dependency.PipelineConfigCopyInto,
				},
			}).AnyTimes()
		mockDependency.EXPECT().
			Get(genericapirequest.NewContext(), devops.TypeClusterPipelineTemplate, name).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypeClusterPipelineTemplate,
					Object:   test.Template,
					IntoFunc: dependency.ClusterPipelineTemplateCopyInto,
				},
			})
		mockDependency.EXPECT().
			Get(genericapirequest.NewContext(), devops.TypeClusterPipelineTaskTemplate, name).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypeClusterPipelineTaskTemplate,
					Object:   test.TaskTemplate,
					IntoFunc: dependency.ClusterPipelineTaskTemplateCopyInto,
				},
			})

		result, err := test.Run(ctx, name, opts, func(obj runtime.Object) error { return nil }, false)

		if err == nil && test.Err != nil {
			t.Errorf("[%d] %s: Should fail but didnt: %v", i, test.Name, test.Err)
		} else if err != nil && test.Err == nil {
			t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
		}

		preview, ok := result.(*devops.JenkinsfilePreview)
		if !ok || !strings.Contains(preview.Jenkinsfile, test.Expect) {
			t.Errorf("[%d] %s invalid type, should be devops.JenkinsfilePreview, expect contains :%s, result is :%s",
				i, test.Name, test.Expect, preview.Jenkinsfile)
		}
	}
}
