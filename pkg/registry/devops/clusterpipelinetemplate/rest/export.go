package rest

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/registry/rest"
)

func NewExportRest(getter dependency.Manager) rest.Storage {

	name := devops.TypeClusterPipelineTemplate
	return generic.NewExportREST(name, getter, devops.TypeClusterPipelineTemplate)
}
