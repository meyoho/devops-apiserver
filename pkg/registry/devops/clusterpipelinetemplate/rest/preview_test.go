package rest_test

import (
	"context"
	"strings"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mdependency "alauda.io/devops-apiserver/pkg/mock/dependency"
	devopsrest "alauda.io/devops-apiserver/pkg/registry/devops/clusterpipelinetemplate/rest"
	"github.com/golang/mock/gomock"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

func TestPreview(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockDependency := mdependency.NewMockManager(ctrl)
	previewREST := devopsrest.NewPreviewREST(mockDependency)

	type TestCase struct {
		Name                        string
		ClusterPipelineTemplate     func() (*devops.ClusterPipelineTemplate, error)
		ClusterPipelineTaskTemplate func() (*devops.ClusterPipelineTaskTemplate, error)
		Prepare                     func() (ctx context.Context, name string, opts runtime.Object)
		Run                         func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error)
		Expect                      string
		Err                         error
	}

	table := []TestCase{
		{
			Name: "case 0: all ok simplest jenkinsfile",
			ClusterPipelineTemplate: func() (tempalte *devops.ClusterPipelineTemplate, err error) {
				tempalte = &devops.ClusterPipelineTemplate{
					Spec: devops.PipelineTemplateSpec{
						Stages: []devops.PipelineStage{
							{
								Name: "test",
								Tasks: []devops.PipelineTemplateTask{
									{
										Name: "test",
										Type: "test",
										Kind: "ClusterPipelineTaskTemplate",
									},
								},
							},
						},
					},
				}
				return
			},
			ClusterPipelineTaskTemplate: func() (taskTemplate *devops.ClusterPipelineTaskTemplate, err error) {
				taskTemplate = &devops.ClusterPipelineTaskTemplate{
					Spec: devops.PipelineTaskTemplateSpec{
						Body: "echo '1'",
					},
				}
				taskTemplate.Name = "test"
				return
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				// ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				ctx = genericapirequest.NewContext()
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{}
				return
			},
			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, nil)
			},
			Err:    nil,
			Expect: "echo '1'",
		},
	}
	for i, test := range table {
		ctx, name, opts := test.Prepare()

		options := metav1.GetOptions{ResourceVersion: "0"}
		options.Kind = "ClusterPipelineTemplate"

		clusterPipelineTemplate, _ := test.ClusterPipelineTemplate()
		mockDependency.EXPECT().Get(ctx, devops.TypeClusterPipelineTemplate, name).Return(dependency.ItemList{
			&dependency.Item{
				Kind:     devops.TypeClusterPipelineTemplate,
				Object:   clusterPipelineTemplate,
				IntoFunc: dependency.ClusterPipelineTemplateCopyInto,
			},
		})
		clusterTaskTemplate, _ := test.ClusterPipelineTaskTemplate()
		mockDependency.EXPECT().Get(ctx, devops.TypeClusterPipelineTaskTemplate, name).Return(dependency.ItemList{
			&dependency.Item{
				Kind:     devops.TypeClusterPipelineTaskTemplate,
				Object:   clusterTaskTemplate,
				IntoFunc: dependency.ClusterPipelineTaskTemplateCopyInto,
			},
		})

		result, err := test.Run(ctx, name, opts, func(obj runtime.Object) error { return nil }, false)

		if err == nil && test.Err != nil {
			t.Errorf("[%d] %s: Should fail but didnt: %v", i, test.Name, test.Err)
		} else if err != nil && test.Err == nil {
			t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
		}

		preview, ok := result.(*devops.JenkinsfilePreview)
		if ok {
			strings.Contains(preview.Jenkinsfile, test.Expect)
		} else {
			t.Errorf("[%d] %s invalid type, should be devops.JenkinsfilePreview", i, test.Name)
		}
	}
}
