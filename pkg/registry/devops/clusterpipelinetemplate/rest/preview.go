package rest

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/registry/generic"
)

const ClusterPipelineTemplatePreviewREST = "ClusterPipelineTemplatePreviewREST"

func NewPreviewREST(getter dependency.Manager) *generic.PreviewREST {
	return generic.NewPreviewREST(
		"ClusterPipelineTemplatePreviewREST",
		getter,
		getClusterPipelineTemplate,
	)
}

func getClusterPipelineTemplate(ctx context.Context, getter dependency.Manager, name string) (*devops.PipelineTemplateRef, error) {
	return &devops.PipelineTemplateRef{
		Kind:      devops.TypeClusterPipelineTemplate,
		Name:      name,
		Namespace: "",
	}, nil
}
