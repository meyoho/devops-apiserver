package imagerepository

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {
	columnDefinitions := []metav1alpha1.TableColumnDefinition{
		{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
		{Name: registry.RepositoryRepository, Type: registry.TableTypeString, Description: "Repository"},
		{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "The State of the imageRepository."},
		{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
		{Name: "Newest Tag", Type: registry.TableTypeString, Priority: 1, Description: "Newest Commit"},
		{Name: "ImageRegistry", Type: registry.TableTypeString, Priority: 1, Description: "ImageRegistry instance name"},
	}

	h.TableHandler(columnDefinitions, printImageRepositoryList)
	h.TableHandler(columnDefinitions, printImageRepository)

	registry.AddDefaultHandlers(h)

}

func printImageRepositoryList(imageRepositoryList *devops.ImageRepositoryList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(imageRepositoryList.Items))
	for i := range imageRepositoryList.Items {
		r, err := printImageRepository(&imageRepositoryList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

func printImageRepository(imageRepository *devops.ImageRepository, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: imageRepository},
	}

	row.Cells = append(row.Cells, imageRepository.Name, imageRepository.Spec.Image, imageRepository.Status.Phase, registry.TranslateTimestamp(imageRepository.CreationTimestamp))
	if options.Wide {
		// adding to rows
		tag := "-"
		if len(imageRepository.Status.Tags) > 0 {
			var newest *metav1.Time
			for _, t := range imageRepository.Status.Tags {
				if t.CreatedAt != nil && (newest == nil || newest.Before(t.CreatedAt)) {
					newest = t.CreatedAt
					tag = t.Name
				}
			}
		}
		row.Cells = append(row.Cells, tag, imageRepository.Spec.ImageRegistry.Name)
	}
	return []metav1alpha1.TableRow{row}, nil
}
