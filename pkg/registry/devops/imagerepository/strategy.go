package imagerepository

import (
	"context"
	"fmt"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsregistrygeneric "alauda.io/devops-apiserver/pkg/registry/generic"
)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	imageRepository, ok := obj.(*devops.ImageRepository)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a ImageRepository")
	}
	return labels.Set(imageRepository.ObjectMeta.Labels), toSelectableFields(imageRepository), imageRepository.Initializers != nil, nil
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.ImageRepository) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// MatchImageRepository is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchImageRepository(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// Strategy strategy for ImageRepository
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// PrepareForCreate adds basic fields for creation
func (Strategy) PrepareForCreate(ctx context.Context, obj runtime.Object) {
	imageRepository, ok := obj.(*devops.ImageRepository)
	if ok {
		imageRepository.Status.Phase = devops.ServiceStatusPhaseCreating
		imageRepository.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate currently does not do anything specific
func (Strategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) {
	var (
		newImageRepository *devops.ImageRepository
	)
	newImageRepository, _ = obj.(*devops.ImageRepository)
	newImageRepository.Status.LastUpdate = &metav1.Time{Time: time.Now()}
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
}

// Validate validate a request object
func (Strategy) Validate(ctx context.Context, obj runtime.Object) (errs field.ErrorList) {
	errs = field.ErrorList{}
	imageRepository, ok := obj.(*devops.ImageRepository)
	if ok {
		errs = append(errs, validateImageRepository(imageRepository)...)
	}
	return
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) (errs field.ErrorList) {
	errs = field.ErrorList{}
	imageRepository, ok := obj.(*devops.ImageRepository)
	if ok {
		errs = append(errs, validateImageRepository(imageRepository)...)
	}
	return
}

func validateImageRepository(imageRepository *devops.ImageRepository) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if imageRepository.Spec.ImageRegistry.Name == "" {
		errs = append(errs, field.Invalid(
			field.NewPath("spec").Child("imageregistry").Child("name"),
			imageRepository.Spec.ImageRegistry.Name,
			"please provide a imageregistry instance name",
		))
	}
	if imageRepository.Spec.Image == "" {
		errs = append(errs, field.Invalid(
			field.NewPath("spec").Child("image"),
			imageRepository.Spec.Image,
			"please provide a image repository",
		))
	}
	return
}

func GetRepository(getter devopsregistrygeneric.ResourceGetter, ctx context.Context, name string) (reposiotry *devops.ImageRepository, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	reposiotry = obj.(*devops.ImageRepository)
	if reposiotry == nil {
		err = fmt.Errorf("unexpect object type: %v", obj)
	}
	return
}
