package rest

import (
	"context"
	"fmt"
	"net/http"
	"sort"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
)

var _ = rest.GetterWithOptions(&ImageTagREST{})

type ImageTagREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

func NewImageTagREST(
	getter dependency.Manager,
	roundTripper http.RoundTripper,
) rest.Storage {
	return &ImageTagREST{
		Getter:       getter,
		RoundTripper: roundTripper,
	}
}

func (t *ImageTagREST) New() runtime.Object {
	return &devops.ImageTagOptions{}
}

func (t *ImageTagREST) Get(ctx context.Context, name string, options runtime.Object) (runtime.Object, error) {
	var (
		serviceType                    devops.ImageRegistryType
		endpoint, username, credential string
		err                            error
	)

	tagOpt, ok := options.(*devops.ImageTagOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", tagOpt)
	}
	dependencies := t.Getter.Get(ctx, devops.TypeImageRepository, name)
	if err = dependencies.Validate(); err != nil {
		return nil, err
	}

	repository := &devops.ImageRepository{}
	registry := &devops.ImageRegistry{}
	secret := &corev1.Secret{}
	dependencies.GetInto(devops.TypeImageRepository, repository).GetInto(devops.TypeImageRegistry, registry).GetInto(devops.TypeSecret, secret)

	serviceType = registry.Spec.Type
	endpoint = registry.GetEndpoint()

	if secret != nil && secret.Data != nil {
		username = strings.TrimSpace(string(secret.Data[corev1.BasicAuthUsernameKey]))
		credential = strings.TrimSpace(string(secret.Data[corev1.BasicAuthPasswordKey]))
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(endpoint),
		devopsclient.NewBasicAuth(username, credential),
		devopsclient.NewTransport(t.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		serviceType.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	tags, err := serviceClient.GetImageTags(ctx, repository.Spec.Image)
	if err != nil {
		return nil, fmt.Errorf("error get tags from repository: %s, err: %v", repository.Spec.Image, err)
	}
	sort.Slice(tags, func(i, j int) bool {
		// default desc
		mode := tagOpt.SortMode != devops.ParameterSortModeAscending
		// String sorting rules:
		// a < b True
		// a < A False
		// a < aa True
		if tags[i].CreatedAt == nil || tags[j].CreatedAt == nil {
			return (tags[i].Name > tags[j].Name) == mode
		}
		return (tags[i].CreatedAt.Time.After(tags[j].CreatedAt.Time) || (tags[i].CreatedAt.Time.Equal(tags[j].CreatedAt.Time) && tags[i].Name > tags[j].Name)) == mode
	})
	return &devops.ImageTagResult{Tags: tags}, nil
}

func (t *ImageTagREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ImageTagOptions{}, false, ""
}
