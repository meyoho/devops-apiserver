package rest

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"

	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/runtime"

	"alauda.io/devops-apiserver/pkg/dependency"
	"k8s.io/apiserver/pkg/registry/rest"
)

type AvailableREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

func NewAvailableREST(getter dependency.Manager, roundTripper http.RoundTripper) rest.Storage {
	return &AvailableREST{
		Getter:       getter,
		RoundTripper: roundTripper,
	}
}

var _ = rest.GetterWithOptions(&AvailableREST{})

func (r *AvailableREST) New() runtime.Object {
	return &devops.ImageRepositoryOptions{}
}

func (r *AvailableREST) Get(ctx context.Context, name string, options runtime.Object) (runtime.Object, error) {
	opts, ok := options.(*devops.ImageRepositoryOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opts))
	}

	dependencies := r.Getter.Get(ctx, devops.TypeImageRepository, name)
	if err := dependencies.Validate(); err != nil {
		return nil, err
	}

	repository := &devops.ImageRepository{}
	registry := &devops.ImageRegistry{}
	secret := &corev1.Secret{}
	dependencies.GetInto(devops.TypeImageRepository, repository).GetInto(devops.TypeImageRegistry, registry).GetInto(devops.TypeSecret, secret)

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(registry.GetEndpoint()),
		devopsclient.NewBasicAuth(k8s.GetDataBasicAuthFromSecret(secret).Username, k8s.GetDataBasicAuthFromSecret(secret).Password),
		devopsclient.NewTransport(r.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		registry.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get registry %s client: %v", registry.GetEndpoint(), err))
	}

	status, err := serviceClient.RepositoryAvailable(ctx, opts.Image, getLastAttempt(repository.Status.HTTPStatus))
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error check RepositoryAvailable %s: %v", opts.Image, err))
	}

	return &devops.ImageRepositoryRemoteStatus{Status: status}, nil
}

func getLastAttempt(status *devops.HostPortStatus) time.Time {
	if status == nil || status.LastAttempt == nil {
		return time.Time{}
	}
	return status.LastAttempt.Time
}

func (r *AvailableREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ImageRepositoryOptions{}, false, ""
}
