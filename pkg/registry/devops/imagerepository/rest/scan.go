package rest

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	corev1 "k8s.io/api/core/v1"
)

type ImageScanREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

func NewImageScanREST(
	getter dependency.Manager,
	roundTripper http.RoundTripper,
) rest.Storage {
	return &ImageScanREST{
		Getter:       getter,
		RoundTripper: roundTripper,
	}
}

func (t *ImageScanREST) New() runtime.Object {
	return &devops.ImageScanOptions{}
}

var _ = rest.NamedCreater(&ImageScanREST{})

func (t *ImageScanREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (runtime.Object, error) {
	var (
		serviceType                    devops.ImageRegistryType
		endpoint, username, credential string
		err                            error
	)
	scanOpt, ok := obj.(*devops.ImageScanOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", scanOpt)
	}

	dependencies := t.Getter.Get(ctx, devops.TypeImageRepository, name)
	if err = dependencies.Validate(); err != nil {
		return nil, err
	}

	repository := &devops.ImageRepository{}
	registry := &devops.ImageRegistry{}
	secret := &corev1.Secret{}
	dependencies.GetInto(devops.TypeImageRepository, repository).GetInto(devops.TypeImageRegistry, registry).GetInto(devops.TypeSecret, secret)

	serviceType = registry.Spec.Type
	endpoint = registry.GetEndpoint()

	if secret != nil && secret.Data != nil {
		username = strings.TrimSpace(string(secret.Data[corev1.BasicAuthUsernameKey]))
		credential = strings.TrimSpace(string(secret.Data[corev1.BasicAuthPasswordKey]))
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(endpoint),
		devopsclient.NewBasicAuth(username, credential),
		devopsclient.NewTransport(t.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		serviceType.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	result := serviceClient.TriggerScanImage(ctx, repository.Spec.Image, scanOpt.Tag)
	for index, tag := range repository.Status.Tags {
		if tag.Name == scanOpt.Tag {
			if result.StatusCode == 200 {
				repository.Status.Tags[index].ScanStatus = devops.ImageTagScanStatusAnalyzing
			} else {
				repository.Status.Tags[index].ScanStatus = devops.ImageTagScanStatusError
			}
		}
	}
	return result, nil
}

var _ = rest.GetterWithOptions(&ImageScanREST{})

func (t *ImageScanREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		serviceType                    devops.ImageRegistryType
		endpoint, username, credential string
		err                            error
	)
	scanOpt, ok := opts.(*devops.ImageScanOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", scanOpt)
	}

	dependencies := t.Getter.Get(ctx, devops.TypeImageRepository, name)
	if err = dependencies.Validate(); err != nil {
		return nil, err
	}
	repository := &devops.ImageRepository{}
	registry := &devops.ImageRegistry{}
	secret := &corev1.Secret{}
	dependencies.GetInto(devops.TypeImageRepository, repository).GetInto(devops.TypeImageRegistry, registry).GetInto(devops.TypeSecret, secret)

	serviceType = registry.Spec.Type
	endpoint = registry.GetEndpoint()
	if secret != nil && secret.Data != nil {
		username = strings.TrimSpace(string(secret.Data[corev1.BasicAuthUsernameKey]))
		credential = strings.TrimSpace(string(secret.Data[corev1.BasicAuthPasswordKey]))
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(endpoint),
		devopsclient.NewBasicAuth(username, credential),
		devopsclient.NewTransport(t.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		serviceType.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}
	result, err := serviceClient.GetVulnerability(ctx, repository.Spec.Image, scanOpt.Tag)
	if err != nil {
		return nil, fmt.Errorf("error get repository: %s, tag: %s vulnerability error: %v", repository.Spec.Image, scanOpt.Tag, err)
	}
	return result, nil
}

func (t *ImageScanREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ImageScanOptions{}, false, ""
}
