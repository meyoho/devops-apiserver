package imagerepository

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestImageRepositoryPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Minute))
	old := metav1.NewTime(time.Now().Add(-time.Hour))
	imageRepository := &devops.ImageRepository{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "image-repo-1",
			CreationTimestamp: now,
		},
		Spec: devops.ImageRepositorySpec{
			Image: "index.alauda.cn/alaudaorg/repo",
			ImageRegistry: devops.LocalObjectReference{
				Name: "imageRegistry",
			},
		},
		Status: devops.ImageRepositoryStatus{
			ServiceStatus: devops.ServiceStatus{
				Phase: devops.ServiceStatusPhaseError,
			},
			Tags: []devops.ImageTag{
				devops.ImageTag{
					Name:      "new",
					CreatedAt: &now,
				},
				devops.ImageTag{
					Name:      "old",
					CreatedAt: &old,
				},
			},
		},
	}
	list := &devops.ImageRepositoryList{
		Items: []devops.ImageRepository{*imageRepository},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: imageRepository},
			Cells: []interface{}{
				imageRepository.Name, imageRepository.Spec.Image, imageRepository.Status.Phase, registry.TranslateTimestamp(now),
				imageRepository.Status.Tags[0].Name, imageRepository.Spec.ImageRegistry.Name,
			},
		},
	}

	result, err := printImageRepositoryList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing imageRepository: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
