package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	"alauda.io/devops-apiserver/pkg/registry/devops/projectmanagement"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// RepositoryREST implements the log endpoint for a Pod
type ProjectREST struct {
	projectManagementStore        *devopsregistry.REST
	projectManagementBindingStore *devopsregistry.REST
	SecretLister                  corev1listers.SecretLister
	ConfigMapLister               corev1listers.ConfigMapLister
	Transport                     http.RoundTripper
}

// ProjectREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&ProjectREST{})

// New creates a new Pod log options object
func (r *ProjectREST) New() runtime.Object {
	return &devops.CreateProjectOptions{}
}

// NewProjectREST starts a new project store
func NewProjectREST(
	projectManagementStore rest.StandardStorage,
	projectManagementBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	configMapLister corev1listers.ConfigMapLister,
	transport http.RoundTripper,

) rest.Storage {
	api := &ProjectREST{
		projectManagementStore:        projectManagementStore.(*devopsregistry.REST),
		projectManagementBindingStore: projectManagementBindingStore.(*devopsregistry.REST),
		SecretLister:                  secretLister,
		ConfigMapLister:               configMapLister,
		Transport:                     transport,
	}
	return api
}

// NewGetOptions creates a new options object
func (r *ProjectREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ListProjectOptions{}, false, ""
}

// Get retrieves a runtime.Object that will stream the contents of projectmanagement
func (r *ProjectREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		projectsOptions = &devops.ListProjectOptions{}
		err             error
		service         *devops.ProjectManagement
		secret          *corev1.Secret
	)
	projectsOptions, ok := opts.(*devops.ListProjectOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}

	service, err = projectmanagement.GetProjectManagement(r.projectManagementStore, ctx, name)
	if err != nil {
		return nil, err
	}
	if projectsOptions.Namespace != "" && projectsOptions.SecretName != "" {
		secret, err = r.SecretLister.Secrets(projectsOptions.Namespace).Get(projectsOptions.SecretName)
	} else {
		secret, err = r.SecretLister.Secrets(service.Spec.Secret.Namespace).Get(service.Spec.Secret.Name)
	}
	if err != nil {
		return nil, err
	}
	data := k8s.GetDataBasicAuthFromSecret(secret)
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetEndpoint()),
		devopsclient.NewBasicAuth(data.Username, data.Password),
		devopsclient.NewTransport(r.Transport),
	)
	serviceClient, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	Projects, err := serviceClient.GetProjects(ctx, projectsOptions.Page, projectsOptions.PageSize)
	if err != nil {
		return nil, fmt.Errorf("err happend when get porject, err is %v", err)
	}
	return Projects, nil
}

var _ = rest.NamedCreater(&ProjectREST{})

func (r *ProjectREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (runtime.Object, error) {

	var (
		err                  error
		service              *devops.ProjectManagement
		projectCreateOptions = &devops.CreateProjectOptions{}
		secret               *corev1.Secret
	)

	projectCreateOptions, ok := obj.(*devops.CreateProjectOptions)

	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", projectCreateOptions)
	}
	projectName := projectCreateOptions.Name
	projectDescription := projectCreateOptions.Data["description"]
	projectKey := projectCreateOptions.Data["key"]
	projectLead := projectCreateOptions.Data["lead"]
	service, err = projectmanagement.GetProjectManagement(r.projectManagementStore, ctx, name)
	if err != nil {
		return nil, err
	}
	if projectCreateOptions.Namespace != "" && projectCreateOptions.SecretName != "" {
		secret, err = r.SecretLister.Secrets(projectCreateOptions.Namespace).Get(projectCreateOptions.SecretName)
	} else {
		secret, err = r.SecretLister.Secrets(service.Spec.Secret.Namespace).Get(service.Spec.Secret.Name)
	}
	if err != nil {
		return nil, err
	}
	data := k8s.GetDataBasicAuthFromSecret(secret)
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetEndpoint()),
		devopsclient.NewBasicAuth(data.Username, data.Password),
		devopsclient.NewTransport(r.Transport),
	)
	serviceClient, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}
	if projectLead == "" {
		projectLead = data.Username
	}
	Project, err := serviceClient.CreateProject(ctx, projectName, projectDescription, projectLead, projectKey)
	if err != nil {
		return nil, fmt.Errorf(" project named %s  is %v", projectName, err)
	}
	return Project, nil
}
