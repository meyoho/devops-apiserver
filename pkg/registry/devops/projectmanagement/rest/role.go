package rest

import (
	"context"
	"net/http"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	internalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/registry/generic"
	corev1 "k8s.io/api/core/v1"
)

type ProjectManagementRolesOperation struct {
	generic.GetRolesMappingFunc
	generic.ApplyRolesMappingFunc
	Factory internalthirdparty.ProjectManagementClientFactory
}

func NewProjectManagementRolesOperation(transport http.RoundTripper) ProjectManagementRolesOperation {
	factory := internalthirdparty.NewProjectManagementClientFactory()
	factory.SetTransport(transport)
	projectmanagementroleoperation := ProjectManagementRolesOperation{}
	projectmanagementroleoperation.Factory = factory
	projectmanagementroleoperation.ApplyRolesMappingFunc = projectmanagementroleoperation.ProjectManagementApplyRolesMappingFunc
	projectmanagementroleoperation.GetRolesMappingFunc = projectmanagementroleoperation.ProjectManagementGetRolesMappingFunc
	return projectmanagementroleoperation
}
func (r *ProjectManagementRolesOperation) getProjectManagementClient(getter dependency.Manager, context context.Context, name string) (client internalthirdparty.ProjectmanagementClientInterface, err error) {
	var username, apiToken string

	depItemList := getter.Get(context, devops.TypeProjectManagement, name)
	if err := depItemList.Validate(); err != nil {
		return nil, err
	}

	projetmanagement := &devops.ProjectManagement{}
	secret := &corev1.Secret{}
	depItemList.GetInto(devops.TypeProjectManagement, projetmanagement).GetInto(devops.TypeSecret, secret)

	if secret != nil && secret.Data != nil {
		username = strings.TrimSpace(string(secret.Data[corev1.BasicAuthUsernameKey]))
		apiToken = strings.TrimSpace(string(secret.Data[corev1.BasicAuthPasswordKey]))
	}

	pmEndppoint := projetmanagement.Spec.HTTP.Host
	client = r.Factory.GetProjectmanageClient(projetmanagement.Spec.Type, pmEndppoint, username, apiToken, 30)
	return
}

func (r *ProjectManagementRolesOperation) ProjectManagementGetRolesMappingFunc(getter dependency.Manager, context context.Context, name string, rolemappinglist *devops.RoleMappingListOptions) (*devops.RoleMapping, error) {
	serviceClient, err := r.getProjectManagementClient(getter, context, name)
	if err != nil {
		return nil, err
	}
	projects, err := serviceClient.GetProjectList(rolemappinglist)
	if err != nil {
		return nil, err
	}
	permissionNameIDmap, err := serviceClient.GetPermissionNameIdMap()
	if err != nil {
		return nil, err
	}
	rolemapping, err := serviceClient.GetRoleMappping(projects, permissionNameIDmap)
	if err != nil {
		return nil, err
	}
	return rolemapping, nil
}

func (r *ProjectManagementRolesOperation) ProjectManagementApplyRolesMappingFunc(getter dependency.Manager, context context.Context, name string, rolemapping *devops.RoleMapping) (rolemappinglist *devops.RoleMapping, err error) {
	serviceClient, err := r.getProjectManagementClient(getter, context, name)
	if err != nil {
		return nil, err
	}

	permissionNameIDmap, err := serviceClient.GetPermissionNameIdMap()
	if err != nil {
		return nil, err
	}
	projects, err := serviceClient.GetProjectList(nil)
	if err != nil {
		return nil, err
	}
	operations := &rolemapping.Spec
	projectIdNameMap := make(map[string]string)
	for _, project := range projects {
		projectIdNameMap[project.Name] = project.ID
	}
	for _, operation := range *operations {
		//range for different project
		for _, userandoperation := range operation.UserRoleOperations {
			//range for different user and operation in one project
			switch userandoperation.Operation {
			case "add":
				{
					serviceClient.AddUserforRole(userandoperation, projectIdNameMap, operation, permissionNameIDmap)
				}
			case "remove":
				{
					serviceClient.RemoveUserforRole(userandoperation, projectIdNameMap, operation, permissionNameIDmap)
				}
			}
		}
	}
	rolemappinglist = &devops.RoleMapping{}
	rolemappinglist.Spec = *operations
	return rolemappinglist, err
}
