package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	"alauda.io/devops-apiserver/pkg/registry/devops/projectmanagement"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// UserREST implements the log endpoint for a Pod
type UserREST struct {
	projectManagementStore        *devopsregistry.REST
	projectManagementBindingStore *devopsregistry.REST
	SecretLister                  corev1listers.SecretLister
	ConfigMapLister               corev1listers.ConfigMapLister
	Transport                     http.RoundTripper
}

// UserREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&UserREST{})

// New creates a new Pod log options object
func (r *UserREST) New() runtime.Object {
	return &devops.UserSearchOptions{}
}

// NewUserREST starts a new project store
func NewUserREST(
	projectManagementStore rest.StandardStorage,
	projectManagementBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	configMapLister corev1listers.ConfigMapLister,
	transport http.RoundTripper,

) rest.Storage {
	api := &UserREST{
		projectManagementStore:        projectManagementStore.(*devopsregistry.REST),
		projectManagementBindingStore: projectManagementBindingStore.(*devopsregistry.REST),
		SecretLister:                  secretLister,
		ConfigMapLister:               configMapLister,
		Transport:                     transport,
	}
	return api
}

// NewGetOptions creates a new options object
func (r *UserREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.UserSearchOptions{}, false, ""
}

func (r *UserREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		err         error
		service     *devops.ProjectManagement
		userOptions = &devops.UserSearchOptions{}
		secret      *corev1.Secret
	)
	userOptions, ok := opts.(*devops.UserSearchOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}
	service, err = projectmanagement.GetProjectManagement(r.projectManagementStore, ctx, name)
	if err != nil {
		return nil, fmt.Errorf("projectmanagement service %s can't found", name)
	}
	secret, err = r.SecretLister.Secrets(userOptions.NameSpace).Get(userOptions.SecretName)
	if err != nil {
		return nil, err
	}
	data := k8s.GetDataBasicAuthFromSecret(secret)
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetEndpoint()),
		devopsclient.NewBasicAuth(data.Username, data.Password),
		devopsclient.NewTransport(r.Transport),
	)
	serviceClient, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	Users, err := serviceClient.SearchForUsers(ctx, userOptions.Name)
	if err != nil {
		return nil, fmt.Errorf("error happend when get Users for usersearch, and err is %v", err)
	}
	return Users, nil
}
