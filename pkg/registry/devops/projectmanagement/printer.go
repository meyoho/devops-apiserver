package projectmanagement

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// ColumnDefinitions column definitions for PipelineConfig
var columnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
	{Name: registry.ResourceType, Type: registry.TableTypeString, Description: "Type of ProjectManagement"},
	{Name: registry.ResourceHost, Type: registry.TableTypeString, Description: "Host address of the ProjectManagement instance"},
	{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "Status of the connectivity to the ProjectManagement instance"},
	{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
}

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {

	h.TableHandler(columnDefinitions, printProjectManagementList)
	h.TableHandler(columnDefinitions, printerProjectManagement)

	registry.AddDefaultHandlers(h)

}

// printProjectManagementList prints a ProjectManagement list
func printProjectManagementList(ProjectManagementList *devops.ProjectManagementList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(ProjectManagementList.Items))
	for i := range ProjectManagementList.Items {
		r, err := printerProjectManagement(&ProjectManagementList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

// printerProjectManagement prints a ProjectManagement
func printerProjectManagement(projectManagement *devops.ProjectManagement, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: projectManagement},
	}

	// basic rows
	row.Cells = append(row.Cells, projectManagement.Name, projectManagement.Spec.Type, projectManagement.Spec.HTTP.Host, projectManagement.Status.Phase, registry.TranslateTimestamp(projectManagement.CreationTimestamp))

	if options.Wide {
		// for later use
	}

	return []metav1alpha1.TableRow{row}, nil
}
