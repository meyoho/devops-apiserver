package setting_test

import (
	"alauda.io/devops-apiserver/pkg/registry/devops/setting"
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

type Table struct {
	name     string
	new      *devopsv1alpha1.Setting
	old      *devopsv1alpha1.Setting
	expected *devopsv1alpha1.Setting
	method   func(*Table)
}

func TestStrategy(t *testing.T) {
	scheme := runtime.NewScheme()
	strategy := setting.NewStrategy(scheme)

	if strategy.NamespaceScoped() {
		t.Errorf("projectmanageservice strategy should not be namespace scoped")
	}
	if strategy.AllowCreateOnUpdate() {
		t.Errorf("projectmanageservice strategy should not allow create on update")
	}
	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("projectmanageservice strategy should not allow unconditional update")
	}

	table := []Table{
		{
			name: "create: invalid input",
			new: &devopsv1alpha1.Setting{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.SettingSpec{},
				Status: devopsv1alpha1.ToolCategoryStatus{
					Phase: "Ready",
				},
			},
			expected: &devopsv1alpha1.Setting{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.SettingSpec{},
				Status: devopsv1alpha1.ToolCategoryStatus{
					Phase: "Ready",
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) == 0 {

				}
				validGetAttrs(t, tst)
			},
		},
		{
			name: "create: valid input",
			new: &devopsv1alpha1.Setting{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.SettingSpec{
					DefaultDomain: "",
					VersionGate:   "ga",
				},
				Status: devopsv1alpha1.ToolCategoryStatus{},
			},
			expected: &devopsv1alpha1.Setting{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.SettingSpec{
					DefaultDomain: "",
					VersionGate:   "ga",
				},
				Status: devopsv1alpha1.ToolCategoryStatus{
					Phase: "Ready",
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) != 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input should not generate error: %v",
						tst.name, err,
					)
				}
				validGetAttrs(t, tst)
			},
		},
	}

	for i, tst := range table {
		tst.method(&tst)

		if tst.new.Status.Phase != tst.expected.Status.Phase {
			t.Errorf(
				"Test %d: \"%v\" - status.phase are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
	}
}

func validGetAttrs(t *testing.T, tst *Table) {
	_, _, _, getErr := setting.GetAttrs(tst.new)
	if getErr != nil {
		t.Errorf(
			"Test: \"%s\" - GetAttr return err for object: %v",
			tst.name, getErr,
		)
	}
}
