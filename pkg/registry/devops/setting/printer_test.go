package setting

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestSettingPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	tc := &devops.Setting{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "setting-1",
			CreationTimestamp: now,
		},
		Spec: devops.SettingSpec{
			DefaultDomain: "",
			VersionGate:   "",
		},
		Status: devops.ToolCategoryStatus{
			Phase: "Ready",
		},
	}
	list := &devops.SettingList{
		Items: []devops.Setting{*tc},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: tc},
			Cells: []interface{}{
				tc.Name, tc.Spec.DefaultDomain, tc.Spec.VersionGate, tc.Status.Phase, registry.TranslateTimestamp(tc.CreationTimestamp),
			},
		},
	}

	result, err := printSettingList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing registry: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
