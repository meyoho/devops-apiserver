package setting_test

import (
	"alauda.io/devops-apiserver/pkg/registry/devops/setting"
	"reflect"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apiserver"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	genericregistry "k8s.io/apiserver/pkg/registry/generic"
)

const defaultEtcdPathPrefix = "/registry/devops.alauda.io"

func TestNewRest(t *testing.T) {
	registry, _, err := setting.NewREST(apiserver.Scheme, genericregistry.RESTOptions{}, devopsgeneric.NewFakeStore)
	if err != nil {
		t.Errorf("Should not error while creating: %v", err)
	}
	expected := []string{"integrations", "toolchain", "devops"}
	if reflect.DeepEqual(expected, registry.Categories()) {
		t.Errorf("Categories are different: %v != %v", expected, registry.Categories())
	}
	expected = []string{"setting"}
	if !reflect.DeepEqual(expected, registry.ShortNames()) {
		t.Errorf("Short names are different: %v != %v", expected, registry.ShortNames())
	}
	obj := registry.NewFunc()
	if toolc, ok := obj.(*devops.Setting); toolc == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.Setting", reflect.TypeOf(obj))
	}
	objList := registry.NewListFunc()
	if cqt, ok := objList.(*devops.SettingList); cqt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.SettingList", reflect.TypeOf(objList))
	}

	_, _, err = setting.NewREST(apiserver.Scheme, genericregistry.RESTOptions{}, devopsgeneric.NewStandardStore)
	if err == nil {
		t.Errorf("Empty rest options did not return error")
	}
}
