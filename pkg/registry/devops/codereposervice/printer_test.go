package codereposervice

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestCodeRepoServicePrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	codeRepoService := &devops.CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "codeRepoService-1",
			CreationTimestamp: now,
		},
		Spec: devops.CodeRepoServiceSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "http://codeRepoService.io",
				},
			},
			Type: devops.CodeRepoServiceTypeGithub,
		},
		Status: devops.ServiceStatus{
			Phase: devops.ServiceStatusPhaseReady,
		},
	}
	list := &devops.CodeRepoServiceList{
		Items: []devops.CodeRepoService{*codeRepoService},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: codeRepoService},
			Cells: []interface{}{
				"codeRepoService-1", codeRepoService.Spec.Type, codeRepoService.Spec.HTTP.Host, codeRepoService.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printCodeRepoServiceList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing codeRepoService: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
