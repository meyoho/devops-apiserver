/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package codereposervice

import (
	"context"
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"
	glog "k8s.io/klog"

	"alauda.io/devops-apiserver/pkg/apis/devops"
)

// NewStrategy creates a new Strategy instance for codereposervice
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// Strategy codeRepoService strategy
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns false
func (Strategy) NamespaceScoped() bool {
	return false
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// PrepareForCreate prepares an object for create request
func (Strategy) PrepareForCreate(ctx context.Context, new runtime.Object) {
	newObj, ok := new.(*devops.CodeRepoService)
	if ok {
		newObj.Status.Phase = devops.ServiceStatusPhaseCreating
		newObj.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate prepares an object for update request
func (Strategy) PrepareForUpdate(ctx context.Context, new, old runtime.Object) {
	var (
		newObj, oldObj *devops.CodeRepoService
	)
	newObj, _ = new.(*devops.CodeRepoService)
	if old != nil {
		oldObj, _ = old.(*devops.CodeRepoService)
	}

	if newObj != nil && newObj.Status.Phase.IsInvalid() {
		newObj.Status.Phase = devops.StatusCreating
	}

	if isChanged(oldObj, newObj) {
		glog.Infof("codeRepoService %s was changed", newObj.GetName())
		newObj.Status.HTTPStatus = nil
		newObj.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	}
}

// Validate validates a create object, and is invoked after PrepareForCreate before the object is persisted
func (Strategy) Validate(ctx context.Context, obj runtime.Object) (errs field.ErrorList) {
	codeRepoService, ok := obj.(*devops.CodeRepoService)
	if ok {
		return validation.ValidateCodeRepoService(codeRepoService)
	}
	return
}

// ValidateUpdate validates an update object, and is invoked after PrepareForUpdate before the object is persisted
func (Strategy) ValidateUpdate(ctx context.Context, new, old runtime.Object) (errs field.ErrorList) {
	newObj, ok := new.(*devops.CodeRepoService)
	oldObj, ok1 := old.(*devops.CodeRepoService)
	if ok && ok1 {
		errs = validation.ValidateCodeRepoService(newObj)
		return append(errs, validation.ValidateCodeRepoServiceUpdate(newObj, oldObj)...)
	}
	return
}

// Canonicalize allows an object to be mutated into a canonical form, and is invoked after validation has succeeded but before the object has been persisted.
func (Strategy) Canonicalize(obj runtime.Object) {
}

func isChanged(old, new *devops.CodeRepoService) bool {
	if old == nil || new == nil {
		return true
	}
	return !apiequality.Semantic.DeepEqual(old.Spec, new.Spec)
}

// MatchCodeRepoService is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchCodeRepoService(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	codeRepoService, ok := obj.(*devops.CodeRepoService)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a CodeRepoService")
	}
	return labels.Set(codeRepoService.ObjectMeta.Labels), toSelectableFields(codeRepoService), codeRepoService.Initializers != nil, nil
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.CodeRepoService) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, false)
}

type codereposerviceStatusStrategy struct {
	Strategy
}

// NewStatusStrategy creates a new StatusStragtegy for CodeRepoService
func NewStatusStrategy(strategy Strategy) codereposerviceStatusStrategy {
	return codereposerviceStatusStrategy{
		Strategy: strategy,
	}
}

func (codereposerviceStatusStrategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) {
	newObj := obj.(*devops.CodeRepoService)
	oldObj := old.(*devops.CodeRepoService)

	// update is not allowed to set spec
	newObj.Spec = oldObj.Spec
}

func (codereposerviceStatusStrategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) (errs field.ErrorList) {

	return
}
