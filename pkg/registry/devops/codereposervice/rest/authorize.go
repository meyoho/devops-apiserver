package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"

	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	bindingregistry "alauda.io/devops-apiserver/pkg/registry/devops/coderepobinding"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	glog "k8s.io/klog"

	"k8s.io/apiserver/pkg/registry/rest"
	"k8s.io/client-go/kubernetes"

	corev1 "k8s.io/api/core/v1"
	corev1listers "k8s.io/client-go/listers/core/v1"

	"k8s.io/apimachinery/pkg/runtime/schema"
)

// SecretREST implements the secret operation for a service
type SecretREST struct {
	// CodeRepoServiceStore *devopsregistry.REST
	// CodeRepoBindingStore *devopsregistry.REST
	CodeRepoServiceStore rest.StandardStorage
	CodeRepoBindingStore rest.StandardStorage
	SecretLister         corev1listers.SecretLister
	ConfigMapLister      corev1listers.ConfigMapLister
	KubernetesClent      kubernetes.Interface
	RoundTripper         http.RoundTripper
	Provider             devops.AnnotationProvider
}

// NewSecretREST starts a new secret store
func NewSecretREST(
	codeRepoServiceStore rest.StandardStorage,
	codeRepoBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	configMapLister corev1listers.ConfigMapLister,
	kubernetesClient kubernetes.Interface,
	roundTripper http.RoundTripper,
	provider devops.AnnotationProvider,
) rest.Storage {
	return &SecretREST{
		// CodeRepoServiceStore: codeRepoServiceStore.(*devopsregistry.REST),
		// CodeRepoBindingStore: codeRepoBindingStore.(*devopsregistry.REST),
		CodeRepoServiceStore: codeRepoServiceStore,
		CodeRepoBindingStore: codeRepoBindingStore,
		SecretLister:         secretLister,
		ConfigMapLister:      configMapLister,
		KubernetesClent:      kubernetesClient,
		RoundTripper:         roundTripper,
		Provider:             provider,
	}
}

// SecretREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&SecretREST{})
var _ = rest.NamedCreater(&SecretREST{})

// New creates a new secret options object
func (r *SecretREST) New() runtime.Object {
	return &devops.CodeRepoServiceAuthorizeCreate{}
}

// ProducesObject SecretREST implements StorageMetadata, return CodeRepoServiceAuthorizeResponse as the generating object
func (r *SecretREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.CodeRepoServiceAuthorizeResponse{}
}

// Get retrieves a runtime.Object that will stream the contents of codeRepoService
func (r *SecretREST) Get(ctx context.Context, name string, opts runtime.Object) (result runtime.Object, err error) {
	var (
		validateOptions  = &devops.CodeRepoServiceAuthorizeOptions{}
		validateResponse = &devops.CodeRepoServiceAuthorizeResponse{}
		secret           *corev1.Secret
		service          *devops.CodeRepoService
		update           bool
	)

	validateOptions, ok := opts.(*devops.CodeRepoServiceAuthorizeOptions)
	if !ok {
		return nil, errors.NewBadRequest("Options is of invalid type")
	}
	service, secret, err = r.getSecretAndCodeRepoService(ctx, name, validateOptions.SecretName, validateOptions.Namespace)
	if err != nil {
		return nil, err
	}

	defer func() {
		if update && secret != nil && err == nil {
			if _, updateSecretErr := r.KubernetesClent.CoreV1().Secrets(secret.GetNamespace()).Update(secret); updateSecretErr != nil {
				glog.Errorf("Error updating Secret \"%s/%s\". err: %v", secret.GetNamespace(), secret.GetName(), err)
			}
		}
	}()

	// secret is not allowed to be used by two service
	if secret.Type == devops.SecretTypeOAuth2 {
		glog.V(3).Infof("check whether secret %s is used by another service", secret.GetName())
		serviceName, refered := r.IsSecretUsedByAnotherService(secret, name)
		if refered && len(secret.GetOwnerReferences()) > 1 {
			return nil, errors.NewBadRequest(fmt.Sprintf("secret has been used by service '%s', please select another secret", serviceName))
		}
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetHost()),
		devopsclient.NewGenericOpts(service, secret),
		devopsclient.NewTransport(r.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	_, err = serviceClient.Authenticate(ctx)
	if err != nil {
		// return authorize url if secret's type is oauth2
		if secret.Type == devops.SecretTypeOAuth2 {

			if validateOptions.RedirectURL != "" {
				secret = secret.DeepCopy()
				secret.Data[devops.OAuth2RedirectURLKey] = []byte(validateOptions.RedirectURL)
				update = true
			}

			redirectURL := k8s.GetOAuthRedirectURL(r.ConfigMapLister, secret, r.Provider)
			authorizeURL := serviceClient.GetAuthorizeUrl(ctx, redirectURL)
			glog.V(3).Infof("authorizeURL is %s", authorizeURL)
			validateResponse.AuthorizeUrl = authorizeURL
			validateResponse.RedirectUrl = redirectURL
			err = nil
		} else {
			glog.Errorf("Error using secret %s authorize, error is %s", secret.GetName(), err)
			err = errors.NewBadRequest(fmt.Sprintf("username or password in secret '%s' is incorrect", secret.GetName()))
		}
	}

	return validateResponse, err
}

// Create utilizes provided authorization code to request for access token and refresh token
// to the designated secret. Used to satisfy the oAuth2 authentication flow
func (r *SecretREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (response runtime.Object, err error) {
	var (
		payload       = &devops.CodeRepoServiceAuthorizeCreate{}
		typedResponse = &devops.CodeRepoServiceAuthorizeResponse{}
		secret        *corev1.Secret
		service       *devops.CodeRepoService
		update        bool
		// status           *devops.HostPortStatus
	)
	payload, ok := obj.(*devops.CodeRepoServiceAuthorizeCreate)
	if !ok {
		err = errors.NewBadRequest("Payload is of invalid type")
		return
	}
	glog.V(5).Infof("POST Authorize request. name %v, payload: secret %s/%s, code: %s, get access token: %v", name, payload.Namespace, payload.SecretName, payload.Code, payload.GetAccessToken)
	defer func() {
		if update && secret != nil && err == nil {
			if _, updateSecretErr := r.KubernetesClent.CoreV1().Secrets(secret.GetNamespace()).Update(secret); updateSecretErr != nil {
				glog.Errorf("Error updating Secret \"%s/%s\". err: %v", secret.GetNamespace(), secret.GetName(), err)
			}
		}
		if err != nil {
			glog.Errorf("POST Authorize request error. name %v, payload: secret %s/%s, code: %s, get access token: %v , err: %v", name, payload.Namespace, payload.SecretName, payload.Code, payload.GetAccessToken, err)
		}
	}()
	service, secret, err = r.getSecretAndCodeRepoService(ctx, name, payload.SecretName, payload.Namespace)
	if err != nil {
		return
	}
	if secret.Type != devops.SecretTypeOAuth2 {
		err = errors.NewBadRequest(fmt.Sprintf("Secret \"%s/%s\" type \"%s\" is not oAuth2 type", secret.GetNamespace(), secret.GetName(), secret.Type))
		return
	}

	glog.V(5).Infof("Check whether secret \"%s/%s\" is used by another service", secret.GetNamespace(), secret.GetName())
	serviceName, refered := r.IsSecretUsedByAnotherService(secret, name)
	if refered && len(secret.GetOwnerReferences()) > 1 {
		err = errors.NewBadRequest(fmt.Sprintf("secret has been used by service '%s', please select another secret", serviceName))
		return
	}
	// get a copy of the secret before proceeding
	secret = secret.DeepCopy()
	if payload.Code != "" {
		secret.Data[devops.OAuth2CodeKey] = []byte(payload.Code)
		if payload.RedirectURL != "" {
			secret.Data[devops.OAuth2RedirectURLKey] = []byte(payload.RedirectURL)
		}
		// in case the user request using the code
		// should clear out the other oAuth2 data
		// in order to start a new authorization process
		delete(secret.Data, devops.OAuth2AccessTokenKey)
		delete(secret.Data, devops.OAuth2AccessTokenKeyKey)
		delete(secret.Data, devops.OAuth2ScopeKey)
		delete(secret.Data, devops.OAuth2RefreshTokenKey)
		delete(secret.Data, devops.OAuth2CreatedAtKey)
		delete(secret.Data, devops.OAuth2ExpiresInKey)
		update = true
	}
	// if requested will try to fetch the access token
	if payload.GetAccessToken {
		secret, update, err = r.getAccessToken(secret, service, typedResponse)
	}

	response = typedResponse
	return
}

func (r *SecretREST) getSecretAndCodeRepoService(ctx context.Context, name, secretName, secretNamespace string) (service *devops.CodeRepoService, secret *corev1.Secret, err error) {
	glog.V(5).Infof("Fetching CodeRepoService \"%s\"", name)
	service, err = bindingregistry.GetCodeRepoService(r.CodeRepoServiceStore, ctx, name)
	if err != nil {
		return
	}
	glog.V(5).Infof("Fetching secret \"%s/%s\"", secretNamespace, secretName)
	secret, err = r.SecretLister.Secrets(secretNamespace).Get(secretName)
	return
}

func (r *SecretREST) getAccessToken(secret *corev1.Secret, service *devops.CodeRepoService, response *devops.CodeRepoServiceAuthorizeResponse) (secretCopy *corev1.Secret, update bool, err error) {
	glog.V(5).Infof("Checking secret \"%s/%s\" to see if the needed data exists for oAuth2", secret.GetNamespace(), secret.GetName())
	oAuth2Data := k8s.GetDataOAuth2FromSecret(secret)
	if !oAuth2Data.HasEnoughParamsToGetAccessToken() {
		err = errors.NewBadRequest(fmt.Sprintf("Secret \"%s/%s\" has not enought parameters to get access token.", secret.GetNamespace(), secret.GetName()))
		return
	}
	glog.V(5).Infof("Generating service client for CodeRepoService \"%s\"", service.GetName())
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetHost()),
		devopsclient.NewGenericOpts(service, secret),
		devopsclient.NewTransport(r.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)

	if err != nil {
		err = errors.NewInternalError(err)
		return
	}
	redirectURL := k8s.GetOAuthRedirectURL(r.ConfigMapLister, secret, r.Provider)

	response.RedirectUrl = redirectURL
	glog.V(5).Infof("Fetching access token for CodeRepoService \"%s\" with RedirectURL: \"%s\"", service.GetName(), redirectURL)
	secretCopy, err = serviceClient.AccessToken(context.TODO(), redirectURL)
	if err != nil {
		err = errors.NewInternalError(err)
		return
	}
	if secretCopy != nil {
		// setting code repo service as its owner
		gvk := schema.GroupVersionKind{Group: devops.GroupName, Version: v1alpha1.Version, Kind: v1alpha1.TypeCodeRepoService}
		secretCopy.OwnerReferences = k8s.AddOwnerReference(secretCopy.OwnerReferences, *metav1.NewControllerRef(service, gvk))
		update = true
		response.GotAccessToken = true
	}
	return
}

// IsSecretUsedByAnotherService secret is used by another service
func (r *SecretREST) IsSecretUsedByAnotherService(secret *corev1.Secret, name string) (string, bool) {
	serviceOwnerReferences := k8s.GetOwnerReferences(secret.ObjectMeta, devops.TypeCodeRepoService)
	if len(serviceOwnerReferences) > 0 {
		for _, ref := range serviceOwnerReferences {
			if ref.Name != name {
				return ref.Name, true
			}
		}
	}
	return "", false
}

// NewGetOptions creates a new options object
func (r *SecretREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoServiceAuthorizeOptions{}, false, ""
}
