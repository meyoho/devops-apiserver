package rest_test

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	mockregistry "alauda.io/devops-apiserver/pkg/mock/apiserver/registry"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	crsrest "alauda.io/devops-apiserver/pkg/registry/devops/codereposervice/rest"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	k8sinformer "k8s.io/client-go/informers"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	corev1listers "k8s.io/client-go/listers/core/v1"
	k8stesting "k8s.io/client-go/testing"
)

var _ = Describe("SecretREST.Create", func() {
	// test SecretREST.Create method
	// to request for a new auth code

	var (
		ctrl                *gomock.Controller
		restAPI             *crsrest.SecretREST
		codeRepoServiceMock *mockregistry.MockStandardStorage
		codeRepoBindingMock *mockregistry.MockStandardStorage
		secretLister        corev1listers.SecretLister
		configmapLister     corev1listers.ConfigMapLister
		k8sclient           *k8sfake.Clientset
		ctx                 context.Context
		name                string
		object              runtime.Object
		createValidation    rest.ValidateObjectFunc
		opts                *metav1.CreateOptions
		codeRepoService     *devops.CodeRepoService
		secret              *corev1.Secret
		configMap           *corev1.ConfigMap
		roundTripper        *mhttp.MockRoundTripper
		stop                chan struct{}

		response runtime.Object
		err      error
	)

	BeforeEach(func() {
		stop = make(chan struct{})
		ctrl = gomock.NewController(GinkgoT())
		codeRepoServiceMock = mockregistry.NewMockStandardStorage(ctrl)
		codeRepoBindingMock = mockregistry.NewMockStandardStorage(ctrl)
		roundTripper = mhttp.NewMockRoundTripper(ctrl)
		ctx = context.TODO()

		object = &devops.CodeRepoServiceAuthorizeCreate{
			Namespace:      "namespace",
			SecretName:     "secret",
			Code:           "code",
			GetAccessToken: true,
		}
		name = "codereposervice"

		codeRepoService = &devops.CodeRepoService{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: devops.CodeRepoServiceSpec{
				ToolSpec: devops.ToolSpec{
					HTTP: devops.HostPort{
						Host: "http://localhost",
					},
				},
				Type:   devops.CodeRepoServiceTypeGithub,
				Public: true,
			},
		}

		// add product annotation to for use of
		// "ace_ui_endpoint" value in configmap
		secret = &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "secret",
				Namespace: "namespace",
				Annotations: map[string]string{

					devops.AnnotationsKeyProduct: "ACE",
				},
			},
			Type: devops.SecretTypeOAuth2,
			Data: map[string][]byte{
				devops.OAuth2ClientIDKey:     []byte("clientkey"),
				devops.OAuth2ClientSecretKey: []byte("clientsecret"),
			},
		}
		configMap = &corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      devops.SettingsConfigMapName,
				Namespace: "alauda-system",
			},
			Data: map[string]string{
				"ace_ui_endpoint": "http://ace.ui.alauda.cn/console/",
			},
		}

		k8sclient = k8sfake.NewSimpleClientset(secret, configMap)
	})

	ExecuteTest := func() {

		provider := devops.NewAnnotationProvider(devops.UsedBaseDomain)

		informer := k8sinformer.NewSharedInformerFactory(k8sclient, time.Minute)
		configmapLister = informer.Core().V1().ConfigMaps().Lister()
		secretLister = informer.Core().V1().Secrets().Lister()
		informer.Start(stop)
		informer.WaitForCacheSync(stop)
		restAPI = crsrest.NewSecretREST(codeRepoServiceMock, codeRepoBindingMock, secretLister, configmapLister, k8sclient, roundTripper, provider).(*crsrest.SecretREST)
		response, err = restAPI.Create(ctx, name, object, createValidation, opts)
	}

	AfterEach(func() {
		close(stop)
		ctrl.Finish()
	})

	It("should update secret with code", func() {
		// adding mock expections
		codeRepoServiceMock.EXPECT().
			Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"}).
			Return(codeRepoService, nil)
		// http request for access token
		// ignoring payload for now
		request, _ := http.NewRequest(
			http.MethodPost,
			"https://github.com/login/oauth/access_token",
			ioutil.NopCloser(bytes.NewBufferString(`{
				"": "",
				"": "",
				"": "",
				"": "",
				"": ""
			}`)),
		)
		roundTripper.EXPECT().
			RoundTrip(mhttp.NewRequestMatcher(request)).
			Return(&http.Response{
				Body: ioutil.NopCloser(bytes.NewBufferString(`{
					"token_type": "bearer",
					"access_token": "accesstoken",
					"scope": "user,repo"
				}`)),
			}, nil)

		ExecuteTest()

		Expect(err).To(BeNil())
		Expect(response).ToNot(BeNil())
		authResponse := response.(*devops.CodeRepoServiceAuthorizeResponse)
		Expect(authResponse.GotAccessToken).To(BeTrue())

		// checking secret operation
		Expect(k8sclient.Actions()).To(ContainElement(
			WithTransform(
				func(action k8stesting.Action) bool {
					return action.Matches("update", "secrets")
				},
				BeTrue(),
			),
		), "should update secret")

		updatedSecret, secretErr := k8sclient.CoreV1().Secrets(secret.Namespace).Get(secret.Name, metav1.GetOptions{ResourceVersion: "0"})
		Expect(secretErr).To(BeNil(), "should still have the secret in the client")
		Expect(updatedSecret).ToNot(BeNil(), "should return a secret")
		Expect(updatedSecret.OwnerReferences).ToNot(BeNil(), "should have owner references")
		Expect(updatedSecret.OwnerReferences).To(HaveLen(1), "should have one owner references only")
		Expect(updatedSecret.Data).To(HaveLen(3))
	})
})
