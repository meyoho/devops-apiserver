package rest

import (
	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	// devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	"context"
	"fmt"

	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
	glog "k8s.io/klog"
)

type ProjectREST struct {
	codeRepoServiceStoreStore rest.StandardStorage
	secretLister              corev1listers.SecretLister
	RoundTripper              http.RoundTripper
}

func NewProjectREST(
	codeRepoServiceStoreStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	roundTripper http.RoundTripper,
) rest.Storage {
	return &ProjectREST{
		codeRepoServiceStoreStore: codeRepoServiceStoreStore,
		secretLister:              secretLister,
		RoundTripper:              roundTripper,
	}
}

// ProjectREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&ProjectREST{})

// New creates a new CreateProjectOptions object
func (r *ProjectREST) New() runtime.Object {
	return &devops.CreateProjectOptions{}
}

// NewGetOptions creates a new options object
func (r *ProjectREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ListProjectOptions{}, false, ""
}

func (r *ProjectREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	listOptions, ok := opts.(*devops.ListProjectOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object:%#v , expect to be ListProjectOptions", opts)
	}

	client, err := r.getCodeRepoServiceClient(ctx, name, listOptions.SecretName, listOptions.Namespace)
	if err != nil {
		return nil, err
	}

	subResourceProjectDataList, err := client.ListCodeRepoProjects(ctx, *listOptions)
	if err != nil {
		glog.Errorf("List subresource projects error:%#v", err)
		return nil, err
	}

	return subResourceProjectDataList, err
}

var _ = rest.NamedCreater(&ProjectREST{})

func (r *ProjectREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (runtime.Object, error) {

	createOptions, ok := obj.(*devops.CreateProjectOptions)
	if !ok {
		glog.Errorf("invalid create project options object %#v, expecte CreateProjectOptions", obj)
		return nil, fmt.Errorf("invalid create project options object")
	}

	client, err := r.getCodeRepoServiceClient(ctx, name, createOptions.SecretName, createOptions.Namespace)
	if err != nil {
		return nil, err
	}

	projectData, err := client.CreateCodeRepoProject(ctx, *createOptions)
	if err != nil {
		return nil, err
	}

	return projectData, nil
}

func (r *ProjectREST) getCodeRepoServiceClient(ctx context.Context, name, secretName, secretNamespace string) (
	devopsclient.Interface, error,
) {

	codeRepoService, err := getCodeRepoService(ctx, r.codeRepoServiceStoreStore, name, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("Error get codeRepoServiceStore '%s', error:%#v", name, err)
		return nil, err
	}
	if codeRepoService == nil {
		glog.Errorf("Not found codeRepoService '%s'", name)
		return nil, errors.NewNotFound(devops.Resource(devops.ResourceKindCodeRepoService), name)
	}

	secret, err := r.secretLister.Secrets(secretNamespace).Get(secretName)
	if err != nil {
		glog.Errorf("Error get secret '%s/%s', error:%#v", secretNamespace, secretName, err)
		return nil, err
	}
	if secret == nil {
		glog.Errorf("Not found secret '%s'", secretName)
		return nil, errors.NewNotFound(devops.Resource(devops.ResourceKindCodeRepoService), secretName)
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(codeRepoService.GetHost()),
		devopsclient.NewGenericOpts(codeRepoService, secret),
		devopsclient.NewTransport(r.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		codeRepoService.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		glog.Errorf("Get CodeRepoServiceClient Error:%#v", err)
		return nil, err
	}

	return serviceClient, nil
}

func getCodeRepoService(ctx context.Context, getter devopsgeneric.ResourceGetter, name string, options metav1.GetOptions) (
	*devops.CodeRepoService, error,
) {
	obj, err := getter.Get(ctx, name, &options)
	if err != nil {
		return nil, err
	}
	codeRepoService := obj.(*devops.CodeRepoService)
	if codeRepoService == nil {
		err = fmt.Errorf("unexpected object type: %#v", obj)
		return nil, err
	}
	return codeRepoService, nil
}
