package rest

import (
	"context"
	"fmt"
	"k8s.io/api/core/v1"
	glog "k8s.io/klog"
	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	internalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
)

type RolesREST struct {
	Factory internalthirdparty.CodeRepoServiceClientFactory
}

func NewRolesREST(transport http.RoundTripper) RolesREST {
	factory := internalthirdparty.NewCodeRepoServiceClientFactory(transport)
	rolesREST := RolesREST{
		Factory: factory,
	}
	return rolesREST
}

func (r RolesREST) GetRolesMappingFunc(
	getter dependency.Manager, context context.Context, name string, listOptions *devops.RoleMappingListOptions) (
	*devops.RoleMapping, error) {

	client, err := r.getCodeRepoServiceClient(getter, context, name)
	if err != nil {
		return nil, err
	}

	roleMapping, err := client.GetRolesMapping(listOptions)
	if err != nil {
		glog.Errorf("Error Get CodeRepoService roles mapping,err:%#v,  toolname:%s, listoptions:%#v ", err, name, listOptions)
		return nil, err
	}

	glog.V(7).Infof("Get CodeRepoService roles mapping, rolemapping:%#v", roleMapping)
	return &roleMapping, err
}

func (r RolesREST) ApplyRolesMappingFunc(
	getter dependency.Manager, context context.Context, name string, roleMapping *devops.RoleMapping) (
	rolemappinglist *devops.RoleMapping, err error) {

	client, err := r.getCodeRepoServiceClient(getter, context, name)
	if err != nil {
		return nil, err
	}

	err = client.ApplyRoleMapping(roleMapping)

	if err != nil {
		glog.Errorf("Error Apply CodeRepoService rolesmapping,err:%#v,  toolname:%s, roleMapping:%#v ", err, name, *roleMapping)
		return nil, err
	}
	glog.V(7).Infof("Applied CodeRepoService rolemapping, toolname:%s, roleMapping:%#v ", name, *roleMapping)

	// get current rolemapping
	listOptions := &devops.RoleMappingListOptions{Projects: r.getAllProjects(roleMapping)}
	result, err := client.GetRolesMapping(listOptions)

	return &result, err
}

func (r RolesREST) getAllProjects(roleMapping *devops.RoleMapping) []string {
	projects := make([]string, 0, len(roleMapping.Spec))
	for _, item := range roleMapping.Spec {
		projects = append(projects, item.Project.Name)
	}

	return projects
}

func (r RolesREST) getCodeRepoServiceClient(getter dependency.Manager, context context.Context, name string) (
	clientInterface internalthirdparty.CodeRepoServiceClientInterface, err error) {

	tool, secret, err := r.getToolAndSecret(context, name, getter)
	if err != nil {
		glog.Errorf("Error get CodeRepoService and secret, error:%s , name:%s", err.Error(), name)
		return nil, err
	}

	client, err := r.Factory.GetCodeRepoServiceClient(&tool, &secret)
	if err != nil {
		glog.Errorf("Error get CodeRepoServiceClient, error:%s , tool:%#v, secret:%#v", err.Error(), tool, secret)
		return nil, err
	}

	return client, nil
}

func (r RolesREST) getToolAndSecret(context context.Context, name string, getter dependency.Manager) (tool devops.CodeRepoService, secret v1.Secret, err error) {
	depItemList := getter.Get(context, devops.TypeCodeRepoService, name)
	if err = depItemList.Validate(); err != nil {
		return tool, secret, err
	}

	depItemList.GetInto(devops.TypeCodeRepoService, &tool).GetInto(devops.TypeSecret, &secret)
	if tool.Name == "" {
		return tool, secret, fmt.Errorf("error get CodeRepoService '%s' from dependencyGetter", name)
	}
	if secret.Name == "" {
		return tool, secret, fmt.Errorf("error get Secret of CodeRepoService '%s' from dependencyGetter", name)
	}
	return tool, secret, err
}
