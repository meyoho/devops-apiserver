package rest

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	mockregistry "alauda.io/devops-apiserver/pkg/mock/apiserver/registry"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sinformer "k8s.io/client-go/informers"
	k8sfake "k8s.io/client-go/kubernetes/fake"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

type datapool struct {
	gitlabSecret                      *corev1.Secret
	gitlabCodeRepoService             *devops.CodeRepoService
	getGitLabGroupListRequest         *http.Request
	getGitLabGroupRequest             func(name string) *http.Request
	getGitlabGroupListResponse        func() *http.Response
	getGitlabGroupResponse            func(name string) *http.Response
	notFoundGitlabGroupResponse       func(request *http.Request) *http.Response
	gitlabProjectDataList             *devops.ProjectDataList
	createGitLabGroupRequest          *http.Request
	createdGitLabGroupSuccessResponse func() *http.Response
	currentUserResponse               func() *http.Response
}

var data = datapool{
	currentUserResponse: func() *http.Response {
		return &http.Response{
			Status: "200 OK",
			Header: http.Header{
				"Content-Type": []string{
					"application/json",
				},
			},
			StatusCode: http.StatusOK,
			Proto:      "HTTP/1.1",
			ProtoMajor: 1,
			ProtoMinor: 0,
			// Request:    request,
			Body: ioutil.NopCloser(bytes.NewBufferString(`{
"id": 1,
"username": "john_smith",
"email": "john@example.com",
"name": "John Smith",
"state": "active",
"avatar_url": "http://localhost:3000/uploads/user/avatar/1/index.jpg",
"web_url": "http://localhost:3000/john_smith",
"created_at": "2012-05-23T08:00:58Z",
"bio": null,
"location": null,
"public_email": "john@example.com",
"skype": "",
"linkedin": "",
"twitter": "",
"website_url": "",
"organization": "",
"last_sign_in_at": "2012-06-01T11:41:01Z",
"confirmed_at": "2012-05-23T09:05:22Z",
"theme_id": 1,
"last_activity_on": "2012-05-23",
"color_scheme_id": 2,
"projects_limit": 100,
"current_sign_in_at": "2012-06-02T06:36:55Z",
"identities": [
{"provider": "github", "extern_uid": "2435223452345"},
{"provider": "bitbucket", "extern_uid": "john_smith"},
{"provider": "google_oauth2", "extern_uid": "8776128412476123468721346"}
],
"can_create_group": true,
"can_create_project": true,
"two_factor_enabled": true,
"external": false,
"private_profile": false
}`)),
		}
	},
	gitlabCodeRepoService: &devops.CodeRepoService{
		TypeMeta: metav1.TypeMeta{
			Kind:       devops.ResourceKindCodeRepoService,
			APIVersion: devops.APIVersionV1Alpha1,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "gitlab",
			Namespace: "default",
		},
		Spec: devops.CodeRepoServiceSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "https://gitlab.com",
				},
			},
			Public: true,
			Type:   devops.CodeRepoServiceTypeGitlab,
		},
	},
	gitlabSecret: &corev1.Secret{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Secret",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "gitlab",
			Namespace: "global-credentials",
		},
		Data: map[string][]byte{
			"username": []byte("YWRtaW4="),
			"password": []byte("MTIzNDU2"),
		},
		Type: corev1.SecretTypeBasicAuth,
	},
	getGitlabGroupListResponse: func() *http.Response {
		return &http.Response{
			Status: "200 OK",
			Header: http.Header{
				"Content-Type": []string{
					"application/json",
				},
			},
			StatusCode: http.StatusOK,
			Proto:      "HTTP/1.1",
			ProtoMajor: 1,
			ProtoMinor: 0,
			// Request:    request,
			Body: ioutil.NopCloser(bytes.NewBufferString(`[{
				"id": 1,
				"name": "group1",
				"path": "group1",
				"full_path": "group1",
				"description": "An interesting group",
				"visibility": "public",
				"lfs_enabled": true
			}]`)),
		}
	},
	gitlabProjectDataList: &devops.ProjectDataList{
		Items: []devops.ProjectData{
			devops.ProjectData{
				ObjectMeta: metav1.ObjectMeta{
					Name: "group1",
				},
			},
		},
	},
	getGitLabGroupRequest: func(name string) *http.Request {
		req, _ := http.NewRequest(http.MethodGet, "https://gitlab.com/api/v4/groups/"+name, nil)
		return req
	},
	getGitLabGroupListRequest: func() *http.Request {
		req, _ := http.NewRequest(http.MethodGet, "https://gitlab.com/api/v4/groups", nil)
		return req
	}(),
	getGitlabGroupResponse: func(name string) *http.Response {
		return &http.Response{
			Status: "200 OK",
			Header: http.Header{
				"Content-Type": []string{
					"application/json",
				},
			},
			StatusCode: http.StatusOK,
			Proto:      "HTTP/1.1",
			ProtoMajor: 1,
			ProtoMinor: 0,
			// Request:    request,
			Body: ioutil.NopCloser(bytes.NewBufferString(fmt.Sprintf(`{
			"id": 1,
			"name": "%s",
			"path": "%s",
			"full_path": "%s",
			"description": "An interesting group",
			"visibility": "public",
			"lfs_enabled": true
			}`, name, name, name))),
		}
	},
	createGitLabGroupRequest: func() *http.Request {
		req, _ := http.NewRequest(http.MethodPost, "https://gitlab.com/api/v4/groups", nil)
		return req
	}(),
	notFoundGitlabGroupResponse: func(request *http.Request) *http.Response {
		return &http.Response{
			Status:     "404 Not Found",
			StatusCode: http.StatusNotFound,
			Proto:      "HTTP/1.1",
			Header: http.Header{
				"Content-Type": []string{
					"application/json",
				},
			},
			Request:    request,
			ProtoMajor: 1,
			ProtoMinor: 0,
			Body: ioutil.NopCloser(bytes.NewBufferString(`{
    "error": "404 Not Found Group"
		}`)),
		}
	},
	createdGitLabGroupSuccessResponse: func() *http.Response {
		return &http.Response{
			Status: "201 CREATED",
			Header: http.Header{
				"Content-Type": []string{
					"application/json",
				},
			},
			StatusCode: http.StatusCreated,
			Proto:      "HTTP/1.1",
			ProtoMajor: 1,
			ProtoMinor: 0,
			Body: ioutil.NopCloser(bytes.NewBufferString(`{
				"id": 2,
				"name": "group2",
				"path": "group2",
				"full_path": "group2",
				"description": "An interesting group",
				"visibility": "public",
				"lfs_enabled": true
			}`)),
		}
	},
}

var _ = Describe("CodeRepoService ProjectRest", func() {

	var (
		ctrl                 *gomock.Controller
		codeRepoServiceStore *mockregistry.MockStandardStorage
		k8sclient            *k8sfake.Clientset
		ctx                  context.Context
		secretLister         corev1listers.SecretLister
		stop                 chan struct{}
		roundTripper         *mhttp.MockRoundTripper
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		stop = make(chan struct{})

		// mock dependency
		codeRepoServiceStore = mockregistry.NewMockStandardStorage(ctrl)
		k8sclient = k8sfake.NewSimpleClientset(data.gitlabSecret)
		roundTripper = mhttp.NewMockRoundTripper(ctrl)

		// prepare our dependency
		k8sInformerFactory := k8sinformer.NewSharedInformerFactory(k8sclient, 0)
		secretLister = k8sInformerFactory.Core().V1().Secrets().Lister()
		k8sInformerFactory.Start(stop)
		k8sInformerFactory.WaitForCacheSync(stop)
	})

	AfterEach(func() {
		// clean resources
		close(stop)
		ctrl.Finish()
	})

	Describe("Get ProjectDataList", func() {
		Context("When data correctly", func() {

			It("Should return ProjectDataList", func() {
				// change output
				codeRepoServiceStore.EXPECT().Get(ctx, "gitlab", &metav1.GetOptions{ResourceVersion: "0"}).Return(data.gitlabCodeRepoService, nil).AnyTimes()

				listTeamsRequest, _ := http.NewRequest(http.MethodGet, "/api/v4/groups", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listTeamsRequest)).Return(data.getGitlabGroupListResponse(), nil).AnyTimes()

				listUserRequest, _ := http.NewRequest(http.MethodGet, "/api/v4/user", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(listUserRequest)).Return(data.currentUserResponse(), nil).AnyTimes()

				// do case
				projectREST := NewProjectREST(codeRepoServiceStore, secretLister, roundTripper).(*ProjectREST)
				result, err := projectREST.Get(ctx, "gitlab", &devops.ListProjectOptions{
					SecretName: "gitlab",
					Namespace:  "global-credentials",
				})

				// assert
				gomega.Expect(err).To(gomega.BeNil())
				actualList, ok := result.(*devops.ProjectDataList)
				gomega.Expect(ok).To(gomega.BeTrue())
				gomega.Expect(len(actualList.Items)).To(
					gomega.BeEquivalentTo(2),
				)
				gomega.Expect(actualList.Items[0].Name).To(
					gomega.BeEquivalentTo(data.gitlabProjectDataList.Items[0].Name),
				)
				gomega.Expect(actualList.Items[1].Name).To(
					gomega.BeEquivalentTo("john_smith"),
				)
			})
		})

		Context("When secret is not exists", func() {
			It("Should return secret not exists ErrorStatus", func() {
				// change output
				codeRepoServiceStore.EXPECT().Get(ctx, "gitlab", &metav1.GetOptions{ResourceVersion: "0"}).Return(data.gitlabCodeRepoService, nil)
				// do case
				projectREST := NewProjectREST(codeRepoServiceStore, secretLister, roundTripper).(*ProjectREST)
				_, err := projectREST.Get(ctx, "gitlab", &devops.ListProjectOptions{
					SecretName: "gitlab1",
					Namespace:  "global-credentials",
				})

				// assert
				gomega.Expect(err).ToNot(gomega.BeNil())
				_, ok := err.(*errors.StatusError)
				gomega.Expect(ok).To(gomega.BeTrue())
			})
		})

		Context("When codereposervice is not exists", func() {
			It("Should return codeRepoService not exists ErrorStatus", func() {
				// change output
				codeRepoServiceStore.EXPECT().Get(ctx, "gitlab", &metav1.GetOptions{ResourceVersion: "0"}).Return(nil, errors.NewNotFound(devops.Resource(devops.ResourceKindCodeRepoService), "gitlab"))
				// do case
				projectREST := NewProjectREST(codeRepoServiceStore, secretLister, roundTripper).(*ProjectREST)
				_, err := projectREST.Get(ctx, "gitlab", &devops.ListProjectOptions{
					SecretName: "gitlab",
					Namespace:  "global-credentials",
				})

				// assert
				gomega.Expect(err).ToNot(gomega.BeNil())
				_, ok := err.(*errors.StatusError)
				gomega.Expect(ok).To(gomega.BeTrue())
			})
		})
	})

	Describe("Create ProjectData", func() {
		Context("When data is correct", func() {
			It("Should create project", func() {
				// change output
				codeRepoServiceStore.EXPECT().Get(ctx, "gitlab", &metav1.GetOptions{ResourceVersion: "0"}).Return(data.gitlabCodeRepoService, nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(data.getGitLabGroupRequest("group2"))).
					Return(data.notFoundGitlabGroupResponse(data.getGitLabGroupRequest("group2")), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(data.createGitLabGroupRequest)).Return(data.createdGitLabGroupSuccessResponse(), nil)

				// do case
				projectREST := NewProjectREST(codeRepoServiceStore, secretLister, roundTripper).(*ProjectREST)

				result, err := projectREST.Create(ctx, "gitlab", &devops.CreateProjectOptions{
					SecretName: "gitlab",
					Namespace:  "global-credentials",
					Name:       "group2",
				}, nil, nil)
				gomega.Expect(err).To(gomega.BeNil())
				createdProjectData := result.(*devops.ProjectData)
				gomega.Expect(createdProjectData.Name).To(gomega.BeEquivalentTo("group2"))
			})
		})

		Context("When project is already exists", func() {
			It("Should return error", func() {
				// change output
				codeRepoServiceStore.EXPECT().Get(ctx, "gitlab", &metav1.GetOptions{ResourceVersion: "0"}).Return(data.gitlabCodeRepoService, nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(data.getGitLabGroupRequest("group1"))).Return(data.getGitlabGroupResponse("group1"), nil)
				// do case
				projectREST := NewProjectREST(codeRepoServiceStore, secretLister, roundTripper).(*ProjectREST)

				_, err := projectREST.Create(ctx, "gitlab", &devops.CreateProjectOptions{
					SecretName: "gitlab",
					Namespace:  "global-credentials",
					Name:       "group1",
				}, nil, nil)
				gomega.Expect(err).ToNot(gomega.BeNil())
			})
		})
	})

})
