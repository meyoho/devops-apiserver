package rest_test

import (
	"context"
	"fmt"
	"strings"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mdependency "alauda.io/devops-apiserver/pkg/mock/dependency"
	devopsrest "alauda.io/devops-apiserver/pkg/registry/devops/pipelinetemplate/rest"
	"github.com/golang/mock/gomock"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

func TestPreview(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockDependency := mdependency.NewMockManager(ctrl)

	previewREST := devopsrest.NewPreviewREST(mockDependency)

	type TestCase struct {
		Name                 string
		PipelineTemplate     func() (*devops.PipelineTemplate, error)
		PipelineTaskTemplate func() ([]*devops.PipelineTaskTemplate, error)
		Prepare              func() (ctx context.Context, name string, opts runtime.Object)
		Run                  func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error)
		Expect               string
		Err                  error
	}

	table := []TestCase{
		{
			Name: "case 0: all ok simplest jenkinsfile",
			PipelineTemplate: func() (tempalte *devops.PipelineTemplate, err error) {
				tempalte = &devops.PipelineTemplate{
					Spec: devops.PipelineTemplateSpec{
						Stages: []devops.PipelineStage{
							{
								Name: "test",
								Tasks: []devops.PipelineTemplateTask{
									{
										Name: "test",
										Kind: "PipelineTaskTemplate",
									},
								},
							},
						},
					},
				}
				return
			},
			PipelineTaskTemplate: func() (results []*devops.PipelineTaskTemplate, err error) {
				taskTemplate := &devops.PipelineTaskTemplate{
					Spec: devops.PipelineTaskTemplateSpec{
						Body: "echo '1'",
					},
				}
				taskTemplate.Name = "test"
				results = append(results, taskTemplate)
				return
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{}
				return
			},
			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, nil)
			},
			Err:    nil,
			Expect: "echo '1'",
		},
		{
			Name: "case 1: test values",
			PipelineTemplate: func() (tempalte *devops.PipelineTemplate, err error) {
				tempalte = &devops.PipelineTemplate{
					Spec: devops.PipelineTemplateSpec{
						Stages: []devops.PipelineStage{
							{
								Name: "test",
								Tasks: []devops.PipelineTemplateTask{
									{
										Name: "test",
										Kind: "PipelineTaskTemplate",
									},
								},
							},
						},
						Arguments: []devops.PipelineTemplateArgumentGroup{
							{
								DisplayName: devops.I18nName{
									Zh: "param",
									En: "param",
								},
								Items: []devops.PipelineTemplateArgumentValue{
									{
										PipelineTemplateArgument: devops.PipelineTemplateArgument{
											Name: "Param",
											Schema: devops.PipelineTaskArgumentSchema{
												Type: "string",
											},
											Display: devops.PipelineTaskArgumentDisplay{
												Type: "string",
												Name: devops.I18nName{
													Zh: "param",
													En: "param",
												},
											},
											Binding: []string{"test.args.Param"},
										},
									},
								},
							},
						},
					},
				}
				return
			},
			PipelineTaskTemplate: func() (results []*devops.PipelineTaskTemplate, err error) {
				taskTemplate := &devops.PipelineTaskTemplate{
					Spec: devops.PipelineTaskTemplateSpec{
						Body: "echo '{{.Param}}'",
						Arguments: []devops.PipelineTaskArgument{
							{
								Name: "Param",
								Schema: devops.PipelineTaskArgumentSchema{
									Type: "string",
								},
								Display: devops.PipelineTaskArgumentDisplay{
									Type: "string",
									Name: devops.I18nName{
										Zh: "param",
										En: "param",
									},
								},
							},
						},
					},
				}
				taskTemplate.Name = "test"
				results = append(results, taskTemplate)
				return
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{
					Values: map[string]string{
						"Param": "param",
					},
				}
				return
			},
			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, nil)
			},
			Err:    nil,
			Expect: "echo 'param'",
		},
		{
			Name: "case 2: test _pipeline_ arg value",
			PipelineTemplate: func() (tempalte *devops.PipelineTemplate, err error) {
				tempalte = &devops.PipelineTemplate{
					Spec: devops.PipelineTemplateSpec{
						Stages: []devops.PipelineStage{
							{
								Name: "test",
								Tasks: []devops.PipelineTemplateTask{
									{
										Name: "test",
										Kind: "PipelineTaskTemplate",
									},
								},
							},
						},
						Arguments: []devops.PipelineTemplateArgumentGroup{
							{
								DisplayName: devops.I18nName{
									Zh: "param",
									En: "param",
								},
								Items: []devops.PipelineTemplateArgumentValue{
									{
										PipelineTemplateArgument: devops.PipelineTemplateArgument{
											Name: "Param",
											Schema: devops.PipelineTaskArgumentSchema{
												Type: "string",
											},
											Display: devops.PipelineTaskArgumentDisplay{
												Type: "string",
												Name: devops.I18nName{
													Zh: "param",
													En: "param",
												},
											},
											Binding: []string{"test.args.Param"},
										},
									},
								},
							},
						},
					},
				}
				return
			},
			PipelineTaskTemplate: func() (results []*devops.PipelineTaskTemplate, err error) {
				taskTemplate := &devops.PipelineTaskTemplate{
					Spec: devops.PipelineTaskTemplateSpec{
						Body: "echo '{{.Param}}'",
						Arguments: []devops.PipelineTaskArgument{
							{
								Name: "Param",
								Schema: devops.PipelineTaskArgumentSchema{
									Type: "string",
								},
								Display: devops.PipelineTaskArgumentDisplay{
									Type: "string",
									Name: devops.I18nName{
										Zh: "param",
										En: "param",
									},
								},
							},
						},
					},
				}
				taskTemplate.Name = "test"
				results = append(results, taskTemplate)
				return
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{
					Values: map[string]string{
						"Param":      "param",
						"_pipeline_": `{ "agent": {"label": "gooo", "raw": "whatever"} }`,
					},
				}
				return
			},
			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, nil)
			},
			Err:    nil,
			Expect: "whatever",
		},
		{
			Name: "case 3: test post task",
			PipelineTemplate: func() (tempalte *devops.PipelineTemplate, err error) {
				tempalte = &devops.PipelineTemplate{
					Spec: devops.PipelineTemplateSpec{
						Stages: []devops.PipelineStage{
							{
								Name: "test",
								Tasks: []devops.PipelineTemplateTask{
									{
										Name: "test",
										Kind: "PipelineTaskTemplate",
									},
								},
							},
						},
						Arguments: []devops.PipelineTemplateArgumentGroup{
							{
								DisplayName: devops.I18nName{
									Zh: "param",
									En: "param",
								},
								Items: []devops.PipelineTemplateArgumentValue{
									{
										PipelineTemplateArgument: devops.PipelineTemplateArgument{
											Name: "Param",
											Schema: devops.PipelineTaskArgumentSchema{
												Type: "string",
											},
											Display: devops.PipelineTaskArgumentDisplay{
												Type: "string",
												Name: devops.I18nName{
													Zh: "param",
													En: "param",
												},
											},
											Binding: []string{"test.args.Param"},
										},
									},
									{
										PipelineTemplateArgument: devops.PipelineTemplateArgument{
											Name: "Message",
											Schema: devops.PipelineTaskArgumentSchema{
												Type: "string",
											},
											Display: devops.PipelineTaskArgumentDisplay{
												Type: "string",
												Name: devops.I18nName{
													Zh: "param",
													En: "param",
												},
											},
											Binding: []string{"notify.args.Message"},
										},
									},
								},
							},
						},
						Post: map[string][]devops.PipelineTemplateTask{
							"always": []devops.PipelineTemplateTask{
								devops.PipelineTemplateTask{
									Name: "Notify",
									ID:   "notify",
									Kind: "PipelineTaskTemplate",
								},
							},
						},
					},
				}
				return
			},
			PipelineTaskTemplate: func() (results []*devops.PipelineTaskTemplate, err error) {
				taskTemplate := &devops.PipelineTaskTemplate{
					Spec: devops.PipelineTaskTemplateSpec{
						Body: "echo '{{.Param}}'",
						Arguments: []devops.PipelineTaskArgument{
							{
								Name: "Param",
								Schema: devops.PipelineTaskArgumentSchema{
									Type: "string",
								},
								Display: devops.PipelineTaskArgumentDisplay{
									Type: "string",
									Name: devops.I18nName{
										Zh: "param",
										En: "param",
									},
								},
							},
						},
					},
				}
				taskTemplate.Name = "test"
				taskTemplate2 := &devops.PipelineTaskTemplate{
					Spec: devops.PipelineTaskTemplateSpec{
						Body: "echo '{{.Message}}'",
						Arguments: []devops.PipelineTaskArgument{
							{
								Name: "Message",
								Schema: devops.PipelineTaskArgumentSchema{
									Type: "string",
								},
								Display: devops.PipelineTaskArgumentDisplay{
									Type: "string",
									Name: devops.I18nName{
										Zh: "param",
										En: "param",
									},
								},
							},
						},
					},
				}
				taskTemplate2.Name = "Notify"
				results = append(results, taskTemplate)
				results = append(results, taskTemplate2)
				return
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsfilePreviewOptions{
					Values: map[string]string{
						"Param":   "hello",
						"Message": "notify-message",
					},
				}
				return
			},
			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return previewREST.Create(ctx, name, obj, createValidation, nil)
			},
			Err:    nil,
			Expect: "notify-message",
		},
	}
	for i, test := range table {
		ctx, name, opts := test.Prepare()

		options := metav1.GetOptions{ResourceVersion: "0"}
		options.Kind = "PipelineTemplate"
		template, _ := test.PipelineTemplate()
		mockDependency.EXPECT().
			Get(ctx, devops.TypePipelineTemplate, name).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypePipelineTemplate,
					Object:   template,
					IntoFunc: dependency.PipelineTemplateCopyInto,
				},
			})

		taskTemplates, _ := test.PipelineTaskTemplate()
		for _, taskTemplate := range taskTemplates {
			mockDependency.EXPECT().
				Get(ctx, devops.TypePipelineTaskTemplate, taskTemplate.Name).
				Return(dependency.ItemList{
					&dependency.Item{
						Kind:     devops.TypePipelineTaskTemplate,
						Object:   taskTemplate,
						IntoFunc: dependency.PipelineTaskTemplateCopyInto,
					},
				})
		}

		result, err := test.Run(ctx, name, opts, func(obj runtime.Object) error { return nil }, false)

		if err == nil && test.Err != nil {
			t.Errorf("[%d] %s: Should fail but didnt: %v", i, test.Name, test.Err)
		} else if err != nil && test.Err == nil {
			t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
		}

		preview, ok := result.(*devops.JenkinsfilePreview)
		if !ok || !strings.Contains(preview.Jenkinsfile, test.Expect) {
			t.Errorf("[%d] %s invalid type, should be devops.JenkinsfilePreview", i, test.Name)
		}
		fmt.Print(preview.Jenkinsfile)
	}
}
