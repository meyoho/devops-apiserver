package rest

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/registry/generic"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

const PipelineTemplatePreviewREST = "PipelineTemplatePreviewREST"

func NewPreviewREST(getter dependency.Manager) *generic.PreviewREST {
	return generic.NewPreviewREST(
		"PipelineTemplatePreviewREST",
		getter,
		getPipelineTemplate,
	)
}

func getPipelineTemplate(ctx context.Context, getter dependency.Manager, name string) (*devops.PipelineTemplateRef, error) {

	return &devops.PipelineTemplateRef{
		Kind:      devops.TypePipelineTemplate,
		Name:      name,
		Namespace: genericapirequest.NamespaceValue(ctx),
	}, nil
}
