package pipelinetemplatesync

import (
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {
	columnDefinitions := []metav1alpha1.TableColumnDefinition{
		{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
		{Name: "Code Repository", Type: registry.TableTypeString, Description: "Code Repository to sync templates from"},
		{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "Pipeline Template Sync Status"},
		{Name: "Synced Templates", Type: registry.TableTypeString, Description: "Number of sucessfuly synced templates."},
		{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
		{Name: "Commit ID", Type: registry.TableTypeString, Priority: 1, Description: "Commit ID of the synced templates"},
		{Name: "Last Sync", Type: registry.TableTypeString, Priority: 1, Description: "Last type templates were synced"},
	}

	h.TableHandler(columnDefinitions, printPipelineTemplateSyncList)
	h.TableHandler(columnDefinitions, printPipelineTemplateSync)

	registry.AddDefaultHandlers(h)

}

func printPipelineTemplateSyncList(pipelineList *devops.PipelineTemplateSyncList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(pipelineList.Items))
	for i := range pipelineList.Items {
		r, err := printPipelineTemplateSync(&pipelineList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

func printPipelineTemplateSync(pipeline *devops.PipelineTemplateSync, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: pipeline},
	}

	codeRepo := "-"
	if pipeline.Spec.Source.Git != nil {
		codeRepo = fmt.Sprintf("%s (%s)", pipeline.Spec.Source.Git.URI, pipeline.Spec.Source.Git.Ref)
	}

	syncedTemplates := "-"
	status := "-"
	if pipeline.Status != nil && pipeline.Status.Conditions != nil {

		status = string(pipeline.Status.Phase)
		syncedNumber := 0
		for _, k := range pipeline.Status.Conditions {
			switch k.Status {
			case devops.SyncStatusSkip, devops.SyncStatusSuccess:
				syncedNumber++
			}
		}
		syncedTemplates = fmt.Sprintf("%d/%d", syncedNumber, len(pipeline.Status.Conditions))
	}

	// basic rows
	row.Cells = append(row.Cells, pipeline.Name, codeRepo, status, syncedTemplates, registry.TranslateTimestamp(pipeline.CreationTimestamp))
	if options.Wide {
		commitID := "-"
		lastSync := "-"
		if pipeline.Status != nil {
			commitID = pipeline.Status.CommitID
			if !pipeline.Status.EndTime.IsZero() {
				lastSync = registry.TranslateTimestamp(pipeline.Status.EndTime)
			}

		}
		row.Cells = append(row.Cells, commitID, lastSync)
	}
	return []metav1alpha1.TableRow{row}, nil
}
