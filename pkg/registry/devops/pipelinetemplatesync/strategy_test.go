package pipelinetemplatesync_test

import (
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/pipelinetemplatesync"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestStrategy(t *testing.T) {
	Schema := runtime.NewScheme()
	strategy := pipelinetemplatesync.NewStrategy(Schema)

	if !strategy.NamespaceScoped() {
		t.Errorf("PipelineTemplateSync strategy should be namespace scoped")
	}

	if strategy.AllowCreateOnUpdate() {
		t.Errorf("PipelineTemplateSync strategy should not allow create on update")
	}

	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("PipelineTemplateSync strategy should not allow unconditional update")
	}

	type Table struct {
		name     string
		new      *devopsv1alpha1.PipelineTemplateSync
		old      *devopsv1alpha1.PipelineTemplateSync
		expected *devopsv1alpha1.PipelineTemplateSync
		method   func(*Table)
	}

	table := []Table{
		{
			name: "simpe-pipelineTemplateSync",
			new: &devopsv1alpha1.PipelineTemplateSync{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.PipelineTemplateSyncSpec{
					Source: devopsv1alpha1.PipelineSource{
						Git:            nil,
						CodeRepository: nil,
					},
				},
			},
			old: nil,
			expected: &devopsv1alpha1.PipelineTemplateSync{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineTemplateSyncSpec{},
			},
			method: func(tb *Table) {
				strategy.PrepareForCreate(nil, tb.new)

				err := strategy.Validate(nil, tb.new)
				if len(err) != 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input generate error: %v",
						tb.name, err,
					)
				}
			},
		},
		{
			name: "lack phase",
			new: &devopsv1alpha1.PipelineTemplateSync{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.PipelineTemplateSyncSpec{
					Source: devopsv1alpha1.PipelineSource{
						Git:            nil,
						CodeRepository: nil,
					},
				},
				Status: &devopsv1alpha1.PipelineTemplateSyncStatus{},
			},
			old: nil,
			expected: &devopsv1alpha1.PipelineTemplateSync{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineTemplateSyncSpec{},
			},
			method: func(tb *Table) {
				strategy.PrepareForCreate(nil, tb.new)

				err := strategy.Validate(nil, tb.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input generate error: %v",
						tb.name, err,
					)
				}
			},
		},
	}

	for _, tb := range table {
		tb.method(&tb)
	}
}
