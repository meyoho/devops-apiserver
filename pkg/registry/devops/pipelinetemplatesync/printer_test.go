package pipelinetemplatesync

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestPipelineTaskPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	nowMinute := metav1.NewTime(time.Now().Add(-time.Minute))
	template1 := &devops.PipelineTemplateSync{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "pipeline-sync",
			CreationTimestamp: now,
		},
		Spec: devops.PipelineTemplateSyncSpec{
			Source: devops.PipelineSource{
				Git: &devops.PipelineSourceGit{
					URI: "https://github.com/alauda/alauda",
					Ref: "release",
				},
			},
		},
		Status: &devops.PipelineTemplateSyncStatus{
			Phase: devops.PipelineTemplateSyncPhaseReady,
			Conditions: []devops.PipelineTemplateSyncCondition{
				devops.PipelineTemplateSyncCondition{
					Status: devops.SyncStatusFailure,
				},
				devops.PipelineTemplateSyncCondition{
					Status: devops.SyncStatusSuccess,
				},
				devops.PipelineTemplateSyncCondition{
					Status: devops.SyncStatusSkip,
				},
			},
			CommitID: "123456",
			EndTime:  nowMinute,
		},
	}
	list := &devops.PipelineTemplateSyncList{
		Items: []devops.PipelineTemplateSync{*template1},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: &list.Items[0]},
			Cells: []interface{}{
				"pipeline-sync", "https://github.com/alauda/alauda (release)", string(devops.PipelineTemplateSyncPhaseReady), "2/3", registry.TranslateTimestamp(now), "123456", registry.TranslateTimestamp(nowMinute),
			},
		},
	}

	result, err := printPipelineTemplateSyncList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing pipeline task template: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
