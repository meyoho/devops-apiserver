package pipeline

import (
	"context"
	"fmt"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"net/url"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	util "alauda.io/devops-apiserver/pkg/util/generic"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// URLBuilder URL Builder interface
type URLBuilder interface {
	GetPipelineDependencies(
		getter devopsgeneric.ResourceGetter,
		jenkinsGetter devopsgeneric.ResourceGetter,
		jenkinsBindingGetter devopsgeneric.ResourceGetter,
		ctx context.Context,
		name string,
	) (pipe *devops.Pipeline, jenkinsBinding *devops.JenkinsBinding, jenkins *devops.Jenkins, err error)

	GetTestReportURL( // TODO should remove at v2.3
		pipe *devops.Pipeline,
		jenkinsBinding *devops.JenkinsBinding,
		jenkins *devops.Jenkins,
		opts *devops.PipelineTestReportOptions,
	) (urlLoc *url.URL, err error)
	GetSecret(
		secretLister corev1listers.SecretLister,
		account devops.UserAccount,
		namespace string,
	) (*corev1.Secret, error)
}

// make sure the builder implements interface
var _ URLBuilder = URLBuilderImp{}

// URLBuilderImp builder implementation for URL builder
type URLBuilderImp struct {
}

// GetPipelineDependencies get pipeline dependencies/related resources
func (URLBuilderImp) GetPipelineDependencies(
	getter devopsgeneric.ResourceGetter,
	jenkinsGetter devopsgeneric.ResourceGetter,
	jenkinsBindingGetter devopsgeneric.ResourceGetter,
	ctx context.Context,
	name string,
) (pipe *devops.Pipeline, jenkinsBinding *devops.JenkinsBinding, jenkins *devops.Jenkins, err error) {
	// fetching basic resources for this query
	pipe, err = getPipeline(getter, ctx, name)
	if err != nil {
		return
	}
	jenkinsBinding, err = getJenkinsBinding(jenkinsBindingGetter, ctx, pipe.Spec.JenkinsBinding.Name)
	if err != nil {
		return
	}
	jenkins, err = getJenkins(jenkinsGetter, ctx, jenkinsBinding.Spec.Jenkins.Name)
	return
}

// GetTestReportURL return the test report URL
func (i URLBuilderImp) GetTestReportURL(
	pipe *devops.Pipeline,
	jenkinsBinding *devops.JenkinsBinding,
	jenkins *devops.Jenkins,
	opts *devops.PipelineTestReportOptions,
) (urlLoc *url.URL, err error) {

	return
}

// GetSecret fetches a secret from a lister
func (URLBuilderImp) GetSecret(
	secretLister corev1listers.SecretLister,
	account devops.UserAccount,
	namespace string,
) (secret *corev1.Secret, err error) {
	if account.Secret.Name == "" {
		return
	}
	if account.Secret.Namespace != "" {
		namespace = account.Secret.Namespace
	}
	return secretLister.Secrets(namespace).Get(account.Secret.Name)
}

func getPipeline(getter devopsgeneric.ResourceGetter, ctx context.Context, name string) (pipe *devops.Pipeline, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	pipe = obj.(*devops.Pipeline)
	if pipe == nil {
		err = fmt.Errorf("Unexpected object type: %#v", obj)
	}
	return
}

func getJenkinsBinding(getter devopsgeneric.ResourceGetter, ctx context.Context, name string) (jenkBinding *devops.JenkinsBinding, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	jenkBinding = obj.(*devops.JenkinsBinding)
	if jenkBinding == nil {
		err = fmt.Errorf("Unexpected object type: %#v", obj)
	}
	return
}

func getJenkins(getter devopsgeneric.ResourceGetter, ctx context.Context, name string) (jenkins *devops.Jenkins, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	jenkins = obj.(*devops.Jenkins)
	if jenkins == nil {
		err = fmt.Errorf("Unexpected object type: %#v", obj)
	}
	return
}

func getJenkinsHostAndScheme(jenkins *devops.Jenkins) (scheme, host string) {
	return util.GetSchemaAndHost(jenkins.Spec.HTTP.Host)
}

// IsStageLog returns true if the log options is for a stage
func IsStageLog(opts *devops.PipelineLogOptions) bool {
	return opts != nil && opts.Stage > 0
}

// IsStepLog returns true if the log options is for a step
func IsStepLog(opts *devops.PipelineLogOptions) bool {
	return opts != nil && opts.Step > 0
}
