package rest

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mockregistry "alauda.io/devops-apiserver/pkg/mock/apiserver/registry"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

var _ = Describe("View", func() {
	var (
		mockCtrl         *gomock.Controller
		err              error
		getter           dependency.Manager
		configMapStorage *mockregistry.MockStandardStorage
		pipelineStorage  *mockregistry.MockStandardStorage
		ctx              context.Context
		namespace        string
		kind             string
		name             string
		viewTemplate     string
	)

	BeforeEach(func() {
		getter = dependency.DefaultManager
		mockCtrl = gomock.NewController(GinkgoT())

		configMapStorage = mockregistry.NewMockStandardStorage(mockCtrl)
		pipelineStorage = mockregistry.NewMockStandardStorage(mockCtrl)
		getter.AddDependency("ConfigMap", func(ctx context.Context, name string, opts *metav1.GetOptions) (runtime.Object, error) {
			return configMapStorage.Get(ctx, name, opts)
		})

		getter.AddDependency(devops.TypePipeline, func(ctx context.Context, name string, opts *metav1.GetOptions) (runtime.Object, error) {
			return pipelineStorage.Get(ctx, name, opts)
		})
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Context("ConfigMap", func() {
		It("should succeed", func() {
			namespace = "default"
			ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), namespace)
			kind = devops.TypeConfigMap
			name = "default"
			configMapStorage.EXPECT().
				Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"}).
				Return(&corev1.ConfigMap{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "default",
						Namespace: "default",
					},
					Data: map[string]string{
						"markdown": `#Clone
| Name | Value |
| :--- | :--- |
| 仓库地址 | [DevOps-Apiserver](https://bitbucket.org/mathildetech/devops-apiserver) |
| 分支 | Master |`,
					},
				}, nil)
			builder := viewTemplateBuilder{
				Kind:      kind,
				Namespace: namespace,
				Name:      name,
				Mgr:       getter,
			}
			viewTemplate, err = builder.Build()
			Expect(err).To(BeNil())
			Expect(viewTemplate).To(Equal(`#Clone
| Name | Value |
| :--- | :--- |
| 仓库地址 | [DevOps-Apiserver](https://bitbucket.org/mathildetech/devops-apiserver) |
| 分支 | Master |`))
		})
	})
})
