package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	pipelineregistry "alauda.io/devops-apiserver/pkg/registry/devops/pipeline"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	corev1listers "k8s.io/client-go/listers/core/v1"
	glog "k8s.io/klog"

	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

// InputREST implements the log endpoint for a Pod
type InputREST struct {
	Store               rest.StandardStorage
	JenkinsStore        rest.StandardStorage
	JenkinsBindingStore rest.StandardStorage
	SecretLister        corev1listers.SecretLister
	RoundTripper        http.RoundTripper
	Builder             pipelineregistry.URLBuilder
	AnnotationProvider  devops.AnnotationProvider
}

// NewInputREST starts a new log store
func NewInputREST(
	pipelineStore rest.StandardStorage,
	jenkinsStore rest.StandardStorage,
	jenkinsBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	roundTripper http.RoundTripper,
	builder pipelineregistry.URLBuilder,
	provider devops.AnnotationProvider,
) rest.Storage {
	return &InputREST{
		Store:               pipelineStore,
		JenkinsStore:        jenkinsStore,
		JenkinsBindingStore: jenkinsBindingStore,
		SecretLister:        secretLister,
		RoundTripper:        roundTripper,
		Builder:             builder,
		AnnotationProvider:  provider,
	}
}

// InputREST implements GetterWithOptions
var _ = rest.NamedCreater(&InputREST{})

// New creates a new PipelineInputOptions object
func (r *InputREST) New() runtime.Object {
	return &devops.PipelineInputOptions{}
}

// Create handle the POST request for subresource
func (r *InputREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (
	runtime.Object, error) {
	pipelineInputOptions, ok := obj.(*devops.PipelineInputOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("Expected object of type devops.PipelineInputOptions but got %#v", opts))
	}
	namespace := genericapirequest.NamespaceValue(ctx)
	glog.V(6).Infof(
		"Got pipeline input options: is %#v, namespace is %s, name is %s",
		pipelineInputOptions,
		namespace,
		name,
	)
	var (
		err            error
		secret         *corev1.Secret
		jenkinsClient  devopsclient.Interface
		pipe           *devops.Pipeline
		jenkinsBinding *devops.JenkinsBinding
		jenkins        *devops.Jenkins
	)

	// fetch pipeline dependencies
	if pipe, jenkinsBinding, jenkins, err = r.Builder.GetPipelineDependencies(r.Store, r.JenkinsStore, r.JenkinsBindingStore, ctx, name); err != nil {
		glog.Errorf("Error fetching pipeline dependencies %s/%s: %v", namespace, name, err)
		return nil, err
	}
	if pipe.Status.Jenkins == nil || pipe.Status.Jenkins.Build == "" {
		return nil, errors.NewBadRequest(fmt.Sprintf("Pipeline has no build ID %s/%s: %v", namespace, name, pipe.Status))
	}

	userAccount := jenkinsBinding.Spec.Account
	secret, err = r.Builder.GetSecret(r.SecretLister, userAccount, namespace)
	if err != nil {
		glog.Errorf("Got error when fetching secret: %v", err)
		return nil, err
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(jenkins.GetHostPort().Host),
		devopsclient.NewBasicAuth(k8s.GetDataBasicAuthFromSecret(secret).Username, k8s.GetDataBasicAuthFromSecret(secret).Password),
		devopsclient.NewTransport(r.RoundTripper),
	)
	if jenkinsClient, err = factory.NewClient(jenkins.GetKindType(), "", clientOpts); err != nil {
		glog.Errorf("Got error when create JenkinsClient: %v", err)
		return nil, err
	}

	pcName := pipe.Spec.PipelineConfig.Name
	if belongToMultiBranch(pipe, r.AnnotationProvider) {
		branchName := getBranchName(pipe, r.AnnotationProvider)
		err = jenkinsClient.SubmitBranchInputStep(ctx, namespace, pcName, branchName, "jenkins", pipe.Status.Jenkins.Build, fmt.Sprintf("%d", pipelineInputOptions.Stage), fmt.Sprintf("%d", pipelineInputOptions.Step), *pipelineInputOptions)
	} else {
		err = jenkinsClient.SubmitInputStep(ctx, namespace, pcName, "jenkins", pipe.Status.Jenkins.Build, fmt.Sprintf("%d", pipelineInputOptions.Stage), fmt.Sprintf("%d", pipelineInputOptions.Step), *pipelineInputOptions)
	}

	return &devops.PipelineInputResponse{
		StatusCode: 200, // TODO should fix this later
		Message:    "",  // TODO should fix this later
	}, err
}
