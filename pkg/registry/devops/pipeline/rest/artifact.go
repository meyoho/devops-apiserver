package rest

import (
	"context"
	"fmt"
	"net/http"
	"sort"

	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	"k8s.io/apimachinery/pkg/api/errors"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
)

// ArtiFactREST implements the artifact for a Pipeline
type ArtiFactREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

// NewArtiFactREST starts a new artifact store
func NewArtiFactREST(
	dependencyGetter dependency.Manager,
	roundTripper http.RoundTripper,
) rest.Storage {
	return &ArtiFactREST{
		Getter: dependencyGetter,

		RoundTripper: roundTripper,
	}
}

// ArtiFactREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&ArtiFactREST{})

// New creates a new ArtifactOption object
func (r *ArtiFactREST) New() runtime.Object {
	return &devops.ArtifactOption{}
}

// Get retrieves a runtime.Object that contains the atrifact info from a pipeline
func (r *ArtiFactREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	pipeline, _, jenkins, secret, err := getDependency(ctx, name, r.Getter)
	namespace := genericapirequest.NamespaceValue(ctx)

	if err != nil {
		return nil, err
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(jenkins.GetHostPort().Host),
		devopsclient.NewBasicAuth(k8s.GetDataBasicAuthFromSecret(secret).Username, k8s.GetDataBasicAuthFromSecret(secret).Password),
		devopsclient.NewTransport(r.RoundTripper),
	)

	jenkinsClient, err := factory.NewClient(
		jenkins.GetKindType(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get ContinuousIntegrationClient from Jenkins: %s, err: %v", jenkins.GetName(), err))
	}
	pcname := pipeline.Spec.PipelineConfig.Name

	runId := pipeline.Status.Jenkins.Build
	artifactlist, err := jenkinsClient.GetArtifactList(ctx, namespace, pcname, "jenkins", runId)

	if err != nil {
		return nil, errors.NewBadRequest(fmt.Sprintf("error getting response, error: %v", err))
	}
	result := &devops.PipelineArtifactList{}
	result.ArtifactList = append(result.ArtifactList,
		devops.PipelineArtifact{
			Name:         "pipeline.log",
			DownLoadAble: true,
			// can not get the size of pipeline.log so size is -1 meanings don't need show the size
			Size: -1,
		})
	// add 1 is for pipeline.log
	result.Length = len(artifactlist.ArtifactList) + 1
	for _, artifact := range artifactlist.ArtifactList {
		item := devops.PipelineArtifact{
			Name:         artifact.Name,
			Size:         int(artifact.Size),
			DownLoadAble: artifact.DownLoadAble,
		}
		result.ArtifactList = append(result.ArtifactList, item)
	}
	sort.Sort(result)
	return result, nil
}

// NewGetOptions creates a new options object
func (r *ArtiFactREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ArtifactOption{}, false, ""
}
