package rest

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"text/template"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

const (
	configMapKeyMarkdown   = "markdown"
	templateNameView       = "view"
	typeGraphicalTemplates = "graph"
)

var (
	// ErrNotFoundViewTemplate the template could not be found
	ErrNotFoundViewTemplate = errors.New("NotFoundViewTemplate")
)

type ViewREST struct {
	Manager         dependency.Manager
	RoundTripper    http.RoundTripper
	SystemNamespace string
}

func NewViewREST(manager dependency.Manager, roundTripper http.RoundTripper, ns string) rest.Storage {
	return &ViewREST{
		Manager:         manager,
		RoundTripper:    roundTripper,
		SystemNamespace: ns,
	}
}

var _ = rest.Getter(&ViewREST{})

func (viewREST *ViewREST) New() runtime.Object {
	return &v1alpha1.PipelineViewResult{}
}

func (v *ViewREST) Get(ctx context.Context, name string, options *metav1.GetOptions) (runtime.Object, error) {
	response := &v1alpha1.PipelineViewResult{}
	depItemList := v.Manager.Get(ctx, devops.TypePipeline, name)
	if err := depItemList.Validate(); err != nil {
		return response, err
	}
	pipelineconfig := &devops.PipelineConfig{}
	pipeline := &devops.Pipeline{}
	depItemList.GetInto(devops.TypePipelineConfig, pipelineconfig).GetInto(devops.TypePipeline, pipeline)

	builder := viewTemplateBuilder{
		Namespace:       pipeline.Namespace,
		Name:            name,
		Mgr:             v.Manager,
		PipelineConfig:  pipelineconfig,
		SystemNamespace: v.SystemNamespace,
	}
	template, err := builder.Build()
	if err != nil {
		return response, err
	}

	view, err := renderGoTemplate(template, pipeline)
	if err != nil {
		return response, err
	}
	response.Markdown = view
	return response, nil
}

func renderGoTemplate(goTemplate string, data *devops.Pipeline) (string, error) {
	builder := strings.Builder{}
	parse, err := template.New(templateNameView).Parse(goTemplate)
	if err != nil {
		return builder.String(), err
	}
	pipeline := &v1alpha1.Pipeline{}
	v1alpha1.Convert_devops_Pipeline_To_v1alpha1_Pipeline(data, pipeline, nil)
	pipelineBytes, err := json.Marshal(pipeline)
	if err != nil {
		return builder.String(), err
	}
	pipelineMap := make(map[string]interface{})
	err = json.Unmarshal(pipelineBytes, &pipelineMap)
	if err != nil {
		return builder.String(), err
	}
	err = parse.Execute(&builder, pipelineMap)
	if err != nil {
		return builder.String(), err
	}
	return builder.String(), nil
}

type viewTemplateBuilder struct {
	Kind            string
	Name            string
	Namespace       string
	SystemNamespace string
	Mgr             dependency.Manager
	PipelineConfig  *devops.PipelineConfig
	Funcs           []viewFunc
}

func (info viewTemplateBuilder) Build() (view string, err error) {
	funcs := info.Funcs
	if funcs == nil {
		funcs = viewRegistries
	}
	for _, f := range funcs {
		view, err = f(info)
		if err != nil && err != ErrNotFoundViewTemplate {
			break
		}
		if err == nil {
			break
		}
	}

	if err == ErrNotFoundViewTemplate {
		// use default
		info.Name = k8s.ConfigMapNameDevOpsDefaultViewTemplate
		info.Kind = devops.TypeConfigMap
		info.Namespace = info.SystemNamespace

		for _, f := range funcs {
			view, err = f(info)
			if err != nil && err != ErrNotFoundViewTemplate {
				break
			}
			if err == nil {
				break
			}
		}
	}

	return
}

type viewFunc func(info viewTemplateBuilder) (string, error)

var viewRegistries = make([]viewFunc, 0)

func byPipelineTemplate(info viewTemplateBuilder) (string, error) {

	name := info.Name
	namespace := info.Namespace
	kind := info.Kind

	if info.PipelineConfig != nil && info.PipelineConfig.Spec.Template.PipelineTemplate != nil {
		// use a graphical template
		if v, ok := info.PipelineConfig.Labels[devops.LabelTemplateName]; ok {
			name = v
		}
		if v, ok := info.PipelineConfig.Labels[devops.LabelTemplateKind]; ok {
			kind = v
		}
	}

	if info.PipelineConfig != nil {
		// use template
		ref := info.PipelineConfig.Spec.Template.PipelineTemplateSource.PipelineTemplateRef
		if ref.Name != "" {
			name = ref.Name
		}
		if ref.Namespace != "" {
			namespace = ref.Namespace
		}
		if ref.Kind != "" {
			kind = ref.Kind
		}
	}

	if name == "" || (kind != devops.TypeClusterPipelineTemplate && kind != devops.TypePipelineTemplate) {
		return "", ErrNotFoundViewTemplate
	}

	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), namespace)
	view := strings.Builder{}
	// template type needs to be prefixed and suffixed
	view.WriteString(`{{- $items := .status.information.items }}
{{- range $_, $item := $items }}

`)
	depItemList := info.Mgr.Get(ctx, kind, name)
	if err := depItemList.Validate(); err != nil {
		return "", err
	}
	pipelineTemplate := devops.NewPipelineTemplate(kind)
	depItemList.GetInto(kind, pipelineTemplate)

	// filtering tasks that already exist
	filterTask := make(map[string]struct{})
	for _, stage := range pipelineTemplate.GetPiplineTempateSpec().Stages {
		for _, task := range stage.Tasks {
			filterTaskKey := fmt.Sprintf("%s-%s", task.Kind, task.Name)
			if _, ok := filterTask[filterTaskKey]; ok {
				continue
			}
			filterTask[filterTaskKey] = struct{}{}
			ctx = genericapirequest.NewContext()
			depItemList := info.Mgr.Get(ctx, task.Kind, task.Name)
			if err := depItemList.Validate(); err != nil {
				return "", err
			}
			if task.Kind == devops.TypeClusterPipelineTaskTemplate {
				clusterPipelineTemplate := devops.ClusterPipelineTaskTemplate{}
				depItemList.GetInto(devops.TypeClusterPipelineTaskTemplate, &clusterPipelineTemplate)
				view.WriteString(clusterPipelineTemplate.Spec.View.Markdown)
			} else {
				pipelineTemplate := devops.PipelineTaskTemplate{}
				depItemList.GetInto(devops.TypePipelineTaskTemplate, &pipelineTemplate)
				view.WriteString(pipelineTemplate.Spec.View.Markdown)
			}
		}
	}
	view.WriteString(`{{- end }}`)
	return view.String(), nil
}

func byConfigMap(info viewTemplateBuilder) (string, error) {
	name := info.Name
	namespace := info.Namespace
	kind := info.Kind

	if info.PipelineConfig != nil && info.PipelineConfig.Spec.View.Reference.Name != "" {
		name = info.PipelineConfig.Spec.View.Reference.Name
		namespace = info.PipelineConfig.Spec.View.Reference.Namespace
		kind = info.PipelineConfig.Spec.View.Reference.Kind
	}

	if kind != devops.TypeConfigMap || name == "" {
		return "", ErrNotFoundViewTemplate
	}

	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), namespace)
	depItemList := info.Mgr.Get(ctx, kind, name)
	if err := depItemList.Validate(); err != nil {
		return "", err
	}
	configmap := corev1.ConfigMap{}
	depItemList.GetInto(kind, &configmap)
	if md, ok := configmap.Data[configMapKeyMarkdown]; ok {
		return md, nil
	}
	return "", ErrNotFoundViewTemplate
}

func init() {
	// it should be a sequential structure
	// first look to see if the referenced template resource exists.
	// if not, determine whether it is a template pipeline or a graphical pipeline.
	// finally use the default template
	viewRegistries = append(viewRegistries, byConfigMap)
	viewRegistries = append(viewRegistries, byPipelineTemplate)
}
