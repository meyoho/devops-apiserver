package rest

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"testing"

	mdependency "alauda.io/devops-apiserver/pkg/mock/dependency"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

func TestFetchStage(t *testing.T) {

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	roundTripper := mhttp.NewMockRoundTripper(ctrl)
	dependencyGetter := mdependency.NewMockManager(ctrl)
	provider := devops.NewAnnotationProvider(devops.UsedBaseDomain)

	storage := NewTaskREST(
		dependencyGetter,
		roundTripper,
		provider,
	)
	taskREST := storage.(*TaskREST)

	type TestCase struct {
		Name     string
		MockData func() (*devops.Pipeline, *devops.JenkinsBinding, *devops.Jenkins, error)
		URL      func() (*url.URL, error)
		Secret   func() (*corev1.Secret, error)

		Request  func(TestCase) *http.Request
		req      *http.Request
		Response func(TestCase) (*http.Response, error)
		Prepare  func(TestCase) (ctx context.Context, name string, opts runtime.Object)
		Expected runtime.Object
		Err      error
		Stop     chan struct{}
	}

	// basic shared data
	basicPipeline, basicJenkinsBinding, basicJenkins, basicSecret := getBasicTestResources()
	// baseNextStart := int64(0)
	// offset123 := int64(123)
	basicRequest, _ := http.NewRequest(http.MethodGet, "http://localhost:1/blue/rest/organizations/jenkins/pipelines/test/pipelines/test-pipeline/runs/1/nodes/2/steps/", nil)
	basicRequest.SetBasicAuth(
		string(basicSecret.Data[corev1.BasicAuthUsernameKey]),
		string(basicSecret.Data[corev1.BasicAuthPasswordKey]),
	)
	basicRequest.Host = ""

	basicResponse := &http.Response{
		Status:     "200 OK",
		StatusCode: http.StatusOK,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 1,
		Header:     http.Header{},
		Request:    nil,
		Body:       ioutil.NopCloser(bytes.NewBufferString(stepJSON)),
	}

	table := []TestCase{
		{
			Name: "case 0: all ok",
			MockData: func() (*devops.Pipeline, *devops.JenkinsBinding, *devops.Jenkins, error) {
				return basicPipeline, basicJenkinsBinding, basicJenkins, nil
			},
			Secret: func() (*corev1.Secret, error) {
				return basicSecret, nil
			},
			URL: func() (*url.URL, error) {
				return &url.URL{
					Scheme: "http",
					Host:   "localhost:1",
					Path:   "/blue/rest/organizations/jenkins/pipelines/test/pipelines/test-pipeline/runs/1/nodes/2/steps/",
				}, nil
			},
			Request: func(tc TestCase) *http.Request {
				url, _ := tc.URL()
				if url == nil {
					return nil
				}
				request, _ := http.NewRequest(http.MethodGet, url.RequestURI(), nil)
				request.URL = url
				secret, _ := tc.Secret()
				if secret != nil {
					request.SetBasicAuth(
						string(secret.Data[corev1.BasicAuthUsernameKey]),
						string(secret.Data[corev1.BasicAuthUsernameKey]),
					)
				}

				return request
			},
			Response: func(tc TestCase) (*http.Response, error) {
				basicResponse.Request = tc.Request(tc)
				return basicResponse, nil
			},
			Prepare: func(tc TestCase) (ctx context.Context, name string, opts runtime.Object) {
				// preparing request data
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "pipeline"
				opts = &devops.PipelineTaskOptions{Stage: 2}
				return
			},
			// should return according to our data
			Expected: &devops.PipelineTask{
				Tasks: stepsObj,
			},
			Err: nil,
		},
		{
			Name: "case 1: fetch stages",
			MockData: func() (*devops.Pipeline, *devops.JenkinsBinding, *devops.Jenkins, error) {
				return basicPipeline, basicJenkinsBinding, basicJenkins, nil
			},
			Secret: func() (*corev1.Secret, error) {
				return basicSecret, nil
			},
			URL: func() (*url.URL, error) {
				return &url.URL{
					Scheme: "http",
					Host:   "localhost:1",
					Path:   "/blue/rest/organizations/jenkins/pipelines/test/pipelines/test-pipeline/runs/1/nodes/",
				}, nil
			},
			Request: func(tc TestCase) *http.Request {
				url, _ := tc.URL()
				if url == nil {
					return nil
				}
				request, _ := http.NewRequest(http.MethodGet, url.RequestURI(), nil)
				request.URL = url
				secret, _ := tc.Secret()
				if secret != nil {
					request.SetBasicAuth(
						string(secret.Data[corev1.BasicAuthUsernameKey]),
						string(secret.Data[corev1.BasicAuthPasswordKey]),
					)
				}

				return request
			},
			Response: func(tc TestCase) (*http.Response, error) {
				basicResponse.Request = tc.Request(tc)
				basicResponse.Body = ioutil.NopCloser(bytes.NewBufferString(stagesJSON))
				return basicResponse, nil
			},
			Prepare: func(tc TestCase) (ctx context.Context, name string, opts runtime.Object) {
				// preparing request data
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "pipeline"
				opts = &devops.PipelineTaskOptions{Stage: 0}
				return
			},
			// should return according to our data
			Expected: &devops.PipelineTask{
				Tasks: stagesObj,
			},
			Err: nil,
		},
	}

	for i, test := range table {
		func() {
			test.Stop = make(chan struct{})
			defer close(test.Stop)
			t.Logf("Case: %d: %s", i, test.Name)

			ctx, name, opts := test.Prepare(test)

			// getting mock data
			pipeline, jenkinsBinding, jenkins, err := test.MockData()
			secret, _ := test.Secret()

			dependencyGetter.EXPECT().
				Get(ctx, devops.TypePipeline, name).
				Return(dependency.ItemList{
					&dependency.Item{
						Kind:     devops.TypePipeline,
						Object:   pipeline,
						IntoFunc: dependency.PipelineCopyInto,
					},
					&dependency.Item{
						Kind:     devops.TypeJenkinsBinding,
						Object:   jenkinsBinding,
						IntoFunc: dependency.JenkinsBindingCopyInto,
					},
					&dependency.Item{
						Kind:     devops.TypeJenkins,
						Object:   jenkins,
						IntoFunc: dependency.JenkinsCopyInto,
					}, &dependency.Item{
						Kind:     devops.TypeSecret,
						Object:   secret,
						IntoFunc: dependency.CoreV1SecretCopyInto,
					},
				})

			req := test.Request(test)
			if req != nil {
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewRequestMatcher(req)).
					Return(test.Response(test))
			}

			res, err := taskREST.Get(ctx, name, opts)
			if err == nil && test.Err != nil {
				t.Errorf("[%d] %s: Should fail but didnt: %v", i, test.Name, test.Err)
			} else if err != nil && test.Err == nil {
				t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
			}
			if !reflect.DeepEqual(res, test.Expected) {
				result := res.(*devops.PipelineTask)

				t.Errorf("[%d] %s: Should be equal but aren't: %v != %v", i, test.Name, result, test.Expected)
				t.Logf("Tasks: %v", result.Tasks)
				expected := test.Expected.(*devops.PipelineTask)
				t.Logf("Expected: %v", expected.Tasks)
			}
		}()

	}
}

const stepJSON = `[
    {
        "_class": "io.jenkins.blueocean.rest.impl.pipeline.PipelineStepImpl",
        "_links": {
            "self": {
                "_class": "io.jenkins.blueocean.rest.hal.Link",
                "href": "/blue/rest/organizations/jenkins/pipelines/daniel/pipelines/daniel-my-pipeline/runs/13/nodes/8/steps/9/"
            }
        },
        "actions": [
            {
                "_class": "org.jenkinsci.plugins.workflow.support.actions.LogActionImpl",
                "_links": {
                    "self": {
                        "_class": "io.jenkins.blueocean.rest.hal.Link",
                        "href": "/blue/rest/organizations/jenkins/pipelines/daniel/pipelines/daniel-my-pipeline/runs/13/nodes/8/steps/9/log/"
                    }
                },
                "urlName": "log"
            }
        ],
        "displayDescription": "this is my pipeline",
        "displayName": "Print Message",
        "durationInMillis": 51,
        "id": "9",
        "input": null,
        "result": "SUCCESS",
        "startTime": "2018-07-25T03:05:01.191+0000",
        "state": "FINISHED",
        "type": "STEP"
    }
]`

var stepsObj = []devops.PipelineBlueOceanTask{
	devops.PipelineBlueOceanTask{
		PipelineBlueOceanRef: devops.PipelineBlueOceanRef{
			Href: "/blue/rest/organizations/jenkins/pipelines/daniel/pipelines/daniel-my-pipeline/runs/13/nodes/8/steps/9/",
			ID:   "9",
			Type: "STEP",
		},
		Actions: []devops.PipelineBlueOceanRef{
			devops.PipelineBlueOceanRef{
				URLName: "log",
			},
		},
		DisplayDescription: "this is my pipeline",
		DisplayName:        "Print Message",
		DurationInMillis:   51,

		Input:     nil,
		Result:    "SUCCESS",
		StartTime: "2018-07-25T03:05:01.191+0000",
		State:     "FINISHED",
	},
}

const stagesJSON = `[
    {
        "_class": "io.jenkins.blueocean.rest.impl.pipeline.PipelineNodeImpl",
        "_links": {
            "self": {
                "_class": "io.jenkins.blueocean.rest.hal.Link",
                "href": "/blue/rest/organizations/jenkins/pipelines/daniel/pipelines/daniel-my-pipeline/runs/13/nodes/5/"
            },
            "actions": {
                "_class": "io.jenkins.blueocean.rest.hal.Link",
                "href": "/blue/rest/organizations/jenkins/pipelines/daniel/pipelines/daniel-my-pipeline/runs/13/nodes/5/actions/"
            },
            "steps": {
                "_class": "io.jenkins.blueocean.rest.hal.Link",
                "href": "/blue/rest/organizations/jenkins/pipelines/daniel/pipelines/daniel-my-pipeline/runs/13/nodes/5/steps/"
            }
        },
        "actions": [
            {
                "_class": "org.jenkinsci.plugins.workflow.support.actions.LogActionImpl",
                "_links": {
                    "self": {
                        "_class": "io.jenkins.blueocean.rest.hal.Link",
                        "href": "/blue/rest/organizations/jenkins/pipelines/daniel/pipelines/daniel-my-pipeline/runs/13/nodes/5/log/"
                    }
                },
                "urlName": "log"
            }
        ],
        "displayDescription": null,
        "displayName": "first",
        "durationInMillis": 72,
        "id": "5",
        "input": null,
        "result": "SUCCESS",
        "startTime": "2018-07-25T03:05:01.001+0000",
        "state": "FINISHED",
        "type": "STAGE",
        "causeOfBlockage": null,
        "edges": [
            {
                "_class": "io.jenkins.blueocean.rest.impl.pipeline.PipelineNodeImpl$EdgeImpl",
                "id": "8",
                "type": "STAGE"
            }
        ]
    }
]`

var stagesObj = []devops.PipelineBlueOceanTask{
	devops.PipelineBlueOceanTask{
		PipelineBlueOceanRef: devops.PipelineBlueOceanRef{
			ID:   "5",
			Type: "STAGE",
		},
		Actions: []devops.PipelineBlueOceanRef{
			devops.PipelineBlueOceanRef{
				URLName: "log",
			},
		},
		DisplayDescription: "",
		DisplayName:        "first",
		DurationInMillis:   72,

		Input:     nil,
		Result:    "SUCCESS",
		StartTime: "2018-07-25T03:05:01.001+0000",
		State:     "FINISHED",
		Edges: []devops.PipelineBlueOceanRef{
			devops.PipelineBlueOceanRef{
				ID:   "8",
				Type: "STAGE",
			},
		},
	},
}
