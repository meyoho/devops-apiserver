package rest

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"

	corev1 "k8s.io/api/core/v1"
)

// setupAuth will set the auth for http request base on a Secret
func setupAuth(request *http.Request, secret *corev1.Secret) (err error) {
	if secret != nil && secret.Data != nil && secret.Type == corev1.SecretTypeBasicAuth {
		username := strings.TrimSpace(string(secret.Data[corev1.BasicAuthUsernameKey]))
		apiToken := strings.TrimSpace(string(secret.Data[corev1.BasicAuthPasswordKey]))
		request.SetBasicAuth(username, apiToken)
	} else {
		err = fmt.Errorf("Secret [%v] is not suitable for http request", secret)
	}

	return
}

func getDependency(ctx context.Context, name string, getter dependency.Manager) (pipeline *devops.Pipeline,
	jenkinsBinding *devops.JenkinsBinding, jenkins *devops.Jenkins, secret *corev1.Secret, err error) {
	depItemList := getter.Get(ctx, devops.TypePipeline, name)
	if err = depItemList.Validate(); err != nil {
		return
	}

	pipeline = &devops.Pipeline{}
	jenkinsBinding = &devops.JenkinsBinding{}
	jenkins = &devops.Jenkins{}
	secret = &corev1.Secret{}

	depItemList.GetInto(devops.TypePipeline, pipeline).
		GetInto(devops.TypeJenkinsBinding, jenkinsBinding).
		GetInto(devops.TypeJenkins, jenkins).
		GetInto(devops.TypeSecret, secret)
	return
}

// belongToMultiBranch detects if the pipeline is belong to multi-branch pipeline
func belongToMultiBranch(pipeline *devops.Pipeline, provider devops.AnnotationProvider) (isMultiBranchPipeline bool) {
	_, isMultiBranchPipeline = pipeline.Annotations[provider.AnnotationsJenkinsMultiBranchName()]
	return
}

// getBranchName return the branch name only the pipeline belong to a mulit-branch pipeline
func getBranchName(pipeline *devops.Pipeline, provider devops.AnnotationProvider) (branchName string) {
	branchName, _ = pipeline.Annotations[provider.AnnotationsJenkinsMultiBranchName()]
	return
}
