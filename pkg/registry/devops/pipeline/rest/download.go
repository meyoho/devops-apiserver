package rest

import (
	"context"
	"encoding/base64"
	"fmt"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/registry/generic/download"

	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"

	"net/http"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
)

// DownLoadREST implements the download for a Pipeline
type DownLoadREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

// NewDownLoadREST represent the download for pipeline artifacts
func NewDownLoadREST(
	dependencyGetter dependency.Manager,
	roundTripper http.RoundTripper,
) rest.Storage {
	return &DownLoadREST{
		Getter:       dependencyGetter,
		RoundTripper: roundTripper,
	}
}

// DownLoadREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&DownLoadREST{})

// New creates a new ArtifactOption object
func (r *DownLoadREST) New() runtime.Object {
	return &devops.DownloadOption{}
}

// Get retrieves a runtime.Object that contains the download file info from a pipeline
func (r *DownLoadREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	downloadOpts, ok := opts.(*devops.DownloadOption)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opts))
	}

	pipeline, _, jenkins, secret, err := getDependency(ctx, name, r.Getter)
	namespace := genericapirequest.NamespaceValue(ctx)

	if err != nil {
		return nil, err
	}
	filename := downloadOpts.FileName
	downloadall := downloadOpts.All

	var username, password string
	if data, ok := secret.Data["username"]; ok {
		username = strings.TrimSpace(string(data))
	} else {
		return nil, fmt.Errorf("username data in secret is invaild，please check your secret")
	}

	if data, ok := secret.Data["password"]; ok {
		password = strings.TrimSpace(string(data))
	} else {
		return nil, fmt.Errorf("password data in secret is invaild，please check your secret")
	}

	authcode := base64.StdEncoding.EncodeToString([]byte(username + ":" + password))
	pcname := pipeline.Spec.PipelineConfig.Name
	runId := pipeline.Status.Jenkins.Build

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(jenkins.Spec.HTTP.Host),
		devopsclient.NewBearerToken(authcode),
		devopsclient.NewTransport(r.RoundTripper),
	)
	jenkinsclient, err := factory.NewClient(
		jenkins.GetKindType(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	result := &download.ArtifactFile{
		Project:     namespace,
		Name:        pcname,
		ID:          runId,
		FileName:    filename,
		DownloadAll: downloadall,
		Downloader:  jenkinsclient,
	}

	return result, nil
}

// NewGetOptions creates a new options object
func (r *DownLoadREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.DownloadOption{}, false, ""
}
