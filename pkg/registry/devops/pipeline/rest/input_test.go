package rest

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	urlutil "alauda.io/devops-apiserver/pkg/util/generic"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	mockregistry "alauda.io/devops-apiserver/pkg/mock/apiserver/registry"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	pipelineregistry "alauda.io/devops-apiserver/pkg/registry/devops/pipeline"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/watch"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	k8sinformer "k8s.io/client-go/informers"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	clienttesting "k8s.io/client-go/testing"
)

var _ = Describe("input request", func() {
	var (
		ctrl                *gomock.Controller
		pipelineStore       *mockregistry.MockStandardStorage
		jenkinsStore        *mockregistry.MockStandardStorage
		jenkinsBindingStore *mockregistry.MockStandardStorage
		roundTripper        *mhttp.MockRoundTripper
		inputREST           *InputREST
		ctx                 context.Context

		pipelineName string
		opts         *devops.PipelineInputOptions

		pipe           *devops.Pipeline
		jenkinsBinding *devops.JenkinsBinding
		jenkins        *devops.Jenkins
		secret         *corev1.Secret
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		pipelineStore = mockregistry.NewMockStandardStorage(ctrl)
		jenkinsStore = mockregistry.NewMockStandardStorage(ctrl)
		jenkinsBindingStore = mockregistry.NewMockStandardStorage(ctrl)
		roundTripper = mhttp.NewMockRoundTripper(ctrl)

		k8sclient := &k8sfake.Clientset{}
		informerFactory := k8sinformer.NewSharedInformerFactory(k8sclient, 5*time.Minute)
		provider := devops.AnnotationProvider{BaseDomain: devops.UsedBaseDomain}

		storage := NewInputREST(
			pipelineStore, jenkinsStore, jenkinsBindingStore,
			informerFactory.Core().V1().Secrets().Lister(),
			roundTripper, pipelineregistry.URLBuilderImp{},
			provider,
		)
		inputREST = storage.(*InputREST)

		pipe = &devops.Pipeline{
			ObjectMeta: metav1.ObjectMeta{
				Namespace:   "test",
				Name:        "pipeline",
				Annotations: map[string]string{},
			},
			Spec: devops.PipelineSpec{
				PipelineConfig: devops.LocalObjectReference{
					Name: "pipeline",
				},
				JenkinsBinding: devops.LocalObjectReference{
					Name: "jenkinsbinding",
				},
			},
			Status: devops.PipelineStatus{
				Jenkins: &devops.PipelineStatusJenkins{
					Build: "1",
				},
			},
		}
		jenkinsBinding = &devops.JenkinsBinding{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: "test",
			},
			Spec: devops.JenkinsBindingSpec{
				Jenkins: devops.JenkinsInstance{
					Name: "jenkins",
				},
				Account: devops.UserAccount{
					Secret: devops.SecretKeySetRef{
						SecretReference: corev1.SecretReference{
							Name: "secret",
						},
					},
				},
			},
		}
		jenkins = &devops.Jenkins{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: "test",
			},
			Spec: devops.JenkinsSpec{
				ToolSpec: devops.ToolSpec{
					HTTP: devops.HostPort{
						Host: "http://localhost:1",
					},
				},
			},
		}
		secret = &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "secret",
				Namespace: "test",
			},
			Type: corev1.SecretTypeBasicAuth,
			Data: map[string][]byte{
				"username": []byte("admin"),
				"password": []byte("123456"),
			},
		}

		k8sclient.AddReactor("get", "secret", func(action clienttesting.Action) (bool, runtime.Object, error) {
			return true, secret, nil
		})
		// Watch is a little bit special
		k8sclient.AddWatchReactor("secrets", func(action clienttesting.Action) (bool, watch.Interface, error) {
			fakeWatcher := watch.NewFake()
			if secret != nil {
				fakeWatcher.Add(secret)
			}

			return true, fakeWatcher, nil
		})
		k8sclient.AddReactor("list", "secrets", func(action clienttesting.Action) (bool, runtime.Object, error) {
			var list *corev1.SecretList
			if secret != nil {
				list = &corev1.SecretList{Items: []corev1.Secret{*secret}}
			}
			return true, list, nil
		})

		// starting informers to cache our mock data
		informerFactory.Start(make(chan struct{}))
		informerFactory.WaitForCacheSync(make(chan struct{}))
	})

	JustBeforeEach(func() {
		ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
		pipelineName = "pipeline"
		opts = &devops.PipelineInputOptions{
			Approve: true,
			InputID: "inputid",
			Parameters: []devops.PipelineParameter{
				{
					Name:  "name",
					Value: "bob",
				},
			},
		}
	})

	Describe("Handle the input request", func() {
		JustBeforeEach(func() {
			pipelineStore.EXPECT().
				Get(ctx, pipelineName, &metav1.GetOptions{ResourceVersion: "0"}).
				Return(pipe, nil)
			jenkinsBindingStore.EXPECT().
				Get(ctx, "jenkinsbinding", &metav1.GetOptions{ResourceVersion: "0"}).
				Return(jenkinsBinding, nil)
			jenkinsStore.EXPECT().
				Get(ctx, "jenkins", &metav1.GetOptions{ResourceVersion: "0"}).
				Return(jenkins, nil)
		})

		Context("Conditions are ready", func() {
			It("conditions check", func() {
				Expect(pipe).NotTo(Equal(nil))
				Expect(jenkinsBinding).NotTo(Equal(nil))
				Expect(jenkins).NotTo(Equal(nil))
				Expect(roundTripper).NotTo(BeNil())
			})
		})

		Context("Approve the request", func() {
			JustBeforeEach(func() {
				// all POST requests in Jenkins will need the crumb
				crumbRequest, _ := http.NewRequest(http.MethodGet, "http://localhost:1/crumbIssuer/api/json", nil)
				crumbRequest.SetBasicAuth("admin", "123456")

				// mocking our http call
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewVerboseRequestMatcher(crumbRequest)).
					Return(&http.Response{
						Status:     "200 OK",
						StatusCode: http.StatusOK,
						Header: map[string][]string{
							"Content-Type": []string{"application/json"},
						},
						Body: ioutil.NopCloser(bytes.NewBufferString(`
{"_class":"hudson.security.csrf.DefaultCrumbIssuer","crumb":"6e78c5725ea0c522bdcb787d548465aa","crumbRequestField":"Jenkins-Crumb"}
						`)),
					}, nil)

				// the real business request
				urlLoc := &url.URL{
					Host:   "localhost:1",
					Path:   "/blue/rest/organizations/jenkins/pipelines/test/pipelines/test-pipeline/runs/1/nodes/0/steps/0/",
					Scheme: "http",
				}
				payload := strings.NewReader(url.Values{"json": {`{"parameter": [{"name": "name", "value": "bob"}]}`}}.Encode())
				request, _ := http.NewRequest(http.MethodPost, urlLoc.String(), payload)
				request.Header.Add(urlutil.HeaderKeyContentType, urlutil.HeaderValueContentTypeForm)
				request.SetBasicAuth("admin", "123456")

				// mocking our http call
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewVerboseRequestMatcher(request)).
					Return(&http.Response{
						Status:     "200 OK",
						StatusCode: http.StatusOK,
						Body:       ioutil.NopCloser(bytes.NewBufferString("")),
					}, nil)

				crumbURLLoc := &url.URL{
					Host:   "localhost:1",
					Scheme: "http",
					Path:   "crumbIssuer/api/json",
				}
				crumbRequest, _ = http.NewRequest(http.MethodGet, crumbURLLoc.String(), nil)
				// crumbRequest.Header.Add(urlutil.HeaderKeyContentType, urlutil.HeaderValueContentTypeForm)
				crumbRequest.SetBasicAuth("admin", "123456")
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewVerboseRequestMatcher(crumbRequest)).
					Return(&http.Response{
						Status:     "200 OK",
						StatusCode: http.StatusOK,
						Body:       ioutil.NopCloser(bytes.NewBufferString(getCrumbFromJenkins())),
					}, nil)

				// setting annotation for the input request test
				pipe.ObjectMeta.Annotations = map[string]string{
					"alauda.io/jenkins-build-uri": `job/test/job/test-pipeline/1/`,
				}
			})

			It("should success", func() {
				_, err := inputREST.Create(ctx, pipelineName, opts, nil, nil)
				Expect(err).To(BeNil())
			})

			Context("no parameters for input request", func() {
				JustBeforeEach(func() {
					opts.Parameters = nil
				})

				It("should success", func() {
					_, err := inputREST.Create(ctx, pipelineName, opts, nil, nil)
					Expect(err).To(BeNil())
				})
			})

			Context("Multi-branch for the input request", func() {
				JustBeforeEach(func() {
					pipe.ObjectMeta.Annotations = map[string]string{
						devops.AnnotationsJenkinsMultiBranchName: "master",
					}

					// the real business request
					urlLoc := &url.URL{
						Host:   "localhost:1",
						Path:   "/blue/rest/organizations/jenkins/pipelines/test/pipelines/test-pipeline/branches/master/runs/1/nodes/0/steps/0/",
						Scheme: "http",
					}
					payload := strings.NewReader(url.Values{"json": {`{"parameter": [{"name": "name", "value": "bob"}]}`}}.Encode())
					request, _ := http.NewRequest(http.MethodPost, urlLoc.String(), payload)
					request.Header.Add(urlutil.HeaderKeyContentType, urlutil.HeaderValueContentTypeForm)
					request.SetBasicAuth("admin", "123456")

					// mocking our http call
					roundTripper.EXPECT().
						RoundTrip(mhttp.NewVerboseRequestMatcher(request)).
						Return(&http.Response{
							Status:     "200 OK",
							StatusCode: http.StatusOK,
							Body:       ioutil.NopCloser(bytes.NewBufferString("")),
						}, nil)
				})

				It("should success", func() {
					_, err := inputREST.Create(ctx, pipelineName, opts, nil, nil)
					Expect(err).To(BeNil())
				})
			})
		})
	})
})

func getCrumbFromJenkins() string {
	return `{
		"crumb": "",
		"crumbRequestField":""
	}`
}
