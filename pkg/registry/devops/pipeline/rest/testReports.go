package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	pipelineregistry "alauda.io/devops-apiserver/pkg/registry/devops/pipeline"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	glog "k8s.io/klog"

	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"

	corev1 "k8s.io/api/core/v1"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// TestReportREST implements the test reports endpoint for a Pipeline
type TestReportREST struct {
	Store               rest.StandardStorage
	JenkinsStore        rest.StandardStorage
	JenkinsBindingStore rest.StandardStorage
	SecretLister        corev1listers.SecretLister
	RoundTripper        http.RoundTripper
	Builder             pipelineregistry.URLBuilder
	Provider            devops.AnnotationProvider
}

// TestReportError represents the error when request test report
type TestReportError struct {
	Messageg string
	Code     int
	Errors   []string
}

// NewTestReportREST starts a new test report store
func NewTestReportREST(
	pipelineStore rest.StandardStorage,
	jenkinsStore rest.StandardStorage,
	jenkinsBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	roundTripper http.RoundTripper,
	builder pipelineregistry.URLBuilder,
	provider devops.AnnotationProvider,
) rest.Storage {
	return &TestReportREST{
		Store:               pipelineStore,
		JenkinsStore:        jenkinsStore,
		JenkinsBindingStore: jenkinsBindingStore,
		SecretLister:        secretLister,
		RoundTripper:        roundTripper,
		Builder:             builder,
		Provider:            provider,
	}
}

// TestReportREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&TestReportREST{})

// New creates a new PipelineTestReportOptions object
func (r *TestReportREST) New() runtime.Object {
	return &devops.PipelineTestReportOptions{}
}

// ProducesMIMETypes TestReportREST implements StorageMetadata
func (r *TestReportREST) ProducesMIMETypes(verb string) []string {
	// the "produces" section for pipelines/{name}/testreports
	return []string{
		"application/json",
	}
}

// ProducesObject implements StorageMetadata, return PipelineTestReport as the generating object
func (r *TestReportREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.PipelineTestReport{}
}

// Get retrieves a runtime.Object that contains the test report from a pipeline
func (r *TestReportREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	testReportOpts, ok := opts.(*devops.PipelineTestReportOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opts))
	}
	namespace := genericapirequest.NamespaceValue(ctx)
	glog.V(3).Infof(
		"got pipeline test report options: %v, namespace: %v",
		testReportOpts,
		namespace,
	)

	var (
		err            error
		secret         *corev1.Secret
		jenkinsClient  devopsclient.Interface
		pipe           *devops.Pipeline
		jenkinsBinding *devops.JenkinsBinding
		jenkins        *devops.Jenkins
		testReport     *devops.PipelineTestReport
	)

	// fetch pipeline dependencies
	if pipe, jenkinsBinding, jenkins, err = r.Builder.GetPipelineDependencies(r.Store, r.JenkinsStore, r.JenkinsBindingStore, ctx, name); err != nil {
		glog.Errorf("Error fetching pipeline dependencies when process the pipeline test report %s/%s: %v", namespace, name, err)
		return nil, err
	}
	if pipe.Status.Jenkins == nil || pipe.Status.Jenkins.Build == "" {
		glog.V(3).Infof("Pipeline has no build ID %s/%s: %v", namespace, name, pipe.Status)
		return &devops.PipelineTestReport{
			Items: []devops.PipelineTestReportItem{},
		}, nil
	}

	userAccount := jenkinsBinding.Spec.Account
	if secret, err = r.Builder.GetSecret(r.SecretLister, userAccount, namespace); err != nil {
		glog.Errorf("error fetching secret %s/%s: %v", namespace, name, err)
		return nil, err
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(jenkins.GetHostPort().Host),
		devopsclient.NewBasicAuth(k8s.GetDataBasicAuthFromSecret(secret).Username, k8s.GetDataBasicAuthFromSecret(secret).Password),
		devopsclient.NewTransport(r.RoundTripper),
	)
	if jenkinsClient, err = factory.NewClient(jenkins.GetKindType(), "", clientOpts); err != nil {
		glog.Errorf("Got error when create JenkinsClient for pipeline test report: %v", err)
		return nil, err
	}

	reporter := PipelineTestReporter{
		JenkinsClient: jenkinsClient,
		Namespace:     namespace,
		PipelineName:  pipe.Spec.PipelineConfig.Name,
		BranchName:    getBranchName(pipe, r.Provider),
		Organization:  "jenkins",
		BuildNum:      pipe.Status.Jenkins.Build,
		Start:         testReportOpts.Start,
		Limit:         testReportOpts.Limit,
	}

	glog.V(5).Infof("Pipeline reporter %#v", reporter)
	if belongToMultiBranch(pipe, r.Provider) {
		testReport, err = reporter.getTestReportFromMultiBranch(ctx)
	} else {
		testReport, err = reporter.getTestReport(ctx)
	}
	return testReport, err
}

// PipelineTestReporter for getting test report from pipeline
type PipelineTestReporter struct {
	JenkinsClient devopsclient.Interface

	Namespace    string
	PipelineName string
	BranchName   string
	Organization string

	BuildNum string

	Start int64
	Limit int64
}

// getTestReport fetch the test report from a regular pipeline
func (r *PipelineTestReporter) getTestReport(ctx context.Context) (testReport *devops.PipelineTestReport, err error) {
	testReport = &devops.PipelineTestReport{}

	var summary *devops.PipelineTestReportSummary
	if summary, err = r.JenkinsClient.GetTestReportSummary(ctx, r.Namespace, r.PipelineName,
		r.Organization, r.BuildNum); err == nil {
		testReport.Summary = summary

		glog.V(5).Infof("test report summary of pipeline %#v", testReport.Summary)

		// Just skip below logic if there're no test reports
		if testReport.Summary == nil || testReport.Summary.Total == 0 {
			return
		}

		reportItems := make([]devops.PipelineTestReportItem, 0)
		if items, err := r.JenkinsClient.GetTestReports(ctx, r.Namespace, r.PipelineName, r.Organization,
			r.BuildNum, "FAILED", "UNKNOWN", "", r.Start, r.Limit); err == nil {
			reportItems = append(reportItems, items.Items...)
		} else {
			return testReport, err
		}
		if items, err := r.JenkinsClient.GetTestReports(ctx, r.Namespace, r.PipelineName, r.Organization,
			r.BuildNum, "FAILED", "REGRESSION", "", r.Start, r.Limit); err == nil {
			reportItems = append(reportItems, items.Items...)
		} else {
			return testReport, err
		}
		if items, err := r.JenkinsClient.GetTestReports(ctx, r.Namespace, r.PipelineName, r.Organization,
			r.BuildNum, "SKIPPED", "UNKNOWN", "", r.Start, r.Limit); err == nil {
			reportItems = append(reportItems, items.Items...)
		} else {
			return testReport, err
		}

		if items, err := r.JenkinsClient.GetTestReports(ctx, r.Namespace, r.PipelineName, r.Organization,
			r.BuildNum, "PASSED", "FIXED", "", r.Start, r.Limit); err == nil {
			reportItems = append(reportItems, items.Items...)
		} else {
			return testReport, err
		}

		if items, err := r.JenkinsClient.GetTestReports(ctx, r.Namespace, r.PipelineName, r.Organization,
			r.BuildNum, "PASSED", "UNKNOWN", "", r.Start, r.Limit); err == nil {
			reportItems = append(reportItems, items.Items...)
		} else {
			return testReport, err
		}

		testReport.Items = reportItems
	}
	return
}

// getTestReportFromMultiBranch fetch the test report from a multi-branch pipeline
func (r *PipelineTestReporter) getTestReportFromMultiBranch(ctx context.Context) (testReport *devops.PipelineTestReport, err error) {
	testReport = &devops.PipelineTestReport{}

	var summary *devops.PipelineTestReportSummary
	if summary, err = r.JenkinsClient.GetBranchTestReportSummary(ctx, r.Namespace, r.PipelineName, r.BranchName,
		r.Organization, r.BuildNum); err == nil {
		testReport.Summary = summary

		glog.V(5).Infof("test report summary of multi-branch pipeline %#v", testReport.Summary)
		// Just skip below logic if there're no test reports
		if testReport.Summary == nil || testReport.Summary.Total == 0 {
			return
		}

		reportItems := make([]devops.PipelineTestReportItem, 0)
		if items, err := r.JenkinsClient.GetBranchTestReports(ctx, r.Namespace, r.PipelineName, r.BranchName, r.Organization,
			r.BuildNum, "FAILED", "UNKNOWN", "", r.Start, r.Limit); err == nil {
			reportItems = append(reportItems, items.Items...)
		} else {
			return testReport, err
		}

		if items, err := r.JenkinsClient.GetBranchTestReports(ctx, r.Namespace, r.PipelineName, r.BranchName, r.Organization,
			r.BuildNum, "FAILED", "REGRESSION", "", r.Start, r.Limit); err == nil {
			reportItems = append(reportItems, items.Items...)
		} else {
			return testReport, err
		}
		if items, err := r.JenkinsClient.GetBranchTestReports(ctx, r.Namespace, r.PipelineName, r.BranchName, r.Organization,
			r.BuildNum, "SKIPPED", "UNKNOWN", "", r.Start, r.Limit); err == nil {
			reportItems = append(reportItems, items.Items...)
		} else {
			return testReport, err
		}

		if items, err := r.JenkinsClient.GetBranchTestReports(ctx, r.Namespace, r.PipelineName, r.BranchName, r.Organization,
			r.BuildNum, "PASSED", "FIXED", "", r.Start, r.Limit); err == nil {
			reportItems = append(reportItems, items.Items...)
		} else {
			return testReport, err
		}

		if items, err := r.JenkinsClient.GetBranchTestReports(ctx, r.Namespace, r.PipelineName, r.BranchName, r.Organization,
			r.BuildNum, "PASSED", "UNKNOWN", "", r.Start, r.Limit); err == nil {
			reportItems = append(reportItems, items.Items...)
		} else {
			return testReport, err
		}

		testReport.Items = reportItems
	}
	return
}

// NewGetOptions creates a new options object
func (r *TestReportREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.PipelineTestReportOptions{}, false, ""
}
