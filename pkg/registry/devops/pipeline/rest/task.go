package rest

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	glog "k8s.io/klog"

	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

// TaskREST implements the log endpoint for a Pod
type TaskREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
	Provider     devops.AnnotationProvider
}

// NewTaskREST starts a new log store
func NewTaskREST(
	dependencyGetter dependency.Manager, roundTripper http.RoundTripper, provider devops.AnnotationProvider) rest.Storage {
	return &TaskREST{
		Getter:       dependencyGetter,
		RoundTripper: roundTripper,
		Provider:     provider,
	}
}

// TaskREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&TaskREST{})

// New creates a new PipelineLogOptions object
func (r *TaskREST) New() runtime.Object {
	return &devops.Pipeline{}
}

// ProducesMIMETypes LogREST implements StorageMetadata
func (r *TaskREST) ProducesMIMETypes(verb string) []string {
	// the "produces" section for pipelines/{name}/log
	return []string{
		"application/json",
	}
}

// ProducesObject implements StorageMetadata, return PipelineLog as the generating object
func (r *TaskREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.PipelineTask{}
}

// Get retrieves a runtime.Object that will stream the contents of the pod log
func (r *TaskREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	pipelineTaskOpts, ok := opts.(*devops.PipelineTaskOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opts))
	}
	namespace := genericapirequest.NamespaceValue(ctx)
	glog.V(3).Infof(
		"got pipeline task options: stage: %d namespace: %v",
		pipelineTaskOpts.Stage,
		namespace,
	)

	pipeline, _, jenkins, secret, err := getDependency(ctx, name, r.Getter)
	if err != nil {
		glog.Errorf("Error fetching pipeline dependencies %s/%s: %v", namespace, name, err)
		return nil, err
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(jenkins.GetHostPort().Host),
		devopsclient.NewBasicAuth(k8s.GetDataBasicAuthFromSecret(secret).Username, k8s.GetDataBasicAuthFromSecret(secret).Password),
		devopsclient.NewTransport(r.RoundTripper),
	)
	jenkinsClient, err := factory.NewClient(
		jenkins.GetKindType(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get ContinuousIntegrationClient from Jenkins: %s, err: %v", jenkins.GetName(), err))
	}

	branchName, isBranchPipeline := pipeline.Annotations[r.Provider.AnnotationsJenkinsMultiBranchName()]
	build := pipeline.Status.Jenkins.Build

	var tasks []devops.PipelineBlueOceanTask
	if isBranchPipeline {
		if pipelineTaskOpts.Stage > 0 {
			tasks, err = jenkinsClient.GetBranchJobSteps(ctx, namespace, pipeline.Spec.PipelineConfig.Name, branchName, DefaultOrganization, build, strconv.FormatInt(pipelineTaskOpts.Stage, 10))
		} else {
			tasks, err = jenkinsClient.GetBranchJobNodes(ctx, namespace, pipeline.Spec.PipelineConfig.Name, branchName, DefaultOrganization, build)
		}
	} else {
		if pipelineTaskOpts.Stage > 0 {
			tasks, err = jenkinsClient.GetJobSteps(ctx, namespace, pipeline.Spec.PipelineConfig.Name, DefaultOrganization, build, strconv.FormatInt(pipelineTaskOpts.Stage, 10))
		} else {
			tasks, err = jenkinsClient.GetJobNodes(ctx, namespace, pipeline.Spec.PipelineConfig.Name, DefaultOrganization, build)
		}
	}

	if tasks == nil {
		tasks = []devops.PipelineBlueOceanTask{}
	}

	return &devops.PipelineTask{
		Tasks: tasks,
	}, err
}

// NewGetOptions creates a new options object
func (r *TaskREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.PipelineTaskOptions{}, false, ""
}
