package rest

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	mockregistry "alauda.io/devops-apiserver/pkg/mock/apiserver/registry"
	mockdevopsregistry "alauda.io/devops-apiserver/pkg/mock/devops/registry"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	corev1 "k8s.io/api/core/v1"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	k8sinformer "k8s.io/client-go/informers"
	k8sfake "k8s.io/client-go/kubernetes/fake"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Pipeline test report", func() {
	var (
		ctrl                *gomock.Controller
		pipelineStore       *mockregistry.MockStandardStorage
		jenkinsStore        *mockregistry.MockStandardStorage
		jenkinsBindingStore *mockregistry.MockStandardStorage
		roundTripper        *mhttp.MockRoundTripper
		urlBuilder          *mockdevopsregistry.MockURLBuilder

		testReportREST *TestReportREST

		namespace    string
		pipelineName string
		ctx          context.Context
		opts         *devops.PipelineTestReportOptions
	)

	BeforeEach(func() {
		ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
		ctrl = gomock.NewController(GinkgoT())
		pipelineStore = mockregistry.NewMockStandardStorage(ctrl)
		jenkinsStore = mockregistry.NewMockStandardStorage(ctrl)
		jenkinsBindingStore = mockregistry.NewMockStandardStorage(ctrl)
		roundTripper = mhttp.NewMockRoundTripper(ctrl)
		urlBuilder = mockdevopsregistry.NewMockURLBuilder(ctrl)

		provider := devops.NewAnnotationProvider(devops.UsedBaseDomain)
		testReportRESTStorage := NewTestReportREST(
			pipelineStore,
			jenkinsStore,
			jenkinsBindingStore,
			nil,
			roundTripper,
			urlBuilder,
			provider,
		)
		testReportREST = testReportRESTStorage.(*TestReportREST)

		namespace = "test"
		pipelineName = "pipeline"

		basicPipeline, basicJenkinsBinding, basicJenkins, basicSecret := getBasicTestResources()
		urlBuilder.EXPECT().
			GetPipelineDependencies(
				pipelineStore,
				jenkinsStore,
				jenkinsBindingStore,
				ctx,
				pipelineName,
			).Return(basicPipeline, basicJenkinsBinding, basicJenkins, nil)

		// mocking Informer and Lister for secrets
		k8sclient := &k8sfake.Clientset{}
		informerFactory := k8sinformer.NewSharedInformerFactory(k8sclient, 5*time.Minute)
		testReportREST.SecretLister = informerFactory.Core().V1().Secrets().Lister()
		urlBuilder.EXPECT().
			GetSecret(
				testReportREST.SecretLister,
				basicJenkinsBinding.Spec.Account,
				namespace,
			).Return(basicSecret, nil)

		opts = &devops.PipelineTestReportOptions{
			Start: 0,
			Limit: 100,
		}

		prepareTestReport := JenkinsTestReport{
			Namespace:    namespace,
			Name:         basicPipeline.Spec.PipelineConfig.Name,
			Build:        basicPipeline.Status.Jenkins.Build,
			RoundTripper: roundTripper,
			UserName:     string(basicSecret.Data[corev1.BasicAuthUsernameKey]),
			Password:     string(basicSecret.Data[corev1.BasicAuthPasswordKey]),
		}
		prepareTestReport.prepareFailedReport()
		prepareTestReport.prepareFixedReport()
		prepareTestReport.preparePassedReport()
		prepareTestReport.prepareRegeressionReport()
		prepareTestReport.prepareSkipedReport()

		summaryResponse := &http.Response{
			StatusCode: http.StatusOK,
			Body:       ioutil.NopCloser(bytes.NewBufferString(getTestReportSummaryFromJenkins())),
		}
		summaryRequest, _ := http.NewRequest(http.MethodGet,
			"http://localhost:1/blue/rest/organizations/jenkins/pipelines/test/pipelines/test-pipeline/runs/1/blueTestSummary/",
			nil)
		roundTripper.EXPECT().
			RoundTrip(mhttp.NewVerboseRequestMatcher(summaryRequest)).
			Return(summaryResponse, nil)
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	Context("basic test", func() {
		It("should success", func() {
			result, err := testReportREST.Get(ctx, pipelineName, opts)
			Expect(err).To(BeNil())
			Expect(result).NotTo(BeNil())

			report := result.(*devops.PipelineTestReport)

			Expect(len(report.Items)).To(BeEquivalentTo(5))
			Expect(report.Items[0].Name).To(BeEquivalentTo("test-name"))

			// DEVOPS-3252, make sure all data can be parsed
			Expect(report.Summary.Failed).To(Equal(int64(1)))
			Expect(report.Summary.ExistingFailed).To(Equal(int64(1)))
			Expect(report.Summary.Fixed).To(Equal(int64(1)))
			Expect(report.Summary.Passed).To(Equal(int64(1)))
			Expect(report.Summary.Regressions).To(Equal(int64(1)))
			Expect(report.Summary.Passed).To(Equal(int64(1)))
			Expect(report.Summary.Total).To(Equal(int64(5)))
		})
	})
})

func getTestReportFromJenkins() string {
	return `[{
		"age": 1,
		"name": "test-name"
	}]`
}

// JenkinsTestReport is only use for test convenience
type JenkinsTestReport struct {
	Namespace    string
	Name         string
	Build        string
	UserName     string
	Password     string
	RoundTripper *mhttp.MockRoundTripper
}

func (j *JenkinsTestReport) prepareRegeressionReport() {
	j.prepareReportRequest("state=REGRESSION")
}

func (j *JenkinsTestReport) prepareFailedReport() {
	j.prepareReportRequest("state=%21REGRESSION&status=FAILED")
}

func (j *JenkinsTestReport) prepareSkipedReport() {
	j.prepareReportRequest("status=SKIPPED")
}

func (j *JenkinsTestReport) prepareFixedReport() {
	j.prepareReportRequest("state=FIXED")
}

func (j *JenkinsTestReport) preparePassedReport() {
	j.prepareReportRequest("status=PASSED")
}

func (j *JenkinsTestReport) prepareReportRequest(urlQuery string) {
	bluePath := fmt.Sprintf("/blue/rest/organizations/jenkins/pipelines/%s/pipelines/%s-%s/runs/%s/tests/?limit=100&start=0&%s",
		j.Namespace, j.Namespace, j.Name, j.Build, urlQuery)

	request, _ := http.NewRequest(http.MethodGet,
		fmt.Sprintf("http://localhost:1%s", bluePath),
		nil)
	request.SetBasicAuth(j.UserName, j.Password)
	request.Host = ""

	response := &http.Response{
		Status:     "200 OK",
		StatusCode: http.StatusOK,
		Header:     http.Header{},
		Body:       ioutil.NopCloser(bytes.NewBufferString(getTestReportFromJenkins())),
	}
	j.RoundTripper.EXPECT().
		RoundTrip(mhttp.NewVerboseRequestMatcher(request)).
		Return(response, nil)
}

func getTestReportSummaryFromJenkins() string {
	return `{
"existingFailed": 1,
"failed": 1,
"fixed": 1,
"passed": 1,
"regressions": 1,
"skipped": 1,
"total": 5
}`
}
