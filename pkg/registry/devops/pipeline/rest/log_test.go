package rest

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mdependency "alauda.io/devops-apiserver/pkg/mock/dependency"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"bytes"
	"context"
	"fmt"
	"github.com/golang/mock/gomock"
	"io/ioutil"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"net/http"
	"reflect"
	"testing"
)

func TestFetchLogs(t *testing.T) {

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	roundTripper := mhttp.NewMockRoundTripper(ctrl)
	dependencyGetter := mdependency.NewMockManager(ctrl)
	provider := devops.NewAnnotationProvider(devops.UsedBaseDomain)

	storage := NewLogREST(
		dependencyGetter,
		roundTripper,
		provider,
	)
	logREST := storage.(*LogREST)

	type TestCase struct {
		Name           string
		Prepare        func(TestCase) (ctx context.Context, name string, opts runtime.Object)
		Run            func(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error)
		Expected       runtime.Object
		Pipeline       func() (*devops.Pipeline, error)
		JenkinsBinding func() (*devops.JenkinsBinding, error)
		Jenkins        func() (*devops.Jenkins, error)
		Secret         func() (*corev1.Secret, error)
		Err            error
		Stop           chan struct{}
	}

	// basic shared data
	basicPipeline, basicJenkinsBinding, basicJenkins, basicSecret := getBasicTestResources()
	baseNextStart := int64(0)
	offset123 := int64(123)
	table := []TestCase{
		{
			Name: "case 0: all ok, pipeline without log url",
			Pipeline: func() (*devops.Pipeline, error) {
				return basicPipeline, nil
			},
			JenkinsBinding: func() (*devops.JenkinsBinding, error) {
				return basicJenkinsBinding, nil
			},
			Jenkins: func() (*devops.Jenkins, error) {
				return basicJenkins, nil
			},
			Secret: func() (*corev1.Secret, error) {
				return basicSecret, nil
			},
			Prepare: func(tc TestCase) (ctx context.Context, name string, opts runtime.Object) {
				// preparing request data
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "pipeline"
				opts = &devops.PipelineLogOptions{Start: 0}

				// creating our http request
				request, _ := http.NewRequest(http.MethodGet, "http://localhost:1/job/test/job/test-pipeline/1/logText/progressiveText?start=0", nil)
				request.SetBasicAuth("admin", "123456")
				request.Host = ""
				request.Header.Add("Accept", "*/*")

				// mocking our http call
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewRequestMatcher(request)).
					// this is our http response
					Return(&http.Response{
						Status:     "200 OK",
						StatusCode: http.StatusOK,
						Proto:      "HTTP/1.1",
						ProtoMajor: 1,
						ProtoMinor: 0,
						Header: http.Header{
							"X-More-Data":  []string{"false"},
							"Content-Type": []string{"text/plain"},
						},
						Request: request,
						Body:    ioutil.NopCloser(bytes.NewBufferString("MY LOG")),
					}, nil)
				return
			},
			Run: func(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
				// run the method
				return logREST.Get(ctx, name, opts)
			},
			// should return according to our data
			Expected: &devops.PipelineLog{
				HasMore:   false,
				Text:      "MY LOG",
				NextStart: &baseNextStart,
			},
			Err: nil,
		},
		{
			Name: "case 1: hostname with extra //, pipeline WITH log url",
			Pipeline: func() (*devops.Pipeline, error) {
				newPipeline := basicPipeline.DeepCopy()
				newPipeline.ObjectMeta.Annotations = map[string]string{
					devops.AnnotationsJenkinsProgressiveLogURI: "/job/test/job/test-pipeline/1/logText/progressiveText",
				}
				return newPipeline, nil
			},
			JenkinsBinding: func() (*devops.JenkinsBinding, error) {
				return basicJenkinsBinding, nil
			},
			Jenkins: func() (*devops.Jenkins, error) {
				newJenkins := basicJenkins.DeepCopy()
				newJenkins.Spec.HTTP.Host = "http://localhost:1"
				return newJenkins, nil
			},
			Secret: func() (*corev1.Secret, error) {
				return basicSecret, nil
			},
			Prepare: func(tc TestCase) (ctx context.Context, name string, opts runtime.Object) {
				// preparing request data
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "pipeline"
				opts = &devops.PipelineLogOptions{Start: 0}

				// creating our http request
				request, _ := http.NewRequest(http.MethodGet, "http://localhost:1/job/test/job/test-pipeline/1/logText/progressiveText?start=0", nil)
				request.SetBasicAuth("admin", "123456")
				request.Host = ""
				request.Header.Add("Accept", "*/*")

				// mocking our http call
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewRequestMatcher(request)).
					// this is our http response
					Return(&http.Response{
						Status:     "200 OK",
						StatusCode: http.StatusOK,
						Proto:      "HTTP/1.1",
						ProtoMajor: 1,
						ProtoMinor: 0,
						Header: http.Header{
							// Jenkins has more response header
							"X-More-Data": []string{"false"},
							// Jenkins next offset response header
							"X-Text-Size":  []string{"123"},
							"Content-Type": []string{"text/plain"},
						},
						Request: request,
						Body:    ioutil.NopCloser(bytes.NewBufferString("MY LOG")),
					}, nil)

				return
			},
			Run: func(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
				// run the method
				return logREST.Get(ctx, name, opts)
			},
			// should return according to our data
			Expected: &devops.PipelineLog{
				HasMore:   false,
				Text:      "MY LOG",
				NextStart: &offset123,
			},
			Err: nil,
		},
		{
			Name: "case 2: with start and more",
			Pipeline: func() (*devops.Pipeline, error) {
				return basicPipeline, nil
			},
			JenkinsBinding: func() (*devops.JenkinsBinding, error) {
				return basicJenkinsBinding, nil
			},
			Jenkins: func() (*devops.Jenkins, error) {
				newJenkins := basicJenkins.DeepCopy()
				newJenkins.Spec.HTTP.Host = "http://localhost:1/"
				return newJenkins, nil
			},
			Secret: func() (*corev1.Secret, error) {
				return basicSecret, nil
			},
			Prepare: func(tc TestCase) (ctx context.Context, name string, opts runtime.Object) {
				// preparing request data
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "pipeline"
				opts = &devops.PipelineLogOptions{Start: 135}

				// creating our http request
				request, _ := http.NewRequest(http.MethodGet, "http://localhost:1/job/test/job/test-pipeline/1/logText/progressiveText?start=135", nil)
				request.SetBasicAuth("admin", "123456")
				request.Host = ""
				request.Header.Add("Accept", "*/*")

				// mocking our http call
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewRequestMatcher(request)).
					// this is our http response
					Return(&http.Response{
						Status:     "200 OK",
						StatusCode: http.StatusOK,
						Proto:      "HTTP/1.1",
						ProtoMajor: 1,
						ProtoMinor: 0,
						Header: http.Header{
							"X-More-Data":  []string{"true"},
							"Content-Type": []string{"text/plain"},
						},
						Request: request,
						Body:    ioutil.NopCloser(bytes.NewBufferString("MORE LOG")),
					}, nil)

				return
			},
			Run: func(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
				// run the method
				return logREST.Get(ctx, name, opts)
			},
			// should return according to our data
			Expected: &devops.PipelineLog{
				HasMore: true,
				Text:    "MORE LOG",

				NextStart: &baseNextStart,
			},
			Err: nil,
		},
		{
			Name: "case 3: with stage",
			Pipeline: func() (*devops.Pipeline, error) {
				return basicPipeline, nil
			},
			JenkinsBinding: func() (*devops.JenkinsBinding, error) {
				return basicJenkinsBinding, nil
			},
			Jenkins: func() (*devops.Jenkins, error) {
				newJenkins := basicJenkins.DeepCopy()
				newJenkins.Spec.HTTP.Host = "http://localhost:1/"
				return newJenkins, nil
			},
			Secret: func() (*corev1.Secret, error) {
				return basicSecret, nil
			},
			Prepare: func(tc TestCase) (ctx context.Context, name string, opts runtime.Object) {
				// preparing request data
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "pipeline"
				opts = &devops.PipelineLogOptions{Start: 10, Stage: 6}

				// creating our http request
				request, _ := http.NewRequest(
					http.MethodGet,
					"http://localhost:1/blue/rest/organizations/jenkins/pipelines"+ //basic blue ocean rest log address
						"/test/pipelines/test-pipeline"+ // /namespace/pipelines/namespace-pipelineconfig/
						"/runs/1/nodes/6/log/?start=10", // /runs/build number/nodes(stage)/stage id/logs
					nil,
				)
				request.SetBasicAuth("admin", "123456")
				request.Host = ""
				request.Header.Add("Accept", "*/*")

				// mocking our http call
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewRequestMatcher(request)).
					// this is our http response
					Return(&http.Response{
						Status:     "200 OK",
						StatusCode: http.StatusOK,
						Proto:      "HTTP/1.1",
						ProtoMajor: 1,
						ProtoMinor: 0,
						Header: http.Header{
							"X-More-Data":  []string{"true"},
							"Content-Type": []string{"text/plain"},
						},
						Request: request,
						Body:    ioutil.NopCloser(bytes.NewBufferString("MORE LOG")),
					}, nil)

				return
			},
			Run: func(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
				// run the method
				return logREST.Get(ctx, name, opts)
			},
			// should return according to our data
			Expected: &devops.PipelineLog{
				HasMore:   true,
				Text:      "MORE LOG",
				NextStart: &baseNextStart,
			},
			Err: nil,
		},
		{
			Name: "case 4: with stage and step",
			Pipeline: func() (*devops.Pipeline, error) {
				return basicPipeline, nil
			},
			JenkinsBinding: func() (*devops.JenkinsBinding, error) {
				return basicJenkinsBinding, nil
			},
			Jenkins: func() (*devops.Jenkins, error) {
				newJenkins := basicJenkins.DeepCopy()
				newJenkins.Spec.HTTP.Host = "http://localhost:1/"
				return newJenkins, nil
			},
			Secret: func() (*corev1.Secret, error) {
				return basicSecret, nil
			},
			Prepare: func(tc TestCase) (ctx context.Context, name string, opts runtime.Object) {
				// preparing request data
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "pipeline"
				opts = &devops.PipelineLogOptions{Start: 33, Stage: 6, Step: 11}

				// creating our http request
				request, _ := http.NewRequest(
					http.MethodGet,
					"http://localhost:1/blue/rest/organizations/jenkins/pipelines"+ //basic blue ocean rest log address
						"/test/pipelines/test-pipeline"+ // /namespace/pipelines/namespace-pipelineconfig/
						"/runs/1/nodes/6/steps/11/log/?start=33", // /runs/build number/nodes(stage)/stage id/logs
					nil,
				)
				request.SetBasicAuth("admin", "123456")
				request.Host = ""
				request.Header.Add("Accept", "*/*")

				// mocking our http call
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewRequestMatcher(request)).
					// this is our http response
					Return(&http.Response{
						Status:     "200 OK",
						StatusCode: http.StatusOK,
						Proto:      "HTTP/1.1",
						ProtoMajor: 1,
						ProtoMinor: 0,
						Header: http.Header{
							"X-More-Data":  []string{"true"},
							"Content-Type": []string{"text/plain"},
						},
						Request: request,
						Body:    ioutil.NopCloser(bytes.NewBufferString("MORE LOG")),
					}, nil)

				return
			},
			Run: func(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
				// run the method
				return logREST.Get(ctx, name, opts)
			},
			// should return according to our data
			Expected: &devops.PipelineLog{
				HasMore: true,
				Text:    "MORE LOG",

				NextStart: &baseNextStart,
			},
			Err: nil,
		},
		{
			Name: "case 6: with stage and step, no log content output",
			Pipeline: func() (*devops.Pipeline, error) {
				return basicPipeline, nil
			},
			JenkinsBinding: func() (*devops.JenkinsBinding, error) {
				return basicJenkinsBinding, nil
			},
			Jenkins: func() (*devops.Jenkins, error) {
				newJenkins := basicJenkins.DeepCopy()
				newJenkins.Spec.HTTP.Host = "http://localhost:1/"
				return newJenkins, nil
			},
			Secret: func() (*corev1.Secret, error) {
				return basicSecret, nil
			},
			Prepare: func(tc TestCase) (ctx context.Context, name string, opts runtime.Object) {
				// preparing request data
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "pipeline"
				opts = &devops.PipelineLogOptions{Start: 44, Step: 12, Stage: 5}

				// creating our http request
				request, _ := http.NewRequest(
					http.MethodGet,
					"http://localhost:1/blue/rest/organizations/jenkins/pipelines"+ //basic blue ocean rest log address
						"/test/pipelines/test-pipeline"+ // /namespace/pipelines/namespace-pipelineconfig/
						"/runs/1/nodes/5/steps/12/log/?start=44", // /runs/build number/nodes(stage)/stage id/logs
					nil,
				)
				request.SetBasicAuth("admin", "123456")
				request.Host = ""
				request.Header.Add("Accept", "*/*")
				fmt.Printf("expected request: %v \n", request)

				// mocking our http call
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewRequestMatcher(request)).
					// this is our http response
					Return(&http.Response{
						Status:     "404 NotFound",
						StatusCode: http.StatusNotFound,
						Proto:      "HTTP/1.1",
						ProtoMajor: 1,
						ProtoMinor: 0,
						Request:    request,
						Body:       ioutil.NopCloser(bytes.NewBufferString("")),
					}, nil)

				return
			},
			Run: func(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
				// run the method
				return logREST.Get(ctx, name, opts)
			},
			// should return according to our data
			Expected: &devops.PipelineLog{
				HasMore: false,
				Text:    "",
			},
			Err: nil,
		},
	}

	for i, test := range table {
		func() {
			test.Stop = make(chan struct{})
			defer close(test.Stop)

			// fmt.Println(i, "starting", test.Name)

			ctx, name, opts := test.Prepare(test)

			pipeline, _ := test.Pipeline()
			jenkinsBinding, _ := test.JenkinsBinding()
			jenkins, _ := test.Jenkins()
			secret, _ := test.Secret()

			dependencyGetter.EXPECT().
				Get(ctx, devops.TypePipeline, name).
				Return(dependency.ItemList{
					&dependency.Item{
						Kind:     devops.TypePipeline,
						Object:   pipeline,
						IntoFunc: dependency.PipelineCopyInto,
					},
					&dependency.Item{
						Kind:     devops.TypeJenkinsBinding,
						Object:   jenkinsBinding,
						IntoFunc: dependency.JenkinsBindingCopyInto,
					},
					&dependency.Item{
						Kind:     devops.TypeJenkins,
						Object:   jenkins,
						IntoFunc: dependency.JenkinsCopyInto,
					}, &dependency.Item{
						Kind:     devops.TypeSecret,
						Object:   secret,
						IntoFunc: dependency.CoreV1SecretCopyInto,
					},
				})

			res, err := test.Run(ctx, name, opts)
			if err == nil && test.Err != nil {
				t.Errorf("[%d] %s: Should fail but didnt: %v", i, test.Name, test.Err)
			} else if err != nil && test.Err == nil {
				t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
			}
			if !reflect.DeepEqual(res, test.Expected) {
				t.Errorf("[%d] %s: Should be equal but aren't: %v != %v", i, test.Name, res, test.Expected)
			}
		}()

	}
}

func getBasicTestResources() (
	pipe *devops.Pipeline,
	jenkinsBinding *devops.JenkinsBinding,
	jenkins *devops.Jenkins,
	secret *corev1.Secret,
) {
	// basic shared data
	pipe = &devops.Pipeline{
		ObjectMeta: metav1.ObjectMeta{
			Namespace:   "test",
			Name:        "pipeline",
			Annotations: map[string]string{},
		},
		Spec: devops.PipelineSpec{
			PipelineConfig: devops.LocalObjectReference{
				Name: "pipeline",
			},
			JenkinsBinding: devops.LocalObjectReference{
				Name: "jenkinsbinding",
			},
		},
		Status: devops.PipelineStatus{
			Jenkins: &devops.PipelineStatusJenkins{
				Build: "1",
			},
		},
	}
	jenkinsBinding = &devops.JenkinsBinding{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.JenkinsBindingSpec{
			Jenkins: devops.JenkinsInstance{
				Name: "jenkins",
			},
			Account: devops.UserAccount{
				Secret: devops.SecretKeySetRef{
					SecretReference: corev1.SecretReference{
						Name: "secret",
					},
				},
			},
		},
	}
	jenkins = &devops.Jenkins{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.JenkinsSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "http://localhost:1",
				},
			},
		},
	}
	secret = &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "secret",
			Namespace: "test",
		},
		Type: corev1.SecretTypeBasicAuth,
		Data: map[string][]byte{
			"username": []byte("admin"),
			"password": []byte("123456"),
		},
	}
	return
}
