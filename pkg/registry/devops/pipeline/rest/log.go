package rest

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"

	"alauda.io/devops-apiserver/pkg/dependency"

	glog "k8s.io/klog"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"

	openapiruntime "github.com/go-openapi/runtime"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

const DefaultOrganization = "jenkins"

// LogREST implements the log endpoint for a Pod
type LogREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
	Provider     devops.AnnotationProvider
}

// NewLogREST starts a new log store
func NewLogREST(
	getter dependency.Manager,
	roundTripper http.RoundTripper,
	provider devops.AnnotationProvider,
) rest.Storage {
	return &LogREST{
		Getter:       getter,
		RoundTripper: roundTripper,
		Provider:     provider,
	}
}

// LogREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&LogREST{})

// New creates a new PipelineLogOptions object
func (r *LogREST) New() runtime.Object {
	return &devops.Pipeline{}
}

// ProducesMIMETypes LogREST implements StorageMetadata
func (r *LogREST) ProducesMIMETypes(verb string) []string {
	// the "produces" section for pipelines/{name}/log
	return []string{
		"application/json",
	}
}

// ProducesObject LogREST implements StorageMetadata, return PipelineLog as the generating object
func (r *LogREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.PipelineLog{}
}

// Get retrieves a runtime.Object that will stream the contents of the pod log
func (r *LogREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	interOpts, ok := opts.(*devops.PipelineLogOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opts))
	}
	namespace := genericapirequest.NamespaceValue(ctx)
	glog.V(3).Infof(
		"got log options: start: %d stage: %d step: %d namespace: %v",
		interOpts.Start, interOpts.Stage, interOpts.Step, namespace,
	)

	pipeline, _, jenkins, secret, err := getDependency(ctx, name, r.Getter)
	if err != nil {
		glog.Errorf("Error fetching pipeline dependencies %s/%s: %v", namespace, name, err)
		return nil, err
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(jenkins.GetHostPort().Host),
		devopsclient.NewBasicAuth(k8s.GetDataBasicAuthFromSecret(secret).Username, k8s.GetDataBasicAuthFromSecret(secret).Password),
		devopsclient.NewTransport(r.RoundTripper),
	)
	jenkinsClient, err := factory.NewClient(
		jenkins.GetKindType(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get ContinuousIntegrationClient from Jenkins: %s, err: %v", jenkins.GetName(), err))
	}

	branchName, isBranchPipeline := pipeline.Annotations[r.Provider.AnnotationsJenkinsMultiBranchName()]
	build := pipeline.Status.Jenkins.Build

	var log *devops.PipelineLog
	if isBranchPipeline {
		switch {
		case IsStageLog(interOpts) && IsStepLog(interOpts):
			log, err = jenkinsClient.GetBranchJobStepLog(ctx, namespace, pipeline.Spec.PipelineConfig.Name, branchName, DefaultOrganization, build, strconv.FormatInt(interOpts.Stage, 10), strconv.FormatInt(interOpts.Step, 10), interOpts.Start)
		case IsStageLog(interOpts):
			log, err = jenkinsClient.GetBranchJobNodeLog(ctx, namespace, pipeline.Spec.PipelineConfig.Name, branchName, DefaultOrganization, build, strconv.FormatInt(interOpts.Stage, 10), interOpts.Start)
		default:
			// fallback
			buildNumber, err := strconv.ParseInt(build, 10, 64)
			if err != nil {
				glog.Errorf("Error get pipeline build number %s/%s: %v", namespace, name, err)
				return nil, err
			}
			log, err = jenkinsClient.GetBranchJobProgressiveLog(ctx, namespace, pipeline.Spec.PipelineConfig.Name, branchName, buildNumber, interOpts.Start)
		}

	} else {
		switch {
		case IsStageLog(interOpts) && IsStepLog(interOpts):
			log, err = jenkinsClient.GetJobStepLog(ctx, namespace, pipeline.Spec.PipelineConfig.Name, DefaultOrganization, build, strconv.FormatInt(interOpts.Stage, 10), strconv.FormatInt(interOpts.Step, 10), interOpts.Start)
		case IsStageLog(interOpts):
			log, err = jenkinsClient.GetJobNodeLog(ctx, namespace, pipeline.Spec.PipelineConfig.Name, DefaultOrganization, build, strconv.FormatInt(interOpts.Stage, 10), interOpts.Start)
		default:
			// fallback
			buildNumber, err := strconv.ParseInt(build, 10, 64)
			if err != nil {
				glog.Errorf("Error get pipeline build number %s/%s: %v", namespace, name, err)
				return nil, err
			}
			log, err = jenkinsClient.GetJobProgressiveLog(ctx, namespace, pipeline.Spec.PipelineConfig.Name, buildNumber, interOpts.Start)
		}
	}
	if err != nil {
		if apiErr, ok := err.(*openapiruntime.APIError); ok && apiErr.Code == 404 {
			return &devops.PipelineLog{
				HasMore: false,
				Text:    "",
			}, nil
		} else {
			return nil, errors.NewInternalError(fmt.Errorf("error get logs from Pipeline: %s/%s, err: %v", namespace, name, err))
		}
	}
	return log, nil
}

// NewGetOptions creates a new options object
func (r *LogREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.PipelineLogOptions{}, false, ""
}

// IsStageLog returns true if the log options is for a stage
func IsStageLog(opts *devops.PipelineLogOptions) bool {
	return opts != nil && opts.Stage > 0
}

// IsStepLog returns true if the log options is for a step
func IsStepLog(opts *devops.PipelineLogOptions) bool {
	return opts != nil && opts.Step > 0
}
