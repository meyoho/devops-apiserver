/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package pipeline

import (
	"context"
	"fmt"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	k8sutil "alauda.io/devops-apiserver/pkg/util/k8s"
	"k8s.io/klog"
)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper, provider devops.AnnotationProvider) Strategy {
	return Strategy{typer, k8sutil.SimpleNameGenerator, provider}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	pipeline, ok := obj.(*devops.Pipeline)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a Pipeline")
	}
	return labels.Set(pipeline.ObjectMeta.Labels), toSelectableFields(pipeline), pipeline.Initializers != nil, nil
}

// MatchPipeline is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchPipeline(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.Pipeline) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// Strategy strategy for Pipeline
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
	devops.AnnotationProvider
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// PrepareForCreate adds basic fields for creation
func (s Strategy) PrepareForCreate(ctx context.Context, obj runtime.Object) {
	pipeline := obj.(*devops.Pipeline)
	s.generateNameCompatible(pipeline)

	pipeline.Status.Conditions = initializeConditions()
	pipeline.Status = calculateStatus(pipeline)
}

// PrepareForUpdate currently does not do anything specific
// all the updates come from the Jenkins Sync Controller
// TODO: Add Stop part
func (Strategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) {
	newPipeline := obj.(*devops.Pipeline)

	newPipeline.Status = calculateStatus(newPipeline)
}

func initializeConditions() []devops.Condition {
	metaTime := metav1.NewTime(time.Now())
	return []devops.Condition{
		{
			Type:        devops.PipelineConditionTypeSynced,
			LastAttempt: &metaTime,
			Status:      devops.ConditionStatusUnknown,
		},
		{
			Type:        devops.PipelineConditionTypeCompleted,
			LastAttempt: &metaTime,
			Status:      devops.ConditionStatusUnknown,
		},
		{
			Type:        devops.PipelineConditionTypeCancelled,
			LastAttempt: &metaTime,
			Status:      devops.ConditionStatusUnknown,
		},
	}
}

func calculateStatus(pipeline *devops.Pipeline) devops.PipelineStatus {
	metaTime := metav1.NewTime(time.Now())
	status := pipeline.Status.DeepCopy()

	conditions := status.Conditions
	status.Phase = devops.PipelinePhasePending
	status.UpdatedAt = &metaTime

	for _, cond := range conditions {
		if cond.Type == devops.PipelineConditionTypeSynced {
			if cond.Status == devops.ConditionStatusUnknown {
				status.Phase = devops.PipelinePhasePending
				break
			} else if cond.Status == devops.ConditionStatusFalse {
				status.Phase = devops.PipelinePhaseError
				break
			}
		}

		if cond.Type == devops.PipelineConditionTypeCompleted {
			if cond.Status == devops.ConditionStatusTrue {
				status.Phase = devops.PipelinePhase(cond.Reason)
				break
			} else if cond.Status == devops.ConditionStatusUnknown {
				status.Phase = devops.PipelinePhaseQueued
			} else if cond.Status == devops.ConditionStatusFalse {
				status.Phase = devops.PipelinePhase(cond.Reason)
			}
		}

		if cond.Type == devops.PipelineConditionTypeCancelled {
			if pipeline.Spec.Cancel && cond.Status == devops.ConditionStatusUnknown {
				status.Phase = devops.PipelinePhaseCancelling
			} else if cond.Status == devops.ConditionStatusTrue {
				status.Phase = devops.PipelinePhaseCancelled
			}
		}
	}
	return *status
}

// Validate valites a request object
func (Strategy) Validate(ctx context.Context, obj runtime.Object) (errs field.ErrorList) {
	pipeline := obj.(*devops.Pipeline)
	return validation.ValidatePipeline(pipeline)
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) (errs field.ErrorList) {
	errs = validation.ValidatePipeline(obj.(*devops.Pipeline))
	return append(errs, validation.ValidatePipelineUpdate(obj.(*devops.Pipeline), old.(*devops.Pipeline))...)
}

func (Strategy) generateNameCompatible(pipeline *devops.Pipeline) {

	// client is in old version , so we just generate a new name to override old logic
	if pipeline.GenerateName == "" {
		klog.Infof("Change Name of Pipeline %s/%s to use GenerageName logic. Client Should not set the name field, this logic has been deprecated. ", pipeline.Namespace, pipeline.Name)
		pipeline.GenerateName = pipeline.Spec.PipelineConfig.Name
		pipeline.Name = ""
	}
}
