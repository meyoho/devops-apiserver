package pipeline

import (
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {
	pipelineColumnDefinitions := []metav1alpha1.TableColumnDefinition{
		{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
		{Name: "Build", Type: registry.TableTypeString, Description: "Jenkins build number"},
		{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "The State of the pipeline."},
		{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
		{Name: "PipelineConfig", Type: registry.TableTypeString, Priority: 1, Description: "PipelineConfig which it belongs to."},
		{Name: "JenkinsBinding", Type: registry.TableTypeString, Priority: 1, Description: "JenkinsBinding which it relates to."},
		// {Name: "Stages", Type: registry.TableTypeString,  Priority: 1, Description: "Pipeline stages."},
	}

	h.TableHandler(pipelineColumnDefinitions, printPipelineList)
	h.TableHandler(pipelineColumnDefinitions, printPipeline)

	registry.AddDefaultHandlers(h)

}

func printPipelineList(pipelineList *devops.PipelineList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(pipelineList.Items))
	for i := range pipelineList.Items {
		r, err := printPipeline(&pipelineList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

func printPipeline(pipeline *devops.Pipeline, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: pipeline},
	}

	buildNumber := "-"
	if pipeline.Status.Jenkins != nil && pipeline.Status.Jenkins.Build != "" {
		buildNumber = fmt.Sprintf("#%s", pipeline.Status.Jenkins.Build)
	}
	row.Cells = append(row.Cells, pipeline.Name, buildNumber, pipeline.Status.Phase, registry.TranslateTimestamp(pipeline.CreationTimestamp))
	if options.Wide {
		// adding to rows
		row.Cells = append(row.Cells, pipeline.Spec.PipelineConfig.Name, pipeline.Spec.JenkinsBinding.Name)
	}
	return []metav1alpha1.TableRow{row}, nil
}
