package pipeline_test

import (
	"context"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/pipeline"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestPipelineStrategy(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("pipeline.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/registry/devops/pipeline", []Reporter{junitReporter})
}

var _ = Describe("Pipeline.Strategy", func() {
	var (
		scheme             *runtime.Scheme
		annotationProvider devopsv1alpha1.AnnotationProvider
		strategy           pipeline.Strategy

		newPipeline *devopsv1alpha1.Pipeline
		oldPipeline *devopsv1alpha1.Pipeline
	)

	BeforeEach(func() {
		scheme = runtime.NewScheme()
		annotationProvider = devopsv1alpha1.AnnotationProvider{BaseDomain: "alauda.io"}
		strategy = pipeline.NewStrategy(scheme, annotationProvider)
	})

	Context("Create a new pipeline", func() {
		BeforeEach(func() {
			newPipeline = &devopsv1alpha1.Pipeline{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineSpec{},
				// content is not important, it should be kept the same
				Status: devopsv1alpha1.PipelineStatus{},
			}

			strategy.PrepareForCreate(context.TODO(), newPipeline)
		})

		It("Should initializing all conditions and set phase to creating", func() {
			Expect(newPipeline.Status.Phase).To(Equal(devopsv1alpha1.PipelinePhasePending))
			Expect(len(newPipeline.Status.Conditions)).To(Equal(3))
			Expect(newPipeline.Status.Conditions[0].Type).To(Equal(devopsv1alpha1.PipelineConditionTypeSynced))
			Expect(newPipeline.Status.Conditions[0].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
			Expect(newPipeline.Status.Conditions[1].Type).To(Equal(devopsv1alpha1.PipelineConditionTypeCompleted))
			Expect(newPipeline.Status.Conditions[1].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
			Expect(newPipeline.Status.Conditions[2].Type).To(Equal(devopsv1alpha1.PipelineConditionTypeCancelled))
			Expect(newPipeline.Status.Conditions[2].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
		})
	})
	Context("Pipeline triggered", func() {
		BeforeEach(func() {
			newPipeline = &devopsv1alpha1.Pipeline{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineSpec{},
				// content is not important, it should be kept the same
				Status: devopsv1alpha1.PipelineStatus{
					Conditions: []devopsv1alpha1.Condition{
						{
							Type:   devopsv1alpha1.PipelineConditionTypeSynced,
							Status: devopsv1alpha1.ConditionStatusTrue,
						},
						{
							Type:   devopsv1alpha1.PipelineConditionTypeCompleted,
							Status: devopsv1alpha1.ConditionStatusUnknown,
						},
						{
							Type:   devopsv1alpha1.PipelineConditionTypeCancelled,
							Status: devopsv1alpha1.ConditionStatusUnknown,
						},
					},
				},
			}
			oldPipeline = newPipeline.DeepCopy()

			strategy.PrepareForUpdate(context.TODO(), newPipeline, oldPipeline)
		})

		It("Should set Pipeline phase to Queued", func() {
			Expect(newPipeline.Status.Phase).To(Equal(devopsv1alpha1.PipelinePhaseQueued))
			Expect(len(newPipeline.Status.Conditions)).To(Equal(3))
			Expect(newPipeline.Status.Conditions[0].Type).To(Equal(devopsv1alpha1.PipelineConditionTypeSynced))
			Expect(newPipeline.Status.Conditions[0].Status).To(Equal(devopsv1alpha1.ConditionStatusTrue))
			Expect(newPipeline.Status.Conditions[1].Type).To(Equal(devopsv1alpha1.PipelineConditionTypeCompleted))
			Expect(newPipeline.Status.Conditions[1].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
			Expect(newPipeline.Status.Conditions[2].Type).To(Equal(devopsv1alpha1.PipelineConditionTypeCancelled))
			Expect(newPipeline.Status.Conditions[2].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
		})
	})
	Context("Pipeline triggered failed", func() {
		BeforeEach(func() {
			newPipeline = &devopsv1alpha1.Pipeline{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.PipelineSpec{},
				// content is not important, it should be kept the same
				Status: devopsv1alpha1.PipelineStatus{
					Conditions: []devopsv1alpha1.Condition{
						{
							Type:   devopsv1alpha1.PipelineConditionTypeSynced,
							Status: devopsv1alpha1.ConditionStatusFalse,
							Reason: devopsv1alpha1.PipelineConditionReasonTriggerFailed,
						},
						{
							Type:   devopsv1alpha1.PipelineConditionTypeCompleted,
							Status: devopsv1alpha1.ConditionStatusUnknown,
						},
						{
							Type:   devopsv1alpha1.PipelineConditionTypeCancelled,
							Status: devopsv1alpha1.ConditionStatusUnknown,
						},
					},
				},
			}
			oldPipeline = newPipeline.DeepCopy()

			strategy.PrepareForUpdate(context.TODO(), newPipeline, oldPipeline)
		})

		It("Should set Pipeline phase to Error", func() {
			Expect(newPipeline.Status.Phase).To(Equal(devopsv1alpha1.PipelinePhaseError))
			Expect(len(newPipeline.Status.Conditions)).To(Equal(3))
			Expect(newPipeline.Status.Conditions[0].Type).To(Equal(devopsv1alpha1.PipelineConditionTypeSynced))
			Expect(newPipeline.Status.Conditions[0].Status).To(Equal(devopsv1alpha1.ConditionStatusFalse))
			Expect(newPipeline.Status.Conditions[1].Type).To(Equal(devopsv1alpha1.PipelineConditionTypeCompleted))
			Expect(newPipeline.Status.Conditions[1].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
			Expect(newPipeline.Status.Conditions[2].Type).To(Equal(devopsv1alpha1.PipelineConditionTypeCancelled))
			Expect(newPipeline.Status.Conditions[2].Status).To(Equal(devopsv1alpha1.ConditionStatusUnknown))
		})
	})
})
