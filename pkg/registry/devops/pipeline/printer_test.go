package pipeline

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestPipelinePrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	config1 := &devops.Pipeline{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "pipeline-1",
			CreationTimestamp: now,
		},
		Spec: devops.PipelineSpec{
			JenkinsBinding: devops.LocalObjectReference{
				Name: "jenkinsbinding1",
			},
			PipelineConfig: devops.LocalObjectReference{
				Name: "pipeline-config",
			},
		},
		Status: devops.PipelineStatus{
			Phase: devops.PipelinePhaseComplete,
			Jenkins: &devops.PipelineStatusJenkins{
				Build: "1",
			},
		},
	}
	list := &devops.PipelineList{
		Items: []devops.Pipeline{*config1},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: &list.Items[0]},
			Cells: []interface{}{
				"pipeline-1", "#1", devops.PipelinePhaseComplete, registry.TranslateTimestamp(now),
				list.Items[0].Spec.PipelineConfig.Name, list.Items[0].Spec.JenkinsBinding.Name,
			},
		},
	}

	result, err := printPipelineList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing pipeline config: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
