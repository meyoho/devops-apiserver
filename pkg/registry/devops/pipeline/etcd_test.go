package pipeline_test

import (
	"reflect"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apiserver"
	"alauda.io/devops-apiserver/pkg/registry/devops/pipeline"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	genericregistry "k8s.io/apiserver/pkg/registry/generic"
)

const defaultEtcdPathPrefix = "/registry/devops.alauda.io"

func TestNewRest(t *testing.T) {

	annotationprovider := devops.NewAnnotationProvider(devops.UsedBaseDomain)
	registry, err := pipeline.NewREST(apiserver.Scheme, genericregistry.RESTOptions{}, devopsgeneric.NewFakeStore, annotationprovider)
	if err != nil {
		t.Errorf("Should not error while creating: %v", err)
	}
	expected := []string{"all", "devops"}
	if !reflect.DeepEqual(expected, registry.Categories()) {
		t.Errorf("Categories are different: %v != %v", expected, registry.Categories())
	}
	expected = []string{"pipe"}
	if !reflect.DeepEqual(expected, registry.ShortNames()) {
		t.Errorf("Short names are different: %v != %v", expected, registry.ShortNames())
	}
	obj := registry.NewFunc()
	if cpt, ok := obj.(*devops.Pipeline); cpt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.Pipeline", reflect.TypeOf(obj))
	}
	objList := registry.NewListFunc()
	if cpt, ok := objList.(*devops.PipelineList); cpt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.PipelineList", reflect.TypeOf(objList))
	}
	_, err = pipeline.NewREST(apiserver.Scheme, genericregistry.RESTOptions{}, devopsgeneric.NewStandardStore, annotationprovider)
	if err == nil {
		t.Errorf("Empty rest options did not return error")
	}
}
