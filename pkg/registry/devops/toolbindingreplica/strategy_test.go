package toolbindingreplica_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/toolbindingreplica"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation/field"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestStategyValidate(t *testing.T) {
	Scheme := runtime.NewScheme()

	strategy := toolbindingreplica.NewStrategy(Scheme)

	type Table struct {
		name     string
		input    *devops.ToolBindingReplica
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	table := []Table{
		{
			name: "empty selector and empty spec",
			input: &devops.ToolBindingReplica{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.ToolBindingReplicaSpec{
					Selector: devops.ToolSelector{
						ObjectReference: corev1.ObjectReference{
							Kind: devops.TypeCodeRepoService,
						},
					},
					CodeRepoService: &devops.CodeRepoBindingReplicaTemplate{},
				},
			},
			expected: field.ErrorList{
				field.Required(
					field.NewPath("spec").Child("secret"),
					"should not be empty",
				),
				field.Required(
					field.NewPath("spec").Child("selector").Child("name"),
					"should not be empty",
				),
				field.Required(
					field.NewPath("spec").Child("codeRepoService").Child("owners"),
					"should not be empty",
				),
			},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.Validate(nil, obj)
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: %v != %v",
				i, tst.name,
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						tst.expected, res,
					)
				}
			}
		}
	}
}
