package rest_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsfake "alauda.io/devops-apiserver/pkg/client/clientset/internalversion/fake"
	mockregistry "alauda.io/devops-apiserver/pkg/mock/apiserver/registry"
	toolbindingreplicaRest "alauda.io/devops-apiserver/pkg/registry/devops/toolbindingreplica/rest"
	"context"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/testing"
)

var _ = Describe("ToolBindingReplica.rest.project", func() {

	var (
		ctrl                    *gomock.Controller
		toolBindingReplicaStore *mockregistry.MockStandardStorage
		devopsclient            *devopsfake.Clientset
		ctx                     context.Context
		projectREST             *toolbindingreplicaRest.ProjectRest
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		toolBindingReplicaStore = mockregistry.NewMockStandardStorage(ctrl)
		devopsclient = devopsfake.NewSimpleClientset()
		ctx = context.TODO()
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	Describe("list projects", func() {

		JustBeforeEach(func() {
			toolBindingReplica := toolBindingReplica(devops.TypeCodeRepoService, "gitlab")
			devopsclient = &devopsfake.Clientset{}
			toolBindingReplicaStore.EXPECT().Get(ctx, "gitlab", &metav1.GetOptions{ResourceVersion: "0"}).Return(toolBindingReplica, nil)

			devopsclient.AddReactor("get", "codereposervices", func(action testing.Action) (handled bool, ret runtime.Object, err error) {
				if action.GetSubresource() == "" {
					return true, toolBindingReplica, nil
				}
				return true, projectDataList("group1", "group2", "group3"), nil
			})

			projectREST = toolbindingreplicaRest.NewProjectRest(toolBindingReplicaStore, devopsclient)
		})

		Context("When secret is not set and request remote projects", func() {
			It("should return remote projects", func() {

				list, err := projectREST.Get(ctx, "gitlab", &devops.ListProjectOptions{
					TypeMeta: metav1.TypeMeta{
						Kind: "ListProjectOptions",
					},
					IsRemote: true,
				})
				gomega.Expect(err).To(gomega.BeNil())
				projectDataList := list.(*devops.ProjectDataList)
				gomega.Expect(len(projectDataList.Items)).To(gomega.BeEquivalentTo(3))
				gomega.Expect(len(projectDataList.Items[1].Data)).To(gomega.BeEquivalentTo(2))
				gomega.Expect(projectDataList.Items[0].Name).To(gomega.BeEquivalentTo("group1"))
				gomega.Expect(projectDataList.Items[1].Name).To(gomega.BeEquivalentTo("group2"))
				gomega.Expect(projectDataList.Items[2].Name).To(gomega.BeEquivalentTo("group3"))
			})
		})

		Context("When kind is codereposervice and request remote projects", func() {
			It("should return remote projects", func() {

				list, err := projectREST.Get(ctx, "gitlab", &devops.ListProjectOptions{
					TypeMeta: metav1.TypeMeta{
						Kind: "ListProjectOptions",
					},
					SecretName: "secret",
					Namespace:  "global-credentials",
					IsRemote:   true,
				})
				gomega.Expect(err).To(gomega.BeNil())
				projectDataList := list.(*devops.ProjectDataList)
				gomega.Expect(len(projectDataList.Items)).To(gomega.BeEquivalentTo(3))
				gomega.Expect(len(projectDataList.Items[1].Data)).To(gomega.BeEquivalentTo(2))
				gomega.Expect(projectDataList.Items[0].Name).To(gomega.BeEquivalentTo("group1"))
				gomega.Expect(projectDataList.Items[1].Name).To(gomega.BeEquivalentTo("group2"))
				gomega.Expect(projectDataList.Items[2].Name).To(gomega.BeEquivalentTo("group3"))
			})
		})

		Context("When kind is codereposervice and request local projects", func() {
			It("should remote local projects", func() {
				list, err := projectREST.Get(ctx, "gitlab", &devops.ListProjectOptions{
					TypeMeta: metav1.TypeMeta{
						Kind: "ListProjectOptions",
					},
					SecretName: "secret",
					Namespace:  "global-credentials",
					IsRemote:   false,
				})
				gomega.Expect(err).To(gomega.BeNil())
				projectDataList := list.(*devops.ProjectDataList)
				gomega.Expect(len(projectDataList.Items)).To(gomega.BeEquivalentTo(2))
				gomega.Expect(len(projectDataList.Items[1].Data)).To(gomega.BeEquivalentTo(2))
				gomega.Expect(projectDataList.Items[0].Name).To(gomega.BeEquivalentTo("group1"))
				gomega.Expect(projectDataList.Items[1].Name).To(gomega.BeEquivalentTo("group2"))
			})
		})
	})
})

func toolBindingReplica(selectorKind string, toolName string) *devops.ToolBindingReplica {
	return &devops.ToolBindingReplica{
		ObjectMeta: metav1.ObjectMeta{
			Name: toolName,
		},
		Spec: devops.ToolBindingReplicaSpec{
			Selector: devops.ToolSelector{
				ObjectReference: corev1.ObjectReference{
					Kind: selectorKind,
					Name: toolName,
				},
			},
			Secret: &devops.SecretKeySetRef{
				SecretReference: corev1.SecretReference{
					Name:      "secret",
					Namespace: "global-credentials",
				},
			},
			CodeRepoService: &devops.CodeRepoBindingReplicaTemplate{
				Owners: []devops.OwnerInRepository{
					{
						Name: "group1",
					},
					{
						Name: "group2",
					},
					{
						Name: "delete",
					},
				},
			},
		},
	}
}

func projectDataList(names ...string) *devops.ProjectDataList {
	var list = devops.ProjectDataList{
		TypeMeta: metav1.TypeMeta{
			Kind: "ProjectDataList",
		},
		Items: []devops.ProjectData{},
	}

	for _, name := range names {
		item := devops.ProjectData{
			TypeMeta: metav1.TypeMeta{
				Kind: "ProjectData",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Data: map[string]string{
				"what": "ever",
				"but":  "remote",
			},
		}
		list.Items = append(list.Items, item)
	}
	return &list
}
