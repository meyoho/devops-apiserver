package rest_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestREST(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("rest.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/registry/devops/toolbindingreplica/rest", []Reporter{junitReporter})
}
