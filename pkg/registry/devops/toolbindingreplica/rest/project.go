package rest

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	"context"
	"fmt"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	glog "k8s.io/klog"
)

type ProjectRest struct {
	toolBindingReplicaStore rest.StandardStorage
	devopsClient            clientset.InterfaceExpansion
}

func NewProjectRest(
	toolBindingReplicaStore rest.StandardStorage,
	devopsClient clientset.Interface,
) *ProjectRest {
	return &ProjectRest{
		toolBindingReplicaStore: toolBindingReplicaStore,
		devopsClient:            clientset.NewExpansion(devopsClient),
	}
}

func (r *ProjectRest) ProducesObject(verb string) interface{} {
	return ""
}

// ProjectRest implements GetterWithOptions
var _ = rest.GetterWithOptions(&ProjectRest{})

func (r ProjectRest) New() runtime.Object {
	return &devops.ProjectData{}
}

// NewGetOptions creates a new options object
func (r *ProjectRest) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ListProjectOptions{}, false, ""
}

func (r *ProjectRest) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	listOptions, ok := opts.(*devops.ListProjectOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object:%#v , expect to be ListProjectOptions", opts)
	}

	toolBindingReplica, err := getToolBindingReplica(ctx, r.toolBindingReplicaStore, name)
	if err != nil {
		glog.Errorf("Error get toolbindingreplica '%s', error:%#v", name, err)
		return nil, err
	}

	if toolBindingReplica == nil {
		glog.Errorf("Not found toolbindingreplica '%s'", name)
		return nil, errors.NewNotFound(devops.Resource(devops.ResourceKindToolBindingReplica), name)
	}

	toolSelector := toolBindingReplica.Spec.Selector

	// using secret name on toolbindingreplica
	if listOptions.SecretName == "" {
		listOptions.SecretName = toolBindingReplica.Spec.Secret.Name
		listOptions.Namespace = toolBindingReplica.Spec.Secret.Namespace
	}

	fmt.Println("list projects....")
	projectDataList, err := r.devopsClient.DevopsExpansion().DevOpsTool().ListProjects(toolSelector.Kind, toolSelector.Name, *listOptions)
	if err != nil {
		glog.Errorf("List Projects error:%#v, listOptions:%#v", err, listOptions)
		return nil, err
	}

	if listOptions.IsRemote {
		return projectDataList, err
	}

	localProjectDataList := []devops.ProjectData{}

	projectsRef := toolBindingReplica.Spec.GetProjectsRef()
	for _, projectData := range projectDataList.Items {
		for _, ref := range projectsRef {
			if projectData.Name == ref.Name {
				localProjectDataList = append(localProjectDataList, projectData)
			}
		}
	}

	projectDataList.Items = localProjectDataList

	return projectDataList, err
}

func getToolBindingReplica(ctx context.Context, getter devopsgeneric.ResourceGetter, name string) (
	*devops.ToolBindingReplica, error,
) {
	obj, err := getter.Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return nil, err
	}
	toolBindingReplica := obj.(*devops.ToolBindingReplica)
	if toolBindingReplica == nil {
		err = fmt.Errorf("unexpected object type: %#v", obj)
		return nil, err
	}
	return toolBindingReplica, nil
}
