package toolbindingreplica

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// ColumnDefinitions column definitions for ToolBindingReplica
var columnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
	{Name: registry.ToolName, Type: registry.TableTypeString, Description: "Name of tool"},
	{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "Status of the toolbindingreplica"},
	{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
}

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {

	h.TableHandler(columnDefinitions, printToolBindingReplicaList)
	h.TableHandler(columnDefinitions, printerToolBindingReplica)

	registry.AddDefaultHandlers(h)
}

// printToolBindingReplicaList prints a toolBindingReplica list
func printToolBindingReplicaList(toolBindingReplicaList *devops.ToolBindingReplicaList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(toolBindingReplicaList.Items))
	for i := range toolBindingReplicaList.Items {
		r, err := printerToolBindingReplica(&toolBindingReplicaList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

// printerToolBindingReplica prints a toolBindingReplica
func printerToolBindingReplica(toolBindingReplica *devops.ToolBindingReplica, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: toolBindingReplica},
	}

	// basic rows
	row.Cells = append(row.Cells, toolBindingReplica.Name, toolBindingReplica.Spec.Selector.Name, toolBindingReplica.Status.Phase, registry.TranslateTimestamp(toolBindingReplica.CreationTimestamp))

	if options.Wide {
		// for later use
	}

	return []metav1alpha1.TableRow{row}, nil
}
