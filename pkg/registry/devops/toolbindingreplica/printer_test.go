package toolbindingreplica

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestToolBindingReplicaPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	toolbindingreplica := &devops.ToolBindingReplica{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "jenkins-1",
			CreationTimestamp: now,
		},
		Spec: devops.ToolBindingReplicaSpec{
			Selector: devops.ToolSelector{
				ObjectReference: corev1.ObjectReference{
					Name: "jenkins-1",
				},
			},
		},
		Status: devops.ToolBindingReplicaStatus{
			Phase: devops.ToolBindingReplicaPhase(devops.StatusReady),
		},
	}
	list := &devops.ToolBindingReplicaList{
		Items: []devops.ToolBindingReplica{*toolbindingreplica},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: toolbindingreplica},
			Cells: []interface{}{
				"jenkins-1", toolbindingreplica.Spec.Selector.Name, toolbindingreplica.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printToolBindingReplicaList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing jenkins: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
