/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package toolbindingreplica

import (
	"context"
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"
	glog "k8s.io/klog"
)

// NewStrategy creates a new Strategy instance for ToolBindingReplica
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	apiserver, ok := obj.(*devops.ToolBindingReplica)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a ToolBindingReplica")
	}
	return labels.Set(apiserver.ObjectMeta.Labels), toSelectableFields(apiserver), apiserver.Initializers != nil, nil
}

// Strategy ToolBindingReplica strategy
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// PrepareForCreate prepares an object for create request
func (Strategy) PrepareForCreate(ctx context.Context, new runtime.Object) {
	newObj, ok := new.(*devops.ToolBindingReplica)
	if ok {
		newObj.Status.Phase = getPhase(*newObj)
		newObj.Status.HTTPStatus = nil
		return
	}

	glog.Warningf("runtime object type %t is unexpect", new)
}

// PrepareForUpdate prepares an object for update request
func (Strategy) PrepareForUpdate(ctx context.Context, new, old runtime.Object) {
	var (
		newObj *devops.ToolBindingReplica
		oldObj *devops.ToolBindingReplica
	)
	newObj, _ = new.(*devops.ToolBindingReplica)
	oldObj, _ = old.(*devops.ToolBindingReplica)

	// not allow to update status
	newObj.Status = oldObj.Status
	newObj.Status.Phase = getPhase(*newObj)
	newObj.Status.LastAttempt = nil // reset last attempt for current new desired state
	newObj.Status.LastUpdate = &metav1.Time{Time: time.Now()}
}

// Validate validates a request object
func (Strategy) Validate(ctx context.Context, obj runtime.Object) (errs field.ErrorList) {
	newObj, ok := obj.(*devops.ToolBindingReplica)
	if ok {
		return validation.ValidateToolBindingReplica(newObj)
	}
	return
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx context.Context, new, old runtime.Object) (errs field.ErrorList) {
	newObj, ok := new.(*devops.ToolBindingReplica)
	oldObj, ok1 := old.(*devops.ToolBindingReplica)
	if ok && ok1 {
		return validation.ValidateToolBindingReplicaUpdate(newObj, oldObj)
	}
	return
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
	// Often implemented as a type check or empty method
}

// MatchToolBindingReplica is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchToolBindingReplica(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.ToolBindingReplica) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

type tbrStatusStrategy struct {
	Strategy
}

func NewStautsStrategy(strategy Strategy) tbrStatusStrategy {
	return tbrStatusStrategy{
		Strategy: strategy,
	}
}

func (tbrStatusStrategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) {
	newObj := obj.(*devops.ToolBindingReplica)
	oldObj := old.(*devops.ToolBindingReplica)

	// update is not allowed to set spec
	newObj.Spec = oldObj.Spec
}

func (tbrStatusStrategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) (errs field.ErrorList) {
	newObj, ok := obj.(*devops.ToolBindingReplica)
	if ok {
		return validation.ValidateToolBindingReplicaStatus(*newObj)
	}
	return
}

func getPhase(tbr devops.ToolBindingReplica) devops.ToolBindingReplicaPhase {
	if tbr.Spec.Selector.Kind == devops.TypeJenkins || tbr.Spec.Selector.Kind == devops.TypeCodeQualityTool {
		return devops.ToolBindingReplicaPhaseSyncing
	}

	if tbr.Spec.Selector.Kind == devops.TypeCodeRepoService {
		if tbr.Spec.CodeRepoService == nil || tbr.Spec.CodeRepoService.Template == nil {
			return devops.NewToolBindingReplicaPhase(devops.ServiceStatusPhaseCreating)
		}
		return devops.ToolBindingReplicaPhaseSyncing
	}

	if tbr.Spec.Selector.Kind == devops.TypeImageRegistry {
		if tbr.Spec.ImageRegistry == nil || tbr.Spec.ImageRegistry.Template == nil {
			return devops.NewToolBindingReplicaPhase(devops.ServiceStatusPhaseCreating)
		}
		return devops.ToolBindingReplicaPhaseSyncing
	}

	if tbr.Spec.Selector.Kind == devops.TypeDocumentManagement {
		if tbr.Spec.DocumentManagement == nil || tbr.Spec.DocumentManagement.Template == nil {
			return devops.NewToolBindingReplicaPhase(devops.ServiceStatusPhaseCreating)
		}
		return devops.ToolBindingReplicaPhaseSyncing
	}

	if tbr.Spec.Selector.Kind == devops.TypeProjectManagement {
		if tbr.Spec.ProjectManagement == nil || tbr.Spec.ProjectManagement.Template == nil {
			return devops.NewToolBindingReplicaPhase(devops.ServiceStatusPhaseCreating)
		}
		return devops.ToolBindingReplicaPhaseSyncing
	}

	if tbr.Spec.Selector.Kind == devops.TypeArtifactRegistry {
		if tbr.Spec.ArtifactRegistry == nil || tbr.Spec.ArtifactRegistry.Template == nil {
			return devops.NewToolBindingReplicaPhase(devops.ServiceStatusPhaseCreating)
		}
		return devops.ToolBindingReplicaPhaseSyncing
	}

	//TODO add other tools here
	return devops.NewToolBindingReplicaPhase(devops.ServiceStatusPhaseCreating)
}
