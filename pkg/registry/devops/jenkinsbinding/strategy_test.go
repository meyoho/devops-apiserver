package jenkinsbinding_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/jenkinsbinding"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation/field"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestStategyValidate(t *testing.T) {
	Scheme := runtime.NewScheme()

	strategy := jenkinsbinding.NewStrategy(Scheme)

	type Table struct {
		name     string
		input    *devops.JenkinsBinding
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	table := []Table{
		{
			name: "create: empty jenkins and account data",
			input: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devops.JenkinsBindingSpec{},
			},
			expected: field.ErrorList{
				field.Invalid(
					field.NewPath("spec").Child("jenkins").Child("name"),
					"",
					"please provide a jenkins instance name",
				),
				field.Invalid(
					field.NewPath("spec").Child("account").Child("secret").Child("name"),
					"",
					"please provide a secret information with Jenkins' username and API Token",
				),
			},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.Validate(nil, obj)
			},
		},
		{
			name: "create: normal jenkins",
			input: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devops.UserAccount{
						Secret: devops.SecretKeySetRef{
							SecretReference: corev1.SecretReference{
								Name: "secret",
							},
						},
					},
				},
			},
			expected: field.ErrorList{},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.Validate(nil, obj)
			},
		},
		{
			name: "update: empty jenkins and account data",
			input: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devops.JenkinsBindingSpec{},
			},
			expected: field.ErrorList{
				field.Invalid(
					field.NewPath("spec").Child("jenkins").Child("name"),
					"",
					"please provide a jenkins instance name",
				),
				field.Invalid(
					field.NewPath("spec").Child("account").Child("secret").Child("name"),
					"",
					"please provide a secret information with Jenkins' username and API Token",
				),
			},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.ValidateUpdate(nil, obj, nil)
			},
		},
		{
			name: "update: empty jenkins and account data",
			input: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devops.UserAccount{
						Secret: devops.SecretKeySetRef{
							SecretReference: corev1.SecretReference{
								Name: "secret",
							},
						},
					},
				},
			},
			expected: field.ErrorList{},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.ValidateUpdate(nil, obj, nil)
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: %v != %v",
				i, tst.name,
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						tst.expected, res,
					)
				}
			}
		}
	}

}

func TestStategyPrepareForUpdate(t *testing.T) {
	Scheme := runtime.NewScheme()

	strategy := jenkinsbinding.NewStrategy(Scheme)

	type Table struct {
		name     string
		new      *devops.JenkinsBinding
		old      *devops.JenkinsBinding
		expected *devops.JenkinsBinding
	}

	table := []Table{
		{
			name: "no old jenkinsbinding",
			new: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins",
					},
				},
				Status: devops.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
						HTTPStatus: &devops.HostPortStatus{
							StatusCode: 100,
						},
					},
				},
			},
			old: nil,
			expected: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins",
					},
				},
				Status: devops.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					ServiceStatus: devops.ServiceStatus{
						Phase:      devops.StatusReady,
						HTTPStatus: nil},
				},
			},
		},
		{
			name: "no change on the data",
			new: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devops.UserAccount{
						Secret: devops.SecretKeySetRef{
							SecretReference: corev1.SecretReference{
								Name:      "abc",
								Namespace: "default",
							},
						},
					},
				},
				Status: devops.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
						HTTPStatus: &devops.HostPortStatus{
							StatusCode: 100},
					},
				},
			},
			old: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devops.UserAccount{
						Secret: devops.SecretKeySetRef{
							SecretReference: corev1.SecretReference{
								Name:      "abc",
								Namespace: "default",
							},
						},
					},
				},
				Status: devops.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
						HTTPStatus: &devops.HostPortStatus{
							StatusCode: 100,
						},
					},
				},
			},
			expected: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devops.UserAccount{
						Secret: devops.SecretKeySetRef{
							SecretReference: corev1.SecretReference{
								Name: "abc",
							},
						},
					},
				},
				Status: devops.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
						HTTPStatus: &devops.HostPortStatus{
							StatusCode: 100,
						},
					},
				},
			},
		},
		{
			name: "changed jenkins instance",
			new: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "new-jenkins",
					},
					Account: devops.UserAccount{
						Secret: devops.SecretKeySetRef{
							SecretReference: corev1.SecretReference{
								Name: "abc",
							},
						},
					},
				},
				Status: devops.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
						HTTPStatus: &devops.HostPortStatus{
							StatusCode: 100,
						},
						Conditions: []devops.BindingCondition{},
					},
				},
			},
			old: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "jenkins",
					},
					Account: devops.UserAccount{
						Secret: devops.SecretKeySetRef{
							SecretReference: corev1.SecretReference{
								Name: "abc",
							},
						},
					},
				},
				Status: devops.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
						HTTPStatus: &devops.HostPortStatus{
							StatusCode: 100,
						},
					},
				},
			},
			expected: &devops.JenkinsBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsBindingSpec{
					Jenkins: devops.JenkinsInstance{
						Name: "new-jenkins",
					},
					Account: devops.UserAccount{
						Secret: devops.SecretKeySetRef{
							SecretReference: corev1.SecretReference{
								Name: "abc",
							},
						},
					},
				},
				Status: devops.JenkinsBindingStatus{
					// content is not important, it should be kept the same
					ServiceStatus: devops.ServiceStatus{
						Phase:      devops.StatusReady,
						HTTPStatus: nil,
						Conditions: nil,
					},
				},
			},
		},
	}

	for i, tst := range table {
		strategy.PrepareForUpdate(nil, tst.new, tst.old)

		if tst.new.Status.Phase != tst.expected.Status.Phase {
			t.Errorf(
				"Test %d: \"%v\" - status.status are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
		if (tst.new.Status.HTTPStatus == nil && tst.expected.Status.HTTPStatus != nil) ||
			(tst.new.Status.HTTPStatus != nil && tst.expected.Status.HTTPStatus == nil) {
			t.Errorf(
				"Test %d: \"%v\" - http status are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
		if len(tst.new.Status.Conditions) != len(tst.expected.Status.Conditions) {
			t.Errorf(
				"Test %d: \"%v\" - conditions len are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
	}

}
