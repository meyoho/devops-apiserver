package jenkinsbinding

import (
	"testing"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestJenkinsBindingPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	jenkins := &devops.JenkinsBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "jenkins-binding-1",
			CreationTimestamp: now,
		},
		Spec: devops.JenkinsBindingSpec{
			Jenkins: devops.JenkinsInstance{
				Name: "jenkins",
			},
			Account: devops.UserAccount{
				Secret: devops.SecretKeySetRef{
					SecretReference: corev1.SecretReference{
						Name: "secret",
					},
				},
			},
		},
		Status: devops.JenkinsBindingStatus{
			ServiceStatus: devops.ServiceStatus{
				Phase: devops.StatusReady,
			},
		},
	}
	list := &devops.JenkinsBindingList{
		Items: []devops.JenkinsBinding{*jenkins},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: jenkins},
			Cells: []interface{}{
				jenkins.Name, jenkins.Spec.Jenkins.Name, jenkins.Spec.Account.Secret.Name, jenkins.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printJenkinsBindingList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing jenkins: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
