package rest

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	glog "k8s.io/klog"

	"k8s.io/apimachinery/pkg/api/errors"
)

// ProxyREST implements the preview endpoint
type ProxyREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

// NewProxyREST implements the preview endpoint
func NewProxyREST(
	getter dependency.Manager,
	roundTripper http.RoundTripper,
) rest.Storage {
	return &ProxyREST{
		Getter:       getter,
		RoundTripper: roundTripper,
	}
}

var _ = rest.NamedCreater(&ProxyREST{})

// New create a new JenkinsBindingProxyOptions runtime object
func (p *ProxyREST) New() runtime.Object {
	return &devops.JenkinsBindingProxyOptions{}
}

// Create make a request to jenkins as a proxy
// This proxy will only do the proxy work for un-protected api of Jenkins
func (p *ProxyREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (result runtime.Object, err error) {
	proxyOpt, ok := obj.(*devops.JenkinsBindingProxyOptions)
	if !ok {
		err = errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", proxyOpt))
		return
	}

	dependencies := p.Getter.Get(ctx, devops.TypeJenkinsBinding, name)
	if err = dependencies.Validate(); err != nil {
		return
	}
	jenkins := &devops.Jenkins{}
	binding := &devops.JenkinsBinding{}
	dependencies.GetInto(devops.TypeJenkins, jenkins).GetInto(devops.TypeJenkinsBinding, binding)

	host := jenkins.Spec.HTTP.Host
	if !strings.HasSuffix(host, "/") {
		host = host + "/"
	}

	apiURL := proxyOpt.URL
	if strings.HasPrefix(apiURL, "/") {
		apiURL = strings.TrimPrefix(apiURL, "/")
	}

	apiURL = host + apiURL
	glog.V(9).Info("api url is ", apiURL)
	result, err = p.doRequest(apiURL, proxyOpt)
	return
}

func (p *ProxyREST) doRequest(apiURL string,
	payload *devops.JenkinsBindingProxyOptions) (result *devops.JenkinsBindingProxyResult, err error) {
	var (
		request  *http.Request
		response *http.Response
		method   string
	)

	switch payload.Method {
	case http.MethodGet, http.MethodPost:
		method = payload.Method
	default:
		method = http.MethodGet
	}

	request, err = http.NewRequest(method, apiURL, strings.NewReader(payload.Payload))
	if err != nil {
		glog.Errorf("error generating request: %v", err)
		return nil, err
	}

	// fill http request header
	for name, heads := range payload.Header {
		for _, value := range heads {
			request.Header.Add(name, value)
		}
	}

	response, err = p.RoundTripper.RoundTrip(request)
	if err != nil {
		glog.Errorf("error when sending request: %v", err)
		return nil, err
	}

	result = &devops.JenkinsBindingProxyResult{
		Code:   response.StatusCode,
		Header: response.Header,
	}

	textByte, err := ioutil.ReadAll(response.Body)
	if err != nil {
		glog.Errorf("Error reading response body: %v", err)
	}
	if textByte != nil {
		result.Data = string(textByte)
	} else {
		glog.Errorf("Response body is empty")
	}
	return
}
