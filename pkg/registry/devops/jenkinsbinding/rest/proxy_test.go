package rest

import (
	"bytes"
	"context"
	"io/ioutil"
	"testing"

	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mdependency "alauda.io/devops-apiserver/pkg/mock/dependency"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"

	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

func TestDoRequest(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	roundTripper := mhttp.NewMockRoundTripper(ctrl)
	getter := mdependency.NewMockManager(ctrl)

	proxyREST := &ProxyREST{
		RoundTripper: roundTripper,
	}

	type TestCase struct {
		Name           string
		Prepare        func() (ctx context.Context, name string, opts runtime.Object)
		Run            func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error)
		JenkinsBinding func() (*devops.JenkinsBinding, error)
		Jenkins        func() (*devops.Jenkins, error)
		Expect         string
		Err            error
	}

	jenkins, jenkinsBinding := getTestResources()
	table := []TestCase{
		{
			Name: "case 0: all ok use case",
			JenkinsBinding: func() (*devops.JenkinsBinding, error) {
				return jenkinsBinding, nil
			},
			Jenkins: func() (*devops.Jenkins, error) {
				return jenkins, nil
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "jenkinsBinding"
				opts = &devops.JenkinsBindingProxyOptions{
					Method: "GET",
					URL:    "abc",
				}

				getter.EXPECT().Get(ctx, devops.TypeJenkinsBinding, name).
					Return(dependency.ItemList{
						&dependency.Item{
							Kind:   devops.TypeJenkinsBinding,
							Object: jenkinsBinding,
						},
						&dependency.Item{
							Kind:   devops.TypeJenkins,
							Object: jenkins,
						},
					})

				request, _ := http.NewRequest(http.MethodGet, "http://localhost/abc", nil)

				// mocking our http call
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewRequestMatcher(request)).
					// this is our http response
					Return(&http.Response{
						Status:     "200 OK",
						StatusCode: http.StatusOK,
						Proto:      "HTTP/1.1",
						ProtoMajor: 1,
						ProtoMinor: 0,
						Request:    request,
						Body:       ioutil.NopCloser(bytes.NewBufferString("great")),
					}, nil)
				return
			},
			Run: func(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, includeUninitialized bool) (runtime.Object, error) {
				return proxyREST.Create(ctx, name, obj, createValidation, nil)
			},
			Err:    nil,
			Expect: "great",
		},
	}
	for i, test := range table {
		ctx, name, opts := test.Prepare()

		proxyREST.Getter = getter
		result, err := test.Run(ctx, name, opts, func(obj runtime.Object) error { return nil }, false)

		if err == nil && test.Err != nil {
			t.Errorf("[%d] %s: Should fail but did't: %v", i, test.Name, test.Err)
		} else if err != nil && test.Err == nil {
			t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
		}

		_, ok := result.(*devops.JenkinsBindingProxyResult)
		if !ok {
			t.Errorf("[%d] %s invalid type, should be devops.JenkinsBindingProxyResult", i, test.Name)
		}
	}
}

func getTestResources() (jenkins *devops.Jenkins, jenkinsBinding *devops.JenkinsBinding) {
	jenkins = &devops.Jenkins{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: "test",
			Name:      "jenkins",
		},
		Spec: devops.JenkinsSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "http://localhost",
				},
			},
		},
	}
	jenkinsBinding = &devops.JenkinsBinding{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: "test",
			Name:      "jenkinsBinding",
		},
		Spec: devops.JenkinsBindingSpec{
			Jenkins: devops.JenkinsInstance{
				Name: "jenkins",
			},
		},
	}
	return
}
