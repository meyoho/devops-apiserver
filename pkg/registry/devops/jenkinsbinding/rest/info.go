package rest

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/dependency"
	"context"
	"fmt"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	"net/http"
)

// ProxyREST implements the preview endpoint
type InfoREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

// NewProxyREST implements the preview endpoint
func NewInfoREST(
	getter dependency.Manager,
	roundTripper http.RoundTripper,
) rest.Storage {
	return &InfoREST{
		Getter:       getter,
		RoundTripper: roundTripper,
	}
}

var _ = rest.GetterWithOptions(&InfoREST{})

// New create a new JenkinsBindingInfoOptions runtime object
func (r *InfoREST) New() runtime.Object {
	return &devops.JenkinsBindingInfoOptions{}
}

func (r *InfoREST) Get(ctx context.Context, name string, options runtime.Object) (runtime.Object, error) {
	var err error

	infoOpt, ok := options.(*devops.JenkinsBindingInfoOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", infoOpt)
	}

	dependencies := r.Getter.Get(ctx, devops.TypeJenkinsBinding, name)
	if err = dependencies.Validate(); err != nil {
		return nil, err
	}

	jenkins := &devops.Jenkins{}
	dependencies.GetInto(devops.TypeJenkins, jenkins)

	target := infoOpt.Target
	for _, condition := range jenkins.Status.Conditions {
		if condition.Type == v1alpha1.JenkinsConditionStatusType && condition.Name == target {
			return &devops.JenkinsBindingInfoResult{
				Result: condition.Message,
			}, nil
		}
	}

	return &devops.JenkinsBindingInfoResult{}, nil
}

func (r *InfoREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.JenkinsBindingInfoOptions{}, false, ""
}
