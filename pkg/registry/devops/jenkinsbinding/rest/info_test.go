package rest

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"errors"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mdependency "alauda.io/devops-apiserver/pkg/mock/dependency"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

func TestInfo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	roundTripper := mhttp.NewMockRoundTripper(ctrl)
	dependencyGetter := mdependency.NewMockManager(ctrl)
	infoREST := NewInfoREST(dependencyGetter, roundTripper).(*InfoREST)

	type TestCase struct {
		Name           string
		JenkinsBinding func() (*devops.JenkinsBinding, error)
		Jenkins        func() (*devops.Jenkins, error)
		Prepare        func() (ctx context.Context, name string, opts runtime.Object)
		Run            func(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error)
		Err            error
	}

	table := []TestCase{
		{
			Name: "case 0, get nodes",
			JenkinsBinding: func() (binding *devops.JenkinsBinding, err error) {
				return &devops.JenkinsBinding{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "JenkinsBinding",
						Namespace: "test",
					},
					Spec: devops.JenkinsBindingSpec{
						Jenkins: devops.JenkinsInstance{
							Name: "Jenkins",
						},
					},
				}, nil
			},
			Jenkins: func() (jenkins *devops.Jenkins, err error) {
				return &devops.Jenkins{
					ObjectMeta: metav1.ObjectMeta{
						Name: "Jenkins",
					},
					Spec: devops.JenkinsSpec{
						ToolSpec: devops.ToolSpec{
							HTTP: devops.HostPort{
								Host: "http://localhost",
							},
						},
					},
					Status: devops.JenkinsStatus{
						ServiceStatus: devops.ServiceStatus{
							Conditions: []devops.BindingCondition{
								{
									Name:    v1alpha1.JenkinsNodesCondition,
									Message: `{"labels": ["master", "slave1"]}`,
									Type:    v1alpha1.JenkinsConditionStatusType,
								},
							},
						},
					},
				}, nil
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.JenkinsBindingInfoOptions{
					Target: v1alpha1.JenkinsNodesCondition,
				}

				return
			},
			Run: func(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
				result, err := infoREST.Get(ctx, name, opts)
				if err != nil {
					return nil, err
				}
				actualResult := result.(*devops.JenkinsBindingInfoResult).Result
				if result.(*devops.JenkinsBindingInfoResult).Result != `{"labels": ["master", "slave1"]}` {
					return nil, errors.New(`test failed because result not equals to {"labels": ["master", "slave1"]} but got ` + actualResult)
				}
				return result, err
			},
			Err: nil,
		},
	}

	for i, test := range table {
		ctx, name, opts := test.Prepare()

		jenkinsBinding, _ := test.JenkinsBinding()
		jenkins, _ := test.Jenkins()

		dependencyGetter.EXPECT().
			Get(ctx, devops.TypeJenkinsBinding, name).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypeJenkinsBinding,
					Object:   jenkinsBinding,
					IntoFunc: dependency.JenkinsBindingCopyInto,
				},
				&dependency.Item{
					Kind:     devops.TypeJenkins,
					Object:   jenkins,
					IntoFunc: dependency.JenkinsCopyInto,
				},
			})

		_, err := test.Run(ctx, name, opts)

		if err == nil && test.Err != nil {
			t.Errorf("[%d] %s: Should fail but didn't: %v", i, test.Name, test.Err)
		} else if err != nil && test.Err == nil {
			t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
		}
	}
}
