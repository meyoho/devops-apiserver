package jenkinsbinding

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// ColumnDefinitions column definitions for PipelineConfig
var columnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
	{Name: "JenkinsBinding", Type: registry.TableTypeString, Description: "JenkinsBinding instance name"},
	{Name: registry.ResourceSecret, Type: registry.TableTypeString, Description: "Secret name used for accessing"},
	{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "Status of the connectivity to the JenkinsBinding instance"},
	{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
}

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {

	h.TableHandler(columnDefinitions, printJenkinsBindingList)
	h.TableHandler(columnDefinitions, printJenkinsBinding)

	registry.AddDefaultHandlers(h)

}

// PrintJenkinsBindingBindingList prints a jenkins list
func printJenkinsBindingList(jenkinsBindingList *devops.JenkinsBindingList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(jenkinsBindingList.Items))
	for i := range jenkinsBindingList.Items {
		r, err := printJenkinsBinding(&jenkinsBindingList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

// PrintJenkinsBinding prints a jenkins
func printJenkinsBinding(jenkins *devops.JenkinsBinding, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: jenkins},
	}

	// basic rows
	row.Cells = append(row.Cells, jenkins.Name, jenkins.Spec.Jenkins.Name, jenkins.Spec.Account.Secret.Name, jenkins.Status.Phase, registry.TranslateTimestamp(jenkins.CreationTimestamp))

	if options.Wide {
		// for later use
	}

	return []metav1alpha1.TableRow{row}, nil
}
