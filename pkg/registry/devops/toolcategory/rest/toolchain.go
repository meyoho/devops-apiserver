package rest

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"context"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
)

var toolchainLogFilename = "tooltypetoolchain"

// Toolchain implements the log endpoint for a Pod
type Toolchain struct {
	Getter dependency.Manager
}

func NewToolChainREST(
	getter dependency.Manager,
) rest.Storage {
	return &Toolchain{
		Getter: getter,
	}
}

var _ = rest.GetterWithOptions(&Toolchain{})

// New creates a new Pod log options object
func (r *Toolchain) New() runtime.Object {
	return &devops.ToolCategory{}
}

func (r *Toolchain) Get(ctx context.Context, name string, options runtime.Object) (runtime.Object, error) {
	return nil, nil
}

func (r *Toolchain) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ImageTagOptions{}, false, ""
}
