package rest

import (
	"context"
	"reflect"
	"sort"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	"k8s.io/klog"

	"alauda.io/devops-apiserver/pkg/dependency"
	metainternalversion "k8s.io/apimachinery/pkg/apis/meta/internalversion"
)

var settingsLogFilename = "toolcategorySettings"

// Toolchain implements the log endpoint for a Pod
type Settings struct {
	Getter dependency.Manager
}

func NewSettingsREST(
	getter dependency.Manager,
) rest.Storage {
	return &Settings{
		Getter: getter,
	}
}

var _ = rest.Lister(&Settings{})

// New creates a new Pod log options object
func (r *Settings) New() runtime.Object {
	return &devops.ToolCategory{}
}

// Get retrieves a runtime.Object that will stream the contents of codeRepoService
func (r *Settings) List(ctx context.Context, options *metainternalversion.ListOptions) (runtime.Object, error) {
	logMethodName := "List"
	var (
		err error
	)

	os := &metainternalversion.ListOptions{}
	os.ResourceVersion = "0"

	//categoryList, err := r.Getter.List(ctx, reflect.TypeOf(devops.ToolCategory{}).Name(), os)
	//klog.V(9).Infof("[%s %s] categoryList is %#v", settingslogFilename, logMethodName, categoryList)
	//if err != nil {
	//	return nil, err
	//}

	typeList, err := r.Getter.List(ctx, reflect.TypeOf(devops.ToolType{}).Name(), os)
	klog.V(9).Infof("[%s %s] typeList is %#v", settingsLogFilename, logMethodName, typeList)
	if err != nil {
		return nil, err
	}

	cateList := make([]devops.ToolCategoryResp, 0)
	indexMap := make(map[string]int)

	for _, toolTypeItem := range typeList.(*devops.ToolTypeList).Items {
		for _, cateRef := range toolTypeItem.Spec.ToolCategoryRef {
			index, ok := indexMap[cateRef.Name]
			if !ok {

				//TODO 这里需要获取category

				cate := devops.ToolCategory{}
				r.Getter.Get(ctx, reflect.TypeOf(cate).Name(), cateRef.Name).GetInto(reflect.TypeOf(cate).Name(), &cate)

				cateList = append(cateList, devops.ToolCategoryResp{Name: cate.Name, DisplayName: cate.Spec.DisplayName, Enabled: cate.Spec.Enabled, Items: make([]devops.ToolTypeResp, 0)})
				indexMap[cateRef.Name] = len(cateList) - 1
				index = indexMap[cateRef.Name]
			}
			var featuregate string
			if value, ok := toolTypeItem.Annotations["featuregate"]; ok {
				featuregate = value
			}
			cateList[index].Items = append(cateList[index].Items, devops.ToolTypeResp{ //TODO
				DisplayName:          toolTypeItem.Spec.DisplayName,
				Description:          toolTypeItem.Spec.Description,
				RecommendedVersion:   toolTypeItem.Spec.RecommendedVersion,
				Name:                 toolTypeItem.Name,
				Kind:                 toolTypeItem.Spec.Kind,
				Type:                 toolTypeItem.Spec.Type,
				Public:               toolTypeItem.Spec.Public,
				Enabled:              toolTypeItem.Spec.Enabled,
				SupportedSecretTypes: toolTypeItem.Spec.SupportedSecretTypes,
				Host:                 toolTypeItem.Spec.Host,
				Html:                 toolTypeItem.Spec.Html,
				RoleSyncEnabled:      toolTypeItem.Spec.RoleSyncEnabled,
				Enterprise:           toolTypeItem.Spec.Enterprise,
				ApiPath:              toolTypeItem.Spec.ApiPath,
				FeatureGate:          featuregate,
			})
		}
	}

	klog.V(9).Infof("[%s %s] cateList is %#v", settingsLogFilename, logMethodName, cateList)

	setting := &devops.Setting{}
	itemList := r.Getter.Get(ctx, reflect.TypeOf(devops.Setting{}).Name(), "global-settings").GetInto(reflect.TypeOf(devops.Setting{}).Name(), setting)

	if err = itemList.Validate(); err != nil {
		return nil, err
	}

	sort.Sort(sortToolChains(cateList))
	toolChain := devops.SettingResp{ToolChains: cateList, DefaultDomain: setting.Spec.DefaultDomain, VersionGate: setting.Spec.VersionGate}

	return &toolChain, nil
}

// NewGetOptions creates a new options object
func (r *Settings) NewList() runtime.Object {
	return &devops.ToolCategory{}
}

var fixedOrdering = map[string]int{
	devops.ToolChainProjectManagementName:     0,
	devops.ToolChainDocumentManagementName:    1,
	devops.ToolChainCodeRepositoryName:        2,
	devops.ToolChainContinuousIntegrationName: 3,
	devops.ToolChainCodeQualityToolName:       4,
	devops.ToolChainArtifactRepositoryName:    5,
	devops.ToolChainTestToolName:              6,
}

type sortToolChains []devops.ToolCategoryResp

func (e sortToolChains) Swap(i, j int) {
	toolChains := []devops.ToolCategoryResp(e)
	toolChains[i], toolChains[j] = toolChains[j], toolChains[i]
}

func (e sortToolChains) Len() int {
	toolChains := []devops.ToolCategoryResp(e)
	return len(toolChains)
}

func (e sortToolChains) Less(i, j int) bool {
	toolChains := []devops.ToolCategoryResp(e)
	in, jn := len(fixedOrdering), len(fixedOrdering)
	if v, ok := fixedOrdering[toolChains[i].Name]; ok {
		in = v
	}
	if v, ok := fixedOrdering[toolChains[j].Name]; ok {
		jn = v
	}
	return in < jn
}
