package toolcategory

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestToolCategoryPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	tc := &devops.ToolCategory{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "code-quality-tool-1",
			CreationTimestamp: now,
		},
		Spec: devops.ToolCategorySpec{
			DisplayName: devops.ToolCategorySpecDisplayName{
				En: "",
				Zh: "",
			},
			Enabled: true,
		},
		Status: devops.ToolCategoryStatus{
			Phase: "Ready",
		},
	}
	list := &devops.ToolCategoryList{
		Items: []devops.ToolCategory{*tc},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: tc},
			Cells: []interface{}{
				tc.Name, tc.Spec.DisplayName.Zh, tc.Spec.DisplayName.En, tc.Spec.Enabled, tc.Status.Phase, registry.TranslateTimestamp(tc.CreationTimestamp),
			},
		},
	}

	result, err := printToolCategoryList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing registry: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
