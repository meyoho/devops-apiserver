package pipelinetasktemplate

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {
	pipelineConfigColumnDefinitions := []metav1alpha1.TableColumnDefinition{
		{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
		{Name: registry.RepositoryDisplayName, Type: registry.TableTypeString, Description: "Pipeline Task Name"},
		{Name: registry.RepositoryVersion, Type: registry.TableTypeString, Description: "Pipeline Task Version"},
		{Name: registry.RepositoryCategory, Type: registry.TableTypeString, Description: "Category of PipelineTaskTemplate."},
		{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "PipelineTaskTemplate Status"},
		{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
		{Name: registry.RepositoryAgent, Type: registry.TableTypeString, Priority: 1, Description: "Agent label used"},
		{Name: registry.RepositoryEngine, Type: registry.TableTypeString, Priority: 1, Description: "Engine used for template"},
	}

	h.TableHandler(pipelineConfigColumnDefinitions, printPipelineList)
	h.TableHandler(pipelineConfigColumnDefinitions, printPipeline)

	registry.AddDefaultHandlers(h)

}

func printPipelineList(pipelineList *devops.PipelineTaskTemplateList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(pipelineList.Items))
	for i := range pipelineList.Items {
		r, err := printPipeline(&pipelineList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

func printPipeline(pipeline *devops.PipelineTaskTemplate, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: pipeline},
	}

	displayName, version, category := registry.GetPipelineNames(pipeline.ObjectMeta)
	phase := string(pipeline.Status.Phase)
	if len(phase) == 0 {
		phase = "-"
	}
	// basic rows
	row.Cells = append(row.Cells, pipeline.Name, displayName, version, category, phase, registry.TranslateTimestamp(pipeline.CreationTimestamp))
	if options.Wide {
		// adding to rows
		agent := "-"
		if pipeline.Spec.Agent != nil && pipeline.Spec.Agent.Label != "" {
			agent = pipeline.Spec.Agent.Label
		}
		row.Cells = append(row.Cells, agent, string(pipeline.Spec.Engine))
	}
	return []metav1alpha1.TableRow{row}, nil
}
