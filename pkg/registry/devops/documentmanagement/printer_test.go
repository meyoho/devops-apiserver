package documentmanagement

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestDocumentManagementPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	pmObj := &devops.DocumentManagement{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "document-m-1",
			CreationTimestamp: now,
		},
		Spec: devops.DocumentManagementSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "http://confluence.io",
				},
			},
			Type: devops.DocumentManageTypeConfluence,
		},
		Status: devops.ServiceStatus{
			Phase: devops.ServiceStatusPhaseReady,
		},
	}
	list := &devops.DocumentManagementList{
		Items: []devops.DocumentManagement{*pmObj},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: pmObj},
			Cells: []interface{}{
				pmObj.Name, pmObj.Spec.Type, pmObj.Spec.HTTP.Host, pmObj.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printDocumentManagementList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing registry: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
