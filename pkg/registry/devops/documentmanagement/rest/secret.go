package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/util/k8s"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	"alauda.io/devops-apiserver/pkg/registry/devops/documentmanagementbinding"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// SecretREST implements the secret operation for a service
type SecretREST struct {
	DocumentManagementStore         *devopsregistry.REST
	DocumentManagementBindingsStore *devopsregistry.REST
	SecretLister                    corev1listers.SecretLister
	ConfigMapLister                 corev1listers.ConfigMapLister
	Transport                       http.RoundTripper
}

// NewSecretREST starts a new secret store
func NewSecretREST(
	DocumentManagementStore rest.StandardStorage,
	DocumentManagementBindingsStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	configMapLister corev1listers.ConfigMapLister,
	transport http.RoundTripper,
) rest.Storage {
	api := &SecretREST{
		DocumentManagementStore:         DocumentManagementStore.(*devopsregistry.REST),
		DocumentManagementBindingsStore: DocumentManagementBindingsStore.(*devopsregistry.REST),
		SecretLister:                    secretLister,
		ConfigMapLister:                 configMapLister,
		Transport:                       transport,
	}
	return api
}

// SecretREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&SecretREST{})

// New creates a new secret options object
func (r *SecretREST) New() runtime.Object {
	return &devops.CodeRepoServiceAuthorizeOptions{}
}

// ProducesObject SecretREST implements StorageMetadata, return CodeRepoServiceAuthorizeResponse as the generating object
func (r *SecretREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.CodeRepoServiceAuthorizeResponse{}
}

// Get retrieves a runtime.Object that will stream the contents of jenkins
func (r *SecretREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		validateOptions  = &devops.CodeRepoServiceAuthorizeOptions{}
		validateResponse = &devops.CodeRepoServiceAuthorizeResponse{}
		secret           *corev1.Secret
		service          *devops.DocumentManagement
		status           *devops.HostPortStatus
		err              error
	)

	validateOptions, ok := opts.(*devops.CodeRepoServiceAuthorizeOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}

	service, err = documentmanagementbinding.GetDocumentManagement(r.DocumentManagementStore, ctx, name)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, fmt.Errorf("documentmanagement '%s' is not found'", name)
		}
		return nil, fmt.Errorf("error getting documentmanagement '%s', err: %v", name, err)
	}
	secret, err = r.SecretLister.Secrets(validateOptions.Namespace).Get(validateOptions.SecretName)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, fmt.Errorf("secret '%s/%s' is not found'", validateOptions.Namespace, validateOptions.SecretName)
		}
		return nil, fmt.Errorf("error getting secret '%s/%s', err: %v", validateOptions.Namespace, validateOptions.SecretName, err)
	}

	basicAuth := k8s.GetDataBasicAuthFromSecret(secret)
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.Spec.HTTP.Host),
		devopsclient.NewBasicAuth(basicAuth.Username, basicAuth.Password),
		devopsclient.NewTransport(r.Transport),
	)
	serviceClient, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	status, err = serviceClient.Authenticate(ctx)
	if err != nil {
		return nil, fmt.Errorf("authorize failed: %v", err)
	}
	if status != nil && status.StatusCode >= 400 {
		err = fmt.Errorf("username or password in secret '%s' is incorrect", secret.GetName())
	}
	return validateResponse, err
}

// NewGetOptions creates a new options object
func (r *SecretREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoServiceAuthorizeOptions{}, false, ""
}
