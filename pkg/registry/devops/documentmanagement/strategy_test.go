package documentmanagement_test

import (
	"testing"

	corev1 "k8s.io/api/core/v1"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/documentmanagement"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

type Table struct {
	name     string
	new      *devopsv1alpha1.DocumentManagement
	old      *devopsv1alpha1.DocumentManagement
	expected *devopsv1alpha1.DocumentManagement
	method   func(*Table)
}

func TestStrategy(t *testing.T) {
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	scheme := runtime.NewScheme()
	strategy := documentmanagement.NewStrategy(scheme)

	if strategy.NamespaceScoped() {
		t.Errorf("documentmanageservice strategy should not be namespace scoped")
	}
	if strategy.AllowCreateOnUpdate() {
		t.Errorf("documentmanageservice strategy should not allow create on update")
	}
	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("documentmanageservice strategy should not allow unconditional update")
	}

	table := []Table{
		{
			name: "create: invalid input",
			new: &devopsv1alpha1.DocumentManagement{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.DocumentManagementSpec{},
			},
			expected: &devopsv1alpha1.DocumentManagement{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.DocumentManagementSpec{},
				Status: devopsv1alpha1.ServiceStatus{
					Phase: devopsv1alpha1.StatusCreating,
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input did not generate error: %v",
						tst.name, err,
					)
				}

				validGetAttrs(t, tst)
			},
		},
		{
			name: "create: valid input",
			new: &devopsv1alpha1.DocumentManagement{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.DocumentManagementSpec{
					ToolSpec: devopsv1alpha1.ToolSpec{
						HTTP: devopsv1alpha1.HostPort{
							Host: "https://aaa.bbb.ccc",
						},
						Secret: devopsv1alpha1.SecretKeySetRef{
							SecretReference: *Secret,
						},
					},
					Type: "Confluence",
				},
				Status: devopsv1alpha1.ServiceStatus{},
			},

			expected: &devopsv1alpha1.DocumentManagement{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.DocumentManagementSpec{
					ToolSpec: devopsv1alpha1.ToolSpec{
						HTTP: devopsv1alpha1.HostPort{
							Host: "https://aaa.bbb.ccc",
						},
						Secret: devopsv1alpha1.SecretKeySetRef{
							SecretReference: *Secret,
						},
					},
					Type: "Confluence",
				},
				Status: devopsv1alpha1.ServiceStatus{
					Phase: devopsv1alpha1.StatusCreating,
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)
				err := strategy.Validate(nil, tst.new)
				if len(err) != 0 {
					t.Errorf(
						"Test: \"%s\" - Validate valid input should not generate error: %v",
						tst.name, err,
					)
				}

				validGetAttrs(t, tst)
			},
		},
	}

	for i, tst := range table {
		tst.method(&tst)

		if tst.new.Status.Phase != tst.expected.Status.Phase {
			t.Errorf(
				"Test %d: \"%v\" - status.phase are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
	}

}

func validGetAttrs(t *testing.T, tst *Table) {
	_, _, _, getErr := documentmanagement.GetAttrs(tst.new)
	if getErr != nil {
		t.Errorf(
			"Test: \"%s\" - GetAttr return err for object: %v",
			tst.name, getErr,
		)
	}
}
