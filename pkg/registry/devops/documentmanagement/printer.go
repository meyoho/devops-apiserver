package documentmanagement

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// ColumnDefinitions column definitions for PipelineConfig
var columnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
	{Name: registry.ResourceType, Type: registry.TableTypeString, Description: "Type of DocumentManagement"},
	{Name: registry.ResourceHost, Type: registry.TableTypeString, Description: "Host address of the DocumentManagement instance"},
	{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "Status of the connectivity to the DocumentManagement instance"},
	{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
}

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {

	h.TableHandler(columnDefinitions, printDocumentManagementList)
	h.TableHandler(columnDefinitions, printerDocumentManagement)

	registry.AddDefaultHandlers(h)

}

// printDocumentManagementList prints a DocumentManagement list
func printDocumentManagementList(DocumentManagementList *devops.DocumentManagementList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(DocumentManagementList.Items))
	for i := range DocumentManagementList.Items {
		r, err := printerDocumentManagement(&DocumentManagementList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

// printerDocumentManagement prints a DocumentManagement
func printerDocumentManagement(documentManagement *devops.DocumentManagement, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: documentManagement},
	}

	// basic rows
	row.Cells = append(row.Cells, documentManagement.Name, documentManagement.Spec.Type, documentManagement.Spec.HTTP.Host, documentManagement.Status.Phase, registry.TranslateTimestamp(documentManagement.CreationTimestamp))

	if options.Wide {
		// for later use
	}

	return []metav1alpha1.TableRow{row}, nil
}
