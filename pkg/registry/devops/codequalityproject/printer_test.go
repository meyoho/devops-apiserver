package codequalityproject

import (
	"testing"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
)

func TestCodeQulaityPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Minute))
	analyzeTime := metav1.NewTime(time.Now())
	codeQualityProject := &devops.CodeQualityProject{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "code-quality-project-1",
			CreationTimestamp: now,
		},
		Spec: devops.CodeQualityProjectSpec{
			CodeQualityTool: devops.LocalObjectReference{
				Name: "sonarqube",
			},
			CodeRepository: devops.LocalObjectReference{
				Name: "code-repository-diablo",
			},
			CodeQualityBinding: devops.LocalObjectReference{
				Name: "code-quality-binding-1",
			},
			Project: devops.CodeQualityProjectInfo{
				ProjectKey:  "github.com:alaudaorg:diablo",
				ProjectName: "diablo",
				CodeAddress: "github.com/alaudaorg/diablo",
			},
		},
		Status: devops.CodeQualityProjectStatus{
			Phase: devops.ServiceStatusPhaseError,
			CodeQualityConditions: []devops.CodeQualityCondition{
				{
					Branch: "master",
					BindingCondition: devops.BindingCondition{
						Status:      "finished",
						LastAttempt: &analyzeTime,
						Message:     "",
					},
				},
			},
		},
	}
	list := &devops.CodeQualityProjectList{
		Items: []devops.CodeQualityProject{*codeQualityProject},
	}
	expected := []metav1alpha1.TableRow{
		{
			Object: runtime.RawExtension{Object: codeQualityProject},
			Cells: []interface{}{
				codeQualityProject.Name, codeQualityProject.Status.Phase, registry.TranslateTimestamp(now),
				codeQualityProject.Spec.CodeRepository, codeQualityProject.Spec.CodeQualityTool, codeQualityProject.Spec.Project.ProjectKey,
			},
		},
	}
	result, err := printCodeQualityProjectList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing codequalityproject: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
