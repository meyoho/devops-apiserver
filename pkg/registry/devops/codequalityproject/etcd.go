package codequalityproject

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/generic"
)

// NewREST returns a RESTStorage object that will work against API services.
func NewREST(scheme *runtime.Scheme, optsGetter generic.RESTOptionsGetter, factory devopsgeneric.StoreFactory) (*registry.REST, error) {
	strategy := NewStrategy(scheme)

	store, err := factory(
		optsGetter,
		"codequalityproject",
		func() runtime.Object { return &devops.CodeQualityProject{} },
		func() runtime.Object { return &devops.CodeQualityProjectList{} },
		MatchCodeQualityProject,
		GetAttrs,
		strategy, strategy, strategy,
		AddHandlers,
	)
	if err != nil {
		return nil, err
	}
	return &registry.REST{
		Store:           store,
		Short:           []string{"cqp"},
		AddedCategories: []string{"all", "devops"},
	}, nil
}
