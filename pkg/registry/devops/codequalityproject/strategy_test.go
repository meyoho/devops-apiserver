package codequalityproject_test

import (
	"alauda.io/devops-apiserver/pkg/registry/devops/codequalityproject"
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
	"time"
)

type Table struct {
	name     string
	new      *devopsv1alpha1.CodeQualityProject
	old      *devopsv1alpha1.CodeQualityProject
	expected *devopsv1alpha1.CodeQualityProject
	method   func(*Table)
}

func TestStrategy(t *testing.T) {
	scheme := runtime.NewScheme()
	strategy := codequalityproject.NewStrategy(scheme)
	analyzeTime := metav1.NewTime(time.Now())

	if !strategy.NamespaceScoped() {
		t.Errorf("codequalityproject strategy should be namespace scoped")
	}
	if strategy.AllowCreateOnUpdate() {
		t.Errorf("codequalityproject strategy should not allow create on update")
	}
	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("codequalityproject strategy should not allow unconditonal update")
	}

	table := []Table{
		{
			name: "create: invalid input",
			new: &devopsv1alpha1.CodeQualityProject{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.CodeQualityProjectSpec{},
			},
			expected: &devopsv1alpha1.CodeQualityProject{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.CodeQualityProjectSpec{},
				Status: devopsv1alpha1.CodeQualityProjectStatus{
					Phase: devopsv1alpha1.StatusCreating,
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) == 0 {
					t.Errorf("Test: \"%s\" - Validate invalid input did not generate error: %v",
						tst.name, err,
					)
				}
				validGetAttrs(t, tst)
			},
		},
		{
			name: "create: valid input",
			new: &devopsv1alpha1.CodeQualityProject{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.CodeQualityProjectSpec{
					CodeQualityTool: devopsv1alpha1.LocalObjectReference{
						Name: "sonarqube",
					},
					CodeQualityBinding: devopsv1alpha1.LocalObjectReference{
						Name: "code-quality-binding-1",
					},
					CodeRepository: devopsv1alpha1.LocalObjectReference{
						Name: "code-repository-diablo",
					},
					Project: devopsv1alpha1.CodeQualityProjectInfo{
						ProjectKey:  "github.com:alaudaorg:diablo",
						ProjectName: "diablo",
						CodeAddress: "github.com/alaudaorg/diablo",
					},
				},
			},
			expected: &devopsv1alpha1.CodeQualityProject{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.CodeQualityProjectSpec{
					CodeQualityTool: devopsv1alpha1.LocalObjectReference{
						Name: "sonarqube",
					},
					CodeQualityBinding: devopsv1alpha1.LocalObjectReference{
						Name: "code-quality-binding-1",
					},
					CodeRepository: devopsv1alpha1.LocalObjectReference{
						Name: "code-repository-diablo",
					},
					Project: devopsv1alpha1.CodeQualityProjectInfo{
						ProjectKey:  "github.com:alaudaorg:diablo",
						ProjectName: "diablo",
						CodeAddress: "github.com/alaudaorg/diablo",
					},
				},
				Status: devopsv1alpha1.CodeQualityProjectStatus{
					Phase: devopsv1alpha1.StatusCreating,
					CodeQualityConditions: []devopsv1alpha1.CodeQualityCondition{
						{
							Branch: "master",
							BindingCondition: devopsv1alpha1.BindingCondition{
								Status:      "finished",
								LastAttempt: &analyzeTime,
								Message:     "",
							},
						},
					},
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)
				err := strategy.Validate(nil, tst.new)
				if len(err) != 0 {
					t.Errorf(
						"Test: \"%s\" - Validate valid input should not generate error: %v",
						tst.name, err,
					)
				}
				validGetAttrs(t, tst)
			},
		},
	}
	for i, tst := range table {
		tst.method(&tst)
		if tst.new.Status.Phase != tst.expected.Status.Phase {
			t.Errorf(
				"Test %d: \"%v\" - status.phase are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
	}
}

func validGetAttrs(t *testing.T, tst *Table) {
	_, _, _, getErr := codequalityproject.GetAttrs(tst.new)
	if getErr != nil {
		t.Errorf(
			"Test: \"%s\" - GetAttr return err for object: %v",
			tst.name, getErr,
		)
	}
}
