package codequalityproject_test

import (
	"reflect"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apiserver"
	"alauda.io/devops-apiserver/pkg/registry/devops/codequalityproject"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	genericregistry "k8s.io/apiserver/pkg/registry/generic"
)

const defaultEtcdPathPrefix = "/registry/devops.alauda.io"

func TestNewREST(t *testing.T) {
	registry, err := codequalityproject.NewREST(apiserver.Scheme, genericregistry.RESTOptions{}, devopsgeneric.NewFakeStore)
	if err != nil {
		t.Errorf("Should not error while creating: %v", err)
	}
	expected := []string{"all", "devops"}
	if !reflect.DeepEqual(expected, registry.Categories()) {
		t.Errorf("Categories are different: %v != %v", expected, registry.Categories())
	}
	expected = []string{"cqp"}
	if !reflect.DeepEqual(expected, registry.ShortNames()) {
		t.Errorf("Short names are different: %v != %v", expected, registry.ShortNames())
	}
	obj := registry.NewFunc()
	if cpt, ok := obj.(*devops.CodeQualityProject); cpt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.CodeQualityProject", reflect.TypeOf(obj))
	}
	objList := registry.NewListFunc()
	if cpt, ok := objList.(*devops.CodeQualityProjectList); cpt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.CodeQualityProjectList", reflect.TypeOf(objList))
	}
	_, err = codequalityproject.NewREST(apiserver.Scheme, genericregistry.RESTOptions{}, devopsgeneric.NewStandardStore)
	if err == nil {
		t.Errorf("Empty rest options did not return error")
	}
}
