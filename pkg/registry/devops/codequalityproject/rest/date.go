package rest

import (
	"context"
	"fmt"
	"net/http"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
)

type LastAnalysisDateREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

func NewLastAnalysisDateREST(getter dependency.Manager, roundTripper http.RoundTripper) rest.Storage {
	return &LastAnalysisDateREST{
		Getter:       getter,
		RoundTripper: roundTripper,
	}
}

var _ = rest.GetterWithOptions(&LastAnalysisDateREST{})

func (r *LastAnalysisDateREST) New() runtime.Object {
	return &devops.CodeQualityProjectOptions{}
}

func (r *LastAnalysisDateREST) Get(ctx context.Context, name string, options runtime.Object) (runtime.Object, error) {
	opts, ok := options.(*devops.CodeQualityProjectOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opts))
	}

	dependencies := r.Getter.Get(ctx, devops.TypeCodeQualityProject, name)
	if err := dependencies.Validate(); err != nil {
		return nil, err
	}

	codeQualityProject := &devops.CodeQualityProject{}
	service := &devops.CodeQualityTool{}
	secret := &corev1.Secret{}
	dependencies.GetInto(devops.TypeCodeQualityProject, codeQualityProject).GetInto(devops.TypeCodeQualityTool, service).GetInto(devops.TypeSecret, secret)

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetEndpoint()),
		devopsclient.NewBasicAuth(k8s.GetDataBasicAuthFromSecret(secret).Username, k8s.GetDataBasicAuthFromSecret(secret).Password),
		devopsclient.NewTransport(r.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get CodeQualityTool %s client: %v", service.GetEndpoint(), err))
	}

	date, err := serviceClient.GetLastAnalysisDate(ctx, opts.ProjectKey)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error GetLastAnalysisDate %s: %v", opts.ProjectKey, err))
	}

	return &devops.CodeQualityProjectLastAnalysisDate{Date: &metav1.Time{Time: *date}}, nil
}

func (r *LastAnalysisDateREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeQualityProjectOptions{}, false, ""
}
