package codequalityproject

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {
	columnDefinitions := []metav1alpha1.TableColumnDefinition{
		{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
		{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "The State of the CodeQualityProject"},
		{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
		{Name: "CodeRepository", Type: registry.TableTypeString, Priority: 1, Description: "CodeRepository Name In DevOps"},
		{Name: "CodeQualityTool", Type: registry.TableTypeString, Priority: 1, Description: "CodeQualityTool Instance Name"},
		{Name: "CodeQualityProjectKey", Type: registry.TableTypeString, Priority: 1, Description: "CodeQuality Project Key"},
	}

	h.TableHandler(columnDefinitions, printCodeQualityProjectList)
	h.TableHandler(columnDefinitions, printCodeQualityProject)
	registry.AddDefaultHandlers(h)
}

func printCodeQualityProjectList(codeQualityProjectList *devops.CodeQualityProjectList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(codeQualityProjectList.Items))
	for i := range codeQualityProjectList.Items {
		r, err := printCodeQualityProject(&codeQualityProjectList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

func printCodeQualityProject(codeQualityProject *devops.CodeQualityProject, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: codeQualityProject},
	}

	row.Cells = append(row.Cells, codeQualityProject.Name, codeQualityProject.Status.Phase, registry.TranslateTimestamp(codeQualityProject.CreationTimestamp), codeQualityProject.Spec.CodeRepository, codeQualityProject.Spec.CodeQualityTool, codeQualityProject.Spec.Project.ProjectKey)
	if options.Wide {
		// for later use
	}
	return []metav1alpha1.TableRow{row}, nil
}
