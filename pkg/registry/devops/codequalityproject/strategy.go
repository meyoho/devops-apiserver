package codequalityproject

import (
	"context"
	"fmt"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsregistrygeneric "alauda.io/devops-apiserver/pkg/registry/generic"
)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	codeQualityProject, ok := obj.(*devops.CodeQualityProject)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a CodeQualityProject")
	}
	return labels.Set(codeQualityProject.ObjectMeta.Labels), toSelectableFields(codeQualityProject), codeQualityProject.Initializers != nil, nil
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.CodeQualityProject) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// MatchCodeQualityProject is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchCodeQualityProject(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// Strategy strategy for CodeQualityProject
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// PrepareForCreate adds basic fields for creation
func (Strategy) PrepareForCreate(ctx context.Context, obj runtime.Object) {
	codeQualityProject, ok := obj.(*devops.CodeQualityProject)
	if ok {
		codeQualityProject.Status.Phase = devops.ServiceStatusPhaseCreating
		codeQualityProject.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate currently does not do anything specific
func (Strategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) {
	var (
		newCodeQualityProject *devops.CodeQualityProject
	)
	newCodeQualityProject, _ = obj.(*devops.CodeQualityProject)
	newCodeQualityProject.Status.LastUpdate = &metav1.Time{Time: time.Now()}
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
	// Often implemented as a type check or empty method
}

// Valitdate validate a request object
func (Strategy) Validate(ctx context.Context, obj runtime.Object) (errs field.ErrorList) {
	errs = field.ErrorList{}
	codeQualityProject, ok := obj.(*devops.CodeQualityProject)
	if ok {
		errs = append(errs, validateCodeQualityProject(codeQualityProject)...)
	}
	return
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) (errs field.ErrorList) {
	errs = field.ErrorList{}
	codeQualityProject, ok := obj.(*devops.CodeQualityProject)
	if ok {
		errs = append(errs, validateCodeQualityProject(codeQualityProject)...)
	}
	return
}

func validateCodeQualityProject(codeQualityProject *devops.CodeQualityProject) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if codeQualityProject.Spec.CodeQualityTool.Name == "" {
		errs = append(errs, field.Invalid(
			field.NewPath("spec").Child("codequalitytool").Child("name"),
			codeQualityProject.Spec.CodeQualityTool.Name,
			"please provide a codequalitytool instance name",
		))
	}
	if codeQualityProject.Spec.CodeQualityBinding.Name == "" {
		errs = append(errs, field.Invalid(
			field.NewPath("spec").Child("codequalitybinding").Child("name"),
			codeQualityProject.Spec.CodeQualityBinding.Name,
			"please provide a codequalitybinding instance name",
		))
	}
	if codeQualityProject.Spec.CodeRepository.Name == "" {
		errs = append(errs, field.Invalid(
			field.NewPath("spec").Child("coderepository").Child("name"),
			codeQualityProject.Spec.CodeRepository.Name,
			"please provide a coderepository instance name",
		))
	}
	return
}

func GetProject(getter devopsregistrygeneric.ResourceGetter, ctx context.Context, name string) (project *devops.CodeQualityProject, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	project = obj.(*devops.CodeQualityProject)
	if project == nil {
		err = fmt.Errorf("unexpect object type: %v", obj)
	}
	return
}
