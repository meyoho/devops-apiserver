package tooltype

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// ColumnDefinitions colum definitions for CodeQualityTool
var columnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
	{Name: "ZH", Type: registry.TableTypeString, Description: ""},
	{Name: "EN", Type: registry.TableTypeString, Description: ""},
	{Name: "TYPE", Type: registry.TableTypeString, Description: ""},
	{Name: "KIND", Type: registry.TableTypeString, Description: ""},
	{Name: "HOST", Type: registry.TableTypeString, Description: ""},
	{Name: "ENABLE", Type: "bool", Description: ""},
	{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "Status of the connectivity to the CodeQualityTool instance"},
	{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
}

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {
	h.TableHandler(columnDefinitions, printToolTypeList)
	h.TableHandler(columnDefinitions, printerToolType)

	registry.AddDefaultHandlers(h)
}

// printCodeQualityToolList prints a CodeQualityTool list
func printToolTypeList(tcl *devops.ToolTypeList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(tcl.Items))
	for i := range tcl.Items {
		r, err := printerToolType(&tcl.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

// printerCodeQualityTool prints a CodeQualityTool
func printerToolType(tc *devops.ToolType, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: tc},
	}

	// basic rows
	row.Cells = append(row.Cells, tc.Name, tc.Spec.DisplayName.Zh, tc.Spec.DisplayName.En, tc.Spec.Type, tc.Spec.Kind, tc.Spec.Host, tc.Spec.Enabled, tc.Status.Phase, registry.TranslateTimestamp(tc.CreationTimestamp))

	if options.Wide {
		// for later use
	}

	return []metav1alpha1.TableRow{row}, nil
}
