/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tooltype

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	"context"
	"fmt"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"
)

// NewStrategy creates a new Strategy instance for toolcategory
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// Strategy codeQualityTool strategy
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns false
func (Strategy) NamespaceScoped() bool {
	return false
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// PrepareForCreate prepares an object for create request
func (Strategy) PrepareForCreate(ctx context.Context, new runtime.Object) {
	newObj, ok := new.(*devops.ToolType)
	if ok {
		newObj.Status.Phase = "Ready"
	}
}

// PrepareForUpdate prepares an object for update request
func (Strategy) PrepareForUpdate(ctx context.Context, new, old runtime.Object) {
	var (
		newObj, oldObj *devops.ToolType
	)
	newObj, _ = new.(*devops.ToolType)
	if old != nil {
		oldObj, _ = old.(*devops.ToolType)
	}
	if newObj != nil && newObj.Status.Phase != "Ready" {
		newObj.Status.Phase = devops.StatusCreating
	}
	if isChanged(oldObj, newObj) {
		//maybe nothing todo
	}
}

// Validate validates a request object
func (Strategy) Validate(ctx context.Context, obj runtime.Object) (errs field.ErrorList) {
	newObj, ok := obj.(*devops.ToolType)
	if ok {
		return validation.ValidateToolType(newObj)
	}
	return
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx context.Context, new, old runtime.Object) (errs field.ErrorList) {
	newObj, ok := new.(*devops.ToolType)
	oldObj, ok1 := old.(*devops.ToolType)
	if ok && ok1 {
		return validation.ValidateToolTypeUpdate(newObj, oldObj)
	}
	return
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
	// Often implemented as a type check or empty method
}

func isChanged(old, new *devops.ToolType) bool {
	return !apiequality.Semantic.DeepEqual(old.Spec, new.Spec)
}

// MatchCodeQualityTool is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields
func MatchToolType(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	tc, ok := obj.(*devops.ToolType)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a CodeQualityTool")
	}
	return labels.Set(tc.ObjectMeta.Labels), toSelectableFields(tc), tc.Initializers != nil, nil
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.ToolType) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, false)
}

type toolTypeStatusStrategy struct {
	Strategy
}

// NewStatusStrategy creates a new StatusStragtegy for CodeRepoBinding
func NewStatusStrategy(strategy Strategy) toolTypeStatusStrategy {
	return toolTypeStatusStrategy{
		Strategy: strategy,
	}
}

func (toolTypeStatusStrategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) {
	newObj := obj.(*devops.ToolType)
	oldObj := old.(*devops.ToolType)

	// update is not allowed to set spec
	newObj.Spec = oldObj.Spec
}

func (toolTypeStatusStrategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) (errs field.ErrorList) {

	return
}
