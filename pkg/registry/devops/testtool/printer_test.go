package testtool

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestTestToolPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	ttObj := &devops.TestTool{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "test-tool-1",
			CreationTimestamp: now,
		},
		Spec: devops.TestToolSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "http://redwoodhq.io",
				},
			},
			Type: devops.TestToolTypeRedwoodHQ,
		},
		Status: devops.ServiceStatus{
			Phase: devops.ServiceStatusPhaseReady,
		},
	}
	list := &devops.TestToolList{
		Items: []devops.TestTool{*ttObj},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: ttObj},
			Cells: []interface{}{
				ttObj.Name, ttObj.Spec.Type, ttObj.Spec.HTTP.Host, ttObj.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printTestToolList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing registry: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
