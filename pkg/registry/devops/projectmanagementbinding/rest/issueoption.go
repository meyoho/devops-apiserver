package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
)

// IssueOptionREST implements the secret operation for a service
type IssueOptionREST struct {
	Getter    dependency.Manager
	Transport http.RoundTripper
}

// NewIssueOptionREST starts a new secret store
func NewIssueOptionREST(
	getter dependency.Manager,
	transport http.RoundTripper,
) rest.Storage {
	api := &IssueOptionREST{
		Getter:    getter,
		Transport: transport,
	}
	return api
}

// IssueOptionREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&IssueOptionREST{})

// New creates a new secret options object
func (r *IssueOptionREST) New() runtime.Object {
	return &devops.IssueSearchOptions{}
}

// NewGetOptions creates a new options object
func (r *IssueOptionREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.IssueSearchOptions{}, false, ""
}

// Get retrieves a runtime.Object that will stream the contents of jenkins
func (r *IssueOptionREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {

	var (
		secret  *corev1.Secret
		service *devops.ProjectManagement
		binding *devops.ProjectManagementBinding
		err     error
	)

	issuetype, ok := opts.(*devops.IssueSearchOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}

	secret = &corev1.Secret{}
	service = &devops.ProjectManagement{}
	binding = &devops.ProjectManagementBinding{}
	depItemList := r.Getter.Get(ctx, devops.TypeProjectManagementBinding, name)
	depItemList.GetInto(devops.TypeSecret, secret).GetInto(devops.TypeProjectManagement, service).GetInto(devops.TypeProjectManagementBinding, binding)

	data := k8s.GetDataBasicAuthFromSecret(secret)

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetEndpoint()),
		devopsclient.NewBasicAuth(data.Username, data.Password),
		devopsclient.NewTransport(r.Transport),
	)
	client, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	if issuetype.Type == "" {
		return nil, fmt.Errorf("type can not be nil")
	}
	filterdata, err := client.GetIssueOptions(ctx, issuetype.Type)
	if err != nil {
		return nil, err
	}
	return filterdata, err
}
