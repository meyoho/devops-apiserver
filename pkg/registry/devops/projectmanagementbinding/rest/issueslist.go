package rest

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
)

// IssueListREST implements the secret operation for a service
type IssueListREST struct {
	Getter    dependency.Manager
	Transport http.RoundTripper
}

// NewIssueListREST starts a new secret store
func NewIssueListREST(
	getter dependency.Manager,
	transport http.RoundTripper,
) rest.Storage {
	api := &IssueListREST{
		Getter:    getter,
		Transport: transport,
	}
	return api
}

// IssueListREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&IssueListREST{})

// New creates a new issuelist options object
func (r *IssueListREST) New() runtime.Object {
	return &devops.ListIssuesOptions{}
}

// NewGetOptions creates a new options object
func (r *IssueListREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ListIssuesOptions{}, false, ""
}

// Get retrieves a runtime.Object that will stream the contents of jenkins
func (r *IssueListREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {

	var (
		secret  *corev1.Secret
		service *devops.ProjectManagement
		binding *devops.ProjectManagementBinding
		err     error
	)

	listissueoption, ok := opts.(*devops.ListIssuesOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}

	secret = &corev1.Secret{}
	service = &devops.ProjectManagement{}
	binding = &devops.ProjectManagementBinding{}
	depItemList := r.Getter.Get(ctx, devops.TypeProjectManagementBinding, name)
	depItemList.GetInto(devops.TypeSecret, secret).GetInto(devops.TypeProjectManagement, service).GetInto(devops.TypeProjectManagementBinding, binding)

	data := k8s.GetDataBasicAuthFromSecret(secret)

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetEndpoint()),
		devopsclient.NewBasicAuth(data.Username, data.Password),
		devopsclient.NewTransport(r.Transport),
	)
	client, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	projectkeylist := []string{}

	for _, condition := range binding.Status.Conditions {
		if condition.Name == v1alpha1.ProjectmanagementProjectName {
			projectlist := generic.ProjectInfoList{}
			err := json.Unmarshal([]byte(condition.Message), &projectlist)
			if err != nil {
				return nil, err
			}
			for _, project := range projectlist.Item {
				if project.Status == string(v1alpha1.ServiceStatusPhaseReady) {
					projectkeylist = append(projectkeylist, project.Key)
				}
			}
			break
		}
	}

	listissueoption.Projects = projectkeylist

	issuelist, err := client.GetIssueList(ctx, *listissueoption)

	if err != nil {
		return nil, err
	}

	return issuelist, err
}
