package rest

import (
	"context"
	"fmt"
	"net/http"

	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/devops-client/factory"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
)

// IssueREST implements the secret operation for a service
type IssueREST struct {
	Getter    dependency.Manager
	Transport http.RoundTripper
}

// NewIssueREST starts a new secret store
func NewIssueREST(
	getter dependency.Manager,
	transport http.RoundTripper,
) rest.Storage {
	api := &IssueREST{
		Getter:    getter,
		Transport: transport,
	}
	return api
}

// IssueREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&IssueREST{})

// New creates a new secret options object
func (r *IssueREST) New() runtime.Object {
	return &devops.IssueKeyOptions{}
}

// NewGetOptions creates a new options object
func (r *IssueREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.IssueKeyOptions{}, false, ""
}

// Get retrieves a runtime.Object that will stream the contents of jenkins
func (r *IssueREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {

	var (
		secret  *corev1.Secret
		service *devops.ProjectManagement
		err     error
		binding *devops.ProjectManagementBinding
	)

	issuekeyopiton, ok := opts.(*devops.IssueKeyOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}

	secret = &corev1.Secret{}
	service = &devops.ProjectManagement{}
	binding = &devops.ProjectManagementBinding{}
	depItemList := r.Getter.Get(ctx, devops.TypeProjectManagementBinding, name)
	depItemList.GetInto(devops.TypeSecret, secret).GetInto(devops.TypeProjectManagement, service).GetInto(devops.TypeProjectManagementBinding, binding)

	data := k8s.GetDataBasicAuthFromSecret(secret)
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetEndpoint()),
		devopsclient.NewBasicAuth(data.Username, data.Password),
		devopsclient.NewTransport(r.Transport),
	)
	client, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	issuelist, err := client.GetIssueDetail(ctx, issuekeyopiton.Key)
	if err != nil {
		return nil, err
	}
	return issuelist, err
}
