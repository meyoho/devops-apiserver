package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"

	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	"alauda.io/devops-apiserver/pkg/registry/devops/projectmanagementbinding"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// SecretREST implements the secret operation for a service
type SecretREST struct {
	ProjectmanagementStore         *devopsregistry.REST
	ProjectmanagementBindingsStore *devopsregistry.REST
	SecretLister                   corev1listers.SecretLister
	ConfigMapLister                corev1listers.ConfigMapLister
	Transport                      http.RoundTripper
}

// NewSecretREST starts a new secret store
func NewSecretREST(
	ProjectmanagementStore rest.StandardStorage,
	ProjectmanagementBindingsStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	configMapLister corev1listers.ConfigMapLister,
	transport http.RoundTripper,
) rest.Storage {
	api := &SecretREST{
		ProjectmanagementStore:         ProjectmanagementStore.(*devopsregistry.REST),
		ProjectmanagementBindingsStore: ProjectmanagementBindingsStore.(*devopsregistry.REST),
		SecretLister:                   secretLister,
		ConfigMapLister:                configMapLister,
		Transport:                      transport,
	}
	return api
}

// SecretREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&SecretREST{})

// New creates a new secret options object
func (r *SecretREST) New() runtime.Object {
	return &devops.CodeRepoServiceAuthorizeOptions{}
}

// ProducesObject SecretREST implements StorageMetadata, return CodeRepoServiceAuthorizeResponse as the generating object
func (r *SecretREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.CodeRepoServiceAuthorizeResponse{}
}

// Get retrieves a runtime.Object that will stream the contents of jenkins
func (r *SecretREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		validateResponse = &devops.CodeRepoServiceAuthorizeResponse{}
		secret           *corev1.Secret
		service          *devops.ProjectManagement
		status           *devops.HostPortStatus
		err              error
	)
	binding, err := projectmanagementbinding.GetProjectmanagementBinding(r.ProjectmanagementBindingsStore, ctx, name)
	if err != nil {
		return nil, fmt.Errorf("error get projectmanagement binding %s, err: %v", name, err)
	}

	service, err = projectmanagementbinding.GetProjectmanagement(r.ProjectmanagementStore, ctx, binding.Spec.ProjectManagement.Name)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, fmt.Errorf("projectmanagement '%s' is not found'", binding.Spec.ProjectManagement.Name)
		}
		return nil, fmt.Errorf("error getting projectmanagement '%s', err: %v", binding.Spec.ProjectManagement.Name, err)
	}
	secret, err = r.SecretLister.Secrets(binding.Spec.Secret.Namespace).Get(binding.Spec.Secret.Name)
	if err != nil {
		return nil, err
	}

	data := k8s.GetDataBasicAuthFromSecret(secret)
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetEndpoint()),
		devopsclient.NewBasicAuth(data.Username, data.Password),
		devopsclient.NewTransport(r.Transport),
	)
	client, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}
	status, err = client.Authenticate(ctx)
	if err != nil {
		return nil, fmt.Errorf("authorize failed: %v", err)
	}
	if status != nil && status.StatusCode >= 400 {
		err = fmt.Errorf("username or password in secret '%s' is incorrect", secret.GetName())
	}
	return validateResponse, err
}

// NewGetOptions creates a new options object
func (r *SecretREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoServiceAuthorizeOptions{}, false, ""
}
