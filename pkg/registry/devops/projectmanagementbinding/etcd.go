/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package projectmanagementbinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/generic"
)

// NewREST returns a RESTStorage object that will work against API services.
func NewREST(scheme *runtime.Scheme, optsGetter generic.RESTOptionsGetter, factory devopsgeneric.StoreFactory) (*registry.REST, error) {
	strategy := NewStrategy(scheme)

	store, err := factory(
		optsGetter,
		"projectmanagementbindings",
		func() runtime.Object { return &devops.ProjectManagementBinding{} },
		func() runtime.Object { return &devops.ProjectManagementBindingList{} },
		MatchProjectManageBinding,
		GetAttrs,
		strategy, strategy, strategy,
		nil,
	)
	if err != nil {
		return nil, err
	}
	return &registry.REST{
		Store:           store,
		Short:           []string{"pmb"},
		AddedCategories: []string{"toolbindings", "toolchain", "devops"},
	}, nil
}
