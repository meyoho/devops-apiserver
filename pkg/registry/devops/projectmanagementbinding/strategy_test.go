package projectmanagementbinding_test

import (
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/projectmanagementbinding"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

type Table struct {
	name     string
	new      *devopsv1alpha1.ProjectManagementBinding
	old      *devopsv1alpha1.ProjectManagementBinding
	expected *devopsv1alpha1.ProjectManagementBinding
	method   func(*Table)
}

func TestStrategy(t *testing.T) {
	scheme := runtime.NewScheme()
	strategy := projectmanagementbinding.NewStrategy(scheme)
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	Secret.Namespace = "global-credentials"
	ProjectInfo := devopsv1alpha1.ProjectManagementProjectInfo{}
	ProjectInfo.ID = "ID"
	ProjectInfo.Name = "Name"
	List := []devopsv1alpha1.ProjectManagementProjectInfo{}
	List = append(List, ProjectInfo)
	if !strategy.NamespaceScoped() {
		t.Errorf("ProjectManagementBinding strategy should be namespace scoped")
	}
	if strategy.AllowCreateOnUpdate() {
		t.Errorf("ProjectManagementBinding strategy should not allow create on update")
	}
	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("ProjectManagementBinding strategy should not allow unconditional update")
	}

	table := []Table{
		{
			name: "create: invalid input",
			new: &devopsv1alpha1.ProjectManagementBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.ProjectManagementBindingSpec{},
				Status:     devopsv1alpha1.ServiceStatus{},
			},
			expected: &devopsv1alpha1.ProjectManagementBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.ProjectManagementBindingSpec{},
				Status: devopsv1alpha1.ServiceStatus{
					Phase: devopsv1alpha1.StatusCreating,
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input did not generate error: %v",
						tst.name, err,
					)
				}

				validGetAttrs(t, tst)
			},
		},

		{
			name: "create: valid input",
			new: &devopsv1alpha1.ProjectManagementBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.ProjectManagementBindingSpec{
					ProjectManagement: devopsv1alpha1.LocalObjectReference{
						Name: "name",
					},
					Secret: devopsv1alpha1.SecretKeySetRef{
						SecretReference: *Secret,
					},
					ProjectManagementProjectInfos: List,
				},
			},
			expected: &devopsv1alpha1.ProjectManagementBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.ProjectManagementBindingSpec{
					ProjectManagement: devopsv1alpha1.LocalObjectReference{
						Name: "name",
					},
					Secret: devopsv1alpha1.SecretKeySetRef{
						SecretReference: *Secret,
					},
					ProjectManagementProjectInfos: List,
				},
				Status: devopsv1alpha1.ServiceStatus{
					Phase: devopsv1alpha1.StatusCreating,
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) != 0 {
					t.Errorf(
						"Test: \"%s\" - Validate valid input should not generate error: %v",
						tst.name, err,
					)
				}

				validGetAttrs(t, tst)
			},
		},
	}

	for i, tst := range table {
		tst.method(&tst)

		if tst.new.Status.Phase != tst.expected.Status.Phase {
			t.Errorf(
				"Test %d: \"%v\" - status.phase are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
	}

}

func validGetAttrs(t *testing.T, tst *Table) {
	_, _, _, getErr := projectmanagementbinding.GetAttrs(tst.new)
	if getErr != nil {
		t.Errorf(
			"Test: \"%s\" - GetAttr return err for object: %v",
			tst.name, getErr,
		)
	}
}
