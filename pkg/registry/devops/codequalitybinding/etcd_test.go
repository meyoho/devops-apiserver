package codequalitybinding_test

import (
	"reflect"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apiserver"
	"alauda.io/devops-apiserver/pkg/registry/devops/codequalitybinding"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	genericreggitry "k8s.io/apiserver/pkg/registry/generic"
)

const defaultEtcdPathPrefix = "/registry/devops.alauda.io"

func TestNewREST(t *testing.T) {
	registry, err := codequalitybinding.NewREST(apiserver.Scheme, genericreggitry.RESTOptions{}, devopsgeneric.NewFakeStore)
	if err != nil {
		t.Errorf("Should not error while creating: %v", err)
	}
	expected := []string{"toolbindings", "toolchain", "devops"}
	if !reflect.DeepEqual(expected, registry.Categories()) {
		t.Errorf("Categories are different: %v != %v", expected, registry.Categories())
	}
	expected = []string{"cqb"}
	if !reflect.DeepEqual(expected, registry.ShortNames()) {
		t.Errorf("Short names are different: %v != %v", expected, registry.ShortNames())
	}
	obj := registry.NewFunc()
	if cpt, ok := obj.(*devops.CodeQualityBinding); cpt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.CodeQualityBinding", reflect.TypeOf(obj))
	}
	objList := registry.NewListFunc()
	if cpt, ok := objList.(*devops.CodeQualityBindingList); cpt == nil || !ok {
		t.Errorf("Type is not as expected: %v != devops.CodeQualityBindingList", reflect.TypeOf(objList))
	}
	_, err = codequalitybinding.NewREST(apiserver.Scheme, genericreggitry.RESTOptions{}, devopsgeneric.NewStandardStore)
	if err == nil {
		t.Errorf("Empty rest options did not return error")
	}
}
