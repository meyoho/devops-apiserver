package codequalitybinding

import (
	"context"
	"fmt"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	glog "k8s.io/klog"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	k8sregistrygeneric "k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	devopsregistrygeneric "alauda.io/devops-apiserver/pkg/registry/generic"
)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// Getters get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	apiserver, ok := obj.(*devops.CodeQualityBinding)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a CodeQualityBinding")
	}
	return labels.Set(apiserver.ObjectMeta.Labels), toSelectableFields(apiserver), apiserver.Initializers != nil, nil
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.CodeQualityBinding) fields.Set {
	return k8sregistrygeneric.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// MatchCodeQualityBinding is the filter used by the k8sregistrygeneric etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchCodeQualityBinding(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// Strategy codequalitybinding strategy
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns true
func (Strategy) NamespaceScoped() bool {
	return true
}

// PrepareForCreate adds basic fields for creation
func (Strategy) PrepareForCreate(ctx context.Context, obj runtime.Object) {
	codeQualityBinding, ok := obj.(*devops.CodeQualityBinding)
	if ok {
		codeQualityBinding.Status.Phase = devops.ServiceStatusPhaseCreating
		codeQualityBinding.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate Verify if any of the following parts were changed and
// cleanup status if any change happened
// to make sure the controller will check the data again
func (Strategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) {
	var (
		newCodeQualityBinding, oldCodeQualityBinding *devops.CodeQualityBinding
	)
	newCodeQualityBinding, _ = obj.(*devops.CodeQualityBinding)
	if old != nil {
		oldCodeQualityBinding, _ = old.(*devops.CodeQualityBinding)
	}

	if isChanged(oldCodeQualityBinding, newCodeQualityBinding) {
		glog.V(5).Infof("codeQualityBinding %s was changed", newCodeQualityBinding.GetName())
		//newCodeQualityBinding.Status.Phase = devops.ServiceStatusPhaseCreating
		newCodeQualityBinding.Status.LastUpdate = &metav1.Time{Time: time.Now()}
		newCodeQualityBinding.Status.HTTPStatus = nil
	}
}

func isChanged(old, new *devops.CodeQualityBinding) bool {
	return !apiequality.Semantic.DeepEqual(old, new)
}

// Validate validates a request object
func (Strategy) Validate(ctx context.Context, obj runtime.Object) (errs field.ErrorList) {
	codeQualityBinding := obj.(*devops.CodeQualityBinding)
	return validation.ValidateCodeQualityBinding(codeQualityBinding)
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) (errs field.ErrorList) {
	codeQualityBinding := obj.(*devops.CodeQualityBinding)
	return validation.ValidateCodeQualityBinding(codeQualityBinding)
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
	// Often implemented as a type check or empty method
}

func GetCodeQualityBinding(getter devopsregistrygeneric.ResourceGetter, ctx context.Context, name string) (codeQualityBinding *devops.CodeQualityBinding, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	codeQualityBinding = obj.(*devops.CodeQualityBinding)
	if codeQualityBinding == nil {
		err = fmt.Errorf("unexpectd object type: %v", obj)
	}
	return
}

func GetCodeQualityTool(getter devopsregistrygeneric.ResourceGetter, ctx context.Context, name string) (codeQualityTool *devops.CodeQualityTool, err error) {
	var (
		obj runtime.Object
	)
	obj, err = getter.Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	codeQualityTool = obj.(*devops.CodeQualityTool)
	if codeQualityTool == nil {
		err = fmt.Errorf("unexpected object type: %v", obj)
	}
	return
}
