package rest

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"

	"k8s.io/apimachinery/pkg/api/errors"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
)

type ProjectsREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

func NewProjectsREST(getter dependency.Manager, roundTripper http.RoundTripper) rest.Storage {
	return &ProjectsREST{
		Getter:       getter,
		RoundTripper: roundTripper,
	}
}

var _ = rest.GetterWithOptions(&ProjectsREST{})

func (r *ProjectsREST) New() runtime.Object {
	return &devops.CorrespondCodeQualityProjectsOptions{}
}

func (r *ProjectsREST) Get(ctx context.Context, name string, options runtime.Object) (runtime.Object, error) {
	opts, ok := options.(*devops.CorrespondCodeQualityProjectsOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opts))
	}

	dependencies := r.Getter.Get(ctx, devops.TypeCodeQualityBinding, name)
	if err := dependencies.Validate(); err != nil {
		return nil, err
	}

	binding := &devops.CodeQualityBinding{}
	service := &devops.CodeQualityTool{}
	secret := &corev1.Secret{}
	dependencies.GetInto(devops.TypeCodeQualityBinding, binding).GetInto(devops.TypeCodeQualityTool, service).GetInto(devops.TypeSecret, secret)

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetEndpoint()),
		devopsclient.NewBasicAuth(k8s.GetDataBasicAuthFromSecret(secret).Username, k8s.GetDataBasicAuthFromSecret(secret).Password),
		devopsclient.NewTransport(r.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get CodeQualityTool %s client: %v", service.GetEndpoint(), err))
	}

	// transform rest options due to GetOptions does not support complex type
	var repos []devops.CodeRepository
	codeRepo := &devops.CodeRepository{}
	optRepos := strings.Split(opts.Repositories, ",")
	for _, codeRepoName := range optRepos {
		dependencies = r.Getter.Get(ctx, devops.TypeCodeRepository, codeRepoName)
		if err = dependencies.Validate(); err != nil {
			return nil, err
		}
		dependencies.GetInto(devops.TypeCodeRepository, codeRepo)
		repos = append(repos, *codeRepo.DeepCopy())
	}

	projects, err := serviceClient.GetCorrespondCodeQualityProjects(ctx, &devops.CodeRepositoryList{Items: repos}, binding)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get CorrespondCodeQualityProjects %v: %v", opts, err))
	}

	return &devops.CodeQualityProjectList{Items: projects}, nil
}

func (r *ProjectsREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CorrespondCodeQualityProjectsOptions{}, false, ""
}
