package codequalitybinding

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestCodeQualityBindingPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	binding := &devops.CodeQualityBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "sonarqube-binding-1",
			CreationTimestamp: now,
		},
		Spec: devops.CodeQualityBindingSpec{
			CodeQualityTool: devops.LocalObjectReference{
				Name: "sonarqube",
			},
			Secret: devops.SecretKeySetRef{
				SecretReference: corev1.SecretReference{
					Name: "secret",
				},
			},
			CodeRepository: devops.CodeRepositoryInfo{
				All: true,
			},
			Pattern: devops.CodeRepositoryPattern{
				Prefix: nil,
			},
		},
		Status: devops.ServiceStatus{
			Phase: devops.ServiceStatusPhaseError,
		},
	}
	list := &devops.CodeQualityBindingList{
		Items: []devops.CodeQualityBinding{*binding},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: binding},
			Cells: []interface{}{
				binding.Name, binding.Spec.CodeQualityTool.Name, binding.Spec.Secret.Name, binding.Spec.CodeRepository.All, binding.Spec.Pattern.Prefix, binding.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printCodeQualityBindingList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing binding: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
