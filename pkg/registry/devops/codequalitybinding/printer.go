package codequalitybinding

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// ColumnDefinitions column definitions for PipelineConfig
var columnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
	{Name: "CodeQualityTool", Type: registry.TableTypeString, Description: "CodeQualityTool instance name"},
	{Name: registry.ResourceSecret, Type: registry.TableTypeString, Description: "Secret name used for accessing"},
	{Name: "Synced CodeRepository", Type: "bool", Description: "Sync All DevOps CodeRepository"},
	{Name: "Synced CodeRepository Pattern", Type: "number", Description: "Sync CodeRepository Pattern is Prefix"},
	{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "Status of the connectivity to the CodeQuanlityBinding instance"},
	{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
}

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {
	h.TableHandler(columnDefinitions, printCodeQualityBindingList)
	h.TableHandler(columnDefinitions, printCodeQualityBinding)

	registry.AddDefaultHandlers(h)
}

// printCodeQualityBindingList prints a CodeQualityBinding list
func printCodeQualityBindingList(codeQualityBindingList *devops.CodeQualityBindingList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(codeQualityBindingList.Items))
	for i := range codeQualityBindingList.Items {
		r, err := printCodeQualityBinding(&codeQualityBindingList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

// printCodeQualityBinding prints a CodeQualityBinding
func printCodeQualityBinding(codeQualityBinding *devops.CodeQualityBinding, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: codeQualityBinding},
	}

	// basic rows
	row.Cells = append(row.Cells, codeQualityBinding.Name, codeQualityBinding.Spec.CodeQualityTool.Name, codeQualityBinding.Spec.Secret.Name, codeQualityBinding.Spec.CodeRepository.All, codeQualityBinding.Spec.Pattern.Prefix, codeQualityBinding.Status.Phase, registry.TranslateTimestamp(codeQualityBinding.CreationTimestamp))
	if options.Wide {
		// for later use
	}

	return []metav1alpha1.TableRow{row}, nil
}
