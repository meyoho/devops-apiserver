package imageregistry

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestImageRegistryPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	registryObj := &devops.ImageRegistry{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "registry-1",
			CreationTimestamp: now,
		},
		Spec: devops.ImageRegistrySpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "http://registry.io",
				},
			},
			Type: devops.RegistryTypeDocker,
		},
		Status: devops.ServiceStatus{
			Phase: devops.ServiceStatusPhaseReady,
		},
	}
	list := &devops.ImageRegistryList{
		Items: []devops.ImageRegistry{*registryObj},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: registryObj},
			Cells: []interface{}{
				"registry-1", registryObj.Spec.Type, registryObj.Spec.HTTP.Host, registryObj.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printImageRegistryList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing registry: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
