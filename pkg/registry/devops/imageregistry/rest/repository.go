package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"

	"alauda.io/devops-apiserver/pkg/dependency"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

type RepositoryREST struct {
	Getter       dependency.Manager
	SecretLister corev1listers.SecretLister
	Transport    http.RoundTripper
}

var _ = rest.GetterWithOptions(&RepositoryREST{})

// NewRepositoryREST starts a new repository store
func NewRepositoryREST(
	getter dependency.Manager,
	secretLister corev1listers.SecretLister,
	transport http.RoundTripper,
) rest.Storage {
	api := &RepositoryREST{
		Getter:       getter,
		SecretLister: secretLister,
		Transport:    transport,
	}
	return api
}

func (r *RepositoryREST) New() runtime.Object {
	return &devops.CodeRepoServiceAuthorizeOptions{}
}

func (r *RepositoryREST) Get(ctx context.Context, name string, options runtime.Object) (runtime.Object, error) {
	opt, ok := options.(*devops.CodeRepoServiceAuthorizeOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opt))
	}

	var err error
	dependencies := r.Getter.Get(ctx, devops.TypeImageRegistry, name)
	if err = dependencies.Validate(); err != nil {
		return nil, err
	}

	imageregistry := &devops.ImageRegistry{}
	dependencies.GetInto(devops.TypeImageRegistry, imageregistry)

	var secret *corev1.Secret
	if len(opt.SecretName) > 0 {
		secret, err = r.SecretLister.Secrets(opt.Namespace).Get(opt.SecretName)
		if err != nil {
			return nil, errors.NewInternalError(fmt.Errorf("get secret %s error: %v", opt.SecretName, err))
		}
	}

	username := k8s.GetValueInSecret(secret, corev1.BasicAuthUsernameKey)
	password := k8s.GetValueInSecret(secret, corev1.BasicAuthPasswordKey)
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(imageregistry.GetEndpoint()),
		devopsclient.NewBasicAuth(username, password),
		devopsclient.NewTransport(r.Transport),
	)
	serviceClient, err := factory.NewClient(imageregistry.GetType().String(), "", clientOpts)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("get devops client error: %v", err))
	}
	repos, err := serviceClient.GetImageRepos(ctx)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("get remote repositories error: %v", err))
	}

	return repos, nil
}

func (r *RepositoryREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoServiceAuthorizeOptions{}, false, ""
}
