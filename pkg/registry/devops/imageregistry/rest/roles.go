package rest

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"k8s.io/apimachinery/pkg/util/validation/field"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	internalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	glog "k8s.io/klog"
)

const apiName = "imageregistries-roles"

// RolesREST implementation for operating Roles
type RolesREST struct {
	Factory internalthirdparty.ImageRegistryClientFactory
}

// NewRolesREST constructor
func NewRolesREST(transport http.RoundTripper) RolesREST {
	factory := internalthirdparty.NewImageRegistryClientFactory()
	factory.SetTransport(transport)
	rolesREST := RolesREST{
		Factory: factory,
	}
	return rolesREST
}

func (r RolesREST) getImageRegistryClient(getter dependency.Manager, context context.Context, name string) (client internalthirdparty.ImageRegistryClientInterface, err error) {
	glog.V(5).Infof("[%s] getting client for image registry %s...", apiName, name)

	defer func() {
		if err != nil {
			glog.Errorf("[%s] getting client for %s error: %s", apiName, name, err)
		}
	}()
	depItemList := getter.Get(context, devops.TypeImageRegistry, name)
	if err = depItemList.Validate(); err != nil {
		return
	}

	imageRegistry := &devops.ImageRegistry{}
	secret := &corev1.Secret{}
	depItemList.GetInto(devops.TypeImageRegistry, imageRegistry).GetInto(devops.TypeSecret, secret)
	var username, apiToken string
	if secret != nil && secret.Data != nil {
		username = strings.TrimSpace(string(secret.Data[corev1.BasicAuthUsernameKey]))
		apiToken = strings.TrimSpace(string(secret.Data[corev1.BasicAuthPasswordKey]))
	} else {
		err = errors.NewBadRequest(fmt.Sprintf("ImageRegistry %s must have a secret to operate roles", name))
		return
	}

	client = r.Factory.GetImageRegistryClient(imageRegistry.Spec.Type, imageRegistry.Spec.HTTP.Host, username, apiToken)
	if client == nil {
		err = errors.NewBadRequest(fmt.Sprintf("ImageRegistry %s could not generate client...", name))
	}
	return
}

// GetRoleMapping get role mapping
func (r RolesREST) GetRoleMapping(getter dependency.Manager, context context.Context, name string, opts *devops.RoleMappingListOptions) (roleMap *devops.RoleMapping, err error) {
	glog.V(5).Infof("[%s] GetRoleMapping ImageRegistry %s...", apiName, name)
	defer func() {
		if err != nil {
			glog.Errorf("[%s] GetRoleMapping ImageRegistry %s error: %s", apiName, name, err)
		} else {
			glog.V(5).Infof("[%s] GetRoleMapping ImageRegistry %s finished", apiName, name)
		}
	}()
	var client internalthirdparty.ImageRegistryClientInterface
	if client, err = r.getImageRegistryClient(getter, context, name); err != nil {
		return
	}

	roleMap, err = client.GetRoleMapping(opts)
	return
}

// ApplyRoleMapping applies a list of roles and users to multiple projects
func (r RolesREST) ApplyRoleMapping(getter dependency.Manager, context context.Context, name string, rolemapping *devops.RoleMapping) (rolemappinglist *devops.RoleMapping, err error) {
	glog.V(5).Infof("[%s] ApplyRoleMapping ImageRegistry %s...", apiName, name)
	defer func() {
		if err != nil {
			glog.Errorf("[%s] ApplyRoleMapping ImageRegistry %s error: %s", apiName, name, err)
		} else {
			glog.V(5).Infof("[%s] ApplyRoleMapping ImageRegistry %s finished", apiName, name)
		}
	}()
	var client internalthirdparty.ImageRegistryClientInterface
	if client, err = r.getImageRegistryClient(getter, context, name); err != nil {
		return
	}

	rolemapping, err = r.processRoleMapping(rolemapping, client, name, err)

	rolemappinglist = rolemapping
	return
}

func (r *RolesREST) processRoleMapping(rolemapping *devops.RoleMapping, client internalthirdparty.ImageRegistryClientInterface, name string, err error) (*devops.RoleMapping, error) {

	if len(rolemapping.Spec) > 0 {
		errs := field.ErrorList{}
		for i, proj := range rolemapping.Spec {
			fieldPath := field.NewPath("spec").Index(i)
			if len(proj.UserRoleOperations) > 0 {
				for j, user := range proj.UserRoleOperations {
					if err = client.UserProjectOperation(proj.Project, user); err != nil {
						errs = append(errs, field.InternalError(fieldPath.Child("userRoleOperations").Index(j), err))
						glog.Errorf("[%s] ApplyRoleMapping ImageRegistry %s failed to apply operation on project %s for: user: %s role: %s op: %s. Error: %s", apiName, name, proj.Project.Name, user.User.Username, user.Role.Name, user.Operation, err)
					}
				}
			}
		}
		if len(errs) > 0 {
			err = errs.ToAggregate()
		}
	}
	return rolemapping, err
}
