package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	v1 "k8s.io/api/core/v1"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
	glog "k8s.io/klog"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	"alauda.io/devops-apiserver/pkg/util/k8s"
)

// ProjectREST implements platform manage the project with registry
type ProjectREST struct {
	ImageRegistryStore rest.StandardStorage
	SecretLister       corev1listers.SecretLister
	Transport          http.RoundTripper
}

// NewProjectREST starts a new project store
func NewProjectREST(
	imageRegistryStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	transport http.RoundTripper,
) rest.Storage {
	api := &ProjectREST{
		ImageRegistryStore: imageRegistryStore,
		SecretLister:       secretLister,
		Transport:          transport,
	}
	return api
}

// ProjectREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&ProjectREST{})

// New creates a new CreateProjectOptions object
func (r *ProjectREST) New() runtime.Object {
	return &devops.CreateProjectOptions{}
}

// NewGetOptions creates a new options object
func (r *ProjectREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.ListProjectOptions{}, false, ""
}

func (r *ProjectREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	listOptions, ok := opts.(*devops.ListProjectOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object:%#v, expect to be ListProjectOptions", opts))
	}
	client, err := r.getClient(ctx, name, listOptions.SecretName, listOptions.Namespace)
	if err != nil {
		return nil, err
	}
	projects, err := client.GetImageProjects(ctx)
	if err != nil {
		glog.Errorf("List image registry projects error: %s", err)
		return nil, errors.NewBadRequest(err.Error())
	}
	return projects, nil
}

var _ = rest.NamedCreater(&ProjectREST{})

func (r *ProjectREST) Create(ctx context.Context, name string, obj runtime.Object, createValidation rest.ValidateObjectFunc, opts *metav1.CreateOptions) (runtime.Object, error) {
	projectOpt, ok := obj.(*devops.CreateProjectOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %v", projectOpt))
	}
	client, err := r.getClient(ctx, name, projectOpt.SecretName, projectOpt.Namespace)
	if err != nil {
		return nil, err
	}
	err = validation.ValidateImageCreateProject(projectOpt.Name)
	if err != nil {
		return nil, errors.NewBadRequest(err.Error())
	}
	result, err := client.CreateImageProject(ctx, projectOpt.Name)
	return result, nil
}

func (r *ProjectREST) getClient(ctx context.Context, name, secretName, secretNamespace string) (devopsclient.Interface, error) {
	var username, credential string

	service, err := getImageService(ctx, r.ImageRegistryStore, name, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("Error get ImageRegistry Service: %s, error: %s", name, err)
		return nil, errors.NewBadRequest(err.Error())
	}
	if service == nil {
		glog.Errorf("Not found ImageRegistry: %s", name)
		return nil, errors.NewNotFound(devops.Resource(devops.ResourceKindImageRegistry), name)
	}
	if (secretName == "" || secretNamespace == "") && service.Spec.Secret.Namespace != "" && service.Spec.Secret.Name != "" {
		secretName = service.Spec.Secret.Name
		secretNamespace = service.Spec.Secret.Namespace
	}

	var secret *v1.Secret = nil
	if secretName != "" {
		secret, err = r.SecretLister.Secrets(secretNamespace).Get(secretName)
		if err != nil {
			glog.Errorf("Error get secret '%s/%s', error:%#v", secretNamespace, secretName, err)
			return nil, errors.NewBadRequest(err.Error())
		}
	}

	if secret == nil && secretName != "" {
		glog.Errorf("Not found secret: %s", secretName)
		return nil, errors.NewNotFound(devops.Resource(devops.ResourceKindImageRegistry), secretName)
	}
	basicAuth := k8s.GetDataBasicAuthFromSecret(secret)
	if basicAuth == nil {
		username = ""
		credential = ""
	} else {
		username = basicAuth.Username
		credential = basicAuth.Password
	}
	endpoint := service.GetEndpoint()
	serviceType := service.Spec.Type
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(endpoint),
		devopsclient.NewBasicAuth(username, credential),
		devopsclient.NewTransport(r.Transport),
	)
	serviceClient, err := factory.NewClient(
		serviceType.String(),
		"",
		clientOpts,
	)
	return serviceClient, err
}

func getImageService(ctx context.Context, getter devopsgeneric.ResourceGetter, name string, options metav1.GetOptions) (*devops.ImageRegistry, error) {
	obj, err := getter.Get(ctx, name, &options)
	if err != nil {
		return nil, err
	}
	service := obj.(*devops.ImageRegistry)
	if service == nil {
		return nil, fmt.Errorf("unexpectd object type: %#v", obj)
	}
	return service, nil
}
