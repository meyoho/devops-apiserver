package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	"alauda.io/devops-apiserver/pkg/registry/devops/imageregistrybinding"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// SecretREST implements the secret operation for a service
type SecretREST struct {
	ImageRegistryStore        *devopsregistry.REST
	ImageRegistryBindingStore *devopsregistry.REST
	SecretLister              corev1listers.SecretLister
	ConfigMapLister           corev1listers.ConfigMapLister
	Transport                 http.RoundTripper
}

// NewSecretREST starts a new secret store
func NewSecretREST(
	imageRegistryStore rest.StandardStorage,
	imageRegistryBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	configMapLister corev1listers.ConfigMapLister,
	transport http.RoundTripper,
) rest.Storage {
	api := &SecretREST{
		ImageRegistryStore:        imageRegistryStore.(*devopsregistry.REST),
		ImageRegistryBindingStore: imageRegistryBindingStore.(*devopsregistry.REST),
		SecretLister:              secretLister,
		ConfigMapLister:           configMapLister,
		Transport:                 transport,
	}
	return api
}

// SecretREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&SecretREST{})

// New creates a new secret options object
func (r *SecretREST) New() runtime.Object {
	return &devops.CodeRepoServiceAuthorizeOptions{}
}

// ProducesObject SecretREST implements StorageMetadata, return CodeRepoServiceAuthorizeResponse as the generating object
func (r *SecretREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.CodeRepoServiceAuthorizeResponse{}
}

// Get retrieves a runtime.Object that will stream the contents of codeRepoService
func (r *SecretREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		validateOptions  = &devops.CodeRepoServiceAuthorizeOptions{}
		validateResponse = &devops.CodeRepoServiceAuthorizeResponse{}
		secret           *corev1.Secret
		service          *devops.ImageRegistry
		status           *devops.HostPortStatus
		err              error
	)

	validateOptions, ok := opts.(*devops.CodeRepoServiceAuthorizeOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opts))
	}

	service, err = imageregistrybinding.GetImageRegistry(r.ImageRegistryStore, ctx, name)
	if err != nil {
		return nil, err
	}

	secret, err = r.SecretLister.Secrets(validateOptions.Namespace).Get(validateOptions.SecretName)
	if err != nil {
		return nil, err
	}

	username := k8s.GetValueInSecret(secret, corev1.BasicAuthUsernameKey)
	credential := k8s.GetValueInSecret(secret, corev1.BasicAuthPasswordKey)
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.GetEndpoint()),
		devopsclient.NewBasicAuth(username, credential),
		devopsclient.NewTransport(r.Transport),
	)
	serviceClient, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}
	status, err = serviceClient.Authenticate(ctx)
	if err != nil {
		// this needs to be bad request otherwise will cause UI to start login
		return nil, errors.NewBadRequest(fmt.Sprintf("authorize failed: %s", err))
	}
	if status != nil && status.StatusCode >= 400 {
		err = errors.NewBadRequest(fmt.Sprintf("username or password in secret '%s' is incorrect", secret.GetName()))
	}
	return validateResponse, err
}

// NewGetOptions creates a new options object
func (r *SecretREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoServiceAuthorizeOptions{}, false, ""
}
