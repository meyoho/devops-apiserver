/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package imageregistry

import (
	"context"
	"fmt"

	apiequality "k8s.io/apimachinery/pkg/api/equality"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
)

type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	apiserver, ok := obj.(*devops.ImageRegistry)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a ImageRegistry Service")
	}
	return labels.Set(apiserver.ObjectMeta.Labels), toSelectableFields(apiserver), apiserver.Initializers != nil, nil
}

// MatchImageRegistry is the filter used by the generic etcd backend to watch events
func MatchImageRegistry(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// toSelectableFields returns a field set that represents the object
func toSelectableFields(obj *devops.ImageRegistry) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// NamespaceScoped returns false
func (Strategy) NamespaceScoped() bool {
	return false
}

// PrepareForCreate adds basic fields for creation
func (Strategy) PrepareForCreate(ctx context.Context, obj runtime.Object) {
	imageRegistry, ok := obj.(*devops.ImageRegistry)
	if ok {
		// imageRegistry.Spec.HTTP.Host = registrygeneric.RemoveTrailingDash(imageRegistry.Spec.HTTP.Host)
		imageRegistry.Status.Phase = devops.ServiceStatusPhaseCreating
		imageRegistry.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate prepares an object for update request
func (Strategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) {
	var (
		newImageRegistry, oldImageRegistry *devops.ImageRegistry
	)
	newImageRegistry, _ = obj.(*devops.ImageRegistry)
	if old != nil {
		oldImageRegistry, _ = old.(*devops.ImageRegistry)
	}

	if newImageRegistry != nil && newImageRegistry.Status.Phase.IsInvalid() {
		newImageRegistry.Status.Phase = devops.StatusCreating
	}
	// first check if the address is different
	// or if this a first attempt
	if isChanged(oldImageRegistry, newImageRegistry) {
		// newImageRegistry.Spec.HTTP.Host = registrygeneric.RemoveTrailingDash(newImageRegistry.Spec.HTTP.Host)
		newImageRegistry.Status.HTTPStatus = nil
	}
}

// Validate validates an request object
func (Strategy) Validate(ctx context.Context, obj runtime.Object) (errs field.ErrorList) {
	imageRegistry := obj.(*devops.ImageRegistry)
	return validation.ValidateImageRegistry(imageRegistry)
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize prepares object to save
func (Strategy) Canonicalize(obj runtime.Object) {
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) (errs field.ErrorList) {
	imageRegistry := obj.(*devops.ImageRegistry)
	return validation.ValidateImageRegistry(imageRegistry)
}

func isChanged(old, new *devops.ImageRegistry) bool {
	if old == nil || new == nil {
		return true
	}
	return !apiequality.Semantic.DeepEqual(old.Spec, new.Spec)
}

type imageregistryStatusStrategy struct {
	Strategy
}

// NewStatusStrategy creates a new StatusStragtegy for CodeRepoBinding
func NewStatusStrategy(strategy Strategy) imageregistryStatusStrategy {
	return imageregistryStatusStrategy{
		Strategy: strategy,
	}
}

func (imageregistryStatusStrategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) {
	newObj := obj.(*devops.ImageRegistry)
	oldObj := old.(*devops.ImageRegistry)

	// update is not allowed to set spec
	newObj.Spec = oldObj.Spec
}

func (imageregistryStatusStrategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) (errs field.ErrorList) {

	return
}
