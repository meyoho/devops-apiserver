package codequalitytool

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// ColumnDefinitions colum definitions for CodeQualityTool
var columnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
	{Name: registry.ResourceType, Type: registry.TableTypeString, Description: "Type of CodeQualityTool instance"},
	{Name: registry.ResourceHost, Type: registry.TableTypeString, Description: "Host address of the CodeQualityTool instance"},
	{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "Status of the connectivity to the CodeQualityTool instance"},
	{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
}

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {
	h.TableHandler(columnDefinitions, printCodeQualityToolList)
	h.TableHandler(columnDefinitions, printerCodeQualityTool)

	registry.AddDefaultHandlers(h)
}

// printCodeQualityToolList prints a CodeQualityTool list
func printCodeQualityToolList(codeQualityToolList *devops.CodeQualityToolList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(codeQualityToolList.Items))
	for i := range codeQualityToolList.Items {
		r, err := printerCodeQualityTool(&codeQualityToolList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

// printerCodeQualityTool prints a CodeQualityTool
func printerCodeQualityTool(codeQualityToolObj *devops.CodeQualityTool, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: codeQualityToolObj},
	}

	// basic rows
	row.Cells = append(row.Cells, codeQualityToolObj.Name, codeQualityToolObj.Spec.Type, codeQualityToolObj.Spec.HTTP.Host, codeQualityToolObj.Status.Phase, registry.TranslateTimestamp(codeQualityToolObj.CreationTimestamp))

	if options.Wide {
		// for later use
	}

	return []metav1alpha1.TableRow{row}, nil
}
