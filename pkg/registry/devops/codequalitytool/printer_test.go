package codequalitytool

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestCodeQualityToolPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	cqtObj := &devops.CodeQualityTool{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "code-quality-tool-1",
			CreationTimestamp: now,
		},
		Spec: devops.CodeQualityToolSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "http://sonarcloud.io",
				},
			},
			Type: devops.CodeQualityToolTypeSonarqube,
		},
		Status: devops.ServiceStatus{
			Phase: devops.ServiceStatusPhaseReady,
		},
	}
	list := &devops.CodeQualityToolList{
		Items: []devops.CodeQualityTool{*cqtObj},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: cqtObj},
			Cells: []interface{}{
				cqtObj.Name, cqtObj.Spec.Type, cqtObj.Spec.HTTP.Host, cqtObj.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printCodeQualityToolList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing registry: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
