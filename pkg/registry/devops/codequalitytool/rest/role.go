package rest

import (
	"fmt"
	"net/http"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops"
	internalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/util"
	"k8s.io/apimachinery/pkg/apis/meta/internalversion"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8srequest "k8s.io/apiserver/pkg/endpoints/request"
	glog "k8s.io/klog"

	"context"

	v1 "k8s.io/api/core/v1"
)

type RolesREST struct {
	Factory         internalthirdparty.CodeQualityClientFactory
	systemNamespace string
}

func NewRolesREST(transport http.RoundTripper, systemNamespace string) RolesREST {
	factory := internalthirdparty.NewCodeQualityClientFactory()
	factory.SetTransport(transport)
	rolesREST := RolesREST{
		Factory:         factory,
		systemNamespace: systemNamespace,
	}
	return rolesREST
}

func (r RolesREST) GetRolesMappingFunc(
	getter dependency.Manager, context context.Context, name string, listOptions *devops.RoleMappingListOptions) (
	*devops.RoleMapping, error) {

	client, err := r.getCodeQualityClient(getter, context, name)
	if err != nil {
		return nil, err
	}

	roleMapping, err := client.GetRolesMapping(listOptions)
	if err != nil {
		glog.Errorf("Error Get CodeQualityTool roles mapping,err:%#v,  toolname:%s, listoptions:%#v ", err, name, listOptions)
		return nil, err
	}

	glog.V(7).Infof("Get CodeQualityTool roles mapping, rolemapping:%#v", roleMapping)
	return &roleMapping, err
}

func (r RolesREST) ApplyRolesMappingFunc(
	getter dependency.Manager, context context.Context, name string, roleMapping *devops.RoleMapping) (
	rolemappinglist *devops.RoleMapping, err error) {

	client, err := r.getCodeQualityClient(getter, context, name)
	if err != nil {
		return nil, err
	}

	//ignore the errors , will not interupt the flow
	_ = r.AttchCodeQualityProjectsToRoleMapping(getter, name, roleMapping)
	err = client.ApplyRoleMapping(roleMapping)

	if err != nil {
		glog.Errorf("Error Apply CodeQualityTool rolesmapping,err:%#v,  toolname:%s, roleMapping:%#v ", err, name, *roleMapping)
		return nil, err
	}
	glog.V(7).Infof("Applied CodeQualityTool rolemapping, toolname:%s, roleMapping:%#v ", name, *roleMapping)

	// get current rolemapping
	listOptions := &devops.RoleMappingListOptions{Projects: r.getAllProjects(roleMapping)}
	result, err := client.GetRolesMapping(listOptions)

	return &result, err
}

func (r RolesREST) getAllProjects(roleMapping *devops.RoleMapping) []string {
	projects := make([]string, 0, len(roleMapping.Spec))
	for _, item := range roleMapping.Spec {
		projects = append(projects, item.Project.Name)
	}

	return projects
}

func (r RolesREST) AttchCodeQualityProjectsToRoleMapping(getter dependency.Manager, name string, roleMapping *devops.RoleMapping) error {
	if len(roleMapping.Spec) == 0 {
		return nil
	}

	errs := util.MultiErrors{}

	for i := range roleMapping.Spec {
		projectUsersRole := &roleMapping.Spec[i]
		bindingNamespace := projectUsersRole.Project.Name

		projectKeys, err := r.getCodeQualityProjectKeyList(getter, bindingNamespace, name)
		if err != nil {
			glog.Errorf("Error get codequality project keys list in namespace '%s' of CodeQualityTool '%s'", bindingNamespace, name)
			errs = append(errs, err)
			continue
		}
		if projectUsersRole.Project.Annotations == nil {
			projectUsersRole.Project.Annotations = map[string]string{}
		}
		projectUsersRole.Project.Annotations["codequalityProjectKeys"] = strings.Join(projectKeys, ",")
		glog.V(9).Infof("Attach CodeQualityProjectKeys to projectUsersRole: %s, keys: %s ", projectUsersRole.Project.Name, strings.Join(projectKeys, ","))
	}

	if len(errs) == 0 {
		return nil
	}
	return &errs
}

func (r RolesREST) getCodeQualityProjectKeyList(getter dependency.Manager, namespace string, toolName string) ([]string, error) {
	ctx := k8srequest.WithNamespace(k8srequest.NewDefaultContext(), namespace)
	labelSelector := &metav1.LabelSelector{
		MatchLabels: map[string]string{
			devops.LabelCodeQualityTool: toolName,
		},
	}
	selector, err := metav1.LabelSelectorAsSelector(labelSelector)
	if err != nil {
		glog.Errorf("cannot transfer labelselector %#v to selector , error:%#v", labelSelector, err)
		return []string{}, err
	}

	list, err := getter.List(ctx, devops.TypeCodeQualityProjectList, &internalversion.ListOptions{
		LabelSelector: selector,
	})
	if err != nil {
		glog.Errorf("list codequality project list error:%#v, namespace:%s, toolName:%s", err, namespace, toolName)
		return []string{}, err
	}
	codequalityProjects := list.(*devops.CodeQualityProjectList)

	projectKeys := []string{}
	for _, item := range codequalityProjects.Items {
		if item.Spec.Project.ProjectKey != "" {
			projectKeys = append(projectKeys, item.Spec.Project.ProjectKey)
		}
	}
	return projectKeys, nil
}

func (r RolesREST) getCodeQualityClient(getter dependency.Manager, context context.Context, name string) (
	internalthirdparty.CodeQualityClientInterface, error) {

	tool, secret, err := r.getToolAndSecret(context, name, getter)
	if err != nil {
		glog.Errorf("Error get CodeQuality and secret, error:%s , name:%s", err.Error(), name)
		return nil, err
	}

	configMap, err := r.getDevOpsConfigMap(getter)
	if err != nil {
		glog.Errorf("Error to get devops configmap: %#v ", err)
		return nil, err
	}

	client, err := r.Factory.GetCodeQualityClient(&tool, &secret, devopsthirdparty.BaseOpts{
		ConfigMap: configMap,
	})
	if err != nil {
		glog.Errorf("Error get CodeQualityClient, error:%s , tool:%#v, secret:%#v", err.Error(), tool, secret)
		return nil, err
	}

	return client, nil
}

func (r RolesREST) getDevOpsConfigMap(getter dependency.Manager) (*v1.ConfigMap, error) {
	context := k8srequest.NewContext()
	context = k8srequest.WithNamespace(context, r.systemNamespace)
	depList := getter.Get(context, devops.TypeConfigMap, devops.SettingsConfigMapName)
	if err := depList.Validate(); err != nil {
		return nil, err
	}
	configMap := v1.ConfigMap{}
	depList.GetInto(devops.TypeConfigMap, &configMap)
	// GetInto will not return error if error happened, trick it here
	if configMap.Name == "" {
		return nil, fmt.Errorf("Error to get config map '%s/%s'", r.systemNamespace, devops.SettingsConfigMapName)
	}

	return &configMap, nil
}

func (r RolesREST) getToolAndSecret(context context.Context, name string, getter dependency.Manager) (tool devops.CodeQualityTool, secret v1.Secret, err error) {
	depItemList := getter.Get(context, devops.TypeCodeQualityTool, name)
	if err = depItemList.Validate(); err != nil {
		return tool, secret, err
	}

	depItemList.GetInto(devops.TypeCodeQualityTool, &tool).GetInto(devops.TypeSecret, &secret)
	if tool.Name == "" {
		return tool, secret, fmt.Errorf("error get CodeQualityTool '%s' from dependencyGetter", name)
	}
	if secret.Name == "" {
		return tool, secret, fmt.Errorf("error get Secret of CodeQualityTool '%s' from dependencyGetter", name)
	}
	return tool, secret, err
}
