package rest

import (
	"context"
	"fmt"
	"net/http"

	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/util/k8s"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/registry/devops/codequalitybinding"
)

// SecretREST implements the secret operation for a service
type SecretREST struct {
	CodeQualityToolStore    rest.StandardStorage
	CodeQualityBindingStore rest.StandardStorage
	SecretLister            corev1listers.SecretLister
	ConfigMapLister         corev1listers.ConfigMapLister
	Transport               http.RoundTripper
}

// NewSecretREST starts a new secret store
func NewSecretREST(
	codeQualityToolStore rest.StandardStorage,
	codeQualityBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	configMapLister corev1listers.ConfigMapLister,
	transport http.RoundTripper,
) rest.Storage {
	api := &SecretREST{
		CodeQualityToolStore:    codeQualityToolStore,
		CodeQualityBindingStore: codeQualityBindingStore,
		SecretLister:            secretLister,
		ConfigMapLister:         configMapLister,
		Transport:               transport,
	}
	return api
}

// SecretREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&SecretREST{})

// New creates a new secret options object
func (r *SecretREST) New() runtime.Object {
	return &devops.CodeRepoServiceAuthorizeOptions{}
}

// ProducesObject SecretREST implements StorageMetadata, return CodeRepoServiceAuthorizeOptions as the generating object
func (r *SecretREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.CodeRepoServiceAuthorizeResponse{}
}

// Get retrieves a runtime.Object that will stream the contents of codeQualityTool
func (r *SecretREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		validateOptions  = &devops.CodeRepoServiceAuthorizeOptions{}
		validateResponse = &devops.CodeRepoServiceAuthorizeResponse{}
		secret           *corev1.Secret
		service          *devops.CodeQualityTool
		status           *devops.HostPortStatus
		err              error
	)

	validateOptions, ok := opts.(*devops.CodeRepoServiceAuthorizeOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object:%#v, expect to be ListProjectOptions", opts))
	}

	service, err = codequalitybinding.GetCodeQualityTool(r.CodeQualityToolStore, ctx, name)
	if err != nil {
		return nil, err
	}

	secret, err = r.SecretLister.Secrets(validateOptions.Namespace).Get(validateOptions.SecretName)
	if err != nil {
		return nil, err
	}

	basicAuth := k8s.GetDataBasicAuthFromSecret(secret)
	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(service.Spec.HTTP.Host),
		devopsclient.NewBasicAuth(basicAuth.Username, basicAuth.Password),
		devopsclient.NewTransport(r.Transport),
	)
	serviceClient, err := factory.NewClient(
		service.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	status, err = serviceClient.Authenticate(ctx)
	if err != nil {
		return nil, errors.NewInternalError(err)
	}
	if status != nil && status.StatusCode >= 400 {
		err = errors.NewBadRequest(fmt.Sprintf("username or password in secret '%s' is incorrect", secret.GetName()))
	}
	return validateResponse, err
}

// NewGetOptions creates a new options object
func (r *SecretREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoServiceAuthorizeOptions{}, false, ""
}
