package jenkins_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry/devops/jenkins"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation/field"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestStategyValidate(t *testing.T) {
	Scheme := runtime.NewScheme()

	strategy := jenkins.NewStrategy(Scheme)

	type Table struct {
		name     string
		input    *devops.Jenkins
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	table := []Table{
		{
			name: "empty host",
			input: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "",
						},
					},
				},
			},
			expected: field.ErrorList{
				field.Invalid(
					field.NewPath("spec").Child("http").Child("host"),
					"",
					"provided hostname is not a valid url",
				),
			},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.Validate(nil, obj)
			},
		},
		{
			name: "invalid host",
			input: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "aaa_xxd$",
						},
					},
				},
			},
			expected: field.ErrorList{
				field.Invalid(
					field.NewPath("spec").Child("http").Child("host"),
					"aaa_xxd$",
					"provided hostname is not a valid url",
				),
			},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.Validate(nil, obj)
			},
		},
		{
			name: "valid http host with ip and port",
			input: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "http://10.0.0.1:1234",
						},
					},
				},
			},
			expected: field.ErrorList{},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.Validate(nil, obj)
			},
		},
		{
			name: "valid https host with domain",
			input: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "https://mynewhost.com.cn",
						},
					},
				},
			},
			expected: field.ErrorList{},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.Validate(nil, obj)
			},
		},
		{
			name: "invalid url on update",
			input: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "https://123xxx#.com.cn",
						},
					},
				},
			},
			expected: field.ErrorList{
				field.Invalid(
					field.NewPath("spec").Child("http").Child("host"),
					"https://123xxx#.com.cn",
					"provided hostname is not a valid url",
				),
			},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.ValidateUpdate(nil, obj, obj)
			},
		},
		{
			name: "valid url on update",
			input: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "https://123.com.cn",
						},
					},
				},
			},
			expected: field.ErrorList{},
			method: func(obj runtime.Object) field.ErrorList {
				return strategy.ValidateUpdate(nil, obj, obj)
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: %v != %v",
				i, tst.name,
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						tst.expected, res,
					)
				}
			}
		}
	}

}

func TestStategyPrepareForUpdate(t *testing.T) {
	Scheme := runtime.NewScheme()

	strategy := jenkins.NewStrategy(Scheme)

	type Table struct {
		name     string
		new      *devops.Jenkins
		old      *devops.Jenkins
		expected *devops.Jenkins
	}

	table := []Table{
		{
			name: "no old jenkins",
			new: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "http://test.com",
						},
					},
				},
			},
			old: nil,
			// will be set in the method, should be the same
			expected: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "http://test.com",
						},
					},
				},
				Status: devops.JenkinsStatus{
					ServiceStatus: devops.ServiceStatus{
						Phase:      devops.StatusCreating,
						HTTPStatus: nil,
					},
				},
			},
		},
		{
			name: "same jenkins address",
			new: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "http://test.com",
						},
					},
				},
				Status: devops.JenkinsStatus{
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
					},
				},
			},
			old: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "http://test.com",
						},
					},
				},
				Status: devops.JenkinsStatus{
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
					},
				},
			},
			// will be set in the method, should be the same
			expected: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "http://test.com",
						},
					},
				},
				Status: devops.JenkinsStatus{
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
					},
				},
			},
		},
		{
			name: "address updated",
			new: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "http://newaddress.com",
						},
					},
				},
				Status: devops.JenkinsStatus{
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
						HTTPStatus: &devops.HostPortStatus{
							// content is not important, we will clean up anyway
							StatusCode: 100,
						}},
				},
			},
			old: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "http://test.com",
						},
					},
				},
				Status: devops.JenkinsStatus{
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
					},
				},
			},
			// will be set in the method, should be the same
			expected: &devops.Jenkins{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devops.JenkinsSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "http://newaddress.com",
						},
					},
				},
				Status: devops.JenkinsStatus{
					ServiceStatus: devops.ServiceStatus{
						Phase: devops.StatusReady,
						// should clear http status
						HTTPStatus: nil,
					},
				},
			},
		},
	}

	for i, tst := range table {
		strategy.PrepareForUpdate(nil, tst.new, tst.old)
		// we are only concerned with status here now
		if tst.new.Status.ServiceStatus.Phase != tst.expected.Status.ServiceStatus.Phase {
			t.Errorf(
				"Test %d: \"%v\" - status are different: %v != %v",
				i, tst.name,
				tst.new.Status.Phase, tst.expected.Status.Phase,
			)
		}
	}

}
