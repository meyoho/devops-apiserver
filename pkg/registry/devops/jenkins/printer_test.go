package jenkins

import (
	"testing"
	"time"

	// mockprinters "alauda.io/devops-apiserver/pkg/mock/printers"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/registry"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

func TestJenkinsPrinterList(t *testing.T) {
	assert := assert.New(t)
	opts := printers.PrintOptions{Wide: true}
	now := metav1.NewTime(time.Now().Add(-time.Hour))
	jenkins := &devops.Jenkins{
		ObjectMeta: metav1.ObjectMeta{
			Name:              "jenkins-1",
			CreationTimestamp: now,
		},
		Spec: devops.JenkinsSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "http://jenkins.io",
				},
			},
		},
		Status: devops.JenkinsStatus{
			ServiceStatus: devops.ServiceStatus{
				Phase: devops.StatusReady,
			},
		},
	}
	list := &devops.JenkinsList{
		Items: []devops.Jenkins{*jenkins},
	}

	expected := []metav1alpha1.TableRow{
		metav1alpha1.TableRow{
			Object: runtime.RawExtension{Object: jenkins},
			Cells: []interface{}{
				"jenkins-1", jenkins.Spec.HTTP.Host, jenkins.Status.Phase, registry.TranslateTimestamp(now),
			},
		},
	}

	result, err := printJenkinsList(list, opts)
	assert.EqualValuesf(nil, err, "Should not fail printing jenkins: %v", err)
	assert.EqualValuesf(expected, result, "result is not equal to expected: %v != %v", expected, result)
}
