package rest

import (
	"context"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/controller/generic"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	"alauda.io/devops-apiserver/pkg/registry/devops/jenkinsbinding"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
	corev1listers "k8s.io/client-go/listers/core/v1"
	"k8s.io/klog"
)

// SecretREST implements the secret operation for a service
type SecretREST struct {
	JenkinsStore         *devopsregistry.REST
	JenkinsBindingsStore *devopsregistry.REST
	SecretLister         corev1listers.SecretLister
	ConfigMapLister      corev1listers.ConfigMapLister
	RoundTripper         http.RoundTripper
}

// NewSecretREST starts a new secret store
func NewSecretREST(
	jenkinsStore rest.StandardStorage,
	jenkinsBindingStore rest.StandardStorage,
	secretLister corev1listers.SecretLister,
	configMapLister corev1listers.ConfigMapLister,
	roundTripper http.RoundTripper,
) rest.Storage {
	return &SecretREST{
		JenkinsStore:         jenkinsStore.(*devopsregistry.REST),
		JenkinsBindingsStore: jenkinsBindingStore.(*devopsregistry.REST),
		SecretLister:         secretLister,
		ConfigMapLister:      configMapLister,
		RoundTripper:         roundTripper,
	}
}

// SecretREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&SecretREST{})

// New creates a new secret options object
func (r *SecretREST) New() runtime.Object {
	return &devops.CodeRepoServiceAuthorizeOptions{}
}

// ProducesObject SecretREST implements StorageMetadata, return CodeRepoServiceAuthorizeResponse as the generating object
func (r *SecretREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.CodeRepoServiceAuthorizeResponse{}
}

// Get retrieves a runtime.Object that will stream the contents of jenkins
func (r *SecretREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	var (
		validateOptions  = &devops.CodeRepoServiceAuthorizeOptions{}
		validateResponse = &devops.CodeRepoServiceAuthorizeResponse{}
		secret           *corev1.Secret
		service          *devops.Jenkins
		status           *v1alpha1.HostPortStatus
		err              error
	)

	validateOptions, ok := opts.(*devops.CodeRepoServiceAuthorizeOptions)
	if !ok {
		return nil, fmt.Errorf("invalid options object: %#v", opts)
	}

	service, err = jenkinsbinding.GetJenkins(r.JenkinsStore, ctx, name)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, fmt.Errorf("jenkins '%s' is not found'", name)
		}
		return nil, fmt.Errorf("error getting jenkins '%s', err: %v", name, err)
	}

	secret, err = r.SecretLister.Secrets(validateOptions.Namespace).Get(validateOptions.SecretName)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, fmt.Errorf("secret '%s/%s' is not found'", validateOptions.Namespace, validateOptions.SecretName)
		}
		return nil, fmt.Errorf("error getting secret '%s/%s', err: %v", validateOptions.Namespace, validateOptions.SecretName, err)
	}

	data := k8s.GetDataBasicAuthFromSecret(secret)
	status, err = generic.BasicAuth(service.Spec.HTTP.Host, data.Username, data.Password)
	if err != nil {
		return nil, fmt.Errorf("authorize failed: %v", err)
	}
	if status != nil && status.StatusCode >= 400 {
		klog.Errorf("jenkins basic auth error:%#v, secret name:%s/%s, jenkins:%s", status, secret.Namespace, secret.Name, name)
		err = errors.NewBadRequest(fmt.Sprintf("username or password in secret '%s' is incorrect", secret.GetName()))
		// jenkins is too stupid, it just return 500 when user token is wrong.
		// so, Do not try to set status.StatusCode to err, just hard code 400
	}
	return validateResponse, err
}

// NewGetOptions creates a new options object
func (r *SecretREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoServiceAuthorizeOptions{}, false, ""
}
