/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package jenkins

import (
	"context"
	"fmt"
	"net/url"
	"regexp"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/registry/generic"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/names"
)

const (
	regexURL string = "^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$" // Ref: https://stackoverflow.com/questions/136505/searching-for-uuids-in-text-with-regex
)

var urlregexp = regexp.MustCompile(regexURL)

// NewStrategy creates a new Strategy instance for projects
func NewStrategy(typer runtime.ObjectTyper) Strategy {
	return Strategy{typer, names.SimpleNameGenerator}
}

// GetAttrs get basic attributes for a runtime.Object
func GetAttrs(obj runtime.Object) (labels.Set, fields.Set, bool, error) {
	apiserver, ok := obj.(*devops.Jenkins)
	if !ok {
		return nil, nil, false, fmt.Errorf("given object is not a Jenkins")
	}
	return labels.Set(apiserver.ObjectMeta.Labels), toSelectableFields(apiserver), apiserver.Initializers != nil, nil
}

// MatchJenkins is the filter used by the generic etcd backend to watch events
// from etcd to clients of the apiserver only interested in specific labels/fields.
func MatchJenkins(label labels.Selector, field fields.Selector) storage.SelectionPredicate {
	return storage.SelectionPredicate{
		Label:    label,
		Field:    field,
		GetAttrs: GetAttrs,
	}
}

// toSelectableFields returns a field set that represents the object.
func toSelectableFields(obj *devops.Jenkins) fields.Set {
	return generic.ObjectMetaFieldsSet(&obj.ObjectMeta, true)
}

// Strategy jenkins strategy
type Strategy struct {
	runtime.ObjectTyper
	names.NameGenerator
}

// NamespaceScoped returns false
func (Strategy) NamespaceScoped() bool {
	return false
}

// PrepareForCreate adds basic fields for creation
func (Strategy) PrepareForCreate(ctx context.Context, obj runtime.Object) {
	jenkins, ok := obj.(*devops.Jenkins)
	if ok {
		jenkins.Status.Phase = devops.StatusCreating
		jenkins.Status.HTTPStatus = nil
	}
}

// PrepareForUpdate prepares an object for update request
func (Strategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) {
	var (
		newJenkins, oldJenkins *devops.Jenkins
	)
	newJenkins, _ = obj.(*devops.Jenkins)
	if old != nil {
		oldJenkins, _ = old.(*devops.Jenkins)
	}
	if newJenkins != nil && newJenkins.Status.Phase.IsInvalid() {
		newJenkins.Status.Phase = devops.StatusCreating
	}
	// first check if the address is different
	// or if this is a first attempt
	if isChanged(oldJenkins, newJenkins) {
		newJenkins.Status.HTTPStatus = nil
		newJenkins.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	}
}

// Validate validates an request object
func (Strategy) Validate(ctx context.Context, obj runtime.Object) (errs field.ErrorList) {
	errs = field.ErrorList{}
	jenkins, ok := obj.(*devops.Jenkins)
	if ok {
		errs = append(errs, validateHTTP(jenkins)...)
	}
	return
}

// AllowCreateOnUpdate returns false
func (Strategy) AllowCreateOnUpdate() bool {
	return false
}

// AllowUnconditionalUpdate returns false
func (Strategy) AllowUnconditionalUpdate() bool {
	return false
}

// Canonicalize preapres object to save
func (Strategy) Canonicalize(obj runtime.Object) {
}

// ValidateUpdate validates an update request
func (Strategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) (errs field.ErrorList) {
	errs = field.ErrorList{}
	jenkins, ok := obj.(*devops.Jenkins)
	if ok {
		errs = append(errs, validateHTTP(jenkins)...)
	}
	return
}

func isChanged(old, new *devops.Jenkins) bool {
	if old == nil || new == nil {
		return true
	}
	return !apiequality.Semantic.DeepEqual(old.Spec, new.Spec)
}

func validateHTTP(jenkins *devops.Jenkins) (errs field.ErrorList) {
	errs = field.ErrorList{}
	_, err := url.Parse(jenkins.Spec.HTTP.Host)
	if err != nil || jenkins.Spec.HTTP.Host == "" || !urlregexp.MatchString(jenkins.Spec.HTTP.Host) {
		errs = append(errs, field.Invalid(
			field.NewPath("spec").Child("http").Child("host"),
			jenkins.Spec.HTTP.Host,
			"provided hostname is not a valid url",
		))
	}
	return
}
