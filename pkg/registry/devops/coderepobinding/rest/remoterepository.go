package rest

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"

	"alauda.io/devops-apiserver/pkg/dependency"
	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/util/k8s"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/runtime"
	glog "k8s.io/klog"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apiserver/pkg/registry/rest"
)

var (
	sessionStore map[string]*CodeRepoServiceSession
)

func GetSessionStore() map[string]*CodeRepoServiceSession {
	if sessionStore == nil {
		sessionStore = map[string]*CodeRepoServiceSession{}
	}
	return sessionStore
}

type CodeRepoServiceSession struct {
	LastModifiedAt              time.Time                           `json:"lastModifiedAt"`
	CodeRepoBindingRepositories *devops.CodeRepoBindingRepositories `json:"codeRepoBindingRepositories"`
}

func (s *CodeRepoServiceSession) IsExpired() bool {
	return s.LastModifiedAt.Add(devops.TTLSession).Before(time.Now())
}

// RepositoryREST implements the log endpoint for a Pod
type RepositoryREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

// NewRepositoryREST starts a new log store
func NewRepositoryREST(
	getter dependency.Manager,
	roundTripper http.RoundTripper,
) rest.Storage {
	return &RepositoryREST{
		Getter:       getter,
		RoundTripper: roundTripper,
	}
}

// RepositoryREST implements GetterWithOptions
var _ = rest.GetterWithOptions(&RepositoryREST{})

// New creates a new Pod log options object
func (r *RepositoryREST) New() runtime.Object {
	return &devops.CodeRepoBindingRepositories{}
}

// ProducesObject SecretREST implements StorageMetadata, return CodeRepoBindingRepositories as the generating object
func (r *RepositoryREST) ProducesObject(verb string) interface{} {
	return &v1alpha1.CodeRepoBindingRepositories{}
}

// Get retrieves a runtime.Object that will stream the contents of codeRepoService
func (r *RepositoryREST) Get(ctx context.Context, name string, opts runtime.Object) (runtime.Object, error) {
	dependencies := r.Getter.Get(ctx, devops.TypeCodeRepoBinding, name)
	if err := dependencies.Validate(); err != nil {
		return nil, err
	}

	codeRepoBinding := &devops.CodeRepoBinding{}
	codeRepoService := &devops.CodeRepoService{}
	secret := &corev1.Secret{}
	dependencies.GetInto(devops.TypeCodeRepoBinding, codeRepoBinding).GetInto(devops.TypeCodeRepoService, codeRepoService).GetInto(devops.TypeSecret, secret)

	var sessionKey string
	if secret.Type == v1alpha1.SecretTypeOAuth2 {
		sessionKey = fmt.Sprintf("%s-%s",
			codeRepoService.Name,
			k8s.GetValueInSecret(secret, v1alpha1.OAuth2AccessTokenKey))
	} else {
		sessionKey = fmt.Sprintf("%s-%s-%s",
			codeRepoService.Name,
			k8s.GetValueInSecret(secret, corev1.BasicAuthUsernameKey),
			k8s.GetValueInSecret(secret, corev1.BasicAuthPasswordKey))
	}
	ss := GetSessionStore()
	if session, ok := ss[sessionKey]; ok && !session.IsExpired() {
		glog.Infof("Get info from session(sessionKey: %s)", sessionKey)
		return session.CodeRepoBindingRepositories, nil
	}

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(codeRepoService.GetHost()),
		devopsclient.NewGenericOpts(codeRepoService, secret),
		devopsclient.NewTransport(r.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		codeRepoService.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, err
	}

	OriginCodeRepo, err := serviceClient.GetRemoteRepos(ctx)
	if err != nil {
		return OriginCodeRepo, err
	}

	ss[sessionKey] = &CodeRepoServiceSession{
		CodeRepoBindingRepositories: OriginCodeRepo,
		LastModifiedAt:              time.Now(),
	}
	return OriginCodeRepo, nil
}

// NewGetOptions creates a new options object
func (r *RepositoryREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoBindingRepositoryOptions{}, false, ""
}
