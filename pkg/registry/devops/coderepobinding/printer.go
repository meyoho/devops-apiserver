package coderepobinding

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// ColumnDefinitions column definitions for PipelineConfig
var columnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
	{Name: "CodeRepoService", Type: registry.TableTypeString, Description: "CodeRepoService instance name"},
	{Name: registry.ResourceSecret, Type: registry.TableTypeString, Description: "Secret name used for accessing"},
	{Name: "Synced Accounts", Type: "number", Description: "Number of accounts synced"},
	{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "Status of the connectivity to the CodeRepoBinding instance"},
	{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
}

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {

	h.TableHandler(columnDefinitions, printCodeRepoBindingList)
	h.TableHandler(columnDefinitions, printCodeRepoBinding)

	registry.AddDefaultHandlers(h)

}

// printCodeRepoBindingList prints a codeRepoService list
func printCodeRepoBindingList(codeRepoServiceList *devops.CodeRepoBindingList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(codeRepoServiceList.Items))
	for i := range codeRepoServiceList.Items {
		r, err := printCodeRepoBinding(&codeRepoServiceList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

// printCodeRepoBinding prints a codeRepoService
func printCodeRepoBinding(codeRepoService *devops.CodeRepoBinding, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: codeRepoService},
	}

	// basic rows
	row.Cells = append(row.Cells, codeRepoService.Name, codeRepoService.Spec.CodeRepoService.Name, codeRepoService.Spec.Account.Secret.Name, len(codeRepoService.Spec.Account.Owners), codeRepoService.Status.Phase, registry.TranslateTimestamp(codeRepoService.CreationTimestamp))

	if options.Wide {
		// for later use
	}

	return []metav1alpha1.TableRow{row}, nil
}
