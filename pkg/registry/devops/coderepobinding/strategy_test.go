package coderepobinding_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/registry/devops/coderepobinding"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

type Table struct {
	name     string
	new      *devopsv1alpha1.CodeRepoBinding
	old      *devopsv1alpha1.CodeRepoBinding
	expected *devopsv1alpha1.CodeRepoBinding
	method   func(*Table)
}

func TestStrategy(t *testing.T) {
	scheme := runtime.NewScheme()
	strategy := coderepobinding.NewStrategy(scheme)

	if !strategy.NamespaceScoped() {
		t.Errorf("CodeRepoBinding strategy should be namespace scoped")
	}
	if strategy.AllowCreateOnUpdate() {
		t.Errorf("CodeRepoBinding strategy should not allow create on update")
	}
	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("CodeRepoBinding strategy should not allow unconditional update")
	}

	table := []Table{
		{
			name: "create: invalid input",
			new: &devopsv1alpha1.CodeRepoBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.CodeRepoBindingSpec{},
				Status:     devopsv1alpha1.ServiceStatus{},
			},
			expected: &devopsv1alpha1.CodeRepoBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.CodeRepoBindingSpec{},
				Status: devopsv1alpha1.ServiceStatus{
					Phase: devopsv1alpha1.StatusCreating,
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input did not generate error: %v",
						tst.name, err,
					)
				}

				validGetAttrs(t, tst)
			},
		},
		{
			name: "create: valid input",
			new: &devopsv1alpha1.CodeRepoBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.CodeRepoBindingSpec{
					CodeRepoService: devopsv1alpha1.LocalObjectReference{
						Name: "github",
					},
					Account: devopsv1alpha1.CodeRepoBindingAccount{
						Secret: devopsv1alpha1.SecretKeySetRef{
							SecretReference: corev1.SecretReference{
								Name: "secret1",
							},
						},
					},
				},
				Status: devopsv1alpha1.ServiceStatus{},
			},
			expected: &devopsv1alpha1.CodeRepoBinding{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.CodeRepoBindingSpec{
					CodeRepoService: devopsv1alpha1.LocalObjectReference{
						Name: "github",
					},
					Account: devopsv1alpha1.CodeRepoBindingAccount{
						Secret: devopsv1alpha1.SecretKeySetRef{
							SecretReference: corev1.SecretReference{
								Name: "secret1",
							},
						},
					},
				},
				Status: devopsv1alpha1.ServiceStatus{
					Phase: devopsv1alpha1.StatusCreating,
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) != 0 {
					t.Errorf(
						"Test: \"%s\" - Validate valid input should not generate error: %v",
						tst.name, err,
					)
				}

				validGetAttrs(t, tst)
			},
		},
	}

	for i, tst := range table {
		tst.method(&tst)

		if tst.new.Status.Phase != tst.expected.Status.Phase {
			t.Errorf(
				"Test %d: \"%v\" - status.phase are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
	}

}

func validGetAttrs(t *testing.T, tst *Table) {
	_, _, _, getErr := coderepobinding.GetAttrs(tst.new)
	if getErr != nil {
		t.Errorf(
			"Test: \"%s\" - GetAttr return err for object: %v",
			tst.name, getErr,
		)
	}
}
