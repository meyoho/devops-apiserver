package rest

import (
	"context"
	"fmt"
	"net/http"
	"sort"
	"strings"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"

	devopsclient "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/registry/rest"
)

var _ = rest.GetterWithOptions(&RepoBranchREST{})

type RepoBranchREST struct {
	Getter       dependency.Manager
	RoundTripper http.RoundTripper
}

func NewRepoBranchREST(
	getter dependency.Manager,
	roundTripper http.RoundTripper,
) rest.Storage {
	return &RepoBranchREST{
		Getter:       getter,
		RoundTripper: roundTripper,
	}
}

func (r *RepoBranchREST) New() runtime.Object {
	return &devops.CodeRepoBranchOptions{}
}

func (r *RepoBranchREST) Get(ctx context.Context, name string, options runtime.Object) (runtime.Object, error) {
	var (
		err error
	)

	opt, ok := options.(*devops.CodeRepoBranchOptions)
	if !ok {
		return nil, errors.NewBadRequest(fmt.Sprintf("invalid options object: %#v", opt))
	}

	dependencies := r.Getter.Get(ctx, devops.TypeCodeRepository, name)
	if err = dependencies.Validate(); err != nil {
		return nil, err
	}

	repository := &devops.CodeRepository{}
	repoService := &devops.CodeRepoService{}
	secret := &corev1.Secret{}

	dependencies.GetInto(devops.TypeCodeRepository, repository).GetInto(devops.TypeCodeRepoService, repoService).GetInto(devops.TypeSecret, secret)

	clientOpts := devopsclient.NewOptions(
		devopsclient.NewURL(repoService.GetHost()),
		devopsclient.NewGenericOpts(repoService, secret),
		devopsclient.NewTransport(r.RoundTripper),
	)
	serviceClient, err := factory.NewClient(
		repoService.Spec.Type.String(),
		"",
		clientOpts,
	)
	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get CodeRepoServiceClient from CodeRepoService: %s, err: %v", serviceClient, err))
	}
	branches, err := serviceClient.GetBranches(ctx, repository.GetOwnerName(), repository.GetRepoName(), repository.GetRepoFullName())

	if err != nil {
		return nil, errors.NewInternalError(fmt.Errorf("error get branches from name/repo: %s/%s, repofullname: %s, err: %v", repository.GetOwnerName(), repository.GetRepoName(), repository.GetRepoFullName(), err))
	}

	sort.Slice(branches, func(i, j int) bool {
		if strings.ToLower(branches[j].Name) == "master" {
			return false
		}
		if strings.ToLower(branches[i].Name) == "master" {
			return true
		}
		mode := opt.SortMode != devops.ParameterSortModeDescending
		// String sorting rules:
		// a < b True
		// a < A False
		// a < aa True
		return (branches[i].Name > branches[j].Name) == mode
	})

	return &devops.CodeRepoBranchResult{Branches: branches}, nil
}

func (t *RepoBranchREST) NewGetOptions() (runtime.Object, bool, string) {
	return &devops.CodeRepoBranchOptions{}, false, ""
}
