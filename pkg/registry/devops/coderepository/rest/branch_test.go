package rest

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mdependency "alauda.io/devops-apiserver/pkg/mock/dependency"
	mhttp "alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

var githubBranchJSON = `
[
  {
    "name": "release-1.8",
    "commit": {
      "sha": "a654bda3de8174c0a27b098c6f768ee8f47ffbe6",
      "url": "https://api.github.com/repos/kubernetes/kubernetes/commits/a654bda3de8174c0a27b098c6f768ee8f47ffbe6"
    },
    "protected": true
  },
  {
    "name": "release-1.9",
    "commit": {
      "sha": "b5ec061c7ab9995a801206ea74f614ced04206d2",
      "url": "https://api.github.com/repos/kubernetes/kubernetes/commits/b5ec061c7ab9995a801206ea74f614ced04206d2"
    },
    "protected": true
  }
]
`

var giteeBranchJSON = `
`

func TestBranch(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	roundTripper := mhttp.NewMockRoundTripper(ctrl)
	dependencyGetter := mdependency.NewMockManager(ctrl)
	repoBranchREST := &RepoBranchREST{
		Getter:       dependencyGetter,
		RoundTripper: roundTripper,
	}

	type TestCase struct {
		Name            string
		CodeRepository  func() (*devops.CodeRepository, error)
		CodeRepoService func() (*devops.CodeRepoService, error)
		Secret          func() (*corev1.Secret, error)
		Prepare         func() (ctx context.Context, name string, options runtime.Object)
		Run             func(ctx context.Context, name string, options runtime.Object) (runtime.Object, error)
		Expect          []devops.CodeRepoBranch
		Err             error
	}
	table := []TestCase{
		{
			Name: "github 200",
			CodeRepository: func() (*devops.CodeRepository, error) {
				return &devops.CodeRepository{
					ObjectMeta: metav1.ObjectMeta{
						Namespace: "test-ns",
						Name:      "test",
					},
					Spec: devops.CodeRepositorySpec{
						CodeRepoBinding: devops.LocalObjectReference{
							Name: "test",
						},
						Repository: devops.OriginCodeRepository{
							CodeRepoServiceType: devops.CodeRepoServiceTypeGithub,
							FullName:            "owner/repo",
							HTMLURL:             "https://github.com/owner/repo",
							Name:                "repo",
							Owner: devops.OwnerInRepository{
								Type: devops.OriginCodeRepoOwnerTypeUser,
								ID:   "123456",
								Name: "owner",
							},
						},
					},
				}, nil
			},
			CodeRepoService: func() (*devops.CodeRepoService, error) {
				return &devops.CodeRepoService{
					ObjectMeta: metav1.ObjectMeta{
						Namespace: "test-ns",
						Name:      "test",
						Labels: map[string]string{
							devops.LabelToolItemKind: string(devops.CodeRepoServiceTypeGithub),
							devops.LabelToolItemType: string(devops.CodeRepoServiceTypeGithub),
						},
					},
					Spec: devops.CodeRepoServiceSpec{
						Type: devops.CodeRepoServiceTypeGithub,
						ToolSpec: devops.ToolSpec{
							HTTP: devops.HostPort{
								Host:      "https://api.github.com/",
								AccessURL: "https://github.com/",
							},
							Secret: devops.SecretKeySetRef{
								SecretReference: corev1.SecretReference{
									Namespace: "global-credentials",
									Name:      "github-secret",
								},
							},
						},
					},
				}, nil
			},
			Secret: func() (*corev1.Secret, error) {
				return &corev1.Secret{
					ObjectMeta: metav1.ObjectMeta{
						Namespace: "global-credentials",
						Name:      "github-secret",
					},
					Type: corev1.SecretTypeBasicAuth,
				}, nil
			},
			Prepare: func() (ctx context.Context, name string, opts runtime.Object) {
				ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), "test")
				name = "test"
				opts = &devops.CodeRepoBranchOptions{
					SortMode: devops.ParameterSortModeDescending,
					SortBy:   devops.ParameterSortByNatural,
				}
				requestURL, _ := url.Parse("https://api.github.com/repos/owner/repo/branches?page=1&per_page=100")
				request, _ := http.NewRequest(http.MethodGet,
					requestURL.RequestURI(), nil)
				request.URL = requestURL
				request.Header.Add("Accept", "*/*")

				response := &http.Response{
					Status:     "200 OK",
					StatusCode: http.StatusOK,
					Proto:      "HTTP/1.1",
					ProtoMajor: 1,
					ProtoMinor: 0,
					Request:    request,
					Header: http.Header{
						"Link": []string{`<https://api.github.com/repositories/20580498/branches?page=1>; rel="prev", <https://api.github.com/repositories/20580498/branches?page=1>; rel="first"`},
					},
					Body: ioutil.NopCloser(bytes.NewBuffer([]byte(githubBranchJSON))),
				}
				response.Header.Set("X-RateLimit-Limit", "5000")
				response.Header.Set("X-RateLimit-Remaining", "4999")
				response.Header.Set("X-RateLimit-Reset", "1561014258")
				roundTripper.EXPECT().
					RoundTrip(mhttp.NewRequestMatcher(request)).
					Return(response, nil)
				return
			},
			Run: func(ctx context.Context, name string, options runtime.Object) (runtime.Object, error) {
				return repoBranchREST.Get(ctx, name, options)
			},
			Expect: []devops.CodeRepoBranch{
				devops.CodeRepoBranch{Name: "release-1.8", Commit: "a654bda3de8174c0a27b098c6f768ee8f47ffbe6"},
				devops.CodeRepoBranch{Name: "release-1.9", Commit: "b5ec061c7ab9995a801206ea74f614ced04206d2"},
			},
			Err: nil,
		},
	}

	for i, test := range table {
		ctx, name, opts := test.Prepare()

		repoSitory, _ := test.CodeRepository()
		repoRepoService, _ := test.CodeRepoService()
		secret, _ := test.Secret()
		dependencyGetter.EXPECT().
			Get(ctx, devops.TypeCodeRepository, name).
			Return(dependency.ItemList{
				&dependency.Item{
					Kind:     devops.TypeCodeRepository,
					Object:   repoSitory,
					IntoFunc: dependency.CodeRepositoryCopyInto,
				},
				&dependency.Item{
					Kind:     devops.TypeCodeRepoService,
					Object:   repoRepoService,
					IntoFunc: dependency.CodeRepoServiceCopyInto,
				},
				&dependency.Item{
					Kind:     devops.TypeSecret,
					Object:   secret,
					IntoFunc: dependency.CoreV1SecretCopyInto,
				},
			})

		result, err := test.Run(ctx, name, opts)
		if err == nil && test.Err != nil {
			t.Errorf("[%d] %s: Should fail but didn't: %v", i, test.Name, test.Err)
		} else if err != nil && test.Err == nil {
			t.Errorf("[%d] %s: Failed but shouldn't: %v", i, test.Name, err)
		}
		preview, ok := result.(*devops.CodeRepoBranchResult)
		if !ok {
			t.Errorf("[%d] %s invalid type, should be devops.CodeRepoBranchResult.", i, test.Name)
		} else if !reflect.DeepEqual(preview.Branches, test.Expect) {
			t.Errorf("got: [%v], want: [%v]", preview.Branches, test.Expect)
		}
	}
}
