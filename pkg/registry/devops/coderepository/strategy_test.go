package coderepository_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/registry/devops/coderepository"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

type Table struct {
	name     string
	new      *devopsv1alpha1.CodeRepository
	old      *devopsv1alpha1.CodeRepository
	expected *devopsv1alpha1.CodeRepository
	method   func(*Table)
}

func TestStrategy(t *testing.T) {
	scheme := runtime.NewScheme()
	strategy := coderepository.NewStrategy(scheme)

	if !strategy.NamespaceScoped() {
		t.Errorf("coderepository strategy should be namespace scoped")
	}
	if strategy.AllowCreateOnUpdate() {
		t.Errorf("coderepository strategy should not allow create on update")
	}
	if strategy.AllowUnconditionalUpdate() {
		t.Errorf("coderepository strategy should not allow unconditional update")
	}

	table := []Table{
		{
			name: "create: invalid input",
			new: &devopsv1alpha1.CodeRepository{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.CodeRepositorySpec{},
			},
			expected: &devopsv1alpha1.CodeRepository{
				ObjectMeta: metav1.ObjectMeta{},
				Spec:       devopsv1alpha1.CodeRepositorySpec{},
				Status: devopsv1alpha1.CodeRepositoryStatus{
					ServiceStatus: devopsv1alpha1.ServiceStatus{
						Phase: devopsv1alpha1.StatusReady,
					},
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) == 0 {
					t.Errorf(
						"Test: \"%s\" - Validate invalid input did not generate error: %v",
						tst.name, err,
					)
				}

				validGetAttrs(t, tst)
			},
		},
		{
			name: "create: valid input",
			new: &devopsv1alpha1.CodeRepository{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.CodeRepositorySpec{
					CodeRepoBinding: devopsv1alpha1.LocalObjectReference{
						Name: "binding",
					},
					Repository: devopsv1alpha1.OriginCodeRepository{
						CodeRepoServiceType: devopsv1alpha1.CodeRepoServiceTypeGithub,
					},
				},
			},
			expected: &devopsv1alpha1.CodeRepository{
				ObjectMeta: metav1.ObjectMeta{},
				Spec: devopsv1alpha1.CodeRepositorySpec{
					CodeRepoBinding: devopsv1alpha1.LocalObjectReference{
						Name: "binding",
					},
					Repository: devopsv1alpha1.OriginCodeRepository{
						CodeRepoServiceType: devopsv1alpha1.CodeRepoServiceTypeGithub,
					},
				},
				Status: devopsv1alpha1.CodeRepositoryStatus{
					ServiceStatus: devopsv1alpha1.ServiceStatus{
						Phase: devopsv1alpha1.StatusReady,
					},
				},
			},
			method: func(tst *Table) {
				strategy.PrepareForCreate(nil, tst.new)

				err := strategy.Validate(nil, tst.new)
				if len(err) != 0 {
					t.Errorf(
						"Test: \"%s\" - Validate valid input should not generate error: %v",
						tst.name, err,
					)
				}

				validGetAttrs(t, tst)
			},
		},
	}

	for i, tst := range table {
		tst.method(&tst)

		if tst.new.Status.Phase != tst.expected.Status.Phase {
			t.Errorf(
				"Test %d: \"%v\" - status.phase are different: %v != %v",
				i, tst.name,
				tst.new.Status, tst.expected.Status,
			)
		}
	}

}

func validGetAttrs(t *testing.T, tst *Table) {
	_, _, _, getErr := coderepository.GetAttrs(tst.new)
	if getErr != nil {
		t.Errorf(
			"Test: \"%s\" - GetAttr return err for object: %v",
			tst.name, getErr,
		)
	}
}
