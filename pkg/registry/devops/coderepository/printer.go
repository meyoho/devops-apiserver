package coderepository

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	registry "alauda.io/devops-apiserver/pkg/registry"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/kubernetes/pkg/printers"
)

// AddHandlers handlers for printer in table conversion
// see: https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L78
func AddHandlers(h printers.PrintHandler) {
	columnDefinitions := []metav1alpha1.TableColumnDefinition{
		{Name: registry.ObjectMetaName, Type: registry.TableTypeString, Format: registry.ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaNameKey]},
		{Name: registry.RepositoryRepository, Type: registry.TableTypeString, Description: "Repository"},
		{Name: registry.ResourceStatus, Type: registry.TableTypeString, Description: "The State of the codeRepository."},
		{Name: registry.ObjectMetaCreationTimestampAge, Type: registry.TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[registry.ObjectMetaCreationTimestampKey]},
		{Name: "Last Commit", Type: registry.TableTypeString, Priority: 1, Description: "Last Commit"},
		{Name: "CodeRepoService", Type: registry.TableTypeString, Priority: 1, Description: "CodeRepoService instance name"},
	}

	h.TableHandler(columnDefinitions, printCodeRepositoryList)
	h.TableHandler(columnDefinitions, printCodeRepository)

	registry.AddDefaultHandlers(h)

}

func printCodeRepositoryList(codeRepositoryList *devops.CodeRepositoryList, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	rows := make([]metav1alpha1.TableRow, 0, len(codeRepositoryList.Items))
	for i := range codeRepositoryList.Items {
		r, err := printCodeRepository(&codeRepositoryList.Items[i], options)
		if err != nil {
			return nil, err
		}
		rows = append(rows, r...)
	}
	return rows, nil
}

func printCodeRepository(codeRepository *devops.CodeRepository, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {

	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: codeRepository},
	}

	row.Cells = append(row.Cells, codeRepository.Name, codeRepository.GetRepoFullName(), codeRepository.Status.Phase, registry.TranslateTimestamp(codeRepository.CreationTimestamp))
	if options.Wide {
		// adding to rows
		row.Cells = append(row.Cells, codeRepository.Status.Repository.LatestCommit.CommitID, codeRepository.GetServiceName())
	}
	return []metav1alpha1.TableRow{row}, nil
}
