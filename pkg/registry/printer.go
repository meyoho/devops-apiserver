package registry

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"time"

	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1alpha1 "k8s.io/apimachinery/pkg/apis/meta/v1beta1"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/duration"
	"k8s.io/kubernetes/pkg/printers"
)

// TranslateTimestamp returns the elapsed time since timestamp in
// human-readable approximation.
func TranslateTimestamp(timestamp metav1.Time) string {
	if timestamp.IsZero() {
		return "<unknown>"
	}
	return duration.HumanDuration(time.Now().Sub(timestamp.Time))
}

// ObjectMetaColumnDefinitions simple ObjectMeta definitions
var ObjectMetaColumnDefinitions = []metav1alpha1.TableColumnDefinition{
	{Name: ObjectMetaName, Type: TableTypeString, Format: ObjectMetaNameKey, Description: metav1.ObjectMeta{}.SwaggerDoc()[ObjectMetaNameKey]},
	{Name: ObjectMetaCreationTimestampAge, Type: TableTypeString, Description: metav1.ObjectMeta{}.SwaggerDoc()[ObjectMetaCreationTimestampKey]},
}

// AddDefaultHandlers add default printers
func AddDefaultHandlers(h printers.PrintHandler) {
	// types without defined columns
	h.DefaultTableHandler(ObjectMetaColumnDefinitions, PrintObjectMeta)
}

// PrintObjectMeta will print a name and created timestamp for objects
func PrintObjectMeta(obj runtime.Object, options printers.PrintOptions) ([]metav1alpha1.TableRow, error) {
	if meta.IsListType(obj) {
		rows := make([]metav1alpha1.TableRow, 0, 16)
		err := meta.EachListItem(obj, func(obj runtime.Object) error {
			nestedRows, err := PrintObjectMeta(obj, options)
			if err != nil {
				return err
			}
			rows = append(rows, nestedRows...)
			return nil
		})
		if err != nil {
			return nil, err
		}
		return rows, nil
	}

	rows := make([]metav1alpha1.TableRow, 0, 1)
	m, err := meta.Accessor(obj)
	if err != nil {
		return nil, err
	}
	row := metav1alpha1.TableRow{
		Object: runtime.RawExtension{Object: obj},
	}
	row.Cells = append(row.Cells, m.GetName(), TranslateTimestamp(m.GetCreationTimestamp()))
	rows = append(rows, row)
	return rows, nil
}

// GetPipelineNames returns pipeline names from objectMeta
func GetPipelineNames(pipeline metav1.ObjectMeta) (displayName, version, category string) {
	displayName = pipeline.Name
	version = "-"
	name := ""
	ok := false

	if len(pipeline.Annotations) > 0 {
		if name, ok = pipeline.Annotations[devops.AnnotationsKeyDisplayNameEn]; ok && name != "" {
			displayName = name
		} else if name, ok = pipeline.Annotations[devops.AnnotationsKeyDisplayNameZhCN]; ok && name != "" {
			displayName = name
		}
		if name, ok = pipeline.Annotations[devops.AnnotationsTemplateVersion]; ok && name != "" {
			version = name
		}
	}

	// getting category
	category = "-"
	if len(pipeline.Labels) > 0 {
		name, ok := pipeline.Labels["category"]
		if ok && name != "" {
			category = name
		}

	}
	return
}
