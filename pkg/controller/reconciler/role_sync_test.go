package reconciler_test

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"encoding/json"

	"alauda.io/devops-apiserver/pkg/role"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsfake "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	aceclient "alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	"alauda.io/devops-apiserver/pkg/mock/ace"
	"alauda.io/devops-apiserver/pkg/mock/devops/controller"
	"alauda.io/devops-apiserver/pkg/mock/thirdparty/external"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var (
	configmapName      = v1alpha1.SettingsConfigMapName
	configmapNamespace = "alauda-system"
	roleSyncKey        = v1alpha1.SettingsKeyRoleMapping
)

var _ = Describe("RoleSyncReconciler.Reconcile", func() {
	var (
		ctrl                 *gomock.Controller
		managerMock          *controller.MockManager
		alaudaFactoryMock    *external.MockAlaudaClientFactory
		thirdpartyMock       *external.MockInterface
		aceClientMock        *ace.MockInterface
		k8sclient            *k8sfake.Clientset
		devopsclient         *devopsfake.Clientset
		roleReconciler       *reconciler.RoleSyncReconciler
		opts                 reconciler.RoleSyncOptions
		result               reconcile.Result
		source               = "ace"
		toolType             = "test"
		systemNamespace      = "alauda-system"
		credentialsNamespace = "global-credentials"
		extraConfig          = manager.ExtraConfig{
			SystemNamespace:      systemNamespace,
			CredentialsNamespace: credentialsNamespace,
		}
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		managerMock = controller.NewMockManager(ctrl)
		alaudaFactoryMock = external.NewMockAlaudaClientFactory(ctrl)
		aceClientMock = ace.NewMockInterface(ctrl)
		thirdpartyMock = external.NewMockInterface(ctrl)

	})

	AfterEach(func() {
		ctrl.Finish()
	})

	It("should run the whole reconciling function", func() {

		toolSchema := v1alpha1.RoleSyncScheme
		toolSchema.SetSource(source)
		toolSchema.AddPlatform(
			role.NewPlatform(toolType).
				SetEnabled(true).
				SetSyncType(role.SyncTypeRBACMutualExclusive).
				SetRoleMapping(map[string]string{
					"namespace_admin":     "dev_admin",
					"namespace_auditor":   "dev_auditor",
					"namespace_developer": "dev_developer",
					"project_admin":       "admin",
					"project_auditor":     "auditor",
					"space_admin":         "dev_admin",
					"space_auditor":       "dev_auditor",
					"space_developer":     "dev_developer",
				}),
		)
		schemeJson, _ := json.Marshal(toolSchema)
		// basic data
		configmap := &corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      configmapName,
				Namespace: configmapNamespace,
			},
			Data: map[string]string{
				roleSyncKey: string(schemeJson),
			},
		}
		namespaceDev := &corev1.Namespace{ObjectMeta: metav1.ObjectMeta{
			Name: "dev", Annotations: map[string]string{
				v1alpha1.AnnotationsKeyProject: "dev",
			},
		}}
		namespaceTest := &corev1.Namespace{ObjectMeta: metav1.ObjectMeta{
			Name: "test", Annotations: map[string]string{
				v1alpha1.AnnotationsKeyProject: "test",
			},
		}}

		// preparing k8s clients
		k8sclient = k8sfake.NewSimpleClientset(configmap, namespaceDev, namespaceTest)
		devopsclient = devopsfake.NewSimpleClientset()

		// preparing manager
		managerMock.EXPECT().GetKubeClient().Return(k8sclient)
		managerMock.EXPECT().GetDevOpsClient().Return(devopsclient).AnyTimes()
		managerMock.EXPECT().GetThirdParty().Return(thirdpartyMock)
		managerMock.EXPECT().GetExtraConfig().Return(extraConfig)

		thirdpartyMock.EXPECT().AlaudaFactory().Return(alaudaFactoryMock)
		alaudaFactoryMock.EXPECT().Client(source, configmap).Return(thirdparty.NewACEClient(aceClientMock, devops.NewAnnotationProvider(devops.UsedBaseDomain)), nil)

		opts = reconciler.RoleSyncOptions{
			Name: toolType,
			GetToolData: func(request reconcile.Request) (data reconciler.ToolData, err error) {
				data = reconciler.ToolData{
					ToolType: toolType,
					// service instance
					ToolInstance: &v1alpha1.Jenkins{
						ObjectMeta: metav1.ObjectMeta{Name: "jenkins"},
					},
					// bindings (only for help in the future)
					Bindings: []runtime.Object{
						&v1alpha1.JenkinsBinding{
							ObjectMeta: metav1.ObjectMeta{
								Name:      "binding",
								Namespace: namespaceDev.Name,
							},
						},
						&v1alpha1.JenkinsBinding{
							ObjectMeta: metav1.ObjectMeta{
								Name:      "binding",
								Namespace: namespaceTest.Name,
							},
						},
					},
					// important data is here, each namespace contains which projects
					NamespaceProjectsMap: map[string][]string{
						namespaceDev.Name:  []string{"dev-dev", "dev-test"},
						namespaceTest.Name: []string{"test-dev"},
					},
				}
				return
			},
			// get current role mapping
			GetRoleMapping: func(data reconciler.ToolData, opts *v1alpha1.RoleMappingListOptions) (result *v1alpha1.RoleMapping, err error) {
				// some basic assertions
				Expect(opts.Projects).ToNot(BeNil(), "get role mapping functon should have projects")
				Expect(opts.Projects).To(ContainElement("dev-dev"), "dev-dev project should be inside the projects options")
				Expect(opts.Projects).To(ContainElement("dev-test"), "dev-test project should be inside the projects options")
				Expect(opts.Projects).To(ContainElement("test-dev"), "test-dev project should be inside the projects options")

				// Tool specific role mapping
				result = &v1alpha1.RoleMapping{
					Spec: []v1alpha1.ProjectUserRoleOperation{
						// first project
						v1alpha1.ProjectUserRoleOperation{
							Project: getProjectData("dev-dev"),
							UserRoleOperations: []v1alpha1.UserRoleOperation{
								// should change to dev_admin
								genUserRoleOperation("devuser", "dev@alauda.io", "dev_auditor"),
								// should add test as auditor
							},
						},
						// second project
						v1alpha1.ProjectUserRoleOperation{
							Project:            getProjectData("dev-test"),
							UserRoleOperations: []v1alpha1.UserRoleOperation{
								// should be the same as dev-dev
							},
						},
						// third project
						v1alpha1.ProjectUserRoleOperation{
							Project: getProjectData("test-dev"),
							UserRoleOperations: []v1alpha1.UserRoleOperation{
								// should have no change
								genUserRoleOperation("testuser", "test@alauda.io", "admin"),
								// should demote to dev_developer
								genUserRoleOperation("anothertest", "anothertest@alauda.io", "auditor"),
								// should ignore this user
								genUserRoleOperation("inexisting", "inexisting@alauda.io", "admin"),
							},
						},
					},
				}
				return
			},
			// apply the change
			ApplyRoleMapping: func(data reconciler.ToolData, mapping *v1alpha1.RoleMapping) (result *v1alpha1.RoleMapping, err error) {
				Expect(mapping).ToNot(BeNil(), "apply mapping should not be nil")

				Expect(mapping.Spec).To(ContainElement(
					WithTransform(func(proj v1alpha1.ProjectUserRoleOperation) bool {
						return proj.Project.Name == "dev-dev" && len(proj.UserRoleOperations) > 0
					}, BeTrue()),
				), "contain project dev-dev")
				Expect(mapping.Spec).To(ContainElement(
					WithTransform(func(proj v1alpha1.ProjectUserRoleOperation) bool {
						return proj.Project.Name == "dev-test" && len(proj.UserRoleOperations) > 0
					}, BeTrue()),
				), "contain project dev-test")
				Expect(mapping.Spec).To(ContainElement(
					WithTransform(func(proj v1alpha1.ProjectUserRoleOperation) bool {
						return proj.Project.Name == "test-dev" && len(proj.UserRoleOperations) > 0
					}, BeTrue()),
				), "contain project test-dev")
				result = mapping
				return
			},
		}

		// setting ace client
		// first namespace
		aceClientMock.EXPECT().
			GetRolesWithUsers(context.TODO(), aceclient.ProjectOpts(namespaceDev.Name)).
			Return(aceclient.RoleUserAssignmentList{
				&aceclient.RoleUserAssignment{
					User:     "devuser",
					Email:    "dev@alauda.io",
					RoleName: "namespace_developer",
				},
				&aceclient.RoleUserAssignment{
					User:     "testuser",
					Email:    "test@alauda.io",
					RoleName: "project_auditor",
				},
			}, nil)

		// second namespace
		aceClientMock.EXPECT().
			GetRolesWithUsers(context.TODO(), aceclient.ProjectOpts(namespaceTest.Name)).
			Return(aceclient.RoleUserAssignmentList{
				&aceclient.RoleUserAssignment{
					User:     "testuser",
					Email:    "test@alauda.io",
					RoleName: "project_admin",
				},
				&aceclient.RoleUserAssignment{
					User:     "anothertest",
					Email:    "anothertest@alauda.io",
					RoleName: "space_developer",
				},
			}, nil)

		rec, err := reconciler.NewRoleSyncReconciler(managerMock, opts)
		Expect(err).To(BeNil(), "should construct a reconciler without error")
		Expect(rec).ToNot(BeNil(), "should construct a reconciler without error")
		roleReconciler = rec.(*reconciler.RoleSyncReconciler)
		Expect(roleReconciler).ToNot(BeNil(), "reconciler should be of RoleSyncReconciler pointer")

		// start reconcile
		request := reconcile.Request{NamespacedName: types.NamespacedName{Namespace: "", Name: "name"}}
		result, err = roleReconciler.Reconcile(request)
		Expect(err).To(BeNil(), "should not return error")
		Expect(result).To(Equal(reconcile.Result{}), "result should be empty struct")

	})
})

func genUserRoleOperation(username, email, rolename string) v1alpha1.UserRoleOperation {
	return v1alpha1.UserRoleOperation{
		User: v1alpha1.UserMeta{
			Username: username,
			Email:    email,
		},
		Role: v1alpha1.RoleMeta{
			Name: rolename,
		},
	}
}

func getProjectData(name string) v1alpha1.ProjectData {
	return v1alpha1.ProjectData{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
	}
}
