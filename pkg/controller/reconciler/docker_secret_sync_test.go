package reconciler_test

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"testing"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"alauda.io/devops-apiserver/pkg/controller/manager"
	//"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsfake "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	acemock "alauda.io/devops-apiserver/pkg/mock/ace"
	"alauda.io/devops-apiserver/pkg/mock/devops/controller"
	"alauda.io/devops-apiserver/pkg/mock/thirdparty/external"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("DockerSecretReconciler.Reconcile", func() {
	var (
		ctrl                   *gomock.Controller
		managerMock            *controller.MockManager
		alaudaFactoryMock      *external.MockAlaudaClientFactory
		thirdpartyMock         *external.MockInterface
		aceClientMock          *acemock.MockInterface
		k8sclient              *k8sfake.Clientset
		devopsclient           *devopsfake.Clientset
		dockerSecretReconciler reconcile.Reconciler
		systemNamespace        = "alauda-system"
		credentialsNamespace   = "global-credentials"
		extraConfig            = manager.ExtraConfig{
			SystemNamespace:      systemNamespace,
			CredentialsNamespace: credentialsNamespace,
		}
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		managerMock = controller.NewMockManager(ctrl)
		alaudaFactoryMock = external.NewMockAlaudaClientFactory(ctrl)
		aceClientMock = acemock.NewMockInterface(ctrl)
		thirdpartyMock = external.NewMockInterface(ctrl)

	})

	JustBeforeEach(func() {
		dockerSecretReconciler = reconciler.NewDockerSecretSyncReconcilerByManager(managerMock)
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	PDescribe("Reconcile to delete dockersecrets synced by imageregistrybinding", func() {
		BeforeEach(func() {
			k8sclient = k8sfake.NewSimpleClientset(devopsConfigmap)
			devopsclient = devopsfake.NewSimpleClientset()
			thirdpartyMock.EXPECT().AlaudaFactory().Return(alaudaFactoryMock).AnyTimes()
			alaudaFactoryMock.EXPECT().Client("", devopsConfigmap).Return(thirdparty.NewACEClient(aceClientMock, devops.NewAnnotationProvider(devops.UsedBaseDomain)))

			managerMock.EXPECT().GetDevOpsClient().Return(devopsclient)
			managerMock.EXPECT().GetKubeClient().Return(k8sclient)
			managerMock.EXPECT().GetThirdParty().Return(thirdpartyMock)
			managerMock.EXPECT().GetExtraConfig().Return(extraConfig)
			aceClientMock.EXPECT().ListRegions(context.TODO()).Return(regions, nil)

			// mock list namespaces in region1 and region2
			namespaceArray := &[]*ace.KubernetesResource{}
			unmarshal([]byte(namespaceListJson), namespaceArray)
			namespaceK8sResourceList := &ace.KubernetesResourceList{
				StandardList: ace.StandardList{
					Count:         3,
					PageSize:      1,
					NumberOfPages: 3,
				},
				Items: *namespaceArray,
			}
			aceClientMock.EXPECT().ListK8SResources(context.TODO(), "region-1", "", "namespaces", gomock.Any()).Return(
				namespaceK8sResourceList, nil)
			aceClientMock.EXPECT().ListK8SResources(context.TODO(), "region-2", "", "namespaces", gomock.Any()).Return(
				&ace.KubernetesResourceList{}, nil)

			// mock list secrets in namespaces "ns1, ns2, alauda-system"
			secret1List := &[]*ace.KubernetesResource{}
			unmarshal([]byte(secretsJson("ns1", "ns1-secret")), secret1List)
			secret1ResList := &ace.KubernetesResourceList{StandardList: ace.StandardList{Count: 1, PageSize: 1, NumberOfPages: 1}, Items: *secret1List}
			aceClientMock.EXPECT().ListK8SResources(context.TODO(), "region-1", "ns1", "secrets", gomock.Any()).Return(secret1ResList, nil)
			aceClientMock.EXPECT().ListK8SResources(context.TODO(), "region-1", "ns1", "secrets", gomock.Any()).Return(secret1ResList, nil)

			secret2List := &[]*ace.KubernetesResource{}
			unmarshal([]byte(secretsJson("ns2", "ns2-secret")), secret2List)
			secret2ResList := &ace.KubernetesResourceList{StandardList: ace.StandardList{Count: 1, PageSize: 1, NumberOfPages: 1}, Items: *secret2List}
			aceClientMock.EXPECT().ListK8SResources(context.TODO(), "region-1", "ns2", "secrets", gomock.Any()).Return(secret2ResList, nil)
			aceClientMock.EXPECT().ListK8SResources(context.TODO(), "region-1", "ns2", "secrets", gomock.Any()).Return(secret2ResList, nil)
			emptySecretList := &[]*ace.KubernetesResource{}
			unmarshal([]byte("[]"), emptySecretList)
			emptySecretResList := &ace.KubernetesResourceList{Items: *emptySecretList}
			aceClientMock.EXPECT().ListK8SResources(context.TODO(), "region-1", "alauda-system", "secrets", gomock.Any()).Return(emptySecretResList, nil)

			//mock delete secrets 'region1:ns1/ns1-secret' 'region1:ns2/ns2-secret'
			aceClientMock.EXPECT().DeleteK8SResource(context.TODO(), gomock.Any(), gomock.Any(), "secrets", gomock.Any(), gomock.Any()).Return(nil)
			aceClientMock.EXPECT().DeleteK8SResource(context.TODO(), gomock.Any(), gomock.Any(), "secrets", gomock.Any(), gomock.Any()).Return(nil)
			// mock get serviceaccount and update service account 'region-1:ns1/default' 'region-1:ns2/default'
			sa1 := &ace.KubernetesResource{}
			unmarshal([]byte(serviceacccountsJson(`{"name":"ns1-secret"}`)), sa1)
			aceClientMock.EXPECT().GetK8SResource(context.TODO(), "region-1", "ns1", "serviceaccounts", "default", gomock.Any()).Return(sa1, nil)
			aceClientMock.EXPECT().UpdateK8SResource(context.TODO(), "region-1", "ns1", "serviceaccounts", "default", gomock.Any()).Return(nil)
			sa2 := &ace.KubernetesResource{}
			unmarshal([]byte(serviceacccountsJson(`{"name":"ns-secret"}`)), sa2) // serviceaccounts is not contains secret of ns2-secret, so, no need to update it
			aceClientMock.EXPECT().GetK8SResource(context.TODO(), "region-1", "ns2", "serviceaccounts", "default", gomock.Any()).Return(sa2, nil)
		})

		Context("When imageregistrybinding is deleted", func() {
			It("Should delete dockersecrets that synced by imageregistrybinding", func() {
				_, err := dockerSecretReconciler.Reconcile(reconcile.Request{
					NamespacedName: types.NamespacedName{
						Namespace: "devops-a9-a9-biz",
						Name:      "harbor",
					},
				})

				Expect(err).To(BeNil())
			})
		})

	})
})

func serviceacccountsJson(imagePullSecretsStr string) string {
	s := `{
    "resource_actions": [
        "k8s_others:create",
        "k8s_others:delete",
        "k8s_others:update",
        "k8s_others:view"
    ],
    "kubernetes": {
        "secrets": [
            {
                "name": "default-token-fc9gq"
            }
        ],
        "kind": "ServiceAccount",
        "imagePullSecrets": [
            %s
        ],
        "apiVersion": "v1",
        "metadata": {
            "uid": "a6e850b5-364e-11e9-9d47-525400bbcebe",
            "namespace": "a9-biz",
            "resourceVersion": "25825038",
            "creationTimestamp": "2019-02-22T03:05:04Z",
            "selfLink": "/api/v1/namespaces/a9-biz/serviceaccounts/default",
            "name": "default"
        }
    }
}`
	return fmt.Sprintf(s, imagePullSecretsStr)
}

func secretsJson(namespace, name string) string {
	var str = `[
    {
        "resource_actions": [
            "k8s_others:create",
            "k8s_others:delete",
            "k8s_others:update",
            "k8s_others:view"
        ],
        "kubernetes": {
            "type": "kubernetes.io/dockerconfigjson",
            "kind": "Secret",
            "data": {
                ".dockerconfigjson": "eyJhdXRocyI6eyJoYXJib3IuaGFyYm9yLnNwYXJyb3cuaG9zdCI6eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJIYXJib3IxMjM0NSIsImVtYWlsIjoibXlAZW1haWwuY29tIiwiYXV0aCI6IllXUnRhVzQ2U0dGeVltOXlNVEl6TkRVPSJ9fX0="
            },
            "apiVersion": "v1",
            "metadata": {
                "name": "%s",
                "labels": {
                    "alauda.io/generatorName": "harbor",
                    "alauda.io/generatedBy": "devops-dockersecret-controller",
                    "alauda.io/generatorNamespace": "devops-a9-a9-biz"
                },
                "namespace": "%s",
                "resourceVersion": "25825009",
                "creationTimestamp": "2019-02-28T05:16:03Z",
                "annotations": {
                    "alauda.io/generatorName": "harbor",
                    "alauda.io/generatedBy": "devops-dockersecret-controller",
                    "alauda.io/generatorNamespace": "devops-a9-a9-biz"
                },
                "selfLink": "/api/v1/namespaces/a9-biz/secrets/dockercfg--devops-a9-a9-biz--harbor",
                "uid": "f1aa3cb1-3b17-11e9-a740-525400bbcebe"
            }
        }
    }
]`
	return fmt.Sprintf(str, name, namespace)
}

func unmarshal(jsonBts []byte, obj interface{}) {
	err := json.Unmarshal(jsonBts, obj)
	if err != nil {
		fmt.Printf("%s", string(jsonBts))
	}
	Expect(err).To(BeNil())
}

var devopsConfigmap = &corev1.ConfigMap{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "devops-config",
		Namespace: "alauda-system",
	},
	TypeMeta: metav1.TypeMeta{
		Kind: "ConfigMap",
	},
	Data: map[string]string{
		"ace_api_endpoint": "http://address",
		"ace_root_account": "alauda",
		"ace_token":        "default",
		"ace_ui_endpoint":  "http://address/console",
		"product":          "ace",
	},
}

var emptyNamespaceList = corev1.NamespaceList{
	Items: []corev1.Namespace{},
}

var namespaceListJson = `[
  {
      "resource_actions": [
          "namespace:consume",
          "namespace:create",
          "namespace:delete",
          "namespace:update",
          "namespace:view"
      ],
      "kubernetes": {
          "status": {
              "phase": "Active"
          },
          "kind": "Namespace",
          "spec": {
              "finalizers": [
                  "kubernetes"
              ]
          },
          "apiVersion": "v1",
          "metadata": {
              "name": "ns1",
              "labels": {
                  "project.alauda.io/name": "ns1"
              },
              "resourceVersion": "24397562",
              "creationTimestamp": "2019-02-26T08:06:35Z",
              "annotations": {
                  "resource.alauda.io/status": "Initializing"
              },
              "selfLink": "/api/v1/namespaces/a6-default",
              "uid": "6f435b92-399d-11e9-9439-5254000cec49"
          }
      }
  },
  {
      "resource_actions": [
          "namespace:consume",
          "namespace:create",
          "namespace:delete",
          "namespace:update",
          "namespace:view"
      ],
      "kubernetes": {
          "status": {
              "phase": "Active"
          },
          "kind": "Namespace",
          "spec": {
              "finalizers": [
                  "kubernetes"
              ]
          },
          "apiVersion": "v1",
          "metadata": {
              "name": "ns2",
              "labels": {
                  "project.alauda.io/name": "ns2"
              },
              "resourceVersion": "6207488",
              "creationTimestamp": "2018-12-25T05:55:43Z",
              "annotations": {
                  "resource.alauda.io/status": "Initializing"
              },
              "selfLink": "/api/v1/namespaces/a6-zpyu",
              "uid": "b79877b0-0809-11e9-981d-5254000cec49"
          }
      }
  },
  {
      "resource_actions": [
          "namespace:consume",
          "namespace:create",
          "namespace:delete",
          "namespace:update",
          "namespace:view"
      ],
      "kubernetes": {
          "status": {
              "phase": "Active"
          },
          "kind": "Namespace",
          "spec": {
              "finalizers": [
                  "kubernetes"
              ]
          },
          "apiVersion": "v1",
          "metadata": {
              "name": "alauda-system",
              "labels": {
                  "project.alauda.io/name": "alauda-system"
              },
              "resourceVersion": "6207488",
              "creationTimestamp": "2018-12-25T05:55:43Z",
              "annotations": {
                  "resource.alauda.io/status": "Initializing"
              },
              "selfLink": "/api/v1/namespaces/alauda-system",
              "uid": "b79877b0-0809-11e9-981d-5254000cec49"
          }
      }
  }
]`
var regions = []ace.Region{
	ace.Region{
		Name:        "ace-1",
		RootAccount: "alauda",
		ID:          "region-1",
	},
	ace.Region{
		Name:        "ace-2",
		RootAccount: "alauda",
		ID:          "region-2",
	},
}

func dockerSecretLabelSelector(imageRegistryBindingNamespace, imageRegistryBindingName string) string {
	return fmt.Sprintf("alauda.io/generatedBy=devops-dockersecret-controller,alauda.io/generatorName=%s,alauda.io/generatorNamespace=%s",
		imageRegistryBindingName, imageRegistryBindingNamespace)
}

func TestDecode(t *testing.T) {
	bts, err := base64.StdEncoding.DecodeString("eyJhdXRocyI6eyJoYXJib3IuaGFyYm9yLWRlZmF1bHQuazhzLXByb2QubWF0aGlsZGUuY29tLmNuIjp7InVzZXJuYW1lIjoiYWRtaW4iLCJwYXNzd29yZCI6IkhhcmJvcjEyMzQ1IiwiZW1haWwiOiJteUBlbWFpbC5jb20iLCJhdXRoIjoiWVdSdGFXNDZTR0Z5WW05eU1USXpORFU9In19fQ==")
	if err != nil {
		fmt.Printf("decode err %s  \n", err)
	} else {
		fmt.Printf("deocde ok : %s", string(bts))
	}
}

type UrlValuesOptionsMatcher struct {
	opts ace.UrlValuesOptions
}

func newUrlValuesOptionsMatcher(opts ace.UrlValuesOptions) UrlValuesOptionsMatcher {
	return UrlValuesOptionsMatcher{
		opts: opts,
	}
}

func (m UrlValuesOptionsMatcher) Matches(options interface{}) (res bool) {

	target := options.(ace.UrlValuesOptions)
	if len(m.opts.Values) != len(target.Values) {
		res = false
		return
	}

	for key, _ := range m.opts.Values {
		if m.opts.Values.Get(key)[0] != target.Values.Get(key)[0] {
			res = false
			return
		}
	}
	res = true
	return
}

func (m UrlValuesOptionsMatcher) String() string {
	return "UrlValuesOptionsMatcher"
}
