package reconciler

import (
	"fmt"

	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	klogr "k8s.io/klog/klogr"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ reconcile.Reconciler = &ToolReconciler{}

// ToolReconciler  will reconciler devops tool,
// default conditioner is: [service conditioner, secret conditioner, auth conditioner]
// you can add more conditioner by AppendConditioner
type ToolReconciler struct {
	Invoker string
	Kind    string

	CacheClient  client.Client
	DevopsClient clientset.Interface

	AvailableFunc func(tool v1alpha1.ToolInterface, lastAttempt *metav1.Time) (*v1alpha1.HostPortStatus, error)

	devopsToolClient clientset.InterfaceExpansion
	conditionerFuncs []func(tool v1alpha1.ToolInterface) generic.Conditioner

	ItemsDeleter LocalGeneratedItemsDeleter
}

// NewToolReconcilerByManager inits reconciler using manager
func NewToolReconcilerByManager(mgr manager.Manager, invokerName string, toolKind string, parentKind string, itemResourceStr string, serviceLabel string) *ToolReconciler {
	r := &ToolReconciler{
		Invoker:      invokerName,
		Kind:         toolKind,
		CacheClient:  mgr.GetClient(),
		DevopsClient: mgr.GetDevOpsClient(),
	}

	r.ItemsDeleter = NewLocalGeneratedItemsDeleter(r.CacheClient, clientset.NewExpansion(mgr.GetDevOpsClient()),
		parentKind, itemResourceStr,
		func(parent types.NamespacedName) *metav1.LabelSelector {
			return &metav1.LabelSelector{
				MatchLabels: map[string]string{
					serviceLabel: parent.Name,
				}}
		}, mgr.GetAnnotationProvider())

	r.init()

	return r
}

// NewToolReconciler inits a reconciler using the client interfaces
func NewToolReconciler(invokerName string, kind string, cacheClient client.Client, devopsClient clientset.Interface, parentKind string, itemResourceStr string, serviceLabel string, annotationProvider v1alpha1.AnnotationProvider) *ToolReconciler {
	r := &ToolReconciler{
		Invoker:      invokerName,
		Kind:         kind,
		CacheClient:  cacheClient,
		DevopsClient: devopsClient,
	}

	r.ItemsDeleter = NewLocalGeneratedItemsDeleter(r.CacheClient, clientset.NewExpansion(devopsClient),
		parentKind, itemResourceStr,
		func(parent types.NamespacedName) *metav1.LabelSelector {
			return &metav1.LabelSelector{
				MatchLabels: map[string]string{
					serviceLabel: parent.Name,
				}}
		}, annotationProvider)

	r.init()

	return r
}

func (reconciler *ToolReconciler) init() {
	if reconciler.devopsToolClient == nil && reconciler.DevopsClient != nil {
		reconciler.devopsToolClient = clientset.NewExpansion(reconciler.DevopsClient)
	}
}

// Reconcile main reconcile function for the resource
func (reconciler *ToolReconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {

	target := fmt.Sprintf("%s%s", reconciler.Kind, request)
	glog.V(5).Infof("[%s] Reconciling %s", reconciler.Invoker, target)
	var tool v1alpha1.ToolInterface
	reconciler.init()
	tool, err = reconciler.devopsToolClient.DevopsV1alpha1Expansion().DevOpsTool().Get(reconciler.Kind, request.Name, metav1.GetOptions{ResourceVersion: "0"})

	if err != nil {
		utilruntime.HandleError(err)
		if !k8serrors.IsNotFound(err) {
			glog.Errorf("[%s] get tool '%s' returned error: %#v", reconciler.Invoker, target, err)
			return reconcile.Result{}, nil
		}

		log := klogr.New().WithName(fmt.Sprintf("[%s]", reconciler.Invoker)).WithValues("devopstool", request.String())
		reconciler.ItemsDeleter.DeleteItemsOf(log, request.NamespacedName)
		err = nil
		return
	}

	if tool == nil {
		utilruntime.HandleError(err)
		return reconcile.Result{}, nil
	}

	if !generic.ShouldConditionalComputation(tool.GetStatus().HTTPStatus, v1alpha1.TTLServiceCheck) {
		glog.V(5).Infof("[%s] tool '%s' has been reconcile in %d minutes, skip reconcile", reconciler.Invoker, target, int(v1alpha1.TTLServiceCheck.Minutes()))
		return reconcile.Result{}, nil
	}

	runtimeObject := tool.(runtime.Object)
	toolCopy := runtimeObject.DeepCopyObject().(v1alpha1.ToolInterface)

	conditioners := generic.NewStandardConditionProcess(reconciler.Invoker, toolCopy.GetObjectMeta().GetName())
	serviceConditioner := generic.NewServiceConditioner(toolCopy.GetObjectMeta().GetName(), reconciler.getAvailableFunc(toolCopy), toolCopy.GetStatus().HTTPStatus)
	secretConditioner := generic.NewRecSecretConditioner(toolCopy.GetToolSpec().Secret, reconciler.CacheClient)
	authConditioner := generic.NewAuthorizationConditioner(toolCopy.GetObjectMeta().GetName(), toolCopy.GetToolSpec().Secret, reconciler.AuthorizationFunc())
	conditioners.Add(serviceConditioner).Add(secretConditioner).Add(authConditioner)

	if len(reconciler.conditionerFuncs) > 0 {
		for _, getConditioner := range reconciler.conditionerFuncs {
			conditioners.Add(getConditioner(toolCopy))
		}
	}
	currentConditions := conditioners.Conditions()

	serviceStatus := toolCopy.GetStatus()
	serviceStatus.HTTPStatus = serviceConditioner.CurrentStatus
	newStatus := generic.GetServicePhaseStatus(currentConditions, *serviceStatus)
	oldStatus := *(tool.GetStatus())
	*serviceStatus = newStatus
	glog.V(3).Infof("[%s] Tool \"%s\" Phase: \"%s\" Reason: \"%s\" Message: \"%s\"", reconciler.Invoker, target,
		serviceStatus.Phase, serviceStatus.Reason, serviceStatus.Message,
	)
	err = nil
	// update only when there is a change in status or when time check was exausted
	if generic.ShouldUpdateByServiceStatus(oldStatus, newStatus, v1alpha1.TTLServiceCheck) {
		glog.V(5).Infof("[%s] Will update tool to %#v", reconciler.Invoker, toolCopy)
		_, err = reconciler.devopsToolClient.DevopsV1alpha1Expansion().DevOpsTool().Update(toolCopy)
	}
	return reconcile.Result{}, err
}

func (reconciler *ToolReconciler) getAvailableFunc(tool v1alpha1.ToolInterface) generic.CheckFunction {
	return func(lastAttempt *metav1.Time) (*v1alpha1.HostPortStatus, error) {
		if reconciler.AvailableFunc == nil {
			glog.V(5).Infof("[%s] tool reconciler '%s' is not set AvaiableFunc， will use default abaiable func", reconciler.Invoker, reconciler.Kind)
			return generic.GetBasicAuth(tool.GetToolSpec().HTTP.Host, "", "")(lastAttempt)
		}
		return reconciler.AvailableFunc(tool, lastAttempt)
	}
}

// AppendConditioner appends a custom conditioner to the reconciler
func (reconciler *ToolReconciler) AppendConditioner(getConditioner func(tool v1alpha1.ToolInterface) generic.Conditioner) *ToolReconciler {
	reconciler.conditionerFuncs = append(reconciler.conditionerFuncs, getConditioner)
	return reconciler
}

// AuthorizationFunc returns a authorization function
func (reconciler *ToolReconciler) AuthorizationFunc() generic.AuthorizationFunc {
	return func(name string, opts *v1alpha1.CodeRepoServiceAuthorizeOptions) (*v1alpha1.CodeRepoServiceAuthorizeResponse, error) {
		return reconciler.devopsToolClient.DevopsV1alpha1Expansion().DevOpsTool().Authorize(reconciler.Kind, name, opts)
	}
}
