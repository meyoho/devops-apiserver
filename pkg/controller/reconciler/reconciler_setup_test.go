package reconciler_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestReconciler(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("reconciler.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/reconciler", []Reporter{junitReporter})
}
