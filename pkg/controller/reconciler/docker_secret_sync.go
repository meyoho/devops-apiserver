package reconciler

import (
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/util"
	"alauda.io/devops-apiserver/pkg/util/generic"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"alauda.io/devops-apiserver/pkg/util/parallel"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/util/retry"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	DockerSecretSyncControllerName = "devops-dockersecret-controller"
	generatedBy                    = DockerSecretSyncControllerName
	podServiceAccountName          = "default"
)

// DockerSecretSyncReconciler
type DockerSecretSyncReconciler struct {
	Client           kubernetes.Interface
	DevopsClient     clientset.Interface
	ThirdPartyClient thirdparty.Interface
	systemNamespace  string
	Provider         v1alpha1.AnnotationProvider
}

var _ reconcile.Reconciler = &DockerSecretSyncReconciler{}

func NewDockerSecretSyncReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	return NewDockerSecretSyncReconciler(mgr.GetKubeClient(), mgr.GetDevOpsClient(), mgr.GetThirdParty(), mgr.GetExtraConfig(), mgr.GetAnnotationProvider())
}

func NewDockerSecretSyncReconciler(client kubernetes.Interface, devopsClient clientset.Interface, thirdPartyClient thirdparty.Interface, extra manager.ExtraConfig, provider v1alpha1.AnnotationProvider) reconcile.Reconciler {
	return &DockerSecretSyncReconciler{
		Client:           client,
		DevopsClient:     devopsClient,
		ThirdPartyClient: thirdPartyClient,
		systemNamespace:  extra.SystemNamespace,
		Provider:         provider,
	}
}

func (rec *DockerSecretSyncReconciler) getAlaudaProductClient() (thirdparty.AlaudaProductClient, error) {
	configmap, err := rec.Client.CoreV1().ConfigMaps(rec.systemNamespace).Get(configmapName, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("cannot find configmap %s in ns %s, err:%s ", configmapName, rec.systemNamespace, err.Error())
		return nil, err
	}

	client, err := rec.ThirdPartyClient.AlaudaFactory().Client("", configmap)
	if err != nil {
		glog.Errorf("[%s] Error to Construct Alauda Prouct Client, error:%s", DockerSecretSyncControllerName, err.Error())
		return nil, err
	}
	return client, nil
}

func (rec *DockerSecretSyncReconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {

	begin := time.Now()

	defer func() {
		elapsed := time.Now().Sub(begin).Seconds()
		if err != nil {
			glog.Errorf("[%s] Reconciled  %s imageregistrybinding's dockersecret, elapsed:%fs, error:%s", DockerSecretSyncControllerName, request, elapsed, err)
		} else {
			glog.Infof("[%s] Reconciled %s imageregistrybinding's dockersecret, elapsed:%fs, error: nil", DockerSecretSyncControllerName, request, elapsed)
		}
		// ignore all errors and prevent requeue, just wait for next loop
		err = nil
	}()

	glog.V(3).Infof("[%s] Reconciling %s imageregistrybinding's dockersecret", DockerSecretSyncControllerName, request)

	var client thirdparty.AlaudaProductClient
	client, err = rec.getAlaudaProductClient()
	if err != nil {
		return reconcile.Result{}, err
	}

	var imageRegistryBinding *v1alpha1.ImageRegistryBinding
	imageRegistryBinding, err = rec.DevopsClient.DevopsV1alpha1().ImageRegistryBindings(request.Namespace).Get(request.Name, metav1.GetOptions{ResourceVersion: "0"})

	if err != nil && errors.IsNotFound(err) {
		// imageRegistryBinding was deleted, we should delete secret that has sync to clusters namespaces
		glog.V(5).Infof("[%s] ImageRegistryBinding '%s' is already deleted", DockerSecretSyncControllerName, request)
		err = rec.reconcileDelete(client, request.Name, request.Namespace)
		return reconcile.Result{}, err
	}

	if err != nil {
		return reconcile.Result{}, err
	}

	if imageRegistryBinding.GetSecretName() == "" {
		glog.V(7).Infof("[%s] Skip reconcile dockersecret, secret name is empty, ImageRegistryBinding: '%s/%s'", DockerSecretSyncControllerName, request.Namespace, request.Name)
		return reconcile.Result{}, nil
	}

	var imageRegistry *v1alpha1.ImageRegistry
	imageRegistry, err = rec.DevopsClient.DevopsV1alpha1().ImageRegistries().Get(imageRegistryBinding.Spec.ImageRegistry.Name, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return reconcile.Result{}, err
	}

	secretFound := true
	var secret *v1.Secret
	secret, err = rec.Client.CoreV1().Secrets(imageRegistryBinding.GetSecretNamespace()).Get(imageRegistryBinding.GetSecretName(), metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		if errors.IsNotFound(err) {
			secretFound = false
		} else {
			glog.Errorf("[%s] get dockersecret error, imageregistrybinding '%s' error:%s , secretNamespace:%s, secretName:%s",
				DockerSecretSyncControllerName, request, err.Error(), imageRegistryBinding.GetSecretNamespace(), imageRegistryBinding.GetSecretName())
			return reconcile.Result{}, err
		}
	}

	// sync secret to clusters
	var conds = []v1alpha1.BindingCondition{}
	// imageRegistryBinding is exsits, will sync secret used by it to all clusters
	if imageRegistryBinding.Status.Phase == v1alpha1.ServiceStatusPhaseReady && secretFound {
		glog.V(5).Infof("[%s] ImageRegistryBinding '%s' phase is Ready, ensure secret created in clusters", DockerSecretSyncControllerName, request)
		conds = rec.reconcileCreate(client, imageRegistryBinding, imageRegistry, secret)
	} else if (imageRegistryBinding.Status.Phase == v1alpha1.StatusError && ifSecretIsNotFound(imageRegistryBinding)) || !secretFound { // sometimes, secret status is not timely in imageregistrybindg
		// secret is not found, we should delete all secret that sync to clusters
		glog.V(5).Infof("[%s] ImageRegistryBinding '%s' Secret is NotFound, ensure secret deleted in clusters ", DockerSecretSyncControllerName, request)
		err = rec.reconcileDelete(client, imageRegistryBinding.Name, imageRegistryBinding.Namespace)
		conds = append(conds, dockerSecretSyncCondition(imageRegistryBinding.GetSecretName(), imageRegistryBinding.GetSecretNamespace(), err))
	} else {
		glog.Infof("[%s] Skip, ImageRegistryBinding '%s''s phase is %s", DockerSecretSyncControllerName, request, imageRegistryBinding.Status.Phase)
		return reconcile.Result{}, nil
	}

	// update imageregistrybinding
	imageRegistryBindingCopy := imageRegistryBinding.DeepCopy()
	// we will not change the main status on imageregistrybinding
	imageRegistryBindingCopy.Status.Conditions = v1alpha1.BindingConditions(imageRegistryBindingCopy.Status.Conditions).ReplaceBy(DockerSecretSyncControllerName, conds)
	_, err = rec.DevopsClient.DevopsV1alpha1().ImageRegistryBindings(imageRegistryBindingCopy.Namespace).UpdateStatus(imageRegistryBindingCopy)
	if err != nil {
		glog.Errorf("[%s] update imageregistrybinding error:%s, imageregistrybinding:%#v", DockerSecretSyncControllerName, err.Error(), imageRegistryBindingCopy)
		return reconcile.Result{}, err
	}

	err = v1alpha1.BindingConditions(conds).Errors()
	if err != nil {
		glog.Errorf("[%s] ImageRegistryBinding '%s', error: %s, phase: %s, reason: %s, message:%s",
			DockerSecretSyncControllerName, request, err.Error(), imageRegistryBindingCopy.Status.Phase, imageRegistryBinding.Status.Reason, imageRegistryBinding.Status.Message)
	} else {
		glog.Infof("[%s] ImageRegistryBinding '%s', phase: %s, reason: %s, message:%s",
			DockerSecretSyncControllerName, request, imageRegistryBindingCopy.Status.Phase, imageRegistryBinding.Status.Reason, imageRegistryBinding.Status.Message)
	}
	return reconcile.Result{}, err
}

func dockerSecretSyncCondition(secretName, secretNamespace string, err error) v1alpha1.BindingCondition {
	now := metav1.NewTime(time.Now())
	cond := v1alpha1.BindingCondition{
		Name:        secretName,
		Namespace:   secretNamespace,
		Type:        v1alpha1.TypeSecret,
		LastAttempt: &now,
		Owner:       DockerSecretSyncControllerName,
	}
	if err != nil {
		cond.Status = v1alpha1.StatusError
		cond.Message = err.Error()
		cond.Reason = err.Error()
	} else {
		cond.Status = v1alpha1.StatusReady
	}

	return cond
}

func (rec *DockerSecretSyncReconciler) reconcileCreate(client thirdparty.AlaudaProductClient,
	imageRegistryBinding *v1alpha1.ImageRegistryBinding, imageRegistry *v1alpha1.ImageRegistry, secret *v1.Secret) []v1alpha1.BindingCondition {

	currentNamespace, err := rec.Client.CoreV1().Namespaces().Get(imageRegistryBinding.Namespace, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("[%s] get project biz namespaces '%s' of imageregistrybinding got error:%s, ImageRegistryBinding: %s/%s",
			DockerSecretSyncControllerName, imageRegistryBinding.Namespace, err.Error(), imageRegistryBinding.Namespace, imageRegistryBinding.Name)
		return []v1alpha1.BindingCondition{dockerSecretSyncCondition(secret.Name, secret.Namespace, err)}
	}

	conds := []v1alpha1.BindingCondition{}

	namespaces, listErr := client.GetProjectNamespaces(currentNamespace)
	if listErr != nil {
		glog.Errorf("[%s] ERROR to get project biz namespaces by namespace %s got error:%s, ImageRegistryBinding: %s/%s", DockerSecretSyncControllerName, currentNamespace.Name, listErr.Error(), imageRegistryBinding.Namespace, imageRegistryBinding.Name)
		conds = append(conds, dockerSecretSyncCondition(secret.Name, secret.Namespace, listErr))
		// we do not return here, because, some namespaces could be returned. just record this information into conditons
	}

	if len(namespaces) == 0 {
		glog.V(3).Infof("[%s] project biz namespace is empty, ImageRegistryBinding: %s/%s", DockerSecretSyncControllerName, imageRegistryBinding.Namespace, imageRegistryBinding.Name)
	}

	// we should create secret in devops cluster namespace besides all biz cluster namespaces
	// so we should append current devops cluster namespace
	namespaces = rec.appendCurrentDevOpsNamespace(namespaces, currentNamespace)

	registryHost := generic.GetHostInUrl(imageRegistry.Spec.HTTP.Host)
	pTasks := parallel.P(fmt.Sprintf("Ensure dockersecret exists; ImageRegistryBinding:%s/%s", imageRegistryBinding.Namespace, imageRegistryBinding.Name))
	for _, clusterNamespaceClient := range namespaces {
		var nsClient = clusterNamespaceClient
		pTasks.Add(func() (i interface{}, e error) {
			cond := rec.createImagePullSecretInCluster(nsClient, imageRegistryBinding, secret, registryHost)
			return cond, nil
		})
	}
	condsInterface, _ := pTasks.SetConcurrent(10).Do().Wait()
	for _, cond := range condsInterface {
		conds = append(conds, cond.(v1alpha1.BindingCondition))
	}

	return conds
}

func (rec *DockerSecretSyncReconciler) appendCurrentDevOpsNamespace(nsClients []thirdparty.ClusterNamespaceInterface, current *v1.Namespace) (result []thirdparty.ClusterNamespaceInterface) {

	devopsNamespaceClient := thirdparty.NewDevOpsCluster(rec.Client.CoreV1().RESTClient()).Namespace(current.Name)

	nsClients = append(nsClients, devopsNamespaceClient)

	return nsClients
}

// deleteImagePullSecretsInClusterNamespace will delete imagePullSecrets and set service account in specified cluster
func (rec *DockerSecretSyncReconciler) deleteImagePullSecretsInClusterNamespace(clusterNsInterface thirdparty.ClusterNamespaceInterface, bindingName string, bindingNamespace string) error {

	glog.V(3).Infof("[%s] checking to delete image pull secrets in cluster namespace:%s, ImageRegistryBinding: %s/%s",
		DockerSecretSyncControllerName, clusterNsInterface, bindingNamespace, bindingName)

	labelselector := &metav1.LabelSelector{
		MatchLabels: dockerSecretLabels(bindingNamespace, bindingName, rec.Provider),
	}
	selector, err := metav1.LabelSelectorAsSelector(labelselector)
	if err != nil {
		return errorLabelSelectorAsSelector(labelselector, err)
	}

	var errHappend error
	secretsShouldDelete := &v1.SecretList{}
	err = clusterNsInterface.List(v1.ResourceSecrets.String(), metav1.ListOptions{
		LabelSelector: selector.String(),
	}, secretsShouldDelete)
	if err != nil {
		errHappend = err
		glog.Errorf("[%s] error list secrets with labels %s in namespace:%s, error:%s", DockerSecretSyncControllerName, selector.String(), clusterNsInterface, err.Error())
	}
	if len(secretsShouldDelete.Items) == 0 {
		glog.V(7).Infof("[%s] skip namespace '%s'; ImageRegistryBinding: %s/%s", DockerSecretSyncControllerName, clusterNsInterface, bindingNamespace, bindingName)
		return nil
	}

	// delete all secrets that generated by imageregistrybinding
	glog.V(3).Infof("[%s] deleting secret in namespace '%s'; ImageRegistryBinding: %s/%s, labelSelector: %s",
		DockerSecretSyncControllerName, clusterNsInterface, bindingNamespace, bindingName, selector)

	err = clusterNsInterface.DeleteCollection(v1.ResourceSecrets.String(), &metav1.DeleteOptions{}, metav1.ListOptions{
		LabelSelector: selector.String(),
	})
	if err != nil {
		glog.Errorf("[%s] error to delete image pull secrets in cluster:%s, error:%s", DockerSecretSyncControllerName, clusterNsInterface, err.Error())
		errHappend = err
	}

	// remove secrets config in default sa
	err = rec.removeImagePullSecretsInServiceAccount(clusterNsInterface, secretsShouldDelete)
	if err != nil {
		errHappend = err
		glog.Errorf("[%s] error to remove image pull secrets in serviceaccount %s/%s in cluster:%s, error:%s", DockerSecretSyncControllerName, clusterNsInterface.CurrentNamespace(), podServiceAccountName, clusterNsInterface, err.Error())
	}

	if errHappend != nil {
		return errHappend //any errors happend, just return it
	}

	return nil
}

func (rec *DockerSecretSyncReconciler) removeImagePullSecretsInServiceAccount(clusterNsInterface thirdparty.ClusterNamespaceInterface, secretsShouldDelete *v1.SecretList) error {
	serviceAccount := &v1.ServiceAccount{}
	err := clusterNsInterface.Get(v1alpha1.ResourceServiceAccounts, podServiceAccountName, metav1.GetOptions{ResourceVersion: "0"}, serviceAccount)
	if err != nil {
		if errors.IsNotFound(err) {
			glog.Infof("[%s] serviceaccount '%s/%s' is not exists, no need to remove imagepullsecrets in cluster:%s", DockerSecretSyncControllerName, clusterNsInterface.CurrentNamespace(), podServiceAccountName, clusterNsInterface.CurrentCluster().String())
			return nil
		}
		glog.Errorf("[%s] get serviceaccount '%s/%s' error:%s in cluster:%s", DockerSecretSyncControllerName, clusterNsInterface.CurrentNamespace(), podServiceAccountName, err.Error(), clusterNsInterface.CurrentCluster().String())
		return err
	}

	if len(serviceAccount.ImagePullSecrets) == 0 {
		glog.V(9).Infof("[%s] skip update serviceaccount '%s/%s'", DockerSecretSyncControllerName, clusterNsInterface.CurrentNamespace(), podServiceAccountName)
		return nil
	}

	shouldDelete := secretsAsLocalObjects(secretsShouldDelete.Items)
	imagePullSecretsCopy, deletedSecretsRef := k8s.LocalObjects(serviceAccount.ImagePullSecrets).Sub(shouldDelete)

	if len(deletedSecretsRef) == 0 {
		glog.V(9).Infof("[%s] skip update serviceaccount '%s/%s' in cluster:%s", DockerSecretSyncControllerName, clusterNsInterface.CurrentNamespace(), podServiceAccountName, clusterNsInterface.CurrentCluster())
		return nil
	}

	glog.V(9).Infof("[%s] will remove imagePullSecrets '%s' in serviceaccout '%s/%s' of cluster:%s ", DockerSecretSyncControllerName, deletedSecretsRef, clusterNsInterface.CurrentNamespace(), podServiceAccountName, clusterNsInterface.CurrentCluster())

	// update sa
	err = retry.RetryOnConflict(retry.DefaultRetry, func() error {
		serviceAccount.ImagePullSecrets = imagePullSecretsCopy
		sericeAccountCopy := serviceAccount.DeepCopy()
		updateErr := clusterNsInterface.Update(v1alpha1.ResourceServiceAccounts, sericeAccountCopy)
		if !errors.IsConflict(updateErr) {
			return updateErr //stop retry here
		}
		// reget serviceAccount
		glog.Errorf("[%s] will retry,  removed imagePulSecrets '%s' of serviceaccout '%s/%s' in cluster:%s  want update to %#v",
			DockerSecretSyncControllerName, deletedSecretsRef, clusterNsInterface.CurrentNamespace(), podServiceAccountName, clusterNsInterface.CurrentCluster(), imagePullSecretsCopy)
		e := clusterNsInterface.Get(v1alpha1.ResourceServiceAccounts, podServiceAccountName, metav1.GetOptions{ResourceVersion: "0"}, serviceAccount)
		if e != nil {
			return e // stop retry
		}
		return updateErr // trigger to retry
	})

	if err != nil {
		glog.Errorf("[%s] removed imagePulSecrets '%s' of serviceaccout '%s/%s' in cluster:%s error:%s, want update to %#v",
			DockerSecretSyncControllerName, deletedSecretsRef, clusterNsInterface.CurrentNamespace(), podServiceAccountName, clusterNsInterface.CurrentCluster(), err.Error(), imagePullSecretsCopy)
		return err
	}

	glog.Infof("[%s] removed imagePullSecrets '%s' of serviceaccout '%s/%s'  in cluster:%s ",
		DockerSecretSyncControllerName, deletedSecretsRef, clusterNsInterface.CurrentNamespace(), podServiceAccountName, clusterNsInterface.CurrentCluster())
	glog.V(9).Infof("[%s] current imagePullSecrets of serviceaccount %s/%s' is:%#v in cluster:%s",
		DockerSecretSyncControllerName, clusterNsInterface.CurrentNamespace(), podServiceAccountName, imagePullSecretsCopy, clusterNsInterface.CurrentCluster())
	return nil
}

func secretsAsLocalObjects(items []v1.Secret) []v1.LocalObjectReference {
	if len(items) == 0 {
		return []v1.LocalObjectReference{}
	}
	result := []v1.LocalObjectReference{}
	for _, item := range items {
		result = append(result, v1.LocalObjectReference{Name: item.Name})
	}

	return result
}

// reconcileDelete will delete all secret that synced by current imageregistrybinding. because current imageregistrybindign is deleted or secret is deleted.
func (rec *DockerSecretSyncReconciler) reconcileDelete(client thirdparty.AlaudaProductClient, bindingName string, bindingNamespace string) error {

	currentNamespace, err := rec.Client.CoreV1().Namespaces().Get(bindingNamespace, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("[%s] get project biz namespaces '%s' of imageregistrybinding got error:%s, ImageRegistryBinding: %s/%s",
			DockerSecretSyncControllerName, bindingNamespace, err.Error(), bindingNamespace, bindingName)
		// just
		currentNamespace = &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: bindingNamespace}}
	}

	namespaceList, listErr := client.GetProjectNamespaces(currentNamespace)

	errs := util.MultiErrors{}
	if listErr != nil {
		glog.Errorf("[%s] ERROR to get project biz namespaces by namespace %s got error:%s, ImageRegistryBinding: %s/%s", DockerSecretSyncControllerName, currentNamespace.Name, listErr.Error(), bindingNamespace, bindingName)
		errs = append(errs, listErr)
		// we do not return here, because, some namespaces could be returned. just record this information into conditons
	}

	if len(namespaceList) == 0 {
		glog.V(3).Infof("[%s] project biz namespace is empty, ImageRegistryBinding: %s/%s", DockerSecretSyncControllerName, bindingNamespace, bindingName)
	}

	// we should delete secret in devops cluster namespace besides all biz cluster namespaces
	// so we should append current devops cluster namespace
	namespaceList = rec.appendCurrentDevOpsNamespace(namespaceList, currentNamespace)

	// delete image pull secrets in namespace list
	pTasks := parallel.P(fmt.Sprintf("delete dockersecret ,ImageRegistryBinding:%s/%s", bindingNamespace, bindingName))
	for _, clusterNamespaceClient := range namespaceList {
		var nsClient = clusterNamespaceClient
		pTasks.Add(func() (i interface{}, e error) {
			err := rec.deleteImagePullSecretsInClusterNamespace(nsClient, bindingName, bindingNamespace)
			return nil, err
		})
	}
	_, err = pTasks.SetConcurrent(10).Do().Wait()
	if err != nil {
		glog.Errorf("[%s] delete imagepullSecrets got error:%s, ImageRegistryBinding: '%s/%s'", DockerSecretSyncControllerName, err.Error(), bindingNamespace, bindingName)
		errs = append(errs, err)
	}

	if len(errs) == 0 {
		return nil
	}

	return &errs
}

func ifSecretIsNotFound(binding *v1alpha1.ImageRegistryBinding) bool {
	for _, cond := range binding.Status.Conditions {
		if cond.Type == v1alpha1.JenkinsBindingStatusTypeSecret && cond.Status == v1alpha1.JenkinsBindingStatusConditionStatusNotFound {
			return true
		}
	}
	return false
}

func appendConditionToImageRegistryBinding(imageRegistryBinding *v1alpha1.ImageRegistryBinding, conds []v1alpha1.BindingCondition) {
	// remove the condition that controll by current controller
	originConds := []v1alpha1.BindingCondition{}
	for _, cond := range imageRegistryBinding.Status.Conditions {
		if cond.Owner != DockerSecretSyncControllerName {
			originConds = append(originConds, cond)
		}
	}

	for _, cond := range conds {
		originConds = append(originConds, cond)
	}
	imageRegistryBinding.Status.Conditions = originConds

	errCond := v1alpha1.BindingConditions(conds).Aggregate()
	if imageRegistryBinding.Status.Phase == v1alpha1.StatusReady && errCond != nil {
		imageRegistryBinding.Status.Phase = v1alpha1.StatusError
		imageRegistryBinding.Status.Reason = errCond.Reason
		imageRegistryBinding.Status.Message = errCond.Message
	}

	if imageRegistryBinding.Status.Phase == v1alpha1.StatusReady {
		imageRegistryBinding.Status.Reason = ""
		imageRegistryBinding.Status.Message = ""
	}
}

func (rec *DockerSecretSyncReconciler) appendDevOpsCluster(clusters thirdparty.ParallelClusterInterface) (result thirdparty.ParallelClusterInterface) {

	devopsCluster := thirdparty.NewDevOpsCluster(rec.Client.CoreV1().RESTClient())

	clusters.AppendCluster(devopsCluster)
	return clusters
}

func (rec *DockerSecretSyncReconciler) GenerateDockerCfgSecret(namespace string, imageRegistryBinding *v1alpha1.ImageRegistryBinding, secret *v1.Secret, host string) (dockerCfgSecret *v1.Secret, dockerConf k8s.DockerConfigration) {

	var (
		username = k8s.GetValueInSecret(secret, v1.BasicAuthUsernameKey)
		password = k8s.GetValueInSecret(secret, v1.BasicAuthPasswordKey)
	)

	generatedDockerSecretName := imageRegistryBinding.GetDockerCfgSecretName()

	dockerConf = k8s.NewDockerConfigration(host, username, password)
	data := k8s.EncodeDockerConf(dockerConf)
	dockerCfgSecret = &v1.Secret{
		TypeMeta: metav1.TypeMeta{
			Kind: v1alpha1.TypeSecret,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      generatedDockerSecretName,
			Namespace: namespace,
		},
		Type:       v1.SecretTypeDockerConfigJson,
		StringData: data,
	}
	annotateDockerSecret(imageRegistryBinding, dockerCfgSecret, rec.Provider)
	return
}

func (rec *DockerSecretSyncReconciler) createImagePullSecretInCluster(namespaceClient thirdparty.ClusterNamespaceInterface,
	imageRegistryBinding *v1alpha1.ImageRegistryBinding, secret *v1.Secret, host string) (cond v1alpha1.BindingCondition) {

	var err error
	now := metav1.NewTime(time.Now())

	glog.V(3).Infof("[%s] checking to create image pull secrets in namespace:%s, ImageRegistryBinding: %s/%s",
		DockerSecretSyncControllerName, namespaceClient, imageRegistryBinding.Namespace, imageRegistryBinding.Name)

	dockerCfgSecretWillSync, dockerConf := rec.GenerateDockerCfgSecret(namespaceClient.CurrentNamespace(), imageRegistryBinding, secret, host)
	glog.V(5).Infof("[%s] ensure dockersecret '%s' exist in namespace:%s ", DockerSecretSyncControllerName, dockerCfgSecretWillSync.Name, namespaceClient.String())
	cond = v1alpha1.BindingCondition{
		Name:        dockerCfgSecretWillSync.Name,
		Namespace:   namespaceClient.CurrentCluster().GetClusterName() + "/" + dockerCfgSecretWillSync.Namespace,
		Type:        v1alpha1.TypeSecret,
		LastAttempt: &now,
		Owner:       DockerSecretSyncControllerName,
	}

	defer func() {
		if err != nil {
			cond.Reason = err.Error()
			cond.Message = fmt.Sprintf("Create ImagePullSecret in Namespace:%s, Cluster:%s, error", namespaceClient.CurrentNamespace(), namespaceClient.CurrentCluster())
			cond.Status = v1alpha1.StatusError
			return
		}
		cond.Status = v1alpha1.StatusReady
		return
	}()

	var dockerCfgSecretInK8S = &v1.Secret{}
	err = namespaceClient.Get(v1.ResourceSecrets.String(), dockerCfgSecretWillSync.Name, metav1.GetOptions{ResourceVersion: "0"}, dockerCfgSecretInK8S)

	if err != nil && !errors.IsNotFound(err) {
		glog.Errorf("[%s] get secret '%s/%s' error:%s , cluster:'%s'", DockerSecretSyncControllerName, dockerCfgSecretWillSync.Namespace, dockerCfgSecretWillSync.Name, err.Error(), namespaceClient)
		return
	}

	if err != nil && errors.IsNotFound(err) {
		// not found the _generatedDockerCfgSecret, we should generate new one
		glog.V(3).Infof("[%s] creating secret %s/%s , cluster:'%s'", DockerSecretSyncControllerName, dockerCfgSecretWillSync.Namespace, dockerCfgSecretWillSync.Name, namespaceClient)
		err = namespaceClient.Create(v1.ResourceSecrets.String(), dockerCfgSecretWillSync, dockerCfgSecretWillSync)
	} else {
		// dockerCfgSecret has created, we should update the old one
		glog.V(3).Infof("[%s] updating secret %s/%s , cluster:'%s'", DockerSecretSyncControllerName, dockerCfgSecretWillSync.Namespace, dockerCfgSecretWillSync.Name, namespaceClient)
		err = rec.updateCfgSecret(namespaceClient, dockerCfgSecretInK8S, host, dockerConf.Auths[host].Username, dockerConf.Auths[host].Password)
	}

	if err != nil {
		glog.Errorf("[%s] create or update dockersecret %s/%s error: %v, cluster:'%s'", DockerSecretSyncControllerName, dockerCfgSecretWillSync.Namespace, dockerCfgSecretWillSync.Name, err, namespaceClient)
		return
	}

	glog.Infof("[%s] create or update dockersecret '%s/%s' success , cluster:'%s' ", DockerSecretSyncControllerName, dockerCfgSecretWillSync.Namespace, dockerCfgSecretWillSync.Name, namespaceClient)

	err = rec.ensureImagePullSecretSetInServiceAccount(namespaceClient, dockerCfgSecretWillSync.Name)
	if err != nil {
		return
	}

	return
}

func (rec *DockerSecretSyncReconciler) ensureImagePullSecretSetInServiceAccount(namespaceClusterClient thirdparty.ClusterNamespaceInterface, secretName string) error {

	sa := &v1.ServiceAccount{}
	err := namespaceClusterClient.Get(v1alpha1.ResourceServiceAccounts, podServiceAccountName, metav1.GetOptions{ResourceVersion: "0"}, sa)
	if err != nil {
		glog.Errorf("[%s] get serviceaccount '%s/%s' error:%s", DockerSecretSyncControllerName, namespaceClusterClient.CurrentNamespace(), podServiceAccountName, err.Error())
		return err
	}

	if sa.ImagePullSecrets == nil {
		sa.ImagePullSecrets = []v1.LocalObjectReference{}
	}

	found := k8s.LocalObjects(sa.ImagePullSecrets).IsPresent(secretName)

	if found {
		glog.V(5).Infof("[%s] skip set of serviceaccount '%s/%s',  imagepullsecrets '%s' is already exists， cluster:%s",
			DockerSecretSyncControllerName, namespaceClusterClient.CurrentNamespace(), podServiceAccountName, secretName, namespaceClusterClient)
		return nil
	}

	err = retry.RetryOnConflict(retry.DefaultRetry, func() error {

		sa.ImagePullSecrets = append(sa.ImagePullSecrets, v1.LocalObjectReference{Name: secretName})
		updateErr := namespaceClusterClient.Update(v1alpha1.ResourceServiceAccounts, sa.DeepCopy())
		if !errors.IsConflict(updateErr) {
			return updateErr // will stop retry
		}
		// reget sa
		glog.Errorf("[%s] will retry, set serviceaccount '%s/%s' , imagepullsecret:'%s', cluster:%s",
			DockerSecretSyncControllerName, namespaceClusterClient.CurrentNamespace(), podServiceAccountName, secretName, namespaceClusterClient.CurrentCluster())
		e := namespaceClusterClient.Get(v1alpha1.ResourceServiceAccounts, podServiceAccountName, metav1.GetOptions{ResourceVersion: "0"}, sa)
		if e != nil {
			return e //// will stop retry
		}
		return updateErr //trigger to retry
	})

	if err != nil {
		glog.Errorf("[%s] set serviceaccount '%s/%s' error, imagepullsecret:'%s', cluster:%s, error:%s",
			DockerSecretSyncControllerName, namespaceClusterClient.CurrentNamespace(), podServiceAccountName, secretName, namespaceClusterClient.CurrentCluster(), err.Error())
		return err
	}

	glog.Infof("[%s] set imagepullsecret '%s' in serviceaccount '%s/%s', current is %#v, in cluster:%s",
		DockerSecretSyncControllerName, secretName, namespaceClusterClient.CurrentNamespace(), podServiceAccountName, sa.ImagePullSecrets, namespaceClusterClient.CurrentCluster())
	return nil
}

func (rec *DockerSecretSyncReconciler) updateCfgSecret(namespaceClusterClient thirdparty.ClusterNamespaceInterface, dockerCfgSecret *v1.Secret, newHost string, username, password string) error {

	dockerConf, err := k8s.DecodeDockerConf(dockerCfgSecret)
	if err != nil {
		return err
	}

	auth := dockerConf.GetAuthByHost(newHost)

	// "auth == nil" means the host is not configured in secret
	if auth == nil || auth.Username != username || auth.Password != password {
		dockerCfgSecretCopy := dockerCfgSecret.DeepCopy()
		dockerConf = k8s.NewDockerConfigration(newHost, username, password)
		dockerCfgSecretCopy.StringData = k8s.EncodeDockerConf(dockerConf)

		err := namespaceClusterClient.Update(v1.ResourceSecrets.String(), dockerCfgSecretCopy)
		if err != nil {
			glog.Errorf("[%s] update secret '%s/%s' into cluster '%s'; err: %s, secret:%#v", DockerSecretSyncControllerName, dockerCfgSecret.Namespace, dockerCfgSecret.Name, namespaceClusterClient, err.Error(), dockerCfgSecretCopy)
			return err
		}
		glog.V(5).Infof("[%s] dockercfgsecret '%s/%s' has been updated", DockerSecretSyncControllerName, dockerCfgSecretCopy.Namespace, dockerCfgSecretCopy.Name)
	}

	glog.V(5).Infof("[%s] skip to update dockercfgsecret '%s/%s' , it has not change", DockerSecretSyncControllerName, dockerCfgSecret.Namespace, dockerCfgSecret.Name)
	return nil
}

func dockerSecretLabels(imageRegistryBindingNamespace, imageRegistryBindingName string, provider v1alpha1.AnnotationProvider) map[string]string {
	return map[string]string{
		provider.AnnotationsGeneratedBy():        generatedBy,
		provider.AnnotationsGeneratorNamespace(): imageRegistryBindingNamespace,
		provider.AnnotationsGeneratorName():      imageRegistryBindingName,
	}
}

func annotateDockerSecret(imageRegistryBinding *v1alpha1.ImageRegistryBinding, secret *v1.Secret, provider v1alpha1.AnnotationProvider) {
	ensureLabels := dockerSecretLabels(imageRegistryBinding.Namespace, imageRegistryBinding.Name, provider)

	if secret.Annotations == nil {
		secret.Annotations = ensureLabels
	}
	if secret.Labels == nil {
		secret.Labels = ensureLabels
	}

	for key, value := range secret.Annotations {
		if len(validation.IsValidLabelValue(value)) == 0 {
			secret.Labels[key] = value
		}
	}
	return
}

func errorLabelSelectorAsSelector(labelSelector *metav1.LabelSelector, err error) error {
	if err != nil {
		glog.Errorf("try convert labelselector %#v as selector, got errors:%s", labelSelector, err.Error())
	}
	return err
}
