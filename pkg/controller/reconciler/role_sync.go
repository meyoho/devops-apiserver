package reconciler

import (
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/role"
	"alauda.io/devops-apiserver/pkg/util"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	configmapName          = v1alpha1.SettingsConfigMapName
	RoleSyncConditionOwner = "RoleSync"
)

// ToolData data type returned by the tool for role syncing
type ToolData struct {
	// ToolInstance is the instace of the the DevOps Tool or integrated service. e.g CodeRepoService Gitlab, etc.
	ToolInstance runtime.Object
	// Bindings collections of bindings for the instance. Used only by clients of RoleSyncReconciler
	Bindings []runtime.Object

	// NamespaceProjectsMap a map of namespace ~ tool projects.
	// For instance: a specific namespace "dev" already binds gitlab's "dev", "dev-project" groups.
	// In this case the map should be "dev": ["dev", "dev-project"]
	NamespaceProjectsMap map[string][]string

	// ToolType denotes the kind of tool, e.g "gitlab", "harbor" to be used when calculating permissions
	ToolType string
}

// RoleSyncOptions options for generic reconciler loop
type RoleSyncOptions struct {
	// Name of your controller. Useful for logging.
	Name string
	//  GetToolData should return a set of data defined by ToolData regarding the tool in question
	GetToolData func(reconcile.Request) (data ToolData, err error)
	// GetRoleMapping function to return RoleMapping information based on the projects fetched from the platform
	GetRoleMapping func(data ToolData, opts *v1alpha1.RoleMappingListOptions) (roleMapping *v1alpha1.RoleMapping, err error)
	// ApplyRoleMapping function to apply/send a request to the API Server in order to update the role mapping
	ApplyRoleMapping func(data ToolData, roleMapping *v1alpha1.RoleMapping) (result *v1alpha1.RoleMapping, err error)
	// GetCurrentProjectsUserRoles func(tool runtime.Object, bindings []runtime.Object, namespaceToolProjects map[string][]string) (err error)
}

// Init validates and inits RoleSyncOptions
func (opts RoleSyncOptions) Init() (RoleSyncOptions, error) {
	if opts.Name == "" {
		opts.Name = "Unknown"
	}
	if opts.GetToolData == nil {
		return opts, fmt.Errorf("RoleSyncReconciler \"%s\" needs a GetToolData function", opts.Name)
	}
	if opts.GetRoleMapping == nil {
		return opts, fmt.Errorf("RoleSyncReconciler \"%s\" needs a GetRoleMapping function", opts.Name)
	}
	if opts.ApplyRoleMapping == nil {
		return opts, fmt.Errorf("RoleSyncReconciler \"%s\" needs a ApplyRoleMapping function", opts.Name)
	}
	return opts, nil
}

// RoleSyncReconciler common reconciler for role sync
type RoleSyncReconciler struct {
	Client                kubernetes.Interface
	DevopsClient          clientset.Interface
	devopsClientExpansion clientset.InterfaceExpansion
	ThirdPartyClient      thirdparty.Interface
	Opts                  RoleSyncOptions
	systemNamespace       string
}

var _ reconcile.Reconciler = &RoleSyncReconciler{}

// NewRoleSyncReconciler constructor function for NewRoleSyncReconciler
func NewRoleSyncReconciler(mgr manager.Manager, opts RoleSyncOptions) (rec reconcile.Reconciler, err error) {
	opts, err = opts.Init()
	if err == nil {
		rec = &RoleSyncReconciler{
			Client:                mgr.GetKubeClient(),
			DevopsClient:          mgr.GetDevOpsClient(),
			devopsClientExpansion: clientset.NewExpansion(mgr.GetDevOpsClient()),
			ThirdPartyClient:      mgr.GetThirdParty(),
			Opts:                  opts,
			systemNamespace:       mgr.GetExtraConfig().SystemNamespace,
		}

	}
	return
}

// NewRoleSyncReconciler constructor function for NewRoleSyncReconciler
func NewRoleSyncReconcilerBy(client kubernetes.Interface, devopsClient clientset.Interface, thirdPartyClient thirdparty.Interface, opts RoleSyncOptions, systemNamespace string) (rec reconcile.Reconciler, err error) {
	opts, err = opts.Init()
	if err == nil {
		rec = &RoleSyncReconciler{
			Client:                client,
			DevopsClient:          devopsClient,
			ThirdPartyClient:      thirdPartyClient,
			devopsClientExpansion: clientset.NewExpansion(devopsClient),
			Opts:                  opts,
			systemNamespace:       systemNamespace,
		}
	}
	return
}

// Step logs a step and returns
func (rec *RoleSyncReconciler) Step(step string, args ...interface{}) string {
	step = fmt.Sprintf(step, args...)
	glog.V(5).Infof("[RoleSync:%s] %s...", rec.Opts.Name, step)
	return step
}

func (rec *RoleSyncReconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	var toolData ToolData
	toolData, err = rec.reconcile(request)

	if toolData.ToolInstance == nil {
		// we cannot update condition
		// we had print the eror in rec.reconcile
		// for this kind of reconciler it is necessary to ignore errors
		// and wait for the next reconcile loop
		return reconcile.Result{}, nil
	}

	tool := toolData.ToolInstance.(v1alpha1.ToolInterface)
	// there is always a long time of reconcile role sync, so we'd better get the tool object again to prevent the error "object has modified"
	toolLatest, err := rec.devopsClientExpansion.DevopsV1alpha1Expansion().DevOpsTool().Get(
		tool.GetKind(), tool.GetObjectMeta().GetName(), metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("Error to get tool '%s/%s' before update role sync status, err:%s", tool.GetKind(), tool.GetObjectMeta().GetName(), err.Error())
		return reconcile.Result{}, nil
	}

	toolCopyObj := toolLatest.(runtime.Object).DeepCopyObject()
	toolCopy, _ := toolCopyObj.(v1alpha1.ToolInterface)

	cond := newRoleSyncCondition(err)
	toolCopy.GetStatus().Conditions = v1alpha1.BindingConditions(toolCopy.GetStatus().Conditions).ReplaceBy(RoleSyncConditionOwner, []v1alpha1.BindingCondition{cond})

	_, err = rec.devopsClientExpansion.DevopsV1alpha1Expansion().DevOpsTool().UpdateStatus(toolCopy)
	if err != nil {
		glog.Errorf("Error to Update status of tool '%s/%s', err:%s", toolCopy.GetKind(), toolCopy.GetObjectMeta().GetName(), err.Error())
	}

	// just wait for next loop
	return reconcile.Result{}, nil
}

func newRoleSyncCondition(err error) v1alpha1.BindingCondition {
	metaTime := metav1.NewTime(time.Now())
	cond := v1alpha1.BindingCondition{
		Name:        "rolesync",
		Namespace:   "",
		Type:        RoleSyncConditionOwner,
		Owner:       RoleSyncConditionOwner,
		LastAttempt: &metaTime,
		Status:      v1alpha1.StatusReady,
	}

	if err != nil {
		cond.Status = v1alpha1.StatusError
		cond.Message = err.Error()
		cond.Reason = err.Error()
	}
	return cond
}

// Reconcile reconcile loop for role sync
func (rec *RoleSyncReconciler) reconcile(request reconcile.Request) (data ToolData, err error) {
	// errors are just for displaying information, not really to treat reconcile loops here
	step := "start"
	defer func() {
		if err != nil {
			glog.Errorf("[RoleSync:%s] error while executing step [%s]: %#v", rec.Opts.Name, step, err)
		}
	}()

	// get role sync scheme and configmap
	var (
		configmap      *corev1.ConfigMap
		roleSyncScheme *role.Scheme
	)

	// step = "Getting configmap and schema"
	step = rec.Step("Getting configmap and schema for %s", request)
	roleSyncScheme, configmap, err = rec.GetConfigmapSchema()
	if err != nil {
		return
	}

	// get tool specific data for the client
	// this should be initialized by the client with the necessary functions
	// step = ""
	step = rec.Step("Getting tool data from API Server for %s", request)
	data, err = rec.Opts.GetToolData(request)
	if err != nil {
		return
	}

	// validate if it is necessary to calculate role sync
	// should fail if the ToolType is not available in the mapping
	// step = "Initiating role sync processor"
	step = rec.Step("Initiating role sync processor with source \"%s\" and target \"%s\" for %s...", roleSyncScheme.Source, data.ToolType, request)
	var processor role.Processor
	processor, err = roleSyncScheme.Init(roleSyncScheme.Source, data.ToolType)
	if err != nil {
		return
	}

	// get bindings namespaces already filtered
	step = rec.Step("Getting namespace list for %s", request)
	var nsList *corev1.NamespaceList
	nsList, err = rec.GetFilteredNamespaces(data.NamespaceProjectsMap)
	if err != nil {
		return
	}

	// get ace/acp project user data
	step = rec.Step("Getting project's user/role data from source \"%s\" for %s...", roleSyncScheme.Source, request)
	var projectsData []*role.ProjectUserRoleAssignment
	alaudaClient, err := rec.ThirdPartyClient.AlaudaFactory().Client(roleSyncScheme.Source, configmap)
	if err != nil {
		glog.Errorf("[%s] Error to Construct Alauda Prouct Client, error:%s", DockerSecretSyncControllerName, err.Error())
		return
	}
	projectsData, err = alaudaClient.GetProjectUserRoles(nsList)
	if err != nil {
		return
	}

	// setting each tool project for specific namespace
	var (
		toolProjects []string
		allProjects  = make([]string, 0, len(data.NamespaceProjectsMap))
	)
	for _, proj := range projectsData {
		toolProjects = data.NamespaceProjectsMap[proj.Namespace]
		if len(toolProjects) > 0 {
			proj.AddTool(toolProjects...)
			allProjects = append(allProjects, toolProjects...)
		}
	}
	// remove all duplicated projects
	allProjects = util.RemoveDuplicateStrings(allProjects)

	step = rec.Step("Getting target's \"%s\" role mapping with projects %#v for %s...", data.ToolType, allProjects, request)
	var toolRoleMapping *v1alpha1.RoleMapping
	// request tool projects
	toolRoleMapping, err = rec.Opts.GetRoleMapping(data, &v1alpha1.RoleMappingListOptions{Projects: allProjects})
	if err != nil {
		//
		return
	}
	step = rec.Step("Got following target's \"%s\" role mapping with projects %#v as %#v for %s...", data.ToolType, allProjects, toolRoleMapping, request)

	// calculate operations
	step = rec.Step("Calculating role operations with source \"%s\" and target \"%s\" for %s...", roleSyncScheme.Source, data.ToolType, request)

	targetData := ConvertRoleMappingToProjectUserRoleAssignmet(toolRoleMapping)

	for _, project := range projectsData {
		project.Log("source", 7)
	}

	for _, project := range targetData {
		project.Log("target", 7)
	}

	operations := processor.Process(projectsData, targetData)
	if operations == nil {
		err = fmt.Errorf("Processor returned nil for source \"%s\" and target \"%s\" for %s", roleSyncScheme.Source, data.ToolType, request)
		return
	}

	for _, project := range operations {
		for _, user := range project.UserRoles {
			glog.V(7).Infof("calculated operation project:%#v usera:%#v", project, *(user.UserAssignment))
		}
	}

	// apply tool operations
	step = rec.Step("Apply role operations to target \"%s\" for %s...", data.ToolType, request)
	calculatedRoleMapping := ConvertProjectUserAssignmentOperationToRoleMapping(operations)
	glog.V(9).Infof("calculated RoleMapping:%#v", *calculatedRoleMapping)
	calculatedRoleMapping, err = rec.Opts.ApplyRoleMapping(data, calculatedRoleMapping)
	if err != nil {
		return
	}

	return
}

// GetFilteredNamespaces lists and filters namespaces based on the given namespace~tool project map
func (rec *RoleSyncReconciler) GetFilteredNamespaces(namespaceToolProject map[string][]string) (nsList *corev1.NamespaceList, err error) {
	nsList, err = rec.Client.CoreV1().Namespaces().List(metav1.ListOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	nsList = nsList.DeepCopy()
	// filter only binding related namespaces removing non existing namespace for project
	for i := 0; i < len(nsList.Items); i++ {
		namespace := nsList.Items[i]
		if _, ok := namespaceToolProject[namespace.GetName()]; !ok {
			nsList.Items = append(nsList.Items[:i], nsList.Items[i+1:]...)
			i--
		}
	}
	return
}

// GetConfigmapSchema returns scheme for role sync with configmap
func (rec *RoleSyncReconciler) GetConfigmapSchema() (scheme *role.Scheme, configmap *corev1.ConfigMap, err error) {
	configmap, err = rec.Client.CoreV1().ConfigMaps(rec.systemNamespace).Get(configmapName, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	if configmap.Data == nil || configmap.Data[v1alpha1.SettingsKeyRoleMapping] == "" {
		// error, the schema should be loaded
		err = fmt.Errorf("Configmap is empty or does not have role mapping key")
		return
	}
	scheme, err = role.NewFromString(configmap.Data[v1alpha1.SettingsKeyRoleMapping])
	return
}

// ConvertRoleMappingToProjectUserRoleAssignmet convert RoleMapping to a data the role calculator can understand
func ConvertRoleMappingToProjectUserRoleAssignmet(roleMapping *v1alpha1.RoleMapping) (result []*role.ProjectUserRoleAssignment) {
	result = make([]*role.ProjectUserRoleAssignment, 0, len(roleMapping.Spec))
	if roleMapping != nil && len(roleMapping.Spec) > 0 {
		for _, spec := range roleMapping.Spec {
			project := role.NewProject(spec.Project.GetNamespace()).SetName(spec.Project.GetName())
			if len(spec.UserRoleOperations) > 0 {
				for _, userRole := range spec.UserRoleOperations {
					project.AddUserRole(
						role.NewUserAssignment(userRole.User.Username, userRole.User.Email, userRole.Role.Name, "").
							SetCustom(userRole.Role.Custom),
					)
				}
			}
			result = append(result, project)
		}
	}
	return
}

// ConvertProjectUserAssignmentOperationToRoleMapping convert result from role calculator back to role mapping
func ConvertProjectUserAssignmentOperationToRoleMapping(operations []*role.ProjectUserAssigmentOperation) (mapping *v1alpha1.RoleMapping) {
	mapping = new(v1alpha1.RoleMapping)
	mapping.Spec = make([]v1alpha1.ProjectUserRoleOperation, 0, len(operations))
	if len(operations) > 0 {
		for _, op := range operations {
			project := v1alpha1.ProjectUserRoleOperation{
				Project: v1alpha1.ProjectData{
					ObjectMeta: metav1.ObjectMeta{
						Name:      op.Project.Name,
						Namespace: op.Project.Namespace,
					},
				},
				UserRoleOperations: make([]v1alpha1.UserRoleOperation, 0, len(op.UserRoles)),
			}
			if len(op.UserRoles) > 0 {
				for _, userRole := range op.UserRoles {
					project.UserRoleOperations = append(project.UserRoleOperations, v1alpha1.UserRoleOperation{
						User: v1alpha1.UserMeta{
							Username: userRole.UserAssignment.Username,
							Email:    userRole.UserAssignment.Email,
						},
						Role: v1alpha1.RoleMeta{
							Name:   userRole.UserAssignment.Role,
							Custom: userRole.UserAssignment.Custom,
						},
						Operation: v1alpha1.RoleOperation(userRole.Operation),
					})
				}
			}
			mapping.Spec = append(mapping.Spec, project)
		}
	}
	return
}
