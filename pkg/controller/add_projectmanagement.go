package controller

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/projectmanagement"
	"alauda.io/devops-apiserver/pkg/controller/devops/projectmanagementbinding"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, projectmanagement.Add)
	//AddToManagerFuncs = append(AddToManagerFuncs, projectmanagement.AddRoleSync)
	AddToManagerFuncs = append(AddToManagerFuncs, projectmanagementbinding.Add)

}
