package controller

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/migrate/addjenkinslabel"
)

func init() {
	AddToManagerFuncs = append(AddToManagerFuncs, addjenkinslabel.AddRunnable)
}
