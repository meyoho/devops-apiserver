package configmap

import (
	"context"
	"encoding/json"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/role"
	"alauda.io/devops-apiserver/pkg/toolchain"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

type installToolChainFunc func(cacheClient client.Client, objectMeta metav1.ObjectMeta, toolSpec v1alpha1.ToolSpec, item *toolchain.Item) (err error)

var installFuncs = map[string]installToolChainFunc{}

func addInstallFunc(kind string, upsertFunc installToolChainFunc) {
	installFuncs[strings.ToLower(kind)] = upsertFunc
}

// AddRunnable add Runnable as runnable
func AddRunnable(mgr manager.Manager) error {
	return mgr.Add(NewRunnable(mgr.GetClient(), mgr.GetExtraConfig()))
}

// Runnable bootstrapper for devops-config
type Runnable struct {
	CacheClient      client.Client
	InstallToolChain func(*corev1.ConfigMap, *toolchain.Scheme) error
	installFuncs     map[string]installToolChainFunc
	SetACEKeys       func(*corev1.ConfigMap)
	ACEDefaults      map[string]string

	systemNamespace string
}

var _ sigsmanager.Runnable = &Runnable{}

const (
	configmapName = v1alpha1.SettingsConfigMapName
	domainKey     = v1alpha1.SettingsKeyDomain
	roleSyncKey   = v1alpha1.SettingsKeyRoleMapping
	runnableName  = "configmap-runnable"
)

// Start adds a start method to be a runnable
func (run *Runnable) Start(stop <-chan struct{}) (err error) {
	glog.V(5).Infof("[%s] Starting runnable", runnableName)

	cm := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: run.systemNamespace,
			Name:      configmapName,
		},
	}

	var (
		domain     string
		roleSync   string
		usedScheme *toolchain.Scheme
		roleScheme *role.Scheme
	)

	_, err = controllerutil.CreateOrUpdate(context.TODO(), run.CacheClient, cm, func(existing runtime.Object) error {
		configMap := existing.(*corev1.ConfigMap)

		if configMap.Data == nil {
			configMap.Data = make(map[string]string)
		}

		// the key is empty, we can generate the schema and update its data
		if domain = configMap.Data[domainKey]; len(configMap.Data) == 0 || domain == "" {
			usedScheme = v1alpha1.ToolScheme
		} else {
			// the data is present, we should merge current scheme with our default scheme
			// this action will add or update items, but will not change VersionGate, Installed items
			// or disabled items
			usedScheme, _ = toolchain.NewFromString(domain)
			usedScheme = usedScheme.Merge(v1alpha1.ToolScheme)
		}
		// the key is empty, we can generate the schema and update its data
		if roleSync = configMap.Data[roleSyncKey]; len(configMap.Data) == 0 || roleSync == "" {
			roleScheme = v1alpha1.RoleSyncScheme
		} else {
			// the data is present, we should merge current scheme with our default scheme
			// this action will add only newly added platforms and priorities but will not
			// update items contents
			roleScheme, _ = role.NewFromString(roleSync)
			roleScheme = roleScheme.Merge(v1alpha1.RoleSyncScheme)
		}
		content, err := usedScheme.MarshalIndent()
		if err != nil {
			return err
		}

		roleSyncContent, err := json.MarshalIndent(roleScheme, "", "  ")
		if err != nil {
			return err
		}

		configMap.Data[domainKey] = string(content)
		configMap.Data[roleSyncKey] = string(roleSyncContent)
		if run.SetACEKeys != nil {
			run.SetACEKeys(configMap)
		}

		return nil
	})

	if err != nil {
		glog.Errorf("[%s] Error during configmap \"%s/%s\" manipulation, err: %#v  ", runnableName, run.systemNamespace, configmapName, err)
	} else if run.InstallToolChain != nil {
		// should install our ToolChain
		err = run.InstallToolChain(cm, usedScheme)
		if err != nil {
			glog.Errorf("[%s] Error installing public ToolChain \"%s/%s\", err: %#v", runnableName, run.systemNamespace, configmapName, err)
		}
	}
	<-stop
	return
}

// NewRunnable constructs a new bootstrapper
func NewRunnable(cacheClient client.Client, extra manager.ExtraConfig) sigsmanager.Runnable {
	run := &Runnable{CacheClient: cacheClient}
	run.InstallToolChain = run.installToolChain
	run.installFuncs = installFuncs
	run.SetACEKeys = run.setACEKeysForConfigmap
	run.ACEDefaults = map[string]string{
		v1alpha1.SettingsKeyACEEndpoint:    v1alpha1.ACEEndpointDefault,
		v1alpha1.SettingsKeyACEAPIEndpoint: v1alpha1.ACEAPIEndpointDefault,
		v1alpha1.SettingsKeyACEToken:       "",
		v1alpha1.SettingsKeyACERootAccount: "",
	}
	run.systemNamespace = extra.SystemNamespace
	return run
}

func (run *Runnable) setACEKeysForConfigmap(configmap *corev1.ConfigMap) {
	if run.ACEDefaults != nil {
		var ok bool
		for k, v := range run.ACEDefaults {
			if _, ok = configmap.Data[k]; !ok {
				configmap.Data[k] = v
			}
		}
	}

	return
}

// installToolChain installs all public non-installed ToolChain Resources
// TODO: find a better way to use this.
func (run *Runnable) installToolChain(configMap *corev1.ConfigMap, schema *toolchain.Scheme) (err error) {
	needsUpdate := false
	for _, cat := range schema.Categories {
		if !cat.Enabled || len(cat.Items) == 0 {
			continue
		}
		if run.installCategoryItems(cat, schema) {
			needsUpdate = true
		}
	}
	if !needsUpdate {
		return
	}

	objectKey, err := client.ObjectKeyFromObject(configMap)
	if err != nil {
		return err
	}

	configMap = &corev1.ConfigMap{}
	err = run.CacheClient.Get(context.TODO(), objectKey, configMap)
	if err != nil {
		return
	}
	configMap = configMap.DeepCopy()
	jsonContent, err := schema.Marshal()
	if err == nil {
		configMap.Data[domainKey] = string(jsonContent)
		err = run.CacheClient.Update(context.TODO(), configMap)
	}
	return
}

func (run *Runnable) installCategoryItems(cat *toolchain.Category, schema *toolchain.Scheme) (installed bool) {
	for _, item := range cat.Items {
		// should not create any non public, or already installed items
		// if by mistake the API url was not provided we should also avoid installing
		if !item.Enabled || !item.Public || item.APIAccessURI == "" || schema.IsInstalled(item.Name) {
			continue
		}
		err := run.upsertToolChain(item, schema)
		if err != nil {
			glog.Errorf("[%s] Error creating %s of type %s \"%s\", err: %#v", runnableName, item.Kind, item.Type, item.Name, err)
		} else if !schema.IsInstalled(item.Name) {
			schema.SetInstalled(item.Name)
			installed = true
		}
	}
	return
}

// upsertToolChain will generate and install the tool chain Item
func (run *Runnable) upsertToolChain(item *toolchain.Item, schema *toolchain.Scheme) (err error) {
	glog.V(5).Infof("[%v] Will create %s of type %s \"%s\"...", runnableName, item.Kind, item.Type, item.Name)
	toolSpec := v1alpha1.ToolSpec{
		HTTP: v1alpha1.HostPort{
			Host:      item.APIAccessURI,
			AccessURL: item.WebAccessURI,
		},
	}
	objectMeta := metav1.ObjectMeta{Name: item.Name}
	if upsert, ok := run.installFuncs[strings.ToLower(item.Kind)]; ok {
		err = upsert(run.CacheClient, objectMeta, toolSpec, item)
	} else {
		glog.Errorf("[%v] %s of type %s \"%s\" install is not implemented... this is a coding error", runnableName, item.Kind, item.Type, item.Name)
	}
	return
}
