package configmap

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/toolchain"
	"context"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func init() {
	addInstallFunc(v1alpha1.TypeImageRegistry, installImageRegistry)
}

func installImageRegistry(cacheClient client.Client, objectMeta metav1.ObjectMeta, toolSpec v1alpha1.ToolSpec, item *toolchain.Item) (err error) {
	imageRegistry := &v1alpha1.ImageRegistry{}
	err = cacheClient.Get(context.TODO(), client.ObjectKey{Name: item.Name}, imageRegistry)

	if err != nil && errors.IsNotFound(err) {
		imageRegistry = &v1alpha1.ImageRegistry{
			ObjectMeta: objectMeta,
			Spec: v1alpha1.ImageRegistrySpec{
				ToolSpec: toolSpec,
				Type:     v1alpha1.ImageRegistryType(item.Type),
				Public:   item.Public,
			},
		}
		err = cacheClient.Create(context.TODO(), imageRegistry)
	}
	return
}
