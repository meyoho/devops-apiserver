package configmap

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/toolchain"
	"context"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func init() {
	addInstallFunc(v1alpha1.TypeCodeRepoService, installCodeRepoService)
}

func installCodeRepoService(cacheClient client.Client, objectMeta metav1.ObjectMeta, toolSpec v1alpha1.ToolSpec, item *toolchain.Item) (err error) {

	codeRepoService := &v1alpha1.CodeRepoService{}
	err = cacheClient.Get(context.TODO(), client.ObjectKey{Name: item.Name}, codeRepoService)

	if err != nil && errors.IsNotFound(err) {
		codeRepoService = &v1alpha1.CodeRepoService{
			ObjectMeta: objectMeta,
			Spec: v1alpha1.CodeRepoServiceSpec{
				ToolSpec: toolSpec,
				Type:     v1alpha1.CodeRepoServiceType(item.Type),
				Public:   item.Public,
			},
		}
		err = cacheClient.Create(context.TODO(), codeRepoService)
	}
	return
}
