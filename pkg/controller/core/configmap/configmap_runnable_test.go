package configmap_test

import (
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"context"
	"encoding/json"
	"reflect"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"testing"

	"alauda.io/devops-apiserver/pkg/role"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/core/configmap"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/toolchain"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/runtime"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8stesting "k8s.io/client-go/testing"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

func TestConfigmapController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("configmap.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/core/configmap", []Reporter{junitReporter})
}

func ShouldHaveActions(actions []k8stesting.Action) {
	Expect(actions).ToNot(BeNil())
	Expect(actions).ToNot(BeEmpty())
}

var _ = Describe("Runnable.Start", func() {

	var (
		runnable             sigsmanager.Runnable
		cacheClient          client.Client
		stop                 chan struct{}
		err                  error
		customScheme         = v1alpha1.GetScheme()
		customRoleSyncScheme = v1alpha1.GetRoleSyncScheme()
		namespace            = "alauda-system"
		configmapName        = v1alpha1.SettingsConfigMapName
		domainKey            = v1alpha1.SettingsKeyDomain
		roleSyncKey          = v1alpha1.SettingsKeyRoleMapping
		toolScheme           = v1alpha1.ToolScheme
		versionGate          string
		// setACEKeys    func(*corev1.ConfigMap)
		// tool      v1alpha1.ToolChain
		systemNamespace      = "alauda-system"
		credentialsNamespace = "global-credentials"
		extraConfig          = manager.ExtraConfig{
			SystemNamespace:      systemNamespace,
			CredentialsNamespace: credentialsNamespace,
		}
	)

	BeforeEach(func() {
		cacheClient = testtools.NewFakeClient()
		stop = make(chan struct{})
		versionGate = "ga"
		// setACEKeys = func(cm *corev1.ConfigMap) {

		// }
	})

	JustBeforeEach(func() {
		runnable = configmap.NewRunnable(cacheClient, extraConfig)
		if configmapRunnable, ok := runnable.(*configmap.Runnable); ok {
			configmapRunnable.InstallToolChain = func(cm *corev1.ConfigMap, scheme *toolchain.Scheme) error {
				return nil
			}
		}
		// we will stop this one to run the case, in production this should get stuck there
		go func() {
			stop <- struct{}{}
		}()
		err = runnable.Start(stop)
	})

	ShouldHaveScheme := func(cacheClient client.Client, scheme *toolchain.Scheme, versionGate string) {
		configMap := &corev1.ConfigMap{}
		err := cacheClient.Get(context.TODO(), client.ObjectKey{Name: configmapName, Namespace: namespace}, configMap)

		Expect(err).To(BeNil())
		Expect(configMap).ToNot(BeNil())
		Expect(configMap.Data).ToNot(BeNil())
		Expect(configMap.Data).To(HaveLen(6))
		schemeJson, _ := scheme.Marshal()
		Expect(configMap.Data[domainKey]).To(MatchJSON(schemeJson))
		if versionGate != "" {
			configmapScheme, err := toolchain.NewFromString(configMap.Data[domainKey])
			Expect(err).To(BeNil())
			Expect(configmapScheme).ToNot(BeNil())
			Expect(configmapScheme.VersionGate).To(BeEquivalentTo(versionGate))
		}
	}

	Context("Configmap does not exist", func() {
		It("should create configmap with default scheme", func() {
			Expect(err).To(BeNil())

			ShouldHaveScheme(cacheClient, toolScheme, versionGate)
		})
	})
	Context("Configmap exists but is empty", func() {
		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(&corev1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name:      configmapName,
					Namespace: namespace,
				},
				Data: map[string]string{},
			})
		})
		It("should update configmap with default scheme", func() {
			Expect(err).To(BeNil())

			ShouldHaveScheme(cacheClient, toolScheme, versionGate)
		})
	})

	Context("Configmap exists with data", func() {
		BeforeEach(func() {
			// disabling all categories here
			customScheme.VersionGate = "beta"
			versionGate = customScheme.VersionGate
			for i, c := range customScheme.Categories {
				c.Enabled = false
				for _, item := range c.Items {
					if item.Public {
						customScheme.SetInstalled(item.Name)
					}
				}
				// remove last category
				if i+1 == len(customScheme.Categories) {
					customScheme.Categories = customScheme.Categories[:i]
					break
				}
			}

			jsonContent, jsonErr := customScheme.Marshal()
			Expect(jsonErr).To(BeNil())

			customRoleSyncScheme.AddPlatform(role.NewPlatform("newPlatform"))
			roleSync, roleJSONErr := json.Marshal(customRoleSyncScheme)
			Expect(roleJSONErr).To(BeNil())

			cacheClient = testtools.NewFakeClient(&corev1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name:      configmapName,
					Namespace: namespace,
				},
				Data: map[string]string{
					domainKey:   string(jsonContent),
					roleSyncKey: string(roleSync),
				},
			})
		})
		It("should merge schems and update configmap with merged scheme", func() {
			Expect(err).To(BeNil())
			ShouldHaveScheme(cacheClient, customScheme.Merge(toolScheme), versionGate)
		})
	})
})

var _ = Describe("Runnable.installToolChain", func() {

	var (
		runnable    *configmap.Runnable
		cacheClient client.Client
		err         error

		scheme               *toolchain.Scheme
		configMap            *corev1.ConfigMap
		namespace            = "new-alauda-system"
		configmapName        = v1alpha1.SettingsConfigMapName
		domainKey            = v1alpha1.SettingsKeyDomain
		systemNamespace      = "new-alauda-system"
		credentialsNamespace = "global-credentials"
		extraConfig          = manager.ExtraConfig{
			SystemNamespace:      systemNamespace,
			CredentialsNamespace: credentialsNamespace,
		}
	)

	BeforeEach(func() {
		scheme = v1alpha1.GetScheme()

		configMap = &corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      configmapName,
				Namespace: namespace,
			},
			Data: make(map[string]string),
		}
		if scheme != nil {
			jsonContent, _ := scheme.Marshal()
			configMap.Data[domainKey] = string(jsonContent)
		}
		cacheClient = testtools.NewFakeClient(configMap)
	})

	JustBeforeEach(func() {
		runnable = configmap.NewRunnable(cacheClient, extraConfig).(*configmap.Runnable)
		err = runnable.InstallToolChain(configMap, scheme)
	})

	Context("No public tools exists", func() {
		It("should create tools and update configmap", func() {
			Expect(err).To(BeNil())

			codeRepoServiceList := &v1alpha1.CodeRepoServiceList{}
			err = cacheClient.List(context.TODO(), &client.ListOptions{}, codeRepoServiceList)
			Expect(err).To(BeNil())
			Expect(codeRepoServiceList).NotTo(BeNil())
			Expect(len(codeRepoServiceList.Items)).NotTo(BeZero())

			imageRegistryList := &v1alpha1.ImageRegistryList{}
			err = cacheClient.List(context.TODO(), &client.ListOptions{}, imageRegistryList)
			Expect(err).To(BeNil())
			Expect(imageRegistryList).NotTo(BeNil())
			Expect(len(imageRegistryList.Items)).NotTo(BeZero())

			newConfigMap := &corev1.ConfigMap{}
			objectKey, err := client.ObjectKeyFromObject(configMap)
			Expect(err).To(BeNil())

			err = cacheClient.Get(context.TODO(), objectKey, newConfigMap)
			Expect(err).To(BeNil())
			Expect(reflect.DeepEqual(configMap, newConfigMap)).To(BeFalse())
		})
	})

	Context("All public tools exists", func() {

		BeforeEach(func() {
			objects := []runtime.Object{}
			if scheme != nil {
				for _, c := range scheme.Categories {
					for _, i := range c.Items {
						if !i.Public {
							continue
						}
						scheme.SetInstalled(i.Name)
						switch i.Kind {
						case "CodeRepoService":
							objects = append(objects, &v1alpha1.CodeRepoService{
								ObjectMeta: metav1.ObjectMeta{
									Name: i.Name,
								},
								Spec: v1alpha1.CodeRepoServiceSpec{
									Type:   v1alpha1.CodeRepoServiceType(i.Type),
									Public: true,
								},
							})
						case "ImageRegistry":
							objects = append(objects, &v1alpha1.ImageRegistry{
								ObjectMeta: metav1.ObjectMeta{
									Name: i.Name,
								},
								Spec: v1alpha1.ImageRegistrySpec{
									Type:   v1alpha1.ImageRegistryType(i.Type),
									Public: true,
								},
							})
						}
					}
				}
				jsonContent, _ := scheme.Marshal()
				configMap.Data[domainKey] = string(jsonContent)
			}

			cacheClient = testtools.NewFakeClient(append(objects, configMap)...)
		})
		It("should not create tools and not update configmap", func() {
			Expect(err).To(BeNil())

			newConfigMap := &corev1.ConfigMap{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: namespace, Name: configmapName}, newConfigMap)
			Expect(err).To(BeNil())
			Expect(reflect.DeepEqual(newConfigMap, configMap)).To(BeTrue())

		})
	})
})
