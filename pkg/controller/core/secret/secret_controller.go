package secret

import (
	"context"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/manager"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName = "secret-controller"
	requeueTime    = time.Minute * 10
)

// Add injects the manager so to inject the controller in the manager
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconciler(mgr))
}

// NewReconciler returns a new reconcile.Reconciler
// no-op for this case
func NewReconciler(mgr manager.Manager) reconcile.Reconciler {
	rec := &Reconciler{
		CacheClient:  mgr.GetClient(),
		DevopsClient: mgr.GetDevOpsClient(),
	}
	// set its own function

	rec.RefreshSecretFunc = rec.RefreshSecret
	return rec
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}
	// we sync some oauth2 secrets
	err = ctrl.Watch(
		&source.Kind{Type: &corev1.Secret{}}, &handler.EnqueueRequestForObject{},
		// predicates are a filter to avoid extra reconciliation
		// only the events that return true will be reconciled
		&predicate.Funcs{
			CreateFunc: func(evt event.CreateEvent) bool {
				// we just want oauth2 secrets
				return IsOAuth2Secret(evt.Object)
			},
			UpdateFunc: func(evt event.UpdateEvent) bool {
				// we just want oauth2 secrets
				return IsOAuth2Secret(evt.ObjectNew)
			},
			DeleteFunc: func(evt event.DeleteEvent) bool {
				return IsOAuth2Secret(evt.Object)
			},
		},
	)
	return nil
}

// IsOAuth2Secret returns true if the object is a Secret of type OAuth2
func IsOAuth2Secret(object runtime.Object) bool {
	if object == nil {
		return false
	}
	secret, ok := object.(*corev1.Secret)
	if secret == nil || !ok {
		return false
	}
	return secret.Type == v1alpha1.SecretTypeOAuth2
}

// Reconciler this reconcile primary objective is to
// refresh access tokens for secrets
type Reconciler struct {
	CacheClient client.Client
	// DevOpsClient should only be used to request sub-resource
	DevopsClient clientset.Interface
	// Refresh token function
	// used to simplify reconcile loop, and to be overrided on unit tests
	RefreshSecretFunc func(*v1alpha1.CodeRepoService, *corev1.Secret) (*corev1.Secret, error)
}

var _ reconcile.Reconciler = &Reconciler{}

// Reconcile our reconcile function will sync our secrets
func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	glog.V(5).Infof("[%s] Reconciling %#v", controllerName, request)
	secret := &corev1.Secret{}
	err = rec.CacheClient.Get(context.TODO(), request.NamespacedName, secret)
	if err != nil && errors.IsNotFound(err) {
		utilruntime.HandleError(err)
		err = nil
		return
	}
	// there is no specific owner, and there is we should clear it out
	// and leave it as is
	if len(secret.OwnerReferences) == 0 {
		// make sure we are not doing it all over again
		// has redirect_uri
		if (len(secret.Data) == 2) &&
			secret.Data[v1alpha1.OAuth2ClientIDKey] != nil &&
			secret.Data[v1alpha1.OAuth2ClientSecretKey] != nil {
			// this was already cleared out, we should just leave it alone
			return
		}

		if (len(secret.Data) == 3) &&
			secret.Data[v1alpha1.OAuth2ClientIDKey] != nil &&
			secret.Data[v1alpha1.OAuth2ClientSecretKey] != nil &&
			secret.Data[v1alpha1.OAuth2RedirectURLKey] != nil {
			// this was already cleared out, we should just leave it alone
			return
		}

		// now we clear the data out and update the secret
		err = rec.resetSecret(secret)
		return
	}
	var codeRepoService *v1alpha1.CodeRepoService
	codeRepoService, err = rec.getCodeRepoServiceFromOwners(secret)

	// skip on not found error and wait until next reconcile loop
	if codeRepoService == nil || errors.IsNotFound(err) {
		err = nil
		return
	}

	// verifying if secret has all the needed data to refresh token
	// if not we should just quit the loop and wait until
	dataOauth2 := k8s.GetDataOAuth2FromSecret(secret)
	if !dataOauth2.HasEnoughParamsToGetAccessToken() {
		glog.V(2).Infof("[%s] Secret \"%s/%s\" has not enought data to refresh token", controllerName, secret.Namespace, secret.Name)
		return
	}

	// refresh tokens inside secret
	secretCopy, refreshErr := rec.RefreshSecretFunc(codeRepoService, secret)
	if refreshErr != nil {
		// in case of error we requeue and return
		glog.Errorf("[%s] Secret \"%s/%s\" could not be refreshed by CodeRepoService \"%s\": %s", controllerName, secret.Namespace, secret.Name, codeRepoService.Name, refreshErr.Error())
		result = reconcile.Result{
			Requeue:      true,
			RequeueAfter: requeueTime,
		}
		err = refreshErr
		return
	}
	// should update secret on server
	if secretCopy != nil {
		err = rec.updateSecret(secretCopy)
	}
	glog.V(5).Infof("[%s] Request \"%s/%s\" completed: result: %#v; err: %s", controllerName, request.Namespace, request.Name, result, err)
	return
}

func (rec *Reconciler) getCodeRepoServiceFromOwners(secret *corev1.Secret) (codeRepoService *v1alpha1.CodeRepoService, err error) {
	for _, owner := range secret.OwnerReferences {
		if owner.Kind == v1alpha1.TypeCodeRepoService {
			codeRepoService = &v1alpha1.CodeRepoService{}
			err = rec.CacheClient.Get(context.TODO(), client.ObjectKey{Name: owner.Name}, codeRepoService)
			if err == nil {
				break
			}
			glog.V(2).Infof("[%s] Get CodeRepoService \"%s\" error: %s", controllerName, owner.Name, err.Error())
			codeRepoService = nil
		}
	}
	return
}

// RefreshSecret function to refresh oAuth2 Secret Access and refresh tokens
// TODO: Should move the logic to the APIServer and use devopsClient call here
// 1. move refresh token logic to apiservice as subresource
// 2. first verify that our secret's access code is still valid
// or that we just updated the resource. If just updated, or code is valid
// just return nil, nil and the controller will not update the secret
func (rec *Reconciler) RefreshSecret(codeRepoService *v1alpha1.CodeRepoService, secret *corev1.Secret) (secretCopy *corev1.Secret, err error) {
	// get redirectURL from secret
	var redirectURL string
	if v, ok := secret.Data[v1alpha1.OAuth2RedirectURLKey]; ok {
		redirectURL = string(v)
	}
	// refresh access token on the server
	authOptions := &v1alpha1.CodeRepoServiceAuthorizeCreate{
		SecretName:     secret.Name,
		Namespace:      secret.Namespace,
		GetAccessToken: true,
		RedirectURL:    redirectURL,
	}
	_, err = rec.DevopsClient.DevopsV1alpha1().CodeRepoServices().ProcessAuthorization(codeRepoService.Name, authOptions)
	return
}

func (rec *Reconciler) updateSecret(secret *corev1.Secret) (err error) {
	err = rec.CacheClient.Update(context.TODO(), secret)
	return
}

func (rec *Reconciler) resetSecret(secret *corev1.Secret) (err error) {
	secretCopy := secret.DeepCopy()
	secretCopy.ObjectMeta.OwnerReferences = nil
	secretCopy.Data = make(map[string][]byte, 0)
	secretCopy.Data[v1alpha1.OAuth2ClientIDKey] = secret.Data[v1alpha1.OAuth2ClientIDKey]
	secretCopy.Data[v1alpha1.OAuth2ClientSecretKey] = secret.Data[v1alpha1.OAuth2ClientSecretKey]
	if _, ok := secret.Data[v1alpha1.OAuth2RedirectURLKey]; ok {
		secretCopy.Data[v1alpha1.OAuth2RedirectURLKey] = secret.Data[v1alpha1.OAuth2RedirectURLKey]
	}
	return rec.updateSecret(secretCopy)
}
