package secret_test

import (
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"fmt"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/core/secret"
	devopsctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	sigsctrlmock "alauda.io/devops-apiserver/pkg/mock/sigs.k8s.io/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestSecretController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("secret.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/core/secret", []Reporter{junitReporter})
}

var _ = Describe("Add", func() {
	// Will test the bootstrapping of the controller
	var (
		mockCtrl       *gomock.Controller
		managerMock    *devopsctrlmock.MockManager
		controllerMock *sigsctrlmock.MockController
		cacheClient    client.Client
		devopsClient   *clientset.Clientset
		err            error
		controllerName string
	)

	// We can prepare our tests
	BeforeEach(func() {
		// starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		// constructing our mocks
		managerMock = devopsctrlmock.NewMockManager(mockCtrl)
		controllerMock = sigsctrlmock.NewMockController(mockCtrl)

		cacheClient = testtools.NewFakeClient()
		devopsClient = clientset.NewSimpleClientset()
		controllerName = "secret-controller"
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	// here we really run our functions
	It("should create a new controller using manager", func() {
		// first add expectations here
		// will get our kubeclient from manager
		managerMock.EXPECT().GetClient().Return(cacheClient)
		managerMock.EXPECT().GetDevOpsClient().Return(devopsClient)
		managerMock.EXPECT().NewController(
			// because the reconciler is created internally it will always be different
			// therefore no way to reference the correct expectation here
			controllerName, gomock.Any(),
		).Return(controllerMock, nil) // returning our mock here

		// will watch a secret and regular equeue for request
		controllerMock.EXPECT().Watch(
			&source.Kind{Type: &corev1.Secret{}},
			&handler.EnqueueRequestForObject{},
			// we use predicates here. we could expose the constructor function
			// but in order to make it work we need to make sure
			// that there it is a singleton
			gomock.Any(),
		).Return(nil)

		// invoke the method here finaly
		err = secret.Add(managerMock)

		Expect(err).To(BeNil())
	})

	It("should return an error when failing to create controller", func() {
		// first add expectations here
		// will get our kubeclient from manager
		managerMock.EXPECT().GetClient().Return(cacheClient)
		managerMock.EXPECT().GetDevOpsClient().Return(devopsClient)
		managerMock.EXPECT().NewController(
			// because the reconciler is created internally it will always be different
			// therefore no way to reference the correct expectation here
			controllerName, gomock.Any(),
		).Return(nil, fmt.Errorf("create controller error")) // returning our mock here

		// invoke the method here finaly
		err = secret.Add(managerMock)

		Expect(err).To(Equal(fmt.Errorf("create controller error")))
	})
})

var _ = Describe("IsOAuth2Secret", func() {
	var (
		object runtime.Object
		isTrue bool
	)

	JustBeforeEach(func() {
		isTrue = secret.IsOAuth2Secret(object)
	})

	Context("Nil object", func() {
		BeforeEach(func() {
			object = nil
		})
		It("Should be false", func() {
			Expect(isTrue).ToNot(BeTrue())
		})
	})

	Context("oAuth2 Secret", func() {
		BeforeEach(func() {
			object = &corev1.Secret{ObjectMeta: metav1.ObjectMeta{}, Type: v1alpha1.SecretTypeOAuth2}
		})

		It("Should be true", func() {
			Expect(isTrue).To(BeTrue())
		})
	})
	Context("Another type of Secret", func() {
		BeforeEach(func() {
			object = &corev1.Secret{ObjectMeta: metav1.ObjectMeta{}, Type: corev1.SecretTypeOpaque}
		})

		It("Should be false", func() {
			Expect(isTrue).ToNot(BeTrue())
		})
	})
	Context("Another type of object", func() {
		BeforeEach(func() {
			object = &corev1.ConfigMap{ObjectMeta: metav1.ObjectMeta{}}
		})

		It("Should be false", func() {
			Expect(isTrue).ToNot(BeTrue())
		})
	})
})
