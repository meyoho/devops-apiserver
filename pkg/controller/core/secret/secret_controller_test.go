package secret_test

import (
	// "fmt"

	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"context"
	"fmt"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/core/secret"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {

	var (
		mockCtrl          *gomock.Controller
		secretReconciler  *secret.Reconciler
		cacheClient       client.Client
		devopsClient      *clientset.Clientset
		request           reconcile.Request
		result            reconcile.Result
		secretObj         *corev1.Secret
		codeRepoService   *v1alpha1.CodeRepoService
		refreshSecretFunc func(*v1alpha1.CodeRepoService, *corev1.Secret) (*corev1.Secret, error)
		err               error
	)

	// We can prepare our tests
	BeforeEach(func() {
		// starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		// starts our basic structs
		secretReconciler = &secret.Reconciler{}
		refreshSecretFunc = secretReconciler.RefreshSecret
		cacheClient = testtools.NewFakeClient()
		devopsClient = clientset.NewSimpleClientset()
		request.Name = "some-secret"
		request.Namespace = "default"

		secretObj = GetSecret(request.Name, request.Namespace, map[string][]byte{
			v1alpha1.OAuth2ClientIDKey:     []byte("client-id"),
			v1alpha1.OAuth2ClientSecretKey: []byte("client-key"),
			"another":                      []byte("value"),
		}, nil)
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	// here we really run our functions
	JustBeforeEach(func() {
		secretReconciler.CacheClient = cacheClient
		secretReconciler.DevopsClient = devopsClient
		secretReconciler.RefreshSecretFunc = refreshSecretFunc
		// runs our test here
		result, err = secretReconciler.Reconcile(request)

	})

	Context("secret does not exist", func() {
		It("should not return an error", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("secret does not have an owner reference", func() {
		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(secretObj)
		})
		It("should clean its data and keep only client-id and client-key", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			secretObj := &corev1.Secret{}
			// verify if it was really updated
			err = cacheClient.Get(context.TODO(), request.NamespacedName, secretObj)
			Expect(err).To(BeNil())
			Expect(secretObj).ToNot(BeNil())
			Expect(secretObj.Data).To(Equal(map[string][]byte{
				v1alpha1.OAuth2ClientIDKey:     []byte("client-id"),
				v1alpha1.OAuth2ClientSecretKey: []byte("client-key"),
			}))
		})
	})

	Context("secret has owner reference, but CodeRepoService is not found", func() {
		BeforeEach(func() {
			secretObj.ObjectMeta.OwnerReferences = []metav1.OwnerReference{
				metav1.OwnerReference{
					Kind: v1alpha1.TypeCodeRepoService,
					Name: "non-existing-code-repo",
				},
			}

			cacheClient = testtools.NewFakeClient(secretObj)
		})
		It("should clean its data and keep only client-id and client-key", func() {

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			codeRepoService := &v1alpha1.CodeRepoService{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Name: "non-existing-code-repo"}, codeRepoService)
			Expect(errors.IsNotFound(err)).To(BeTrue())
		})
	})
	Context("secret has owner reference, CodeRepoService exists, but secret is clean", func() {
		BeforeEach(func() {
			secretObj.ObjectMeta.OwnerReferences = []metav1.OwnerReference{
				metav1.OwnerReference{
					Kind: v1alpha1.TypeCodeRepoService,
					Name: "code-repo-service",
				},
			}
			codeRepoService = GetCodeRepoService("code-repo-service", "http://some-host.com", v1alpha1.CodeRepoServiceTypeGithub)
			cacheClient = testtools.NewFakeClient(secretObj, codeRepoService)
			devopsClient = clientset.NewSimpleClientset(codeRepoService)
		})
		It("should not do anything and return nil", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

		})
	})

	Context("refresh secret error", func() {
		BeforeEach(func() {
			// we need a secret with at least this three keys
			secretObj = GetSecret(request.Name, request.Namespace, map[string][]byte{
				v1alpha1.OAuth2ClientIDKey:     []byte("client-id"),
				v1alpha1.OAuth2ClientSecretKey: []byte("client-key"),
				v1alpha1.OAuth2CodeKey:         []byte("code"),
			}, []metav1.OwnerReference{
				metav1.OwnerReference{
					Kind: v1alpha1.TypeCodeRepoService,
					Name: "code-repo-service",
				},
			})
			codeRepoService = GetCodeRepoService("code-repo-service", "http://some-host.com", v1alpha1.CodeRepoServiceTypeGithub)
			cacheClient = testtools.NewFakeClient(secretObj, codeRepoService)
			devopsClient = clientset.NewSimpleClientset(codeRepoService)
			refreshSecretFunc = func(crs *v1alpha1.CodeRepoService, sec *corev1.Secret) (*corev1.Secret, error) {
				// simulating an error
				return nil, fmt.Errorf("Some random error")
			}
		})
		It("should return an error and requeue reconcile after 10 minutes", func() {
			Expect(err).ToNot(BeNil())
			Expect(result).To(Equal(reconcile.Result{
				Requeue:      true,
				RequeueAfter: time.Minute * 10,
			}))
		})
	})

	Context("refresh secret successfully", func() {
		BeforeEach(func() {
			// we need a secret with at least this three keys
			secretObj = GetSecret(request.Name, request.Namespace, map[string][]byte{
				v1alpha1.OAuth2ClientIDKey:     []byte("client-id"),
				v1alpha1.OAuth2ClientSecretKey: []byte("client-key"),
				v1alpha1.OAuth2CodeKey:         []byte("code"),
			}, []metav1.OwnerReference{
				metav1.OwnerReference{
					Kind: v1alpha1.TypeCodeRepoService,
					Name: "code-repo-service",
				},
			})
			codeRepoService = GetCodeRepoService("code-repo-service", "http://some-host.com", v1alpha1.CodeRepoServiceTypeGithub)
			cacheClient = testtools.NewFakeClient(secretObj, codeRepoService)
			devopsClient = clientset.NewSimpleClientset(codeRepoService)
			refreshSecretFunc = func(crs *v1alpha1.CodeRepoService, sec *corev1.Secret) (*corev1.Secret, error) {
				// simulating success
				return sec.DeepCopy(), nil
			}
		})
		It("should not return error and update secret", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("oauth2", func() {
		BeforeEach(func() {
			// we need a secret with at least this three keys
			secretObj = GetSecret(request.Name, request.Namespace, map[string][]byte{
				v1alpha1.OAuth2ClientIDKey:     []byte("client-id"),
				v1alpha1.OAuth2ClientSecretKey: []byte("client-key"),
				v1alpha1.OAuth2CodeKey:         []byte("code"),
				v1alpha1.OAuth2RedirectURLKey:  []byte("http://localhost/"),
			}, []metav1.OwnerReference{
				metav1.OwnerReference{
					Kind: v1alpha1.TypeCodeRepoService,
					Name: "code-repo-service",
				},
			})
			codeRepoService = GetCodeRepoService("code-repo-service", "http://some-host.com", v1alpha1.CodeRepoServiceTypeGithub)
			cacheClient = testtools.NewFakeClient(secretObj, codeRepoService)
			devopsClient = clientset.NewSimpleClientset(codeRepoService)
			refreshSecretFunc = func(crs *v1alpha1.CodeRepoService, sec *corev1.Secret) (*corev1.Secret, error) {
				// simulating success
				s := sec.DeepCopy()
				s.Data[v1alpha1.OAuth2AccessTokenKey] = []byte("token")
				_ = cacheClient.Update(context.TODO(), s)
				return nil, nil
			}
		})
		It("should not return error and update secret", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
			// verify that it has an update secret action
			secretObj := &corev1.Secret{}
			err = cacheClient.Get(context.TODO(), request.NamespacedName, secretObj)
			Expect(err).To(BeNil())
			Expect(secretObj).ToNot(BeNil())
			Expect(secretObj.Data).To(Equal(map[string][]byte{
				v1alpha1.OAuth2ClientIDKey:     []byte("client-id"),
				v1alpha1.OAuth2ClientSecretKey: []byte("client-key"),
				v1alpha1.OAuth2CodeKey:         []byte("code"),
				v1alpha1.OAuth2RedirectURLKey:  []byte("http://localhost/"),
				v1alpha1.OAuth2AccessTokenKey:  []byte("token"),
			}))
		})
	})
})

func GetSecret(name, namespace string, data map[string][]byte, ownerReferences []metav1.OwnerReference) *corev1.Secret {
	return &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:            name,
			Namespace:       namespace,
			OwnerReferences: ownerReferences,
		},
		Type: v1alpha1.SecretTypeOAuth2,
		Data: data,
	}
}

func GetCodeRepoService(name, host string, codeServiceType v1alpha1.CodeRepoServiceType) *v1alpha1.CodeRepoService {
	return &v1alpha1.CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: v1alpha1.CodeRepoServiceSpec{
			ToolSpec: v1alpha1.ToolSpec{
				HTTP: v1alpha1.HostPort{
					Host: host,
				},
			},
			Type: codeServiceType,
		},
	}
}
