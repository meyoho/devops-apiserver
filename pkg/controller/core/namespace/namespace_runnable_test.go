package namespace_test

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/core/namespace"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

var _ = Describe("Runnable.Start", func() {

	var (
		runnable    sigsmanager.Runnable
		cacheClient client.Client
		stop        chan struct{}
		err         error
	)

	BeforeEach(func() {
		cacheClient = testtools.NewFakeClient()
		stop = make(chan struct{})
		provider := devops.NewAnnotationProvider(devops.UsedBaseDomain)

		runnable = namespace.NewRunnable(cacheClient, "global-credentials", provider)
	})

	JustBeforeEach(func() {
		// we will stop this one to run the case, in production this should get stuck there
		go func() {
			stop <- struct{}{}
		}()
		err = runnable.Start(stop)
	})

	It("should run and not return an error", func() {
		Expect(err).To(BeNil())

		namespace := &corev1.Namespace{}
		getErr := cacheClient.Get(context.TODO(), client.ObjectKey{Name: "global-credentials"}, namespace)
		Expect(getErr).To(BeNil())
		Expect(namespace).ToNot(BeNil())
	})
})
