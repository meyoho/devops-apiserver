package namespace

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"context"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const controllerName = "namespace-controller"

// Add injects the manager so to inject the controller in the manager
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconciler(mgr))
}

// NewReconciler returns a new reconcile.Reconciler
// no-op for this case
func NewReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &Reconciler{CacheClient: mgr.GetClient(), Provider: mgr.GetAnnotationProvider(), CredentialsNamespace: mgr.GetExtraConfig().CredentialsNamespace}
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	// Creates a new controller
	var ctrl controller.Controller
	globalCredentialsNamespace := mgr.GetExtraConfig().CredentialsNamespace
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
		// we just need to keep an eye on our global-credentials namespace
		MaxConcurrentReconciles: 1,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}
	// we only watch our global-credentials namespace and keep it active
	// ignore all other namespaces and events using predicates
	err = ctrl.Watch(
		&source.Kind{Type: &corev1.Namespace{}}, &handler.EnqueueRequestForObject{},
		// predicates are a filter to avoid extra reconciliation
		// only the events that return true will be reconciled
		&predicate.Funcs{
			CreateFunc: func(evt event.CreateEvent) bool {
				// we dont mind create events
				return false
			},
			UpdateFunc: func(evt event.UpdateEvent) bool {
				// we dont mind update events
				return false
			},
			// We only care for the delete events of our specific namespace
			DeleteFunc: func(evt event.DeleteEvent) bool {
				if evt.Meta == nil {
					return false
				}
				return globalCredentialsNamespace == evt.Meta.GetName()
			},
		},
	)
	return
}

// Reconciler this reconcile primary objective is to
// maintain the global-credentials namespace active.
// and not do anything else
type Reconciler struct {
	CacheClient          client.Client
	CredentialsNamespace string
	Provider             v1alpha1.AnnotationProvider
}

var _ reconcile.Reconciler = &Reconciler{}

// Reconcile our reconcile function that will keep our namespace alive
func (reconciler *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	glog.V(5).Infof("[%s] Reconciling %#v", controllerName, request)
	err = CreateNamespace(reconciler.CacheClient, reconciler.CredentialsNamespace, reconciler.Provider)
	return
}

// CreateNamespace will check if a namespace exists and create if it doesn't
func CreateNamespace(cacheClient client.Client, name string, provider v1alpha1.AnnotationProvider) error {
	namespace := &corev1.Namespace{}
	err := cacheClient.Get(context.TODO(), client.ObjectKey{Name: name}, namespace)
	if err != nil && errors.IsNotFound(err) {
		// namespace does not exist, we should create it
		namespace = &corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
				Annotations: map[string]string{
					provider.AnnotationsKeyProduct(): v1alpha1.ProductName,
				},
			},
		}
		err = cacheClient.Create(context.TODO(), namespace)
	}
	return err
}
