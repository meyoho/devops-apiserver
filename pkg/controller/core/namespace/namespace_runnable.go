package namespace

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

// AddRunnable add Runnable as runnable
func AddRunnable(mgr manager.Manager) error {
	return mgr.Add(NewRunnable(mgr.GetClient(), mgr.GetExtraConfig().CredentialsNamespace, mgr.GetAnnotationProvider()))
}

const (
	runnableName = "configmap-runnable"
)

// Runnable bootstrapper for global-credentials
type Runnable struct {
	CacheClient          client.Client
	CredentialsNamespace string
	Provider             devops.AnnotationProvider
}

var _ sigsmanager.Runnable = &Runnable{}

// Start adds a start method to be a runnable
func (nb *Runnable) Start(stop <-chan struct{}) (err error) {
	glog.V(5).Infof("[%s] Starting runnable", runnableName)
	err = CreateNamespace(nb.CacheClient, nb.CredentialsNamespace, nb.Provider)
	<-stop
	return
}

// NewRunnable constructs a new bootstrapper
func NewRunnable(cacheClient client.Client, CredentialsNamespace string, provider devops.AnnotationProvider) sigsmanager.Runnable {
	return &Runnable{CacheClient: cacheClient, CredentialsNamespace: CredentialsNamespace, Provider: provider}
}
