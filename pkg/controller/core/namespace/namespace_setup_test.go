package namespace_test

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"fmt"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"testing"

	"alauda.io/devops-apiserver/pkg/controller/core/namespace"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	devopsctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	sigsctrlmock "alauda.io/devops-apiserver/pkg/mock/sigs.k8s.io/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestNamespaceController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("namespace.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/core/namespace", []Reporter{junitReporter})
}

var _ = Describe("Add", func() {
	// Will test the bootstrapping of the controller
	var (
		mockCtrl       *gomock.Controller
		managerMock    *devopsctrlmock.MockManager
		controllerMock *sigsctrlmock.MockController
		cacheClient    client.Client
		err            error
		controllerName string
		extraConfig    manager.ExtraConfig
	)

	// We can prepare our tests
	BeforeEach(func() {
		// starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		// constructing our mocks
		managerMock = devopsctrlmock.NewMockManager(mockCtrl)
		controllerMock = sigsctrlmock.NewMockController(mockCtrl)
		extraConfig = manager.ExtraConfig{
			CredentialsNamespace: "global-credentials",
			AnnotationProvider:   devops.NewAnnotationProvider(devops.UsedBaseDomain),
		}
		cacheClient = testtools.NewFakeClient()
		controllerName = "namespace-controller"
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	// here we really run our functions
	It("should create a new controller using manager", func() {
		// first add expectations here
		// will get our kubeclient from manager
		managerMock.EXPECT().GetClient().Return(cacheClient)
		managerMock.EXPECT().GetExtraConfig().Return(extraConfig).AnyTimes()
		managerMock.EXPECT().GetAnnotationProvider().Return(extraConfig.AnnotationProvider).AnyTimes()
		managerMock.EXPECT().NewController(
			// because the reconciler is created internally it will always be different
			// therefore no way to reference the correct expectation here
			controllerName, gomock.Any(),
		).Return(controllerMock, nil) // returning our mock here

		// will watch a namespace and regular equeue for request
		controllerMock.EXPECT().Watch(
			&source.Kind{Type: &corev1.Namespace{}},
			&handler.EnqueueRequestForObject{},
			// we use predicates here. we could expose the constructor function
			// but in order to make it work we need to make sure
			// that there it is a singleton
			gomock.Any(),
		).Return(nil)

		// invoke the method here finaly
		err = namespace.Add(managerMock)

		Expect(err).To(BeNil())
	})

	It("should return an error when failing to create controller", func() {
		// first add expectations here
		// will get our kubeclient from manager
		managerMock.EXPECT().GetClient().Return(cacheClient)
		managerMock.EXPECT().GetExtraConfig().Return(extraConfig).AnyTimes()
		managerMock.EXPECT().GetAnnotationProvider().Return(extraConfig.AnnotationProvider).AnyTimes()
		managerMock.EXPECT().NewController(
			// because the reconciler is created internally it will always be different
			// therefore no way to reference the correct expectation here
			controllerName, gomock.Any(),
		).Return(nil, fmt.Errorf("create controller error")) // returning our mock here

		// invoke the method here finaly
		err = namespace.Add(managerMock)

		Expect(err).To(Equal(fmt.Errorf("create controller error")))
	})
})

var _ = Describe("AddRunnable", testtools.GenRunnableSetup(func(mgr *devopsctrlmock.MockManager) func() {
	return func() {
		cacheClient := testtools.NewFakeClient()
		extraConfig := manager.ExtraConfig{
			CredentialsNamespace: "global-credentials",
			AnnotationProvider:   devops.NewAnnotationProvider(devops.UsedBaseDomain),
		}
		// first add expectations here
		// will get our kubeclient from manager
		mgr.EXPECT().GetClient().Return(cacheClient)
		mgr.EXPECT().GetExtraConfig().Return(extraConfig).AnyTimes()
		mgr.EXPECT().GetAnnotationProvider().Return(extraConfig.AnnotationProvider).AnyTimes()

		// initiates a new runnable that we cannot get
		mgr.EXPECT().Add(gomock.Any()).Return(nil)
		// invoke the method here finaly
		err := namespace.AddRunnable(mgr)

		Expect(err).To(BeNil())
	}
}))
