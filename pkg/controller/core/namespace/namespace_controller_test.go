package namespace_test

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/core/namespace"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"context"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {

	var (
		mockCtrl            *gomock.Controller
		cacheClient         client.Client
		namespaceReconciler *namespace.Reconciler
		request             reconcile.Request
		result              reconcile.Result
		namespaceObj        *corev1.Namespace
		err                 error
	)

	// We can prepare our tests
	BeforeEach(func() {
		provider := devops.NewAnnotationProvider(devops.UsedBaseDomain)
		// starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		// starts our basic structs
		namespaceReconciler = &namespace.Reconciler{
			Provider: provider,
		}

		cacheClient = testtools.NewFakeClient()
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	// here we really run our functions
	JustBeforeEach(func() {
		namespaceReconciler.CacheClient = cacheClient
		namespaceReconciler.CredentialsNamespace = "global-credentials"
		// runs our test here
		result, err = namespaceReconciler.Reconcile(request)

	})

	Context("namespace does not exist", func() {
		It("Should not an error", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			namespaceObj = &corev1.Namespace{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Name: "global-credentials"}, namespaceObj)
			Expect(err).To(BeNil())
			Expect(namespaceObj).ToNot(BeNil())
		})
	})
})
