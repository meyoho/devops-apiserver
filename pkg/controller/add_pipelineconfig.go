package controller

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/pipelineconfig"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, pipelineconfig.Add)
	AddToManagerFuncs = append(AddToManagerFuncs, pipelineconfig.AddRunnable)
}
