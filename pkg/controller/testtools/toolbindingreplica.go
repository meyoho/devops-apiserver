package testtools

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// GetToolBingdingReplica gets a standard resource of ToolBindingReplica
// just for test
func GetToolBingdingReplica(name, namespace string, spec devopsv1alpha1.ToolBindingReplicaSpec) *devopsv1alpha1.ToolBindingReplica {
	return &devopsv1alpha1.ToolBindingReplica{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: spec,
	}
}
