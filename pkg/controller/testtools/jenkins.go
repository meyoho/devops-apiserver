package testtools

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// GetJenkins get jenkins instance
func GetJenkins(name, host, secretName, secretNamespace string) *v1alpha1.Jenkins {
	return &v1alpha1.Jenkins{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: v1alpha1.JenkinsSpec{
			ToolSpec: v1alpha1.ToolSpec{
				HTTP: v1alpha1.HostPort{
					Host: host,
				},
				Secret: v1alpha1.SecretKeySetRef{
					SecretReference: corev1.SecretReference{
						Name:      secretName,
						Namespace: secretNamespace,
					},
				},
			},
		},
	}
}

// GetJenkinsBinding get jenkinsbinding instance
func GetJenkinsBinding(name, namespace, jenkins, secretName, secretNamespace string) *v1alpha1.JenkinsBinding {
	return &v1alpha1.JenkinsBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: v1alpha1.JenkinsBindingSpec{
			Jenkins: v1alpha1.JenkinsInstance{
				Name: jenkins,
			},
			Account: v1alpha1.UserAccount{
				Secret: v1alpha1.SecretKeySetRef{
					SecretReference: corev1.SecretReference{
						Name:      secretName,
						Namespace: secretNamespace,
					},
				},
			},
		},
	}
}
