package testtools

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

func GetPipelineConfig(name, namespace, jenkinsbinding string) *v1alpha1.PipelineConfig {
	metaTime := metav1.NewTime(time.Now())

	return &v1alpha1.PipelineConfig{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: v1alpha1.PipelineConfigSpec{
			JenkinsBinding: v1alpha1.LocalObjectReference{
				Name: jenkinsbinding,
			},
		},
		Status: v1alpha1.PipelineConfigStatus{
			Conditions: []v1alpha1.Condition{
				{
					Type:        v1alpha1.PipelineConfigConditionTypeInitialized,
					LastAttempt: &metaTime,
					Status:      v1alpha1.ConditionStatusUnknown,
				},
				{
					Type:        v1alpha1.PipelineConfigConditionTypeSynced,
					LastAttempt: &metaTime,
					Status:      v1alpha1.ConditionStatusUnknown,
				},
			},
		},
	}
}

func AddConfigParameters(config *v1alpha1.PipelineConfig, params ...v1alpha1.PipelineParameter) *v1alpha1.PipelineConfig {
	if config.Spec.Parameters == nil {
		config.Spec.Parameters = []v1alpha1.PipelineParameter{}
	}
	config.Spec.Parameters = append(config.Spec.Parameters, params...)
	return config
}

func AddConfigTriggers(config *v1alpha1.PipelineConfig, params ...v1alpha1.PipelineTrigger) *v1alpha1.PipelineConfig {
	if config.Spec.Triggers == nil {
		config.Spec.Triggers = []v1alpha1.PipelineTrigger{}
	}
	config.Spec.Triggers = append(config.Spec.Triggers, params...)
	return config
}
