package testtools

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetPipelineTemplateSync(name, namespace, git, secret string) *v1alpha1.PipelineTemplateSync {
	return &v1alpha1.PipelineTemplateSync{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: v1alpha1.PipelineTemplateSyncSpec{
			Source: v1alpha1.PipelineSource{
				Git: &v1alpha1.PipelineSourceGit{
					URI: git,
					Ref: "master",
				},
				Secret: &v1alpha1.SecretKeySetRef{
					SecretReference: corev1.SecretReference{
						Name:      secret,
						Namespace: namespace,
					},
				},
				SourceType: v1alpha1.PipelineSourceTypeGit,
			},
		},
	}
}
