package testtools

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"fmt"
	"sigs.k8s.io/controller-runtime/pkg/client"

	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	devopsctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	sigsctrlmock "alauda.io/devops-apiserver/pkg/mock/sigs.k8s.io/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

// ConstructorManagerFunc constructor
type ConstructorManagerFunc func(mgr manager.Manager) error

// GenControllerSetupTest test method generator
func GenControllerSetupTest(name string, constructor ConstructorManagerFunc, sourceKind *source.Kind, eventHandler handler.EventHandler, predicates ...interface{}) func() {

	return func() {

		// Will test the bootstrapping of the controller
		var (
			mockCtrl             *gomock.Controller
			managerMock          *devopsctrlmock.MockManager
			controllerMock       *sigsctrlmock.MockController
			cacheClient          client.Client
			k8sClient            *k8sfake.Clientset
			devopsClient         *clientset.Clientset
			thirdPartyClient     thirdparty.Interface
			err                  error
			controllerName       string
			systemNamespace      = "alauda-system"
			credentialsNamespace = "global-credentials"
			extraConfig          = manager.ExtraConfig{
				SystemNamespace:      systemNamespace,
				CredentialsNamespace: credentialsNamespace,
				AnnotationProvider:   devops.NewAnnotationProvider(devops.UsedBaseDomain),
			}
		)

		BeforeEach(func() {
			// starts mock controller
			mockCtrl = gomock.NewController(GinkgoT())
			// constructing our mocks
			managerMock = devopsctrlmock.NewMockManager(mockCtrl)
			controllerMock = sigsctrlmock.NewMockController(mockCtrl)

			cacheClient = NewFakeClient()
			k8sClient = k8sfake.NewSimpleClientset()
			devopsClient = clientset.NewSimpleClientset()
			cluserNamespace := "alauda-system"
			thirdPartyClient, _ = thirdparty.New(nil, nil, nil, "", cluserNamespace, devops.NewAnnotationProvider(devops.UsedBaseDomain))
			controllerName = name
		})

		AfterEach(func() {
			mockCtrl.Finish()
		})

		It("should create a new "+name+" controller using manager", func() {
			managerMock.EXPECT().GetClient().Return(cacheClient).AnyTimes()
			managerMock.EXPECT().GetKubeClient().Return(k8sClient).AnyTimes()
			managerMock.EXPECT().GetDevOpsClient().Return(devopsClient).AnyTimes()
			managerMock.EXPECT().GetThirdParty().Return(thirdPartyClient).AnyTimes()
			managerMock.EXPECT().GetExtraConfig().Return(extraConfig).AnyTimes()
			managerMock.EXPECT().GetAnnotationProvider().Return(extraConfig.AnnotationProvider).AnyTimes()
			managerMock.EXPECT().NewController(
				controllerName, gomock.Any(),
			).Return(controllerMock, nil).AnyTimes()
			controllerMock.EXPECT().Watch(
				gomock.Any(),
				gomock.Any(),
				//TODO: add more clear predicates
				gomock.Any(),
				//predicates...,
			).Return(nil).AnyTimes()

			err = constructor(managerMock)
			Expect(err).To(BeNil())
		})

		It("should return an error when failing to create "+name+" controller", func() {
			managerMock.EXPECT().GetClient().Return(cacheClient).AnyTimes()
			managerMock.EXPECT().GetKubeClient().Return(k8sClient).AnyTimes()
			managerMock.EXPECT().GetDevOpsClient().Return(devopsClient).AnyTimes()
			managerMock.EXPECT().GetThirdParty().Return(thirdPartyClient).AnyTimes()
			managerMock.EXPECT().GetExtraConfig().Return(extraConfig).AnyTimes()
			managerMock.EXPECT().GetAnnotationProvider().Return(extraConfig.AnnotationProvider).AnyTimes()
			managerMock.EXPECT().NewController(
				controllerName, gomock.Any(),
			).Return(nil, fmt.Errorf("create controller error")).AnyTimes()

			err = constructor(managerMock)
			Expect(err).To(Equal(fmt.Errorf("create controller error")))
		})
	}
}
