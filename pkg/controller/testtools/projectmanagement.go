package testtools

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetProjectManagementRole(ptype devopsv1alpha1.ProjectManagementType, host string, name string) *devopsv1alpha1.ProjectManagement {
	return &devopsv1alpha1.ProjectManagement{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: devopsv1alpha1.ProjectManagementSpec{
			Type: ptype,
			ToolSpec: devopsv1alpha1.ToolSpec{
				HTTP: devopsv1alpha1.HostPort{
					Host: host,
				},
			},
		},
	}
}

func GetProjectManagementBinding(name string) *devopsv1alpha1.ProjectManagementBindingList {
	bindingitems := []devopsv1alpha1.ProjectManagementBinding{
		devopsv1alpha1.ProjectManagementBinding{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: devopsv1alpha1.ProjectManagementBindingSpec{
				ProjectManagement: devopsv1alpha1.LocalObjectReference{
					Name: name,
				},
			},
		},
	}
	return &devopsv1alpha1.ProjectManagementBindingList{
		Items: bindingitems,
	}
}
