package testtools

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// GetDocumentBindingObj get an document binding object
func GetDocumentBindingObj(name, namespace, documentName, secretName, secretNamespace string, spaces ...string) *v1alpha1.DocumentManagementBinding {
	refs := make([]v1alpha1.DocumentManagementSpaceRef, 0, len(spaces))
	for _, space := range spaces {
		refs = append(refs, v1alpha1.DocumentManagementSpaceRef{
			Name: space,
		})
	}
	return &v1alpha1.DocumentManagementBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: v1alpha1.DocumentManagementBindingSpec{
			DocumentManagement: v1alpha1.LocalObjectReference{
				Name: documentName,
			},
			Secret: v1alpha1.SecretKeySetRef{
				SecretReference: corev1.SecretReference{
					Name:      secretName,
					Namespace: secretNamespace,
				},
			},
			DocumentManagementSpaceRefs: refs,
		},
	}
}

func GetDocumentManagement(name, host, secretName, secretNamespace string) *v1alpha1.DocumentManagement {
	return &v1alpha1.DocumentManagement{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: v1alpha1.DocumentManagementSpec{
			ToolSpec: v1alpha1.ToolSpec{
				HTTP: v1alpha1.HostPort{
					Host: host,
				},
				Secret: v1alpha1.SecretKeySetRef{
					SecretReference: corev1.SecretReference{
						Name:      secretName,
						Namespace: secretNamespace,
					},
				},
			},
			Type: v1alpha1.DocumentManageTypeConfluence,
		},
	}
}
