package testtools

import (
	devopsctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
)

// ManagerTestCaseFunc generates a test case for mock manager
type ManagerTestCaseFunc func(mgrMock *devopsctrlmock.MockManager) func()

// GenRunnableSetup generates test cases for Runnable
func GenRunnableSetup(testCase ManagerTestCaseFunc) func() {
	return func() {
		// Will test the bootstrapping of the runnable
		var (
			mockCtrl    *gomock.Controller
			managerMock *devopsctrlmock.MockManager
		)

		// We can prepare our tests
		BeforeEach(func() {
			// starts mock controller
			mockCtrl = gomock.NewController(GinkgoT())
			// constructing our mocks
			managerMock = devopsctrlmock.NewMockManager(mockCtrl)
		})

		// We verify our mockCtrl and finish
		AfterEach(func() {
			mockCtrl.Finish()
		})

		// here we really run our functions
		It("should create a new runnable using manager", func() {
			testCase(managerMock)()
		})
	}
}
