package testtools

import (
	devclientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type ReconcileConstructorFunc func(client.Client, devclientset.Interface) reconcile.Reconciler

func GenBindingControllerTest(kind string, name, namespace string, reconcilerConstruct ReconcileConstructorFunc, devopsObjects []runtime.Object, k8sObjects []runtime.Object) func() {
	return func() {
		var (
			mockCtrl     *gomock.Controller
			cacheClient  client.Client
			devopsClient *clientset.Clientset
			request      reconcile.Request
			result       reconcile.Result
			err          error
			reconciler   reconcile.Reconciler
			//systemNamespace string
		)
		BeforeEach(func() {
			mockCtrl = gomock.NewController(GinkgoT())

			cacheClient = NewFakeClient()
			devopsClient = clientset.NewSimpleClientset()
			request.Name = name
			request.Namespace = namespace
			//systemNamespace = "alauda-system"
		})

		AfterEach(func() {
			mockCtrl.Finish()
		})

		JustBeforeEach(
			func() {
				reconciler = reconcilerConstruct(cacheClient, devopsClient)
				result, err = reconciler.Reconcile(request)
			})

		Context(kind+" does not exist", func() {
			It("should not return error ", func() {
				Expect(err).To(BeNil())
				Expect(result).To(Equal(reconcile.Result{}))
			})
		})
		Context(kind+" exists", func() {
			BeforeEach(func() {
				devopsClient = clientset.NewSimpleClientset(devopsObjects...)
				cacheClient = NewFakeClient(append(k8sObjects, devopsObjects...)...)
			})
			It("should not return error and should have multiple actions", func() {
				Expect(err).To(BeNil())
				Expect(result).To(Equal(reconcile.Result{}))

			})
		})
	}
}
