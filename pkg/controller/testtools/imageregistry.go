package testtools

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

func GetImageRegistry(imageType devopsv1alpha1.ImageRegistryType, host string, name string) *devopsv1alpha1.ImageRegistry {
	return &devopsv1alpha1.ImageRegistry{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: devopsv1alpha1.ImageRegistrySpec{
			Type: imageType,
			ToolSpec: devopsv1alpha1.ToolSpec{
				HTTP: devopsv1alpha1.HostPort{
					Host: host,
				},
			},
		},
	}
}

func GetImageRegistryBinding(registry devopsv1alpha1.LocalObjectReference, name, namespace string) *devopsv1alpha1.ImageRegistryBinding {
	return &devopsv1alpha1.ImageRegistryBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: devopsv1alpha1.ImageRegistryBindingSpec{
			ImageRegistry: registry,
			Secret:        devopsv1alpha1.SecretKeySetRef{},
			RepoInfo: devopsv1alpha1.ImageRegistryBindingRepo{
				Repositories: []string{"test"},
			},
		},
	}
}

func GetImageRepository(registry devopsv1alpha1.LocalObjectReference, name, namespace string) *devopsv1alpha1.ImageRepository {
	return &devopsv1alpha1.ImageRepository{
		ObjectMeta: metav1.ObjectMeta{
			Name:              name,
			Namespace:         namespace,
			CreationTimestamp: metav1.NewTime(time.Now().Add(-time.Minute)),
			Annotations:       map[string]string{"scanDisabled": "false"},
		},
		Spec: devopsv1alpha1.ImageRepositorySpec{
			ImageRegistry: registry,
		},
	}
}
