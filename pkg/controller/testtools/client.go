package testtools

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
)

func GetScheme() *runtime.Scheme {
	scheme := clientgoscheme.Scheme
	_ = v1alpha1.AddToScheme(scheme)
	return scheme
}

func NewFakeClient(initObjects ...runtime.Object) client.Client {
	return fake.NewFakeClientWithScheme(GetScheme(), initObjects...)
}
