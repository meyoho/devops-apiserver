package handler

import (
	"k8s.io/client-go/util/workqueue"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
)

// QueueEventHandler queue events for later dispatch
type QueueEventHandler interface {
	CreateEvent(event.CreateEvent)
	UpdateEvent(event.UpdateEvent)
	DeleteEvent(event.DeleteEvent)
	GenericEvent(event.GenericEvent)
}

var _ handler.EventHandler = &EventEnqueueRequestForObject{}

// EventEnqueueRequestForObject enqueues a Request containing the Name and Namespace of the object that is the source of the Event.
// (e.g. the created / deleted / updated objects Name and Namespace).  handler.EnqueueRequestForObject is used by almost all
// Controllers that have associated Resources (e.g. CRDs) to reconcile the associated Resource.
type EventEnqueueRequestForObject struct {
	Handler             handler.EventHandler
	CreateEventHandler  CreateEventHandler
	UpdateEventHandler  UpdateEventHandler
	DeleteEventHandler  DeleteEventHandler
	GenericEventHandler GenericEventHandler
}

// CreateEventHandler handle the create event
type CreateEventHandler = func(event.CreateEvent)

// UpdateEventHandler handle the update event
type UpdateEventHandler = func(event.UpdateEvent)

// DeleteEventHandler handle the delete event
type DeleteEventHandler = func(event.DeleteEvent)

// GenericEventHandler handle the generic event
type GenericEventHandler = func(event.GenericEvent)

// Create implements EventHandler
func (e *EventEnqueueRequestForObject) Create(evt event.CreateEvent, q workqueue.RateLimitingInterface) {
	e.Handler.Create(evt, q)
	if e.CreateEventHandler != nil {
		e.CreateEventHandler(evt)
	}
}

// Update implements EventHandler
func (e *EventEnqueueRequestForObject) Update(evt event.UpdateEvent, q workqueue.RateLimitingInterface) {
	e.Handler.Update(evt, q)
	if e.UpdateEventHandler != nil {
		e.UpdateEventHandler(evt)
	}
}

// Delete implements EventHandler
func (e *EventEnqueueRequestForObject) Delete(evt event.DeleteEvent, q workqueue.RateLimitingInterface) {
	e.Handler.Delete(evt, q)
	if e.DeleteEventHandler != nil {
		e.DeleteEventHandler(evt)
	}
}

// Generic implements EventHandler
func (e *EventEnqueueRequestForObject) Generic(evt event.GenericEvent, q workqueue.RateLimitingInterface) {
	e.Handler.Generic(evt, q)
	if e.GenericEventHandler != nil {
		e.GenericEventHandler(evt)
	}
}
