package handler_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestHandler(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("handler.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/handler", []Reporter{junitReporter})
}
