package handler_test

import (
	"alauda.io/devops-apiserver/pkg/controller/handler"
	metamock "alauda.io/devops-apiserver/pkg/mock/apimachinery/meta"
	runtimemock "alauda.io/devops-apiserver/pkg/mock/apimachinery/runtime"
	workqueuemock "alauda.io/devops-apiserver/pkg/mock/client-go/workqueue"
	devopsctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	ctrlmock "alauda.io/devops-apiserver/pkg/mock/sigs.k8s.io/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"

	// . "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/event"
)

var _ = Describe("EventEnqueueRequestForObject", func() {
	var (
		ctrl             *gomock.Controller
		handlerMock      *ctrlmock.MockEventHandler
		queueMock        *workqueuemock.MockRateLimitingInterface
		objectMock       *metamock.MockObject
		runObjectMock    *runtimemock.MockObject
		eventQueue       *handler.EventEnqueueRequestForObject
		eventHandlerMock *devopsctrlmock.MockQueueEventHandler
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		handlerMock = ctrlmock.NewMockEventHandler(ctrl)
		queueMock = workqueuemock.NewMockRateLimitingInterface(ctrl)
		objectMock = metamock.NewMockObject(ctrl)
		runObjectMock = runtimemock.NewMockObject(ctrl)
		eventHandlerMock = devopsctrlmock.NewMockQueueEventHandler(ctrl)

		eventQueue = &handler.EventEnqueueRequestForObject{
			Handler:             handlerMock,
			CreateEventHandler:  eventHandlerMock.CreateEvent,
			UpdateEventHandler:  eventHandlerMock.UpdateEvent,
			DeleteEventHandler:  eventHandlerMock.DeleteEvent,
			GenericEventHandler: eventHandlerMock.GenericEvent,
		}
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	It("CreateEvent should execute correctly", func() {

		event := event.CreateEvent{
			Meta:   objectMock,
			Object: runObjectMock,
		}

		handlerMock.EXPECT().Create(event, queueMock).MaxTimes(1)

		eventHandlerMock.EXPECT().CreateEvent(event)

		eventQueue.Create(event, queueMock)
	})

	It("UpdateEvent should execute correctly", func() {

		event := event.UpdateEvent{
			MetaNew:   objectMock,
			ObjectNew: runObjectMock,
			MetaOld:   nil,
			ObjectOld: nil,
		}

		handlerMock.EXPECT().Update(event, queueMock).MaxTimes(1)

		eventHandlerMock.EXPECT().UpdateEvent(event)

		eventQueue.Update(event, queueMock)
	})

	It("DeleteEvent should execute correctly", func() {

		event := event.DeleteEvent{
			Meta:   objectMock,
			Object: runObjectMock,
		}

		handlerMock.EXPECT().Delete(event, queueMock).MaxTimes(1)

		eventHandlerMock.EXPECT().DeleteEvent(event)

		eventQueue.Delete(event, queueMock)
	})

	It("GenericEvent should execute correctly", func() {

		event := event.GenericEvent{
			Meta:   objectMock,
			Object: runObjectMock,
		}

		handlerMock.EXPECT().Generic(event, queueMock).MaxTimes(1)

		eventHandlerMock.EXPECT().GenericEvent(event)

		eventQueue.Generic(event, queueMock)
	})
})
