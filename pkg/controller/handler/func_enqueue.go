package handler

import (
	"k8s.io/client-go/util/workqueue"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
)

var _ handler.EventHandler = &FuncEnqueueRequestForObject{}

// FuncEnqueueRequestForObject enqueues a Request containing the Name and Namespace of the object that is the source of the Event.
// Do function provided.
type FuncEnqueueRequestForObject struct {
	CreateFunc  CreateEventHandler
	UpdateFunc  UpdateEventHandler
	DeleteFunc  DeleteEventHandler
	GenericFunc GenericEventHandler
}

// Create implements EventHandler
func (f *FuncEnqueueRequestForObject) Create(evt event.CreateEvent, q workqueue.RateLimitingInterface) {
	if f.CreateFunc != nil {
		f.CreateFunc(evt)
	}
}

// Update implements EventHandler
func (f *FuncEnqueueRequestForObject) Update(evt event.UpdateEvent, q workqueue.RateLimitingInterface) {
	if f.UpdateFunc != nil {
		f.UpdateFunc(evt)
	}
}

// Delete implements EventHandler
func (f *FuncEnqueueRequestForObject) Delete(evt event.DeleteEvent, q workqueue.RateLimitingInterface) {
	if f.DeleteFunc != nil {
		f.DeleteFunc(evt)
	}
}

// Generic implements EventHandler
func (f *FuncEnqueueRequestForObject) Generic(evt event.GenericEvent, q workqueue.RateLimitingInterface) {
	if f.GenericFunc != nil {
		f.GenericFunc(evt)
	}
}
