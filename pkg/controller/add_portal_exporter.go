package controller

import "alauda.io/devops-apiserver/pkg/controller/devops/portalexporter"

func init() {
	AddToManagerFuncs = append(AddToManagerFuncs, portalexporter.AddRunnable)
}
