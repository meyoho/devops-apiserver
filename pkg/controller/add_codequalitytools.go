package controller

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/codequalitybinding"
	"alauda.io/devops-apiserver/pkg/controller/devops/codequalityproject"
	"alauda.io/devops-apiserver/pkg/controller/devops/codequalitytool"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, codequalitytool.Add)
	//AddToManagerFuncs = append(AddToManagerFuncs, codequalitytool.AddRoleSync)
	AddToManagerFuncs = append(AddToManagerFuncs, codequalitybinding.Add)
	AddToManagerFuncs = append(AddToManagerFuncs, codequalityproject.Add)

}
