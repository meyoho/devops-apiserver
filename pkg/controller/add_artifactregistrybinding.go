package controller

import "alauda.io/devops-apiserver/pkg/controller/devops/artifactregistrybinding"

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, artifactregistrybinding.Add)

}
