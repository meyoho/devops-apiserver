package controller

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/artifactregistry"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, artifactregistry.Add)
	//AddToManagerFuncs = append(AddToManagerFuncs, artifactregistry.AddRoleSync)
}
