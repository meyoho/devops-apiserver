package controller

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/imageregistry"
	"alauda.io/devops-apiserver/pkg/controller/devops/imageregistrybinding"
	"alauda.io/devops-apiserver/pkg/controller/devops/imagerepository"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, imageregistry.Add)
	AddToManagerFuncs = append(AddToManagerFuncs, imageregistrybinding.Add)
	AddToManagerFuncs = append(AddToManagerFuncs, imagerepository.Add)
	AddToManagerFuncs = append(AddToManagerFuncs, imageregistrybinding.AddDockerSecret)
	//AddToManagerFuncs = append(AddToManagerFuncs, imageregistry.AddRoleSync)
}
