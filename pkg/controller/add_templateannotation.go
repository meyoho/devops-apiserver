package controller

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/templateannotation"
	"alauda.io/devops-apiserver/pkg/controller/devops/templatetaskannotation"
)

func init() {
	AddToManagerFuncs = append(AddToManagerFuncs, templateannotation.AddRunnable)
	AddToManagerFuncs = append(AddToManagerFuncs, templatetaskannotation.AddRunnable)

}
