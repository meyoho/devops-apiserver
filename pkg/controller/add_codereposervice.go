package controller

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/coderepobinding"
	"alauda.io/devops-apiserver/pkg/controller/devops/codereposervice"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, codereposervice.Add)
	AddToManagerFuncs = append(AddToManagerFuncs, coderepobinding.Add)
	// AddToManagerFuncs = append(AddToManagerFuncs, coderepository.Add)
	//AddToManagerFuncs = append(AddToManagerFuncs, codereposervice.AddRoleSync)
}
