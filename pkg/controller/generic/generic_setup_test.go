package generic_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestGeneric(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("generic.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/generic", []Reporter{junitReporter})
}
