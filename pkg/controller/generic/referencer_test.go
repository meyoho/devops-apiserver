package generic_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("TestGenericReferencer", func() {
	var (
		mockCtrl     *gomock.Controller
		devopsClient *clientset.Clientset
		referencer   generic.Referencer

		pipelinetemplate     *v1alpha1.PipelineTemplate
		pipelineconfig       *v1alpha1.PipelineConfig
		pipelinetasktemplate *v1alpha1.PipelineTaskTemplate
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		devopsClient = clientset.NewSimpleClientset()

		pipelinetemplate = &v1alpha1.PipelineTemplate{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "some",
				Namespace: "default",
				Annotations: map[string]string{
					v1alpha1.AnnotationsTemplateName:    "some",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
				Labels: map[string]string{
					v1alpha1.AnnotationsTemplateName:    "some",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
					"alauda.io/latest":                  "true",
				},
			},
			Spec: v1alpha1.PipelineTemplateSpec{
				Stages: []v1alpha1.PipelineStage{
					v1alpha1.PipelineStage{
						Tasks: []v1alpha1.PipelineTemplateTask{
							v1alpha1.PipelineTemplateTask{
								Name: "tasktemplate.1.0.0",
							},
						},
					},
				},
			},
		}

		pipelineconfig = &v1alpha1.PipelineConfig{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pipeline-config",
				Namespace: "default",
				Labels: map[string]string{
					v1alpha1.LabelTemplateKind:    v1alpha1.TypePipelineTemplate,
					v1alpha1.LabelTemplateName:    "some",
					v1alpha1.LabelTemplateVersion: "1.0.0",
				},
			},
		}

		pipelinetasktemplate = &v1alpha1.PipelineTaskTemplate{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "tasktemplate.1.0.0",
				Namespace: "default",
				Annotations: map[string]string{
					v1alpha1.AnnotationsTemplateName:    "tasktemplate",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
				Labels: map[string]string{
					v1alpha1.AnnotationsTemplateName:    "tasktemplate",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
			},
		}
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Context("PipelineTemplateReferencer", func() {
		JustBeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset(pipelinetemplate)
			referencer = generic.NewPipelineTemplateReferencer(devopsClient)
		})

		It("not referenced", func() {
			refed, err := referencer.Referenced(pipelinetemplate)
			Expect(err).To(BeNil())
			Expect(refed).To(BeFalse())
		})

		It("be referenced", func() {
			devopsClient = clientset.NewSimpleClientset(pipelinetemplate, pipelineconfig)
			referencer = generic.NewPipelineTemplateReferencer(devopsClient)
			refed, err := referencer.Referenced(pipelinetemplate)
			Expect(err).To(BeNil())
			Expect(refed).To(BeTrue())
		})
	})

	Context("PipelineTaskTemplateReferencer", func() {
		JustBeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset(pipelinetasktemplate)
			referencer = generic.NewPipelineTaskTemplateReferencer(devopsClient)
		})

		It("not referenced", func() {
			refed, err := referencer.Referenced(pipelinetasktemplate)
			Expect(err).To(BeNil())
			Expect(refed).To(BeFalse())
		})

		It("be referenced", func() {
			devopsClient = clientset.NewSimpleClientset(pipelinetasktemplate, pipelinetemplate)
			referencer = generic.NewPipelineTaskTemplateReferencer(devopsClient)
			refed, err := referencer.Referenced(pipelinetasktemplate)
			Expect(err).To(BeNil())
			Expect(refed).To(BeTrue())
		})
	})

})
