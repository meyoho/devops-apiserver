package generic

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/runtime"
)

// CheckVersion checks a specific version
func CheckVersion(obj runtime.Object) {
	gvk := obj.GetObjectKind().GroupVersionKind()
	if gvk.Version == "" || gvk.Group == "" {
		gvk.Version = v1alpha1.Version
		gvk.Group = v1alpha1.GroupName
		obj.GetObjectKind().SetGroupVersionKind(gvk)
	}
}
