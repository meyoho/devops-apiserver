package generic

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"

	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	kubeListers "k8s.io/client-go/listers/core/v1"
	glog "k8s.io/klog"
)

// Conditioner evaluates and generates a condition
type Conditioner interface {
	// Check returns true if the condition should be checked
	Check() bool
	// GetCondition get a condition generated
	GetCondition() v1alpha1.BindingCondition
}

// ConditionerProcessor processes a list of conditioners
// and aglomerates its result
type ConditionerProcessor interface {
	Add(conditioner Conditioner) ConditionerProcessor
	Conditions() []v1alpha1.BindingCondition
}

// StandardConditionProcessor processes a number of conditions and
// return a list of conditions
type StandardConditionProcessor struct {
	conditioners []Conditioner
	Kind         string
	Name         string
	init         sync.Once
}

var _ ConditionerProcessor = &StandardConditionProcessor{}

func NewStandardConditionProcess(kind, name string) *StandardConditionProcessor {
	return &StandardConditionProcessor{
		Kind: kind,
		Name: name,
	}
}

func (cp *StandardConditionProcessor) Add(conditioner Conditioner) ConditionerProcessor {
	cp.init.Do(func() {
		if cp.conditioners == nil {
			cp.conditioners = []Conditioner{}
		}
	})
	cp.conditioners = append(cp.conditioners, conditioner)
	return cp
}

// Conditions generates conditions according
func (cp *StandardConditionProcessor) Conditions() (conditions []v1alpha1.BindingCondition) {
	conditions = make([]v1alpha1.BindingCondition, 0, len(cp.conditioners))
	for _, c := range cp.conditioners {
		if c.Check() {
			condition := c.GetCondition()
			glog.V(7).Infof("[%s] \"%s\" processed condition: %#v", cp.Kind, cp.Name, condition)
			conditions = append(conditions, condition)
		} else {
			glog.V(7).Infof("[%s] \"%s\" condition skipped: %#v", cp.Kind, cp.Name, c)
		}
	}
	return
}

// AlwaysCheck always check condition
type AlwaysCheck struct{}

func (AlwaysCheck) Check() bool {
	return true
}

type CheckFunction func(lastAttempt *metav1.Time) (*v1alpha1.HostPortStatus, error)

// NewServiceConditioner check service conditions
func NewServiceConditioner(name string, checkFunc CheckFunction, currentStatus *v1alpha1.HostPortStatus) *ServiceConditioner {
	return &ServiceConditioner{
		Name:          name,
		CheckFuncion:  checkFunc,
		CurrentStatus: currentStatus,
	}
}

type ServiceConditioner struct {
	AlwaysCheck
	Name          string
	CheckFuncion  func(lastAttempt *metav1.Time) (*v1alpha1.HostPortStatus, error)
	CurrentStatus *v1alpha1.HostPortStatus
}

var _ Conditioner = &ServiceConditioner{}

func (s *ServiceConditioner) GetCondition() v1alpha1.BindingCondition {
	var err error
	s.CurrentStatus, err = s.CheckFuncion(GetLastAttempt(s.CurrentStatus))
	if s.CurrentStatus != nil {
		s.CurrentStatus.Response = ""
	}
	return ConvertHTTPStatusToCondition(s.Name, s.CurrentStatus, err)
}

type GetResourceFunc func(name string) (runtime.Object, error)
type GetServiceConditionFunc func(runtime.Object, error) v1alpha1.BindingCondition

// DevOpsToolConditioner common conditioner for DevOpsTools
type DevOpsToolConditioner struct {
	AlwaysCheck
	Name                string
	GetResourceFunc     GetResourceFunc
	GetServiceCondition GetServiceConditionFunc
}

var _ Conditioner = &DevOpsToolConditioner{}

// GetCondition returns the condition for the DevOps Tool
func (s *DevOpsToolConditioner) GetCondition() v1alpha1.BindingCondition {
	return s.GetServiceCondition(s.GetResourceFunc(s.Name))
}

// NewDevOpsToolConditioner general constructor for DevOps tools
func NewDevOpsToolConditioner(name string, getFunc GetResourceFunc, getServiceConditionFunc GetServiceConditionFunc) *DevOpsToolConditioner {
	return &DevOpsToolConditioner{
		Name:                name,
		GetResourceFunc:     getFunc,
		GetServiceCondition: getServiceConditionFunc,
	}
}

// NewJenkinsToolConditioner checks jenkins as a condition
func NewJenkinsToolConditioner(name string, devopsClient clientset.Interface) *DevOpsToolConditioner {
	return NewDevOpsToolConditioner(
		name,
		func(name string) (runtime.Object, error) {
			return devopsClient.DevopsV1alpha1().Jenkinses().Get(name, metav1.GetOptions{ResourceVersion: "0"})
		},
		func(obj runtime.Object, err error) v1alpha1.BindingCondition {
			var toolConditions []v1alpha1.BindingCondition
			if obj != nil {
				jenkins, ok := obj.(*v1alpha1.Jenkins)
				if ok {
					toolConditions = v1alpha1.BindingConditions(jenkins.Status.ServiceStatus.Conditions).Get("")
				}
			}
			return ConvertToolConditionsToCondition(name, toolConditions, err)
		},
	)
}

// DevOpsToolInterfaceConditioner retrieves the tool and its conditions
type DevOpsToolInterfaceConditioner struct {
	AlwaysCheck
	Name          string
	Kind          string
	Client        clientset.InterfaceExpansion
	ToolInterface v1alpha1.ToolInterface
}

var _ Conditioner = &DevOpsToolInterfaceConditioner{}

// GetCondition generates and retrieves condition
func (c *DevOpsToolInterfaceConditioner) GetCondition() v1alpha1.BindingCondition {
	tool, err := c.Client.DevopsV1alpha1Expansion().DevOpsTool().Get(c.Kind, c.Name, metav1.GetOptions{ResourceVersion: "0"})
	var (
		toolConditions []v1alpha1.BindingCondition
	)
	if err == nil {
		c.ToolInterface = tool
		status := tool.GetStatus()
		if status != nil && status.Conditions != nil {
			toolConditions = v1alpha1.BindingConditions(status.Conditions).Get("")
		}
	}
	return ConvertToolConditionsToCondition(c.Name, toolConditions, err)
}

// GetHTTPStatus return https status for the service
func (c *DevOpsToolInterfaceConditioner) GetHTTPStatus() *v1alpha1.HostPortStatus {
	if c.ToolInterface != nil && c.ToolInterface.GetStatus() != nil {
		return c.ToolInterface.GetStatus().HTTPStatus
	}
	return nil
}

// NewDevOpsToolInterfaceConditioner constructor for DevOpsToolInterfaceConditioner
func NewDevOpsToolInterfaceConditioner(kind, name string, client clientset.InterfaceExpansion) *DevOpsToolInterfaceConditioner {
	return &DevOpsToolInterfaceConditioner{
		Name:   name,
		Kind:   kind,
		Client: client,
	}
}

func NewRecSecretConditioner(secretRef v1alpha1.SecretKeySetRef, cacheClient client.Client) *RecSecretConditioner {

	return &RecSecretConditioner{
		Name:      secretRef.Name,
		Namespace: secretRef.Namespace,
		Getter: func(namespace, name string) (*corev1.Secret, error) {
			secret := &corev1.Secret{}
			err := cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: namespace, Name: name}, secret)
			return secret, err
		},
	}
}

type RecSecretConditioner struct {
	Name      string
	Namespace string
	Secret    *corev1.Secret
	Getter    func(namespace, name string) (*corev1.Secret, error)
}

var _ Conditioner = &RecSecretConditioner{}

func (s *RecSecretConditioner) Check() bool {
	return s.Name != ""
}

func (s *RecSecretConditioner) GetCondition() v1alpha1.BindingCondition {
	var err error
	s.Secret, err = s.Getter(s.Namespace, s.Name)
	return ConvertSecretResponseToCondition(s.Name, s.Namespace, s.Secret, err)
}

type ProjectsInfoConditioner struct {
	ProjectManagement        *v1alpha1.ProjectManagement
	ProjectManagementBinding *v1alpha1.ProjectManagementBinding
	SecretName               string
	SecretNamespace          string
	ProjectInfoFunction      ProjectInfoFunc
}

type ProjectInfoFunc func(serviceName string, opts v1alpha1.ListProjectOptions) (result *v1alpha1.ProjectDataList, err error)

func NewProjectsInfoConditioner(pm *v1alpha1.ProjectManagement, pmbinding *v1alpha1.ProjectManagementBinding, secret v1alpha1.SecretKeySetRef, projectfunc ProjectInfoFunc) *ProjectsInfoConditioner {
	return &ProjectsInfoConditioner{
		ProjectManagement:        pm,
		ProjectManagementBinding: pmbinding,
		SecretName:               secret.Name,
		SecretNamespace:          secret.Namespace,
		ProjectInfoFunction:      projectfunc,
	}
}

var _ Conditioner = &ProjectsInfoConditioner{}

func (pmconditioner *ProjectsInfoConditioner) Check() bool {
	return pmconditioner.SecretName != "" && pmconditioner.ProjectManagement != nil && pmconditioner.ProjectManagementBinding != nil
}

type nameandkey struct {
	name string
	key  string
}

func (pmconditioner *ProjectsInfoConditioner) GetCondition() (condition v1alpha1.BindingCondition) {
	opts := v1alpha1.ListProjectOptions{
		SecretName: pmconditioner.SecretName,
		Namespace:  pmconditioner.SecretNamespace,
		PageSize:   "500",
		Page:       "1",
	}
	projectlist, err := pmconditioner.ProjectInfoFunction(pmconditioner.ProjectManagement.Name, opts)
	if err != nil {
		return ConverProjectsInfoToCondition("", err)
	}
	bindingprojectexist := make(map[string]bool)
	bindingprojectlist := []nameandkey{}

	for _, projectbinding := range pmconditioner.ProjectManagementBinding.Spec.ProjectManagementProjectInfos {
		bindingprojectexist[projectbinding.Key] = true
		bindingprojectlist = append(bindingprojectlist, nameandkey{projectbinding.Name, projectbinding.Key})
	}

	pmaddress := pmconditioner.ProjectManagement.Spec.HTTP.AccessURL

	projectinfolist := &ProjectInfoList{}

	projectexist := make(map[string]bool)
	// project exsit on projectmanagement tool and exsit in binding
	for _, project := range projectlist.Items {
		if bindingprojectexist[project.Annotations["key"]] {
			projectinfo := &ProjectInfo{
				Name:        project.Name,
				Leader:      project.Annotations["lead"],
				ProjectLink: fmt.Sprintf("%s/projects/%s/summary", pmaddress, project.Annotations["key"]),
				Key:         project.Annotations["key"],
				ID:          project.Annotations["id"],
				Status:      v1alpha1.ServiceStatusPhaseReady.String(),
				Message:     "",
			}
			projectinfolist.Item = append(projectinfolist.Item, *projectinfo)
			projectexist[project.Annotations["key"]] = true
		}
	}

	// the project deleted on the projectmangement tool but still exsit in binding
	for _, project := range bindingprojectlist {
		if !projectexist[project.key] {
			projectinfo := &ProjectInfo{
				Name: project.name,
				//Leader:pmaddress
				ProjectLink: "",
				Key:         "",
				ID:          "",
				Status:      v1alpha1.ServiceStatusPhaseError.String(),
				Message:     "This Project is not exsit on ProjectMangement Tool",
			}
			projectinfolist.Item = append(projectinfolist.Item, *projectinfo)
		}
	}

	message, _ := json.Marshal(projectinfolist)

	return ConverProjectsInfoToCondition(string(message), nil)
}

type ProjectInfoList struct {
	Item []ProjectInfo `json:"item"`
}

type ProjectInfo struct {
	Name        string `json:"name"`
	Leader      string `json:"leader"`
	Key         string `json:"key"`
	ProjectLink string `json:"projectlink"`
	ID          string `json:"id"`
	Status      string `json:"status"`
	Message     string `json:"message"`
}

type RecIssueOptionConditioner struct {
	ProjectManagement   *v1alpha1.ProjectManagement
	SecretName          string
	SecretNamespace     string
	BindingName         string
	IssueOptionFunction IssueOptionFunc
}

type IssueOptionFunc func(name string, issuetype *v1alpha1.IssueSearchOptions) (result *v1alpha1.IssueFilterDataList, err error)

func NewRecIssueOptionConditioner(pm *v1alpha1.ProjectManagement, bindingname string, secret v1alpha1.SecretKeySetRef, issueoptionfunc IssueOptionFunc) *RecIssueOptionConditioner {
	return &RecIssueOptionConditioner{
		ProjectManagement:   pm,
		SecretName:          secret.Name,
		SecretNamespace:     secret.Namespace,
		IssueOptionFunction: issueoptionfunc,
		BindingName:         bindingname,
	}

}

var _ Conditioner = &RecIssueOptionConditioner{}

func (issueoption *RecIssueOptionConditioner) Check() bool {
	return issueoption.SecretName != "" && issueoption.ProjectManagement != nil
}

func (issueoption *RecIssueOptionConditioner) GetCondition() v1alpha1.BindingCondition {

	opts := &v1alpha1.IssueSearchOptions{
		Type: "all",
	}
	optionlist, err := issueoption.IssueOptionFunction(issueoption.BindingName, opts)

	return ConverIssueOptionToCondition(optionlist, err)
}

func NewSecretConditioner(secretRef v1alpha1.SecretKeySetRef, lister kubeListers.SecretLister) *SecretConditioner {

	return &SecretConditioner{
		Name:      secretRef.Name,
		Namespace: secretRef.Namespace,
		Lister:    lister,
	}
}

type SecretConditioner struct {
	Name      string
	Namespace string
	Secret    *corev1.Secret
	Lister    kubeListers.SecretLister
}

var _ Conditioner = &SecretConditioner{}

func (s *SecretConditioner) Check() bool {
	return s.Name != ""
}

func (s *SecretConditioner) GetCondition() v1alpha1.BindingCondition {
	var err error
	s.Secret, err = s.Lister.Secrets(s.Namespace).Get(s.Name)
	return ConvertSecretResponseToCondition(s.Name, s.Namespace, s.Secret, err)
}

type AuthorizationFunc func(name string, opts *v1alpha1.CodeRepoServiceAuthorizeOptions) (*v1alpha1.CodeRepoServiceAuthorizeResponse, error)

type AuthorizationConditioner struct {
	Name                  string
	SecretName            string
	SecretNamespace       string
	AuthorizationFunction AuthorizationFunc
	AuthorizationResponse *v1alpha1.CodeRepoServiceAuthorizeResponse
}

var _ Conditioner = &AuthorizationConditioner{}

func NewAuthorizationConditioner(name string, secretRef v1alpha1.SecretKeySetRef, authFunc AuthorizationFunc) *AuthorizationConditioner {
	return &AuthorizationConditioner{
		Name:                  name,
		SecretName:            secretRef.Name,
		SecretNamespace:       secretRef.Namespace,
		AuthorizationFunction: authFunc,
	}
}

func (s *AuthorizationConditioner) Check() bool {
	return s.SecretName != ""
}

func (s *AuthorizationConditioner) GetCondition() v1alpha1.BindingCondition {
	opts := &v1alpha1.CodeRepoServiceAuthorizeOptions{
		SecretName: s.SecretName,
		Namespace:  s.SecretNamespace,
	}
	var err error
	s.AuthorizationResponse, err = s.AuthorizationFunction(s.Name, opts)
	return ConvertCodeRepoServiceAuthorizationToCondition(s.SecretName, s.SecretNamespace, s.AuthorizationResponse, err)
}
