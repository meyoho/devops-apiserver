package generic_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
)

var (
	gvk = schema.GroupVersionKind{
		Kind:    v1alpha1.TypeJenkins,
		Group:   v1alpha1.GroupName,
		Version: v1alpha1.Version,
	}
)

var _ = Describe("AddOwner", func() {
	var (
		owner *v1alpha1.Jenkins
		child *v1alpha1.Jenkins
		added bool
	)

	BeforeEach(func() {
		owner = testtools.GetJenkins("owner", "https://www.test", "", "")
		child = testtools.GetJenkins("child", "https://www.child.test", "", "")

		child.SetOwnerReferences([]metav1.OwnerReference{
			metav1.OwnerReference{Name: "someother"},
		})
	})

	JustBeforeEach(func() {
		added = generic.AddOwner(child, owner, gvk)
	})

	It("should add a owner reference to child", func() {
		Expect(added).To(BeTrue())
		Expect(child.GetOwnerReferences()).To(HaveLen(2))

		Expect(generic.HasOwner(child, owner)).To(BeTrue())
	})
})

var _ = Describe("RemoveOwner", func() {
	var (
		owner   *v1alpha1.Jenkins
		child   *v1alpha1.Jenkins
		removed bool
	)

	BeforeEach(func() {
		owner = testtools.GetJenkins("owner", "https://www.test", "", "")
		child = testtools.GetJenkins("child", "https://www.child.test", "", "")
		owner.SetUID(types.UID("123"))
		generic.AddOwner(child, owner, gvk)
	})

	JustBeforeEach(func() {
		removed = generic.RemoveOwner(child, owner)
	})

	It("should remove owner reference from child", func() {
		Expect(removed).To(BeTrue())
		Expect(child.GetOwnerReferences()).To(HaveLen(0))
	})
})
