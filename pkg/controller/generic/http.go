package generic

import (
	"io/ioutil"
	"net/http"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/util/generic"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

var (
	defaultClient = generic.NewHTTPClient()
)

func CheckService(url string, header map[string]string) (status *v1alpha1.HostPortStatus, err error) {
	var req *http.Request
	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}

	if header == nil {
		header = map[string]string{}
	}

	if len(header) > 0 {
		for key, val := range header {
			req.Header.Add(key, val)
		}
	}

	glog.V(3).Infof("Check Service url: %s; req.Header: %v", url, req.Header)
	client := generic.NewHTTPClient()
	return ConvertResponseToHostPortStatus(client.Do(req))
}

func ConvertResponseToHostPortStatus(response *http.Response, err error) (*v1alpha1.HostPortStatus, error) {
	var data []byte
	lastAttempt := metav1.Now()
	status := &v1alpha1.HostPortStatus{
		StatusCode:  -1,
		LastAttempt: &lastAttempt,
	}

	if err != nil {
		status.ErrorMessage = err.Error()
	}

	if response != nil {
		status.StatusCode = response.StatusCode
		if !response.Close {
			defer response.Body.Close()
			data, err = ioutil.ReadAll(response.Body)
			if err != nil {
				return status, err
			}
			status.Response = string(data)
		}
	}
	return status, err
}

// BasicAuth does a basic auth request based on the provided url, username, password and transport
func BasicAuth(url, username, password string) (status *v1alpha1.HostPortStatus, err error) {
	var (
		req            *http.Request
		taigahost      = "api.taiga.io"
		taigaaccessurl = "https://tree.taiga.io/"
	)

	//only public taiga's host is different accessurl
	if strings.Contains(url, taigahost) {
		url = taigaaccessurl
	}

	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}

	var header map[string]string
	if username != "" && password != "" {
		req.SetBasicAuth(username, password)
		header = map[string]string{
			"Authorization": req.Header.Get("Authorization"),
		}
	}

	return CheckService(url, header)
}

// GetBasicAuth constructor function for a basic auth request
func GetBasicAuth(url, username, password string) func(*metav1.Time) (*v1alpha1.HostPortStatus, error) {
	return func(lastAttempt *metav1.Time) (*v1alpha1.HostPortStatus, error) {
		return BasicAuth(url, username, password)
	}
}
