package generic

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	corev1 "k8s.io/api/core/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ConvertHTTPStatusToCondition get a service status and transform into a BindingCondition
func ConvertHTTPStatusToCondition(name string, status *v1alpha1.HostPortStatus, err error) (condition v1alpha1.BindingCondition) {
	condition.Type = "HTTPStatus"
	condition.Name = name
	if status == nil {
		return
	}

	condition.LastAttempt = status.LastAttempt
	if err != nil || status.StatusCode >= 500 || status.StatusCode == http.StatusNotFound {
		condition.Status = v1alpha1.ServiceStatusPhaseError.String()
		condition.Reason = string(metav1.StatusReasonServiceUnavailable)
		if err != nil {
			condition.Message = err.Error()
		} else {
			condition.Message = fmt.Sprintf("Service response: status %d", status.StatusCode)
		}
	} else {
		condition.Status = v1alpha1.ServiceStatusPhaseReady.String()
		condition.Reason = ""
		condition.Message = ""
	}
	return
}

// ConvertSecretResponseToCondition convert a secret get response into a condition
func ConvertSecretResponseToCondition(name, namespace string, _ *corev1.Secret, err error) (condition v1alpha1.BindingCondition) {
	condition.Type = "Secret"
	condition.Name = name
	condition.Namespace = namespace
	condition.LastAttempt = MetaTimeNow()
	if err != nil {
		condition.Status = v1alpha1.ServiceStatusPhaseError.String()
		condition.Reason = string(errors.ReasonForError(err))
		condition.Message = err.Error()
	} else {
		condition.Status = v1alpha1.ServiceStatusPhaseReady.String()
		condition.Reason = ""
		condition.Message = ""
	}
	return
}

// ConvertCodeRepoServiceAuthorizationToCondition get a condition based on authorization response
func ConvertCodeRepoServiceAuthorizationToCondition(name, namespace string, auth *v1alpha1.CodeRepoServiceAuthorizeResponse, err error) (condition v1alpha1.BindingCondition) {
	condition.Type = "Authorization"
	condition.Name = name
	condition.Namespace = namespace

	condition.LastAttempt = MetaTimeNow()

	switch {
	case err != nil:
		condition.Status = v1alpha1.ServiceStatusPhaseError.String()
		condition.Reason = string(errors.ReasonForError(err))
		condition.Message = err.Error()
	case auth != nil && auth.AuthorizeUrl != "":
		condition.Status = v1alpha1.ServiceStatusNeedsAuthorization.String()
		condition.Reason = v1alpha1.StatusNeedsAuthorization
		condition.Message = fmt.Sprintf("Authorization method needs human intervation. Authorization URL: \"%s\"", auth.AuthorizeUrl)
	case auth != nil && auth.AuthorizeUrl == "":
		condition.Status = v1alpha1.ServiceStatusPhaseReady.String()
		condition.Reason = ""
		condition.Message = ""
	}
	return
}

// ConvertUserCoundToCondition convert some user count data to a Condition
func ConvertUserCoundToCondition(name string, userCount int, err error) (condition v1alpha1.BindingCondition) {
	condition.Type = v1alpha1.ProjectmanagementStatusTypeUserCount
	condition.Name = name

	condition.LastAttempt = MetaTimeNow()
	if err != nil {
		condition.Status = v1alpha1.ServiceStatusPhaseError.String()
		condition.Reason = string(errors.ReasonForError(err))
		condition.Message = err.Error()
	} else {
		condition.Status = v1alpha1.ServiceStatusPhaseReady.String()
		condition.Reason = strconv.Itoa(userCount)
		condition.Message = fmt.Sprintf("Instance have %d users.", userCount)
	}
	return condition
}

func ConverIssueOptionToCondition(optionlist *v1alpha1.IssueFilterDataList, err error) (condition v1alpha1.BindingCondition) {

	message, _ := json.Marshal(optionlist)

	condition.Type = v1alpha1.ProjectmanagementIssueOption
	condition.Name = v1alpha1.ProjectmanagementIssueOptionName
	condition.LastAttempt = MetaTimeNow()

	if err != nil {
		condition.Status = v1alpha1.ServiceStatusPhaseError.String()
		condition.Reason = string(errors.ReasonForError(err))
		condition.Message = err.Error()
	} else {
		condition.Status = v1alpha1.ServiceStatusPhaseReady.String()
		condition.Reason = ""
		condition.Message = string(message)

	}
	return condition
}

func ConverProjectsInfoToCondition(message string, err error) (condition v1alpha1.BindingCondition) {
	condition.Type = v1alpha1.ProjectmanagementProjectType
	condition.Name = v1alpha1.ProjectmanagementProjectName
	condition.LastAttempt = MetaTimeNow()

	if err != nil {
		condition.Status = v1alpha1.ServiceStatusPhaseError.String()
		condition.Reason = string(errors.ReasonForError(err))
		condition.Message = err.Error()
	} else {
		condition.Status = v1alpha1.ServiceStatusPhaseReady.String()
		condition.Reason = ""
		condition.Message = string(message)

	}
	return condition
}

func ConvertToolConditionsToCondition(name string, conditions []v1alpha1.BindingCondition, err error) (condition v1alpha1.BindingCondition) {
	if len(conditions) > 0 {
		condition = conditions[0]

	} else {
		condition.Type = "HTTPStatus"
		condition.Name = name
		condition.LastAttempt = MetaTimeNow()
		if err != nil {
			condition.Status = v1alpha1.ServiceStatusPhaseError.String()
			condition.Reason = string(errors.ReasonForError(err))
			condition.Message = err.Error()
		} else {
			condition.Status = v1alpha1.ServiceStatusPhaseReady.String()
			condition.Reason = ""
			condition.Message = ""
		}
	}
	return
}

func ConvertErrorToCondition(name, namespace, conditionType string, err error) (condition v1alpha1.BindingCondition) {
	condition.Type = conditionType
	condition.Name = name
	condition.Namespace = namespace
	condition.LastAttempt = MetaTimeNow()
	if err != nil {
		condition.Status = v1alpha1.ServiceStatusPhaseError.String()
		condition.Reason = string(errors.ReasonForError(err))
		condition.Message = err.Error()
	} else {
		condition.Status = v1alpha1.ServiceStatusPhaseReady.String()
		condition.Reason = ""
		condition.Message = ""
	}
	return
}

func MetaTimeNow() *metav1.Time {
	now := metav1.NewTime(time.Now())
	return &now
}

// GetServicePhase get service from conditions, we just care about some condtion of tool, and ignore some conditon lik RoleSync or DockerSecretSync
// as default , the owner should be empty
func GetServicePhase(owner string, conditions []v1alpha1.BindingCondition) (phase v1alpha1.ServiceStatusPhase, reason, message string) {
	phase = v1alpha1.ServiceStatusPhaseReady
	for _, c := range conditions {
		if c.Owner != owner {
			continue
		}

		if c.Status != v1alpha1.ServiceStatusPhaseReady.String() {
			phase = v1alpha1.ServiceStatusPhaseError
			reason = c.Reason
			message = c.Message
			break
		}
	}
	return
}

func GetServicePhaseStatus(conditions []v1alpha1.BindingCondition, status v1alpha1.ServiceStatus) v1alpha1.ServiceStatus {
	status.Phase, status.Reason, status.Message = GetServicePhase("", conditions)
	status.Conditions = v1alpha1.BindingConditions(status.Conditions).ReplaceBy("", conditions)
	return status
}

func ShouldConditionalComputation(httpStatus *v1alpha1.HostPortStatus, checkTTL time.Duration) bool {
	return !IsCheckedInLastAttempt(httpStatus, checkTTL)
}

func ShouldCheckStatus(_ v1alpha1.ServiceStatusPhase, httpStatus *v1alpha1.HostPortStatus, checkTTL time.Duration) bool {
	return !IsCheckedInLastAttempt(httpStatus, checkTTL)
}

// ShouldUpdateService returns true if should update service
func ShouldUpdateService(previousPhase, currentPhase v1alpha1.ServiceStatusPhase, currentConditions, previousConditions []v1alpha1.BindingCondition, httpStatus *v1alpha1.HostPortStatus, checkTTL time.Duration) bool {
	return previousPhase != currentPhase || len(currentConditions) != len(previousConditions) || !IsCheckedInLastAttempt(httpStatus, checkTTL)
}

func ShouldUpdateByServiceStatus(old v1alpha1.ServiceStatus, new v1alpha1.ServiceStatus, checkTTL time.Duration) bool {
	return ShouldUpdateService(old.Phase, new.Phase, new.Conditions, old.Conditions, old.HTTPStatus, checkTTL)
}

// IsCheckedInLastAttempt return true if checked in the last attempt
func IsCheckedInLastAttempt(status *v1alpha1.HostPortStatus, ttlCheck time.Duration) bool {
	return status != nil && IsNotExpired(status.LastAttempt, ttlCheck)
}

// IsNotExpired returns true if time is afte the ttl
func IsNotExpired(update *metav1.Time, ttl time.Duration) bool {
	return update != nil && update.Add(ttl).After(time.Now())
}

// GetLastAttempt get the last attempt
func GetLastAttempt(status *v1alpha1.HostPortStatus) *metav1.Time {
	if status == nil || status.LastAttempt == nil {
		return nil
	}
	return status.LastAttempt
}

// ShouldUpdateBinding returns true if meets any condition
func ShouldUpdateBinding(previousPhase, currentPhase v1alpha1.ServiceStatusPhase, currentConditions, previousConditions []v1alpha1.BindingCondition, lastUpdate *metav1.Time, checkTTL time.Duration) bool {
	return previousPhase != currentPhase || len(currentConditions) != len(previousConditions) || !IsNotExpired(lastUpdate, checkTTL)
}
