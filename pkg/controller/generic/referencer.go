package generic

import (
	"fmt"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Referencer validates a resource be referenced by some other resources.
type Referencer interface {
	Referenced(resource interface{}) (bool, error)
}

var _ Referencer = &PipelineTemplateReferencer{}
var _ Referencer = &PipelineTaskTemplateReferencer{}

// PipelineTemplateReferencer validates weather pipelinetemplate
// referenced by pipelineconfig.
type PipelineTemplateReferencer struct {
	DevOpsClient clientset.InterfaceExpansion
}

// PipelineTaskTemplateReferencer validates weather pipelinetasktemplate
// referenced by pipelinetemplate.
type PipelineTaskTemplateReferencer struct {
	DevOpsClient clientset.InterfaceExpansion
}

// NewPipelineTemplateReferencer returns a PipelineTemplateReferencer
func NewPipelineTemplateReferencer(devopsClient clientset.Interface) *PipelineTemplateReferencer {
	return &PipelineTemplateReferencer{
		DevOpsClient: clientset.NewExpansion(devopsClient),
	}
}

// NewPipelineTaskTemplateReferencer returns a PipelineTaskTemplateReferencer
func NewPipelineTaskTemplateReferencer(devopsClient clientset.Interface) *PipelineTaskTemplateReferencer {
	return &PipelineTaskTemplateReferencer{
		DevOpsClient: clientset.NewExpansion(devopsClient),
	}
}

// Referenced implements Referencer
func (r *PipelineTemplateReferencer) Referenced(resource interface{}) (bool, error) {
	pipelineTemplate, ok := resource.(devopsv1alpha1.PipelineTemplateInterface)
	if !ok {
		return true, fmt.Errorf("unknown resource: %#v, actual type is: %T", resource, resource)
	}

	labelSelector := metav1.LabelSelector{
		MatchLabels: map[string]string{
			devopsv1alpha1.LabelTemplateName:    pipelineTemplate.GetAnnotations()[devopsv1alpha1.AnnotationsTemplateName],
			devopsv1alpha1.LabelTemplateVersion: pipelineTemplate.GetAnnotations()[devopsv1alpha1.AnnotationsTemplateVersion],
			devopsv1alpha1.LabelTemplateKind:    pipelineTemplate.GetKind(),
		},
	}

	selector, err := metav1.LabelSelectorAsSelector(&labelSelector)
	if err != nil {
		return true, err
	}

	pipelineConfigList, err := r.DevOpsClient.DevopsV1alpha1().PipelineConfigs(pipelineTemplate.GetNamespace()).List(metav1.ListOptions{
		LabelSelector:   selector.String(),
		ResourceVersion: "0",
	})
	if err != nil {
		return true, err
	}

	return len(pipelineConfigList.Items) > 0, nil
}

// Referenced implements Referencer
func (r *PipelineTaskTemplateReferencer) Referenced(resource interface{}) (bool, error) {
	pipelineTaskTemplate, ok := resource.(devopsv1alpha1.PipelineTaskTemplateInterface)
	if !ok {
		return true, fmt.Errorf("unknown resource: %#v, actual type is: %T", resource, resource)
	}

	namespace, name := pipelineTaskTemplate.GetNamespace(), pipelineTaskTemplate.GetName()
	if len(namespace) != 0 {
		pipelineTemplateList, err := r.DevOpsClient.DevopsV1alpha1().PipelineTemplates(namespace).List(devopsv1alpha1.ListOptions())
		if err != nil {
			return true, err
		}

		for _, pipelineTemplate := range pipelineTemplateList.Items {
			for _, stage := range pipelineTemplate.Spec.Stages {
				for _, task := range stage.Tasks {
					if task.Name == name {
						return true, nil
					}
				}
			}
		}

		return false, nil
	}

	// cluster
	pipelineTemplateList, err := r.DevOpsClient.DevopsV1alpha1().PipelineTemplates("").List(devopsv1alpha1.ListOptions())
	if err != nil {
		return true, err
	}
	for _, pipelineTemplate := range pipelineTemplateList.Items {
		for _, stage := range pipelineTemplate.Spec.Stages {
			for _, task := range stage.Tasks {
				if task.Name == name && task.Kind == devopsv1alpha1.TypeClusterPipelineTaskTemplate {
					return true, nil
				}
			}
		}
	}

	clusterPipelineTemplateList, err := r.DevOpsClient.DevopsV1alpha1().ClusterPipelineTemplates().List(devopsv1alpha1.ListOptions())
	if err != nil {
		return true, err
	}
	for _, clusterPipelineTemplate := range clusterPipelineTemplateList.Items {
		for _, stage := range clusterPipelineTemplate.Spec.Stages {
			for _, task := range stage.Tasks {
				if task.Name == name {
					return true, nil
				}
			}
		}
	}

	return false, nil
}
