package generic

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

// HasOwner returns true if the owner is present
func HasOwner(obj metav1.Object, owner metav1.Object) bool {
	_, ok, _ := GetOwner(obj, owner)
	return ok
}

// IsOwner returns true if the object and the OwnerReference point to the same object
func IsOwner(ref metav1.OwnerReference, obj metav1.Object) bool {
	return obj.GetUID() == ref.UID && obj.GetName() == ref.Name

}

// AddOwner adds object as owner
func AddOwner(obj, owner metav1.Object, gvk schema.GroupVersionKind) (added bool) {
	_, ok, _ := GetOwner(obj, owner)
	if !ok {
		added = true
		ref := metav1.NewControllerRef(owner, gvk)
		refs := obj.GetOwnerReferences()
		if refs == nil {
			refs = []metav1.OwnerReference{}
		}
		refs = append(refs, *ref)
		obj.SetOwnerReferences(refs)
	}
	return
}

// RemoveOwner remove object from OwnerReferences
func RemoveOwner(obj, owner metav1.Object) (removed bool) {
	index, ok, _ := GetOwner(obj, owner)
	if ok {
		removed = true
		refs := obj.GetOwnerReferences()
		refs = append(refs[:index], refs[index+1:]...)
		obj.SetOwnerReferences(refs)
	}
	return
}

// GetOwner returns owner if found
func GetOwner(obj, owner metav1.Object) (index int, ok bool, result metav1.OwnerReference) {
	index = -1
	if obj == nil || owner == nil {
		return
	}

	for i, own := range obj.GetOwnerReferences() {
		if IsOwner(own, owner) {
			index = i
			ok = true
			result = own
			return
		}
	}
	return
}
