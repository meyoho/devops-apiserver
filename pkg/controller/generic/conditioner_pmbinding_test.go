package generic_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	mocks "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Test RecIssueOptionConditioner and ProjectsInfoConditioner", func() {
	defer GinkgoRecover()

	issuefilterlist := &v1alpha1.IssueFilterDataList{
		Status: []v1alpha1.IssueFilterData{
			{
				Name: "In Progeress",
				ID:   "1",
			},
		},
		Priority: []v1alpha1.IssueFilterData{
			{
				Name: "Hight",
				ID:   "1",
			},
		},
		IssueType: []v1alpha1.IssueFilterData{
			{
				Name: "Bug",
				ID:   "1",
			},
		},
	}

	result := &devopsv1alpha1.ProjectDataList{
		Items: []devopsv1alpha1.ProjectData{
			devopsv1alpha1.ProjectData{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						"key":         "TEST",
						"description": "This is TEST project",
						"leader":      "admin",
						"id":          "1",
					},
				},
			},
			devopsv1alpha1.ProjectData{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						"key":         "TEST1",
						"description": "This is TEST1 description",
						"leader":      "admin",
						"id":          "2",
					},
				},
			},
		},
	}
	mockCtrl := gomock.NewController(GinkgoT())

	pmmock := mocks.NewMockProjectManagementExpansion(mockCtrl)
	pmmock.EXPECT().ListProjects("zpyutest", v1alpha1.ListProjectOptions{
		Namespace:  "namespace",
		SecretName: "name",
		Page:       "1",
		PageSize:   "500",
	}).
		Return(result, nil)

	pmbindmock := mocks.NewMockProjectManagementBindingExpansion(mockCtrl)
	pmbindmock.EXPECT().
		IssueOptions("bindingname",
			&v1alpha1.IssueSearchOptions{
				Type: "all"}).
		Return(issuefilterlist, nil)

	pm := GetProjectManagement("Jira", "http://127.0.0.1:30000", "zpyutest")
	pmbinding := GetProjectManagementBinding(v1alpha1.LocalObjectReference{Name: "projectManagement"}, "name", "namespace")

	secret := v1alpha1.SecretKeySetRef{
		SecretReference: corev1.SecretReference{
			Name:      "name",
			Namespace: "namespace",
		},
	}

	message := `{"status":[{"name":"In Progeress","id":"1","data":null}],"issuetype":[{"name":"Bug","id":"1","data":null}],"priority":[{"name":"Hight","id":"1","data":null}]}`

	recissueoptionconditioner := generic.NewRecIssueOptionConditioner(pm, "bindingname", secret, pmbindmock.IssueOptions)
	conditon := recissueoptionconditioner.GetCondition()

	It("Should get the right RecIssueOptionCondition", func() {
		Expect(conditon.Name).To(Equal(v1alpha1.ProjectmanagementIssueOptionName))
		Expect(conditon.Status).To(Equal(v1alpha1.ServiceStatusPhaseReady.String()))
		Expect(conditon.Message).To(Equal(message))
	})

	projectinfoconditioner := generic.NewProjectsInfoConditioner(pm, pmbinding, secret, pmmock.ListProjects)

	projectinfoscondition := projectinfoconditioner.GetCondition()

	It("Test ProjectsInfoConditioner", func() {
		Expect(projectinfoscondition.Name).To(Equal("ProjectsInfo"))

		Expect(projectinfoscondition.Message).To(Equal(`{"item":[{"name":"","leader":"","key":"TEST","projectlink":"http://127.0.0.1:30000/projects/TEST/summary","id":"1","status":"Ready","message":""},{"name":"","leader":"","key":"TEST1","projectlink":"http://127.0.0.1:30000/projects/TEST1/summary","id":"2","status":"Ready","message":""}]}`))
	})

})

func GetProjectManagement(ptype devopsv1alpha1.ProjectManagementType, host string, name string) *devopsv1alpha1.ProjectManagement {
	return &devopsv1alpha1.ProjectManagement{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: devopsv1alpha1.ProjectManagementSpec{
			Type: ptype,
			ToolSpec: devopsv1alpha1.ToolSpec{
				HTTP: devopsv1alpha1.HostPort{
					Host:      host,
					AccessURL: host,
				},
			},
		},
	}
}

func GetProjectManagementBinding(projectmanagement devopsv1alpha1.LocalObjectReference, name string, namespace string) *devopsv1alpha1.ProjectManagementBinding {
	return &devopsv1alpha1.ProjectManagementBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: devopsv1alpha1.ProjectManagementBindingSpec{
			ProjectManagement: projectmanagement,
			Secret:            devopsv1alpha1.SecretKeySetRef{},
			ProjectManagementProjectInfos: []devopsv1alpha1.ProjectManagementProjectInfo{
				devopsv1alpha1.ProjectManagementProjectInfo{
					ID:   "1",
					Name: "TEST",
					Key:  "TEST",
				},
				devopsv1alpha1.ProjectManagementProjectInfo{
					ID:   "2",
					Name: "TEST1",
					Key:  "TEST1",
				},
			},
		},
	}
}
