package generic_test

import (
	v1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/generic"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type FakeConditioner struct {
	check     bool
	condition v1alpha1.BindingCondition
}

func (f FakeConditioner) Check() bool {
	return f.check
}

func (f FakeConditioner) GetCondition() v1alpha1.BindingCondition {
	return f.condition
}

var _ = Describe("StandardConditionProcessor.Conditions", func() {

	var (
		conditionProcessor generic.ConditionerProcessor
		conditions         []v1alpha1.BindingCondition
		conditioners       []generic.Conditioner
	)

	BeforeEach(func() {
		conditionProcessor = generic.NewStandardConditionProcess("somekind", "somename")
		conditioners = []generic.Conditioner{
			FakeConditioner{
				check: false,
				condition: v1alpha1.BindingCondition{
					Status: "status_not",
				},
			},
			FakeConditioner{
				check: true,
				condition: v1alpha1.BindingCondition{
					Status: "status",
				},
			},
		}
	})

	JustBeforeEach(func() {
		if conditioners != nil {
			for _, c := range conditioners {
				conditionProcessor.Add(c)
			}
		}
		conditions = conditionProcessor.Conditions()
	})

	It("should generate few number of conditions", func() {
		Expect(conditions).ToNot(BeNil())
		Expect(conditions).To(HaveLen(1))
		Expect(conditions).To(ContainElement(v1alpha1.BindingCondition{
			Status: "status",
		}))
	})
})
