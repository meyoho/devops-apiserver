package metrics

import (
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"time"
)

var _ reconcile.Reconciler = &Decorator{}

type Decorator struct {
	R          reconcile.Reconciler
	controller string
	kind       string

	reconcileTime bool
}

func (d *Decorator) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	reconcileStartTS := time.Now()
	defer func() {
		d.updateMetrics(request, time.Now().Sub(reconcileStartTS))
	}()
	return d.R.Reconcile(request)
}

func (d *Decorator) WithResourceReconcileTime() *Decorator {
	d.reconcileTime = true
	return d
}

func (d *Decorator) updateMetrics(request reconcile.Request, reconcileTime time.Duration) {
	if d.reconcileTime {
		ResourceReconcileTime.WithLabelValues(d.controller, d.kind, request.Namespace, request.Name).Set(reconcileTime.Seconds())
		ControllerReconcileTimeCorrection.WithLabelValues(d.controller, d.kind, request.Namespace, request.Name).Observe(reconcileTime.Seconds())
	}
}

func DecorateReconciler(controller string, kind string, r reconcile.Reconciler) *Decorator {
	d := &Decorator{R: r, kind: kind, controller: controller}
	return d
}
