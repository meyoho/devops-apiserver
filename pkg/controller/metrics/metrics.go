package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"sigs.k8s.io/controller-runtime/pkg/metrics"
)

var (
	ResourceReconcileTime = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "resource_reconcile_time_seconds",
		Help: "Length of time per reconciliation per k8s resource",
	}, []string{"controller", "kind", "namespace", "name"})

	ControllerReconcileTimeCorrection = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "controller_runtime_reconcile_correction_time_seconds",
		Help: "Length of time per reconciliation per k8s resource",
	}, []string{"controller", "kind", "namespace", "name"})
)

func init() {
	metrics.Registry.MustRegister(
		ResourceReconcileTime,
		ControllerReconcileTimeCorrection,
	)
}
