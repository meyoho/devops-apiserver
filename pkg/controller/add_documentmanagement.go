package controller

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/documentmanagement"
	"alauda.io/devops-apiserver/pkg/controller/devops/documentmanagementbinding"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, documentmanagement.Add)
	AddToManagerFuncs = append(AddToManagerFuncs, documentmanagementbinding.Add)

}
