package artifactregistrymanager_test

import (
	"context"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		mockCtrl *gomock.Controller
		//k8sClient    *k8sfake.Clientset
		cacheClient  client.Client
		devopsClient *clientset.Clientset
		request      reconcile.Request
		//thirdPartyClient  thirdparty.Interface
		result            reconcile.Result
		err               error
		armToolReconciler *reconciler.ToolReconciler
		armObject         *devopsv1alpha1.ArtifactRegistryManager
		//systemNamespace   = "new-alauda-systen"
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		//k8sClient = k8sfake.NewSimpleClientset()
		cacheClient = testtools.NewFakeClient()
		devopsClient = clientset.NewSimpleClientset()
		//thirdPartyClient, _ = thirdparty.New(nil, k8sClient, nil, "", systemNamespace, devopsv1alpha1.NewAnnotationProvider(devopsv1alpha1.UsedBaseDomain))

		request.Name = "Nexus"

		armToolReconciler = reconciler.NewToolReconciler("artifactrogistrymanager-controller", devopsv1alpha1.TypeArtifactRegistryManager, cacheClient, devopsClient, devopsv1alpha1.TypeArtifactRegistryManager, "artifactregistry", "artifactRegistryManager", devopsv1alpha1.AnnotationProvider{})
		armObject = GetArmTool(devopsv1alpha1.ArtifactRegistryManagerTypeNexus, "http://nexus.io", request.Name)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	JustBeforeEach(func() {
		result, err = armToolReconciler.Reconcile(request)
	})

	Context("arm does not exist", func() {
		It("shoud not return error", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("arm can be update", func() {
		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(armObject)
			armToolReconciler = reconciler.NewToolReconciler("artifactrogistrymanager-controller", devopsv1alpha1.TypeArtifactRegistryManager, cacheClient, devopsClient, devopsv1alpha1.TypeArtifactRegistryManager, "artifactregistry", "artifactRegistryManager", devopsv1alpha1.AnnotationProvider{})
		})

		It("get a artifactrogistrymanager", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			armObject := &devopsv1alpha1.ArtifactRegistryManager{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Name: request.Name}, armObject)
			Expect(err).To(BeNil())
			Expect(armObject).NotTo(BeNil())
			Expect(armObject.Name).To(Equal(request.Name))
		})

		It("update a arm", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

		})
	})
})

func GetArmTool(toolType devopsv1alpha1.ArtifactRegistryManagerType, host string, name string) *devopsv1alpha1.ArtifactRegistryManager {
	return &devopsv1alpha1.ArtifactRegistryManager{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: devopsv1alpha1.ArtifactRegistryManagerSpec{
			Type: toolType,
			ToolSpec: devopsv1alpha1.ToolSpec{
				HTTP: devopsv1alpha1.HostPort{
					Host: host,
				},
			},
		},
	}
}
