package artifactregistrymanager

import (
	"context"
	"time"

	"fmt"

	"alauda.io/devops-apiserver/pkg/controller/metrics"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	klogr "k8s.io/klog/klogr"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName = "artifactregistrymanager-controller"
)

func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	return NewReconciler(mgr.GetClient(), mgr.GetDevOpsClient(), mgr.GetExtraConfig(), mgr.GetAnnotationProvider())
}

func NewReconciler(cacheClient client.Client, devopsClient clientset.Interface, extra manager.ExtraConfig, provider v1alpha1.AnnotationProvider) reconcile.Reconciler {
	r := &Reconciler{
		CacheClient:     cacheClient,
		DevOpsClient:    clientset.NewExpansion(devopsClient),
		systemNamespace: extra.SystemNamespace,
	}

	r.ItemsDeleter = reconciler.NewLocalGeneratedItemsDeleter(cacheClient, r.DevOpsClient,
		v1alpha1.TypeArtifactRegistryManager, "artifactregistries",
		func(parent types.NamespacedName) *metav1.LabelSelector {
			return &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"artifactRegistryManager": parent.Name,
				}}
		}, provider)

	r.ItemsDeleter.IsClusterScopedItems = true

	return metrics.DecorateReconciler(
		controllerName,
		v1alpha1.TypeArtifactRegistryManager, r,
	).WithResourceReconcileTime()
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.ArtifactRegistryManager{}},
		&handler.EnqueueRequestForObject{},
		predicate.EmptyOwnerTTL(controllerName, v1alpha1.TTLSession),
	)

	return
}

type Reconciler struct {
	reconciler.ToolReconciler
	CacheClient     client.Client
	DevOpsClient    clientset.InterfaceExpansion
	systemNamespace string

	ItemsDeleter reconciler.LocalGeneratedItemsDeleter
}

var _ reconcile.Reconciler = &Reconciler{}

func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	glog.V(5).Infof("[%s] Reconciling %s", controllerName, request)

	artifactRegistryManager := &v1alpha1.ArtifactRegistryManager{}
	err = rec.CacheClient.Get(context.TODO(), request.NamespacedName, artifactRegistryManager)

	if err != nil {
		utilruntime.HandleError(err)
		if !errors.IsNotFound(err) {
			return reconcile.Result{}, err
		}
		log := klogr.New().WithName(fmt.Sprintf("[%s]", controllerName)).WithValues("artifactregistry", request.String())
		rec.ItemsDeleter.DeleteItemsOf(log, request.NamespacedName)
		glog.V(5).Infof("[%s] artifact registry manager '%s' is not found", controllerName, request)
		return reconcile.Result{}, nil
	}

	glog.V(9).Infof("[%s] Get manager successful ", controllerName)

	managerCopy := artifactRegistryManager.DeepCopy()
	serviceName := artifactRegistryManager.GetName()
	conditioner := generic.NewStandardConditionProcess(controllerName, serviceName)
	serviceConditioner := generic.NewServiceConditioner(serviceName, generic.GetBasicAuth(managerCopy.Spec.HTTP.Host, "", ""), managerCopy.Status.HTTPStatus)
	secretConditioner := generic.NewRecSecretConditioner(artifactRegistryManager.Spec.Secret, rec.CacheClient)
	authConditioner := generic.NewAuthorizationConditioner(artifactRegistryManager.GetName(), artifactRegistryManager.Spec.Secret, rec.DevOpsClient.DevopsV1alpha1().ArtifactRegistryManagers().Authorize)
	conditioner.Add(serviceConditioner).Add(secretConditioner).Add(authConditioner)

	glog.V(9).Infof("[%s] conditioner init completed", controllerName)

	currentConditions := conditioner.Conditions()

	managerCopy.Status.Conditions = currentConditions
	glog.V(9).Infof("[%s] status.cond is %#v", controllerName, managerCopy.Status.Conditions)

	managerCopy.Status.HTTPStatus = serviceConditioner.CurrentStatus
	glog.V(9).Infof("[%s] status.httpstatus is %#v", controllerName, managerCopy.Status.HTTPStatus)

	managerCopy.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	glog.V(9).Infof("[%s] Status.LastUpdate is %#v", controllerName, managerCopy.Status.LastUpdate)

	managerCopy.Status = generic.GetServicePhaseStatus(currentConditions, managerCopy.Status)
	glog.V(9).Infof("[%s] Status is %#v", controllerName, managerCopy.Status)

	err = rec.DevOpsClient.DevopsV1alpha1Expansion().ArtifactRegistryManagers().InitManager(artifactRegistryManager.Name, &v1alpha1.ArtifactRegistryManagerOptions{})
	if err != nil {
		glog.V(5).Infof("[%s] sync script error is %#v ", controllerName, err)
	}

	glog.V(9).Infof("[%s] sync script completed", controllerName)

	err = rec.CacheClient.Update(context.TODO(), managerCopy)
	if err != nil {
		glog.Errorf("[%s] Error update artifactregistrymanager, error:%#v", controllerName, err)
		return result, errors.NewBadRequest(err.Error())
	}

	glog.V(9).Infof("[%s] Reconciling end%s", controllerName, request)

	return
}
