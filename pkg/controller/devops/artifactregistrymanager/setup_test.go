package artifactregistrymanager_test

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/artifactregistrymanager"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestArtifactRegistryManagerController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("artifactregistrymanager.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/artifactregistrymanager", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"artifactregistrymanager-controller",
		artifactregistrymanager.Add,
		&source.Kind{Type: &v1alpha1.ArtifactRegistryManager{}},
		&handler.EnqueueRequestForObject{},
	),
)
