package codequalityproject_test

import (
	"context"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/devops/codequalityproject"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		mockCtrl    *gomock.Controller
		cacheClient client.Client
		//k8sClient          *k8sfake.Clientset
		devopsClient *clientset.Clientset
		request      reconcile.Request
		result       reconcile.Result
		reconciler   reconcile.Reconciler
		//roundTripper       *mhttp.MockRoundTripper
		err                error
		codeQualityProject *devopsv1alpha1.CodeQualityProject
		//systemNamespace    = "alauda-system"
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		//k8sClient = k8sfake.NewSimpleClientset()
		devopsClient = clientset.NewSimpleClientset()
		cacheClient = testtools.NewFakeClient()
		//roundTripper = mhttp.NewMockRoundTripper(mockCtrl)
		codeQualityProject = GetCodeQualityProject()

		reconciler = codequalityproject.NewReconciler(cacheClient, devopsClient)

		request.Name = "sonar-project"
		request.Namespace = "namespace1"
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})
	JustBeforeEach(
		func() {
			result, err = reconciler.Reconcile(request)
		})

	Context("codequalitybinding does not exist", func() {
		BeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset(codeQualityProject)
			cacheClient = testtools.NewFakeClient(codeQualityProject)
			reconciler = codequalityproject.NewReconciler(cacheClient, devopsClient)
		})

		It("should delete codequalityproject ", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			cqp := &devopsv1alpha1.CodeQualityProject{}
			err := cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: codeQualityProject.Namespace, Name: codeQualityProject.Name}, cqp)
			Expect(err).NotTo(BeNil())
			isNotFoundErr := errors.IsNotFound(err)
			Expect(isNotFoundErr).To(BeTrue())
		})
	})

	Context("codequalitytool does not exist", func() {
		BeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset(codeQualityProject, GetCodeQualityBinding())
			cacheClient = testtools.NewFakeClient(codeQualityProject, GetCodeQualityBinding())
			reconciler = codequalityproject.NewReconciler(cacheClient, devopsClient)
		})

		It("should return NotFound err", func() {
			isNotFoundErr := errors.IsNotFound(err)
			Expect(isNotFoundErr).To(BeTrue())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("every thing is ready", func() {
		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(codeQualityProject)
			reconciler = codequalityproject.NewReconciler(cacheClient, devopsClient)
		})
		It("should add codequalityproject's condition", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})
})

func GetCodeQualityProject() *devopsv1alpha1.CodeQualityProject {
	return &devopsv1alpha1.CodeQualityProject{
		TypeMeta: metav1.TypeMeta{
			APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			Kind:       devopsv1alpha1.TypeCodeQualityProject,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "sonar-project",
			Namespace: "namespace1",
		},
		Spec: devopsv1alpha1.CodeQualityProjectSpec{
			CodeQualityTool: devopsv1alpha1.LocalObjectReference{
				Name: "sonar",
			},
			CodeQualityBinding: devopsv1alpha1.LocalObjectReference{
				Name: "sonar-binding",
			},
		},
	}
}

func GetCodeQualityBinding() *devopsv1alpha1.CodeQualityBinding {
	return &devopsv1alpha1.CodeQualityBinding{
		TypeMeta: metav1.TypeMeta{
			APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			Kind:       devopsv1alpha1.TypeCodeQualityBinding,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "sonar-binding",
			Namespace: "namespace1",
		},
		Spec: devopsv1alpha1.CodeQualityBindingSpec{
			CodeQualityTool: devopsv1alpha1.LocalObjectReference{
				Name: "sonar",
			},
			Secret: devopsv1alpha1.SecretKeySetRef{
				SecretReference: corev1.SecretReference{
					Name:      "github-secret",
					Namespace: "namespace1",
				},
			},
		},
	}
}

func GetCodeQualityTool() *devopsv1alpha1.CodeQualityTool {
	return &devopsv1alpha1.CodeQualityTool{
		ObjectMeta: metav1.ObjectMeta{
			Name: "sonar",
		},
		Spec: devopsv1alpha1.CodeQualityToolSpec{
			Type: "Sonarqube",
			ToolSpec: devopsv1alpha1.ToolSpec{
				HTTP: devopsv1alpha1.HostPort{
					Host: "http://sonarcloud.io",
				},
			},
		},
	}
}
