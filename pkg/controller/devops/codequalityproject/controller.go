package codequalityproject

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"

	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/source"

	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const controllerName = "codequalityproject-controller"

func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	return NewReconciler(mgr.GetClient(), mgr.GetDevOpsClient())
}

func NewReconciler(cacheClient client.Client, devopsClient clientset.Interface) reconcile.Reconciler {
	return &Reconciler{
		CacheClient:  cacheClient,
		DevOpsClient: clientset.NewExpansion(devopsClient),
	}
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.CodeQualityProject{}}, &handler.EnqueueRequestForObject{},
		predicate.Funcs{
			UpdateFunc: func(event event.UpdateEvent) bool {
				result := true

				oldObject := event.ObjectOld
				newObject := event.ObjectNew

				oldProject := oldObject.(*v1alpha1.CodeQualityProject)
				newProject := newObject.(*v1alpha1.CodeQualityProject)

				if newProject != nil && oldProject != nil && newProject.Spec.Project.LastAnalysis != nil && oldProject.Spec.Project.LastAnalysis != nil {
					glog.V(5).Infof("The new CodeQualityProject [%s] analysis date is newer than old's one [%s], will reconcile it", newProject.Name, oldProject.Name)
					result = newProject.Spec.Project.LastAnalysis.After(oldProject.Spec.Project.LastAnalysis.Time)
				}

				return result
			},
		},
	)
	return
}

type Reconciler struct {
	CacheClient  client.Client
	DevOpsClient clientset.InterfaceExpansion
}

var _ reconcile.Reconciler = &Reconciler{}

func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	glog.V(5).Infof("[%s] Reconciling %s", controllerName, request)

	project := &v1alpha1.CodeQualityProject{}
	err = rec.CacheClient.Get(context.TODO(), request.NamespacedName, project)
	if err != nil {
		if !errors.IsNotFound(err) {
			utilruntime.HandleError(err)
			return
		}
		glog.Infof("[%s] codequalityproject '%s/%s' is not found'", controllerName, request.Namespace, request.Name)
		err = nil
		return
	}

	codeQualityBindingName := project.Spec.CodeQualityBinding.Name
	codeQualityBinding := &v1alpha1.CodeQualityBinding{}
	err = rec.CacheClient.Get(context.TODO(), client.ObjectKey{Namespace: project.Namespace, Name: codeQualityBindingName}, codeQualityBinding)
	if err != nil {
		if !errors.IsNotFound(err) {
			utilruntime.HandleError(err)
			return
		}

		glog.Infof("[%s] codequalitybinding '%s' is not found, delete codequalityproject '%s/%s'", controllerName, codeQualityBindingName, project.Namespace, project.Name)
		err = rec.CacheClient.Delete(context.TODO(), project)
		if err != nil {
			glog.Errorf("[%s] can not delete codequalityproject '%s/%s', err: %v", controllerName, project.Namespace, project.Name, err)
		}

		err = nil
		return
	}

	codeQualityToolName := codeQualityBinding.Spec.CodeQualityTool.Name
	codeQualityTool := &v1alpha1.CodeQualityTool{}
	err = rec.CacheClient.Get(context.TODO(), client.ObjectKey{Name: codeQualityToolName}, codeQualityTool)
	if err != nil {
		utilruntime.HandleError(err)
		return
	}

	secret := &corev1.Secret{}
	err = rec.CacheClient.Get(context.TODO(), client.ObjectKey{Namespace: codeQualityBinding.GetSecretNamespace(), Name: codeQualityBinding.GetSecretName()}, secret)
	if err != nil {
		glog.Errorf("[%s] can not get secret '%s/%s' for codequalityproject '%s/%s, err: %v",
			controllerName, codeQualityBinding.GetSecretNamespace(), codeQualityBinding.GetSecretName(), request.Namespace, request.Name, err)
		return
	}

	projectCopy := project.DeepCopy()
	err, abortReconcile := rec.retrieveAnalysisDataToProject(projectCopy, codeQualityTool, secret)

	if abortReconcile {
		err = nil
		return
	}
	if err != nil {
		utilruntime.HandleError(err)
		return
	}

	err = rec.CacheClient.Update(context.TODO(), projectCopy)
	if err != nil {
		glog.Errorf("[%s] error when update codequalityproject '%s/%s, err: %v",
			controllerName, project.Namespace, project.Name, err)
	}

	return

}

func (rec *Reconciler) retrieveAnalysisDataToProject(projectCopy *v1alpha1.CodeQualityProject, service *v1alpha1.CodeQualityTool, secret *corev1.Secret) (err error, abortReconcile bool) {
	glog.V(5).Infof("[%s] retrieving analysis data, codequalityproject=%s/%s", controllerName, projectCopy.Namespace, projectCopy.Name)
	// check remote service project is exist or not
	isProjectExistInSonarQube, err := rec.DevOpsClient.DevopsV1alpha1Expansion().CodeQualityProjects(projectCopy.Namespace).Remote(projectCopy.Name, &v1alpha1.CodeQualityProjectOptions{ProjectKey: projectCopy.Spec.Project.ProjectKey})
	if err != nil {
		glog.Errorf("[%s] can not get project '%s/%s' in codequalitybinding '%s/%s', error: %v",
			controllerName, projectCopy.GetNamespace(), projectCopy.GetName(), projectCopy.GetNamespace(), projectCopy.Spec.CodeQualityBinding.Name, err)
		return
	}

	if !isProjectExistInSonarQube.Exist {
		glog.V(5).Infof("[%s] can not found project: %s on service, delete it", controllerName, projectCopy.Spec.Project.ProjectKey)
		err = rec.CacheClient.Delete(context.TODO(), projectCopy)
		if err != nil {
			glog.Errorf("[%s] can not delete codequalityproject '%s/%s', err: %v", controllerName, projectCopy.Namespace, projectCopy.Name, err)
		}
		glog.Infof("[%s] delete codequalityproject '%s/%s', becaust we can not found project: %s on remote server. codequalitybinding=%s",
			controllerName, projectCopy.Namespace, projectCopy.Name, projectCopy.Spec.Project.ProjectKey, projectCopy.Spec.CodeQualityBinding.Name)
		abortReconcile = true
		return
	}

	conditions, err := rec.DevOpsClient.DevopsV1alpha1Expansion().CodeQualityProjects(projectCopy.Namespace).Reports(projectCopy.Name, &v1alpha1.CodeQualityProjectOptions{ProjectKey: projectCopy.Spec.Project.ProjectKey})

	now := metav1.Now()
	if err != nil {
		projectCopy.Status.Phase = v1alpha1.ServiceStatusPhaseError
		projectCopy.Status.Message = err.Error()
		projectCopy.Status.LastUpdate = &now
		utilruntime.HandleError(err)
		err = nil
	} else {
		// If no analysis data found, delete this project
		if conditions == nil || len(conditions.Conditions) == 0 {
			glog.V(5).Infof("[%s] no analysis data for codequalityproject '%s/%s' found, will delete it", controllerName, projectCopy.Namespace, projectCopy.Name)
			err = rec.CacheClient.Delete(context.TODO(), projectCopy)
			if err != nil {
				glog.Errorf("[%s] can not delete codequalityproject '%s/%s', err: %v", controllerName, projectCopy.Namespace, projectCopy.Name, err)
			}
			abortReconcile = true
			return
		}

		lastAnalysisDate, err := rec.DevOpsClient.DevopsV1alpha1Expansion().CodeQualityProjects(projectCopy.Namespace).LastAnalysisDate(projectCopy.Name, &v1alpha1.CodeQualityProjectOptions{
			ProjectKey: projectCopy.Spec.Project.ProjectKey,
		})
		if err == nil {
			projectCopy.Spec.Project.LastAnalysis = lastAnalysisDate.Date
		}

		projectCopy.Status.Phase = v1alpha1.ServiceStatusPhaseReady
		projectCopy.Status.Message = ""
		projectCopy.Status.LastUpdate = &now
		projectCopy.Status.CodeQualityConditions = conditions.Conditions
	}

	err = nil
	return
}
