package codequalityproject_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/codequalityproject"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
	"testing"
)

func TestCodeQualityProjectController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("codequalityproject.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/codequalityproject", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"codequalityproject-controller",
		codequalityproject.Add,
		&source.Kind{Type: &v1alpha1.CodeQualityProject{}},
		&handler.EnqueueRequestForObject{},
	),
)
