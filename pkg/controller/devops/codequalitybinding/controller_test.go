package codequalitybinding_test

import (
	"context"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/metrics"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	devopsctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/controller/devops/codequalitybinding"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		mockCtrl *gomock.Controller
		//k8sClient                    *k8sfake.Clientset
		cacheClient                  client.Client
		devopsClient                 *clientset.Clientset
		request                      reconcile.Request
		result                       reconcile.Result
		codeQualityBindingReconciler *codequalitybinding.Reconciler
		codeQualityProjectSyncMock   *devopsctrlmock.MockCodeQualityProjectSyncer
		secret                       *corev1.Secret
		// roundTripper                 *mhttp.MockRoundTripper
		err                error
		codeQualityBinding *devopsv1alpha1.CodeQualityBinding
		codeQualityTool    *devopsv1alpha1.CodeQualityTool
		// systemNamespace              = "alauda-system"
		provider devopsv1alpha1.AnnotationProvider
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		//k8sClient = k8sfake.NewSimpleClientset()
		cacheClient = testtools.NewFakeClient()
		devopsClient = clientset.NewSimpleClientset()
		//roundTripper = mhttp.NewMockRoundTripper(mockCtrl)
		codeQualityBinding = GetCodeQualityBinding()
		codeQualityTool = GetCodeQualityTool()
		secret = testtools.GetSecret("sonar-secret", "namespace1", corev1.SecretTypeBasicAuth, nil)
		provider = devopsv1alpha1.AnnotationProvider{BaseDomain: "GetAnnotationProvider"}
		dr := codequalitybinding.NewReconciler(cacheClient, devopsClient, provider).(*metrics.Decorator)
		codeQualityBindingReconciler = dr.R.(*codequalitybinding.Reconciler)
		codeQualityProjectSyncMock = devopsctrlmock.NewMockCodeQualityProjectSyncer(mockCtrl)
		codeQualityBindingReconciler.Sync = codeQualityProjectSyncMock

		request.Name = "sonar-binding"
		request.Namespace = "namespace1"
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})
	JustBeforeEach(
		func() {
			result, err = codeQualityBindingReconciler.Reconcile(request)
		})

	Context("codequalitybinding does not exist", func() {
		It("should not return a error", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("codequalitytool does not exist", func() {
		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(codeQualityBinding)
			dr := codequalitybinding.NewReconciler(cacheClient, devopsClient, provider).(*metrics.Decorator)
			codeQualityBindingReconciler = dr.R.(*codequalitybinding.Reconciler)
		})

		It("should delete codequalitybinding ", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
			cqb := &devopsv1alpha1.CodeQualityBinding{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: codeQualityBinding.Namespace, Name: codeQualityBinding.Name}, cqb)
			Expect(err).NotTo(BeNil())
			isNotFoundErr := errors.IsNotFound(err)
			Expect(isNotFoundErr).To(BeTrue())
		})
	})

	Context("every thing is ready", func() {
		BeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset(codeQualityTool)
			cacheClient = testtools.NewFakeClient(codeQualityBinding, codeQualityTool, secret)
			dr := codequalitybinding.NewReconciler(cacheClient, devopsClient, provider).(*metrics.Decorator)
			codeQualityBindingReconciler = dr.R.(*codequalitybinding.Reconciler)

			codeQualityProjectSyncMock.EXPECT().SyncCodeQualityProjects(gomock.Any(), codeQualityTool, secret).Return(nil)
			codeQualityBindingReconciler.Sync = codeQualityProjectSyncMock
		})
		It("should finish successfully", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})
})

func GetCodeQualityTool() *devopsv1alpha1.CodeQualityTool {
	return &devopsv1alpha1.CodeQualityTool{
		ObjectMeta: metav1.ObjectMeta{
			Name: "sonar",
		},
		Spec: devopsv1alpha1.CodeQualityToolSpec{
			Type: "Sonarqube",
			ToolSpec: devopsv1alpha1.ToolSpec{
				HTTP: devopsv1alpha1.HostPort{
					Host: "http://sonarcloud.io",
				},
			},
		},
	}
}

func GetCodeQualityBinding() *devopsv1alpha1.CodeQualityBinding {
	return &devopsv1alpha1.CodeQualityBinding{
		TypeMeta: metav1.TypeMeta{
			APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			Kind:       devopsv1alpha1.TypeCodeQualityBinding,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "sonar-binding",
			Namespace: "namespace1",
		},
		Spec: devopsv1alpha1.CodeQualityBindingSpec{
			CodeQualityTool: devopsv1alpha1.LocalObjectReference{
				Name: "sonar",
			},
			Secret: devopsv1alpha1.SecretKeySetRef{
				SecretReference: corev1.SecretReference{
					Name:      "sonar-secret",
					Namespace: "namespace1",
				},
			},
		},
	}
}
