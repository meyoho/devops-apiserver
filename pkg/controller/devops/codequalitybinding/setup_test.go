package codequalitybinding_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/codequalitybinding"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
	"testing"
)

func TestCodeQualityBindingController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("codequalitybinding.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/codequalitybinding", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"codequalitybinding-controller",
		codequalitybinding.Add,
		&source.Kind{Type: &v1alpha1.CodeQualityBinding{}},
		&handler.EnqueueRequestForObject{},
		&predicate.PhaseTTLPredicate{Type: "codequalitybinding-controller", TTL: v1alpha1.TTLSession},
	),
)
