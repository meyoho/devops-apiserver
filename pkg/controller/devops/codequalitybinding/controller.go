package codequalitybinding

import (
	"context"
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/controller/metrics"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	"k8s.io/klog/klogr"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName = "codequalitybinding-controller"
)

func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	return NewReconciler(mgr.GetClient(), mgr.GetDevOpsClient(), mgr.GetAnnotationProvider())
}

func NewReconciler(cacheClient client.Client, devopsClient clientset.Interface, provider v1alpha1.AnnotationProvider) reconcile.Reconciler {
	r := &Reconciler{
		CacheClient:  cacheClient,
		DevOpsClient: clientset.NewExpansion(devopsClient),
		Sync:         NewCodeQualityProjectSync(controllerName, cacheClient, clientset.NewExpansion(devopsClient)),
	}

	r.ItemsDeleter = reconciler.NewLocalGeneratedItemsDeleter(cacheClient, r.DevOpsClient, v1alpha1.TypeCodeQualityBinding,
		v1alpha1.ResourceNameCodeQualityProject,
		func(parent types.NamespacedName) *metav1.LabelSelector {
			return &metav1.LabelSelector{
				MatchLabels: map[string]string{
					v1alpha1.LabelCodeQualityBinding: parent.Name,
				}}
		}, provider)

	return metrics.DecorateReconciler(controllerName, v1alpha1.TypeCodeQualityBinding, r).WithResourceReconcileTime()
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.CodeQualityBinding{}},
		&handler.EnqueueRequestForObject{},
		predicate.EmptyOwnerTTL(controllerName, v1alpha1.TTLSession),
	)

	return
}

type Reconciler struct {
	CacheClient  client.Client
	DevOpsClient clientset.InterfaceExpansion
	Sync         CodeQualityProjectSyncer

	ItemsDeleter reconciler.LocalGeneratedItemsDeleter
}

var _ reconcile.Reconciler = &Reconciler{}

func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	log := klogr.New().WithName(fmt.Sprintf("[%s]", controllerName)).WithValues("CodeQualityBinding", request.String())

	log.V(5).Info("Reconciling")

	binding := &v1alpha1.CodeQualityBinding{}
	err = rec.CacheClient.Get(context.TODO(), request.NamespacedName, binding)
	if err != nil {
		utilruntime.HandleError(err)
		if !errors.IsNotFound(err) {
			return
		}

		glog.Infof("[%s] CodeQualityBinding %s is not found", controllerName, request)
		rec.ItemsDeleter.DeleteItemsOf(log, request.NamespacedName)
		err = nil
		return
	}

	codeQualityToolName := binding.Spec.CodeQualityTool.Name

	codeQualityTool := &v1alpha1.CodeQualityTool{}
	err = rec.CacheClient.Get(context.TODO(), client.ObjectKey{Name: codeQualityToolName}, codeQualityTool)
	if err != nil {
		if !errors.IsNotFound(err) {
			utilruntime.HandleError(err)
			return
		}

		glog.Infof("[%s] codequalitytool '%s' is not found, delete codequalitybinding '%s/%s'", controllerName, codeQualityToolName, binding.Namespace, binding.Name)
		err = rec.CacheClient.Delete(context.TODO(), binding)
		if err != nil {
			glog.Errorf("[%s] delete codequalitybinding '%s/%s' error:%s", controllerName, binding.Namespace, binding.Name, err.Error())
		}

		err = nil
		return
	}

	bindingCopy := binding.DeepCopy()
	conditioner := generic.NewStandardConditionProcess(controllerName, binding.GetName())
	secretConditioner := generic.NewRecSecretConditioner(binding.Spec.Secret, rec.CacheClient)
	serviceConditioner := generic.NewDevOpsToolInterfaceConditioner(v1alpha1.TypeCodeQualityTool, codeQualityToolName, rec.DevOpsClient)
	authConditioner := generic.NewAuthorizationConditioner(codeQualityToolName, binding.Spec.Secret, rec.DevOpsClient.DevopsV1alpha1().CodeQualityTools().Authorize)

	conditioner.Add(serviceConditioner).Add(secretConditioner).Add(authConditioner)

	currentConditions := conditioner.Conditions()
	bindingCopy.Status.Conditions = currentConditions
	bindingCopy.Status.HTTPStatus = serviceConditioner.GetHTTPStatus()
	bindingCopy.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	bindingCopy.Status = generic.GetServicePhaseStatus(currentConditions, bindingCopy.Status)

	if bindingCopy.Status.Phase == v1alpha1.ServiceStatusPhaseReady {
		err = rec.Sync.SyncCodeQualityProjects(bindingCopy, codeQualityTool, secretConditioner.Secret)
		if err != nil {
			glog.Errorf("[%s] failed to sync up projects in sonarqube to k8s, err: %v", controllerName, err)
		}
	}

	err = nil

	err = rec.CacheClient.Update(context.TODO(), bindingCopy)
	glog.V(5).Infof("[%s] will update \"%s/%s\"", controllerName, request.Namespace, request.Name)
	return
}
