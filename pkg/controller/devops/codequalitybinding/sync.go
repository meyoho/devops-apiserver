package codequalitybinding

import (
	"context"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/controller/generic"
	corev1 "k8s.io/api/core/v1"
	glog "k8s.io/klog"
)

type CodeQualityProjectSyncer interface {
	SyncCodeQualityProjects(binding *v1alpha1.CodeQualityBinding, service *v1alpha1.CodeQualityTool, secret *corev1.Secret) (err error)
}

func NewCodeQualityProjectSync(owner string, cacheClient client.Client, devopsClient clientset.InterfaceExpansion) CodeQualityProjectSyncer {
	return &CodeQualityProjectSync{
		Owner:        owner,
		CacheClient:  cacheClient,
		DevOpsClient: devopsClient,
	}
}

type CodeQualityProjectSync struct {
	Owner        string
	CacheClient  client.Client
	DevOpsClient clientset.InterfaceExpansion
}

func (c *CodeQualityProjectSync) SyncCodeQualityProjects(binding *v1alpha1.CodeQualityBinding, service *v1alpha1.CodeQualityTool, secret *corev1.Secret) (err error) {
	glog.V(5).Infof("[%s] sync projects defined in binding to k8s", c.Owner)
	binding.Status.Phase = v1alpha1.ServiceStatusPhaseReady
	binding.Status.Message = ""
	// Get all code repos in namespace
	codeRepositoryList := &v1alpha1.CodeRepositoryList{}
	err = c.CacheClient.List(context.TODO(), &client.ListOptions{Namespace: binding.Namespace}, codeRepositoryList)
	if err != nil {
		glog.Errorf("[%s] List codeRepository error: %s", c.Owner, err)
		return
	}

	var repoNames []string
	for _, repo := range codeRepositoryList.Items {
		repoNames = append(repoNames, repo.Name)
	}
	remoteProjects, err := c.DevOpsClient.DevopsV1alpha1Expansion().CodeQualityBindings(binding.Namespace).Projects(binding.Name, &v1alpha1.CorrespondCodeQualityProjectsOptions{Repositories: strings.Join(repoNames, ",")})
	if err != nil {
		glog.Errorf("[%s] Error when get codequalityproject from remote sonarqube server %s, error: %s", c.Owner, service.GetEndpoint(), err.Error())
		return err
	}

	// Get all projects in the local
	projectList := &v1alpha1.CodeQualityProjectList{}
	err = c.CacheClient.List(context.TODO(), &client.ListOptions{
		LabelSelector: labels.SelectorFromSet(map[string]string{v1alpha1.LabelCodeQualityTool: service.GetName()}),
		Namespace:     binding.Namespace,
	}, projectList)
	if err != nil {
		glog.Errorf("[%s] Error when get codequalityproject from k8s, error: %s", c.Owner, err.Error())
		return err
	}

	localProjects := make(map[string]v1alpha1.CodeQualityProject)
	for _, project := range projectList.Items {
		if project.Spec.CodeQualityBinding.Name == binding.Name {
			localProjects[project.Name] = project
		}
	}

	now := *generic.MetaTimeNow()
	for _, remoteProject := range remoteProjects.Items {
		condition := v1alpha1.BindingCondition{
			Type:        v1alpha1.JenkinsBindingStatusTypeCodeQualityProject,
			Name:        remoteProject.GetName(),
			LastAttempt: &now,
			Message:     "",
			Reason:      "",
			Status:      v1alpha1.JenkinsBindingStatusConditionStatusReady,
		}
		binding.Status.Conditions = append(binding.Status.Conditions, condition)

		localProject, foundInLocal := localProjects[remoteProject.Name]
		delete(localProjects, remoteProject.Name)

		if foundInLocal {
			if !remoteProject.Spec.Project.LastAnalysis.Equal(localProject.Spec.Project.LastAnalysis) {
				localProject.Spec.Project.LastAnalysis = remoteProject.Spec.Project.LastAnalysis
			}
			err = c.CacheClient.Update(context.TODO(), &localProject)
			if err != nil {
				glog.Errorf("[%s] Error when update codequalityproject '%s/%s, error: %s", c.Owner, remoteProject.GetNamespace(), remoteProject.GetName(), err.Error())
			}
		} else {
			glog.V(7).Infof("[%s] creating CodeQualityProject %s", c.Owner, remoteProject.GetName())
			err = c.CacheClient.Create(context.TODO(), &remoteProject)
			if err != nil {
				glog.Errorf("[%s] error creating CodeQualityProject: %v", c.Owner, err)
			}
		}
	}

	for _, projectShouldBeDeleted := range localProjects {
		err = c.DevOpsClient.DevopsV1alpha1().CodeQualityProjects(binding.Namespace).Delete(projectShouldBeDeleted.Name, &metav1.DeleteOptions{})
		if err != nil {
			glog.Errorf("[%s] Error when delete codequalityproject '%s/%s, error: %s", c.Owner, binding.GetNamespace(), projectShouldBeDeleted.Name, err.Error())
		}
	}
	return
}
