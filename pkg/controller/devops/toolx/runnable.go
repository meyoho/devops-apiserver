package toolx

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/util"
	"fmt"
	"github.com/ghodss/yaml"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

const toolxPath = "./toolx"

func AddRunnable(manager manager.Manager) error {

	return manager.Add(newRunable(manager.GetKubeClient(), manager.GetDevOpsClient()))
}

type Runnable struct {
	kubeClient   kubernetes.Interface
	devopsClient versioned.Interface
}

var _ sigsmanager.Runnable = &Runnable{}

func (r *Runnable) Start(s <-chan struct{}) (err error) {

	logName := "Toolx-Start"
	klog.V(5).Infof("[%s] Starting runnable", logName)
	bts, _, err := (&util.DiskLoader{Path: toolxPath}).Files()
	if err != nil {
		return err
	}

	errs := util.MultiErrors{}
	settingList := make([]*v1alpha1.Setting, 0, 0)
	toolCategoryList := make([]*v1alpha1.ToolCategory, 0, 0)
	toolTypeList := make([]*v1alpha1.ToolType, 0, 0)

	klog.V(9).Infof("[%s] bts is %#v", logName, bts)

	for vpath, content := range bts {
		typeMeta := &metav1.TypeMeta{}
		err := yaml.Unmarshal(content, typeMeta)
		if err != nil {
			errs = append(errs, fmt.Errorf("Unmarshall file %s error: %s", vpath, err.Error()))
			continue
		}

		klog.V(5).Infof("[%s] typeMeta is %#v", logName, typeMeta)

		kind := typeMeta.Kind

		switch kind {
		case v1alpha1.TypeSetting:
			{
				setting := &v1alpha1.Setting{}
				err = yaml.Unmarshal(content, setting)
				if err != nil {
					errs = append(errs, fmt.Errorf("Unmarshall file %s to %s error: %s", vpath, kind, err.Error()))
					continue
				}
				klog.V(9).Infof("[%s] unmarshal setting is %#v", logName, *setting)

				settingList = append(settingList, setting)

			}
		case v1alpha1.TypeToolCategory:
			{
				category := &v1alpha1.ToolCategory{}
				err = yaml.Unmarshal(content, category)
				if err != nil {
					errs = append(errs, fmt.Errorf("Unmarshall file %s to %s error: %s", vpath, kind, err.Error()))
					continue
				}
				klog.V(9).Infof("[%s] unmarshal category is %#v", logName, *category)

				toolCategoryList = append(toolCategoryList, category)

			}
		case v1alpha1.TypeToolType:
			{
				toolType := &v1alpha1.ToolType{}
				err = yaml.Unmarshal(content, toolType)
				if err != nil {
					errs = append(errs, fmt.Errorf("Unmarshall file %s to %s error: %s", vpath, kind, err.Error()))
					continue
				}
				klog.V(9).Infof("[%s] unmarshal toolType is %#v", logName, *toolType)

				toolTypeList = append(toolTypeList, toolType)

			}
		default:
			errs = append(errs, fmt.Errorf("Unknown kind %s of file %s", kind, vpath))
		}
	}

	for _, e := range errs {
		klog.Errorf("%s unmarshal err is %#v", logName, e)
	}

	klog.V(9).Infof("[%s] settingList is %#v", logName, settingList)
	klog.V(9).Infof("[%s] toolCategoryList is %#v", logName, toolCategoryList)
	klog.V(9).Infof("[%s] toolTypeList is %#v", logName, toolTypeList)

	for _, setting := range settingList {
		klog.V(9).Infof("[%s] setting is %#v", logName, *setting)
		_, err := r.devopsClient.DevopsV1alpha1().Settings().Create(setting)
		if err != nil {
			if errors.IsAlreadyExists(err) {
				klog.V(3).Infof("%s named %s is already exist", logName, setting.Name)
			} else {
				klog.Errorf("%s create setting named %s err is %#v", logName, setting.Name, err)
			}
		}
	}

	for _, tc := range toolCategoryList {
		klog.V(9).Infof("[%s] toolcategory is %#v", logName, *tc)
		_, err := r.devopsClient.DevopsV1alpha1().ToolCategories().Create(tc)
		if err != nil {
			if errors.IsAlreadyExists(err) {
				klog.V(3).Infof("%s named %s is already exist", logName, tc.Name)
				oriTc, err := r.devopsClient.DevopsV1alpha1().ToolCategories().Get(tc.Name, v1alpha1.GetOptions())
				if err != nil {
					klog.Errorf("%s create toolcategory named %s err is %#v", logName, tc.Name, err)
					continue
				}
				newTc, err := oriTc.Merge(tc)
				if err != nil {
					klog.Errorf("%s create toolcategory named %s err is %#v", logName, tc.Name, err)
					continue
				}
				_, err = r.devopsClient.DevopsV1alpha1().ToolCategories().Update(newTc)
				if err != nil {
					klog.Errorf("%s create toolcategory named %s err is %#v", logName, tc.Name, err)
					continue
				}
			} else {
				klog.Errorf("%s create toolcategory named %s err is %#v", logName, tc.Name, err)
				continue
			}
		}
	}

	for _, tt := range toolTypeList {
		klog.V(9).Infof("[%s] tooltype is %#v", logName, *tt)
		_, err := r.devopsClient.DevopsV1alpha1().ToolTypes().Create(tt)
		if err != nil {
			if errors.IsAlreadyExists(err) {
				klog.V(3).Infof("%s named %s is already exist", logName, tt.Name)
				oriTt, err := r.devopsClient.DevopsV1alpha1().ToolTypes().Get(tt.Name, v1alpha1.GetOptions())
				if err != nil {
					klog.Errorf("%s create tooltype named %s err is %#v", logName, tt.Name, err)
					continue
				}
				klog.V(9).Infof("%s oriTt is %#v", logName, oriTt)

				newTt, err := oriTt.Merge(tt)
				if err != nil {
					klog.Errorf("%s create tooltype named %s err is %#v", logName, tt.Name, err)
					continue
				}
				klog.V(9).Infof("%s newTt is %#v", logName, newTt)

				_, err = r.devopsClient.DevopsV1alpha1().ToolTypes().Update(newTt)
				if err != nil {
					klog.Errorf("%s create tooltype named %s err is %#v", logName, tt.Name, err)
					continue
				}
				klog.V(9).Infof("%s newTt named %s update success.", logName, newTt.Name)

			} else {
				klog.Errorf("%s create tooltype named %s err is %#v", logName, tt.Name, err)
				continue
			}
		}
	}
	r.deleteUselessToolType()
	<-s
	return err
}

func newRunable(k kubernetes.Interface, d versioned.Interface) *Runnable {
	return &Runnable{kubeClient: k, devopsClient: d}
}

func (r *Runnable) deleteUselessToolType() {
	logName := "Toolx-Start"
	UselessToolType := []string{
		"Maven2", "confluence", "alauda-registry", "redwoodhq",
	}
	for _, name := range UselessToolType {
		err := r.devopsClient.DevopsV1alpha1().ToolTypes().Delete(name, &metav1.DeleteOptions{})
		if err != nil {
			klog.Errorf("%s delete tooltype named %s err is %#v", logName, name, err)
		} else {
			klog.V(9).Infof("%s delete tooltype %s success", logName, name)
		}
	}

}
