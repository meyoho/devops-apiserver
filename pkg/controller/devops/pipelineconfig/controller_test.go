package pipelineconfig_test

import (
	"log"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/devops/pipelineconfig"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		ctrl         *gomock.Controller
		cacheClient  client.Client
		devopsClient *clientset.Clientset
		// roundTripper     *mhttp.MockRoundTripper
		request reconcile.Request

		reconciler reconcile.Reconciler
		err        error
		result     reconcile.Result
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		cacheClient = testtools.NewFakeClient()
		devopsClient = clientset.NewSimpleClientset()
		// roundTripper = mhttp.NewMockRoundTripper(ctrl)
	})

	JustBeforeEach(func() {
		reconciler = pipelineconfig.NewReconciler(cacheClient, devopsClient, v1alpha1.NewAnnotationProvider(v1alpha1.UsedBaseDomain))
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	Context("pipelineconfig is not found", func() {
		BeforeEach(func() {
			request = reconcile.Request{
				NamespacedName: types.NamespacedName{
					Name:      "pipelineconfig",
					Namespace: "default",
				},
			}
		})
		It("should not return error and should have get a pipelineconfig", func() {
			result, err = reconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("pipelineconfig of template is found and should reconcile", func() {
		BeforeEach(func() {
			request = reconcile.Request{
				NamespacedName: types.NamespacedName{
					Name:      "pipelineconfig",
					Namespace: "default",
				},
			}

			pipelineConfig := testtools.GetPipelineConfig(request.Name, request.Namespace, "jenkins")
			pipelineConfig.Spec.Template = v1alpha1.PipelineTemplateWithValue{
				PipelineTemplateSource: v1alpha1.PipelineTemplateSource{
					PipelineTemplateRef: v1alpha1.PipelineTemplateRef{
						Kind: v1alpha1.TypeClusterPipelineTemplate,
						Name: "A",
					},
				},
				Values: map[string]string{},
			}

			template := &v1alpha1.ClusterPipelineTemplate{
				TypeMeta:   metav1.TypeMeta{Kind: v1alpha1.TypeClusterPipelineTemplate, APIVersion: v1alpha1.APIVersionV1Alpha1},
				ObjectMeta: metav1.ObjectMeta{Name: "A"},
			}

			devopsClient = clientset.NewSimpleClientset(pipelineConfig, template)
			cacheClient = testtools.NewFakeClient(pipelineConfig, template)
		})
		// override GennerateJenkinsfile function
		JustBeforeEach(func() {
			if rec, ok := reconciler.(*pipelineconfig.Reconciler); ok {
				log.Println("will override function")
				rec.GenerateJenkinsfileFunc = func(config *v1alpha1.PipelineConfig) error {
					config.Spec.Strategy.Jenkins.Jenkinsfile = "this is the jenkinsfile"
					return nil
				}
			}
		})
		It("should not return error and should have updated a pipelineconfig with jenkinsfile", func() {
			result, err = reconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			pipelineConfig, err := devopsClient.DevopsV1alpha1().PipelineConfigs(request.Namespace).Get(request.Name, metav1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(pipelineConfig.Status.Conditions[0].Status).To(Equal(v1alpha1.ConditionStatusTrue))
			Expect(pipelineConfig.Status.Conditions[0].LastAttempt).ToNot(BeNil())
			Expect(pipelineConfig.Spec.Strategy.Jenkins.Jenkinsfile).To(Equal("this is the jenkinsfile"))

		})
	})

	Context("pipelineconfig of script created, should reconcile", func() {

		var (
			pipelineConfig = &v1alpha1.PipelineConfig{}
		)
		BeforeEach(func() {
			request = reconcile.Request{
				NamespacedName: types.NamespacedName{
					Name:      "pipelineconfig",
					Namespace: "default",
				},
			}

			pipelineConfig = testtools.GetPipelineConfig(request.Name, request.Namespace, "jenkins")
			pipelineConfig.Spec.Strategy.Jenkins.Jenkinsfile = "this is scripted pipeline"

			// force trigger a render
			pipelineConfig.Status.Phase = v1alpha1.PipelineConfigPhaseCreating
			devopsClient = clientset.NewSimpleClientset(pipelineConfig)
			cacheClient = testtools.NewFakeClient(pipelineConfig)
		})
		JustBeforeEach(func() {
			if rec, ok := reconciler.(*pipelineconfig.Reconciler); ok {
				rec.GenerateJenkinsfileFunc = func(config *v1alpha1.PipelineConfig) error {
					return nil
				}
			}
		})

		It("should not return error and set condition to true", func() {
			result, err = reconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			pipelineConfig, err := devopsClient.DevopsV1alpha1().PipelineConfigs(request.Namespace).Get(request.Name, metav1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(pipelineConfig.Status.Conditions[0].Status).To(Equal(v1alpha1.ConditionStatusTrue))
			Expect(pipelineConfig.Status.Conditions[0].LastAttempt).ToNot(BeNil())
			Expect(pipelineConfig.Spec.Strategy.Jenkins.Jenkinsfile).To(Equal("this is scripted pipeline"))
		})
	})

	Context("pipelineconfig of script created, should reconcile", func() {

		var (
			pipelineConfig = &v1alpha1.PipelineConfig{}
		)
		BeforeEach(func() {
			request = reconcile.Request{
				NamespacedName: types.NamespacedName{
					Name:      "pipelineconfig",
					Namespace: "default",
				},
			}

			pipelineConfig = testtools.GetPipelineConfig(request.Name, request.Namespace, "jenkins")
			pipelineConfig.Spec.Strategy.Jenkins.JenkinsfilePath = "jenkinsfile1"

			// force trigger a render
			pipelineConfig.Status.Phase = v1alpha1.PipelineConfigPhaseCreating
			devopsClient = clientset.NewSimpleClientset(pipelineConfig)
			cacheClient = testtools.NewFakeClient(pipelineConfig)
		})
		JustBeforeEach(func() {

			if rec, ok := reconciler.(*pipelineconfig.Reconciler); ok {
				rec.GenerateJenkinsfileFunc = func(config *v1alpha1.PipelineConfig) error {
					return nil
				}
			}
		})

		It("should not return error and set condition to true", func() {
			result, err = reconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			pipelineConfig, err := devopsClient.DevopsV1alpha1().PipelineConfigs(request.Namespace).Get(request.Name, metav1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(pipelineConfig.Status.Conditions[0].Status).To(Equal(v1alpha1.ConditionStatusTrue))
			Expect(pipelineConfig.Status.Conditions[0].LastAttempt).ToNot(BeNil())
			Expect(pipelineConfig.Spec.Strategy.Jenkins.Jenkinsfile).To(Equal(""))
			Expect(pipelineConfig.Spec.Strategy.Jenkins.JenkinsfilePath).To(Equal("jenkinsfile1"))
		})
	})

	Context("pipelineconfig of mulit branch created, should reconcile", func() {
		var (
			pipelineConfig = &v1alpha1.PipelineConfig{}
		)
		BeforeEach(func() {
			request = reconcile.Request{
				NamespacedName: types.NamespacedName{
					Name:      "pipelineconfig",
					Namespace: "default",
				},
			}

			pipelineConfig = testtools.GetPipelineConfig(request.Name, request.Namespace, "jenkins")
			pipelineConfig.Spec.Strategy.Jenkins.JenkinsfilePath = "multi-branch-jenkinsfile"
			pipelineConfig.Spec.Strategy.Jenkins.MultiBranch = v1alpha1.MultiBranchPipeline{
				Behaviours: v1alpha1.MultiBranchBehaviours{
					DiscoverBranches: "master",
				},
			}

			devopsClient = clientset.NewSimpleClientset(pipelineConfig)
			cacheClient = testtools.NewFakeClient(pipelineConfig)
		})
		JustBeforeEach(func() {

			if rec, ok := reconciler.(*pipelineconfig.Reconciler); ok {
				rec.GenerateJenkinsfileFunc = func(config *v1alpha1.PipelineConfig) error {
					return nil
				}
			}
		})

		It("should not return error and set condition to true", func() {
			result, err = reconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			pipelineConfig, err := devopsClient.DevopsV1alpha1().PipelineConfigs(request.Namespace).Get(request.Name, metav1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(pipelineConfig.Status.Conditions[0].Status).To(Equal(v1alpha1.ConditionStatusTrue))
			Expect(pipelineConfig.Status.Conditions[0].LastAttempt).ToNot(BeNil())
			Expect(pipelineConfig.Spec.Strategy.Jenkins.Jenkinsfile).To(Equal(""))
			Expect(pipelineConfig.Spec.Strategy.Jenkins.JenkinsfilePath).To(Equal("multi-branch-jenkinsfile"))
		})
	})

})
