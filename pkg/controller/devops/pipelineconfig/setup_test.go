package pipelineconfig_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/pipelineconfig"
	devopspredicate "alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/testtools"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestPipelineConfigController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("pipelineconfig.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/pipelineconfig", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"pipelineconfig-controller",
		pipelineconfig.Add,
		&source.Kind{Type: &v1alpha1.PipelineConfig{}},
		&handler.EnqueueRequestForObject{},
		devopspredicate.PhasePredicate{Type: "pipelineconfig-controller", Phase: string(v1alpha1.PipelineConfigPhaseCreating)},
	),
)
