package pipelineconfig

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/klog/klogr"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

const (
	runnableName = "pipelineconfig-runnable"
)

// Runnable create defualut notificationtemplate
type Runnable struct {
	cacheClient client.Client
	Provider    v1alpha1.AnnotationProvider
	config      manager.ExtraConfig
}

func NewRunnable(cacheClient client.Client, extra manager.ExtraConfig, provider v1alpha1.AnnotationProvider) sigsmanager.Runnable {
	return &Runnable{
		cacheClient: cacheClient,
		Provider:    provider,
		config:      extra,
	}
}

func AddRunnable(mgr manager.Manager) error {
	return mgr.Add(NewRunnable(mgr.GetClient(), mgr.GetExtraConfig(), mgr.GetAnnotationProvider()))
}

var _ sigsmanager.Runnable = &Runnable{}

// Start adds a start method to be a runnable
func (run *Runnable) Start(stop <-chan struct{}) (err error) {
	log := klogr.New().WithName(runnableName)
	log.Info("Starting runnable")
	cm := run.defaultViewTemplate()
	_, err = controllerutil.CreateOrUpdate(context.TODO(), run.cacheClient, cm, func(existing runtime.Object) error {
		cm := existing.(*corev1.ConfigMap)
		cm.Data = run.defaultViewTemplate().Data
		return nil
	})
	if err != nil {
		log.WithValues(runnableName).Error(err, "Error during CreateOrUpdate default ViewPipelineTemplate")
	}

	<-stop
	return
}

func (run *Runnable) defaultViewTemplate() *corev1.ConfigMap {
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      k8s.ConfigMapNameDevOpsDefaultViewTemplate,
			Namespace: run.config.SystemNamespace,
		},
		Data: map[string]string{"markdown": `{{- $noHead := true }}
{{- $items := .status.information.items }}
{{- range $_, $value := $items }}
	{{- if ne (printf "%.1s" $value.type) "_" }}
		{{- if eq $value.type "title" }}

## {{$value.value}}

			{{- $noHead = true }}
		{{- else}}
			{{- if $noHead }}
				{{- $noHead = false }}
| Name | Value |
| :--- | :---- |
			{{- end }}
| {{$value.name}} | {{ if eq $value.type "url"}}[{{$value.value}}]({{$value.value}}){{else}}{{$value.value}}{{end}} |
		{{- end }}
	{{- end }}
{{- end }}`},
	}
}
