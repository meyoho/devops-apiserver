package pipelineconfig

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"github.com/appscode/jsonpatch"

	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
	"k8s.io/klog/klogr"
	"sigs.k8s.io/controller-runtime/pkg/handler"

	"hash/fnv"

	jenkinsfilext "bitbucket.org/mathildetech/jenkinsfilext/v2/domain"
	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/rand"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	hashutil "k8s.io/kubernetes/pkg/util/hash"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName = "pipelineconfig-controller"
)

// Add injects the manager so to inject the controller in the manager
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

// NewReconcilerByManager returns a new reconcile.Reconciler
func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	rec := NewReconciler(mgr.GetClient(), mgr.GetDevOpsClient(), mgr.GetAnnotationProvider())
	// set its own function
	return rec
}

// NewReconciler returns a new reconcile.Reconciler
func NewReconciler(cacheClient client.Client, devopsClient clientset.Interface, provider v1alpha1.AnnotationProvider) reconcile.Reconciler {
	rec := &Reconciler{
		CacheClient:        cacheClient,
		DevopsClient:       clientset.NewExpansion(devopsClient),
		AnnotationProvider: provider,
	}
	rec.GenerateJenkinsfileFunc = rec.renderJenkinsfile
	return rec
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}
	// we sync some oauth2 secrets
	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.PipelineConfig{}}, &handler.EnqueueRequestForObject{},
		predicate.Funcs{
			CreateFunc: func(event event.CreateEvent) bool {
				pipelineConfig := event.Object.(*v1alpha1.PipelineConfig)
				return needReconcile(pipelineConfig)
			},
			UpdateFunc: func(updateEvent event.UpdateEvent) bool {
				newPipelineConfig := updateEvent.ObjectNew.(*v1alpha1.PipelineConfig)
				return needReconcile(newPipelineConfig)
			},
			DeleteFunc: func(deleteEvent event.DeleteEvent) bool {
				return false
			},
			GenericFunc: func(genericEvent event.GenericEvent) bool {
				return false
			},
		},
	)
	return
}

// Reconciler this reconcile primary objective is to
// refresh access tokens for secrets
type Reconciler struct {
	CacheClient             client.Client
	DevopsClient            clientset.InterfaceExpansion
	GenerateJenkinsfileFunc func(config *v1alpha1.PipelineConfig) error
	AnnotationProvider      v1alpha1.AnnotationProvider
	BaseDomain              string
}

var _ reconcile.Reconciler = &Reconciler{}

// Reconcile our reconcile function will sync our secrets
func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	log := klogr.New().WithName(controllerName).WithValues("PipelineConfig", fmt.Sprintf("%s", request))
	log.V(5).Info("Reconciling begin...")

	defer func() {
		if err != nil {
			log.Error(err, "Reconciling finished", "result", result)
		} else {
			log.V(5).Info("Reconciling finished")
		}
		err = nil // wait for next loop
		result = reconcile.Result{}
	}()

	config := &v1alpha1.PipelineConfig{}
	err = rec.CacheClient.Get(context.TODO(), request.NamespacedName, config)
	if err != nil {
		return
	}

	configCopy := config.DeepCopy()

	now := metav1.NewTime(time.Now())
	initializedCond := v1alpha1.Condition{
		Type:        v1alpha1.PipelineConfigConditionTypeInitialized,
		Status:      v1alpha1.ConditionStatusTrue,
		LastAttempt: &now,
	}

	err, reason := rec.initializePipelineConfig(configCopy)
	if err != nil {
		initializedCond.Status = v1alpha1.ConditionStatusFalse
		initializedCond.Reason = reason
		initializedCond.Message = err.Error()
	}

	setCondition(initializedCond, configCopy.Status.Conditions)

	err = rec.updatePipelineConfig(log, config, configCopy)
	return
}

func (rec *Reconciler) initializePipelineConfig(pipelineConfig *v1alpha1.PipelineConfig) (err error, reason string) {
	log := klogr.New().WithName(controllerName).WithValues("PipelineConfig", fmt.Sprintf("%s/%s", pipelineConfig.Namespace, pipelineConfig.Name))

	// [DEVOPS-3168] sync secret
	log.V(7).Info("Starting to sync code repository secret")
	err = rec.syncCodeRepositorySecret(pipelineConfig)
	if err != nil {
		log.Error(err, "Failed to sync PipelineConfig's code repository secret")
		return err, v1alpha1.PipelineConfigConditionReasonCodeRepositorySecretSyncFailed
	}

	log.V(7).Info("Starting to sync templates")
	err = rec.syncTemplate(pipelineConfig)
	if err != nil {
		log.Error(err, "Failed to Sync PipelineConfig's templates")
		return err, v1alpha1.PipelineConfigConditionReasonSyncPipelineTemplateFailed
	}

	log.V(7).Info("Starting to generate Jenkinsfile")
	err = rec.GenerateJenkinsfileFunc(pipelineConfig)
	if err != nil {
		log.Error(err, "Failed to generate Jenkinsfile for PipelineConfig")
		return err, v1alpha1.PipelineConfigConditionReasonRenderJenkinsfileFailed
	}

	return nil, ""
}

func (rec *Reconciler) updatePipelineConfig(logger logr.Logger, old *v1alpha1.PipelineConfig, new *v1alpha1.PipelineConfig) error {
	oldBts, err := json.Marshal(old)
	if err != nil {
		logger.Error(err, "marshall pipelineconfig error", "oldPipelineConfig", old)
		return err
	}

	newBts, err := json.Marshal(new)
	if err != nil {
		logger.Error(err, "marshall pipelineconfig error", "newPipelineConfig", old)
		return err
	}

	patchOperations, err := jsonpatch.CreatePatch(oldBts, newBts)
	if err != nil {
		logger.Error(err, "create patch error")
		return err
	}
	if len(patchOperations) == 0 {
		return nil
	}

	patch, err := json.Marshal(patchOperations)
	if err != nil {
		logger.Error(err, "marshall patchOperations error", "patchOperations", old)
		return err
	}

	logger.V(9).Info("patch object", string(patch))

	_, err = rec.DevopsClient.DevopsV1alpha1().PipelineConfigs(new.Namespace).Patch(new.Name, types.JSONPatchType, patch)
	if err != nil {
		return err
	}
	return nil
}

func needRenderJenkinsfile(config *v1alpha1.PipelineConfig) bool {
	byRef := config.Spec.Template.PipelineTemplateRef.Name != ""
	byMold := config.Spec.Template.PipelineTemplate != nil

	return byRef || byMold
}

func (rec *Reconciler) renderJenkinsfile(config *v1alpha1.PipelineConfig) error {
	log := klogr.New().WithName(controllerName).WithValues("PipelineConfig", fmt.Sprintf("%s/%s", config.Namespace, config.Name))
	if !needRenderJenkinsfile(config) {
		return nil
	}

	// render jenkinsfile
	log.V(7).Info("PipelineConfig has template. Will render Jenkinsfile...")
	var preview *v1alpha1.JenkinsfilePreview
	// we doest not need to pass values into preview options, because we just want use values in current pipeline config
	preview, err := rec.DevopsClient.DevopsV1alpha1().PipelineConfigs(config.Namespace).Preview(config.Name, &v1alpha1.JenkinsfilePreviewOptions{})
	if err != nil {
		log.Error(err, "Error to Preview Jenkinsfile")
		return err
	}

	config.Spec.Strategy.Jenkins.Jenkinsfile = preview.Jenkinsfile
	return nil
}

func (rec *Reconciler) setErrorStatus(config *v1alpha1.PipelineConfig, msg string, err error) error {
	log := klogr.New().WithName(controllerName).WithValues("PipelineConfig", fmt.Sprintf("%s/%s", config.Namespace, config.Name))

	pipelineConfigCopy := config.DeepCopy()
	pipelineConfigCopy.Status.Phase = v1alpha1.PipelineConfigPhaseError
	pipelineConfigCopy.Status.Message = msg
	pipelineConfigCopy.Status.Reason = err.Error()

	updateErr := rec.updatePipelineConfig(log, config, pipelineConfigCopy)

	if updateErr != nil {
		log.Error(updateErr, "Update status to error got errors", "error", err.Error())
		return err
	}

	log.Info("Updated pipelineconfig status to error success", "msg", msg, "error", err.Error())
	return nil
}

func (rec *Reconciler) syncTemplate(config *v1alpha1.PipelineConfig) error {
	log := klogr.New().WithName(controllerName).WithValues("PipelineConfig", fmt.Sprintf("%s/%s", config.Namespace, config.Name))

	templateSource := config.Spec.Template.PipelineTemplateSource
	if templateSource.PipelineTemplateRef.Name == "" && templateSource.PipelineTemplate == nil {
		log.V(7).Info("pipelineconfig is not created by template or graph")
		return nil
	}

	// created by template
	if templateSource.PipelineTemplateRef.Name != "" {
		log.V(7).Info("pipelineconfig is created by pipelinetemplate ref")
		return rec.syncPipelineTemplateRef(config)
	}

	// created by graph
	if templateSource.PipelineTemplate != nil {
		log.V(7).Info("pipelineconfig is created by pipelinetemplatemold")
		return rec.syncPipelineTemplateMold(config)
	}

	return nil
}

func (rec *Reconciler) syncPipelineTemplateRef(config *v1alpha1.PipelineConfig) error {
	templateRef := config.Spec.Template.PipelineTemplateRef
	_, err := rec.DevopsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Get(
		templateRef.Kind, templateRef.Namespace, templateRef.Name, metav1.GetOptions{ResourceVersion: "0"})

	return err
}

func (rec *Reconciler) syncPipelineTemplateMold(config *v1alpha1.PipelineConfig) error {
	log := klogr.New().WithName(controllerName).WithValues("PipelineConfig", fmt.Sprintf("%s/%s", config.Namespace, config.Name))

	template, err := rec.getTemplate(*config)
	if err != nil {
		log.Error(err, "get template created by pipelieconfig error")
		return err
	}

	if template == nil {
		log.V(3).Info("Creating pipelinetemplate by pipelineconfig, not found template created by pipelineconfig")
		template, err := rec.buildTemplate(*config)
		if err != nil {
			return err
		}
		if config, err = rec.updateToAppendTemplateLabels(config, template); err != nil {
			return err
		}
		_, err = rec.DevopsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Create(template)
		if err != nil {
			log.Error(err, fmt.Sprintf("Created %s error", template))
			return err
		}
		log.Info(fmt.Sprintf("Created %s", template))
		return nil
	}

	// template is not nil, we should compare templatemold,
	// if it has changed, we should update it , or not changed, we need to do nothing.

	oldHash := template.Annotations[v1alpha1.AnnotationsTemplateMoldHash]
	newHash := computeHash(*config.Spec.Template.PipelineTemplate)
	if newHash == oldHash {
		log.V(5).Info("spec.template.pipelinetemplate has not change")
		return nil
	}

	log.V(3).Info(fmt.Sprintf("Updating PipelineTemplate %s that created by PipelineConfig, spec.template.pipelineTemplate has changed", template))
	copy := template.DeepCopy()
	copy.Spec, err = rec.buildTemplateSpec(*config)
	if err != nil {
		return err
	}
	_, err = rec.DevopsClient.DevopsV1alpha1Expansion().PipelineTemplates(copy.Namespace).Update(copy)
	if err != nil {
		log.Error(err, fmt.Sprintf("Update PipelineTemplate %s of pipelineconfig error", copy))
		return err
	}
	log.Info(fmt.Sprintf("Updated PipelineTemplate %s of pipelineconfig", copy))
	return nil
}

// appendTemplateLabels
func (rec *Reconciler) updateToAppendTemplateLabels(config *v1alpha1.PipelineConfig, template v1alpha1.PipelineTemplateInterface) (*v1alpha1.PipelineConfig, error) {
	log := klogr.New().WithName(controllerName).WithValues("PipelineConfig", fmt.Sprintf("%s/%s", config.Namespace, config.Name))

	if config.Labels == nil {
		config.Labels = map[string]string{}
	}
	config.ObjectMeta.Labels[v1alpha1.LabelTemplateKind] = template.GetKind()
	config.ObjectMeta.Labels[v1alpha1.LabelTemplateName] = template.GetName()
	config.ObjectMeta.Labels[v1alpha1.LabelTemplateVersion] = template.GetAnnotations()[v1alpha1.AnnotationsTemplateVersion]

	copy := config.DeepCopy()
	updated, err := rec.DevopsClient.DevopsV1alpha1().PipelineConfigs(config.Namespace).Update(copy)
	if err != nil {
		log.Error(err, "Updated pipelineconfig error , append template labels")
		return nil, err
	}
	return updated, nil
}

// buildTemplate will build a pipelinetemplate according config.spec.template.pipelineTemplate
func (rec *Reconciler) buildTemplate(config v1alpha1.PipelineConfig) (v1alpha1.PipelineTemplateInterface, error) {
	log := klogr.New().WithName(controllerName).WithValues("PipelineConfig", fmt.Sprintf("%s/%s", config.Namespace, config.Name))

	templateMold := config.Spec.Template.PipelineTemplateSource.PipelineTemplate
	if templateMold == nil {
		return nil, nil
	}

	// template hash and name has been generated in admission plugin of pipelineconfig
	hash := computeHash(*config.Spec.Template.PipelineTemplate)
	name := fmt.Sprintf("pipelineconfig-%s-template-%s", config.Name, hash)
	trueB := true
	template := v1alpha1.PipelineTemplate{
		TypeMeta: metav1.TypeMeta{
			Kind:       v1alpha1.TypePipelineTemplate,
			APIVersion: v1alpha1.APIVersionV1Alpha1,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: config.Namespace,
			Labels: map[string]string{
				v1alpha1.LabelTemplateSource:   v1alpha1.LabelTemplateSourceCustomer,
				v1alpha1.LabelTemplateCategory: v1alpha1.LabelConfigTemplateCategory,
				v1alpha1.LabelTemplateName:     name,
			},
			Annotations: map[string]string{
				v1alpha1.AnnotationsTemplateVersion:    v1alpha1.PipelineConfigTemplateDefaultVersion,
				v1alpha1.AnnotationsStyleIcon:          v1alpha1.PipelineConfigTemplateDefaultVersion,
				v1alpha1.AnnotationsKeyDisplayNameZhCN: fmt.Sprintf("Template of PipelineConfig %s", config.Name),
				v1alpha1.AnnotationsKeyDisplayNameEn:   fmt.Sprintf("Template of PipelineConfig %s", config.Name),
				v1alpha1.AnnotationsTemplateMoldHash:   hash,
			},
			Finalizers: []string{
				v1alpha1.FinalizerPipelineConfigReferenced,
			},
			OwnerReferences: []metav1.OwnerReference{
				metav1.OwnerReference{
					APIVersion: v1alpha1.APIVersionV1Alpha1,
					Kind:       v1alpha1.TypePipelineConfig,
					UID:        config.GetUID(),
					Controller: &trueB,
					Name:       config.Name,
				},
			},
		},
	}

	if templateMold.Name != "" {
		template.Name = templateMold.Name
	}
	if templateMold.Annotations != nil {
		for key, val := range templateMold.Annotations {
			template.Annotations[key] = val
		}
	}
	if templateMold.Labels != nil {
		for key, val := range templateMold.Labels {
			template.Labels[key] = val
		}
	}

	templateSpec, err := rec.buildTemplateSpec(config)
	if err != nil {
		log.Error(err, "build template error")
		return nil, err
	}

	template.Spec = templateSpec
	return &template, nil
}

// buildTemplateSpec will build a pipeline templatespec if config is created by graph.
// before invoke it , you must ensure config is created by graph
func (rec *Reconciler) buildTemplateSpec(config v1alpha1.PipelineConfig) (v1alpha1.PipelineTemplateSpec, error) {
	log := klogr.New().WithName(controllerName).WithValues("PipelineConfig", fmt.Sprintf("%s/%s", config.Namespace, config.Name))

	// convert jenkinsfilext pipeline templates
	templateSpec := config.Spec.Template.PipelineTemplate.Spec
	jxtPipeineTemplateSpec := jenkinsfilext.PipelineTemplateSpec{}
	err := convertByJSON(templateSpec, &jxtPipeineTemplateSpec)
	if err != nil {
		return v1alpha1.PipelineTemplateSpec{}, err
	}

	// convert jenkinsfilext task templates
	var jxtTaskTemplateRefs = map[string]jenkinsfilext.PipelineTaskTemplate{}
	for _, task := range allTasks(templateSpec) {
		taskTemplate, err := rec.DevopsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Get(task.Kind, config.Namespace, task.Name, metav1.GetOptions{ResourceVersion: "0"})
		if err != nil {
			log.Info(fmt.Sprintf("Cannot find %s/%s, err:%s", task.Kind, task.Name, err.Error()))
			return v1alpha1.PipelineTemplateSpec{}, err
		}

		jxtTaskTemplate := jenkinsfilext.PipelineTaskTemplate{}
		err = convertByJSON(taskTemplate, &jxtTaskTemplate)
		if err != nil {
			return v1alpha1.PipelineTemplateSpec{}, err
		}

		jxtTaskTemplateRefs[taskTemplate.GetKind()+"/"+taskTemplate.GetName()] = jxtTaskTemplate
	}
	jxtBuildTemplate, err := jenkinsfilext.BuildTemplate(jxtPipeineTemplateSpec, jxtTaskTemplateRefs)
	if err != nil {
		log.Error(err, "Build Template error", "PipelineTemplateSpec", jxtPipeineTemplateSpec, "TaskTemplateRefs", jxtTaskTemplateRefs)
		return templateSpec, err
	}
	err = convertByJSON(jxtBuildTemplate, &templateSpec)

	return templateSpec, err
}

func allTasks(spec v1alpha1.PipelineTemplateSpec) []v1alpha1.PipelineTemplateTask {
	var all = make([]v1alpha1.PipelineTemplateTask, 0, 5)

	var index = map[string]struct{}{}
	for _, stage := range spec.Stages {
		for _, task := range stage.Tasks {
			key := task.Kind + "/" + task.Name
			if _, ok := index[key]; ok {
				continue
			}

			index[key] = struct{}{}
			all = append(all, task)
		}
	}

	for _, tasks := range spec.Post {
		for _, task := range tasks {
			key := task.Kind + "/" + task.Name
			if _, ok := index[key]; ok {
				continue
			}

			index[key] = struct{}{}
			all = append(all, task)
		}
	}

	return all
}

func convertByJSON(origin interface{}, result interface{}) error {
	jsonBytes, err := json.Marshal(origin)
	if err != nil {
		glog.Errorf("Marshall origin error:%s, origin:%#v", err, origin)
		return err
	}

	err = json.Unmarshal(jsonBytes, &result)
	if err != nil {
		glog.Errorf("Unmarshll to result error:%s, json is:%s", err.Error(), string(jsonBytes))
		return err
	}
	return nil
}

func computeHash(templateMold v1alpha1.PipelineTemplateMold) string {
	hasher := fnv.New32a()
	hashutil.DeepHashObject(hasher, templateMold)
	//TODO: need to avoid hash collision
	return rand.SafeEncodeString(fmt.Sprint(hasher.Sum32()))
}

// getTemplate will return template that created by graph pipelineconfig
func (rec *Reconciler) getTemplate(config v1alpha1.PipelineConfig) (template *v1alpha1.PipelineTemplate, err error) {
	log := klogr.New().WithName(controllerName).WithValues("PipelineConfig", fmt.Sprintf("%s/%s", config.Namespace, config.Name))

	templateName, ok := config.ObjectMeta.Labels[v1alpha1.LabelTemplateName]
	if !ok {
		return nil, nil
	}

	template, err = rec.DevopsClient.DevopsV1alpha1().PipelineTemplates(config.Namespace).Get(templateName, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		if errors.IsNotFound(err) {
			log.V(5).Info(fmt.Sprintf("pipelinetemplate %s is not exists, err:%s", templateName, err.Error()))
			return nil, nil
		}

		log.Error(err, "get pipelinetemplate error", "pipelinetemplate", templateName)
		return nil, err
	}

	return template, nil
}

func (rec *Reconciler) syncCodeRepositorySecret(config *v1alpha1.PipelineConfig) error {
	if config.Spec.Source.CodeRepository == nil {
		return nil
	}
	codeRepo := &v1alpha1.CodeRepository{}
	err := rec.CacheClient.Get(context.TODO(), client.ObjectKey{Namespace: config.GetNamespace(), Name: config.Spec.Source.CodeRepository.Name}, codeRepo)
	if err != nil {
		return err
	}
	binding := &v1alpha1.CodeRepoBinding{}
	err = rec.CacheClient.Get(context.TODO(), client.ObjectKey{Namespace: config.GetNamespace(), Name: codeRepo.Spec.CodeRepoBinding.Name}, binding)
	if err != nil {
		return err
	}
	if !reflect.DeepEqual(config.Spec.Source.Secret, &binding.Spec.Account.Secret) {
		config.Spec.Source.Secret = &binding.Spec.Account.Secret
	}
	return nil
}

func needReconcile(pipelineConfig *v1alpha1.PipelineConfig) bool {
	initializedCond := getCondition(v1alpha1.PipelineConfigConditionTypeInitialized, pipelineConfig.Status.Conditions)
	// we won't reconcile PipelineConfig if its conditions is nil.
	if initializedCond == nil {
		return false
	}

	if initializedCond.Status == v1alpha1.ConditionStatusUnknown {
		return true
	}

	// Initializing maybe failed due to some external problems(devops-apiserver is restarting, network problem).
	// So, we need initialize PipelineConfig again after 5 minutes without the need to change its spec manually.
	if initializedCond.Status == v1alpha1.ConditionStatusFalse {
		lastReconcileTime := initializedCond.LastAttempt
		now := time.Now()
		return lastReconcileTime != nil && lastReconcileTime.Add(time.Minute*5).After(now)
	}

	return false
}

func getCondition(condType string, conditions []v1alpha1.Condition) *v1alpha1.Condition {
	for _, cond := range conditions {
		if cond.Type == condType {
			return &cond
		}
	}
	return nil
}

func setCondition(newCond v1alpha1.Condition, conditions []v1alpha1.Condition) {
	for i, cond := range conditions {
		if cond.Type == newCond.Type {
			conditions[i] = newCond
			return
		}
	}
}
