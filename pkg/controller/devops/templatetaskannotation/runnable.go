package templatetaskannotation

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/util"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

// AddRunnable add Runnable as runnable
func AddRunnable(mgr manager.Manager) error {
	return mgr.Add(NewRunnable(mgr.GetKubeClient(), mgr.GetDevOpsClient()))
}

// NewRunnable constructs a new bootstrapper
func NewRunnable(client kubernetes.Interface, devopsclient clientset.Interface) sigsmanager.Runnable {
	r := &Runnable{
		Client:       client,
		DevOpsClient: clientset.NewExpansion(devopsclient),
	}
	return r
}

const (
	runnableName = "templatetaskannotation-runable"
)

// Runnable bootstrapper for global-credentials
type Runnable struct {
	Client       kubernetes.Interface
	DevOpsClient clientset.InterfaceExpansion
}

var _ sigsmanager.Runnable = &Runnable{}

// Start adds a start method to be a runnable
func (nb *Runnable) Start(stop <-chan struct{}) (err error) {
	klog.V(5).Infof("[%s] Starting runnable", runnableName)

	nb.UpdateTaskTempalteAnnotation()
	nb.UpdateClusterTaskTempalteAnnotation()

	<-stop
	return
}

func (nb *Runnable) UpdateTaskTempalteAnnotation() {

	kind := devopsv1alpha1.TypePipelineTaskTemplate

	templates, err := nb.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().List(kind, "", metav1.ListOptions{ResourceVersion: "0"})
	if err != nil {
		klog.Errorf("runnable %v get pipelinetemplate failed, err is %v ", runnableName, err)
		return
	}

	result, err := nb.ReplaceAnnotation(templates)

	ShowPlacedTaskTemplate(kind, result)

	if err != nil {
		klog.Errorf("runnable %v replace annotation failed, err is %v ", runnableName, err)
		return
	}

}

func (nb *Runnable) UpdateClusterTaskTempalteAnnotation() {
	kind := devopsv1alpha1.TypeClusterPipelineTaskTemplate
	templates, err := nb.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().List(kind, "", metav1.ListOptions{ResourceVersion: "0"})

	if err != nil {
		klog.Errorf("runnable %v get clusterpipelinetempalte failed, err is %v ", runnableName, err)
	}
	result, err := nb.ReplaceAnnotation(templates)

	ShowPlacedTaskTemplate(kind, result)

	if err != nil {
		klog.Errorf("runnable %v replace annotation failed, err is %v ", runnableName, err)
		return
	}
}

func ShowPlacedTaskTemplate(kind string, templates []devopsv1alpha1.PipelineTaskTemplateInterface) {
	for _, template := range templates {
		klog.V(5).Infof("Runnable %v replace %v %v successed", runnableName, kind, template.GetName())
	}
}

func (rb *Runnable) ReplaceAnnotation(templates []devopsv1alpha1.PipelineTaskTemplateInterface) (result []devopsv1alpha1.PipelineTaskTemplateInterface, err error) {

	errs := util.MultiErrors{}

	for _, template := range templates {
		klog.V(5).Infof("%s ready replace annotation for %s %s", runnableName, template.GetKind(), template.GetName())

		changed, templateCopy := GetCommpatiableTemplate(template)
		if !changed {
			continue
		}

		newtemplate, err := rb.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Update(templateCopy)
		result = append(result, newtemplate)
		if err != nil {
			klog.Errorf("%v replace annotation for %v %v failed err is %s", runnableName, template.GetKind(), template.GetName(), err.Error())
			errs = append(errs, err)
		}
		klog.V(5).Infof("runnable %s replace %s %s annotation successfully", runnableName, template.GetKind(), template.GetName())
	}

	if len(errs) != 0 {
		return result, &errs
	}
	return result, nil
}

func GetCommpatiableTemplate(template devopsv1alpha1.PipelineTaskTemplateInterface) (bool, devopsv1alpha1.PipelineTaskTemplateInterface) {

	obj, changed := k8s.CompatiableBaseDomain(template)

	result := obj.(devopsv1alpha1.PipelineTaskTemplateInterface)

	return changed, result
}
