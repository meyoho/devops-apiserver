package projectmanagement

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	rolesynccontrollerName = "projectmanagementRoleSync-controller"
)

type ProjectManagementRoleSyncReconciler struct {
	reconcile.Reconciler
	Client           kubernetes.Interface
	DevopsClient     clientset.Interface
	ThirdPartyClient thirdparty.Interface
}

// Add injects the manager so to inject the controller in the manager
func AddRoleSync(mgr manager.Manager) error {

	Pmreconciler, err := NewProjectManagementRoleSyncReconciler(mgr)
	if err != nil {
		return err
	}
	return addrolesync(mgr, Pmreconciler)

}

func addrolesync(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(rolesynccontrollerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &devopsv1alpha1.ProjectManagement{}},
		&handler.EnqueueRequestForObject{},
		predicate.RoleSyncEnable, predicate.RoleSyncTTL(rolesynccontrollerName),
	)
	return nil
}

func NewProjectManagementRoleSyncReconciler(mgr manager.Manager) (roleReconciler reconcile.Reconciler, err error) {
	return NewProjectManagementRoleSyncReconcilerBy(mgr.GetKubeClient(), mgr.GetDevOpsClient(), mgr.GetThirdParty(), mgr.GetExtraConfig())
}

func NewProjectManagementRoleSyncReconcilerBy(client kubernetes.Interface, devopsClient clientset.Interface, thirdPartyClient thirdparty.Interface, extra manager.ExtraConfig) (roleReconciler reconcile.Reconciler, err error) {
	rec := &ProjectManagementRoleSyncReconciler{
		Client:           client,
		DevopsClient:     devopsClient,
		ThirdPartyClient: thirdPartyClient,
	}
	roleReconciler, err = reconciler.NewRoleSyncReconcilerBy(client, devopsClient, thirdPartyClient, reconciler.RoleSyncOptions{
		Name:             rolesynccontrollerName,
		GetToolData:      rec.GetProjectManagementToolData,
		GetRoleMapping:   rec.GetProjectManagementRoleMapping,
		ApplyRoleMapping: rec.ApplyProjectManagementRoleMapping,
	}, extra.SystemNamespace)
	if err != nil {
		roleReconciler = nil
		return
	}
	rec.Reconciler = roleReconciler
	return rec, nil
}

func (p *ProjectManagementRoleSyncReconciler) GetProjectManagementToolData(request reconcile.Request) (data reconciler.ToolData, err error) {
	Name := request.Name

	pm, err := p.DevopsClient.DevopsV1alpha1().ProjectManagements().Get(Name, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return data, err
	}
	//will get all namespaces
	bindings, err := p.DevopsClient.DevopsV1alpha1().ProjectManagementBindings("").List(metav1.ListOptions{})
	if err != nil {
		return data, err
	}
	filterbindings := []runtime.Object{}
	projectmap := make(map[string][]string)

	for _, binding := range bindings.Items {
		if binding.Spec.ProjectManagement.Name == Name {
			filterbindings = append(filterbindings, &binding)
			projectarray := []string{}
			for _, projectinfo := range binding.Spec.ProjectManagementProjectInfos {
				projectarray = append(projectarray, projectinfo.Name)
			}
			projectmap[binding.ObjectMeta.Namespace] = projectarray
		}
	}
	data.ToolInstance = pm
	data.ToolType = string(pm.Spec.Type)
	data.Bindings = filterbindings
	data.NamespaceProjectsMap = projectmap
	return data, nil
}

func (p *ProjectManagementRoleSyncReconciler) GetProjectManagementRoleMapping(data reconciler.ToolData, opts *v1alpha1.RoleMappingListOptions) (roleMapping *v1alpha1.RoleMapping, err error) {
	project, _ := data.ToolInstance.(*devopsv1alpha1.ProjectManagement)

	serviceName := project.Name
	result, err := p.DevopsClient.DevopsV1alpha1().ProjectManagements().GetRoleMapping(serviceName, opts)
	return result, nil
}

func (p *ProjectManagementRoleSyncReconciler) ApplyProjectManagementRoleMapping(data reconciler.ToolData, roleMapping *v1alpha1.RoleMapping) (result *v1alpha1.RoleMapping, err error) {
	project, _ := data.ToolInstance.(*devopsv1alpha1.ProjectManagement)

	serviceName := project.Name
	result, _ = p.DevopsClient.DevopsV1alpha1().ProjectManagements().ApplyRoleMapping(serviceName, roleMapping)
	return result, nil
}
