package projectmanagement_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/projectmanagement"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var _ = Describe("AddRoleSync",
	testtools.GenControllerSetupTest(
		"projectmanagementRoleSync-controller",
		projectmanagement.AddRoleSync,
		&source.Kind{Type: &v1alpha1.ProjectManagement{}},
		&handler.EnqueueRequestForObject{},
		predicate.RoleSyncEnable, predicate.RoleSyncTTL("projectmanagementRoleSync-controller"),
	),
)
