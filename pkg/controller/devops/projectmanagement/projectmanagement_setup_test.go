package projectmanagement_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/projectmanagement"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestProjectManagementController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("projectmanagement.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/projectmanagement", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"projectmanagement-controller",
		projectmanagement.Add,
		&source.Kind{Type: &v1alpha1.ProjectManagement{}},
		&handler.EnqueueRequestForObject{},
	),
)
