package projectmanagement_test

import (
	"context"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		mockCtrl                       *gomock.Controller
		cacheClient                    client.Client
		devopsClient                   *clientset.Clientset
		request                        reconcile.Request
		result                         reconcile.Result
		err                            error
		Jira                           devopsv1alpha1.ProjectManagementType = "Jira"
		newprojectmanagementReconciler *reconciler.ToolReconciler
		newprojectmanagementObj        *devopsv1alpha1.ProjectManagement
		//systemNamespace                = "alauda-system"
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		cacheClient = testtools.NewFakeClient()
		devopsClient = clientset.NewSimpleClientset()

		newprojectmanagementReconciler = reconciler.NewToolReconciler("new-projectmanagement-controller", devopsv1alpha1.TypeProjectManagement, cacheClient, devopsClient, devopsv1alpha1.TypeProjectManagement, "projectmanagementbindings", "projectManagement", devopsv1alpha1.AnnotationProvider{})

		request.Name = "projectmanagement"

		newprojectmanagementObj = GetProjectManagement(Jira, "http://some-host", request.Name)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})
	JustBeforeEach(
		func() {
			result, err = newprojectmanagementReconciler.Reconcile(request)
		})
	Context("projectmanagement does not exist", func() {
		It("should not return error ", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})
	Context("projectManagement can be updated  ", func() {
		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(newprojectmanagementObj)
			newprojectmanagementReconciler = reconciler.NewToolReconciler("new-projectmanagement-controller", devopsv1alpha1.TypeProjectManagement, cacheClient, devopsClient, devopsv1alpha1.TypeProjectManagement, "projectmanagementbindings", "projectManagement", devopsv1alpha1.AnnotationProvider{})
		})
		It("get a projectmanangement", func() {

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			newprojectmanagementObj = &devopsv1alpha1.ProjectManagement{}
			err = cacheClient.Get(context.TODO(), request.NamespacedName, newprojectmanagementObj)
			Expect(err).To(BeNil())
			Expect(newprojectmanagementObj).NotTo(BeNil())
			Expect(newprojectmanagementObj.Name).To(Equal(request.Name))

		})

		It("update a projectmanagement", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})
})

func GetProjectManagement(ptype devopsv1alpha1.ProjectManagementType, host string, name string) *devopsv1alpha1.ProjectManagement {
	return &devopsv1alpha1.ProjectManagement{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: devopsv1alpha1.ProjectManagementSpec{
			Type: ptype,
			ToolSpec: devopsv1alpha1.ToolSpec{
				HTTP: devopsv1alpha1.HostPort{
					Host: host,
				},
			},
		},
	}
}
