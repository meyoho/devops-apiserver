package projectmanagement_test

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/devops/projectmanagement"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		devopsClient                 *clientset.Clientset
		request                      reconcile.Request
		tooldata                     reconciler.ToolData
		err                          error
		projectrolereconciler        projectmanagement.ProjectManagementRoleSyncReconciler
		requestName                  string
		Jira                         devopsv1alpha1.ProjectManagementType = "Jira"
		newprojectmanagementObj      *devopsv1alpha1.ProjectManagement
		projectmanagementbindinglist *devopsv1alpha1.ProjectManagementBindingList
	)
	BeforeEach(func() {
		devopsClient = clientset.NewSimpleClientset()
		projectrolereconciler.DevopsClient = devopsClient
		requestName = "projectmanagementname"
		newprojectmanagementObj = testtools.GetProjectManagementRole(Jira, "http://some-host", requestName)
		projectmanagementbindinglist = testtools.GetProjectManagementBinding(requestName)

	})
	JustBeforeEach(
		func() {
			request.Name = requestName
			projectrolereconciler.DevopsClient = devopsClient

			tooldata, err = projectrolereconciler.GetProjectManagementToolData(request)
		})
	Context("tooldata should not exist", func() {
		It("should return error ", func() {
			Expect(err).NotTo(BeNil())
			Expect(tooldata).To(Equal(reconciler.ToolData{}))
		})
	})
	Context("should return pm ", func() {
		BeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset(newprojectmanagementObj)

		})
		It("should return projectmanagement", func() {
			newprojectmanagementObj, err = projectrolereconciler.DevopsClient.DevopsV1alpha1().ProjectManagements().Get(requestName, metav1.GetOptions{ResourceVersion: "0"})

			Expect(err).To(BeNil())
			Expect(newprojectmanagementObj).NotTo(BeNil())
			Expect(newprojectmanagementObj.Name).To(Equal(requestName))

		})
	})
	Context("should return projectmanagementbindinglist", func() {
		BeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset(newprojectmanagementObj, projectmanagementbindinglist)
		})
		It("should return projectmanagementbindinglist", func() {
			projectmanagementbindinglist, err = projectrolereconciler.DevopsClient.DevopsV1alpha1().ProjectManagementBindings("").List(metav1.ListOptions{})
			Expect(err).To(BeNil())
			Expect(projectmanagementbindinglist).ToNot(BeNil())
			Expect(projectmanagementbindinglist.Items[0].Name).To(Equal(requestName))
			Expect(tooldata).ToNot(BeNil())
			Expect(tooldata.ToolType).To(Equal(string(Jira)))
			Expect(tooldata.ToolInstance).To(Equal(newprojectmanagementObj))
			Expect(tooldata.Bindings[0]).To(Equal(&projectmanagementbindinglist.Items[0]))
		})
	})
})
