package projectmanagement

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/metrics"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName = "projectmanagement-controller"
)

// Add injects the manager so to inject the controller in the manager
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconciler(mgr))
}

// NewReconciler inits a new reconciler for ProjectManagement
func NewReconciler(mgr manager.Manager) reconcile.Reconciler {
	r := reconciler.NewToolReconcilerByManager(mgr, controllerName, devopsv1alpha1.TypeProjectManagement, devopsv1alpha1.TypeProjectManagement, "projectmanagementbindings", "projectManagement")

	return metrics.DecorateReconciler(
		controllerName,
		devopsv1alpha1.TypeProjectManagement, r,
	).WithResourceReconcileTime()
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &devopsv1alpha1.ProjectManagement{}},
		&handler.EnqueueRequestForObject{},
	)
	return nil
}
