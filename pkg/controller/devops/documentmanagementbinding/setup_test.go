package documentmanagementbinding_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/documentmanagementbinding"
	"alauda.io/devops-apiserver/pkg/controller/testtools"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestDocumentManagementController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("documentmanagementbinding.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/documentmanagementbinding", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"document-management-binding-controller",
		documentmanagementbinding.Add,
		&source.Kind{Type: &v1alpha1.DocumentManagementBinding{}},
		&handler.EnqueueRequestForObject{},
	),
)
