package documentmanagementbinding_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	verclientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/devops/documentmanagementbinding"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		mockCtrl     *gomock.Controller
		cacheClient  client.Client
		devopsClient *clientset.Clientset
		request      reconcile.Request
		result       reconcile.Result
		err          error
		reconciler   *documentmanagementbinding.Reconciler
		bindingObj   *v1alpha1.DocumentManagementBinding
		//systemNamespace  = "alauda-system"
		// getprojectmanagementFunc func(Name string) (*devopsv1alpha1.ProjectManagement, error)
	)
	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		reconciler = &documentmanagementbinding.Reconciler{}
		// getprojectmanagementFunc = reconciler.GetProjectManagement

		cacheClient = testtools.NewFakeClient()
		devopsClient = clientset.NewSimpleClientset()
		request.Name = "documentmanagementbinding"
		request.Namespace = "default"

		// bindingObj = GetProjectManagementBinding(devopsv1alpha1.LocalObjectReference{Name: "projectManagement"}, request.Name, request.Namespace)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	JustBeforeEach(
		func() {
			reconciler.CacheClient = cacheClient
			reconciler.DevOpsClient = verclientset.NewExpansion(devopsClient)
			// reconciler.GetprojectmanagementFunc = getprojectmanagementFunc
			result, err = reconciler.Reconcile(request)
		})

	Context("DocumentManagementBinding does not exist", func() {
		It("should not return error ", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})
	Context("DocumentManagementBinding exists", func() {
		BeforeEach(func() {
			bindingObj = testtools.GetDocumentBindingObj(request.Name, request.Namespace, "document", "secret", "default")
			serviceObj := testtools.GetDocumentManagement("document", "http://invalid.url.com", "", "")
			secretObj := testtools.GetSecret("secret", "default", corev1.SecretTypeBasicAuth, map[string][]byte{
				corev1.BasicAuthUsernameKey: []byte("username"),
				corev1.BasicAuthPasswordKey: []byte("password"),
			})
			devopsClient = clientset.NewSimpleClientset(bindingObj, serviceObj)
			cacheClient = testtools.NewFakeClient(bindingObj, serviceObj, secretObj)

		})
		It("should not return error and should have multiple actions", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})
})
