package documentmanagementbinding

import (
	"context"
	"time"

	"alauda.io/devops-apiserver/pkg/controller/metrics"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const controllerName = "document-management-binding-controller"

// Add a controller to the manager
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

// NewReconcilerByManager constructs a reconciler using a manager
func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	return NewReconciler(mgr.GetClient(), mgr.GetDevOpsClient())
}

// NewReconciler creates a reconciler using clients
func NewReconciler(cacheClient client.Client, devopsClient clientset.Interface) reconcile.Reconciler {
	r := &Reconciler{
		CacheClient:  cacheClient,
		DevOpsClient: clientset.NewExpansion(devopsClient),
	}

	return metrics.DecorateReconciler(controllerName, v1alpha1.TypeDocumentManagementBinding, r).WithResourceReconcileTime()
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.DocumentManagementBinding{}},
		&handler.EnqueueRequestForObject{},
		predicate.EmptyOwnerTTL(controllerName, v1alpha1.TTLSession),
	)
	return
}

// Reconciler reconciler for DocumentManagementBinding
type Reconciler struct {
	CacheClient  client.Client
	DevOpsClient clientset.InterfaceExpansion
}

var _ reconcile.Reconciler = &Reconciler{}

// Reconcile reconcile method for DocumentManagementBinding Reconciler
func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	glog.V(5).Infof("[%s] Reconciling \"%s/%s\"", controllerName, request.Namespace, request.Name)
	defer func() {

		if err != nil {
			glog.Errorf("[%s] Reconciling finished for \"%s/%s\": result: %#v err: %#v", controllerName, request.Namespace, request.Name, result, err)
		} else {
			glog.V(5).Infof("[%s] Reconciling finished for \"%s/%s\": err: %#v", controllerName, request.Namespace, request.Name, err)
		}
		err = nil
		result = reconcile.Result{}
	}()
	binding := &v1alpha1.DocumentManagementBinding{}
	err = rec.CacheClient.Get(context.TODO(), request.NamespacedName, binding)
	if err != nil {
		return
	}
	if binding == nil {
		return
	}
	bindingCopy := binding.DeepCopy()
	serviceName := binding.Spec.DocumentManagement.Name
	conditioner := generic.NewStandardConditionProcess(controllerName, binding.GetName())
	secretConditioner := generic.NewRecSecretConditioner(binding.Spec.Secret, rec.CacheClient)
	serviceConditioner := generic.NewDevOpsToolInterfaceConditioner(v1alpha1.TypeDocumentManagement, serviceName, rec.DevOpsClient)
	authConditioner := generic.NewAuthorizationConditioner(binding.Spec.DocumentManagement.Name, binding.Spec.Secret, rec.DevOpsClient.DevopsV1alpha1().DocumentManagements().Authorize)

	conditioner.Add(serviceConditioner).Add(secretConditioner).Add(authConditioner)

	currentConditions := conditioner.Conditions()
	bindingCopy.Status.Conditions = currentConditions
	bindingCopy.Status.HTTPStatus = serviceConditioner.GetHTTPStatus()
	bindingCopy.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	bindingCopy.Status = generic.GetServicePhaseStatus(currentConditions, bindingCopy.Status)

	glog.V(5).Infof("[%s] Will update \"%s/%s\"", controllerName, request.Namespace, request.Name)
	err = rec.CacheClient.Update(context.TODO(), bindingCopy)

	return
}
