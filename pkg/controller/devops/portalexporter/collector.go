package portalexporter

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"context"
	"k8s.io/klog/klogr"
	"os"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"time"
)

const (
	runnableName = "PortalMetricsCollector"

	productName              = "devops"
	pipelineConfigMetricName = "pipelineconfig_count"
	qualityGateMetricName    = "qualitygate_success_rate"
)

var (
	version  = os.Getenv("PRODUCT_VERSION")
	Interval = time.Minute * 5
	log      = klogr.New().WithName(runnableName)
)

func AddRunnable(mgr manager.Manager) error {
	return mgr.Add(NewRunnable(mgr.GetClient()))
}

func NewRunnable(cacheClient client.Client) PortalMetricsCollector {
	return PortalMetricsCollector{CacheClient: cacheClient}
}

type PortalMetricsCollector struct {
	CacheClient client.Client
}

func (r PortalMetricsCollector) Start(stop <-chan struct{}) error {
	ticker := time.NewTicker(Interval)

	for {
		select {
		case <-ticker.C:
			r.collect()
		case <-stop:
			return nil
		}
	}

}

func (r PortalMetricsCollector) collect() {
	log.V(5).Info("Starting to collect metrics for portal")

	r.collectPipelineConfigs()
	r.collectCodeQualityProjects()

	log.V(5).Info("Finished collect")
}

func (r PortalMetricsCollector) collectPipelineConfigs() {
	pipelineConfigs := &v1alpha1.PipelineConfigList{}
	err := r.CacheClient.List(context.TODO(), &client.ListOptions{}, pipelineConfigs)
	if err != nil {
		log.Error(err, "Failed to calculate metric 'pipelines'. Will skip this collect")
		return
	}

	PortalGauge.WithLabelValues(productName, version, pipelineConfigMetricName, "").Set(float64(len(pipelineConfigs.Items)))

}

func (r PortalMetricsCollector) collectCodeQualityProjects() {
	codeQualityProjects := &v1alpha1.CodeQualityProjectList{}
	err := r.CacheClient.List(context.TODO(), &client.ListOptions{}, codeQualityProjects)
	if err != nil {
		log.Error(err, "Failed to calculate metric 'pipelines'. Will skip this collect")
		return
	}

	if len(codeQualityProjects.Items) == 0 {
		log.V(7).Info("No CodeQualityProjects exist, skip this collect")
		return
	}

	succeedCount := 0
	failedCount := 0
	for _, project := range codeQualityProjects.Items {
		for _, condition := range project.Status.CodeQualityConditions {
			if condition.Status == "OK" {
				succeedCount++
			} else {
				failedCount++
			}
		}
	}

	PortalGauge.WithLabelValues(productName, version, qualityGateMetricName, "").Set(float64(succeedCount / (failedCount + succeedCount) * 100))

}
