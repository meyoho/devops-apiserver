package portalexporter

import (
	"github.com/prometheus/client_golang/prometheus"
	"sigs.k8s.io/controller-runtime/pkg/metrics"
)

// metrics for portal
var (
	PortalGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "product_metric",
	}, []string{"product", "version", "metricname", "clustername"})
)

func init() {
	metrics.Registry.MustRegister(
		PortalGauge,
	)
}
