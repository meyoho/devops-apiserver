package imagerepository_test

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/imagerepository"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	"k8s.io/apimachinery/pkg/runtime"
)

var _ = Describe("Reconciler.Reconcile", testtools.GenBindingControllerTest(
	"imagerepositories",
	"imagerepository",
	"default",
	imagerepository.NewReconciler,
	[]runtime.Object{
		testtools.GetImageRegistry("Harbor", "http://invalid.host.com", "imageregistry"),
		testtools.GetImageRegistryBinding(devopsv1alpha1.LocalObjectReference{Name: "imageregistry"}, "imageregistrybinding", "default"),
		testtools.GetImageRepository(devopsv1alpha1.LocalObjectReference{Name: "imageregistry"}, "imagerepository", "default"),
	},
	[]runtime.Object{},
))
