package imagerepository

import (
	"context"
	"sort"

	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/controller/predicate"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName = "imagerepository-controller"
)

// Add adds controller to the Manager
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

// NewReconcilerByManager constructs a reconciler using a manager
func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	return NewReconciler(mgr.GetClient(), mgr.GetDevOpsClient())
}

// Reconciler main reconciler for ImageRepository
type Reconciler struct {
	CacheClient  client.Client
	DevopsClient clientset.InterfaceExpansion
}

// NewReconciler constructor for Reconciler
func NewReconciler(cacheClient client.Client, devopsClient clientset.Interface) reconcile.Reconciler {
	return &Reconciler{
		CacheClient:  cacheClient,
		DevopsClient: clientset.NewExpansion(devopsClient),
	}

}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}
	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.ImageRepository{}},
		&handler.EnqueueRequestForObject{},
		&predicate.PhaseTTLPredicate{TTL: v1alpha1.TTLServiceCheck, Type: v1alpha1.TypeImageRepository},
	)
	return err
}

var _ reconcile.Reconciler = &Reconciler{}

// Reconcile reconcile resources
func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	glog.V(5).Infof("[%s] Reconciling %s", controllerName, request)
	defer func() {
		logFunc := glog.V(5).Infof
		if err != nil {
			logFunc = glog.Errorf
		}
		logFunc("[%s] Reconciling %s ended %v err %v", controllerName, request, result, err)
		err = nil
	}()

	repository := &v1alpha1.ImageRepository{}
	err = rec.CacheClient.Get(context.TODO(), request.NamespacedName, repository)
	if err != nil {
		utilruntime.HandleError(err)
		if errors.IsNotFound(err) {
			err = nil
		}
		return
	}

	repositoryCopy := repository.DeepCopy()
	glog.V(7).Infof("[%s] %s getting binding %s", controllerName, request, repository.Spec.ImageRegistryBinding.Name)
	binding := &v1alpha1.ImageRegistryBinding{}
	err = rec.CacheClient.Get(context.TODO(), client.ObjectKey{Namespace: request.Namespace, Name: repository.Spec.ImageRegistryBinding.Name}, binding)
	if err != nil {
		if errors.IsNotFound(err) {
			glog.V(5).Infof("[%s] Can not find binding contains the repo %s, deleting...", controllerName, request)
			_ = rec.CacheClient.Delete(context.TODO(), repository)
		}
		return
	}

	glog.V(7).Infof("[%s] %s getting registry %s", controllerName, request, repository.Spec.ImageRegistry.Name)
	registry := &v1alpha1.ImageRegistry{}
	err = rec.CacheClient.Get(context.TODO(), client.ObjectKey{Name: repository.Spec.ImageRegistry.Name}, registry)
	if err != nil {
		glog.Errorf("[%s] Can not get registry with name: %s in repo: %s", controllerName, repository.Spec.ImageRegistry.Name, request)
		return
	}

	secret := &corev1.Secret{}
	secretRef := binding.Spec.Secret
	if secretRef.Name != "" {
		namespace := binding.Namespace
		if secretRef.Namespace != "" {
			namespace = secretRef.Namespace
		}
		glog.V(7).Infof("[%s] %s getting secret %s/%s", controllerName, request, namespace, secretRef.Name)
		err = rec.CacheClient.Get(context.TODO(), client.ObjectKey{Namespace: namespace, Name: secretRef.Name}, secret)
		if err != nil {
			return
		}
		repositoryCopy.ObjectMeta.Annotations["scanDisabled"] = "false"
	} else {
		repositoryCopy.ObjectMeta.Annotations["scanDisabled"] = "true"
	}

	err = rec.processRepository(repositoryCopy)

	_ = rec.CacheClient.Update(context.TODO(), repositoryCopy)
	glog.V(5).Infof("[%s] Repository %s update result: %s", controllerName, request, err)

	return
}

func (rec *Reconciler) processRepository(repository *v1alpha1.ImageRepository) error {
	glog.V(5).Infof("[%s] Processing repository %s/%s", controllerName, repository.Namespace, repository.Name)
	// get repository link
	if repository.Annotations == nil {
		repository.Annotations = map[string]string{}
	}

	// check repository is available
	status, err := rec.DevopsClient.DevopsV1alpha1Expansion().ImageRepositories(repository.Namespace).Remote(repository.Name, &v1alpha1.ImageRepositoryOptions{Image: repository.Spec.Image})
	if err != nil {
		glog.Errorf("[%s] Check repository %s/%s available failed, error is: %s", controllerName, repository.Namespace, repository.Name, err)
		repository.Status.Phase = v1alpha1.ServiceStatusPhaseError
		repository.Status.Reason = string(errors.ReasonForError(err))
		repository.Status.Message = repository.Status.HTTPStatus.Response
		return err
	}
	repository.Status.HTTPStatus = status.Status
	// fetch tags for the repository
	link, err := rec.DevopsClient.DevopsV1alpha1Expansion().ImageRepositories(repository.Namespace).Link(repository.Name, &v1alpha1.ImageRepositoryOptions{Image: repository.Spec.Image})
	if err == nil {
		repository.Annotations[v1alpha1.LabelImageRepositoryLink] = link.Link
	} else {
		glog.Errorf("[%s] Get repository %s/%s link failed, error is: %s", controllerName, repository.Namespace, repository.Name, err)
	}

	tagsResult, err := rec.DevopsClient.DevopsV1alpha1Expansion().ImageRepositories(repository.Namespace).GetImageTags(repository.Name, &v1alpha1.ImageTagOptions{})
	if err != nil {
		glog.Errorf("[%s] list repository %s/%s tags failed, error is: %s", controllerName, repository.Namespace, repository.Name, err)
		repository.Status.Phase = v1alpha1.ServiceStatusPhaseError
		repository.Status.Reason = string(v1alpha1.ServiceStatusPhaseListTagError)
		repository.Status.Message = err.Error()
		return err
	}

	repository.Status.Message = ""
	repository.Status.Phase = v1alpha1.ServiceStatusPhaseReady
	tags := tagsResult.Tags
	sort.Slice(tags, func(i, j int) bool {
		// String sorting rules:
		// a < b True
		// a < A False
		// a < aa True
		if tags[i].CreatedAt == nil || tags[j].CreatedAt == nil {
			return tags[i].Name > tags[j].Name
		}
		return tags[i].CreatedAt.After(tags[j].CreatedAt.Time) || (tags[i].CreatedAt.Time.Equal(tags[j].CreatedAt.Time) && tags[i].Name > tags[j].Name)
	})
	repository.Status.Tags = tags
	if tags != nil && len(tags) > 0 {
		glog.V(5).Infof("[%s] repository %s/%s Add the latestTag: %v", controllerName, repository.Namespace, repository.Name, tags[0])
		repository.Status.LatestTag = tags[0]
	}
	return err

}

func (rec *Reconciler) shouldUpdated(repository, copy *v1alpha1.ImageRepository) bool {
	if copy.Status.Phase != repository.Status.Phase {
		return true
	}
	if repository.Status.LastUpdate == nil && copy.Status.LastUpdate != nil {
		return true
	}
	if copy.Status.LastUpdate != nil &&
		repository.Status.LastUpdate != nil {
		return copy.Status.LatestTag.Digest != repository.Status.LatestTag.Digest || copy.Status.LastUpdate.After(repository.Status.LastUpdate.Time) || copy.Status.LatestTag.Name != repository.Status.LatestTag.Name
	}
	return false
}

func getLastAttempt(status *v1alpha1.HostPortStatus) *metav1.Time {
	if status == nil || status.LastAttempt == nil {
		return nil
	}
	return status.LastAttempt
}
