package imagerepository_test

import (
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/imagerepository"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestImageRepositoryController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("imagerepository.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/imagerepository", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"imagerepository-controller",
		imagerepository.Add,
		&source.Kind{Type: &v1alpha1.ImageRepository{}},
		&handler.EnqueueRequestForObject{},
		&predicate.PhaseTTLPredicate{TTL: v1alpha1.TTLServiceCheck, Type: v1alpha1.TypeImageRepository},
	),
)
