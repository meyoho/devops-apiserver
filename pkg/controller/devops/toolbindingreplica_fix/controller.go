package toolbindingreplica_fix

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	glog "k8s.io/klog"

	"k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

// TODO replace DevOpsClient and kubeClient with client.Client so that we can get resources from cache
const controllerName = "devops-toolbindingreplica-controller-fix-old-toolbinding"

var toolTypes = map[string]runtime.Object{
	devopsv1alpha1.TypeCodeQualityTool:    &devopsv1alpha1.CodeQualityBinding{},
	devopsv1alpha1.TypeCodeRepoService:    &devopsv1alpha1.CodeRepoBinding{},
	devopsv1alpha1.TypeDocumentManagement: &devopsv1alpha1.DocumentManagementBinding{},
	devopsv1alpha1.TypeImageRegistry:      &devopsv1alpha1.ImageRegistryBinding{},
	devopsv1alpha1.TypeJenkins:            &devopsv1alpha1.JenkinsBinding{},
	devopsv1alpha1.TypeProjectManagement:  &devopsv1alpha1.ProjectManagementBinding{},
}

// Add adds new reconciler
func Add(mgr manager.Manager) error {
	for kind := range toolTypes {
		if err := add(mgr, NewReconcilerByManager(kind, mgr)); err != nil {
			return err
		}
	}

	return nil
}

// NewReconcilerByManager creates a reconciler by manager
func NewReconcilerByManager(kind string, mgr manager.Manager) reconcile.Reconciler {
	return &Reconciler{
		Kind:             kind,
		Client:           mgr.GetKubeClient(),
		DevOpsClient:     clientset.NewExpansion(mgr.GetDevOpsClient()),
		ThirdPartyClient: mgr.GetThirdParty(),
	}
}

// NewReconciler creates a new reconciler
func NewReconciler(kind string, kubeClient kubernetes.Interface, devopsClient clientset.Interface, thirdPartyClient thirdparty.Interface) reconcile.Reconciler {
	return &Reconciler{
		Kind:             kind,
		Client:           kubeClient,
		DevOpsClient:     clientset.NewExpansion(devopsClient),
		ThirdPartyClient: thirdPartyClient,
	}
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	if err = ctrl.Watch(&source.Kind{Type: toolTypes[r.(*Reconciler).Kind]},
		&handler.EnqueueRequestForOwner{
			IsController: true,
			OwnerType:    &devopsv1alpha1.ToolBindingReplica{},
		},
	); err != nil {
		return
	}

	return
}

// Reconciler sync
type Reconciler struct {
	Kind             string
	Client           kubernetes.Interface
	DevOpsClient     clientset.InterfaceExpansion
	ThirdPartyClient thirdparty.Interface
}

var _ reconcile.Reconciler = &Reconciler{}

// Reconcile reconciles toolbinding
func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	found, err := rec.DevOpsClient.DevopsV1alpha1Expansion().DevOpsToolBinding().Get(rec.Kind, request.Namespace, request.Name, devopsv1alpha1.GetOptions())
	if err != nil {
		if errors.IsNotFound(err) {
			return reconcile.Result{}, nil
		}
		glog.Errorf("[%s] get toolbinding %s/%s error, %#v", controllerName, request.Namespace, request.Name, err)
		return reconcile.Result{}, nil
	}

	// remove tbr owner reference in toolbinding
	owners := found.GetOwnerReferences()
	if len(owners) < 1 {
		return reconcile.Result{}, nil
	}

	filteredOwners := make([]v1.OwnerReference, 0, len(owners)-1)
	for _, owner := range owners {
		if owner.Kind != devopsv1alpha1.TypeToolBindingReplica {
			filteredOwners = append(filteredOwners, owner)
		}
	}

	if foundObj, ok := found.(runtime.Object); ok {
		obj := foundObj.DeepCopyObject()
		if foundCopy, ok := obj.(v1.Object); ok {
			foundCopy.SetOwnerReferences(filteredOwners)
			_, err = rec.DevOpsClient.DevopsV1alpha1Expansion().DevOpsToolBinding().Update(rec.Kind, request.Namespace, foundCopy)
			if err != nil {
				glog.Errorf("[%s] update toolbinding %s/%s error, %#v", controllerName, request.Namespace, request.Name, err)
				return reconcile.Result{}, nil
			}
			glog.V(5).Infof("[%s] update toolbinding %s/%s successfully, removed the tbr owner reference", controllerName, request.Namespace, request.Name)
		}
	}

	return reconcile.Result{}, nil
}
