package toolbindingreplica_fix_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/devops/toolbindingreplica_fix"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	ctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	extmock "alauda.io/devops-apiserver/pkg/mock/thirdparty/external"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {

	var (
		mockCtrl                        *gomock.Controller
		thirdParty                      *extmock.MockInterface
		managerMock                     *ctrlmock.MockManager
		k8sClient                       *k8sfake.Clientset
		devopsClient                    *clientset.Clientset
		request                         reconcile.Request
		result                          reconcile.Result
		jenkinsBinding                  *v1alpha1.JenkinsBinding
		toolBindingReplicaFixReconciler reconcile.Reconciler
		err                             error
	)

	// We can prepare our tests
	BeforeEach(func() {
		// // starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		managerMock = ctrlmock.NewMockManager(mockCtrl)
		thirdParty = extmock.NewMockInterface(mockCtrl)
		k8sClient = k8sfake.NewSimpleClientset()
		devopsClient = clientset.NewSimpleClientset()
		// starts our basic structs
		request.Name = "some"
		request.Namespace = "default"
		toolBindingReplicaFixReconciler = toolbindingreplica_fix.NewReconciler(v1alpha1.TypeJenkins, k8sClient, devopsClient, thirdParty)
		jenkinsBinding = testtools.GetJenkinsBinding(request.Name, request.Namespace, "jenkins", "username", "password")
		jenkinsBinding.OwnerReferences = []metav1.OwnerReference{
			metav1.OwnerReference{
				Kind: v1alpha1.TypeToolBindingReplica,
			},
		}
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	// here we really run our functions
	JustBeforeEach(func() {
		managerMock.EXPECT().GetDevOpsClient().Return(devopsClient).AnyTimes()
		managerMock.EXPECT().GetKubeClient().Return(k8sClient).AnyTimes()
		managerMock.EXPECT().GetThirdParty().Return(thirdParty).AnyTimes()
		// result, err = toolBindingReplicaReconciler.Reconcile(request)
	})

	Context("toolbinding owner reference tbr can be removed", func() {
		BeforeEach(func() {
			// create jenkinsbinding with owner reference tbr
			jenkinsBinding, err = devopsClient.DevopsV1alpha1().JenkinsBindings(request.Namespace).Create(jenkinsBinding)
		})
		It("get toolbinding", func() {

			result, err = toolBindingReplicaFixReconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// verify if it was really updated
			jenkinsBinding, err = devopsClient.DevopsV1alpha1().JenkinsBindings(request.Namespace).Get(request.Name, v1alpha1.GetOptions())

			Expect(err).To(BeNil())
			Expect(jenkinsBinding).ToNot(BeNil())
			for _, owner := range jenkinsBinding.OwnerReferences {
				Expect(owner.Kind).NotTo(Equal(v1alpha1.TypeToolBindingReplica))
			}
		})

	})
})
