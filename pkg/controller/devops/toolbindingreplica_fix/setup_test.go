package toolbindingreplica_fix_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/toolbindingreplica_fix"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestToolBindingReplicaController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("toolbindingreplica_fix.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/toolbindingreplica_fix", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"devops-toolbindingreplica-controller-fix-old-toolbinding",
		toolbindingreplica_fix.Add,
		&source.Kind{Type: &v1alpha1.ToolBindingReplica{}},
		&handler.EnqueueRequestForObject{},
	),
)
