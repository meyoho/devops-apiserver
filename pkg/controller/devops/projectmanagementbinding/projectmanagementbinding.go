package projectmanagementbinding

import (
	"context"
	"time"

	"alauda.io/devops-apiserver/pkg/controller/metrics"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName = "projectmanagementbinding-controller"
)

func Add(mgr manager.Manager) error {
	return add(mgr, NewReconciler(mgr))

}

type Reconciler struct {
	CacheClient  client.Client
	DevopsClient clientset.InterfaceExpansion
	//Get projectmanagement
	GetprojectmanagementFunc func(Name string) (*devopsv1alpha1.ProjectManagement, error)
}

func NewReconciler(mgr manager.Manager) reconcile.Reconciler {
	rec := &Reconciler{
		CacheClient:  mgr.GetClient(),
		DevopsClient: clientset.NewExpansion(mgr.GetDevOpsClient()),
	}

	// set its own function
	rec.GetprojectmanagementFunc = rec.GetProjectManagement
	return metrics.DecorateReconciler(
		controllerName,
		v1alpha1.TypeProjectManagementBinding, rec).WithResourceReconcileTime()
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}
	err = ctrl.Watch(
		&source.Kind{Type: &devopsv1alpha1.ProjectManagementBinding{}},
		&handler.EnqueueRequestForObject{},
		predicate.EmptyOwnerTTL(controllerName, v1alpha1.TTLSession),
	)
	return nil
}

var _ reconcile.Reconciler = &Reconciler{}

func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	glog.V(5).Infof("[%s] Reconciling %#v", controllerName, request)

	projectmanagementBinding := &v1alpha1.ProjectManagementBinding{}
	err = rec.CacheClient.Get(context.TODO(), request.NamespacedName, projectmanagementBinding)
	if err != nil && errors.IsNotFound(err) {
		utilruntime.HandleError(err)
		err = nil
		return
	}
	projectmanagementBindingCopy := projectmanagementBinding.DeepCopy()
	projectmanagement, err := rec.GetprojectmanagementFunc(projectmanagementBindingCopy.Spec.ProjectManagement.Name)
	if projectmanagement == nil {
		utilruntime.HandleError(err)
		return
	}
	secret := projectmanagementBinding.Spec.Secret

	bindingName := projectmanagementBinding.GetName()
	conditioner := generic.NewStandardConditionProcess(controllerName, bindingName)
	secretConditioner := generic.NewRecSecretConditioner(secret, rec.CacheClient)
	serviceName := projectmanagement.GetName()
	serviceConditioner := generic.NewDevOpsToolInterfaceConditioner(v1alpha1.TypeProjectManagement, serviceName, rec.DevopsClient)
	//serviceConditioner := generic.NewServiceConditioner(projectmanagement.GetName(), thirdparty.GetBasicAuth(projectmanagement.Spec.HTTP.Host, "", ""), projectmanagement.Status.HTTPStatus)
	authConditioner := generic.NewAuthorizationConditioner(projectmanagement.GetName(), secret, rec.DevopsClient.DevopsV1alpha1().ProjectManagements().Authorize)

	projectsinfoConditioner := generic.NewProjectsInfoConditioner(projectmanagement, projectmanagementBinding, secret, rec.DevopsClient.DevopsV1alpha1().ProjectManagements().ListProjects)
	issueoptionConditioner := generic.NewRecIssueOptionConditioner(projectmanagement, bindingName, secret, rec.DevopsClient.DevopsV1alpha1().ProjectManagementBindings(request.Namespace).IssueOptions)

	conditioner.Add(secretConditioner).Add(serviceConditioner).Add(authConditioner).Add(issueoptionConditioner).Add(projectsinfoConditioner)

	currentConditions := conditioner.Conditions()
	projectmanagementBindingCopy.Status.Conditions = currentConditions
	projectmanagementBindingCopy.Status.HTTPStatus = serviceConditioner.GetHTTPStatus()
	projectmanagementBindingCopy.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	projectmanagementBindingCopy.Status = generic.GetServicePhaseStatus(currentConditions, projectmanagementBindingCopy.Status)

	err = rec.CacheClient.Update(context.TODO(), projectmanagementBindingCopy)
	return

}

func (rec *Reconciler) GetProjectManagement(name string) (*devopsv1alpha1.ProjectManagement, error) {
	projectManagement := &v1alpha1.ProjectManagement{}
	err := rec.CacheClient.Get(context.TODO(), client.ObjectKey{Name: name}, projectManagement)
	return projectManagement, err
}
