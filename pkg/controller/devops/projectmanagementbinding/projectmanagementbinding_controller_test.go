package projectmanagementbinding_test

import (
	"context"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	fakeclientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/devops/projectmanagementbinding"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		mockCtrl                              *gomock.Controller
		cacheClient                           client.Client
		devopsClient                          *fakeclientset.Clientset
		request                               reconcile.Request
		result                                reconcile.Result
		err                                   error
		newprojectmanagementbindingReconciler *projectmanagementbinding.Reconciler
		newprojectmanagementObjBinding        *devopsv1alpha1.ProjectManagementBinding
		getprojectmanagementFunc              func(Name string) (*devopsv1alpha1.ProjectManagement, error)
	)
	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		newprojectmanagementbindingReconciler = &projectmanagementbinding.Reconciler{}
		getprojectmanagementFunc = newprojectmanagementbindingReconciler.GetProjectManagement

		cacheClient = testtools.NewFakeClient()
		devopsClient = fakeclientset.NewSimpleClientset()
		request.Name = "projectmanagementbinding"
		request.Namespace = "default"
		newprojectmanagementObjBinding = GetProjectManagementBinding(devopsv1alpha1.LocalObjectReference{Name: "projectManagement"}, request.Name, request.Namespace)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	JustBeforeEach(
		func() {
			newprojectmanagementbindingReconciler.CacheClient = cacheClient
			newprojectmanagementbindingReconciler.DevopsClient = clientset.NewExpansion(devopsClient)
			newprojectmanagementbindingReconciler.GetprojectmanagementFunc = getprojectmanagementFunc
			result, err = newprojectmanagementbindingReconciler.Reconcile(request)
		})

	Context("projectManagementbinding does not exist", func() {
		It("should not return error ", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})
	Context("I can get and update projectManagementBinding", func() {
		BeforeEach(func() {
			newprojectmanagementObjBinding = GetProjectManagementBinding(devopsv1alpha1.LocalObjectReference{Name: "projectManagement"}, request.Name, request.Namespace)
			devopsClient = fakeclientset.NewSimpleClientset(newprojectmanagementObjBinding)
			cacheClient = testtools.NewFakeClient(newprojectmanagementObjBinding)

			getprojectmanagementFunc = func(Name string) (management *devopsv1alpha1.ProjectManagement, e error) {
				return &devopsv1alpha1.ProjectManagement{}, nil
			}
		})
		It("now can update projectmanagementbinding", func() {
			newprojectmanagementObjBinding := &devopsv1alpha1.ProjectManagementBinding{}
			err = cacheClient.Get(context.TODO(), request.NamespacedName, newprojectmanagementObjBinding)

			Expect(err).To(BeNil())
			Expect(newprojectmanagementObjBinding).NotTo(BeNil())

		})
	})
})

func GetProjectManagementBinding(projectmanagement devopsv1alpha1.LocalObjectReference, name string, namespace string) *devopsv1alpha1.ProjectManagementBinding {
	return &devopsv1alpha1.ProjectManagementBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: devopsv1alpha1.ProjectManagementBindingSpec{
			ProjectManagement: projectmanagement,
			Secret:            devopsv1alpha1.SecretKeySetRef{},
			ProjectManagementProjectInfos: []devopsv1alpha1.ProjectManagementProjectInfo{
				devopsv1alpha1.ProjectManagementProjectInfo{
					ID:   "id",
					Name: "name",
				},
			},
		},
	}
}
