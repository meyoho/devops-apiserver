package projectmanagementbinding_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/projectmanagementbinding"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestSecretController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("projectmanagementbinding.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/projectmanagementbinding", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"projectmanagementbinding-controller",
		projectmanagementbinding.Add,
		&source.Kind{Type: &v1alpha1.ProjectManagementBinding{}},
		&handler.EnqueueRequestForObject{},
	),
)
