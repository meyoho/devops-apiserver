package jenkins

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/metrics"

	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName = "jenkins-controller"
)

// Add injects the manager so to inject the controller in the manager
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconciler(mgr))
}

// NewReconciler returns a new reconcile.Reconciler
func NewReconciler(mgr manager.Manager) reconcile.Reconciler {
	rec := reconciler.NewToolReconcilerByManager(mgr, controllerName, v1alpha1.TypeJenkins, v1alpha1.TypeJenkins, "jenkinsbindings", "jenkins")

	return metrics.DecorateReconciler(
		controllerName,
		v1alpha1.TypeJenkins, rec,
	).WithResourceReconcileTime()
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.Jenkins{}}, &handler.EnqueueRequestForObject{},
		&predicate.PhaseTTLPredicate{TTL: v1alpha1.TTLServiceCheck, Type: v1alpha1.TypeJenkins},
	)
	return nil
}
