package jenkins_test

import (
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/devops/jenkins"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	ctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	extmock "alauda.io/devops-apiserver/pkg/mock/thirdparty/external"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {

	var (
		mockCtrl          *gomock.Controller
		cacheClient       client.Client
		thirdParty        *extmock.MockInterface
		managerMock       *ctrlmock.MockManager
		jenkinsReconciler reconcile.Reconciler
		k8sClient         *k8sfake.Clientset
		devopsClient      *clientset.Clientset
		request           reconcile.Request
		result            reconcile.Result
		jenkinsObj        *v1alpha1.Jenkins
		err               error
	)

	// We can prepare our tests
	BeforeEach(func() {
		// // starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		managerMock = ctrlmock.NewMockManager(mockCtrl)
		thirdParty = extmock.NewMockInterface(mockCtrl)
		// starts our basic structs
		cacheClient = testtools.NewFakeClient()
		k8sClient = k8sfake.NewSimpleClientset()
		devopsClient = clientset.NewSimpleClientset()
		request.Name = "some"
		request.Namespace = ""
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	// here we really run our functions
	JustBeforeEach(func() {
		managerMock.EXPECT().GetClient().Return(cacheClient).AnyTimes()
		managerMock.EXPECT().GetDevOpsClient().Return(devopsClient).AnyTimes()
		managerMock.EXPECT().GetKubeClient().Return(k8sClient).AnyTimes()
		managerMock.EXPECT().GetThirdParty().Return(thirdParty).AnyTimes()
		managerMock.EXPECT().GetAnnotationProvider().Return(v1alpha1.AnnotationProvider{BaseDomain: "GetAnnotationProvider"}).AnyTimes()
	})

	// if not added to the client it should return not found error on the client side
	Context("jenkins does not exist", func() {
		It("should not return an error", func() {

			jenkinsReconciler = jenkins.NewReconciler(managerMock)
			// runs our test here
			result, err = jenkinsReconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("jenkins does not have secret", func() {
		BeforeEach(func() {
			// k8sClient = k8sfake.NewSimpleClientset(secretObj)
			devopsClient = clientset.NewSimpleClientset(testtools.GetJenkins("some", "http://invalid.url.com", "", ""))
		})
		It("should fetch jenkins management and reconcile", func() {
			jenkinsReconciler = jenkins.NewReconciler(managerMock)
			// runs our test here
			result, err = jenkinsReconciler.Reconcile(request)

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// verify if it was really updated
			jenkinsObj, err = devopsClient.DevopsV1alpha1().Jenkinses().Get("some", metav1.GetOptions{ResourceVersion: "0"})

			Expect(err).To(BeNil())
			Expect(jenkinsObj).ToNot(BeNil())
			Expect(jenkinsObj.Status.Conditions).ToNot(BeEmpty(), "should have conditions")
			Expect(jenkinsObj.Status.Conditions).To(HaveLen(1), "should have one condition")
		})
	})

	Context("jenkins does not have secret", func() {
		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(testtools.GetSecret("secret", "default", corev1.SecretTypeBasicAuth, map[string][]byte{
				corev1.BasicAuthUsernameKey: []byte("admin"),
				corev1.BasicAuthPasswordKey: []byte("password"),
			}))
			devopsClient = clientset.NewSimpleClientset(testtools.GetJenkins("some", "http://invalid.url.com", "secret", "default"))
		})
		It("should fetch jenkins management and reconcile", func() {
			jenkinsReconciler = jenkins.NewReconciler(managerMock)
			// runs our test here
			result, err = jenkinsReconciler.Reconcile(request)

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// verify if it was really updated
			jenkinsObj, err = devopsClient.DevopsV1alpha1().Jenkinses().Get("some", metav1.GetOptions{ResourceVersion: "0"})

			Expect(err).To(BeNil())
			Expect(jenkinsObj).ToNot(BeNil())
			Expect(jenkinsObj.Status.Conditions).ToNot(BeEmpty(), "should have conditions")
			Expect(jenkinsObj.Status.Conditions).To(HaveLen(3), "should have three conditions")
		})
	})
})
