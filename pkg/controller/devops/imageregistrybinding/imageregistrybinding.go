package imageregistrybinding

import (
	"fmt"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/controller/metrics"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/controller/predicate"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	util "alauda.io/devops-apiserver/pkg/util/generic"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	pkgruntime "k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	"k8s.io/klog/klogr"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName        = "imageregistrybinding-controller"
	syncRepoOwner         = "sync-repository"
	syncRepoConditionType = "SyncRepositories"
	syncRepoErrorReason   = "SyncRepositoriesFailed"
)

var (
	groupVersionKind = schema.GroupVersionKind{
		Group:   v1alpha1.GroupName,
		Version: v1alpha1.Version,
		Kind:    v1alpha1.TypeCodeRepoBinding,
	}
)

func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

type Reconciler struct {
	CacheClient  client.Client
	DevopsClient clientset.InterfaceExpansion

	ItemsDeleter reconciler.LocalGeneratedItemsDeleter
}

func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	return NewReconciler(mgr.GetClient(), mgr.GetDevOpsClient(), mgr.GetAnnotationProvider())
}

func NewReconciler(cacheClient client.Client, devopsClient clientset.Interface, provider v1alpha1.AnnotationProvider) reconcile.Reconciler {
	r := &Reconciler{
		CacheClient:  cacheClient,
		DevopsClient: clientset.NewExpansion(devopsClient),
	}

	r.ItemsDeleter = reconciler.NewLocalGeneratedItemsDeleter(cacheClient, r.DevopsClient,
		v1alpha1.TypeImageRegistryBinding, v1alpha1.ResourceNameImageRepository,
		func(parent types.NamespacedName) *metav1.LabelSelector {
			return &metav1.LabelSelector{
				MatchLabels: map[string]string{
					v1alpha1.LabelImageRegistryBinding: parent.Name,
				}}
		}, provider)
	// set its own function
	return metrics.DecorateReconciler(controllerName, v1alpha1.TypeImageRegistryBinding, r).WithResourceReconcileTime()
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}
	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.ImageRegistryBinding{}},
		&handler.EnqueueRequestForObject{},
		predicate.EmptyOwnerTTL(controllerName, v1alpha1.TTLSession),
	)
	return err
}

var _ reconcile.Reconciler = &Reconciler{}

// Reconcile reconcile loop for ImageRegistryBinding
func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	log := klogr.New().WithName(fmt.Sprintf("[%s]", controllerName)).WithValues("ImageRegistryBinding", request.String())

	log.V(5).Info("Reconciling")
	defer func() {
		logFunc := glog.V(5).Infof
		if err != nil {
			logFunc = glog.Errorf
		}
		logFunc("[%s] Reconciling %s ended %v err %v", controllerName, request, result, err)
		err = nil
	}()

	binding, err := rec.DevopsClient.DevopsV1alpha1().ImageRegistryBindings(request.Namespace).Get(request.Name, v1alpha1.GetOptions())
	if err != nil {
		utilruntime.HandleError(err)
		if !errors.IsNotFound(err) {
			return
		}
		log.Info("ImageRegistryBinding is not found")
		rec.ItemsDeleter.DeleteItemsOf(log, request.NamespacedName)
		err = nil
		return
	}

	bindingCopy := binding.DeepCopy()
	serviceName := binding.Spec.ImageRegistry.Name
	conditioner := generic.NewStandardConditionProcess(controllerName, binding.GetName())
	serviceConditioner := generic.NewDevOpsToolInterfaceConditioner(v1alpha1.TypeImageRegistry, serviceName, rec.DevopsClient)
	secretConditioner := generic.NewRecSecretConditioner(binding.Spec.Secret, rec.CacheClient)
	authConditioner := generic.NewAuthorizationConditioner(binding.Spec.ImageRegistry.Name, binding.Spec.Secret, rec.DevopsClient.DevopsV1alpha1().ImageRegistries().Authorize)
	conditioner.Add(serviceConditioner).Add(secretConditioner).Add(authConditioner)

	currentConditions := conditioner.Conditions()
	// bindingCopy.Status.HTTPStatus = nil
	bindingCopy.Status.HTTPStatus = serviceConditioner.GetHTTPStatus()
	bindingCopy.Status.LastUpdate = &metav1.Time{Time: time.Now()}

	// needs to check if the current condtions are healthy...
	glog.V(5).Infof("[%s] ImageRegistryBinding %s currentConditions  %s", controllerName, request, bindingCopy.Status)
	bindingCopy.Status = generic.GetServicePhaseStatus(currentConditions, bindingCopy.Status)

	glog.V(5).Infof("[%s] ImageRegistryBinding %s before SyncRepos %s", controllerName, request, bindingCopy.Status)

	// if binding phase is ok we need to fetch repositories
	if bindingCopy.Status.Phase == v1alpha1.ServiceStatusPhaseReady {
		// Phase = Ready indicate the toolinterface is not nil
		imageRegistry := serviceConditioner.ToolInterface.(*v1alpha1.ImageRegistry)
		secret := secretConditioner.Secret

		var conditions []v1alpha1.BindingCondition
		conditions, err = rec.SyncRepos(bindingCopy, imageRegistry, secret)
		if err != nil {
			// set bindingCopy.Status.Phase to error
			bindingCopy.Status.Phase = v1alpha1.ServiceStatusPhaseError
			bindingCopy.Status.Message = err.Error()
			bindingCopy.Status.Reason = syncRepoErrorReason
		}
		if conditions != nil {
			// having repo conditions we replace them using the same owner
			bindingCopy.Status.Conditions = v1alpha1.BindingConditions(bindingCopy.Status.Conditions).ReplaceBy(syncRepoOwner, conditions)
		}
	}
	glog.V(5).Infof("[%s] ImageRegistryBinding %s after SyncRepos %s", controllerName, request, bindingCopy.Status)

	// ignore the error, just wait for next loop
	err = nil

	_, err = rec.DevopsClient.DevopsV1alpha1().ImageRegistryBindings(binding.GetNamespace()).UpdateStatus(bindingCopy)

	return
}

// SyncRepos syncing repositories
func (rec *Reconciler) SyncRepos(binding *v1alpha1.ImageRegistryBinding, registry *v1alpha1.ImageRegistry, secret *corev1.Secret) (conditions []v1alpha1.BindingCondition, err error) {
	logName := "SyncRepos"

	var remoteRepos *v1alpha1.ImageRegistryBindingRepositories
	remoteRepos, err = rec.DevopsClient.DevopsV1alpha1().ImageRegistryBindings(binding.Namespace).GetImageRepos(binding.GetName())
	glog.V(9).Infof("%s %s remoteRepos is %#v", controllerName, logName, remoteRepos)

	if err != nil {
		glog.Errorf("[%s] %s SyncRepos get remote repositories error %s", controllerName, logName, err)
		return
	}
	// filter only by the ones used in the binding
	remoteRepoItems := getRepoFromPath(remoteRepos.Items, binding.Spec.RepoInfo.Repositories)
	glog.V(9).Infof("%s %s remoteRepoItems is %#v", controllerName, logName, remoteRepoItems)

	conditions = make([]v1alpha1.BindingCondition, 0, len(remoteRepos.Items))
	repoIndex := map[string]*v1alpha1.ImageRepository{}
	selector := v1alpha1.GetSimpleSelector(v1alpha1.LabelImageRegistry, registry.GetName())
	localRepos, _ := rec.DevopsClient.DevopsV1alpha1().ImageRepositories(binding.GetNamespace()).List(metav1.ListOptions{
		LabelSelector:   selector.String(),
		ResourceVersion: "0",
	})
	glog.V(9).Infof("%s %s localRepos is %#v", controllerName, logName, localRepos)

	endpoint, err := rec.getRegistryEndpoint(*binding)
	if err != nil {
		glog.Errorf("[%s] Cannot get imageregistry endpoint:%s", controllerName, err.Error())
		// ignore this error
	}

	for _, repo := range remoteRepoItems {
		newLocal := generateRepo(repo, binding, endpoint)
		condition := rec.NewImageRepoCondition(newLocal.Namespace, newLocal.Name)

		// sync repo to k8s
		var foundRepo *v1alpha1.ImageRepository
		for _, localRepo := range localRepos.Items {
			if newLocal.GetName() == localRepo.GetName() {
				foundRepo = &localRepo
				break
			}
		}
		// If the  repo is not present we should create one new
		if foundRepo == nil {
			glog.V(6).Infof("[%s] SyncRepos will create repository %s/%s for ImageRegistryBinding %s/%s", controllerName, newLocal.Namespace, newLocal.Name, binding.Namespace, binding.Name)
			_, err := rec.DevopsClient.DevopsV1alpha1().ImageRepositories(binding.GetNamespace()).Create(newLocal)
			if err != nil {
				glog.Errorf("[%s] SyncRepos error while creating repository %s/%s: %v", controllerName, newLocal.Namespace, newLocal.Name, err)
				condition = generic.ConvertErrorToCondition(binding.Name, binding.Namespace, condition.Type, err)
				condition.Owner = syncRepoOwner
			}
		} else {
			repoIndex[foundRepo.GetName()] = foundRepo
		}
		conditions = append(conditions, condition)
	}

	for _, local := range localRepos.Items {
		// if not already synced, and has owner for CodeRepoBinding should delete
		currentRepo, ok := repoIndex[local.GetName()]
		if !ok && generic.HasOwner(&local, binding) {
			glog.V(6).Infof("[%s] will delete ImageRepository %s/%s as it is not bound anymore", controllerName, local.Namespace, local.Name)
			err = rec.DevopsClient.DevopsV1alpha1().ImageRepositories(local.Namespace).Delete(local.Name, nil)
			if err != nil {
				glog.Errorf("[%s] error delete ImageRepository %s/%s: err %#v", controllerName, local.Namespace, local.Name, err)
			}
		} else if ok && currentRepo != nil {
			if local.Annotations == nil {
				local.Annotations = make(map[string]string)
			}
			if endpoint != local.Annotations[v1alpha1.AnnotationsImageRegistryEndpoint] {
				glog.V(6).Infof("[%s] will update ImageRepository %s/%s becouse endpoint changed", controllerName, local.Namespace, local.Name)

				local.Annotations[v1alpha1.AnnotationsImageRegistryEndpoint] = endpoint
				_, err = rec.DevopsClient.DevopsV1alpha1().ImageRepositories(local.Namespace).Update(&local)
				if err != nil {
					glog.Errorf("[%s] error update ImageRepository %s/%s: err %#v", controllerName, local.Namespace, local.Name, err)
				}
			}
		}
	}
	return
}

// NewImageRepoCondition generate image repository condition in binding
func (rec *Reconciler) NewImageRepoCondition(namespace, name string) v1alpha1.BindingCondition {
	metaTime := metav1.NewTime(time.Now())
	return v1alpha1.BindingCondition{
		Type:        v1alpha1.JenkinsBindingStatusTypeImageRepository,
		Name:        name,
		Namespace:   namespace,
		LastAttempt: &metaTime,
		Owner:       syncRepoOwner,
		Status:      v1alpha1.JenkinsBindingStatusConditionStatusReady,
	}
}

// NewImageRegistryConditioner checks registry as a condition
func (rec *Reconciler) NewImageRegistryConditioner(name string, devopsClient clientset.Interface) *generic.DevOpsToolConditioner {
	return generic.NewDevOpsToolConditioner(
		name,
		func(name string) (pkgruntime.Object, error) {
			return devopsClient.DevopsV1alpha1().ImageRegistries().Get(name, metav1.GetOptions{ResourceVersion: "0"})
		},
		func(obj pkgruntime.Object, err error) v1alpha1.BindingCondition {
			var toolConditions []v1alpha1.BindingCondition
			if obj != nil {
				registry, ok := obj.(*v1alpha1.ImageRegistry)
				if ok {
					toolConditions = registry.Status.Conditions
				}
			}
			return generic.ConvertToolConditionsToCondition(name, toolConditions, err)
		},
	)
}

func generateRepo(repo string, binding *v1alpha1.ImageRegistryBinding, endpoint string) (newRepo *v1alpha1.ImageRepository) {
	registryName := binding.GetLabels()[v1alpha1.LabelImageRegistry]
	name := registryName + "/" + repo
	names := strings.Split(name, "/")
	repoName := strings.Join(names, "-")

	newRepo = &v1alpha1.ImageRepository{
		TypeMeta: metav1.TypeMeta{
			Kind:       v1alpha1.TypeImageRepository,
			APIVersion: v1alpha1.APIVersionV1Alpha1,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      repoName,
			Namespace: binding.GetNamespace(),
			Annotations: map[string]string{
				v1alpha1.AnnotationsImageRegistryEndpoint: endpoint,
			},
		},
		Spec: v1alpha1.ImageRepositorySpec{
			ImageRegistry:        v1alpha1.LocalObjectReference{Name: registryName},
			ImageRegistryBinding: v1alpha1.LocalObjectReference{Name: binding.GetName()},
			Image:                repo,
		},
	}
	generic.AddOwner(newRepo, binding, groupVersionKind)
	return
}

func getRepoFromPath(remoteRepos, repoPaths []string) (repos []string) {
	glog.V(7).Infof("remoteRepos: %v, repoPaths: %v", remoteRepos, repoPaths)
	repoMap := make(map[string]struct{})
	for _, repoPath := range repoPaths {
		if repoPath == "/" {
			repos = remoteRepos
			return
		}
		for _, remoteRepo := range remoteRepos {
			repoTrim := strings.TrimPrefix(remoteRepo, repoPath)
			if repoTrim == "" || strings.HasPrefix(repoTrim, "/") {
				repoMap[remoteRepo] = struct{}{}
			}
		}
	}
	for key := range repoMap {
		repos = append(repos, key)
	}
	return
}

func (rec *Reconciler) getRegistryEndpoint(binding v1alpha1.ImageRegistryBinding) (endpoint string, err error) {

	imageRegistry, err := rec.DevopsClient.DevopsV1alpha1().ImageRegistries().Get(binding.Spec.ImageRegistry.Name, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return "", err
	}

	if imageRegistry.GetType() != v1alpha1.RegistryTypeDockerHub && imageRegistry.GetType() != v1alpha1.RegistryTypeAlauda {
		return util.GetHostInUrl(imageRegistry.Spec.HTTP.Host), nil
	}

	if imageRegistry.Spec.Data["endpoint"] != "" {
		return imageRegistry.Spec.Data["endpoint"], nil
	}

	var registryEndpoint string
	if imageRegistry.GetType() == v1alpha1.RegistryTypeDockerHub {
		registryEndpoint = v1alpha1.DockerHubRegistry
	}
	return registryEndpoint, nil
}
