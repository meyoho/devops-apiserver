package imageregistrybinding_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/imageregistrybinding"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestImageRegistryBindingController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("imageregistrybinding.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/imageregistrybinding", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"imageregistrybinding-controller",
		imageregistrybinding.Add,
		&source.Kind{Type: &v1alpha1.ImageRegistryBinding{}},
		&handler.EnqueueRequestForObject{},
		&predicate.PhaseTTLPredicate{TTL: v1alpha1.TTLServiceCheck, Type: v1alpha1.TypeImageRegistryBinding},
	),
)

var _ = Describe("AddDockerSecret",
	testtools.GenControllerSetupTest(
		reconciler.DockerSecretSyncControllerName,
		imageregistrybinding.AddDockerSecret,
		&source.Kind{Type: &v1alpha1.ImageRegistryBinding{}},
		&handler.EnqueueRequestForObject{},
		&predicate.PhaseTTLPredicate{TTL: v1alpha1.TTLServiceCheck, Type: v1alpha1.TypeImageRegistryBinding},
	),
)
