package imageregistrybinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"

	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

// AddDockerSecret injects the manager so to inject the controller in the manager
func AddDockerSecret(mgr manager.Manager) error {
	return addDockerSecret(mgr, reconciler.NewDockerSecretSyncReconcilerByManager(mgr))
}

func addDockerSecret(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(reconciler.DockerSecretSyncControllerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}
	// we sync some oauth2 secrets
	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.ImageRegistryBinding{}}, &handler.EnqueueRequestForObject{},
		predicate.ConditionTTL(reconciler.DockerSecretSyncControllerName, v1alpha1.TTLDockerSecretSync, reconciler.DockerSecretSyncControllerName),
	)
	return err
}
