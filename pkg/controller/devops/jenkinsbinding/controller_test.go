package jenkinsbinding_test

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/jenkinsbinding"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"

	// . "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

var _ = Describe("Reconciler.Reconcile", testtools.GenBindingControllerTest(
	"jenkinsbindings",
	"jenkinsbinding",
	"default",
	jenkinsbinding.NewReconciler,
	[]runtime.Object{
		testtools.GetJenkins("jenkins", "http://invalid.host.com", "secret", "default"),
		testtools.GetJenkinsBinding("jenkinsbinding", "default", "jenkins", "secret", "default"),
	},
	[]runtime.Object{
		testtools.GetSecret(
			"secret",
			"default",
			corev1.SecretTypeBasicAuth,
			map[string][]byte{
				corev1.BasicAuthUsernameKey: []byte("username"),
				corev1.BasicAuthPasswordKey: []byte("password"),
			},
		),
	},
))
