package jenkinsbinding_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/jenkinsbinding"
	"alauda.io/devops-apiserver/pkg/controller/testtools"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestJenkinsBindingController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("jenkinsbinding.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/jenkinsbinding", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"jenkins-binding-controller",
		jenkinsbinding.Add,
		&source.Kind{Type: &v1alpha1.JenkinsBinding{}},
		&handler.EnqueueRequestForObject{},
	),
)
