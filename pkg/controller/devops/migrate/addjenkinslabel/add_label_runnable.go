package addjenkinslabel

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"context"
	"errors"
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const RunnableName = "add_label_runnable"

// Add injects the manager so to inject the controller in the manager
func AddRunnable(mgr manager.Manager) error {
	return mgr.Add(NewRunnable(mgr.GetClient()))
}

func NewRunnable(cacheClient client.Client) AddLabelRunnable {
	return AddLabelRunnable{cacheClient: cacheClient}
}

type AddLabelRunnable struct {
	cacheClient client.Client
}

func (a AddLabelRunnable) Start(stop <-chan struct{}) error {
	klog.V(5).Infof("[%s] Starting to migrate old resources to add label 'jenkins' to them", RunnableName)

	a.addLabelToResources()

	klog.V(5).Infof("Completed migrating resource")
	<-stop
	return nil
}

func (a AddLabelRunnable) addLabelToResources() {

	jenkinsBindings := &v1alpha1.JenkinsBindingList{}
	err := a.cacheClient.List(context.TODO(), &client.ListOptions{}, jenkinsBindings)
	if err != nil {
		klog.Errorf("[%s] Failed to list JenkinsBindings, reason %#v, stop migrating", RunnableName, err)
	}
	jenkinsBindingToJenkinsMap := make(map[string]string)
	klog.V(5).Infof("[%s] Starting to migrate JenkinsBindings", RunnableName)
	for _, jenkinsBinding := range jenkinsBindings.Items {
		jenkinsBindingToJenkinsMap[namespaceAndNameToKey(jenkinsBinding.Namespace, jenkinsBinding.Name)] = jenkinsBinding.Spec.Jenkins.Name

		err = a.addLabelToResource(&jenkinsBinding, v1alpha1.LabelJenkins, jenkinsBinding.Spec.Jenkins.Name)
		if err != nil {
			klog.Errorf("[%s] Failed to migrate JenkinsBinding '%s/%s', reason: %#v", RunnableName, jenkinsBinding.Namespace, jenkinsBinding.Name, err)
			return
		}
	}

	klog.V(5).Infof("[%s] Starting to migrate PipelineConfigs", RunnableName)
	pipelineConfigs := &v1alpha1.PipelineConfigList{}
	err = a.cacheClient.List(context.TODO(), &client.ListOptions{}, pipelineConfigs)
	if err != nil {
		klog.Errorf("[%s] Failed to list PipelineConfig, reason %#v, skip migrating PipelineConfigs", RunnableName, err)
	}
	for _, pipelineConfig := range pipelineConfigs.Items {
		jenkinsBindingKey := namespaceAndNameToKey(pipelineConfig.Namespace, pipelineConfig.Spec.JenkinsBinding.Name)
		jenkinsName := jenkinsBindingToJenkinsMap[jenkinsBindingKey]

		err := a.addLabelToResource(&pipelineConfig, v1alpha1.LabelJenkins, jenkinsName)
		// skip if cannot update resource
		if err != nil {
			klog.Errorf("[%s] Failed to migrate PipelineConfig '%s/%s', will skip it, reason: %#v", RunnableName, pipelineConfig.Namespace, pipelineConfig.Name, err)
		}
	}

	klog.V(5).Infof("[%s] Starting to migrate Pipelines", RunnableName)
	pipelines := &v1alpha1.PipelineList{}
	err = a.cacheClient.List(context.TODO(), &client.ListOptions{}, pipelines)
	if err != nil {
		klog.Errorf("[%s] Failed to list Pipelines, reason %#v, stop migrating", RunnableName, err)
		return
	}
	for _, pipeline := range pipelines.Items {
		jenkinsBindingKey := namespaceAndNameToKey(pipeline.Namespace, pipeline.Spec.JenkinsBinding.Name)
		jenkinsName := jenkinsBindingToJenkinsMap[jenkinsBindingKey]

		err := a.addLabelToResource(&pipeline, v1alpha1.LabelJenkins, jenkinsName)
		// skip if cannot update resource
		if err != nil {
			klog.Errorf("[%s] Failed to migrate Pipeline '%s/%s', will skip it, reason: %#v", RunnableName, pipeline.Namespace, pipeline.Name, err)
		}
	}
}

func (a AddLabelRunnable) addLabelToResource(resource runtime.Object, key, value string) error {
	obj, ok := resource.(metav1.Object)
	if !ok {
		return errors.New(fmt.Sprintf("failed to add label %s=%s to kind %#v", key, value, obj.GetNamespace()))
	}

	labels := obj.GetLabels()
	if labels == nil {
		labels = make(map[string]string)
		obj.SetLabels(labels)
	} else if val := labels[key]; val == value {
		// won't update object if already has same label
		return nil
	}

	labels[key] = value
	return a.cacheClient.Update(context.TODO(), resource)
}

func namespaceAndNameToKey(namespace, name string) string {
	// '#' is not a valid kubernetes resource name character so that we can use it to ensure the generated key is unique
	return fmt.Sprintf("%s#%s", namespace, name)
}
