package addjenkinslabel_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/migrate/addjenkinslabel"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var _ = Describe("Runnable.Start", func() {
	var (
		runnable    addjenkinslabel.AddLabelRunnable
		cacheClient client.Client

		stop chan struct{}
		err  error
	)

	BeforeEach(func() {
		stop = make(chan struct{})
		cacheClient = testtools.NewFakeClient()
	})

	JustBeforeEach(func() {
		runnable = addjenkinslabel.NewRunnable(cacheClient)
		go func() {
			stop <- struct{}{}
		}()

		err = runnable.Start(stop)
		Expect(err).To(BeNil())
	})

	Context("Resources don't have label", func() {
		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(
				generateJenkinsBinding("default", "jenkinsBinding-1", "jenkins"),
				generatePipelineConfig("default", "pipelineConfig-1", "jenkinsBinding-1"),
				generatePipeline("default", "pipeline-1", "jenkinsBinding-1"),
				generatePipelineConfig("default", "pipelineConfig-2", "jenkinsBinding-2"))
		})

		It("Should add label to resources if has JenkinsBinding", func() {
			jenkinsBinding := &v1alpha1.JenkinsBinding{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "default", Name: "jenkinsBinding-1"}, jenkinsBinding)
			Expect(err).To(BeNil())

			Expect(jenkinsBinding.GetLabels()[v1alpha1.LabelJenkins]).To(Equal("jenkins"))

			pipelineConfig := &v1alpha1.PipelineConfig{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "default", Name: "pipelineConfig-1"}, pipelineConfig)
			Expect(err).To(BeNil())

			Expect(pipelineConfig.GetLabels()[v1alpha1.LabelJenkins]).To(Equal("jenkins"))

			pipeline := &v1alpha1.Pipeline{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "default", Name: "pipeline-1"}, pipeline)
			Expect(err).To(BeNil())

			Expect(pipeline.GetLabels()[v1alpha1.LabelJenkins]).To(Equal("jenkins"))
		})

		It("Should not add label if JenkinsBinding not exists", func() {
			pipelineConfig := &v1alpha1.PipelineConfig{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "default", Name: "pipelineConfig-2"}, pipelineConfig)
			Expect(err).To(BeNil())

			Expect(pipelineConfig.GetLabels()[v1alpha1.LabelJenkins]).To(Equal(""))
		})
	})

	Context("Multiple Jenkins", func() {

		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(
				generateJenkinsBinding("default", "jenkinsBinding-1", "jenkins-1"),
				generateJenkinsBinding("default", "jenkinsBinding-2", "jenkins-2"),
				generatePipelineConfig("default", "pipelineConfig-1", "jenkinsBinding-1"),
				generatePipelineConfig("default", "pipelineConfig-2", "jenkinsBinding-2"))
		})

		It("Should add correct jenkins label", func() {
			jenkinsBinding := &v1alpha1.JenkinsBinding{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "default", Name: "jenkinsBinding-1"}, jenkinsBinding)
			Expect(err).To(BeNil())

			Expect(jenkinsBinding.GetLabels()[v1alpha1.LabelJenkins]).To(Equal("jenkins-1"))

			jenkinsBinding = &v1alpha1.JenkinsBinding{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "default", Name: "jenkinsBinding-2"}, jenkinsBinding)
			Expect(err).To(BeNil())

			Expect(jenkinsBinding.GetLabels()[v1alpha1.LabelJenkins]).To(Equal("jenkins-2"))

			pipelineConfig := &v1alpha1.PipelineConfig{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "default", Name: "pipelineConfig-1"}, pipelineConfig)
			Expect(err).To(BeNil())

			Expect(pipelineConfig.GetLabels()[v1alpha1.LabelJenkins]).To(Equal("jenkins-1"))

			pipelineConfig = &v1alpha1.PipelineConfig{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "default", Name: "pipelineConfig-2"}, pipelineConfig)
			Expect(err).To(BeNil())

			Expect(pipelineConfig.GetLabels()[v1alpha1.LabelJenkins]).To(Equal("jenkins-2"))
		})
	})

})

func generateJenkinsBinding(namespace, name, jenkins string) *v1alpha1.JenkinsBinding {
	return &v1alpha1.JenkinsBinding{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: namespace,
			Name:      name,
		},

		Spec: v1alpha1.JenkinsBindingSpec{
			Jenkins: v1alpha1.JenkinsInstance{
				Name: jenkins,
			},
		},
	}
}

func generatePipelineConfig(namespace, name, jenkinsBinding string) *v1alpha1.PipelineConfig {
	return &v1alpha1.PipelineConfig{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: namespace,
			Name:      name,
		},

		Spec: v1alpha1.PipelineConfigSpec{
			JenkinsBinding: v1alpha1.LocalObjectReference{
				Name: jenkinsBinding,
			},
		},
	}
}

func generatePipeline(namespace, name, jenkinsBinding string) *v1alpha1.Pipeline {
	return &v1alpha1.Pipeline{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: namespace,
			Name:      name,
		},

		Spec: v1alpha1.PipelineSpec{
			JenkinsBinding: v1alpha1.LocalObjectReference{
				Name: jenkinsBinding,
			},
		},
	}
}
