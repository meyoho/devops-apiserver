package addjenkinslabel_test

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/migrate/addjenkinslabel"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	devopsctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"testing"
)

func TestAddLabelRunnable(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("add_label_runnable.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/migrate/addjenkinslabel", []Reporter{junitReporter})
}

var _ = Describe("AddRunnable", testtools.GenRunnableSetup(func(mgr *devopsctrlmock.MockManager) func() {
	return func() {
		cacheClient := testtools.NewFakeClient()

		// first add expectations here
		// will get our kubeclient from manager
		mgr.EXPECT().GetClient().Return(cacheClient)
		// initiates a new runnable that we cannot get
		mgr.EXPECT().Add(gomock.Any()).Return(nil)

		// invoke the method here finaly
		err := addjenkinslabel.AddRunnable(mgr)

		Expect(err).To(BeNil())
	}
}))
