package pipelinetemplatesync

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"sigs.k8s.io/controller-runtime/pkg/handler"

	devopspredicate "alauda.io/devops-apiserver/pkg/controller/predicate"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName string = "pipelinetemplatesync-controller"
)

// AddTemplateSync injects the manager so to inject the controller in the manager
func AddTemplateSync(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

// NewReconcilerByManager returns a new reconcile.Reconciler
func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	config := mgr.GetExtraConfig()
	rec := NewReconciler(
		controllerName,
		config.CredentialsNamespace,
		v1alpha1.TypePipelineTemplateSync,
		mgr.GetKubeClient(), mgr.GetDevOpsClient(),
		mgr.GetAnnotationProvider(),
	)
	return rec
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
		// only syncs one namespace at a time
		MaxConcurrentReconciles: 1,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}
	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.PipelineTemplateSync{}}, &handler.EnqueueRequestForObject{},
		devopspredicate.PhasePredicate{Type: controllerName, Phase: string(v1alpha1.PipelineTemplateSyncPhasePending)},
	)
	return
}
