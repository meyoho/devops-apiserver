package pipelinetemplatesync_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/pipelinetemplatesync"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"testing"

	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	devopspredicate "alauda.io/devops-apiserver/pkg/controller/predicate"
	devopsctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestPipelineTemplateSyncController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("pipelinetemplatesync.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/pipelinetemplatesync", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"pipelinetemplatesync-controller",
		pipelinetemplatesync.AddTemplateSync,
		&source.Kind{Type: &v1alpha1.PipelineTemplateSync{}},
		&handler.EnqueueRequestForObject{},
		devopspredicate.PhasePredicate{Type: "pipelinetemplatesync-controller", Phase: string(v1alpha1.PipelineTemplateSyncPhasePending)},
	),
)

var _ = Describe("AddRunnable", testtools.GenRunnableSetup(func(mgr *devopsctrlmock.MockManager) func() {
	var (
		systemNamespace      = "alauda-system"
		credentialsNamespace = "global-credentials"
		extraConfig          = manager.ExtraConfig{
			SystemNamespace:      systemNamespace,
			CredentialsNamespace: credentialsNamespace,
			AnnotationProvider:   v1alpha1.NewAnnotationProvider(v1alpha1.UsedBaseDomain),
		}
	)
	return func() {
		k8sClient := k8sfake.NewSimpleClientset()
		devopsClient := clientset.NewSimpleClientset()
		// first add expectations here
		// will get our kubeclient from manager
		mgr.EXPECT().GetKubeClient().Return(k8sClient)
		mgr.EXPECT().GetDevOpsClient().Return(devopsClient)
		// initiates a new runnable that we cannot get
		mgr.EXPECT().Add(gomock.Any()).Return(nil)
		mgr.EXPECT().GetExtraConfig().Return(extraConfig)
		mgr.EXPECT().GetAnnotationProvider().Return(extraConfig.AnnotationProvider).AnyTimes()
		// invoke the method here finaly
		err := pipelinetemplatesync.AddClusterTemplateSyncRunnable(mgr)

		Expect(err).To(BeNil())
	}
}))
