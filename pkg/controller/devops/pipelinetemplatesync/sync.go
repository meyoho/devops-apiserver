package pipelinetemplatesync

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"fmt"

	"time"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	"github.com/blang/semver"
	"github.com/ghodss/yaml"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/util"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
)

// NewSync will sync templates into k8s
// it will sync template according by information of PipelineTemplateSync
func NewSync(owner string, client kubernetes.Interface, devopsClient clientset.Interface, sync v1alpha1.PipelineTemplateSyncInterface, credentialsNamespace string, provider v1alpha1.AnnotationProvider) SyncerInterface {
	return &syncer{
		Owner:                owner,
		Client:               client,
		DevOpsClient:         clientset.NewExpansion(devopsClient),
		Sync:                 sync,
		credentialsNamespace: credentialsNamespace,
		annotationProvider:   provider,
	}
}

var (
	opAdd     operation = "Add"
	opUpdate  operation = "Update"
	opSkip    operation = "Skip"
	opDelete  operation = "Delete"
	opUnKnown operation = "UnKnown"
)

type operation string

// SyncerInterface will sync templates into k8s
type SyncerInterface interface {
	SyncTemplates() error
}

type syncer struct {
	Owner        string
	Client       kubernetes.Interface
	DevOpsClient clientset.InterfaceExpansion

	Sync v1alpha1.PipelineTemplateSyncInterface

	// taskTemplatePathMap will save name of task template map to path
	taskTemplatePathMap map[string]string
	// pipelineTemplatePathMap will save name of pipeline template map to path
	pipelineTemplatePathMap map[string]string

	mockGetFiles func() (content map[string][]byte, workingVersion string, err error)

	credentialsNamespace string

	annotationProvider v1alpha1.AnnotationProvider
}

func (s *syncer) SyncTemplates() error {
	glog.Infof("[%s] Begin Sync Templates %s...", s.Owner, s)
	begin := time.Now()
	defer func() {
		glog.Infof("[%s] Ended Sync Templates %s elapsed %fs", s.Owner, s, time.Now().Sub(begin).Seconds())
	}()

	if s.Sync == nil {
		return fmt.Errorf("syncer.Sync should not be nil, you should pass a specifial Sync resource in k8s")
	}

	err := s.updateSyncStatusToStart(begin)
	if err != nil {
		// acquire to retry
		s.updateSyncStatusToEnd("", nil, err)
		return err
	}

	files, workingVersion, err := s.getFiles()
	if err != nil {
		glog.Errorf("[%s] Get Template Files  error:%s", s.Owner, err.Error())
		// update sync status to false
		s.updateSyncStatusToEnd(workingVersion, nil, err)
		return err
	}

	// add , update, delete all template according diff with template that already in k8s
	conditions, err := s.syncTemplates(files)

	// update sync conditions and status
	s.updateSyncStatusToEnd(workingVersion, conditions, err)

	// set all related pipeline config's template version annotation
	if err != nil {
		return err
	}

	s.updateRelatedPipelineConfig(conditions)
	return nil
}

// updatePipelineConfig it is better to do reconcile in controller
func (s *syncer) updateRelatedPipelineConfig(conds []v1alpha1.PipelineTemplateSyncCondition) {

	for _, cond := range conds {
		if (cond.Type == v1alpha1.TypePipelineTemplate || cond.Type == v1alpha1.TypeClusterPipelineTemplate) && cond.Status == v1alpha1.SyncStatusSuccess {

			selector, err := templateLabelSelector(cond.Type, cond.Name)
			if err != nil {
				glog.Errorf("[%s] transfer template label selector error:%s, kind:%s, name:%s", s.Owner, err.Error(), cond.Type, cond.Name)
				continue
			}

			// Only Creating or Updating success will set latest template version onto pipelineconfig annotations
			pipelineConfigList, err := s.DevOpsClient.DevopsV1alpha1().PipelineConfigs("").
				List(metav1.ListOptions{LabelSelector: selector})
			if err != nil {
				glog.Errorf("[%s] find pipelineconfigs by template error %v", s.Owner, err)
				continue
			}

			for _, config := range pipelineConfigList.Items {
				s.setLatestVersionToPipelineConfig(config, cond.Version)
			}
		}
	}
	return
}

// templateLabelSelector return template selector on pipelineconfig
func templateLabelSelector(templateKind, templateName string) (string, error) {
	labelSelector := &metav1.LabelSelector{
		MatchLabels: map[string]string{
			v1alpha1.LabelTemplateKind: templateKind,
			v1alpha1.LabelTemplateName: templateName,
		},
	}
	selector, err := metav1.LabelSelectorAsSelector(labelSelector)
	if err != nil {
		return "", err
	}
	return selector.String(), nil
}

func (s *syncer) setLatestVersionToPipelineConfig(config v1alpha1.PipelineConfig, version string) error {
	copy := config.DeepCopy()
	if copy.GetAnnotations() == nil {
		copy.SetAnnotations(map[string]string{})
	}

	annotations := copy.GetAnnotations()
	annotations[s.annotationProvider.AnnotationsTemplateLatestVersion()] = version
	copy.SetAnnotations(annotations)

	_, err := s.DevOpsClient.DevopsV1alpha1().PipelineConfigs(config.Namespace).Update(copy)
	glog.Infof("[%s] Annotated PipelineConfig %s/%s template latest version to %s, err: %#v", s.Owner, config.Namespace, config.Name, version, err)
	return err
}

func (s *syncer) updateSyncStatusToStart(begin time.Time) error {

	if s.Sync.GetStatus() == nil {
		status := &v1alpha1.PipelineTemplateSyncStatus{}
		s.Sync.SetStatus(status)
	}

	copy := s.Sync.DeepCopyObject().(v1alpha1.PipelineTemplateSyncInterface)
	copy.GetStatus().StartTime = metav1.Time{Time: begin}
	copy.GetStatus().Phase = v1alpha1.PipelineTemplateSyncPhaseSyncing

	err := s.updatePipelineTemplateSyncInterface(copy)
	if err != nil {
		glog.Infof("[%s] Update %s %#v", s.Owner, s.Sync.GetKind(), copy)
		glog.Errorf("[%s] Error to Update %s %s/%s status to syncing, error: %s", s.Owner, s.Sync.GetKind(), s.Sync.GetNamespace(), s.Sync.GetName(), err.Error())
	}

	return err
}

func (s *syncer) updatePipelineTemplateSyncInterface(syncInter v1alpha1.PipelineTemplateSyncInterface) error {
	if syncInter.GetKind() == v1alpha1.TypePipelineTemplateSync {
		sync := syncInter.(*v1alpha1.PipelineTemplateSync)
		_, err := s.DevOpsClient.DevopsV1alpha1().PipelineTemplateSyncs(s.Sync.GetNamespace()).Update(sync)
		return err
	}

	if syncInter.GetKind() == v1alpha1.TypeClusterPipelineTemplateSync {
		sync := syncInter.(*v1alpha1.ClusterPipelineTemplateSync)
		_, err := s.DevOpsClient.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Update(sync)
		return err
	}

	return fmt.Errorf("Error to update PipelineTemplateSync stauts, Kind of PipelineTemplateSync '%s' is unknow", s.Sync.GetKind())
}

func (s *syncer) getPipelineTemplateSyncInterface() (sync v1alpha1.PipelineTemplateSyncInterface, err error) {
	if s.Sync.GetKind() != v1alpha1.TypePipelineTemplateSync && s.Sync.GetKind() != v1alpha1.TypeClusterPipelineTemplateSync {
		return nil, fmt.Errorf("Error to update PipelineTemplateSync stauts to end, Kind of PipelineTemplateSync '%s' is unknow", s.Sync.GetKind())
	}

	if s.Sync.GetKind() == v1alpha1.TypePipelineTemplateSync {
		sync, err = s.DevOpsClient.DevopsV1alpha1().PipelineTemplateSyncs(s.Sync.GetNamespace()).Get(s.Sync.GetName(), v1alpha1.GetOptions())
	}
	if s.Sync.GetKind() == v1alpha1.TypeClusterPipelineTemplateSync {
		sync, err = s.DevOpsClient.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Get(s.Sync.GetName(), v1alpha1.GetOptions())
	}

	return sync, err
}

func (s *syncer) updateSyncStatusToEnd(workingVersion string, conds []v1alpha1.PipelineTemplateSyncCondition, err error) error {

	syncInter, er := s.getPipelineTemplateSyncInterface()
	if er != nil {
		return er
	}
	copy := syncInter.DeepCopyObject().(v1alpha1.PipelineTemplateSyncInterface)

	if copy.GetStatus() == nil {
		copy.SetStatus(&v1alpha1.PipelineTemplateSyncStatus{})
	}
	copy.GetStatus().CommitID = workingVersion
	copy.GetStatus().EndTime = metav1.Now()
	copy.GetStatus().Phase = v1alpha1.PipelineTemplateSyncPhaseReady
	copy.GetStatus().Message = "Success"
	copy.GetStatus().Error = ""
	if err != nil {
		copy.GetStatus().Message = "Failed"
		copy.GetStatus().Error = err.Error()
		copy.GetStatus().Phase = v1alpha1.PipelineTemplateSyncPhaseError
	}
	copy.GetStatus().Conditions = conds

	err = s.updatePipelineTemplateSyncInterface(copy)
	if err != nil {
		glog.Infof("[%s] Update PipelineTemplateSync %#v", s.Owner, copy)
		glog.Errorf("[%s] Update PipelineTemplateSync %s/%s status to end error: %s", s.Owner, s.Sync.GetNamespace(), s.Sync.GetName(), err.Error())
	}

	return err
}

func (s *syncer) syncTemplates(files map[string][]byte) ([]v1alpha1.PipelineTemplateSyncCondition, error) {

	// unmarshall files to template
	templates, taskTemplates, templatePathMap, taskTemplatePathMap, err := s.unmarshall(files)
	if err != nil {
		glog.Errorf("[%s] Error to unmarshall files to golang struct, errors: %s", s.Owner, err.Error())
		return nil, err
	}
	s.pipelineTemplatePathMap, s.taskTemplatePathMap = templatePathMap, taskTemplatePathMap

	// change kind and add label
	s.preProcess(&templates, &taskTemplates)

	// DIFF templates with templates in k8s
	addTemplates, updateTemplates, nochangeTemplates, deleteTemplates, err := s.diffTemplate(templates)

	if err != nil {
		return nil, err
	}
	addTaskTemplates, updateTaskTemplates, nochangeTaskTemplates, deleteTaskTemplates, err := s.diffTaskTemplate(taskTemplates)
	if err != nil {
		return nil, err
	}

	conds := make([]v1alpha1.PipelineTemplateSyncCondition, 0, len(templates)+len(taskTemplates))

	// NO CHANGE templates and task templates
	skipConds := s.batchTaskTemplateConds(opSkip, nil, nochangeTaskTemplates...)
	s.debugPrintTaskTemplate(opSkip, nochangeTaskTemplates)
	conds = append(conds, skipConds...)
	skipConds = s.batchPipelineTemplateConds(opSkip, nil, nochangeTemplates...)
	s.debugPrintPipelineTemplate(opSkip, nochangeTemplates)
	conds = append(conds, skipConds...)

	// DELETE task templates
	delConds := s.deleteTaskTemplates(deleteTaskTemplates)
	conds = append(conds, delConds...)

	// ADD OR UPDATE task template
	// will createOrUpdate task templates firstly , if it is fail, will break the flow
	taskTemplateSyncConds := s.createOrUpdateTaskTemplates(addTaskTemplates, updateTaskTemplates)
	conds = append(conds, taskTemplateSyncConds...)
	if condsContainsError(taskTemplateSyncConds) {
		err = fmt.Errorf("task templates should be synced successed firstly")
		conds = append(conds, s.batchPipelineTemplateConds(opAdd, err, addTemplates...)...)
		conds = append(conds, s.batchPipelineTemplateConds(opUpdate, err, updateTemplates...)...)
		conds = append(conds, s.batchPipelineTemplateConds(opDelete, err, updateTemplates...)...)
		return conds, nil
	}

	// ADD OR UPDATE pipeline template
	pipeTemplateSyncConds := s.createOrUpdateTemplates(addTemplates, updateTemplates)
	conds = append(conds, pipeTemplateSyncConds...)

	// DELETE pipeline templates
	delConds = s.deleteTemplates(deleteTemplates)
	conds = append(conds, delConds...)

	return conds, nil
}

func (s *syncer) batchTaskTemplateConds(op operation, err error, taskTemplates ...v1alpha1.PipelineTaskTemplateInterface) []v1alpha1.PipelineTemplateSyncCondition {
	conds := make([]v1alpha1.PipelineTemplateSyncCondition, 0, len(taskTemplates))

	for _, item := range taskTemplates {
		conds = append(conds, s.asCondition(op, err, s.taskTemplatePathMap[item.GetName()], item.GetKind(), nil, item.GetObjectMeta()))
	}

	return conds
}

func (s *syncer) batchPipelineTemplateConds(op operation, err error, templates ...v1alpha1.PipelineTemplateInterface) []v1alpha1.PipelineTemplateSyncCondition {
	conds := make([]v1alpha1.PipelineTemplateSyncCondition, 0, len(templates))

	for _, item := range templates {
		conds = append(conds, s.asCondition(op, err, s.pipelineTemplatePathMap[item.GetName()], item.GetKind(), nil, item.GetObjectMeta()))
	}

	return conds
}

func condsContainsError(conds []v1alpha1.PipelineTemplateSyncCondition) bool {
	if len(conds) == 0 {
		return false
	}

	for _, cond := range conds {
		if cond.Status == v1alpha1.SyncStatusFailure {
			return true
		}
	}

	return false
}

func (s *syncer) createOrUpdateTaskTemplates(
	add []v1alpha1.PipelineTaskTemplateInterface,
	update []v1alpha1.PipelineTaskTemplateInterface) []v1alpha1.PipelineTemplateSyncCondition {

	conds := make([]v1alpha1.PipelineTemplateSyncCondition, 0, len(add)+len(update))
	for _, item := range add {
		err := s.createTaskTemplate(item)
		conds = append(conds, s.asCondition(opAdd, err, s.taskTemplatePathMap[item.GetName()], item.GetKind(), nil, item))
	}

	for _, item := range update {
		old, err := s.updateTaskTemplate(item, true)
		conds = append(conds, s.asCondition(opUpdate, err, s.taskTemplatePathMap[item.GetName()], item.GetKind(), old, item))
	}

	return conds
}

func (s *syncer) createTaskTemplate(
	add v1alpha1.PipelineTaskTemplateInterface) error {

	if update, target := k8s.GetTargetAnnotation(add.GetAnnotations()); update {
		add.SetAnnotations(target)
	}

	// create redundant pipeline Task template firstly
	err := s.redundantTaskTemplate(add)
	if err != nil {
		return err
	}

	addLatestLabel(add.GetObjectMeta())
	addTaskTemplateNameAnnotation(add, add.GetName())

	_, err = s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Create(add)
	if err == nil {
		glog.Infof("[%s] Created %s, err:%#v", s.Owner, add, err)
		return nil
	}

	if !errors.IsAlreadyExists(err) {
		glog.Errorf("[%s] Created %s, err:%#v", s.Owner, add, err)
		return err
	}

	// IsAlreadyExists
	// we found the resource for some reasons, we should update it
	glog.Infof("[%s] Have try to create %s, But already exists, change to update it", s.Owner, add)
	_, err = s.updateTaskTemplate(add, false)
	return err
}

func (s *syncer) updateTaskTemplate(
	update v1alpha1.PipelineTaskTemplateInterface, redundant bool) (old v1alpha1.PipelineTaskTemplateInterface, err error) {
	old, err = s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Get(update.GetKind(), update.GetNamespace(), update.GetName(), v1alpha1.GetOptions())
	// could not fetch task (old)
	if err != nil {
		glog.Errorf("[%s] get %s error:%s", s.Owner, update, err.Error())
		return
	}

	if redundant {
		// create redundant pipeline Task template firstly
		err = s.redundantTaskTemplate(update)
		if err != nil {
			return nil, err
		}
	}

	addLatestLabel(update.GetObjectMeta())
	addTaskTemplateNameAnnotation(update, update.GetName())

	update.SetUID(old.GetUID())
	update.SetSelfLink(old.GetSelfLink())
	update.SetResourceVersion(old.GetResourceVersion())

	_, err = s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Update(update)
	glog.Infof("[%s] Updated %s, err:%#v", s.Owner, update, err)

	return
}

func (s *syncer) redundantTaskTemplate(taskTemplate v1alpha1.PipelineTaskTemplateInterface) error {

	redundancy, err := s.prepareRedundantTaskTemplate(taskTemplate)
	if err != nil {
		return err
	}

	// save it into k8s
	already, err := s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Get(redundancy.GetKind(), redundancy.GetNamespace(), redundancy.GetName(), metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		if errors.IsNotFound(err) {
			redundancy.SetUID("")
			redundancy.SetSelfLink("")
			redundancy.SetResourceVersion("")
			_, err := s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Create(redundancy)
			glog.Infof("[%s] Created redundancy %s, err:%#v", s.Owner, redundancy, err)
			return err
		}

		glog.Errorf("[%s] Get %s error:%s", redundancy, s.Owner, err.Error())
		return err
	}

	redundancy.GetObjectMeta().SetUID(already.GetObjectMeta().GetUID())
	redundancy.GetObjectMeta().SetSelfLink(already.GetObjectMeta().GetSelfLink())
	redundancy.GetObjectMeta().SetResourceVersion(already.GetObjectMeta().GetResourceVersion())
	already, err = s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Update(redundancy)
	glog.Infof("[%s] Updated redundancy %s, err:%#v", s.Owner, redundancy, err)

	return err
}

func (s *syncer) prepareRedundantTaskTemplate(taskTemplate v1alpha1.PipelineTaskTemplateInterface) (v1alpha1.PipelineTaskTemplateInterface, error) {

	// init redundancy resource
	copy := taskTemplate.DeepCopyObject()
	redundancy := copy.(v1alpha1.PipelineTaskTemplateInterface)

	// append templateName to save original template name
	annotations := redundancy.GetAnnotations()
	if annotations == nil {
		annotations = map[string]string{}
	}
	annotations[v1alpha1.AnnotationsTemplateName] = taskTemplate.GetName()
	redundancy.SetAnnotations(annotations)

	// append templateName to label
	labels := redundancy.GetLabels()
	if labels == nil {
		labels = map[string]string{}
	}
	labels[v1alpha1.LabelTemplateName] = taskTemplate.GetName()

	delete(labels, v1alpha1.LabelTemplateLatest)
	redundancy.SetLabels(labels)

	// change pipeline task template name to versioned name
	name, err := redundancy.GetVersionedName()
	if err != nil {
		return nil, err
	}
	redundancy.SetName(name)

	return redundancy, nil
}

func (s *syncer) createOrUpdateTemplates(
	add []v1alpha1.PipelineTemplateInterface,
	update []v1alpha1.PipelineTemplateInterface) []v1alpha1.PipelineTemplateSyncCondition {

	conds := make([]v1alpha1.PipelineTemplateSyncCondition, 0, len(add)+len(update))

	for _, item := range add {
		err := s.createPipelineTemplate(item)
		conds = append(conds, s.asCondition(opAdd, err, s.pipelineTemplatePathMap[item.GetName()], item.GetKind(), nil, item))
	}

	for _, item := range update {
		err := s.updatePipelineTemplate(item, true)
		conds = append(conds, s.asCondition(opUpdate, err, s.pipelineTemplatePathMap[item.GetName()], item.GetKind(), nil, item))
	}

	return conds
}

// createPipelineTemplate
// 1. will create a versioned pipeline template replication firstly
//   - . change name to versioned name
//   - . change task ref name to latest task template versioned name
// 2. create this pipeline template with alauda.io/latest:true
func (s *syncer) createPipelineTemplate(template v1alpha1.PipelineTemplateInterface) error {
	// create redundant pipeline template firstly
	source := template.GetAnnotations()
	if update, target := k8s.GetTargetAnnotation(source); update {
		template.SetAnnotations(target)
	}

	err := s.redundantPipelineTemplate(template)
	if err != nil {
		return err
	}

	addLatestLabel(template.GetObjectMeta())
	addTemplateNameAnnotation(template, template.GetName())

	_, err = s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Create(template)
	if err == nil {
		glog.Infof("[%s] Created %s, err:%#v", s.Owner, template, err)
		return nil
	}

	if !errors.IsAlreadyExists(err) {
		glog.Errorf("[%s] Created %s, err:%#v", s.Owner, template, err)
		return err
	}

	glog.Infof("[%s] Have try to create %s, But already exists, change to update it", s.Owner, template)
	// IsAlreadyExists
	// we should update it
	err = s.updatePipelineTemplate(template, false)
	return err
}

// updatePipelineTemplate
// 1. will create a versioned pipeline template replication firstly
//   - . change name to versioned name
//   - . change task ref name to latest task template versioned name
// 2. update this pipeline template with latest:true
func (s *syncer) updatePipelineTemplate(template v1alpha1.PipelineTemplateInterface, redundat bool) error {

	source := template.GetAnnotations()
	if update, target := k8s.GetTargetAnnotation(source); update {
		template.SetAnnotations(target)
	}

	old, err := s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Get(template.GetKind(), s.getNamespace(), template.GetName(), v1alpha1.GetOptions())
	if err != nil {
		return err
	}

	// create redundant pipeline template firstly
	if redundat {
		err = s.redundantPipelineTemplate(template)
		if err != nil {
			return err
		}
	}

	addLatestLabel(template.GetObjectMeta())
	addTemplateNameAnnotation(template, template.GetName())

	template.SetUID(old.GetUID())
	template.SetSelfLink(old.GetSelfLink())
	template.SetResourceVersion(old.GetResourceVersion())

	oldAnnotations := old.GetAnnotations()
	if oldAnnotations != nil {
		if hasDeleted, ok := oldAnnotations[devops.PreviousDeletedTimestamp]; ok {
			newAnnotation := template.GetAnnotations()
			newAnnotation[devops.PreviousDeletedTimestamp] = hasDeleted
			template.SetAnnotations(newAnnotation)
		}
	}

	_, err = s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Update(template)
	glog.Infof("[%s] Updated %s, err:%#v", s.Owner, template, err)
	return err
}

//redundantPipelineTemplate will create or update a redundant pipeline template
func (s *syncer) redundantPipelineTemplate(pipelineTemplate v1alpha1.PipelineTemplateInterface) error {
	redundancy, err := s.prepareRedundantPipelineTemplate(pipelineTemplate)
	if err != nil {
		return err
	}

	// save it into k8s
	found, err := s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Get(redundancy.GetKind(),
		redundancy.GetNamespace(), redundancy.GetName(), metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		if errors.IsNotFound(err) {
			redundancy.SetUID("")
			redundancy.SetSelfLink("")
			redundancy.SetResourceVersion("")
			_, err := s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Create(redundancy)
			glog.Infof("[%s] Created redundancy %s, err:%#v", s.Owner, redundancy, err)
			return nil
		}

		glog.Errorf("[%s] Get %s error:%#v", s.Owner, redundancy, err.Error())
		return err
	}

	redundancy.SetUID(found.GetUID())
	redundancy.SetSelfLink(found.GetSelfLink())
	redundancy.SetResourceVersion(found.GetResourceVersion())
	_, err = s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Update(redundancy)
	glog.Infof("[%s] Updated redundancy %s, err:%#v", s.Owner, redundancy, err)

	return err
}

func (s *syncer) prepareRedundantPipelineTemplate(pipelineTemplate v1alpha1.PipelineTemplateInterface) (v1alpha1.PipelineTemplateInterface, error) {

	// init redundancy resource
	copy := pipelineTemplate.DeepCopyObject()
	redundancy := copy.(v1alpha1.PipelineTemplateInterface)

	// append alauda.io/templateName to save original template name
	annotations := redundancy.GetAnnotations()
	if annotations == nil {
		annotations = map[string]string{}
	}
	annotations[v1alpha1.AnnotationsTemplateName] = pipelineTemplate.GetName()
	redundancy.SetAnnotations(annotations)

	// append alauda.io/templateName to label
	labels := redundancy.GetLabels()
	if labels == nil {
		labels = map[string]string{}
	}
	labels[v1alpha1.LabelTemplateName] = pipelineTemplate.GetName()

	delete(labels, v1alpha1.LabelTemplateLatest)
	redundancy.SetLabels(labels)

	// change pipeline template name to versioned name
	name, err := redundancy.GetVersionedName()
	if err != nil {
		return nil, err
	}
	redundancy.SetName(name)

	// change task ref name to versioned task ref name
	spec := redundancy.GetPiplineTempateSpec()
	for i := range spec.Stages {
		stage := &spec.Stages[i]
		for j := range stage.Tasks {
			task := &stage.Tasks[j]

			if s.isClusterScopedResource() && task.Kind == v1alpha1.TypePipelineTaskTemplate {
				err = fmt.Errorf("cannot reference a PipelineTaskTemplate named '%s' in cluster scoped in template of '%s', it may should be type of ClusterPipelineTaskTemplate", task.Name, pipelineTemplate)
				glog.Errorf("[%s] %s", s.Owner, err.Error())
				return nil, err
			}

			taskTemplate, err := s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Get(
				task.Kind, s.getNamespace(), task.Name, metav1.GetOptions{ResourceVersion: "0"})
			if err != nil {
				glog.Errorf("[%s] get task template %s/%s err:%s , it is referenced by %s", s.Owner, task.Kind, task.Name, err.Error(), pipelineTemplate)
				return nil, err
			}
			task.Name, err = taskTemplate.GetVersionedName()
			if task.Display.Zh == "" && task.Display.En == "" {
				task.Display.Zh = taskTemplate.GetName()
				task.Display.En = taskTemplate.GetName()
			}
			if task.ID == "" {
				task.ID = taskTemplate.GetName()
			}
			if err != nil {
				glog.Errorf("[%s] %s cannot get versioned name:%s", s.Owner, taskTemplate, err.Error())
				return nil, err
			}
		}
	}

	return redundancy, nil
}

func (s *syncer) asCondition(op operation, err error, path string, kind string, old metav1.Object, new metav1.Object) v1alpha1.PipelineTemplateSyncCondition {

	condition := v1alpha1.PipelineTemplateSyncCondition{
		LastTransitionTime: metav1.Now(),
		LastUpdateTime:     metav1.Now(),
		Target:             path,
		Name:               new.GetName(),
		Type:               kind,
		Version:            s.getVersion(new),
	}

	condition.Status = v1alpha1.SyncStatusSuccess
	switch op {
	case opSkip:
		condition.Status = v1alpha1.SyncStatusSkip
		condition.PreviousVersion = s.getVersion(new)
		condition.Message = "Skiped"
	case opDelete:
		condition.Status = v1alpha1.SyncStatusDeleted
		condition.PreviousVersion = s.getVersion(new)
		condition.Message = "Deleted"
	case opUnKnown:
		condition.Status = v1alpha1.SyncStatusFailure
	}
	if err != nil {
		condition.Status = v1alpha1.SyncStatusFailure
		condition.Message = fmt.Sprintf("%s Failed: %s", op, err.Error())
	}

	if old != nil {
		condition.PreviousVersion = s.getVersion(old)
	}

	return condition
}

func (s *syncer) deleteTemplates(templates []v1alpha1.PipelineTemplateInterface) (conditions []v1alpha1.PipelineTemplateSyncCondition) {

	conds := make([]v1alpha1.PipelineTemplateSyncCondition, 0, len(templates))
	for _, item := range templates {

		err := s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Delete(item.GetKind(), item.GetNamespace(), item.GetName(), &metav1.DeleteOptions{})
		glog.Infof("[%s] Deleted %s, err:%#v", s.Owner, item, err)

		errs := util.MultiErrors{}
		if s.isClusterScopedResource() {
			errs = s.deleteVersionedClusterPipelineTemplates(item.GetName())
		} else {
			errs = s.deleteVersionedPipelineTemplates(item.GetName())
		}

		if len(errs) == 0 {
			conds = append(conds, s.asCondition(opDelete, nil, s.pipelineTemplatePathMap[item.GetName()], item.GetKind(), item, item))
		} else {
			conds = append(conds, s.asCondition(opDelete, &errs, s.pipelineTemplatePathMap[item.GetName()], item.GetKind(), item, item))
		}
	}
	return conds
}

// deleteVersionedClusterPipelineTemplates will deleete all versioned clusterpipelinetemplate named {templateName}
func (s *syncer) deleteVersionedClusterPipelineTemplates(templateName string) util.MultiErrors {
	errs := util.MultiErrors{}

	label, err := versionedTemplatesLabelSelector(templateName, s.templateSource()) // officialTemplateLabelSelector(item.GetName())
	if err != nil {
		errs = append(errs, err)
	}
	TemplateList, err := s.DevOpsClient.DevopsV1alpha1().ClusterPipelineTemplates().List(metav1.ListOptions{ResourceVersion: "0", LabelSelector: label})
	if err != nil {
		errs = append(errs, err)
	}

	if TemplateList != nil {
		for _, origintemplate := range TemplateList.Items {
			if origintemplate.Annotations[v1alpha1.LabelTemplateName] == templateName {
				err = s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Delete(origintemplate.GetKind(), origintemplate.GetNamespace(), origintemplate.GetName(), &metav1.DeleteOptions{})
				if err != nil {
					errs = append(errs, err)
				}
				glog.Infof("[%s] Deleted %s, err:%#v", s.Owner, origintemplate.String(), err)
			}
		}
	}

	return errs
}

// deleteVersionedPipelineTemplates will delete all versioned pipelinetemplate named {templateName}
func (s *syncer) deleteVersionedPipelineTemplates(templateName string) util.MultiErrors {
	errs := util.MultiErrors{}

	label, err := versionedTemplatesLabelSelector(templateName, s.templateSource())
	if err != nil {
		errs = append(errs, err)
	}

	TemplateList, err := s.DevOpsClient.DevopsV1alpha1().PipelineTemplates(s.getNamespace()).List(metav1.ListOptions{ResourceVersion: "0", LabelSelector: label})
	if err != nil {
		errs = append(errs, err)
	}
	if TemplateList != nil {
		for _, origintemplate := range TemplateList.Items {
			if origintemplate.Annotations[v1alpha1.LabelTemplateName] == templateName {
				err = s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Delete(origintemplate.GetKind(), origintemplate.GetNamespace(), origintemplate.GetName(), &metav1.DeleteOptions{})
				if err != nil {
					errs = append(errs, err)
				}
				glog.Infof("[%s] Deleted %s, err:%#v", s.Owner, origintemplate.String(), err)
			}
		}
	}

	return errs
}

func (s *syncer) deleteTaskTemplates(taskTemplates []v1alpha1.PipelineTaskTemplateInterface) []v1alpha1.PipelineTemplateSyncCondition {

	conds := make([]v1alpha1.PipelineTemplateSyncCondition, 0, len(taskTemplates))
	for _, item := range taskTemplates {
		err := s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Delete(item.GetKind(), item.GetNamespace(), item.GetName(), &metav1.DeleteOptions{})
		if errors.IsNotFound(err) {
			glog.Infof("[%s] %s is not found already, err:%#v", s.Owner, item, err)
			err = nil
		}
		glog.Infof("[%s] Deleted %s, err:%#v", s.Owner, item, err)

		errs := util.MultiErrors{}

		if s.isClusterScopedResource() {
			errs = s.deleteVersionedClusterPipelineTaskTemplates(item.GetName())
		} else {
			errs = s.deleteVersionedPipelineTaskTemplates(item.GetName())
		}

		if len(errs) == 0 {
			conds = append(conds, s.asCondition(opDelete, nil, s.taskTemplatePathMap[item.GetName()], item.GetKind(), item, item))
		} else {
			conds = append(conds, s.asCondition(opDelete, &errs, s.taskTemplatePathMap[item.GetName()], item.GetKind(), item, item))
		}
	}

	return conds
}

// deleteVersionedClusterPipelineTaskTemplates will delete all versioned ClusterPipelineTaskTemplate named {templateName}
func (s *syncer) deleteVersionedClusterPipelineTaskTemplates(templateName string) util.MultiErrors {
	errs := util.MultiErrors{}

	label, err := versionedTemplatesLabelSelector(templateName, s.templateSource())
	if err != nil {
		errs = append(errs, err)
	}

	TemplateTaskList, err := s.DevOpsClient.DevopsV1alpha1().ClusterPipelineTaskTemplates().List(metav1.ListOptions{ResourceVersion: "0", LabelSelector: label})
	if err != nil {
		errs = append(errs, err)
	}

	if TemplateTaskList == nil {
		return errs
	}

	for _, tasktemplate := range TemplateTaskList.Items {
		if tasktemplate.Annotations[v1alpha1.LabelTemplateName] == templateName {
			err = s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Delete(tasktemplate.GetKind(), tasktemplate.GetNamespace(), tasktemplate.GetName(), &metav1.DeleteOptions{})
			if err != nil {
				errs = append(errs, err)
			}
			glog.Infof("[%s] Deleted %s, err:%#v", s.Owner, tasktemplate.String(), err)
		}
	}

	return errs
}

// deleteVersionedPipelineTaskTemplates will delete all versioned PipelineTaskTemplate named {templateName}
func (s *syncer) deleteVersionedPipelineTaskTemplates(templateName string) util.MultiErrors {
	errs := util.MultiErrors{}

	label, err := versionedTemplatesLabelSelector(templateName, s.templateSource())
	if err != nil {
		errs = append(errs, err)
	}
	TemplateTaskList, err := s.DevOpsClient.DevopsV1alpha1().PipelineTaskTemplates(s.getNamespace()).List(metav1.ListOptions{ResourceVersion: "0", LabelSelector: label})
	if err != nil {
		errs = append(errs, err)
	}
	if TemplateTaskList == nil {
		return errs
	}

	for _, tasktemplate := range TemplateTaskList.Items {
		if tasktemplate.Annotations[v1alpha1.LabelTemplateName] == templateName {
			err = s.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Delete(tasktemplate.GetKind(), tasktemplate.GetNamespace(), tasktemplate.GetName(), &metav1.DeleteOptions{})
			if err != nil {
				errs = append(errs, err)
			}
			glog.Infof("[%s] Deleted %s, err:%#v", s.Owner, tasktemplate.String(), err)
		}
	}

	return errs
}

func (s *syncer) getOriginTemplate(templatetype string) (origin []metav1.Object, err error) {

	var original []metav1.Object
	var label string
	var clusterTemplateList *v1alpha1.ClusterPipelineTemplateList
	var TemplateList *v1alpha1.PipelineTemplateList

	if label, err = latestTemplateLabelSelector(s.templateSource()); err != nil {
		return origin, err
	}

	if templatetype == v1alpha1.TypeClusterPipelineTemplate {

		clusterTemplateList, err = s.DevOpsClient.DevopsV1alpha1().ClusterPipelineTemplates().List(metav1.ListOptions{ResourceVersion: "0", LabelSelector: label})
		if err != nil {
			glog.Errorf("[%s] List ClusterPipelienTemplates error %s", s.Owner, err.Error())
			return origin, err
		}
		original = make([]metav1.Object, 0, len(clusterTemplateList.Items))
		for i := range clusterTemplateList.Items {
			item := clusterTemplateList.Items[i]
			version := s.getVersion(&item)
			glog.V(8).Infof("[%s] find clusterpipelinetemplate %s version:%s", s.Owner, &item, version)
			original = append(original, &item)
		}

		return original, nil
	}

	if templatetype == v1alpha1.TypePipelineTemplate {

		TemplateList, err = s.DevOpsClient.DevopsV1alpha1().PipelineTemplates(s.getNamespace()).List(metav1.ListOptions{ResourceVersion: "0", LabelSelector: label})
		if err != nil {
			glog.Errorf("[%s] List PipelienTemplates error %s", s.Owner, err.Error())
			return origin, err
		}
		original = make([]metav1.Object, 0, len(TemplateList.Items))
		for i := range TemplateList.Items {
			item := TemplateList.Items[i]
			version := s.getVersion(&item)
			glog.V(8).Infof("[%s] find pipelinetemplate %s version:%s", s.Owner, &item, version)
			original = append(original, &item)
		}

		return original, nil
	}

	return original, fmt.Errorf("TaskTemplate Kind:%s is unknown, it should be %s or %s", templatetype, v1alpha1.TypeClusterPipelineTaskTemplate, v1alpha1.TypePipelineTaskTemplate)
}

//diffTemplate won't care about versioned resource, just diff latest resource
func (s *syncer) diffTemplate(templates []v1alpha1.PipelineTemplateInterface) (
	add []v1alpha1.PipelineTemplateInterface,
	update []v1alpha1.PipelineTemplateInterface,
	nochange []v1alpha1.PipelineTemplateInterface,
	delete []v1alpha1.PipelineTemplateInterface,
	err error,
) {
	var original []metav1.Object
	// if current is global namespace, we will sync clustertemplates
	if s.isClusterScopedResource() {
		if original, err = s.getOriginTemplate(v1alpha1.TypeClusterPipelineTemplate); err != nil {
			return nil, nil, nil, nil, err
		}
	} else {
		if original, err = s.getOriginTemplate(v1alpha1.TypePipelineTemplate); err != nil {
			return nil, nil, nil, nil, err
		}
	}

	wanted := make([]metav1.Object, 0, len(templates))
	for i := range templates {
		item := templates[i]
		version := s.getVersion(item)
		glog.V(8).Infof("[%s] want %s version:%s", s.Owner, item, version)
		wanted = append(wanted, item)
	}
	glog.V(8).Infof("Template Sync Stragety is %s", s.Sync.GetSpec().Strategy)

	addObjs, updateObjs, noChangeObjs, deleteObjs := util.DiffObject(original, wanted, s.diffChangeFunc())

	return trustedAsTemplate(addObjs), trustedAsTemplate(updateObjs), trustedAsTemplate(noChangeObjs), trustedAsTemplate(deleteObjs), nil
}

// latestTemplateLabelSelector return label selector that select all official or customer latest templates synced from repository or path
func latestTemplateLabelSelector(source string) (string, error) {
	labelseletormap := map[string]string{
		v1alpha1.LabelTemplateSource: source,
		v1alpha1.LabelCreatedMethod:  v1alpha1.LabelRepositorySync,
		v1alpha1.LabelTemplateLatest: "true",
	}

	labelSelector := &metav1.LabelSelector{
		MatchLabels: labelseletormap,
	}

	selector, err := metav1.LabelSelectorAsSelector(labelSelector)
	if err != nil {
		return "", err
	}
	return selector.String(), nil
}

// versionedTemplatesLabelSelector will return selector that used to select all versioned template named templateName
// and the template should also be synced by repository or disk path
func versionedTemplatesLabelSelector(templateName, source string) (string, error) {

	labelseletormap := map[string]string{
		v1alpha1.LabelTemplateSource: source,
		v1alpha1.LabelCreatedMethod:  v1alpha1.LabelRepositorySync,
	}
	if templateName != "" {
		labelseletormap[v1alpha1.LabelTemplateName] = templateName
	}
	labelSelector := &metav1.LabelSelector{
		MatchLabels: labelseletormap,
	}

	selector, err := metav1.LabelSelectorAsSelector(labelSelector)
	if err != nil {
		return "", err
	}
	return selector.String(), nil
}

//diffTaskTemplate won't care about versioned resource, just diff latest resource
func (s *syncer) diffTaskTemplate(taskTemplates []v1alpha1.PipelineTaskTemplateInterface) (
	add []v1alpha1.PipelineTaskTemplateInterface,
	update []v1alpha1.PipelineTaskTemplateInterface,
	nochange []v1alpha1.PipelineTaskTemplateInterface,
	delete []v1alpha1.PipelineTaskTemplateInterface,
	err error,
) {
	var original []metav1.Object
	if s.isClusterScopedResource() {
		if original, err = s.getOriginTaskTemplate(v1alpha1.TypeClusterPipelineTaskTemplate); err != nil {
			return nil, nil, nil, nil, err
		}
	} else {
		if original, err = s.getOriginTaskTemplate(v1alpha1.TypePipelineTaskTemplate); err != nil {
			return nil, nil, nil, nil, err
		}
	}
	wanted := make([]metav1.Object, 0, len(taskTemplates))
	for i := range taskTemplates {
		item := taskTemplates[i]
		version := s.getVersion(item)
		glog.V(8).Infof("[%s] want %s version:%s", s.Owner, item.String(), version)
		wanted = append(wanted, item)
	}

	addObjs, updateObjs, noChangeObjs, deleteObjs := util.DiffObject(original, wanted, s.diffChangeFunc())

	return trustedAsTaskTemplate(addObjs), trustedAsTaskTemplate(updateObjs), trustedAsTaskTemplate(noChangeObjs), trustedAsTaskTemplate(deleteObjs), nil
}

func (s *syncer) getOriginTaskTemplate(templatetype string) (origin []metav1.Object, err error) {

	var original []metav1.Object
	var label string
	var clusterTemplateTaskList *v1alpha1.ClusterPipelineTaskTemplateList
	var TemplateTaskList *v1alpha1.PipelineTaskTemplateList

	if label, err = latestTemplateLabelSelector(s.templateSource()); err != nil {
		return origin, err
	}

	if templatetype == v1alpha1.TypeClusterPipelineTaskTemplate {

		clusterTemplateTaskList, err = s.DevOpsClient.DevopsV1alpha1().ClusterPipelineTaskTemplates().List(metav1.ListOptions{ResourceVersion: "0", LabelSelector: label})
		if err != nil {
			glog.Errorf("[%s] List ClusterPipelienTaskTemplates error %s", s.Owner, err.Error())
			return origin, err
		}
		original = make([]metav1.Object, 0, len(clusterTemplateTaskList.Items))
		for i := range clusterTemplateTaskList.Items {
			item := clusterTemplateTaskList.Items[i]
			version := s.getVersion(&item)
			glog.V(8).Infof("[%s] find clusterpipelinetasktemplate %s version:%s", s.Owner, &item, version)
			original = append(original, &item)
		}

		return original, nil
	}

	if templatetype == v1alpha1.TypePipelineTaskTemplate {

		TemplateTaskList, err = s.DevOpsClient.DevopsV1alpha1().PipelineTaskTemplates(s.getNamespace()).List(metav1.ListOptions{ResourceVersion: "0", LabelSelector: label})
		if err != nil {
			glog.Errorf("[%s] List PipelienTemplates error %s", s.Owner, err.Error())
			return origin, err
		}
		original = make([]metav1.Object, 0, len(TemplateTaskList.Items))
		for i := range TemplateTaskList.Items {
			item := TemplateTaskList.Items[i]
			version := s.getVersion(&item)
			glog.V(8).Infof("[%s] find pipelinetemplate %s version:%s", s.Owner, &item, version)
			original = append(original, &item)
		}

		return original, nil
	}

	return original, fmt.Errorf("Template Kind:%s is unknown, it should be %s or %s", templatetype, v1alpha1.TypeClusterPipelineTaskTemplate, v1alpha1.TypePipelineTaskTemplate)
}

func trustedAsTemplate(objs []metav1.Object) []v1alpha1.PipelineTemplateInterface {
	result := make([]v1alpha1.PipelineTemplateInterface, 0, len(objs))

	for _, obj := range objs {
		result = append(result, obj.(v1alpha1.PipelineTemplateInterface))
	}
	return result
}

func trustedAsTaskTemplate(objs []metav1.Object) []v1alpha1.PipelineTaskTemplateInterface {
	result := make([]v1alpha1.PipelineTaskTemplateInterface, 0, len(objs))

	for _, obj := range objs {
		result = append(result, obj.(v1alpha1.PipelineTaskTemplateInterface))
	}
	return result
}

// preProcess
// 1. auto trans template kind according by working namespace
// 2. auto labled official or customer
func (s *syncer) preProcess(templates *[]v1alpha1.PipelineTemplateInterface,
	taskTemplates *[]v1alpha1.PipelineTaskTemplateInterface) {

	for i := range *templates {
		template := (*templates)[i]
		k8s.CompatiableBaseDomain(template.GetObjectMeta())
		(*templates)[i] = s.transTemplateKind(template)
		s.labeledOfficialOrCustomer((*templates)[i].GetObjectMeta())
	}

	for i := range *taskTemplates {
		tasktemplate := (*taskTemplates)[i]
		k8s.CompatiableBaseDomain(tasktemplate)
		(*taskTemplates)[i] = s.transTaskTemplateKind(tasktemplate)
		s.labeledOfficialOrCustomer((*taskTemplates)[i].GetObjectMeta())
	}

	return
}

func (s *syncer) versionChange(old metav1.Object, new metav1.Object) bool {
	if old == nil {
		return true
	}

	return IsNewer(s.getVersion(old), s.getVersion(new))
}

func (s *syncer) forceChange(old metav1.Object, new metav1.Object) bool {
	return true
}

// IsNewer returns true if the newVer is newer than oldVer
func IsNewer(oldVer string, newVer string) bool {

	v1, _ := semver.ParseTolerant(oldVer)
	v2, _ := semver.ParseTolerant(newVer)

	return v2.Compare(v1) > 0
}

func (s *syncer) labeledOfficialOrCustomer(objectMeta metav1.Object) {

	labels := objectMeta.GetLabels()
	if labels == nil {
		labels = map[string]string{}
	}

	if labels[v1alpha1.LabelTemplateSource] == "" {
		labels[v1alpha1.LabelTemplateSource] = s.templateSource()
	}
	labels[v1alpha1.LabelCreatedMethod] = v1alpha1.LabelRepositorySync

	objectMeta.SetLabels(labels)
}

func (s *syncer) transTemplateKind(template v1alpha1.PipelineTemplateInterface) v1alpha1.PipelineTemplateInterface {
	if s.isClusterScopedResource() {
		return s.transToClusterTemplate(template)
	}
	return s.transToNamespacedTemplate(template)
}

func (s *syncer) transTaskTemplateKind(taskTemplate v1alpha1.PipelineTaskTemplateInterface) v1alpha1.PipelineTaskTemplateInterface {
	if s.isClusterScopedResource() {
		return s.transToClusterTaskTemplate(taskTemplate)
	}

	return s.transToNamespacedTaskTemplate(taskTemplate)
}

// isClusterScopedResource judge if pipelinetemplatesync is in global or in namespaced
// if working in global, the resource must be cluster scoped
// if working in namespaced the resource must be namespaced
func (s *syncer) isClusterScopedResource() bool {
	return s.getNamespace() == "" || s.getNamespace() == s.credentialsNamespace
}

// up to now, we will only import official resource from disk path
func (s *syncer) templateSource() string {
	if s.Sync.GetSpec().Source.Disk != nil {
		return v1alpha1.LabelTemplateSourceOfficial
	}
	return v1alpha1.LabelTemplateSourceCustomer
}

func (s *syncer) transToClusterTemplate(template v1alpha1.PipelineTemplateInterface) v1alpha1.PipelineTemplateInterface {

	objectMeta := *(template.GetObjectMeta().(*metav1.ObjectMeta))
	objectMeta.Namespace = ""

	return &v1alpha1.ClusterPipelineTemplate{
		TypeMeta: metav1.TypeMeta{
			Kind: v1alpha1.TypeClusterPipelineTemplate,
		},
		ObjectMeta: objectMeta,
		Spec:       *template.GetPiplineTempateSpec(),
	}
}

func (s *syncer) transToNamespacedTemplate(template v1alpha1.PipelineTemplateInterface) v1alpha1.PipelineTemplateInterface {

	objectMeta := *(template.GetObjectMeta().(*metav1.ObjectMeta))
	objectMeta.Namespace = s.getNamespace()

	return &v1alpha1.PipelineTemplate{
		TypeMeta: metav1.TypeMeta{
			Kind: v1alpha1.TypePipelineTemplate,
		},
		ObjectMeta: objectMeta,
		Spec:       *template.GetPiplineTempateSpec(),
	}
}

func (s *syncer) transToClusterTaskTemplate(template v1alpha1.PipelineTaskTemplateInterface) v1alpha1.PipelineTaskTemplateInterface {

	objectMeta := *(template.GetObjectMeta().(*metav1.ObjectMeta))
	objectMeta.Namespace = ""

	return &v1alpha1.ClusterPipelineTaskTemplate{
		TypeMeta: metav1.TypeMeta{
			Kind: v1alpha1.TypeClusterPipelineTaskTemplate,
		},
		ObjectMeta: objectMeta,
		Spec:       *template.GetPiplineTaskTempateSpec(),
	}
}

func (s *syncer) transToNamespacedTaskTemplate(template v1alpha1.PipelineTaskTemplateInterface) v1alpha1.PipelineTaskTemplateInterface {

	objectMeta := *(template.GetObjectMeta().(*metav1.ObjectMeta))
	objectMeta.Namespace = s.getNamespace()

	return &v1alpha1.PipelineTaskTemplate{
		TypeMeta: metav1.TypeMeta{
			Kind: v1alpha1.TypePipelineTaskTemplate,
		},
		ObjectMeta: objectMeta,
		Spec:       *template.GetPiplineTaskTempateSpec(),
	}
}

// getNamespace will return namespace that sync worked in
func (s *syncer) getNamespace() string {
	return s.Sync.GetNamespace()
}

func (s *syncer) unmarshall(files map[string][]byte) (
	templates []v1alpha1.PipelineTemplateInterface,
	taskTemplates []v1alpha1.PipelineTaskTemplateInterface,
	pipelineTemplatePathMap map[string]string, taskTemplatePathMap map[string]string,
	err error) {

	templates = make([]v1alpha1.PipelineTemplateInterface, 0, len(files))
	taskTemplates = make([]v1alpha1.PipelineTaskTemplateInterface, 0, len(files))
	pipelineTemplatePathMap = make(map[string]string, len(files))
	taskTemplatePathMap = make(map[string]string, len(files))

	errs := util.MultiErrors{}

	for path, content := range files {
		typeMeta := &metav1.TypeMeta{}
		err := yaml.Unmarshal(content, typeMeta)
		if err != nil {
			glog.Errorf("[%s] Unmarshall file %s error: %s", s.Owner, path, err.Error())
			errs = append(errs, fmt.Errorf("Unmarshall file %s error: %s", path, err.Error()))
			continue
		}

		kind := typeMeta.Kind

		switch kind {
		case v1alpha1.TypePipelineTaskTemplate, v1alpha1.TypeClusterPipelineTaskTemplate:
			{
				taskTemplate := v1alpha1.NewPipelineTaskTemplate(kind)
				err = yaml.Unmarshal(content, taskTemplate)
				if err != nil {
					glog.Errorf("[%s] Unmarshall file %s to %s error: %s", s.Owner, path, kind, err.Error())
					errs = append(errs, fmt.Errorf("Unmarshall file %s to %s error: %s", path, kind, err.Error()))
					continue
				}

				taskTemplates = append(taskTemplates, taskTemplate)
				// kind of task template will change in preprocess, so do not save it in key
				// and we are not need to use it neither
				taskTemplatePathMap[taskTemplate.GetName()] = path
			}
		case v1alpha1.TypePipelineTemplate, v1alpha1.TypeClusterPipelineTemplate:
			{
				template := v1alpha1.NewPipelineTemplate(kind)
				err = yaml.Unmarshal(content, template)
				if err != nil {
					glog.Errorf("[%s] Unmarshall file %s to %s error: %s", s.Owner, path, kind, err.Error())
					errs = append(errs, fmt.Errorf("Unmarshall file %s to %s error: %s", path, kind, err.Error()))
					continue
				}
				templates = append(templates, template)
				// kind of pipeline template will change in preprocess, so do not save it in key
				// and we are not need to use it neither
				pipelineTemplatePathMap[template.GetName()] = path
			}
		default:
			errs = append(errs, fmt.Errorf("Unknown kind %s of file %s", kind, path))
		}
	}

	if len(errs) == 0 {
		return templates, taskTemplates, pipelineTemplatePathMap, taskTemplatePathMap, nil
	}

	return templates, taskTemplates, pipelineTemplatePathMap, taskTemplatePathMap, &errs
}

func (s *syncer) String() string {
	info := fmt.Sprintf("Stragegy: %s, ClusterScopedResource: %t", s.Sync.GetSpec().Strategy, s.isClusterScopedResource())

	if s.Sync.GetSpec().Source.Disk != nil {
		return fmt.Sprintf("DISK: %s, %s", s.Sync.GetSpec().Source.Disk.Path, info)
	}

	if s.Sync.GetSpec().Source.Git != nil {
		return fmt.Sprintf("GIT: %s %s, %s", s.Sync.GetSpec().Source.Git.URI, s.Sync.GetSpec().Source.Git.Ref, info)
	}

	return fmt.Sprintf("%#v, %s", s.Sync.GetSpec().Source, info)
}

func (s *syncer) getFiles() (map[string][]byte, string, error) {
	if s.mockGetFiles != nil {
		return s.mockGetFiles()
	}

	if s.Sync.GetSpec().Source.Disk != nil {
		return (&util.DiskLoader{Path: s.Sync.GetSpec().Source.Disk.Path}).Files()
	}

	piplineSource := s.Sync.GetSpec().Source
	if piplineSource.Secret == nil {
		return (&util.SCMLoader{PipelineSource: piplineSource, Secret: nil}).Files()
	}

	namespace := s.Sync.GetNamespace()
	if piplineSource.Secret.Namespace != "" {
		namespace = piplineSource.Secret.Namespace
	}
	secret, err := s.Client.CoreV1().Secrets(namespace).Get(piplineSource.Secret.Name, v1alpha1.GetOptions())
	if err != nil {
		glog.Errorf("[%s] can't found secret, namespace: %s, name: %s", s.Owner, namespace, piplineSource.Secret.Name)
		return nil, "", err
	}
	return (&util.SCMLoader{PipelineSource: piplineSource, Secret: secret}).Files()
}

func addLatestLabel(object metav1.Object) {
	annotations := object.GetAnnotations()
	annotations[v1alpha1.LabelTemplateLatest] = "true"
	object.SetAnnotations(annotations)

	labels := object.GetLabels()
	labels[v1alpha1.LabelTemplateLatest] = "true"
	object.SetLabels(labels)
}

func addTemplateNameAnnotation(template v1alpha1.PipelineTemplateInterface, unVersionedTemplateName string) {
	annotations := template.GetAnnotations()
	if annotations == nil {
		annotations = map[string]string{}
	}
	annotations[v1alpha1.AnnotationsTemplateName] = unVersionedTemplateName
	template.SetAnnotations(annotations)
}

func addTaskTemplateNameAnnotation(template v1alpha1.PipelineTaskTemplateInterface, unVersionedTemplateName string) {
	annotations := template.GetAnnotations()
	if annotations == nil {
		annotations = map[string]string{}
	}
	annotations[v1alpha1.AnnotationsTemplateName] = unVersionedTemplateName
	template.SetAnnotations(annotations)
}

func (s *syncer) getVersion(obj metav1.Object) string {
	if obj == nil {
		return ""
	}

	annotations := obj.GetAnnotations()
	if len(annotations) == 0 {
		return ""
	}

	return annotations[v1alpha1.AnnotationsTemplateVersion]
}

func (s *syncer) debugPrintPipelineTemplate(op operation, templates []v1alpha1.PipelineTemplateInterface) {
	for _, item := range templates {
		version := s.getVersion(item)
		glog.V(5).Infof("[%s] %s %s version:%s", s.Owner, op, item, version)
	}
}

func (s *syncer) debugPrintTaskTemplate(op operation, templates []v1alpha1.PipelineTaskTemplateInterface) {
	for _, item := range templates {
		version := s.getVersion(item)
		glog.V(5).Infof("[%s] %s %s version:%s", s.Owner, op, item, version)
	}
}

func (s *syncer) diffChangeFunc() func(ori, wan metav1.Object) bool {

	if s.Sync.GetSpec().Strategy == v1alpha1.TemplateSyncForce {
		return s.forceChange
	}

	return s.versionChange
}
