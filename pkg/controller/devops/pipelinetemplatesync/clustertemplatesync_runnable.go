package pipelinetemplatesync

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

// AddClusterTemplateSyncRunnable add Runnable as runnable
func AddClusterTemplateSyncRunnable(mgr manager.Manager) error {
	return mgr.Add(NewRunnable(mgr.GetKubeClient(), mgr.GetDevOpsClient(), mgr.GetExtraConfig()))
}

const (
	runnableName       = "clusterpipelinetemplatesync-runnable"
	importTemplatePath = "/templates"
)

// Runnable bootstrapper for global-credentials
type Runnable struct {
	Client       kubernetes.Interface
	DevOpsClient clientset.Interface
	Syncer       func(path string) error

	credentialsNamespace string
	annotationProvider   v1alpha1.AnnotationProvider
}

var _ sigsmanager.Runnable = &Runnable{}

// Start adds a start method to be a runnable
func (nb *Runnable) Start(stop <-chan struct{}) (err error) {
	glog.V(5).Infof("[%s] Starting runnable", runnableName)
	err = nb.Syncer(importTemplatePath)
	if err != nil {
		glog.Errorf("[%s] Error to create template syncer: %s", runnableName, err.Error())
	}
	<-stop
	return
}

// NewRunnable constructs a new bootstrapper
func NewRunnable(client kubernetes.Interface, devopsclient clientset.Interface, extra manager.ExtraConfig) sigsmanager.Runnable {

	r := &Runnable{Client: client, DevOpsClient: devopsclient, credentialsNamespace: extra.CredentialsNamespace, annotationProvider: extra.AnnotationProvider}
	if r.Syncer == nil {
		r.Syncer = r.defaultPathSyncer
	}
	return r
}

func (r *Runnable) defaultPathSyncer(path string) error {
	sync := &v1alpha1.ClusterPipelineTemplateSync{
		TypeMeta: metav1.TypeMeta{
			Kind:       v1alpha1.TypeClusterPipelineTemplateSync,
			APIVersion: v1alpha1.APIVersionV1Alpha1,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: "TemplateSyncOfficial",
		},
		Spec: v1alpha1.PipelineTemplateSyncSpec{
			Strategy: v1alpha1.TemplateSyncForce,
			Source: v1alpha1.PipelineSource{
				SourceType: v1alpha1.PipelineSourceTypeDisk,
				Disk: &v1alpha1.PipelineSourceDisk{
					Path: path,
				},
			},
		},
		Status: &v1alpha1.PipelineTemplateSyncStatus{
			Phase: v1alpha1.PipelineTemplateSyncPhasePending,
		},
	}
	already, err := r.DevOpsClient.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Get(sync.Name, metav1.GetOptions{ResourceVersion: "0"})

	if errors.IsNotFound(err) {
		_, err = r.DevOpsClient.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Create(sync)
		if err != nil {
			glog.Errorf("[%s] error to create ClusterPipelineTemplateSync %s, error:%s", runnableName, sync.Name, err)
			return err
		}
		return nil
	}

	if err != nil {
		glog.Errorf("[%s] error to get ClusterPipelineTemplateSync %s, error:%s", runnableName, sync.Name, err)
		return err
	}

	copy := already.DeepCopy()
	copy.Spec = sync.Spec
	copy.Status = sync.Status
	sync, err = r.DevOpsClient.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Update(copy)
	if err != nil {
		glog.Errorf("[%s] error to update ClusterPipelineTemplateSync %s, error:%s", runnableName, sync.Name, err)
		return err
	}

	return nil
}
