package pipelinetemplatesync_test

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/pipelinetemplatesync"

	"alauda.io/devops-apiserver/pkg/controller/manager"
	devopsctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
)

var _ = Describe("Runnable.Run", func() {
	// Will test the bootstrapping of the runnable
	var (
		mockCtrl *gomock.Controller

		runnable             *pipelinetemplatesync.Runnable
		stopChan             chan struct{}
		systemNamespace      = "alauda-system"
		credentialsNamespace = "global-credentials"
		extraConfig          = manager.ExtraConfig{
			SystemNamespace:      systemNamespace,
			CredentialsNamespace: credentialsNamespace,
		}
	)
	prepare := func() {
		runnable = pipelinetemplatesync.NewRunnable(nil, nil, extraConfig).(*pipelinetemplatesync.Runnable)
	}

	// We can prepare our tests
	BeforeEach(func() {
		// starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		// constructing our mocks
		_ = devopsctrlmock.NewMockSyncerInterface(mockCtrl)

		stopChan = make(chan struct{}, 1)
		stopChan <- struct{}{}
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should run sync path templates function", func() {
		prepare()
		runnable.Syncer = func(path string) error {
			return nil
		}

		runnable.Start(stopChan)
	})
})
