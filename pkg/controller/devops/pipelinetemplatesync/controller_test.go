package pipelinetemplatesync_test

import (
	"errors"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/pipelinetemplatesync"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	devopsctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/types"
	k8sfake "k8s.io/client-go/kubernetes/fake"
)

var _ = Describe("Reconciler.Reconcile", func() {
	// Will test the bootstrapping of the runnable
	var (
		mockCtrl         *gomock.Controller
		templateSyncMock *devopsctrlmock.MockSyncerInterface

		name      = "sync"
		namespace = "default"
		request   = reconcile.Request{
			NamespacedName: types.NamespacedName{
				Name:      name,
				Namespace: namespace,
			},
		}

		reconciler           *pipelinetemplatesync.Reconciler
		k8sClient            *k8sfake.Clientset
		devopsClient         *clientset.Clientset
		syncObj              *v1alpha1.PipelineTemplateSync
		systemNamespace      = "alauda-system"
		credentialsNamespace = "global-credentials"
		extraConfig          = manager.ExtraConfig{
			SystemNamespace:      systemNamespace,
			CredentialsNamespace: credentialsNamespace,
		}
	)
	prepare := func() {
		syncObj = testtools.GetPipelineTemplateSync(request.Name, request.Namespace, "gituri", "secret")

		k8sClient = k8sfake.NewSimpleClientset()
		devopsClient = clientset.NewSimpleClientset(syncObj)
		provider := v1alpha1.NewAnnotationProvider(v1alpha1.UsedBaseDomain)
		reconciler = pipelinetemplatesync.NewReconciler("test-controller", extraConfig.CredentialsNamespace, v1alpha1.TypePipelineTemplateSync, k8sClient, devopsClient, provider)
		reconciler.Syncer = func(sync v1alpha1.PipelineTemplateSyncInterface) pipelinetemplatesync.SyncerInterface {
			return templateSyncMock
		}
	}

	// We can prepare our tests
	BeforeEach(func() {
		// starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		// constructing our mocks
		templateSyncMock = devopsctrlmock.NewMockSyncerInterface(mockCtrl)
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should finish successfuly", func() {
		prepare()
		templateSyncMock.EXPECT().SyncTemplates().Return(nil)
		result, err := reconciler.Reconcile(request)
		Expect(err).To(BeNil())
		Expect(result).To(Equal(reconcile.Result{}))
	})

	It("should have an error while syncing but not return error", func() {
		prepare()
		templateSyncMock.EXPECT().SyncTemplates().Return(errors.New("some error"))

		result, err := reconciler.Reconcile(request)
		Expect(err).To(BeNil())
		Expect(result).To(Equal(reconcile.Result{}))
	})
})
