package pipelinetemplatesync

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

// Reconciler this reconcile primary objective is to
// refresh access tokens for secrets
type Reconciler struct {
	// Kind should be v1alpha1.TypeClusterPipelineTemplateSync or v1alpha1.TypePipelineTemplateSync,
	Kind         string
	Client       kubernetes.Interface
	DevopsClient clientset.Interface

	Syncer func(sync v1alpha1.PipelineTemplateSyncInterface) SyncerInterface

	credentialsNamespace string
	controllerName       string
	annotationProider    v1alpha1.AnnotationProvider
}

// NewReconciler will create a template sync Reconciler
func NewReconciler(controllerName, credentialsNamespace string,
	kind string,
	client kubernetes.Interface, devopsClient clientset.Interface, provider v1alpha1.AnnotationProvider) *Reconciler {

	return &Reconciler{
		controllerName:       controllerName,
		credentialsNamespace: credentialsNamespace,
		Client:               client,
		DevopsClient:         devopsClient,
		Kind:                 kind,
		annotationProider:    provider,
	}
}

var _ reconcile.Reconciler = &Reconciler{}

// Reconcile our reconcile function will sync our secrets
func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	glog.V(5).Infof("[%s] Reconciling \"%s\"", rec.controllerName, request)
	defer func() {

		if err != nil {
			glog.Errorf("[%s] Reconciling finished for \"%s\": result: %#v err: %#v", rec.controllerName, request, result, err)
		} else {
			glog.V(5).Infof("[%s] Reconciling finished for \"%s\": err: %#v", rec.controllerName, request, err)
		}
		err = nil
		result = reconcile.Result{}
	}()
	var sync v1alpha1.PipelineTemplateSyncInterface
	if rec.Kind == v1alpha1.TypePipelineTemplateSync {
		sync, err = rec.DevopsClient.DevopsV1alpha1().PipelineTemplateSyncs(request.Namespace).Get(request.Name, v1alpha1.GetOptions())
		if err != nil {
			return
		}
	} else if rec.Kind == v1alpha1.TypeClusterPipelineTemplateSync {
		sync, err = rec.DevopsClient.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Get(request.Name, v1alpha1.GetOptions())
		if err != nil {
			return
		}
	} else {
		err = fmt.Errorf("kind '%s' is not expected, it should be ClusterPipelineTemplateSync or PipelineTemplateSync", rec.Kind)
		return
	}

	if rec.Syncer == nil {
		rec.Syncer = rec.defaultSyncer
	}

	rec.Syncer(sync).SyncTemplates()
	return
}

// defaultSyncer return a templates syncer
func (rec *Reconciler) defaultSyncer(sync v1alpha1.PipelineTemplateSyncInterface) SyncerInterface {

	if sync.GetSpec().Strategy == "" {
		sync.GetSpec().Strategy = v1alpha1.TemplateSyncVersionUpgrage
	}

	return NewSync(rec.controllerName, rec.Client, rec.DevopsClient, sync, rec.credentialsNamespace, rec.annotationProider)
}
