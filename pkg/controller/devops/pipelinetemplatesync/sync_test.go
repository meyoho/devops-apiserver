package pipelinetemplatesync

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/util"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("Version compare", func() {
	var (
		highVer string
		lowVer  string
	)

	BeforeEach(func() {
		highVer = "1.2.0"
		lowVer = "1.1.0"
	})

	It("Without any prefix", func() {
		Expect(IsNewer(lowVer, highVer)).To(BeTrue())
	})

	Context("Only have major and minor version", func() {
		JustBeforeEach(func() {
			highVer = "1.2"
			lowVer = "1.1"
		})

		It("Should success", func() {
			Expect(IsNewer(lowVer, highVer)).To(BeTrue())
		})
	})

	Context("Only have major and minor version", func() {
		JustBeforeEach(func() {
			highVer = "1.2"
			lowVer = "1.1"
		})

		It("Should success", func() {
			Expect(IsNewer(lowVer, highVer)).To(BeTrue())
		})
	})

	Context("With prefix version", func() {
		JustBeforeEach(func() {
			highVer = "v1.2"
			lowVer = "v1.1"
		})

		It("Should success", func() {
			Expect(IsNewer(lowVer, highVer)).To(BeTrue())
		})
	})

	Context("With patch version", func() {
		JustBeforeEach(func() {
			highVer = "0.1.2"
			lowVer = "0.1.1"
		})

		It("Should success", func() {
			Expect(IsNewer(lowVer, highVer)).To(BeTrue())
		})
	})

})

var _ = Describe("syncer.unmarshall", func() {

	It("Should unmarshall to go struct", func() {
		s := &syncer{}
		templates, taskTemplates, templateMap, taskMap, err := s.unmarshall(templateData)

		Expect(err).To(BeNil())
		Expect(templates).To(HaveLen(1))
		Expect(templates[0].GetKind(), "ClusterPipelineTemplate")
		Expect(templates[0].GetName()).To(BeEquivalentTo("alaudaBuildImage-mig"))
		Expect(templates[0].GetAnnotations()["alauda.io/version"]).To(BeEquivalentTo("0.2.5"))

		Expect(taskTemplates).To(HaveLen(1))
		Expect(taskTemplates[0].GetKind(), "ClusterPipelineTaskTemplate")
		Expect(taskTemplates[0].GetName()).To(BeEquivalentTo("alaudaBuildImageTask-mig"))
		Expect(taskTemplates[0].GetAnnotations()["alauda.io/version"]).To(BeEquivalentTo("0.1"))

		Expect(templateMap["alaudaBuildImage-mig"]).To(BeEquivalentTo("/build.yaml"))
		Expect(taskMap["alaudaBuildImageTask-mig"]).To(BeEquivalentTo("/build-task.yaml"))
	})

})

var _ = Describe("syncer.preprocess", func() {

	var (
		syncInfo             v1alpha1.PipelineTemplateSync
		templateSyncer       *syncer
		credentialsNamespace = "global-credentials"
	)

	BeforeEach(func() {
		syncInfo = v1alpha1.PipelineTemplateSync{
			TypeMeta: metav1.TypeMeta{
				Kind: v1alpha1.TypePipelineTemplateSync,
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: "TemplateSync",
			},
		}
		provider := v1alpha1.NewAnnotationProvider(v1alpha1.UsedBaseDomain)
		templateSyncer = NewSync("case1", nil, nil, &syncInfo, credentialsNamespace, provider).(*syncer)
	})

	Context("When namespace global-credentials and sync from PipelineTemplateSync", func() {
		It("Shoul trans template kind to cluster template and add label of customer", func() {
			templateSyncer.Sync.SetNamespace(credentialsNamespace)
			templates, taskTemplates, _, _, err := templateSyncer.unmarshall(templateData)
			Expect(err).To(BeNil())
			templateSyncer.preProcess(&templates, &taskTemplates)

			for _, item := range templates {
				Expect(item.GetKind()).To(BeEquivalentTo(v1alpha1.TypeClusterPipelineTemplate))
				Expect(item.GetLabels()[v1alpha1.LabelTemplateSource]).To(BeEquivalentTo(v1alpha1.LabelTemplateSourceCustomer))
				Expect(item.GetAnnotations()[v1alpha1.AnnotationsTemplateVersion]).To(Equal("0.2.5"))
			}

			for _, item := range taskTemplates {
				Expect(item.GetKind()).To(BeEquivalentTo(v1alpha1.TypeClusterPipelineTaskTemplate))
				Expect(item.GetLabels()[v1alpha1.LabelTemplateSource]).To(BeEquivalentTo(v1alpha1.LabelTemplateSourceCustomer))
				Expect(item.GetAnnotations()[v1alpha1.AnnotationsTemplateVersion]).To(Equal("0.1"))

			}

		})
	})

	Context("When sync from disk", func() {
		It("Shoul trans template kind to cluster template and add label of official", func() {
			templateSyncer.Sync.GetSpec().Source.Disk = &v1alpha1.PipelineSourceDisk{Path: "/pipelinetemplates"}
			templates, taskTemplates, _, _, err := templateSyncer.unmarshall(templateData)
			Expect(err).To(BeNil())
			templateSyncer.preProcess(&templates, &taskTemplates)
			for _, item := range templates {
				Expect(item.GetKind()).To(BeEquivalentTo(v1alpha1.TypeClusterPipelineTemplate))
				Expect(item.GetLabels()[v1alpha1.LabelTemplateSource]).To(BeEquivalentTo(v1alpha1.LabelTemplateSourceOfficial))
			}

			for _, item := range taskTemplates {
				Expect(item.GetKind()).To(BeEquivalentTo(v1alpha1.TypeClusterPipelineTaskTemplate))
				Expect(item.GetLabels()[v1alpha1.LabelTemplateSource]).To(BeEquivalentTo(v1alpha1.LabelTemplateSourceOfficial))
			}
		})
	})

	Context("When namespace is not global-credentials and sync from PipelineTemplateSync", func() {
		It("Shoul trans template kind to pipeline template and add label of customer", func() {
			templateSyncer.Sync.SetNamespace("not-global-credentials")
			templates, taskTemplates, _, _, err := templateSyncer.unmarshall(templateData)
			Expect(err).To(BeNil())
			templateSyncer.preProcess(&templates, &taskTemplates)
			for _, item := range templates {
				Expect(item.GetKind()).To(BeEquivalentTo(v1alpha1.TypePipelineTemplate))
				Expect(item.GetNamespace()).To(BeEquivalentTo(templateSyncer.Sync.GetNamespace()))
				Expect(item.GetLabels()[v1alpha1.LabelTemplateSource]).To(BeEquivalentTo(v1alpha1.LabelTemplateSourceCustomer))
			}

			for _, item := range taskTemplates {
				Expect(item.GetKind()).To(BeEquivalentTo(v1alpha1.TypePipelineTaskTemplate))
				Expect(item.GetNamespace()).To(BeEquivalentTo(templateSyncer.Sync.GetNamespace()))
				Expect(item.GetLabels()[v1alpha1.LabelTemplateSource]).To(BeEquivalentTo(v1alpha1.LabelTemplateSourceCustomer))
			}
		})
	})
})

var templateData = map[string][]byte{
	"/build.yaml": []byte(`
apiVersion: devops.alauda.io/v1alpha1
kind: ClusterPipelineTemplate
metadata:
  annotations:
    alauda.io/description.en: Clone code and builds a docker image
    alauda.io/description.zh-CN: 克隆代码并构建镜像
    alauda.io/displayName.en: Build docker image
    alauda.io/displayName.zh-CN: 构建
    alauda.io/readme.en: Clone code and builds a docker image
    alauda.io/readme.zh-CN: 克隆代码并构建镜像
    alauda.io/style.icon: build
    alauda.io/version: 0.2.5
    migrated: "true"
  creationTimestamp: null
  labels:
    badges: scm,docker
    category: CICD
  name: alaudaBuildImage-mig
spec:
  agent:
    label: docker-mig-mig
  arguments:
  - displayName:
      en: SonarQube Code Scan Configuration
      zh-CN: 配置SonarQube代码扫描
    items:
    - default: "false"
      display:
        description:
          en: If Enable SonarQube Scan
          zh-CN: 是否开启代码扫描
        name:
          en: Enable SonarQube Scan
          zh-CN: 代码扫描
        type: boolean
      name: sonarScanEnabled
      required: false
      schema:
        type: boolean
    - binding:
      - CodeScan.args.integrationSonarQubeID
      default: ""
      display:
        description:
          en: SonarQube Integration Instance
          zh-CN: SonarQube集成实例
        name:
          en: SonarQube Integration Instance
          zh-CN: SonarQube集成实例
        type: string
      name: integrationSonarQubeID
      relation:
      - action: show
        when:
          name: sonarScanEnabled
          value: true
      required: true
      schema:
        type: string
    - binding:
      - CodeScan.args.qualityGate
      default: ""
      display:
        description:
          en: SonarQube Scan QualityGate
          zh-CN: SonarQube扫描质量阈
        name:
          en: SonarQube Scan QualityGate
          zh-CN: SonarQube扫描质量阈
        related: integrationSonarQubeID
        type: string
      name: qualityGate
      relation:
      - action: show
        when:
          name: sonarScanEnabled
          value: true
      required: false
      schema:
        type: string
    - binding:
      - CodeScan.args.lang
      default: ""
      display:
        description:
          en: SonarQube Project Develop Language
          zh-CN: 项目开发语言
        name:
          en: SonarQube Project Develop Language
          zh-CN: 项目开发语言
        related: integrationSonarQubeID
        type: string
      name: lang
      relation:
      - action: show
        when:
          name: sonarScanEnabled
          value: true
      required: false
      schema:
        type: string
    - binding:
      - CodeScan.args.sonarContextDir
      default: .
      display:
        description:
          en: Sonar Scan Context Directory
          zh-CN: 源码扫描路径
        name:
          en: Sonar Scan Context Directory
          zh-CN: 源码扫描路径
        type: string
      name: sonarContextDir
      relation:
      - action: show
        when:
          name: sonarScanEnabled
          value: true
      required: true
      schema:
        type: string
    - binding:
      - CodeScan.args.propertiesPath
      display:
        description:
          en: SonarQube Properties File Path
          zh-CN: SonarQube配置文件路径
        name:
          en: SonarQube Properties File Path
          zh-CN: SonarQube配置文件路径
        type: string
      name: propertiesPath
      relation:
      - action: show
        when:
          name: sonarScanEnabled
          value: true
      required: false
      schema:
        type: string
    - binding:
      - CodeScan.args.waitForQualityGate
      default: "false"
      display:
        description:
          en: If Enable Wait QualityGat
          zh-CN: 是否等待扫描结果
        name:
          en: Enable Wait QualityGate
          zh-CN: 等待扫描结果
        type: boolean
      name: waitForQualityGate
      relation:
      - action: show
        when:
          name: sonarScanEnabled
          value: true
      required: false
      schema:
        type: boolean
  - displayName:
      en: CI Configuration
      zh-CN: 配置持续集成
    items:
    - binding:
      - BuildImage.args.ciEnabled
      default: "true"
      display:
        description:
          en: CI enabled
          zh-CN: 是否开启代码构建
        name:
          en: CIEnabled
          zh-CN: 代码构建
        type: boolean
      name: ciEnabled
      required: false
      schema:
        type: boolean
    - binding:
      - BuildImage.args.contextDir
      default: ./
      display:
        description:
          en: Path of ci command executing
          zh-CN: 构建命令的执行目录
        name:
          en: Build context
          zh-CN: 构建路径
        type: string
      name: contextDir
      relation:
      - action: show
        when:
          name: ciEnabled
          value: true
      required: true
      schema:
        type: string
    - binding:
      - BuildImage.args.useYaml
      default: "true"
      display:
        description:
          en: If enabled will use the alaudaci.yml file in the code repository to
            start a CI container.
          zh-CN: 若使用自定义YAML镜像，则会使用您在代码仓库的alaudaci.yml中指定的镜像作为持续集成的运行环境。
        name:
          en: Use YAML for CI container
          zh-CN: 使用 YAML 构建的镜像
        type: boolean
      name: useYaml
      relation:
      - action: show
        when:
          name: ciEnabled
          value: true
      required: false
      schema:
        type: boolean
    - binding:
      - BuildImage.args.ciYamlFile
      default: ./alaudaci.yml
      display:
        description:
          en: Path of ci alaudaci.yml
          zh-CN: alaudaci.yml 文件路径
        name:
          en: alaudaci.yml's path
          zh-CN: alaudaci.yml 文件路径
        type: string
      name: ciYamlFile
      relation:
      - action: show
        when: {}
      required: true
      schema:
        type: string
      validation:
        maxLength: 0
        pattern: .*\.yml$
    - binding:
      - BuildImage.args.ciImage
      display:
        description:
          en: Image to execute comand
          zh-CN: 用来执行构建的镜像
        name:
          en: CI image
          zh-CN: CI镜像
        type: alauda.io/dockerimagerepositorymix
      name: ciImage
      relation:
      - action: show
        when: {}
      required: true
      schema:
        type: alauda.io/dockerimagerepositorymix
    - binding:
      - BuildImage.args.ciCommands
      display:
        description:
          en: ci commands
          zh-CN: 执行构建过程中的自定义命令
        name:
          en: CI commands
          zh-CN: 自定义命令
        type: code
      name: ciCommands
      relation:
      - action: show
        when: {}
      required: true
      schema:
        type: string
  - displayName:
      en: Image Repository Configuration
      zh-CN: 配置镜像仓库
    items:
    - binding:
      - BuildImage.args.buildImageEnabled
      default: "false"
      display:
        description:
          en: Build Image
          zh-CN: 是否生成镜像
        name:
          en: Build Image
          zh-CN: Docker构建
        type: boolean
      name: buildImageEnabled
      required: false
      schema:
        type: boolean
    - binding:
      - BuildImage.args.dockerfilePath
      default: ./
      display:
        description:
          en: Path of Dockerfile, eg ./
          zh-CN: Dockerfile的路径，不包含名称，例如当前目录./
        name:
          en: Dockerfile's path
          zh-CN: Dockerfile路径
        type: string
      name: dockerfilePath
      relation:
      - action: show
        when:
          name: buildImageEnabled
          value: true
      required: true
      schema:
        type: string
    - binding:
      - BuildImage.args.dockerfileFromImageRegistryCredentialsId
      display:
        description:
          en: Credentials of base image registry in your Dockerfile
          zh-CN: Dockerfile基础镜像所在镜像中心的登录凭据
        name:
          en: Registry credentials of base image
          zh-CN: 基础镜像中心凭据
        related: jenkins_integration_id
        type: string
      name: dockerfileFromImageRegistryCredentialsId
      relation:
      - action: show
        when:
          name: buildImageEnabled
          value: true
      required: false
      schema:
        type: string
    - binding:
      - BuildImage.args.image
      display:
        description:
          en: Image repository to store the built image
          zh-CN: 构建生成的镜像
        name:
          en: Image repository
          zh-CN: 镜像仓库
        type: alauda.io/dockerimagerepositorymix
      name: image
      relation:
      - action: show
        when:
          name: buildImageEnabled
          value: true
      required: true
      schema:
        type: alauda.io/dockerimagerepositorymix
    - binding:
      - BuildImage.args.imageTag
      default: latest
      display:
        description:
          en: Customized image tag
          zh-CN: 镜像版本, 例如 latest
        name:
          en: Customized image tag
          zh-CN: 镜像版本
        type: string
      name: imageTag
      relation:
      - action: show
        when:
          name: buildImageEnabled
          value: true
      required: true
      schema:
        type: string
    - binding:
      - BuildImage.args.imageExtraTag
      display:
        description:
          en: extra image tag, eg. commitid
          zh-CN: 附加镜像版本, 例如 代码提交版本号
        name:
          en: Auto generated tag
          zh-CN: 附加版本
        type: string
      name: imageExtraTag
      relation:
      - action: show
        when:
          name: buildImageEnabled
          value: true
      required: false
      schema:
        type: string
      validation:
        maxLength: 0
        pattern: ^[^\s]*$
    - binding:
      - BuildImage.args.useImageCache
      default: "true"
      display:
        description:
          en: Use image cache
          zh-CN: 开启镜像缓存
        name:
          en: Use image cache
          zh-CN: 镜像缓存
        type: boolean
      name: useImageCache
      relation:
      - action: show
        when:
          name: buildImageEnabled
          value: true
      required: false
      schema:
        type: boolean
  engine: graph
  options:
    timeout: 5400
  stages:
  - conditions: null
    name: Clone
    tasks:
    - kind: ClusterPipelineTaskTemplate
      name: Clone-mig
      type: public/clone-mig
  - conditions: null
    name: CodeScan
    tasks:
    - kind: ClusterPipelineTaskTemplate
      name: CodeScan-mig
      options:
        timeout: 7200
      relation:
      - action: show
        when:
          name: sonarScanEnabled
          value: true
      type: public/alaudaCodeScan-mig
  - conditions: null
    name: BuildImage
    tasks:
    - kind: ClusterPipelineTaskTemplate
      name: BuildImage-mig
      options:
        timeout: 3600
      type: public/alaudaBuildImage-mig
  withSCM: true	
  `),
	"/build-task.yaml": []byte(`
apiVersion: devops.alauda.io/v1alpha1
kind: ClusterPipelineTaskTemplate
metadata:
  annotations:
    alauda.io/description.en: BuildImage
    alauda.io/description.zh-CN: 构建
    alauda.io/displayName.en: BuildImage
    alauda.io/displayName.zh-CN: 构建
    alauda.io/readme.en: BuildImage
    alauda.io/readme.zh-CN: 构建镜像
    alauda.io/style.icon: ""
    alauda.io/version: "0.1"
    migrated: "true"
  creationTimestamp: null
  labels:
    catgory: CI
  name: alaudaBuildImageTask-mig
spec:
  agent:
    label: docker-mig
  arguments:
  - default: ./
    display:
      description:
        en: ""
        zh-CN: ""
      name:
        en: path of build
        zh-CN: 构建目录
      type: string
    name: contextDir
    required: true
    schema:
      type: string
  - default: "true"
    display:
      description:
        en: CI enabled
        zh-CN: 是否开启代码构建
      name:
        en: CIEnabled
        zh-CN: 代码构建
      type: boolean
    name: ciEnabled
    required: false
    schema:
      type: boolean
  - default: "true"
    display:
      description:
        en: If enabled with use yaml image build, we will use the alaudaci.yml file
          under your code repository as the environment configuration for CI.
        zh-CN: 若使用自定义YAML镜像，则会使用您在代码仓库的alaudaci.yml中指定的镜像作为持续集成的运行环境。
      name:
        en: Use YAML Build Image
        zh-CN: 使用 YAML 构建的镜像
      type: boolean
    name: useYaml
    relation:
    - action: show
      when:
        name: ciEnabled
        value: true
    required: false
    schema:
      type: boolean
  - default: ./alaudaci.yml
    display:
      description:
        en: ""
        zh-CN: ""
      name:
        en: path of alaudaci.yaml
        zh-CN: alaudaci.yml 文件路径
      type: string
    name: ciYamlFile
    relation:
    - action: show
      when: {}
    required: true
    schema:
      type: string
    validation:
      maxLength: 0
      pattern: .*\.yml$
  - display:
      description:
        en: ""
        zh-CN: ""
      name:
        en: CI Image
        zh-CN: CI镜像
      type: alauda.io/dockerimagerepositorymix
    name: ciImage
    relation:
    - action: show
      when: {}
    required: true
    schema:
      type: alauda.io/dockerimagerepositorymix
  - display:
      description:
        en: ""
        zh-CN: ""
      name:
        en: ci commands
        zh-CN: 自定义命令
      type: code
    name: ciCommands
    relation:
    - action: show
      when: {}
    required: true
    schema:
      type: string
  - default: "false"
    display:
      description:
        en: Build Image
        zh-CN: 是否生成镜像
      name:
        en: Build Image
        zh-CN: Docker构建
      type: boolean
    name: buildImageEnabled
    required: false
    schema:
      type: boolean
  - default: ./
    display:
      description:
        en: ""
        zh-CN: ""
      name:
        en: path of Dockerfile
        zh-CN: Dockerfile路径
      type: string
    name: dockerfilePath
    relation:
    - action: show
      when:
        name: buildImageEnabled
        value: true
    required: true
    schema:
      type: string
  - display:
      description:
        en: image name that will build
        zh-CN: ""
      name:
        en: image
        zh-CN: 构建生成的镜像
      type: alauda.io/dockerimagerepositorymix
    name: image
    relation:
    - action: show
      when:
        name: buildImageEnabled
        value: true
    required: true
    schema:
      type: alauda.io/dockerimagerepositorymix
  - default: latest
    display:
      description:
        en: ""
        zh-CN: ""
      name:
        en: image tag
        zh-CN: 镜像版本
      type: string
    name: imageTag
    relation:
    - action: show
      when:
        name: buildImageEnabled
        value: true
    required: true
    schema:
      type: string
  - default: "true"
    display:
      description:
        en: ""
        zh-CN: ""
      name:
        en: Use Image Cache
        zh-CN: 开启镜像缓存
      type: boolean
    name: useImageCache
    relation:
    - action: show
      when:
        name: buildImageEnabled
        value: true
    required: false
    schema:
      type: boolean
  - display:
      description:
        en: ""
        zh-CN: ""
      name:
        en: extra image tag
        zh-CN: 附加版本号
      type: string
    name: imageExtraTag
    relation:
    - action: show
      when:
        name: buildImageEnabled
        value: true
    required: false
    schema:
      type: string
    validation:
      maxLength: 0
      pattern: ^[^\s]*$
  - display:
      description:
        en: ""
        zh-CN: ""
      name:
        en: Credentials of base image registry in your Dockerfile
        zh-CN: Dockerfile基础镜像所在镜像中心的登录凭据
      related: jenkins_integration_id
      type: string
    name: dockerfileFromImageRegistryCredentialsId
    relation:
    - action: show
      when:
        name: buildImageEnabled
        value: true
    required: false
    schema:
      type: string
  body: "script{\n  \n  def build = alauda.build().setContextPath(\"{{.contextDir}}\")\n
    \ {{- if .ciEnabled}}\n  build.\n    {{- if .useYaml}}\n    withYaml(\"{{.ciYamlFile}}\").\n
    \   {{- else}}\n    withCIImage(\"{{.ciImage.repositoryPath}}:{{.ciImage.tag}}\",
    \"{{.ciImageRegistryCredentialsId}}\"){\n        sh '''{{.ciCommands}}'''\n    }.\n
    \   {{- end}}\n  startBuild()\n  {{- end}}\n  BUILD_IMAGE_ENABLED = {{.buildImageEnabled}}\n\n
    \ {{- if .dockerfileFromImageRegistryCredentialsId}}\n      withCredentials([usernamePassword(credentialsId:
    \"{{.dockerfileFromImageRegistryCredentialsId}}\", usernameVariable: \"USERNAME\",
    passwordVariable: \"PASSWORD\")]){\n        def registory = readFile \"{{.dockerfilePath}}Dockerfile\"\n
    \       def dockerfilelines = registory.split(\"\\n\")\n        def dockerhub
    = \"index.docker.io\"\n        dockerfilelines.each{\n          if(it.startsWith(\"FROM\")){\n
    \               from = it\n              }\n            }\n        if (from.contains(\"/\")){\n
    \           image = from.substring(from.indexOf(\" \"),).trim()\n            if
    (image.contains(\"http\")){\n                registry = image\n                }\n
    \           else if((image.contains(\".\"))){\n                registryorusername
    = image.substring(0,image.indexOf(\"/\"))\n                if (registryorusername.contains(\".\")){\n
    \                   registry = image\n                  }else{\n                    registry
    = dockerhub\n                  }\n                }\n            else{\n                registry
    = dockerhub\n                }\n            }\n        else{\n            registry
    = dockerhub\n        }\n      sh \"docker login ${registry} -u ${USERNAME} -p
    ${PASSWORD}\"\n      }\n  {{end}}\n\n  {{- if .buildImageEnabled}}\n  def image
    = build.\n    setUseImageCache({{.useImageCache}}).\n    setDockerfileLocation(\"{{.dockerfilePath}}\").\n
    \   setImage(\"{{.image.repositoryPath}}:{{.imageTag}}\").\n    startBuildImage()\n
    \ image.\n    withRegistry([\n      credentialsId: \"{{.imageRegistryCredentialsId}}\",\n
    \     url: \"{{.image.repositoryPath}}\"\n    ]).\n    push(\"{{.imageExtraTag}}\")\n
    \ {{- end}}\n}\n"
  engine: gotpl		
`),
}

var _ = Describe("syncer.SyncTemplates", func() {
	It("should no errors", func() {
		s := &syncer{}
		s.mockGetFiles = getfiles

		files, _, err := s.getFiles()
		Expect(err).To(BeNil())

		for path := range files {
			Expect(path).ToNot(BeEmpty())
		}
		templates, taskTemplates, templatePathMap, taskPathMap, err := s.unmarshall(files)

		Expect(len(templatePathMap)).To(BeEquivalentTo(len(templates)))
		Expect(len(taskPathMap)).To(BeEquivalentTo(len(taskTemplates)))

		for _, item := range templates {
			Expect(item.GetName()).ToNot(BeEmpty())
			Expect(templatePathMap[item.GetName()]).ToNot(BeEmpty())
		}

		for _, item := range taskTemplates {
			Expect(item.GetName()).ToNot(BeEmpty())
			Expect(taskPathMap[item.GetName()]).ToNot(BeEmpty())
		}
	})

})

func getfiles() (content map[string][]byte, version string, err error) {
	load := &util.GitLoader{
		PipelineSourceGit: v1alpha1.PipelineSourceGit{
			URI: "https://chengjingtao@bitbucket.org/chengjingtao/pipeline-template-mig.git",
			Ref: "master",
		},
		Secret: &v1.Secret{
			Data: map[string][]byte{
				"password": []byte("YtXCqMUH29Rj5gksSV8R"),
				"username": []byte("chengjingtao"),
			},
			Type: "kubernetes.io/basic-auth",
		},
	}
	return load.Files()
}
