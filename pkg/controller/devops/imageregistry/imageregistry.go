package imageregistry

import (
	"context"
	"fmt"
	"time"

	"k8s.io/client-go/util/retry"

	"alauda.io/devops-apiserver/pkg/util"

	"alauda.io/devops-apiserver/pkg/controller/devops/imageregistry/policyconditioner"
	"alauda.io/devops-apiserver/pkg/controller/metrics"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	devopshandler "alauda.io/devops-apiserver/pkg/controller/handler"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	sigspredicate "sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName = "imageregistry-controller"
)

// Add injects the manager so to inject the controller in the manager
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconciler(mgr))
}

// NewReconciler returns a new reconcile.Reconciler
// no-op for this case
func NewReconciler(mgr manager.Manager) reconcile.Reconciler {
	return NewReconcilerBy(mgr.GetClient(), mgr.GetDevOpsClient(), mgr.GetAnnotationProvider())
}

func NewReconcilerBy(cacheClient client.Client, devopsClient clientset.Interface, provider v1alpha1.AnnotationProvider) reconcile.Reconciler {
	rec := reconciler.NewToolReconciler(controllerName, v1alpha1.TypeImageRegistry, cacheClient, devopsClient, v1alpha1.TypeImageRegistry, "imageregistrybindings", "imageRegistry", provider)

	rec.AvailableFunc = func(tool devopsv1alpha1.ToolInterface, lastAttempt *metav1.Time) (status *devopsv1alpha1.HostPortStatus, e error) {
		imageRegistryTool := tool.(*v1alpha1.ImageRegistry)
		return generic.CheckService(imageRegistryTool.GetEndpoint(), nil)
	}

	rec.AppendConditioner(func(tool devopsv1alpha1.ToolInterface) generic.Conditioner {
		return &endpointConditioner{
			rec:           rec,
			imageRegistry: tool.(*v1alpha1.ImageRegistry),
		}
	})

	// policy conditioner
	policyConditionerFuncs := policyconditioner.Factory()
	for i := range policyConditionerFuncs {
		fn := policyConditionerFuncs[i]
		conditioner := func(tool devopsv1alpha1.ToolInterface) generic.Conditioner {
			return fn(cacheClient, devopsClient, provider, tool.(*v1alpha1.ImageRegistry))
		}
		rec.AppendConditioner(conditioner)
	}

	return metrics.DecorateReconciler(
		controllerName,
		v1alpha1.TypeImageRegistry, rec,
	).WithResourceReconcileTime()
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}
	if err = ctrl.Watch(
		&source.Kind{Type: &devopsv1alpha1.ImageRegistry{}},
		&handler.EnqueueRequestForObject{},
		&predicate.PhaseTTLPredicate{TTL: v1alpha1.TTLServiceCheck, Type: v1alpha1.TypeImageRegistry},
	); err != nil {
		utilruntime.HandleError(err)
		return err
	}

	// watch project create
	if err = ctrl.Watch(
		&source.Kind{Type: &corev1.Namespace{}},
		&devopshandler.FuncEnqueueRequestForObject{
			CreateFunc: func(event.CreateEvent) {
				err := triggerReconcileImageregistryWithPolicy(mgr)
				if err != nil {
					glog.Errorf("[%s] Trigger reconcile imageregistry with policy binding when namespace create, error: %v", controllerName, err)
				}
			},
			UpdateFunc: func(event.UpdateEvent) {
				err := triggerReconcileImageregistryWithPolicy(mgr)
				if err != nil {
					glog.Errorf("[%s] Trigger reconcile imageregistry with policy binding when namespace update, error: %v", controllerName, err)
				}
			},
		},
		&sigspredicate.Funcs{
			CreateFunc:  func(e event.CreateEvent) bool { return isProjectNamespace(mgr, e.Meta) },
			UpdateFunc:  func(e event.UpdateEvent) bool { return isProjectNamespace(mgr, e.MetaNew) },
			DeleteFunc:  func(event.DeleteEvent) bool { return false },
			GenericFunc: func(event.GenericEvent) bool { return false },
		},
	); err != nil {
		utilruntime.HandleError(err)
		return err
	}
	return err
}

func isProjectNamespace(mgr manager.Manager, obj metav1.Object) bool {
	provider := mgr.GetAnnotationProvider()
	if obj != nil {
		_, ok := obj.GetLabels()[provider.LabelAlaudaIOProjectKey()]
		return ok
	}
	return false
}

func triggerReconcileImageregistryWithPolicy(mgr manager.Manager) error {
	cacheClient := mgr.GetClient()
	imageregistryList := &v1alpha1.ImageRegistryList{}
	err := cacheClient.List(context.TODO(), &client.ListOptions{}, imageregistryList)
	if err != nil {
		return err
	}

	var errs util.MultiErrors
	for _, imageregistry := range imageregistryList.Items {
		if imageregistry.Spec.BindingPolicy != nil && (imageregistry.Spec.BindingPolicy.Share != nil || imageregistry.Spec.BindingPolicy.Individual != nil) {
			imageregistryCopy := imageregistry.DeepCopy()
			// clear status last attempt time to force reconcile
			if imageregistryCopy.Status.HTTPStatus != nil {
				imageregistryCopy.Status.HTTPStatus.LastAttempt = &metav1.Time{Time: time.Time{}}
			}
			if err = retry.RetryOnConflict(retry.DefaultRetry, func() error { return cacheClient.Update(context.TODO(), imageregistryCopy) }); err != nil {
				glog.Errorf("[%s] Update imageregistry %v error: %v", controllerName, imageregistryCopy, err)
				errs = append(errs, err)
			}
		}
	}
	if len(errs) > 0 {
		return &errs
	}
	return nil

}

type endpointConditioner struct {
	rec           *reconciler.ToolReconciler
	imageRegistry *devopsv1alpha1.ImageRegistry
}

func (c *endpointConditioner) Check() bool {

	if c.imageRegistry == nil {
		return false
	}

	if c.imageRegistry.Spec.Data["endpoint"] != "" {
		return false
	}

	typeMatched := c.imageRegistry.GetType() == devopsv1alpha1.RegistryTypeAlauda || c.imageRegistry.GetType() == devopsv1alpha1.RegistryTypeDockerHub

	return typeMatched
}

// GetCondition get a condition generated
func (c *endpointConditioner) GetCondition() v1alpha1.BindingCondition {
	err := c.assignEndpoint()

	metaTime := metav1.NewTime(time.Now())
	cond := v1alpha1.BindingCondition{
		Name:        "Endpoint",
		LastAttempt: &metaTime,
		Type:        fmt.Sprintf("%s.endpoint", v1alpha1.TypeImageRegistry),
		Status:      v1alpha1.StatusReady,
	}

	if err != nil {
		cond.Status = v1alpha1.StatusError
		cond.Message = err.Error()
		cond.Reason = err.Error()
	}
	return cond
}

func (c *endpointConditioner) assignEndpoint() error {
	glog.V(9).Infof("[%s] Assignning registry endpoint, imageregistry=%s", controllerName, c.imageRegistry.Name)
	imageRegistry := c.imageRegistry
	rec := c.rec

	secretRef := &imageRegistry.Spec.Secret
	if secretRef.Name == "" {
		secretRef, _ = c.stealSecretFromBinding()
		if secretRef == nil {
			return nil
		}
		glog.V(9).Infof("[%s] Stole Secret from binding %#v, imageregistry=%s", controllerName, secretRef, c.imageRegistry.Name)
	}

	secret := &corev1.Secret{}
	err := rec.CacheClient.Get(context.TODO(), client.ObjectKey{Namespace: secretRef.Namespace, Name: secretRef.Name}, secret)
	if err != nil {
		glog.Errorf("[%s] get secret '%s/%s' error:%#v", controllerName, secretRef.Namespace, secretRef.Name, err)
		return err
	}

	if imageRegistry.GetType() != devopsv1alpha1.RegistryTypeAlauda && imageRegistry.GetType() != devopsv1alpha1.RegistryTypeDockerHub {
		return nil
	}

	// no need to provide a common interface just for dockerhub
	var registryEndpoint string
	if imageRegistry.GetType() == devopsv1alpha1.RegistryTypeDockerHub {
		registryEndpoint = v1alpha1.DockerHubRegistry
	}

	if imageRegistry.Spec.Data == nil {
		imageRegistry.Spec.Data = map[string]string{}
	}
	glog.V(5).Infof("[%s] Got registry  endpoint '%s' , imageregistry=%s", controllerName, registryEndpoint, c.imageRegistry.Name)
	if imageRegistry.Spec.Data["endpoint"] == registryEndpoint {
		// endpoint has not change , do nothing
		return nil
	}

	imageRegistry.Spec.Data["endpoint"] = registryEndpoint
	// imageRegistry is a copied instance
	_, err = c.rec.DevopsClient.DevopsV1alpha1().ImageRegistries().Update(c.imageRegistry)
	if err != nil {
		glog.Errorf("[%s] Update registry in get endpoint failed: %s", controllerName, err.Error())
		return err
	}
	glog.Infof("[%s] Updated registry to set endpoint %s success", controllerName, imageRegistry.Spec.Data["endpoint"])
	return nil

}

func (c *endpointConditioner) stealSecretFromBinding() (*v1alpha1.SecretKeySetRef, error) {
	labelSelector := &metav1.LabelSelector{
		MatchLabels: map[string]string{
			v1alpha1.LabelImageRegistry: c.imageRegistry.Name,
		},
	}
	selector, _ := metav1.LabelSelectorAsSelector(labelSelector)

	bindings, err := c.rec.DevopsClient.DevopsV1alpha1().ImageRegistryBindings("").List(metav1.ListOptions{
		LabelSelector: selector.String(),
	})
	if err != nil {
		glog.Errorf("[%s] Error happend to steal secret from binding:%s", controllerName, err.Error())
		return nil, err
	}
	for _, binding := range bindings.Items {
		// the binding in error phase may keeps the wrong secret, we should not use it.
		if binding.Status.Phase == v1alpha1.StatusReady && binding.Spec.Secret.Name != "" {
			return &binding.Spec.Secret, nil
		}
	}
	return nil, nil
}
