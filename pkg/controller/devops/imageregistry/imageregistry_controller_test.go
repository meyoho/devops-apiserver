package imageregistry_test

import (
	"context"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		mockCtrl                *gomock.Controller
		cacheClient             client.Client
		devopsClient            *clientset.Clientset
		request                 reconcile.Request
		result                  reconcile.Result
		err                     error
		Harbor                  devopsv1alpha1.ImageRegistryType = "Harbor"
		imageRegistryReconciler reconcile.Reconciler
		imageRegistryObj        *devopsv1alpha1.ImageRegistry
		//systemNamespace         = "alauda-system"
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())

		devopsClient = clientset.NewSimpleClientset()
		cacheClient = testtools.NewFakeClient()

		request.Name = "imageregistry"
		imageRegistryObj = testtools.GetImageRegistry(Harbor, "http://some-host", request.Name)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	JustBeforeEach(func() {
		imageRegistryReconciler = reconciler.NewToolReconciler("new-imageregistry-controller", devopsv1alpha1.TypeImageRegistry, cacheClient, devopsClient, devopsv1alpha1.TypeImageRegistry, "imageregistrybindings", "imageRegistry", devopsv1alpha1.AnnotationProvider{})
		result, err = imageRegistryReconciler.Reconcile(request)
	})

	PContext("imageregistry does not exist", func() {
		It("should not return error ", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("imageregistry can be updated", func() {
		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(imageRegistryObj)
		})
		It("get an imageregistry", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			imageRegistryObj := &devopsv1alpha1.ImageRegistry{}
			err = cacheClient.Get(context.TODO(), request.NamespacedName, imageRegistryObj)
			Expect(err).To(BeNil())
			Expect(imageRegistryObj).NotTo(BeNil())
			Expect(imageRegistryObj.Name).To(Equal(request.Name))
		})
		It("update an imageregistry", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})
})
