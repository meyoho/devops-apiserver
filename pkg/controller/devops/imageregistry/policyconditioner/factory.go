package policyconditioner

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type policyConditionerFunc func(cacheClient client.Client, devopsClient clientset.Interface, provider v1alpha1.AnnotationProvider, imageRegistry *v1alpha1.ImageRegistry) generic.Conditioner

// Factory returns match policy conditioners
func Factory() []policyConditionerFunc {
	var funcs []policyConditionerFunc
	individualPolicyFunc := func(cacheClient client.Client, devopsClient clientset.Interface, provider v1alpha1.AnnotationProvider, imageRegistry *v1alpha1.ImageRegistry) generic.Conditioner {
		return &policyIndividualConditioner{
			cacheClient:   cacheClient,
			devopsClient:  devopsClient,
			provider:      provider,
			imageRegistry: imageRegistry,
		}
	}

	sharePolicyFunc := func(cacheClient client.Client, devopsClient clientset.Interface, provider v1alpha1.AnnotationProvider, imageRegistry *v1alpha1.ImageRegistry) generic.Conditioner {
		return &policyShareConditioner{
			cacheClient:   cacheClient,
			devopsClient:  devopsClient,
			provider:      provider,
			imageRegistry: imageRegistry,
		}
	}

	funcs = append(funcs, individualPolicyFunc, sharePolicyFunc)
	return funcs
}
