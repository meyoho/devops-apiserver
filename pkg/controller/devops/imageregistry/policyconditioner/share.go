package policyconditioner

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"

	"k8s.io/klog/klogr"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"

	"alauda.io/devops-apiserver/pkg/util"

	apiequality "k8s.io/apimachinery/pkg/api/equality"
	"k8s.io/apimachinery/pkg/labels"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/util/retry"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	controllerName = "imageregistry-controller-policy-conditioner"
)

// policyShareConditioner implements policy binding for imageregistry
type policyShareConditioner struct {
	cacheClient   client.Client
	devopsClient  clientset.Interface
	provider      v1alpha1.AnnotationProvider
	imageRegistry *v1alpha1.ImageRegistry
}

// Name returns Conditioner name
func (c *policyShareConditioner) Name() string {
	return "SharePolicy"
}

// Check implements @Conditioner
func (c *policyShareConditioner) Check() bool {
	if c.imageRegistry == nil {
		return false
	}
	// check policy
	return true
}

// GetCondition implements @Conditioner
func (c *policyShareConditioner) GetCondition() v1alpha1.BindingCondition {
	log := klogr.New().WithName(fmt.Sprintf("[%s]", controllerName)).WithValues("ImageRegistry", c.imageRegistry.Name)
	log.V(5).Info("Policy share conditioner gets conditions")
	attempt := metav1.Now()
	cond := v1alpha1.BindingCondition{
		Name:        c.Name(),
		LastAttempt: &attempt,
		Status:      v1alpha1.StatusReady,
		Type:        fmt.Sprintf("%s.%s", v1alpha1.TypeImageRegistry, c.Name()),
	}

	// 2 situations
	// without policy, do clean job
	// with policy, do reconcile

	// clean
	if c.imageRegistry.Spec.BindingPolicy == nil || c.imageRegistry.Spec.BindingPolicy.Share == nil {
		msg, err := c.cleanBinding(log)
		if err != nil {
			log.Error(err, "Clean bindings generated by policy error", "policy", c.Name())
			cond.Status = v1alpha1.StatusError
			cond.Reason = err.Error()
		}
		cond.Message = msg
		return cond
	}

	// reconcile
	msg, err := c.reconcileBinding(log)
	if err != nil {
		log.Error(err, "Reconcile bindings to all needed namespaces error", "policy", c.Name())
		cond.Status = v1alpha1.StatusError
		cond.Reason = err.Error()
	}
	cond.Message = msg
	return cond
}

func (c *policyShareConditioner) cleanBinding(log logr.Logger) (string, error) {
	// clean imageregistrybinding generated by policy
	selector := labels.SelectorFromSet(c.getLabel())
	listOpts := &client.ListOptions{
		LabelSelector: selector,
	}

	bindingList := &v1alpha1.ImageRegistryBindingList{}
	err := c.cacheClient.List(context.TODO(), listOpts, bindingList)
	if err != nil {
		return "", fmt.Errorf("list bindings by label %v error: %v", listOpts, err)
	}

	var errs util.MultiErrors
	for _, binding := range bindingList.Items {
		err = c.cacheClient.Delete(context.TODO(), &binding)
		if err != nil {
			log.Error(err, "Deleting imageregistrybindings error", "imageregistrybinding", binding)
			errs = append(errs, fmt.Errorf("delete binding %v error: %v", binding, err))
		}
	}
	log.V(5).Info("Deleted imageregistrybindings", "imageregistrybindings", bindingList.Items)

	if len(errs) > 0 {
		return "", &errs
	}
	return "", nil
}

func (c *policyShareConditioner) reconcileBinding(log logr.Logger) (string, error) {
	listOpts := &client.ListOptions{}
	err := listOpts.SetLabelSelector(c.provider.LabelAlaudaIOProjectKey())
	if err != nil {
		return "", fmt.Errorf("parse ListOptions error: %v", err)
	}

	namespaceList := &corev1.NamespaceList{}
	err = c.cacheClient.List(context.TODO(), listOpts, namespaceList)
	if err != nil {
		return "", fmt.Errorf("list namespaces by label %v error: %v", listOpts, err)
	}

	return c.applyBindingToProjects(log, namespaceList)
}

func (c *policyShareConditioner) getLabel() map[string]string {
	return map[string]string{
		v1alpha1.LabelImageRegistryType:      c.imageRegistry.GetType().String(),
		v1alpha1.LabelImageRegistry:          c.imageRegistry.GetName(),
		v1alpha1.LabelBindingPolicyGenerated: c.Name(),
	}
}

type reconcileReport struct {
	create []string
	update []string
	fail   []string
}

func (m reconcileReport) format() string {
	return fmt.Sprintf("Create bindings in %d namespaces, update %d namespaces due to existed, and failure in %d namespaces", len(m.create), len(m.update), len(m.fail))
}

// apply ensures binding
// returns message and error
func (c *policyShareConditioner) applyBindingToProjects(log logr.Logger, namespaceList *corev1.NamespaceList) (string, error) {
	namespaces := make(map[string]bool, len(namespaceList.Items))
	for _, namespace := range namespaceList.Items {
		namespaces[namespace.Name] = false
	}

	labelSelector := labels.SelectorFromSet(c.getLabel())
	bindingList := &v1alpha1.ImageRegistryBindingList{}
	err := c.cacheClient.List(context.TODO(), &client.ListOptions{LabelSelector: labelSelector}, bindingList)
	if err != nil {
		return "", err
	}

	oldBindings := make(map[string]string, len(bindingList.Items))
	for _, binding := range bindingList.Items {
		// imageregistrybinding has existed
		namespaces[binding.GetNamespace()] = true
		oldBindings[binding.GetNamespace()] = binding.GetName()
	}

	var (
		report reconcileReport
		errs   util.MultiErrors
	)
	for namespace, hasBinding := range namespaces {
		if !hasBinding {
			// create binding
			report.create = append(report.create, namespace)
			binding := c.newImageRegistryBinding(namespace)
			log.V(5).Info("Create imageregistrybinding in namespace", "imageregistrybinding", binding, "namespace", namespace)
			err = c.cacheClient.Create(context.TODO(), binding)
			if err != nil {
				log.Error(err, "Create imageregistrybinding error", "imageregistrybinding", binding)
				errs = append(errs, fmt.Errorf("create imageregistrybinding in %s error: %v", namespace, err))
				report.fail = append(report.fail, namespace)
			} else {
				log.V(5).Info("Create imageregistrybinding sucessfully", "imageregistrybinding", binding)
			}
		} else {
			report.update = append(report.update, namespace)
			oldBindingName := oldBindings[namespace]
			// refetch binding to get latest version
			binding := &v1alpha1.ImageRegistryBinding{}
			err = c.cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: namespace, Name: oldBindingName}, binding)
			if err != nil {
				log.Error(err, "Get imageregistrybinding error", "imageregistrybinding", binding)
				errs = append(errs, fmt.Errorf("get imageregistrybinding error: %v", err))
				report.fail = append(report.fail, namespace)
				continue
			}

			newSpec := v1alpha1.ImageRegistryBindingSpec{
				ImageRegistry: v1alpha1.LocalObjectReference{Name: c.imageRegistry.Name},
				Secret:        c.imageRegistry.Spec.Secret,
				RepoInfo: v1alpha1.ImageRegistryBindingRepo{
					Repositories: c.imageRegistry.Spec.BindingPolicy.Share.Repositories,
				},
			}
			if !apiequality.Semantic.DeepEqual(binding.Spec, newSpec) {
				err = retry.RetryOnConflict(retry.DefaultRetry, func() error {
					binding.Spec = newSpec
					log.V(5).Info("Update imageregistrybinding in namespace", "imageregistrybinding", binding, "namespace", namespace)
					var e error
					if e = c.cacheClient.Update(context.TODO(), binding); e != nil {
						e = c.cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: namespace, Name: oldBindingName}, binding)
						if e != nil {
							return e
						}
						binding.Spec = newSpec
						return c.cacheClient.Update(context.TODO(), binding)
					}
					return e
				})

				if err != nil {
					log.Error(err, "Update imageregistrybinding error", "imageregistrybinding", binding)
					errs = append(errs, fmt.Errorf("update imageregistrybinding in %s error: %v", namespace, err))
					report.fail = append(report.fail, namespace)
				} else {
					log.V(5).Info("Update imageregistrybinding sucessfully", "imageregistrybinding", binding)
				}
			}
		}
	}

	if len(errs) > 0 {
		return report.format(), &errs
	}

	return report.format(), nil
}

func (c *policyShareConditioner) newImageRegistryBinding(namespace string) *v1alpha1.ImageRegistryBinding {
	return &v1alpha1.ImageRegistryBinding{
		TypeMeta: metav1.TypeMeta{
			APIVersion: v1alpha1.APIVersionV1Alpha1,
			Kind:       v1alpha1.TypeImageRegistryBinding,
		},
		ObjectMeta: metav1.ObjectMeta{
			GenerateName: fmt.Sprintf("%s-policy-binding-", c.imageRegistry.Name),
			Namespace:    namespace,
			Labels:       c.getLabel(),
			OwnerReferences: []metav1.OwnerReference{
				*common.OwnerReference(c.imageRegistry.GetObjectMeta(), c.imageRegistry.GetKind()),
			},
		},
		Spec: v1alpha1.ImageRegistryBindingSpec{
			ImageRegistry: v1alpha1.LocalObjectReference{Name: c.imageRegistry.Name},
			Secret:        c.imageRegistry.Spec.Secret,
			RepoInfo: v1alpha1.ImageRegistryBindingRepo{
				Repositories: c.imageRegistry.Spec.BindingPolicy.Share.Repositories,
			},
		},
		Status: v1alpha1.ServiceStatus{},
	}
}
