package imageregistry

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	"alauda.io/devops-apiserver/pkg/util"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	rolesynccontrollerName = "imageRegistryRoleSync-controller"
)

type ImageRegistryRoleSyncReconciler struct {
	reconcile.Reconciler
	Client           kubernetes.Interface
	DevopsClient     clientset.Interface
	ThirdPartyClient thirdparty.Interface
}

// Add injects the manager so to inject the controller in the manager
func AddRoleSync(mgr manager.Manager) error {

	irreconciler, err := NewImageRegistryRoleSyncReconciler(mgr)
	if err != nil {
		return err
	}
	return addrolesync(mgr, irreconciler)

}

func addrolesync(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(rolesynccontrollerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &devopsv1alpha1.ImageRegistry{}},
		&handler.EnqueueRequestForObject{},
		predicate.RoleSyncEnable,
		predicate.RoleSyncEnable, predicate.RoleSyncTTL(rolesynccontrollerName),
	)
	return nil
}

// NewImageRegistryRoleSyncReconciler constructor for RoleSync Reconciler
func NewImageRegistryRoleSyncReconciler(mgr manager.Manager) (roleReconciler reconcile.Reconciler, err error) {
	return NewImageRegistryRoleSyncReconcilerBy(mgr.GetKubeClient(), mgr.GetDevOpsClient(), mgr.GetThirdParty(), mgr.GetExtraConfig())
}

// NewImageRegistryRoleSyncReconcilerBy sub constructor
func NewImageRegistryRoleSyncReconcilerBy(client kubernetes.Interface, devopsClient clientset.Interface, thirdPartyClient thirdparty.Interface, extra manager.ExtraConfig) (roleReconciler reconcile.Reconciler, err error) {
	rec := &ImageRegistryRoleSyncReconciler{
		Client:           client,
		DevopsClient:     devopsClient,
		ThirdPartyClient: thirdPartyClient,
	}
	roleReconciler, err = reconciler.NewRoleSyncReconcilerBy(client, devopsClient, thirdPartyClient, reconciler.RoleSyncOptions{
		Name:             rolesynccontrollerName,
		GetToolData:      rec.GetImageRegistryToolData,
		GetRoleMapping:   rec.GetImageRegistryRoleMapping,
		ApplyRoleMapping: rec.ApplyImageRegistryRoleMapping,
	}, extra.SystemNamespace)
	if err != nil {
		roleReconciler = nil
		return
	}
	rec.Reconciler = roleReconciler
	return rec, nil
}

// GetImageRegistryToolData function to get ToolData
func (p *ImageRegistryRoleSyncReconciler) GetImageRegistryToolData(request reconcile.Request) (data reconciler.ToolData, err error) {
	Name := request.Name
	var (
		im       *v1alpha1.ImageRegistry
		bindings *v1alpha1.ImageRegistryBindingList
		// secret   *corev1.Secret
	)
	im, err = p.DevopsClient.DevopsV1alpha1().ImageRegistries().Get(Name, v1alpha1.GetOptions())
	if err != nil {
		return
	}
	if im.Spec.Secret.Name == "" || im.Spec.Secret.Namespace == "" {
		err = errors.NewBadRequest(fmt.Sprintf("ImageRegistry %s does not have a secret and cannot sync roles", im.Name))
		return
	}

	// secret, err = p.Client.CoreV1().Secrets(im.Spec.Secret.Namespace).Get(im.Spec.Secret.Name, v1alpha1.GetOptions())
	// if err != nil {
	// 	return
	// }
	//will get all namespaces
	bindings, err = p.DevopsClient.DevopsV1alpha1().ImageRegistryBindings("").List(metav1.ListOptions{ResourceVersion: "0"})
	if err != nil {
		return data, err
	}

	filterbindings := []runtime.Object{}
	imagemap := make(map[string][]string)
	var namespaceRepos []string
	var ok bool
	for _, binding := range bindings.Items {
		if binding.Spec.ImageRegistry.Name == Name && len(binding.Spec.RepoInfo.Repositories) > 0 {
			filterbindings = append(filterbindings, &binding)
			if namespaceRepos, ok = imagemap[binding.ObjectMeta.Namespace]; !ok || namespaceRepos == nil {
				namespaceRepos = make([]string, 0, len(binding.Spec.RepoInfo.Repositories))
			}
			namespaceRepos = append(namespaceRepos, binding.Spec.RepoInfo.Repositories...)

			imagemap[binding.ObjectMeta.Namespace] = namespaceRepos
		}
	}
	// removing duplicates
	// allProjects := make([]string, 0, len(imagemap))
	for k, v := range imagemap {
		imagemap[k] = util.RemoveDuplicateStrings(v)
		// allProjects = append(allProjects, imagemap[k]...)
	}
	// p.ThirdPartyClient.ImageRegistryFactory().GetImageRegistryClient(im.Spec.Type, im.Spec.HTTP.Host)
	data.ToolInstance = im
	data.ToolType = string(im.Spec.Type)
	data.Bindings = filterbindings
	data.NamespaceProjectsMap = imagemap
	return data, nil
}

// GetImageRegistryRoleMapping get tool data mapping
func (p *ImageRegistryRoleSyncReconciler) GetImageRegistryRoleMapping(data reconciler.ToolData, opts *v1alpha1.RoleMappingListOptions) (roleMapping *v1alpha1.RoleMapping, err error) {
	imageRegistry, _ := data.ToolInstance.(*devopsv1alpha1.ImageRegistry)
	if err != nil {
		return nil, err
	}
	serviceName := imageRegistry.Name
	roleMapping, err = p.DevopsClient.DevopsV1alpha1().ImageRegistries().GetRoleMapping(serviceName, opts)
	return
}

// ApplyImageRegistryRoleMapping apply method
func (p *ImageRegistryRoleSyncReconciler) ApplyImageRegistryRoleMapping(data reconciler.ToolData, roleMapping *v1alpha1.RoleMapping) (result *v1alpha1.RoleMapping, err error) {
	project, _ := data.ToolInstance.(*devopsv1alpha1.ImageRegistry)
	if err != nil {
		return nil, err
	}
	serviceName := project.Name
	result, err = p.DevopsClient.DevopsV1alpha1().ImageRegistries().ApplyRoleMapping(serviceName, roleMapping)
	return
}
