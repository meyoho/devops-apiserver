package imageregistry_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/controller/predicate"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/imageregistry"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestImageRegistryController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("imageregistry.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/imageregistry", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"imageregistry-controller",
		imageregistry.Add,
		&source.Kind{Type: &v1alpha1.ImageRegistry{}},
		&handler.EnqueueRequestForObject{},
		&predicate.PhaseTTLPredicate{TTL: v1alpha1.TTLServiceCheck, Type: v1alpha1.TypeImageRegistry},
	),
)
