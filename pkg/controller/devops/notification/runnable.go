package notification

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"bitbucket.org/mathildetech/courier/pkg/apis/aiops/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog/klogr"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

const (
	defaultTemplateName        = "notificationtemplate-devops-default"
	defaultTemplateDisplayName = "notificationtemplate-devops-default"
	runnableName               = "notificationtemplate-runnable"

	MethodEmail = "email"
)

// Runnable create defualut notificationtemplate
type Runnable struct {
	courierClient   client.Client
	platformAddress string
	Provider        v1alpha1.AnnotationProvider
}

func NewRunnable(Client kubernetes.Interface, courierClient client.Client, extra manager.ExtraConfig, provider v1alpha1.AnnotationProvider) sigsmanager.Runnable {
	platformAddress, _ := k8s.GetACPPlatformAddress(Client, extra.SystemNamespace)
	return &Runnable{
		courierClient:   courierClient,
		platformAddress: platformAddress,
		Provider:        provider,
	}
}

func AddRunnable(mgr manager.Manager) error {
	scheme, err := v1beta1.SchemeBuilder.Build()
	if err != nil {
		return err
	}
	c, err := client.New(mgr.GetConfig(), client.Options{Scheme: scheme})
	if err != nil {
		return err
	}

	return mgr.Add(NewRunnable(mgr.GetKubeClient(), c, mgr.GetExtraConfig(), mgr.GetAnnotationProvider()))
}

var _ sigsmanager.Runnable = &Runnable{}

// Start adds a start method to be a runnable
func (run *Runnable) Start(stop <-chan struct{}) (err error) {
	log := klogr.New().WithName(runnableName)
	log.Info("Starting runnable")
	tp := run.defaultTemplate()
	_, err = controllerutil.CreateOrUpdate(context.TODO(), run.courierClient, tp, func(existing runtime.Object) error {
		template := existing.(*v1beta1.NotificationTemplate)
		tp := run.defaultTemplate()
		tp.Spec.DeepCopyInto(&template.Spec)
		return nil
	})
	if err != nil {
		log.WithValues(defaultTemplateName).Error(err, "Error during notification")
	}

	<-stop
	return
}

func (run *Runnable) defaultTemplate() *v1beta1.NotificationTemplate {
	return &v1beta1.NotificationTemplate{
		ObjectMeta: metav1.ObjectMeta{
			Name:        defaultTemplateName,
			Annotations: map[string]string{run.Provider.AnnotationsKeyNotificationDisplayName(): defaultTemplateDisplayName},
			Labels:      map[string]string{run.Provider.LabelKeyNotificationType(): MethodEmail},
		},
		Spec: v1beta1.NotificationTemplateSpec{
			Content: `
{{- $status := "SUCCESS" }}
{{- $pipelinename :=  .pipeline.metadata.name }}
{{- $pipelinenamespace :=  .pipeline.metadata.namespace }}
{{- $pipelineconfigname := .pipeline.spec.pipelineConfig.name }}
{{- $items := .pipeline.status.information.items }}

{{- range $_, $value := $items }}
	{{- if eq $value.type "_Summary" }}
		{{- $status = $value.value.status }}
时间: {{- $value.value.create_date }}
		{{- if eq $value.value.status "SUCCESS" }}

流水线执行成功
		{{- else if eq $value.value.status "FAILURE" }}

流水线执行失败
		{{- else if eq $value.value.status "ABORTED" }}

流水线被取消
		{{- end }}
	{{- end }}
{{- end }}

{{- if eq $status "SUCCESS" }}
	{{- range $_, $item := $items }}
		{{- if eq $item.type "_Clone"}}

【{{$item.name}}】

commitID: {{$item.value.commit_id}}
		{{- else if eq $item.type "_Sonar"}}

【{{$item.name}}】

代码扫描结果: {{$item.value.sonar_url}}
		{{- else if eq $item.type "_Maven_Deploy"}}

【{{$item.name}}】

分发仓库: {{$item.value.maven_package}}
		{{- else if eq $item.type "_Docker"}}

【{{$item.name}}】

构建镜像: {{ range $index, $v := $item.value.build_image }}{{if ne $index 0}}","{{end}}{{$v}}{{end}}
		{{- else if eq $item.type "_Deploy"}}

【{{$item.name}}】

{{if $item.value.createApp}}创建的应用{{else}}更新的应用{{end}}: {{$item.value.serviceName}}
		{{- else if eq $item.type "_Sync"}}

【{{$item.name}}】

同步的源镜像: {{$item.value.source_image}}
同步的目标镜像: {{$item.value.target_image}}
		{{- end }}
	{{- end }}
	{{- range $_, $value := $items }}
		{{- if ne (printf "%.1s" $value.type) "_" }}

name: {{$value.name}}
value: {{$value.value}}
type: {{$value.type}}
desc: {{$value.description}}
		{{- end }}
	{{- end }}
{{- end}}


link: ` + run.platformAddress + `#/workspace/{{$pipelinenamespace}}/pipelines/all/{{$pipelineconfigname}}/{{$pipelinename}}`,
			Subject: "【流水线通知】来自 {{.pipeline.metadata.name}}",
		},
	}
}
