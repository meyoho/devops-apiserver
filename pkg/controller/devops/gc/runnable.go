package gc

import (
	"time"

	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/controller/generic"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

type DevOpsGCOptions struct {
	EnableTemplateGC bool
}

var DefaultDevOpsGCOptions = DevOpsGCOptions{}

func AddRunnable(mgr manager.Manager) error {
	return mgr.Add(NewRunnable(mgr.GetClient(), mgr.GetKubeClient(), mgr.GetDevOpsClient(), mgr.GetThirdParty(), mgr.GetExtraConfig()))
}

const (
	runnableName = "devops-gc-runnable"
)

// NewRunnable constructs a new bootstrapper
func NewRunnable(cacheClient client.Client, client kubernetes.Interface, devopsClient clientset.Interface, thirdPartyClient thirdparty.Interface, extra manager.ExtraConfig) sigsmanager.Runnable {
	return &Runnable{
		CacheClient:      cacheClient,
		Client:           client,
		DevOpsClient:     clientset.NewExpansion(devopsClient),
		ThirdPartyClient: thirdPartyClient,

		PipelineTemplateReferencer:     generic.NewPipelineTemplateReferencer(devopsClient),
		PipelineTaskTemplateReferencer: generic.NewPipelineTaskTemplateReferencer(devopsClient),
		systemNamespace:                extra.SystemNamespace,

		Provider: extra.AnnotationProvider,
	}
}

type Runnable struct {
	CacheClient      client.Client
	Client           kubernetes.Interface
	DevOpsClient     clientset.InterfaceExpansion
	ThirdPartyClient thirdparty.Interface

	PipelineTemplateReferencer     generic.Referencer
	PipelineTaskTemplateReferencer generic.Referencer

	systemNamespace string

	Provider v1alpha1.AnnotationProvider
}

var _ sigsmanager.Runnable = &Runnable{}

func (r *Runnable) Start(stop <-chan struct{}) (err error) {

	//1. we need wait all the template annotation have delete alauda.io  DEVOPS-2936
	//2. reduce the pressure of apiserver
	time.Sleep(time.Hour)
	err = r.StartInmediately(stop)
	return
}

//Startfortest Just for test
func (r *Runnable) StartInmediately(stop <-chan struct{}) (err error) {
	glog.V(5).Infof("[%s] Starting runnable", runnableName)

	for _, gcFunc := range r.GCList() {
		var f = gcFunc
		go wait.Until(f, v1alpha1.TTLDevOpsGC, stop)
	}

	<-stop
	return
}

const (
	devOpsNamespaceGC            = "DevOpsNamespaceGC"
	devOpsPipelineTemplateGC     = "DevOpsPipelineTemplateGC"
	devOpsPipelineTaskTemplateGC = "DevOpsPipelineTaskTemplateGC"
	devOpsPipelineGC             = "DevOpsPipelineGC"
)

func (r *Runnable) GCList() []func() {
	lists := []func(){
		wrapper(devOpsNamespaceGC, r.namespaceGC),
		wrapper(devOpsPipelineGC, r.PipelineGC),
	}

	if DefaultDevOpsGCOptions.EnableTemplateGC {
		lists = append(lists,
			wrapper(devOpsPipelineTemplateGC, r.pipelineTemplateGC),
			wrapper(devOpsPipelineTaskTemplateGC, r.pipelineTaskTemplateGC),
		)
	}

	return lists
}

type gcFunc func() error

func wrapper(name string, f gcFunc) func() {

	return func() {
		begin := time.Now()
		glog.Infof("[%s] [%s] Beginning", runnableName, name)
		err := f()

		if err == nil {
			glog.Infof("[%s] [%s] Ended, elapsed %fs", runnableName, name, time.Now().Sub(begin).Seconds())
		} else {
			glog.Errorf("[%s] [%s] Ended error, %s, elapsed %fs", runnableName, name, err.Error(), time.Now().Sub(begin).Seconds())
		}
	}
}

func (r *Runnable) namespaceGC() error {

	productClient, err := r.getAlaudaProductClient()
	if err != nil {
		return err
	}

	// range all namespaces, judge namespace and delete ns
	nslist, err := r.Client.CoreV1().Namespaces().List(metav1.ListOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("[%s] Error list namespace, err=%s", devOpsNamespaceGC, err.Error())
		return err
	}

	for _, ns := range nslist.Items {
		exists, err := productClient.ProjectExists(&ns)
		if err != nil {
			glog.Errorf("[%s] Skip namespace '%s', %s", devOpsNamespaceGC, ns.Name, err.Error())
			continue
		}

		if !exists {
			glog.V(3).Infof("[%s] Deleting namespace '%s' , project of this namespace is not exists", devOpsNamespaceGC, ns.Name)

			err = r.Client.CoreV1().Namespaces().Delete(ns.Name, &metav1.DeleteOptions{})
			if err != nil {
				glog.Errorf("[%s] Deleted namespace '%s' error, %s", devOpsNamespaceGC, ns.Name, err.Error())
				continue
			}
			glog.Infof("[%s] Deleted namespace '%s', project of this namespace is not exists， annotation:%#v", devOpsNamespaceGC, ns.Name, ns.Annotations)
			continue
		}

		glog.V(7).Infof("[%s] Skip namespace '%s', project of this namespace is exists", devOpsNamespaceGC, ns.Name)
	}

	return nil
}

func (r *Runnable) getAlaudaProductClient() (thirdparty.AlaudaProductClient, error) {
	configmap, err := r.Client.CoreV1().ConfigMaps(r.systemNamespace).Get(v1alpha1.SettingsConfigMapName, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("cannot find configmap %s in ns %s, err:%#v ", v1alpha1.SettingsConfigMapName, r.systemNamespace, err)
		return nil, err
	}

	client, _ := r.ThirdPartyClient.AlaudaFactory().Client("", configmap)
	return client, nil
}
