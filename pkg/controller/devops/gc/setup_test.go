package gc_test

import (
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/devops/gc"
	"testing"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/manager"

	"alauda.io/devops-apiserver/pkg/controller/testtools"
	devopsctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	k8sfake "k8s.io/client-go/kubernetes/fake"
)

func TestNamespaceController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("gc.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/gc", []Reporter{junitReporter})
}

var _ = Describe("AddRunnable", testtools.GenRunnableSetup(func(mgr *devopsctrlmock.MockManager) func() {
	return func() {
		extraConfig := manager.ExtraConfig{
			CredentialsNamespace: "global-credentials",
			AnnotationProvider:   devops.NewAnnotationProvider(devops.UsedBaseDomain),
		}
		k8sClient := k8sfake.NewSimpleClientset()
		cacheClient := testtools.NewFakeClient()
		devopsClient := clientset.NewSimpleClientset()
		thirdPartyClient, _ := thirdparty.New(nil, nil, nil, "", "alauda-system", extraConfig.AnnotationProvider)

		// first add expectations here
		// will get our kubeclient from manager
		mgr.EXPECT().GetDevOpsClient().Return(devopsClient)
		mgr.EXPECT().GetClient().Return(cacheClient)
		mgr.EXPECT().GetKubeClient().Return(k8sClient)

		mgr.EXPECT().GetAnnotationProvider().Return(extraConfig.AnnotationProvider).AnyTimes()
		mgr.EXPECT().GetExtraConfig().Return(extraConfig).AnyTimes()
		mgr.EXPECT().GetThirdParty().Return(thirdPartyClient)

		// initiates a new runnable that we cannot get
		mgr.EXPECT().Add(gomock.Any()).Return(nil)

		// invoke the method here finaly
		err := gc.AddRunnable(mgr)

		Expect(err).To(BeNil())
	}
}))
