package gc_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"context"
	"fmt"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"time"

	"k8s.io/apimachinery/pkg/api/errors"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/devops/gc"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	mockace "alauda.io/devops-apiserver/pkg/mock/ace"
	"alauda.io/devops-apiserver/pkg/mock/thirdparty/external"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

var _ = Describe("Runnable.Start", func() {

	var (
		ctrl                 *gomock.Controller
		runnable             sigsmanager.Runnable
		cacheClient          client.Client
		k8sClient            *k8sfake.Clientset
		devopsClient         *clientset.Clientset
		stop                 chan struct{}
		thirdPartyClient     *external.MockInterface
		err                  error
		alaudaClientFactory  *external.MockAlaudaClientFactory
		aceClient            *mockace.MockInterface
		systemNamespace      = "alauda-system"
		credentialsNamespace = "global-credentials"
		provider             = devopsv1alpha1.NewAnnotationProvider(devops.UsedBaseDomain)

		extraConfig = manager.ExtraConfig{
			SystemNamespace:      systemNamespace,
			CredentialsNamespace: credentialsNamespace,
			AnnotationProvider:   devopsv1alpha1.NewAnnotationProvider(devopsv1alpha1.UsedBaseDomain),
		}
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())

		k8sClient = k8sfake.NewSimpleClientset(
			nsListMap["should-delete-1"],
			nsListMap["should-delete-2"],
			nsListMap["should-delete-3"],

			nsListMap["should-keep-1"],
			nsListMap["should-keep-2"],
			nsListMap["should-keep-3"],
			nsListMap["should-keep-4"],
			nsListMap["should-keep-5"],
			devopsConfigMap)
		cacheClient = testtools.NewFakeClient()
		devopsClient = clientset.NewSimpleClientset()
		alaudaClientFactory = external.NewMockAlaudaClientFactory(ctrl)
		thirdPartyClient = external.NewMockInterface(ctrl)
		aceClient = mockace.NewMockInterface(ctrl)

		ctx := context.TODO()
		aceClient.EXPECT().ListProjects(ctx).Return(&ace.ProjectList{
			StandardList: ace.StandardList{
				PageSize:      1,
				NumberOfPages: 1,
				Count:         3,
			},
			Items: []*ace.Project{
				&ace.Project{
					Name: "a6",
				},
				&ace.Project{
					Name: "a7",
				},
			},
		}, nil)
		aceClient.EXPECT().ListSpaces(ctx, "a6").Return(&ace.SpaceList{
			StandardList: ace.StandardList{
				PageSize:      1,
				NumberOfPages: 1,
				Count:         3,
			},
			Items: []*ace.Space{
				{
					Name: "dev",
				},
				{
					Name: "arch",
				},
				{
					Name: "ops",
				},
			},
		}, nil)
		aceClient.EXPECT().ListSpaces(ctx, "a7").Return(&ace.SpaceList{
			StandardList: ace.StandardList{
				PageSize:      1,
				NumberOfPages: 1,
				Count:         0,
			},
			Items: []*ace.Space{},
		}, nil)

		alaudaClientFactory.EXPECT().Client("", gomock.Any()).Return(thirdparty.NewACEClient(aceClient, provider), nil)
		thirdPartyClient.EXPECT().AlaudaFactory().Return(alaudaClientFactory)
		// enable gc
		gc.DefaultDevOpsGCOptions.EnableTemplateGC = true
		runnable = gc.NewRunnable(cacheClient, k8sClient, devopsClient, thirdPartyClient, extraConfig)
		stop = make(chan struct{})
	})

	JustBeforeEach(func() {
		// we will stop this one to run the case, in production this should get stuck there
		go func() {
			// need a time interval to run gcFunc
			// maybe need a better solution
			<-time.After(1 * time.Second)
			stop <- struct{}{}
		}()

		err = runnable.(*gc.Runnable).StartInmediately(stop)
	})

	Context("test namespace gc", func() {
		It("should run and not return an error", func() {
			Expect(err).To(BeNil())

			ns, err := k8sClient.CoreV1().Namespaces().Get("should-keep-1", metav1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(ns.Name).To(BeEquivalentTo("should-keep-1"))

			ns, err = k8sClient.CoreV1().Namespaces().Get("should-keep-2", metav1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(ns.Name).To(BeEquivalentTo("should-keep-2"))

			ns, err = k8sClient.CoreV1().Namespaces().Get("should-keep-3", metav1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(ns.Name).To(BeEquivalentTo("should-keep-3"))

			ns, err = k8sClient.CoreV1().Namespaces().Get("should-keep-4", metav1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(ns.Name).To(BeEquivalentTo("should-keep-4"))

			ns, err = k8sClient.CoreV1().Namespaces().Get("should-keep-5", metav1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(ns.Name).To(BeEquivalentTo("should-keep-5"))
		})
	})

	Context("test pipeline template gc", func() {
		pipelineTemplateTest1 := &devopsv1alpha1.PipelineTemplate{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pipelinetemplate-test-1.0.0.1",
				Namespace: "alauda-system",
				Annotations: map[string]string{
					devopsv1alpha1.AnnotationsTemplateVersion: "0.0.1",
					devopsv1alpha1.AnnotationsTemplateName:    "pipelinetemplate-test-1",
				},
			},
			Spec: devopsv1alpha1.PipelineTemplateSpec{
				Stages: []devopsv1alpha1.PipelineStage{
					devopsv1alpha1.PipelineStage{
						Tasks: []devopsv1alpha1.PipelineTemplateTask{
							devopsv1alpha1.PipelineTemplateTask{
								Name: "task-template-1",
							},
						},
					},
				},
			},
		}
		pipelineTemplateShouldDelete := &devopsv1alpha1.PipelineTemplate{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pipelinetemplate-should-delete",
				Namespace: "any",
				Annotations: map[string]string{
					devopsv1alpha1.AnnotationsTemplateName:    "pipelinetemplate-should-delete",
					devopsv1alpha1.AnnotationsTemplateVersion: "",
				},
			},
		}
		pipelineconfig := &devopsv1alpha1.PipelineConfig{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pipeline-config",
				Namespace: "alauda-system",
				Labels: map[string]string{
					devopsv1alpha1.LabelTemplateKind:    devopsv1alpha1.TypePipelineTemplate,
					devopsv1alpha1.LabelTemplateName:    "pipelinetemplate-test-1",
					devopsv1alpha1.LabelTemplateVersion: "0.0.1",
				},
			},
		}

		pipelineTaskTemplateTest1 := &devopsv1alpha1.PipelineTaskTemplate{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "task-template-1",
				Namespace: "alauda-system",
				Annotations: map[string]string{
					devopsv1alpha1.AnnotationsTemplateName:    "task-template-1",
					devopsv1alpha1.AnnotationsTemplateVersion: "",
				},
			},
		}
		pipelineTaskTemplateShouldDelete := &devopsv1alpha1.PipelineTaskTemplate{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "task-template-should-be-deleted",
				Namespace: "any",
			},
		}

		latestTemplate := &devopsv1alpha1.PipelineTemplate{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "latest-template",
				Namespace: "any",
				Labels: map[string]string{
					"latest": "true",
				},
				Annotations: map[string]string{
					devopsv1alpha1.AnnotationsTemplateName:    "latest-template",
					devopsv1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
			},
		}

		latestTemplateWithVersion := &devopsv1alpha1.PipelineTemplate{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "latest-template.1.0.0",
				Namespace: "any",
				Labels: map[string]string{
					devopsv1alpha1.AnnotationsTemplateName:    "latest-template",
					devopsv1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
				Annotations: map[string]string{
					devopsv1alpha1.AnnotationsTemplateName:    "latest-template",
					devopsv1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
			},
		}
		BeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset(
				pipelineTemplateTest1,
				pipelineconfig,
				pipelineTemplateShouldDelete,
				pipelineTaskTemplateTest1,
				pipelineTaskTemplateShouldDelete,
				latestTemplate,
				latestTemplateWithVersion,
			)
			runnable = gc.NewRunnable(cacheClient, k8sClient, devopsClient, thirdPartyClient, extraConfig)
		})
		It("old pipelinetemplate should auto add finalizers", func() {
			Expect(err).To(BeNil())
			p1, err := devopsClient.DevopsV1alpha1().PipelineTemplates("alauda-system").Get("pipelinetemplate-test-1.0.0.1", devopsv1alpha1.GetOptions())
			Expect(err).To(BeNil())
			Expect(p1.GetFinalizers()).To(BeEquivalentTo([]string{devopsv1alpha1.FinalizerPipelineConfigReferenced}))
		})

		It("no referenced pipelinetemplate should be deleted", func() {
			Expect(err).To(BeNil())
			_, err = devopsClient.DevopsV1alpha1().PipelineTemplates("any").Get("pipelinetemplate-should-delete", devopsv1alpha1.GetOptions())
			Expect(errors.IsNotFound(err)).To(BeTrue())
		})

		It("old pipelinetasktemplate should auto add finalizers", func() {
			Expect(err).To(BeNil())
			taskTemplate, err := devopsClient.DevopsV1alpha1().PipelineTaskTemplates("alauda-system").Get("task-template-1", devopsv1alpha1.GetOptions())
			Expect(err).To(BeNil())
			Expect(taskTemplate.GetFinalizers()).To(BeEquivalentTo([]string{devopsv1alpha1.FinalizerPipelineTemplateReferenced}))
		})

		It("no referenced pipelinetasktemplate should be deleted", func() {
			Expect(err).To(BeNil())
			_, err = devopsClient.DevopsV1alpha1().PipelineTaskTemplates("any").Get("task-template-should-be-deleted", devopsv1alpha1.GetOptions())
			Expect(errors.IsNotFound(err)).To(BeTrue())
		})

		It("latest template should not be deleted", func() {
			Expect(err).To(BeNil())
			_, err = devopsClient.DevopsV1alpha1().PipelineTemplates("any").Get("latest-template", devopsv1alpha1.GetOptions())
			Expect(err).To(BeNil())
		})

		It("latest template with version should also not be deleted", func() {
			Expect(err).To(BeNil())
			_, err = devopsClient.DevopsV1alpha1().PipelineTemplates("any").Get("latest-template.1.0.0", devopsv1alpha1.GetOptions())
			Expect(err).To(BeNil())
		})

	})

	// fake client.Client doesn't support label selector so we skip this unit test
	PContext("test pipeline gc", func() {
		pipelineConfigTest1 := &devopsv1alpha1.PipelineConfig{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-pc-1",
				Namespace: "any",
				Annotations: map[string]string{
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsKeyPipelineConfigBranches):          "[\"branch-1\", \"branch-2\"]",
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsKeyPipelineConfigStaleBranches):     "[\"stale-branch-1\", \"stale-branch-2\"]",
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsKeyPipelineConfigPullRequests):      "[\"pr-1\", \"pr-2\"]",
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsKeyPipelineConfigStalePullRequests): "[\"stale-pr-1\", \"stale-pr-2\"]",
				},
				Labels: map[string]string{
					devopsv1alpha1.LabelPipelineKind: devopsv1alpha1.LabelPipelineKindMultiBranch,
				},
			},
			Status: devopsv1alpha1.PipelineConfigStatus{
				Phase: devopsv1alpha1.PipelineConfigPhaseReady,
			},
		}
		pipelineConfigTest2 := &devopsv1alpha1.PipelineConfig{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test-pc-2",
				Namespace: "any",
				Annotations: map[string]string{
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsKeyPipelineConfigBranches):          "[\"branch-1\", \"branch-2\"]",
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsKeyPipelineConfigStaleBranches):     "[\"stale-branch-1\", \"stale-branch-2\"]",
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsKeyPipelineConfigPullRequests):      "[\"pr-1\", \"pr-2\"]",
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsKeyPipelineConfigStalePullRequests): "[\"stale-pr-1\", \"stale-pr-2\"]",
				},
				Labels: map[string]string{
					devopsv1alpha1.LabelPipelineKind: devopsv1alpha1.LabelPipelineKindMultiBranch,
				},
			},
			Status: devopsv1alpha1.PipelineConfigStatus{
				Phase: devopsv1alpha1.PipelineConfigPhaseSyncing,
			},
		}

		pipelineShouldBeKept1 := &devopsv1alpha1.Pipeline{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pipeline-test-keep-1",
				Namespace: "any",
				Annotations: map[string]string{
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsJenkinsMultiBranchName): "branch-1",
				},
				Labels: map[string]string{
					devopsv1alpha1.LabelPipelineConfig: "test-pc-1",
				},
			},
		}
		pipelineShouldBeKept2 := &devopsv1alpha1.Pipeline{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pipeline-test-keep-2",
				Namespace: "any",
				Annotations: map[string]string{
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsJenkinsMultiBranchName): "stale-branch-1",
				},
				Labels: map[string]string{
					devopsv1alpha1.LabelPipelineConfig: "test-pc-1",
				},
			},
		}

		pipelineShouldBeKept3 := &devopsv1alpha1.Pipeline{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pipeline-test-keep-3",
				Namespace: "any",
				Annotations: map[string]string{
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsJenkinsMultiBranchName): "pr-1",
				},
				Labels: map[string]string{
					devopsv1alpha1.LabelPipelineConfig: "test-pc-1",
				},
			},
		}

		pipelineShouldBeKept4 := &devopsv1alpha1.Pipeline{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pipeline-test-keep-4",
				Namespace: "any",
				Annotations: map[string]string{
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsJenkinsMultiBranchName): "stale-pr-1",
				},
				Labels: map[string]string{
					devopsv1alpha1.LabelPipelineConfig: "test-pc-1",
				},
			},
		}

		pipelineShouldBeKept5 := &devopsv1alpha1.Pipeline{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pipeline-test-keep-5",
				Namespace: "any",
				Annotations: map[string]string{
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsJenkinsMultiBranchName): "test",
				},
				Labels: map[string]string{
					devopsv1alpha1.LabelPipelineConfig: "test-pc-2",
				},
			},
		}

		pipelineShouldBeDeleted1 := &devopsv1alpha1.Pipeline{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pipeline-test-1",
				Namespace: "any",
				Annotations: map[string]string{
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsJenkinsMultiBranchName): "branch-3",
				},
				Labels: map[string]string{
					devopsv1alpha1.LabelPipelineConfig: "test-pc-1",
				},
			},
		}
		pipelineShouldBeDeleted2 := &devopsv1alpha1.Pipeline{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "pipeline-test-2",
				Namespace: "any",
				Annotations: map[string]string{
					GetFormatedAnnotation(devopsv1alpha1.AnnotationsJenkinsMultiBranchName): "pr-4",
				},
				Labels: map[string]string{
					devopsv1alpha1.LabelPipelineConfig: "test-pc-1",
				},
			},
		}

		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(
				pipelineConfigTest1,
				pipelineConfigTest2,
				pipelineShouldBeKept1,
				pipelineShouldBeKept2,
				pipelineShouldBeKept3,
				pipelineShouldBeKept4,
				pipelineShouldBeKept5,
				pipelineShouldBeDeleted1,
				pipelineShouldBeDeleted2,
			)
			runnable = gc.NewRunnable(cacheClient, k8sClient, devopsClient, thirdPartyClient, extraConfig)
		})

		It("Pipeline that its branch not be deleted should not be deleted", func() {
			Expect(err).To(BeNil())
			p1 := &devopsv1alpha1.Pipeline{}
			err := cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "any", Name: "pipeline-test-keep-1"}, p1)
			Expect(err).To(BeNil())
			Expect(p1).To(Equal(pipelineShouldBeKept1))
		})

		It("Pipeline that its PR that not be closed should not be deleted", func() {
			Expect(err).To(BeNil())
			p1 := &devopsv1alpha1.Pipeline{}
			err := cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "any", Name: "pipeline-test-keep-2"}, p1)
			Expect(err).To(BeNil())
			Expect(p1).To(Equal(pipelineShouldBeKept2))
		})

		It("Pipeline that its stale branch that not be deleted in Jenkins should not be deleted", func() {
			Expect(err).To(BeNil())
			p1 := &devopsv1alpha1.Pipeline{}
			err := cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "any", Name: "pipeline-test-keep-3"}, p1)
			Expect(err).To(BeNil())
			Expect(p1).To(Equal(pipelineShouldBeKept3))
		})

		It("Pipeline that its branch that not be deleted in Jenkins should not be deleted", func() {
			Expect(err).To(BeNil())
			p1 := &devopsv1alpha1.Pipeline{}
			err := cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "any", Name: "pipeline-test-keep-4"}, p1)
			Expect(err).To(BeNil())
			Expect(p1).To(Equal(pipelineShouldBeKept4))
		})

		It("Pipeline that its PipelineConfig not ready should not be deleted", func() {
			Expect(err).To(BeNil())
			p1 := &devopsv1alpha1.Pipeline{}
			err := cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "any", Name: "pipeline-test-keep-5"}, p1)
			Expect(err).To(BeNil())
			Expect(p1).To(Equal(pipelineShouldBeKept5))
		})

		It("Pipeline that its branch is deleted should be deleted", func() {
			Expect(err).To(BeNil())
			p1 := &devopsv1alpha1.Pipeline{}
			err := cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "any", Name: "pipeline-test-1"}, p1)
			Expect(errors.IsNotFound(err)).To(BeTrue())
		})

		It("Pipeline that its PR is deleted should be deleted", func() {
			Expect(err).To(BeNil())
			p1 := &devopsv1alpha1.Pipeline{}
			err := cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: "any", Name: "pipeline-test-2"}, p1)
			Expect(errors.IsNotFound(err)).To(BeTrue())
		})
	})
})

func GetFormatedAnnotation(param string) string {
	return fmt.Sprintf("%s/%s", devops.UsedBaseDomain, param)
}

var devopsConfigMap = &corev1.ConfigMap{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "devops-config",
		Namespace: "alauda-system",
	},
}

var nsListMap = map[string]*corev1.Namespace{
	"should-delete-1": &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: "should-delete-1",
			Annotations: map[string]string{
				"project": "a8",
			},
		},
	},

	"should-delete-2": &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: "should-delete-2",
			Annotations: map[string]string{
				"project":    "a7",
				"subProject": "dev",
			},
		},
	},

	"should-delete-3": &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: "should-delete-3",
			Annotations: map[string]string{
				"project":    "a9",
				"subProject": "",
			},
		},
	},

	"should-keep-1": &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: "should-keep-1",
			Annotations: map[string]string{
				"project":    "a6",
				"subProject": "dev",
			},
		},
	},

	"should-keep-2": &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: "should-keep-2",
			Annotations: map[string]string{
				"project":    "a6",
				"subProject": "arch",
			},
		},
	},

	"should-keep-3": &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: "should-keep-3",
			Annotations: map[string]string{
				"project":    "a6",
				"subProject": "ops",
			},
		},
	},

	"should-keep-4": &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:        "should-keep-4",
			Annotations: map[string]string{},
		},
	},

	"should-keep-5": &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: "should-keep-5",
		},
	},
}

var nslist = []runtime.Object{}
