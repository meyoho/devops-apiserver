package gc

import (
	"fmt"
	"time"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	glog "k8s.io/klog"
)

func (r *Runnable) pipelineTaskTemplateGC() error {
	// list resources as follow
	// pipelinetasktemplate clusterpipelinetasktemplate
	pipelineTaskTemplateList, err := r.DevOpsClient.DevopsV1alpha1().PipelineTaskTemplates("").List(devopsv1alpha1.ListOptions())
	if err != nil {
		glog.Errorf("[%s] list pipelinetasktemplates error: %s", devOpsPipelineTaskTemplateGC, err.Error())
		return err
	}

	clusterPipelineTaskTemplateList, err := r.DevOpsClient.DevopsV1alpha1().ClusterPipelineTaskTemplates().List(devopsv1alpha1.ListOptions())
	if err != nil {
		glog.Errorf("[%s] list clusterpipelinetasktemplates error: %s", devOpsPipelineTaskTemplateGC, err.Error())
		return err
	}

	err = r.doPipelineTaskTemplateGC(r.getPipelineTaskTemplateGCTarget(pipelineTaskTemplateList, clusterPipelineTaskTemplateList))
	if err != nil {
		return err
	}

	return nil
}

// getPipelineTaskTemplateGCTarget do some filter and validation
func (r *Runnable) getPipelineTaskTemplateGCTarget(pipelineTaskTemplateList *devopsv1alpha1.PipelineTaskTemplateList, clusterPipelineTaskTemplateList *devopsv1alpha1.ClusterPipelineTaskTemplateList) []devopsv1alpha1.PipelineTaskTemplateInterface {
	templates := make([]devopsv1alpha1.PipelineTaskTemplateInterface, 0, len(pipelineTaskTemplateList.Items)+len(clusterPipelineTaskTemplateList.Items))
	latestTemplates := make(map[string]string, 0)
	// append pipelinetasktemplate
	for i, pipelineTaskTemplate := range pipelineTaskTemplateList.Items {
		if len(pipelineTaskTemplate.GetFinalizers()) == 0 {
			err := r.setPipelineTaskTemplateFinalizers(devopsv1alpha1.TypePipelineTaskTemplate, pipelineTaskTemplate.Namespace, pipelineTaskTemplate.Name)
			if err != nil {
				glog.Errorf("[%s] pipelinetasktemplate %s/%s set finalizers error: %s", devOpsPipelineTaskTemplateGC, pipelineTaskTemplate.Namespace, pipelineTaskTemplate.Name, err.Error())
			}
		}
		// Fix issue: http://jira.alaudatech.com/browse/DEVOPS-2240
		// the recent created template should not be appened
		if time.Since(pipelineTaskTemplate.GetCreationTimestamp().Time) < pipelineTemplateGCBeforeInterval {
			glog.V(7).Infof("[%s] Ignore to gc pipelinetasktemplate %s/%s due to created recently", devOpsPipelineTaskTemplateGC, pipelineTaskTemplate.Namespace, pipelineTaskTemplate.Name)
			continue
		}

		// latest template should not be deleted
		if pipelineTaskTemplate.GetLabels()[devopsv1alpha1.LabelTemplateLatest] == "true" {
			latestTemplates[fmt.Sprintf("%s/%s", pipelineTaskTemplate.GetNamespace(), pipelineTaskTemplate.GetName())] = pipelineTaskTemplate.GetAnnotations()[devopsv1alpha1.AnnotationsTemplateVersion]
			glog.V(7).Infof("[%s] Ignore to add pipelinetasktemplate %s/%s to gc targets due to it is latest", devOpsPipelineTaskTemplateGC, pipelineTaskTemplate.Namespace, pipelineTaskTemplate.Name)
			continue
		}
		templates = append(templates, &pipelineTaskTemplateList.Items[i])
	}
	// append clusterpipelinetasktemplate
	for i, clusterPipelineTaskTemplate := range clusterPipelineTaskTemplateList.Items {
		if len(clusterPipelineTaskTemplate.GetFinalizers()) == 0 {
			err := r.setPipelineTaskTemplateFinalizers(devopsv1alpha1.TypeClusterPipelineTaskTemplate, clusterPipelineTaskTemplate.Namespace, clusterPipelineTaskTemplate.Name)
			if err != nil {
				glog.Errorf("[%s] clusterpipelinetasktemplate %s/%s set finalizers error: %s", devOpsPipelineTaskTemplateGC, clusterPipelineTaskTemplate.Namespace, clusterPipelineTaskTemplate.Name, err.Error())
			}
		}
		// Fix issue: http://jira.alaudatech.com/browse/DEVOPS-2240
		// the recent created template should not be appened
		if time.Since(clusterPipelineTaskTemplate.GetCreationTimestamp().Time) < pipelineTemplateGCBeforeInterval {
			glog.V(7).Infof("[%s] Ignore to gc clusterpipelinetasktemplate %s/%s due to created recently", devOpsPipelineTaskTemplateGC, clusterPipelineTaskTemplate.Namespace, clusterPipelineTaskTemplate.Name)
			continue
		}
		// latest template should not be deleted
		if clusterPipelineTaskTemplate.GetLabels()[devopsv1alpha1.LabelTemplateLatest] == "true" {
			latestTemplates[fmt.Sprintf("%s/%s", clusterPipelineTaskTemplate.GetNamespace(), clusterPipelineTaskTemplate.GetName())] = clusterPipelineTaskTemplate.GetAnnotations()[devopsv1alpha1.AnnotationsTemplateVersion]
			glog.V(7).Infof("[%s] Ignore to add clusterpipelinetasktemplate %s/%s to gc targets due to it is latest", devOpsPipelineTaskTemplateGC, clusterPipelineTaskTemplate.Namespace, clusterPipelineTaskTemplate.Name)
			continue
		}
		templates = append(templates, &clusterPipelineTaskTemplateList.Items[i])
	}

	// filter to exclude latest template with version
	filtered := make([]devopsv1alpha1.PipelineTaskTemplateInterface, 0, len(templates))
	for _, template := range templates {
		nameKey := fmt.Sprintf("%s/%s", template.GetNamespace(), template.GetAnnotations()[devopsv1alpha1.AnnotationsTemplateName])
		if latestVersion, ok := latestTemplates[nameKey]; ok {
			if template.GetAnnotations()[devopsv1alpha1.AnnotationsTemplateVersion] == latestVersion {
				// latest template, ignore
				glog.V(7).Infof("[%s] Ignore to add template %s/%s to gc targets due to it is same version with latest", devOpsPipelineTaskTemplateGC, template.GetNamespace(), template.GetName())
				continue
			}
		}
		filtered = append(filtered, template)
	}
	return filtered
}

// doPipelineTaskTemplateGC clean pipelinetasktemplate and clusterpipelinetasktemplate
func (r *Runnable) doPipelineTaskTemplateGC(targets []devopsv1alpha1.PipelineTaskTemplateInterface) error {
	for _, template := range targets {
		kind, namespace, name := template.GetKind(), template.GetNamespace(), template.GetName()
		referenced, err := r.PipelineTaskTemplateReferencer.Referenced(template)
		if err != nil {
			glog.Errorf("[%s] Ingore to gc pipelinetasktemplate[%s/%s], check referenced by pipelineconfig error: %s", devOpsPipelineTaskTemplateGC, namespace, name, err.Error())
			continue
		}

		if referenced {
			glog.V(7).Infof("[%s] Skip gc pipelinetasktemplate[%s/%s], be used", devOpsPipelineTaskTemplateGC, namespace, name)
			continue
		}

		glog.Infof("[%s] Deleting pipelinetasktemplate[%s/%s]", devOpsPipelineTaskTemplateGC, namespace, name)
		// first clear finalizers
		err = r.cleanPipelineTaskTemplateFinalizers(kind, namespace, name)
		if err != nil {
			glog.Errorf("[%s] Ignore to gc pipelinetasktemplate[%s/%s], clean up finalizers error: %s", devOpsPipelineTaskTemplateGC, namespace, name, err.Error())
			continue
		}
		// delete
		err = r.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Delete(kind, namespace, name, nil)
		if err != nil {
			glog.Errorf("[%s] delete pipelinetasktemplate[%s/%s] error: %s", devOpsPipelineTaskTemplateGC, namespace, name, err.Error())
		} else {
			glog.Infof("[%s] delete pipelinetasktemplate[%s/%s] success", devOpsPipelineTaskTemplateGC, namespace, name)
		}

	}

	return nil
}

func (r *Runnable) setPipelineTaskTemplateFinalizers(kind, namespace, name string) error {
	tpl, err := r.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Get(kind, namespace, name, devopsv1alpha1.GetOptions())
	if err != nil {
		return err
	}

	tpl.SetFinalizers([]string{devopsv1alpha1.FinalizerPipelineTemplateReferenced})
	_, err = r.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Update(tpl)
	return err
}

func (r *Runnable) cleanPipelineTaskTemplateFinalizers(kind, namespace, name string) error {
	tpl, err := r.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Get(kind, namespace, name, devopsv1alpha1.GetOptions())
	if err != nil {
		return err
	}

	if len(tpl.GetFinalizers()) == 0 {
		return nil
	}

	tpl.SetFinalizers([]string{})
	_, err = r.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Update(tpl)
	return err
}
