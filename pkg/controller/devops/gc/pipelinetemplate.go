package gc

import (
	"fmt"
	"time"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	glog "k8s.io/klog"
)

const (
	pipelineTemplateGCBeforeInterval = 2 * time.Hour
)

func (r *Runnable) pipelineTemplateGC() error {
	// list resources as follow
	// pipelinetemplate clusterpipelinetemplate
	pipelineTemplateList, err := r.DevOpsClient.DevopsV1alpha1().PipelineTemplates("").List(devopsv1alpha1.ListOptions())
	if err != nil {
		glog.Errorf("[%s] list pipelinetemplates error: %s", devOpsPipelineTemplateGC, err.Error())
		return err
	}

	clusterPipelineTemplateList, err := r.DevOpsClient.DevopsV1alpha1().ClusterPipelineTemplates().List(devopsv1alpha1.ListOptions())
	if err != nil {
		glog.Errorf("[%s] list clusterpipelinetemplates error: %s", devOpsPipelineTemplateGC, err.Error())
		return err
	}

	err = r.doPipelineTemplateGC(r.getPipelineTemplateGCTarget(pipelineTemplateList, clusterPipelineTemplateList))
	if err != nil {
		return err
	}

	return nil
}

// getPipelineTemplateGCTarget do some filter and validation
func (r *Runnable) getPipelineTemplateGCTarget(pipelineTemplateList *devopsv1alpha1.PipelineTemplateList, clusterPipelineTemplateList *devopsv1alpha1.ClusterPipelineTemplateList) []devopsv1alpha1.PipelineTemplateInterface {
	templates := make([]devopsv1alpha1.PipelineTemplateInterface, 0, len(pipelineTemplateList.Items)+len(clusterPipelineTemplateList.Items))
	latestTemplates := make(map[string]string, 0)
	// append pipelinetemplate
	for i, pipelineTemplate := range pipelineTemplateList.Items {
		if len(pipelineTemplate.GetFinalizers()) == 0 {
			err := r.setPipelineTemplateFinalizers(devopsv1alpha1.TypePipelineTemplate, pipelineTemplate.Namespace, pipelineTemplate.Name)
			if err != nil {
				glog.Errorf("[%s] pipelinetemplate %s/%s set finalizers error: %s", devOpsPipelineTemplateGC, pipelineTemplate.Namespace, pipelineTemplate.Name, err.Error())
			}
		}
		// Fix issue: http://jira.alaudatech.com/browse/DEVOPS-2240
		// the recent created template should not be appened
		if time.Since(pipelineTemplate.GetCreationTimestamp().Time) < pipelineTemplateGCBeforeInterval {
			glog.V(7).Infof("[%s] Ignore to gc pipelinetemplate %s/%s due to created recently", devOpsPipelineTemplateGC, pipelineTemplate.Namespace, pipelineTemplate.Name)
			continue
		}
		// latest template should not be deleted
		if pipelineTemplate.GetLabels()[devopsv1alpha1.LabelTemplateLatest] == "true" {
			latestTemplates[fmt.Sprintf("%s/%s", pipelineTemplate.GetNamespace(), pipelineTemplate.GetName())] = pipelineTemplate.GetAnnotations()[devopsv1alpha1.AnnotationsTemplateVersion]
			glog.V(7).Infof("[%s] Ignore to add pipelinetemplate %s/%s to gc targets due to it is latest", devOpsPipelineTemplateGC, pipelineTemplate.Namespace, pipelineTemplate.Name)
			continue
		}

		templates = append(templates, &pipelineTemplateList.Items[i])
	}
	// append clusterpipelinetemplate
	for i, clusterPipelineTemplate := range clusterPipelineTemplateList.Items {
		if len(clusterPipelineTemplate.GetFinalizers()) == 0 {
			err := r.setPipelineTemplateFinalizers(devopsv1alpha1.TypeClusterPipelineTemplate, clusterPipelineTemplate.Namespace, clusterPipelineTemplate.Name)
			if err != nil {
				glog.Errorf("[%s] clusterpipelinetemplate %s/%s set finalizers error: %s", devOpsPipelineTemplateGC, clusterPipelineTemplate.Namespace, clusterPipelineTemplate.Name, err.Error())
			}
		}
		// Fix issue: http://jira.alaudatech.com/browse/DEVOPS-2240
		// the recent created template should not be appened
		if time.Since(clusterPipelineTemplate.GetCreationTimestamp().Time) < pipelineTemplateGCBeforeInterval {
			glog.V(7).Infof("[%s] Ignore to gc clusterpipelinetemplate %s/%s due to created recently", devOpsPipelineTemplateGC, clusterPipelineTemplate.Namespace, clusterPipelineTemplate.Name)
			continue
		}
		// latest template should not be deleted
		if clusterPipelineTemplate.GetLabels()[devopsv1alpha1.LabelTemplateLatest] == "true" {
			latestTemplates[fmt.Sprintf("%s/%s", clusterPipelineTemplate.GetNamespace(), clusterPipelineTemplate.GetName())] = clusterPipelineTemplate.GetAnnotations()[devopsv1alpha1.AnnotationsTemplateVersion]
			glog.V(7).Infof("[%s] Ignore to add clusterpipelinetemplate %s/%s to gc targets due to it is latest", devOpsPipelineTemplateGC, clusterPipelineTemplate.Namespace, clusterPipelineTemplate.Name)
			continue
		}
		templates = append(templates, &clusterPipelineTemplateList.Items[i])
	}

	// filter to exclude latest template with version
	filtered := make([]devopsv1alpha1.PipelineTemplateInterface, 0, len(templates))
	for _, template := range templates {
		nameKey := fmt.Sprintf("%s/%s", template.GetNamespace(), template.GetAnnotations()[devopsv1alpha1.AnnotationsTemplateName])
		if latestVersion, ok := latestTemplates[nameKey]; ok {
			if template.GetAnnotations()[devopsv1alpha1.AnnotationsTemplateVersion] == latestVersion {
				// latest template, ignore
				glog.V(7).Infof("[%s] Ignore to add template %s/%s to gc targets due to it is same version with latest", devOpsPipelineTemplateGC, template.GetNamespace(), template.GetName())
				continue
			}
		}
		filtered = append(filtered, template)
	}
	return filtered
}

// doPipelineTemplateGC clean pipelinetemplate and clusterpipelinetemplate
func (r *Runnable) doPipelineTemplateGC(targets []devopsv1alpha1.PipelineTemplateInterface) error {
	for _, template := range targets {
		kind, namespace, name := template.GetKind(), template.GetNamespace(), template.GetName()
		referenced, err := r.PipelineTemplateReferencer.Referenced(template)
		if err != nil {
			glog.Errorf("[%s] Ignore to gc pipelinetemplate[%s/%s], check referenced by pipelineconfig error: %s", devOpsPipelineTemplateGC, namespace, name, err.Error())
			continue
		}

		if referenced {
			glog.V(7).Infof("[%s] Skip gc pipelinetemplate[%s/%s], be used", devOpsPipelineTemplateGC, namespace, name)
			continue
		}

		glog.Infof("[%s] Deleting pipelinetemplate[%s/%s]", devOpsPipelineTemplateGC, namespace, name)
		// first clear finalizers
		err = r.cleanPipelineTemplateFinalizers(kind, namespace, name)
		if err != nil {
			glog.Errorf("[%s] Ignore to gc pipelinetemplate[%s/%s], clean up finalizers error: %s", devOpsPipelineTemplateGC, namespace, name, err.Error())
			continue
		}
		// delete
		err = r.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Delete(kind, namespace, name, nil)
		if err != nil {
			glog.Errorf("[%s] delete pipelinetemplate[%s/%s] error: %s", devOpsPipelineTemplateGC, namespace, name, err.Error())
		} else {
			glog.Infof("[%s] delete pipelinetemplate[%s/%s] success", devOpsPipelineTemplateGC, namespace, name)
		}

	}

	return nil
}

func (r *Runnable) setPipelineTemplateFinalizers(kind, namespace, name string) error {
	tpl, err := r.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Get(kind, namespace, name, devopsv1alpha1.GetOptions())
	if err != nil {
		return err
	}

	tpl.SetFinalizers([]string{devopsv1alpha1.FinalizerPipelineConfigReferenced})
	_, err = r.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Update(tpl)
	return err
}

func (r *Runnable) cleanPipelineTemplateFinalizers(kind, namespace, name string) error {
	// refetch resource to avoid conflict
	tpl, err := r.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Get(kind, namespace, name, devopsv1alpha1.GetOptions())
	if err != nil {
		return err
	}

	if len(tpl.GetFinalizers()) == 0 {
		return nil
	}

	tpl.SetFinalizers([]string{})
	_, err = r.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Update(tpl)
	return err
}
