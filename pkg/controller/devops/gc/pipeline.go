package gc

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"encoding/json"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"time"

	glog "k8s.io/klog"
)

const (
	pipelineConfigReadyInterval = 5 * time.Minute
)

// PipelineGC will delete all pipelines belong to closed and deleted PR or Branch
func (r *Runnable) PipelineGC() error {

	// we only list multi-branch PipelineConfig
	req, _ := labels.NewRequirement(v1alpha1.LabelPipelineKind, selection.Equals, []string{v1alpha1.LabelPipelineKindMultiBranch})
	multiBranchSelector := labels.NewSelector().Add(*req)

	pipelineConfigList := &v1alpha1.PipelineConfigList{}
	err := r.CacheClient.List(context.TODO(), &client.ListOptions{
		LabelSelector: multiBranchSelector,
	}, pipelineConfigList)

	if err != nil {
		glog.Errorf("[%s] list pipelineconfigs error: %s", devOpsPipelineGC, err.Error())
		return err
	}

	pipelinesNeedToDeleted := r.getPipelineGCTarget(pipelineConfigList)
	err = r.doPipelineGC(pipelinesNeedToDeleted)
	if err != nil {
		glog.Errorf("[%s] failed to perform pipeline GC error: %s", devOpsPipelineGC, err.Error())
		return err
	}
	return nil
}

func (r *Runnable) getPipelineGCTarget(pipelineConfigList *v1alpha1.PipelineConfigList) []v1alpha1.Pipeline {
	var targetPipelines []v1alpha1.Pipeline
	for _, pipelineConfig := range pipelineConfigList.Items {
		// If PipelineConfig is not ready, there might are some branches not synced,
		// so we should not delete targetPipelines in such branches
		if pipelineConfig.Status.Phase != v1alpha1.PipelineConfigPhaseReady {
			glog.V(7).Infof("[%s] Ignore Pipelines in PipelineConfig '%s/%s', this PipelineConfig's phase is not 'Ready'", devOpsPipelineGC, pipelineConfig.Namespace, pipelineConfig.Name)
			continue
		}

		lastUpdated := pipelineConfig.Status.LastUpdate
		// PipelineConfig has been updated recently, so it might has changed its branch source configuration.
		// We need to wait some time for the branches be scanned and synced.
		if lastUpdated != nil && time.Since(lastUpdated.Time) < pipelineConfigReadyInterval {
			glog.V(7).Infof("[%s] Ignore Pipelines in PipelineConfig '%s/%s', this PipelineConfig has been update recently", devOpsPipelineGC, pipelineConfig.Namespace, pipelineConfig.Name)
			continue
		}

		var branches []string
		branches = append(branches, getBranches(&pipelineConfig, r.Provider)...)
		branches = append(branches, getStaleBranches(&pipelineConfig, r.Provider)...)
		branches = append(branches, getPullRequests(&pipelineConfig, r.Provider)...)
		branches = append(branches, getStalePullRequests(&pipelineConfig, r.Provider)...)

		branchesMap := make(map[string]string)
		for _, b := range branches {
			branchesMap[b] = ""
		}

		req, err := labels.NewRequirement(v1alpha1.LabelPipelineConfig, selection.Equals, []string{pipelineConfig.Name})
		if err != nil {
			glog.Errorf("[%s] Unable to get pipelines of PipelineConfig '%s/%s, error: %s", devOpsPipelineGC, pipelineConfig.Namespace, pipelineConfig.Name, err.Error())
			continue
		}
		pipelineSelector := labels.NewSelector().Add(*req)

		pipelineList := &v1alpha1.PipelineList{}
		err = r.CacheClient.List(context.TODO(), &client.ListOptions{
			Namespace:     pipelineConfig.GetNamespace(),
			LabelSelector: pipelineSelector,
		}, pipelineList)
		if err != nil {
			glog.Errorf("[%s] Unable to get pipelines of PipelineConfig '%s/%s, error: %s", devOpsPipelineGC, pipelineConfig.Namespace, pipelineConfig.Name, err.Error())
			continue
		}

		for _, pipeline := range pipelineList.Items {
			branchName := pipeline.Annotations[r.Provider.AnnotationsJenkinsMultiBranchName()]
			if _, ok := branchesMap[branchName]; ok {
				continue
			}

			targetPipelines = append(targetPipelines, pipeline)
		}
	}
	return targetPipelines
}

func (r Runnable) doPipelineGC(pipelines []v1alpha1.Pipeline) error {
	for _, pipeline := range pipelines {
		err := r.CacheClient.Delete(context.TODO(), &pipeline)

		if err != nil {
			glog.Errorf("[%s] delete Pipeline '%s/%s' error: %s", devOpsPipelineGC, pipeline.Namespace, pipeline.Name, err.Error())
		} else {
			glog.Infof("[%s] delete Pipeline '%s/%s' success", devOpsPipelineGC, pipeline.Namespace, pipeline.Name)
		}
	}
	return nil
}

func getBranches(pipelineConfig *v1alpha1.PipelineConfig, provider v1alpha1.AnnotationProvider) []string {
	return toStringArray(pipelineConfig.Annotations[provider.AnnotationsKeyPipelineConfigBranches()])
}

func getStaleBranches(pipelineConfig *v1alpha1.PipelineConfig, provider v1alpha1.AnnotationProvider) []string {
	return toStringArray(pipelineConfig.Annotations[provider.AnnotationsKeyPipelineConfigStaleBranches()])

}

func getPullRequests(pipelineConfig *v1alpha1.PipelineConfig, provider v1alpha1.AnnotationProvider) []string {
	return toStringArray(pipelineConfig.Annotations[provider.AnnotationsKeyPipelineConfigPullRequests()])

}

func getStalePullRequests(pipelineConfig *v1alpha1.PipelineConfig, provider v1alpha1.AnnotationProvider) []string {
	return toStringArray(pipelineConfig.Annotations[provider.AnnotationsKeyPipelineConfigStalePullRequests()])

}

func toStringArray(plainString string) (arr []string) {
	if plainString == "" {
		return
	}

	arr = make([]string, 0, 10)

	err := json.Unmarshal([]byte(plainString), &arr)
	// ignore error
	if err != nil {
		return
	}

	return
}
