package documentmanagement_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/documentmanagement"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestDocumentManagementController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("documentmanagement.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/documentmanagement", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"documentmanagement-controller",
		documentmanagement.Add,
		&source.Kind{Type: &v1alpha1.DocumentManagement{}},
		&handler.EnqueueRequestForObject{},
	),
)
