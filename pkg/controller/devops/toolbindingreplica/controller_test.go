package toolbindingreplica_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/devops/toolbindingreplica"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	ctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	extmock "alauda.io/devops-apiserver/pkg/mock/thirdparty/external"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	k8stesting "k8s.io/client-go/testing"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {

	var (
		mockCtrl                     *gomock.Controller
		thirdParty                   *extmock.MockInterface
		managerMock                  *ctrlmock.MockManager
		k8sClient                    *k8sfake.Clientset
		devopsClient                 *clientset.Clientset
		request                      reconcile.Request
		result                       reconcile.Result
		toolBindingReplicaObj        *v1alpha1.ToolBindingReplica
		toolBindingReplicaReconciler reconcile.Reconciler
		err                          error
	)

	// We can prepare our tests
	BeforeEach(func() {
		// // starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		managerMock = ctrlmock.NewMockManager(mockCtrl)
		thirdParty = extmock.NewMockInterface(mockCtrl)
		k8sClient = k8sfake.NewSimpleClientset()
		devopsClient = clientset.NewSimpleClientset()
		// starts our basic structs
		request.Name = "some"
		request.Namespace = "default"
		toolBindingReplicaReconciler = toolbindingreplica.NewReconciler(k8sClient, devopsClient, thirdParty)
		toolBindingReplicaObj = testtools.GetToolBingdingReplica(request.Name, request.Namespace, v1alpha1.ToolBindingReplicaSpec{})
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	// here we really run our functions
	JustBeforeEach(func() {
		managerMock.EXPECT().GetDevOpsClient().Return(devopsClient).AnyTimes()
		managerMock.EXPECT().GetKubeClient().Return(k8sClient).AnyTimes()
		managerMock.EXPECT().GetThirdParty().Return(thirdParty).AnyTimes()
		// result, err = toolBindingReplicaReconciler.Reconcile(request)
	})

	// if not added to the client it should return not found error on the client side
	Context("toolbindingreplica does not exist", func() {
		It("should not return an error", func() {

			toolBindingReplicaReconciler = toolbindingreplica.NewReconciler(k8sClient, devopsClient, thirdParty)
			// runs our test here
			result, err = toolBindingReplicaReconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("toolbindingreplica can be updated", func() {
		BeforeEach(func() {
			// k8sClient = k8sfake.NewSimpleClientset(secretObj)
			devopsClient = clientset.NewSimpleClientset(toolBindingReplicaObj)

		})
		It("get a toolbindingreplica", func() {

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// verify if it was really updated
			toolBindingReplicaObj, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas("default").Get("some", metav1.GetOptions{ResourceVersion: "0"})

			Expect(err).To(BeNil())
			Expect(toolBindingReplicaObj).ToNot(BeNil())
			Expect(toolBindingReplicaObj.Name).To(Equal(request.Name))
		})
		It("update an toolbindingreplica", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			devopsClient.DevopsV1alpha1().ToolBindingReplicas("default").Update(toolBindingReplicaObj)
			actions := devopsClient.Actions()
			Expect(actions).ToNot(BeEmpty())
			Expect(actions).To(
				ContainElement(
					WithTransform(func(act k8stesting.Action) bool {
						return act.Matches("update", "toolbindingreplicas")
					}, BeTrue())),
			)
		})
		It("delete a toolbindingreplica", func() {

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// verify if it was really updated
			err = devopsClient.DevopsV1alpha1().ToolBindingReplicas("default").Delete(request.Name, &metav1.DeleteOptions{})

			Expect(err).To(BeNil())
		})
	})
})
