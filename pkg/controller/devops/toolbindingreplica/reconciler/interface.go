package reconciler

import (
	"fmt"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
)

const (
	currentVersion = "v1alpha1"
)

// Interface Sync ToolBindingReplicaTemplate to ensure data consistency
type Interface interface {
	ReadyToReconcile() bool
	Reconcile() ([]devopsv1alpha1.BindingCondition, error)
}

// NewReconciler generate toolbinding reconciler
func NewReconciler(toolBindingReplica devopsv1alpha1.ToolBindingReplica, clientset clientset.Interface, k8sClient kubernetes.Interface, provider devopsv1alpha1.AnnotationProvider) (Interface, error) {
	// some tool may need self reconciler
	switch toolBindingReplica.Spec.Selector.Kind {
	default:
		genericReconciler := NewGenericReconciler(toolBindingReplica, clientset, k8sClient, provider)
		if genericReconciler != nil {
			return genericReconciler, nil
		}
	}
	glog.Errorf("cannot find reconciler in constructorChain for toolbindingreplica %#v , check whether it contains required field", toolBindingReplica)
	return nil, fmt.Errorf("cannot find reconciler")
}
