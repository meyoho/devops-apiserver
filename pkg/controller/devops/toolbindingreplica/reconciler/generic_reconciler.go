package reconciler

import (
	"fmt"
	"strings"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	v1 "k8s.io/api/core/v1"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/validation"
	genericregistry "k8s.io/apiserver/pkg/registry/generic/registry"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
)

// GenericReconciler generic reconciler for different tools
type GenericReconciler struct {
	devopsv1alpha1.ToolBindingReplica
	devopsclientset         clientset.Interface
	k8sClient               kubernetes.Interface
	devopsToolBindingClient clientset.InterfaceExpansion
	kind                    string
	provider                devopsv1alpha1.AnnotationProvider
}

// NewGenericReconciler constructor
func NewGenericReconciler(toolBindingReplica devopsv1alpha1.ToolBindingReplica, devopsclientset clientset.Interface, k8sClient kubernetes.Interface, provider devopsv1alpha1.AnnotationProvider) *GenericReconciler {
	reconciler := &GenericReconciler{
		ToolBindingReplica: toolBindingReplica,
		devopsclientset:    devopsclientset,
		k8sClient:          k8sClient,
		kind:               toolBindingReplica.Spec.Selector.Kind,
		provider:           provider,
	}
	// set devopsToolBindingClient
	if reconciler.devopsclientset != nil {
		reconciler.devopsToolBindingClient = clientset.NewExpansion(reconciler.devopsclientset)
	}
	return reconciler
}

// ReadyToReconcile check ready
func (r *GenericReconciler) ReadyToReconcile() bool {
	switch r.kind {
	case devopsv1alpha1.TypeCodeQualityTool:
		return r.ToolBindingReplica.Spec.CodeQuality != nil &&
			r.ToolBindingReplica.Spec.CodeQuality.Template != nil &&
			len(r.ToolBindingReplica.Spec.CodeQuality.Template.Bindings) > 0

	case devopsv1alpha1.TypeCodeRepoService:
		return r.ToolBindingReplica.Spec.CodeRepoService != nil &&
			r.ToolBindingReplica.Spec.CodeRepoService.Template != nil &&
			len(r.ToolBindingReplica.Spec.CodeRepoService.Template.Bindings) > 0

	case devopsv1alpha1.TypeDocumentManagement:
		return r.ToolBindingReplica.Spec.DocumentManagement != nil &&
			r.ToolBindingReplica.Spec.DocumentManagement.Template != nil &&
			len(r.ToolBindingReplica.Spec.DocumentManagement.Template.Bindings) > 0

	case devopsv1alpha1.TypeImageRegistry:
		return r.ToolBindingReplica.Spec.ImageRegistry != nil &&
			r.ToolBindingReplica.Spec.ImageRegistry.Template != nil &&
			len(r.ToolBindingReplica.Spec.ImageRegistry.Template.Bindings) > 0

	case devopsv1alpha1.TypeJenkins:
		return r.ToolBindingReplica.Spec.ContinuousIntegration != nil &&
			r.ToolBindingReplica.Spec.ContinuousIntegration.Template != nil &&
			len(r.ToolBindingReplica.Spec.ContinuousIntegration.Template.Bindings) > 0

	case devopsv1alpha1.TypeProjectManagement:
		return r.ToolBindingReplica.Spec.ProjectManagement != nil &&
			r.ToolBindingReplica.Spec.ProjectManagement.Template != nil &&
			len(r.ToolBindingReplica.Spec.ProjectManagement.Template.Bindings) > 0

	case devopsv1alpha1.TypeArtifactRegistry:
		return r.ToolBindingReplica.Spec.ArtifactRegistry != nil &&
			r.ToolBindingReplica.Spec.ArtifactRegistry.Template != nil &&
			len(r.ToolBindingReplica.Spec.ArtifactRegistry.Template.Bindings) > 0
	}

	return false
}

// Reconcile reconcile toolBinding
func (r *GenericReconciler) Reconcile() ([]devopsv1alpha1.BindingCondition, error) {
	tbr := r.ToolBindingReplica

	// Get old binding list firstly
	labelSelector := &metav1.LabelSelector{
		MatchLabels: tbr.ObjectReferenceLabel(r.provider),
	}
	selector, err := metav1.LabelSelectorAsSelector(labelSelector)
	if err != nil {
		return []devopsv1alpha1.BindingCondition{}, errorLabelSelectorAsSelector(labelSelector, err)
	}
	oldBindingList, err := r.devopsToolBindingClient.DevopsV1alpha1Expansion().DevOpsToolBinding().List(r.kind, metav1.ListOptions{
		LabelSelector:   selector.String(),
		ResourceVersion: "0",
	})
	if err != nil {
		glog.V(5).Infof("%s List toolbindings by %#v got errors:%#v", r.logPrefix(), labelSelector, err)
		return []devopsv1alpha1.BindingCondition{}, err
	}

	oldBindingObjects := r.getOldBindings(oldBindingList)
	// check ToolBindingReplica being deleted
	if !tbr.GetDeletionTimestamp().IsZero() {
		r.deleteBindings(oldBindingObjects)
		// clean finalizers
		// wait for k8s delete tbr
		tbrCopy := tbr.DeepCopy()
		tbrCopy.SetFinalizers([]string{})
		_, err = r.devopsclientset.DevopsV1alpha1().ToolBindingReplicas(tbr.Namespace).Update(tbrCopy)
		glog.V(5).Infof("%s has DeletionTimestamp, finally clear finalizers, error: %#v", r.logPrefix(), err)
		return []devopsv1alpha1.BindingCondition{}, err
	}

	// generate all CodeQualityBindings that we should create or update
	needCreateOrUpdateBindings, err := r.generateBindings()
	if err != nil {
		glog.Errorf("%s Generate ToolBinding list error:%#v", r.logPrefix(), err)
		glog.Infof("%s ToolBindingReplica if ToolBinding is %#v", r.logPrefix(), r.ToolBindingReplica)
		return []devopsv1alpha1.BindingCondition{}, err
	}

	needDeleteBindings := r.subtractBindings(oldBindingObjects, needCreateOrUpdateBindings)
	// delete all bindings that we should delete
	deleteConditions := r.deleteBindings(needDeleteBindings)
	// create or update bindings that we should do
	createOrUpdateConditions := r.createOrUpdateBindings(needCreateOrUpdateBindings)

	conditions := []devopsv1alpha1.BindingCondition{}
	conditions = append(conditions, deleteConditions...)
	conditions = append(conditions, createOrUpdateConditions...)

	return conditions, nil
}

func (r *GenericReconciler) getOldBindings(oldBindingList interface{}) []metav1.Object {
	var oldBindingObjects []metav1.Object

	switch r.kind {
	case devopsv1alpha1.TypeCodeQualityTool:
		if codeQualityBindingList, ok := oldBindingList.(*devopsv1alpha1.CodeQualityBindingList); ok {
			for _, codeQualityBinding := range codeQualityBindingList.Items {
				oldBindingObjects = append(oldBindingObjects, codeQualityBinding.DeepCopy())
			}
		}
	case devopsv1alpha1.TypeCodeRepoService:
		if codeRepoBindingList, ok := oldBindingList.(*devopsv1alpha1.CodeRepoBindingList); ok {
			for _, codeRepoBinding := range codeRepoBindingList.Items {
				oldBindingObjects = append(oldBindingObjects, codeRepoBinding.DeepCopy())
			}
		}
	case devopsv1alpha1.TypeDocumentManagement:
		if documentManagementBindingList, ok := oldBindingList.(*devopsv1alpha1.DocumentManagementBindingList); ok {
			for _, documentManagementBinding := range documentManagementBindingList.Items {
				oldBindingObjects = append(oldBindingObjects, documentManagementBinding.DeepCopy())
			}
		}
	case devopsv1alpha1.TypeImageRegistry:
		if imageRegistryBindingList, ok := oldBindingList.(*devopsv1alpha1.ImageRegistryBindingList); ok {
			for _, imageRegistryBinding := range imageRegistryBindingList.Items {
				oldBindingObjects = append(oldBindingObjects, imageRegistryBinding.DeepCopy())
			}
		}
	case devopsv1alpha1.TypeJenkins:
		if jenkinsBindingList, ok := oldBindingList.(*devopsv1alpha1.JenkinsBindingList); ok {
			for _, jenkinsBinding := range jenkinsBindingList.Items {
				oldBindingObjects = append(oldBindingObjects, jenkinsBinding.DeepCopy())
			}
		}
	case devopsv1alpha1.TypeProjectManagement:
		if projectManagementBindingList, ok := oldBindingList.(*devopsv1alpha1.ProjectManagementBindingList); ok {
			for _, projectManagementBinding := range projectManagementBindingList.Items {
				oldBindingObjects = append(oldBindingObjects, projectManagementBinding.DeepCopy())
			}
		}
	case devopsv1alpha1.TypeArtifactRegistry:
		if artifactRegistryBindingList, ok := oldBindingList.(*devopsv1alpha1.ArtifactRegistryBindingList); ok {
			for _, artifactRegistryBinding := range artifactRegistryBindingList.Items {
				oldBindingObjects = append(oldBindingObjects, artifactRegistryBinding.DeepCopy())
			}
		}
	}

	return oldBindingObjects
}

func (r *GenericReconciler) newToolBinding(namespace v1.Namespace, toolBindingSpec interface{}) metav1.Object {
	switch r.kind {
	case devopsv1alpha1.TypeCodeQualityTool:
		codeQualityBindingSpec, ok := toolBindingSpec.(devopsv1alpha1.CodeQualityBindingSpec)
		if !ok {
			return nil
		}
		codeQualityBindingSpec.CodeRepository.All = true

		codeQualityBinding := &devopsv1alpha1.CodeQualityBinding{
			TypeMeta: metav1.TypeMeta{
				Kind:       devopsv1alpha1.ResourceKindCodeQualityToolBinding,
				APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			},
			ObjectMeta: r.toolBindingObjectMeta(namespace),
			Spec:       codeQualityBindingSpec,
		}
		glog.V(9).Infof("%s New CodeQualityBinding %#v", r.logPrefix(), *codeQualityBinding)
		return codeQualityBinding

	case devopsv1alpha1.TypeCodeRepoService:
		codeRepoBindingSpec, ok := toolBindingSpec.(devopsv1alpha1.CodeRepoBindingSpec)
		if !ok {
			return nil
		}

		for i := range codeRepoBindingSpec.Account.Owners {
			var owner = &codeRepoBindingSpec.Account.Owners[i]
			owner.All = len(owner.Repositories) == 0 // if repositories setted, we will set all=false
		}

		codeRepoBinding := &devopsv1alpha1.CodeRepoBinding{
			TypeMeta: metav1.TypeMeta{
				Kind:       devopsv1alpha1.ResourceKindCodeRepoBinding,
				APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			},
			ObjectMeta: r.toolBindingObjectMeta(namespace),
			Spec:       codeRepoBindingSpec,
		}
		glog.V(9).Infof("%s New CodeRepoBinding %#v", r.logPrefix(), *codeRepoBinding)
		return codeRepoBinding

	case devopsv1alpha1.TypeDocumentManagement:
		documentManagementBindingSpec, ok := toolBindingSpec.(devopsv1alpha1.DocumentManagementBindingSpec)
		if !ok {
			return nil
		}

		documentManagementBinding := &devopsv1alpha1.DocumentManagementBinding{
			TypeMeta: metav1.TypeMeta{
				Kind:       devopsv1alpha1.ResourceKindDocumentManagementBinding,
				APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			},
			ObjectMeta: r.toolBindingObjectMeta(namespace),
			Spec:       documentManagementBindingSpec,
		}
		glog.V(9).Infof("%s New DocumentManagementBinding %#v", r.logPrefix(), *documentManagementBinding)
		return documentManagementBinding

	case devopsv1alpha1.TypeImageRegistry:
		imageRegistryBindingSpec, ok := toolBindingSpec.(devopsv1alpha1.ImageRegistryBindingSpec)
		if !ok {
			return nil
		}

		imageRegistryBinding := &devopsv1alpha1.ImageRegistryBinding{
			TypeMeta: metav1.TypeMeta{
				Kind:       devopsv1alpha1.ResourceKindImageRegistryBinding,
				APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			},
			ObjectMeta: r.toolBindingObjectMeta(namespace),
			Spec:       imageRegistryBindingSpec,
		}
		glog.V(9).Infof("%s New ImageRegistryBinding %#v", r.logPrefix(), *imageRegistryBinding)
		return imageRegistryBinding

	case devopsv1alpha1.TypeJenkins:
		jenkinsBindingSpec, ok := toolBindingSpec.(devopsv1alpha1.JenkinsBindingSpec)
		if !ok {
			return nil
		}

		jenkinsBinding := &devopsv1alpha1.JenkinsBinding{
			TypeMeta: metav1.TypeMeta{
				Kind:       devopsv1alpha1.ResourceKindJenkinsBinding,
				APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			},
			ObjectMeta: r.toolBindingObjectMeta(namespace),
			Spec:       jenkinsBindingSpec,
		}
		glog.V(9).Infof("%s New JenkinsBinding %#v", r.logPrefix(), *jenkinsBinding)
		return jenkinsBinding

	case devopsv1alpha1.TypeProjectManagement:
		projectManagementBindingSpec, ok := toolBindingSpec.(devopsv1alpha1.ProjectManagementBindingSpec)
		if !ok {
			return nil
		}

		projectManagementBinding := &devopsv1alpha1.ProjectManagementBinding{

			TypeMeta: metav1.TypeMeta{
				Kind:       devopsv1alpha1.TypeProjectManagementBinding,
				APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			},
			ObjectMeta: r.toolBindingObjectMeta(namespace),
			Spec:       projectManagementBindingSpec,
		}
		glog.V(9).Infof("%s New projectManagementBinding %#v", r.logPrefix(), *projectManagementBinding)
		return projectManagementBinding

	case devopsv1alpha1.TypeArtifactRegistry:
		artifactRegistryBindingSpec, ok := toolBindingSpec.(devopsv1alpha1.ArtifactRegistryBindingSpec)
		if !ok {
			return nil
		}

		artifactRegistryBinding := &devopsv1alpha1.ArtifactRegistryBinding{

			TypeMeta: metav1.TypeMeta{
				Kind:       devopsv1alpha1.TypeArtifactRegistryBinding,
				APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			},
			ObjectMeta: r.toolBindingObjectMeta(namespace),
			Spec:       artifactRegistryBindingSpec,
		}
		glog.V(9).Infof("%s New artifactRegistryBinding %#v", r.logPrefix(), *artifactRegistryBinding)
		return artifactRegistryBinding
	}

	return nil
}

func (r *GenericReconciler) mergeToolBindingSpec(toolBinding metav1.Object, toolBindingSpec interface{}) {
	switch r.kind {
	case devopsv1alpha1.TypeCodeRepoService:
		codeRepoBinding, codeRepoBindingOK := toolBinding.(*devopsv1alpha1.CodeRepoBinding)
		codeRepoBindingSpec, codeRepoBindingSpecOK := toolBindingSpec.(devopsv1alpha1.CodeRepoBindingSpec)
		if codeRepoBindingOK && codeRepoBindingSpecOK {
			for i := range codeRepoBindingSpec.Account.Owners {
				var owner = &codeRepoBindingSpec.Account.Owners[i]
				owner.All = len(owner.Repositories) == 0 // if repositories setted, we will set all=false
			}

			codeRepoBinding.Spec.Account.Owners = append(codeRepoBinding.Spec.Account.Owners, codeRepoBindingSpec.Account.Owners...)
		}

	case devopsv1alpha1.TypeDocumentManagement:
		documentManagementBinding, documentManagementBindingOK := toolBinding.(*devopsv1alpha1.DocumentManagementBinding)
		documentManagementBindingSpec, documentManagementBindingSpecOK := toolBindingSpec.(devopsv1alpha1.DocumentManagementBindingSpec)
		if documentManagementBindingOK && documentManagementBindingSpecOK {
			documentManagementBinding.Spec.DocumentManagementSpaceRefs = append(documentManagementBinding.Spec.DocumentManagementSpaceRefs, documentManagementBindingSpec.DocumentManagementSpaceRefs...)
		}

	case devopsv1alpha1.TypeImageRegistry:
		imageRegistryBinding, imageRegistryBindingOK := toolBinding.(*devopsv1alpha1.ImageRegistryBinding)
		imageRegistryBindingSpec, imageRegistryBindingSpecOK := toolBindingSpec.(devopsv1alpha1.ImageRegistryBindingSpec)
		if imageRegistryBindingOK && imageRegistryBindingSpecOK {
			imageRegistryBinding.Spec.RepoInfo.Repositories = append(imageRegistryBinding.Spec.RepoInfo.Repositories, imageRegistryBindingSpec.RepoInfo.Repositories...)
		}

	case devopsv1alpha1.TypeProjectManagement:
		projectManagementBinding, projectManagementBindingOK := toolBinding.(*devopsv1alpha1.ProjectManagementBinding)
		projectManagementBindingSpec, projectManagementBindingSpecOK := toolBindingSpec.(devopsv1alpha1.ProjectManagementBindingSpec)
		if projectManagementBindingOK && projectManagementBindingSpecOK {
			projectManagementBinding.Spec.ProjectManagementProjectInfos = append(projectManagementBinding.Spec.ProjectManagementProjectInfos, projectManagementBindingSpec.ProjectManagementProjectInfos...)
		}
	}
}

// buildAllToolBindings we should get all resources that assign to current ToolBindingReplica and group it by namespace.
// when CodeQualityBinding was new already, we need do nothing
func (r *GenericReconciler) buildAllToolBindings(namespaceMap map[string]v1.Namespace) map[string]metav1.Object {
	toolBindingReplica := r.ToolBindingReplica
	toolBindings := map[string]metav1.Object{}

	bindIfNeed := func(namespaceName string, bindingSpecCopy interface{}) {
		toolBinding, ok := toolBindings[namespaceName]
		if !ok {
			toolBindings[namespaceName] = r.newToolBinding(namespaceMap[namespaceName], bindingSpecCopy)
			// glog.V(9).Infof("%s new [%s]'s coderepobindinding with resource: %#v", r.logPrefix(), namespaceName, binding.Spec.Account.Owners)
		} else {
			r.mergeToolBindingSpec(toolBinding, bindingSpecCopy)
			// glog.V(9).Infof("%s merge [%s]'s coderepobindinding with resource: %#v", r.logPrefix(), namespaceName, binding.Spec.Account.Owners)
		}
	}

	bindBySelector := func(bindingSpecCopy interface{}) {
		for _, namespace := range namespaceMap {
			toolBinding, ok := toolBindings[namespace.Name]
			if !ok {
				toolBindings[namespace.Name] = r.newToolBinding(namespaceMap[namespace.Name], bindingSpecCopy)
			} else {
				r.mergeToolBindingSpec(toolBinding, bindingSpecCopy)
			}
		}
	}

	switch r.kind {
	case devopsv1alpha1.TypeCodeQualityTool:
		for _, binding := range toolBindingReplica.Spec.CodeQuality.Template.Bindings {
			for _, namespaceName := range binding.Selector.NamespacesRef {
				bindIfNeed(namespaceName, *binding.Spec.DeepCopy())
			}
			if !labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				continue
			}
			// bind current resource to all namespaces specified by label selector
			bindBySelector(*binding.Spec.DeepCopy())
		}

	case devopsv1alpha1.TypeCodeRepoService:
		for _, binding := range toolBindingReplica.Spec.CodeRepoService.Template.Bindings {
			for _, namespaceName := range binding.Selector.NamespacesRef {
				bindIfNeed(namespaceName, *binding.Spec.DeepCopy())
			}
			if !labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				continue
			}
			// bind current resource to all namespaces specified by label selector
			bindBySelector(*binding.Spec.DeepCopy())
		}

	case devopsv1alpha1.TypeDocumentManagement:
		for _, binding := range toolBindingReplica.Spec.DocumentManagement.Template.Bindings {
			for _, namespaceName := range binding.Selector.NamespacesRef {
				bindIfNeed(namespaceName, *binding.Spec.DeepCopy())
			}
			if !labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				continue
			}
			// bind current resource to all namespaces specified by label selector
			bindBySelector(*binding.Spec.DeepCopy())
		}

	case devopsv1alpha1.TypeImageRegistry:
		for _, binding := range toolBindingReplica.Spec.ImageRegistry.Template.Bindings {
			for _, namespaceName := range binding.Selector.NamespacesRef {
				bindIfNeed(namespaceName, *binding.Spec.DeepCopy())
			}
			if !labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				continue
			}
			// bind current resource to all namespaces specified by label selector
			bindBySelector(*binding.Spec.DeepCopy())
		}

	case devopsv1alpha1.TypeJenkins:
		for _, binding := range toolBindingReplica.Spec.ContinuousIntegration.Template.Bindings {
			for _, namespaceName := range binding.Selector.NamespacesRef {
				bindIfNeed(namespaceName, *binding.Spec.DeepCopy())
			}
			if !labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				continue
			}
			// bind current resource to all namespaces specified by label selector
			bindBySelector(*binding.Spec.DeepCopy())
		}

	case devopsv1alpha1.TypeProjectManagement:
		for _, binding := range toolBindingReplica.Spec.ProjectManagement.Template.Bindings {
			for _, namespaceName := range binding.Selector.NamespacesRef {
				bindIfNeed(namespaceName, *binding.Spec.DeepCopy())
			}
			if !labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				continue
			}
			//bind current resource to all namespaces specified by label selector
			bindBySelector(*binding.Spec.DeepCopy())
		}

	case devopsv1alpha1.TypeArtifactRegistry:
		for _, binding := range toolBindingReplica.Spec.ArtifactRegistry.Template.Bindings {
			for _, namespaceName := range binding.Selector.NamespacesRef {
				bindIfNeed(namespaceName, *binding.Spec.DeepCopy())
			}
			if !labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				continue
			}
			//bind current resource to all namespaces specified by label selector
			bindBySelector(*binding.Spec.DeepCopy())
		}
	}
	return toolBindings
}

func (r *GenericReconciler) prepareGenerate() ([]string, metav1.LabelSelector) {
	tbr := r.ToolBindingReplica
	enumNamespaceName := []string{}
	labelSelector := metav1.LabelSelector{}

	switch r.kind {
	case devopsv1alpha1.TypeCodeQualityTool:
		for _, binding := range tbr.Spec.CodeQuality.Template.Bindings {
			if labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
			enumNamespaceName = append(enumNamespaceName, binding.Selector.NamespacesRef...)
		}

	case devopsv1alpha1.TypeCodeRepoService:
		for _, binding := range tbr.Spec.CodeRepoService.Template.Bindings {
			if labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
			enumNamespaceName = append(enumNamespaceName, binding.Selector.NamespacesRef...)
		}

	case devopsv1alpha1.TypeDocumentManagement:
		for _, binding := range tbr.Spec.DocumentManagement.Template.Bindings {
			if labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
			enumNamespaceName = append(enumNamespaceName, binding.Selector.NamespacesRef...)
		}

	case devopsv1alpha1.TypeImageRegistry:
		for _, binding := range tbr.Spec.ImageRegistry.Template.Bindings {
			if labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
			enumNamespaceName = append(enumNamespaceName, binding.Selector.NamespacesRef...)
		}

	case devopsv1alpha1.TypeJenkins:
		for _, binding := range tbr.Spec.ContinuousIntegration.Template.Bindings {
			if labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
			enumNamespaceName = append(enumNamespaceName, binding.Selector.NamespacesRef...)
		}

	case devopsv1alpha1.TypeProjectManagement:
		for _, binding := range tbr.Spec.ProjectManagement.Template.Bindings {
			if labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
			enumNamespaceName = append(enumNamespaceName, binding.Selector.NamespacesRef...)
		}

	case devopsv1alpha1.TypeArtifactRegistry:
		for _, binding := range tbr.Spec.ArtifactRegistry.Template.Bindings {
			if labelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
			enumNamespaceName = append(enumNamespaceName, binding.Selector.NamespacesRef...)
		}
	}

	return enumNamespaceName, labelSelector
}

func (r *GenericReconciler) generateBindings() ([]metav1.Object, error) {
	bindings := []metav1.Object{}

	enumNamespaceName, labelSelector := r.prepareGenerate()

	selectedNamespaces, err := r.getSelectedNamespaces(enumNamespaceName, &labelSelector)
	if err != nil {
		return bindings, err
	}

	toolBindingsMap := r.buildAllToolBindings(selectedNamespaces)

	result := []metav1.Object{}
	for _, binding := range toolBindingsMap {
		result = append(result, binding)
	}

	return result, nil
}

func (r *GenericReconciler) subtractBindings(olds []metav1.Object, news []metav1.Object) []metav1.Object {
	var inOldButNotInNew = []metav1.Object{}

	var bindingKey = func(obj metav1.Object) string {
		return fmt.Sprintf("%s/%s", obj.GetNamespace(), obj.GetName())
	}

	var newMap = make(map[string]metav1.Object, len(news))
	for _, new := range news {
		newMap[bindingKey(new)] = new
	}

	for _, old := range olds {
		if _, ok := newMap[bindingKey(old)]; !ok {
			inOldButNotInNew = append(inOldButNotInNew, old)
		}
	}

	return inOldButNotInNew
}

func (r *GenericReconciler) getBindingType() string {
	switch r.kind {
	case devopsv1alpha1.TypeCodeQualityTool:
		return devopsv1alpha1.TypeCodeQualityBinding

	case devopsv1alpha1.TypeCodeRepoService:
		return devopsv1alpha1.TypeCodeRepoBinding

	case devopsv1alpha1.TypeDocumentManagement:
		return devopsv1alpha1.TypeDocumentManagementBinding

	case devopsv1alpha1.TypeImageRegistry:
		return devopsv1alpha1.TypeImageRegistryBinding

	case devopsv1alpha1.TypeJenkins:
		return devopsv1alpha1.TypeJenkinsBinding

	case devopsv1alpha1.TypeProjectManagement:
		return devopsv1alpha1.TypeProjectManagementBinding

	case devopsv1alpha1.TypeArtifactRegistry:
		return devopsv1alpha1.TypeArtifactRegistryBinding
	}
	return ""
}

func (r *GenericReconciler) deleteBindings(bindings []metav1.Object) []devopsv1alpha1.BindingCondition {
	conditions := []devopsv1alpha1.BindingCondition{}
	toolBindingType := r.getBindingType()
	for _, binding := range bindings {
		cond := devopsv1alpha1.BindingCondition{Name: binding.GetName(), Namespace: binding.GetNamespace(), Type: devopsv1alpha1.TypeToolBinding}

		err := r.devopsToolBindingClient.DevopsV1alpha1Expansion().DevOpsToolBinding().Delete(r.kind, binding.GetNamespace(), binding.GetName(), nil)
		if err != nil {
			glog.V(5).Infof("%s Deleted %s [%s/%s] Error %s",
				r.logPrefix(), toolBindingType, binding.GetNamespace(), binding.GetName(), err.Error())
			cond.Status = devopsv1alpha1.StatusError
			cond.Reason = err.Error()
			cond.Message = err.Error()

			conditions = append(conditions, cond)
		} else {
			glog.V(5).Infof("%s Deleted %s [%s/%s] Successfully", r.logPrefix(), toolBindingType, binding.GetNamespace(), binding.GetName())
			cond.Status = devopsv1alpha1.StatusReady
		}
	}
	return conditions
}

func (r *GenericReconciler) copyToolBinding(old, new metav1.Object) metav1.Object {
	switch r.kind {
	case devopsv1alpha1.TypeCodeQualityTool:
		if codeQualityBinding, ok := old.(*devopsv1alpha1.CodeQualityBinding); ok {
			codeQualityBindingCopy := codeQualityBinding.DeepCopy()
			if codeQualityBinding, ok = new.(*devopsv1alpha1.CodeQualityBinding); ok {
				codeQualityBindingCopy.Spec = codeQualityBinding.Spec
			}
			return codeQualityBindingCopy
		}
	case devopsv1alpha1.TypeCodeRepoService:
		if codeRepoBinding, ok := old.(*devopsv1alpha1.CodeRepoBinding); ok {
			codeRepoBindingCopy := codeRepoBinding.DeepCopy()
			if codeRepoBinding, ok = new.(*devopsv1alpha1.CodeRepoBinding); ok {
				codeRepoBindingCopy.Spec = codeRepoBinding.Spec
			}
			return codeRepoBindingCopy
		}
	case devopsv1alpha1.TypeDocumentManagement:
		if documentManagementBinding, ok := old.(*devopsv1alpha1.DocumentManagementBinding); ok {
			documentManagementBindingCopy := documentManagementBinding.DeepCopy()
			if documentManagementBinding, ok = new.(*devopsv1alpha1.DocumentManagementBinding); ok {
				documentManagementBindingCopy.Spec = documentManagementBinding.Spec
			}
			return documentManagementBindingCopy
		}
	case devopsv1alpha1.TypeImageRegistry:
		if imageRegistryBinding, ok := old.(*devopsv1alpha1.ImageRegistryBinding); ok {
			imageRegistryBindingCopy := imageRegistryBinding.DeepCopy()
			if imageRegistryBinding, ok = new.(*devopsv1alpha1.ImageRegistryBinding); ok {
				imageRegistryBindingCopy.Spec = imageRegistryBinding.Spec
			}
			return imageRegistryBindingCopy
		}
	case devopsv1alpha1.TypeJenkins:
		if jenkinsBinding, ok := old.(*devopsv1alpha1.JenkinsBinding); ok {
			jenkinsBindingCopy := jenkinsBinding.DeepCopy()
			if jenkinsBinding, ok = new.(*devopsv1alpha1.JenkinsBinding); ok {
				jenkinsBindingCopy.Spec = jenkinsBinding.Spec
			}
			return jenkinsBindingCopy
		}
	case devopsv1alpha1.TypeProjectManagement:
		if projectManagementBinding, ok := old.(*devopsv1alpha1.ProjectManagementBinding); ok {
			projectManagementBindingCopy := projectManagementBinding.DeepCopy()
			if projectManagementBinding, ok = new.(*devopsv1alpha1.ProjectManagementBinding); ok {
				projectManagementBindingCopy.Spec = projectManagementBinding.Spec
			}
			return projectManagementBindingCopy
		}
	case devopsv1alpha1.TypeArtifactRegistry:
		if artifactRegistryBinding, ok := old.(*devopsv1alpha1.ArtifactRegistryBinding); ok {
			artifactRegistryBindingCopy := artifactRegistryBinding.DeepCopy()
			if artifactRegistryBinding, ok = new.(*devopsv1alpha1.ArtifactRegistryBinding); ok {
				artifactRegistryBindingCopy.Spec = artifactRegistryBinding.Spec
			}
			return artifactRegistryBindingCopy
		}
	}
	return nil
}

func (r *GenericReconciler) specChanged(old, new interface{}) bool {
	switch r.kind {
	case devopsv1alpha1.TypeCodeQualityTool:
		oldCodeQualityBinding, ok1 := old.(*devopsv1alpha1.CodeQualityBinding)
		newCodeQualityBinding, ok2 := new.(*devopsv1alpha1.CodeQualityBinding)
		if ok1 && ok2 {
			return !apiequality.Semantic.DeepEqual(oldCodeQualityBinding.Spec, newCodeQualityBinding.Spec)
		}
	case devopsv1alpha1.TypeCodeRepoService:
		oldCodeRepoBinding, ok1 := old.(*devopsv1alpha1.CodeRepoBinding)
		newCodeRepoBinding, ok2 := new.(*devopsv1alpha1.CodeRepoBinding)
		if ok1 && ok2 {
			return !apiequality.Semantic.DeepEqual(oldCodeRepoBinding.Spec, newCodeRepoBinding.Spec)
		}
	case devopsv1alpha1.TypeDocumentManagement:
		oldDocumentManagementBinding, ok1 := old.(*devopsv1alpha1.DocumentManagementBinding)
		newDocumentManagementBinding, ok2 := new.(*devopsv1alpha1.DocumentManagementBinding)
		if ok1 && ok2 {
			return !apiequality.Semantic.DeepEqual(oldDocumentManagementBinding.Spec, newDocumentManagementBinding.Spec)
		}
	case devopsv1alpha1.TypeImageRegistry:
		oldImageRegistryBinding, ok1 := old.(*devopsv1alpha1.ImageRegistryBinding)
		newImageRegistryBinding, ok2 := new.(*devopsv1alpha1.ImageRegistryBinding)
		if ok1 && ok2 {
			return !apiequality.Semantic.DeepEqual(oldImageRegistryBinding.Spec, newImageRegistryBinding.Spec)
		}
	case devopsv1alpha1.TypeJenkins:
		oldjenkinsBinding, ok1 := old.(*devopsv1alpha1.JenkinsBinding)
		newjenkinsBinding, ok2 := new.(*devopsv1alpha1.JenkinsBinding)
		if ok1 && ok2 {
			return !apiequality.Semantic.DeepEqual(oldjenkinsBinding.Spec, newjenkinsBinding.Spec)
		}
	case devopsv1alpha1.TypeProjectManagement:
		oldProjectManagementBinding, ok1 := old.(*devopsv1alpha1.ProjectManagementBinding)
		newProjectManagementBinding, ok2 := new.(*devopsv1alpha1.ProjectManagementBinding)
		if ok1 && ok2 {
			return !apiequality.Semantic.DeepEqual(oldProjectManagementBinding.Spec, newProjectManagementBinding.Spec)
		}
	case devopsv1alpha1.TypeArtifactRegistry:
		oldArtifactRegistryBinding, ok1 := old.(*devopsv1alpha1.ArtifactRegistryBinding)
		newArtifactRegistryBinding, ok2 := new.(*devopsv1alpha1.ArtifactRegistryBinding)
		if ok1 && ok2 {
			return !apiequality.Semantic.DeepEqual(oldArtifactRegistryBinding.Spec, newArtifactRegistryBinding.Spec)
		}
	}

	return false
}

func (r *GenericReconciler) createOrUpdateBindings(bindings []metav1.Object) []devopsv1alpha1.BindingCondition {
	conditions := []devopsv1alpha1.BindingCondition{}
	toolBindingType := r.getBindingType()

	for _, binding := range bindings {
		cond := devopsv1alpha1.BindingCondition{Name: binding.GetName(), Namespace: binding.GetNamespace(), Type: devopsv1alpha1.TypeToolBinding}

		found, err := r.devopsToolBindingClient.DevopsV1alpha1Expansion().DevOpsToolBinding().Get(r.kind, binding.GetNamespace(), binding.GetName(), metav1.GetOptions{ResourceVersion: "0"})
		if err == nil && r.specChanged(found, binding) {
			foundCopy := r.copyToolBinding(found, binding)
			_, err = r.devopsToolBindingClient.DevopsV1alpha1Expansion().DevOpsToolBinding().Update(r.kind, binding.GetNamespace(), foundCopy)
			glog.Infof("%s Updated %s [%s/%s], Error:%#v",
				r.logPrefix(), toolBindingType, binding.GetNamespace(), binding.GetName(), err)

		} else if errors.IsNotFound(err) {
			_, err = r.devopsToolBindingClient.DevopsV1alpha1Expansion().DevOpsToolBinding().Create(r.kind, binding.GetNamespace(), binding)
			glog.Infof("%s Created %s [%s/%s], Error:%#v",
				r.logPrefix(), toolBindingType, binding.GetNamespace(), binding.GetName(), err)
		}

		// conflict error can be ignored
		// err code: 409
		// err msg : "Operation cannot be fulfilled ... the object has been modified; please apply your changes to the latest version and try again"
		if err != nil && !isOptimisticLockError(err) {
			glog.Errorf("%s Created/Updated %s:%#v, error:%#v", r.logPrefix(), toolBindingType, binding, err)
			cond.Status = devopsv1alpha1.StatusError
			cond.Reason = err.Error()
			cond.Message = err.Error()
		} else {
			cond.Status = devopsv1alpha1.StatusReady
			cond.Reason = ""
			cond.Message = ""
		}

		conditions = append(conditions, cond)
	}
	return conditions
}

func isOptimisticLockError(err error) bool {
	return errors.IsConflict(err) && strings.Contains(err.Error(), genericregistry.OptimisticLockErrorMsg)
}

func errorLabelSelectorAsSelector(labelSelector *metav1.LabelSelector, err error) error {
	glog.Errorf("try convert labelselector %#v as selector, got errors:%#v", labelSelector, err)
	return err
}

func (r *GenericReconciler) logPrefix() string {
	return fmt.Sprintf("ToolBindingReplica [%s/%s]: ", r.ToolBindingReplica.Namespace, r.ToolBindingReplica.Name)
}

func labelSelectorIsNotEmpty(labelSelector *metav1.LabelSelector) bool {
	return len(labelSelector.MatchLabels) > 0 || len(labelSelector.MatchExpressions) > 0
}

func (r *GenericReconciler) getSelectedNamespaces(namespacsRef []string, labelSelector *metav1.LabelSelector) (map[string]v1.Namespace, error) {
	namespaces := map[string]v1.Namespace{}

	for _, namespaceName := range namespacsRef {
		namespace, err := r.k8sClient.CoreV1().Namespaces().Get(namespaceName, metav1.GetOptions{ResourceVersion: "0"})
		if err != nil {
			glog.Errorf("%s Get namespace '%s' error:%s", r.logPrefix(), namespaceName, err.Error())
			return namespaces, err
		}

		namespaces[namespaceName] = *namespace
	}

	if !labelSelectorIsNotEmpty(labelSelector) {
		glog.V(9).Infof("%s selected namespaces is %#v", r.logPrefix(), namespaces)
		return namespaces, nil
	}

	selector, err := metav1.LabelSelectorAsSelector(labelSelector)
	if err != nil {
		glog.Errorf("%s Transform label selector as selector error, labelSelector:%#v, error:%#v", r.logPrefix(), labelSelector, err)
		return namespaces, err
	}
	namespaceList, err := r.k8sClient.CoreV1().Namespaces().List(metav1.ListOptions{
		LabelSelector:   selector.String(),
		ResourceVersion: "0",
	})
	if err != nil {
		glog.Errorf("%s List namespaces by label selector:%s error:%#v", r.logPrefix(), selector.String(), err)
		return namespaces, err
	}

	for _, namespace := range namespaceList.Items {
		if _, ok := namespaces[namespace.Name]; !ok {
			namespaces[namespace.Name] = namespace
		}
	}

	glog.V(9).Infof("%s selected namespaces is %#v", r.logPrefix(), namespaces)
	return namespaces, nil
}

func (r *GenericReconciler) toolBindingOwnerReferences() []metav1.OwnerReference {
	return []metav1.OwnerReference{
		*metav1.NewControllerRef(&r.ToolBindingReplica, schema.GroupVersionKind{
			Group:   devopsv1alpha1.GroupName,
			Version: currentVersion,
			Kind:    devopsv1alpha1.TypeToolBindingReplica,
		}),
	}
}

func (r *GenericReconciler) toolBindingObjectMeta(namespace v1.Namespace) metav1.ObjectMeta {
	if namespace.Labels == nil {
		namespace.Labels = make(map[string]string)
	}

	meta := metav1.ObjectMeta{
		Name:        r.ToolBindingReplica.Name,
		Namespace:   namespace.Name,
		Labels:      namespace.Labels, // copy labels of namespace to ToolBinding Labels
		Annotations: namespace.Annotations,
	}

	if meta.Annotations == nil {
		meta.Annotations = map[string]string{}
	}
	// copy labels to annotations
	for key, value := range namespace.Labels {
		err := validation.IsValidLabelValue(value)
		if len(err) == 0 {
			meta.Annotations[key] = value
		}
	}

	for key, value := range r.ToolBindingReplica.ObjectReferenceLabel(r.provider) {
		meta.Labels[key] = value
	}

	return meta
}
