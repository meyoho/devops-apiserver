package reconciler

import (
	"testing"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestSubtractCodeRepoBindings(t *testing.T) {
	type table struct {
		olds  []devopsv1alpha1.CodeRepoBinding
		news  []devopsv1alpha1.CodeRepoBinding
		toDel []devopsv1alpha1.CodeRepoBinding
	}
	var data = []table{
		table{
			olds:  []devopsv1alpha1.CodeRepoBinding{},
			news:  []devopsv1alpha1.CodeRepoBinding{},
			toDel: []devopsv1alpha1.CodeRepoBinding{},
		},
		table{
			olds: []devopsv1alpha1.CodeRepoBinding{},
			news: []devopsv1alpha1.CodeRepoBinding{
				devopsv1alpha1.CodeRepoBinding{
					TypeMeta: metav1.TypeMeta{
						Kind: devopsv1alpha1.TypeCodeRepoBinding,
					},
					ObjectMeta: metav1.ObjectMeta{
						Namespace: "ns-0",
						Name:      "binding-0",
					},
				},
			},
			toDel: []devopsv1alpha1.CodeRepoBinding{},
		},
		table{
			olds: []devopsv1alpha1.CodeRepoBinding{
				devopsv1alpha1.CodeRepoBinding{
					TypeMeta: metav1.TypeMeta{
						Kind: devopsv1alpha1.TypeCodeRepoBinding,
					},
					ObjectMeta: metav1.ObjectMeta{
						Namespace: "ns-0",
						Name:      "binding-0",
					},
				},
			},
			news: []devopsv1alpha1.CodeRepoBinding{},
			toDel: []devopsv1alpha1.CodeRepoBinding{
				devopsv1alpha1.CodeRepoBinding{
					TypeMeta: metav1.TypeMeta{
						Kind: devopsv1alpha1.TypeCodeRepoBinding,
					},
					ObjectMeta: metav1.ObjectMeta{
						Namespace: "ns-0",
						Name:      "binding-0",
					},
				},
			},
		},
		table{
			olds: []devopsv1alpha1.CodeRepoBinding{
				devopsv1alpha1.CodeRepoBinding{
					TypeMeta: metav1.TypeMeta{
						Kind: devopsv1alpha1.TypeCodeRepoBinding,
					},
					ObjectMeta: metav1.ObjectMeta{
						Namespace: "ns-0",
						Name:      "binding-0",
					},
				},
			},
			news: []devopsv1alpha1.CodeRepoBinding{
				devopsv1alpha1.CodeRepoBinding{
					TypeMeta: metav1.TypeMeta{
						Kind: devopsv1alpha1.TypeCodeRepoBinding,
					},
					ObjectMeta: metav1.ObjectMeta{
						Namespace: "ns-1",
						Name:      "binding-1",
					},
				},
			},
			toDel: []devopsv1alpha1.CodeRepoBinding{
				devopsv1alpha1.CodeRepoBinding{
					TypeMeta: metav1.TypeMeta{
						Kind: devopsv1alpha1.TypeCodeRepoBinding,
					},
					ObjectMeta: metav1.ObjectMeta{
						Namespace: "ns-0",
						Name:      "binding-0",
					},
				},
			},
		},
		table{
			olds: []devopsv1alpha1.CodeRepoBinding{
				devopsv1alpha1.CodeRepoBinding{
					TypeMeta: metav1.TypeMeta{
						Kind: devopsv1alpha1.TypeCodeRepoBinding,
					},
					ObjectMeta: metav1.ObjectMeta{
						Namespace: "ns-0",
						Name:      "binding-0",
					},
				},
			},
			news: []devopsv1alpha1.CodeRepoBinding{
				devopsv1alpha1.CodeRepoBinding{
					TypeMeta: metav1.TypeMeta{
						Kind: devopsv1alpha1.TypeCodeRepoBinding,
					},
					ObjectMeta: metav1.ObjectMeta{
						Namespace: "ns-0",
						Name:      "binding-0",
					},
				},
			},
			toDel: []devopsv1alpha1.CodeRepoBinding{},
		},
	}

	for i, tbl := range data {
		r := &GenericReconciler{}
		var oldObjects, newObjects []metav1.Object
		for _, old := range tbl.olds {
			oldObjects = append(oldObjects, &old)
		}
		for _, new := range tbl.news {
			newObjects = append(newObjects, &new)
		}
		actual := r.subtractBindings(oldObjects, newObjects)
		if len(actual) != len(tbl.toDel) {
			t.Errorf("case %d assert error, %d != %d , actual is %#v", i, len(actual), len(tbl.toDel), actual)
		}
		if len(actual) > 0 {
			if actual[0].GetName() != tbl.toDel[0].Name {
				t.Errorf("case %d assert error, %s != %s , actual is %#v", i, actual[0].GetName(), tbl.toDel[0].Name, actual)
			}
		}
	}
}

func TestNewReconciler(t *testing.T) {

	codeRepoService := devopsv1alpha1.ToolBindingReplica{
		TypeMeta: metav1.TypeMeta{
			Kind: devopsv1alpha1.TypeCodeRepoService,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "case1",
			Namespace: "ns1",
		},
		Spec: devopsv1alpha1.ToolBindingReplicaSpec{
			Selector: devopsv1alpha1.ToolSelector{
				ObjectReference: corev1.ObjectReference{
					Kind: devopsv1alpha1.TypeCodeRepoService,
					Name: "github",
				},
			},
		},
	}

	jenkins := devopsv1alpha1.ToolBindingReplica{
		TypeMeta: metav1.TypeMeta{
			Kind: devopsv1alpha1.TypeJenkins,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "case2",
			Namespace: "ns2",
		},
		Spec: devopsv1alpha1.ToolBindingReplicaSpec{
			Selector: devopsv1alpha1.ToolSelector{
				ObjectReference: corev1.ObjectReference{
					Kind: devopsv1alpha1.TypeJenkins,
					Name: "jenkins",
				},
			},
		},
	}

	projectManagement := devopsv1alpha1.ToolBindingReplica{
		TypeMeta: metav1.TypeMeta{
			Kind: devopsv1alpha1.TypeProjectManagement,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "case3",
			Namespace: "ns3",
		},
		Spec: devopsv1alpha1.ToolBindingReplicaSpec{
			Selector: devopsv1alpha1.ToolSelector{
				ObjectReference: corev1.ObjectReference{
					Kind: devopsv1alpha1.TypeProjectManagement,
					Name: "jira",
				},
			},
		},
	}

	imageRegistry := devopsv1alpha1.ToolBindingReplica{
		TypeMeta: metav1.TypeMeta{
			Kind: devopsv1alpha1.TypeImageRegistry,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "case4",
			Namespace: "ns4",
		},
		Spec: devopsv1alpha1.ToolBindingReplicaSpec{
			Selector: devopsv1alpha1.ToolSelector{
				ObjectReference: corev1.ObjectReference{
					Kind: devopsv1alpha1.TypeImageRegistry,
					Name: "harbor",
				},
			},
		},
	}

	documentManagement := devopsv1alpha1.ToolBindingReplica{
		TypeMeta: metav1.TypeMeta{
			Kind: devopsv1alpha1.TypeDocumentManagement,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "case5",
			Namespace: "ns5",
		},
		Spec: devopsv1alpha1.ToolBindingReplicaSpec{
			Selector: devopsv1alpha1.ToolSelector{
				ObjectReference: corev1.ObjectReference{
					Kind: devopsv1alpha1.TypeDocumentManagement,
					Name: "confluence",
				},
			},
		},
	}

	artifactRegistry := devopsv1alpha1.ToolBindingReplica{
		TypeMeta: metav1.TypeMeta{
			Kind: devopsv1alpha1.TypeArtifactRegistry,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "case6",
			Namespace: "ns6",
		},
		Spec: devopsv1alpha1.ToolBindingReplicaSpec{
			Selector: devopsv1alpha1.ToolSelector{
				ObjectReference: corev1.ObjectReference{
					Kind: devopsv1alpha1.TypeArtifactRegistry,
					Name: "maven2",
				},
			},
		},
	}

	type castData struct {
		getReconciler   func() (Interface, error)
		readyReconciler func() (Interface, error)
	}

	provider := devopsv1alpha1.NewAnnotationProvider(devopsv1alpha1.UsedBaseDomain)
	var table = []castData{
		{
			getReconciler: func() (Interface, error) {
				r, err := NewReconciler(codeRepoService, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
			readyReconciler: func() (Interface, error) {
				codeRepoService.Spec.CodeRepoService = &devopsv1alpha1.CodeRepoBindingReplicaTemplate{
					Template: &devopsv1alpha1.CodeRepoBindingReplicaTemplateSpec{
						Bindings: []devopsv1alpha1.CodeRepoBindingTemplate{
							devopsv1alpha1.CodeRepoBindingTemplate{},
						},
					},
				}
				r, err := NewReconciler(codeRepoService, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
		},

		{
			getReconciler: func() (Interface, error) {
				r, err := NewReconciler(jenkins, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
			readyReconciler: func() (Interface, error) {
				jenkins.Spec.ContinuousIntegration = &devopsv1alpha1.CIBindingReplicaTemplate{
					Template: &devopsv1alpha1.CIBindingReplicaTemplateSpec{
						Bindings: []devopsv1alpha1.CIBindingTemplate{
							devopsv1alpha1.CIBindingTemplate{},
						},
					},
				}
				r, err := NewReconciler(jenkins, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
		},

		{
			getReconciler: func() (Interface, error) {
				r, err := NewReconciler(projectManagement, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
			readyReconciler: func() (Interface, error) {
				projectManagement.Spec.ProjectManagement = &devopsv1alpha1.ProjectManagementBindingReplicaTemplate{
					Template: &devopsv1alpha1.ProjectManagementBindingReplicaTemplateSpec{
						Bindings: []devopsv1alpha1.ProjectManagementBindingTemplate{
							devopsv1alpha1.ProjectManagementBindingTemplate{},
						},
					},
				}
				r, err := NewReconciler(projectManagement, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
		},

		{
			getReconciler: func() (Interface, error) {
				r, err := NewReconciler(imageRegistry, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
			readyReconciler: func() (Interface, error) {
				imageRegistry.Spec.ImageRegistry = &devopsv1alpha1.ImageRegistryBindingReplicaTemplate{
					Template: &devopsv1alpha1.ImageRegistryBindingReplicaTemplateSpec{
						Bindings: []devopsv1alpha1.ImageRegistryBindingTemplate{
							devopsv1alpha1.ImageRegistryBindingTemplate{},
						},
					},
				}
				r, err := NewReconciler(imageRegistry, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
		},

		{
			getReconciler: func() (Interface, error) {
				r, err := NewReconciler(documentManagement, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
			readyReconciler: func() (Interface, error) {
				documentManagement.Spec.DocumentManagement = &devopsv1alpha1.DocumentManagementBindingReplicaTemplate{
					Template: &devopsv1alpha1.DocumentManagementBindingReplicaTemplateSpec{
						Bindings: []devopsv1alpha1.DocumentManagementBindingTemplate{
							devopsv1alpha1.DocumentManagementBindingTemplate{},
						},
					},
				}
				r, err := NewReconciler(documentManagement, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
		},

		{
			getReconciler: func() (Interface, error) {
				r, err := NewReconciler(artifactRegistry, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
			readyReconciler: func() (Interface, error) {
				artifactRegistry.Spec.ArtifactRegistry = &devopsv1alpha1.ArtifactRegistryBindingReplicaTemplate{
					Template: &devopsv1alpha1.ArtifactRegistryBindingReplicaTemplateSpec{
						Bindings: []devopsv1alpha1.ArtifactRegistryBindingTemplate{
							devopsv1alpha1.ArtifactRegistryBindingTemplate{},
						},
					},
				}
				r, err := NewReconciler(artifactRegistry, nil, nil, provider)
				reconciler, _ := r.(*GenericReconciler)
				return reconciler, err
			},
		},
	}

	for _, item := range table {
		reconciler, err := item.getReconciler()
		assert.NotNil(t, reconciler)
		assert.Nil(t, err)
		assert.False(t, reconciler.ReadyToReconcile())

		reconciler, err = item.readyReconciler()
		assert.NotNil(t, reconciler)
		assert.Nil(t, err)
		assert.True(t, reconciler.ReadyToReconcile())
	}

}
