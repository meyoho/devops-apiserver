package toolbindingreplica

import (
	"encoding/json"
	"fmt"
	"time"

	"sigs.k8s.io/controller-runtime/pkg/event"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/devops/toolbindingreplica/reconciler"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	devopshandler "alauda.io/devops-apiserver/pkg/controller/handler"
	"alauda.io/devops-apiserver/pkg/controller/manager"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

// TODO replace DevOpsClient and kubeClient with client.Client so that we can get resources from cache
const controllerName = "devops-toolbindingreplica-controller"
const (
	// TTLBindingCheck is check interval
	TTLBindingCheck = time.Minute * 3
	currentVersion  = "v1alpha1"
	syncPeriod      = time.Minute * 5
	maxRetries      = 15
)

// Add adds new reconciler
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

// NewReconcilerByManager creates a reconciler by manager
func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {

	return &Reconciler{
		Client:           mgr.GetKubeClient(),
		DevOpsClient:     clientset.NewExpansion(mgr.GetDevOpsClient()),
		ThirdPartyClient: mgr.GetThirdParty(),
		Provider:         mgr.GetAnnotationProvider(),
	}
}

// NewReconciler creates a new reconciler
func NewReconciler(kubeClient kubernetes.Interface, devopsClient clientset.Interface, thirdPartyClient thirdparty.Interface) reconcile.Reconciler {
	return &Reconciler{
		Client:           kubeClient,
		DevOpsClient:     clientset.NewExpansion(devopsClient),
		ThirdPartyClient: thirdPartyClient,
	}
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	// watch normal event
	if err = ctrl.Watch(&source.Kind{
		Type: &devopsv1alpha1.ToolBindingReplica{}},
		&handler.EnqueueRequestForObject{},
	); err != nil {
		return
	}

	if err = ctrl.Watch(&source.Kind{
		Type: &corev1.Namespace{}},
		&devopshandler.FuncEnqueueRequestForObject{
			CreateFunc: func(evt event.CreateEvent) {
				r.(*Reconciler).reconcileToolBindingTriggerByNamespace(evt.Meta)
			},
		},
	); err != nil {
		return
	}

	return
}

// Reconciler sync
type Reconciler struct {
	Client           kubernetes.Interface
	DevOpsClient     clientset.InterfaceExpansion
	ThirdPartyClient thirdparty.Interface
	Provider         devopsv1alpha1.AnnotationProvider
}

var _ reconcile.Reconciler = &Reconciler{}

// Reconcile starts sync
func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	glog.V(5).Infof("[%s] Reconciling %s", controllerName, request)
	namespace, name := request.Namespace, request.Name
	var (
		needSync           bool
		toolbindingreplica *devopsv1alpha1.ToolBindingReplica
	)
	needSync, toolbindingreplica, err = rec.selfCheck(namespace, name)
	if err != nil {
		return
	}

	if !needSync {
		glog.V(5).Infof("ToolBindingReplica[%s/%s] skip this sync", namespace, name)
		return
	}

	toolbindingreplicaCopy := toolbindingreplica.DeepCopy()
	toolbindingreplicaCopy.Status.Conditions = []devopsv1alpha1.BindingCondition{}

	if !toolbindingreplicaCopy.GetDeletionTimestamp().IsZero() && len(toolbindingreplicaCopy.GetFinalizers()) != 0 {
		err = rec.finalize(toolbindingreplicaCopy)
		glog.V(5).Infof("ToolBindingReplica[%s/%s] complete finalize, err: %#v", toolbindingreplicaCopy.Namespace, toolbindingreplicaCopy.Name, err)
		return
	}

	// if it has been deleted, not need reconcile
	if !toolbindingreplicaCopy.GetDeletionTimestamp().IsZero() && len(toolbindingreplicaCopy.GetFinalizers()) == 0 {
		return
	}

	if len(toolbindingreplicaCopy.GetFinalizers()) == 0 {
		toolbindingreplicaCopy.SetFinalizers([]string{controllerName})
		_, err = rec.DevOpsClient.DevopsV1alpha1().ToolBindingReplicas(toolbindingreplicaCopy.Namespace).Update(toolbindingreplicaCopy)
		if err != nil {
			glog.Errorf("ToolBindingReplica[%s/%s] set finalizers err: %s", toolbindingreplicaCopy.Namespace, toolbindingreplicaCopy.Name, err.Error())
		}
		return
	}

	// what ever, we should update toolbindingreplica
	defer func() {
		glog.V(5).Infof("after all syncing, toolbindingreplica[%s/%s] status.phase:%s , status.reason:%s, err: %#v",
			toolbindingreplicaCopy.Namespace, toolbindingreplicaCopy.Name, toolbindingreplicaCopy.Status.Phase, toolbindingreplicaCopy.Status.Reason, err)

		updateError := rec.updateStatus(toolbindingreplicaCopy, err)
		if err == nil && updateError != nil {
			err = updateError
		}
	}()

	// reconcile tool status in toolbindingreplica
	var tool devopsv1alpha1.ToolInterface
	tool, err = rec.reconcileTool(toolbindingreplicaCopy)
	if err != nil {
		runtime.HandleError(err)
		return
	}

	// reconcile secret status in toolbindingreplica
	_, err = rec.reconcileSecret(toolbindingreplicaCopy, tool)
	if err != nil {
		runtime.HandleError(err)
		return
	}

	// at last, we will sync tool bindings according toolbindingtemplate
	err = rec.reconcileToolBindings(toolbindingreplicaCopy)
	if err != nil {
		runtime.HandleError(err)
		return
	}

	return
}

// finalize gc
func (rec *Reconciler) finalize(tbrCopy *devopsv1alpha1.ToolBindingReplica) error {
	// call reconciler.Reconcile
	reconciler, err := reconciler.NewReconciler(*tbrCopy, rec.DevOpsClient, rec.Client, rec.Provider)
	if err != nil {
		return err
	}

	_, err = reconciler.Reconcile()
	return err
}

func (rec *Reconciler) newNamespaceNeedReconcileToolBinding(labelSelector metav1.LabelSelector, name string) bool {
	selector, err := metav1.LabelSelectorAsSelector(&labelSelector)
	if err != nil {
		glog.Errorf("[%s] newNamespaceNeedReconcileToolBinding LabelSelectorAsSelector err :%s\n", controllerName, err.Error())
		return false
	}

	namespaceList, err := rec.Client.Core().Namespaces().List(metav1.ListOptions{
		LabelSelector:   selector.String(),
		ResourceVersion: "0",
	})
	if err != nil {
		glog.Errorf("[%s] newNamespaceNeedReconcileToolBinding list namespaces err: %s\n", controllerName, err.Error())
		return false
	}

	for _, namespace := range namespaceList.Items {
		if namespace.GetName() == name {
			return true
		}
	}

	return false
}

// reconcileToolBindingTriggerByNamespace
// when create new namespace after toolbindingreplica
// trigger sync tool binding fast.
func (rec *Reconciler) reconcileToolBindingTriggerByNamespace(obj metav1.Object) {
	// list all toolbindingreplica
	toolBindingReplicaList, err := rec.DevOpsClient.DevopsV1alpha1().ToolBindingReplicas("").List(devopsv1alpha1.ListOptions())
	if err != nil {
		glog.Errorf("List ToolBindingReplica due to syncToolBindingTriggerByNamespace fail, err: %s", err.Error())
	}
	for _, toolBindingReplica := range toolBindingReplicaList.Items {

		if !reconciler.NewGenericReconciler(toolBindingReplica, rec.DevOpsClient, rec.Client, rec.Provider).ReadyToReconcile() {
			continue
		}

		labelSelector := toolBindingReplica.GetLabelSelector()
		if rec.newNamespaceNeedReconcileToolBinding(labelSelector, obj.GetName()) {
			copy := toolBindingReplica.DeepCopy()
			// just need sync tool binding
			rec.reconcileToolBindings(copy)
		}
	}
}

func (rec *Reconciler) selfCheck(namespace, name string) (needSync bool, toolbindingreplica *devopsv1alpha1.ToolBindingReplica, unrecoverableErr error) {
	toolbindingreplica, err := rec.DevOpsClient.DevopsV1alpha1().ToolBindingReplicas(namespace).Get(name, devopsv1alpha1.GetOptions())

	if err != nil {
		if errors.IsNotFound(err) {
			runtime.HandleError(err)
			return false, toolbindingreplica, nil
		}
		runtime.HandleError(err)
		return false, toolbindingreplica, err
	}

	glog.V(9).Infof("find toolbindingreplica[%s/%s]", toolbindingreplica.Namespace, toolbindingreplica.Name)
	glog.V(9).Infof("toolbindingreplica %#v", toolbindingreplica)
	if isCheckedInLastAttempt(toolbindingreplica.Status.LastAttempt, TTLBindingCheck) && toolbindingreplica.GetDeletionTimestamp().IsZero() {
		glog.V(5).Infof("ToolBindingReplica %s was checked in the last %v", toolbindingreplica.GetName(), TTLBindingCheck)
		return false, toolbindingreplica, nil
	}

	return true, toolbindingreplica, nil
}

func isCheckedInLastAttempt(lastAttempt *metav1.Time, ttlCheck time.Duration) bool {
	if lastAttempt == nil {
		return false
	}
	return lastAttempt.Add(ttlCheck).After(time.Now())
}

func (rec *Reconciler) updateStatus(toolBindingReplica *devopsv1alpha1.ToolBindingReplica, err error) error {
	if err != nil {
		toolBindingReplica.Status.Phase = devopsv1alpha1.NewToolBindingReplicaPhase(devopsv1alpha1.ServiceStatusPhaseError)
		toolBindingReplica.Status.Reason = err.Error()
		toolBindingReplica.Status.Message = err.Error()
	} else {
		toolBindingReplica.AggregateStatus()
	}
	_now := metav1.NewTime(time.Now())
	toolBindingReplica.Status.LastAttempt = &_now
	_, err = rec.DevOpsClient.DevopsV1alpha1().ToolBindingReplicas(toolBindingReplica.Namespace).UpdateStatus(toolBindingReplica)
	if err != nil {
		toolBindingReplicaJSON, _ := json.Marshal(toolBindingReplica)
		glog.Errorf("Error to update status of ToolBindingReplica '%s/%s', err: %s, ToolBindingReplica: %s", toolBindingReplica.Namespace, toolBindingReplica.Name, err.Error(), string(toolBindingReplicaJSON))
		runtime.HandleError(err)
	}
	glog.V(5).Infof("Synced toolbindingreplica[%s/%s], err: %v", toolBindingReplica.Namespace, toolBindingReplica.Name, err)
	return err
}

func (rec *Reconciler) getToolObject(toolSelector devopsv1alpha1.ToolSelector) (devopsv1alpha1.ToolInterface, error) {
	return rec.DevOpsClient.DevopsV1alpha1Expansion().DevOpsTool().Get(toolSelector.Kind, toolSelector.Name, devopsv1alpha1.GetOptions())
}

func (rec *Reconciler) reconcileTool(toolbindingreplicaCopy *devopsv1alpha1.ToolBindingReplica) (devopsv1alpha1.ToolInterface, error) {

	cond := devopsv1alpha1.BindingCondition{Type: toolbindingreplicaCopy.Spec.Selector.Kind, Name: toolbindingreplicaCopy.Spec.Selector.Name, Namespace: toolbindingreplicaCopy.Spec.Selector.Namespace}
	tool, err := rec.getToolObject(toolbindingreplicaCopy.Spec.Selector)

	if err != nil {
		if errors.IsNotFound(err) {
			cond.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
			cond.Reason = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
			cond.Message = err.Error()

			toolbindingreplicaCopy.Status.Conditions = append(toolbindingreplicaCopy.Status.Conditions, cond)
			return nil, nil
		}
		return nil, err
	}

	// no error
	if tool == nil {
		errMsg := fmt.Sprintf("tool '%s/%s' is not found", toolbindingreplicaCopy.Spec.Selector.Namespace, toolbindingreplicaCopy.Spec.Selector.Name)
		cond.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
		cond.Reason = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
		cond.Message = errMsg
	} else {
		cond.Status = string(tool.GetStatus().Phase)
		cond.Reason = tool.GetStatus().Reason
		cond.Message = tool.GetStatus().Message
	}

	toolbindingreplicaCopy.Status.Conditions = append(toolbindingreplicaCopy.Status.Conditions, cond)
	return tool, nil
}

func (rec *Reconciler) reconcileSecret(toolbindingreplicaCopy *devopsv1alpha1.ToolBindingReplica, tool devopsv1alpha1.ToolInterface) (*corev1.Secret, error) {

	secret, err := rec.Client.CoreV1().Secrets(toolbindingreplicaCopy.Spec.Secret.Namespace).Get(toolbindingreplicaCopy.Spec.Secret.Name, devopsv1alpha1.GetOptions())

	cond := devopsv1alpha1.BindingCondition{Type: devopsv1alpha1.TypeSecret, Name: toolbindingreplicaCopy.Spec.Secret.Name, Namespace: toolbindingreplicaCopy.Spec.Secret.Namespace}

	if err != nil {
		if errors.IsNotFound(err) {
			cond.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
			cond.Reason = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
			cond.Message = err.Error()

			toolbindingreplicaCopy.Status.Conditions = append(toolbindingreplicaCopy.Status.Conditions, cond)
			return nil, nil
		}
		return nil, err
	}

	if secret == nil {
		errMsg := fmt.Sprintf("secret '%s/%s' is not found", toolbindingreplicaCopy.Spec.Secret.Namespace, toolbindingreplicaCopy.Spec.Secret.Name)
		cond.Status = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
		cond.Reason = devopsv1alpha1.JenkinsBindingStatusConditionStatusNotFound
		cond.Message = errMsg

		toolbindingreplicaCopy.Status.Conditions = append(toolbindingreplicaCopy.Status.Conditions, cond)
		return nil, nil
	}

	// secret is exists and it is not same as tool, we should authorize the secret
	if tool != nil && toolbindingreplicaCopy.Spec.Secret.Namespace != tool.GetToolSpec().Secret.Namespace || toolbindingreplicaCopy.Spec.Secret.Name != tool.GetToolSpec().Secret.Name {
		toolSelector := toolbindingreplicaCopy.Spec.Selector
		authResponse, err := rec.DevOpsClient.DevopsV1alpha1Expansion().DevOpsTool().Authorize(toolSelector.Kind, toolSelector.Name, &devopsv1alpha1.CodeRepoServiceAuthorizeOptions{
			SecretName: toolbindingreplicaCopy.Spec.Secret.Name,
			Namespace:  toolbindingreplicaCopy.Spec.Secret.Namespace,
		})
		cond = generic.ConvertCodeRepoServiceAuthorizationToCondition(toolSelector.Name, toolSelector.Namespace, authResponse, err)
	} else {
		// secret is same as secret of tool
		cond.Status = devopsv1alpha1.StatusReady
		cond.Reason = ""
		cond.Message = ""
	}

	toolbindingreplicaCopy.Status.Conditions = append(toolbindingreplicaCopy.Status.Conditions, cond)
	return secret, nil
}

// reconcileToolBinding will create all tool bindings that needed, and push status to ready if everything works well
func (rec *Reconciler) reconcileToolBindings(toolbindingreplicaCopy *devopsv1alpha1.ToolBindingReplica) error {
	// before sync toolbindings , we must ensure there is no errors in ToolBindingReplica
	var existsError = toolbindingreplicaCopy.AggregateStatus()
	if existsError {
		glog.Warningf("toolbindingreplica [%s/%s] exists error, will not sync tool bindings. reason: %s",
			toolbindingreplicaCopy.Namespace, toolbindingreplicaCopy.Name, toolbindingreplicaCopy.Status.Reason)
		glog.V(7).Infof("toolbindingreplica [%s/%s] stauts is:%#v ", toolbindingreplicaCopy.Namespace, toolbindingreplicaCopy.Name, toolbindingreplicaCopy.Status)
		return nil // error will in conditions
	}

	reconciler, err := reconciler.NewReconciler(*toolbindingreplicaCopy, rec.DevOpsClient, rec.Client, rec.Provider)
	if err != nil {
		return err
	}

	if !reconciler.ReadyToReconcile() {
		glog.V(5).Infof("toolbindingreplica [%s/%s] has no bindings, will not sync tool bindings, status will go back to creating", toolbindingreplicaCopy.Namespace, toolbindingreplicaCopy.Name)
		// no matter the status that current resource in, we will set it to creating.
		toolbindingreplicaCopy.Status.Phase = devopsv1alpha1.NewToolBindingReplicaPhase(devopsv1alpha1.ServiceStatusPhaseCreating)
		toolbindingreplicaCopy.Status.Reason = ""
		toolbindingreplicaCopy.Status.Message = ""
		return nil
	}

	// sync toolbindings
	glog.V(7).Infof("toolbindingreplica [%s/%s] no error condition exists, will sync tool bindings", toolbindingreplicaCopy.Namespace, toolbindingreplicaCopy.Name)
	conditions, err := reconciler.Reconcile()
	if err != nil {
		return err
	}
	toolbindingreplicaCopy.Status.Conditions = appendToolBindingConditions(toolbindingreplicaCopy.Status.Conditions, conditions)

	// aggregate all conditions ,and try to push status to next
	if toolbindingreplicaCopy.AggregateStatus() { // some errors in toolbindingreplica
		glog.Errorf("aggregate all condition of toolbindingreplica [%s/%s] , found some errors", toolbindingreplicaCopy.Namespace, toolbindingreplicaCopy.Name)
		return fmt.Errorf(toolbindingreplicaCopy.Status.Reason)
	}

	toolbindingreplicaCopy.Status.Phase = devopsv1alpha1.NewToolBindingReplicaPhase(devopsv1alpha1.ServiceStatusPhaseReady)
	toolbindingreplicaCopy.Status.Reason = ""
	toolbindingreplicaCopy.Status.Message = ""
	return nil
}

func appendToolBindingConditions(conditions []devopsv1alpha1.BindingCondition, toolBindingConditions []devopsv1alpha1.BindingCondition) (appended []devopsv1alpha1.BindingCondition) {
	appended = []devopsv1alpha1.BindingCondition{}
	for _, cond := range conditions {
		if cond.Type == devopsv1alpha1.TypeToolBinding {
			continue
		}
		appended = append(appended, cond)
	}

	appended = append(appended, toolBindingConditions...)
	return appended
}
