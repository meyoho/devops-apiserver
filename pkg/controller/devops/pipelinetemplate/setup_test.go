package pipelinetemplate_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/pipelinetemplate"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestPipelineTemplateController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("pipelinetemplate.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/pipelinetemplate", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"pipelinetemplate-controller",
		pipelinetemplate.Add,
		&source.Kind{Type: &v1alpha1.PipelineTemplate{}},
		&handler.EnqueueRequestForObject{},
	),
)
