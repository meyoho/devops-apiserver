package pipelinetemplate_test

import (
	"time"

	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"k8s.io/apimachinery/pkg/api/errors"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/devops/pipelinetemplate"
	ctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8stesting "k8s.io/client-go/testing"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {

	var (
		mockCtrl                   *gomock.Controller
		managerMock                *ctrlmock.MockManager
		cacheClient                client.Client
		devopsClient               *clientset.Clientset
		request                    reconcile.Request
		result                     reconcile.Result
		pipelineTemplateObj        *v1alpha1.PipelineTemplate
		pipelineTemplateReconciler reconcile.Reconciler
		err                        error
	)

	// We can prepare our tests
	BeforeEach(func() {
		// // starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		managerMock = ctrlmock.NewMockManager(mockCtrl)
		cacheClient = testtools.NewFakeClient()
		devopsClient = clientset.NewSimpleClientset()
		// starts our basic structs
		request.Name = "some"
		request.Namespace = "default"
		pipelineTemplateReconciler = pipelinetemplate.NewReconciler(cacheClient, devopsClient)

		pipelineTemplateObj = &v1alpha1.PipelineTemplate{
			ObjectMeta: metav1.ObjectMeta{
				Name:      request.Name,
				Namespace: request.Namespace,
				Annotations: map[string]string{
					v1alpha1.AnnotationsTemplateName:    "some",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
				Labels: map[string]string{
					v1alpha1.AnnotationsTemplateName:    "some",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
					"latest":                            "true",
				},
			},
		}
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	// here we really run our functions
	JustBeforeEach(func() {
		managerMock.EXPECT().GetDevOpsClient().Return(devopsClient).AnyTimes()
		managerMock.EXPECT().GetClient().Return(cacheClient).AnyTimes()
	})

	// if not added to the client it should return not found error on the client side
	Context("pipelinetemplate does not exist", func() {
		It("should not return an error", func() {

			pipelineTemplateReconciler = pipelinetemplate.NewReconciler(cacheClient, devopsClient)
			// runs our test here
			result, err = pipelineTemplateReconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("pipelinetemplate can be updated", func() {
		BeforeEach(func() {
			// k8sClient = k8sfake.NewSimpleClientset(secretObj)
			devopsClient = clientset.NewSimpleClientset(pipelineTemplateObj)

		})
		It("get a pipelinetemplate", func() {

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// verify if it was really updated
			pipelineTemplateObj, err = devopsClient.DevopsV1alpha1().PipelineTemplates("default").Get("some", metav1.GetOptions{ResourceVersion: "0"})

			Expect(err).To(BeNil())
			Expect(pipelineTemplateObj).ToNot(BeNil())
			Expect(pipelineTemplateObj.Name).To(Equal(request.Name))
		})
		It("update an pipelinetemplate", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			devopsClient.DevopsV1alpha1().PipelineTemplates("default").Update(pipelineTemplateObj)
			actions := devopsClient.Actions()
			Expect(actions).ToNot(BeEmpty())
			Expect(actions).To(
				ContainElement(
					WithTransform(func(act k8stesting.Action) bool {
						return act.Matches("update", "pipelinetemplates")
					}, BeTrue())),
			)
		})
		It("delete a pipelinetemplate", func() {

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// verify if it was really updated
			err = devopsClient.DevopsV1alpha1().PipelineTemplates("default").Delete(request.Name, &metav1.DeleteOptions{})

			Expect(err).To(BeNil())
		})
	})

	Context("pipelinetemplate controller logic", func() {
		BeforeEach(func() {
			pipelineTemplateObj = pipelineTemplateObj.DeepCopy()
			pipelineTemplateObj.SetFinalizers([]string{"finalizers"})
			pipelineTemplateObj.SetDeletionTimestamp(&metav1.Time{Time: time.Now()})
			devopsClient = clientset.NewSimpleClientset(pipelineTemplateObj)
			pipelineTemplateReconciler = pipelinetemplate.NewReconciler(cacheClient, devopsClient)
		})

		It("should clean finalizers and be deleted by k8s gc", func() {
			Expect(err).To(BeNil())
			result, err = pipelineTemplateReconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// should be deleted
			pipelineTemplateObj, err = devopsClient.DevopsV1alpha1().PipelineTemplates("default").Get("some", v1alpha1.GetOptions())
			if err != nil {
				Expect(errors.IsNotFound(err)).To(BeTrue())
			} else {
				Expect(pipelineTemplateObj.GetFinalizers()).To(BeEquivalentTo([]string{}))
			}
		})

		It("should update status if be referenced by pipelineconfig and be deleted if no pipelineconfig referenced later", func() {
			pipelineconfig := &v1alpha1.PipelineConfig{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "pipeline-config",
					Namespace: "default",
					Labels: map[string]string{
						v1alpha1.LabelTemplateKind:    v1alpha1.TypePipelineTemplate,
						v1alpha1.LabelTemplateName:    "some",
						v1alpha1.LabelTemplateVersion: "1.0.0",
					},
				},
			}
			devopsClient = clientset.NewSimpleClientset(pipelineTemplateObj, pipelineconfig)
			pipelineTemplateReconciler = pipelinetemplate.NewReconciler(cacheClient, devopsClient)

			Expect(err).To(BeNil())
			result, err = pipelineTemplateReconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
			// should update status
			pipelineTemplateObj, err = devopsClient.DevopsV1alpha1().PipelineTemplates("default").Get("some", v1alpha1.GetOptions())
			Expect(err).To(BeNil())
			Expect(pipelineTemplateObj.Status.Phase).To(Equal(v1alpha1.TemplateTerminating))

			err = devopsClient.DevopsV1alpha1().PipelineConfigs("default").Delete("pipeline-config", nil)
			Expect(err).To(BeNil())
			result, err = pipelineTemplateReconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// should be deleted
			pipelineTemplateObj, err = devopsClient.DevopsV1alpha1().PipelineTemplates("default").Get("some", v1alpha1.GetOptions())
			if err != nil {
				Expect(errors.IsNotFound(err)).To(BeTrue())
			} else {
				Expect(pipelineTemplateObj.GetFinalizers()).To(BeEquivalentTo([]string{}))
			}
		})
	})
})
