package codereposervice

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/metrics"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const controllerName = "CodeRepoService-Controller"

// Add injects the manager so to inject the controller in the manager
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconciler(mgr))
}

// NewReconciler returns a new reconcile.Reconciler
// no-op for this case
func NewReconciler(mgr manager.Manager) reconcile.Reconciler {
	r := reconciler.NewToolReconcilerByManager(mgr, controllerName, devops.TypeCodeRepoService, devops.TypeCodeRepoService, "coderepobindings", "codeRepoService")

	r.AvailableFunc = func(tool v1alpha1.ToolInterface, lastAttempt *metav1.Time) (*v1alpha1.HostPortStatus, error) {
		codeRepoService := tool.(*v1alpha1.CodeRepoService)
		return generic.CheckService(codeRepoService.GetHost(), nil)
	}

	return metrics.DecorateReconciler(
		controllerName,
		v1alpha1.TypeCodeRepoService, r,
	).WithResourceReconcileTime()
}

func add(mgr manager.Manager, r reconcile.Reconciler) error {
	ctrl, err := mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(&source.Kind{Type: &v1alpha1.CodeRepoService{}}, &handler.EnqueueRequestForObject{})

	return err
}
