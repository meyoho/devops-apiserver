package codereposervice_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/codereposervice"
	"alauda.io/devops-apiserver/pkg/controller/testtools"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestCodeRepoServiceController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("codereposervice.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/codereposervice", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"CodeRepoService-Controller",
		codereposervice.Add,
		&source.Kind{Type: &v1alpha1.CodeRepoService{}},
		&handler.EnqueueRequestForObject{},
	),
)
