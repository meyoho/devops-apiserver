package codereposervice_test

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8stesting "k8s.io/client-go/testing"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		mockCtrl    *gomock.Controller
		cacheClient client.Client
		//k8sClient                 *k8sfake.Clientset
		devopsClient              *clientset.Clientset
		request                   reconcile.Request
		result                    reconcile.Result
		err                       error
		codeRepoServiceReconciler *reconciler.ToolReconciler
		codeRepoServiceObj        *devopsv1alpha1.CodeRepoService
		//systemNamespace           = "alauda-system"
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		cacheClient = testtools.NewFakeClient()
		//k8sClient = k8sfake.NewSimpleClientset()
		devopsClient = clientset.NewSimpleClientset()

		codeRepoServiceReconciler = reconciler.NewToolReconciler("new-codereposervice-controller", devopsv1alpha1.TypeCodeRepoService, cacheClient, devopsClient, devopsv1alpha1.TypeCodeRepoService, "coderepobindings", "codeRepoService", devopsv1alpha1.AnnotationProvider{})

		request.Name = "codereposervice"

		codeRepoServiceObj = GetCodeRepoService(devopsv1alpha1.CodeRepoServiceTypeGithub, "http://some-host", request.Name)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})
	JustBeforeEach(
		func() {
			result, err = codeRepoServiceReconciler.Reconcile(request)
		})
	Context("codereposervice does not exist", func() {
		It("should not return error ", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})
	Context("codereposervice can be updated  ", func() {
		BeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset(codeRepoServiceObj)
			codeRepoServiceReconciler = reconciler.NewToolReconciler("new-codereposervice-controller", devopsv1alpha1.TypeCodeRepoService, cacheClient, devopsClient, devopsv1alpha1.TypeCodeRepoService, "coderepobindings", "codeRepoService", devopsv1alpha1.AnnotationProvider{})
		})
		It("get a projectmanangement", func() {

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			codeRepoServiceObj, err = devopsClient.Devops().CodeRepoServices().Get(request.Name, metav1.GetOptions{ResourceVersion: "0"})
			Expect(err).To(BeNil())
			Expect(codeRepoServiceObj).NotTo(BeNil())
			Expect(codeRepoServiceObj.Name).To(Equal(request.Name))

		})

		It("update a codereposervice", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			actions := devopsClient.Actions()
			Expect(actions).ToNot(BeEmpty())
			Expect(actions).To(
				ContainElement(
					WithTransform(func(act k8stesting.Action) bool {
						return act.Matches("update", "codereposervices")
					}, BeTrue())),
			)
		})
	})
})

func GetCodeRepoService(tpe devopsv1alpha1.CodeRepoServiceType, host string, name string) *devopsv1alpha1.CodeRepoService {
	return &devopsv1alpha1.CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: devopsv1alpha1.CodeRepoServiceSpec{
			Type: tpe,
			ToolSpec: devopsv1alpha1.ToolSpec{
				HTTP: devopsv1alpha1.HostPort{
					Host: host,
				},
			},
		},
	}
}
