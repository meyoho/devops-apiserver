package pipelinetasktemplate_test

import (
	"time"

	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"k8s.io/apimachinery/pkg/api/errors"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/devops/pipelinetasktemplate"
	"alauda.io/devops-apiserver/pkg/controller/devops/pipelinetemplate"
	ctrlmock "alauda.io/devops-apiserver/pkg/mock/devops/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	k8stesting "k8s.io/client-go/testing"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {

	var (
		mockCtrl                       *gomock.Controller
		managerMock                    *ctrlmock.MockManager
		cacheClient                    client.Client
		k8sClient                      *k8sfake.Clientset
		devopsClient                   *clientset.Clientset
		request                        reconcile.Request
		result                         reconcile.Result
		pipelineTaskTemplateObj        *v1alpha1.PipelineTaskTemplate
		pipelineTaskTemplateReconciler reconcile.Reconciler
		err                            error
	)

	// We can prepare our tests
	BeforeEach(func() {
		// // starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		managerMock = ctrlmock.NewMockManager(mockCtrl)
		k8sClient = k8sfake.NewSimpleClientset()
		devopsClient = clientset.NewSimpleClientset()
		cacheClient = testtools.NewFakeClient()
		// starts our basic structs
		request.Name = "some"
		request.Namespace = "default"
		pipelineTaskTemplateReconciler = pipelinetasktemplate.NewReconciler(k8sClient, devopsClient)

		pipelineTaskTemplateObj = &v1alpha1.PipelineTaskTemplate{
			ObjectMeta: metav1.ObjectMeta{
				Name:      request.Name,
				Namespace: request.Namespace,
				Annotations: map[string]string{
					v1alpha1.AnnotationsTemplateName:    "some",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
				Labels: map[string]string{
					v1alpha1.AnnotationsTemplateName:    "some",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
					"latest":                            "true",
				},
			},
		}
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	// here we really run our functions
	JustBeforeEach(func() {
		managerMock.EXPECT().GetClient().Return(cacheClient).AnyTimes()
		managerMock.EXPECT().GetDevOpsClient().Return(devopsClient).AnyTimes()
		managerMock.EXPECT().GetKubeClient().Return(k8sClient).AnyTimes()
	})

	// if not added to the client it should return not found error on the client side
	Context("pipelinetasktemplate does not exist", func() {
		It("should not return an error", func() {

			pipelineTaskTemplateReconciler = pipelinetemplate.NewReconciler(cacheClient, devopsClient)
			// runs our test here
			result, err = pipelineTaskTemplateReconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("pipelinetasktemplate can be updated", func() {
		BeforeEach(func() {
			// k8sClient = k8sfake.NewSimpleClientset(secretObj)
			devopsClient = clientset.NewSimpleClientset(pipelineTaskTemplateObj)

		})
		It("get a pipelinetasktemplate", func() {

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// verify if it was really updated
			pipelineTaskTemplateObj, err = devopsClient.DevopsV1alpha1().PipelineTaskTemplates("default").Get("some", metav1.GetOptions{ResourceVersion: "0"})

			Expect(err).To(BeNil())
			Expect(pipelineTaskTemplateObj).ToNot(BeNil())
			Expect(pipelineTaskTemplateObj.Name).To(Equal(request.Name))
		})
		It("update an pipelinetasktemplate", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			devopsClient.DevopsV1alpha1().PipelineTaskTemplates("default").Update(pipelineTaskTemplateObj)
			actions := devopsClient.Actions()
			Expect(actions).ToNot(BeEmpty())
			Expect(actions).To(
				ContainElement(
					WithTransform(func(act k8stesting.Action) bool {
						return act.Matches("update", "pipelinetasktemplates")
					}, BeTrue())),
			)
		})
		It("delete a pipelinetasktemplate", func() {

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// verify if it was really updated
			err = devopsClient.DevopsV1alpha1().PipelineTaskTemplates("default").Delete(request.Name, &metav1.DeleteOptions{})

			Expect(err).To(BeNil())
		})
	})

	Context("pipelinetasktemplate controller logic", func() {
		BeforeEach(func() {
			pipelineTaskTemplateObj = pipelineTaskTemplateObj.DeepCopy()
			pipelineTaskTemplateObj.SetFinalizers([]string{"finalizers"})
			pipelineTaskTemplateObj.SetDeletionTimestamp(&metav1.Time{Time: time.Now()})
			devopsClient = clientset.NewSimpleClientset(pipelineTaskTemplateObj)
			pipelineTaskTemplateReconciler = pipelinetasktemplate.NewReconciler(k8sClient, devopsClient)
		})

		It("should clean finalizers and be deleted by k8s gc", func() {
			Expect(err).To(BeNil())
			result, err = pipelineTaskTemplateReconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// should be deleted
			pipelineTaskTemplateObj, err = devopsClient.DevopsV1alpha1().PipelineTaskTemplates("default").Get("some", v1alpha1.GetOptions())
			if err != nil {
				Expect(errors.IsNotFound(err)).To(BeTrue())
			} else {
				Expect(pipelineTaskTemplateObj.GetFinalizers()).To(BeEquivalentTo([]string{}))
			}
		})

		It("should update status if be referenced by pipelinetemplate and be deleted if no pipelinetemplate referenced later", func() {
			pipelinetemplate := &v1alpha1.PipelineTemplate{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "some.1.0.0",
					Namespace: "default",
					Annotations: map[string]string{
						v1alpha1.AnnotationsTemplateVersion: "0.0.1",
						v1alpha1.AnnotationsTemplateName:    "some",
					},
				},
				Spec: v1alpha1.PipelineTemplateSpec{
					Stages: []v1alpha1.PipelineStage{
						v1alpha1.PipelineStage{
							Tasks: []v1alpha1.PipelineTemplateTask{
								v1alpha1.PipelineTemplateTask{
									Name: "some",
								},
							},
						},
					},
				},
			}
			devopsClient = clientset.NewSimpleClientset(pipelineTaskTemplateObj, pipelinetemplate)
			pipelineTaskTemplateReconciler = pipelinetasktemplate.NewReconciler(k8sClient, devopsClient)

			Expect(err).To(BeNil())
			result, err = pipelineTaskTemplateReconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
			// should update status
			pipelineTaskTemplateObj, err = devopsClient.DevopsV1alpha1().PipelineTaskTemplates("default").Get("some", v1alpha1.GetOptions())
			Expect(err).To(BeNil())
			Expect(pipelineTaskTemplateObj.Status.Phase).To(Equal(v1alpha1.TemplateTerminating))

			err = devopsClient.DevopsV1alpha1().PipelineTemplates("default").Delete("some.1.0.0", nil)
			Expect(err).To(BeNil())
			result, err = pipelineTaskTemplateReconciler.Reconcile(request)
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			// should be deleted
			pipelineTaskTemplateObj, err = devopsClient.DevopsV1alpha1().PipelineTaskTemplates("default").Get("some", v1alpha1.GetOptions())
			if err != nil {
				Expect(errors.IsNotFound(err)).To(BeTrue())
			} else {
				Expect(pipelineTaskTemplateObj.GetFinalizers()).To(BeEquivalentTo([]string{}))
			}
		})
	})
})
