package pipelinetasktemplate_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/controller/devops/pipelinetasktemplate"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestPipelineTaskTemplateController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("pipelinetasktemplate.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/pipelinetasktemplate", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"pipelinetasktemplate-controller",
		pipelinetasktemplate.Add,
		&source.Kind{Type: &v1alpha1.PipelineTaskTemplate{}},
		&handler.EnqueueRequestForObject{},
	),
)
