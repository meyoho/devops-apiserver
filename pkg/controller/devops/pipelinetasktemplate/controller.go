package pipelinetasktemplate

import (
	"alauda.io/devops-apiserver/pkg/controller/generic"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/klog/klogr"

	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const controllerName = "pipelinetasktemplate-controller"

// Add adds new reconciler
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

// NewReconcilerByManager creates a reconciler by manager
func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	return &Reconciler{
		Client:       mgr.GetKubeClient(),
		DevOpsClient: clientset.NewExpansion(mgr.GetDevOpsClient()),

		Referencer: generic.NewPipelineTaskTemplateReferencer(mgr.GetDevOpsClient()),
	}
}

// NewReconciler creates a new reconciler
func NewReconciler(kubeClient kubernetes.Interface, devopsClient clientset.Interface) reconcile.Reconciler {
	return &Reconciler{
		Client:       kubeClient,
		DevOpsClient: clientset.NewExpansion(devopsClient),

		Referencer: generic.NewPipelineTaskTemplateReferencer(devopsClient),
	}
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	// watch normal event
	if err = ctrl.Watch(&source.Kind{
		Type: &devopsv1alpha1.PipelineTaskTemplate{}},
		&handler.EnqueueRequestForObject{},
	); err != nil {
		return
	}

	if err = ctrl.Watch(&source.Kind{
		Type: &devopsv1alpha1.ClusterPipelineTaskTemplate{}},
		&handler.EnqueueRequestForObject{},
	); err != nil {
		return
	}

	return
}

// Reconciler sync
type Reconciler struct {
	Client       kubernetes.Interface
	DevOpsClient clientset.InterfaceExpansion

	Referencer generic.Referencer
}

var _ reconcile.Reconciler = &Reconciler{}

// Reconcile handles user delete template
func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	log := klogr.New().WithName(controllerName).WithValues("PipelineTaskTemplate", request.String())
	log.V(5).Info("Reconciling")
	var (
		kind     string
		template devopsv1alpha1.PipelineTaskTemplateInterface
	)
	namespace, name := request.Namespace, request.Name
	// pipelinetemplate kind
	if len(namespace) == 0 {
		kind = devopsv1alpha1.TypeClusterPipelineTaskTemplate
	} else {
		kind = devopsv1alpha1.TypePipelineTaskTemplate
	}

	template, err = rec.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Get(kind, namespace, name, devopsv1alpha1.GetOptions())
	if err != nil {
		if errors.IsNotFound(err) {
			return reconcile.Result{}, nil
		}
		log.Error(err, "get template error")
		return
	}

	// ignore pipelinetemplate without deleteTimestamp
	if template.GetDeletionTimestamp() == nil || template.GetDeletionTimestamp().IsZero() {
		log.V(7).Info("ignore due to without deleteTimestamp")
		return reconcile.Result{}, nil
	}

	referenced, err := rec.Referencer.Referenced(template)
	if err != nil {
		log.Error(err, "check referenced error")
		return
	}

	if !referenced && finalize(template) {
		log.V(5).Info("template without referenced and finalizers, will be deleted by k8s later")
		return reconcile.Result{}, nil
	}

	obj := template.DeepCopyObject()
	templateCopy := obj.(devopsv1alpha1.PipelineTaskTemplateInterface)

	if referenced {
		// the template is used
		// update status
		status := templateCopy.GetStatus()
		if status.Phase != devopsv1alpha1.TemplateTerminating {
			status.Phase = devopsv1alpha1.TemplateTerminating
			_, err = rec.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().UpdateStatus(templateCopy)
		}
	} else {
		// the template no used
		// clean finalizers

		templateCopy.SetFinalizers([]string{})
		// wait k8s gc
		_, err = rec.DevOpsClient.DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Update(templateCopy)
	}

	if err != nil {
		log.Error(err, "update template error")
		return
	}

	return reconcile.Result{}, nil
}

func finalize(template devopsv1alpha1.PipelineTaskTemplateInterface) bool {
	return len(template.GetFinalizers()) == 0
}
