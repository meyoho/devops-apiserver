package coderepository_test

import (
	"testing"

	"github.com/golang/mock/gomock"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/coderepository"
	"alauda.io/devops-apiserver/pkg/controller/testtools"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestCodeRepoServiceController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("coderepository.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/coderepository", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"coderepository-controller",
		coderepository.Add,
		&source.Kind{Type: &v1alpha1.CodeRepository{}},
		&handler.EnqueueRequestForObject{},
		gomock.Any(),
	),
)
