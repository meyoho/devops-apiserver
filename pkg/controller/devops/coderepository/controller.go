package coderepository

import (
	"context"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/labels"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"k8s.io/apimachinery/pkg/api/errors"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const controllerName = "coderepository-controller"

func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	return &Reconciler{
		CacheClient:      mgr.GetClient(),
		ThirdPartyClient: mgr.GetThirdParty(),
	}
}

func NewReconciler(cacheClient client.Client, thirdPartyClient thirdparty.Interface) reconcile.Reconciler {
	return &Reconciler{
		CacheClient:      cacheClient,
		ThirdPartyClient: thirdPartyClient,
	}
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.CodeRepository{}}, &handler.EnqueueRequestForObject{},
		// If the repository is empty
		// then the commit will also be empty
		// resulting in a very large number of requests

		// If the commitID is not empty and PushAt is not updated
		// this coderepository will not be deleted even if coderepobinding is removed.
		predicate.Funcs{},
	)
	return
}

type Reconciler struct {
	CacheClient      client.Client
	ThirdPartyClient thirdparty.Interface
}

var _ reconcile.Reconciler = &Reconciler{}

func (rec *Reconciler) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	glog.V(5).Infof("[%s] Reconciling %s", controllerName, request)
	codeRepository := &v1alpha1.CodeRepository{}
	err := rec.CacheClient.Get(context.TODO(), request.NamespacedName, codeRepository)
	if err != nil {
		utilruntime.HandleError(err)
		if !errors.IsNotFound(err) {
			return reconcile.Result{}, err
		}
		glog.V(5).Infof("[%s] CodeRepository '%s' is not foud", controllerName, request)
		return reconcile.Result{}, nil
	}

	if !generic.ShouldConditionalComputation(codeRepository.Status.HTTPStatus, v1alpha1.TTLServiceCheck) {
		glog.V(5).Infof("[%s] CodeRepository has been reconcile in %d miniutes, skip reconcile", controllerName, int(v1alpha1.TTLServiceCheck.Minutes()))
		return reconcile.Result{}, nil
	}

	repositoryCopy := codeRepository.DeepCopy()
	foundBinding, err := rec.findBindingReferToRepo(repositoryCopy)
	if err != nil {
		glog.Errorf("[%s] try to find coderepobining used by coderepository '%s/%s' error:%#v", controllerName, repositoryCopy.Namespace, repositoryCopy.Name, err)
		// error can be ignored here and wait for the next reconcile loop
		// if coderepobinding is lost, this data will loss it's ownerreference
		return reconcile.Result{}, nil
	}

	if foundBinding == nil {
		err = rec.doSomethingIfNoBindingReferToRepo(repositoryCopy)
	}

	// error can be ignored here and wait for the next reconcile loop
	if err != nil {
		return reconcile.Result{}, nil
	}

	err = rec.updateCodeRepository(repositoryCopy)
	return reconcile.Result{}, err
}

func (c *Reconciler) findBindingReferToRepo(repositoryCopy *v1alpha1.CodeRepository) (*v1alpha1.CodeRepoBinding, error) {
	// coderepository has the same namespace with coderepobinding
	foundBinding := &v1alpha1.CodeRepoBinding{}
	err := c.CacheClient.Get(context.TODO(), client.ObjectKey{Namespace: repositoryCopy.GetNamespace(), Name: repositoryCopy.GetName()}, foundBinding)
	return foundBinding, err
}

func (c *Reconciler) doSomethingIfAnyBindingReferToRepo(repositoryCopy *v1alpha1.CodeRepository, foundBinding *v1alpha1.CodeRepoBinding) error {
	glog.V(5).Infof("[%s] Found binding %s contains the repo %s", controllerName, foundBinding.GetName(), repositoryCopy.GetName())
	service := &v1alpha1.CodeRepoService{}
	err := c.CacheClient.Get(context.TODO(), client.ObjectKey{Name: foundBinding.Spec.CodeRepoService.Name}, service)
	if err != nil {
		return err
	}

	secret := &corev1.Secret{}
	err = c.CacheClient.Get(context.TODO(), client.ObjectKey{Namespace: foundBinding.GetSecretNamespace(), Name: foundBinding.GetSecretName()}, secret)
	if err != nil {
		return err
	}

	serviceClient, err := c.ThirdPartyClient.CodeRepoServiceFactory().GetCodeRepoServiceClient(service, secret)
	if err != nil {
		return err
	}

	commit, status := serviceClient.GetLatestRepoCommit(
		repositoryCopy.GetRepoID(),
		repositoryCopy.GetOwnerName(),
		repositoryCopy.GetRepoName(),
		repositoryCopy.GetRepoFullName())
	glog.V(5).Infof("[%s] Repository %s return commit: %v, status: %v", controllerName, repositoryCopy.Name, commit, status)
	repositoryCopy.Status.HTTPStatus = status
	if commit != nil {
		repositoryCopy.Status.Repository.LatestCommit = *commit
	}
	if status != nil && status.StatusCode >= 400 {
		repositoryCopy.Status.Phase = v1alpha1.ServiceStatusPhaseError
		repositoryCopy.Status.Message = status.Response
	} else {
		repositoryCopy.Status.Message = ""
		repositoryCopy.Status.Phase = v1alpha1.ServiceStatusPhaseReady
	}
	return nil
}

func (c *Reconciler) doSomethingIfNoBindingReferToRepo(repositoryCopy *v1alpha1.CodeRepository) error {
	glog.V(5).Infof("[%s] Can not find binding contains the repo %s", controllerName, repositoryCopy.GetName())
	pipelineConfigList, errPipe := c.getPipelineConfigs(repositoryCopy.GetNamespace(), repositoryCopy.GetName())
	if errPipe != nil || pipelineConfigList.Items == nil || len(pipelineConfigList.Items) == 0 {
		// Neither coderepobinding nor pielineconfig referenced to the repository, delete it
		glog.V(5).Infof("[%s] No pipeline refer to the repo, delete it", controllerName)
		return c.CacheClient.Delete(context.TODO(), repositoryCopy)
	} else {
		// No coderepobinding but has pielineconfig, mark it as WaitingToDelete
		glog.V(5).Infof("[%s] but %d pipelines refer to the repo, change it's status to WaitToDelete", controllerName, len(pipelineConfigList.Items))
		repositoryCopy.Status.Phase = v1alpha1.ServiceStatusPhaseWaitingToDelete
	}
	return nil
}

func (c *Reconciler) getPipelineConfigs(namespace, codeRepoName string) (*v1alpha1.PipelineConfigList, error) {
	pipelineConfigList := &v1alpha1.PipelineConfigList{}
	err := c.CacheClient.List(context.TODO(), &client.ListOptions{
		Namespace:     namespace,
		LabelSelector: labels.SelectorFromSet(map[string]string{v1alpha1.LabelCodeRepository: codeRepoName}),
	}, pipelineConfigList)
	if err != nil {
		return nil, err
	}
	return pipelineConfigList, nil
}

func (c *Reconciler) updateCodeRepository(repository *v1alpha1.CodeRepository) (err error) {
	return c.CacheClient.Update(context.TODO(), repository)
}
