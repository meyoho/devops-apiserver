package coderepository_test

import (
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"sigs.k8s.io/controller-runtime/pkg/client"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/devops/coderepository"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		mockCtrl         *gomock.Controller
		cacheClient      client.Client
		k8sClient        *k8sfake.Clientset
		request          reconcile.Request
		thirdPartyClient thirdparty.Interface
		result           reconcile.Result
		reconciler       reconcile.Reconciler
		roundTripper     *mhttp.MockRoundTripper
		err              error
		codeRepository   *devopsv1alpha1.CodeRepository
		systemNamespace  = "alauda-system"
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		cacheClient = testtools.NewFakeClient()
		k8sClient = k8sfake.NewSimpleClientset()
		roundTripper = mhttp.NewMockRoundTripper(mockCtrl)

		reconciler = coderepository.NewReconciler(cacheClient, thirdPartyClient)

		request.Name = "devops-demo1"
		request.Namespace = "namespace1"
		thirdPartyClient, _ = thirdparty.New(roundTripper, k8sClient, nil, "", systemNamespace, devopsv1alpha1.NewAnnotationProvider(devopsv1alpha1.UsedBaseDomain))

		codeRepository = GetCodeRepository()
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})
	JustBeforeEach(
		func() {
			result, err = reconciler.Reconcile(request)
			fmt.Printf("Reconcile........ \n")
		})

	Context("coderepository does not exist", func() {
		It("should not return error ", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	PContext("coderepository's binding is not exists", func() {
		BeforeEach(func() {
			cacheClient = testtools.NewFakeClient(codeRepository)
			reconciler = coderepository.NewReconciler(cacheClient, thirdPartyClient)
		})
		It("should be deleted", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			err := cacheClient.Get(context.TODO(), request.NamespacedName, &devopsv1alpha1.CodeRepository{})
			isNotFoundError := errors.IsNotFound(err)
			Expect(isNotFoundError).To(BeTrue())
		})
	})

	PContext("coderepository's binding is exists", func() {

		BeforeEach(func() {
			k8sClient = k8sfake.NewSimpleClientset(GetSecret())
			codeRepo := GetCodeRepository()
			codeRepo.Status.Repository.LatestCommit.CommitID = "we2341"
			cacheClient = testtools.NewFakeClient(codeRepo, GetCodeRepoBinding())
			roundTripper.EXPECT().RoundTrip(gomock.Any()).Return(newHttpResponse(200, latestCommitResponseJson), nil)

			provider := devopsv1alpha1.NewAnnotationProvider(devopsv1alpha1.UsedBaseDomain)
			thirdPartyClient, _ = thirdparty.New(roundTripper, k8sClient, nil, "", systemNamespace, provider)
			reconciler = coderepository.NewReconciler(cacheClient, thirdPartyClient)
			fmt.Printf("Renew Reconcile........ \n")
		})

		It("should update latest commit in coderepository", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			codeRepository := &devopsv1alpha1.CodeRepository{}
			err = cacheClient.Get(context.TODO(), request.NamespacedName, codeRepository)
			Expect(err).To(BeNil())
			Expect(codeRepository.Status.Repository.LatestCommit.CommitID).To(BeEquivalentTo("we2341"))
		})
	})
})

func GetCodeRepository() *devopsv1alpha1.CodeRepository {
	return &devopsv1alpha1.CodeRepository{
		TypeMeta: metav1.TypeMeta{
			APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			Kind:       devopsv1alpha1.TypeCodeRepository,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "devops-demo1",
			Namespace: "namespace1",
		},
		Spec: devopsv1alpha1.CodeRepositorySpec{
			CodeRepoBinding: devopsv1alpha1.LocalObjectReference{
				Name: "github",
			},
			Repository: devopsv1alpha1.OriginCodeRepository{
				CodeRepoServiceType: devopsv1alpha1.CodeRepoServiceTypeGithub,
				Name:                "devops-demo1",
			},
		},
	}
}

func GetCodeRepoBinding() *devopsv1alpha1.CodeRepoBinding {
	return &devopsv1alpha1.CodeRepoBinding{
		TypeMeta: metav1.TypeMeta{
			APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			Kind:       devopsv1alpha1.TypeCodeRepoBinding,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: "github",
		},
		Spec: devopsv1alpha1.CodeRepoBindingSpec{
			CodeRepoService: devopsv1alpha1.LocalObjectReference{
				Name: "github",
			},
			Account: devopsv1alpha1.CodeRepoBindingAccount{
				Secret: devopsv1alpha1.SecretKeySetRef{
					SecretReference: corev1.SecretReference{
						Name:      "github-secret",
						Namespace: "namespace1",
					},
				},
			},
		},
	}
}

func GetSecret() *corev1.Secret {
	return &corev1.Secret{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "v1",
			Kind:       "Secret",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "github-secret",
			Namespace: "namespace1",
		},
		Type: devopsv1alpha1.SecretTypeOAuth2,
	}
}

var latestCommitResponseJson = `{
"commitID":"we2341",
"commitMessage":"this is message",
"commitAt":"",
"commitName":"sample",
"commitEmail":"sample@com"
}`

func newHttpResponse(statusCode int, body string) *http.Response {
	return &http.Response{
		Status: fmt.Sprintf("%d", statusCode),
		Header: http.Header{
			"Content-Type": []string{
				"application/json",
			},
		},
		StatusCode: statusCode,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 0,
		Body:       ioutil.NopCloser(bytes.NewBufferString(fmt.Sprintf(body))),
	}
}
