package jenkinssyncheartbeat

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"context"
	"errors"
	"fmt"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
	"sync/atomic"
	"time"
)

const controllerName = "jenkins-sync-heartbeat-controller"

func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	return NewReconciler(mgr.GetClient(), mgr.GetAnnotationProvider())
}

func NewReconciler(cacheClient client.Client, provider v1alpha1.AnnotationProvider) reconcile.Reconciler {
	return &Reconciler{
		CacheClient:      cacheClient,
		ResourceEnqueued: 0,
		ResourceRecords:  make(map[string]ResourceTouchRecord),
		Provider:         provider,
	}
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(&source.Kind{Type: &v1alpha1.Jenkins{}},
		&handler.EnqueueRequestForObject{},
		predicate.Funcs{
			CreateFunc: func(createEvent event.CreateEvent) bool {
				return atomic.CompareAndSwapInt32(&r.(*Reconciler).ResourceEnqueued, 0, 1)
			},
			UpdateFunc: func(updateEvent event.UpdateEvent) bool {
				return atomic.CompareAndSwapInt32(&r.(*Reconciler).ResourceEnqueued, 0, 1)
			},
			DeleteFunc: func(deleteEvent event.DeleteEvent) bool {
				return atomic.CompareAndSwapInt32(&r.(*Reconciler).ResourceEnqueued, 0, 1)
			},
			GenericFunc: func(event event.GenericEvent) bool {
				return atomic.CompareAndSwapInt32(&r.(*Reconciler).ResourceEnqueued, 0, 1)
			},
		})
	return
}

// Reconciler reconciler for Jenkins heart beat to maintain the connection
type Reconciler struct {
	CacheClient client.Client
	// 0 represents no resource has enqueued, 1 represents at least one resource enqueued
	ResourceEnqueued int32
	// resource touch records group by jenkins name and resource type or resource type if this resource not belong to a Jenkins
	// et. "jenkins-example-name/pipeline" -> record
	// "secret-example-name" -> record
	ResourceRecords map[string]ResourceTouchRecord
	Provider        v1alpha1.AnnotationProvider
}

type ResourceTouchRecord = types.NamespacedName

var listOneResourceOption = metav1.ListOptions{
	Limit: 1,
}

var _ reconcile.Reconciler = &Reconciler{}

func (rec *Reconciler) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	glog.V(5).Infof("[%s] Starting Jenkins Sync heartbeat to ensure watch is alive", controllerName)

	result := reconcile.Result{
		Requeue: true,
		// TODO make this configurable
		RequeueAfter: time.Second * 30,
	}

	jenkinsList := &v1alpha1.JenkinsList{}
	err := rec.CacheClient.List(context.TODO(), &client.ListOptions{}, jenkinsList)
	if err != nil {
		glog.Errorf("[%s] Failed to list Jenkins, "+
			"reason %v, will not touch Jenkins, JenkinsBinding, PipelineConfig, and Pipeline.",
			controllerName, err)
	}

	for _, item := range jenkinsList.Items {
		glog.V(5).Infof("[%s] Starting to touch resources belong to Jenkins '%s'", controllerName, item.Name)
		jenkinsName := item.Name

		rec.touchResource(jenkinsName, "jenkinsBinding", &v1alpha1.JenkinsBinding{}, &v1alpha1.JenkinsBindingList{})
		rec.touchResource(jenkinsName, "pipelineConfig", &v1alpha1.PipelineConfig{}, &v1alpha1.PipelineConfigList{})
		rec.touchResource(jenkinsName, "pipeline", &v1alpha1.Pipeline{}, &v1alpha1.PipelineList{})
	}

	rec.touchResource("", "jenkins", &v1alpha1.Jenkins{}, &v1alpha1.JenkinsList{})
	rec.touchResource("", "codeRepository", &v1alpha1.CodeRepository{}, &v1alpha1.CodeRepositoryList{})
	rec.touchResource("", "namespace", &v1.Namespace{}, &v1.NamespaceList{})
	rec.touchResource("", "secret", &v1.Secret{}, &v1.SecretList{})

	return result, nil
}

func (rec *Reconciler) touchResource(jenkinsName, resourceKind string, resource runtime.Object, resourceList runtime.Object) {
	var labelSelector labels.Selector
	var recordKey string
	if jenkinsName == "" {
		labelSelector = labels.Everything()
		recordKey = resourceKind
	} else {
		labelSelector = labels.SelectorFromSet(map[string]string{"jenkins": jenkinsName})
		recordKey = jenkinsName + "/" + resourceKind
	}

	glog.V(5).Infof("[%s] Starting to touch %#v", controllerName, recordKey)

	foundRecord := false
	if record, ok := rec.ResourceRecords[recordKey]; ok {
		err := rec.CacheClient.Get(context.TODO(), record, resource)
		if err == nil {
			foundRecord = true
		}
	}

	if !foundRecord {
		err := rec.CacheClient.List(context.TODO(), &client.ListOptions{
			Raw:           &listOneResourceOption,
			LabelSelector: labelSelector,
		}, resourceList)

		if err != nil {
			glog.V(5).Infof("[%s] Unable to touch %s, reason %s", controllerName, recordKey, err.Error())
			return
		}

		items, err := meta.ExtractList(resourceList)
		if err != nil {
			glog.V(5).Infof("[%s] Unable to touch %s, reason %s", controllerName, recordKey, err.Error())
			return
		}

		if len(items) == 0 {
			glog.V(5).Infof("[%s] Unable to touch %s, no resource exists", controllerName, recordKey)
			return
		}

		resource = items[0]
		resourceMeta := resource.(metav1.Object)
		record := ResourceTouchRecord{
			Namespace: resourceMeta.GetNamespace(),
			Name:      resourceMeta.GetName(),
		}

		rec.ResourceRecords[recordKey] = record
	}

	err := rec.updateHeartbeatAnnotation(resource)
	if err != nil {
		glog.V(5).Infof("[%s] Unable to touch %s to trigger event, reason %s", controllerName, recordKey, err.Error())
		delete(rec.ResourceRecords, recordKey)
	}
}

func (rec *Reconciler) updateHeartbeatAnnotation(resource runtime.Object) error {
	objMeta, ok := resource.(metav1.Object)
	if !ok {
		return errors.New(fmt.Sprintf("failed to add annotation to kind %#v", resource.GetObjectKind()))
	}

	annotations := objMeta.GetAnnotations()
	if annotations == nil {
		annotations = make(map[string]string)
		objMeta.SetAnnotations(annotations)
	}

	currentTime := time.Now().String()
	annotations[rec.Provider.AnnotationsHeartbeatTouchTime()] = currentTime

	return rec.CacheClient.Update(context.TODO(), resource)
}
