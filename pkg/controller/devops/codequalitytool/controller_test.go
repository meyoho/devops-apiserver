package codequalitytool_test

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	k8stesting "k8s.io/client-go/testing"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		mockCtrl *gomock.Controller
		//k8sClient                 *k8sfake.Clientset
		cacheClient               client.Client
		devopsClient              *clientset.Clientset
		request                   reconcile.Request
		result                    reconcile.Result
		err                       error
		codeQualityToolReconciler *reconciler.ToolReconciler
		codeQualityToolObj        *devopsv1alpha1.CodeQualityTool
		//systemNamespace           = "alauda-system"
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		//k8sClient = k8sfake.NewSimpleClientset()
		cacheClient = testtools.NewFakeClient()
		devopsClient = clientset.NewSimpleClientset()

		request.Name = "sonar"

		codeQualityToolReconciler = reconciler.NewToolReconciler("codequalitytool-controller", devopsv1alpha1.TypeCodeQualityTool, cacheClient, devopsClient, devopsv1alpha1.TypeCodeQualityTool, "codequalitybindings", "codeQualityTool", devopsv1alpha1.AnnotationProvider{})
		codeQualityToolObj = GetCodeQualityTool(devopsv1alpha1.CodeQualityToolTypeSonarqube, "http://sonarcloud.io", request.Name)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	JustBeforeEach(func() {
		result, err = codeQualityToolReconciler.Reconcile(request)
	})

	Context("codequalitytool does not exist", func() {
		It("shoud not return error", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})

	Context("codequalitytool can be update", func() {
		BeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset(codeQualityToolObj)
			cacheClient = testtools.NewFakeClient(codeQualityToolObj)
			codeQualityToolReconciler = reconciler.NewToolReconciler("codequalitytool-controller", devopsv1alpha1.TypeCodeQualityTool, cacheClient, devopsClient, devopsv1alpha1.TypeCodeQualityTool, "codequalitybindings", "codeQualityTool", devopsv1alpha1.AnnotationProvider{})
		})

		It("get a codequalitytool", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			codeQualityToolObj, err = devopsClient.Devops().CodeQualityTools().Get(request.Name, metav1.GetOptions{ResourceVersion: "0"})
			Expect(err).To(BeNil())
			Expect(codeQualityToolObj).NotTo(BeNil())
			Expect(codeQualityToolObj.Name).To(Equal(request.Name))
		})

		It("update a codequalitytool", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			actions := devopsClient.Actions()
			Expect(actions).NotTo(BeNil())
			Expect(actions).To(
				ContainElement(WithTransform(func(act k8stesting.Action) bool {
					return act.Matches("update", "codequalitytools")
				}, BeTrue())),
			)
		})
	})
})

func GetCodeQualityTool(toolType devopsv1alpha1.CodeQualityToolType, host string, name string) *devopsv1alpha1.CodeQualityTool {
	return &devopsv1alpha1.CodeQualityTool{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Spec: devopsv1alpha1.CodeQualityToolSpec{
			Type: toolType,
			ToolSpec: devopsv1alpha1.ToolSpec{
				HTTP: devopsv1alpha1.HostPort{
					Host: host,
				},
			},
		},
	}
}
