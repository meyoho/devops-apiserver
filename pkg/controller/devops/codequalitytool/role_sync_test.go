package codequalitytool_test

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/controller/devops/codequalitytool"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"alauda.io/devops-apiserver/pkg/mock/devops/controller"
	extmock "alauda.io/devops-apiserver/pkg/mock/thirdparty/external"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	. "github.com/onsi/ginkgo"
)

var _ = Describe("CodeQualityTool.RoleSync", func() {

	var (
		ctrl *gomock.Controller

		managerMock          *controller.MockManager
		request              reconcile.Request
		k8sClient            *k8sfake.Clientset
		devopsClient         *clientset.Clientset
		thirdParty           *extmock.MockInterface
		systemNamespace      = "alauda-system"
		credentialsNamespace = "global-credentials"
		extraConfig          = manager.ExtraConfig{
			SystemNamespace:      systemNamespace,
			CredentialsNamespace: credentialsNamespace,
		}
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		managerMock = controller.NewMockManager(ctrl)
		request.Name = "sonarqube"
		thirdParty = extmock.NewMockInterface(ctrl)
		k8sClient = k8sfake.NewSimpleClientset()
		devopsClient = clientset.NewSimpleClientset()
	})
	JustBeforeEach(func() {
		managerMock.EXPECT().GetDevOpsClient().Return(devopsClient).AnyTimes()
		managerMock.EXPECT().GetKubeClient().Return(k8sClient).AnyTimes()
		managerMock.EXPECT().GetThirdParty().Return(thirdParty).AnyTimes()
		managerMock.EXPECT().GetExtraConfig().Return(extraConfig).AnyTimes()
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	Describe("AddRoleSync",
		testtools.GenControllerSetupTest(
			"CodeQualityToolRoleSync-controller",
			codequalitytool.AddRoleSync,
			&source.Kind{Type: &devops.CodeQualityTool{}},
			&handler.EnqueueRequestForObject{},
			predicate.RoleSyncEnable, predicate.RoleSyncTTL("CodeQualityToolRoleSync-controller"),
		),
	)

	Describe("GetToolData", func() {
		BeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset(codeQualityTool, codeQualityBindingList)
		})

		It("Should return tool data", func() {
			reconciler, err := codequalitytool.NewRoleSyncReconcilerByMgr(managerMock)

			Expect(err).To(BeNil())
			toolData, err := reconciler.GetToolData(request)
			Expect(err).To(BeNil())

			tool := toolData.ToolInstance.(*devops.CodeQualityTool)
			Expect(tool).ToNot(BeNil())
			Expect(tool.Name).To(BeEquivalentTo("sonarqube"))
			Expect(tool.GetHostPort().Host).To(BeEquivalentTo("http://111.111.111.111"))

			namespaces := []string{}
			projects := []string{}

			for nsValue, projValue := range toolData.NamespaceProjectsMap {
				namespaces = append(namespaces, nsValue)
				projects = append(projects, projValue...)
			}

			Expect(len(namespaces)).To(BeEquivalentTo(3))
			Expect(namespaces).To(ContainElement("devops-a6-dev"))
			Expect(namespaces).To(ContainElement("devops-a6-ops"))
			Expect(namespaces).To(ContainElement("devops-a7-dev"))

			Expect(len(projects)).To(BeEquivalentTo(3))
			Expect(projects).To(ContainElement("devops-a6-dev"))
			Expect(projects).To(ContainElement("devops-a6-ops"))
			Expect(projects).To(ContainElement("devops-a7-dev"))

		})
	})
})

var codeQualityTool = &devops.CodeQualityTool{
	ObjectMeta: metav1.ObjectMeta{
		Name: "sonarqube",
	},
	Spec: devops.CodeQualityToolSpec{
		Type: devops.CodeQualityToolTypeSonarqube,
		ToolSpec: devops.ToolSpec{
			HTTP: devops.HostPort{
				Host: "http://111.111.111.111",
			},
		},
	},
}
var codeQualityBindingList = &devops.CodeQualityBindingList{
	Items: []devops.CodeQualityBinding{
		{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "binding1",
				Namespace: "devops-a6-dev",
				Labels: map[string]string{
					devops.LabelCodeQualityToolType: "Sonarqube",
					devops.LabelCodeQualityTool:     "sonarqube",
				},
			},
			Spec: devops.CodeQualityBindingSpec{},
		},
		{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "binding2",
				Namespace: "devops-a6-ops",
				Labels: map[string]string{
					devops.LabelCodeQualityToolType: "Sonarqube",
					devops.LabelCodeQualityTool:     "sonarqube",
				},
			},
			Spec: devops.CodeQualityBindingSpec{},
		},
		{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "binding3",
				Namespace: "devops-a7-dev",
				Labels: map[string]string{
					devops.LabelCodeQualityToolType: "Sonarqube",
					devops.LabelCodeQualityTool:     "sonarqube",
				},
			},
			Spec: devops.CodeQualityBindingSpec{},
		},
		{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "binding4",
				Namespace: "devops-a7-dev",
				Labels: map[string]string{
					devops.LabelCodeQualityToolType: "Sonarqube",
					devops.LabelCodeQualityTool:     "sonarqube1",
				},
			},
			Spec: devops.CodeQualityBindingSpec{},
		},
	},
}
