package codequalitytool_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/codequalitytool"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestCodeQualityToolController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("codequalitytool.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/codequalitytool", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"codequalitytool-controller",
		codequalitytool.Add,
		&source.Kind{Type: &v1alpha1.CodeQualityTool{}},
		&handler.EnqueueRequestForObject{},
	),
)
