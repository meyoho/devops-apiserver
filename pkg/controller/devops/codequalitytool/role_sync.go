package codequalitytool

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	RolesynccontrollerName = "CodeQualityToolRoleSync-controller"
)

type RoleSyncReconciler struct {
	reconcile.Reconciler
	Client           kubernetes.Interface
	DevopsClient     clientset.Interface
	ThirdPartyClient thirdparty.Interface
}

// Add injects the manager so to inject the controller in the manager
func AddRoleSync(mgr manager.Manager) error {

	reconciler, err := NewRoleSyncReconcilerByMgr(mgr)
	if err != nil {
		return err
	}

	ctrl, err := mgr.NewController(RolesynccontrollerName, controller.Options{
		Reconciler: reconciler,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &devops.CodeQualityTool{}},
		&handler.EnqueueRequestForObject{},
		predicate.RoleSyncEnable, predicate.RoleSyncTTL(RolesynccontrollerName),
	)

	return err
}

func NewRoleSyncReconcilerByMgr(mgr manager.Manager) (r *RoleSyncReconciler, err error) {

	return NewRoleSyncReconciler(mgr.GetKubeClient(), mgr.GetDevOpsClient(), mgr.GetThirdParty(), mgr.GetExtraConfig())
}

func NewRoleSyncReconciler(client kubernetes.Interface, devopsClient clientset.Interface, thirdPartyClient thirdparty.Interface, extra manager.ExtraConfig) (r *RoleSyncReconciler, err error) {

	r = &RoleSyncReconciler{
		Client:           client,
		DevopsClient:     devopsClient,
		ThirdPartyClient: thirdPartyClient,
	}

	r.Reconciler, err = reconciler.NewRoleSyncReconcilerBy(client, devopsClient, thirdPartyClient, reconciler.RoleSyncOptions{
		Name:             RolesynccontrollerName,
		GetToolData:      r.GetToolData,
		GetRoleMapping:   r.GetRoleMapping,
		ApplyRoleMapping: r.ApplyRoleMapping,
	}, extra.SystemNamespace)

	return
}

func (r *RoleSyncReconciler) GetToolData(request reconcile.Request) (reconciler.ToolData, error) {
	name := request.Name

	// get codequalitytool
	tool, err := r.DevopsClient.DevopsV1alpha1().CodeQualityTools().Get(name, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("get 'CodeQualityTool/%s' error:%s", name, err)
		return reconciler.ToolData{}, err
	}

	// get all bindings the owner by current codequalitytool
	toolLabelSelector, err := codeQualitySelector(tool.Spec.Type.String(), tool.Name)
	if err != nil {
		return reconciler.ToolData{}, err
	}
	bindings, err := r.DevopsClient.DevopsV1alpha1().CodeQualityBindings("").List(
		metav1.ListOptions{LabelSelector: toolLabelSelector.String()})
	if err != nil {
		return reconciler.ToolData{}, err
	}

	// construct ToolData
	toolData := reconciler.ToolData{
		ToolInstance:         tool,
		ToolType:             tool.Spec.Type.String(),
		Bindings:             bindingsAsRuntimeObjects(bindings),
		NamespaceProjectsMap: namespaceProjectMap(bindings),
	}

	glog.V(9).Infof("get tool data %#v", toolData)

	return toolData, nil
}

// get namespaceProject map , eg. {"devops-a6-a6-dev": [ "devops-a6-a6-dev" ],}
// sonarqube is different between with other tools, we will virtual some projects
// reference doc: http://confluence.alaudatech.com/pages/viewpage.action?pageId=39322740
func namespaceProjectMap(bindList *devops.CodeQualityBindingList) map[string][]string {
	res := map[string][]string{}

	for _, binding := range bindList.Items {
		res[binding.Namespace] = []string{binding.Namespace}
	}
	return res
}

func bindingsAsRuntimeObjects(bindList *devops.CodeQualityBindingList) []runtime.Object {
	objects := make([]runtime.Object, 0, len(bindList.Items))

	for _, item := range bindList.Items {
		objects = append(objects, &item)
	}
	return objects
}

func codeQualitySelector(itemType string, name string) (labels.Selector, error) {
	labelSelector := &metav1.LabelSelector{
		MatchLabels: map[string]string{
			devops.LabelCodeQualityToolType: itemType,
			devops.LabelCodeQualityTool:     name,
		},
	}
	selector, err := metav1.LabelSelectorAsSelector(labelSelector)
	if err != nil {
		glog.Errorf("error convert labelselector %#v as selector, got errors:%#v", labelSelector, err)
	}
	return selector, err
}

func (r *RoleSyncReconciler) GetRoleMapping(toolData reconciler.ToolData, options *devops.RoleMappingListOptions) (
	roleMapping *devops.RoleMapping, err error) {

	tool := toolData.ToolInstance.(*devops.CodeQualityTool)
	roleMapping, err = r.DevopsClient.DevopsV1alpha1().CodeQualityTools().GetRoleMapping(tool.Name, options)
	return
}

func (r *RoleSyncReconciler) ApplyRoleMapping(toolData reconciler.ToolData, roleMapping *devops.RoleMapping) (
	result *devops.RoleMapping, err error) {

	tool := toolData.ToolInstance.(*devops.CodeQualityTool)
	roleMapping, err = r.DevopsClient.DevopsV1alpha1().CodeQualityTools().ApplyRoleMapping(tool.Name, roleMapping)
	return nil, err
}
