package codequalitytool

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/metrics"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const controllerName = "codequalitytool-controller"

func Add(mgr manager.Manager) error {
	return add(mgr, NewReconciler(mgr))
}

func NewReconciler(mgr manager.Manager) reconcile.Reconciler {
	r := reconciler.NewToolReconcilerByManager(mgr, controllerName, v1alpha1.TypeCodeQualityTool, v1alpha1.TypeCodeQualityTool, "codequalitybindings", "codeQualityTool")

	r.AvailableFunc = func(tool v1alpha1.ToolInterface, lastAttempt *metav1.Time) (*v1alpha1.HostPortStatus, error) {
		codeQualityTool := tool.(*v1alpha1.CodeQualityTool)
		return generic.CheckService(codeQualityTool.GetEndpoint(), nil)
	}

	return metrics.DecorateReconciler(
		controllerName,
		v1alpha1.TypeCodeQualityTool, r,
	).WithResourceReconcileTime()
}

func add(mgr manager.Manager, r reconcile.Reconciler) error {
	ctrl, err := mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(&source.Kind{
		Type: &v1alpha1.CodeQualityTool{}},
		&handler.EnqueueRequestForObject{})

	return err
}
