package templateannotation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("Templateannotation", func() {

	var (
		pipelineTemplateObj             v1alpha1.PipelineTemplate
		nobasedomainpipelineTemplateObj v1alpha1.PipelineTemplate
	)

	BeforeEach(func() {
		// // starts mock controller

		pipelineTemplateObj = v1alpha1.PipelineTemplate{
			ObjectMeta: metav1.ObjectMeta{

				Annotations: map[string]string{
					externalversions.GetFormatedAnnotation(v1alpha1.UsedBaseDomain, v1alpha1.AnnotationsTemplateName):    "some",
					externalversions.GetFormatedAnnotation(v1alpha1.UsedBaseDomain, v1alpha1.AnnotationsTemplateVersion): "1.0.0",
				},
				Labels: map[string]string{
					externalversions.GetFormatedAnnotation(v1alpha1.UsedBaseDomain, v1alpha1.AnnotationsTemplateVersion): "1.0.0",
					v1alpha1.AnnotationsTemplateName: "some",
					"alauda.io/latest":               "true",
				},
			},
		}

		nobasedomainpipelineTemplateObj = v1alpha1.PipelineTemplate{
			ObjectMeta: metav1.ObjectMeta{

				Annotations: map[string]string{
					v1alpha1.AnnotationsTemplateName:    "some",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
				Labels: map[string]string{
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
					v1alpha1.AnnotationsTemplateName:    "some",
				},
			},
		}

	})

	Context("test replce annotation", func() {

		It("replace should success", func() {

			chagned, result := GetCommpatiableTemplate(&pipelineTemplateObj)
			Expect(chagned).To(Equal(true))

			Expect(result.GetAnnotations()).To(Equal(
				map[string]string{
					v1alpha1.AnnotationsTemplateName:    "some",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
			))
			Expect(result.GetLabels()).To(Equal(
				map[string]string{
					v1alpha1.AnnotationsTemplateName:    "some",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
					v1alpha1.LabelTemplateLatest:        "true",
				},
			))

		})

		It("replace should not happen", func() {

			chagned, result := GetCommpatiableTemplate(&nobasedomainpipelineTemplateObj)
			Expect(chagned).To(Equal(false))

			Expect(result.GetAnnotations()).To(Equal(
				map[string]string{
					v1alpha1.AnnotationsTemplateName:    "some",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
			))
			Expect(result.GetLabels()).To(Equal(
				map[string]string{
					v1alpha1.AnnotationsTemplateName:    "some",
					v1alpha1.AnnotationsTemplateVersion: "1.0.0",
				},
			))

		})

	})

})
