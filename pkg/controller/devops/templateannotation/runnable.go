package templateannotation

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/util"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

// AddRunnable add Runnable as runnable
func AddRunnable(mgr manager.Manager) error {
	return mgr.Add(NewRunnable(mgr.GetKubeClient(), mgr.GetDevOpsClient()))
}

// NewRunnable constructs a new bootstrapper
func NewRunnable(client kubernetes.Interface, devopsclient clientset.Interface) sigsmanager.Runnable {
	r := &Runnable{
		Client:       client,
		DevOpsClient: clientset.NewExpansion(devopsclient),
	}
	return r
}

const (
	runnableName = "templateannotation-runable"
)

// Runnable bootstrapper for global-credentials
type Runnable struct {
	Client       kubernetes.Interface
	DevOpsClient clientset.InterfaceExpansion
}

var _ sigsmanager.Runnable = &Runnable{}

// Start adds a start method to be a runnable
func (nb *Runnable) Start(stop <-chan struct{}) (err error) {
	glog.V(5).Infof("[%s] Starting runnable", runnableName)

	nb.UpdateTempalteAnnotation()
	nb.UpdateClusterTempalteAnnotation()

	<-stop
	return
}

func (nb *Runnable) UpdateTempalteAnnotation() {

	kind := devopsv1alpha1.TypePipelineTemplate

	templates, err := nb.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().List(kind, "", metav1.ListOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("runnable %v get pipelinetemplate failed, err is %v ", runnableName, err)
		return
	}

	_, err = nb.ReplaceAnnotation(templates)

	if err != nil {
		glog.Errorf("runnable %v replace annotation failed, err is %v ", runnableName, err)
		return
	}

}

func (nb *Runnable) UpdateClusterTempalteAnnotation() {

	kind := devopsv1alpha1.TypeClusterPipelineTemplate
	templates, err := nb.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().List(kind, "", metav1.ListOptions{ResourceVersion: "0"})

	if err != nil {
		glog.Errorf("runnable %v get clusterpipelinetempalte failed, err is %v ", runnableName, err)
	}
	_, err = nb.ReplaceAnnotation(templates)

	if err != nil {
		glog.Errorf("runnable %v replace annotation failed, err is %v ", runnableName, err)
		return
	}
}

func (rb *Runnable) ReplaceAnnotation(templates []devopsv1alpha1.PipelineTemplateInterface) (result []devopsv1alpha1.PipelineTemplateInterface, err error) {

	errs := util.MultiErrors{}
	for _, template := range templates {

		glog.V(5).Infof("%s replace annotation for %s %s", runnableName, template.GetKind(), template.GetName())
		changed, templateCopy := GetCommpatiableTemplate(template)
		if !changed {
			continue
		}

		newtemplate, err := rb.DevOpsClient.DevopsV1alpha1Expansion().PipelineTemplateInterface().Update(templateCopy)
		result = append(result, newtemplate)
		if err != nil {
			glog.Errorf("%v replace annotation for %v %v failed, err is %s", runnableName, template.GetKind(), template.GetName(), err.Error())
			errs = append(errs, err)
		}
		glog.V(5).Infof("runnable %s replace %s %s annotation successfully", runnableName, template.GetKind(), template.GetName())

	}

	if len(errs) != 0 {
		return result, &errs
	}
	return result, nil
}

func GetCommpatiableTemplate(template devopsv1alpha1.PipelineTemplateInterface) (bool, devopsv1alpha1.PipelineTemplateInterface) {
	obj, changed := k8s.CompatiableBaseDomain(template)
	result := obj.(devopsv1alpha1.PipelineTemplateInterface)

	return changed, result
}
