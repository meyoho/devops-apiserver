package artifactregistry

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	rolesynccontrollerName = "artifactregistryRoleSync-controller"
)

type ArtifactRegistryRoleSyncReconciler struct {
	reconcile.Reconciler
	Client           kubernetes.Interface
	DevopsClient     clientset.Interface
	ThirdPartyClient thirdparty.Interface
	Provider         devopsv1alpha1.AnnotationProvider
}

// Add injects the manager so to inject the controller in the manager
func AddRoleSync(mgr manager.Manager) error {

	Pmreconciler, err := NewArtifactRegistryRoleSyncReconciler(mgr)
	if err != nil {
		return err
	}
	return addrolesync(mgr, Pmreconciler)

}

func addrolesync(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(rolesynccontrollerName, controller.Options{
		Reconciler: r,
	})
	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &devopsv1alpha1.ArtifactRegistry{}},
		&handler.EnqueueRequestForObject{},
		predicate.RoleSyncEnable, predicate.RoleSyncTTL(rolesynccontrollerName),
	)
	return nil
}

func NewArtifactRegistryRoleSyncReconciler(mgr manager.Manager) (roleReconciler reconcile.Reconciler, err error) {
	return NewArtifactRegistryRoleSyncReconcilerBy(mgr.GetKubeClient(), mgr.GetDevOpsClient(), mgr.GetThirdParty(), mgr.GetExtraConfig(), mgr.GetAnnotationProvider())
}

func NewArtifactRegistryRoleSyncReconcilerBy(client kubernetes.Interface, devopsClient clientset.Interface, thirdPartyClient thirdparty.Interface, extra manager.ExtraConfig, provider devopsv1alpha1.AnnotationProvider) (roleReconciler reconcile.Reconciler, err error) {
	rec := &ArtifactRegistryRoleSyncReconciler{
		Client:           client,
		DevopsClient:     devopsClient,
		ThirdPartyClient: thirdPartyClient,
		Provider:         provider,
	}
	roleReconciler, err = reconciler.NewRoleSyncReconcilerBy(client, devopsClient, thirdPartyClient, reconciler.RoleSyncOptions{
		Name:             rolesynccontrollerName,
		GetToolData:      rec.GetProjectManagementToolData,
		GetRoleMapping:   rec.GetProjectManagementRoleMapping,
		ApplyRoleMapping: rec.ApplyProjectManagementRoleMapping,
	}, extra.SystemNamespace)
	if err != nil {
		roleReconciler = nil
		return
	}
	rec.Reconciler = roleReconciler
	return rec, nil
}

func (p *ArtifactRegistryRoleSyncReconciler) GetProjectManagementToolData(request reconcile.Request) (data reconciler.ToolData, err error) {

	methodName := "GetProjectManagementToolData"

	Name := request.Name

	ar, err := p.DevopsClient.DevopsV1alpha1().ArtifactRegistries().Get(Name, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return data, err
	}
	//will get all namespaces
	bindings, err := p.DevopsClient.DevopsV1alpha1().ArtifactRegistryBindings("").List(metav1.ListOptions{})
	if err != nil {
		return data, err
	}
	filterbindings := []runtime.Object{}
	projectmap := make(map[string][]string)

	for _, binding := range bindings.Items {
		if binding.Spec.ArtifactRegistry.Name == Name {
			filterbindings = append(filterbindings, &binding)
			projectarray := []string{binding.Annotations[p.Provider.LabelAlaudaIOProjectKey()]}
			projectmap[binding.ObjectMeta.Namespace] = projectarray
		}
	}
	data.ToolInstance = ar
	data.ToolType = string(ar.Spec.Type)
	data.Bindings = filterbindings
	data.NamespaceProjectsMap = projectmap

	glog.V(9).Infof("%s %s result is %#v", rolesynccontrollerName, methodName, data)

	return data, nil
}

func (p *ArtifactRegistryRoleSyncReconciler) GetProjectManagementRoleMapping(data reconciler.ToolData, opts *devopsv1alpha1.RoleMappingListOptions) (roleMapping *devopsv1alpha1.RoleMapping, err error) {

	methodName := "GetProjectManagementRoleMapping"
	ar, isOk := data.ToolInstance.(*devopsv1alpha1.ArtifactRegistry)
	if !isOk {
		return
	}

	serviceName := ar.Name
	roleMapping, err = p.DevopsClient.DevopsV1alpha1().ArtifactRegistries().GetRoleMapping(serviceName, opts)
	glog.V(9).Infof("%s %s result is %#v", rolesynccontrollerName, methodName, roleMapping)
	return
}

func (p *ArtifactRegistryRoleSyncReconciler) ApplyProjectManagementRoleMapping(data reconciler.ToolData, roleMapping *devopsv1alpha1.RoleMapping) (result *devopsv1alpha1.RoleMapping, err error) {
	methodName := "ApplyProjectManagementRoleMapping"

	ar, isOk := data.ToolInstance.(*devopsv1alpha1.ArtifactRegistry)

	glog.V(9).Infof("%s %s ar is %#v", rolesynccontrollerName, methodName, ar)

	if !isOk {
		return
	}

	serviceName := ar.Name

	glog.V(9).Infof("%s %s serviceName is %#v and roleMapping is %#v", rolesynccontrollerName, methodName, serviceName, roleMapping)

	result, err = p.DevopsClient.DevopsV1alpha1().ArtifactRegistries().ApplyRoleMapping(serviceName, roleMapping)
	return
}
