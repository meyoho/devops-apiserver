package coderepobinding

import (
	"context"
	"fmt"
	"reflect"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/controller/metrics"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"k8s.io/apimachinery/pkg/runtime/schema"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/reconciler"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/types"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
	klogr "k8s.io/klog/klogr"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	controllerName        = "coderepobinding-controller"
	syncRepoConditionType = "SyncRepositories"
	syncRepoOwner         = "sync-repository"
	syncRepoErrorReason   = "SyncRepositoriesFailed"
)

var (
	groupVersionKind = schema.GroupVersionKind{
		Group:   v1alpha1.GroupName,
		Version: v1alpha1.Version,
		Kind:    v1alpha1.TypeCodeRepoBinding,
	}
)

// Add init function for controller-manager
func Add(mgr manager.Manager) error {
	return add(mgr, NewReconcilerByManager(mgr))
}

// NewReconcilerByManager CodeRepoBinding reconciler constructor
func NewReconcilerByManager(mgr manager.Manager) reconcile.Reconciler {
	return NewReconciler(mgr.GetClient(), mgr.GetDevOpsClient(), mgr.GetAnnotationProvider())
}

// NewReconciler CodeRepoBinding reconciler constructor
func NewReconciler(cacheClient client.Client, devopsClient clientset.Interface, provider v1alpha1.AnnotationProvider) reconcile.Reconciler {
	r := &Reconciler{
		CacheClient:  cacheClient,
		DevOpsClient: clientset.NewExpansion(devopsClient),
	}
	r.ItemsDeleter = reconciler.NewLocalGeneratedItemsDeleter(cacheClient, r.DevOpsClient,
		v1alpha1.TypeCodeRepoBinding, v1alpha1.ResourceNameCodeRepository,
		func(parent types.NamespacedName) *metav1.LabelSelector {
			return &metav1.LabelSelector{
				MatchLabels: map[string]string{
					v1alpha1.LabelCodeRepoBinding: parent.Name,
				}}
		}, provider)

	return metrics.DecorateReconciler(controllerName, v1alpha1.TypeCodeRepoBinding, r).WithResourceReconcileTime()
}

func add(mgr manager.Manager, r reconcile.Reconciler) (err error) {
	var ctrl controller.Controller
	ctrl, err = mgr.NewController(controllerName, controller.Options{
		Reconciler: r,
	})

	if err != nil {
		utilruntime.HandleError(err)
		return err
	}

	err = ctrl.Watch(
		&source.Kind{Type: &v1alpha1.CodeRepoBinding{}}, &handler.EnqueueRequestForObject{},
		predicate.EmptyOwnerTTL(controllerName, v1alpha1.TTLSession),
	)
	return
}

// Reconciler reconciler for CodeRepoBinding
type Reconciler struct {
	CacheClient  client.Client
	DevOpsClient clientset.InterfaceExpansion

	ItemsDeleter reconciler.LocalGeneratedItemsDeleter
}

var _ reconcile.Reconciler = &Reconciler{}

// Reconcile main reconcile function
func (rec *Reconciler) Reconcile(request reconcile.Request) (result reconcile.Result, err error) {
	log := klogr.New().WithName(fmt.Sprintf("[%s]", controllerName)).WithValues("CodeRepoBinding", request.String())

	log.V(5).Info("Reconciling")
	binding := &v1alpha1.CodeRepoBinding{}
	err = rec.CacheClient.Get(context.TODO(), request.NamespacedName, binding)
	if err != nil {
		utilruntime.HandleError(err)
		if !errors.IsNotFound(err) {
			return
		}
		log.Info("CodeRepoBinding is not found")
		rec.ItemsDeleter.DeleteItemsOf(log, request.NamespacedName)
		err = nil
		return
	}

	codeRepoService := &v1alpha1.CodeRepoService{}
	err = rec.CacheClient.Get(context.TODO(), client.ObjectKey{Name: binding.Spec.CodeRepoService.Name}, codeRepoService)
	if err != nil {
		if !errors.IsNotFound(err) {
			return
		}
		log.Info(fmt.Sprintf("codereposervice '%s' is not found , will delete coderepobinding", codeRepoService.Name))
		err = rec.CacheClient.Delete(context.TODO(), binding)
		if err != nil {
			log.Error(err, "delete error")
		}
		err = nil
		return
	}

	// will look for secrets refered in the binding and set as OwnerReference
	rec.reconcileSecretOwnerReference(binding)

	// generate binding related conditions
	serviceName := binding.Spec.CodeRepoService.Name
	conditioner := generic.NewStandardConditionProcess(controllerName, binding.Name)
	secretConditioner := generic.NewRecSecretConditioner(binding.Spec.Account.Secret, rec.CacheClient)
	serviceConditioner := generic.NewDevOpsToolInterfaceConditioner(v1alpha1.TypeCodeRepoService, serviceName, rec.DevOpsClient)
	authConditioner := generic.NewAuthorizationConditioner(binding.Spec.CodeRepoService.Name, binding.Spec.Account.Secret, rec.DevOpsClient.DevopsV1alpha1().CodeRepoServices().Authorize)
	conditioner.Add(secretConditioner).Add(serviceConditioner).Add(authConditioner)
	currentConditions := conditioner.Conditions()

	bindingCopy := binding.DeepCopy()
	bindingCopy.Status.HTTPStatus = serviceConditioner.GetHTTPStatus()
	bindingCopy.Status.LastUpdate = &metav1.Time{Time: time.Now()}
	glog.V(6).Infof("[%s] CodeRepoBinding %s currentConditions %s", controllerName, request, bindingCopy.Status)
	bindingCopy.Status = generic.GetServicePhaseStatus(currentConditions, bindingCopy.Status)
	glog.V(5).Infof("[%s] CodeRepoBinding %s before SyncRepos %s", controllerName, request, bindingCopy.Status)

	// if Binding is ready is necessary to fetch remote repos
	// and sync to k8s
	if bindingCopy.Status.Phase == v1alpha1.ServiceStatusPhaseReady {
		conditions, err := rec.SyncRepos(bindingCopy, codeRepoService, secretConditioner.Secret)
		if err != nil {
			// set bindingCopy.Status.Phase to error
			bindingCopy.Status.Phase = v1alpha1.ServiceStatusPhaseError
			bindingCopy.Status.Message = err.Error()
			bindingCopy.Status.Reason = syncRepoErrorReason
		}
		if conditions != nil {
			// having repo conditions we replace them using the same owner
			bindingCopy.Status.Conditions = v1alpha1.BindingConditions(bindingCopy.Status.Conditions).ReplaceBy(syncRepoOwner, conditions)
		}
	}
	rec.handleSyncStatusRepos(bindingCopy, codeRepoService)
	glog.V(5).Infof("[%s] CodeRepoBinding %s after SyncRepos %s", controllerName, request, bindingCopy.Status)
	// ignore the error, just wait for next loop
	err = nil

	err = rec.CacheClient.Status().Update(context.TODO(), bindingCopy)
	glog.V(6).Infof("[%s] update status result: err? %#v", controllerName, err)
	return
}

// [DEVOPS-3032] sync binding status to localrepos
func (rec *Reconciler) handleSyncStatusRepos(bindingCopy *v1alpha1.CodeRepoBinding, codeRepoService *v1alpha1.CodeRepoService) {
	selector := v1alpha1.GetServiceSelector(codeRepoService.GetName())

	localRepos := &v1alpha1.CodeRepositoryList{}
	err := rec.CacheClient.List(context.TODO(), &client.ListOptions{
		LabelSelector: selector,
	}, localRepos)

	if err != nil || localRepos.Items == nil {
		return
	}
	for i := range localRepos.Items {
		localRepoDeep := localRepos.Items[i].DeepCopy()

		if !generic.HasOwner(localRepoDeep, bindingCopy) {
			continue
		}

		// [DEVOPS-3168] sync secret
		localRepoDeep.Annotations[devops.LabelsSecretName] = bindingCopy.GetSecretName()
		localRepoDeep.Annotations[devops.LabelsSecretNamespace] = bindingCopy.GetSecretNamespace()
		if !reflect.DeepEqual(localRepoDeep.Annotations, localRepos.Items[i].Annotations) {
			err := rec.CacheClient.Update(context.TODO(), localRepoDeep)
			if err != nil {
				glog.Errorf("[%s] update coderepositories result: err? %#v", controllerName, err)
			}
		}

		localRepoDeep.Status.Phase = bindingCopy.Status.Phase
		localRepoDeep.Status.Message = bindingCopy.Status.Message
		localRepoDeep.Status.Reason = bindingCopy.Status.Reason
		err = rec.CacheClient.Status().Update(context.TODO(), localRepoDeep)
		if err != nil {
			glog.Errorf("[%s] update status coderepositories result: err? %#v", controllerName, err)
		}
	}
}

// SyncRepos sync remote repos and binding spec to k8s
func (rec *Reconciler) SyncRepos(bindingCopy *v1alpha1.CodeRepoBinding, codeRepoService *v1alpha1.CodeRepoService, secret *corev1.Secret) ([]v1alpha1.BindingCondition, error) {
	var bindingConditions []v1alpha1.BindingCondition
	glog.V(5).Infof("[%s] coderepobinding/%s/%s sync repos defined in binding to k8s", controllerName, bindingCopy.Namespace, bindingCopy.Name)
	// fetch all repos in the service
	localRepos := &v1alpha1.CodeRepositoryList{}
	_ = rec.CacheClient.List(context.TODO(), &client.ListOptions{
		LabelSelector: v1alpha1.GetServiceSelector(codeRepoService.GetName()),
		Namespace:     bindingCopy.Namespace,
	}, localRepos)

	// fetch repos from the remote git server
	remoteRepos, err := rec.getRemoteRepos(bindingCopy)
	if err != nil {
		glog.Errorf("[%s] GetSyncRepos error %s", controllerName, err)
		return bindingConditions, err
	}

	codeRepoIndex := map[string]*v1alpha1.CodeRepository{}
	bindingConditions = make([]v1alpha1.BindingCondition, 0, len(remoteRepos))
	for _, remoteRepo := range remoteRepos {
		newLocalRepo := rec.generateLocalRepo(remoteRepo, bindingCopy)

		now := metav1.NewTime(time.Now())
		// add repo condition to status
		condition := v1alpha1.BindingCondition{
			Type:        v1alpha1.JenkinsBindingStatusTypeRepository,
			Name:        newLocalRepo.Name,
			LastAttempt: &now,
			Message:     "",
			Reason:      "",
			Owner:       syncRepoOwner,
			Status:      v1alpha1.JenkinsBindingStatusConditionStatusReady,
		}

		// sync repo to k8s
		var foundRepo *v1alpha1.CodeRepository
		for _, localRepo := range localRepos.Items {
			if newLocalRepo.GetName() == localRepo.GetName() {
				foundRepo = &localRepo
				break
			}
		}
		err = rec.processCodeRepository(foundRepo, &newLocalRepo, bindingCopy.Namespace, codeRepoIndex)
		if err != nil {
			glog.Errorf("[%s] error processing CodeRepository: %v", controllerName, err)
			condition.Status = v1alpha1.StatusError
			condition.Message = err.Error()
			condition.Reason = err.Error()
		}

		bindingConditions = append(bindingConditions, condition)
	}

	for _, local := range localRepos.Items {
		// if not already synced, and has owner for CodeRepoBinding should delete
		if _, ok := codeRepoIndex[local.GetName()]; !ok && generic.HasOwner(&local, bindingCopy) {
			glog.V(6).Infof("[%s] will delete CodeRepository %s/%s as it is not bound anymore", controllerName, local.Namespace, local.Name)
			err = rec.CacheClient.Delete(context.TODO(), &local)
			if err != nil {
				glog.Errorf("[%s] error delete CodeRepository %s/%s: err %#v", controllerName, local.Namespace, local.Name, err)
			}
		}
	}

	return bindingConditions, nil
}

func (rec *Reconciler) processCodeRepository(foundRepo, newLocalRepo *v1alpha1.CodeRepository, namespace string, codeRepoIndex map[string]*v1alpha1.CodeRepository) (err error) {
	if newLocalRepo == nil {
		return
	}
	if foundRepo == nil {
		glog.V(5).Infof("[%s] creating CodeRepository %s", controllerName, newLocalRepo.GetName())
		codeRepoIndex[newLocalRepo.GetName()] = newLocalRepo
		err = rec.CacheClient.Create(context.TODO(), newLocalRepo)
		return
	}
	// no update
	glog.V(5).Infof("[%s] updating CodeRepository %s, local pushed at %s remote %s", controllerName, newLocalRepo.GetName(),
		foundRepo.Spec.Repository.PushedAt, newLocalRepo.Spec.Repository.PushedAt,
	)
	foundRepoCopy := foundRepo.DeepCopy()
	codeRepoIndex[foundRepoCopy.GetName()] = foundRepoCopy

	// check if the repo was updated, and then update our repo here and its status
	if foundRepoCopy.Spec.Repository.PushedAt != nil && newLocalRepo.Spec.Repository.PushedAt != nil &&
		!foundRepoCopy.Spec.Repository.PushedAt.Before(newLocalRepo.Spec.Repository.PushedAt) {
		glog.V(5).Infof("[%s] updating CodeRepository %s skipped, pushed at not changed", controllerName, newLocalRepo.GetName())
		return
	}
	// update spec
	foundRepoCopy.Spec.Repository = newLocalRepo.Spec.Repository

	err = rec.CacheClient.Update(context.TODO(), foundRepoCopy)
	return
}

// getRemoteRepos get the repos included in binding which will be sync.
func (rec *Reconciler) getRemoteRepos(bindingCopy *v1alpha1.CodeRepoBinding) (remoteRepos []v1alpha1.OriginCodeRepository, repoErr error) {
	remoteRepos = []v1alpha1.OriginCodeRepository{}
	opts := &v1alpha1.CodeRepoBindingRepositoryOptions{}
	remoteRepositories, err := rec.DevOpsClient.DevopsV1alpha1().CodeRepoBindings(bindingCopy.Namespace).GetRemoteRepositories(bindingCopy.Name, opts)
	if err != nil {
		glog.Errorf("[%s] Error fetch remote repositories, err: %v", controllerName, err)
		bindingCopy.Status.Message = err.Error()
		return remoteRepos, err
	}

	for _, bindingOwner := range bindingCopy.Spec.Account.Owners {
		for _, remoteOwner := range remoteRepositories.Owners {
			if bindingOwner.Type == remoteOwner.Type && bindingOwner.Name == remoteOwner.Name {
				if bindingOwner.All {
					remoteRepos = append(remoteRepos, remoteOwner.Repositories...)
					continue
				}

				for _, bindingRepo := range bindingOwner.Repositories {
					for _, remoteRepo := range remoteOwner.Repositories {
						if bindingRepo == remoteRepo.Name {
							remoteRepos = append(remoteRepos, remoteRepo)
						}
					}
				}
			}
		}
	}

	return remoteRepos, nil
}

// generateLocalRepo generate local repo by sync repo defined
func (rec *Reconciler) generateLocalRepo(remoteRepo v1alpha1.OriginCodeRepository, binding *v1alpha1.CodeRepoBinding) (localRepo v1alpha1.CodeRepository) {
	serviceName := binding.GetLabels()[v1alpha1.LabelCodeRepoService]
	snippets := []string{serviceName}
	names := strings.Split(remoteRepo.FullName, "/")
	for _, name := range names {
		snippets = append(snippets, name)
	}
	repoName := strings.Join(snippets, "-")

	localRepo = v1alpha1.CodeRepository{
		TypeMeta: metav1.TypeMeta{
			Kind:       v1alpha1.TypeCodeRepository,
			APIVersion: v1alpha1.APIVersionV1Alpha1,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:            repoName,
			Namespace:       binding.GetNamespace(),
			OwnerReferences: []metav1.OwnerReference{},
		},
		Spec: v1alpha1.CodeRepositorySpec{
			CodeRepoBinding: v1alpha1.LocalObjectReference{
				Name: binding.GetName(),
			},
			Repository: remoteRepo,
		},
	}
	return
}

func (rec *Reconciler) reconcileSecretOwnerReference(binding *v1alpha1.CodeRepoBinding) error {
	secret := &corev1.Secret{}
	err := rec.CacheClient.Get(context.TODO(), client.ObjectKey{Namespace: binding.GetSecretNamespace(), Name: binding.GetSecretName()}, secret)
	if err != nil {
		glog.Errorf("[%s] error when get coderepobinding's secret %s, err: %v", controllerName, binding.GetSecretName(), err)
		return err
	}

	glog.V(6).Infof("[%s] add binding refer %s in secret %s", controllerName, binding.GetName(), secret.GetName())

	// no need to add Reference under different namespaces
	// DEVOPS-4020
	// secret and coderepobinding namespaces are inconsistent
	// so do not added ownerReference for credentials
	if secret.Namespace != binding.Namespace {
		glog.V(3).Infof("secret and coderepobinding namespaces are inconsistent, secret: %s/%s, coderepobindg: %s/%s",
			secret.Namespace, secret.Name, binding.Namespace, binding.Name)
		return nil
	}

	// we must ensure current secret is not set owner reference to current coderepobinding
	if secret.Type != v1alpha1.SecretTypeOAuth2 {
		return nil
	}

	var errRemove, errCreate error
	// we must ensure current secret is set owner reference to current coderepobinding
	errCreate = rec.addBindingOwnerReferenceInSecret(secret, binding)
	if errCreate != nil {
		glog.Errorf("[%s] add coderepobinding %s/%s owner reference in secret %s/%s error:%s", controllerName, binding.Namespace, binding.Name, secret.Namespace, secret.Name, errCreate.Error())
	}

	// we must ensure others secrets is not set owner reference to current coderepobinding
	oauth2Secrets := &corev1.SecretList{}
	err = rec.CacheClient.List(context.TODO(), &client.ListOptions{
		FieldSelector: fields.OneTermEqualSelector("type", string(v1alpha1.SecretTypeOAuth2)),
		Namespace:     binding.Namespace,
	}, oauth2Secrets)

	if err != nil {
		glog.Errorf("[%s] list secret of OAuth2 error %#v", controllerName, err)
	} else {
		begin := time.Now()
		glog.V(5).Infof("[%s] ensure other secrets is not set ownerreference 'coderepobinding|%s/%s'", controllerName, binding.Namespace, binding.Name)
		for _, otherSecret := range oauth2Secrets.Items {
			// we does not care about current secret, we have already deal it
			if otherSecret.UID == secret.UID {
				continue
			}

			// other secret should not be set owner reference to current coderepobinding
			err = rec.removeBindingOwnerReferenceInSecret(&otherSecret, binding)
			if err != nil {
				glog.Errorf("[%s] remove owner reference 'coderepobinding|%s/%s' of secret '%s/%s' error:%#v", controllerName, binding.Namespace, binding.Name, otherSecret.Namespace, otherSecret.Name, err.Error())
			}
		}
		glog.V(5).Infof("[%s] ensured other secrets is not set ownerreference 'coderepobinding|%s/%s', elapsed %fs ", controllerName, binding.Namespace, binding.Name, time.Now().Sub(begin).Seconds())
	}

	if errRemove != nil {
		return errRemove
	}
	if errCreate != nil {
		return errCreate
	}
	return nil
}

func (c *Reconciler) removeBindingOwnerReferenceInSecret(secret *corev1.Secret, binding *v1alpha1.CodeRepoBinding) error {

	newSecretCopy := secret.DeepCopy()
	if len(newSecretCopy.OwnerReferences) == 0 {
		return nil
	}

	changed := generic.RemoveOwner(secret, binding)

	if changed {
		glog.V(6).Infof("[%s] removing coderepobinding reference '%s/%s' in secret '%s/%s'", controllerName, binding.Namespace, binding.GetName(), secret.GetNamespace(), secret.GetName())
		return c.updateSecret(newSecretCopy)
	}
	return nil
}

func (c *Reconciler) addBindingOwnerReferenceInSecret(secret *corev1.Secret, binding *v1alpha1.CodeRepoBinding) error {
	glog.V(6).Infof("[%s] add binding refer %s in secret %s", controllerName, binding.GetName(), secret.GetName())

	newSecretCopy := secret.DeepCopy()

	generic.AddOwner(newSecretCopy, binding, groupVersionKind)

	return c.updateSecret(newSecretCopy)
}

func (c *Reconciler) updateSecret(secret *corev1.Secret) error {
	err := c.CacheClient.Update(context.TODO(), secret)
	if err != nil {
		glog.Errorf("[%s] error when update secret, err: %v", controllerName, err)
		return err
	}
	return nil
}
