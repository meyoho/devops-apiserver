package coderepobinding_test

import (
	"context"
	"fmt"
	"log"

	"alauda.io/devops-apiserver/pkg/controller/testtools"
	"sigs.k8s.io/controller-runtime/pkg/client"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	coderepository "alauda.io/devops-apiserver/pkg/controller/devops/coderepobinding"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var _ = Describe("Reconciler.Reconcile", func() {
	var (
		mockCtrl *gomock.Controller
		// k8sClient    *k8sfake.Clientset
		devopsClient *clientset.Clientset
		cacheClient  client.Client
		request      reconcile.Request
		result       reconcile.Result
		reconciler   reconcile.Reconciler
		// roundTripper    *mhttp.MockRoundTripper
		err             error
		codeRepoBinding *devopsv1alpha1.CodeRepoBinding
		// systemNamespace = "alauda-system"
		provider devopsv1alpha1.AnnotationProvider
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		devopsClient = clientset.NewSimpleClientset()
		cacheClient = testtools.NewFakeClient()
		// roundTripper = mhttp.NewMockRoundTripper(mockCtrl)
		codeRepoBinding = GetCodeRepoBinding()
		provider = devopsv1alpha1.AnnotationProvider{BaseDomain: "GetAnnotationProvider"}

		reconciler = coderepository.NewReconciler(cacheClient, devopsClient, provider)

		request.Name = "devops-demo1"
		request.Namespace = "namespace1"
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})
	JustBeforeEach(
		func() {
			log.Println("test will reconcile")
			result, err = reconciler.Reconcile(request)
			fmt.Printf("Reconcile........ \n")
		})

	Context("codereposervice is does exist", func() {
		BeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset()
			reconciler = coderepository.NewReconciler(cacheClient, devopsClient, provider)
		})
		It("should delete coderepobinding ", func() {
			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))

			crb := &devopsv1alpha1.CodeRepoBinding{}
			err = cacheClient.Get(context.TODO(), client.ObjectKey{Namespace: codeRepoBinding.Namespace, Name: codeRepoBinding.Name}, crb)

			Expect(err).NotTo(BeNil())
			isNotFoudErr := errors.IsNotFound(err)
			Expect(isNotFoudErr).To(BeTrue())
		})
	})

	Context("every thing is ready", func() {
		BeforeEach(func() {
			devopsClient = clientset.NewSimpleClientset()
			reconciler = coderepository.NewReconciler(cacheClient, devopsClient, provider)
		})
		It("should sync coderepository to k8s", func() {

			Expect(err).To(BeNil())
			Expect(result).To(Equal(reconcile.Result{}))
		})
	})
})

func GetCodeRepoBinding() *devopsv1alpha1.CodeRepoBinding {
	return &devopsv1alpha1.CodeRepoBinding{
		TypeMeta: metav1.TypeMeta{
			APIVersion: devopsv1alpha1.APIVersionV1Alpha1,
			Kind:       devopsv1alpha1.TypeCodeRepoBinding,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: "github",
		},
		Spec: devopsv1alpha1.CodeRepoBindingSpec{
			CodeRepoService: devopsv1alpha1.LocalObjectReference{
				Name: "github",
			},
			Account: devopsv1alpha1.CodeRepoBindingAccount{
				Secret: devopsv1alpha1.SecretKeySetRef{
					SecretReference: corev1.SecretReference{
						Name:      "github-secret",
						Namespace: "namespace1",
					},
				},
			},
		},
	}
}
