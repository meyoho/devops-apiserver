package coderepobinding_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/devops/coderepobinding"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/testtools"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

func TestCodeRepoBindingController(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("coderepobinding.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/devops/coderepobinding", []Reporter{junitReporter})
}

var _ = Describe("Add",
	testtools.GenControllerSetupTest(
		"coderepobinding-controller",
		coderepobinding.Add,
		&source.Kind{Type: &v1alpha1.CodeRepoBinding{}},
		&handler.EnqueueRequestForObject{},
		&predicate.PhaseTTLPredicate{Type: "coderepobinding-controller", TTL: v1alpha1.TTLSession},
	),
)

/*

var _ = Describe("Add", func() {

	// Will test the bootstrapping of the controller
	var (
		mockCtrl         *gomock.Controller
		managerMock      *devopsctrlmock.MockManager
		controllerMock   *sigsctrlmock.MockController
		k8sClient        *k8sfake.Clientset
		devopsClient     *clientset.Clientset
		thirdPartyClient thirdparty.Interface
		err              error
		controllerName   string
	)

	BeforeEach(func() {
		// starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		// constructing our mocks
		managerMock = devopsctrlmock.NewMockManager(mockCtrl)
		controllerMock = sigsctrlmock.NewMockController(mockCtrl)

		k8sClient = k8sfake.NewSimpleClientset()
		devopsClient = clientset.NewSimpleClientset()
		thirdPartyClient, _ = thirdparty.New(nil, k8sClient)
		controllerName = "coderepobinding-controller"
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should create a new coderepobinding controller using manager", func() {
		managerMock.EXPECT().GetKubeClient().Return(k8sClient)
		managerMock.EXPECT().GetDevOpsClient().Return(devopsClient)
		managerMock.EXPECT().GetThirdParty().Return(thirdPartyClient)
		managerMock.EXPECT().NewController(
			controllerName, gomock.Any(),
		).Return(controllerMock, nil)

		controllerMock.EXPECT().Watch(
			gomock.Any(),
			&handler.EnqueueRequestForObject{},
			gomock.Any(),
		).Return(nil)

		err = coderepobinding.Add(managerMock)
		Expect(err).To(BeNil())
	})

	It("should return an error when failing to create coderepobinding controller", func() {
		managerMock.EXPECT().GetKubeClient().Return(k8sClient)
		managerMock.EXPECT().GetDevOpsClient().Return(devopsClient)
		managerMock.EXPECT().GetThirdParty().Return(thirdPartyClient)

		managerMock.EXPECT().NewController(
			controllerName, gomock.Any(),
		).Return(nil, fmt.Errorf("create controller error"))

		err = coderepobinding.Add(managerMock)
		Expect(err).To(Equal(fmt.Errorf("create controller error")))
	})
})
*/
