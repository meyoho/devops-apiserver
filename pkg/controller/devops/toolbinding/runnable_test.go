package toolbinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("Templateannotation", func() {

	var (
		bindingObjectMeta *v1.ObjectMeta
		tool              v1alpha1.ToolInterface
		toolLabels        map[string]string
		labelKeys         []string
		baseDomain        string
	)

	BeforeEach(func() {
		bindingObjectMeta = &v1.ObjectMeta{Name: "test", Namespace: "testnamespace", Labels: make(map[string]string)}

		tool = &v1alpha1.ArtifactRegistry{
			TypeMeta: v1.TypeMeta{
				Kind: "ArtifactRegistry",
			},
			ObjectMeta: v1.ObjectMeta{
				Name: "kftest",
			},
		}
		labelKeys = []string{v1alpha1.LabelToolItemPublicString, v1alpha1.LabelToolTypeString}
		baseDomain = "test.io"
		toolLabels = map[string]string{baseDomain + "/" + v1alpha1.LabelToolItemPublicString: "false", baseDomain + "/" + v1alpha1.LabelToolTypeString: "artifactregistry"}
	})

	Context("test processLabel", func() {

		It("all right", func() {
			bom := bindingObjectMeta.DeepCopy()
			t := &v1alpha1.ArtifactRegistry{
				TypeMeta: v1.TypeMeta{
					Kind: tool.GetKind(),
				},
				ObjectMeta: v1.ObjectMeta{
					Name:   tool.GetObjectMeta().GetName(),
					Labels: toolLabels,
				},
			}

			isUpdate, error := processLabel(bom, t, labelKeys, baseDomain)
			Expect(isUpdate).To(Equal(true))
			Expect(error).To(BeNil())
			Expect(bom.Labels[v1alpha1.LabelToolItemPublicString]).To(Equal("false"))
			Expect(bom.Labels[v1alpha1.LabelToolTypeString]).To(Equal("artifactregistry"))
		})

		It("binding label is nil", func() {
			bom := bindingObjectMeta.DeepCopy()
			bom.Labels = nil
			t := &v1alpha1.ArtifactRegistry{
				TypeMeta: v1.TypeMeta{
					Kind: tool.GetKind(),
				},
				ObjectMeta: v1.ObjectMeta{
					Name:   tool.GetObjectMeta().GetName(),
					Labels: toolLabels,
				},
			}

			isUpdate, error := processLabel(bom, t, labelKeys, baseDomain)
			Expect(isUpdate).To(Equal(true))
			Expect(error).To(BeNil())
			Expect(bom.Labels[v1alpha1.LabelToolItemPublicString]).To(Equal("false"))
			Expect(bom.Labels[v1alpha1.LabelToolTypeString]).To(Equal("artifactregistry"))
		})

		It("have at less one label", func() {
			bom := bindingObjectMeta.DeepCopy()
			bom.Labels[v1alpha1.LabelToolItemPublicString]="true"
			t := &v1alpha1.ArtifactRegistry{
				TypeMeta: v1.TypeMeta{
					Kind: tool.GetKind(),
				},
				ObjectMeta: v1.ObjectMeta{
					Name:   tool.GetObjectMeta().GetName(),
					Labels: toolLabels,
				},
			}

			isUpdate, error := processLabel(bom, t, labelKeys, baseDomain)
			Expect(isUpdate).To(Equal(true))
			Expect(error).To(BeNil())
			Expect(bom.Labels[v1alpha1.LabelToolItemPublicString]).To(Equal("true"))
			Expect(bom.Labels[v1alpha1.LabelToolTypeString]).To(Equal("artifactregistry"))
		})

		It("have two label", func() {
			bom := bindingObjectMeta.DeepCopy()
			bom.Labels[v1alpha1.LabelToolItemPublicString]="true"
			bom.Labels[v1alpha1.LabelToolTypeString]="codereposervice"
			t := &v1alpha1.ArtifactRegistry{
				TypeMeta: v1.TypeMeta{
					Kind: tool.GetKind(),
				},
				ObjectMeta: v1.ObjectMeta{
					Name:   tool.GetObjectMeta().GetName(),
					Labels: toolLabels,
				},
			}

			isUpdate, error := processLabel(bom, t, labelKeys, baseDomain)
			Expect(isUpdate).To(Equal(false))
			Expect(error).To(BeNil())
			Expect(bom.Labels[v1alpha1.LabelToolItemPublicString]).To(Equal("true"))
			Expect(bom.Labels[v1alpha1.LabelToolTypeString]).To(Equal("codereposervice"))
		})

		It("tool label is nil", func() {
			bom := bindingObjectMeta.DeepCopy()

			t := &v1alpha1.ArtifactRegistry{
				TypeMeta: v1.TypeMeta{
					Kind: tool.GetKind(),
				},
				ObjectMeta: v1.ObjectMeta{
					Name:   tool.GetObjectMeta().GetName(),
					Labels: nil,
				},
			}

			isUpdate, error := processLabel(bom, t, labelKeys, baseDomain)
			Expect(isUpdate).To(Equal(false))
			Expect(error.Error()).To(Equal("[toolbinding-runnable/processLabel] process binding: [testnamespace/test] with kind ArtifactRegistry tool kftest has not set toolItemPublic"))
			Expect(bom.Labels[v1alpha1.LabelToolItemPublicString]).To(Equal(""))
			Expect(bom.Labels[v1alpha1.LabelToolTypeString]).To(Equal(""))
		})

		It("tool label has not set label", func() {
			bom := bindingObjectMeta.DeepCopy()

			t := &v1alpha1.ArtifactRegistry{
				TypeMeta: v1.TypeMeta{
					Kind: tool.GetKind(),
				},
				ObjectMeta: v1.ObjectMeta{
					Name:   tool.GetObjectMeta().GetName(),
					Labels: make(map[string]string),
				},
			}

			isUpdate, error := processLabel(bom, t, labelKeys, baseDomain)
			Expect(isUpdate).To(Equal(false))
			Expect(error.Error()).To(Equal("[toolbinding-runnable/processLabel] process binding: [testnamespace/test] with kind ArtifactRegistry tool kftest has not set toolItemPublic"))
			Expect(bom.Labels[v1alpha1.LabelToolItemPublicString]).To(Equal(""))
			Expect(bom.Labels[v1alpha1.LabelToolTypeString]).To(Equal(""))
		})
	})
})
