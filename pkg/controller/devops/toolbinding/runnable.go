package toolbinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"fmt"
	"k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
	"sync"
)

const toolxPath = "./toolx"

func AddRunnable(manager manager.Manager) error {

	return manager.Add(newRunnable(manager.GetKubeClient(), manager.GetDevOpsClient(), manager.GetAnnotationProvider()))
}

type Runnable struct {
	kubeClient   kubernetes.Interface
	devopsClient versioned.Interface
	provider     v1alpha1.AnnotationProvider
}

var _ sigsmanager.Runnable = &Runnable{}

func (r *Runnable) Start(s <-chan struct{}) (err error) {

	logName := "toolbinding-runnable"
	klog.V(5).Infof("[%s] Starting runnable", logName)

	//new key
	toolItemPublicString := v1alpha1.LabelToolItemPublicString
	toolTypeString := v1alpha1.LabelToolTypeString

	type Function struct {
		Id               string
		ProcessLabelFunc func()
	}

	f := make([]Function, 0)

	//artifactregistrybinding go routing
	f = append(f, Function{Id: "artifactregistrybinding", ProcessLabelFunc: func() {

		//get bindinglist
		bindingList, err := r.devopsClient.DevopsV1alpha1().ArtifactRegistryBindings("").List(v1alpha1.ListOptions())
		if err != nil {
			klog.V(3).Infof("[%s/%s] get bindings error,error is: %s", logName, "Start", err.Error())

			return
		}

		//loop bindinglist
		for _, originBinding := range bindingList.Items {

			// annotation for public start

			binding := originBinding.DeepCopy()
			klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

			klog.V(5).Infof("[%s/%s] binding: [%s/%s]", logName, "Start", binding.Namespace, binding.Name)

			//get binding`s labels
			bindingObjectMeta := binding.ObjectMeta

			//get tool
			tool, err := r.devopsClient.DevopsV1alpha1().ArtifactRegistries().Get(binding.Spec.ArtifactRegistry.Name, v1alpha1.GetOptions())
			if err != nil {
				klog.V(3).Infof("[%s/%s] binding: [%s/%s],get tool: [%s] error,error is: %s", logName, "Start", binding.Namespace, binding.Name, binding.Spec.ArtifactRegistry.Name, err.Error())

				continue
			}

			klog.V(9).Infof("[%s/%s] binding: [%s/%s],get tool: %s", logName, "Start", binding.Namespace, binding.Name, tool.GetName())

			isUpdate, err := processLabel(&bindingObjectMeta, tool, []string{toolItemPublicString, toolTypeString}, r.provider.BaseDomain)
			if err != nil {
				continue
			}

			if isUpdate {
				klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

				_, err = r.devopsClient.DevopsV1alpha1().ArtifactRegistryBindings(binding.GetNamespace()).Update(binding)
				if err != nil {
					klog.V(3).Infof("[%s/%s] binding: [%s/%s],update toolbinding error,error is: %s", logName, "Start", binding.Namespace, binding.Name, err.Error())
					continue
				}
				klog.V(5).Infof("[%s/%s] binding: [%s/%s],update toolbinding", logName, "Start", binding.Namespace, binding.Name)
			}

		}
	}})

	//codequalitybinding go routing
	f = append(f, Function{Id: "codequalitybinding", ProcessLabelFunc: func() {

		//get bindinglist
		bindingList, err := r.devopsClient.DevopsV1alpha1().CodeQualityBindings("").List(v1alpha1.ListOptions())
		if err != nil {
			klog.V(3).Infof("[%s/%s] get bindings error,error is: %s", logName, "Start", err.Error())

			return
		}

		//loop bindinglist
		for _, originBinding := range bindingList.Items {

			// annotation for public start

			binding := originBinding.DeepCopy()
			klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

			klog.V(5).Infof("[%s/%s] binding: [%s/%s]", logName, "Start", binding.Namespace, binding.Name)

			//get binding`s labels
			bindingObjectMeta := binding.ObjectMeta

			//get tool
			tool, err := r.devopsClient.DevopsV1alpha1().CodeQualityTools().Get(binding.Spec.CodeQualityTool.Name, v1alpha1.GetOptions())
			if err != nil {
				klog.V(3).Infof("[%s/%s] binding: [%s/%s],get tool: [%s] error,error is: %s", logName, "Start", binding.Namespace, binding.Name, binding.Spec.CodeQualityTool.Name, err.Error())

				continue
			}

			klog.V(9).Infof("[%s/%s] binding: [%s/%s],get tool: %s", logName, "Start", binding.Namespace, binding.Name, tool.GetName())

			isUpdate, err := processLabel(&bindingObjectMeta, tool, []string{toolItemPublicString, toolTypeString}, r.provider.BaseDomain)
			if err != nil {
				continue
			}

			if isUpdate {
				klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

				_, err = r.devopsClient.DevopsV1alpha1().CodeQualityBindings(binding.GetNamespace()).Update(binding)
				if err != nil {
					klog.V(3).Infof("[%s/%s] binding: [%s/%s],update toolbinding error,error is: %s", logName, "Start", binding.Namespace, binding.Name, err.Error())
					continue
				}
				klog.V(5).Infof("[%s/%s] binding: [%s/%s],update toolbinding", logName, "Start", binding.Namespace, binding.Name)
			}

		}
	}})

	//codeRepoBindingList go routing
	f = append(f, Function{Id: "codeRepoBinding", ProcessLabelFunc: func() {

		//get bindinglist
		bindingList, err := r.devopsClient.DevopsV1alpha1().CodeRepoBindings("").List(v1alpha1.ListOptions())
		if err != nil {
			klog.V(3).Infof("[%s/%s] get bindings error,error is: %s", logName, "Start", err.Error())

			return
		}

		//loop bindinglist
		for _, originBinding := range bindingList.Items {

			// annotation for public start

			binding := originBinding.DeepCopy()
			klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

			klog.V(5).Infof("[%s/%s] binding: [%s/%s]", logName, "Start", binding.Namespace, binding.Name)

			//get binding`s labels
			bindingObjectMeta := binding.ObjectMeta

			//get tool
			tool, err := r.devopsClient.DevopsV1alpha1().CodeRepoServices().Get(binding.Spec.CodeRepoService.Name, v1alpha1.GetOptions())
			if err != nil {
				klog.V(3).Infof("[%s/%s] binding: [%s/%s],get tool: [%s] error,error is: %s", logName, "Start", binding.Namespace, binding.Name, binding.Spec.CodeRepoService.Name, err.Error())

				continue
			}

			klog.V(9).Infof("[%s/%s] binding: [%s/%s],get tool: %s", logName, "Start", binding.Namespace, binding.Name, tool.GetName())

			isUpdate, err := processLabel(&bindingObjectMeta, tool, []string{toolItemPublicString, toolTypeString}, r.provider.BaseDomain)
			if err != nil {
				continue
			}

			if isUpdate {
				klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

				_, err = r.devopsClient.DevopsV1alpha1().CodeRepoBindings(binding.GetNamespace()).Update(binding)
				if err != nil {
					klog.V(3).Infof("[%s/%s] binding: [%s/%s],update toolbinding error,error is: %s", logName, "Start", binding.Namespace, binding.Name, err.Error())
					continue
				}
				klog.V(5).Infof("[%s/%s] binding: [%s/%s],update toolbinding", logName, "Start", binding.Namespace, binding.Name)
			}

		}
	}})

	//documentManagementBinding go routing
	f = append(f, Function{Id: "documentManagementBinding", ProcessLabelFunc: func() {

		//get bindinglist
		bindingList, err := r.devopsClient.DevopsV1alpha1().DocumentManagementBindings("").List(v1alpha1.ListOptions())
		if err != nil {
			klog.V(3).Infof("[%s/%s] get bindings error,error is: %s", logName, "Start", err.Error())

			return
		}

		//loop bindinglist
		for _, originBinding := range bindingList.Items {

			// annotation for public start

			binding := originBinding.DeepCopy()
			klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

			klog.V(5).Infof("[%s/%s] binding: [%s/%s]", logName, "Start", binding.Namespace, binding.Name)

			//get binding`s labels
			bindingObjectMeta := binding.ObjectMeta

			//get tool
			tool, err := r.devopsClient.DevopsV1alpha1().DocumentManagements().Get(binding.Spec.DocumentManagement.Name, v1alpha1.GetOptions())
			if err != nil {
				klog.V(3).Infof("[%s/%s] binding: [%s/%s],get tool: [%s] error,error is: %s", logName, "Start", binding.Namespace, binding.Name, binding.Spec.DocumentManagement.Name, err.Error())

				continue
			}

			klog.V(9).Infof("[%s/%s] binding: [%s/%s],get tool: %s", logName, "Start", binding.Namespace, binding.Name, tool.GetName())

			isUpdate, err := processLabel(&bindingObjectMeta, tool, []string{toolItemPublicString, toolTypeString}, r.provider.BaseDomain)
			if err != nil {
				continue
			}

			if isUpdate {
				klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

				_, err = r.devopsClient.DevopsV1alpha1().DocumentManagementBindings(binding.GetNamespace()).Update(binding)
				if err != nil {
					klog.V(3).Infof("[%s/%s] binding: [%s/%s],update toolbinding error,error is: %s", logName, "Start", binding.Namespace, binding.Name, err.Error())
					continue
				}
				klog.V(5).Infof("[%s/%s] binding: [%s/%s],update toolbinding", logName, "Start", binding.Namespace, binding.Name)
			}

		}
	}})

	//imageRegistryBinding go routing
	f = append(f, Function{Id: "imageRegistryBinding", ProcessLabelFunc: func() {

		//get bindinglist
		bindingList, err := r.devopsClient.DevopsV1alpha1().ImageRegistryBindings("").List(v1alpha1.ListOptions())
		if err != nil {
			klog.V(3).Infof("[%s/%s] get bindings error,error is: %s", logName, "Start", err.Error())

			return
		}

		//loop bindinglist
		for _, originBinding := range bindingList.Items {

			// annotation for public start

			binding := originBinding.DeepCopy()
			klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

			klog.V(5).Infof("[%s/%s] binding: [%s/%s]", logName, "Start", binding.Namespace, binding.Name)

			//get binding`s labels
			bindingObjectMeta := binding.ObjectMeta

			//get tool
			tool, err := r.devopsClient.DevopsV1alpha1().ImageRegistries().Get(binding.Spec.ImageRegistry.Name, v1alpha1.GetOptions())
			if err != nil {
				klog.V(3).Infof("[%s/%s] binding: [%s/%s],get tool: [%s] error,error is: %s", logName, "Start", binding.Namespace, binding.Name, binding.Spec.ImageRegistry.Name, err.Error())

				continue
			}

			klog.V(9).Infof("[%s/%s] binding: [%s/%s],get tool: %s", logName, "Start", binding.Namespace, binding.Name, tool.GetName())

			isUpdate, err := processLabel(&bindingObjectMeta, tool, []string{toolItemPublicString, toolTypeString}, r.provider.BaseDomain)
			if err != nil {
				continue
			}

			if isUpdate {
				klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

				_, err = r.devopsClient.DevopsV1alpha1().ImageRegistryBindings(binding.GetNamespace()).Update(binding)
				if err != nil {
					klog.V(3).Infof("[%s/%s] binding: [%s/%s],update toolbinding error,error is: %s", logName, "Start", binding.Namespace, binding.Name, err.Error())
					continue
				}
				klog.V(5).Infof("[%s/%s] binding: [%s/%s],update toolbinding", logName, "Start", binding.Namespace, binding.Name)
			}

		}
	}})

	//jenkinsBinding go routing
	f = append(f, Function{Id: "jenkinsBinding", ProcessLabelFunc: func() {

		//get bindinglist
		bindingList, err := r.devopsClient.DevopsV1alpha1().JenkinsBindings("").List(v1alpha1.ListOptions())
		if err != nil {
			klog.V(3).Infof("[%s/%s] get bindings error,error is: %s", logName, "Start", err.Error())

			return
		}

		//loop bindinglist
		for _, originBinding := range bindingList.Items {

			// annotation for public start

			binding := originBinding.DeepCopy()
			klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

			klog.V(5).Infof("[%s/%s] binding: [%s/%s]", logName, "Start", binding.Namespace, binding.Name)

			//get binding`s labels
			bindingObjectMeta := binding.ObjectMeta

			//get tool
			tool, err := r.devopsClient.DevopsV1alpha1().Jenkinses().Get(binding.Spec.Jenkins.Name, v1alpha1.GetOptions())
			if err != nil {
				klog.V(3).Infof("[%s/%s] binding: [%s/%s],get tool: [%s] error,error is: %s", logName, "Start", binding.Namespace, binding.Name, binding.Spec.Jenkins.Name, err.Error())

				continue
			}

			klog.V(9).Infof("[%s/%s] binding: [%s/%s],get tool: %s", logName, "Start", binding.Namespace, binding.Name, tool.GetName())

			isUpdate, err := processLabel(&bindingObjectMeta, tool, []string{toolItemPublicString, toolTypeString}, r.provider.BaseDomain)
			if err != nil {
				continue
			}

			if isUpdate {
				klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

				_, err = r.devopsClient.DevopsV1alpha1().JenkinsBindings(binding.GetNamespace()).Update(binding)
				if err != nil {
					klog.V(3).Infof("[%s/%s] binding: [%s/%s],update toolbinding error,error is: %s", logName, "Start", binding.Namespace, binding.Name, err.Error())
					continue
				}
				klog.V(5).Infof("[%s/%s] binding: [%s/%s],update toolbinding", logName, "Start", binding.Namespace, binding.Name)
			}

		}
	}})

	//projectManagementBinding go routing
	f = append(f, Function{Id: "projectManagementBinding", ProcessLabelFunc: func() {

		//get bindinglist
		bindingList, err := r.devopsClient.DevopsV1alpha1().ProjectManagementBindings("").List(v1alpha1.ListOptions())
		if err != nil {
			klog.V(3).Infof("[%s/%s] get bindings error,error is: %s", logName, "Start", err.Error())

			return
		}

		//loop bindinglist
		for _, originBinding := range bindingList.Items {

			// annotation for public start

			binding := originBinding.DeepCopy()
			klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

			klog.V(5).Infof("[%s/%s] binding: [%s/%s]", logName, "Start", binding.Namespace, binding.Name)

			//get binding`s labels
			bindingObjectMeta := binding.ObjectMeta

			//get tool
			tool, err := r.devopsClient.DevopsV1alpha1().ProjectManagements().Get(binding.Spec.ProjectManagement.Name, v1alpha1.GetOptions())
			if err != nil {
				klog.V(3).Infof("[%s/%s] binding: [%s/%s],get tool: [%s] error,error is: %s", logName, "Start", binding.Namespace, binding.Name, binding.Spec.ProjectManagement.Name, err.Error())

				continue
			}

			klog.V(9).Infof("[%s/%s] binding: [%s/%s],get tool: %s", logName, "Start", binding.Namespace, binding.Name, tool.GetName())

			isUpdate, err := processLabel(&bindingObjectMeta, tool, []string{toolItemPublicString, toolTypeString}, r.provider.BaseDomain)
			if err != nil {
				continue
			}

			if isUpdate {
				klog.V(9).Infof("[%s/%s] binding: [%s/%s],binding is #%v", logName, "Start", binding.Namespace, binding.Name, binding)

				_, err = r.devopsClient.DevopsV1alpha1().ProjectManagementBindings(binding.GetNamespace()).Update(binding)
				if err != nil {
					klog.V(3).Infof("[%s/%s] binding: [%s/%s],update toolbinding error,error is: %s", logName, "Start", binding.Namespace, binding.Name, err.Error())
					continue
				}
				klog.V(5).Infof("[%s/%s] binding: [%s/%s],update toolbinding", logName, "Start", binding.Namespace, binding.Name)
			}

		}
	}})

	//set waitgroup
	wg := sync.WaitGroup{}
	wg.Add(len(f))

	for _, function := range f {

		go func(function Function) {
			klog.V(3).Infof("[%s/%s] start gorouting function is %s", logName, "Start", function.Id)
			defer wg.Done()
			function.ProcessLabelFunc()
		}(function)
	}

	wg.Wait()
	klog.V(3).Infof("[%s/%s] end", logName, "Start")

	<-s
	return err
}

func newRunnable(k kubernetes.Interface, d versioned.Interface, p v1alpha1.AnnotationProvider) *Runnable {
	return &Runnable{kubeClient: k, devopsClient: d, provider: p}
}

func processLabel(bindingObjectMeta *v1.ObjectMeta, tool v1alpha1.ToolInterface, labelKeys []string, baseDomain string) (isUpdate bool, err error) {

	logName := "toolbinding-runnable"
	processLabel := "processLabel"

	if bindingObjectMeta == nil || tool == nil {
		klog.V(3).Infof("[%s/%s] the bindingObjectMeta or tool is nil", logName, processLabel)

		return isUpdate, errors.NewBadRequest(fmt.Sprintf("[%s/%s] the bindingObjectMeta or tool is nil", logName, processLabel))

	}

	for _, labelKey := range labelKeys {
		klog.V(9).Infof("[%s/%s] binding: [%s/%s] process label: %s", logName, processLabel, bindingObjectMeta.Namespace, bindingObjectMeta.Name, labelKey)

		//checkout annotationKey
		if _, ok := (*bindingObjectMeta).Labels[labelKey]; !ok {
			klog.V(9).Infof("[%s/%s] binding: [%s/%s],the label: %s does not exist", logName, processLabel, bindingObjectMeta.Namespace, bindingObjectMeta.Name, labelKey)

			if bindingObjectMeta.GetLabels() == nil {
				bindingObjectMeta.Labels = make(map[string]string)
			}

			if value, ok := (tool.GetObjectMeta().GetLabels())[baseDomain+"/"+labelKey]; ok {
				bindingObjectMeta.Labels[labelKey] = value
				isUpdate = true
				klog.V(9).Infof("[%s/%s] binding: [%s/%s],set label: %s value is: %s", logName, processLabel, bindingObjectMeta.Namespace, bindingObjectMeta.Name, labelKey, value)

			} else {
				klog.V(3).Infof("[%s/%s] binding :[%s/%s],the label %s does not exist,the label does not exist in kind %s tool %s", logName, processLabel, bindingObjectMeta.Namespace, bindingObjectMeta.Name, labelKey, tool.GetKind(), tool.GetObjectMeta().GetName())

				return isUpdate, errors.NewBadRequest(fmt.Sprintf("[%s/%s] process binding: [%s/%s] with kind %s tool %s has not set %s", logName, processLabel, bindingObjectMeta.Namespace, bindingObjectMeta.Name, tool.GetKind(), tool.GetObjectMeta().GetName(), labelKey))
			}

		}
	}

	return isUpdate, nil
}
