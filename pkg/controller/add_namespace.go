package controller

import (
	"alauda.io/devops-apiserver/pkg/controller/core/namespace"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, namespace.Add)
	AddToManagerFuncs = append(AddToManagerFuncs, namespace.AddRunnable)
}
