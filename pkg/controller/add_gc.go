package controller

import (
	"alauda.io/devops-apiserver/pkg/controller/devops/gc"
)

func init() {
	AddToManagerFuncs = append(AddToManagerFuncs, gc.AddRunnable)
}
