package controller

import "alauda.io/devops-apiserver/pkg/controller/devops/notification"

func init() {
	AddToManagerFuncs = append(AddToManagerFuncs, notification.AddRunnable)
}
