package controller

import "alauda.io/devops-apiserver/pkg/controller/devops/jenkinssyncheartbeat"

func init() {
	AddToManagerFuncs = append(AddToManagerFuncs, jenkinssyncheartbeat.Add)
}
