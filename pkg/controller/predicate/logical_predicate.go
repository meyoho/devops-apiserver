package predicate

import (
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
)

type LogicalPredicate struct {
	predicates []predicate.Predicate
}

var _ predicate.Predicate = LogicalPredicate{}

// Create checks annotations on create
func (a LogicalPredicate) Create(evt event.CreateEvent) bool {
	if len(a.predicates) == 0 {
		return true
	}
	var res = a.predicates[0].Create(evt)
	if len(a.predicates) == 1 {
		return res
	}

	for _, item := range a.predicates[1:] {
		res = res && item.Create(evt)
	}
	return res
}

// Delete check annotations on delete
func (a LogicalPredicate) Delete(evt event.DeleteEvent) bool {
	if len(a.predicates) == 0 {
		return true
	}
	var res = a.predicates[0].Delete(evt)
	if len(a.predicates) == 1 {
		return res
	}

	for _, item := range a.predicates[1:] {
		res = res && item.Delete(evt)
	}
	return res
}

// Update check annotations on update
func (a LogicalPredicate) Update(evt event.UpdateEvent) bool {
	if len(a.predicates) == 0 {
		return true
	}
	var res = a.predicates[0].Update(evt)
	if len(a.predicates) == 1 {
		return res
	}

	for _, item := range a.predicates[1:] {
		res = res && item.Update(evt)
	}
	return res
}

// Generic checks annotations on generic event
func (a LogicalPredicate) Generic(evt event.GenericEvent) bool {
	if len(a.predicates) == 0 {
		return true
	}
	var res = a.predicates[0].Generic(evt)
	if len(a.predicates) == 1 {
		return res
	}

	for _, item := range a.predicates[1:] {
		res = res && item.Generic(evt)
	}
	return res
}

func And(predicates ...predicate.Predicate) predicate.Predicate {
	return LogicalPredicate{
		predicates: predicates,
	}
}
