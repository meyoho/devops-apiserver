package predicate_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/event"

	devopspredicate "alauda.io/devops-apiserver/pkg/controller/predicate"
)

var _ = Describe("PhasePredicate", func() {
	var (
		predicateObj devopspredicate.PhasePredicate

		obj    *v1alpha1.PipelineConfig
		result bool
	)

	ShouldBeTrue := func() {
		Expect(result).To(BeTrue(), "result should be true")
	}
	ShouldBeFalse := func() {
		Expect(result).To(BeFalse(), "result should be false")
	}

	AllCases := func() {
		Context("has expected phase", func() {
			BeforeEach(func() {
				obj.Status.Phase = v1alpha1.PipelineConfigPhaseCreating
			})
			It("should return true", ShouldBeTrue)
		})
		Context("has unexpected phase", func() {
			BeforeEach(func() {
				obj.Status.Phase = v1alpha1.PipelineConfigPhaseSyncing
			})
			It("should return false", ShouldBeFalse)
		})
	}

	BeforeEach(func() {
		predicateObj = devopspredicate.PhasePredicate{Phase: string(v1alpha1.PipelineConfigPhaseCreating)}
		obj = &v1alpha1.PipelineConfig{Status: v1alpha1.PipelineConfigStatus{Phase: v1alpha1.PipelineConfigPhaseCreating}}
	})

	Context("When phases contains target event phase", func() {
		var evt event.CreateEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
			predicateObj = devopspredicate.PhasePredicate{Phases: []string{string(v1alpha1.PipelineConfigPhaseCreating), string(v1alpha1.PipelineConfigPhaseSyncing)}}
		})
		JustBeforeEach(func() {
			result = predicateObj.Create(evt)
		})
		It("should return true", ShouldBeTrue)
	})

	Context("When phases not contains target event phase", func() {
		var evt event.CreateEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
			predicateObj = devopspredicate.PhasePredicate{Phases: []string{string(v1alpha1.PipelineConfigPhaseDisabled), string(v1alpha1.PipelineConfigPhaseSyncing)}}
		})
		JustBeforeEach(func() {
			result = predicateObj.Create(evt)
		})
		It("should return false", ShouldBeFalse)
	})

	Context("Create", func() {
		var evt event.CreateEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Create(evt)
		})
		AllCases()
	})

	Context("Update with New", func() {
		var evt event.UpdateEvent
		BeforeEach(func() {
			evt.MetaNew = obj
			evt.ObjectNew = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Update(evt)
		})
		AllCases()
	})

	Context("Update with Old", func() {
		var evt event.UpdateEvent
		BeforeEach(func() {
			evt.MetaOld = obj
			evt.ObjectOld = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Update(evt)
		})
		Context("has expected phase", func() {
			BeforeEach(func() {
				obj.Status.Phase = v1alpha1.PipelineConfigPhaseCreating
			})
			It("should return false", ShouldBeFalse)
		})
		Context("has unexpected phase", func() {
			BeforeEach(func() {
				obj.Status.Phase = v1alpha1.PipelineConfigPhaseSyncing
			})
			It("should return false", ShouldBeFalse)
		})
	})

	Context("Delete", func() {
		var evt event.DeleteEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Delete(evt)
		})
		AllCases()
	})

	Context("Generic", func() {
		var evt event.GenericEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Generic(evt)
		})
		AllCases()
	})
})
