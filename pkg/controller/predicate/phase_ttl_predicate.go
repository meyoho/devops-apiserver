package predicate

import (
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/controller/reconciler"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/generic"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/event"

	"sigs.k8s.io/controller-runtime/pkg/predicate"
)

const maxFactor = 0.5

// Jitter returns a random duration function using a max factor
func Jitter(duration time.Duration) func() time.Duration {
	newDuration := wait.Jitter(duration, maxFactor)
	return func() time.Duration {
		return newDuration
	}
}

// ConditionTTL inits a PhaseTTLPredicate, will use last attempt of conditons that owner specified
func ConditionTTL(controllerName string, ttl time.Duration, owner string) *PhaseTTLPredicate {
	return &PhaseTTLPredicate{
		Type: controllerName, TTL: ttl,
		GetLastAttempt: ConditionLastAttemptGetter(owner),
	}
}

// RoleSyncTTL inits a PhaseTTLPredicate with standard times for RoleSync logic
func RoleSyncTTL(controllerName string) *PhaseTTLPredicate {
	return ConditionTTL(controllerName, v1alpha1.TTLRoleSyncSession, reconciler.RoleSyncConditionOwner)
}

// EmptyOwnerTTL inits a PhaseTTLPredicate, will use last attempt of conditons that owner=""
func EmptyOwnerTTL(controllerName string, ttl time.Duration) *PhaseTTLPredicate {
	return ConditionTTL(controllerName, ttl, "")
}

// PhaseTTLPredicate adjusts a TTL or phase as parameter
// only should be used if the Resource implements v1alpha1.StatusAccessor interface
type PhaseTTLPredicate struct {
	TTL         time.Duration
	Type        string
	getDuration func() time.Duration

	GetLastAttempt func(status v1alpha1.StatusAccessor) *metav1.Time
}

var _ predicate.Predicate = &PhaseTTLPredicate{}

func (p *PhaseTTLPredicate) shouldReconcile(obj metav1.Object) (result bool) {
	p.init()
	if obj != nil && obj.(v1alpha1.StatusAccessor) != nil {
		statusAccessor := obj.(v1alpha1.StatusAccessor)
		var reason = ""
		result, reason = p.shouldCheck(statusAccessor)
		glog.V(8).Infof("[%s] PhaseTTLPredicate Object %s/%s will reconcile? %v, reason: '%s'", p.Type, obj.GetNamespace(), obj.GetName(), result, reason)
	}

	return
}

func (p *PhaseTTLPredicate) shouldCheck(statusAccessor v1alpha1.StatusAccessor) (res bool, reason string) {
	status := statusAccessor.GetStatus()
	if status == nil {
		return true, "not a statusAccessor"
	}

	if p.GetLastAttempt == nil {
		return generic.ShouldCheckStatus(status.Phase, status.HTTPStatus, p.TTL),
			fmt.Sprintf("should check, phase=%#v, HTTPStatus=%#v, TTL=%fmin", status.Phase, status.HTTPStatus, p.TTL.Minutes())
	}

	lastAttempt := p.GetLastAttempt(statusAccessor)
	if lastAttempt == nil {
		return true, "lastAttempt is nil"
	}

	return !generic.IsNotExpired(lastAttempt, p.TTL), fmt.Sprintf("lastAttempt Expired, lastAttempt:%s, TTL:%fmin ", lastAttempt.String(), p.TTL.Minutes())
}

func ConditionLastAttemptGetter(owner string) func(status v1alpha1.StatusAccessor) *metav1.Time {
	return func(statusAccessor v1alpha1.StatusAccessor) *metav1.Time {

		if statusAccessor == nil {
			return nil
		}
		status := statusAccessor.GetStatus()
		if status == nil {
			return nil
		}

		return getConditionLastAttempt(owner, status.Conditions)
	}
}

func getConditionLastAttempt(owner string, conds []v1alpha1.BindingCondition) *metav1.Time {
	if conds == nil {
		return nil
	}

	for _, cond := range conds {
		if cond.Owner == owner {
			// return the first cond
			return cond.LastAttempt
		}
	}

	return nil
}

func (p *PhaseTTLPredicate) init() {
	if p.getDuration == nil {
		p.getDuration = Jitter(p.TTL)
		p.TTL = p.getDuration()
		glog.V(8).Infof("[%s] PhaseTTLPredicate setting duration to %s", p.Type, p.TTL)
	}
}

// Create checks if an  event needs to reconcile
func (p *PhaseTTLPredicate) Create(evt event.CreateEvent) bool {
	return p.shouldReconcile(evt.Meta)
}

// Update checks if an event needs to reconcile
func (p *PhaseTTLPredicate) Update(evt event.UpdateEvent) bool {
	return p.shouldReconcile(evt.MetaNew) || p.shouldReconcile(evt.MetaOld)
}

// Delete checks if an event needs to reconcile
func (p *PhaseTTLPredicate) Delete(evt event.DeleteEvent) bool {
	// we should always deal with delete event
	return true
}

// Generic checks if an event needs to reconcile
func (p *PhaseTTLPredicate) Generic(evt event.GenericEvent) bool {
	return p.shouldReconcile(evt.Meta)
}
