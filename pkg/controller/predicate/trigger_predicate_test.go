package predicate_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/event"

	devopspredicate "alauda.io/devops-apiserver/pkg/controller/predicate"
)

var _ = Describe("TriggerPredicate", func() {
	var (
		predicateObj devopspredicate.TriggerPredicate
		triggered    bool
		result       bool
	)

	createTrigger := func(evt event.CreateEvent) bool {
		triggered = true
		return true
	}

	updaterigger := func(evt event.UpdateEvent) bool {
		triggered = true
		return true
	}

	deleteTrigger := func(evt event.DeleteEvent) bool {
		triggered = true
		return true
	}

	genericTrigger := func(evt event.GenericEvent) bool {
		triggered = true
		return true
	}

	ShouldBeTrue := func() {
		Expect(result).To(BeTrue())
	}
	ShouldBeFalse := func() {
		Expect(result).To(BeFalse())
	}

	AllCases := func() {
		Context("has customize trigger", func() {
			It("should be triggered", func() { Expect(triggered).To(BeTrue()) })
			It("should return true", ShouldBeTrue)
		})
	}

	BeforeEach(func() {
		predicateObj = devopspredicate.TriggerPredicate{}
		result = false
	})

	Context("Create", func() {
		var evt event.CreateEvent
		BeforeEach(func() {
			triggered = false
			predicateObj.CreateTrigger = createTrigger
		})
		JustBeforeEach(func() {
			result = predicateObj.Create(evt)
		})
		AllCases()
	})

	Context("Update with New", func() {
		var evt event.UpdateEvent
		BeforeEach(func() {
			triggered = false
			predicateObj.UpdateTrigger = updaterigger
		})
		JustBeforeEach(func() {
			result = predicateObj.Update(evt)
		})
		AllCases()
	})

	Context("Update with Old", func() {
		var evt event.UpdateEvent
		BeforeEach(func() {
			triggered = false
			predicateObj.UpdateTrigger = updaterigger
		})
		JustBeforeEach(func() {
			result = predicateObj.Update(evt)
		})
		AllCases()
	})

	Context("Delete", func() {
		var evt event.DeleteEvent
		BeforeEach(func() {
			triggered = false
			predicateObj.DeleteTrigger = deleteTrigger
		})
		JustBeforeEach(func() {
			result = predicateObj.Delete(evt)
		})
		AllCases()
	})

	Context("Generic", func() {
		var evt event.GenericEvent
		BeforeEach(func() {
			triggered = false
			predicateObj.GenericTrigger = genericTrigger
		})
		JustBeforeEach(func() {
			result = predicateObj.Generic(evt)
		})
		AllCases()
	})

	Context("No customize trigger", func() {
		var evt event.GenericEvent
		BeforeEach(func() {
			triggered = false
		})
		JustBeforeEach(func() {
			result = predicateObj.Generic(evt)
		})
		It("should not be triggered", func() { Expect(triggered).To(BeFalse()) })
		It("should return false", ShouldBeFalse)
	})

})
