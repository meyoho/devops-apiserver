package predicate

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
)

// PhasePredicate checks if the object is in specific phase before queuing
// only should be used if the Resource implements v1alpha1.Phaser interface
// if you specified Phases, will check if the object is contained in Phases
type PhasePredicate struct {
	Phase  string
	Type   string
	Phases []string
}

var _ predicate.Predicate = PhasePredicate{}

// Create checks annotations on create
func (a PhasePredicate) Create(evt event.CreateEvent) bool {
	return a.hasPhase(evt.Meta)
}

// Delete check annotations on delete
func (a PhasePredicate) Delete(evt event.DeleteEvent) bool {
	return a.hasPhase(evt.Meta)
}

// Update check annotations on update
func (a PhasePredicate) Update(evt event.UpdateEvent) bool {
	return a.hasPhase(evt.MetaNew)
}

// Generic checks annotations on generic event
func (a PhasePredicate) Generic(evt event.GenericEvent) bool {
	return a.hasPhase(evt.Meta)
}

func (a PhasePredicate) hasPhase(obj metav1.Object) (result bool) {
	defer func() {
		if obj != nil {
			glog.V(8).Infof("[%s] PhasePredicate Event object %s/%s hasPhase %#v? %v", a.Type, obj.GetNamespace(), obj.GetName(), a, result)
		}
	}()

	if obj == nil || obj.(v1alpha1.Phaser) == nil {
		return
	}

	evtPhase := obj.(v1alpha1.Phaser).GetPhase()

	if len(a.Phases) == 0 {
		result = evtPhase == a.Phase
		return
	}

	for _, p := range a.Phases {
		if p == evtPhase {
			result = true
			return
		}
	}

	return
}
