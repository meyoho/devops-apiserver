package predicate_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/event"

	devopspredicate "alauda.io/devops-apiserver/pkg/controller/predicate"
)

var _ = Describe("AnnotationsPredicate", func() {
	var (
		predicateObj devopspredicate.AnnotationPredicate

		obj    *v1alpha1.Jenkins
		result bool
	)

	ShouldBeTrue := func() {
		Expect(result).To(BeTrue())
	}
	ShouldBeFalse := func() {
		Expect(result).To(BeFalse())
	}

	AllCases := func() {
		Context("has annotation with equal value", func() {
			BeforeEach(func() {
				obj.Annotations = map[string]string{"key": "value"}
			})
			It("should return true", ShouldBeTrue)
		})
		Context("has annotation with different value", func() {
			BeforeEach(func() {
				obj.Annotations = map[string]string{"key": "diff"}
			})
			It("should return false", ShouldBeFalse)
		})
		Context("does not have annotation", func() {
			BeforeEach(func() {
				obj.Annotations = map[string]string{}
			})
			It("should return false", ShouldBeFalse)
		})
	}

	BeforeEach(func() {
		predicateObj = devopspredicate.AnnotationPredicate{"key": "value"}
		obj = testtools.GetJenkins("jenkins", "aa", "", "")
	})

	Context("Create", func() {
		var evt event.CreateEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Create(evt)
		})
		AllCases()
	})

	Context("Update with New", func() {
		var evt event.UpdateEvent
		BeforeEach(func() {
			evt.MetaNew = obj
			evt.ObjectNew = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Update(evt)
		})
		AllCases()
	})

	Context("Update with Old", func() {
		var evt event.UpdateEvent
		BeforeEach(func() {
			evt.MetaOld = obj
			evt.ObjectOld = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Update(evt)
		})
		AllCases()
	})

	Context("Delete", func() {
		var evt event.DeleteEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Delete(evt)
		})
		AllCases()
	})

	Context("Generic", func() {
		var evt event.GenericEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Generic(evt)
		})
		AllCases()
	})
})
