package predicate_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestPredicate(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("predicate.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/controller/predicate", []Reporter{junitReporter})
}
