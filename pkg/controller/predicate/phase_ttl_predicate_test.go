package predicate_test

import (
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/controller/predicate"
	"alauda.io/devops-apiserver/pkg/controller/testtools"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/event"

	devopspredicate "alauda.io/devops-apiserver/pkg/controller/predicate"
)

var _ = Describe("PhaseTTLPredicate", func() {
	var (
		predicateObj *devopspredicate.PhaseTTLPredicate

		obj    *v1alpha1.Jenkins
		result bool
	)

	ShouldBeTrue := func() {
		Expect(result).To(BeTrue())
	}
	ShouldBeFalse := func() {
		Expect(result).To(BeFalse())
	}

	AllCases := func(alwaysBeTrue bool) {
		Context("phase is not ready", func() {
			BeforeEach(func() {
				obj.Status.ServiceStatus.Phase = v1alpha1.ServiceStatusPhaseCreating
			})
			It("should return true", ShouldBeTrue)
		})
		Context("phase ready, lastAttempt is over TTL", func() {
			BeforeEach(func() {
				obj.Status.ServiceStatus.Phase = v1alpha1.ServiceStatusPhaseReady
				// now := metav1.Now()
				expired := time.Now().Add(-time.Minute)
				metatime := metav1.NewTime(expired)
				obj.Status.ServiceStatus.HTTPStatus = &v1alpha1.HostPortStatus{LastAttempt: &metatime}
			})

			It("should return true", ShouldBeTrue)
		})
		Context("phase is Ready and not TTL", func() {
			BeforeEach(func() {
				obj.Status.ServiceStatus.Phase = v1alpha1.ServiceStatusPhaseReady
				metatime := metav1.NewTime(time.Now())
				obj.Status.ServiceStatus.HTTPStatus = &v1alpha1.HostPortStatus{LastAttempt: &metatime}
			})
			if alwaysBeTrue {
				It("should return true", ShouldBeTrue)
			} else {
				It("should return false", ShouldBeFalse)
			}
		})
	}

	BeforeEach(func() {
		predicateObj = &devopspredicate.PhaseTTLPredicate{TTL: time.Second}
		obj = testtools.GetJenkins("jenkins", "aa", "", "")
		obj.Status.ServiceStatus.Phase = v1alpha1.ServiceStatusPhaseCreating
	})

	Context("Create", func() {
		var evt event.CreateEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Create(evt)
		})
		AllCases(false)
	})

	Context("Update with New", func() {
		var evt event.UpdateEvent
		BeforeEach(func() {
			evt.MetaNew = obj
			evt.ObjectNew = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Update(evt)
		})
		AllCases(false)
	})

	Context("Update with Old", func() {
		var evt event.UpdateEvent
		BeforeEach(func() {
			evt.MetaOld = obj
			evt.ObjectOld = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Update(evt)
		})
		AllCases(false)
	})

	Context("Delete", func() {
		var evt event.DeleteEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Delete(evt)
		})
		AllCases(true)
	})

	Context("Generic", func() {
		var evt event.GenericEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = predicateObj.Generic(evt)
		})
		AllCases(false)
	})
})

var _ = Describe("Jitter", func() {
	var (
		duration    time.Duration
		getDuration func() time.Duration
	)
	BeforeEach(func() {
		duration = time.Minute
	})
	JustBeforeEach(func() {
		getDuration = predicate.Jitter(duration)
	})
	It("should return a resonable jitter", func() {
		Expect(getDuration()).ToNot(Equal(duration), fmt.Sprintf("duration %s jitter %s", duration, getDuration()))
		Expect(getDuration()).To(BeNumerically("<", duration*2), "jitter response should not double the duration")
	})

})
