package predicate

import (
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
)

var (
	RoleSyncEnable = AnnotationPredicate{"alauda.io/role.sync": "true"}
)

// AnnotationPredicate checks if the object has a set of key/value annotations
type AnnotationPredicate map[string]string

var _ predicate.Predicate = AnnotationPredicate{}

// Create checks annotations on create
func (a AnnotationPredicate) Create(evt event.CreateEvent) bool {
	return a.hasAnnotations(evt.Meta)
}

// Delete check annotations on delete
func (a AnnotationPredicate) Delete(evt event.DeleteEvent) bool {
	return a.hasAnnotations(evt.Meta)
}

// Update check annotations on update
func (a AnnotationPredicate) Update(evt event.UpdateEvent) bool {
	if evt.MetaNew != nil {
		return a.hasAnnotations(evt.MetaNew)
	}
	if evt.MetaOld != nil {
		return a.hasAnnotations(evt.MetaOld)
	}
	return false
}

// Generic checks annotations on generic event
func (a AnnotationPredicate) Generic(evt event.GenericEvent) bool {
	return a.hasAnnotations(evt.Meta)
}

func (a AnnotationPredicate) init() AnnotationPredicate {
	if a == nil {
		a = make(AnnotationPredicate)
	}
	return a
}

func (a AnnotationPredicate) hasAnnotations(obj v1.Object) bool {
	a = a.init()
	if obj != nil {
		annotations := obj.GetAnnotations()
		for key, value := range a {
			if objValue, ok := annotations[key]; !ok || value != objValue {
				return false
			}
		}
	}
	return true
}
