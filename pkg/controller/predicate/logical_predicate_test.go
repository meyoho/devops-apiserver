package predicate_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/event"

	devopspredicate "alauda.io/devops-apiserver/pkg/controller/predicate"
)

var _ = Describe("LogicalPredicate", func() {
	var (
		phasePredicate    devopspredicate.PhasePredicate
		rolsSyncPredicate devopspredicate.AnnotationPredicate
		logicPredicate    devopspredicate.LogicalPredicate

		obj    *v1alpha1.PipelineConfig
		result bool
	)

	ShouldBeTrue := func() {
		Expect(result).To(BeTrue())
	}
	ShouldBeFalse := func() {
		Expect(result).To(BeFalse())
	}

	AllCases := func() {
		Context("true && true", func() {
			BeforeEach(func() {
				obj.Status.Phase = v1alpha1.PipelineConfigPhaseCreating
				obj.Annotations["alauda.io/role.sync"] = "true"
			})
			It("should return true", ShouldBeTrue)
		})

		Context("true && false", func() {
			BeforeEach(func() {
				obj.Status.Phase = v1alpha1.PipelineConfigPhaseCreating
			})
			It("should return true", ShouldBeFalse)
		})

		Context("false && false", func() {
			BeforeEach(func() {
				obj.Status.Phase = v1alpha1.PipelineConfigPhaseSyncing
			})
			It("should return false", ShouldBeFalse)
		})

		Context("false && true", func() {
			BeforeEach(func() {
				obj.Status.Phase = v1alpha1.PipelineConfigPhaseSyncing
				obj.Annotations["alauda.io/role.sync"] = "true"
			})
			It("should return false", ShouldBeFalse)
		})
	}

	BeforeEach(func() {
		phasePredicate = devopspredicate.PhasePredicate{Phase: string(v1alpha1.PipelineConfigPhaseCreating)}
		obj = &v1alpha1.PipelineConfig{
			ObjectMeta: metav1.ObjectMeta{Annotations: map[string]string{}},
			Status:     v1alpha1.PipelineConfigStatus{Phase: v1alpha1.PipelineConfigPhaseCreating}}
		rolsSyncPredicate = devopspredicate.RoleSyncEnable

		logicPredicate = devopspredicate.And(phasePredicate, rolsSyncPredicate).(devopspredicate.LogicalPredicate)
	})

	Context("Create", func() {
		var evt event.CreateEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = logicPredicate.Create(evt)
		})
		AllCases()
	})

	Context("Update with New", func() {
		var evt event.UpdateEvent
		BeforeEach(func() {
			evt.MetaNew = obj
			evt.ObjectNew = obj
		})
		JustBeforeEach(func() {
			result = logicPredicate.Update(evt)
		})
		AllCases()
	})

	Context("Update with Old", func() {
		var evt event.UpdateEvent
		BeforeEach(func() {
			evt.MetaOld = obj
			evt.ObjectOld = obj
		})
		JustBeforeEach(func() {
			result = logicPredicate.Update(evt)
		})

		Context("true && false", func() {
			BeforeEach(func() {
				obj.Status.Phase = v1alpha1.PipelineConfigPhaseCreating
			})
			It("should return true", ShouldBeFalse)
		})

		Context("false && false", func() {
			BeforeEach(func() {
				obj.Status.Phase = v1alpha1.PipelineConfigPhaseSyncing
			})
			It("should return false", ShouldBeFalse)
		})

		Context("false && true", func() {
			BeforeEach(func() {
				obj.Status.Phase = v1alpha1.PipelineConfigPhaseSyncing
				obj.Annotations["alauda.io/role.sync"] = "true"
			})
			It("should return false", ShouldBeFalse)
		})
	})

	Context("Delete", func() {
		var evt event.DeleteEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = logicPredicate.Delete(evt)
		})
		AllCases()
	})

	Context("Generic", func() {
		var evt event.GenericEvent
		BeforeEach(func() {
			evt.Meta = obj
			evt.Object = obj
		})
		JustBeforeEach(func() {
			result = logicPredicate.Generic(evt)
		})
		AllCases()
	})
})
