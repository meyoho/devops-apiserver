package predicate

import (
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
)

// CreateFuncTrigger trigger when create
type CreateFuncTrigger func(evt event.CreateEvent) bool

// UpdateFuncTrigger trigger when update
type UpdateFuncTrigger func(evt event.UpdateEvent) bool

// DeleteFuncTrigger trigger when delete
type DeleteFuncTrigger func(evt event.DeleteEvent) bool

// GenericFuncTrigger trigger when generic
type GenericFuncTrigger func(evt event.GenericEvent) bool

// TriggerPredicate just for trigger some functions
// return false default.
type TriggerPredicate struct {
	CreateTrigger  CreateFuncTrigger
	UpdateTrigger  UpdateFuncTrigger
	DeleteTrigger  DeleteFuncTrigger
	GenericTrigger GenericFuncTrigger
}

var _ predicate.Predicate = &TriggerPredicate{}

// Create implements Predicate interface
func (tp *TriggerPredicate) Create(evt event.CreateEvent) bool {
	if tp.CreateTrigger != nil {
		return tp.CreateTrigger(evt)
	}
	return false
}

// Update implements Predicate interface
func (tp *TriggerPredicate) Update(evt event.UpdateEvent) bool {
	if tp.UpdateTrigger != nil {
		return tp.UpdateTrigger(evt)
	}
	return false
}

// Delete implements Predicate interface
func (tp *TriggerPredicate) Delete(evt event.DeleteEvent) bool {
	if tp.DeleteTrigger != nil {
		return tp.DeleteTrigger(evt)
	}
	return false
}

// Generic implements Predicate interface
func (tp *TriggerPredicate) Generic(evt event.GenericEvent) bool {
	if tp.GenericTrigger != nil {
		return tp.GenericTrigger(evt)
	}
	return false
}
