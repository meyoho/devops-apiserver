package manager

import (
	sigsctrl "alauda.io/devops-apiserver/pkg/mock/sigs.k8s.io/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/controller"
)

var _ = Describe("controllerManager.NewController", func() {

	var (
		name            string
		options         controller.Options
		internalManager *controllerManager
		sigsController  controller.Controller
		managerMock     *sigsctrl.MockManager
		reconcilerMock  *sigsctrl.MockReconciler
		err             error
		mockCtrl        *gomock.Controller
	)

	// We can prepare some stuff
	BeforeEach(func() {
		// starts mock controller
		mockCtrl = gomock.NewController(GinkgoT())
		// constructs mocks using mock controller
		managerMock = sigsctrl.NewMockManager(mockCtrl)
		reconcilerMock = sigsctrl.NewMockReconciler(mockCtrl)

		// specify some name
		name = "some-controller"

		internalManager = &controllerManager{Manager: managerMock, MaxReconciles: 5}
		options = controller.Options{MaxConcurrentReconciles: 0, Reconciler: reconcilerMock}
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("Should return an instance of a controller", func() {
		// Adding our expections here for mocks
		// injects fields in the reconciler
		// we are not interested in what the constructor controller.New actually does
		// so we just mock everything with nil
		managerMock.EXPECT().SetFields(options.Reconciler).Return(nil).AnyTimes()
		managerMock.EXPECT().Add(gomock.Any()).Return(nil).AnyTimes()
		managerMock.EXPECT().GetCache().Return(nil)
		managerMock.EXPECT().GetConfig().Return(nil)
		managerMock.EXPECT().GetScheme().Return(nil)
		managerMock.EXPECT().GetClient().Return(nil)
		managerMock.EXPECT().GetRecorder(name).Return(nil)

		// this call should be here because we need to mark our expectations before running
		sigsController, err = internalManager.NewController(name, options)

		Expect(err).To(BeNil())
		Expect(sigsController).ToNot(BeNil())
	})

})
