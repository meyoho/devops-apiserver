package manager

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"

	"net/http"
	"time"

	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	clientsetversioned "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	externalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	authclientset "bitbucket.org/mathildetech/auth-controller2/pkg/client/clientset/versioned"
	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	clusterregistryclientset "k8s.io/cluster-registry/pkg/client/clientset/versioned"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/manager"
)

// Manager manager interface for controller manager
type Manager interface {
	manager.Manager
	GetDevOpsClient() clientset.Interface
	GetKubeClient() kubernetes.Interface
	GetThirdParty() externalthirdparty.Interface
	GetMaxReconciles() int
	NewController(name string, opts controller.Options) (controller.Controller, error)
	GetExtraConfig() ExtraConfig
	GetAnnotationProvider() devops.AnnotationProvider
}

// Options are the arguments for creating a new Manager
type Options struct {
	// Scheme is the scheme used to resolve runtime.Objects to GroupVersionKinds / Resources
	// Defaults to the kubernetes/client-go scheme.Scheme
	Scheme *runtime.Scheme

	// MapperProvider provides the rest mapper used to map go types to Kubernetes APIs
	MapperProvider func(c *rest.Config) (meta.RESTMapper, error)

	// SyncPeriod determines the minimum frequency at which watched resources are
	// reconciled. A lower period will correct entropy more quickly, but reduce
	// responsiveness to change if there are many watched resources. Change this
	// value only if you know what you are doing. Defaults to 10 hours if unset.
	SyncPeriod *time.Duration

	// LeaderElection determines whether or not to use leader election when
	// starting the manager.
	LeaderElection bool

	// LeaderElectionNamespace determines the namespace in which the leader
	// election configmap will be created.
	LeaderElectionNamespace string

	// LeaderElectionID determines the name of the configmap that leader election
	// will use for holding the leader lock.
	LeaderElectionID string

	// Namespace if specified restricts the manager's cache to watch objects in the desired namespace
	// Defaults to all namespaces
	// Note: If a namespace is specified then controllers can still Watch for a cluster-scoped resource e.g Node
	// For namespaced resources the cache will only hold objects from the desired namespace.
	Namespace string

	// MetricsBindAddress is the TCP address that the controller should bind to
	// for serving prometheus metrics
	MetricsBindAddress string

	// MaxConcurrentReconciles is the maximum number of concurrent Reconciles which can be run. Defaults to 1.
	// Each controller can use this general value or specify its own
	MaxConcurrentReconciles int

	// RoundTripper common round tripper transport for third party clients
	RoundTripper http.RoundTripper

	// It is the endpoint of the multi cluster proxy , eg. alauda erebus
	MultiClusterHost string

	// manager constructor function. Used for mocking tests. Will be populated on the fly
	newManagerFunc func(config *rest.Config, options manager.Options) (manager.Manager, error)
	// kubernetes client constructor function. Used for mocking tests. Will be populated on the fly
	newKubernetesClientFunc func(config *rest.Config) (kubernetes.Interface, error)
	// devops client constructor function. Used for mocking tests. Will be populated on the fly
	newDevOpsClientFunc func(config *rest.Config) (clientsetversioned.Interface, error)
	// thirdparty client constructor function. Used for mocking tests. Will be populated on the fly
	newThirdPartyClientFunc func(roundTripper http.RoundTripper, client kubernetes.Interface, config *rest.Config, multiClusterHost string, namespace string, provider devops.AnnotationProvider) (externalthirdparty.Interface, error)
	// auth client constructor function. Used for mocking tests. Will be populated on the fly
	newAuthClientFunc func(config *rest.Config) (authclientset.Interface, error)
	// newClusterRegistryClientFunc is clusterregistryclient constructor function. Used for mocking tests. Will be populated on the fly
	newClusterRegistryClientFunc func(config *rest.Config) (clusterregistryclientset.Interface, error)
}

// ExtraConfig extra configuration for the server
type ExtraConfig struct {
	// Place you custom config here.

	SystemNamespace      string
	CredentialsNamespace string
	AnnotationProvider   devops.AnnotationProvider
}

// GetManagerOptions returns a converted manager.Options
// TODO: Check periodically if the manager.Options have changes
func (opts Options) GetManagerOptions() manager.Options {
	return manager.Options{
		Scheme:                  opts.Scheme,
		MapperProvider:          opts.MapperProvider,
		SyncPeriod:              opts.SyncPeriod,
		LeaderElection:          opts.LeaderElection,
		LeaderElectionNamespace: opts.LeaderElectionNamespace,
		LeaderElectionID:        opts.LeaderElectionID,
		Namespace:               opts.Namespace,
		MetricsBindAddress:      opts.MetricsBindAddress,
	}
}

// New builds an controller manager
func New(config *rest.Config, options Options, extra ExtraConfig) (Manager, error) {
	var err error
	// settings defaults for options
	options = setOptionsDefaults(options)

	controllerManager := &controllerManager{MaxReconciles: options.MaxConcurrentReconciles}
	// starts internal manager
	if controllerManager.Manager, err = options.newManagerFunc(config, options.GetManagerOptions()); err != nil {
		return nil, err
	}
	// constructs Kubernetes client
	if controllerManager.KubernetesClient, err = options.newKubernetesClientFunc(config); err != nil {
		return nil, err
	}
	// constructs DevOps client
	if controllerManager.DevopsClient, err = options.newDevOpsClientFunc(config); err != nil {
		return nil, err
	}

	// constructs ThirdParty client
	if controllerManager.ThirdPartyClient, err = options.newThirdPartyClientFunc(options.RoundTripper, controllerManager.KubernetesClient, config, options.MultiClusterHost, extra.SystemNamespace, extra.AnnotationProvider); err != nil {
		return nil, err
	}
	controllerManager.ExtraConfig = extra
	return controllerManager, nil
}

// setOptionsDefaults set default values for Options fields
func setOptionsDefaults(options Options) Options {
	// Set General reconciles for controllers. Each controller can use this general value
	// or specify its own
	if options.MaxConcurrentReconciles <= 0 {
		options.MaxConcurrentReconciles = 1
	}
	// set a default round tripper if not set
	if options.RoundTripper == nil {
		options.RoundTripper = http.DefaultTransport
	}
	// sets a constructor function for manager if nil
	if options.newManagerFunc == nil {
		options.newManagerFunc = manager.New
	}
	if options.newKubernetesClientFunc == nil {
		options.newKubernetesClientFunc = func(config *rest.Config) (kubernetes.Interface, error) {
			return kubernetes.NewForConfig(config)
		}
	}
	if options.newDevOpsClientFunc == nil {
		options.newDevOpsClientFunc = func(config *rest.Config) (clientsetversioned.Interface, error) {
			return clientsetversioned.NewForConfig(config)
		}
	}
	if options.newAuthClientFunc == nil {
		options.newAuthClientFunc = func(config *rest.Config) (authclientset.Interface, error) {
			return authclientset.NewForConfig(config)
		}
	}
	if options.newClusterRegistryClientFunc == nil {
		options.newClusterRegistryClientFunc = func(config *rest.Config) (clusterregistryclientset.Interface, error) {
			return clusterregistryclientset.NewForConfig(config)
		}
	}

	if options.newThirdPartyClientFunc == nil {
		options.newThirdPartyClientFunc = externalthirdparty.New
	}
	return options
}
