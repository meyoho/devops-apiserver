package manager

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	externalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"k8s.io/client-go/kubernetes"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/manager"
)

// controllerManager internal type for controller-manager
type controllerManager struct {
	manager.Manager
	DevopsClient     clientset.Interface
	KubernetesClient kubernetes.Interface
	ThirdPartyClient externalthirdparty.Interface
	MaxReconciles    int
	ExtraConfig      ExtraConfig
}

// GetDevOpsClient returns a devops client as clientset.Interface
func (mgr *controllerManager) GetDevOpsClient() clientset.Interface {
	return mgr.DevopsClient
}

// GetKubeClient returns a kubernetes client as kubernetes.Interface
func (mgr *controllerManager) GetKubeClient() kubernetes.Interface {
	return mgr.KubernetesClient
}

// GetThirdParty returns a third party client as thirdparty.Interface
func (mgr *controllerManager) GetThirdParty() externalthirdparty.Interface {
	return mgr.ThirdPartyClient
}

// GetMaxReconciles get the number of max reconciles
func (mgr *controllerManager) GetMaxReconciles() int {
	return mgr.MaxReconciles
}

func (mgr *controllerManager) GetAnnotationProvider() devops.AnnotationProvider {
	return mgr.ExtraConfig.AnnotationProvider
}

// NewController constructs a new controller
func (mgr *controllerManager) NewController(name string, opts controller.Options) (controller.Controller, error) {
	// injects defaults to options if not set
	if opts.MaxConcurrentReconciles <= 0 {
		opts.MaxConcurrentReconciles = mgr.MaxReconciles
	}
	return controller.New(name, mgr.Manager, opts)
}

func (mgr *controllerManager) GetExtraConfig() ExtraConfig {
	return mgr.ExtraConfig
}
