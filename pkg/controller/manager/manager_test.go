package manager

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"fmt"
	"net/http"
	"testing"

	clientsetversioned "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	clientsetfake "alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	externalthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	sigsctrl "alauda.io/devops-apiserver/pkg/mock/sigs.k8s.io/controller"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"k8s.io/client-go/kubernetes"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	rest "k8s.io/client-go/rest"
	sigsmanager "sigs.k8s.io/controller-runtime/pkg/manager"
)

func TestControllerManager(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "pkg/controller/manager")
}

var _ = Describe("New", func() {

	var (
		config               *rest.Config
		options              Options
		mgr                  Manager
		err                  error
		mockCtrl             *gomock.Controller
		systemNamespace      = "alauda-system"
		credentialsNamespace = "global-credentials"
		extraConfig          = ExtraConfig{
			SystemNamespace:      systemNamespace,
			CredentialsNamespace: credentialsNamespace,
		}
	)

	// We can prepare some stuff
	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		config = &rest.Config{}
		options = Options{
			MaxConcurrentReconciles: 100,
			newManagerFunc: func(config *rest.Config, options sigsmanager.Options) (sigsmanager.Manager, error) {
				return sigsctrl.NewMockManager(mockCtrl), nil
			},
			newKubernetesClientFunc: func(config *rest.Config) (kubernetes.Interface, error) {
				return k8sfake.NewSimpleClientset(), nil
			},
			newDevOpsClientFunc: func(config *rest.Config) (clientsetversioned.Interface, error) {
				return clientsetfake.NewSimpleClientset(), nil
			},
		}
	})

	// We verify our mockCtrl and finish
	AfterEach(func() {
		mockCtrl.Finish()
	})

	// here we really run our functions
	JustBeforeEach(func() {
		mgr, err = New(config, options, extraConfig)
	})

	Context("Constructs correctly", func() {
		It("Should return ", func() {
			Expect(err).To(BeNil())
			Expect(mgr).ToNot(BeNil())
			Expect(mgr.GetMaxReconciles()).To(Equal(100))
			Expect(mgr.GetDevOpsClient()).ToNot(BeNil())
			Expect(mgr.GetKubeClient()).ToNot(BeNil())
			Expect(mgr.GetThirdParty()).ToNot(BeNil())
			Expect(mgr.GetExtraConfig()).ToNot(BeNil())
		})
	})

	Context("Manager constructor returns error", func() {
		// replace manager constructor
		BeforeEach(func() {
			options.newManagerFunc = func(config *rest.Config, options sigsmanager.Options) (sigsmanager.Manager, error) {
				return nil, fmt.Errorf("manager new error")
			}
		})
		It("Should return error", func() {
			Expect(err).To(Equal(fmt.Errorf("manager new error")))
			Expect(mgr).To(BeNil())
		})
	})

	Context("Kubernetes constructor returns error", func() {
		// replace kubernetes constructor
		BeforeEach(func() {
			options.newKubernetesClientFunc = func(config *rest.Config) (kubernetes.Interface, error) {
				return nil, fmt.Errorf("k8s new error")
			}
		})
		It("Should return error", func() {
			Expect(err).To(Equal(fmt.Errorf("k8s new error")))
			Expect(mgr).To(BeNil())
		})
	})

	Context("DevOps constructor returns error", func() {
		// replace devops constructor
		BeforeEach(func() {
			options.newDevOpsClientFunc = func(config *rest.Config) (clientsetversioned.Interface, error) {
				return nil, fmt.Errorf("devops new error")
			}
		})
		It("Should return error", func() {
			Expect(err).To(Equal(fmt.Errorf("devops new error")))
			Expect(mgr).To(BeNil())
		})
	})

	Context("ThirdParty constructor returns error", func() {
		// replace devops constructor
		BeforeEach(func() {

			options.newThirdPartyClientFunc = func(http.RoundTripper, kubernetes.Interface, *rest.Config, string, string, devops.AnnotationProvider) (externalthirdparty.Interface, error) {
				return nil, fmt.Errorf("thirdparty new error")
			}
		})
		It("Should return error", func() {
			Expect(err).To(Equal(fmt.Errorf("thirdparty new error")))
			Expect(mgr).To(BeNil())
		})
	})

	Context("All constructors are nil", func() {
		// replace manager constructor
		BeforeEach(func() {
			options.MaxConcurrentReconciles = 0
			options.newManagerFunc = nil
			options.newKubernetesClientFunc = nil
			options.newDevOpsClientFunc = nil
			config = &rest.Config{
				Host: "333333",
			}
		})
		It("Should return error because it cannot connect", func() {
			Expect(err).ToNot(BeNil())
			Expect(mgr).To(BeNil())
		})
	})

})
