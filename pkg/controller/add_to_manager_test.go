package controller_test

import (
	"fmt"
	"testing"

	"alauda.io/devops-apiserver/pkg/controller"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestController(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "pkg/controller")
}

var _ = Describe("AddToManagerFuncs", func() {

	var (
		functions []func(manager.Manager) error
		mgr       manager.Manager
		err       error
	)

	// We can prepare some stuff
	BeforeEach(func() {
		functions = append(functions, func(mngr manager.Manager) error {
			return nil
		})
	})

	// here we really run our functions
	JustBeforeEach(func() {
		controller.AddToManagerFuncs = functions
		err = controller.AddToManager(mgr)
	})

	Context("Function runs correctly", func() {
		It("Should not an error", func() {
			Expect(err).To(BeNil())
		})
	})

	Context("Function returns error", func() {
		BeforeEach(func() {
			// add a bad function
			functions = append(functions, func(mngr manager.Manager) error {
				return fmt.Errorf("Some error")
			})
		})

		It("Should return an error", func() {
			Expect(err).ToNot(BeNil())
		})
	})
})
