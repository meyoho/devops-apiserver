package role

import glog "k8s.io/klog"

// CustomCalculator calculator for custom role permissions.
// It does not compare with target role system and only gives the final role mapping
type CustomCalculator struct {
	BaseCalculator
}

var _ Calculator = CustomCalculator{}

// NewCustomCalculator standard constructor for CustomCalculator
func NewCustomCalculator(base BaseCalculator) CustomCalculator {
	base.LogName = "CustomCalculator"
	calculator := CustomCalculator{BaseCalculator: base}
	calculator.BaseCalculator.ProcessProject = calculator.ProcessProject
	return calculator
}

// ProcessProject processes a project using Mutex calculation
func (c CustomCalculator) ProcessProject(project *ProjectUserAssigmentOperation, target *ProjectUserRoleAssignment, currentUsers []*UserAssignment, usersIndex map[string]string) {
	targetUserRoles := target.GetUserRoles()
	var (
		// processedUsers = map[string]bool{}
		roleForUser      = map[string]*UserAssignment{}
		current          *UserAssignment
		orderedUsernames []*UsernameUserAssignment
		username         string
	)
	// filter out the highest priority among all assigned roles
	roleForUser = c.SelectUserRoleByPriority(target.Project.Name, currentUsers)

	// order by username
	orderedUsernames = c.GetOrdered(roleForUser)
	// after selecting out the target role it adds each one as and edit element
	// because for custom permission system is almost impossible to convert from the target's system permission scheme
	for _, user := range orderedUsernames {
		username = user.Username
		current = user.UserAssignment
		targetAssignments := targetUserRoles.FindUserAssignments(current)
		var addUser = c.AttachTargetUserInfo(*current, targetAssignments)
		glog.V(9).Infof("[%s] Setting project's \"%s\" user \"%s\" to role \"%s\"", c.LogName, target.Project.Name, username, current.GetRole())
		project.AddUserRoleOperation(&addUser, OperationTypeUpdate)
	}
	c.CalculateRemoval(project, targetUserRoles, usersIndex, roleForUser)
}
