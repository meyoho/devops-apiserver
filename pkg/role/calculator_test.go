package role_test

import (
	"alauda.io/devops-apiserver/pkg/role"
)

func getBase() role.BaseCalculator {
	return role.BaseCalculator{
		Source: &role.Platform{Name: "acp", Sync: role.SyncTypeRBAC, Enabled: true, RoleMapping: map[string]string{
			"project_admin":   "alauda_project_admin",
			"project_auditor": "alauda_project_auditor",
			"developer":       "alauda_developer",
		}},
		Target: &role.Platform{Name: "jira", Sync: role.SyncTypeRBAC, Enabled: true, RoleMapping: map[string]string{
			"project_admin":   "jira_project_admin",
			"project_auditor": "jira_project_admin",
			"developer":       "developer",
		}},
		Priorities: []role.Priority{
			role.Priority{Name: "project_admin", Priority: 1},
			role.Priority{Name: "project_auditor", Priority: 3},
			role.Priority{Name: "developer", Priority: 2},
		},
	}
}

func getBasicScenario() (base role.BaseCalculator, source role.ProjectUserAssignments, current role.ProjectUserAssignments) {
	base = getBase()
	// current assignment from source
	source = []*role.ProjectUserRoleAssignment{
		role.NewProject("devops").
			AddUserRole(
				role.NewUserAssignment("dev", "dev@email", "developer", ""),
				role.NewUserAssignment("dev", "dev@email", "project_auditor", ""),
				role.NewUserAssignment("admin", "admin@email", "project_admin", ""),
			),
		role.NewProject("test").
			AddUserRole(
				role.NewUserAssignment("test", "test@email", "developer", ""),
				role.NewUserAssignment("testadmin", "testadmin@email", "project_auditor", ""),
			),
		// an empty source to remove the whole user list
		role.NewProject("empty"),
	}

	// current assignment from target
	current = []*role.ProjectUserRoleAssignment{
		role.NewProject("devops").
			AddUserRole(
				// should remove test and add dev
				role.NewUserAssignment("test", "test@email", "developer", ""),
				// should not change admin
				role.NewUserAssignment("admin", "admin@email", "project_admin", ""),
			),
		role.NewProject("test").
			AddUserRole(
				// should ignore this guy
				role.NewUserAssignment("notfound", "notfound@email", "developer", ""),
				// should change to project_auditor
				role.NewUserAssignment("testadmin", "testadmin@email", "project_admin", ""),
			),
		// should remove all the users from this project
		role.NewProject("empty").
			AddUserRole(
				role.NewUserAssignment("dev", "dev@email", "developer", ""),
				role.NewUserAssignment("test", "test@email", "developer", ""),
				role.NewUserAssignment("admin", "admin@email", "project_admin", ""),
			),

		// this project should be ignored
		role.NewProject("non-existingproject"),
	}
	return
}
