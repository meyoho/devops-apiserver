package role_test

import (
	"alauda.io/devops-apiserver/pkg/role"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("RBACMutexCalculator.Calculate", func() {
	var (
		base role.BaseCalculator

		source  role.ProjectUserAssignments
		current role.ProjectUserAssignments
		result  []*role.ProjectUserAssigmentOperation

		calculator role.RBACMutexCalculator
	)

	BeforeEach(func() {
		base, source, current = getBasicScenario()

		calculator = role.NewRBACMutexCalculator(base)
	})

	JustBeforeEach(func() {
		result = calculator.Calculate(source, current)
	})
	It("should give two products", func() {
		Expect(result).ToNot(BeNil())
		Expect(result).ToNot(BeEmpty())
		/*
			devops:
			- test(developer): remove
			- dev(developer): add
		*/
		Expect(result).To(ContainElement(
			role.NewProjectUserAssigmentOperation(role.NewProject("devops").Project).
				AddUserRoleOperation(role.NewUserAssignment("dev", "dev@email", "developer", ""), role.OperationTypeAdd).
				AddUserRoleOperation(role.NewUserAssignment("test", "test@email", "developer", ""), role.OperationTypeRemove),
		))

		/*
			test:
			*** ignore: notfound
			test(developer): add
			testadmin(project_auditor): edit
		*/
		Expect(result).To(ContainElement(
			role.NewProjectUserAssigmentOperation(role.NewProject("test").Project).
				AddUserRoleOperation(role.NewUserAssignment("test", "test@email", "developer", ""), role.OperationTypeAdd).
				AddUserRoleOperation(role.NewUserAssignment("testadmin", "testadmin@email", "project_auditor", ""), role.OperationTypeUpdate),
		))

		/*
			empty:
			dev(developer): remove
			test(developer): remove
			admin(project_admin): remove
		*/
		Expect(result).To(ContainElement(
			role.NewProjectUserAssigmentOperation(role.NewProject("empty").Project).
				AddUserRoleOperation(role.NewUserAssignment("dev", "dev@email", "developer", ""), role.OperationTypeRemove).
				AddUserRoleOperation(role.NewUserAssignment("test", "test@email", "developer", ""), role.OperationTypeRemove).
				AddUserRoleOperation(role.NewUserAssignment("admin", "admin@email", "project_admin", ""), role.OperationTypeRemove),
		))

	})
})
