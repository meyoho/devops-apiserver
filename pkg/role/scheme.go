package role

import (
	"encoding/json"
	"fmt"
	"sync"
)

// Scheme is the main role mapping structure
// used to map the source platform and its roles
// and the target platforms with its relative roles.
type Scheme struct {
	// Source defines which platform in the Platforms slice will be used as source
	Source string `json:"source"`
	// Platforms is a slice of difference platforms to and its role mapping
	// syncing type, etc.
	Platforms []*Platform `json:"platforms"`
	// Priorities is the base role map to be used as standard comparison
	// and priority map
	Priorities []Priority `json:"roles"`

	// ProcessorFunc constructor for Calculator
	ProcessorFunc ProcessorConstructorFunc `json:"-"`

	initOnce sync.Once
}

// NewScheme constructor for Scheme
func NewScheme() *Scheme {
	s := &Scheme{}
	return s.init()
}

// NewFromString starts a scheme from string
func NewFromString(content string) (s *Scheme, err error) {
	s = &Scheme{}
	err = json.Unmarshal([]byte(content), s)
	s.init()
	return
}

func (s *Scheme) init() *Scheme {
	s.initOnce.Do(func() {
		if s.Platforms == nil {
			s.Platforms = []*Platform{}
		}
		if s.Priorities == nil {
			s.Priorities = []Priority{}
		}
		if s.Source == "" {
			s.Source = "acp"
		}
		if s.ProcessorFunc == nil {
			s.ProcessorFunc = NewStandardProcessor
		}
	})
	return s
}

// SetSource sets a given source for the scheme
func (s *Scheme) SetSource(source string) *Scheme {
	s.Source = source
	return s
}

// AddPlatform adds a new platform to the scheme
func (s *Scheme) AddPlatform(platform ...*Platform) *Scheme {
	s = s.init()
	s.Platforms = append(s.Platforms, platform...)
	return s
}

// AddPriority adds a Priority to the scheme
func (s *Scheme) AddPriority(priority ...Priority) *Scheme {
	s = s.init()
	s.Priorities = append(s.Priorities, priority...)
	return s
}

// AddMissingPriorities add only missing priorities
func (s *Scheme) AddMissingPriorities(priorities ...Priority) *Scheme {
	if priorities != nil {
		for _, p := range priorities {
			found := false
			for _, selfPriority := range s.Priorities {
				if selfPriority.Name == p.Name {
					found = true
					break
				}
			}
			if !found {
				s.AddPriority(p)
			}
		}
	}
	return s
}

// AddMissingPlatforms add only missing platforms
func (s *Scheme) AddMissingPlatforms(platforms ...*Platform) *Scheme {
	if platforms != nil {
		for _, p := range platforms {
			found := false
			for _, selfPlatform := range s.Platforms {
				if selfPlatform.Name == p.Name {
					found = true
					break
				}
			}
			if !found {
				s.AddPlatform(p)
			}
		}
	}
	return s
}

// Init creates a new processing Processor for role operations
func (s *Scheme) Init(source, target string) (Processor, error) {
	return s.InitCustom(source, target, s.Priorities, s.ProcessorFunc)
}

func (s *Scheme) Platform(name string) *Platform {
	for _, platform := range s.Platforms {
		if platform.Name == name {
			return platform
		}
	}

	return nil
}

// InitCustom creates a custom processor form used for custom worflows or prole syncing
func (s *Scheme) InitCustom(source, target string, priorities []Priority, constructor ProcessorConstructorFunc) (processor Processor, err error) {
	if source == "" {
		source = s.Source
	}
	var sourcePlatform, targetPlatform = s.Platform(source), s.Platform(target)
	if sourcePlatform == nil || !sourcePlatform.Enabled {
		err = fmt.Errorf("source platform \"%s\" was not found or is not enabled", source)
		return
	}
	if targetPlatform == nil || !targetPlatform.Enabled {
		err = fmt.Errorf("target platform \"%s\" was not found or is not enabled", target)
		return
	}
	processor = constructor(sourcePlatform, targetPlatform, priorities)
	return
}

// Merge merges missing data from target
func (s *Scheme) Merge(target *Scheme) *Scheme {
	if target != nil {
		s.AddMissingPriorities(target.Priorities...)
		s.AddMissingPlatforms(target.Platforms...)
	}
	return s
}

// ProcessorConstructorFunc calculator constructor function type
type ProcessorConstructorFunc func(source, target *Platform, priorities []Priority) Processor

/*
{
	source: acp
	priority:
	- name: project_owner
	  priority: 0
	- name: project_admin
	  priority: 1
	platforms:
	- name: acp
	  enabled: true
	  roles:
			project_owner: alauda_project_owner
			project_admin": aladua_project_admin
	- name: ace
	  roles:
		project_owner: project_owner
		project_admin: project_admin
	- name: jenkins
	  custom:
		project_owner:
		   admin: true
		   create_job: true
		   delete_job: true

}
*/

// sync=rbac,roles=some:value;some:value
// type=custom,roles=some[some:value;some:value]other[other:value;other2:value2]

// some[some:value;some:value
// other[other:value;other2:value2
//
