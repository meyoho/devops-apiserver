package role_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/role"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestRole(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("role.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/role", []Reporter{junitReporter})
}

var _ = Describe("NewScheme", func() {
	var (
		scheme *role.Scheme
	)

	JustBeforeEach(func() {
		scheme = role.NewScheme()
	})
	It("should have init all the fields", func() {
		Expect(scheme).ToNot(BeNil())
		Expect(scheme.Priorities).ToNot(BeNil())
		Expect(scheme.Platforms).ToNot(BeNil())
		Expect(scheme.Source).ToNot(BeEmpty())
		Expect(scheme.ProcessorFunc).ToNot(BeNil())
	})
})

var _ = Describe("Scheme.Init", func() {
	var (
		source string
		target string
		scheme *role.Scheme

		calculator role.Processor
		err        error
	)

	BeforeEach(func() {
		scheme = role.NewScheme()
		scheme.Platforms = append(scheme.Platforms, createPlatform(
			"source", role.SyncTypeRBAC, true, map[string]string{
				"role1": "source_role_1",
			},
		))

		scheme.Platforms = append(scheme.Platforms, createPlatform(
			"target", role.SyncTypeRBAC, true, map[string]string{
				"role1": "target_role_1",
			},
		))

		source = "source"
		target = "target"
	})

	JustBeforeEach(func() {
		calculator, err = scheme.Init(source, target)
	})

	It("should create a working calculator", func() {
		Expect(calculator).ToNot(BeNil(), "calculator should be created")
		Expect(err).To(BeNil(), "should not error creating calculator")
	})

	Context("source and target are the same", func() {
		BeforeEach(func() {
			target = source
		})
		It("should not return error because it is the same", func() {
			Expect(calculator).ToNot(BeNil(), "calculator should be created")
			Expect(err).To(BeNil(), "should not error creating calculator")
		})
	})

	Context("source not found", func() {
		BeforeEach(func() {
			source = ""
		})
		It("should return error because source is not in scheme", func() {
			Expect(calculator).To(BeNil(), "calculator should not be created")
			Expect(err).ToNot(BeNil(), "should error creating calculator")
		})
	})
	Context("target not found", func() {
		BeforeEach(func() {
			target = "nonexisting"
		})
		It("should return error because target is not in scheme", func() {
			Expect(calculator).To(BeNil(), "calculator should not be created")
			Expect(err).ToNot(BeNil(), "should error creating calculator")
		})
	})
})

var _ = Describe("Scheme.Merge", func() {
	var (
		original *role.Scheme
		target   *role.Scheme
	)

	BeforeEach(func() {
		original = role.NewScheme().
			SetSource("ace").
			AddPriority(
				role.Priority{
					Name:     "role1",
					Priority: 1,
				},
				role.Priority{
					Name:     "role2",
					Priority: 2,
				},
				role.Priority{
					Name:     "role3",
					Priority: 3,
				},
			).
			AddPlatform(
				createPlatform("platform1", role.SyncTypeRBAC, false, map[string]string{"role1": "role1", "role2": "role2", "role3": "role3"}),
				createPlatform("platform2", role.SyncTypeRBACMutualExclusive, true, map[string]string{"role1": "role1", "role2": "role2", "role3": "role3"}),
			)
		target = role.NewScheme().
			SetSource("acp").
			AddPriority(
				// should add this role4
				role.Priority{
					Name:     "role4",
					Priority: 4,
				},
				// should not update this priority
				role.Priority{
					Name:     "role2",
					Priority: 200,
				},
			).
			AddPlatform(
				// shouild ignore platform1
				createPlatform("platform1", role.SyncTypeRBAC, true, map[string]string{"role1": "role1", "role2": "role2", "role3": "role3"}),
				// should add platform3
				createPlatform("platform3", role.SyncTypeRBAC, true, map[string]string{"role1": "role1", "role2": "role2", "role3": "role3"}),
			)
	})

	JustBeforeEach(func() {
		original = original.Merge(target)
	})

	It("should add role4 and platform3", func() {
		Expect(original).ToNot(BeNil())
		// priorities
		Expect(original.Priorities).To(HaveLen(4), "should have 4 role priorities")
		Expect(original.Priorities).To(ContainElement(
			role.Priority{
				Name:     "role4",
				Priority: 4,
			},
		), "should contain role4")
		Expect(original.Priorities).To(ContainElement(
			role.Priority{
				Name:     "role2",
				Priority: 2,
			},
		), "should contain role2 with priority 2 still")

		// platforms
		Expect(original.Platforms).To(HaveLen(3), "should have 3 platforms")
		Expect(original.Platforms).To(ContainElement(
			createPlatform("platform3", role.SyncTypeRBAC, true, map[string]string{"role1": "role1", "role2": "role2", "role3": "role3"}),
		), "should have platform3")
		Expect(original.Platforms).To(ContainElement(
			createPlatform("platform1", role.SyncTypeRBAC, false, map[string]string{"role1": "role1", "role2": "role2", "role3": "role3"}),
		), "platform1 should not change")
	})

})

func createPlatform(name string, sync role.SyncType, enabled bool, rolemapping map[string]string) *role.Platform {
	return &role.Platform{
		Name:        name,
		Sync:        sync,
		Enabled:     enabled,
		RoleMapping: rolemapping,
	}
}
