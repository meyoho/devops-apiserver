package role

import (
	glog "k8s.io/klog"
	// 	"reflect"
)

// Processor processes a user prermission exchange and comparisson
// given a final
type Processor interface {
	Process(sourcePlatform []*ProjectUserRoleAssignment, targetPlatform []*ProjectUserRoleAssignment) (result []*ProjectUserAssigmentOperation)
}

// StandardProcessor standard calculator for processing role changes
type StandardProcessor struct {
	BaseCalculator
	SourceConversor Conversor
	TargetConversor Conversor
	Calculator      Calculator
}

// NewStandardProcessor constructor for StandardProcessor
func NewStandardProcessor(source, target *Platform, priorities []Priority) Processor {
	base := BaseCalculator{
		Source:     source,
		Target:     target,
		Priorities: priorities,
	}
	return &StandardProcessor{
		BaseCalculator:  base,
		SourceConversor: ConversorBySyncType(source.Sync),
		TargetConversor: ConversorBySyncType(target.Sync),
		Calculator:      CalculatorBySyncType(target.Sync, base),
	}
}

var _ Processor = &StandardProcessor{}

// Process process a range of calculations for role syncing
func (c *StandardProcessor) Process(sourcePlatform []*ProjectUserRoleAssignment, targetPlatform []*ProjectUserRoleAssignment) (result []*ProjectUserAssigmentOperation) {
	// sourcePlatform is the project~user role from source
	// targetPlatform is the project~user role for the target platform
	// so in this scenario the logic should be:
	// 1. converting to common roles
	glog.V(7).Infof("Converting source \"%s\" data...", c.Source.Name)
	sourceConverted := c.SourceConversor.ConvertToStandard(sourcePlatform, c.Source)
	glog.V(7).Infof("Converting target \"%s\" data...", c.Target.Name)
	targetConverted := c.TargetConversor.ConvertToStandard(targetPlatform, c.Target)

	glog.V(7).Info("Filling missing data on both...")
	sourceConverted, targetConverted = c.fillMissingProjects(sourceConverted, targetConverted)

	// 2. calculating according to common schema
	glog.V(7).Infof("Calculating operations between \"%s\" and \"%s\"...", c.Source.Name, c.Target.Name)

	operations := c.Calculator.Calculate(sourceConverted, targetConverted)
	if operations != nil {
		// 3. convert to target platform specific role naming
		glog.V(7).Infof("Got total of %d projects. Converting to target platform's \"%s\" format...", len(operations), c.Target.Name)
		result = c.TargetConversor.ConvertToPlatform(operations, c.Target)

		// remove unecessary operations like:
		// adding and removing the same role to the same user
		result = RemoveUnecessaryOperations(result)
	}
	return
}

func (c *StandardProcessor) fillMissingProjects(source, target []*ProjectUserRoleAssignment) ([]*ProjectUserRoleAssignment, []*ProjectUserRoleAssignment) {
	if source == nil {
		source = make([]*ProjectUserRoleAssignment, 0, len(target))
	}
	if target == nil {
		target = make([]*ProjectUserRoleAssignment, 0, len(source))
	}
	source, target = addMissing(source, target)
	target, source = addMissing(target, source)
	return source, target
}

func addMissing(source, target []*ProjectUserRoleAssignment) ([]*ProjectUserRoleAssignment, []*ProjectUserRoleAssignment) {
	for _, proj := range source {
		found := false
		for _, tar := range target {
			if tar.Name == proj.Name {
				found = true
				break
			}
		}
		if !found {
			target = append(target, NewProject(proj.Namespace).SetName(proj.Name))
		}
	}
	return source, target
}
