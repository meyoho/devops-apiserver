package role

import (
	glog "k8s.io/klog"
)

// RBACCalculator calculator for RBAC role permissions
type RBACCalculator struct {
	BaseCalculator
}

var _ Calculator = RBACCalculator{}

// NewRBACCalculator standard constructor for RBACCalculator
func NewRBACCalculator(base BaseCalculator) RBACCalculator {
	base.LogName = "RBACCalculator"
	calculator := RBACCalculator{BaseCalculator: base}
	calculator.BaseCalculator.ProcessProject = calculator.ProcessProject
	return calculator
}

// ProcessProject processes RBAC projects
// target  users state of current tools
// currentUsers target state that we want to be
func (c RBACCalculator) ProcessProject(project *ProjectUserAssigmentOperation, target *ProjectUserRoleAssignment, currentUsers []*UserAssignment, usersIndex map[string]string) {
	targetUserRoles := target.GetUserRoles()
	add, _, remove := targetUserRoles.Diff(currentUsers)

	for _, item := range add {
		glog.V(6).Infof("[%s] project's \"%s\" ADD Operation user \"%s\" with role \"%s\": Role added", c.LogName, target.Project.Name, item.GetUsername(), item.GetRole())
		project.AddUserRoleOperation(item, OperationTypeAdd)
	}
	for _, item := range remove {
		if _, ok := usersIndex[item.GetUsername()]; !ok {
			continue
		}
		glog.V(6).Infof("[%s] project's \"%s\" REMOVE Operation user \"%s\" with role \"%s\": Role change", c.LogName, target.Project.Name, item.GetUsername(), item.GetRole())
		project.AddUserRoleOperation(item, OperationTypeRemove)
	}
}
