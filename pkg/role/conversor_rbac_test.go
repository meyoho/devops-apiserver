package role_test

import (
	"alauda.io/devops-apiserver/pkg/role"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("RBACConversor.Convert", func() {
	var (
		platform *role.Platform
		projects []*role.ProjectUserRoleAssignment
		// priorities []role.Priority
		result    []*role.ProjectUserRoleAssignment
		conversor role.RBACConversor
	)

	BeforeEach(func() {
		platform = &role.Platform{Name: "acp", Sync: role.SyncTypeRBAC, Enabled: true, RoleMapping: map[string]string{
			"project_admin":   "alauda_project_admin",
			"project_auditor": "alauda_project_auditor",
			"developer":       "alauda_developer",
		}}
		projects = []*role.ProjectUserRoleAssignment{
			role.NewProject("devops-ns").
				AddTool("devops", "devops-dev").
				AddUserRole(
					role.NewUserAssignment("dev", "dev@email", "alauda_developer", ""),
					role.NewUserAssignment("admin", "admin@email", "alauda_project_admin", ""),
				),
			role.NewProject("other-ns").
				AddTool("devops", "devops-test").
				AddUserRole(
					role.NewUserAssignment("test", "test@email", "alauda_developer", ""),
					role.NewUserAssignment("testadmin", "testadmin@email", "alauda_project_auditor", ""),
				),
			// this project does not have tool projects
			// and should be treated as a standalone project
			role.NewProject("standalone-project").
				AddUserRole(
					role.NewUserAssignment("test", "test@email", "alauda_developer", ""),
					role.NewUserAssignment("dev", "dev@email", "alauda_developer", ""),
					role.NewUserAssignment("notfound", "notfound@email", "notfound_role", ""),
				),
		}
		// priorities = []role.Priority{
		// 	role.Priority{Name: "project_admin", Priority: 1},
		// 	role.Priority{Name: "project_auditor", Priority: 3},
		// 	role.Priority{Name: "developer", Priority: 2},
		// }
	})

	JustBeforeEach(func() {
		result = conversor.ConvertToStandard(projects, platform)
	})
	It("should give tree projects: devops, devops-dev and devops-test with user 4 different users", func() {
		Expect(result).ToNot(BeNil())
		Expect(result).To(HaveLen(4))

		Expect(result).To(ContainElement(
			role.NewProject("devops-ns").SetName("devops").
				AddUserRole(
					// devops-ns
					role.NewUserAssignment("dev", "dev@email", "developer", ""),
					role.NewUserAssignment("admin", "admin@email", "project_admin", ""),
					// other-ns
					role.NewUserAssignment("test", "test@email", "developer", ""),
					role.NewUserAssignment("testadmin", "testadmin@email", "project_auditor", ""),
				),
		))

		Expect(result).To(ContainElement(
			role.NewProject("devops-ns").SetName("devops-dev").
				AddUserRole(
					// devops-ns
					role.NewUserAssignment("dev", "dev@email", "developer", ""),
					role.NewUserAssignment("admin", "admin@email", "project_admin", ""),
				),
		))

		Expect(result).To(ContainElement(
			role.NewProject("other-ns").SetName("devops-test").
				AddUserRole(
					// other-ns
					role.NewUserAssignment("test", "test@email", "developer", ""),
					role.NewUserAssignment("testadmin", "testadmin@email", "project_auditor", ""),
				),
		))
		Expect(result).To(ContainElement(
			role.NewProject("standalone-project").
				AddUserRole(
					// should remove not found role
					role.NewUserAssignment("test", "test@email", "developer", ""),
					role.NewUserAssignment("dev", "dev@email", "developer", ""),
				),
		))
	})
})

var _ = Describe("RBACConversor.ConvertBack", func() {
	var (
		platform *role.Platform
		projects []*role.ProjectUserAssigmentOperation
		// priorities []role.Priority
		result    []*role.ProjectUserAssigmentOperation
		conversor role.RBACConversor
	)

	BeforeEach(func() {
		platform = &role.Platform{Name: "acp", Sync: role.SyncTypeRBAC, Enabled: true, RoleMapping: map[string]string{
			"project_admin":   "alauda_project_admin",
			"project_auditor": "alauda_project_auditor",
			"developer":       "alauda_developer",
		}}
		projects = []*role.ProjectUserAssigmentOperation{
			role.NewProjectUserAssigmentOperation(role.NewProject("devops").Project).
				AddUserRoleOperation(role.NewUserAssignment("dev", "dev@email", "developer", ""), role.OperationTypeAdd).
				AddUserRoleOperation(role.NewUserAssignment("admin", "admin@email", "project_admin", ""), role.OperationTypeRemove),
			role.NewProjectUserAssigmentOperation(role.NewProject("other").Project).
				AddUserRoleOperation(role.NewUserAssignment("test", "test@email", "developer", ""), role.OperationTypeAdd).
				AddUserRoleOperation(role.NewUserAssignment("testadmin", "testadmin@email", "project_auditor", ""), role.OperationTypeUpdate),
		}
	})

	JustBeforeEach(func() {
		result = conversor.ConvertToPlatform(projects, platform)
	})
	It("should give two projects: devops, test with 4 different users", func() {
		Expect(result).ToNot(BeNil(), "should return a result")
		Expect(result).To(HaveLen(2), "should only return two projects")

		Expect(result).To(ContainElement(
			role.NewProjectUserAssigmentOperation(role.NewProject("devops").Project).
				AddUserRoleOperation(role.NewUserAssignment("dev", "dev@email", "alauda_developer", "").SetCustom(map[string]string{}), role.OperationTypeAdd).
				AddUserRoleOperation(role.NewUserAssignment("admin", "admin@email", "alauda_project_admin", "").SetCustom(map[string]string{}), role.OperationTypeRemove),
		), "should return a devops project with role names converted")

		Expect(result).To(ContainElement(
			role.NewProjectUserAssigmentOperation(role.NewProject("other").Project).
				AddUserRoleOperation(role.NewUserAssignment("test", "test@email", "alauda_developer", "").SetCustom(map[string]string{}), role.OperationTypeAdd).
				AddUserRoleOperation(role.NewUserAssignment("testadmin", "testadmin@email", "alauda_project_auditor", "").SetCustom(map[string]string{}), role.OperationTypeUpdate),
		))
	})
})
