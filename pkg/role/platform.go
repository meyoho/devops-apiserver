package role

// Platform describe a scheme of a platform
type Platform struct {
	// Name unique identifier for a platform. e.g gitlab, jenkins etc
	Name string `json:"name"`
	// Sync sync type method.
	// SyncTypeRole is recommended when RBAC is supported.
	// Otherwise SyncTypeCustom can be used to create a custom scheme
	Sync SyncType `json:"sync"`
	// Enabled enables the syncing to or from this platform.
	// If turned false should be ignored
	Enabled bool `json:"enabled"`
	// RoleMapping role mapping used for this platform.
	// Should be used in conjunction with the Priority slice
	// "alauda_project_admin": "owner"
	RoleMapping map[string]string `json:"roles"`
	// CustomMapping defines a custom permission schema for the platform.
	// Should record a map for each role. e.g.
	// "alauda_project_admin": { "create": "true", "delete": "false" }
	CustomMapping map[string]map[string]string `json:"custom"`
}

// NewPlatform creates a new platform
func NewPlatform(name string) *Platform {
	platform := &Platform{
		Name:          name,
		Sync:          SyncTypeRBAC,
		Enabled:       true,
		RoleMapping:   map[string]string{},
		CustomMapping: map[string]map[string]string{},
	}
	return platform
}

// SetSyncType set sync
func (p *Platform) SetSyncType(sync SyncType) *Platform {
	p.Sync = sync
	return p
}

// SetSyncTypeString set sync type
func (p *Platform) SetSyncTypeString(sync string) *Platform {
	p.Sync = SyncType(sync)
	return p
}

// SetEnabled enables or disables the specific platform
func (p *Platform) SetEnabled(enabled bool) *Platform {
	p.Enabled = enabled
	return p
}

// SetRoleMapping sets a role mapping
func (p *Platform) SetRoleMapping(mapping map[string]string) *Platform {
	p.RoleMapping = mapping
	return p
}

// SetCustom sets a custom permission mapping
func (p *Platform) SetCustom(mapping map[string]map[string]string) *Platform {
	p.CustomMapping = mapping
	return p
}

// StandardRoleMapping returns a standard of the role mapping
func (p *Platform) StandardRoleMapping() (mapping map[string]string) {
	mapping = p.RoleMapping
	if mapping == nil {
		mapping = make(map[string]string)
	}
	return
}

// ReverseRoleMapping returns a reverse of the role mapping
func (p *Platform) ReverseRoleMapping() (mapping map[string]string) {
	var (
		k, v string
	)
	mapping = make(map[string]string)
	for k, v = range p.StandardRoleMapping() {
		mapping[v] = k
	}
	return
}

// GetCustomByRole returns a custom mapping for a given role name
func (p *Platform) GetCustomByRole(name string) (custom map[string]string) {

	custom, _ = p.CustomMapping[name]
	if custom == nil {
		custom = make(map[string]string)
	}
	return

}

// SyncType the sync type used for the platform
type SyncType string

const (
	// SyncTypeRBAC uses standard role-mapping
	SyncTypeRBAC SyncType = "rbac"
	// SyncTypeRBACMutualExclusive uses role-mapping but a user can only have one role
	SyncTypeRBACMutualExclusive SyncType = "rbac-mutex"
	// SyncTypeCustom uses a custom map to describe its permission system
	SyncTypeCustom SyncType = "custom"
)

// Priority a structure to define the Role and priority
type Priority struct {
	Name     string `json:"name"`
	Priority int    `json:"priority"`
}
