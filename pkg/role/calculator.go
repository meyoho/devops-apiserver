package role

import (
	"sort"

	glog "k8s.io/klog"
)

// Calculator interface for calculating role sync operations
type Calculator interface {
	Calculate(sourceData ProjectUserAssignments, targetCurrent ProjectUserAssignments) []*ProjectUserAssigmentOperation
}

// CalculatorBySyncType returns a calculator according to the SyncType
func CalculatorBySyncType(sync SyncType, base BaseCalculator) Calculator {
	switch sync {
	case SyncTypeRBAC:
		return NewRBACCalculator(base)
	case SyncTypeRBACMutualExclusive:
		return NewRBACMutexCalculator(base)
	case SyncTypeCustom:
		return NewCustomCalculator(base)
	default:
		return base
	}
}

// BaseCalculator base structure shared by calculators
type BaseCalculator struct {
	LogName        string
	Source         *Platform
	Target         *Platform
	Priorities     []Priority
	ProcessProject ProcessProjectFunc
}

// ProcessProjectFunc a function to process a project and attributes its operations
type ProcessProjectFunc func(project *ProjectUserAssigmentOperation, target *ProjectUserRoleAssignment, currentUsers []*UserAssignment, usersIndex map[string]string)

// Calculate calculates RBAC permission syncing
func (c BaseCalculator) Calculate(sourceData ProjectUserAssignments, targetCurrent ProjectUserAssignments) (result []*ProjectUserAssigmentOperation) {
	result = make([]*ProjectUserAssigmentOperation, 0, len(sourceData))
	// check each and every project iterating over each user, and when:
	// user is in source and not in target for a specific role: Add Role operation
	// user is in source and target with different role: edit to target role
	// user is not in source but is in target: Remove role operation
	usersIndex := sourceData.GetUsersIndex()
	for _, sourceProject := range sourceData {
		projectOp := NewProjectUserAssigmentOperation(sourceProject.Project)
		glog.V(9).Infof("[%s] Processing project \"%s\" from source \"%s\"", c.LogName, sourceProject.Project.Name, c.Source.Name)
		targetProject := targetCurrent.FindProject(sourceProject.Name)
		if c.ProcessProject != nil {
			c.ProcessProject(projectOp, targetProject, sourceProject.UserRoles, usersIndex)
		}
		if len(projectOp.UserRoles) > 0 {
			result = append(result, projectOp)
		}
	}
	return
}

// SelectUserRoleByPriority filter multiple roles for one user by priority
func (c BaseCalculator) SelectUserRoleByPriority(projectName string, currentUsers []*UserAssignment) (roleForUser map[string]*UserAssignment) {
	var (
		current *UserAssignment
		exists  bool
	)
	roleForUser = make(map[string]*UserAssignment)
	// filter out the highest priority among all assigned roles
	for _, user := range currentUsers {
		glog.V(9).Infof("[%s] Processing project's \"%s\" user \"%s\" with role \"%s\"", c.LogName, projectName, user.GetUsername(), user.GetRole())
		if current, exists = roleForUser[user.GetUsername()]; !exists {
			current = user
		}
		current = c.GetHighPriority(current, user)
		glog.V(9).Infof("[%s] Processing project's \"%s\" user \"%s\" high priority role \"%s\"", c.LogName, projectName, current.GetUsername(), current.GetRole())
		roleForUser[user.GetUsername()] = current
	}
	return
}

// GetHighPriority returns the role with high priority
func (c BaseCalculator) GetHighPriority(current *UserAssignment, compared *UserAssignment) *UserAssignment {
	if current.GetRole() == compared.GetRole() {
		return current
	}
	var (
		currentPriority  int
		comparedPriority int
	)
	for _, p := range c.Priorities {
		if p.Name == current.GetRole() {
			currentPriority = p.Priority
		}
		if p.Name == compared.GetRole() {
			comparedPriority = p.Priority
		}
	}
	if currentPriority < comparedPriority {
		return current
	}
	return compared
}

// GetOrdered gives an ordered slice of users and assignments
func (c BaseCalculator) GetOrdered(userRoles map[string]*UserAssignment) (ordered []*UsernameUserAssignment) {
	ordered = make([]*UsernameUserAssignment, 0, len(userRoles))
	for username, current := range userRoles {
		ordered = append(ordered, &UsernameUserAssignment{Username: username, UserAssignment: current})
	}
	sort.Slice(ordered, func(i, j int) bool {
		return ordered[i].Username < ordered[j].Username
	})
	return
}

// CalculateRemoval calculates any removal processes for a project
// a user will only be removed from a project if the source's user role assignment is not present
// and at the same time the user is present in source's user system
func (c BaseCalculator) CalculateRemoval(project *ProjectUserAssigmentOperation, targetUserRoles []*UserAssignment, usersIndex map[string]string, processed map[string]*UserAssignment) {
	for _, t := range targetUserRoles {
		// check for users that should be removed:
		// two cases: source users, and non-source users
		// all source users that are not found in the project should be removed
		// all non-source users that are not found should be ignore (manually operated users)
		_, isInIndex := usersIndex[t.GetUsername()]
		_, wasProcessed := processed[t.GetUsername()]
		if isInIndex && !wasProcessed {
			glog.V(9).Infof("[%s] project's \"%s\" REMOVE Operation user \"%s\" with role \"%s\": Removed from project", c.LogName, project.Project.Name, t.GetUsername(), t.GetRole())
			project.AddUserRoleOperation(t, OperationTypeRemove)
		}
	}
}

func (c BaseCalculator) AttachTargetUserInfo(user UserAssignment, targetAssignments UserAssignments) UserAssignment {
	if len(targetAssignments) > 0 {
		user.Username = targetAssignments[0].GetUsername()
		user.Email = targetAssignments[0].GetEmail()
	}
	return user
}

type UsernameUserAssignment struct {
	Username       string
	UserAssignment *UserAssignment
}
