package role

import (
	glog "k8s.io/klog"
)

// RBACMutexCalculator calculator for RBAC role permissions.
// The main difference from RBACCalculator being in this type of RBAC system on user can only have one role per project
// while RBACMutexCalculator allows multiple roles for one project.
// e.g Github
type RBACMutexCalculator struct {
	BaseCalculator
}

var _ Calculator = RBACMutexCalculator{}

// NewRBACMutexCalculator standard constructor for RBACMutexCalculator
func NewRBACMutexCalculator(base BaseCalculator) RBACMutexCalculator {
	base.LogName = "RBACMutexCalculator"
	calculator := RBACMutexCalculator{BaseCalculator: base}
	calculator.BaseCalculator.ProcessProject = calculator.ProcessProject
	return calculator
}

// ProcessProject processes a project using Mutex calculation
func (c RBACMutexCalculator) ProcessProject(project *ProjectUserAssigmentOperation, target *ProjectUserRoleAssignment, currentUsers []*UserAssignment, usersIndex map[string]string) {
	targetUserRoles := target.GetUserRoles()
	var (
		// processedUsers = map[string]bool{}
		roleForUser      = map[string]*UserAssignment{}
		orderedUsernames []*UsernameUserAssignment
		current          *UserAssignment
		username         string
	)
	// filter out the highest priority among all assigned roles
	roleForUser = c.SelectUserRoleByPriority(target.Project.Name, currentUsers)

	// order by username
	orderedUsernames = c.GetOrdered(roleForUser)

	// compares with existing roles do decide the correct operation
	for _, user := range orderedUsernames {
		username = user.Username
		current = user.UserAssignment
		glog.V(9).Infof("[%s] Comparing project's \"%s\" user \"%s\" with role \"%s\"", c.LogName, target.Project.Name, username, current.GetRole())
		targetAssignments := targetUserRoles.FindUserAssignments(current)
		hasSameRole, differentRoles := targetAssignments.HasRole(current.GetRole())

		var addUser = c.AttachTargetUserInfo(*current, targetAssignments)
		switch {
		case hasSameRole:
			// no op
		case len(differentRoles) == 0:
			project.AddUserRoleOperation(&addUser, OperationTypeAdd)
		case len(differentRoles) > 0:
			project.AddUserRoleOperation(&addUser, OperationTypeUpdate)
		}
	}
	c.CalculateRemoval(project, targetUserRoles, usersIndex, roleForUser)
}
