package role_test

import (
	"log"

	"alauda.io/devops-apiserver/pkg/role"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("StandardProcessor.Process", func() {
	var (
		processor      *role.StandardProcessor
		source         *role.Platform
		target         *role.Platform
		priorities     []role.Priority
		sourceProjects []*role.ProjectUserRoleAssignment
		targetProjects []*role.ProjectUserRoleAssignment
		result         []*role.ProjectUserAssigmentOperation
	)

	BeforeEach(func() {
		log.SetOutput(GinkgoWriter)
		source = createPlatform("ace", role.SyncTypeRBAC, true, map[string]string{
			"project_admin":       "alauda_project_admin",
			"project_auditor":     "alauda_project_auditor",
			"space_developer":     "alauda_space_developer",
			"space_admin":         "alauda_space_admin",
			"space_auditor":       "alauda_space_auditor",
			"namespace_developer": "alauda_namespace_developer",
			"nemespace_admin":     "alauda_namespace_admin",
			"namespace_auditor":   "alauda_namespace_auditor",
			"customrole":          "customrole",
		})
		target = createPlatform("gitlab", role.SyncTypeRBACMutualExclusive, true, map[string]string{
			"project_admin":       "Owner",
			"project_auditor":     "Guester",
			"space_developer":     "Developer",
			"space_admin":         "Master",
			"space_auditor":       "Reporter",
			"namespace_developer": "Developer",
			"nemespace_admin":     "Master",
			"namespace_auditor":   "Reporter",
			"customrole":          "Developer",
		})
		// in ACE we have: two projects, with three assigned gitlab orgs:
		// devops-ns: devops, devops-dev
		// other-ns: devops, devops-test
		sourceProjects = []*role.ProjectUserRoleAssignment{
			role.NewProject("devops-ns").
				AddTool("devops", "test").
				AddUserRole(

					role.NewUserAssignment("projectadmin", "projectadmin@email", "alauda_project_admin", ""),
					role.NewUserAssignment("projectauditor", "projectauditor@email", "alauda_project_auditor", ""),

					role.NewUserAssignment("namespaceadmin", "namespaceadmin@email", "alauda_namespace_admin", ""),
					role.NewUserAssignment("namespacedev", "namespacedev@email", "alauda_namespace_developer", ""),
					role.NewUserAssignment("namespaceaudit", "namespaceaudit@email", "alauda_namespace_auditor", ""),

					role.NewUserAssignment("spaceadmin", "spaceadmin@email", "alauda_space_admin", ""),
					role.NewUserAssignment("spacedev", "spacedev@email", "alauda_space_developer", ""),
					role.NewUserAssignment("spaceaudit", "spaceaudit@email", "alauda_space_auditor", ""),
					role.NewUserAssignment("customuser", "customuser@email", "customrole", ""),
				),
			// role.NewProject("other-ns").
			// 	AddTool("devops", "devops-test").
			// 	AddUserRole(
			// 		role.NewUserAssignment("test", "test@email", "alauda_developer", ""),
			// 		role.NewUserAssignment("testadmin", "testadmin@email", "alauda_project_auditor", ""),
			// 	),
		}

		targetProjects = []*role.ProjectUserRoleAssignment{
			role.NewProject("devops").
				AddUserRole(
					role.NewUserAssignment("projectadmin", "projectadmin@email", "Owner", ""),
					// role.NewUserAssignment("projectauditor", "projectauditor@email", "alauda_project_auditor", ""),

					role.NewUserAssignment("namespaceadmin", "namespaceadmin@email", "Master", ""),
					// role.NewUserAssignment("namespacedev", "namespacedev@email", "alauda_namespace_developer", ""),
					// role.NewUserAssignment("namespaceaudit", "namespaceaudit@email", "alauda_namespace_auditor", ""),

					role.NewUserAssignment("spaceadmin", "spaceadmin@email", "Master", ""),
					// role.NewUserAssignment("spacedev", "spacedev@email", "alauda_space_developer", ""),
					// role.NewUserAssignment("spaceaudit", "spaceaudit@email", "alauda_space_auditor", ""),
				),
		}

		/*
			"project_admin":       "alauda_project_admin",
				"project_auditor":     "alauda_project_auditor",
				"space_developer":     "alauda_space_developer",
				"space_admin":         "alauda_space_admin",
				"space_auditor":       "alauda_space_auditor",
				"namespace_developer": "alauda_namespace_developer",
				"nemespace_admin":     "alauda_namespace_admin",
				"namespace_auditor":   "alauda_namespace_auditor",
		*/
		priorities = []role.Priority{
			role.Priority{Name: "project_admin", Priority: 0},
			role.Priority{Name: "project_auditor", Priority: 4},

			role.Priority{Name: "space_admin", Priority: 1},
			role.Priority{Name: "space_developer", Priority: 2},
			role.Priority{Name: "space_auditor", Priority: 3},

			role.Priority{Name: "nemespace_admin", Priority: 1},
			role.Priority{Name: "namespace_developer", Priority: 2},
			role.Priority{Name: "namespace_auditor", Priority: 3},
			role.Priority{Name: "customrole", Priority: 4},
		}
	})

	JustBeforeEach(func() {
		processor = role.NewStandardProcessor(source, target, priorities).(*role.StandardProcessor)
		result = processor.Process(sourceProjects, targetProjects)
	})

	It("should succeed in processing data", func() {
		Expect(result).ToNot(BeNil(), "operation map should not be nil")
		Expect(result).ToNot(BeEmpty(), "should contain operations")

		Expect(result).To(HaveLen(2), "should have add operation for developer and remove operation for admin")
		userRoles := result[0].UserRoles
		for _, a := range result {
			log.Println("project", a.Project.Name, "subproject", a.Project.SubProject, "ns", a.Project.Namespace, "len", len(a.UserRoles))
			if a.Project.Name == "devops" {
				userRoles = a.UserRoles
				break
			}

		}
		Expect(userRoles).To(HaveLen(7))
		users := make(map[role.OperationType][]role.UserAssignment)
		for _, userRole := range userRoles {
			if _, ok := users[userRole.Operation]; !ok {
				users[userRole.Operation] = []role.UserAssignment{}
			}
			users[userRole.Operation] = append(users[userRole.Operation], *userRole.UserAssignment)
		}

		Expect(users[role.OperationTypeAdd]).To(ContainElement(
			role.UserAssignment{Username: "customuser", Email: "customuser@email", Role: "Developer", Custom: map[string]string{}},
		), "should customuser to role developer")
		// Expect(result).To(ContainElement(role.UserAssignmentOperation{
		// 	Operation:      role.OperationTypeRemove,
		// 	UserAssignment: role.UserAssignment{Username: "admin", Email: "admin@alauda.io", Role: "developer"},
		// }), "should remove admin from role developer")
	})

})
