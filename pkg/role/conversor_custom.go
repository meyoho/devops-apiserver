package role

// CustomConversor is a implementation custom permissions schemas
// in essence it ignores any conversions or current state of the tool
// and gives an expected state according to the source current mapping
type CustomConversor struct {
	NoOpConversor
}

var _ Conversor = CustomConversor{}

// ConvertToPlatform assigns its custom permissions for each role
func (CustomConversor) ConvertToPlatform(projects []*ProjectUserAssigmentOperation, platform *Platform) (result []*ProjectUserAssigmentOperation) {
	for i, p := range projects {
		for j, user := range p.UserRoles {
			user.UserAssignment.Custom = platform.GetCustomByRole(user.UserAssignment.Role)
			p.UserRoles[j] = user
		}
		projects[i] = p
	}
	return projects
}
