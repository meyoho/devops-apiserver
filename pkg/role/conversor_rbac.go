package role

// RBACConversor converts one platform specific role system to a common denominator role
type RBACConversor struct{}

var _ Conversor = RBACConversor{}

// ConvertToStandard converts using RBAC method
func (RBACConversor) ConvertToStandard(projects []*ProjectUserRoleAssignment, platform *Platform) (result []*ProjectUserRoleAssignment) {
	return CustomConvert(projects, platform, platform.ReverseRoleMapping())
}

// ConvertToPlatform convert back to its role names
func (RBACConversor) ConvertToPlatform(projects []*ProjectUserAssigmentOperation, platform *Platform) (result []*ProjectUserAssigmentOperation) {
	var (
		roleMapping = platform.StandardRoleMapping()
	)
	for i, p := range projects {
		for j, user := range p.UserRoles {
			publicRoleName := user.UserAssignment.Role
			user.UserAssignment, _ = ConvertRoleName(user.UserAssignment, roleMapping)
			p.UserRoles[j].UserAssignment = user.UserAssignment
			if p.UserRoles[j].UserAssignment != nil {
				p.UserRoles[j].UserAssignment.Custom = platform.GetCustomByRole(publicRoleName)
			}
		}
		projects[i] = p
	}
	return projects
}

// CustomConvert provided a custom mapping function it enables to use the same logic to convert forward and backwards from or into standard role names
func CustomConvert(projects []*ProjectUserRoleAssignment, _ *Platform, roleMapping map[string]string) (result []*ProjectUserRoleAssignment) {
	result = make([]*ProjectUserRoleAssignment, 0, len(projects))
	if len(projects) == 0 {
		return
	}

	var (
		index   = map[string]*ProjectUserRoleAssignment{}
		project *ProjectUserRoleAssignment
		ok      bool
	)
	// in order to have a precise way to compare roles
	// is necessary to make projects flat in regards to Projects and ToolProjects,
	// assigning UserRoles that are shared by these.
	for _, proj := range projects {
		if len(proj.ToolProjects) > 0 {
			for _, toolProj := range proj.ToolProjects {
				if project, ok = index[toolProj]; !ok {
					project = NewProject(proj.Namespace).SetName(toolProj)
				}
				index[toolProj] = project.AddUserRole(proj.GetUserRolesCopy()...)
			}
		} else {
			index[proj.Name] = proj
		}

	}
	// after making it plane it is possible to convert role names according to the platform scheme
	for _, project := range index {
		project.UserRoles = ConvertRoleNames(project.UserRoles, roleMapping)
		result = append(result, project)
	}
	return
}

// ConvertRoleNames convert roles names given a provided role mapping and filter out any not-mapped role names
func ConvertRoleNames(userRoles []*UserAssignment, roleMapping map[string]string) []*UserAssignment {
	var (
		user *UserAssignment
		ok   bool
	)
	for i := 0; i < len(userRoles); i++ {
		user = userRoles[i]
		if user, ok = ConvertRoleName(user, roleMapping); ok {
			userRoles[i] = user
		} else {
			userRoles = append(userRoles[:i], userRoles[i+1:]...)
			i--
		}
	}
	return userRoles
}

// ConvertRoleName for one user assignment
func ConvertRoleName(user *UserAssignment, roleMapping map[string]string) (result *UserAssignment, found bool) {
	var name string
	name, found = roleMapping[user.Role]
	if found {
		user.Role = name
	}
	result = user
	return
}
