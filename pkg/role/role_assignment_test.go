package role_test

import (
	"alauda.io/devops-apiserver/pkg/role"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("UserAssignments.Diff", func() {
	var (
		uas         []*role.UserAssignment
		targetState []*role.UserAssignment
		add         role.UserAssignments
		remove      role.UserAssignments
		update      role.UserAssignments
	)

	BeforeEach(func() {
		uas = []*role.UserAssignment{
			// should add namespace_admin and namespace_auditor
			{Username: "zgsu", Email: "zgsu@qq.com", Role: "project_admin", Space: ""},

			// no change
			{Username: "daniel", Email: "fsfd@qq.com", Role: "project_admin", Space: ""},
			{Username: "jtcheng", Email: "jtcheng@qq.com", Role: "project_admin", Space: ""},

			// remove this and add project_admin
			{Username: "zpyu", Email: "fsfd@qq.com", Role: "project_auditor", Space: ""},
		}

		targetState = []*role.UserAssignment{
			{Username: "zgsu", Email: "zgsu@alauda.io", Role: "namespace_admin", Space: ""},
			{Username: "zgsu", Email: "zgsu@alauda.io", Role: "namespace_auditor", Space: ""},
			{Username: "zgsu", Email: "zgsu@alauda.io", Role: "project_admin", Space: ""},

			{Username: "daniel", Email: "daniel@alauda.io", Role: "project_admin", Space: ""},
			{Username: "jtcheng", Email: "", Role: "project_admin", Space: ""},

			{Username: "zpyu", Email: "", Role: "project_admin", Space: ""},
		}
	})

	JustBeforeEach(func() {
		add, update, remove = role.UserAssignments(uas).Diff(targetState)
	})

	It("should return correct operations", func() {
		Expect(add).To(HaveLen(3))
		Expect(add).To(ContainElement(&role.UserAssignment{Username: "zgsu", Email: "zgsu@alauda.io", Role: "namespace_auditor", Space: ""}))
		Expect(add).To(ContainElement(&role.UserAssignment{Username: "zgsu", Email: "zgsu@alauda.io", Role: "namespace_admin", Space: ""}))
		Expect(add).To(ContainElement(&role.UserAssignment{Username: "zpyu", Email: "", Role: "project_admin", Space: ""}))

		Expect(update).To(HaveLen(3))
		Expect(update).To(ContainElement(&role.UserAssignment{Username: "daniel", Email: "daniel@alauda.io", Role: "project_admin", Space: ""}))
		Expect(update).To(ContainElement(&role.UserAssignment{Username: "jtcheng", Email: "", Role: "project_admin", Space: ""}))
		Expect(update).To(ContainElement(&role.UserAssignment{Username: "zgsu", Email: "zgsu@alauda.io", Role: "project_admin", Space: ""}))

		Expect(remove).To(HaveLen(1))
		Expect(remove).To(ContainElement(&role.UserAssignment{Username: "zpyu", Email: "fsfd@qq.com", Role: "project_auditor", Space: ""}))
	})
})

var _ = Describe("Remove.Diff", func() {
	var (
		uas         []*role.UserAssignment
		targetState []*role.UserAssignment
		remove      role.UserAssignments
	)

	BeforeEach(func() {
		uas = []*role.UserAssignment{
			// should add namespace_admin and namespace_auditor
			{Username: "zgsu", Email: "zgsu@qq.com", Role: "project_admin", Space: ""},

			// no change
			{Username: "daniel", Email: "fsfd@qq.com", Role: "project_admin", Space: ""},
			{Username: "jtcheng", Email: "jtcheng@qq.com", Role: "project_admin", Space: ""},

			// remove this and add project_admin
			{Username: "zpyu", Email: "fsfd@qq.com", Role: "project_admin", Space: ""},
		}

		targetState = []*role.UserAssignment{
			//{Username: "zgsu", Email: "zgsu@alauda.io", Role: "namespace_admin", Space: ""},
			//{Username: "zgsu", Email: "zgsu@alauda.io", Role: "namespace_auditor", Space: ""},
			{Username: "zgsu", Email: "zgsu@qq.com", Role: "project_admin", Space: ""},

			//{Username: "daniel", Email: "daniel@alauda.io", Role: "project_admin", Space: ""},
			{Username: "jtcheng", Email: "jtcheng@qq.com", Role: "project_admin", Space: ""},

			{Username: "zpyu", Email: "", Role: "project_admin", Space: ""},
		}
	})

	JustBeforeEach(func() {
		_, _, remove = role.UserAssignments(uas).Diff(targetState)
	})

	It("should return correct operations", func() {
		Expect(remove).To(HaveLen(1))
		Expect(remove).To(ContainElement(&role.UserAssignment{Username: "daniel", Email: "fsfd@qq.com", Role: "project_admin", Space: ""}))
	})
})

var _ = Describe("role.RemoveUnecessaryOperations", func() {
	var (
		Original            []*role.ProjectUserAssigmentOperation
		result              []*role.ProjectUserAssigmentOperation
		jtchengaddoperation role.UserAssignmentOperation

		UserAssigmentOperationProject role.Project
	)

	BeforeEach(func() {
		zpyu := role.UserAssignment{
			Username: "zpyu",
			Role:     "AlaudaProjectAdmin",
			Email:    "",
		}
		zgsu := role.UserAssignment{
			Username: "zgsu",
			Role:     "AlaudaProjectAdmin",
			Email:    "",
		}
		jtcheng := role.UserAssignment{
			Username: "jtcheng",
			Role:     "AlaudaProjectAdmin",
			Email:    "",
		}

		zpyuremoveoperation := role.UserAssignmentOperation{
			Operation:      "remove",
			UserAssignment: &zpyu,
		}
		zgsuremoveoperation := role.UserAssignmentOperation{
			Operation:      "remove",
			UserAssignment: &zgsu,
		}
		zpyuaddoperation := role.UserAssignmentOperation{
			Operation:      "add",
			UserAssignment: &zpyu,
		}
		zgsuaddoperation := role.UserAssignmentOperation{
			Operation:      "add",
			UserAssignment: &zgsu,
		}
		jtchengaddoperation = role.UserAssignmentOperation{
			Operation:      "add",
			UserAssignment: &jtcheng,
		}

		UserAssigmentOperationProject = role.Project{
			Name:      "testproject",
			Namespace: "a6",
		}
		Original = []*role.ProjectUserAssigmentOperation{
			{
				Project: UserAssigmentOperationProject,
				UserRoles: []*role.UserAssignmentOperation{
					&zgsuaddoperation,
					&zpyuaddoperation,
					&zgsuremoveoperation,
					&zpyuremoveoperation,
					&jtchengaddoperation,
				},
			},
		}
	})

	JustBeforeEach(func() {
		result = role.RemoveUnecessaryOperations(Original)
	})

	It("should return correct operations", func() {
		Expect(result).To(HaveLen(1))

		Expect(result).To((Equal([]*role.ProjectUserAssigmentOperation{
			{
				Project: UserAssigmentOperationProject,
				UserRoles: []*role.UserAssignmentOperation{
					&jtchengaddoperation,
				},
			},
		})))

	})
})
