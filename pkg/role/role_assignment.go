package role

import (
	rbacv1 "k8s.io/api/rbac/v1"
	glog "k8s.io/klog"
	"strings"
)

// UserAssignment is in item that gives a current assignment
// of a user to a role or a custom assignment
type UserAssignment struct {
	Username string
	Email    string
	Role     string
	Space    string
	Custom   map[string]string
}

// GetUsername returns username
func (s UserAssignment) GetUsername() string {
	return s.Username
}

// GetEmail returns email
func (s UserAssignment) GetEmail() string {
	return s.Email
}

// GetRole returns role name
func (s UserAssignment) GetRole() string {
	return s.Role
}

// GetSpace returns space name
func (s UserAssignment) GetSpace() string {
	return s.Space
}

// GetCustom retrieves custom permission data
func (s UserAssignment) GetCustom() map[string]string {
	return s.Custom
}

// SetCustom sets a custom permission data
func (s *UserAssignment) SetCustom(custom map[string]string) *UserAssignment {
	s.Custom = custom
	return s
}

// NewUserAssignment creates a new *UserAssignment
func NewUserAssignment(username, email, role, space string) *UserAssignment {
	return &UserAssignment{
		Email:    email,
		Username: username,
		Role:     role,
		Space:    space,
	}
}

// NewUserAssignmentsFromRoleBinding return a list of user assigments according to the the list of users
func NewUserAssignmentsFromRoleBinding(roleBinding rbacv1.RoleBinding) (userAssignments []*UserAssignment) {
	userAssignments = make([]*UserAssignment, 0, len(roleBinding.Subjects))
	username := getUsernameFromAnnotation(roleBinding)
	for _, s := range roleBinding.Subjects {
		switch s.Kind {
		case rbacv1.UserKind:
			userAssignments = append(
				userAssignments,
				NewUserAssignment(userNameFromEmail(username, s.Name), s.Name, roleBinding.RoleRef.Name, ""),
			)
		}
	}
	return
}

func getUsernameFromAnnotation(roleBinding rbacv1.RoleBinding) (username string) {
	// role binding does not provide a username field, so we need to create
	// a new way to set this info: using annotation we can emulate a username field,
	// if not found we then use the email prefix as username
	annotations := roleBinding.GetAnnotations()
	if len(annotations) > 0 {
		username = annotations["alauda.io/username"]
	}
	return
}

func userNameFromEmail(username, email string) string {
	if username == "" {
		username = strings.Split(email, "@")[0]
	}
	return username
}

// UserAssignments slice of *UserAssignment
type UserAssignments []*UserAssignment

// HasRole returns two values:
// hasRole: true if has the exact same role
// differentRoles: a slice of all the roles that does not match
func (ua UserAssignments) HasRole(role string) (hasRole bool, differentRoles UserAssignments) {
	differentRoles = make(UserAssignments, 0, len(ua))
	for _, u := range ua {
		if u.GetRole() == role {
			hasRole = true
		} else {
			differentRoles = append(differentRoles, u)
		}
	}
	return
}

//
// Diff will return the elementes that we should add or update or remove
// current is the state that we want to be

//ua tool platform
//targetState alauda platform now
func (ua UserAssignments) Diff(targetState []*UserAssignment) (add UserAssignments, update UserAssignments, remove UserAssignments) {
	result := make(UserAssignments, 0, len(ua)+len(targetState))

	for _, item := range targetState {
		if !ua.exists(*item, result) {
			add = append(add, item)
		} else {
			update = append(update, item)
		}
	}
	for _, item := range ua {
		//if the alauda platform not contain the user
		if !UserAssignments(targetState).exists(*item, result) {
			remove = append(remove, item)
		}
	}
	return
}

func (ua UserAssignments) exists(target UserAssignment, result UserAssignments) bool {
	//target: alauda   ua: tool
	foundUsers := FindUserAssignments(&target, ua, result)
	for _, item := range foundUsers {
		if item.GetRole() == target.GetRole() {
			return true
		}
	}
	return false
}

// FindUserAssignments looks for assignments of a usuer inside a user assignment slice
func (ua UserAssignments) FindUserAssignments(lookup *UserAssignment) UserAssignments {
	result := make(UserAssignments, 0, len(ua))
	return FindUserAssignments(lookup, ua, result)
}

// FindUserAssignments using a lookup user/role. Does not consider custom permissions
func FindUserAssignments(lookup *UserAssignment, userAssignments []*UserAssignment, result UserAssignments) UserAssignments {
	result = result[:0]
	if len(userAssignments) > 0 {
		for _, user := range userAssignments {
			if user.Username == lookup.Username || (user.Email == lookup.Email && user.Email != "") {
				result = append(result, user)
			}
		}
	}
	return result
}

// OperationType is the kind of operation to be set on a user role assignment
type OperationType string

const (
	// OperationTypeNoOp defines a no operation Operation mode
	OperationTypeNoOp OperationType = "no-change"
	// OperationTypeAdd assign a role to a user
	OperationTypeAdd OperationType = "add"
	// OperationTypeRemove remove a role assignment from a user
	OperationTypeRemove OperationType = "remove"
	// OperationTypeUpdate change user role to a specific role. Used for custom and rbac mutex
	OperationTypeUpdate OperationType = "update"
)

// UserAssignmentOperation Operation on a user role assignment.
// Used as a calculation result for a user-role operation
type UserAssignmentOperation struct {
	Operation      OperationType   `json:"operation"`
	UserAssignment *UserAssignment `json:"userAssignment"`
}

// Project simple crude project data
type Project struct {
	Name       string `json:"name"`
	SubProject string `json:"subproject"`
	Namespace  string `json:"namespace"`
}

// SetName sets name
func (proj *Project) SetName(name string) *Project {
	proj.Name = name
	return proj
}

// ProjectUserRoleAssignment project data
type ProjectUserRoleAssignment struct {
	Project
	UserRoles    []*UserAssignment `json:"userRoles"`
	ToolProjects []string          `json:"toolProjects"`
}

// NewProject new project constructor
func NewProject(namespace string) *ProjectUserRoleAssignment {
	return &ProjectUserRoleAssignment{
		Project: Project{
			Name:      namespace,
			Namespace: namespace,
		},
		UserRoles:    make([]*UserAssignment, 0, 10),
		ToolProjects: make([]string, 0, 3),
	}
}

// AddTool adds a tool
func (proj *ProjectUserRoleAssignment) AddTool(name ...string) *ProjectUserRoleAssignment {
	proj.ToolProjects = append(proj.ToolProjects, name...)
	return proj
}

// AddUserRole adds a user role
func (proj *ProjectUserRoleAssignment) AddUserRole(userRole ...*UserAssignment) *ProjectUserRoleAssignment {
	proj.UserRoles = append(proj.UserRoles, userRole...)
	return proj
}

// SetName sets name
func (proj *ProjectUserRoleAssignment) SetName(name string) *ProjectUserRoleAssignment {
	proj.Project.Name = name
	return proj
}

// SetSubProject sets subproject
func (proj *ProjectUserRoleAssignment) SetSubProject(subproject string) *ProjectUserRoleAssignment {
	proj.Project.SubProject = subproject
	return proj
}

// GetUserRoles return user  role
func (proj *ProjectUserRoleAssignment) GetUserRoles() (result UserAssignments) {
	if proj != nil {
		result = proj.UserRoles
	}
	return
}

// GetUserRoles return user  role
func (proj *ProjectUserRoleAssignment) Log(LogPrefix string, level glog.Level) {
	for _, user := range proj.UserRoles {
		glog.V(level).Infof("calculating %s projectnamespace:%s, project.name:%s, project.subproject:%s, tool:%#v, user:%#v", LogPrefix, proj.Namespace, proj.Name, proj.SubProject, proj.ToolProjects, *user)
	}
}

// GetUserRolesCopy clone a list of user roles for easier copying
func (proj *ProjectUserRoleAssignment) GetUserRolesCopy() (result []*UserAssignment) {
	result = make([]*UserAssignment, len(proj.UserRoles))
	for i, user := range proj.UserRoles {
		result[i] = NewUserAssignment(user.Username, user.Email, user.Role, user.Space)
	}
	return
}

// ProjectUserAssignments a collection of project user assigments
// useful to encapsulate general functions
type ProjectUserAssignments []*ProjectUserRoleAssignment

// FindProject find a project by its name
func (p ProjectUserAssignments) FindProject(projectName string) *ProjectUserRoleAssignment {
	return FindProjectByName(projectName, p)
}

// GetUsersIndex processes all users in all projects and return a map with
// the username as key and email as value. Useful to make quick check operations against a big
// list of users
func (p ProjectUserAssignments) GetUsersIndex() (index map[string]string) {
	index = make(map[string]string)
	for _, proj := range p {
		for _, u := range proj.GetUserRoles() {
			if _, ok := index[u.GetUsername()]; !ok {
				index[u.GetUsername()] = u.GetEmail()
			}
		}
	}
	return
}

// FindProjectByName returns a project if the name matches, returns nil in no match
func FindProjectByName(projectName string, projects []*ProjectUserRoleAssignment) (result *ProjectUserRoleAssignment) {
	if len(projects) > 0 {
		for _, proj := range projects {
			if proj.Name == projectName {
				result = proj
				break
			}
		}
	}
	return
}

// ProjectUserAssigmentOperation operations on a project
type ProjectUserAssigmentOperation struct {
	Project   Project
	UserRoles []*UserAssignmentOperation
}

// NewProjectUserAssigmentOperation creates a new project base on
func NewProjectUserAssigmentOperation(project Project) *ProjectUserAssigmentOperation {
	return &ProjectUserAssigmentOperation{
		Project:   project,
		UserRoles: make([]*UserAssignmentOperation, 0, 10),
	}
}

// AddUserRoleOperation adds a user assignment with an operation
func (p *ProjectUserAssigmentOperation) AddUserRoleOperation(ua *UserAssignment, op OperationType) *ProjectUserAssigmentOperation {
	p.UserRoles = append(p.UserRoles, NewUserAssignmentOperation(ua, op))
	return p
}

func NewUserAssignmentOperation(ua *UserAssignment, op OperationType) *UserAssignmentOperation {
	return &UserAssignmentOperation{
		UserAssignment: ua,
		Operation:      op,
	}
}

func FindProjectByNameInOperations(name string, projects []*ProjectUserAssigmentOperation) *ProjectUserAssigmentOperation {
	for _, proj := range projects {
		if proj.Project.Name == name {
			return proj
		}
	}
	return nil
}

// RemoveUnecessaryOperations
func RemoveUnecessaryOperations(projectsoperations []*ProjectUserAssigmentOperation) []*ProjectUserAssigmentOperation {
	userrole := make(map[string]int)
	for _, projectoperation := range projectsoperations {
		//clear the dict
		for k := range userrole {
			delete(userrole, k)
		}
		//make it as 0 if the user have remove and add to a same role
		for _, useroperation := range projectoperation.UserRoles {
			switch useroperation.Operation {
			case OperationTypeAdd:
				userrole[useroperation.UserAssignment.Username+string(useroperation.UserAssignment.Role)] += 1
			case OperationTypeRemove:
				userrole[useroperation.UserAssignment.Username+string(useroperation.UserAssignment.Role)] -= 1
			case OperationTypeUpdate:
				userrole[useroperation.UserAssignment.Username+string(useroperation.UserAssignment.Role)] = 1
			}
		}
		newprojectoperation := []*UserAssignmentOperation{}
		for _, useroperation := range projectoperation.UserRoles {
			if userrole[useroperation.UserAssignment.Username+string(useroperation.UserAssignment.Role)] != 0 {
				newprojectoperation = append(newprojectoperation, useroperation)
			}
		}
		projectoperation.UserRoles = newprojectoperation
	}
	return projectsoperations
}
