package role_test

import (
	"alauda.io/devops-apiserver/pkg/role"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("RBACCalculator.Calculate", func() {
	var (
		base role.BaseCalculator

		source     role.ProjectUserAssignments
		current    role.ProjectUserAssignments
		result     []*role.ProjectUserAssigmentOperation
		calculator role.RBACCalculator
	)

	BeforeEach(func() {
		base, source, current = getBasicScenario()
		source.FindProject("devops").AddUserRole(role.NewUserAssignment("admin", "admin@email", "developer", ""))
		calculator = role.NewRBACCalculator(base)
	})

	JustBeforeEach(func() {
		result = calculator.Calculate(source, current)
	})
	It("should give two products", func() {
		Expect(result).ToNot(BeNil())
		Expect(result).ToNot(BeEmpty())
		/*
			devops:
			- test(developer): remove
			- dev(developer): add
			- dev(project_auditor): add
		*/
		Expect(role.FindProjectByNameInOperations("devops", result)).ToNot(BeNil())
		project := role.FindProjectByNameInOperations("devops", result)
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignmentOperation(role.NewUserAssignment("dev", "dev@email", "developer", ""), role.OperationTypeAdd)))
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignmentOperation(role.NewUserAssignment("admin", "admin@email", "developer", ""), role.OperationTypeAdd)))
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignmentOperation(role.NewUserAssignment("dev", "dev@email", "project_auditor", ""), role.OperationTypeAdd)))
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignmentOperation(role.NewUserAssignment("test", "test@email", "developer", ""), role.OperationTypeRemove)))

		/*
			test:
			*** ignore: notfound
			test(developer): add
			testadmin(project_admin): remove
			testadmin(project_auditor): add
		*/
		Expect(result).To(ContainElement(
			role.NewProjectUserAssigmentOperation(role.NewProject("test").Project).
				AddUserRoleOperation(role.NewUserAssignment("test", "test@email", "developer", ""), role.OperationTypeAdd).
				AddUserRoleOperation(role.NewUserAssignment("testadmin", "testadmin@email", "project_auditor", ""), role.OperationTypeAdd).
				AddUserRoleOperation(role.NewUserAssignment("testadmin", "testadmin@email", "project_admin", ""), role.OperationTypeRemove),
		))

		/*
			empty:
			dev(developer): remove
			test(developer): remove
			admin(project_admin): remove
		*/
		Expect(result).To(ContainElement(
			role.NewProjectUserAssigmentOperation(role.NewProject("empty").Project).
				AddUserRoleOperation(role.NewUserAssignment("dev", "dev@email", "developer", ""), role.OperationTypeRemove).
				AddUserRoleOperation(role.NewUserAssignment("test", "test@email", "developer", ""), role.OperationTypeRemove).
				AddUserRoleOperation(role.NewUserAssignment("admin", "admin@email", "project_admin", ""), role.OperationTypeRemove),
		))

	})
})
