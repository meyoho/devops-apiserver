// Package role usage is related to role syncing between products
// its main purpose is to abstract role mapping and allow
// syncing a user/role assignment from one source to a target platform.
// In an ideal scenario any product could be used as source
// and any other product could be used as a target
package role
