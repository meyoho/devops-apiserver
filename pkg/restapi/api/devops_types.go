package api

import (
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// ResourceKindJenkins kind for jenkins resource
	ResourceKindJenkinsBinding = "jenkinsbinding"
	// pipeline
	ResourceKindPipelineConfig = "pipelineconfig"
	// coderepo
	ResourceKindCodeRepoBinding = "coderepobinding"
	ResourceKindCodeRepository  = "coderepository"
	// image repo
	ResourceKindImageRegistry   = "imageregistry"
	ResourceKindImageRepository = "imagerepository"
)

// NewObjectMetaSplit because of dependency of different applications
// we need to add a method to use only base types
func NewObjectMetaSplit(name, namespace string, labels, annotations map[string]string, creation time.Time) ObjectMeta {
	return ObjectMeta{
		Name:              name,
		Namespace:         namespace,
		Labels:            labels,
		CreationTimestamp: v1.NewTime(creation),
		Annotations:       annotations,
	}
}

// ImageRegistryBindingRepositoriesDetails presents image repositories
type ImageRegistryBindingRepositoriesDetails struct {
	*v1alpha1.ImageRegistryBindingRepositories
}

// ImageRepositoryList contains a list of ImageRepository in the cluster.
type ImageRepositoryList struct {
	ListMeta ListMeta `json:"listMeta"`

	// Unordered list of ImageRepository.
	Items []v1alpha1.ImageRepository `json:"imagerepositories"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}
