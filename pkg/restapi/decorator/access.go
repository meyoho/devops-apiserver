package decorator

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/emicklei/go-restful"
	authorizationv1 "k8s.io/api/authorization/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/runtime"
	"net/http"
	"sigs.k8s.io/controller-runtime/pkg/client/apiutil"
	"sync"
)

var (
	accessReview     AccessReview
	initAccessReview sync.Once
)

type AccessReview struct {
	decorator.Client
}

func AccessReviewFilter(svr server.Server) AccessReview {
	initAccessReview.Do(func() {
		accessReview = AccessReview{
			Client: decorator.Client{
				Server: svr,
			},
		}
	})

	return accessReview
}

func (a AccessReview) NewAccessReviewFilter(resource runtime.Object, verb string) restful.FilterFunction {
	return func(request *restful.Request, response *restful.Response, chain *restful.FilterChain) {
		gvk, err := apiutil.GVKForObject(resource, Scheme)
		if err != nil {
			_ = response.WriteError(http.StatusInternalServerError, err)
			return
		}

		plural, _ := meta.UnsafeGuessKindToResource(gvk)

		// try to get namespace from path parameter
		namespace := request.PathParameter("namespace")
		accessReview := &authorizationv1.SelfSubjectAccessReview{
			Spec: authorizationv1.SelfSubjectAccessReviewSpec{
				ResourceAttributes: &authorizationv1.ResourceAttributes{
					Namespace: namespace,
					Verb:      verb,
					Group:     gvk.Group,
					Version:   gvk.Version,
					Resource:  plural.Resource,
				},
			},
		}

		client, err := a.Client.GetManager().Client(request)
		if err != nil {
			a.Client.Server.HandleError(err, request, response)
			return
		}

		accessReview, err = client.AuthorizationV1().SelfSubjectAccessReviews().Create(accessReview)
		if err != nil {
			_ = response.WriteError(http.StatusInternalServerError, err)
			return
		}

		if !accessReview.Status.Allowed {
			_ = response.WriteErrorString(http.StatusForbidden, accessReview.Status.Reason)
			return
		}

		chain.ProcessFilter(request, response)
	}

}
