package decorator

import (
	"alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"fmt"
	"github.com/emicklei/go-restful"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/cache"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/runtime/signals"
	"sync"
)

var (
	initClient sync.Once
	clientGen  Client

	Scheme *runtime.Scheme
	stop   <-chan struct{}
)

func init() {
	Scheme = clientgoscheme.Scheme
	_ = v1alpha1.AddToScheme(Scheme)

	stop = signals.SetupSignalHandler()
}

// ClientDecorator decorator for client generator
func ClientDecorator(svr server.Server) Client {
	initClient.Do(func() {
		clientGen = NewClient(svr)

		var err error
		clientGen.cacheClient, err = newCacheClient(svr)
		if err != nil {
			svr.Log().Error("Failed to initialize cache client", zap.Error(err))
			// TODO find a better solution to deal with error happens during the initialization, see http://jira.alauda.cn/browse/DEVOPS-3169
			panic(err)
		}
	})
	return clientGen
}

// Client client injector middleware
// will inject kubernetes and devops client
type Client struct {
	decorator.Client
	cacheClient client.Client
}

// NewClient constructor for new client
func NewClient(svr server.Server) Client {
	return Client{
		Client: decorator.Client{
			Server: svr,
		},
	}
}

func newCacheClient(svr server.Server) (client.Client, error) {
	managerConfig := svr.GetManager().ManagerConfig()
	restConfig, err := managerConfig.Load()

	svr.Log().Debug("Starting initializing cache client", zap.String("config", fmt.Sprintf("%#v", restConfig)))
	if err != nil {
		return nil, err
	}

	mapper, err := NewDynamicRESTMapper(restConfig, WithLazyDiscovery)
	if err != nil {
		return nil, err
	}

	informerCache, err := cache.New(restConfig, cache.Options{Scheme: Scheme, Mapper: mapper})
	if err != nil {
		return nil, err
	}

	writeClient, err := client.New(restConfig, client.Options{Scheme: Scheme, Mapper: mapper})
	if err != nil {
		return nil, err
	}

	go func() {
		err := informerCache.Start(stop)
		if err != nil {
			svr.Log().Error("Unable to start informer cache", zap.Error(err))
			// TODO find a better solution to deal with error happens during the initialization, see http://jira.alauda.cn/browse/DEVOPS-3169
			panic(err)
		}
	}()

	return &client.DelegatingClient{
		Reader: &client.DelegatingReader{
			CacheReader:  informerCache,
			ClientReader: writeClient,
		},
		Writer:       writeClient,
		StatusClient: writeClient,
	}, nil
}

// DevOpsClientFilter filter to populate the context with a user's Bearer token client
// will handle errors and may return errors on request
func (d Client) DevOpsClientFilter(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	// generate config from request
	config, err := d.Client.GetManager().Config(req)
	if err != nil {
		d.Client.Server.HandleError(err, req, res)
		return
	}
	devopsClient, err := versioned.NewForConfig(config)
	if err != nil {
		log.Errorf("DevOpsClientFilter get devopsClient err is %s", err.Error())
		d.Client.Server.HandleError(err, req, res)
		return
	}
	d.SetDevOpsClientContext(devopsClient, err, req, res, chain)
}

// SetDevOpsClientContext given a client and an error will create the common logic to handle error
// and populate the context with the client
func (d Client) SetDevOpsClientContext(devopsClient versioned.Interface, err error, req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	if err != nil || devopsClient == nil {
		d.Client.Server.HandleError(err, req, res)
		return
	}

	req.Request = req.Request.WithContext(context.WithDevOpsClient(req.Request.Context(), devopsClient))

	chain.ProcessFilter(req, res)
}

func (d Client) AppClientFilter(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	// generate config from request
	config, err := d.Client.GetManager().Config(req)
	if err != nil {
		d.Client.Server.HandleError(err, req, res)
		return
	}
	baseDomain := common.LocalBaseDomain()
	client, err := app.NewClient(config, "", baseDomain)
	if err != nil {
		log.Errorf("Can not get appclient and  err is %s", err.Error())
		d.Client.Server.HandleError(err, req, res)
		return
	}
	d.SetAppClientContext(client, err, req, res, chain)
}

func (d Client) SetAppClientContext(appClient *app.ApplicationClient, err error, req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	if err != nil || appClient == nil {
		log.Errorf("SetAppClientContext can not set appclientcontext err is %v and appClient is %v", err, appClient)
		d.Client.Server.HandleError(err, req, res)
		return
	}
	req.Request = req.Request.WithContext(context.WithAppClient(req.Request.Context(), appClient))

	chain.ProcessFilter(req, res)
}

func (d Client) ConfigFilter(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	// generate config from request
	config, err := d.Client.GetManager().Config(req)
	if err != nil {
		log.Errorf("ConfigFilter can not configfilter and err is %s", err.Error())
		d.Client.Server.HandleError(err, req, res)
		return
	}
	d.SetConfigContext(config, err, req, res, chain)
}

func (d Client) SetConfigContext(config *rest.Config, err error, req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	if err != nil || config == nil {
		log.Errorf("SetConfigContext can not set configcontext err is %v and config is %v", err, config)
		d.Client.Server.HandleError(err, req, res)
		return
	}
	req.Request = req.Request.WithContext(context.WithConfig(req.Request.Context(), config))
	chain.ProcessFilter(req, res)
}

func (d Client) CacheClientFilter(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	req.Request = req.Request.WithContext(context.WithCacheClient(req.Request.Context(), d.cacheClient))
	chain.ProcessFilter(req, res)
}
