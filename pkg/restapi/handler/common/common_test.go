package common_test

import (
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	core "k8s.io/api/core/v1"
)

var _ = Describe("Common", func() {

	It("test volumeinfo data", func() {

		containers := []core.Container{
			{
				VolumeMounts: []core.VolumeMount{
					{
						Name:      "volume-4",
						MountPath: "dddd",
					},
					{
						Name:      "volume-5",
						MountPath: "sdfdsfds",
					},
				},
			},
		}
		volumes := []core.Volume{
			{
				Name:         "volume-4",
				VolumeSource: core.VolumeSource{HostPath: &core.HostPathVolumeSource{Path: "dddd"}},
			},
			{
				Name:         "volume-5",
				VolumeSource: core.VolumeSource{EmptyDir: &core.EmptyDirVolumeSource{}},
			},
		}
		volumesinfo := common.GetVolumeInfo(containers, volumes)
		Expect(volumesinfo[0].Len()).To(Equal(2))
		Expect(volumesinfo[0][0].Name).To(Equal("volume-4"))
	})
})
