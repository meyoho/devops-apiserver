package common

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"encoding/json"
	"fmt"
	"github.com/emicklei/go-restful"
	"strconv"
	"strings"
)

// PropertyName is used to get the value of certain property of data cell.
// For example if we want to get the namespace of certain Deployment we can use DeploymentCell.GetProperty(NamespaceProperty)
type PropertyName string

// List of all property names supported by the UI.
const (
	NameProperty                       = "name"
	FullNameProperty                   = "fullName"
	NameLengthProperty                 = "nameLength"
	CreationTimestampProperty          = "creationTimestamp"
	NamespaceProperty                  = "namespace"
	StatusProperty                     = "status"
	ScopeProperty                      = "scope"
	KindProperty                       = "kind"
	DisplayNameProperty                = "displayName"
	DisplayEnNameProperty              = "displayEnName"
	DisplayZhNameProperty              = "displayZhName"
	DomainProperty                     = "domain"
	LabelProperty                      = "label"
	LabelsProperty                     = "labels"
	SecretTypeProperty                 = "secretType"
	ProjectProperty                    = "project"
	ProductNameProperty                = "productName"
	PipelineConfigProperty             = "pipelineConfig"
	CodeRepoServiceProperty            = "codeRepoService"
	CodeRepoBindingProperty            = "codeRepoBinding"
	CodeRepositoryProperty             = "codeRepository"
	CodeQualityBindingProperty         = "codeQualityBinding"
	ExactNameProperty                  = "exactName"
	LabelEqualProperty                 = "labelEq"
	JenkinsProperty                    = "jenkins"
	JenkinsBindingProperty             = "jenkinsBinding"
	StartedAtProperty                  = "startedAt"
	PipelineCreationTimestampProperty  = "pipelineCreationTimestamp"
	ImageRegistryProperty              = "imageRegistry"
	ImageRegistryBindingProperty       = "imageRegistryBinding"
	ImageRepositoryProperty            = "imageRepository"
	LatestCommitAt                     = "latestCommitAt"
	MicroservicesConfigProfileProperty = "profile"
	MicroservicesConfigLabelProperty   = "label"
	CategoryProperty                   = "category"
	MultiBranchCategoryProperty        = "multiBranchCategory"
	MultiBranchNameProperty            = "multiBranchName"
	PipelineStatusProperty             = "pipelineStatus"
	ASMHostName                        = "asmHost"
)

// Parses query parameters of the request and returns a DataSelectQuery object
func ParseDataSelectPathParameter(request *restful.Request) *dataselect.Query {
	paginationQuery := parsePaginationPathParameter(request)
	sortQuery := parseSortPathParameter(request)
	filterQuery := parseFilterPathParameter(request)
	return FilterByDisplaynameToLower(dataselect.NewDataSelectQuery(paginationQuery, sortQuery, filterQuery))
}

func parsePaginationPathParameter(request *restful.Request) *dataselect.PaginationQuery {
	itemsPerPage, err := strconv.ParseInt(request.QueryParameter("itemsPerPage"), 10, 0)
	if err != nil {
		return dataselect.NoPagination
	}

	page, err := strconv.ParseInt(request.QueryParameter("page"), 10, 0)
	if err != nil {
		return dataselect.NoPagination
	}

	// Frontend pages start from 1 and backend starts from 0
	return dataselect.NewPaginationQuery(int(itemsPerPage), int(page-1))
}

// Parses query parameters of the request and returns a SortQuery object
func parseSortPathParameter(request *restful.Request) *dataselect.SortQuery {
	return dataselect.NewSortQuery(strings.Split(request.QueryParameter("sortBy"), ","))
}

func parseFilterPathParameter(request *restful.Request) *dataselect.FilterQuery {
	return dataselect.NewFilterQuery(strings.Split(request.QueryParameter("filterBy"), ","))
}

func FilterByDisplaynameToLower(query *dataselect.Query) *dataselect.Query {
	if query == nil || query.FilterQuery == nil || len(query.FilterQuery.FilterByList) == 0 {
		return query
	}
	for i, f := range query.FilterQuery.FilterByList {
		if f.Property == DisplayNameProperty {
			f.Value = dataselect.StdComparableString(strings.ToLower(fmt.Sprintf("%s", f.Value)))
			query.FilterQuery.FilterByList[i] = f
			break
		}
	}
	return query
}

// ComparableJSONIn type of json comparable
type ComparableJSONIn string

// Compare comparae two object
func (c ComparableJSONIn) Compare(otherV dataselect.ComparableValue) int {
	other := otherV.(dataselect.StdComparableString)
	return strings.Compare(string(c), string(other))
}

// Contains one array should in the json array
func (c ComparableJSONIn) Contains(otherV dataselect.ComparableValue) bool {
	other := otherV.(dataselect.StdComparableString)
	split := strings.Split(string(other), ":")
	cur := string(c)

	var categories []devopsv1alpha1.I18nName
	err := json.Unmarshal([]byte(cur), &categories)
	if err != nil {
		// back compatible
		categories = []devopsv1alpha1.I18nName{
			{
				En: cur,
			},
		}
	}

	count := 0
	for _, item := range split {
		for _, category := range categories {
			if item == category.En {
				count++
				break
			}
		}
	}

	return (count == len(split))
}
