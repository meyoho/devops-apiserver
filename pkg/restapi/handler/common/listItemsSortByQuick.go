package common

import (
	"github.com/emicklei/go-restful"
	"reflect"
	"sort"
	"strings"
)

func ItemsSort(v interface{}) interface{} {
	var result interface{}
	var vValue reflect.Value

	if reflect.TypeOf(v).Kind() == reflect.Ptr {
		pt := reflect.ValueOf(v)
		vValue = pt.Elem()
		result = vValue.Interface()
	} else {
		result = v
		vValue = reflect.ValueOf(result)
	}
	itemsValue := vValue.FieldByName("Items").Interface()

	sort.Slice(itemsValue, func(i, j int) bool {
		return strings.Compare(reflect.ValueOf(itemsValue).Index(i).FieldByName("ObjectMeta").FieldByName("Name").String(), reflect.ValueOf(itemsValue).Index(j).FieldByName("ObjectMeta").FieldByName("Name").String()) < 0
	})

	return result

}

// WriteResponse writes a response
func WriteResponseBySort(data interface{}, err error, req *restful.Request, res *restful.Response, h interface{}) {
	if err != nil {
		_ = reflect.ValueOf(h).FieldByName("Server").MethodByName("HandleError").Call([]reflect.Value{reflect.ValueOf(err), reflect.ValueOf(req), reflect.ValueOf(res)})

		return
	}
	res.WriteAsJson(ItemsSort(data))
}
