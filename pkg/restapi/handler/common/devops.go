package common

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"fmt"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/restapi/api"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	ResourceKindJenkinsBinding = "jenkinsbinding"

	// pipeline
	ResourceKindPipelineConfig              = "pipelineconfig"
	ResourceKindPipeline                    = "pipeline"
	ResourceKindPipelineTemplate            = "PipelineTemplate"
	ResourceKindPipelineTaskTemplate        = "PipelineTaskTemplate"
	ResourceKindPipelineTemplateSync        = "PipelineTemplateSync"
	ResourceKindClusterPipelineTemplate     = "ClusterPipelineTemplate"
	ResourceKindClusterPipelineTaskTemplate = "ClusterPipelineTaskTemplate"

	// coderepo
	ResourceKindCodeRepoBinding = "coderepobinding"

	//projectmanagement
	ResourceKindProjectManagementBinding = "projectmanagementbinding"

	// image repo
	ResourceKindImageRegistryBinding = "imageregistrybinding"

	// code quality
	ResourceKindCodeQualityBinding = "codequalitybinding"

	// DevOpsAPIVersion version for api
	DevOpsAPIVersion = "devops.alauda.io/v1alpha1"
	// AuthAPIVersion version for api
	AUTH_APIVersion = "auth.alauda.io/v1alpha1"
	// ProductName product name
	ProductName = "Alauda DevOps"
	// AnnotationsKeyDisplayName displayName key for annotations
	AnnotationsKeyDisplayName = "displayName"
	// AnnotationsKeyDisplayNameEn english displayName key for annotations
	AnnotationsKeyDisplayNameEn = "displayName.en"
	// AnnotationsKeyDisplayNameZh Chinese displayName key for annotations
	AnnotationsKeyDisplayNameZh = "displayName.zh-CN"
	// AnnotationsKeyProduct product name key for annotations
	AnnotationsKeyProduct = "product"
	// AnnotationsKeyProductVersion product version key for annotations
	AnnotationsKeyProductVersion = "product.version"

	// AnnotationsKeyToolType tool type key for annotations
	AnnotationsKeyToolType = "toolType"

	// AnnotationsKeyCategories categories for anntations
	AnnotationsKeyCategories = "categories"
	// AnnotationsKeyMultiBranchCategory category for multibranch
	AnnotationsKeyMultiBranchCategory = "multiBranchCategory"
	// AnnotationsKeyMultiBranchName branch name for multibranch
	AnnotationsKeyMultiBranchName = "multiBranchName"
	// AnnotationsKeyMultiBranchBranchList branch list for multibranch
	AnnotationsKeyMultiBranchBranchList = "jenkins.branch"
	// AnnotationsKeyMultiBranchStaleBranchList stale branch list for multibranch
	AnnotationsKeyMultiBranchStaleBranchList = "jenkins.stale.branch"
	// AnnotationsKeyMultiBranchPRList pr list for multibranch
	AnnotationsKeyMultiBranchPRList = "jenkins.pr"
	// AnnotationsKeyMultiBranchStalePRList stale pr list for multibranch
	AnnotationsKeyMultiBranchStalePRList = "jenkins.stale.pr"

	// AnnotationsCommit commit ID for pipeline
	AnnotationsCommit = "commit"
	// AnnotationsPipelineConfigName pipeline config name
	AnnotationsPipelineConfigName = "pipelineConfig.name"

	AnnotationGeneratorName = "generatorName"
)

// NewObjectMetaSplit because of dependency of different applications
// we need to add a method to use only base types
func NewObjectMetaSplit(name, namespace string, labels, annotations map[string]string, creation time.Time) ObjectMeta {
	return ObjectMeta{
		Name:              name,
		Namespace:         namespace,
		Labels:            labels,
		CreationTimestamp: metav1.NewTime(creation),
		Annotations:       annotations,
	}
}

// Annotator basic interface for product related annotations
type Annotator interface {
	GetProductAnnotations(metav1.ObjectMeta, devops.AnnotationProvider) metav1.ObjectMeta

	// TODO: see if this is necessary
	// SetDisplayName(metav1.ObjectMeta, string) metav1.ObjectMeta
	// SetDescription(metav1.ObjectMeta, string) metav1.ObjectMeta
}

// DevOpsAnnotator creates product specific annotations
type DevOpsAnnotator struct {
	productVersion string
}

// make sure that this struct satisfies the interface
var _ Annotator = DevOpsAnnotator{}

// GetProductAnnotations add product related anotations
// will overwrite if already existing
func (d DevOpsAnnotator) GetProductAnnotations(meta metav1.ObjectMeta, provider devops.AnnotationProvider) metav1.ObjectMeta {
	if meta.Annotations == nil {
		meta.Annotations = map[string]string{}
	}
	meta.Annotations[provider.AnnotationsKeyProduct()] = ProductName
	meta.Annotations[provider.AnnotationsKeyProductVersion()] = d.productVersion
	return meta
}

// Resource common functionality for resources
type Resource interface {
	GetObjectMeta() api.ObjectMeta
}

// CloneMeta clean up an object meta and clone
func CloneMeta(obj metav1.ObjectMeta) (new metav1.ObjectMeta) {
	new = *obj.DeepCopy()
	new.ResourceVersion = ""
	new.UID = ""
	new.SelfLink = ""
	new.CreationTimestamp = metav1.Time{}
	return
}

// ConvertToListOptions convert dsQuery to a listOptions struct
func ConvertToListOptions(dsQuery *dataselect.Query) (ls metav1.ListOptions) {
	ls = api.ListEverything
	if dsQuery == nil || dsQuery.FilterQuery == nil {
		return
	}
	for _, r := range dsQuery.FilterQuery.FilterByList {
		switch r.Property {
		case LabelProperty:
			str := fmt.Sprintf("%s", r.Value)
			split := strings.Split(str, ":")
			if len(split) > 1 {
				ls.LabelSelector = fmt.Sprintf("%s=%s", split[0], split[1])
			}
		case LabelEqualProperty:
			str := fmt.Sprintf("%s", r.Value)
			split := strings.Split(str, ":")
			if len(split) > 1 {
				ls.LabelSelector = fmt.Sprintf("%s==%s", split[0], split[1])
			}
		}
	}
	return
}

func MergeAnnotations(original, override map[string]string) map[string]string {
	result := make(map[string]string)
	if original == nil {
		original = make(map[string]string)
	}

	for k, v := range original {
		result[k] = v
	}

	if override == nil {
		override = make(map[string]string)
	}
	for k, v := range override {
		result[k] = v
	}
	return result
}

// CheckDevOpsAPIVersion get
func CheckDevOpsAPIVersion(typeMeta metav1.TypeMeta) metav1.TypeMeta {
	typeMeta.APIVersion = DevOpsAPIVersion
	return typeMeta
}

// CheckAuthAPIVersion get
func CheckAuthAPIVersion(typeMeta metav1.TypeMeta) metav1.TypeMeta {
	typeMeta.APIVersion = AUTH_APIVersion
	return typeMeta
}

func GetBaseDomainAnnoLabel(BaseDomain string, LabelAnnotation string) string {
	if BaseDomain == "" {
		BaseDomain = devops.UsedBaseDomain
	}
	return fmt.Sprintf("%s/%s", BaseDomain, LabelAnnotation)
}

func GetDevopsGlobal(BaseDomain string, LabelAnnotation string) string {
	if BaseDomain == "" {
		BaseDomain = devops.UsedBaseDomain
	}
	//should be like devops.alauda.io/global
	return fmt.Sprintf("devops.%s/%s", BaseDomain, LabelAnnotation)
}
