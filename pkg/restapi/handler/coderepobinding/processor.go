package coderepobinding

import (
	"alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/coderepository"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelineconfig"
	"alauda.io/devops-apiserver/pkg/restapi/handler/secret"
	"k8s.io/client-go/kubernetes"

	"context"
	"sync"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListCodeRepo list coderepo instances
func (p processor) ListCodeRepoBinding(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *CodeRepoBindingList, err error) {

	annotationprovider := localcontext.ExtraConfig(ctx).AnnotationProvider
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list coderepobinding end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	// fetch data from list
	coderepolist, err := client.DevopsV1alpha1().CodeRepoBindings(namespace.ToRequestParam()).List(v1alpha1.ListOptions())

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &CodeRepoBindingList{
		Items:    make([]v1alpha1.CodeRepoBinding, 0),
		ListMeta: api.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(coderepolist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToCodeRepoSlice(result, annotationprovider)
	data.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeleteCodeRepo delete coderepobinding instance
func (p processor) DeleteCodeRepoBinding(ctx context.Context, client versioned.Interface, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete coderepobinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().CodeRepoBindings(namespace).Delete(name, &metav1.DeleteOptions{})
	return
}

// GetCodeRepoBinding retrieve coderepobinding instance
func (p processor) GetCodeRepoBinding(ctx context.Context, client versioned.Interface, namespace, name string) (data *v1alpha1.CodeRepoBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get coderepobinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().CodeRepoBindings(namespace).Get(name, common.GetOptionsInCache)
	return
}

//CreateCodeRepoBinding create coderepobinding instance
func (p processor) CreateCodeRepoBinding(ctx context.Context, client versioned.Interface, coderepobinding *v1alpha1.CodeRepoBinding, namespace string) (data *v1alpha1.CodeRepoBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create coderepobinding end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().CodeRepoBindings(namespace).Create(coderepobinding)
	return
}

//UpdateCodeRepoBinding update a coderepobinding instance
func (p processor) UpdateCodeRepoBinding(ctx context.Context, client versioned.Interface, coderepobinding, oldcoderepobinding *v1alpha1.CodeRepoBinding, namespace, name string) (data *v1alpha1.CodeRepoBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update coderepobinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	binding := oldcoderepobinding.DeepCopy()
	binding.SetAnnotations(coderepobinding.GetAnnotations())
	binding.Spec = coderepobinding.Spec

	data, err = client.DevopsV1alpha1().CodeRepoBindings(namespace).Update(binding)
	return
}

func (p processor) GetRemoteRepositoryList(ctx context.Context, client versioned.Interface, namespace, name string) (*CodeRepoBindingRepositoriesDetails, error) {

	opts := &v1alpha1.CodeRepoBindingRepositoryOptions{}
	result, err := client.DevopsV1alpha1().CodeRepoBindings(namespace).GetRemoteRepositories(name, opts)
	if err != nil {
		return nil, err
	}

	return &CodeRepoBindingRepositoriesDetails{
		result,
	}, nil

}
func (p processor) GetCodeRepositoryListInBinding(ctx context.Context, client versioned.Interface, namespace, name string, dataselect *dataselect.Query) (*coderepository.CodeRepositoryList, error) {
	binding, err := client.DevopsV1alpha1().CodeRepoBindings(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}

	crsList, err := client.DevopsV1alpha1().CodeRepositories(namespace).List(api.ListEverything)
	if err != nil {
		log.Error("error while listing repositories", log.Err(err))
	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	var repos []v1alpha1.CodeRepository
	for _, item := range crsList.Items {
		for _, condition := range binding.Status.Conditions {
			if item.GetName() == condition.Name {
				repos = append(repos, item)
			}
		}
	}
	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider
	return toList(repos, nonCriticalErrors, dataselect, annotationProvider), nil

}

func (p processor) GetCodeRepoBindingResource(ctx context.Context, client versioned.Interface, namespace *common.NamespaceQuery, dsQuery *dataselect.Query) (resourceList *common.ResourceList, err error) {
	resourceList = &common.ResourceList{}
	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider
	repositoryList, err := GetCodeRepositoryList(client, namespace, dsQuery, annotationProvider)
	if err != nil {
		return
	}

	wait := sync.WaitGroup{}
	for _, r := range repositoryList.Items {
		go func(namespace, name string) {
			dsQuery := dataselect.GeSimpleFieldQuery(common.CodeRepositoryProperty, name)
			items := pipelineconfig.GetPipelineConfigListAsResourceList(client, namespace, dsQuery, annotationProvider)
			resourceList.Items = append(resourceList.Items, items...)
			wait.Done()
		}(r.ObjectMeta.Namespace, r.ObjectMeta.Name)
		wait.Add(1)
	}
	wait.Wait()
	return
}

// GetCodeRepositoryList returns a list of coderepobindingc
func GetCodeRepositoryList(client versioned.Interface, namespace *common.NamespaceQuery, dsQuery *dataselect.Query, provider devops.AnnotationProvider) (*coderepository.CodeRepositoryList, error) {

	crsList, err := client.DevopsV1alpha1().CodeRepositories(namespace.ToRequestParam()).List(api.ListEverything)
	if err != nil {
		log.Error("error while listing repositories", log.Err(err))

	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	return toList(crsList.Items, nonCriticalErrors, dsQuery, provider), nil
}

func toList(codeRepositories []v1alpha1.CodeRepository, nonCriticalErrors []error, dsQuery *dataselect.Query, provider devops.AnnotationProvider) *coderepository.CodeRepositoryList {
	crsList := &coderepository.CodeRepositoryList{
		Items:    make([]v1alpha1.CodeRepository, 0),
		ListMeta: api.ListMeta{TotalItems: len(codeRepositories)},
	}

	crsCells, filteredTotal := dataselect.GenericDataSelectWithFilter(coderepository.ToCells(codeRepositories, provider), dsQuery)
	codeRepositories = coderepository.FromCells(crsCells)
	crsList.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	crsList.Errors = nonCriticalErrors

	for _, repo := range codeRepositories {
		crsList.Items = append(crsList.Items, toDetailsInList(repo))
	}

	return crsList
}

func toDetailsInList(codeRepository v1alpha1.CodeRepository) v1alpha1.CodeRepository {
	crs := v1alpha1.CodeRepository{
		ObjectMeta: codeRepository.ObjectMeta,
		TypeMeta: metav1.TypeMeta{
			Kind: api.ResourceKindCodeRepository,
		},
		Spec:   codeRepository.Spec,
		Status: codeRepository.Status,
	}
	return crs
}

// ConvertToCodeRepoSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToCodeRepoSlice(filtered []metav1.Object, provider devops.AnnotationProvider) (items []v1alpha1.CodeRepoBinding) {
	items = make([]v1alpha1.CodeRepoBinding, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.CodeRepoBinding); ok {
			cm.ObjectMeta.Annotations[provider.AnnotationsKeyToolType()] = v1alpha1.ToolChainCodeRepositoryName
			cm.Kind = common.ResourceKindCodeRepoBinding
			items = append(items, *cm)
		}
	}
	return
}

func (p processor) GetCodeRepoBindingSecretList(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string, query *dataselect.Query) (secretlist secret.SecretList, err error) {
	extra := localcontext.ExtraConfig(ctx)
	credentialsNamespace := extra.CredentialsNamespace
	systemNamespace := extra.SystemNamespace
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get coderepo binding secrets  end", log.Err(err))
	}()

	binding, err := GetCodeRepoBinding(client, namespace, name)
	if err != nil {
		logger.Error("get coderepo binding err", log.Err(err))
		return
	}
	annotationprovider := localcontext.ExtraConfig(ctx).AnnotationProvider

	namespaceQuery := common.NewSameNamespaceQuery(namespace)
	secretList, err := secret.GetSecretList(k8sclient, appclient, namespaceQuery, query, false, credentialsNamespace, systemNamespace, annotationprovider)
	if err != nil {
		logger.Error("get secret List err ", log.Err(err))
		return
	}
	var secrets []secret.Secret
	for _, se := range secretList.Secrets {
		if binding.Spec.Account.Secret.Name == se.ObjectMeta.Name && binding.ObjectMeta.Namespace == se.ObjectMeta.Namespace {
			secrets = append(secrets, se)
			break
		}
	}

	secretlist = secret.SecretList{
		ListMeta: api.ListMeta{TotalItems: 1},
		Secrets:  secrets,
	}
	return
}

func GetCodeRepoBinding(client versioned.Interface, namespace, name string) (*v1alpha1.CodeRepoBinding, error) {
	crs, err := client.DevopsV1alpha1().CodeRepoBindings(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	return crs, nil
}
