package coderepobinding

import (
	"context"
	"net/http"
	"net/url"

	"alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/coderepository"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/secret"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for CodeRepository
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListCodeRepoBinding(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*CodeRepoBindingList, error)
	DeleteCodeRepoBinding(ctx context.Context, client versioned.Interface, namespace, name string) error
	GetCodeRepoBinding(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.CodeRepoBinding, error)
	CreateCodeRepoBinding(ctx context.Context, client versioned.Interface, coderepositorybinding *v1alpha1.CodeRepoBinding, namespace string) (*v1alpha1.CodeRepoBinding, error)
	UpdateCodeRepoBinding(ctx context.Context, client versioned.Interface, coderepositorybinding, oldcoderepositorybinding *v1alpha1.CodeRepoBinding, namespace, name string) (*v1alpha1.CodeRepoBinding, error)
	GetCodeRepoBindingResource(ctx context.Context, client versioned.Interface, namespace *common.NamespaceQuery, dataselect *dataselect.Query) (*common.ResourceList, error)
	GetCodeRepositoryListInBinding(ctx context.Context, client versioned.Interface, namespace, name string, dataselect *dataselect.Query) (*coderepository.CodeRepositoryList, error)
	GetRemoteRepositoryList(ctx context.Context, client versioned.Interface, namespace, name string) (*CodeRepoBindingRepositoriesDetails, error)
	GetCodeRepoBindingSecretList(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string, query *dataselect.Query) (secret.SecretList, error)
}

// New builder method for coderepositorybinding.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("CodeRepoBinding related APIs").ApiVersion("v1").Path("/api/v1/coderepobinding")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "coderepobinding")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List CodeRepoBinding instances`).
					To(handler.ListCodeRepoBinding).
					Writes(CodeRepoBindingList{}).
					Returns(http.StatusOK, "OK", CodeRepoBindingList{}),
			),
		),
	)
	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List CodeRepoBinding instances`).
					To(handler.ListCodeRepoBinding).
					Writes(CodeRepoBindingList{}).
					Returns(http.StatusOK, "OK", CodeRepoBindingList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create CodeRepoBinding instance").
				To(handler.CreateCodeRepoBinding).
				Returns(http.StatusOK, "Create CodeRepoBinding instance", v1alpha1.CodeRepoBinding{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get CodeRepositorybinding details").
				Param(ws.PathParameter("name", "name of the coderepositorybinding")).
				Param(ws.PathParameter("namespace", "namespace of the coderepositorybinding")).
				To(handler.GetCodeRepoBinding).
				Returns(http.StatusOK, "Get CodeRepoBinding details Complete", v1alpha1.CodeRepoBinding{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete CodeRepository instance").
				Param(ws.PathParameter("name", "name of the coderepositorybinding")).
				Param(ws.PathParameter("namespace", "namespace of the coderepositorybinding")).
				To(handler.DeleteCodeRepoBinding).
				Returns(http.StatusOK, "Retrieve CodeRepositorybinding instance", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update CodeRepositorybinding instance").
				Param(ws.PathParameter("name", "name of the coderepository")).
				Param(ws.PathParameter("namespace", "namespace of the coderepositorybinding")).
				To(handler.UpdateCodeRepoBinding).
				Returns(http.StatusOK, "Update CodeRepositorybinding instance", v1alpha1.CodeRepoBinding{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/resources").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("retrieve resources associated with coderepositorybinding").
				Param(ws.PathParameter("name", "name of the coderepository")).
				Param(ws.PathParameter("namespace", "namespace of the coderepositorybinding")).
				To(handler.GetCodeRepoBindingResource).
				Returns(http.StatusOK, "Get CodeRepositorybinding resource instance", common.ResourceList{}),
		),
	)
	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("/{namespace}/{name}/repositories").
					Filter(clientFilter.DevOpsClientFilter).
					Doc("retrieve repositories associated with coderepositorybinding").
					Param(ws.PathParameter("name", "name of the coderepository")).
					Param(ws.PathParameter("namespace", "namespace of the coderepositorybinding")).
					To(handler.GetCodeRepositoryListInBinding).
					Returns(http.StatusOK, "Get CodeRepositorybinding repositories instance", common.ResourceList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/remote-repositories").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("retrieve repositories associated with coderepositorybinding").
				Param(ws.PathParameter("name", "name of the coderepository")).
				Param(ws.PathParameter("namespace", "namespace of the coderepositorybinding")).
				To(handler.GetRemoteRepositoryList).
				Returns(http.StatusOK, "Get CodeRepositorybinding remote-repositories instance", common.ResourceList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/secrets").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get coderepositorybinding secrets").
				Param(ws.PathParameter("name", "name of the coderepository")).
				Param(ws.PathParameter("namespace", "namespace of the coderepositorybinding")).
				To(handler.GetCodeRepoBindingSecretList).
				Returns(http.StatusOK, "Get CodeRepositorybinding secret list", common.ResourceList{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListCodeRepository list coderepository instances
func (h Handler) ListCodeRepoBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	coderepositorybinding, err := h.Processor.ListCodeRepoBinding(req.Request.Context(), client, query, common.ParseNamespacePathParameter(req))
	common.WriteResponseBySort(*coderepositorybinding, err, req, res, h)
}

//DeleteCodeRepository Delete coderepository instance
func (h Handler) DeleteCodeRepoBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteCodeRepoBinding(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//GetCodeRepoBinding Get coderepositorybinding instance
func (h Handler) GetCodeRepoBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.GetCodeRepoBinding(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//CreateCodeRepoBinding create coderepositorybinding instance
func (h Handler) CreateCodeRepoBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newCodeRepoBinding := new(v1alpha1.CodeRepoBinding)

	if err := req.ReadEntity(newCodeRepoBinding); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	redirectURL := req.QueryParameter("redirectUrl")
	if _, err := url.Parse(redirectURL); err != nil {
		redirectURL = ""
	}

	if newCodeRepoBinding.GetSecretName() != "" {
		opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
			SecretName:  newCodeRepoBinding.GetSecretName(),
			Namespace:   newCodeRepoBinding.GetSecretNamespace(),
			RedirectURL: redirectURL,
		}
		authorizeResponse, err := client.DevopsV1alpha1().CodeRepoServices().Authorize(newCodeRepoBinding.Spec.CodeRepoService.Name, &opts)
		if err != nil {
			log.Errorf("authorizeResponse: %v", log.Any("authorizeResponse", authorizeResponse))
			h.WriteResponse(nil, err, req, res)
			return
		}

		if authorizeResponse != nil && authorizeResponse.AuthorizeUrl != "" {
			res.WriteHeaderAndEntity(http.StatusPreconditionRequired, authorizeResponse)
			return
		}
	}

	namespace := req.PathParameter("namespace")

	data, err := h.Processor.CreateCodeRepoBinding(req.Request.Context(), client, newCodeRepoBinding, namespace)
	h.WriteResponse(data, err, req, res)
}

//UpdateCodeRepoBinding update coderepositorybinding instance
func (h Handler) UpdateCodeRepoBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	provider := localcontext.ExtraConfig(req.Request.Context()).AnnotationProvider
	newCodeRepoBinding := new(v1alpha1.CodeRepoBinding)

	if err := req.ReadEntity(newCodeRepoBinding); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	redirectURL := req.QueryParameter("redirectUrl")
	if _, err := url.Parse(redirectURL); err != nil {
		redirectURL = ""
	}

	opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
		SecretName:  newCodeRepoBinding.GetSecretName(),
		Namespace:   newCodeRepoBinding.GetSecretNamespace(),
		RedirectURL: redirectURL,
	}

	authorizeResponse, err := client.DevopsV1alpha1().CodeRepoServices().Authorize(newCodeRepoBinding.Spec.CodeRepoService.Name, &opts)
	if err != nil {
		log.Errorf("authorizeResponse: %v", log.Any("authorizeResponse", authorizeResponse))
		h.WriteResponse(nil, err, req, res)
		return
	}

	if authorizeResponse != nil && authorizeResponse.AuthorizeUrl != "" {
		res.WriteHeaderAndEntity(http.StatusPreconditionRequired, authorizeResponse)
		return
	}

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	oldCodeRepoBinding, err := h.Processor.GetCodeRepoBinding(req.Request.Context(), client, namespace, name)
	if err != nil {
		errors.HandleInternalError(res, err)
		return
	}

	_, err = h.Processor.UpdateCodeRepoBinding(req.Request.Context(), client, newCodeRepoBinding, oldCodeRepoBinding, namespace, name)

	if err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	newBinding, err := h.Processor.GetCodeRepoBinding(req.Request.Context(), client, namespace, name)
	if err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	result := coderepository.GetResourcesReferToRemovedRepos(client, oldCodeRepoBinding, newBinding, provider)
	h.WriteResponse(result, err, req, res)

}

//GetCodeRepoBindingResource get coderepositorybinding resource
func (h Handler) GetCodeRepoBindingResource(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	namespace := common.ParseNamespacePathParameter(req)
	name := req.PathParameter("name")
	dsQuery := dataselect.GeSimpleLabelQuery(common.CodeRepoBindingProperty, name)

	data, err := h.Processor.GetCodeRepoBindingResource(req.Request.Context(), client, namespace, dsQuery)
	h.WriteResponse(data, err, req, res)
}

func (h Handler) GetCodeRepoBindingSecretList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	k8sclient := abcontext.Client(req.Request.Context())
	appclient := localcontext.AppClient(req.Request.Context())
	secretquery := common.ParseDataSelectPathParameter(req)
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	data, err := h.Processor.GetCodeRepoBindingSecretList(req.Request.Context(), client, k8sclient, appclient, namespace, name, secretquery)
	h.WriteResponse(data, err, req, res)
}

//
func (h Handler) GetCodeRepositoryListInBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	name := req.PathParameter("name")
	namespace := req.PathParameter("namespace")
	list, err := h.Processor.GetCodeRepositoryListInBinding(req.Request.Context(), client, namespace, name, query)
	h.WriteResponse(list, err, req, res)
}

//CodeRepoBindingCronCheck get coderepositorybinding cron
func (h Handler) GetRemoteRepositoryList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	data, err := h.Processor.GetRemoteRepositoryList(req.Request.Context(), client, namespace, name)
	h.WriteResponse(data, err, req, res)

}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
