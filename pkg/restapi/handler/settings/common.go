package settings

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
)

type toolTypeRespCell v1alpha1.ToolTypeResp

func (self toolTypeRespCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case common.NameProperty:
		return dataselect.StdComparableString(self.Name)
	}
	// if name is not supported then just return a constant dummy value, sort will have no effect.
	return nil
}

func toCells(std []v1alpha1.ToolTypeResp) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = toolTypeRespCell(std[i])
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []v1alpha1.ToolTypeResp {
	std := make([]v1alpha1.ToolTypeResp, len(cells))
	for i := range std {
		std[i] = v1alpha1.ToolTypeResp(cells[i].(toolTypeRespCell))
	}
	return std
}
