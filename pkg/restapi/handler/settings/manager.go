// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package settings

import (
	"log"
	"reflect"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"

	backendapi "alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/handler/settings/api"
	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
)

// SettingsManager is a structure containing all settings manager members.
type SettingsManager struct {
	settings        *api.Settings
	devopsSettings  *api.DevopsSettings
	authSettings    *api.AuthSettings
	rawSettings     map[string]string
	authRawSettings map[string]string
}

// load config map data into settings manager and return true if new settings are different.
func (sm *SettingsManager) load(client kubernetes.Interface) (configMap *v1.ConfigMap, isDifferent bool) {
	var err error
	configMap, isDifferent, err = sm.loadConfigMap(client, api.SettingsConfigMapNamespace, api.SettingsConfigMapName, sm.rawSettings)
	if err != nil {
		sm.restoreConfigMap(client)
		return
	}
	if isDifferent {
		sm.rawSettings = configMap.Data
		sm.restoreFromRaw()
	}
	return
}

func (sm *SettingsManager) loadConfigMap(client kubernetes.Interface, namespace, name string, old map[string]string) (configMap *v1.ConfigMap, isDifferent bool, err error) {
	configMap, err = client.CoreV1().ConfigMaps(namespace).Get(name, backendapi.GetOptionsInCache)
	if err != nil {
		log.Printf("Cannot find settings config map: %s", err.Error())
		return
	}
	// Check if anything has changed from the last time when function was executed.
	isDifferent = !reflect.DeepEqual(old, configMap.Data)
	return
}

func (sm *SettingsManager) setAuthSettings(authSett *api.AuthSettings) {
	sm.authSettings = authSett
}

func (sm *SettingsManager) restoreFromRaw() {
	var (
		err        error
		sett       *api.Settings
		devopsSett *api.DevopsSettings
	)
	for key, value := range sm.rawSettings {
		err = nil
		switch key {
		case api.GlobalSettingsKey:
			sett, err = api.Unmarshal(value)
			if sett != nil {
				sm.settings = sett
			}
		case api.SettingsDevopsDomainGlobalConfigKey:
			devopsSett, err = api.UnmarshalDevops(value)
			if devopsSett != nil {
				sm.devopsSettings = devopsSett
			}
		}
		if err != nil {
			log.Printf("Cannot unmarshal settings key %s with %s value: %s", key, value, err.Error())
		}
	}
}

func (sm *SettingsManager) restoreFromRawAuth() {
	authSett, err := api.AuthSettingsFromData(sm.authRawSettings)
	if err != nil {
		log.Printf("Cannot unmarshal auth settings value: %v, err: %s", sm.authRawSettings, err.Error())
	}
	if authSett != nil {
		sm.setAuthSettings(authSett)
	}
}

// restoreConfigMap restores settings config map using default global settings.
func (sm *SettingsManager) restoreConfigMap(client kubernetes.Interface) {
	restoredConfigMap, err := client.CoreV1().ConfigMaps(api.SettingsConfigMapNamespace).
		Create(api.GetDefaultSettingsConfigMap())
	if err != nil {
		log.Printf("Cannot restore settings config map: %s", err.Error())
	} else {
		sm.rawSettings = restoredConfigMap.Data
		sm.restoreFromRaw()
	}
}

// GetDevopsSettings implements SettingsManager interface. Check it for more information.
func (sm *SettingsManager) GetDevopsSettings(client kubernetes.Interface) *api.DevopsSettings {
	cm, _ := sm.load(client)
	var settings api.DevopsSettings
	if cm == nil {
		settings = api.GetDevopsDefaultSettings()
		return &settings
	}

	s := sm.devopsSettings
	if s == nil {
		settings = api.GetDevopsDefaultSettings()
		return &settings
	}

	// return *s
	return s
}

// GetDevopsSettings implements SettingsManager interface. Check it for more information.
func (sm *SettingsManager) GetDevopsSettingsFromToolCategory(client versioned.Interface, query *dataselect.Query) (*v1alpha1.SettingResp, error) {
	setting, err := client.DevopsV1alpha1().ToolCategories().Setting()

	if err != nil {
		log.Printf("GetDevopsSettingsFromToolCategory error is %#v", err)
		return nil, err
	}
	for i := range setting.ToolChains {
		itemCells := toCells(setting.ToolChains[i].Items)
		itemCells, _ = dataselect.GenericDataSelectWithFilter(itemCells, query)
		setting.ToolChains[i].Items = fromCells(itemCells)
	}
	// return *s
	return setting, err
}
