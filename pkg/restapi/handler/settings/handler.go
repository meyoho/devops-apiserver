// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package settings

import (
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/settings/api"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"net/http"
)

// SettingsHandler manages all endpoints related to settings management.
type SettingsHandler struct {
	manager SettingsManager
}

// Handler handler for ProjectManagement
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

type Processor interface{}

func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ProjectManagementBinding related APIs").ApiVersion("v1").Path("/api/v1/settings")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "projectmanagementbinding")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("devops").
					Filter(clientFilter.InsecureFilter).
					Doc(`List ProjectManagementBinding instances`).
					To(handler.handleSettingsDevopsGet).
					Writes(api.DevopsSettings{}).
					Returns(http.StatusOK, "OK", api.DevopsSettings{}),
			),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

func (h Handler) handleSettingsDevopsGet(request *restful.Request, response *restful.Response) {
	settingmanager := new(SettingsManager)
	client := localcontext.DevOpsClient(request.Request.Context())
	query := abcontext.Query(request.Request.Context())
	result, err := settingmanager.GetDevopsSettingsFromToolCategory(client, query)
	if err != nil {
		response.WriteHeaderAndEntity(http.StatusBadRequest, err)
	} else {
		response.WriteHeaderAndEntity(http.StatusOK, result)
	}
}
