package pipelinetemplate

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListPipelineTemplate list pipelinetemplate instances
func (p processor) ListPipelineTemplate(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace string) (data *PipelineTemplateList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list pipelinetemplate end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	pipelinetemplatelist, err := client.DevopsV1alpha1().PipelineTemplates(namespace).List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &PipelineTemplateList{
		Items:    make([]v1alpha1.PipelineTemplate, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSliceX(pipelinetemplatelist.Items, GetCategoryProperty(), GetDisplayZHNameProperty())
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToPipelineTemplateSlice(result)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// RetrievePipelineTemplate retrieve pipelinetemplate instance
func (p processor) RetrievePipelineTemplate(ctx context.Context, client versioned.Interface, namespace, name string) (data *v1alpha1.PipelineTemplate, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipelinetemplate end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().PipelineTemplates(namespace).Get(name, jenkins.GetOptionsInCache)
	data.Kind = v1alpha1.TypePipelineTemplate
	return
}

// PreviewPipelineTemplate perview a jenkinsfile

func (p processor) PreviewPipelineTemplate(ctx context.Context, client versioned.Interface, namespace, name string, options *PreviewOptions) (jenkinsfile string, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("preview pipelinetemplate jenkinsfile end", log.String("name", name), log.Err(err))
	}()

	var source *v1alpha1.PipelineSource
	if options != nil && options.Source != nil {
		source = options.Source
	}
	opts := &v1alpha1.JenkinsfilePreviewOptions{
		Source: source,
		Values: options.Values,
	}
	result, err := client.DevopsV1alpha1().PipelineTemplates(namespace).Preview(name, opts)
	return result.Jenkinsfile, err
}

func (p processor) ExportsPipelineTemplate(ctx context.Context, client versioned.Interface, namespace, name, taskName string) (data *v1alpha1.PipelineExportedVariables, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("get pipelinetemplate export end", log.String("name", name), log.Err(err))
	}()

	opts := &v1alpha1.ExportShowOptions{
		TaskName: taskName,
	}

	data, err = client.DevopsV1alpha1().PipelineTemplates(namespace).Exports(name, opts)
	return
}

// ConvertToPipelineTemplateSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToPipelineTemplateSlice(filtered []metav1.Object) (items []v1alpha1.PipelineTemplate) {
	items = make([]v1alpha1.PipelineTemplate, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.PipelineTemplate); ok {
			cm.Kind = common.ResourceKindPipelineTemplate
			items = append(items, *cm)
		}
	}
	return
}
