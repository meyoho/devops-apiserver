package pipelinetemplate_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestPipelinetemplate(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("pipelinetempalte.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/pipelinetemplate", []Reporter{junitReporter})
}
