package pipelinetemplate_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelinetemplate"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Pipelinetemplate", func() {

	var (
		processor                   pipelinetemplate.Processor
		ctx                         context.Context
		client                      *fake.Clientset
		query                       *dataselect.Query
		filterquery                 *dataselect.Query
		err                         error
		clusterpipelinetemplatelist *pipelinetemplate.PipelineTemplateList
		namespace                   string
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 1,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)

		filterquery = dataselect.NewDataSelectQuery(
			&dataselect.PaginationQuery{
				ItemsPerPage: 10,
				Page:         0,
			},
			defaultSortQuery,
			dataselect.NewFilterQuery([]string{"category", "Build"}),
		)
		client = fake.NewSimpleClientset()
		processor = pipelinetemplate.NewProcessor()
		namespace = "default"
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("get pipelinetempalte in ns default", func() {
		client = fake.NewSimpleClientset(
			GetPipelinetemplateList("testbinding", namespace),
		)
		clusterpipelinetemplatelist, err = processor.ListPipelineTemplate(ctx, client, query, namespace)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(clusterpipelinetemplatelist).ToNot(BeNil(), "should return a list")
		Expect(clusterpipelinetemplatelist.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(clusterpipelinetemplatelist.Items).To(HaveLen(1), "should have one items")
		Expect(clusterpipelinetemplatelist.Items[0].Name).To(Equal("testbinding"))

	})

	It("get filtered pipelinetempalte in ns default", func() {
		client = fake.NewSimpleClientset(
			GetPipelinetemplateList("testbinding", namespace),
		)
		clusterpipelinetemplatelist, err = processor.ListPipelineTemplate(ctx, client, filterquery, namespace)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(clusterpipelinetemplatelist).ToNot(BeNil(), "should return a list")
		Expect(clusterpipelinetemplatelist.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(clusterpipelinetemplatelist.Items).To(HaveLen(1), "should have one item")
		Expect(clusterpipelinetemplatelist.Items[0].Name).To(Equal("testbinding"))

	})

})

func GetPipelinetemplateList(name, namespace string) *v1alpha1.PipelineTemplateList {
	pipelinetempaltelist := &v1alpha1.PipelineTemplateList{
		Items: []v1alpha1.PipelineTemplate{
			{
				ObjectMeta: metav1.ObjectMeta{
					Namespace: namespace,
					Name:      name,
					Labels: map[string]string{
						"category": "Build",
					},
				},
				Spec: v1alpha1.PipelineTemplateSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Namespace: namespace,
					Name:      name + "1",
				},
				Spec: v1alpha1.PipelineTemplateSpec{},
			},
		},
	}
	return pipelinetempaltelist
}
