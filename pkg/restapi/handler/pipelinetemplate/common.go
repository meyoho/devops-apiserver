package pipelinetemplate

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
)

func GetDisplayZHNameProperty() dataselect.PropertyGetter {

	return dataselect.PropertyGetter{
		Name: "displayZhName",
		F: func(obj interface{}) dataselect.ComparableValue {
			template := obj.(*v1alpha1.PipelineTemplate)
			return dataselect.StdCaseInSensitiveComparableString(template.Annotations[common.AnnotationsKeyDisplayNameZh])
		},
	}
}

func GetCategoryProperty() dataselect.PropertyGetter {

	return dataselect.PropertyGetter{
		Name: "category",
		F: func(obj interface{}) dataselect.ComparableValue {
			template := obj.(*v1alpha1.PipelineTemplate)
			return dataselect.StdCaseInSensitiveComparableString(template.Labels[common.CategoryProperty])
		},
	}
}
