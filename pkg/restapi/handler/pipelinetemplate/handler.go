package pipelinetemplate

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for PipelineTemplate
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListPipelineTemplate(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace string) (*PipelineTemplateList, error)
	RetrievePipelineTemplate(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.PipelineTemplate, error)
	PreviewPipelineTemplate(ctx context.Context, client versioned.Interface, namespace, name string, options *PreviewOptions) (string, error)
	ExportsPipelineTemplate(ctx context.Context, client versioned.Interface, namespace, name, taskName string) (*v1alpha1.PipelineExportedVariables, error)
}

// New builder method for pipelinetemplate.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("PipelineTemplate related APIs").ApiVersion("v1").Path("/api/v1/pipelinetemplate")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "pipelinetemplate")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List PipelineTemplate instance`).
					To(handler.ListPipelineTemplate).
					Writes(v1alpha1.PipelineTemplateList{}).
					Returns(http.StatusOK, "OK", v1alpha1.PipelineTemplateList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve PipelineTemplate instance").
				Param(ws.PathParameter("name", "name of the PipelineTemplate")).
				Param(ws.PathParameter("namespace", "namespace of the PipelineTemplate")).
				To(handler.RetrievePipelineTemplate).
				Returns(http.StatusOK, "Retrieve PipelineTemplate instance", v1alpha1.PipelineTemplate{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}/{name}/preview").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("preview a pipelinetemplate").
				Param(ws.PathParameter("name", "name of the PipelineTemplate")).
				Param(ws.PathParameter("namespace", "name of the PipelineTemplate")).
				To(handler.PreviewPipelineTemplate).
				Returns(http.StatusOK, "preview PipelineTemplate instance", ""),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/exports").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get a pipelinetemplate exports").
				Param(ws.PathParameter("name", "name of the PipelineTemplate")).
				Param(ws.PathParameter("namespace", "name of the PipelineTemplate")).
				To(handler.ExportsPipelineTemplate).
				Returns(http.StatusOK, "get  PipelineTemplate export", PipelineExportedVariables{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListPipelineTemplate list pipelinetemplate instances
func (h Handler) ListPipelineTemplate(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	namespace := req.PathParameter("namespace")
	list, err := h.Processor.ListPipelineTemplate(req.Request.Context(), client, query, namespace)
	h.WriteResponse(list, err, req, res)
}

//RetrievePipelineTemplate Retrieve PipelineTemplate instance
func (h Handler) RetrievePipelineTemplate(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrievePipelineTemplate(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//PreviewPipelineTemplate preview a pipelinetemplate
func (h Handler) PreviewPipelineTemplate(req *restful.Request, res *restful.Response) {
	options := new(PreviewOptions)
	if err := req.ReadEntity(options); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	name := req.PathParameter("name")
	namespace := req.PathParameter("namespace")

	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.PreviewPipelineTemplate(req.Request.Context(), client, namespace, name, options)
	h.WriteResponse(data, err, req, res)
}

func (h Handler) ExportsPipelineTemplate(req *restful.Request, res *restful.Response) {
	name := req.PathParameter("name")
	namespace := req.PathParameter("namespace")

	taskName := req.QueryParameter("taskName")
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.ExportsPipelineTemplate(req.Request.Context(), client, namespace, name, taskName)
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
