package imagerepository

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	"sync"
)

// ImageRepositoryList contains a list of ImageRepository in the cluster.
type ImageRepositoryList struct {
	ListMeta jenkins.ListMeta `json:"listMeta"`

	// Unordered list of ImageRepository.
	Items []v1alpha1.ImageRepository `json:"imagerepositories"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// RetryRequest request a retry for pipeline
type RetryRequest struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`

	// empty for now
}

// LogDetails log details
type LogDetails struct {
	*v1alpha1.PipelineLog
}

// TaskDetails jenkins step details
type TaskDetails struct {
	*v1alpha1.PipelineTask
}

type VMap struct {
	vMap    map[string]map[string]string
	rwMutex sync.RWMutex
}

func (v *VMap) PushMainMap(key string, value map[string]string) {
	v.rwMutex.Lock()
	defer v.rwMutex.Unlock()
	v.vMap[key] = value

}

func (v *VMap) PushSubMap(key string, subKey string, subValue string) {
	v.rwMutex.Lock()
	defer v.rwMutex.Unlock()
	if _, ok := v.vMap[key]; !ok {
		v.vMap[key] = map[string]string{subKey: subValue}
	} else {
		v.vMap[key][subKey] = subValue
	}

}

func (v *VMap) GetMainMap(key string) (map[string]string, bool) {
	v.rwMutex.RLock()
	defer v.rwMutex.RUnlock()
	value, ok := v.vMap[key]

	return value, ok
}

func (v *VMap) GetSubMap(key string, subKey string) (string, bool) {
	v.rwMutex.RLock()
	defer v.rwMutex.RUnlock()
	value, ok := v.vMap[key][subKey]

	return value, ok
}

type projectMap struct {
	vMap    map[string][]string
	rwMutex sync.RWMutex
}

func (v *projectMap) PushMainMap(key string, value []string) {
	v.rwMutex.Lock()
	defer v.rwMutex.Unlock()
	v.vMap[key] = value

}

func (v *projectMap) GetMainMap(key string) ([]string, bool) {
	v.rwMutex.RLock()
	defer v.rwMutex.RUnlock()
	value, ok := v.vMap[key]

	return value, ok
}
