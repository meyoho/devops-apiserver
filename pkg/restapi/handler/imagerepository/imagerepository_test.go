package imagerepository_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/imagerepository"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Imageregistry", func() {

	var (
		processor           imagerepository.Processor
		ctx                 context.Context
		client              *fake.Clientset
		query               *dataselect.Query
		err                 error
		imagerepositorylist *imagerepository.ImageRepositoryList
		namespace           *common.NamespaceQuery
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 10,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		client = fake.NewSimpleClientset()
		processor = imagerepository.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("get imageregistry in ns default", func() {
		client = fake.NewSimpleClientset(
			GetImageregistryList("imageregistry"),
		)
		namespace = common.NewNamespaceQuery([]string{"default"})
		imagerepositorylist, err = processor.ListImageRepository(ctx, client, query, namespace)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(imagerepositorylist).ToNot(BeNil(), "should return a list")
		Expect(imagerepositorylist.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(imagerepositorylist.Items).To(HaveLen(1), "should have one items")
		Expect(imagerepositorylist.Items[0].Name).To(Equal("imageregistry"))

	})

})

func GetImageregistryList(name string) *v1alpha1.ImageRepositoryList {
	imagerepositorylist := &v1alpha1.ImageRepositoryList{
		Items: []v1alpha1.ImageRepository{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: "default",
				},
				Spec: v1alpha1.ImageRepositorySpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: "alauda-system",
				},
				Spec: v1alpha1.ImageRepositorySpec{},
			},
		},
	}
	return imagerepositorylist
}
