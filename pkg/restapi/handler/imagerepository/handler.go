package imagerepository

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for ImageRepository
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListImageRepository(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*ImageRepositoryList, error)
	RetrieveImageRepository(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.ImageRepository, error)
	GetRepositoryTags(ctx context.Context, client versioned.Interface, namespace, name, sortBy, sortMode string) (*v1alpha1.ImageTagResult, error)
	ScanImageRepository(ctx context.Context, client versioned.Interface, namespace, name, tag string) (*v1alpha1.ImageResult, error)
	GetVulnerability(ctx context.Context, client versioned.Interface, namespace, name, tag string) (*v1alpha1.VulnerabilityList, error)
	GetImageRepositoryProjectList(ctx context.Context, client versioned.Interface, namespace *common.NamespaceQuery, query *dataselect.Query) (*ImageRepositoryList, error)
}

// New builder method for imagerepository.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ImageRepository related APIs").ApiVersion("v1").Path("/api/v1")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "imagerepository")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/imagerepository/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List ImageRepository instance`).
					Param(ws.PathParameter("namespace", "namespace of the ImageRepository")).
					To(handler.ListImageRepository).
					Writes(ImageRepositoryList{}).
					Returns(http.StatusOK, "OK", ImageRepositoryList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/imagerepository/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve ImageRepository instance").
				Param(ws.PathParameter("name", "name of the ImageRepository")).
				To(handler.RetrieveImageRepository).
				Returns(http.StatusOK, "Retrieve ImageRepository instance", v1alpha1.ImageRepository{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/imagerepository/{namespace}/{name}/tags").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("retrieve  imagerepository tags").
				Param(ws.PathParameter("namespace", "namespace of the ImageRepository")).
				Param(ws.PathParameter("name", "name of the ImageRepository")).
				Param(restful.QueryParameter("sortBy", "sort option. The choices are creationTime")).
				Param(restful.QueryParameter("sortMode", "sort option. The choices are desc or asc")).
				To(handler.GetRepositoryTags).
				Returns(http.StatusOK, "Retrieve ImageRepository instance", v1alpha1.ImageRepository{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/imagerepository/{namespace}/{name}/security").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("scan image ").
				Param(restful.QueryParameter("tag", "Scan image tag name")).
				To(handler.ScanImageRepository).
				Returns(http.StatusOK, "create ImageRepository scan", v1alpha1.ImageResult{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/imagerepository/{namespace}/{name}/security").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("scan image ").
				Param(restful.QueryParameter("tag", "Get image vulnerability tag name")).
				To(handler.GetVulnerability).
				Returns(http.StatusOK, "Get Image Vulnerability Successful", v1alpha1.VulnerabilityList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/imagerepositoryproject/{namespace}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get namespaced imagerepository list").
				To(handler.GetImageRepositoryProjectList).
				Returns(http.StatusOK, "get namespaced imagerepository list", ImageRepositoryList{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListImageRepository list imagerepository instances
func (h Handler) ListImageRepository(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListImageRepository(req.Request.Context(), client, query, common.ParseNamespacePathParameter(req))
	h.WriteResponse(list, err, req, res)
}

//GetRepositoryTags Retrieve ImageRepository instance
func (h Handler) RetrieveImageRepository(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrieveImageRepository(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//GetRepositoryTags Retrieve ImageRepository instance
func (h Handler) GetVulnerability(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())

	data, err := h.Processor.GetVulnerability(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"), req.QueryParameter("tag"))
	h.WriteResponse(data, err, req, res)
}

// GetRepositoryTags get imagerepository tags
func (h Handler) GetRepositoryTags(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	sortBy := req.QueryParameter("sortBy")
	sortMode := req.QueryParameter("sortMode")
	data, err := h.Processor.GetRepositoryTags(req.Request.Context(), client, namespace, name, sortBy, sortMode)

	h.WriteResponse(data, err, req, res)
}

//ScanImageRepository create ImageRepository scan
func (h Handler) ScanImageRepository(req *restful.Request, res *restful.Response) {
	log.Errorf("can I be herer")
	client := localcontext.DevOpsClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	tag := req.QueryParameter("tag")
	log.Errorf("the tag is %v  and namespace is %v, namei si %v", tag, namespace, name)
	data, err := h.Processor.ScanImageRepository(req.Request.Context(), client, namespace, name, tag)
	h.WriteResponse(data, err, req, res)
}

//GetImageRepositoryProjectList
func (h Handler) GetImageRepositoryProjectList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())

	dataSelect := common.ParseDataSelectPathParameter(req)
	namespace := common.ParseNamespacePathParameter(req)

	data, err := h.Processor.GetImageRepositoryProjectList(req.Request.Context(), client, namespace, dataSelect)

	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
