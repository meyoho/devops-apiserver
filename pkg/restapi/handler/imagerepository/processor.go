package imagerepository

import (
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"context"
	"strings"
	"sync"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/imageregistry"
	"alauda.io/devops-apiserver/pkg/restapi/handler/imageregistrybinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListImageRepository list imagerepository instances
func (p processor) ListImageRepository(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *ImageRepositoryList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list imagerepository end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	imagerepositorylist, err := client.DevopsV1alpha1().ImageRepositories(namespace.ToRequestParam()).List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &ImageRepositoryList{
		Items:    make([]v1alpha1.ImageRepository, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}
	provider := localcontext.ExtraConfig(ctx).AnnotationProvider
	// filter using standard filters
	itemCells := ToCells(imagerepositorylist.Items, provider)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	data.Items = FromCells(itemCells)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// RetrieveImageRepository retrieve imagerepository instance
func (p processor) RetrieveImageRepository(ctx context.Context, client versioned.Interface, namespace, name string) (data *v1alpha1.ImageRepository, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get imagerepository end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().ImageRepositories(namespace).Get(name, jenkins.GetOptionsInCache)
	return
}

// ScanImageRepository abort a imagerepository
func (p processor) ScanImageRepository(ctx context.Context, client versioned.Interface, namespace, name, tag string) (imageresult *v1alpha1.ImageResult, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("scan imagerepository end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	scanOpts := &v1alpha1.ImageScanOptions{
		Tag: tag,
	}
	imageresult, err = client.DevopsV1alpha1().ImageRepositories(namespace).ScanImage(name, scanOpts)
	return
}

// GetVulnerability get Vulnerability
func (p processor) GetVulnerability(ctx context.Context, client versioned.Interface, namespace, name, tag string) (vulnerablitylist *v1alpha1.VulnerabilityList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("scan imagerepository end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	scanOpts := &v1alpha1.ImageScanOptions{
		Tag: tag,
	}
	vulnerablitylist, err = client.DevopsV1alpha1().ImageRepositories(namespace).GetVulnerability(name, scanOpts)
	return
}

//// GetRepositoryTags  get Repository Tags
func (p processor) GetRepositoryTags(ctx context.Context, client versioned.Interface, namespace, name, sortBy, sortMode string) (ImageTag *v1alpha1.ImageTagResult, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get imagerepository end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	opts := &v1alpha1.ImageTagOptions{
		SortBy:   sortBy,
		SortMode: sortMode,
	}

	ImageTag, err = client.DevopsV1alpha1().ImageRepositories(namespace).GetImageTags(name, opts)
	if err != nil {
		logger.Error("Error fetching tags: ", log.Err(err))
		return nil, err
	}
	return ImageTag, nil

}

//GetImageRepositoryProjectList
func (p processor) GetImageRepositoryProjectList(ctx context.Context, devopsClient versioned.Interface, namespace *common.NamespaceQuery, dataSelect *dataselect.Query) (result *ImageRepositoryList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("scan imagerepository end", log.Any("namespace", namespace), log.Err(err))
	}()

	result, err = p.ListImageRepository(ctx, devopsClient, dataSelect, namespace)
	if err != nil {
		return
	}

	vMap := VMap{vMap: make(map[string]map[string]string), rwMutex: sync.RWMutex{}}

	projectMap := projectMap{vMap: make(map[string][]string), rwMutex: sync.RWMutex{}}

	//concurrent

	if result == nil || result.Items == nil {
		return
	}

	bindingList, err := imageregistrybinding.NewProcessor().ListImageRegistryBinding(ctx, devopsClient, dataSelect, namespace)
	if err != nil {
		return
	}
	wg := sync.WaitGroup{}
	wg.Add(len(bindingList.Items))

	for _, repo := range result.Items {

		vMap.PushSubMap(repo.Spec.ImageRegistry.Name, strings.Split(repo.Spec.Image, "/")[0], strings.Split(repo.Spec.Image, "/")[0])

	}

	for _, binding := range bindingList.Items {

		//create a goroutine for registry,one registry only create goroutine once

		go func(binding v1alpha1.ImageRegistryBinding) {
			defer wg.Done()

			if _, ok := vMap.GetMainMap(binding.Spec.ImageRegistry.Name); !ok {

				vMap.PushMainMap(binding.Spec.ImageRegistry.Name, make(map[string]string))
			}

			projectListFromBinding := binding.Spec.RepoInfo.Repositories
			if len(projectListFromBinding) == 1 && projectListFromBinding[0] == "/" {

				//is from cache?
				if projectListFromMap, ok := projectMap.GetMainMap(binding.Spec.ImageRegistry.Name); !ok {
					projectListFromBinding = make([]string, 0)
					projectDataListInLoop, err := imageregistrybinding.GetProjectList(devopsClient, binding.Spec.ImageRegistry.Name, binding.Spec.Secret.Name, binding.Spec.Secret.Namespace)
					if err != nil {
						return
					}
					for _, p := range projectDataListInLoop.Items {
						projectListFromBinding = append(projectListFromBinding, p.ObjectMeta.Name)
					}
					projectMap.PushMainMap(binding.Spec.ImageRegistry.Name, projectListFromBinding)
				} else {
					projectListFromBinding = projectListFromMap
				}

			}

			registry, err := imageregistry.NewProcessor().RetrieveImageRegistry(ctx, devopsClient, binding.Spec.ImageRegistry.Name)
			if err != nil {
				return
			}

			//when vMap is completed,use registry`s project reduce vMap
			for _, projectInLoop := range projectListFromBinding {
				projectName := strings.Split(projectInLoop, "/")[0]

				if _, ok := vMap.GetSubMap(binding.Spec.ImageRegistry.Name, projectName); !ok {

					repo := v1alpha1.ImageRepository{
						ObjectMeta: metav1.ObjectMeta{
							Name:              "project-" + projectName,
							Namespace:         binding.ObjectMeta.Namespace,
							CreationTimestamp: binding.ObjectMeta.CreationTimestamp,
							Annotations: map[string]string{
								"imageRegistryEndpoint": strings.Split(registry.Spec.HTTP.Host, "//")[len(strings.Split(registry.Spec.HTTP.Host, "//"))-1],
								"imageRegistryType":     registry.Spec.Type.String(),
								"imageRepositoryLink":   "",
								"secretName":            binding.Spec.Secret.Name,
								"secretNamespace":       binding.Spec.Secret.Namespace,
							},
						},
						Spec: v1alpha1.ImageRepositorySpec{
							Image: projectName,
						}, Status: v1alpha1.ImageRepositoryStatus{
							ServiceStatus: v1alpha1.ServiceStatus{
								Phase:   v1alpha1.ServiceStatusPhase("Ready"),
								Message: "",
							},
							Tags: []v1alpha1.ImageTag{},
						},
					}
					result.Items = append(result.Items, repo)

					result.ListMeta.TotalItems += 1

					vMap.PushSubMap(binding.Spec.ImageRegistry.Name, projectName, projectName)

				}
			}

		}(binding)

	}
	wg.Wait()

	return
}

// ConvertToImageRepositorySlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToImageRepositorySlice(filtered []metav1.Object) (items []v1alpha1.ImageRepository) {
	items = make([]v1alpha1.ImageRepository, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.ImageRepository); ok {
			cm.Kind = v1alpha1.ResourceKindImageRegistry
			items = append(items, *cm)
		}
	}
	return
}
