package imagerepository

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type imagerepositoryCell struct {
	imagerepo *v1alpha1.ImageRepository
	devops.AnnotationProvider
}

func defalutMetav1Time(before, after *metav1.Time) *metav1.Time {
	if before != nil {
		return before
	}
	return after
}

func (self imagerepositoryCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case common.NameProperty:
		return dataselect.StdComparableString(self.imagerepo.ObjectMeta.Name)
	case common.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.imagerepo.ObjectMeta.CreationTimestamp.Time)
	case common.LatestCommitAt:
		// Discard LatestCommit, Use LastUpdate
		target := defalutMetav1Time(self.imagerepo.Status.LatestTag.UpdatedAt, self.imagerepo.Status.LatestTag.CreatedAt)
		target = defalutMetav1Time(target, &metav1.Time{Time: time.Unix(0, 0)})
		return dataselect.StdComparableTime(target.Time)
	case common.NamespaceProperty:
		return dataselect.StdComparableString(self.imagerepo.ObjectMeta.Namespace)
	case common.DisplayNameProperty:
		name := self.imagerepo.ObjectMeta.Name
		if len(self.imagerepo.ObjectMeta.Annotations) != 0 && self.imagerepo.ObjectMeta.Annotations[self.AnnotationsKeyDisplayName()] != "" {
			name = self.imagerepo.ObjectMeta.Annotations[self.AnnotationsKeyDisplayName()]
		}
		return dataselect.StdLowerComparableString(name)
	case common.LabelProperty:
		if len(self.imagerepo.ObjectMeta.Labels) > 0 {
			values := []string{}
			for k, v := range self.imagerepo.ObjectMeta.Labels {
				values = append(values, k+":"+v)
			}
			return dataselect.StdComparableLabel(strings.Join(values, ","))
		}
	}
	// if name is not supported then just return a constant dummy value, sort will have no effect.
	return nil
}

func ToCells(std []v1alpha1.ImageRepository, provider devops.AnnotationProvider) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = imagerepositoryCell{&std[i], provider}
	}
	return cells
}

func FromCells(cells []dataselect.DataCell) []v1alpha1.ImageRepository {
	std := make([]v1alpha1.ImageRepository, len(cells))

	for i := range std {
		std[i] = *cells[i].(imagerepositoryCell).imagerepo.DeepCopy()
		std[i].Kind = v1alpha1.ResourceKindImageRepository
	}
	return std
}
