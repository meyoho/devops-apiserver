package artifactregistrybinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/api"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ArtifactRegistryBindingList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	Items []v1alpha1.ArtifactRegistryBinding `json:"artifactregistrybindings"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type ArtifactRegistryBinding struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	Spec   v1alpha1.ArtifactRegistryBindingSpec `json:"spec"`
	Status v1alpha1.ServiceStatus               `json:"status"`
}

func ConvertToArbSlice(filtered []metav1.Object, provider devops.AnnotationProvider) (items []v1alpha1.ArtifactRegistryBinding) {
	items = make([]v1alpha1.ArtifactRegistryBinding, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.ArtifactRegistryBinding); ok {
			cm.ObjectMeta.Annotations[provider.AnnotationsKeyToolType()] = "artifactRegistry"
			cm.Kind = "artifactregistrybinding"
			items = append(items, *cm)
		}
	}
	return
}
