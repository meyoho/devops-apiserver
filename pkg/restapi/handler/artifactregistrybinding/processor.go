package artifactregistrybinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

func (p processor) GetArtifactRegistryBindingList(ctx context.Context, client devopsclient.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *ArtifactRegistryBindingList, err error) {

	provider := localcontext.ExtraConfig(ctx).AnnotationProvider

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list artifactregistrybinding end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	// fetch data from list
	artifactRegistryBindinglist, err := client.DevopsV1alpha1().ArtifactRegistryBindings(namespace.ToRequestParam()).List(v1alpha1.ListOptions())

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &ArtifactRegistryBindingList{
		Items:    make([]v1alpha1.ArtifactRegistryBinding, 0),
		ListMeta: api.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(artifactRegistryBindinglist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToArbSlice(result, provider)
	data.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

func (p processor) GetArtifactRegistryBinding(ctx context.Context, client devopsclient.Interface, name, namespace string) (crs *v1alpha1.ArtifactRegistryBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("get artifactregistrybinding detail end", log.Any("list", crs), log.Err(err))
	}()

	crs, err = client.DevopsV1alpha1().ArtifactRegistryBindings(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	return crs, nil
}

func (p processor) DeleteArtifactRegistryBinding(ctx context.Context, client devopsclient.Interface, name, namespace string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("delete  artifactregistrybinding end", log.Err(err))
	}()
	return client.DevopsV1alpha1().ArtifactRegistryBindings(namespace).Delete(name, &v1.DeleteOptions{})
}

func (p processor) CreateArtifactRegistryBinding(ctx context.Context, client devopsclient.Interface, arm *v1alpha1.ArtifactRegistryBinding) (data *v1alpha1.ArtifactRegistryBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create  artifactregistrybinding end", log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().ArtifactRegistryBindings(arm.Namespace).Create(arm)
	return
}

func (p processor) UpdateArtifactRegistryBinding(ctx context.Context, client devopsclient.Interface, newARM *v1alpha1.ArtifactRegistryBinding, name string) (data *v1alpha1.ArtifactRegistryBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("update artifactregistrybinding end", log.Err(err))
	}()

	oldARM, err := client.DevopsV1alpha1().ArtifactRegistryBindings(newARM.Namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}

	oldARM.SetAnnotations(newARM.GetAnnotations())
	oldARM.Spec = newARM.Spec

	oldARM, err = client.DevopsV1alpha1().ArtifactRegistryBindings(newARM.Namespace).Update(oldARM)
	if err != nil {
		return nil, err
	}
	return oldARM, nil

}
