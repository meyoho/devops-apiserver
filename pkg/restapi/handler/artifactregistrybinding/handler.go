package artifactregistrybinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
	"net/url"
)

// Handler handler for artifactregistrybinding
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	GetArtifactRegistryBindingList(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *ArtifactRegistryBindingList, err error)
	GetArtifactRegistryBinding(ctx context.Context, client versioned.Interface, name, namespace string) (*v1alpha1.ArtifactRegistryBinding, error)
	DeleteArtifactRegistryBinding(ctx context.Context, client versioned.Interface, name, namespace string) error
	CreateArtifactRegistryBinding(ctx context.Context, client versioned.Interface, arm *v1alpha1.ArtifactRegistryBinding) (*v1alpha1.ArtifactRegistryBinding, error)
	UpdateArtifactRegistryBinding(ctx context.Context, client versioned.Interface, newARM *v1alpha1.ArtifactRegistryBinding, name string) (*v1alpha1.ArtifactRegistryBinding, error)
}

// New builder method for artifactregistrybinding.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ArtifactRegistryBinding related APIs").ApiVersion("v1").Path("/api/v1/artifactregistrybindings")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "ArtifactRegistryBinding")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`Get ArtifactRegistryBindingList`).
					To(handler.GetArtifactRegistryBindingList).
					Returns(http.StatusOK, "OK", ArtifactRegistryBindingList{}),
			),
		),
	)

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`Get ArtifactRegistryBindingList`).
					To(handler.GetArtifactRegistryBindingList).
					Returns(http.StatusOK, "OK", ArtifactRegistryBindingList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ArtifactRegistryBinding instance").
				To(handler.GetArtifactRegistryBindingDetail).
				Returns(http.StatusOK, "get ArtifactRegistryBinding Instance", v1alpha1.ArtifactRegistryBinding{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete ArtifactRegistryBinding instance").
				Param(ws.PathParameter("name", "name of the ArtifactRegistryBinding")).
				To(handler.DeleteArtifactRegistryBinding).
				Returns(http.StatusOK, "Delete ArtifactRegistryBinding Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create ArtifactRegistryBindingManager instance").
				To(handler.CreateArtifactRegistryBinding).
				Returns(http.StatusOK, "Retrieve CodeQualityProject instance", v1alpha1.ArtifactRegistryBinding{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create ArtifactRegistryBindingManager instance").
				To(handler.CreateArtifactRegistryBinding).
				Returns(http.StatusOK, "Retrieve CodeQualityProject instance", v1alpha1.ArtifactRegistryBinding{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update ArtifactRegistryBinding instance").
				Param(restful.PathParameter("name", "ArtifactRegistryBinding name to filter scope")).
				Param(restful.PathParameter("namespace", "ArtifactRegistryBinding namespace to filter scope")).
				To(handler.UpdateArtifactRegistryBinding).
				Returns(http.StatusOK, "Update ArtifactRegistryBinding instance", v1alpha1.ArtifactRegistryBinding{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetArtifactRegistryBindingList list artifactregistry instances
func (h Handler) GetArtifactRegistryBindingList(req *restful.Request, res *restful.Response) {

	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	imageregistrybinding, err := h.Processor.GetArtifactRegistryBindingList(req.Request.Context(), client, query, common.ParseNamespacePathParameter(req))
	common.WriteResponseBySort(imageregistrybinding, err, req, res, h)
}

//GetArtifactRegistryBindingDetail Delete CodeQualityProject instance
func (h Handler) GetArtifactRegistryBindingDetail(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	name := req.PathParameter("name")
	namespace := req.PathParameter("namespace")

	result, err := h.Processor.GetArtifactRegistryBinding(req.Request.Context(), devopsClient, name, namespace)
	h.WriteResponse(result, err, req, res)
}

//RetrieveCodeQualityProject Retrieve CodeQualityProject instance
func (h Handler) DeleteArtifactRegistryBinding(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())

	name := req.PathParameter("name")
	namespace := req.PathParameter("namespace")

	err := h.Processor.DeleteArtifactRegistryBinding(req.Request.Context(), devopsClient, name, namespace)
	h.WriteResponse(nil, err, req, res)
}

//CreateArtifactRegistryBinding create CodeQualityProject instance
func (h Handler) CreateArtifactRegistryBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	spec := new(v1alpha1.ArtifactRegistryBinding)

	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	redirectURL := req.QueryParameter("redirectUrl")
	if _, err := url.Parse(redirectURL); err != nil {
		redirectURL = ""
	}

	if spec.GetSecretName() != "" {
		opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
			SecretName:  spec.GetSecretName(),
			Namespace:   spec.GetSecretNamespace(),
			RedirectURL: redirectURL,
		}
		authorizeResponse, err := client.DevopsV1alpha1().ArtifactRegistries().Authorize(spec.Spec.ArtifactRegistry.Name, &opts)
		if err != nil {
			log.Errorf("authorizeResponse: %v", log.Any("authorizeResponse", authorizeResponse))
			h.WriteResponse(nil, err, req, res)
			return
		}

		if authorizeResponse != nil && authorizeResponse.AuthorizeUrl != "" {
			res.WriteHeaderAndEntity(http.StatusPreconditionRequired, authorizeResponse)
			return
		}
	}
	data, err := h.Processor.CreateArtifactRegistryBinding(req.Request.Context(), client, spec)
	h.WriteResponse(data, err, req, res)
}

//UpdateJenkins update CodeQualityProject instance
func (h Handler) UpdateArtifactRegistryBinding(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	name := req.PathParameter("name")
	spec := new(v1alpha1.ArtifactRegistryBinding)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	redirectURL := req.QueryParameter("redirectUrl")
	if _, err := url.Parse(redirectURL); err != nil {
		redirectURL = ""
	}
	if spec.GetSecretName() != "" {
		opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
			SecretName:  spec.GetSecretName(),
			Namespace:   spec.GetSecretNamespace(),
			RedirectURL: redirectURL,
		}
		authorizeResponse, err := devopsClient.DevopsV1alpha1().ArtifactRegistries().Authorize(spec.Spec.ArtifactRegistry.Name, &opts)
		if err != nil {
			log.Errorf("authorizeResponse: %v", log.Any("authorizeResponse", authorizeResponse))
			h.WriteResponse(nil, err, req, res)
			return
		}

		if authorizeResponse != nil && authorizeResponse.AuthorizeUrl != "" {
			res.WriteHeaderAndEntity(http.StatusPreconditionRequired, authorizeResponse)
			return
		}
	}
	data, err := h.Processor.UpdateArtifactRegistryBinding(req.Request.Context(), devopsClient, spec, name)
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
