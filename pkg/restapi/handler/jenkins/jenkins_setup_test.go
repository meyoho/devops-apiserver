package jenkins

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestJenkins(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("jenkins.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/jenkins", []Reporter{junitReporter})
}
