package jenkins_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"context"
	"fmt"

	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Processor.ListJenkins", func() {
	var (
		processor jenkins.Processor
		ctx       context.Context
		client    *fake.Clientset
		query     *dataselect.Query

		list            *jenkins.JenkinsList
		err             error
		jenkinsinstance *v1alpha1.Jenkins
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		query = dataselect.NoDataSelect
		client = fake.NewSimpleClientset()
		processor = jenkins.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
		list, err = processor.ListJenkins(ctx, client, query)
	})

	It("should return something", func() {
		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Jenkins).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Jenkins).To(HaveLen(0), "should not have any items")
	})
	// TODO: Add a context that can return data, with a query that can filter data

	It("should retrieve a jenkins", func() {
		client = fake.NewSimpleClientset(
			GetJenkins("test", "www.baidu.com"),
		)
		jenkinsinstance, err = processor.RetrieveJenkins(ctx, client, "test")
		Expect(err).To(BeNil())
		Expect(jenkinsinstance.Spec.HTTP.Host).To(Equal("www.baidu.com"))

	})

	It("should create a jenkins", func() {
		client = fake.NewSimpleClientset(
			&v1alpha1.Jenkins{
				Spec: v1alpha1.JenkinsSpec{
					ToolSpec: v1alpha1.ToolSpec{
						HTTP: v1alpha1.HostPort{
							Host: "www.baidu.com",
						},
					},
				},
			},
		)
		jenkinsinstance, err = processor.CreateJenkins(ctx, client,
			GetJenkins("testcreate", "www.baidu.com"),
		)
		Expect(err).To(BeNil())
		Expect(jenkinsinstance.Spec.HTTP.Host).To(Equal("www.baidu.com"))

	})

	It("should update a jenkins", func() {
		client = fake.NewSimpleClientset(
			GetJenkins("testupdate", "www.baidu.com"),
		)
		jenkinsinstance, err = processor.UpdateJenkins(ctx, client,
			GetJenkins("testupdate", "www.newbaidu.com"), "testupdate")
		Expect(err).To(BeNil())
		Expect(jenkinsinstance.Spec.HTTP.Host).To(Equal("www.newbaidu.com"))

	})

	It("should delete a jenkins", func() {
		err = processor.DeleteJenkins(ctx, client, "testupdate")
		Expect(err).ToNot(BeNil())
		Expect(err.Error()).To(Equal(fmt.Sprintf("jenkinses.devops.alauda.io \"%s\" not found", "testupdate")))
	})

})

func GetJenkins(name, host string) *v1alpha1.Jenkins {
	jenkins := &v1alpha1.Jenkins{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
			Annotations: map[string]string{
				"user": "zpyu",
			},
		},
		Spec: v1alpha1.JenkinsSpec{
			ToolSpec: v1alpha1.ToolSpec{
				HTTP: v1alpha1.HostPort{
					Host: host,
				},
			},
		},
	}
	return jenkins
}
