package jenkins

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for Jenkins
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListJenkins(ctx context.Context, client versioned.Interface, query *dataselect.Query) (*JenkinsList, error)
	DeleteJenkins(ctx context.Context, client versioned.Interface, name string) error
	RetrieveJenkins(ctx context.Context, client versioned.Interface, name string) (*v1alpha1.Jenkins, error)
	CreateJenkins(ctx context.Context, client versioned.Interface, jenkins *v1alpha1.Jenkins) (*v1alpha1.Jenkins, error)
	UpdateJenkins(ctx context.Context, client versioned.Interface, jenkins *v1alpha1.Jenkins, name string) (*v1alpha1.Jenkins, error)
}

// New builder method for jenkins.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("Jenkins related APIs").ApiVersion("v1").Path("/api/v1/jenkinses")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "jenkins")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List Jenkins instances`).
					To(handler.ListJenkins).
					Writes(v1alpha1.JenkinsList{}).
					Returns(http.StatusOK, "OK", JenkinsList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create Jenkins instance").
				To(handler.CreateJenkins).
				Returns(http.StatusOK, "Create Jenkins instance", v1alpha1.Jenkins{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete Jenkins instance").
				Param(ws.PathParameter("name", "name of the jenkins")).
				To(handler.DeleteJenkins).
				Returns(http.StatusOK, "Delete Jenkins Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve Jenkins instance").
				Param(ws.PathParameter("name", "name of the jenkins")).
				To(handler.RetrieveJenkins).
				Returns(http.StatusOK, "Retrieve Jenkins instance", v1alpha1.Jenkins{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update Jenkins instance").
				Param(ws.PathParameter("name", "name of the jenkins")).
				To(handler.UpdateJenkins).
				Returns(http.StatusOK, "Update Jenkins instance", v1alpha1.Jenkins{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListJenkins list jenkins instances
func (h Handler) ListJenkins(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListJenkins(req.Request.Context(), client, query)
	h.WriteResponse(list, err, req, res)
}

//DeleteJenkins Delete jenkins instance
func (h Handler) DeleteJenkins(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteJenkins(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//RetrieveJenkins Retrieve jenkins instance
func (h Handler) RetrieveJenkins(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrieveJenkins(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//CreateJenkins create jenkins instance
func (h Handler) CreateJenkins(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newJenkins := new(v1alpha1.Jenkins)

	if err := req.ReadEntity(newJenkins); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.CreateJenkins(req.Request.Context(), client, newJenkins)
	h.WriteResponse(data, err, req, res)
}

//UpdateJenkins update jenkins instance
func (h Handler) UpdateJenkins(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newJenkins := new(v1alpha1.Jenkins)

	if err := req.ReadEntity(newJenkins); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.UpdateJenkins(req.Request.Context(), client, newJenkins, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
