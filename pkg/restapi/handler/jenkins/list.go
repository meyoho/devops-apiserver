package jenkins

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// JenkinsList contains a list of jenkins in the cluster.
type JenkinsList struct {
	ListMeta ListMeta `json:"listMeta"`

	// Unordered list of Jenkins.
	Jenkins []v1alpha1.Jenkins `json:"jenkins"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// ListMeta describes list of objects, i.e. holds information about pagination options set for the list.
type ListMeta struct {
	// Total number of items on the list. Used for pagination.
	TotalItems int `json:"totalItems"`
}

// GetOptionsInCache is a get options used to get one resource in cache.
var GetOptionsInCache = metav1.GetOptions{
	ResourceVersion: "0",
}
