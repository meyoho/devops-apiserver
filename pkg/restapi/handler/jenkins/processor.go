package jenkins

import (
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"context"
	"fmt"

	"alauda.io/devops-apiserver/pkg/restapi/errors"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListJenkins list jenkins instances
func (p processor) ListJenkins(ctx context.Context, client versioned.Interface, query *dataselect.Query) (data *JenkinsList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list jenkins end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	// fetch data from list
	jenkinslist, err := client.DevopsV1alpha1().Jenkinses().List(v1alpha1.ListOptions())

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &JenkinsList{
		Jenkins:  make([]v1alpha1.Jenkins, 0),
		ListMeta: ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(jenkinslist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Jenkins = ConvertToJenkinsSlice(result)
	data.ListMeta = ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeleteJenkins delete jenkins instance
func (p processor) DeleteJenkins(ctx context.Context, client versioned.Interface, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete jenkins end", log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().Jenkinses().Delete(name, &metav1.DeleteOptions{})
	return
}

// RetrieveJenkins retrieve jenkins instance
func (p processor) RetrieveJenkins(ctx context.Context, client versioned.Interface, name string) (data *v1alpha1.Jenkins, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get jenkins end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().Jenkinses().Get(name, GetOptionsInCache)
	return
}

//CreateJenkins create jenkins instance
func (p processor) CreateJenkins(ctx context.Context, client versioned.Interface, jenkins *v1alpha1.Jenkins) (data *v1alpha1.Jenkins, err error) {
	logger := abcontext.Logger(ctx)

	basedomain := localcontext.ExtraConfig(ctx).AnnotationProvider.BaseDomain
	annotation := make(map[string]string)
	annotation[v1alpha1.JenkinsPlatformBaseDomain] = basedomain
	jenkins.Annotations = annotation

	defer func() {
		logger.Debug("Create jenkins end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().Jenkinses().Create(jenkins)
	return
}

//UpdateJenkins update a jenkins instance
func (p processor) UpdateJenkins(ctx context.Context, client versioned.Interface, jenkins *v1alpha1.Jenkins, name string) (data *v1alpha1.Jenkins, err error) {
	logger := abcontext.Logger(ctx)
	basedomain := localcontext.ExtraConfig(ctx).AnnotationProvider.BaseDomain
	defer func() {
		logger.Debug("Update jenkins end", log.Err(err), log.String("name", name))
	}()
	oldJenkins := new(v1alpha1.Jenkins)
	oldJenkins, err = client.DevopsV1alpha1().Jenkinses().Get(name, GetOptionsInCache)

	if err != nil {
		return
	}

	annotation := make(map[string]string)
	annotation[v1alpha1.JenkinsPlatformBaseDomain] = basedomain
	jenkins.Annotations = annotation

	oldJenkins.SetAnnotations(jenkins.GetAnnotations())
	oldJenkins.Spec = jenkins.Spec

	data, err = client.DevopsV1alpha1().Jenkinses().Update(oldJenkins)
	return
}

// ConvertToJenkinsSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToJenkinsSlice(filtered []metav1.Object) (items []v1alpha1.Jenkins) {
	items = make([]v1alpha1.Jenkins, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.Jenkins); ok {
			cm.Kind = v1alpha1.ResourceKindJenkins
			items = append(items, *cm)
		}
	}
	return
}

func AuthorizeService(client versioned.Interface, jenkinsName, secretName, namespace string) (*v1alpha1.CodeRepoServiceAuthorizeResponse, error) {
	opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
		SecretName: secretName,
		Namespace:  namespace,
	}
	authorizeResponse, err := client.DevopsV1alpha1().Jenkinses().Authorize(jenkinsName, &opts)
	fmt.Printf("authorizeResponse: %#v, err:%#v", authorizeResponse, err)
	return authorizeResponse, err
}
