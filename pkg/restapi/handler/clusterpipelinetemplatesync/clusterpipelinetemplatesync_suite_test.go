package clusterpipelinetemplatesync_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestClusterPipelinetemplatesync(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("clusterpipelinetemplatesync.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/clusterpipelinetemplatesync", []Reporter{junitReporter})
}
