package clusterpipelinetemplatesync_test

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/clusterpipelinetemplatesync"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Processor.ClusterPipelineTemplateSync", func() {
	var (
		processor clusterpipelinetemplatesync.Processor
		ctx       context.Context
		client    *fake.Clientset
		query     *dataselect.Query

		list *clusterpipelinetemplatesync.ClusterPipelineTemplateSyncList
		err  error
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 10,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)

		client = fake.NewSimpleClientset()
		processor = clusterpipelinetemplatesync.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("should return clusterpipelinetemplatesync", func() {
		client = fake.NewSimpleClientset(
			GetPipelinesynctList("pipelinenamesync"),
		)
		list, err = processor.ListClusterPipelineTemplateSync(ctx, client, query)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Items).To(HaveLen(2), "should not have any items")
	})

})

func GetPipelinesynctList(name string) *v1alpha1.ClusterPipelineTemplateSyncList {
	list := &v1alpha1.ClusterPipelineTemplateSyncList{
		TypeMeta: metav1.TypeMeta{
			Kind: "clusterpipelinetemplatesync",
		},

		Items: []v1alpha1.ClusterPipelineTemplateSync{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
				},
				Spec: v1alpha1.PipelineTemplateSyncSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name + "1",
				},
				Spec: v1alpha1.PipelineTemplateSyncSpec{},
			},
		},
	}
	return list
}
