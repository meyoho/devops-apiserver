package clusterpipelinetemplatesync

import (
	"context"
	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for ClusterPipelineTemplateSync
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListClusterPipelineTemplateSync(ctx context.Context, client versioned.Interface, query *dataselect.Query) (*ClusterPipelineTemplateSyncList, error)
	RetrieveClusterPipelineTemplateSync(ctx context.Context, client versioned.Interface, name string) (*v1alpha1.ClusterPipelineTemplateSync, error)
	DeleteClusterPipelineTemplateSync(ctx context.Context, client versioned.Interface, name string) error
	CreateClusterPipelineTemplateSync(ctx context.Context, client versioned.Interface, spec *v1alpha1.ClusterPipelineTemplateSync) (*v1alpha1.ClusterPipelineTemplateSync, error)
	UpdateClusterPipelineTemplateSync(ctx context.Context, client versioned.Interface, name string, spec *v1alpha1.ClusterPipelineTemplateSync) (*v1alpha1.ClusterPipelineTemplateSync, error)
}

// New builder method for clusterpipelinetemplatesync.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ClusterPipelineTemplateSync related APIs").ApiVersion("v1").Path("/api/v1/clusterpipelinetemplatesync")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "clusterpipelinetemplatesync")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List ClusterPipelineTemplateSync instance`).
					To(handler.ListClusterPipelineTemplateSync).
					Writes(v1alpha1.ClusterPipelineTemplateSyncList{}).
					Returns(http.StatusOK, "OK", v1alpha1.ClusterPipelineTemplateSyncList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve ClusterPipelineTemplateSync instance").
				Param(ws.PathParameter("name", "name of the ClusterPipelineTemplateSync")).
				To(handler.RetrieveClusterPipelineTemplateSync).
				Returns(http.StatusOK, "Retrieve ClusterPipelineTemplateSync instance", v1alpha1.ClusterPipelineTemplateSync{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete ClusterPipelineTemplateSync instance").
				Param(ws.PathParameter("name", "name of the ClusterPipelineTemplateSync")).
				To(handler.DeleteClusterPipelineTemplateSync).
				Returns(http.StatusOK, "Retrieve ClusterPipelineTemplateSync instance", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("create a clusterpipelinetemplatesync").
				To(handler.CreateClusterPipelineTemplateSync).
				Returns(http.StatusOK, "create ClusterPipelineTemplateSync instance", v1alpha1.ClusterPipelineTemplateSync{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("update clusterpipelinetemplatesync").
				Param(ws.PathParameter("name", "name of the ClusterPipelineTemplateSync")).
				To(handler.UpdateClusterPipelineTemplateSync).
				Returns(http.StatusOK, "create ClusterPipelineTemplateSync instance", v1alpha1.ClusterPipelineTemplateSync{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListClusterPipelineTemplateSync list clusterpipelinetemplatesync instances
func (h Handler) ListClusterPipelineTemplateSync(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListClusterPipelineTemplateSync(req.Request.Context(), client, query)
	h.WriteResponse(list, err, req, res)
}

//RetrieveClusterPipelineTemplateSync Retrieve ClusterPipelineTemplateSync instance
func (h Handler) RetrieveClusterPipelineTemplateSync(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrieveClusterPipelineTemplateSync(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// DeleteClusterPipelineTemplateSync
func (h Handler) DeleteClusterPipelineTemplateSync(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteClusterPipelineTemplateSync(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(nil, err, req, res)
}

//CreateClusterPipelineTemplateSync create ClusterPipelineTemplateSync instance
func (h Handler) CreateClusterPipelineTemplateSync(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())

	spec := new(v1alpha1.ClusterPipelineTemplateSync)
	//req.ReadEntity(spec)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	spec.ObjectMeta.Name = "TemplateSync"

	data, err := h.Processor.CreateClusterPipelineTemplateSync(req.Request.Context(), client, spec)
	h.WriteResponse(data, err, req, res)
}

//UpdateClusterPipelineTemplateSync create ClusterPipelineTemplateSync instance
func (h Handler) UpdateClusterPipelineTemplateSync(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())

	spec := new(v1alpha1.ClusterPipelineTemplateSync)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	name := req.PathParameter("name")

	data, err := h.Processor.UpdateClusterPipelineTemplateSync(req.Request.Context(), client, name, spec)
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
