package clusterpipelinetemplatesync

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListClusterPipelineTemplateSync list clusterpipelinetemplatesync instances
func (p processor) ListClusterPipelineTemplateSync(ctx context.Context, client versioned.Interface, query *dataselect.Query) (data *ClusterPipelineTemplateSyncList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list clusterpipelinetemplatesync end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	clusterpipelinetemplatesynclist, err := client.DevopsV1alpha1().ClusterPipelineTemplateSyncs().List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &ClusterPipelineTemplateSyncList{
		Items:    make([]v1alpha1.ClusterPipelineTemplateSync, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(clusterpipelinetemplatesynclist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToClusterPipelineTemplateSyncSlice(result)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// RetrieveClusterPipelineTemplateSync retrieve clusterpipelinetemplatesync instance
func (p processor) RetrieveClusterPipelineTemplateSync(ctx context.Context, client versioned.Interface, name string) (data *v1alpha1.ClusterPipelineTemplateSync, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get clusterpipelinetemplatesync end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Get(name, jenkins.GetOptionsInCache)
	return
}

func (p processor) DeleteClusterPipelineTemplateSync(ctx context.Context, client versioned.Interface, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete clusterpipelinetemplatesync end", log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Delete(name, &metav1.DeleteOptions{})
	return
}

func (p processor) CreateClusterPipelineTemplateSync(ctx context.Context, client versioned.Interface, spec *v1alpha1.ClusterPipelineTemplateSync) (result *v1alpha1.ClusterPipelineTemplateSync, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create clusterpipelinetemplatesync end", log.Err(err))
	}()

	sync := &v1alpha1.ClusterPipelineTemplateSync{
		TypeMeta: metav1.TypeMeta{
			Kind:       "ClusterPipelineTemplateSync",
			APIVersion: "devops.alauda.io/v1alph1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:        spec.ObjectMeta.Name,
			Annotations: spec.ObjectMeta.Annotations,
			Labels:      spec.ObjectMeta.Labels,
		},
		Spec: spec.Spec,
		Status: &v1alpha1.PipelineTemplateSyncStatus{
			Phase:     spec.Status.Phase,
			StartTime: metav1.Now(),
		},
	}
	result, err = client.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Create(sync)
	return
}

func (p processor) UpdateClusterPipelineTemplateSync(ctx context.Context, client versioned.Interface, name string, spec *v1alpha1.ClusterPipelineTemplateSync) (result *v1alpha1.ClusterPipelineTemplateSync, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update clusterpipelinetemplatesync end", log.Err(err))
	}()
	old, err := client.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Get(name, jenkins.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	old.Spec = spec.Spec
	old.Status = &v1alpha1.PipelineTemplateSyncStatus{
		Phase:     spec.Status.Phase,
		StartTime: metav1.Now(),
	}
	result, err = client.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Update(old)
	if err != nil {
		return nil, err
	}

	return
}

// ConvertToClusterPipelineTemplateSyncSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToClusterPipelineTemplateSyncSlice(filtered []metav1.Object) (items []v1alpha1.ClusterPipelineTemplateSync) {
	items = make([]v1alpha1.ClusterPipelineTemplateSync, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.ClusterPipelineTemplateSync); ok {
			items = append(items, *cm)
		}
	}
	return
}
