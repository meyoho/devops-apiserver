package artifactregistrymanager

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

// Handler handler for artifactregistrymanager
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	GetArtifactRegistryManagerList(ctx context.Context, client versioned.Interface, dsQuery *dataselect.Query) (result *ArtifactRegistryManagerList, err error)
	GetArtifactRegistryManager(ctx context.Context, client versioned.Interface, name string) (*v1alpha1.ArtifactRegistryManager, error)
	DeleteArtifactRegistryManager(ctx context.Context, client versioned.Interface, name string) error
	CreateArtifactRegistryManager(ctx context.Context, client versioned.Interface, arm *v1alpha1.ArtifactRegistryManager) (*v1alpha1.ArtifactRegistryManager, error)
	UpdateArtifactRegistryManager(ctx context.Context, client versioned.Interface, newARM *v1alpha1.ArtifactRegistryManager, name string) (*v1alpha1.ArtifactRegistryManager, error)
}

// New builder method for artifactregistrymanager.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ArtifactRegistryManager related APIs").ApiVersion("v1").Path("/api/v1/artifactregistrymanagers")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "ArtifactRegistryManager")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`Get ArtifactRegistryManagerList`).
					To(handler.GetArtifactRegistryManagerList).
					Returns(http.StatusOK, "OK", v1alpha1.ArtifactRegistryManager{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ArtifactRegistryManager instance").
				To(handler.GetArtifactRegistryManagerDetail).
				Returns(http.StatusOK, "get ArtifactRegistryManager Instance", v1alpha1.ArtifactRegistryManager{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete ArtifactRegistryManager instance").
				Param(ws.PathParameter("name", "name of the ArtifactRegistryManager")).
				To(handler.DeleteArtifactRegistryManager).
				Returns(http.StatusOK, "Delete ArtifactRegistryManager Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create ArtifactRegistryManagerManager instance").
				To(handler.CreateArtifactRegistryManager).
				Returns(http.StatusOK, "Retrieve CodeQualityProject instance", v1alpha1.ArtifactRegistryManager{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update ArtifactRegistryManager instance").
				Param(restful.PathParameter("name", "ArtifactRegistryManager name to filter scope")).
				To(handler.UpdateArtifactRegistryManager).
				Returns(http.StatusOK, "Update ArtifactRegistryManager instance", v1alpha1.CodeQualityProject{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetArtifactRegistryManagerList list artifactregistry instances
func (h Handler) GetArtifactRegistryManagerList(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	dataSelect := common.ParseDataSelectPathParameter(req)
	result, err := h.Processor.GetArtifactRegistryManagerList(req.Request.Context(), devopsClient, dataSelect)
	h.WriteResponse(result, err, req, res)
}

//GetArtifactRegistryManagerDetail Delete CodeQualityProject instance
func (h Handler) GetArtifactRegistryManagerDetail(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	name := req.PathParameter("name")
	result, err := h.Processor.GetArtifactRegistryManager(req.Request.Context(), devopsClient, name)
	h.WriteResponse(result, err, req, res)
}

//RetrieveCodeQualityProject Retrieve CodeQualityProject instance
func (h Handler) DeleteArtifactRegistryManager(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	name := req.PathParameter("name")
	err := h.Processor.DeleteArtifactRegistryManager(req.Request.Context(), devopsClient, name)
	h.WriteResponse(nil, err, req, res)
}

//CreateArtifactRegistryManager create CodeQualityProject instance
func (h Handler) CreateArtifactRegistryManager(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	spec := new(v1alpha1.ArtifactRegistryManager)

	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	data, err := h.Processor.CreateArtifactRegistryManager(req.Request.Context(), client, spec)
	h.WriteResponse(data, err, req, res)
}

//UpdateJenkins update CodeQualityProject instance
func (h Handler) UpdateArtifactRegistryManager(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	name := req.PathParameter("name")
	spec := new(v1alpha1.ArtifactRegistryManager)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	data, err := h.Processor.UpdateArtifactRegistryManager(req.Request.Context(), devopsClient, spec, name)
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
