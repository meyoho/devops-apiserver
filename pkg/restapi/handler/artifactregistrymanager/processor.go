package artifactregistrymanager

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

func (h processor) GetArtifactRegistryManagerList(ctx context.Context, client devopsclient.Interface, dsQuery *dataselect.Query) (result *ArtifactRegistryManagerList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list artifactregistry end", log.Any("list", result), log.Err(err))
	}()
	armList, err := client.DevopsV1alpha1().ArtifactRegistryManagers().List(api.ListEverything)
	if err != nil {
		log.Errorf("error while listing artifactregistrymanager: %v", err)
	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	armCells, filteredTotal := dataselect.GenericDataSelectWithFilter(dataselect.ToObjectCellSlice(armList.Items), dsQuery)
	armArray := dataselect.FromCellToObjectSlice(armCells)

	result = &ArtifactRegistryManagerList{
		Items:    make([]v1alpha1.ArtifactRegistryManager, 0),
		ListMeta: api.ListMeta{TotalItems: len(armArray)},
	}
	result.Items = ConvertToArtifactregistryManagerSlice(armArray)
	result.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	result.Errors = nonCriticalErrors

	return
}

func (h processor) GetArtifactRegistryManager(ctx context.Context, client devopsclient.Interface, name string) (result *v1alpha1.ArtifactRegistryManager, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("get ArtifactRegistryManager end", log.Any("manager", result), log.Err(err))
	}()

	result, err = client.DevopsV1alpha1().ArtifactRegistryManagers().Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (h processor) DeleteArtifactRegistryManager(ctx context.Context, client devopsclient.Interface, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("get ArtifactRegistryManager end", log.Err(err))
	}()
	err = client.DevopsV1alpha1().ArtifactRegistryManagers().Delete(name, &v1.DeleteOptions{})
	return
}

func (h processor) CreateArtifactRegistryManager(ctx context.Context, client devopsclient.Interface, arm *v1alpha1.ArtifactRegistryManager) (result *v1alpha1.ArtifactRegistryManager, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create ArtifactRegistryManager end", log.Any("manager", result), log.Err(err))
	}()
	result, err = client.DevopsV1alpha1().ArtifactRegistryManagers().Create(arm)
	return
}

func (h processor) UpdateArtifactRegistryManager(ctx context.Context, client devopsclient.Interface, newARM *v1alpha1.ArtifactRegistryManager, name string) (oldARM *v1alpha1.ArtifactRegistryManager, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("update ArtifactRegistryManager end", log.Any("manager", oldARM), log.Err(err))
	}()

	oldARM, err = client.DevopsV1alpha1().ArtifactRegistryManagers().Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}

	oldARM.SetAnnotations(newARM.GetAnnotations())
	oldARM.Spec = newARM.Spec

	oldARM, err = client.DevopsV1alpha1().ArtifactRegistryManagers().Update(oldARM)
	if err != nil {
		return nil, err
	}
	return oldARM, nil
}
