package artifactregistrymanager

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"log"
)

type ArtifactRegistryManagerList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	Items []v1alpha1.ArtifactRegistryManager `json:"artifactregistrymanagers"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type ArtifactRegistryManager struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	Spec   v1alpha1.ArtifactRegistryManagerSpec `json:"spec"`
	Status v1alpha1.ServiceStatus               `json:"status"`
}

func GetArtifactRegistryManagerList(client devopsclient.Interface, dsQuery *dataselect.Query) (result *ArtifactRegistryManagerList, err error) {
	log.Println("Getting list of artifactregistrymanager list")

	armList, err := client.DevopsV1alpha1().ArtifactRegistryManagers().List(api.ListEverything)
	if err != nil {
		log.Println("error while listing artifactregistrymanager", err)
	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	armCells, filteredTotal := dataselect.GenericDataSelectWithFilter(dataselect.ToObjectCellSlice(armList.Items), dsQuery)
	armArray := dataselect.FromCellToObjectSlice(armCells)

	result = &ArtifactRegistryManagerList{
		Items:    make([]v1alpha1.ArtifactRegistryManager, 0),
		ListMeta: api.ListMeta{TotalItems: len(armArray)},
	}
	result.Items = ConvertToArtifactregistryManagerSlice(armArray)
	result.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	result.Errors = nonCriticalErrors

	return
}

func ConvertToArtifactregistryManagerSlice(filtered []metav1.Object) (items []v1alpha1.ArtifactRegistryManager) {
	items = make([]v1alpha1.ArtifactRegistryManager, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.ArtifactRegistryManager); ok {
			items = append(items, *cm)
		}
	}
	return
}
