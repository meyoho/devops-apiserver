package artifactregistrymanager

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"encoding/json"
)

func GetArtifactRegistryManagerBlobstore(client devopsclient.Interface, name string) (v1alpha1.BlobStoreOptionList, error) {
	result := v1alpha1.BlobStoreOptionList{}
	err := client.DevopsV1alpha1().RESTClient().Get().
		Resource("artifactregistrymanagers").
		Name(name).
		SubResource("blobstore").
		Do().
		Into(&result)

	return result, err
}

func GetArtifactRegistryManagerSub(client devopsclient.Interface, resource, name, sub string) (map[string]interface{}, error) {
	result := new(map[string]interface{})
	bs, err := client.DevopsV1alpha1().RESTClient().Get().
		Resource(resource).
		Name(name).
		SubResource(sub).
		Do().Raw()
	err = json.Unmarshal(bs, result)

	return *result, err
}

func CreateArtifactRegistryManagerProject(client devopsclient.Interface, name string, body *devops.CreateProjectOptions) (*devops.ArtifactRegistry, error) {
	result := &devops.ArtifactRegistry{}
	err := client.DevopsV1alpha1().RESTClient().Post().Name(name).
		Resource("artifactregistrymanager").SubResource("project").
		Body(body).
		Do().
		Into(result)
	return result, err
}
