package codequalityproject

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for CodeQualityProject
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListCodeQualityProject(ctx context.Context, client versioned.Interface, namespace *common.NamespaceQuery, query *dataselect.Query) (*CodeQualityProjectList, error)
	DeleteCodeQualityProject(ctx context.Context, client versioned.Interface, namespace, name string) error
	RetrieveCodeQualityProject(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.CodeQualityProject, error)
	CreateCodeQualityProject(ctx context.Context, client versioned.Interface, CodeQualityProject *v1alpha1.CodeQualityProject, namespace string) (*v1alpha1.CodeQualityProject, error)
	UpdateCodeQualityProject(ctx context.Context, client versioned.Interface, CodeQualityProject *v1alpha1.CodeQualityProject, namespace, name string) (*v1alpha1.CodeQualityProject, error)
}

// New builder method for codequalityproject.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("CodeQualityProject related APIs").ApiVersion("v1").Path("/api/v1/codequalityproject")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "codequalityproject")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List CodeQualityProject instance`).
					Param(ws.PathParameter("namespace", "name of the CodeQualityProject")).
					To(handler.ListCodeQualityProject).
					Writes(v1alpha1.CodeQualityProjectList{}).
					Returns(http.StatusOK, "OK", CodeQualityProjectList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create CodeQualityProject instance").
				To(handler.CreateCodeQualityProject).
				Returns(http.StatusOK, "Create CodeQualityProject Instance", v1alpha1.Jenkins{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete CodeQualityProject instance").
				Param(ws.PathParameter("name", "name of the CodeQualityProject")).
				Param(ws.PathParameter("namespace", "name of the CodeQualityProject")).
				To(handler.DeleteCodeQualityProject).
				Returns(http.StatusOK, "Delete CodeQualityProject Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve CodeQualityProject instance").
				Param(ws.PathParameter("name", "name of the CodeQualityProject")).
				Param(ws.PathParameter("namespace", "namespace of the CodeQualityProject")).
				To(handler.RetrieveCodeQualityProject).
				Returns(http.StatusOK, "Retrieve CodeQualityProject instance", v1alpha1.CodeQualityProject{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update CodeQualityProject instance").
				Param(restful.PathParameter("name", "CodeQualityProject name to filter scope")).
				To(handler.UpdateCodeQualityProject).
				Returns(http.StatusOK, "Update CodeQualityProject instance", v1alpha1.CodeQualityProject{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListCodeQualityProject list codequalityproject instances
func (h Handler) ListCodeQualityProject(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	namespace := common.ParseNamespacePathParameter(req)

	list, err := h.Processor.ListCodeQualityProject(req.Request.Context(), client, namespace, query)
	h.WriteResponse(list, err, req, res)
}

//DeleteCodeQualityProject Delete CodeQualityProject instance
func (h Handler) DeleteCodeQualityProject(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteCodeQualityProject(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//RetrieveCodeQualityProject Retrieve CodeQualityProject instance
func (h Handler) RetrieveCodeQualityProject(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrieveCodeQualityProject(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//CreateCodeQualityProject create CodeQualityProject instance
func (h Handler) CreateCodeQualityProject(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newCodeQualityProject := new(v1alpha1.CodeQualityProject)

	if err := req.ReadEntity(newCodeQualityProject); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.CreateCodeQualityProject(req.Request.Context(), client, newCodeQualityProject, req.PathParameter("namespace"))
	h.WriteResponse(data, err, req, res)
}

//UpdateJenkins update CodeQualityProject instance
func (h Handler) UpdateCodeQualityProject(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newCodeQualityProject := new(v1alpha1.CodeQualityProject)

	if err := req.ReadEntity(newCodeQualityProject); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.UpdateCodeQualityProject(req.Request.Context(), client, newCodeQualityProject, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
