package codequalityproject_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestCodequalityproject(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("codequalityproject.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/codequalityproject", []Reporter{junitReporter})
}
