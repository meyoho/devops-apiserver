package codequalityproject_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codequalityproject"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Processor.Codequalityproject", func() {
	var (
		processor codequalityproject.Processor
		ctx       context.Context
		client    *fake.Clientset
		query     *dataselect.Query
		namespace *common.NamespaceQuery

		list *codequalityproject.CodeQualityProjectList
		err  error
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 10,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		client = fake.NewSimpleClientset()
		namespace = common.NewNamespaceQuery([]string{"default"})

		processor = codequalityproject.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("should return codequalityproject", func() {
		client = fake.NewSimpleClientset(
			GetCodeQualityProjectList("codequalityproject", "default"),
		)
		list, err = processor.ListCodeQualityProject(ctx, client, namespace, query)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Items).To(HaveLen(2), "should not have any items")
	})

})

func GetCodeQualityProjectList(name, namespace string) *v1alpha1.CodeQualityProjectList {
	codequalityprojectlist := &v1alpha1.CodeQualityProjectList{
		TypeMeta: metav1.TypeMeta{
			Kind: "pipeline",
		},

		Items: []v1alpha1.CodeQualityProject{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha1.CodeQualityProjectSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name + "1",
					Namespace: namespace,
				},
				Spec: v1alpha1.CodeQualityProjectSpec{},
			},
		},
	}
	return codequalityprojectlist
}
