package codequalityproject

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListCodeQualityProject list codequalityproject instances
func (p processor) ListCodeQualityProject(ctx context.Context, client versioned.Interface, namespace *common.NamespaceQuery, query *dataselect.Query) (data *CodeQualityProjectList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list codequalityproject end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	codequalityprojectlist, err := client.DevopsV1alpha1().CodeQualityProjects(namespace.ToRequestParam()).List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(codequalityprojectlist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data = &CodeQualityProjectList{
		Items:    make([]v1alpha1.CodeQualityProject, 0),
		ListMeta: api.ListMeta{TotalItems: 0},
	}

	data.Items = ConvertToCodeQualityProjectSlice(result)
	data.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeleteCodeQualityProject delete codequalityproject instance
func (p processor) DeleteCodeQualityProject(ctx context.Context, client versioned.Interface, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete CodeQualityProject end", log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().CodeQualityProjects(namespace).Delete(name, &metav1.DeleteOptions{})
	return
}

// RetrieveCodeQualityProject retrieve codequalityproject instance
func (p processor) RetrieveCodeQualityProject(ctx context.Context, client versioned.Interface, namespace, name string) (data *v1alpha1.CodeQualityProject, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get codequalityproject end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().CodeQualityProjects(namespace).Get(name, jenkins.GetOptionsInCache)
	return
}

//CreateCodeQualityProject create codequalityproject instance
func (p processor) CreateCodeQualityProject(ctx context.Context, client versioned.Interface, codequalityproject *v1alpha1.CodeQualityProject, namespace string) (data *v1alpha1.CodeQualityProject, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create codequalityproject end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().CodeQualityProjects(namespace).Create(codequalityproject)
	return
}

//UpdateCodeQualityProject update a codequalityproject instance
func (p processor) UpdateCodeQualityProject(ctx context.Context, client versioned.Interface, codequalityproject *v1alpha1.CodeQualityProject, namespace, name string) (data *v1alpha1.CodeQualityProject, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update CodeQualityProject end", log.Err(err), log.String("name", name))
	}()
	oldCodeQualityProject := new(v1alpha1.CodeQualityProject)
	oldCodeQualityProject, err = client.DevopsV1alpha1().CodeQualityProjects(namespace).Get(name, common.GetOptionsInCache)

	if err != nil {
		return
	}

	oldCodeQualityProject.SetAnnotations(codequalityproject.GetAnnotations())
	oldCodeQualityProject.Spec = codequalityproject.Spec

	data, err = client.DevopsV1alpha1().CodeQualityProjects(namespace).Update(oldCodeQualityProject)
	return
}

// ConvertToCodeQualityProjectSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToCodeQualityProjectSlice(filtered []metav1.Object) (items []v1alpha1.CodeQualityProject) {
	items = make([]v1alpha1.CodeQualityProject, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.CodeQualityProject); ok {
			items = append(items, *cm)
		}
	}
	return
}
