package codequalityproject

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/api"
)

// CodeQualityProjectList contains a list of CodeQualityProject in the cluster.
type CodeQualityProjectList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of CodeQualityProject.
	Items []v1alpha1.CodeQualityProject `json:"codequalityprojects"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// RetryRequest request a retry for pipeline
type RetryRequest struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`

	// empty for now
}

// LogDetails log details
type LogDetails struct {
	*v1alpha1.PipelineLog
}

// TaskDetails jenkins step details
type TaskDetails struct {
	*v1alpha1.PipelineTask
}
