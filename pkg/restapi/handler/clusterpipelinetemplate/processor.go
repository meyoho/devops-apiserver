package clusterpipelinetemplate

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListClusterPipelineTemplate list clusterpipelinetemplate instances
func (p processor) ListClusterPipelineTemplate(ctx context.Context, client versioned.Interface, query *dataselect.Query) (data *ClusterPipelineTemplateList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list clusterpipelinetemplate end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	clusterpipelinetemplatelist, err := client.DevopsV1alpha1().ClusterPipelineTemplates().List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &ClusterPipelineTemplateList{
		Items:    make([]v1alpha1.ClusterPipelineTemplate, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := toCells(clusterpipelinetemplatelist.Items)

	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := fromCells(itemCells)

	data.Items = ConvertToClusterPipelineTemplateSlice(result)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// RetrieveClusterPipelineTemplate retrieve clusterpipelinetemplate instance
func (p processor) RetrieveClusterPipelineTemplate(ctx context.Context, client versioned.Interface, name string) (data *v1alpha1.ClusterPipelineTemplate, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get clusterpipelinetemplate end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().ClusterPipelineTemplates().Get(name, jenkins.GetOptionsInCache)
	data.Kind = devops.TypeClusterPipelineTemplate
	return
}

// PreviewClusterPipelineTemplate perview a jenkinsfile
func (p processor) PreviewClusterPipelineTemplate(ctx context.Context, client versioned.Interface, name string, options *PreviewOptions) (jenkinsfile string, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("preview clusterpipelinetemplate jenkinsfile end", log.String("name", name), log.Err(err))
	}()

	var source *v1alpha1.PipelineSource
	if options != nil && options.Source != nil {
		source = options.Source
	}
	opts := &v1alpha1.JenkinsfilePreviewOptions{
		Source: source,
		Values: options.Values,
	}
	result, err := client.DevopsV1alpha1().ClusterPipelineTemplates().Preview(name, opts)
	return result.Jenkinsfile, err
}

func (p processor) ExportsClusterPiplineTemplate(ctx context.Context, client versioned.Interface, name, taskName string) (data *v1alpha1.PipelineExportedVariables, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("get clusterpipelinetemplate export end", log.String("name", name), log.Err(err))
	}()

	opts := &v1alpha1.ExportShowOptions{
		TaskName: taskName,
	}

	data, err = client.DevopsV1alpha1().ClusterPipelineTemplates().Exports(name, opts)
	return
}

//CreateClusterPipelineTemplate create clusterpipelinetemplate instance
func (p processor) CreateClusterPipelineTemplate(ctx context.Context, client versioned.Interface, clusterpipelinetemplate *v1alpha1.ClusterPipelineTemplate) (data *v1alpha1.ClusterPipelineTemplate, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create clusterpipelinetemplate end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().ClusterPipelineTemplates().Create(clusterpipelinetemplate)
	return
}

// ConvertToClusterPipelineTemplateSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToClusterPipelineTemplateSlice(filtered []v1alpha1.ClusterPipelineTemplate) (items []v1alpha1.ClusterPipelineTemplate) {
	for _, item := range filtered {
		item.Kind = common.ResourceKindClusterPipelineTemplate
		items = append(items, item)
	}
	return
}
