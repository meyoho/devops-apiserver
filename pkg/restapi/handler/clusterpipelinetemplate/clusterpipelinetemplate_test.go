package clusterpipelinetemplate_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/clusterpipelinetemplate"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Clusterpipelinetemplate", func() {

	var (
		processor                   clusterpipelinetemplate.Processor
		ctx                         context.Context
		client                      *fake.Clientset
		query                       *dataselect.Query
		err                         error
		clusterpipelinetemplatelist *clusterpipelinetemplate.ClusterPipelineTemplateList
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 1,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		client = fake.NewSimpleClientset()
		processor = clusterpipelinetemplate.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("get all clusterpipelinetempalte", func() {
		client = fake.NewSimpleClientset(
			GetClusterPipelinetemplateList("testbinding"),
		)
		clusterpipelinetemplatelist, err = processor.ListClusterPipelineTemplate(ctx, client, query)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(clusterpipelinetemplatelist).ToNot(BeNil(), "should return a list")
		Expect(clusterpipelinetemplatelist.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(clusterpipelinetemplatelist.Items).To(HaveLen(1), "should have one items")
		Expect(clusterpipelinetemplatelist.Items[0].Name).To(Equal("testbinding"))

	})

	//It("should retrieve a jenkinsbinding", func() {
	//	client = fake.NewSimpleClientset(
	//		GetJenkinsBinding("testbinding", "default"),
	//	)
	//	clusterpipelinetemplatelist, err = processor.ListClusterPipelineTemplate(ctx, client,query)
	//	Expect(err).To(BeNil())
	//	Expect(jenkinsbindinginstance.Spec.Jenkins.Name).To(Equal("jenkinsname"))
	//
	//})

})

func GetClusterPipelinetemplateList(name string) *v1alpha1.ClusterPipelineTemplateList {
	clusterpipelinetempaltelist := &v1alpha1.ClusterPipelineTemplateList{

		Items: []v1alpha1.ClusterPipelineTemplate{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
				},
				Spec: v1alpha1.PipelineTemplateSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name + "1",
				},
				Spec: v1alpha1.PipelineTemplateSpec{},
			},
		},
	}
	return clusterpipelinetempaltelist
}
