package clusterpipelinetemplate

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for ClusterPipelineTemplate
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListClusterPipelineTemplate(ctx context.Context, client versioned.Interface, query *dataselect.Query) (*ClusterPipelineTemplateList, error)
	RetrieveClusterPipelineTemplate(ctx context.Context, client versioned.Interface, name string) (*v1alpha1.ClusterPipelineTemplate, error)
	PreviewClusterPipelineTemplate(ctx context.Context, client versioned.Interface, name string, options *PreviewOptions) (string, error)
	ExportsClusterPiplineTemplate(ctx context.Context, client versioned.Interface, name, taskName string) (*v1alpha1.PipelineExportedVariables, error)
}

// New builder method for clusterpipelinetemplate.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ClusterPipelineTemplate related APIs").ApiVersion("v1").Path("/api/v1/clusterpipelinetemplate")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "clusterclusterpipelinetemplate")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List ClusterPipelineTemplate instance`).
					To(handler.ListClusterPipelineTemplate).
					Writes(v1alpha1.ClusterPipelineTemplateList{}).
					Returns(http.StatusOK, "OK", ClusterPipelineTemplateList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve ClusterPipelineTemplate instance").
				Param(ws.PathParameter("name", "name of the ClusterPipelineTemplate")).
				To(handler.RetrieveClusterPipelineTemplate).
				Returns(http.StatusOK, "Retrieve ClusterPipelineTemplate instance", v1alpha1.ClusterPipelineTemplate{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{name}/preview").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("preview a clusterpipelinetemplate").
				Param(ws.PathParameter("name", "name of the ClusterPipelineTemplate")).
				To(handler.PreviewClusterPipelineTemplate).
				Returns(http.StatusOK, "preview ClusterPipelineTemplate instance", ""),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}/exports").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get a clusterpipelinetemplate exports").
				Param(ws.PathParameter("name", "name of the ClusterPipelineTemplate")).
				To(handler.ExportsClusterPiplineTemplate).
				Returns(http.StatusOK, "get  ClusterPipelineTemplate export", PipelineExportedVariables{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListClusterPipelineTemplate list clusterpipelinetemplate instances
func (h Handler) ListClusterPipelineTemplate(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListClusterPipelineTemplate(req.Request.Context(), client, query)
	h.WriteResponse(list, err, req, res)
}

//RetrieveClusterPipelineTemplate Retrieve ClusterPipelineTemplate instance
func (h Handler) RetrieveClusterPipelineTemplate(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrieveClusterPipelineTemplate(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//PreviewClusterPipelineTemplate preview a clusterpipelinetemplate
func (h Handler) PreviewClusterPipelineTemplate(req *restful.Request, res *restful.Response) {
	options := new(PreviewOptions)
	if err := req.ReadEntity(options); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	name := req.PathParameter("name")
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.PreviewClusterPipelineTemplate(req.Request.Context(), client, name, options)
	h.WriteResponse(data, err, req, res)
}

func (h Handler) ExportsClusterPiplineTemplate(req *restful.Request, res *restful.Response) {
	name := req.PathParameter("name")
	taskName := req.QueryParameter("taskName")
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.ExportsClusterPiplineTemplate(req.Request.Context(), client, name, taskName)
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
