package clusterpipelinetemplate_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestClusterpipelinetemplate(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("clusterpipelinetempalte.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/clusterpipelinetemplate", []Reporter{junitReporter})
}
