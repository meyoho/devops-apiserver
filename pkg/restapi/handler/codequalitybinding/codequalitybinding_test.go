package codequalitybinding_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codequalitybinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Processor.JenkinsBinding", func() {
	var (
		processor codequalitybinding.Processor
		ctx       context.Context
		client    *fake.Clientset
		query     *dataselect.Query

		list                       *codequalitybinding.CodeQualityBindingList
		err                        error
		namespace                  *common.NamespaceQuery
		codequalitybindinginstance *v1alpha1.CodeQualityBinding
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 1,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		namespace = common.NewNamespaceQuery([]string{"default"})
		client = fake.NewSimpleClientset()
		processor = codequalitybinding.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("should return only one codequalitybinding", func() {
		client = fake.NewSimpleClientset(
			GetCodeQualityBindingList("testbinding", "default"),
		)
		list, err = processor.ListCodeQualityBinding(ctx, client, query, namespace)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Items).To(HaveLen(1), "should not have any items")
	})

	It("should retrieve a jenkinsbinding", func() {
		client = fake.NewSimpleClientset(
			GetCodequalityBinding("testbinding", "default"),
		)
		codequalitybindinginstance, err = processor.GetCodeQualityBinding(ctx, client, "default", "testbinding")
		Expect(err).To(BeNil())
		Expect(codequalitybindinginstance.Spec.CodeQualityTool.Name).To(Equal("codequalityname"))

	})

})

func GetCodequalityBinding(name, namespace string) *v1alpha1.CodeQualityBinding {
	codequalitybindinginstance := &v1alpha1.CodeQualityBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: v1alpha1.CodeQualityBindingSpec{
			CodeQualityTool: v1alpha1.LocalObjectReference{
				Name: "codequalityname",
			},
		},
	}
	return codequalitybindinginstance
}

func GetCodeQualityBindingList(name, namespace string) *v1alpha1.CodeQualityBindingList {
	codequalitybindinglist := &v1alpha1.CodeQualityBindingList{
		TypeMeta: metav1.TypeMeta{
			Kind: "codequalitybinding",
		},

		Items: []v1alpha1.CodeQualityBinding{
			{TypeMeta: metav1.TypeMeta{
				Kind: "codequality",
			},
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
					Annotations: map[string]string{
						common.AnnotationsKeyToolType: v1alpha1.ToolChainCodeQualityToolName,
					},
				},
				Spec: v1alpha1.CodeQualityBindingSpec{
					CodeQualityTool: v1alpha1.LocalObjectReference{
						Name: "codequalityname",
					},
				},
			},
			{TypeMeta: metav1.TypeMeta{
				Kind: "codequality",
			},
				ObjectMeta: metav1.ObjectMeta{
					Name:      name + "1",
					Namespace: namespace,
					Annotations: map[string]string{
						common.AnnotationsKeyToolType: v1alpha1.ToolChainCodeQualityToolName,
					},
				},
				Spec: v1alpha1.CodeQualityBindingSpec{
					CodeQualityTool: v1alpha1.LocalObjectReference{
						Name: "codequalityname1",
					},
				},
			},
		},
	}
	return codequalitybindinglist
}
