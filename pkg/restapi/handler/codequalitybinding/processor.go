package codequalitybinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"context"

	"alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codequalityproject"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/secret"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListCodeQuality list codequality instances
func (p processor) ListCodeQualityBinding(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *CodeQualityBindingList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list codequalitybinding end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	// fetch data from list
	codequalitylist, err := client.DevopsV1alpha1().CodeQualityBindings(namespace.ToRequestParam()).List(v1alpha1.ListOptions())

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &CodeQualityBindingList{
		Items:    make([]v1alpha1.CodeQualityBinding, 0),
		ListMeta: common.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(codequalitylist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	provider := localcontext.ExtraConfig(ctx).AnnotationProvider

	data.Items = ConvertToCodeQualitySlice(result, provider)
	data.ListMeta = common.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeleteCodeQuality delete codequalitybinding instance
func (p processor) DeleteCodeQualityBinding(ctx context.Context, client versioned.Interface, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete codequalitybinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().CodeQualityBindings(namespace).Delete(name, &metav1.DeleteOptions{})
	return
}

// GetCodeQualityBinding retrieve codequalitybinding instance
func (p processor) GetCodeQualityBinding(ctx context.Context, client versioned.Interface, namespace, name string) (data *v1alpha1.CodeQualityBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get codequalitybinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().CodeQualityBindings(namespace).Get(name, common.GetOptionsInCache)
	return
}

//CreateCodeQualityBinding create codequalitybinding instance
func (p processor) CreateCodeQualityBinding(ctx context.Context, client versioned.Interface, codequalitybinding *v1alpha1.CodeQualityBinding, namespace string) (data *v1alpha1.CodeQualityBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create codequalitybinding end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().CodeQualityBindings(namespace).Create(codequalitybinding)
	return
}

//UpdateCodeQualityBinding update a codequalitybinding instance
func (p processor) UpdateCodeQualityBinding(ctx context.Context, client versioned.Interface, codequalitybinding, oldcodequalitybinding *v1alpha1.CodeQualityBinding, namespace, name string) (data *v1alpha1.CodeQualityBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update codequalitybinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	binding := oldcodequalitybinding.DeepCopy()
	binding.SetAnnotations(codequalitybinding.GetAnnotations())
	binding.Spec = codequalitybinding.Spec

	data, err = client.DevopsV1alpha1().CodeQualityBindings(namespace).Update(binding)
	return
}

// GetCodeQualityBindingProjects get codequalitybindingprojects
func (p processor) GetCodeQualityBindingProjects(ctx context.Context, client versioned.Interface, namespace, name string, query *dataselect.Query) (data *CodeQualityProjectList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update codequalitybinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	binding, err := client.DevopsV1alpha1().CodeQualityBindings(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}

	cqpList, err := client.DevopsV1alpha1().CodeQualityProjects(namespace).List(api.ListEverything)
	if err != nil {
		log.Error("error while getting code quality projects", log.Err(err))
	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	var projects []v1alpha1.CodeQualityProject
	for _, item := range cqpList.Items {
		if item.Spec.CodeQualityBinding.Name == binding.Name {
			projects = append(projects, item)
		}
	}

	data = &CodeQualityProjectList{
		Items:    make([]v1alpha1.CodeQualityProject, 0),
		ListMeta: api.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(projects)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = codequalityproject.ConvertToCodeQualityProjectSlice(result)
	data.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return data, nil
}

func (p processor) GetCodeQualityBindingSecretList(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string, query *dataselect.Query) (secretlist secret.SecretList, err error) {
	extra := localcontext.ExtraConfig(ctx)
	credentialsNamespace := extra.CredentialsNamespace
	systemNamespace := extra.SystemNamespace
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get Codequality binding secrets  end", log.Err(err))
	}()

	binding, err := GetCodeQualityBinding(client, namespace, name)
	if err != nil {
		logger.Error("get codequalitybinding err", log.Err(err))
		return
	}

	provider := localcontext.ExtraConfig(ctx).AnnotationProvider
	namespaceQuery := common.NewSameNamespaceQuery(namespace)
	secretList, err := secret.GetSecretList(k8sclient, appclient, namespaceQuery, query, false, credentialsNamespace, systemNamespace, provider)
	if err != nil {
		logger.Error("get secret List err ", log.Err(err))
		return
	}
	var secrets []secret.Secret
	for _, se := range secretList.Secrets {
		if binding.Spec.Secret.Name == se.ObjectMeta.Name && binding.ObjectMeta.Namespace == se.ObjectMeta.Namespace {
			secrets = append(secrets, se)
			break
		}
	}

	secretlist = secret.SecretList{
		ListMeta: api.ListMeta{TotalItems: 1},
		Secrets:  secrets,
	}
	return
}

func GetCodeQualityBinding(client versioned.Interface, namespace, name string) (*v1alpha1.CodeQualityBinding, error) {
	crs, err := client.DevopsV1alpha1().CodeQualityBindings(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	return crs, nil
}

// ConvertToCodeQualitySlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToCodeQualitySlice(filtered []metav1.Object, provider devops.AnnotationProvider) (items []v1alpha1.CodeQualityBinding) {
	items = make([]v1alpha1.CodeQualityBinding, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.CodeQualityBinding); ok {
			cm.ObjectMeta.Annotations[provider.AnnotationsKeyToolType()] = v1alpha1.ToolChainCodeQualityToolName
			cm.Kind = common.ResourceKindCodeQualityBinding
			items = append(items, *cm)
		}
	}
	return
}
