package codequalitybinding

import (
	"context"
	"net/http"

	"alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/secret"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for CodeQuality
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListCodeQualityBinding(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*CodeQualityBindingList, error)
	DeleteCodeQualityBinding(ctx context.Context, client versioned.Interface, namespace, name string) error
	GetCodeQualityBinding(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.CodeQualityBinding, error)
	CreateCodeQualityBinding(ctx context.Context, client versioned.Interface, codequalitybinding *v1alpha1.CodeQualityBinding, namespace string) (*v1alpha1.CodeQualityBinding, error)
	UpdateCodeQualityBinding(ctx context.Context, client versioned.Interface, codequalitybinding, oldcodequalitybinding *v1alpha1.CodeQualityBinding, namespace, name string) (*v1alpha1.CodeQualityBinding, error)
	GetCodeQualityBindingProjects(ctx context.Context, client versioned.Interface, namespace, name string, query *dataselect.Query) (*CodeQualityProjectList, error)
	GetCodeQualityBindingSecretList(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string, query *dataselect.Query) (secret.SecretList, error)
}

// New builder method for codequalitybinding.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("CodeQualityBinding related APIs").ApiVersion("v1").Path("/api/v1/codequalitybinding")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "codequalitybinding")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List CodeQualityBinding instances`).
					To(handler.ListCodeQualityBinding).
					Writes(CodeQualityBindingList{}).
					Returns(http.StatusOK, "OK", CodeQualityBindingList{}),
			),
		),
	)
	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List CodeQualityBinding instances`).
					To(handler.ListCodeQualityBinding).
					Writes(CodeQualityBindingList{}).
					Returns(http.StatusOK, "OK", CodeQualityBindingList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create CodeQualityBinding instance").
				To(handler.CreateCodeQualityBinding).
				Returns(http.StatusOK, "Create CodeQualityBinding instance", v1alpha1.CodeQualityBinding{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get CodeQualitybinding details").
				Param(ws.PathParameter("name", "name of the codequalitybinding")).
				Param(ws.PathParameter("namespace", "namespace of the codequalitybinding")).
				To(handler.GetCodeQualityBinding).
				Returns(http.StatusOK, "Get CodeQualityBinding details Complete", v1alpha1.CodeQualityBinding{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete CodeQuality instance").
				Param(ws.PathParameter("name", "name of the codequalitybinding")).
				Param(ws.PathParameter("namespace", "namespace of the codequalitybinding")).
				To(handler.DeleteCodeQualityBinding).
				Returns(http.StatusOK, "Retrieve CodeQualitybinding instance", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update CodeQualitybinding instance").
				Param(ws.PathParameter("name", "name of the codequality")).
				Param(ws.PathParameter("namespace", "namespace of the codequalitybinding")).
				To(handler.UpdateCodeQualityBinding).
				Returns(http.StatusOK, "Update CodeQualitybinding instance", v1alpha1.CodeQualityBinding{}),
		),
	)

	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("/{namespace}/{name}/projects").
					Filter(clientFilter.DevOpsClientFilter).
					Doc("retrieve resources associated with codequalitybinding").
					Param(ws.PathParameter("name", "name of the codequality")).
					Param(ws.PathParameter("namespace", "namespace of the codequalitybinding")).
					To(handler.GetCodeQualityBindingProjects).
					Returns(http.StatusOK, "Get CodeQualitybinding resource instance", CodeQualityProjectList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/secrets").
				Filter(clientFilter.DevOpsClientFilter).
				Filter(clientFilter.InsecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("get codequalitybinding secret").
				Param(ws.PathParameter("name", "name of the codequality")).
				Param(ws.PathParameter("namespace", "namespace of the codequalitybinding")).
				To(handler.GetCodeQualityBindingSecretList).
				Returns(http.StatusOK, "Get CodeQualitybinding secret instance", secret.SecretList{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListCodeQuality list codequality instances
func (h Handler) ListCodeQualityBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	codequalitybinding, err := h.Processor.ListCodeQualityBinding(req.Request.Context(), client, query, common.ParseNamespacePathParameter(req))
	common.WriteResponseBySort(codequalitybinding, err, req, res, h)
}

//DeleteCodeQuality Delete codequality instance
func (h Handler) DeleteCodeQualityBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteCodeQualityBinding(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//GetCodeQualityBinding Get codequalitybinding instance
func (h Handler) GetCodeQualityBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.GetCodeQualityBinding(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//CreateCodeQualityBinding create codequalitybinding instance
func (h Handler) CreateCodeQualityBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newCodeQualityBinding := new(v1alpha1.CodeQualityBinding)

	if err := req.ReadEntity(newCodeQualityBinding); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
		SecretName: newCodeQualityBinding.GetSecretName(),
		Namespace:  newCodeQualityBinding.GetSecretNamespace(),
	}
	_, err := client.DevopsV1alpha1().CodeQualityTools().Authorize(newCodeQualityBinding.Spec.CodeQualityTool.Name, &opts)

	if err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	namespace := req.PathParameter("namespace")

	data, err := h.Processor.CreateCodeQualityBinding(req.Request.Context(), client, newCodeQualityBinding, namespace)
	h.WriteResponse(data, err, req, res)
}

//UpdateCodeQualityBinding update codequalitybinding instance
func (h Handler) UpdateCodeQualityBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newCodeQualityBinding := new(v1alpha1.CodeQualityBinding)

	if err := req.ReadEntity(newCodeQualityBinding); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
		SecretName: newCodeQualityBinding.GetSecretName(),
		Namespace:  newCodeQualityBinding.GetSecretNamespace(),
	}
	_, err := client.DevopsV1alpha1().CodeQualityTools().Authorize(newCodeQualityBinding.Spec.CodeQualityTool.Name, &opts)

	if err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	oldCodeQualityBinding, err := h.Processor.GetCodeQualityBinding(req.Request.Context(), client, namespace, name)
	if err != nil {
		errors.HandleInternalError(res, err)
		return
	}

	data, err := h.Processor.UpdateCodeQualityBinding(req.Request.Context(), client, newCodeQualityBinding, oldCodeQualityBinding, namespace, name)
	h.WriteResponse(data, err, req, res)
}

func (h Handler) GetCodeQualityBindingProjects(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	query := abcontext.Query(req.Request.Context())
	projects, err := h.Processor.GetCodeQualityBindingProjects(req.Request.Context(), client, namespace, name, query)
	h.WriteResponse(projects, err, req, res)
}

func (h Handler) GetCodeQualityBindingSecretList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	k8sclient := abcontext.Client(req.Request.Context())
	appclient := localcontext.AppClient(req.Request.Context())
	secretquery := common.ParseDataSelectPathParameter(req)
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	data, err := h.Processor.GetCodeQualityBindingSecretList(req.Request.Context(), client, k8sclient, appclient, namespace, name, secretquery)
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
