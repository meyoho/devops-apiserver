package codequalitybinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
)

// CodeQualityBindingList contains a list of CodeQualityBinding in the cluster
type CodeQualityBindingList struct {
	ListMeta common.ListMeta `json:"listMeta"`

	// Unordered list of CodeQualityBinding.
	Items []v1alpha1.CodeQualityBinding `json:"codequalitybindings"`

	// List of non-critical errors, that occured during resource retrieval
	Errors []error `json:"errors"`
}

// CodeQualityProjectList contains a list of CodeQualityProject in the cluster.
type CodeQualityProjectList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of CodeQualityProject.
	Items []v1alpha1.CodeQualityProject `json:"codequalityprojects"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}
