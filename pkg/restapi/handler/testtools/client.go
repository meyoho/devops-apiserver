package testtools

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	fakeclient "sigs.k8s.io/controller-runtime/pkg/client/fake"

	"k8s.io/apimachinery/pkg/runtime"
)

func GetScheme() *runtime.Scheme {
	scheme := clientgoscheme.Scheme
	_ = v1alpha1.AddToScheme(scheme)
	return scheme
}

func NewFakeClient(initObjects ...runtime.Object) client.Client {
	return fakeclient.NewFakeClientWithScheme(GetScheme(), initObjects...)
}
