package coderepository_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestCoderepository(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("codequalitybinding.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/codequalitybinding", []Reporter{junitReporter})
}
