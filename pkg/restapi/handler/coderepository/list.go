package coderepository

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelineconfig"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"log"
	"sync"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
)

// CodeRepositoryList contains a list of CodeRepository in the cluster.
type CodeRepositoryList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of CodeRepository.
	Items []v1alpha1.CodeRepository `json:"coderepositories"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// CodeRepository is a presentation layer view of Kubernetes namespaces. This means it is namespace plus
// additional augmented data we can get from other sources.
type CodeRepository struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	Spec   v1alpha1.CodeRepositorySpec   `json:"spec"`
	Status v1alpha1.CodeRepositoryStatus `json:"status"`
}

func GetResourcesReferToRemovedRepos(client versioned.Interface, oldBinding, newBinding *v1alpha1.CodeRepoBinding, provider devops.AnnotationProvider) (resourceList *common.ResourceList) {
	resourceList = &common.ResourceList{}

	var removedRepoNames []string
	for _, oldCondition := range oldBinding.Status.Conditions {
		if oldCondition.Type != v1alpha1.JenkinsBindingStatusTypeRepository {
			continue
		}

		var found bool
		for _, newCondition := range newBinding.Status.Conditions {
			if newCondition.Type != v1alpha1.JenkinsBindingStatusTypeRepository {
				continue
			}

			if oldCondition.Name == newCondition.Name {
				found = true
				break
			}
		}
		if !found {
			removedRepoNames = append(removedRepoNames, oldCondition.Name)
		}
	}

	log.Println("removedRepoNames: ", removedRepoNames)
	wait := sync.WaitGroup{}
	for _, repoName := range removedRepoNames {
		log.Println("get resource refer to ", repoName)
		go func(namespace, name string) {
			repoQuery := dataselect.GeSimpleFieldQuery(common.CodeRepositoryProperty, name)
			items := pipelineconfig.GetPipelineConfigListAsResourceList(client, namespace, repoQuery, provider)
			resourceList.Items = append(resourceList.Items, items...)
			wait.Done()
		}(newBinding.Namespace, repoName)
		wait.Add(1)
	}
	wait.Wait()

	return
}

func GetCodeRepositoryBranches(client versioned.Interface, namespace, name, sortBy, sortMode string) (*v1alpha1.CodeRepoBranchResult, error) {
	opts := &v1alpha1.CodeRepoBranchOptions{
		SortBy:   sortBy,
		SortMode: sortMode,
	}
	log.Println("Get coderepository branches repository: ", name, "namespace: ", namespace, " sortBy: ", sortBy, " sortMode: ", sortMode)

	branches, err := client.DevopsV1alpha1().CodeRepositories(namespace).GetBranches(name, opts)
	if err != nil {
		log.Println("Error get coderepository branches: ", err)
		return nil, err
	}
	return branches, nil
}
