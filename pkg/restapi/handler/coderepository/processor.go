package coderepository

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListCodeRepository list projectmanagement instances
func (p processor) ListCodeRepository(ctx context.Context, client versioned.Interface, namespace *common.NamespaceQuery, query *dataselect.Query) (data *CodeRepositoryList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list coderepository end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()

	// fetch data from list
	coderepositorylist, err := client.DevopsV1alpha1().CodeRepositories(namespace.ToRequestParam()).List(common.ListEverything)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &CodeRepositoryList{
		Items:    make([]v1alpha1.CodeRepository, 0),
		ListMeta: api.ListMeta{TotalItems: 0},
	}
	annotationprovider := localcontext.ExtraConfig(ctx).AnnotationProvider
	// filter using standard filters
	itemCells := ToCells(coderepositorylist.Items, annotationprovider)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := FromCells(itemCells)

	data.Items = result
	data.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// ConvertToCodeRepositorySlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToCodeRepositorySlice(filtered []metav1.Object) (items []v1alpha1.CodeRepository) {
	items = make([]v1alpha1.CodeRepository, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.CodeRepository); ok {
			cm.Kind = v1alpha1.ResourceKindCodeRepository
			items = append(items, *cm)
		}
	}
	return
}
