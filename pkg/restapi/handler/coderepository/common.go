package coderepository

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

//type codeRepositoryCell v1alpha1.CodeRepository

type codeRepositoryCell struct {
	coderepo *v1alpha1.CodeRepository
	devops.AnnotationProvider
}

func defalutMetav1Time(before, after *metav1.Time) *metav1.Time {
	if before != nil {
		return before
	}
	return after
}

func (self codeRepositoryCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case common.NameProperty:
		return dataselect.StdComparableString(self.coderepo.ObjectMeta.Name)
	case common.FullNameProperty:
		return dataselect.StdCaseInSensitiveComparableString(self.coderepo.Spec.Repository.FullName)
	case common.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.coderepo.ObjectMeta.CreationTimestamp.Time)
	case common.LatestCommitAt:
		// Discard LatestCommit, Use PushedAt
		target := defalutMetav1Time(self.coderepo.Spec.Repository.PushedAt, &metav1.Time{Time: time.Unix(0, 0)})
		return dataselect.StdComparableTime(target.Time)
	case common.NamespaceProperty:
		return dataselect.StdComparableString(self.coderepo.ObjectMeta.Namespace)
	case common.DisplayNameProperty:
		name := self.coderepo.ObjectMeta.Name
		if len(self.coderepo.ObjectMeta.Annotations) != 0 && self.coderepo.ObjectMeta.Annotations[self.AnnotationsKeyDisplayName()] != "" {
			name = self.coderepo.ObjectMeta.Annotations[self.AnnotationsKeyDisplayName()]
		}
		return dataselect.StdLowerComparableString(name)
	case common.CodeRepoBindingProperty:
		return dataselect.StdLowerComparableString(self.coderepo.Spec.CodeRepoBinding.Name)
	case common.LabelProperty:
		if len(self.coderepo.ObjectMeta.Labels) > 0 {
			values := []string{}
			for k, v := range self.coderepo.ObjectMeta.Labels {
				values = append(values, k+":"+v)
			}
			return dataselect.StdComparableLabel(strings.Join(values, ","))
		}
	}
	// if name is not supported then just return a constant dummy value, sort will have no effect.
	return nil
}

func ToCells(std []v1alpha1.CodeRepository, provider devops.AnnotationProvider) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = codeRepositoryCell{&std[i], provider}
	}
	return cells
}

func FromCells(cells []dataselect.DataCell) []v1alpha1.CodeRepository {
	std := make([]v1alpha1.CodeRepository, len(cells))
	for i := range std {
		std[i] = *cells[i].(codeRepositoryCell).coderepo.DeepCopy()
		std[i].Kind = v1alpha1.ResourceKindCodeRepository
	}
	return std
}
