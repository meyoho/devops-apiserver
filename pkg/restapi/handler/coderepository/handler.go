package coderepository

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for CodeRepository
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListCodeRepository(ctx context.Context, client versioned.Interface, name *common.NamespaceQuery, query *dataselect.Query) (*CodeRepositoryList, error)
}

// New builder method for coderepository.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("CodeRepository related APIs").ApiVersion("v1").Path("/api/v1/coderepository")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "coderepository")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List CodeRepository instance`).
					To(handler.ListCodeRepository).
					Writes(v1alpha1.CodeRepositoryList{}).
					Returns(http.StatusOK, "OK", CodeRepositoryList{}),
			),
		),
	)

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}/{name}/branches").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List CodeRepository barnches`).
					Param(ws.QueryParameter("sortBy", "sort option. The choices are creationTime")).
					Param(ws.QueryParameter("sortMode", "sort option. The choices are desc or asc")).
					To(handler.ListCodeRepositoryBranches).
					Returns(http.StatusOK, "OK", v1alpha1.CodeRepoBranchResult{}),
			),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListCodeRepository list coderepository instances
func (h Handler) ListCodeRepository(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	namespace := common.ParseNamespacePathParameter(req)

	list, err := h.Processor.ListCodeRepository(req.Request.Context(), client, namespace, query)
	h.WriteResponse(list, err, req, res)
}

// ListCodeRepositoryBranches
func (h Handler) ListCodeRepositoryBranches(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())

	name := req.PathParameter("name")
	namespace := req.PathParameter("namespace")
	sortBy := req.QueryParameter("sortBy")
	sortMode := req.QueryParameter("sortMode")

	result, err := GetCodeRepositoryBranches(devopsClient, namespace, name, sortBy, sortMode)
	h.WriteResponse(result, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
