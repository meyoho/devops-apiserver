package clusterpipelinetasktemplate

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListClusterPipelineTaskTemplate list clusterpipelinetasktemplate instances
func (p processor) ListClusterPipelineTaskTemplate(ctx context.Context, client versioned.Interface, query *dataselect.Query) (data *ClusterPipelineTaskTemplateList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list clusterpipelinetasktemplate end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	clusterpipelinetasktemplatelist, err := client.DevopsV1alpha1().ClusterPipelineTaskTemplates().List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &ClusterPipelineTaskTemplateList{
		Items:    make([]v1alpha1.ClusterPipelineTaskTemplate, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}

	itemCells := toCells(clusterpipelinetasktemplatelist.Items)

	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := fromCells(itemCells)

	data.Items = ConvertToClusterPipelineTaskTemplateSlice(result)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// RetrieveClusterPipelineTaskTemplate retrieve clusterpipelinetasktemplate instance
func (p processor) RetrieveClusterPipelineTaskTemplate(ctx context.Context, client versioned.Interface, name string) (data *v1alpha1.ClusterPipelineTaskTemplate, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get clusterpipelinetasktemplate end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().ClusterPipelineTaskTemplates().Get(name, jenkins.GetOptionsInCache)
	data.Kind = devops.TypeClusterPipelineTaskTemplate
	return
}

// ConvertToClusterPipelineTaskTemplateSlice
// without having to do it manually
func ConvertToClusterPipelineTaskTemplateSlice(filtered []v1alpha1.ClusterPipelineTaskTemplate) (items []v1alpha1.ClusterPipelineTaskTemplate) {
	for _, item := range filtered {
		item.Kind = common.ResourceKindClusterPipelineTaskTemplate
		items = append(items, item)
	}
	return
}
