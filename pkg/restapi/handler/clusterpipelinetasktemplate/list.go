package clusterpipelinetasktemplate

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
)

// ClusterPipelineTaskTemplateList contains a list of ClusterPipelineTaskTemplate
type ClusterPipelineTaskTemplateList struct {
	ListMeta jenkins.ListMeta                       `json:"listMeta"`
	Items    []v1alpha1.ClusterPipelineTaskTemplate `json:"clusterpipelinetasktemplates"`
	Errors   []error                                `json:"errors"`
}
