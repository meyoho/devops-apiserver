package clusterpipelinetasktemplate

import (
	"context"
	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for ClusterPipelineTaskTemplate
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListClusterPipelineTaskTemplate(ctx context.Context, client versioned.Interface, query *dataselect.Query) (*ClusterPipelineTaskTemplateList, error)
	RetrieveClusterPipelineTaskTemplate(ctx context.Context, client versioned.Interface, name string) (*v1alpha1.ClusterPipelineTaskTemplate, error)
}

// New builder method for clusterpipelinetasktemplate.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ClusterPipelineTaskTemplate related APIs").ApiVersion("v1").Path("/api/v1/clusterpipelinetasktemplate")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "clusterpipelinetasktemplate")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List ClusterPipelineTaskTemplate instance`).
					To(handler.ListClusterPipelineTaskTemplate).
					Writes(v1alpha1.ClusterPipelineTaskTemplateList{}).
					Returns(http.StatusOK, "OK", ClusterPipelineTaskTemplateList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve ClusterPipelineTaskTemplate instance").
				Param(ws.PathParameter("name", "name of the ClusterPipelineTaskTemplate")).
				To(handler.RetrieveClusterPipelineTaskTemplate).
				Returns(http.StatusOK, "Retrieve ClusterPipelineTaskTemplate instance", v1alpha1.ClusterPipelineTaskTemplate{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListClusterPipelineTaskTemplate list clusterpipelinetasktemplate instances
func (h Handler) ListClusterPipelineTaskTemplate(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListClusterPipelineTaskTemplate(req.Request.Context(), client, query)
	h.WriteResponse(list, err, req, res)
}

//RetrieveClusterPipelineTaskTemplate Retrieve ClusterPipelineTaskTemplate instance
func (h Handler) RetrieveClusterPipelineTaskTemplate(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrieveClusterPipelineTaskTemplate(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
