package clusterpipelinetasktemplate_test

import (
	"testing"

	"github.com/onsi/ginkgo/reporters"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestClusterpipelineTaskTemplate(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("clusterpipelinetasktempalte.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/clusterpipelinetasktemplate", []Reporter{junitReporter})
}
