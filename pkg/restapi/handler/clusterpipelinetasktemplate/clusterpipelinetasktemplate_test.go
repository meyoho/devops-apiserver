package clusterpipelinetasktemplate_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/clusterpipelinetasktemplate"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Clusterpipelinetasktemplate", func() {

	var (
		processor                       clusterpipelinetasktemplate.Processor
		ctx                             context.Context
		client                          *fake.Clientset
		query                           *dataselect.Query
		err                             error
		clusterpipelinetasktemplatelist *clusterpipelinetasktemplate.ClusterPipelineTaskTemplateList
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 1,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		client = fake.NewSimpleClientset()
		processor = clusterpipelinetasktemplate.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("get all clusterpipelinetempalte", func() {
		client = fake.NewSimpleClientset(
			GetClusterPipelineTasktemplateList("testbinding"),
		)
		clusterpipelinetasktemplatelist, err = processor.ListClusterPipelineTaskTemplate(ctx, client, query)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(clusterpipelinetasktemplatelist).ToNot(BeNil(), "should return a list")
		Expect(clusterpipelinetasktemplatelist.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(clusterpipelinetasktemplatelist.Items).To(HaveLen(1), "should have one items")
		Expect(clusterpipelinetasktemplatelist.Items[0].Name).To(Equal("testbinding"))

	})

})

func GetClusterPipelineTasktemplateList(name string) *v1alpha1.ClusterPipelineTaskTemplateList {
	clusterpipelinetasktempaltelist := &v1alpha1.ClusterPipelineTaskTemplateList{

		Items: []v1alpha1.ClusterPipelineTaskTemplate{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
				},
				Spec: v1alpha1.PipelineTaskTemplateSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name + "1",
				},
				Spec: v1alpha1.PipelineTaskTemplateSpec{},
			},
		},
	}
	return clusterpipelinetasktempaltelist
}
