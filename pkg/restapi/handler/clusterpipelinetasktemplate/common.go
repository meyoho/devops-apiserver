package clusterpipelinetasktemplate

import (
	"strings"

	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
)

type ClusterPipelineTaskTemplateCell devopsv1alpha1.ClusterPipelineTaskTemplate

func (self ClusterPipelineTaskTemplateCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case common.NameProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Name)
	case common.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.ObjectMeta.CreationTimestamp.Time)
	case common.NamespaceProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Namespace)
	case common.DisplayNameProperty:
		if len(self.ObjectMeta.Annotations) > 0 {
			if self.ObjectMeta.Annotations[common.AnnotationsKeyDisplayName] != "" {
				return dataselect.StdLowerComparableString(self.ObjectMeta.Annotations[common.AnnotationsKeyDisplayName])
			}
		}
		return dataselect.StdLowerComparableString(self.ObjectMeta.Name)
	case common.DisplayEnNameProperty:
		if len(self.ObjectMeta.Annotations) > 0 {
			if self.ObjectMeta.Annotations[common.AnnotationsKeyDisplayNameEn] != "" {
				return dataselect.StdCaseInSensitiveComparableString(self.ObjectMeta.Annotations[common.AnnotationsKeyDisplayNameEn])
			}
		}
		return dataselect.StdCaseInSensitiveComparableString(self.ObjectMeta.Name)
	case common.DisplayZhNameProperty:
		if len(self.ObjectMeta.Annotations) > 0 {
			if self.ObjectMeta.Annotations[common.AnnotationsKeyDisplayNameZh] != "" {
				return dataselect.StdCaseInSensitiveComparableString(self.ObjectMeta.Annotations[common.AnnotationsKeyDisplayNameZh])
			}
		}
		return dataselect.StdCaseInSensitiveComparableString(self.ObjectMeta.Name)
	case common.LabelsProperty:
		if len(self.ObjectMeta.Labels) > 0 {
			values := []string{}
			for k, v := range self.ObjectMeta.Labels {
				values = append(values, k+":"+v)
			}
			return dataselect.StdComparableLabel(strings.Join(values, ","))
		}
	case common.CategoryProperty:
		// it could have multi-categories as a json form
		var category string
		var ok bool
		if len(self.ObjectMeta.Annotations) > 0 {
			category, ok = self.ObjectMeta.Annotations[common.AnnotationsKeyCategories]
		}

		if !ok && len(self.ObjectMeta.Labels) > 0 {
			// for back compatible case
			category, ok = self.ObjectMeta.Labels[common.CategoryProperty]
		}

		if category != "" {
			return common.ComparableJSONIn(category)
		}
	}
	// if name is not supported then just return a constant dummy value, sort will have no effect.
	return nil
}

func toCells(std []devopsv1alpha1.ClusterPipelineTaskTemplate) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = ClusterPipelineTaskTemplateCell(std[i])
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []devopsv1alpha1.ClusterPipelineTaskTemplate {
	std := make([]devopsv1alpha1.ClusterPipelineTaskTemplate, len(cells))
	for i := range std {
		std[i] = devopsv1alpha1.ClusterPipelineTaskTemplate(cells[i].(ClusterPipelineTaskTemplateCell))
	}
	return std
}
