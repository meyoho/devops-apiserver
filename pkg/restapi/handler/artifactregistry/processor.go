package artifactregistry

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

func (p processor) ArtifactRegistryList(ctx context.Context, client devopsclient.Interface, dsQuery *dataselect.Query) (data *ArtifactRegistryList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list artifactregistry end", log.Any("list", data), log.Err(err))
	}()
	armList, err := client.DevopsV1alpha1().ArtifactRegistries().List(api.ListEverything)
	if err != nil {
		log.Errorf("error while listing artifactregistry: %v", err)
	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	armCells, filteredTotal := dataselect.GenericDataSelectWithFilter(dataselect.ToObjectCellSlice(armList.Items), dsQuery)
	armArray := dataselect.FromCellToObjectSlice(armCells)

	data = &ArtifactRegistryList{
		Items:    make([]v1alpha1.ArtifactRegistry, 0),
		ListMeta: api.ListMeta{TotalItems: len(armArray)},
	}
	data.Items = ConvertToArtifactregistrySlice(armArray)
	data.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors

	return
}

func (p processor) ArtifactRegistryDetail(ctx context.Context, client devopsclient.Interface, name string) (data *v1alpha1.ArtifactRegistry, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("get artifactregistry detail end", log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().ArtifactRegistries().Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	return data, nil

}

func (p processor) ArtifactRegistryDelete(ctx context.Context, client devopsclient.Interface, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete artifactregistry end", log.Err(err))
	}()
	err = client.DevopsV1alpha1().ArtifactRegistries().Delete(name, &v1.DeleteOptions{})
	return
}

func (p processor) ArtifactRegistryCreate(ctx context.Context, client devopsclient.Interface, arm *v1alpha1.ArtifactRegistry) (data *v1alpha1.ArtifactRegistry, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create artifactregistry end", log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().ArtifactRegistries().Create(arm)
	return
}

func (p processor) ArtifactRegistryUpdate(ctx context.Context, client devopsclient.Interface, newARM *v1alpha1.ArtifactRegistry, name string) (oldARM *v1alpha1.ArtifactRegistry, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update artifactregistry end", log.Err(err))
	}()

	oldARM, err = client.DevopsV1alpha1().ArtifactRegistries().Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}

	oldARM.SetAnnotations(newARM.GetAnnotations())
	oldARM.Spec = newARM.Spec

	oldARM, err = client.DevopsV1alpha1().ArtifactRegistries().Update(oldARM)
	if err != nil {
		return nil, err
	}
	return oldARM, nil
}
