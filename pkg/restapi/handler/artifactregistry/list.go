package artifactregistry

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"log"
)

type ArtifactRegistryList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	Items []v1alpha1.ArtifactRegistry `json:"artifactregistries"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type ArtifactRegistry struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	Spec   v1alpha1.ArtifactRegistrySpec `json:"spec"`
	Status v1alpha1.ServiceStatus        `json:"status"`
}

func GetArtifactRegistryList(client devopsclient.Interface, dsQuery *dataselect.Query) (data *ArtifactRegistryList, err error) {
	log.Println("Getting list of artifactregistry list")

	armList, err := client.DevopsV1alpha1().ArtifactRegistries().List(api.ListEverything)
	if err != nil {
		log.Println("error while listing artifactregistry", err)
	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	armCells, filteredTotal := dataselect.GenericDataSelectWithFilter(dataselect.ToObjectCellSlice(armList.Items), dsQuery)
	armArray := dataselect.FromCellToObjectSlice(armCells)

	data = &ArtifactRegistryList{
		Items:    make([]v1alpha1.ArtifactRegistry, 0),
		ListMeta: api.ListMeta{TotalItems: len(armArray)},
	}
	data.Items = ConvertToArtifactregistrySlice(armArray)
	data.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors

	return
}

func ConvertToArtifactregistrySlice(filtered []metav1.Object) (items []v1alpha1.ArtifactRegistry) {
	items = make([]v1alpha1.ArtifactRegistry, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.ArtifactRegistry); ok {
			items = append(items, *cm)
		}
	}
	return
}
