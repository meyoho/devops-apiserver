package artifactregistry

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
	"net/url"
)

// Handler handler for artifactregistry
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ArtifactRegistryList(ctx context.Context, client devopsclient.Interface, dsQuery *dataselect.Query) (data *ArtifactRegistryList, err error)
	ArtifactRegistryDetail(ctx context.Context, client devopsclient.Interface, name string) (*v1alpha1.ArtifactRegistry, error)
	ArtifactRegistryDelete(ctx context.Context, client devopsclient.Interface, name string) error
	ArtifactRegistryCreate(ctx context.Context, client devopsclient.Interface, arm *v1alpha1.ArtifactRegistry) (*v1alpha1.ArtifactRegistry, error)
	ArtifactRegistryUpdate(ctx context.Context, client devopsclient.Interface, newARM *v1alpha1.ArtifactRegistry, name string) (*v1alpha1.ArtifactRegistry, error)
}

// New builder method for codequalityproject.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ArtifactRegistries related APIs").ApiVersion("v1").Path("/api/v1/artifactregistries")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "artifactregistries")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`Get ArtifactRegistryList`).
					To(handler.GetArtifactRegistryList).
					Returns(http.StatusOK, "OK", ArtifactRegistryList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ArtifactRegistry instance").
				To(handler.GetArtifactRegistryDetail).
				Returns(http.StatusOK, "get ArtifactRegistry Instance", v1alpha1.ArtifactRegistry{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete ArtifactRegistry instance").
				Param(ws.PathParameter("name", "name of the ArtifactRegistry")).
				To(handler.DeleteArtifactRegistry).
				Returns(http.StatusOK, "Delete ArtifactRegistry Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create ArtifactRegistry instance").
				To(handler.CreateArtifactRegistry).
				Returns(http.StatusOK, "Retrieve CodeQualityProject instance", v1alpha1.ArtifactRegistry{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update ArtifactRegistry instance").
				Param(restful.PathParameter("name", "ArtifactRegistry name to filter scope")).
				To(handler.UpdateArtifactRegistry).
				Returns(http.StatusOK, "Update ArtifactRegistry instance", v1alpha1.CodeQualityProject{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetArtifactRegistryList list artifactregistry instances
func (h Handler) GetArtifactRegistryList(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	dataSelect := common.ParseDataSelectPathParameter(req)
	result, err := h.Processor.ArtifactRegistryList(req.Request.Context(), devopsClient, dataSelect)
	h.WriteResponse(result, err, req, res)
}

//GetArtifactRegistryDetail Delete CodeQualityProject instance
func (h Handler) GetArtifactRegistryDetail(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	name := req.PathParameter("name")
	result, err := h.Processor.ArtifactRegistryDetail(req.Request.Context(), devopsClient, name)
	h.WriteResponse(result, err, req, res)
}

//RetrieveCodeQualityProject Retrieve CodeQualityProject instance
func (h Handler) DeleteArtifactRegistry(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	name := req.PathParameter("name")
	err := h.Processor.ArtifactRegistryDelete(req.Request.Context(), devopsClient, name)
	h.WriteResponse(nil, err, req, res)
}

//CreateArtifactRegistry create CodeQualityProject instance
func (h Handler) CreateArtifactRegistry(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	spec := new(v1alpha1.ArtifactRegistry)

	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	redirectURL := req.QueryParameter("redirectUrl")
	if _, err := url.Parse(redirectURL); err != nil {
		redirectURL = ""
	}

	if spec.Spec.Secret.Name != "" {
		opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
			SecretName:  spec.Spec.Secret.Name,
			Namespace:   spec.Spec.Secret.Namespace,
			RedirectURL: redirectURL,
		}
		authorizeResponse, err := client.DevopsV1alpha1().ArtifactRegistries().Authorize(spec.ObjectMeta.Name, &opts)
		if err != nil {
			log.Errorf("authorizeResponse: %v", log.Any("authorizeResponse", authorizeResponse))
			h.WriteResponse(nil, err, req, res)
			return
		}

		if authorizeResponse != nil && authorizeResponse.AuthorizeUrl != "" {
			res.WriteHeaderAndEntity(http.StatusPreconditionRequired, authorizeResponse)
			return
		}
	}

	data, err := h.Processor.ArtifactRegistryCreate(req.Request.Context(), client, spec)
	h.WriteResponse(data, err, req, res)
}

//UpdateJenkins update CodeQualityProject instance
func (h Handler) UpdateArtifactRegistry(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	name := req.PathParameter("name")
	spec := new(v1alpha1.ArtifactRegistry)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	redirectURL := req.QueryParameter("redirectUrl")
	if _, err := url.Parse(redirectURL); err != nil {
		redirectURL = ""
	}

	if spec.Spec.Secret.Name != "" {
		opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
			SecretName:  spec.Spec.Secret.Name,
			Namespace:   spec.Spec.Secret.Namespace,
			RedirectURL: redirectURL,
		}
		authorizeResponse, err := devopsClient.DevopsV1alpha1().ArtifactRegistries().Authorize(spec.ObjectMeta.Name, &opts)
		if err != nil {
			log.Errorf("authorizeResponse: %v", log.Any("authorizeResponse", authorizeResponse))
			h.WriteResponse(nil, err, req, res)
			return
		}

		if authorizeResponse != nil && authorizeResponse.AuthorizeUrl != "" {
			res.WriteHeaderAndEntity(http.StatusPreconditionRequired, authorizeResponse)
			return
		}
	}

	data, err := h.Processor.ArtifactRegistryUpdate(req.Request.Context(), devopsClient, spec, name)
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
