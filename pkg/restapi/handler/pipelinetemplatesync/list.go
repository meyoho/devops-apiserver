package pipelinetemplatesync

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// PipelineTemplateSyncList contains the list of PipelineTemplateSync
type PipelineTemplateSyncList struct {
	ListMeta jenkins.ListMeta                `json:"listMeta"`
	Items    []v1alpha1.PipelineTemplateSync `json:"pipelinetemplatesyncs"`
	Errors   []error                         `json:"errors"`
}

// PreviewOptions used for render jenkinsfile
type PreviewOptions struct {
	Source *v1alpha1.PipelineSource `json:"source"`
	Values map[string]string        `json:"values"`
}

type ExportShowOptions struct {
	TaskName string `json:"taskName"`
}

type PipelineExportedVariables struct {
	metav1.TypeMeta `json:",inline"`
	Values          []GlobalParameter `json:"values"`
}
type GlobalParameter struct {
	// Name the name of parameter
	Name string `json:"name"`
	// Description description of parameter
	// +optional
	Description *I18nName `json:"description"`
}
type I18nName struct {
	// Zh is the Chinese name
	Zh string `json:"zh-CN"`
	// EN is the English name
	En string `json:"en"`
}
