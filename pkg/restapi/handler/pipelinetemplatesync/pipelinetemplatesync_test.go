package pipelinetemplatesync_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelinetemplatesync"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Processor.Pipelinetemplatesync", func() {
	var (
		processor pipelinetemplatesync.Processor
		ctx       context.Context
		client    *fake.Clientset
		query     *dataselect.Query
		namespace *common.NamespaceQuery

		list *pipelinetemplatesync.PipelineTemplateSyncList
		err  error
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 10,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		namespace = common.NewNamespaceQuery([]string{"default"})

		client = fake.NewSimpleClientset()
		processor = pipelinetemplatesync.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("should return pipelinetemplatesync", func() {
		client = fake.NewSimpleClientset(
			GetPipelinesynctList("pipelinenamesync", "default"),
		)
		list, err = processor.ListPipelineTemplateSync(ctx, client, query, namespace)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Items).To(HaveLen(2), "should not have any items")
	})

})

func GetPipelinesynctList(name, namespace string) *v1alpha1.PipelineTemplateSyncList {
	pipelineconfiglist := &v1alpha1.PipelineTemplateSyncList{
		TypeMeta: metav1.TypeMeta{
			Kind: "pipelinetemplatesync",
		},

		Items: []v1alpha1.PipelineTemplateSync{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha1.PipelineTemplateSyncSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name + "1",
					Namespace: namespace,
				},
				Spec: v1alpha1.PipelineTemplateSyncSpec{},
			},
		},
	}
	return pipelineconfiglist
}
