package pipelinetemplatesync

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListPipelineTemplateSync list pipelinetemplatesync instances
func (p processor) ListPipelineTemplateSync(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *PipelineTemplateSyncList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list pipelinetemplatesync end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	pipelinetemplatesynclist, err := client.DevopsV1alpha1().PipelineTemplateSyncs(namespace.ToRequestParam()).List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &PipelineTemplateSyncList{
		Items:    make([]v1alpha1.PipelineTemplateSync, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(pipelinetemplatesynclist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToPipelineTemplateSyncSlice(result)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// RetrievePipelineTemplateSync retrieve pipelinetemplatesync instance
func (p processor) RetrievePipelineTemplateSync(ctx context.Context, client versioned.Interface, namespace, name string) (data *v1alpha1.PipelineTemplateSync, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipelinetemplatesync end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().PipelineTemplateSyncs(namespace).Get(name, jenkins.GetOptionsInCache)
	return
}

func (p processor) DeletePipelineTemplateSync(ctx context.Context, client versioned.Interface, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete pipelinetemplatesync end", log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().PipelineTemplateSyncs(namespace).Delete(name, &metav1.DeleteOptions{})
	return
}

func (p processor) CreatePipelineTemplateSync(ctx context.Context, client versioned.Interface, namespace string, spec *v1alpha1.PipelineTemplateSync) (result *v1alpha1.PipelineTemplateSync, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create pipelinetemplatesync end", log.String("name", namespace), log.Err(err))
	}()

	sync := &v1alpha1.PipelineTemplateSync{
		TypeMeta: metav1.TypeMeta{
			Kind:       "PipelineTemplateSync",
			APIVersion: "devops.alauda.io/v1alph1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:        spec.ObjectMeta.Name,
			Namespace:   namespace,
			Annotations: spec.ObjectMeta.Annotations,
			Labels:      spec.ObjectMeta.Labels,
		},
		Spec: spec.Spec,
		Status: &v1alpha1.PipelineTemplateSyncStatus{
			Phase:     spec.Status.Phase,
			StartTime: metav1.Now(),
		},
	}
	result, err = client.DevopsV1alpha1().PipelineTemplateSyncs(namespace).Create(sync)
	return
}

func (p processor) UpdatePipelineTemplateSync(ctx context.Context, client versioned.Interface, namespace, name string, spec *v1alpha1.PipelineTemplateSync) (result *v1alpha1.PipelineTemplateSync, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update pipelinetemplatesync end", log.String("name", namespace), log.Err(err))
	}()
	old, err := client.DevopsV1alpha1().PipelineTemplateSyncs(namespace).Get(name, jenkins.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	old.Spec = spec.Spec
	old.Status = &v1alpha1.PipelineTemplateSyncStatus{
		Phase:     spec.Status.Phase,
		StartTime: metav1.Now(),
	}
	result, err = client.DevopsV1alpha1().PipelineTemplateSyncs(namespace).Update(old)
	if err != nil {
		return nil, err
	}

	return
}

// ConvertToPipelineTemplateSyncSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToPipelineTemplateSyncSlice(filtered []metav1.Object) (items []v1alpha1.PipelineTemplateSync) {
	items = make([]v1alpha1.PipelineTemplateSync, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.PipelineTemplateSync); ok {
			items = append(items, *cm)
		}
	}
	return
}
