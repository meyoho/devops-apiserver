package pipelinetemplatesync

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for PipelineTemplateSync
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListPipelineTemplateSync(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*PipelineTemplateSyncList, error)
	RetrievePipelineTemplateSync(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.PipelineTemplateSync, error)
	DeletePipelineTemplateSync(ctx context.Context, client versioned.Interface, namespace, name string) error
	CreatePipelineTemplateSync(ctx context.Context, client versioned.Interface, namespace string, spec *v1alpha1.PipelineTemplateSync) (*v1alpha1.PipelineTemplateSync, error)
	UpdatePipelineTemplateSync(ctx context.Context, client versioned.Interface, namespace, name string, spec *v1alpha1.PipelineTemplateSync) (*v1alpha1.PipelineTemplateSync, error)
}

// New builder method for pipelinetemplatesync.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("PipelineTemplateSync related APIs").ApiVersion("v1").Path("/api/v1/pipelinetemplatesync")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "pipelinetemplatesync")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List PipelineTemplateSync instance`).
					To(handler.ListPipelineTemplateSync).
					Writes(v1alpha1.PipelineTemplateSyncList{}).
					Returns(http.StatusOK, "OK", v1alpha1.PipelineTemplateSyncList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve PipelineTemplateSync instance").
				Param(ws.PathParameter("name", "name of the PipelineTemplateSync")).
				Param(ws.PathParameter("namespace", "namespace of the PipelineTemplateSync")).
				To(handler.RetrievePipelineTemplateSync).
				Returns(http.StatusOK, "Retrieve PipelineTemplateSync instance", v1alpha1.PipelineTemplateSync{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete PipelineTemplateSync instance").
				Param(ws.PathParameter("name", "name of the PipelineTemplateSync")).
				Param(ws.PathParameter("namespace", "namespace of the PipelineTemplateSync")).
				To(handler.DeletePipelineTemplateSync).
				Returns(http.StatusOK, "Retrieve PipelineTemplateSync instance", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("create a pipelinetemplatesync").
				Param(ws.PathParameter("namespace", "name of the PipelineTemplateSync")).
				To(handler.CreatePipelineTemplateSync).
				Returns(http.StatusOK, "create PipelineTemplateSync instance", v1alpha1.PipelineTemplateSync{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("update pipelinetemplatesync").
				Param(ws.PathParameter("namespace", "namespace of the PipelineTemplateSync")).
				Param(ws.PathParameter("name", "name of the PipelineTemplateSync")).
				To(handler.UpdatePipelineTemplateSync).
				Returns(http.StatusOK, "create PipelineTemplateSync instance", v1alpha1.PipelineTemplateSync{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListPipelineTemplateSync list pipelinetemplatesync instances
func (h Handler) ListPipelineTemplateSync(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListPipelineTemplateSync(req.Request.Context(), client, query, common.ParseNamespacePathParameter(req))
	h.WriteResponse(list, err, req, res)
}

//RetrievePipelineTemplateSync Retrieve PipelineTemplateSync instance
func (h Handler) RetrievePipelineTemplateSync(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrievePipelineTemplateSync(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// DeletePipelineTemplateSync
func (h Handler) DeletePipelineTemplateSync(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeletePipelineTemplateSync(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(nil, err, req, res)
}

//CreatePipelineTemplateSync create PipelineTemplateSync instance
func (h Handler) CreatePipelineTemplateSync(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())

	spec := new(v1alpha1.PipelineTemplateSync)
	//req.ReadEntity(spec)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	spec.ObjectMeta.Name = "TemplateSync"
	namespace := req.PathParameter("namespace")

	data, err := h.Processor.CreatePipelineTemplateSync(req.Request.Context(), client, namespace, spec)
	h.WriteResponse(data, err, req, res)
}

//UpdatePipelineTemplateSync create PipelineTemplateSync instance
func (h Handler) UpdatePipelineTemplateSync(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())

	spec := new(v1alpha1.PipelineTemplateSync)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	data, err := h.Processor.UpdatePipelineTemplateSync(req.Request.Context(), client, namespace, name, spec)
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
