package pipelineconfig

import (
	"context"
	"net/http"
	controllerruntimeclient "sigs.k8s.io/controller-runtime/pkg/client"
	"strconv"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for PipelineConfig
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListPipelineConfig(ctx context.Context, cacheClient controllerruntimeclient.Client, query *dataselect.Query, namespace *common.NamespaceQuery) (*PipelineConfigList, error)
	DeletePipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string) error
	RetrievePipelineConfig(ctx context.Context, cacheClient controllerruntimeclient.Client, client versioned.Interface, namespace, name string) (*PipelineConfigDetail, error)
	PreviewPipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string, opts *v1alpha1.JenkinsfilePreviewOptions) (string, error)
	TriggerPipelineConfig(ctx context.Context, client versioned.Interface, spec *PipelineConfigTrigger) (*PipelineTriggerResponse, error)
	ScanPipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string) (ScanResult, error)
	GetPipelineConfigLogs(ctx context.Context, client versioned.Interface, namespace, name string, start int) (*v1alpha1.PipelineConfigLog, error)
	CreatePipelineConfig(ctx context.Context, client versioned.Interface, PipelineConfig *PipelineConfigDetail) (*v1alpha1.PipelineConfig, error)
	UpdatePipelineConfig(ctx context.Context, client versioned.Interface, PipelineConfig *PipelineConfigDetail) (*PipelineConfigDetail, error)
}

// New builder method for pipelineconfig.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)
	accessReviewFilter := decorator.AccessReviewFilter(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("PipelineConfig related APIs").ApiVersion("v1").Path("/api/v1/pipelineconfig")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "pipelineconfig")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					// we will read PipelineConfig from cache, so we need to check user's permession here
					Filter(accessReviewFilter.NewAccessReviewFilter(&v1alpha1.PipelineConfig{}, "list")).
					Filter(clientFilter.CacheClientFilter).
					Doc(`List PipelineConfig instance`).
					Param(ws.PathParameter("namespace", "namespace of the piplineconfig")).
					To(handler.ListPipelineConfig).
					Writes(v1alpha1.PipelineConfigList{}).
					Returns(http.StatusOK, "OK", PipelineConfigList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete PipelineConfig instance").
				Param(ws.PathParameter("name", "name of the PipelineConfig")).
				Param(ws.PathParameter("namespace", "namespace of the PipelineConfig")).
				To(handler.DeletePipelineConfig).
				Returns(http.StatusOK, "Delete PipelineConfig Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				// we will read PipelineConfig from cache, so we need to check user's permession here
				Filter(accessReviewFilter.NewAccessReviewFilter(&v1alpha1.PipelineConfig{}, "get")).
				Filter(clientFilter.CacheClientFilter).
				Doc("Retrieve PipelineConfig instance").
				Param(ws.PathParameter("namespace", "namespace of the PipelineConfig")).
				Param(ws.PathParameter("name", "name of the PipelineConfig")).
				To(handler.RetrievePipelineConfig).
				Returns(http.StatusOK, "Retrieve PipelineConfig instance", v1alpha1.PipelineConfig{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("create a pipelineconfig").
				Param(ws.PathParameter("namespace", "namespace of the PipelineConfig")).
				To(handler.CreatePipelineConfig).
				Returns(http.StatusOK, "Retrieve PipelineConfig instance", v1alpha1.PipelineConfig{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("update a pipelineconfig").
				Param(ws.PathParameter("namespace", "namespace of the PipelineConfig")).
				Param(ws.PathParameter("name", "name of the PipelineConfig")).
				To(handler.UpdatePipelineConfig).
				Returns(http.StatusOK, "Update PipelineConfig instance", PipelineConfigDetail{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}/{name}/scan").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("scan multi-branch").
				Param(ws.PathParameter("namespace", "namespace of the PipelineConfig")).
				Param(ws.PathParameter("name", "name of the PipelineConfig")).
				To(handler.ScanPipelineConfig).
				Returns(http.StatusOK, "scan PipelineConfig mutlti-branch instance", ScanResult{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}/{name}/trigger").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("trigger a pipelineconfig").
				Param(ws.PathParameter("namespace", "namespace of the PipelineConfig")).
				Param(ws.PathParameter("name", "name of the PipelineConfig")).
				To(handler.TriggerPipelineConfig).
				Returns(http.StatusOK, "trigger PipelineConfig instance", PipelineTriggerResponse{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}/{name}/preview").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get a preview result of a pipelineconfig").
				Param(ws.PathParameter("namespace", "namespace of the PipelineConfig")).
				Param(ws.PathParameter("name", "name of the PipelineConfig")).
				To(handler.PreviewPipelineConfig).
				Returns(http.StatusOK, "get a preview result of a PipelineConfig", PipelineConfigDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/logs").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get the scanning logs of a PipelineConfig").
				Param(restful.PathParameter("namespace", "Namespace to use")).
				Param(restful.PathParameter("name", "PipelineConfig name to filter scope")).
				Param(restful.QueryParameter("start", "Start offset to fetch logs")).
				To(handler.GetPipelineConfigLogs).
				Returns(http.StatusOK, "get PipelineConfig log", v1alpha1.PipelineConfigLog{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListPipelineConfig list pipelineconfig instances
func (h Handler) ListPipelineConfig(req *restful.Request, res *restful.Response) {
	cacheClient := localcontext.CacheClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListPipelineConfig(req.Request.Context(), cacheClient, query, common.ParseNamespacePathParameter(req))
	h.WriteResponse(list, err, req, res)
}

//DeletePipelineConfig Delete PipelineConfig instance
func (h Handler) DeletePipelineConfig(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeletePipelineConfig(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//RetrievePipelineConfig Retrieve PipelineConfig instance
func (h Handler) RetrievePipelineConfig(req *restful.Request, res *restful.Response) {
	cacheClient := localcontext.CacheClient(req.Request.Context())
	devopsRESTClient := localcontext.DevOpsClient(req.Request.Context())

	data, err := h.Processor.RetrievePipelineConfig(req.Request.Context(), cacheClient, devopsRESTClient, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//GetPipelineConfigLogs get PipelineConfig logs
func (h Handler) GetPipelineConfigLogs(req *restful.Request, res *restful.Response) {
	logger := abcontext.Logger(req.Request.Context())

	client := localcontext.DevOpsClient(req.Request.Context())

	start, err := strconv.Atoi(req.QueryParameter("start"))
	if err != nil {
		logger.Error("parsing start query parameter: original ", log.Any("start", req.QueryParameter("start")), log.Err(err))
		start = 0
	}

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	data, err := h.Processor.GetPipelineConfigLogs(req.Request.Context(), client, namespace, name, start)

	h.WriteResponse(data, err, req, res)
}

//AbortPipelineConfig abort a pipelineconfig
func (h Handler) ScanPipelineConfig(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	scanResult, err := h.Processor.ScanPipelineConfig(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(scanResult, err, req, res)
}

//PreviewPipelineConfig preview a pipelineconfig
func (h Handler) PreviewPipelineConfig(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	spec := new(v1alpha1.JenkinsfilePreviewOptions)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	data, err := h.Processor.PreviewPipelineConfig(req.Request.Context(), client, namespace, name, spec)
	h.WriteResponse(data, err, req, res)
}

// TriggerPipelineConfig trigger pipelineconfig

func (h Handler) TriggerPipelineConfig(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	spec := new(PipelineConfigTrigger)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	spec.Namespace = namespace
	spec.Name = name

	data, err := h.Processor.TriggerPipelineConfig(req.Request.Context(), client, spec)

	h.WriteResponse(data, err, req, res)
}

//CreatePipelineconfig create PipelineConfig instance
func (h Handler) CreatePipelineConfig(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())

	spec := new(PipelineConfigDetail)

	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	data, err := h.Processor.CreatePipelineConfig(req.Request.Context(), client, spec)
	h.WriteResponse(data, err, req, res)
}

//UpdateJenkins update PipelineConfig instance
func (h Handler) UpdatePipelineConfig(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	spec := new(PipelineConfigDetail)

	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	data, err := h.Processor.UpdatePipelineConfig(req.Request.Context(), client, spec)
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
