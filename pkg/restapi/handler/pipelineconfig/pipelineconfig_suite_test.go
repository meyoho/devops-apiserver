package pipelineconfig_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestPipelineconfig(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("pipelineconfig.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/pipelineconfig", []Reporter{junitReporter})
}
