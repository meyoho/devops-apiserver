package pipelineconfig

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"strings"
)

//The code below allows to perform complex data section on []api.Namespace

type PipelineConfigCell struct {
	*v1alpha1.PipelineConfig
	annotation devops.AnnotationProvider
}

func (self PipelineConfigCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case common.NameProperty:
		return dataselect.StdComparableContainsString(self.ObjectMeta.Name)
	case common.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.ObjectMeta.CreationTimestamp.Time)
	case common.NamespaceProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Namespace)
	case common.CodeRepositoryProperty:
		if self.Spec.Source.CodeRepository == nil {
			return nil
		}
		return dataselect.StdComparableString(self.Spec.Source.CodeRepository.Name)
	case common.JenkinsBindingProperty:
		return dataselect.StdEqualString(self.Spec.JenkinsBinding.Name)
	case common.DisplayNameProperty:
		// http://jira.alauda.cn/browse/DEVOPS-2881
		// if there is no displayName on pipelineconfig, just compare to empty
		if len(self.ObjectMeta.Annotations) > 0 {
			return dataselect.StdComparableContainsString(self.ObjectMeta.Annotations[self.annotation.AnnotationsKeyDisplayName()])
		}
		return dataselect.StdComparableContainsString("")
	case common.LabelProperty:
		if len(self.ObjectMeta.Labels) > 0 {
			values := []string{}
			for k, v := range self.ObjectMeta.Labels {
				values = append(values, k+":"+v)
			}
			return dataselect.StdComparableLabel(strings.Join(values, ","))
		}
	}
	// if name is not supported then just return a constant dummy value, sort will have no effect.
	return nil
}

func toCells(std []v1alpha1.PipelineConfig, provider devops.AnnotationProvider) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = PipelineConfigCell{&std[i], provider}
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []v1alpha1.PipelineConfig {
	std := make([]v1alpha1.PipelineConfig, len(cells))
	for i := range std {
		config := cells[i].(PipelineConfigCell).PipelineConfig.DeepCopy()
		std[i] = *config
	}
	return std
}
