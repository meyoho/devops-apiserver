package pipelineconfig

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	"encoding/json"
	"fmt"
	"github.com/appscode/jsonpatch"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"strings"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListPipelineConfig list pipeline instances
func (p processor) ListPipelineConfig(ctx context.Context, cacheClient client.Client, query *dataselect.Query, namespace *common.NamespaceQuery) (resultConfigs *PipelineConfigList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list pipeline end", log.Any("query", query), log.Any("list", resultConfigs), log.Err(err))
	}()

	pipelineConfigList := &v1alpha1.PipelineConfigList{}
	err = cacheClient.List(ctx, &client.ListOptions{
		Namespace: namespace.ToRequestParam(),
	}, pipelineConfigList)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	resultConfigs = &PipelineConfigList{
		Items:    make([]PipelineConfig, 0),
		ListMeta: api.ListMeta{TotalItems: len(pipelineConfigList.Items)},
	}
	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider
	// filter using standard filters
	itemCells := toCells(pipelineConfigList.Items, annotationProvider)

	SelectableData := dataselect.DataSelector{
		GenericDataList: itemCells,
		Query:           query,
	}

	filtered := SelectableData.Filter()
	itemCells, filteredTotal := filtered.GenericDataList, len(filtered.GenericDataList)
	configs := fromCells(itemCells)
	resultConfigs.Items = make([]PipelineConfig, len(configs))
	resultConfigs.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	resultConfigs.Errors = nonCriticalErrors

	pipelinesInNamespace := &v1alpha1.PipelineList{}
	err = cacheClient.List(ctx, &client.ListOptions{
		Namespace: namespace.ToRequestParam(),
	}, pipelinesInNamespace)

	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		log.Error("Unable list pipelines", zap.Error(err))
		return nil, criticalError
	}

	pipelineConfigPipelinesMap := make(map[string][]v1alpha1.Pipeline)
	for _, pipeline := range pipelinesInNamespace.Items {
		pipelineConfigOwner := pipeline.Spec.PipelineConfig.Name
		pipelineConfigPipelinesMap[pipelineConfigOwner] = append(pipelineConfigPipelinesMap[pipelineConfigOwner], pipeline)
	}

	for i, conf := range configs {
		resultConfigs.Items[i] = toPipelineConfig(conf, pipelineConfigPipelinesMap[conf.Name], annotationProvider)
	}

	resultConfigs = sortPipelineConfigList(resultConfigs, query)
	if resultConfigs.Items == nil || len(resultConfigs.Items) == 0 {
		// avoid the items be nil
		resultConfigs.Items = make([]PipelineConfig, len(configs))
	} else {
		resultConfigs.Items = paginate(resultConfigs.Items, query.PaginationQuery)
	}
	return
}

func paginate(items []PipelineConfig, pQuery *dataselect.PaginationQuery) (result []PipelineConfig) {
	startIndex, endIndex := pQuery.GetPaginationSettings(len(items))

	// Return all items if provided settings do not meet requirements
	if !pQuery.IsValidPagination() {
		result = items
		return
	}
	// Return no items if requested page does not exist
	if !pQuery.IsPageAvailable(len(items), startIndex) {
		return
	}

	result = items[startIndex:endIndex]
	return
}

// DeletePipelineConfig delete pipeline instance
func (p processor) DeletePipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete pipelinefoncig end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().PipelineConfigs(namespace).Delete(name, &metav1.DeleteOptions{})
	return
}

// RetrievePipelineConfig retrieve pipeline instance
func (p processor) RetrievePipelineConfig(ctx context.Context, cacheClient client.Client, devopsRESTClient versioned.Interface, namespace, name string) (data *PipelineConfigDetail, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipelineconfig end", log.String("name", name), log.Err(err))
	}()

	logger.Info("Getting details of PipelineConfig", log.String("namespace", namespace), log.String("name", name))

	config := &v1alpha1.PipelineConfig{}
	err = cacheClient.Get(ctx, client.ObjectKey{Namespace: namespace, Name: name}, config)
	if err != nil {
		if !errors.IsNotFoundError(err) {
			return nil, err
		}
		// if we cannot get pipelineConfig from local cache, we will use rest client to request it
		logger.Debug("Cannot get PipelineConfig from local cache, try to get it from server")
		config, err = devopsRESTClient.DevopsV1alpha1().PipelineConfigs(namespace).Get(name, common.GetOptionsInCache)
		if err != nil {
			return nil, err
		}
	}

	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider
	labelSelector := labels.NewSelector()
	req, _err := labels.NewRequirement("pipelineConfig", selection.DoubleEquals, []string{name})
	if _err != nil {
		logger.Warn(fmt.Sprintf("Ignore loading pipelines of pipelineconfig %s, because error:%s", config.GetName(), _err.Error()))
		return &PipelineConfigDetail{
			PipelineConfig: toPipelineConfig(*config, []v1alpha1.Pipeline{}, annotationProvider),
		}, nil
	}

	labelSelector = labelSelector.Add(*req)

	pipelines := &v1alpha1.PipelineList{}
	err = cacheClient.List(ctx, &client.ListOptions{
		Namespace:     namespace,
		LabelSelector: labelSelector,
	}, pipelines)

	if err != nil {
		return nil, err
	}

	if config.Labels != nil {
		if kind, ok := config.Labels[v1alpha1.LabelPipelineKind]; ok && kind == v1alpha1.LabelPipelineKindMultiBranch {
			pipelineConfig, multiPipelines := toMultiBranchPipelineConfig(*config, pipelines.Items, annotationProvider)

			return &PipelineConfigDetail{
				PipelineConfig:        pipelineConfig,
				MulitiBranchPipelines: multiPipelines,
			}, nil
		}
	}

	return &PipelineConfigDetail{
		PipelineConfig: toPipelineConfig(*config, pipelines.Items, annotationProvider),
	}, nil

}

// GetPipelineConfigLogs retrieve pipeline instance
func (p processor) GetPipelineConfigLogs(ctx context.Context, client versioned.Interface, namespace, name string, start int) (Log *v1alpha1.PipelineConfigLog, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipelinelog  end", log.String("namespace", namespace), log.String("name", name), log.Int("start", start), log.Err(err))
	}()
	Log, err = client.DevopsV1alpha1().PipelineConfigs(namespace).GetLogs(name, &v1alpha1.PipelineConfigLogOptions{
		Start: int64(start),
	})
	logger.Info("the scan log is ", log.Any("Log", Log))
	return
}

// ScanPipelineConfig scan pipelineconfig instance
func (p processor) ScanPipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string) (result ScanResult, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipelinelog  end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	var scanResult *v1alpha1.PipelineConfigScanResult
	scanResult, err = client.DevopsV1alpha1().PipelineConfigs(namespace).Scan(name, &v1alpha1.PipelineConfigScanOptions{
		Delay: 0,
	})
	if scanResult != nil {
		result = ScanResult{
			Message: scanResult.Message,
			Success: scanResult.Success,
		}
	}
	return
}

func (p processor) TriggerPipelineConfig(ctx context.Context, client versioned.Interface, spec *PipelineConfigTrigger) (response *PipelineTriggerResponse, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipelinelog  end", log.Err(err))
	}()
	var (
		config *v1alpha1.PipelineConfig
	)
	config, err = client.DevopsV1alpha1().PipelineConfigs(spec.Namespace).Get(spec.Name, api.GetOptionsInCache)
	if err != nil {
		return
	}

	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider
	// read only copy to be writable
	config = config.DeepCopy()
	pipe := generatePipelineFromConfig(config)
	if spec.Commit != "" {
		ann := pipe.ObjectMeta.GetAnnotations()
		if ann == nil {
			ann = make(map[string]string)
		}
		ann[annotationProvider.AnnotationsCommit()] = spec.Commit
		pipe.ObjectMeta.SetAnnotations(ann)
	}

	// only exists on mutliBranch case
	branchName := strings.TrimSpace(spec.Branch)
	if config.Labels != nil && config.Labels[v1alpha1.LabelPipelineKind] == v1alpha1.LabelPipelineKindMultiBranch {
		if branchName == "" {
			err = fmt.Errorf("trigger a multiBranch [%s-%s] pipeline needs branch name", spec.Namespace, spec.Name)
			return
		}

		if pipe.Annotations == nil {
			pipe.Annotations = make(map[string]string, 0)
		}

		pipe.Annotations[annotationProvider.AnnotationsKeyMultiBranchName()] = branchName
		var category string

		allBranches := make([]string, 0)
		allBranches = append(allBranches, collectBranches(annotationProvider.AnnotationsKeyMultiBranchBranchList(), config.Annotations, allBranches)...)
		allBranches = append(allBranches, collectBranches(annotationProvider.AnnotationsKeyMultiBranchStaleBranchList(), config.Annotations, allBranches)...)
		for _, branch := range allBranches {
			if branch == branchName {
				category = "branch"
				break
			}
		}

		if category == "" {
			allPRs := make([]string, 0)
			allPRs = append(allPRs, collectBranches(annotationProvider.AnnotationsKeyMultiBranchPRList(), config.Annotations, allPRs)...)
			allPRs = append(allPRs, collectBranches(annotationProvider.AnnotationsKeyMultiBranchStalePRList(), config.Annotations, allPRs)...)
			for _, pr := range allPRs {
				if pr == branchName {
					category = "pr"
					break
				}
			}
		}

		if category == "" {
			err = fmt.Errorf("unknow branch [%s] category for this multi-branch [%s]", branchName, spec.Name)
			return
		}

		pipe.Annotations[annotationProvider.AnnotationsKeyMultiBranchCategory()] = category
	}

	pipe.Spec.Parameters = append(pipe.Spec.Parameters, spec.Params...)

	pipe, err = client.DevopsV1alpha1().Pipelines(spec.Namespace).Create(pipe)
	if err != nil {
		return
	}
	response = &PipelineTriggerResponse{
		Pipeline: pipe,
	}
	return
}

// PreviewPipelineConfig  preview pipeline config
func (p processor) PreviewPipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string, opts *v1alpha1.JenkinsfilePreviewOptions) (data string, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Preview pipelinecinfig end", log.String("namespace", namespace), log.Any("values", opts.Values), log.Err(err))
	}()

	result, err := client.DevopsV1alpha1().PipelineConfigs(namespace).Preview(name, opts)
	if err != nil {
		return "", err
	}

	return result.Jenkinsfile, nil

}

//CreatePipelineConfig create pipelineconfig instance
func (p processor) CreatePipelineConfig(ctx context.Context, client versioned.Interface, spec *PipelineConfigDetail) (data *v1alpha1.PipelineConfig, err error) {
	if spec == nil {
		return nil, nil
	}
	basedomain := localcontext.ExtraConfig(ctx).AnnotationProvider.BaseDomain
	key := fmt.Sprintf("%s/%s", basedomain, "pipelinecontext.basedomain")
	spec.ObjectMeta.Annotations[key] = basedomain
	config := &v1alpha1.PipelineConfig{
		TypeMeta: metav1.TypeMeta{
			Kind:       "PipelineConfig",
			APIVersion: v1alpha1.APIVersionV1Alpha1,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:        spec.ObjectMeta.Name,
			Namespace:   spec.ObjectMeta.Namespace,
			Labels:      spec.ObjectMeta.Labels,
			Annotations: spec.ObjectMeta.Annotations,
		},
		Spec: spec.PipelineConfig.Spec,
	}
	p.attachEnvironments(ctx, config)
	namespace := spec.ObjectMeta.Namespace
	config, err = client.DevopsV1alpha1().PipelineConfigs(namespace).Create(config)
	if err != nil {
		return nil, err
	}

	return config, nil
}

func (p processor) attachEnvironments(ctx context.Context, config *v1alpha1.PipelineConfig) {

	if config.Spec.Environments == nil {
		config.Spec.Environments = make([]v1alpha1.PipelineEnvironment, 0, 1)
	}

	for _, item := range config.Spec.Environments {
		// check if it has the target envirionment
		if item.Name == "ALAUDA_PROJECT" {
			return
		}
	}

	config.Spec.Environments = append(config.Spec.Environments, v1alpha1.PipelineEnvironment{Name: "ALAUDA_PROJECT", Value: config.Namespace})
	return
}

//UpdatePipelineConfig update a pipeline instance
func (p processor) UpdatePipelineConfig(ctx context.Context, client versioned.Interface, spec *PipelineConfigDetail) (data *PipelineConfigDetail, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update pipeline end", log.Err(err), log.String("name", spec.ObjectMeta.Name))
	}()
	namespace := spec.ObjectMeta.Namespace
	oldPipelineConfig, err := client.DevopsV1alpha1().PipelineConfigs(namespace).Get(spec.ObjectMeta.Name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}

	newPipelineConfig := oldPipelineConfig.DeepCopy()
	newPipelineConfig.Spec = spec.Spec
	p.attachEnvironments(ctx, newPipelineConfig)

	newPipelineConfig.SetAnnotations(common.MergeAnnotations(oldPipelineConfig.ObjectMeta.Annotations, spec.ObjectMeta.Annotations))
	newPipelineConfig.SetLabels(common.MergeAnnotations(oldPipelineConfig.ObjectMeta.Labels, spec.ObjectMeta.Labels))

	oldJson, err := json.Marshal(oldPipelineConfig)
	if err != nil {
		return nil, err
	}

	newJson, err := json.Marshal(newPipelineConfig)
	if err != nil {
		return nil, err
	}

	patchOperations, err := jsonpatch.CreatePatch(oldJson, newJson)
	if err != nil {
		return nil, err
	}

	patch, err := json.Marshal(patchOperations)
	if err != nil {
		return nil, err
	}

	_, err = client.DevopsV1alpha1().PipelineConfigs(namespace).Patch(spec.ObjectMeta.Name, types.JSONPatchType, patch)
	if err != nil {
		return nil, err
	}
	return spec, nil
}

//ConvertToPipelineConfigSlice TODO: need to find a better way to convert items
//without having to do it manually
func ConvertToPipelineConfigSlice(filtered []metav1.Object) (items []v1alpha1.PipelineConfig) {
	items = make([]v1alpha1.PipelineConfig, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.PipelineConfig); ok {
			items = append(items, *cm)
		}
	}
	return
}

// getPipelineConfigSelector get label selector by pipelineConfig
func getPipelineConfigSelector(name string) (selector labels.Selector, err error) {
	selector = labels.NewSelector()
	req, err := labels.NewRequirement("pipelineConfig", selection.DoubleEquals, []string{name})
	if err != nil {
		return nil, fmt.Errorf("labels.NewRequirement error, name: %s , err: %s", name, err.Error())
	}
	selector = selector.Add(*req)
	return selector, nil
}
