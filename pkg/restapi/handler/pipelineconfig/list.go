package pipelineconfig

import (
	appCore "alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"encoding/json"
	goErrors "errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/klog"
	"sort"
	"strconv"
	"strings"
	"sync"
)

// PipelineConfigList a list of PipelineConfigs
type PipelineConfigList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of PipelineConfig.
	Items []PipelineConfig `json:"pipelineconfigs"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// PipelineConfig is a presentation layer view of Kubernetes namespaces. This means it is namespace plus
// additional augmented data we can get from other sources.
type PipelineConfig struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	Spec      v1alpha1.PipelineConfigSpec   `json:"spec"`
	Status    v1alpha1.PipelineConfigStatus `json:"status"`
	Pipelines []v1alpha1.Pipeline           `json:"pipelines"`
}

// GetObjectMeta object meta
func (p PipelineConfig) GetObjectMeta() api.ObjectMeta {
	return p.ObjectMeta
}

// RetryRequest request a retry for pipeline
type RetryRequest struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`

	// empty for now
}

type PipelineConfigTrigger struct {
	Name      string                       `json:"name"`
	Namespace string                       `json:"namespace"`
	Branch    string                       `json:"branch"`
	Commit    string                       `json:"commit"`
	Params    []v1alpha1.PipelineParameter `json:"params"`
}

// LogDetails log details
type LogDetails struct {
	*v1alpha1.PipelineLog
}

// TaskDetails jenkins step details
type TaskDetails struct {
	*v1alpha1.PipelineTask
}

type PipelineTriggerResponse struct {
	*v1alpha1.Pipeline
}

type PipelineConfigDetail struct {
	// same as PipelineConfig
	PipelineConfig

	MulitiBranchPipelines map[string][]v1alpha1.Pipeline `json:"mulitiBranchPipelines"`
	// but add the last execution details
	// TODO
}

type ScanResult struct {
	Message string
	Success bool
}

func toMultiBranchPipelineConfig(config v1alpha1.PipelineConfig, pipelines []v1alpha1.Pipeline, provider devops.AnnotationProvider) (
	pipelineConfig PipelineConfig, multiPipelines map[string][]v1alpha1.Pipeline) {
	pipelineConfig = PipelineConfig{
		ObjectMeta: api.NewObjectMeta(config.ObjectMeta),
		TypeMeta:   api.NewTypeMeta(common.ResourceKindPipelineConfig),
		// data here
		Spec:   config.Spec,
		Status: config.Status,
	}

	if config.Annotations == nil {
		return
	}

	annotations := config.Annotations
	allBranches := make([]string, 0)
	allBranches = append(allBranches, collectBranches(provider.AnnotationsKeyMultiBranchBranchList(), annotations, allBranches)...)
	allBranches = append(allBranches, collectBranches(provider.AnnotationsKeyMultiBranchStaleBranchList(), annotations, allBranches)...)
	allBranches = append(allBranches, collectBranches(provider.AnnotationsKeyMultiBranchPRList(), annotations, allBranches)...)
	allBranches = append(allBranches, collectBranches(provider.AnnotationsKeyMultiBranchStalePRList(), annotations, allBranches)...)

	multiPipelines = make(map[string][]v1alpha1.Pipeline, 0)
	// init it
	for _, branch := range allBranches {
		multiPipelines[branch] = []v1alpha1.Pipeline{}
	}

	lastesPipelines := latestMultiBranchPipeline(allBranches, pipelines, provider)
	// convert data struct
	// branch * max, branch and max will not be a big value
	for branch, items := range lastesPipelines {
		for _, item := range items {
			multiPipelines[branch] = append(multiPipelines[branch], ToPipeline(item))
		}
	}

	return
}

func latestMultiBranchPipeline(branches []string, pipelines []v1alpha1.Pipeline, provider devops.AnnotationProvider) map[string][]v1alpha1.Pipeline {
	max := 5
	branchesMap := make(map[string]struct{}, len(branches))
	for _, b := range branches {
		branchesMap[b] = struct{}{}
	}

	maxBuildNumPipelines := make(map[string][]v1alpha1.Pipeline) // branch:[]pipeline
	currentMinIndexMap := make(map[string]int)                   // branch: minIndex

	// `Time Complexity`: len(pipelines) * max (Most Bad Case), max will not be a big num
	for _, p := range pipelines {
		annotations := p.ObjectMeta.Annotations
		if annotations == nil {
			continue
		}

		if _, ok := annotations[provider.AnnotationsKeyMultiBranchName()]; !ok {
			continue
		}

		branch := annotations[provider.AnnotationsKeyMultiBranchName()]
		_, exists := branchesMap[branch]
		if branch == "" || !exists {
			continue
		}

		currentMinIndex := currentMinIndexMap[branch]

		if _, ok := maxBuildNumPipelines[branch]; !ok {
			maxBuildNumPipelines[branch] = []v1alpha1.Pipeline{}
		}

		if len(maxBuildNumPipelines[branch]) < max {
			// max array is not full
			maxBuildNumPipelines[branch] = append(maxBuildNumPipelines[branch], p)
			currentMinIndex = findMin(maxBuildNumPipelines[branch])
			currentMinIndexMap[branch] = currentMinIndex

		} else {
			// max array is full, we shoul compare it with min value of max array
			minPipeline := maxBuildNumPipelines[branch][currentMinIndex]
			if comparePipeline(p, minPipeline) { //  p > currentMin, we should replace the min element, and find the minIndex again
				maxBuildNumPipelines[branch][currentMinIndex] = p
				currentMinIndex = findMin(maxBuildNumPipelines[branch])
				currentMinIndexMap[branch] = currentMinIndex
			}
		}

	} // after the loop, we will get the latest `max` pipelines of each branch

	// sort the arrar of each branch
	for b, pipes := range maxBuildNumPipelines {
		// the lenght of pipes is not bigger than `max`, so it is not a big spending
		sort.SliceStable(pipes, func(i, j int) bool {
			return comparePipeline(pipes[i], pipes[j])
		})
		maxBuildNumPipelines[b] = pipes
	}

	return maxBuildNumPipelines
}

func findMin(pipelines []v1alpha1.Pipeline) int {
	var min v1alpha1.Pipeline
	var minIndex int
	for index, p := range pipelines {
		if index == 0 {
			min = p
			index = 0
			continue
		}

		if comparePipeline(min, p) { //min > p
			min = p
			minIndex = index
		}
	}
	return minIndex
}

func comparePipeline(left, right v1alpha1.Pipeline) bool {
	// one the pipeline is not sync to  jenkins, so we should compare create timestamp
	if left.Status.Jenkins == nil || right.Status.Jenkins == nil {
		return left.ObjectMeta.GetCreationTimestamp().After(right.ObjectMeta.GetCreationTimestamp().Time)
	}

	// we should compare the build num now
	leftBuild := left.Status.Jenkins.Build
	rightBuild := right.Status.Jenkins.Build

	leftBuildNum, errL := strconv.Atoi(leftBuild)
	rightBuildNum, errR := strconv.Atoi(rightBuild)

	if errL != nil || errR != nil {
		return left.ObjectMeta.GetCreationTimestamp().After(right.ObjectMeta.GetCreationTimestamp().Time)
	}

	return leftBuildNum > rightBuildNum
}

func collectBranches(annotationKey string, annotations map[string]string, allBranches []string) []string {
	if branch, ok := annotations[annotationKey]; ok {
		var branches []string
		if err := json.Unmarshal([]byte(branch), &branches); err == nil {
			allBranches = append(allBranches, branches...)
		}
	}

	return allBranches
}

func generatePipelineFromConfig(config *v1alpha1.PipelineConfig) (pipe *v1alpha1.Pipeline) {
	pipe = &v1alpha1.Pipeline{
		ObjectMeta: common.CloneMeta(config.ObjectMeta),
		Spec: v1alpha1.PipelineSpec{
			JenkinsBinding: config.Spec.JenkinsBinding,
			PipelineConfig: v1alpha1.LocalObjectReference{
				Name: config.GetName(),
			},
			Cause: v1alpha1.PipelineCause{
				Type:    v1alpha1.PipelineCauseTypeManual,
				Message: "Triggered using Alauda DevOps Console",
			},
			RunPolicy: config.Spec.RunPolicy,
			Triggers:  config.Spec.Triggers,
			Strategy:  config.Spec.Strategy,
			Hooks:     config.Spec.Hooks,
			Source:    config.Spec.Source,
		},
	}
	// we should set Name to empty to enable to use generatename
	// https://github.com/kubernetes/apimachinery/blob/release-1.13/pkg/apis/meta/v1/types.go#L105
	pipe.Name = ""
	pipe.GenerateName = config.Name
	return
}

func GetPipelineConfigListAsResourceList(client versioned.Interface, namespace string, dsQuery *dataselect.Query, provider devops.AnnotationProvider) (items []common.ResourceItem) {
	items = []common.ResourceItem{}
	pipelineConfigs, depErr := GetPipelineConfigList(client, common.NewSameNamespaceQuery(namespace), dsQuery, provider)
	if depErr != nil {
		//log.Println(fmt.Sprintf("fetch pipelineconfigs in %s by dsQuery %v; err: %v", namespace, dsQuery, depErr))
	}

	if pipelineConfigs == nil || len(pipelineConfigs.Items) == 0 {
		return
	}

	for _, item := range pipelineConfigs.Items {
		items = append(items, common.ResourceItem{
			Name:      item.ObjectMeta.Name,
			Namespace: item.ObjectMeta.Namespace,
			Kind:      string(item.TypeMeta.Kind),
		})
	}
	return
}

// GetPipelineConfigList returns a PipelineConfigList
func GetPipelineConfigList(client versioned.Interface,
	namespace *common.NamespaceQuery, dsQuery *dataselect.Query, provider devops.AnnotationProvider) (*PipelineConfigList, error) {
	klog.Info("Getting list of pipelineconfigs")
	var (
		configCritical, pipelineCritical error
		configNonCritical                []error
		pipelineConfigList               *v1alpha1.PipelineConfigList
		pipelineList                     *v1alpha1.PipelineList
		wait                             sync.WaitGroup
	)

	// fetch all PipelineConfig
	pipelineConfigList, configCritical = client.DevopsV1alpha1().PipelineConfigs(namespace.ToRequestParam()).List(api.ListEverything)
	configNonCritical, configCritical = errors.HandleError(configCritical)
	if configCritical != nil {
		klog.Errorf("error while listing pipeline configs: %v", configCritical)
		return nil, configCritical
	}

	// filter and page PipelineConfig
	resultConfigs := &PipelineConfigList{
		Items:    make([]PipelineConfig, 0),
		ListMeta: api.ListMeta{TotalItems: len(pipelineConfigList.Items)},
	}

	configCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(pipelineConfigList.Items, provider), dsQuery)
	configs := fromCells(configCells)
	resultConfigs.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	resultConfigs.Errors = configNonCritical
	// prevent sync between different goroutine, we will declare all elements in array
	resultConfigs.Items = make([]PipelineConfig, len(configs))

	klog.Info("filteredTotal:", filteredTotal)

	// fetch list of Pipeline triggered by the PipelineConfigs in current page
	wait = sync.WaitGroup{}
	for i, conf := range configs {
		wait.Add(1)
		go func(index int, conf v1alpha1.PipelineConfig, resultList *PipelineConfigList) {
			defer wait.Done()
			selector, _err := getPipelineConfigSelector(conf.GetName())
			if _err != nil {
				klog.Errorf("Ignore loading pipelines of pipelineconfig %s, because error: %s", conf.GetName(), _err.Error())
				resultList.Items[index] = toPipelineConfig(conf, []v1alpha1.Pipeline{}, provider)
				return
			}

			var (
				pipelines   []v1alpha1.Pipeline
				listOptions = metav1.ListOptions{
					LabelSelector:   selector.String(),
					ResourceVersion: "0",
				}
			)

			pipelineList, pipelineCritical = client.DevopsV1alpha1().Pipelines(namespace.ToRequestParam()).List(listOptions)
			_, pipelineCritical = errors.HandleError(pipelineCritical)
			if pipelineCritical != nil {
				klog.Errorf("error while listing pipelines: %v", pipelineCritical)
			} else {
				pipelines = pipelineList.Items
			}
			klog.V(7).Infof("pipeline's count is %d in pipelineConfig %s", len(pipelineList.Items), conf.GetName())

			resultList.Items[index] = toPipelineConfig(conf, pipelines, provider)
		}(i, conf, resultConfigs)
	}
	wait.Wait()

	return sortPipelineConfigList(resultConfigs, dsQuery), nil
}

// toPipelineConfig append pipelines sorted by CreationTimestamp to PipelineConfig
func toPipelineConfig(config v1alpha1.PipelineConfig, pipelines []v1alpha1.Pipeline, provider devops.AnnotationProvider) PipelineConfig {
	maxLen := 5
	pipelineConfig := PipelineConfig{
		ObjectMeta: api.NewObjectMeta(config.ObjectMeta),
		TypeMeta:   api.NewTypeMeta(api.ResourceKindPipelineConfig),
		Pipelines:  make([]v1alpha1.Pipeline, 0, maxLen),
		// data here
		Spec:   config.Spec,
		Status: config.Status,
	}
	if pipelines != nil && len(pipelines) > 0 {
		// sorted by pipeline creation timestamp desc
		sort.SliceStable(pipelines, func(i, j int) bool {
			if kind, ok := config.Labels[v1alpha1.LabelPipelineKind]; ok && kind == v1alpha1.LabelPipelineKindMultiBranch {
				if pipelines[i].GetObjectMeta().CreationTimestamp.Time.Equal(pipelines[j].GetObjectMeta().CreationTimestamp.Time) {
					return pipelines[i].Annotations[provider.AnnotationsKeyMultiBranchName()] > pipelines[j].Annotations[provider.AnnotationsKeyMultiBranchName()]
				}
				return pipelines[i].GetObjectMeta().CreationTimestamp.After(pipelines[j].GetObjectMeta().CreationTimestamp.Time)
			}
			return comparePipeline(pipelines[i], pipelines[j])
		})

		// only return the first five data
		for _, p := range pipelines {
			if len(pipelineConfig.Pipelines) >= maxLen {
				break
			}

			pipelineConfig.Pipelines = append(pipelineConfig.Pipelines, ToPipeline(p))
		}
	}
	return pipelineConfig
}

func ToPipeline(pipe v1alpha1.Pipeline) v1alpha1.Pipeline {
	return toPipeline(pipe)
}
func toPipeline(pipe v1alpha1.Pipeline) v1alpha1.Pipeline {
	pipeline := v1alpha1.Pipeline{
		ObjectMeta: pipe.ObjectMeta,
		TypeMeta:   metav1.TypeMeta{Kind: common.ResourceKindPipeline},
		// data here
		Spec:   pipe.Spec,
		Status: pipe.Status,
	}
	return pipeline
}

// sortPipelineConfigList sort PipelineConfigList by pipeline CreationTimestamp
func sortPipelineConfigList(configList *PipelineConfigList, dsQuery *dataselect.Query) *PipelineConfigList {
	if dsQuery != nil && dsQuery.SortQuery != nil && len(dsQuery.SortQuery.SortByList) > 0 {
		for _, sortBy := range dsQuery.SortQuery.SortByList {
			if sortBy.Property == common.PipelineCreationTimestampProperty {
				sortByCreationTime(configList, sortBy.Ascending)
			} else if sortBy.Property == common.NameProperty {
				sortByName(configList, sortBy.Ascending)
			}
		}
	}

	return configList
}

func sortByCreationTime(configList *PipelineConfigList, asc bool) {
	sort.SliceStable(configList.Items, func(i, j int) bool {
		before := configList.Items[i].Pipelines
		after := configList.Items[j].Pipelines

		if asc {
			if len(before) == 0 {
				return true
			}
			if len(after) == 0 {
				return false
			}
			return !before[0].GetObjectMeta().CreationTimestamp.After(after[0].GetObjectMeta().CreationTimestamp.Time)
		}

		if len(before) == 0 {
			return false
		}
		if len(after) == 0 {
			return true
		}
		return before[0].GetObjectMeta().CreationTimestamp.After(after[0].GetObjectMeta().CreationTimestamp.Time)
	})
}

func sortByName(configList *PipelineConfigList, asc bool) {
	sort.SliceStable(configList.Items, func(i, j int) bool {
		before := configList.Items[i]
		after := configList.Items[j]

		order := (strings.Compare(before.GetObjectMeta().Name, after.GetObjectMeta().Name) > 0)
		if asc {
			return !order
		}

		return order
	})
}

//func GenerateFromCore(app appCore.Application) ([]PipelineConfig, error) {
//	result := make([]PipelineConfig, 0)
//	pipelineConfigList, err := GetFormCore(app)
//	if err != nil {
//		return result, err
//	}
//	pipelines := make([]v1alpha1.Pipeline, 0)
//	for _, config := range pipelineConfigList {
//		result = append(result, toPipelineConfig(config, pipelines))
//	}
//	return result, nil
//}

func GetFormCore(app appCore.Application) ([]v1alpha1.PipelineConfig, error) {
	list := make([]v1alpha1.PipelineConfig, 0)
	for _, r := range app.Resources {
		if strings.ToLower(r.GetKind()) == api.ResourceKindPipelineConfig {
			item, err := ConverToOriginal(&r)
			if err != nil {
				return list, err
			}
			list = append(list, *item)
		}
	}
	return list, nil
}

func ConverToOriginal(unstr *unstructured.Unstructured) (*v1alpha1.PipelineConfig, error) {
	if unstr == nil {
		return nil, goErrors.New("input unstr is nil")
	}
	data, err := json.Marshal(unstr)
	if err != nil {
		return nil, err
	}
	output := &v1alpha1.PipelineConfig{}
	err = json.Unmarshal(data, output)
	return output, err
}
