package pipelineconfig_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelineconfig"
	"alauda.io/devops-apiserver/pkg/restapi/handler/testtools"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"time"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

//TODO the fake client of client.Client doesn't support label selector now, so we set this unit test to pending
var _ = PDescribe("Processor.Pipelineconfig", func() {
	var (
		processor   pipelineconfig.Processor
		ctx         context.Context
		cacheClient client.Client
		query       *dataselect.Query

		list      *pipelineconfig.PipelineConfigList
		err       error
		namespace *common.NamespaceQuery

		defaultSortQuery *dataselect.SortQuery
		defaultPagequery *dataselect.PaginationQuery
		defaultFilter    *dataselect.FilterQuery
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery = &dataselect.PaginationQuery{
			ItemsPerPage: 10,
			Page:         0,
		}
		defaultSortQuery = &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter = &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		namespace = common.NewNamespaceQuery([]string{"default"})
		cacheClient = testtools.NewFakeClient()
		processor = pipelineconfig.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("should return pipeline", func() {
		cacheClient = testtools.NewFakeClient(
			GetPipelineConfigList("pipelinename", "default"),
		)
		list, err = processor.ListPipelineConfig(ctx, cacheClient, query, namespace)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Items).To(HaveLen(2), "should not have any items")
	})

	Context("sort pipelineconfig list by creationTimestamp as asc", func() {
		BeforeEach(func() {
			defaultSortQuery = &dataselect.SortQuery{
				SortByList: []dataselect.SortBy{{
					Property:  common.PipelineCreationTimestampProperty,
					Ascending: false,
				}},
			}
			query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		})

		It("should success", func() {
			cacheClient = testtools.NewFakeClient(
				GetPipelineConfigList("pipelinename", "default"),
				GetPipelineList("pipeline", "pipelinename", "default", 100),
				GetPipelineList("pipeline2", "pipelinename1", "default", 200),
			)
			list, err = processor.ListPipelineConfig(ctx, cacheClient, query, namespace)

			Expect(err).To(BeNil(), "should not return an error")
			Expect(list).ToNot(BeNil(), "should return a list")
			Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
			Expect(list.Items).To(HaveLen(2), "should not have any items")
			Expect(list.Items[0].ObjectMeta.Name).To(Equal("pipelinename1"))
		})
	})

	Context("sort pipelineconfig list by creationTimestamp as desc - added for DEVOPS-2849", func() {
		BeforeEach(func() {
			defaultSortQuery = &dataselect.SortQuery{
				SortByList: []dataselect.SortBy{{
					Property:  common.PipelineCreationTimestampProperty,
					Ascending: true,
				}},
			}
			query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		})

		It("should success", func() {
			cacheClient = testtools.NewFakeClient(
				GetPipelineConfigList("pipelinename", "default"),
				GetPipelineList("pipeline", "pipelinename", "default", 100),
				GetPipelineList("pipeline2", "pipelinename1", "default", 200),
			)
			list, err = processor.ListPipelineConfig(ctx, cacheClient, query, namespace)

			Expect(err).To(BeNil(), "should not return an error")
			Expect(list).ToNot(BeNil(), "should return a list")
			Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
			Expect(list.Items).To(HaveLen(2), "should not have any items")
			Expect(list.Items[0].ObjectMeta.Name).To(Equal("pipelinename"))
		})
	})

	Context("sort pipelineconfig list by creationTimestamp as default strategy (desc) - added for DEVOPS-2849", func() {
		BeforeEach(func() {
			defaultPagequery := &dataselect.PaginationQuery{
				ItemsPerPage: 10,
				Page:         0,
			}
			defaultSortQuery = &dataselect.SortQuery{
				SortByList: []dataselect.SortBy{{
					Property: common.PipelineCreationTimestampProperty,
				}},
			}
			query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		})

		It("should success", func() {
			cacheClient = testtools.NewFakeClient(
				GetPipelineConfigList("pipelinename", "default"),
				GetPipelineList("pipeline", "pipelinename", "default", 100),
				GetPipelineList("pipeline2", "pipelinename1", "default", 200),
			)
			list, err = processor.ListPipelineConfig(ctx, cacheClient, query, namespace)

			Expect(err).To(BeNil(), "should not return an error")
			Expect(list).ToNot(BeNil(), "should return a list")
			Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
			Expect(list.Items).To(HaveLen(2), "should not have any items")
			Expect(list.Items[0].ObjectMeta.Name).To(Equal("pipelinename1"))
		})
	})

	Context("sort pipelineconfig list by creationTimestamp as default strategy (asc) - more than one page", func() {
		BeforeEach(func() {
			defaultPagequery = &dataselect.PaginationQuery{
				ItemsPerPage: 2,
				Page:         0,
			}
			defaultSortQuery = &dataselect.SortQuery{
				SortByList: []dataselect.SortBy{{
					Property:  common.PipelineCreationTimestampProperty,
					Ascending: true,
				}},
			}
			query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)

			var fakeTime = 1000
			cacheClient = testtools.NewFakeClient(
				GetPipelineConfigList("pipelinename", "default"),
				GetPipelineList("pipeline1", "pipelinename", "default", time.Duration(fakeTime+1000)),
				GetPipelineList("pipeline2", "pipelinename1", "default", time.Duration(fakeTime+1500)),

				GetPipelineConfigList("pipelinename-1", "default"),
				GetPipelineList("pipeline3", "pipelinename-1", "default", time.Duration(fakeTime+2000)),
				GetPipelineList("pipeline4", "pipelinename-11", "default", time.Duration(fakeTime+2500)),

				GetPipelineConfigList("pipelinename-2", "default"),
				GetPipelineList("pipeline5", "pipelinename-2", "default", time.Duration(fakeTime+3000)),
				GetPipelineList("pipeline6", "pipelinename-21", "default", time.Duration(fakeTime+3500)),

				GetPipelineConfigList("pipelinename-3", "default"),
				GetPipelineList("pipeline7", "pipelinename-3", "default", time.Duration(fakeTime+4000)),
				GetPipelineList("pipeline8", "pipelinename-31", "default", time.Duration(fakeTime+4500)),
			)
		})

		It("test many pipelineconfig with more than one page with ascending order", func() {
			list, err = processor.ListPipelineConfig(ctx, cacheClient, query, namespace)
			Expect(err).To(BeNil(), "should not return an error")
			Expect(list).ToNot(BeNil(), "should return a list")
			Expect(list.ListMeta.TotalItems).To(Equal(8))
			Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
			Expect(len(list.Items) <= defaultPagequery.ItemsPerPage).To(BeTrue(), "expected %d, actual is %d", defaultPagequery.ItemsPerPage, len(list.Items))
			Expect(list.Items[0].ObjectMeta.Name).To(Equal("pipelinename"), "get the error order of the pipelineconfig list")
		})

		Context("with desc order", func() {
			BeforeEach(func() {
				defaultSortQuery = &dataselect.SortQuery{
					SortByList: []dataselect.SortBy{{
						Property:  common.PipelineCreationTimestampProperty,
						Ascending: false,
					}},
				}
				query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
			})

			It("", func() {
				list, err = processor.ListPipelineConfig(ctx, cacheClient, query, namespace)
				Expect(list.Items[0].ObjectMeta.Name).To(Equal("pipelinename-31"), "get the error order of the pipelineconfig list")
			})
		})
	})

	Context("pipelineconfig items cannot be nil [DEVOPS-3111]", func() {
		BeforeEach(func() {
			defaultSortQuery = &dataselect.SortQuery{}
			query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		})

		It("should success", func() {
			cacheClient = testtools.NewFakeClient()
			list, err = processor.ListPipelineConfig(ctx, cacheClient, query, namespace)

			Expect(err).To(BeNil(), "should not return an error")
			Expect(list).ToNot(BeNil(), "should return a list")
			Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		})
	})
})

func GetPipelineConfigList(name, namespace string) *v1alpha1.PipelineConfigList {
	pipelineconfiglist := &v1alpha1.PipelineConfigList{
		TypeMeta: metav1.TypeMeta{
			Kind: "pipelineconfig",
		},

		Items: []v1alpha1.PipelineConfig{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha1.PipelineConfigSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name + "1",
					Namespace: namespace,
				},
				Spec: v1alpha1.PipelineConfigSpec{},
			},
		},
	}
	return pipelineconfiglist
}

func GetPipelineList(name, pipelineConfigName, namespace string, createTime time.Duration) *v1alpha1.PipelineList {
	now := time.Now()

	pipelinelist := &v1alpha1.PipelineList{
		TypeMeta: metav1.TypeMeta{
			Kind: "pipeline",
		},

		Items: []v1alpha1.Pipeline{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
					Labels: map[string]string{
						"pipelineConfig": pipelineConfigName,
					},
					CreationTimestamp: metav1.Time{Time: now.Add(createTime)},
				},
				Spec: v1alpha1.PipelineSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name + "1",
					Namespace: namespace,
					Labels: map[string]string{
						"pipelineConfig": pipelineConfigName,
					},
					CreationTimestamp: metav1.Time{Time: now.Add(createTime)},
				},
				Spec: v1alpha1.PipelineSpec{},
			},
		},
	}

	return pipelinelist
}
