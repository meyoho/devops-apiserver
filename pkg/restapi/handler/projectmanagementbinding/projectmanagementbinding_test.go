package projectmanagementbinding_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/projectmanagementbinding"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Processor.JenkinsBinding", func() {
	var (
		processor projectmanagementbinding.Processor
		ctx       context.Context
		client    *fake.Clientset
		query     *dataselect.Query

		list              *projectmanagementbinding.ProjectManagementBindingList
		err               error
		namespace         *common.NamespaceQuery
		prbindinginstance *v1alpha1.ProjectManagementBinding
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 1,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		namespace = common.NewNamespaceQuery([]string{"default"})
		client = fake.NewSimpleClientset()
		processor = projectmanagementbinding.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("should return only one prbinding", func() {
		client = fake.NewSimpleClientset(
			GetProjectManagementBindingList("testbinding", "default"),
		)
		list, err = processor.ListProjectManagementBinding(ctx, client, query, namespace)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Items).To(HaveLen(1), "should not have any items")
	})

	It("should retrieve a jenkinsbinding", func() {
		client = fake.NewSimpleClientset(
			GetPMBinding("testbinding", "default"),
		)
		prbindinginstance, err = processor.GetProjectManagementBinding(ctx, client, "default", "testbinding")
		Expect(err).To(BeNil())
		Expect(prbindinginstance.Spec.ProjectManagement.Name).To(Equal("prname"))

	})

})

func GetPMBinding(name, namespace string) *v1alpha1.ProjectManagementBinding {
	prbindinginstance := &v1alpha1.ProjectManagementBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: v1alpha1.ProjectManagementBindingSpec{
			ProjectManagement: v1alpha1.LocalObjectReference{
				Name: "prname",
			},
		},
	}
	return prbindinginstance
}

func GetProjectManagementBindingList(name, namespace string) *v1alpha1.ProjectManagementBindingList {
	pmbindinglist := &v1alpha1.ProjectManagementBindingList{
		TypeMeta: metav1.TypeMeta{
			Kind: "projectmanagementbinding",
		},

		Items: []v1alpha1.ProjectManagementBinding{
			{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{},
					Name:        name,
					Namespace:   namespace,
				},
				Spec: v1alpha1.ProjectManagementBindingSpec{
					ProjectManagement: v1alpha1.LocalObjectReference{
						Name: "projectname",
					},
				},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{},
					Name:        name + "1",
					Namespace:   namespace,
				},
				Spec: v1alpha1.ProjectManagementBindingSpec{
					ProjectManagement: v1alpha1.LocalObjectReference{
						Name: "projectname1",
					},
				},
			},
		},
	}
	return pmbindinglist
}
