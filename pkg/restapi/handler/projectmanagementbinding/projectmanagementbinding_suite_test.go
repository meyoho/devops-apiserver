package projectmanagementbinding_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestProjectmanagementbinding(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("jenkinsbinding.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/jenkinsbinding", []Reporter{junitReporter})
}
