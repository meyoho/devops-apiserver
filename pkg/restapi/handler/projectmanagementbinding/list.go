package projectmanagementbinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
)

// ProjectManagementBindingList contains a list of ProjectManagementBinding in the cluster.
type ProjectManagementBindingList struct {
	ListMeta common.ListMeta `json:"listMeta"`

	// Unordered list of ProjectManagementBinding.
	Items []v1alpha1.ProjectManagementBinding `json:"projectmanagebindings"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type CronCheckResult struct {
	Next     string `json:"next"`
	Previous string `json:"previous"`
	SanityZh string `json:"sanity_zh_cn"`
	SanityEn string `json:"sanity_en"`
	Error    string `json:"error"`
}

type APIResponse struct {
	Data   CronCheckResult `json:"data"`
	Status string          `json:"status"`
}
