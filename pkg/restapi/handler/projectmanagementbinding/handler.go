package projectmanagementbinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/projectmanagement"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
	"strconv"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for ProjectManagement
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListProjectManagementBinding(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*ProjectManagementBindingList, error)
	DeleteProjectManagementBinding(ctx context.Context, client versioned.Interface, namespace, name string) error
	GetProjectManagementBinding(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.ProjectManagementBinding, error)
	CreateProjectManagementBinding(ctx context.Context, client versioned.Interface, projectmanagementbinding *v1alpha1.ProjectManagementBinding, namespace string) (*v1alpha1.ProjectManagementBinding, error)
	UpdateProjectManagementBinding(ctx context.Context, client versioned.Interface, projectmanagementbinding, oldprojectmanagementbinding *v1alpha1.ProjectManagementBinding, namespace, name string) (*v1alpha1.ProjectManagementBinding, error)
	GetProjectManagementIssueOptions(ctx context.Context, client versioned.Interface, namespace, name, issuetype string) (*v1alpha1.IssueFilterDataList, error)
	GetProjectManagementIssueDetail(ctx context.Context, client versioned.Interface, namespace, name, issuekey string) (*v1alpha1.IssueDetail, error)
	GetProjectManagementIssueList(ctx context.Context, client versioned.Interface, namespace, name string, listissueoption *v1alpha1.ListIssuesOptions) (*v1alpha1.IssueDetailList, error)
}

// New builder method for projectmanagementbinding.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ProjectManagementBinding related APIs").ApiVersion("v1").Path("/api/v1/projectmanagementbinding")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "projectmanagementbinding")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List ProjectManagementBinding instances`).
					To(handler.ListProjectManagementBinding).
					Writes(ProjectManagementBindingList{}).
					Returns(http.StatusOK, "OK", ProjectManagementBindingList{}),
			),
		),
	)
	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List ProjectManagementBinding instances`).
					To(handler.ListProjectManagementBinding).
					Writes(ProjectManagementBindingList{}).
					Returns(http.StatusOK, "OK", ProjectManagementBindingList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create ProjectManagementBinding instance").
				To(handler.CreateProjectManagementBinding).
				Returns(http.StatusOK, "Create ProjectManagementBinding instance", v1alpha1.ProjectManagementBinding{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ProjectManagementbinding details").
				Param(ws.PathParameter("name", "name of the projectmanagementbinding")).
				Param(ws.PathParameter("namespace", "namespace of the projectmanagementbinding")).
				To(handler.GetProjectManagementBinding).
				Returns(http.StatusOK, "Get ProjectManagementBinding details Complete", v1alpha1.ProjectManagementBinding{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete ProjectManagement instance").
				Param(ws.PathParameter("name", "name of the projectmanagementbinding")).
				Param(ws.PathParameter("namespace", "namespace of the projectmanagementbinding")).
				To(handler.DeleteProjectManagementBinding).
				Returns(http.StatusOK, "Retrieve ProjectManagementbinding instance", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update ProjectManagementbinding instance").
				Param(ws.PathParameter("name", "name of the projectmanagement")).
				Param(ws.PathParameter("namespace", "namespace of the projectmanagementbinding")).
				To(handler.UpdateProjectManagementBinding).
				Returns(http.StatusOK, "Update ProjectManagementbinding instance", v1alpha1.ProjectManagementBinding{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/issueoptions").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ProjectManagementbinding instance issue options").
				Param(ws.PathParameter("name", "name of the projectmanagement")).
				Param(ws.PathParameter("namespace", "namespace of the projectmanagementbinding")).
				Param(ws.QueryParameter("type", "type of issue option")).
				To(handler.GetProjectManagementIssueOptions).
				Returns(http.StatusOK, "Get projectmanagement issueoptions", v1alpha1.IssueFilterDataList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/issue").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ProjectManagementbinding instance issue options").
				Param(ws.PathParameter("name", "name of the projectmanagement")).
				Param(ws.PathParameter("namespace", "namespace of the projectmanagementbinding")).
				Param(ws.QueryParameter("key", "key of issue")).
				To(handler.GetProjectManagementIssueDetail).
				Returns(http.StatusOK, "get projectmanagementbinding detail", v1alpha1.IssueDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/issueslist").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ProjectManagementbinding instance issue options").
				Param(ws.PathParameter("name", "name of the projectmanagement")).
				Param(ws.PathParameter("namespace", "namespace of the projectmanagementbinding")).
				Param(ws.QueryParameter("page", "page of issue")).
				Param(ws.QueryParameter("itemsPerPage", "pagesize of issue")).
				Param(ws.QueryParameter("project", "project of issue")).
				Param(ws.QueryParameter("type", "type of issue")).
				Param(ws.QueryParameter("priority", "priority of issue")).
				Param(ws.QueryParameter("status", "status of issue")).
				Param(ws.QueryParameter("summary", "summary of issue")).
				Param(ws.QueryParameter("issuekey", "key of issue")).
				Param(ws.QueryParameter("orderby", "sort option")).
				Param(ws.QueryParameter("sort", "sort DESC or ASC")).
				To(handler.GetProjectManagementIssueList).
				Returns(http.StatusOK, "get projectmanagementbinding issue list", v1alpha1.IssueDetailList{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListProjectManagement list projectmanagement instances
func (h Handler) ListProjectManagementBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	projectmanagementbinding, err := h.Processor.ListProjectManagementBinding(req.Request.Context(), client, query, common.ParseNamespacePathParameter(req))
	common.WriteResponseBySort(projectmanagementbinding, err, req, res, h)
}

//DeleteProjectManagement Delete projectmanagement instance
func (h Handler) DeleteProjectManagementBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteProjectManagementBinding(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//GetProjectManagementBinding Get projectmanagementbinding instance
func (h Handler) GetProjectManagementBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.GetProjectManagementBinding(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//CreateProjectManagementBinding create projectmanagementbinding instance
func (h Handler) CreateProjectManagementBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newProjectManagementBinding := new(v1alpha1.ProjectManagementBinding)

	if err := req.ReadEntity(newProjectManagementBinding); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	_, err := projectmanagement.AuthorizeService(client,
		newProjectManagementBinding.Spec.ProjectManagement.Name, newProjectManagementBinding.GetSecretName(), newProjectManagementBinding.GetSecretNamespace())
	if err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	namespace := req.PathParameter("namespace")

	data, err := h.Processor.CreateProjectManagementBinding(req.Request.Context(), client, newProjectManagementBinding, namespace)
	h.WriteResponse(data, err, req, res)
}

//UpdateProjectManagementBinding update projectmanagementbinding instance
func (h Handler) UpdateProjectManagementBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newProjectManagementBinding := new(v1alpha1.ProjectManagementBinding)

	if err := req.ReadEntity(newProjectManagementBinding); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	_, err := projectmanagement.AuthorizeService(client,
		newProjectManagementBinding.Spec.ProjectManagement.Name, newProjectManagementBinding.GetSecretName(), newProjectManagementBinding.GetSecretNamespace())
	if err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	oldProjectManagementBinding, err := h.Processor.GetProjectManagementBinding(req.Request.Context(), client, namespace, name)
	if err != nil {
		errors.HandleInternalError(res, err)
		return
	}

	data, err := h.Processor.UpdateProjectManagementBinding(req.Request.Context(), client, newProjectManagementBinding, oldProjectManagementBinding, namespace, name)
	h.WriteResponse(data, err, req, res)
}

func (h Handler) GetProjectManagementIssueOptions(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.GetProjectManagementIssueOptions(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"), req.QueryParameter("type"))
	h.WriteResponse(data, err, req, res)

}

func (h Handler) GetProjectManagementIssueDetail(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.GetProjectManagementIssueDetail(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"), req.QueryParameter("key"))
	h.WriteResponse(data, err, req, res)

}

func (h Handler) GetProjectManagementIssueList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	issuelistoption := v1alpha1.ListIssuesOptions{}

	page, err := strconv.Atoi(req.QueryParameter("page"))
	if err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	pagesize, err := strconv.Atoi(req.QueryParameter("itemsPerPage"))
	if err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	issuelistoption.Page = page
	issuelistoption.PageSize = pagesize
	issuelistoption.Project = req.QueryParameter("project")
	issuelistoption.Type = req.QueryParameter("type")
	issuelistoption.Priority = req.QueryParameter("priority")
	issuelistoption.Status = req.QueryParameter("status")
	issuelistoption.Summary = req.QueryParameter("summary")
	issuelistoption.IssueKey = req.QueryParameter("issuekey")
	issuelistoption.OrderBy = req.QueryParameter("orderby")
	issuelistoption.Sort = req.QueryParameter("sort")

	data, err := h.Processor.GetProjectManagementIssueList(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"), &issuelistoption)
	h.WriteResponse(data, err, req, res)

}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
