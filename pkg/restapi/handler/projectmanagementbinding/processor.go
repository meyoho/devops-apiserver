package projectmanagementbinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListProjectManagement list projectmanagement instances
func (p processor) ListProjectManagementBinding(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *ProjectManagementBindingList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list projectmanagementbinding end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	// fetch data from list
	projectmanagementlist, err := client.DevopsV1alpha1().ProjectManagementBindings(namespace.ToRequestParam()).List(v1alpha1.ListOptions())

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &ProjectManagementBindingList{
		Items:    make([]v1alpha1.ProjectManagementBinding, 0),
		ListMeta: common.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(projectmanagementlist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	annotationprovider := localcontext.ExtraConfig(ctx).AnnotationProvider
	data.Items = ConvertToProjectManagementSlice(result, annotationprovider)
	data.ListMeta = common.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeleteProjectManagement delete projectmanagementbinding instance
func (p processor) DeleteProjectManagementBinding(ctx context.Context, client versioned.Interface, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete projectmanagementbinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().ProjectManagementBindings(namespace).Delete(name, &metav1.DeleteOptions{})
	return
}

// GetProjectManagementBinding retrieve projectmanagementbinding instance
func (p processor) GetProjectManagementBinding(ctx context.Context, client versioned.Interface, namespace, name string) (data *v1alpha1.ProjectManagementBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get projectmanagementbinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().ProjectManagementBindings(namespace).Get(name, common.GetOptionsInCache)
	return
}

//CreateProjectManagementBinding create projectmanagementbinding instance
func (p processor) CreateProjectManagementBinding(ctx context.Context, client versioned.Interface, projectmanagementbinding *v1alpha1.ProjectManagementBinding, namespace string) (data *v1alpha1.ProjectManagementBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create projectmanagementbinding end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().ProjectManagementBindings(namespace).Create(projectmanagementbinding)
	return
}

//UpdateProjectManagementBinding update a projectmanagementbinding instance
func (p processor) UpdateProjectManagementBinding(ctx context.Context, client versioned.Interface, projectmanagementbinding, oldprojectmanagementbinding *v1alpha1.ProjectManagementBinding, namespace, name string) (data *v1alpha1.ProjectManagementBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update projectmanagementbinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	binding := oldprojectmanagementbinding.DeepCopy()
	binding.SetAnnotations(projectmanagementbinding.GetAnnotations())
	binding.Spec = projectmanagementbinding.Spec

	data, err = client.DevopsV1alpha1().ProjectManagementBindings(namespace).Update(binding)
	return
}

func (p processor) GetProjectManagementIssueOptions(ctx context.Context, client versioned.Interface, namespace, name, issuetype string) (result *v1alpha1.IssueFilterDataList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get projectmanagementbinding issuesoption end", log.Err(err))
	}()
	issuetypeoption := v1alpha1.IssueSearchOptions{
		Type: issuetype,
	}
	result, err = client.DevopsV1alpha1().ProjectManagementBindings(namespace).IssueOptions(name, &issuetypeoption)
	return
}

func (p processor) GetProjectManagementIssueDetail(ctx context.Context, client versioned.Interface, namespace, name, issuekey string) (result *v1alpha1.IssueDetail, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get projectmanagementbinding issue detail end", log.Err(err))
	}()
	issuekeyoption := v1alpha1.IssueKeyOptions{Key: issuekey}
	result, err = client.DevopsV1alpha1().ProjectManagementBindings(namespace).IssueDetail(name, &issuekeyoption)
	return
}

func (p processor) GetProjectManagementIssueList(ctx context.Context, client versioned.Interface, namespace, name string, listissueoption *v1alpha1.ListIssuesOptions) (result *v1alpha1.IssueDetailList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get projectmanagementbinding issue list end", log.Err(err))
	}()
	result, err = client.DevopsV1alpha1().ProjectManagementBindings(namespace).IssueList(name, listissueoption)
	return
}

// ConvertToProjectManagementSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToProjectManagementSlice(filtered []metav1.Object, provider devops.AnnotationProvider) (items []v1alpha1.ProjectManagementBinding) {
	items = make([]v1alpha1.ProjectManagementBinding, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.ProjectManagementBinding); ok {
			cm.ObjectMeta.Annotations[provider.AnnotationsKeyToolType()] = v1alpha1.ToolChainProjectManagementName
			cm.Kind = common.ResourceKindProjectManagementBinding
			items = append(items, *cm)
		}
	}
	return
}
