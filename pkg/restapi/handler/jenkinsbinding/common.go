package jenkinsbinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
)

type JenkinsBindingCell v1alpha1.JenkinsBinding

func (self JenkinsBindingCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case common.NameProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Name)
	case common.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.ObjectMeta.CreationTimestamp.Time)
	case common.NamespaceProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Namespace)
	case common.JenkinsProperty:
		return dataselect.StdLowerComparableString(self.Spec.Jenkins.Name)
	default:
		// if name is not supported then just return a constant dummy value, sort will have no effect.
		return nil
	}
}

func toCells(std []v1alpha1.JenkinsBinding) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = JenkinsBindingCell(std[i])
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []v1alpha1.JenkinsBinding {
	std := make([]v1alpha1.JenkinsBinding, len(cells))
	for i := range std {
		std[i] = v1alpha1.JenkinsBinding(cells[i].(JenkinsBindingCell))
	}
	return std
}
