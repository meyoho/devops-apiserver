package jenkinsbinding

import (
	"context"
	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for Jenkins
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListJenkinsBinding(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*JenkinsBindingList, error)
	DeleteJenkinsBinding(ctx context.Context, client versioned.Interface, namespace, name string) error
	GetJenkinsBinding(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.JenkinsBinding, error)
	CreateJenkinsBinding(ctx context.Context, client versioned.Interface, jenkinsbinding *v1alpha1.JenkinsBinding, namespace string) (*v1alpha1.JenkinsBinding, error)
	UpdateJenkinsBinding(ctx context.Context, client versioned.Interface, jenkinsbinding, oldjenkinsbinding *v1alpha1.JenkinsBinding, namespace, name string) (*v1alpha1.JenkinsBinding, error)

	GetJenkinsBindingResource(ctx context.Context, client versioned.Interface, namespace, name string) (resourceList *common.ResourceList, err error)
	JenkinsBindingCronCheck(ctx context.Context, client versioned.Interface, namespace, name, cron string) (apiresponse APIResponse, err error)
	GetJenkinsBindingLabels(ctx context.Context, client versioned.Interface, namespace, name, labelMatcher string) (apiresponse *LabelsResult, err error)
	GetJenkinsBindingPlugins(ctx context.Context, client versioned.Interface, namespace, name string) (apiresponse *PluginsResult, err error)
}

// New builder method for jenkinsbinding.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("JenkinsBinding related APIs").ApiVersion("v1").Path("/api/v1/jenkinsbinding")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "jenkinsbinding")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List JenkinsBinding instances`).
					To(handler.ListJenkinsBinding).
					Writes(JenkinsBindingList{}).
					Returns(http.StatusOK, "OK", JenkinsBindingList{}),
			),
		),
	)
	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List JenkinsBinding instances`).
					To(handler.ListJenkinsBinding).
					Writes(JenkinsBindingList{}).
					Returns(http.StatusOK, "OK", JenkinsBindingList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create JenkinsBinding instance").
				To(handler.CreateJenkinsBinding).
				Returns(http.StatusOK, "Create JenkinsBinding instance", v1alpha1.JenkinsBinding{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get Jenkinsbinding details").
				Param(ws.PathParameter("name", "name of the jenkinsbinding")).
				Param(ws.PathParameter("namespace", "namespace of the jenkinsbinding")).
				To(handler.GetJenkinsBinding).
				Returns(http.StatusOK, "Get JenkinsBinding details Complete", v1alpha1.JenkinsBinding{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete Jenkins instance").
				Param(ws.PathParameter("name", "name of the jenkinsbinding")).
				Param(ws.PathParameter("namespace", "namespace of the jenkinsbinding")).
				To(handler.DeleteJenkinsBinding).
				Returns(http.StatusOK, "Retrieve Jenkinsbinding instance", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update Jenkinsbinding instance").
				Param(ws.PathParameter("name", "name of the jenkins")).
				Param(ws.PathParameter("namespace", "namespace of the jenkinsbinding")).
				To(handler.UpdateJenkinsBinding).
				Returns(http.StatusOK, "Update Jenkinsbinding instance", v1alpha1.JenkinsBinding{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/resources").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("retrieve resources associated with jenkinsbinding").
				Param(ws.PathParameter("name", "name of the jenkins")).
				Param(ws.PathParameter("namespace", "namespace of the jenkinsbinding")).
				To(handler.GetJenkinsBindingResource).
				Returns(http.StatusOK, "Get Jenkinsbinding resource instance", common.ResourceList{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/croncheck").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("cron syntax check").
				Param(ws.PathParameter("name", "name of the jenkins")).
				Param(ws.PathParameter("namespace", "namespace of the jenkinsbinding")).
				To(handler.JenkinsBindingCronCheck).
				Returns(http.StatusOK, "Update Jenkinsbinding instance", CronCheckResult{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/labels").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("retrieve labels associated with jenkins").
				Param(ws.PathParameter("name", "name of the jenkinsbinding")).
				Param(ws.PathParameter("namespace", "namespace of the jenkinsbinding")).
				Param(ws.QueryParameter("labelMatcher", "regex matcher to filter labels")).
				To(handler.GetJenkinsBindingLabels).
				Returns(http.StatusOK, "Get available labels", LabelsResult{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/plugins").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("retrieve plugins associated with jenkins").
				Param(ws.PathParameter("name", "name of the jenkinsbinding")).
				Param(ws.PathParameter("namespace", "namespace of the jenkinsbinding")).
				To(handler.GetJenkinsBindingPlugins).
				Returns(http.StatusOK, "Get current plugins", LabelsResult{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListJenkins list jenkins instances
func (h Handler) ListJenkinsBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	jenkinsbinding, err := h.Processor.ListJenkinsBinding(req.Request.Context(), client, query, common.ParseNamespacePathParameter(req))
	common.WriteResponseBySort(jenkinsbinding, err, req, res, h)
}

//DeleteJenkins Delete jenkins instance
func (h Handler) DeleteJenkinsBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteJenkinsBinding(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//GetJenkinsBinding Get jenkinsbinding instance
func (h Handler) GetJenkinsBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.GetJenkinsBinding(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//CreateJenkinsBinding create jenkinsbinding instance
func (h Handler) CreateJenkinsBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newJenkinsBinding := new(v1alpha1.JenkinsBinding)

	if err := req.ReadEntity(newJenkinsBinding); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	_, err := jenkins.AuthorizeService(client,
		newJenkinsBinding.Spec.Jenkins.Name, newJenkinsBinding.GetSecretName(), newJenkinsBinding.GetSecretNamespace())
	if err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	namespace := req.PathParameter("namespace")

	data, err := h.Processor.CreateJenkinsBinding(req.Request.Context(), client, newJenkinsBinding, namespace)
	h.WriteResponse(data, err, req, res)
}

//UpdateJenkinsBinding update jenkinsbinding instance
func (h Handler) UpdateJenkinsBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newJenkinsBinding := new(v1alpha1.JenkinsBinding)

	if err := req.ReadEntity(newJenkinsBinding); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	_, err := jenkins.AuthorizeService(client,
		newJenkinsBinding.Spec.Jenkins.Name, newJenkinsBinding.GetSecretName(), newJenkinsBinding.GetSecretNamespace())
	if err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	oldJenkinsBinding, err := h.Processor.GetJenkinsBinding(req.Request.Context(), client, namespace, name)
	if err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	data, err := h.Processor.UpdateJenkinsBinding(req.Request.Context(), client, newJenkinsBinding, oldJenkinsBinding, namespace, name)
	h.WriteResponse(data, err, req, res)
}

//GetJenkinsBindingResource get jenkinsbinding resource
func (h Handler) GetJenkinsBindingResource(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	data, err := h.Processor.GetJenkinsBindingResource(req.Request.Context(), client, namespace, name)

	h.WriteResponse(data, err, req, res)

}

//JenkinsBindingCronCheck get jenkinsbinding cron
func (h Handler) JenkinsBindingCronCheck(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	cron := req.QueryParameter("cron")

	data, err := h.Processor.JenkinsBindingCronCheck(req.Request.Context(), client, namespace, name, cron)
	h.WriteResponse(data.Data, err, req, res)
}

func (h Handler) GetJenkinsBindingLabels(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	labelMatcher := req.QueryParameter("labelMatcher")

	labels, err := h.Processor.GetJenkinsBindingLabels(req.Request.Context(), client, namespace, name, labelMatcher)
	h.WriteResponse(labels, err, req, res)
}

func (h Handler) GetJenkinsBindingPlugins(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	labels, err := h.Processor.GetJenkinsBindingPlugins(req.Request.Context(), client, namespace, name)
	h.WriteResponse(labels, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
