package jenkinsbinding_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkinsbinding"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Processor.JenkinsBinding", func() {
	var (
		processor jenkinsbinding.Processor
		ctx       context.Context
		client    *fake.Clientset
		query     *dataselect.Query

		list                   *jenkinsbinding.JenkinsBindingList
		err                    error
		namespace              *common.NamespaceQuery
		jenkinsbindinginstance *v1alpha1.JenkinsBinding
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		//query = dataselect.NoDataSelect
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 1,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		namespace = common.NewNamespaceQuery([]string{"default"})
		client = fake.NewSimpleClientset()
		processor = jenkinsbinding.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("should return only one jenkinsbinding", func() {
		client = fake.NewSimpleClientset(
			GetJenkinsBindingList("testbinding", "default"),
		)
		list, err = processor.ListJenkinsBinding(ctx, client, query, namespace)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Items).To(HaveLen(1), "should not have any items")
	})

	It("should retrieve a jenkinsbinding", func() {
		client = fake.NewSimpleClientset(
			GetJenkinsBinding("testbinding", "default"),
		)
		jenkinsbindinginstance, err = processor.GetJenkinsBinding(ctx, client, "default", "testbinding")
		Expect(err).To(BeNil())
		Expect(jenkinsbindinginstance.Spec.Jenkins.Name).To(Equal("jenkinsname"))

	})

})

func GetJenkinsBinding(name, namespace string) *v1alpha1.JenkinsBinding {
	jenkinsbindinginstance := &v1alpha1.JenkinsBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: v1alpha1.JenkinsBindingSpec{
			Jenkins: v1alpha1.JenkinsInstance{
				Name: "jenkinsname",
			},
		},
	}
	return jenkinsbindinginstance
}

func GetJenkinsBindingList(name, namespace string) *v1alpha1.JenkinsBindingList {
	jenkinsbindinglist := &v1alpha1.JenkinsBindingList{
		TypeMeta: metav1.TypeMeta{
			Kind: "jenkinsbinding",
		},

		Items: []v1alpha1.JenkinsBinding{
			{TypeMeta: metav1.TypeMeta{
				Kind: "jenkinsbinding",
			},
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
					Annotations: map[string]string{
						common.AnnotationsKeyToolType: v1alpha1.ToolChainContinuousIntegrationName,
					},
				},
				Spec: v1alpha1.JenkinsBindingSpec{
					Jenkins: v1alpha1.JenkinsInstance{
						Name: "jenkinsname",
					},
				},
			},
			{TypeMeta: metav1.TypeMeta{
				Kind: "jenkisnbinding",
			},
				ObjectMeta: metav1.ObjectMeta{
					Name:      name + "1",
					Namespace: namespace,
					Annotations: map[string]string{
						common.AnnotationsKeyToolType: v1alpha1.ToolChainContinuousIntegrationName,
					},
				},
				Spec: v1alpha1.JenkinsBindingSpec{
					Jenkins: v1alpha1.JenkinsInstance{
						Name: "jenkinsname1",
					},
				},
			},
		},
	}
	return jenkinsbindinglist
}
