package jenkinsbinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelineconfig"
	"context"
	"net/url"
	"regexp"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"encoding/json"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListJenkins list jenkins instances
func (p processor) ListJenkinsBinding(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *JenkinsBindingList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list jenkinsbinding end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	// fetch data from list
	jenkinslist, err := client.DevopsV1alpha1().JenkinsBindings(namespace.ToRequestParam()).List(v1alpha1.ListOptions())

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &JenkinsBindingList{
		Items:    make([]v1alpha1.JenkinsBinding, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := toCells(jenkinslist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := fromCells(itemCells)
	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider

	data.Items = ConvertToJenkinsSlice(result, annotationProvider)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeleteJenkins delete jenkinsbinding instance
func (p processor) DeleteJenkinsBinding(ctx context.Context, client versioned.Interface, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete jenkinsbinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().JenkinsBindings(namespace).Delete(name, &metav1.DeleteOptions{})
	return
}

// GetJenkinsBinding retrieve jenkinsbinding instance
func (p processor) GetJenkinsBinding(ctx context.Context, client versioned.Interface, namespace, name string) (data *v1alpha1.JenkinsBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get jenkinsbinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().JenkinsBindings(namespace).Get(name, jenkins.GetOptionsInCache)
	return
}

//CreateJenkinsBinding create jenkinsbinding instance
func (p processor) CreateJenkinsBinding(ctx context.Context, client versioned.Interface, jenkinsbinding *v1alpha1.JenkinsBinding, namespace string) (data *v1alpha1.JenkinsBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create jenkinsbinding end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().JenkinsBindings(namespace).Create(jenkinsbinding)
	return
}

//UpdateJenkinsBinding update a jenkinsbinding instance
func (p processor) UpdateJenkinsBinding(ctx context.Context, client versioned.Interface, jenkinsbinding, oldjenkinsbinding *v1alpha1.JenkinsBinding, namespace, name string) (data *v1alpha1.JenkinsBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update jenkinsbinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	binding := oldjenkinsbinding.DeepCopy()
	binding.SetAnnotations(jenkinsbinding.GetAnnotations())
	binding.Spec = jenkinsbinding.Spec

	data, err = client.DevopsV1alpha1().JenkinsBindings(namespace).Update(binding)
	return
}

// GetJenkinsBindingResource get jenkinsbinding resource
func (p processor) GetJenkinsBindingResource(ctx context.Context, client versioned.Interface, namespace, name string) (resourceList *common.ResourceList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetJenkinsBindingResource end", log.String("namespace", namespace), log.String("name", name))
	}()
	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider
	dsQuery := dataselect.GeSimpleFieldQuery("jenkinsBinding", name)
	items := pipelineconfig.GetPipelineConfigListAsResourceList(client, namespace, dsQuery, annotationProvider)
	resourceList = &common.ResourceList{
		Items: items,
	}
	return
}

// JenkinsBindingCronCheck get jenkinsbinding cron
func (p processor) JenkinsBindingCronCheck(ctx context.Context, client versioned.Interface, namespace, name, cron string) (apiresponse APIResponse, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetJenkinsBindingResource end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	opt := &v1alpha1.JenkinsBindingProxyOptions{
		URL: "alauda/cronTabCheck?cronText=" + url.QueryEscape(cron),
	}
	result, err := client.DevopsV1alpha1().JenkinsBindings(namespace).Proxy(name, opt)
	if err != nil {
		return
	}

	checkResult := APIResponse{}
	if result.Code == 200 && result.Data != "" {
		err := json.Unmarshal([]byte(result.Data), &checkResult)
		if err != nil {
			return checkResult, err
		}
	} else {
		logger.Error("cron text error, result ", log.Any("result", result))
	}
	return
}

type NodeStatus struct {
	Labels []string `json:"labels"`
}

func (p processor) GetJenkinsBindingLabels(ctx context.Context, client versioned.Interface, namespace, name, labelMatcher string) (apiresponse *LabelsResult, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetJenkinsBindingLabels end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	result, err := client.DevopsV1alpha1().
		JenkinsBindings(namespace).
		GetJenkinsInfo(name, &v1alpha1.JenkinsBindingInfoOptions{Target: v1alpha1.JenkinsNodesCondition})

	if err != nil {
		logger.Warn("Unable to get labels from JenkinsBinding", log.String("namespace", namespace), log.String("name", name), log.Err(err))
		return nil, err
	}

	var nodeStatus NodeStatus
	err = json.Unmarshal([]byte(result.Result), &nodeStatus)
	if err != nil {
		logger.Warn("Unable to get labels from JenkinsBinding", log.String("namespace", namespace), log.String("name", name), log.Err(err))
		return nil, err
	}

	var labelResult LabelsResult
	if labelMatcher != "" {
		matcher, err := regexp.Compile(labelMatcher)
		if err != nil {
			logger.Warn("Unable to match labels", log.String("labelMatcher", labelMatcher), log.Err(err))
			return nil, err
		}

		for _, label := range nodeStatus.Labels {
			if matcher.MatchString(label) {
				labelResult.Matched = append(labelResult.Matched, label)
			} else {
				labelResult.Others = append(labelResult.Others, label)
			}
		}
	} else {
		labelResult.Labels = nodeStatus.Labels
	}

	return &labelResult, nil
}

func (p processor) GetJenkinsBindingPlugins(ctx context.Context, client versioned.Interface, namespace, name string) (apiresponse *PluginsResult, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetJenkinsBindingPlugins end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	result, err := client.DevopsV1alpha1().
		JenkinsBindings(namespace).
		GetJenkinsInfo(name, &v1alpha1.JenkinsBindingInfoOptions{Target: v1alpha1.JenkinsPluginsCondition})
	if err != nil {
		logger.Warn("Unable to get labels from JenkinsBinding", log.String("namespace", namespace), log.String("name", name), log.Err(err))
		return nil, err
	}

	var pluginsResult PluginsResult
	err = json.Unmarshal([]byte(result.Result), &pluginsResult)

	return &pluginsResult, err
}

// ConvertToJenkinsSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToJenkinsSlice(filtered []v1alpha1.JenkinsBinding, provider devops.AnnotationProvider) (items []v1alpha1.JenkinsBinding) {
	items = make([]v1alpha1.JenkinsBinding, 0, len(filtered))
	for _, cm := range filtered {
		cm.ObjectMeta.Annotations[provider.AnnotationsKeyToolType()] = v1alpha1.ToolChainContinuousIntegrationName
		cm.Kind = common.ResourceKindJenkinsBinding
		items = append(items, cm)
	}
	return
}
