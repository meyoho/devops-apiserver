package jenkinsbinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
)

// JenkinsBindingList contains a list of jenkins in the cluster.
type JenkinsBindingList struct {
	ListMeta jenkins.ListMeta `json:"listMeta"`

	// Unordered list  JenkinsBinding.
	Items []v1alpha1.JenkinsBinding `json:"jenkinsbindings"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type CronCheckResult struct {
	Next     string `json:"next"`
	Previous string `json:"previous"`
	SanityZh string `json:"sanity_zh_cn"`
	SanityEn string `json:"sanity_en"`
	Error    string `json:"error"`
}

type APIResponse struct {
	Data   CronCheckResult `json:"data"`
	Status string          `json:"status"`
}

type LabelsResult struct {
	Labels  []string `json:"labels,omitempty"`
	Matched []string `json:"matched,omitempty"`
	Others  []string `json:"others,omitempty"`
}

type PluginsResult struct {
	Plugins []PluginStatus `json:"plugins"`
}

type PluginStatus struct {
	Name        string `json:"name"`
	Version     string `json:"version"`
	Description string `json:"description"`
	Status      string `json:"status"`
	Updatable   bool   `json:"updatable"`
}
