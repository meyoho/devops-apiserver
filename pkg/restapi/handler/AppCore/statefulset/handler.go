package statefulset

import (
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/container"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/event"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/handlecommon"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/pod"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	v1 "k8s.io/api/apps/v1"
	"net/http"
	"strings"
)

// Handler handler for StatefulSet
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
}

// New builder method for application.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	//queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("StatefulSet related APIs").ApiVersion("v1").Path("/api/v1/statefulset")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "statefulset")))

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{statefulset}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get StatefulSet instance").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.GetStatefulSetDetail).
				Returns(http.StatusOK, "Create StatefulSet Complete", StatefulSetDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{statefulset}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Update StatefulSet instance").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.UpdateStatefulSet).
				Returns(http.StatusOK, "Update StatefulSet Complete", v1.StatefulSet{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{statefulset}/start").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("start statefulset").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.StartStopStatefulSet).
				Returns(http.StatusOK, "start statefulset Complete", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{statefulset}/stop").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("start statefulset").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.StartStopStatefulSet).
				Returns(http.StatusOK, "stop statefulset Complete", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{statefulset}/yaml").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update statefulset yaml").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.UpdateStatefulSetDetailYaml).
				Returns(http.StatusOK, "update statefulset yaml", v1.StatefulSet{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{statefulset}/replicas").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update statefulset yaml").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.UpdateStatefulSetReplica).
				Returns(http.StatusOK, "update statefulset replica", v1.StatefulSet{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{statefulset}/event").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("get statefulset event").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.GetStatefulSetEvents).
				Returns(http.StatusOK, "get statefulset event", common.EventList{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{statefulset}/pods").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("get statefulset pods").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.GetStatefulSetPods).
				Returns(http.StatusOK, "get statefulset pods", pod.PodList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{statefulset}/container/{container}/").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update statefulset container").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.PutStatefulSetContainer).
				Returns(http.StatusOK, "update statefulset container", v1.StatefulSet{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{statefulset}/container/{container}/image").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update statefulset container").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.UpdateStatefulSetContainerImage).
				Returns(http.StatusOK, "update statefulset container", v1.StatefulSet{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{statefulset}/container/{container}/resources").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update statefulset resources").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.UpdateStatefulSetContainerResources).
				Returns(http.StatusOK, "update statefulset resources", v1.StatefulSet{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{statefulset}/container/{container}/env").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update statefulset env").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.UpdateStatefulSetContainerEnv).
				Returns(http.StatusOK, "update statefulset env", v1.StatefulSet{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}/{statefulset}/container/{container}/volumeMount").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update statefulset volumeMount").
				Param(ws.PathParameter("namespace", "namespace of statefulset")).
				Param(ws.PathParameter("statefulset", "name of statefulset")).
				To(handler.CreateStatefulSetVolumeMount).
				Returns(http.StatusOK, "update statefulset volumeMount", v1.StatefulSet{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

func (h Handler) GetStatefulSetDetail(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")

	result, err := GetStatefulSetDetail(k8sClient, namespace, name)
	logger.Debug("get statfulset detail end", log.Err(err))

	h.WriteResponse(result, err, req, res)
}

func (h Handler) UpdateStatefulSet(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")
	spec := new(v1.StatefulSet)

	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)

	}

	result, err := UpdateStatefulSetOriginal(k8sClient, namespace, name, spec)
	logger.Debug("get statfulset detail end", log.Err(err))

	h.WriteResponse(result, err, req, res)
}

func (h Handler) StartStopStatefulSet(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")
	spec := new(common.RevisionDetail)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)

	}

	statefulset, err := k8sClient.Apps().StatefulSets(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	list := make([]v1.StatefulSet, 0)
	list = append(list, *statefulset)
	isStop := strings.HasSuffix(req.Request.URL.Path, "stop")
	if !isStop {
		err = handlecommon.StartResources(k8sClient, nil, list)
	} else {
		err = handlecommon.StopResources(k8sClient, nil, list)
	}
	logger.Debug("get statfulset detail end", log.Err(err))

	if err != nil {
		h.WriteResponse(nil, err, req, res)
	} else {
		res.WriteHeader(http.StatusNoContent)
		h.WriteResponse(nil, err, req, res)
	}

}

// UpdateStatefulSetDetailYaml
func (h Handler) UpdateStatefulSetDetailYaml(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")
	spec := new(v1.StatefulSet)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	statefulset, err := UpdateStatefulSetOriginal(k8sClient, namespace, name, spec)
	logger.Debug("get statfulset detail end", log.Err(err))

	h.WriteResponse(statefulset, err, req, res)
}

//UpdateStatefulSetReplica
func (h Handler) UpdateStatefulSetReplica(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")
	spec := new(StatefulSetReplica)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	statefulset, err := UpdateStatefulSetReplica(k8sClient, namespace, name, *spec)
	logger.Debug("get statfulset detail end", log.Err(err))

	h.WriteResponse(statefulset, err, req, res)
}

//GetStatefulSetEvents
func (h Handler) GetStatefulSetEvents(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")
	dataSelect := common.ParseDataSelectPathParameter(req)

	result, err := event.GetResourceEvents(k8sClient, dataSelect, namespace, name)
	logger.Debug("get statfulset detail end", log.Err(err))

	h.WriteResponse(result, err, req, res)
}

//GetStatefulSetPods
func (h Handler) GetStatefulSetPods(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")
	podList, err := GetStatefulSetPods(k8sClient, dataselect.DefaultDataSelect, namespace, name)
	logger.Debug("get statfulset detail end", log.Err(err))

	h.WriteResponse(podList, err, req, res)
}

//PutStatefulSetContainer
func (h Handler) PutStatefulSetContainer(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")
	containerName := req.PathParameter("container")
	isDryRun := (strings.ToLower(req.QueryParameter("isDryRun")) == "true")

	spec := new(container.UpdateContainerRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	statefulset, err := PutStatefulsetContainer(k8sClient, namespace, name, containerName, isDryRun, *spec)
	logger.Debug("get statfulset detail end", log.Err(err))

	h.WriteResponse(statefulset, err, req, res)
}

//UpdateStatefulSetContainerImage
func (h Handler) UpdateStatefulSetContainerImage(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")
	containerName := req.PathParameter("container")

	spec := new(container.UpdateContainerImageRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	statefulset, err := UpdateContainerImage(k8sClient, namespace, name, containerName, *spec)
	logger.Debug("get statfulset detail end", log.Err(err))

	h.WriteResponse(statefulset, err, req, res)
}

//UpdateStatefulSetContainerResources
func (h Handler) UpdateStatefulSetContainerResources(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")
	containerName := req.PathParameter("container")

	spec := new(container.UpdateContainerResourceRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	statefulset, err := UpdateContainerResource(k8sClient, namespace, name, containerName, *spec)
	logger.Debug("get statfulset detail end", log.Err(err))

	h.WriteResponse(statefulset, err, req, res)
}

//UpdateStatefulSetContainerEnv
func (h Handler) UpdateStatefulSetContainerEnv(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")
	containerName := req.PathParameter("container")

	spec := new(container.UpdateContainerEnvRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	statefulset, err := UpdateContainerEnv(k8sClient, namespace, name, containerName, *spec)
	logger.Debug("get statfulset detail end", log.Err(err))

	h.WriteResponse(statefulset, err, req, res)
}

//CreateStatefulSetVolumeMount
func (h Handler) CreateStatefulSetVolumeMount(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("statefulset")
	containerName := req.PathParameter("container")

	spec := new(common.VolumeInfo)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	statefulset, err := CreateStatefulSetVolumeMount(k8sClient, namespace, name, containerName, *spec)
	logger.Debug("get statfulset detail end", log.Err(err))

	h.WriteResponse(statefulset, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
