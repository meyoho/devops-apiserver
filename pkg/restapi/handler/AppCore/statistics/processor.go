package statistics

import (
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codequalityproject"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipeline"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"sync"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"

	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/log"
	"context"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListApplication list secret instances
func (p processor) GetPipelineStatistics(ctx context.Context, cacheClient client.Client, namespace *common.NamespaceQuery, dsQuery *dataselect.Query, startTime, endTime time.Time) (result *PipelineStatistics, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetSecretDetail end", log.Err(err))

	}()

	result = &PipelineStatistics{
		Data: make([]PipelineStatisticsData, 0),
	}
	if !startTime.Before(endTime) {
		return
	}

	pipelineList, err := pipeline.NewProcessor().ListPipeline(ctx, cacheClient, dsQuery, namespace)
	if err != nil {
		logger.Error("error happen when get pipelines", log.Err(err))
		return
	}

	if pipelineList == nil || len(pipelineList.Items) == 0 {
		logger.Error("no pipelines in this period")
		return
	}

	timePool := make(map[time.Time]*Statistics, 0)
	parsePipelinesToTimePool(pipelineList.Items, startTime, endTime, result, timePool)
	parseTimePoolToResult(timePool, result, startTime, endTime)
	return
}

func (p processor) GetStageStatistics(ctx context.Context, cacheClient client.Client, namespace *common.NamespaceQuery, dsQuery *dataselect.Query, startTime, endTime time.Time) (result *StageStatistics, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetSecretDetail end", log.Any("namespace", namespace), log.Err(err))

	}()

	result = &StageStatistics{
		Data: make([]StageStatisticsData, 0),
	}
	if !startTime.Before(endTime) {
		return
	}

	pipelineList, err := pipeline.NewProcessor().ListPipeline(ctx, cacheClient, dsQuery, namespace)
	if err != nil {
		logger.Error("error happen when get pipelines", log.Err(err))
		return
	}

	if pipelineList == nil || len(pipelineList.Items) == 0 {
		logger.Debug("no pipelines in this period")
		return
	}
	logger.Debug("pipeline's count:", log.Int("length", len(pipelineList.Items)))

	var (
		stagePool = &sync.Map{}
		wg        sync.WaitGroup
	)
	for _, pipe := range pipelineList.Items {
		// skip the pipeline which is not finished
		if !pipe.Status.Phase.IsFinalPhase() || pipe.Status.FinishedAt == nil {
			logger.Info("pipelines not finished", log.String("pipeline name", pipe.GetObjectMeta().Name))
			continue
		}

		// skip the pipeline which is not finished between the time range
		finishedAt := pipe.Status.FinishedAt.Time
		if finishedAt.Before(startTime) || finishedAt.After(endTime) {
			logger.Info("pipelines is not in this period", log.String("pipeline name", pipe.GetObjectMeta().Name))
			continue
		}

		// skip the pipeline which has no stages info
		if pipe.Status.Jenkins == nil || pipe.Status.Jenkins.Stages == "" {
			logger.Info("no stages in jenkins, skip..")
			continue
		}

		wg.Add(1)
		go func(p v1alpha1.Pipeline) {
			defer wg.Done()

			parseStagesToStagePool(&p, stagePool)
		}(pipe)
	}
	wg.Wait()

	parseStagePoolToResult(stagePool, result)
	return
}

func (p processor) GetCodeQualityStatistics(ctx context.Context, devopsClient devopsclient.Interface, namespace *common.NamespaceQuery, dsQuery *dataselect.Query) (result CodeQualityStatistics, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetSecretDetail end", log.Any("namespace", namespace), log.Err(err))

	}()

	codeQualityProjectList, err := codequalityproject.NewProcessor().ListCodeQualityProject(ctx, devopsClient, namespace, dsQuery)
	if err != nil {
		logger.Error("error happen when get code quality projects", log.Err(err))
		return
	}

	projects := codeQualityProjectList.Items
	projectNumber := len(projects)
	if projectNumber <= 10 {
		result, err = parseProjectsToCodeQualityStatistics(projects)
		if err != nil {
			logger.Error("error happen when convert code quality projects to statistics", log.Err(err))
		}
		return
	}

	var results []*CodeQualityStatistics
	var wg sync.WaitGroup

	queue := make(chan *CodeQualityStatistics, 1)

	workerNumber := (projectNumber-1)/10 + 1
	wg.Add(workerNumber)
	for i := 0; i < workerNumber; i++ {
		ceil := min((i+1)*10, projectNumber)
		go func(floor, ceil int) {
			statistics, err := parseProjectsToCodeQualityStatistics(projects[floor:ceil])
			if err != nil {
				logger.Error("error happen when convert code quality projects to statistics", log.Err(err))
				queue <- nil
				return
			}
			queue <- &statistics
		}(i*10, ceil)
	}

	go func() {
		for r := range queue {
			results = append(results, r)
			wg.Done()
		}
	}()

	wg.Wait()

	result, err = mergeCodeQualityStatistics(results)
	if err != nil {
		logger.Error("error happen when merge code quality statistics results", log.Err(err))
	}
	return
}
