package statistics

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/handlecommon"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"context"
	"github.com/emicklei/go-restful"

	"net/http"
	"time"
)

// Handler handler for secret
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	GetPipelineStatistics(ctx context.Context, cacheClient client.Client, namespace *common.NamespaceQuery, dsQuery *dataselect.Query, startTime, endTime time.Time) (result *PipelineStatistics, err error)
	GetStageStatistics(ctx context.Context, cacheClient client.Client, namespace *common.NamespaceQuery, dsQuery *dataselect.Query, startTime, endTime time.Time) (result *StageStatistics, err error)
	GetCodeQualityStatistics(ctx context.Context, devopsClient devopsclient.Interface, namespace *common.NamespaceQuery, dsQuery *dataselect.Query) (result CodeQualityStatistics, err error)
}

// New builder method for secret.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	accessReviewFilter := decorator.AccessReviewFilter(srv)
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("statistics related APIs").ApiVersion("v1").Path("/api/v1/statistics")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "statistics")))

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("pipeline/{namespace}").
				Filter(accessReviewFilter.NewAccessReviewFilter(&v1alpha1.Pipeline{}, "list")).
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.CacheClientFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get pipeline statistics").
				Param(ws.PathParameter("namespace", "namespace of pipeline statistics")).
				To(handler.GetPipelineStatistics).
				Returns(http.StatusOK, "Get Secret Detail Complete", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("stage/{namespace}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Filter(clientFilter.CacheClientFilter).
				Doc("Get Stage statistics").
				Param(ws.PathParameter("namespace", "namespace of secret")).
				To(handler.GetStageStatistics).
				Returns(http.StatusOK, "Get Secret Detail Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("codequality/{namespace}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get codequality statistics").
				Param(ws.PathParameter("namespace", "namespace of codequality")).
				To(handler.GetCodeQualityStatistics).
				Returns(http.StatusOK, "Get codequality statistics Detail Complete", struct{}{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetPipelineStatistics
func (h Handler) GetPipelineStatistics(req *restful.Request, res *restful.Response) {

	cacheClient := localcontext.CacheClient(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	dataSelect := common.ParseDataSelectPathParameter(req)
	period := req.QueryParameter("period")

	startTime, endTime, err := handlecommon.GetRecentPeriodTime(period)
	if err != nil {
		h.WriteResponse(nil, err, req, res)

	}

	result, err := h.Processor.GetPipelineStatistics(req.Request.Context(), cacheClient, namespace, dataSelect, startTime, endTime)
	h.WriteResponse(result, err, req, res)
}

// GetStageStatistics
func (h Handler) GetStageStatistics(req *restful.Request, res *restful.Response) {
	cacheClient := localcontext.CacheClient(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	dataSelect := common.ParseDataSelectPathParameter(req)

	period := req.QueryParameter("period")

	startTime, endTime, err := handlecommon.GetRecentPeriodTime(period)
	if err != nil {
		h.WriteResponse(nil, err, req, res)

	}

	result, err := h.Processor.GetStageStatistics(req.Request.Context(), cacheClient, namespace, dataSelect, startTime, endTime)
	h.WriteResponse(result, err, req, res)
}

// CreateSecret create secret instances
func (h Handler) GetCodeQualityStatistics(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	dataSelect := common.ParseDataSelectPathParameter(req)

	result, err := h.Processor.GetCodeQualityStatistics(req.Request.Context(), devopsClient, namespace, dataSelect)
	h.WriteResponse(result, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
