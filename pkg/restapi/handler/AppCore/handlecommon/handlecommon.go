package handlecommon

import (
	"fmt"
	"github.com/spf13/cast"
	v1 "k8s.io/api/apps/v1"
	"k8s.io/client-go/kubernetes"
	"os"
	"time"
)

func GetRecentPeriodTime(period string) (startTime, endTime time.Time, err error) {
	endTime = time.Now()
	startTime = endTime
	dur, err := time.ParseDuration(period)
	if err != nil {
		err = fmt.Errorf("please provide an available parameter 'period'")
		return
	}

	startTime = endTime.Add(dur)
	return
}

func GetAppKey(key string) string {
	domain := os.Getenv("LABEL_BASE_DOMAIN")
	if domain == "" {
		domain = "alauda.io"
	}
	return fmt.Sprintf("app.%s/display-name", domain)
}

func StartResources(k8sClient kubernetes.Interface, deploys []v1.Deployment, statefulsets []v1.StatefulSet) error {
	for _, item := range deploys {
		if *item.Spec.Replicas != 0 {
			return nil
		}

		lastReplicas, ok := item.GetAnnotations()[GetAppKey("last-replicas")]
		if !ok {
			lastReplicas = "1"
		}

		if lastReplicas == "0" {
			lastReplicas = "1"
		}

		num := cast.ToInt32(lastReplicas)
		item.Spec.Replicas = &num
		_, err := k8sClient.Apps().Deployments(item.Namespace).Update(&item)
		if err != nil {
			return err
		}
	}

	for _, item := range statefulsets {
		if *item.Spec.Replicas != 0 {
			return nil
		}

		lastReplicas, ok := item.GetAnnotations()[GetAppKey("last-replicas")]
		if !ok {
			lastReplicas = "1"
		}

		if lastReplicas == "0" {
			lastReplicas = "1"
		}
		num := cast.ToInt32(lastReplicas)
		item.Spec.Replicas = &num
		_, err := k8sClient.Apps().StatefulSets(item.Namespace).Update(&item)
		if err != nil {
			return err
		}
	}
	return nil
}

func StopResources(k8sClient kubernetes.Interface, deploys []v1.Deployment, statefulsets []v1.StatefulSet) error {
	for _, item := range deploys {
		if *item.Spec.Replicas == 0 {
			return nil
		}

		lastReplicas := *item.Spec.Replicas
		num := int32(0)
		item.Spec.Replicas = &num
		ano := map[string]string{
			GetAppKey("last-replicas"): cast.ToString(lastReplicas),
		}
		item.SetAnnotations(ano)
		_, err := k8sClient.Apps().Deployments(item.Namespace).Update(&item)
		if err != nil {
			return err
		}
	}

	for _, item := range statefulsets {
		if *item.Spec.Replicas == 0 {
			return nil
		}

		lastReplicas := *item.Spec.Replicas
		num := int32(0)
		item.Spec.Replicas = &num
		ano := map[string]string{
			GetAppKey("last-replicas"): cast.ToString(lastReplicas),
		}
		item.SetAnnotations(ano)
		_, err := k8sClient.Apps().StatefulSets(item.Namespace).Update(&item)
		if err != nil {
			return err
		}
	}
	return nil
}
