package application

import (
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
)

type ApplicationCell Application

func (self ApplicationCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case common.NameProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Name)
	case common.ExactNameProperty:
		return dataselect.StdExactString(self.ObjectMeta.Name)
	default:
	}
	// if name is not supported then just return a constant dummy value, sort will have no effect.
	return nil
}

func toCells(std []Application) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = ApplicationCell(std[i])
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []Application {
	std := make([]Application, len(cells))
	for i := range std {
		std[i] = Application(cells[i].(ApplicationCell))
	}
	return std
}
