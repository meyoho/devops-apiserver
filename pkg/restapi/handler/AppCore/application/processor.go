package application

import (
	"context"
	"encoding/json"
	"errors"
	goErrors "errors"
	"fmt"
	"time"

	appCore "alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/daemonset"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/deployment"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/handlecommon"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/statefulset"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelineconfig"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	"k8s.io/client-go/kubernetes"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListApplication list application instances
func (p processor) ListApplication(ctx context.Context, client *appCore.ApplicationClient, k8sclient kubernetes.Interface, namespace string, query *dataselect.Query) (list *ApplicationList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get application end", log.String("namespace", namespace), log.Err(err))
	}()

	coreApplications, errs := client.ListApplications(namespace)
	if coreApplications == nil {
		return nil, goErrors.New("get empty app list")
	}
	list = &ApplicationList{
		ListMeta:     api.ListMeta{TotalItems: len(*coreApplications)},
		Applications: make([]Application, 0, len(*coreApplications)),
		Errors:       errs,
	}

	rc, _, criticalError := common.GetRelationResource(k8sclient, namespace)
	if criticalError != nil {
		return list, criticalError
	}
	for _, coreApplication := range *coreApplications {
		application, err := GenerateFromCore(coreApplication, rc, namespace)
		if err != nil {
			return nil, err
		}
		list.Applications = append(list.Applications, application)
	}
	list, err = toApplicationList(list, query)
	if err != nil {
		return nil, err
	}
	return list, nil

}

// GetApplicationTasks  get application task
func (p processor) GetApplication(ctx context.Context, appCoreClient *appCore.ApplicationClient, dclient versioned.Interface, k8sclient kubernetes.Interface, namespace, name string) (application *ApplicationDetail, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get application end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	rc, _, criticalError := common.GetRelationResource(k8sclient, namespace)
	if criticalError != nil {
		return application, criticalError
	}
	app, result := appCoreClient.GetApplication(namespace, name)
	if len(result.Errors()) != 0 {
		errString, err := json.Marshal(result.Errors())
		if err != nil {
			return application, err
		}
		return application, errors.New(string(errString))
	}
	deploymentList, err := deployment.GenerateDetailFromCore(*app, k8sclient, rc)
	if err != nil {
		return application, err
	}
	daemonSetList, err := daemonset.GenerateDetailFromCore(*app, rc)
	if err != nil {
		return application, err
	}
	statefulSetList, err := statefulset.GenerateDetailFromCore(*app, rc)
	if err != nil {
		return application, err
	}
	//pipelineconfigList, err := pipelineconfig.GenerateFromCore(*app)
	//if err != nil {
	//	return application, err
	//}
	//pipelineconfigList = getPipelineCofgByLabel(dclient, namespace, name, pipelineconfigList)
	application = &ApplicationDetail{
		ObjectMeta:   api.NewObjectMetaSplit(name, namespace, nil, nil, time.Now()),
		Description:  app.GetDisplayName(common.LocalBaseDomain()),
		Deployments:  deploymentList,
		Daemonsets:   daemonSetList,
		StatefulSets: statefulSetList,
		// TODO: Change pipeline to definition when present
		Pipelines: &[]pipelineconfig.PipelineConfig{},
		// Configmaps []configmap.ConfigMapDetail `json:"configmaps"`
		Others: getResources(*app),
	}
	return
}

//DeleteApplication
func (p processor) DeleteApplication(ctx context.Context, appCoreClient *appCore.ApplicationClient, namespace, name string, spec *DeleteAppYAMLSpec) ([]appCore.ResourceResult, error) {
	items := make([]appCore.GVKName, 0)
	if spec != nil {
		for _, resource := range spec.RemoveLabelResources {
			item := appCore.GVKName{
				Name:             resource.GetName(),
				GroupVersionKind: resource.GroupVersionKind(),
			}
			items = append(items, item)
		}
	}
	result := appCoreClient.DeleteApplication(namespace, name, items)
	return result.Items, nil
}

//CreateApplication create application instance
func (p processor) CreateApplication(ctx context.Context, appclient *appCore.ApplicationClient, spec *ApplicationSpec, namespace string, isDryRun bool) (application *ApplicationResponse, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create application end", log.Err(err))
	}()

	appInfo := appCore.ApplicationInfo{
		Name:        spec.ObjectMeta.Name,
		Namespace:   namespace,
		DisplayName: spec.Description,
	}
	// validation
	if err = appInfoValidation(appInfo); err != nil {
		return
	}

	if spec.Source == SourceYaml {
		return CreateApplicationByYaml(appclient, namespace, appInfo, spec.Resources)
	}
	// create by workload
	if spec.Source == SourceWorkload {
		return CreateApplicationByWorkload(appclient, namespace, appInfo, spec.Resources)
	}
	log.Debug("create application", log.String("namespace", namespace), log.String("name", spec.ObjectMeta.Name))
	resources := make([]unstructured.Unstructured, 0, 2)

	//create deployment
	for _, deploy := range spec.Deployments {
		yamlList, err := deployment.GenerateYaml(namespace, deploy, nil)
		if err != nil {
			return nil, err
		}
		for _, yaml := range yamlList {
			resources = append(resources, yaml)
		}
	}
	if isDryRun {
		app := &appCore.Application{
			Resources: resources,
		}
		return &ApplicationResponse{
			Application: app,
		}, nil
	}

	return CreateApplicationByYaml(appclient, namespace, appInfo, resources)

}

func appInfoValidation(appInfo appCore.ApplicationInfo) error {
	// verify generated app labels length
	label := fmt.Sprintf("%s.%s", appInfo.Name, appInfo.Namespace)
	if len(label) > 63 {
		return fmt.Errorf("%s must be no more than 63 characters", label)
	}
	return nil
}

//UpdateApplication update a application instance
func (p processor) UpdateApplication(ctx context.Context, appCoreClient *appCore.ApplicationClient, spec *ApplicationSpec, namespace, name string, isDryRun bool) (application *ApplicationResponse, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update Application end", log.Err(err), log.String("name", name))
	}()

	logger.Debug("Update Applicaiton", log.String("namespace", namespace), log.String("name", name))
	appInfo := appCore.ApplicationInfo{
		Name:        spec.ObjectMeta.Name,
		Namespace:   namespace,
		DisplayName: spec.Description,
	}
	if err = appInfoValidation(appInfo); err != nil {
		return
	}

	app, result := appCoreClient.GetApplication(namespace, name)
	err = result.CombineError()
	if err != nil {
		return nil, err
	}

	if app.GetDisplayName(common.LocalBaseDomain()) != spec.Description {
		app, err = appCoreClient.UpdateApplicationDisplayName(namespace, name, spec.Description)
		if err != nil {
			return nil, err
		}
	}
	resources, err := getUpdateApplicationResources(app, namespace, name, spec)

	if err != nil {
		return nil, err
	}

	for _, res := range resources {
		err = insertTimestamp(&res)
		if err != nil {
			return nil, err
		}
	}

	if isDryRun {
		app := &appCore.Application{
			Resources: resources,
		}
		return &ApplicationResponse{
			Application: app,
		}, nil
	}
	retryoption := appCore.ApplicationUpdateOptions{
		UpdateConflictMaxRetry: 2,
	}
	app, result = appCoreClient.UpdateApplication(namespace, name, &resources, retryoption)
	if result.CombineError() != nil {
		return nil, result.CombineError()
	}
	return &ApplicationResponse{
		Application: app,
		Result:      result,
	}, nil
}

func (p processor) GetApplicationYAML(ctx context.Context, appCoreClient *appCore.ApplicationClient, namespace, name string) (appYaml *ApplicationYAML, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get Application Yaml", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	app, result := appCoreClient.GetApplication(namespace, name)

	if app == nil {
		return nil, result.CombineError()
	}
	appYaml = &ApplicationYAML{
		Objects: *app.GetAllResources(),
		Errors:  result.Errors(),
	}
	return
}

func (p processor) UpdateApplicationYAML(ctx context.Context, appCoreClient *appCore.ApplicationClient, namespace, name string, spec *UpdateAppYAMLSpec) ([]appCore.ResourceResult, error) {
	_, result := appCoreClient.UpdateApplication(namespace, name, &spec.Resources)
	return result.Items, nil
}

func (p processor) StartStopApplication(ctx context.Context, appCoreClient *appCore.ApplicationClient, k8sClient kubernetes.Interface, namespace, name string, isStop bool) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Start Application end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	app, _ := appCoreClient.GetApplication(namespace, name)
	if app == nil {
		err = errors.New("app resource not found")
		return
	}

	deployments, err := deployment.GetFormCore(*app)
	if err != nil {
		return
	}
	statefulSets, err := statefulset.GetFormCore(*app)
	if err != nil {
		return
	}
	if !isStop {
		err = handlecommon.StartResources(k8sClient, deployments, statefulSets)
	} else {
		err = handlecommon.StopResources(k8sClient, deployments, statefulSets)
	}
	return
}
