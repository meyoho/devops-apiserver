package application

import (
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/daemonset"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/deployment"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/statefulset"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	goErrors "errors"
	"log"
	"sort"

	appCore "alauda.io/app-core/pkg/app"

	client "k8s.io/client-go/kubernetes"
)

// ApplicationList contains a list of Applications in the cluster.
type ApplicationList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of Applications.
	Applications []Application `json:"applications"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// Application sets a definition for application
type Application struct {
	ObjectMeta   api.ObjectMeta               `json:"objectMeta"`
	Description  string                       `json:"description"`
	Deployments  deployment.DeploymentSlice   `json:"deployments"`
	Daemonsets   daemonset.DaemonSetSlice     `json:"daemonsets"`
	StatefulSets statefulset.StatefulSetSlice `json:"statefulsets"`
}

// GetApplicationList returns a list of all applications in the cluster.
func GetApplicationList(client *appCore.ApplicationClient, k8sclient client.Interface, namespace string, dsQuery *dataselect.Query) (list *ApplicationList, err error) {
	log.Println("Getting list of all applications in namespace: ", namespace)
	coreApplications, errs := client.ListApplications(namespace)
	if coreApplications == nil {
		return nil, goErrors.New("get empty app list")
	}
	list = &ApplicationList{
		ListMeta:     api.ListMeta{TotalItems: len(*coreApplications)},
		Applications: make([]Application, 0, len(*coreApplications)),
		Errors:       errs,
	}

	rc, _, criticalError := common.GetRelationResource(k8sclient, namespace)
	if criticalError != nil {
		return list, criticalError
	}
	for _, coreApplication := range *coreApplications {
		application, err := GenerateFromCore(coreApplication, rc, namespace)
		if err != nil {
			return nil, err
		}
		list.Applications = append(list.Applications, application)
	}
	list, err = toApplicationList(list, dsQuery)
	if err != nil {
		return nil, err
	}
	return list, nil
}

func GenerateFromCore(app appCore.Application, rc *common.ResourceCollection, namespace string) (Application, error) {
	var application Application

	deploymentList, err := deployment.GenerateFromCore(app, rc)
	if err != nil {
		return application, err
	}
	daemonSetList, err := daemonset.GenerateFromCore(app, rc)
	if err != nil {
		return application, err
	}
	statefulSetList, err := statefulset.GenerateFromCore(app, rc)
	if err != nil {
		return application, err
	}
	deploymentSlice := deployment.DeploymentSlice(deploymentList.Deployments)
	sort.Stable(deploymentSlice)
	daemonsetSlice := daemonset.DaemonSetSlice(daemonSetList.DaemonSets)
	sort.Stable(daemonsetSlice)
	statefulsetSlice := statefulset.StatefulSetSlice(statefulSetList.StatefulSets)
	sort.Stable(statefulsetSlice)
	return Application{
		ObjectMeta: api.ObjectMeta{
			Name: app.GetAppCrd().ObjectMeta.GetName(),
		},
		Description:  app.GetDisplayName(common.LocalBaseDomain()),
		Deployments:  deploymentSlice,
		Daemonsets:   daemonsetSlice,
		StatefulSets: statefulsetSlice,
	}, nil
}

func toApplicationList(
	list *ApplicationList,
	dsQuery *dataselect.Query,
) (*ApplicationList, error) {
	appCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(list.Applications), dsQuery)
	apps := fromCells(appCells)
	list.Applications = apps
	list.ListMeta.TotalItems = filteredTotal
	return list, nil
}
