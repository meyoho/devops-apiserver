package application

import (
	"alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/kubernetes"
	"net/http"
	"strings"
)

// Handler handler for Application
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListApplication(ctx context.Context, appclient *app.ApplicationClient, k8sclient kubernetes.Interface, name string, query *dataselect.Query) (list *ApplicationList, err error)
	CreateApplication(ctx context.Context, appclient *app.ApplicationClient, spec *ApplicationSpec, namespace string, isDryRun bool) (application *ApplicationResponse, err error)
	UpdateApplication(ctx context.Context, appclient *app.ApplicationClient, spec *ApplicationSpec, namespace, name string, isDryRun bool) (application *ApplicationResponse, err error)
	GetApplicationYAML(ctx context.Context, appclient *app.ApplicationClient, namespace, name string) (*ApplicationYAML, error)
	UpdateApplicationYAML(ctx context.Context, appclient *app.ApplicationClient, namespace, name string, spec *UpdateAppYAMLSpec) ([]app.ResourceResult, error)
	GetApplication(ctx context.Context, appclient *app.ApplicationClient, client versioned.Interface, k8sclient kubernetes.Interface, namespace, name string) (*ApplicationDetail, error)
	StartStopApplication(ctx context.Context, appclient *app.ApplicationClient, k8sclient kubernetes.Interface, namespace, name string, isStop bool) error
	DeleteApplication(ctx context.Context, appclient *app.ApplicationClient, namespace, name string, deletespec *DeleteAppYAMLSpec) ([]app.ResourceResult, error)
}

// New builder method for application.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("Application related APIs").ApiVersion("v1").Path("/api/v1/applications")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "applicaiton")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Filter(clientFilter.SecureFilter).
					Filter(clientFilter.AppClientFilter).
					Doc(`List Application instance`).
					To(handler.ListApplication).
					Writes(ApplicationList{}).
					Returns(http.StatusOK, "OK", ApplicationList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Create Applicataion instance").
				Param(ws.PathParameter("namespace", "namespace of application")).
				To(handler.CreateApplication).
				Returns(http.StatusOK, "Create Applicaiton Complete", ApplicationResponse{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Update Applicataion instance").
				Param(ws.PathParameter("name", "name of application")).
				Param(ws.PathParameter("namespace", "namespace of application")).
				To(handler.UpdateApplication).
				Returns(http.StatusOK, "Update Applicaiton Complete", ApplicationResponse{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get Applicataion instance").
				Param(ws.PathParameter("name", "name of application")).
				Param(ws.PathParameter("namespace", "namespace of application")).
				To(handler.GetApplication).
				Returns(http.StatusOK, "Get Applicaiton Complete", ApplicationDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}/start").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Start  Applicataion ").
				Param(ws.PathParameter("name", "name of application")).
				Param(ws.PathParameter("namespace", "namespace of application")).
				To(handler.StartStopApplication).
				Returns(http.StatusNoContent, "start  Applicaiton", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}/stop").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Stop  Applicataion ").
				Param(ws.PathParameter("name", "name of application")).
				Param(ws.PathParameter("namespace", "namespace of application")).
				To(handler.StartStopApplication).
				Returns(http.StatusNoContent, "stop  Applicaiton", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/yaml").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get Applicataion Yaml").
				Param(ws.PathParameter("name", "name of application")).
				Param(ws.PathParameter("namespace", "namespace of application")).
				To(handler.GetApplicationYAML).
				Returns(200, "get Applicaiton yaml", ApplicationYAML{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}/yaml").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Update  Applicataion With Yaml ").
				Param(ws.PathParameter("name", "name of application")).
				Param(ws.PathParameter("namespace", "namespace of application")).
				To(handler.UpdateApplicationYAML).
				Returns(200, "update application details", UpdateAppYAMLSpec{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Delete Applicataion").
				Param(ws.PathParameter("name", "name of application")).
				Param(ws.PathParameter("namespace", "namespace of application")).
				To(handler.DeleteApplication).
				Returns(200, "Delete Applicaiton", DeleteAppYAMLSpec{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListApplication list application instances
func (h Handler) ListApplication(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	appCoreClient := localcontext.AppClient(req.Request.Context())

	dataSelect := common.ParseDataSelectPathParameter(req)

	//set the default sort by
	//if len(dataSelect.SortQuery.SortByList) == 0 {
	//	sortBy := dataselect.SortBy{
	//		Property:  dataselect.PropertyName("name"),
	//		Ascending: true,
	//	}
	//
	//	dataSelect.SortQuery.SortByList = append(dataSelect.SortQuery.SortByList, sortBy)
	//}

	namespace := req.PathParameter("namespace")
	result, err := h.Processor.ListApplication(req.Request.Context(), appCoreClient, k8sClient, namespace, dataSelect)
	//dataSelect.SortQuery.SortByList = []dataselect.SortBy{}
	h.WriteResponse(result, err, req, res)
}

// CreateApplication create application instances
func (h Handler) CreateApplication(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	isDryRun := (strings.ToLower(req.QueryParameter("isDryRun")) == "true")
	spec := new(ApplicationSpec)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	result, err := h.Processor.CreateApplication(req.Request.Context(), appCoreClient, spec, namespace, isDryRun)
	h.WriteResponse(result, err, req, res)
}

// UpdateApplication update application instances
func (h Handler) UpdateApplication(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	isDryRun := (strings.ToLower(req.QueryParameter("isDryRun")) == "true")
	spec := new(ApplicationSpec)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	result, err := h.Processor.UpdateApplication(req.Request.Context(), appCoreClient, spec, namespace, name, isDryRun)
	h.WriteResponse(result, err, req, res)
}

// GetApplication get application instances
func (h Handler) GetApplication(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())
	devopsclient := localcontext.DevOpsClient(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	result, err := h.Processor.GetApplication(req.Request.Context(), appCoreClient, devopsclient, k8sClient, namespace, name)
	h.WriteResponse(result, err, req, res)
}

// StartStopApplication start/stop application instances
func (h Handler) StartStopApplication(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	spec := new(ApplicationSpec)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	isStop := strings.HasSuffix(req.Request.URL.Path, "stop")

	err := h.Processor.StartStopApplication(req.Request.Context(), appCoreClient, k8sClient, namespace, name, isStop)
	h.WriteResponse(nil, err, req, res)
}

// GetApplicationYAML get application instances
func (h Handler) GetApplicationYAML(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	result, err := h.Processor.GetApplicationYAML(req.Request.Context(), appCoreClient, namespace, name)
	h.WriteResponse(result, err, req, res)
}

// UpdateApplicationYAML update application instance
func (h Handler) UpdateApplicationYAML(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	spec := new(UpdateAppYAMLSpec)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	result, err := h.Processor.UpdateApplicationYAML(req.Request.Context(), appCoreClient, namespace, name, spec)
	h.WriteResponse(result, err, req, res)
}

//DeleteApplication delete application instance
func (h Handler) DeleteApplication(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	//spec, err := h.Processor.GetApplicationYAML(req.Request.Context(), appCoreClient, namespace, name)
	//if err != nil {
	//	h.WriteResponse(nil, err, req, res)
	//	return
	//}
	//deletespec := new(DeleteAppYAMLSpec)
	//deletespec.RemoveLabelResources = spec.Objects
	result, err := h.Processor.DeleteApplication(req.Request.Context(), appCoreClient, namespace, name, nil)
	h.WriteResponse(result, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
