package node

import (
	appCore "alauda.io/app-core/pkg/app"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"context"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

func (p processor) GetNodeLabels(ctx context.Context, appCoreClient *appCore.ApplicationClient, k8sclient kubernetes.Interface) (result NodeLabels, err error) {
	logger := abcontext.Logger(ctx)
	result = make(NodeLabels)
	defer func() {
		logger.Debug("get node labels")

	}()
	nodes, err := k8sclient.CoreV1().Nodes().List(metav1.ListOptions{ResourceVersion: "0"})
	if err != nil {
		return result, err
	}

	for _, node := range nodes.Items {
		internalIP, labels := getNodeLabel(node)
		result[internalIP] = labels
	}
	return
}

func getNodeLabel(node v1.Node) (internalIP string, labels []Label) {
	labels = []Label{}
	for key, value := range node.Labels {
		newlabel := Label{
			Key:   key,
			Value: value,
		}
		labels = append(labels, newlabel)
	}
	for _, addr := range node.Status.Addresses {
		if addr.Type == v1.NodeInternalIP {
			internalIP = addr.Address
		}
	}
	return internalIP, labels

}
