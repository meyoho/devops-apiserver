package node

import (
	appCore "alauda.io/app-core/pkg/app"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"

	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/kubernetes"
	"net/http"
)

// Handler handler for Deployment
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	GetNodeLabels(ctx context.Context, appCoreClient *appCore.ApplicationClient, k8sclient kubernetes.Interface) (labels NodeLabels, err error)
}

// New builder method for application.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("Node related APIs").ApiVersion("v1").Path("/api/v1/node")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "node")))

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/labels").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get Nodes labels instance").
				To(handler.GetNodeLabels).
				Returns(http.StatusOK, "Get Node Labels", NodeLabels{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

func (h Handler) GetNodeLabels(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	result, err := h.Processor.GetNodeLabels(req.Request.Context(), appCoreClient, k8sClient)

	h.WriteResponse(result, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
