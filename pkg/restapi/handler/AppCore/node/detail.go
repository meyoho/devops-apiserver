package node

type NodeLabels map[string][]Label

type Label struct {
	Value string `json:"value"`
	Key   string `json:"key"`
}
