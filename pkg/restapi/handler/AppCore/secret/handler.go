package secret

import (
	"context"
	"net/http"

	"alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
)

// Handler handler for secret
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	GetSecretListForApp(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, nsQuery *common.NamespaceQuery, dsQuery *dataselect.Query) (*SecretList, error)
	GetSecretList(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, nsQuery *common.NamespaceQuery, dsQuery *dataselect.Query, includePublic bool) (*SecretList, error)
	GetSecretDetail(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string) (*SecretDetail, error, *v1.Secret)
	CreateSecret(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace *common.NamespaceQuery, spec *SecretDetail) (*Secret, error)
	UpdateSecret(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace *common.NamespaceQuery, spec *SecretDetail) (*Secret, error)
	DeleteSecret(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace *common.NamespaceQuery, name string) error
	GetSecretRelatedResources(ctx context.Context, k8sclient kubernetes.Interface, client versioned.Interface, namespace, name string, dsQuery *dataselect.Query) (*ResourceList, error)
}

// New builder method for secret.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("secret related APIs").ApiVersion("v1").Path("/api/v1/secret")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "secret")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("{namespace}").
					Filter(clientFilter.SecureFilter).
					Filter(clientFilter.AppClientFilter).
					Doc(`List secret instance`).
					To(handler.GetSecretList).
					Writes(SecretList{}).
					Returns(http.StatusOK, "OK", SecretList{}),
			),
		),
	)
	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.SecureFilter).
					Filter(clientFilter.AppClientFilter).
					Doc(`List secret instance`).
					To(handler.GetSecretList).
					Writes(SecretList{}).
					Returns(http.StatusOK, "OK", SecretList{}),
			),
		),
	)
	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("app/{namespace}").
					Filter(clientFilter.SecureFilter).
					Filter(clientFilter.AppClientFilter).
					Doc(`List secret instance`).
					To(handler.GetSecretListForApp).
					Writes(SecretList{}).
					Returns(http.StatusOK, "OK", SecretList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get Secret deatil").
				Param(ws.PathParameter("name", "name of secret")).
				Param(ws.PathParameter("namespace", "namespace of secret")).
				To(handler.GetSecretDetail).
				Returns(http.StatusOK, "Get Secret Detail Complete", SecretDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("{name}/resources").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get Secret related resources").
				Param(ws.PathParameter("name", "name of secret")).
				To(handler.GetSecretRelatedResources).
				Returns(http.StatusOK, "Get Secret Detail Complete", ResourceList{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("{namespace}/{name}/resources").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get Secret related resources").
				Param(ws.PathParameter("name", "name of secret")).
				To(handler.GetSecretRelatedResources).
				Returns(http.StatusOK, "Get Secret Detail Complete", ResourceList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("{namespace}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Create Secret ").
				Param(ws.PathParameter("namespace", "namespace of secret")).
				To(handler.CreateSecret).
				Returns(http.StatusOK, "Create Secret ", Secret{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Create Secret ").
				Param(ws.PathParameter("namespace", "namespace of secret")).
				To(handler.CreateSecret).
				Returns(http.StatusOK, "Create Secret ", Secret{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update Secret instance").
				Param(ws.PathParameter("name", "name of secret")).
				Param(ws.PathParameter("namespace", "namespace of secret")).
				To(handler.UpdateSecret).
				Returns(http.StatusOK, "Update Secret Complete", Secret{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update Secret instance").
				Param(ws.PathParameter("name", "name of secret")).
				Param(ws.PathParameter("namespace", "namespace of secret")).
				To(handler.UpdateSecret).
				Returns(http.StatusOK, "Update Secret Complete", Secret{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("delete Secret instance").
				Param(ws.PathParameter("name", "name of secret")).
				Param(ws.PathParameter("namespace", "namespace of secret")).
				To(handler.DeleteSecret).
				Returns(http.StatusOK, "Delete Secret Complete", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("delete Secret instance").
				Param(ws.PathParameter("name", "name of secret")).
				To(handler.DeleteSecret).
				Returns(http.StatusOK, "Delete Secret Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("secret{namespace}/{name}/actions/tradeapp").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update Secret Belong app").
				Param(ws.PathParameter("name", "name of secret")).
				Param(ws.PathParameter("namespace", "namespace of secret")).
				To(handler.UpdateSecretBelongApp).
				Returns(http.StatusOK, "Update Secret belong app ", SecretDetail{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

func NewAppSecret(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("appsecret related APIs").ApiVersion("v1").Path("/api/v1/appsecret")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "appsecret")))

	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("{namespace}").
					Filter(clientFilter.SecureFilter).
					Filter(clientFilter.AppClientFilter).
					Doc(`List appsecret instance`).
					To(handler.GetSecretListForApp).
					Writes(SecretList{}).
					Returns(http.StatusOK, "OK", SecretList{}),
			),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetSecretList get secret instances list
func (h Handler) GetSecretList(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	appCoreClient := localcontext.AppClient(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	dataSelect := common.ParseDataSelectPathParameter(req)

	var includePublic bool
	includePublicStr := req.QueryParameter("includePublic")
	if includePublicStr == "true" {
		includePublic = true
	}

	result, err := h.Processor.GetSecretList(req.Request.Context(), k8sClient, appCoreClient, namespace, dataSelect, includePublic)
	h.WriteResponse(result, err, req, res)
}

// GetSecretList get secret instances list
func (h Handler) GetSecretListForApp(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	appCoreClient := localcontext.AppClient(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	dataSelect := common.ParseDataSelectPathParameter(req)

	result, err := h.Processor.GetSecretListForApp(req.Request.Context(), k8sClient, appCoreClient, namespace, dataSelect)
	h.WriteResponse(result, err, req, res)
}

// GetSecretDetail get secret instances detail
func (h Handler) GetSecretDetail(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	appCoreClient := localcontext.AppClient(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	result, err, _ := h.Processor.GetSecretDetail(req.Request.Context(), k8sClient, appCoreClient, namespace, name)
	h.WriteResponse(result, err, req, res)
}

// CreateSecret create secret instances
func (h Handler) CreateSecret(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	spec := new(SecretDetail)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	result, err := h.Processor.CreateSecret(req.Request.Context(), k8sClient, appCoreClient, namespace, spec)
	h.WriteResponse(result, err, req, res)
}

//UpdateSecret update secret instance

func (h Handler) UpdateSecret(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)

	spec := new(SecretDetail)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	result, err := h.Processor.UpdateSecret(req.Request.Context(), k8sClient, appCoreClient, namespace, spec)
	h.WriteResponse(result, err, req, res)
}

//DeleteSecret delete secret instance
func (h Handler) DeleteSecret(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	name := req.PathParameter("name")
	err := h.Processor.DeleteSecret(req.Request.Context(), k8sClient, appCoreClient, namespace, name)
	h.WriteResponse(nil, err, req, res)
}

// UpdateSecretBelongApp create secret instances
func (h Handler) UpdateSecretBelongApp(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	spec := new(common.AppNameDetail)

	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	result, err, originResource := h.Processor.GetSecretDetail(req.Request.Context(), k8sClient, appCoreClient, namespace, name)
	if err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	err = common.UpdateResourceBelongApplication(appCoreClient, originResource,
		namespace, result.AppName, spec.AppName)
	if err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	h.WriteResponse(result, err, req, res)
}

func (h Handler) GetSecretRelatedResources(req *restful.Request, res *restful.Response) {
	devopsclient := localcontext.DevOpsClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	dataSelect := common.ParseDataSelectPathParameter(req)
	result, err := h.Processor.GetSecretRelatedResources(req.Request.Context(), k8sClient, devopsclient, namespace, name, dataSelect)
	h.WriteResponse(result, err, req, res)

}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
