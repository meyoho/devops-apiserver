// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package secret

import (
	appCore "alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"

	v1 "k8s.io/api/core/v1"
)

// SecretSpec is a common interface for the specification of different secrets.
type SecretSpec interface {
	GetName() string
	GetType() v1.SecretType
	GetNamespace() string
	GetData() map[string][]byte
}

// ImagePullSecretSpec is a specification of an image pull secret implements SecretSpec
type ImagePullSecretSpec struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`

	// The value of the .dockercfg property. It must be Base64 encoded.
	Data []byte `json:"data"`
}

// GetName returns the name of the ImagePullSecret
func (spec *ImagePullSecretSpec) GetName() string {
	return spec.Name
}

// GetType returns the type of the ImagePullSecret, which is always api.SecretTypeDockercfg
func (spec *ImagePullSecretSpec) GetType() v1.SecretType {
	return v1.SecretTypeDockercfg
}

// GetNamespace returns the namespace of the ImagePullSecret
func (spec *ImagePullSecretSpec) GetNamespace() string {
	return spec.Namespace
}

// GetData returns the data the secret carries, it is a single key-value pair
func (spec *ImagePullSecretSpec) GetData() map[string][]byte {
	return map[string][]byte{v1.DockerConfigKey: spec.Data}
}

// GenericSecretSpec is a specification of an generic secret spec
type GenericSecretSpec struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`

	// Data property to store values
	Data map[string][]byte `json:"data"`
	Type v1.SecretType     `json:"type"`
}

// GetName returns the name of the ImagePullSecret
func (spec *GenericSecretSpec) GetName() string {
	return spec.Name
}

// GetType returns the type of the ImagePullSecret, which is always api.SecretTypeDockercfg
func (spec *GenericSecretSpec) GetType() v1.SecretType {
	return spec.Type
}

// GetNamespace returns the namespace of the ImagePullSecret
func (spec *GenericSecretSpec) GetNamespace() string {
	return spec.Namespace
}

// GetData returns the data the secret carries, it is a single key-value pair
func (spec *GenericSecretSpec) GetData() map[string][]byte {
	return spec.Data
}

// Secret is a single secret returned to the frontend.
type Secret struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`
	Type       v1.SecretType  `json:"type"`
	Keys       []string       `json:"keys"`
	AppName    string         `json:"appName"`
}

func (ing Secret) GetObjectMeta() api.ObjectMeta {
	return ing.ObjectMeta
}

// SecretsList is a response structure for a queried secrets list.
type SecretList struct {
	api.ListMeta `json:"listMeta"`

	// Unordered list of Secrets.
	Secrets []Secret `json:"secrets"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

var supportedSecretTypes = []v1.SecretType{v1.SecretTypeBasicAuth, v1.SecretTypeDockerConfigJson, v1.SecretTypeOpaque, v1.SecretTypeTLS, v1.SecretTypeSSHAuth, devopsv1alpha1.SecretTypeOAuth2}

var devopsSupportedSecretTypes = []v1.SecretType{v1.SecretTypeBasicAuth, v1.SecretTypeDockerConfigJson, v1.SecretTypeSSHAuth, devopsv1alpha1.SecretTypeOAuth2}

func (list *SecretList) GetItems() (res []common.Resource) {
	if list == nil {
		res = []common.Resource{}
	} else {
		res = make([]common.Resource, len(list.Secrets))
		for i, d := range list.Secrets {
			res[i] = d
		}
	}
	return
}

var hiddenNamespaces = []string{devopsv1alpha1.NamespaceKubeSystem, devopsv1alpha1.NamespaceDefault}

func isHiddenNamespace(namespace string, systemNamespace string) bool {
	for _, n := range hiddenNamespaces {
		if n == namespace {
			return true
		}
	}

	if namespace == systemNamespace {
		return true
	}
	return false
}

func toSecret(secret *v1.Secret, appName string, credentialsNamespace string) *Secret {
	keys := make([]string, 0, len(secret.Data))
	for k := range secret.Data {
		keys = append(keys, k)
	}
	result := &Secret{
		ObjectMeta: api.NewObjectMeta(secret.ObjectMeta),
		TypeMeta:   api.NewTypeMeta(api.ResourceKindSecret),
		Type:       secret.Type,
		Keys:       keys,
		AppName:    appName,
	}
	if result.ObjectMeta.Annotations == nil {
		result.ObjectMeta.Annotations = make(map[string]string, 0)
	}

	if secret.GetNamespace() == credentialsNamespace {
		common.LocalBaseDomain()

		result.ObjectMeta.Annotations[common.GetDevopsGlobal(common.LocalBaseDomain(), devopsv1alpha1.LabelDevopsAlaudaIOGlobalKey)] = devopsv1alpha1.TrueString
	}
	return result
}

func toSecretList(secrets []v1.Secret, nonCriticalErrors []error, dsQuery *dataselect.Query, appCoreClient *appCore.ApplicationClient, credentialsNamespace string, provider devops.AnnotationProvider) *SecretList {
	newSecretList := &SecretList{
		ListMeta: api.ListMeta{TotalItems: len(secrets)},
		Secrets:  make([]Secret, 0),
		Errors:   nonCriticalErrors,
	}

	secretCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(secrets, provider), dsQuery)
	secrets = fromCells(secretCells)
	newSecretList.ListMeta = api.ListMeta{TotalItems: filteredTotal}

	for i := range secrets {
		setTypeMeta(&secrets[i])
	}

	appNameList := common.GetAppNameListFromAppcore(appCoreClient, secrets, filteredTotal)
	if appNameList != nil {
		for i := range secrets {
			newSecretList.Secrets = append(newSecretList.Secrets, *toSecret(&secrets[i], appNameList[i], credentialsNamespace))
		}
	}

	return newSecretList
}

func ToSecretList(res []common.Resource) (list []Secret) {
	var (
		ok bool
		cm Secret
	)
	list = make([]Secret, 0, len(res))
	for _, r := range res {
		if cm, ok = r.(Secret); ok {
			list = append(list, cm)
		}
	}
	return
}
