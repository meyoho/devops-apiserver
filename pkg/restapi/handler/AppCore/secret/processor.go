package secret

import (
	appCore "alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/handler/coderepobinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/imageregistrybinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkinsbinding"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"fmt"
	v1 "k8s.io/api/core/v1"
	k8serror "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"

	"alauda.io/devops-apiserver/pkg/restapi/handler/common"

	"context"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/log"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// GetSecretList list secret instances
func (p processor) GetSecretList(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace *common.NamespaceQuery, dsQuery *dataselect.Query, includePublic bool) (secretlist *SecretList, err error) {

	extra := localcontext.ExtraConfig(ctx)
	credentialsNamespace := extra.CredentialsNamespace

	provider := localcontext.ExtraConfig(ctx).AnnotationProvider

	items, err := getsecretlist(ctx, client, namespace, devopsSupportedSecretTypes, includePublic)
	if err != nil {
		return nil, err
	}

	newSecretList := &SecretList{
		ListMeta: api.ListMeta{TotalItems: len(items)},
		Secrets:  make([]Secret, 0),
	}

	secretCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(items, provider), dsQuery)
	items = fromCells(secretCells)
	newSecretList.ListMeta = api.ListMeta{TotalItems: filteredTotal}

	for i := range items {
		setTypeMeta(&items[i])
	}

	for i := range items {
		newSecretList.Secrets = append(newSecretList.Secrets, *toSecret(&items[i], "", credentialsNamespace))
	}

	return newSecretList, nil
}

// GetSecretListForApp list secret instances
func (p processor) GetSecretListForApp(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace *common.NamespaceQuery, dsQuery *dataselect.Query) (secretlist *SecretList, err error) {
	extra := localcontext.ExtraConfig(ctx)
	credentialsNamespace := extra.CredentialsNamespace
	provider := localcontext.ExtraConfig(ctx).AnnotationProvider
	items, err := getsecretlist(ctx, client, namespace, supportedSecretTypes, false)
	if err != nil {
		return nil, err
	}
	return toSecretList(items, []error{}, dsQuery, appCoreClient, credentialsNamespace, provider), nil
}

func getsecretlist(ctx context.Context, client kubernetes.Interface, namespace *common.NamespaceQuery, supporttypes []v1.SecretType, includePublic bool) (result []v1.Secret, err error) {
	extra := localcontext.ExtraConfig(ctx)
	credentialsNamespace := extra.CredentialsNamespace
	systemNamespace := extra.SystemNamespace
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetSecretDetail end", log.Err(err))

	}()

	provider := localcontext.ExtraConfig(ctx).AnnotationProvider
	curNamespace := namespace.ToRequestParam()

	secretList, err := client.CoreV1().Secrets(curNamespace).List(api.ListEverything)
	if err != nil {
		return nil, err
	}

	if curNamespace != "" && includePublic {
		publicSecretList, err := client.CoreV1().Secrets(credentialsNamespace).List(api.ListEverything)
		if err != nil {
			return nil, err
		}

		if publicSecretList != nil && len(publicSecretList.Items) > 0 {
			secretList.Items = append(secretList.Items, publicSecretList.Items...)
		}
	}

	// secret type list
	items := func(secrets []v1.Secret) []v1.Secret {
		filtered := make([]v1.Secret, 0, len(secrets))
		for _, s := range secrets {
			if len(s.ObjectMeta.Labels) > 0 && s.ObjectMeta.Labels[provider.AnnotationGeneratorName()] != "" {
				continue
			}
			for _, t := range supporttypes {
				if s.Type == t && !isHiddenNamespace(s.Namespace, systemNamespace) {
					filtered = append(filtered, s)
					break
				}
			}
		}
		return filtered
	}(secretList.Items)

	return items, nil
}

func (p processor) GetSecretDetail(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace, name string) (cmdetail *SecretDetail, err error, rawSecret *v1.Secret) {

	extra := localcontext.ExtraConfig(ctx)
	credentialsNamespace := extra.CredentialsNamespace
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetSecretDetail end", log.String("name", name), log.String("namespace", namespace), log.Err(err))

	}()

	rawSecret, err = client.CoreV1().Secrets(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err, nil
	}
	setTypeMeta(rawSecret)

	detail, err := getSecretDetailAndAppName(rawSecret, appCoreClient, credentialsNamespace)
	if err != nil {
		return nil, err, nil
	}

	return detail, err, rawSecret
}

func (p processor) GetSecretRelatedResources(ctx context.Context, k8sclient kubernetes.Interface, devopsClient versioned.Interface, namespace, name string, dsQuery *dataselect.Query) (resoureList *ResourceList, err error) {
	extra := localcontext.ExtraConfig(ctx)
	credentialsNamespace := extra.CredentialsNamespace
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetSecretDetail end", log.String("name", name), log.String("namespace", namespace), log.Err(err))

	}()

	codeRepoBindings, err := coderepobinding.NewProcessor().ListCodeRepoBinding(ctx, devopsClient, dsQuery, common.NewSameNamespaceQuery(namespace))
	if err != nil {
		return
	}

	jenkinsBindings, err := jenkinsbinding.NewProcessor().ListJenkinsBinding(ctx, devopsClient, dsQuery, common.NewSameNamespaceQuery(namespace))
	if err != nil {
		return
	}

	imageRegistryBindings, err := imageregistrybinding.NewProcessor().ListImageRegistryBinding(ctx, devopsClient, dsQuery, common.NewSameNamespaceQuery(namespace))
	if err != nil {
		return
	}

	resourceList := &ResourceList{
		Items: make([]ResourceItem, 0),
	}
	if len(codeRepoBindings.Items) > 0 {
		for _, item := range codeRepoBindings.Items {
			refSecretName := item.Spec.Account.Secret.Name
			refSecretNamespace := item.Spec.Account.Secret.Namespace
			if refSecretName != name || (refSecretNamespace != credentialsNamespace && namespace != refSecretNamespace) {
				continue
			}
			resourceList.Items = append(resourceList.Items, ResourceItem{
				Name:      item.ObjectMeta.Name,
				Namespace: item.ObjectMeta.Namespace,
				Kind:      api.ResourceKindCodeRepoBinding,
			})
		}
	}
	if len(imageRegistryBindings.Items) > 0 {
		for _, item := range imageRegistryBindings.Items {
			refSecretName := item.Spec.Secret.Name
			refSecretNamespace := item.Spec.Secret.Namespace
			if refSecretName != name || (refSecretNamespace != credentialsNamespace && namespace != refSecretNamespace) {
				continue
			}
			resourceList.Items = append(resourceList.Items, ResourceItem{
				Name:      item.ObjectMeta.Name,
				Namespace: item.ObjectMeta.Namespace,
				Kind:      api.ResourceKindImageRegistry,
			})
		}
	}
	if len(jenkinsBindings.Items) > 0 {
		for _, item := range jenkinsBindings.Items {
			refSecretName := item.Spec.Account.Secret.Name
			refSecretNamespace := item.Spec.Account.Secret.Namespace
			if refSecretName != name || (refSecretNamespace != credentialsNamespace && namespace != refSecretNamespace) {
				continue
			}
			resourceList.Items = append(resourceList.Items, ResourceItem{
				Name:      item.ObjectMeta.Name,
				Namespace: item.ObjectMeta.Namespace,
				Kind:      api.ResourceKindJenkinsBinding,
			})
		}
	}
	return resourceList, nil
}

func (p processor) CreateSecret(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace *common.NamespaceQuery, spec *SecretDetail) (details *Secret, err error) {

	extra := localcontext.ExtraConfig(ctx)
	credentialsNamespace := extra.CredentialsNamespace

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create secret end", log.Err(err))

	}()
	curNamespace := common.GetCurNamespace(namespace, credentialsNamespace)

	secret := &v1.Secret{
		ObjectMeta: metaV1.ObjectMeta{
			Name:        spec.ObjectMeta.Name,
			Namespace:   curNamespace,
			Annotations: spec.ObjectMeta.Annotations,
		},
		Type:       spec.Type,
		Data:       spec.Data,
		StringData: spec.StringData,
	}
	setTypeMeta(secret)

	var newSecret *v1.Secret

	if spec.AppName == "" {
		newSecret, err = client.CoreV1().Secrets(curNamespace).Create(secret)
		if err != nil {
			return nil, err
		}
	} else {
		err = common.CreateResourceAndIpmortToApplication(appCoreClient, secret,
			curNamespace, spec.AppName)
		if err != nil {
			return nil, err
		}
		_, err, newSecret = GetSecretDetail(client, appCoreClient, curNamespace, spec.ObjectMeta.Name, credentialsNamespace)
		if err != nil {
			return nil, err
		}
	}

	return toSecret(newSecret, spec.AppName, credentialsNamespace), err
}

func (p processor) UpdateSecret(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace *common.NamespaceQuery, spec *SecretDetail) (details *Secret, err error) {

	extra := localcontext.ExtraConfig(ctx)
	credentialsNamespace := extra.CredentialsNamespace
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create secret end", log.Err(err))

	}()
	curNamespace := common.GetCurNamespace(namespace, credentialsNamespace)

	oldSercetDetail, err, originSecret := GetSecretDetail(client, appCoreClient, curNamespace, spec.ObjectMeta.Name, credentialsNamespace)
	if err != nil {
		return nil, err
	}

	if originSecret.Type == devopsv1alpha1.SecretTypeOAuth2 {
		ownerReferences := originSecret.GetOwnerReferences()
		if ownerReferences != nil && len(ownerReferences) > 1 {
			return nil, k8serror.NewForbidden(schema.GroupResource{}, originSecret.GetName(),
				fmt.Errorf("secret '%s' is not allowed to update if it is referenced by other resources", originSecret.GetName()))
		}
	}

	anno := common.DevOpsAnnotator{}
	newMeta := api.NewRawObjectMeta(spec.ObjectMeta)
	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider
	originSecret.ObjectMeta = anno.GetProductAnnotations(api.CompleteMeta(newMeta, originSecret.ObjectMeta), annotationProvider)
	// if new secret data not exist, we do not update the old secret data.
	if spec.Data != nil && len(spec.Data) != 0 {
		originSecret.Data = spec.Data
	}

	if spec.StringData != nil && len(spec.StringData) != 0 {
		originSecret.StringData = spec.StringData
	}

	setTypeMeta(originSecret)

	if oldSercetDetail.AppName != "" && spec.AppName != "" && spec.AppName == oldSercetDetail.AppName {
		//old hava val,new have val, and same,update use appcore api
		err = common.UpdateResourceWithApplication(appCoreClient, originSecret, curNamespace, spec.AppName)
	} else if oldSercetDetail.AppName != "" && spec.AppName != "" && spec.AppName != oldSercetDetail.AppName {
		//appName change ,remove old ,update use appcore api
		err = updateSecretWithAppNameChange(appCoreClient, oldSercetDetail.AppName, spec.AppName, curNamespace, originSecret)

	} else if oldSercetDetail.AppName != "" && spec.AppName == "" {
		// old have val,new no val ,remove old ,origin update new
		err = updateSecretWithNoNewAppName(client, appCoreClient, oldSercetDetail.AppName, curNamespace, originSecret)

	} else if oldSercetDetail.AppName == "" && spec.AppName != "" {
		//old no value,new have val
		err = common.UpdateResourceWithApplication(appCoreClient, originSecret,
			curNamespace, spec.AppName)
	} else if oldSercetDetail.AppName == "" && spec.AppName == "" {
		//old no val,new no val,use origin api
		_, err = client.CoreV1().Secrets(curNamespace).Update(originSecret)
	} else {
		err = nil
	}

	if err != nil {
		return nil, err
	}

	_, err, newSecret := GetSecretDetail(client, appCoreClient, curNamespace, spec.ObjectMeta.Name, credentialsNamespace)
	if err != nil {
		return nil, err
	}

	return toSecret(newSecret, spec.AppName, credentialsNamespace), err
}

func (p processor) DeleteSecret(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace *common.NamespaceQuery, name string) (err error) {
	extra := localcontext.ExtraConfig(ctx)
	credentialsNamespace := extra.CredentialsNamespace
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create secret end", log.Err(err))

	}()
	curNamespace := common.GetCurNamespace(namespace, credentialsNamespace)

	secretDetail, err, originSecret := GetSecretDetail(client, appCoreClient, curNamespace, name, credentialsNamespace)
	if err != nil {
		log.Errorf("GetSecretDetail curNamespace %s, name %s, credentialsNamespace %s. err is %s  ", curNamespace, name, credentialsNamespace, err)
		return err
	}
	if originSecret != nil {
		setTypeMeta(originSecret)
	}
	if secretDetail.AppName != "" {
		return k8serror.NewForbidden(schema.GroupResource{}, originSecret.GetName(),
			fmt.Errorf("secret '%s' is not allowed to delete if it is relate by app", originSecret.GetName()))
	}

	err = client.CoreV1().Secrets(curNamespace).Delete(name, &metaV1.DeleteOptions{})
	if err != nil {
		return err
	}

	return nil

}
