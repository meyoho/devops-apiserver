package configuration

import (
	"net/http"

	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/configmap"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
)

// Handler handler for configmap
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
}

// New builder method for configuration.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("configuration related APIs").ApiVersion("v1").Path("/api/v1/configuration")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "configmap")))

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("").
				Filter(clientFilter.InsecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get Configuration deatil").
				Param(ws.PathParameter("name", "name of configmap")).
				Param(ws.PathParameter("namespace", "namespace of configmap")).
				To(handler.GetPlatformConfiguration).
				Returns(http.StatusOK, "Get Configuration Detail Complete", configmap.ConfigMapDetail{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetPlatformConfiguration get configmap instances list
func (h Handler) GetPlatformConfiguration(req *restful.Request, res *restful.Response) {
	insecureClient := abcontext.Client(req.Request.Context())
	appCoreClient := localcontext.AppClient(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())
	extra := localcontext.ExtraConfig(req.Request.Context())
	namespace := extra.SystemNamespace
	name := "global-configmap"

	result, err, _ := configmap.GetConfigMapDetail(insecureClient, appCoreClient, namespace, name)

	logger.Debug("get platform configuration detail end", log.Err(err))
	h.WriteResponse(result, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
