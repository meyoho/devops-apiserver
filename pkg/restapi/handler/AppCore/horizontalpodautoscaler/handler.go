package horizontalpodautoscaler

import (
	"alauda.io/app-core/pkg/app"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	autoscaling "k8s.io/api/autoscaling/v2beta1"
	"k8s.io/client-go/kubernetes"
	"net/http"
)

// Handler handler for HPA
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	GetHorizontalPodAutoscalerList(ctx context.Context, k8sclient kubernetes.Interface, nsQuery *common.NamespaceQuery) (*HorizontalPodAutoscalerList, error)
	GetHorizontalPodAutoscalerDetail(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string) (*HorizontalPodAutoscalerDetail, *autoscaling.HorizontalPodAutoscaler, error)
	CreateHorizontalPodAutoscaler(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace string, spec *HorizontalPodAutoscalerDetail) (*HorizontalPodAutoscalerDetail, error)
	UpdateHorizontalPodAutoscaler(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string, spec *HorizontalPodAutoscalerDetail) (*HorizontalPodAutoscalerDetail, error)
	DeleteHorizontalPodAutoscaler(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string) error
}

// New builder method for hpa.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("horizontalpodautoscaler related APIs").ApiVersion("v1").Path("/api/v1/horizontalpodautoscaler")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "horizontalpodautoscaler")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.SecureFilter).
					Doc(`List hpa instance`).
					To(handler.GetHorizontalPodAutoscalerList).
					Writes(HorizontalPodAutoscalerList{}).
					Returns(http.StatusOK, "OK", HorizontalPodAutoscalerList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get Hpa deatil").
				Param(ws.PathParameter("name", "name of hpa")).
				Param(ws.PathParameter("namespace", "namespace of hpa")).
				To(handler.GetHorizontalPodAutoscalerDetail).
				Returns(http.StatusOK, "Get HorizontalPodAutoscaler Detail Complete", HorizontalPodAutoscalerDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get Hpa deatil").
				Param(ws.PathParameter("name", "name of hpa")).
				Param(ws.PathParameter("namespace", "namespace of hpa")).
				To(handler.CreateHorizontalPodAutoscaler).
				Returns(http.StatusOK, "Create HorizontalPodAutoscaler Complete", HorizontalPodAutoscalerDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update Hpa instance").
				Param(ws.PathParameter("name", "name of hpa")).
				Param(ws.PathParameter("namespace", "namespace of hpa")).
				To(handler.UpdateHorizontalPodAutoscaler).
				Returns(http.StatusOK, "Update HorizontalPodAutoscaler Complete", HorizontalPodAutoscalerDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("delete Hpa instance").
				Param(ws.PathParameter("name", "name of hpa")).
				Param(ws.PathParameter("namespace", "namespace of hpa")).
				To(handler.DeleteHorizontalPodAutoscaler).
				Returns(http.StatusOK, "Delete HorizontalPodAutoscaler Complete", struct{}{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetHorizontalPodAutoscalerList get hpa instances list
func (h Handler) GetHorizontalPodAutoscalerList(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)

	result, err := h.Processor.GetHorizontalPodAutoscalerList(req.Request.Context(), k8sClient, namespace)
	h.WriteResponse(result, err, req, res)
}

// GetHorizontalPodAutoscalerList get hpa instances detail
func (h Handler) GetHorizontalPodAutoscalerDetail(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	appCoreClient := localcontext.AppClient(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	result, _, err := h.Processor.GetHorizontalPodAutoscalerDetail(req.Request.Context(), k8sClient, appCoreClient, namespace, name)
	h.WriteResponse(result, err, req, res)
}

// CreateHorizontalPodAutoscaler create orizontalPodAutoscaler instances
func (h Handler) CreateHorizontalPodAutoscaler(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	spec := new(HorizontalPodAutoscalerDetail)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	result, err := h.Processor.CreateHorizontalPodAutoscaler(req.Request.Context(), k8sClient, appCoreClient, namespace, spec)
	h.WriteResponse(result, err, req, res)
}

//UpdateHorizontalPodAutoscaler update hpa instance

func (h Handler) UpdateHorizontalPodAutoscaler(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	spec := new(HorizontalPodAutoscalerDetail)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	result, err := h.Processor.UpdateHorizontalPodAutoscaler(req.Request.Context(), k8sClient, appCoreClient, namespace, name, spec)
	h.WriteResponse(result, err, req, res)
}

//DeleteHorizontalPodAutoscaler delete hpa instance
func (h Handler) DeleteHorizontalPodAutoscaler(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	err := h.Processor.DeleteHorizontalPodAutoscaler(req.Request.Context(), k8sClient, appCoreClient, namespace, name)
	h.WriteResponse(nil, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
