package horizontalpodautoscaler

import (
	appCore "alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/restapi/api"

	"alauda.io/devops-apiserver/pkg/restapi/handler/common"

	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/log"
	"context"
	autoscaling "k8s.io/api/autoscaling/v2beta1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListApplication list application instances
func (p processor) GetHorizontalPodAutoscalerList(ctx context.Context, client kubernetes.Interface, nsQuery *common.NamespaceQuery) (hpalist *HorizontalPodAutoscalerList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("get hpa list end", log.Err(err))

	}()

	channel := common.GetHorizontalPodAutoscalerListChannel(client, nsQuery, 1)
	hpaList := <-channel.List
	err = <-channel.Error

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	return toHorizontalPodAutoscalerList(hpaList.Items, nonCriticalErrors), nil
}

func (p processor) GetHorizontalPodAutoscalerDetail(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace, name string) (horizontalPodAutoscalerDetail *HorizontalPodAutoscalerDetail, rawHorizontalPodAutoscaler *autoscaling.HorizontalPodAutoscaler, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetHorizontalPodAutoscalerDetail end", log.String("name", name), log.Err(err))

	}()

	rawHorizontalPodAutoscaler, err = client.AutoscalingV2beta1().HorizontalPodAutoscalers(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, nil, err
	}
	setTypeMeta(rawHorizontalPodAutoscaler)

	horizontalPodAutoscalerDetail, err = gerneateHorizontalPodAutoscalerDetail(rawHorizontalPodAutoscaler, appCoreClient)
	if err != nil {
		return nil, nil, err
	}

	return horizontalPodAutoscalerDetail, rawHorizontalPodAutoscaler, nil
}

func (p processor) CreateHorizontalPodAutoscaler(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace string, spec *HorizontalPodAutoscalerDetail) (horizontalPodAutoscalerDetail *HorizontalPodAutoscalerDetail, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create hpa end", log.Err(err))

	}()

	if spec.MinReplicas == nil {
		*spec.MinReplicas = 1 //default val
	}

	id, err := common.GetUUID()
	if err != nil {
		return nil, err
	}
	spec.ObjectMeta.Name = generateHpaName(spec.ScaleTargetRef.Name, id)

	horizontalPodAutoscaler := &autoscaling.HorizontalPodAutoscaler{
		ObjectMeta: metaV1.ObjectMeta{
			Name:        spec.ObjectMeta.Name,
			Namespace:   namespace,
			Annotations: spec.ObjectMeta.Annotations,
		},
		Spec: autoscaling.HorizontalPodAutoscalerSpec{
			ScaleTargetRef: autoscaling.CrossVersionObjectReference{
				Kind:       spec.ScaleTargetRef.Kind,
				Name:       spec.ScaleTargetRef.Name,
				APIVersion: spec.ScaleTargetRef.APIVersion,
			},
			MinReplicas: spec.MinReplicas,
			MaxReplicas: spec.MaxReplicas,
			Metrics:     spec.Metrics,
		},
	}
	setTypeMeta(horizontalPodAutoscaler)

	err = common.CreateResourceAndIpmortToApplication(appCoreClient, horizontalPodAutoscaler,
		namespace, spec.AppName)
	if err != nil {
		return nil, err
	}
	horizontalPodAutoscalerDetail, _, err = GetHorizontalPodAutoscalerDetail(client, appCoreClient, namespace, spec.ObjectMeta.Name)
	if err != nil {
		return nil, err
	}

	return horizontalPodAutoscalerDetail, err
}

func (p processor) UpdateHorizontalPodAutoscaler(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace, name string, spec *HorizontalPodAutoscalerDetail) (horizontalPodAutoscalerDetail *HorizontalPodAutoscalerDetail, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create hpa end", log.Err(err))

	}()
	horizontalPodAutoscalerDetail, oldrawHorizontalPodAutoscaler, err := GetHorizontalPodAutoscalerDetail(client, appCoreClient, namespace, name)
	if err != nil {
		return nil, err
	}

	if spec.MinReplicas == nil {
		*spec.MinReplicas = 1 //default val
	}

	newMeta := api.NewRawObjectMeta(spec.ObjectMeta)
	oldrawHorizontalPodAutoscaler.ObjectMeta = api.CompleteMeta(newMeta, oldrawHorizontalPodAutoscaler.ObjectMeta)
	oldrawHorizontalPodAutoscaler.Spec.ScaleTargetRef.Kind = spec.ScaleTargetRef.Kind
	oldrawHorizontalPodAutoscaler.Spec.ScaleTargetRef.Name = spec.ScaleTargetRef.Name
	oldrawHorizontalPodAutoscaler.Spec.ScaleTargetRef.APIVersion = spec.ScaleTargetRef.APIVersion
	oldrawHorizontalPodAutoscaler.Spec.MinReplicas = spec.MinReplicas
	oldrawHorizontalPodAutoscaler.Spec.MaxReplicas = spec.MaxReplicas
	oldrawHorizontalPodAutoscaler.Spec.Metrics = spec.Metrics

	setTypeMeta(oldrawHorizontalPodAutoscaler)

	err = common.UpdateResourceWithApplication(appCoreClient, oldrawHorizontalPodAutoscaler, namespace, horizontalPodAutoscalerDetail.AppName)
	if err != nil {
		return nil, err
	}

	horizontalPodAutoscalerDetail, _, err = GetHorizontalPodAutoscalerDetail(client, appCoreClient, namespace, name)
	if err != nil {
		return nil, err
	}

	return horizontalPodAutoscalerDetail, err

}

func (p processor) DeleteHorizontalPodAutoscaler(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create hpa end", log.Err(err))

	}()
	_, rawHorizontalPodAutoscaler, err := GetHorizontalPodAutoscalerDetail(client, appCoreClient, namespace, name)
	setTypeMeta(rawHorizontalPodAutoscaler)
	if err != nil {
		return err
	}

	err = common.DeleteResourceFromApplication(appCoreClient, rawHorizontalPodAutoscaler, namespace)
	if err != nil {
		return err
	}
	return err

}
