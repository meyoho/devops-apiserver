package persistentvolumeclaim

import (
	"context"
	"net/http"

	"alauda.io/app-core/pkg/app"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
)

// Handler handler for persistentvolumeclaim
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	GetPersistentVolumeClaimList(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, nsQuery *common.NamespaceQuery, dsQuery *dataselect.Query) (*PersistentVolumeClaimList, error)
	GetPersistentVolumeClaimDetail(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string) (*PersistentVolumeClaimDetail, error, *v1.PersistentVolumeClaim)
	CreatePersistentVolumeClaim(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace string, spec *PersistentVolumeClaimDetail) (*PersistentVolumeClaimDetail, error)
	UpdatePersistentVolumeClaim(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace string, spec *PersistentVolumeClaimDetail) (*PersistentVolumeClaimDetail, error)
	DeletePersistentVolumeClaim(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string) error
}

// New builder method for persistentvolumeclaim.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("persistentvolumeclaim related APIs").ApiVersion("v1").Path("/api/v1/persistentvolumeclaim")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "persistentvolumeclaim")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.SecureFilter).
					Filter(clientFilter.AppClientFilter).
					Doc(`List persistentvolumeclaim instance`).
					To(handler.GetPersistentVolumeClaimList).
					Writes(PersistentVolumeClaimList{}).
					Returns(http.StatusOK, "OK", PersistentVolumeClaimList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get PersistenVolumnClaim deatil").
				Param(ws.PathParameter("name", "name of persistentvolumeclaim")).
				Param(ws.PathParameter("namespace", "namespace of persistentvolumeclaim")).
				To(handler.GetPersistentVolumeClaimDetail).
				Returns(http.StatusOK, "Get PersistentVolumeClaim Detail Complete", PersistentVolumeClaimDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Create PersistenVolumnClaim ").
				Param(ws.PathParameter("name", "name of persistentvolumeclaim")).
				Param(ws.PathParameter("namespace", "namespace of persistentvolumeclaim")).
				To(handler.CreatePersistentVolumeClaim).
				Returns(http.StatusOK, "Create PersistentVolumeClaim ", PersistentVolumeClaimDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update PersistenVolumnClaim instance").
				Param(ws.PathParameter("name", "name of persistentvolumeclaim")).
				Param(ws.PathParameter("namespace", "namespace of persistentvolumeclaim")).
				To(handler.UpdatePersistentVolumeClaim).
				Returns(http.StatusOK, "Update PersistentVolumeClaim Complete", PersistentVolumeClaimDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("delete PersistenVolumnClaim instance").
				Param(ws.PathParameter("name", "name of persistentvolumeclaim")).
				Param(ws.PathParameter("namespace", "namespace of persistentvolumeclaim")).
				To(handler.DeletePersistentVolumeClaim).
				Returns(http.StatusOK, "Delete PersistentVolumeClaim Complete", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("{namespace}/{name}/actions/tradeapp").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update PersistenVolumnClaim Belong app").
				Param(ws.PathParameter("name", "name of persistentvolumeclaim")).
				Param(ws.PathParameter("namespace", "namespace of persistentvolumeclaim")).
				To(handler.UpdatePersistentVolumeClaimBelongApp).
				Returns(http.StatusOK, "Update PersistentVolumeClaim belong app ", PersistentVolumeClaimDetail{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetPersistentVolumeClaimList get persistentvolumeclaim instances list
func (h Handler) GetPersistentVolumeClaimList(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	appCoreClient := localcontext.AppClient(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	dataSelect := common.ParseDataSelectPathParameter(req)

	result, err := h.Processor.GetPersistentVolumeClaimList(req.Request.Context(), k8sClient, appCoreClient, namespace, dataSelect)
	h.WriteResponse(result, err, req, res)
}

// GetPersistentVolumeClaimList get persistentvolumeclaim instances detail
func (h Handler) GetPersistentVolumeClaimDetail(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	appCoreClient := localcontext.AppClient(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	result, err, _ := h.Processor.GetPersistentVolumeClaimDetail(req.Request.Context(), k8sClient, appCoreClient, namespace, name)
	h.WriteResponse(result, err, req, res)
}

// CreatePersistentVolumeClaim create persistentvolumeclaim instances
func (h Handler) CreatePersistentVolumeClaim(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	spec := new(PersistentVolumeClaimDetail)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	extra := localcontext.ExtraConfig(req.Request.Context())
	curNamespace := common.GetCurNamespace(namespace, extra.CredentialsNamespace)

	result, err := h.Processor.CreatePersistentVolumeClaim(req.Request.Context(), k8sClient, appCoreClient, curNamespace, spec)
	h.WriteResponse(result, err, req, res)
}

//UpdatePersistentVolumeClaim update persistentvolumeclaim instance

func (h Handler) UpdatePersistentVolumeClaim(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	spec := new(PersistentVolumeClaimDetail)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	result, err := h.Processor.UpdatePersistentVolumeClaim(req.Request.Context(), k8sClient, appCoreClient, namespace, spec)
	h.WriteResponse(result, err, req, res)
}

//DeletePersistentVolumeClaim delete persistentvolumeclaim instance
func (h Handler) DeletePersistentVolumeClaim(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	err := h.Processor.DeletePersistentVolumeClaim(req.Request.Context(), k8sClient, appCoreClient, namespace, name)
	h.WriteResponse(nil, err, req, res)
}

// UpdatePersistentVolumeClaimBelongApp create persistentvolumeclaim instances
func (h Handler) UpdatePersistentVolumeClaimBelongApp(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	spec := new(common.AppNameDetail)

	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	result, err, originResource := h.Processor.GetPersistentVolumeClaimDetail(req.Request.Context(), k8sClient, appCoreClient, namespace, name)

	if err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	err = common.UpdateResourceBelongApplication(appCoreClient, originResource,
		namespace, result.AppName, spec.AppName)
	if err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	h.WriteResponse(result, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
