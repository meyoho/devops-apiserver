package persistentvolumeclaim

import (
	appCore "alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"fmt"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"

	"alauda.io/devops-apiserver/pkg/restapi/handler/common"

	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/log"
	"context"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/client-go/kubernetes"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListApplication list persistentvolumeclaim instances
func (p processor) GetPersistentVolumeClaimList(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, nsQuery *common.NamespaceQuery, dsQuery *dataselect.Query) (persistentvolumeclaimlist *PersistentVolumeClaimList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetPersistentVolumeClaimDetail end", log.Err(err))

	}()

	channels := &common.ResourceChannels{
		PersistentVolumeClaimList: common.GetPersistentVolumeClaimListChannel(client, nsQuery, 1),
	}

	return GetPersistentVolumeClaimListFromChannels(channels, nsQuery, dsQuery, appCoreClient)
}

func (p processor) GetPersistentVolumeClaimDetail(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace, name string) (cmdetail *PersistentVolumeClaimDetail, err error, rawPersistentVolumeClaim *v1.PersistentVolumeClaim) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetPersistentVolumeClaimDetail end", log.String("name", name), log.String("namespace", namespace), log.Err(err))

	}()

	rawPersistentVolumeClaim, err = client.CoreV1().PersistentVolumeClaims(namespace).Get(name, api.GetOptionsInCache)

	if err != nil {
		return nil, err, nil
	}
	setTypeMeta(rawPersistentVolumeClaim)

	details, err := getPersistentVolumeClaimDetailAndAppName(rawPersistentVolumeClaim, appCoreClient)
	if err != nil {
		return nil, err, nil
	}

	return details, nil, rawPersistentVolumeClaim
}

func (p processor) CreatePersistentVolumeClaim(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace string, spec *PersistentVolumeClaimDetail) (details *PersistentVolumeClaimDetail, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create persistentvolumeclaim end", log.Err(err), log.String("namespace", namespace))

	}()
	reqResources := v1.ResourceRequirements{
		Requests: spec.Capacity,
	}

	pvc := &v1.PersistentVolumeClaim{
		ObjectMeta: metaV1.ObjectMeta{
			Name:        spec.ObjectMeta.Name,
			Namespace:   namespace,
			Annotations: spec.ObjectMeta.Annotations,
		},
		Spec: v1.PersistentVolumeClaimSpec{
			VolumeName:       spec.Volume,
			AccessModes:      spec.AccessModes,
			StorageClassName: spec.StorageClass,
			Resources:        reqResources,
		},
	}
	setTypeMeta(pvc)

	var newPvc *v1.PersistentVolumeClaim

	if spec.AppName == "" {
		newPvc, err = client.CoreV1().PersistentVolumeClaims(namespace).Create(pvc)
		if err != nil {
			return nil, err
		}
	} else {
		err = common.CreateResourceAndIpmortToApplication(appCoreClient, pvc,
			namespace, spec.AppName)
		if err != nil {
			return nil, err
		}
		_, err, newPvc = GetPersistentVolumeClaimDetail(client, appCoreClient, namespace, spec.ObjectMeta.Name)
		if err != nil {
			return nil, err
		}
	}

	details = &PersistentVolumeClaimDetail{
		ObjectMeta:   api.NewObjectMeta(newPvc.ObjectMeta),
		TypeMeta:     api.NewTypeMeta(api.ResourceKindPersistentVolumeClaim),
		AppName:      spec.AppName,
		Status:       newPvc.Status.Phase,
		Volume:       newPvc.Spec.VolumeName,
		Capacity:     newPvc.Status.Capacity,
		AccessModes:  newPvc.Spec.AccessModes,
		StorageClass: newPvc.Spec.StorageClassName,
	}

	return details, err
}

func (p processor) UpdatePersistentVolumeClaim(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace string, spec *PersistentVolumeClaimDetail) (details *PersistentVolumeClaimDetail, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create persistentvolumeclaim end", log.Err(err))

	}()

	oldPvcDetail, err, pvc := GetPersistentVolumeClaimDetail(client, appCoreClient, namespace, spec.ObjectMeta.Name)
	if err != nil {
		return nil, err
	}

	newMeta := api.NewRawObjectMeta(spec.ObjectMeta)
	pvc.ObjectMeta = api.CompleteMeta(newMeta, pvc.ObjectMeta)
	setTypeMeta(pvc)

	if oldPvcDetail.AppName != "" && spec.AppName != "" && spec.AppName == oldPvcDetail.AppName {
		//old hava val,new have val, and same,update use appcore api
		err = common.UpdateResourceWithApplication(appCoreClient, pvc, namespace, spec.AppName)
	} else if oldPvcDetail.AppName != "" && spec.AppName != "" && spec.AppName != oldPvcDetail.AppName {
		//appName change ,remove old ,update use appcore api
		err = updatePersistentVolumeClaimWithAppNameChange(appCoreClient, oldPvcDetail.AppName, spec.AppName, namespace, pvc)

	} else if oldPvcDetail.AppName != "" && spec.AppName == "" {
		// old have val,new no val ,remove old ,origin update new
		err = updatePersistentVolumeClaimWithNoNewAppName(client, appCoreClient, oldPvcDetail.AppName, namespace, pvc)

	} else if oldPvcDetail.AppName == "" && spec.AppName != "" {
		//old no value,new have val
		err = common.UpdateResourceWithApplication(appCoreClient, pvc,
			namespace, spec.AppName)
	} else if oldPvcDetail.AppName == "" && spec.AppName == "" {
		//old no val,new no val,use origin api
		_, err = client.CoreV1().PersistentVolumeClaims(namespace).Update(pvc)
	} else {
		err = nil
	}

	if err != nil {
		return nil, err
	}

	_, err, newPvc := GetPersistentVolumeClaimDetail(client, appCoreClient, namespace, spec.ObjectMeta.Name)
	if err != nil {
		return nil, err
	}

	details = &PersistentVolumeClaimDetail{
		ObjectMeta:   api.NewObjectMeta(newPvc.ObjectMeta),
		TypeMeta:     api.NewTypeMeta(api.ResourceKindPersistentVolumeClaim),
		Status:       newPvc.Status.Phase,
		Volume:       newPvc.Spec.VolumeName,
		Capacity:     newPvc.Status.Capacity,
		AccessModes:  newPvc.Spec.AccessModes,
		StorageClass: newPvc.Spec.StorageClassName,
	}

	return details, err

}

func (p processor) DeletePersistentVolumeClaim(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create persistentvolumeclaim end", log.Err(err))

	}()
	detailPvc, err, originPvc := GetPersistentVolumeClaimDetail(client, appCoreClient, namespace, name)
	if err != nil {
		return err
	}

	setTypeMeta(originPvc)

	if detailPvc.AppName != "" {
		return errors.NewForbidden(schema.GroupResource{}, originPvc.GetName(), fmt.Errorf("PersistentVolumeClaim '%s' is not allowed to delete if it is relate by app", originPvc.GetName()))
	}

	err = client.CoreV1().PersistentVolumeClaims(namespace).Delete(name, &metaV1.DeleteOptions{})
	if err != nil {
		return err
	}

	return err

}
