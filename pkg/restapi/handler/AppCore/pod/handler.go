package pod

import (
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/container"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/tools/remotecommand"
	"net/http"
)

// Handler handler for pod
type Handler struct {
	Server    server.Server
	Processor Processor
}

// TerminalResponse is sent by handleExecShell. The Id is a random session id that binds the original REST request and the SockJS connection.
// Any clientapi in possession of this Id can hijack the terminal session.
type TerminalResponse struct {
	Id string `json:"id"`
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
}

// New builder method for pod.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("pod related APIs").ApiVersion("v1").Path("/api/v1/pod")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "pod")))

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/container").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get Pod deatil").
				Param(ws.PathParameter("name", "name of pod")).
				Param(ws.PathParameter("namespace", "namespace of pod")).
				To(handler.GetPodDetail).
				Returns(http.StatusOK, "Get Pod Detail Complete", PodDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			// this is pod  not name and it must be pod
			ws.GET("/{namespace}/{pod}/shell/{container}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Filter(clientFilter.ConfigFilter).
				Doc("Get Pod deatil").
				Param(ws.PathParameter("pod", "name of pod")).
				Param(ws.PathParameter("namespace", "namespace of pod")).
				Param(ws.PathParameter("container", "container of pod")).
				To(handler.ExecShell).
				Returns(http.StatusOK, "Get Pod Detail Complete", TerminalResponse{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetPodList get pod instances detail
func (h Handler) GetPodDetail(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	result, err := container.GetPodContainers(k8sClient, namespace, name)
	logger.Debug("get pod detail end", log.Err(err))

	h.WriteResponse(result, err, req, res)
}

// ExecShell exec to pod instances
func (h Handler) ExecShell(req *restful.Request, res *restful.Response) {
	sessionId, err := genTerminalSessionId()
	logger := abcontext.Logger(req.Request.Context())

	k8sClient := abcontext.Client(req.Request.Context())
	config := localcontext.ConfigClient(req.Request.Context())

	terminalSessions.Set(sessionId, TerminalSession{
		id:       sessionId,
		bound:    make(chan error),
		sizeChan: make(chan remotecommand.TerminalSize),
	})
	go WaitForTerminal(k8sClient, config, req, sessionId)

	logger.Debug("exec to pod end", log.Err(err))

	h.WriteResponse(TerminalResponse{Id: sessionId}, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
