package daemonset

import (
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/container"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/event"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/pod"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	v1 "k8s.io/api/apps/v1"
	"net/http"
	"strings"
)

// Handler handler for DaemonSet
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
}

// New builder method for application.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	//queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("DaemonSet related APIs").ApiVersion("v1").Path("/api/v1/daemonset")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "daemonset")))

	//ws.Route(
	//	// queryFilter adds all parameters for documentation
	//	// and injects a Query Builder filter as a middleware to enable
	//	// parsing req data and building a *dataselect.Query object into Context
	//	queryFilter.Build(
	//		// this method will add a few response types according to different http status
	//		// to the documentation
	//		abdecorator.WithAuth(
	//			ws.GET("").
	//				Filter(clientFilter.SecureFilter).
	//				Doc(`List DaemonSet instance`).
	//				To(handler.ListDaemonSet).
	//				Writes(DaemonSetList{}).
	//				Returns(http.StatusOK, "OK", DaemonSetList{}),
	//		),
	//	),
	//)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{daemonset}").
				Filter(clientFilter.SecureFilter).
				Doc("Get DaemonSet instance").
				Param(ws.PathParameter("namespace", "namespace of daemonset")).
				Param(ws.PathParameter("daemonset", "name of daemonset")).
				To(handler.GetDaemonSetDetail).
				Returns(http.StatusOK, "Create DaemonSet Complete", DaemonSetDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{daemonset}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Update DaemonSet instance").
				Param(ws.PathParameter("namespace", "namespace of daemonset")).
				Param(ws.PathParameter("daemonset", "name of daemonset")).
				To(handler.UpdateDaemonSet).
				Returns(http.StatusOK, "Update DaemonSet Complete", DaemonSet{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{daemonset}/yaml").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update daemonset yaml").
				Param(ws.PathParameter("namespace", "namespace of daemonset")).
				Param(ws.PathParameter("daemonset", "name of daemonset")).
				To(handler.UpdateDaemonSetDetailYaml).
				Returns(http.StatusOK, "update daemonset yaml", v1.DaemonSet{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{daemonset}/event").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("get daemonset event").
				Param(ws.PathParameter("namespace", "namespace of daemonset")).
				Param(ws.PathParameter("daemonset", "name of daemonset")).
				To(handler.GetDaemonSetEvents).
				Returns(http.StatusOK, "get daemonset event", common.EventList{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{daemonset}/pods").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("get daemonset pods").
				Param(ws.PathParameter("namespace", "namespace of daemonset")).
				Param(ws.PathParameter("daemonset", "name of daemonset")).
				To(handler.GetDaemonSetPods).
				Returns(http.StatusOK, "get daemonset pods", pod.PodList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{daemonset}/container/{container}/").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update daemonset container").
				Param(ws.PathParameter("namespace", "namespace of daemonset")).
				Param(ws.PathParameter("daemonset", "name of daemonset")).
				To(handler.PutDaemonSetContainer).
				Returns(http.StatusOK, "update daemonset container", v1.DaemonSet{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{daemonset}/container/{container}/image").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update daemonset container").
				Param(ws.PathParameter("namespace", "namespace of daemonset")).
				Param(ws.PathParameter("daemonset", "name of daemonset")).
				To(handler.UpdateDaemonSetContainerImage).
				Returns(http.StatusOK, "update daemonset container", v1.DaemonSet{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{daemonset}/container/{container}/resources").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update daemonset resources").
				Param(ws.PathParameter("namespace", "namespace of daemonset")).
				Param(ws.PathParameter("daemonset", "name of daemonset")).
				To(handler.UpdateDaemonSetContainerResources).
				Returns(http.StatusOK, "update daemonset resources", v1.DaemonSet{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{daemonset}/container/{container}/env").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update daemonset env").
				Param(ws.PathParameter("namespace", "namespace of daemonset")).
				Param(ws.PathParameter("daemonset", "name of daemonset")).
				To(handler.UpdateDaemonSetContainerEnv).
				Returns(http.StatusOK, "update daemonset env", v1.DaemonSet{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}/{daemonset}/container/{container}/volumeMount").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update daemonset volumeMount").
				Param(ws.PathParameter("namespace", "namespace of daemonset")).
				Param(ws.PathParameter("daemonset", "name of daemonset")).
				To(handler.CreateDaemonSetVolumeMount).
				Returns(http.StatusOK, "update daemonset volumeMount", v1.DaemonSet{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

func (h Handler) GetDaemonSetDetail(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("daemonset")

	result, err := GetDaemonSetDetail(k8sClient, namespace, name)
	logger.Debug("get daemonset detail end", log.Err(err))

	h.WriteResponse(result, err, req, res)
}

func (h Handler) UpdateDaemonSet(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("daemonset")
	spec := new(v1.DaemonSet)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	result, err := UpdateDeamonSetOriginal(k8sClient, namespace, name, spec)
	logger.Debug("get daemonset detail end", log.Err(err))

	h.WriteResponse(result, err, req, res)
}

// UpdateDaemonSetDetailYaml
func (h Handler) UpdateDaemonSetDetailYaml(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("daemonset")
	spec := new(v1.DaemonSet)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	daemonset, err := UpdateDeamonSetOriginal(k8sClient, namespace, name, spec)
	logger.Debug("update daemon detail end", log.Err(err))

	h.WriteResponse(daemonset, err, req, res)
}

//GetDaemonSetEvents
func (h Handler) GetDaemonSetEvents(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("daemonset")
	dataSelect := common.ParseDataSelectPathParameter(req)

	result, err := event.GetResourceEvents(k8sClient, dataSelect, namespace, name)

	h.WriteResponse(result, err, req, res)
}

//GetDaemonSetPods
func (h Handler) GetDaemonSetPods(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("daemonset")
	podList, err := GetDaemonSetPods(k8sClient, dataselect.DefaultDataSelect, name, namespace)

	h.WriteResponse(podList, err, req, res)
}

//PutDaemonSetContainer
func (h Handler) PutDaemonSetContainer(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("daemonset")
	containerName := req.PathParameter("container")
	isDryRun := (strings.ToLower(req.QueryParameter("isDryRun")) == "true")

	spec := new(container.UpdateContainerRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	daemonset, err := PutDaemonsetContainer(k8sClient, namespace, name, containerName, isDryRun, *spec)
	logger.Debug("get daemonset detail end", log.Err(err))

	h.WriteResponse(daemonset, err, req, res)
}

//UpdateDaemonSetContainerImage
func (h Handler) UpdateDaemonSetContainerImage(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("daemonset")
	containerName := req.PathParameter("container")

	spec := new(container.UpdateContainerImageRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	daemonset, err := UpdateContainerImage(k8sClient, namespace, name, containerName, *spec)
	logger.Debug("get daemonset detail end", log.Err(err))

	h.WriteResponse(daemonset, err, req, res)
}

//UpdateDaemonSetContainerResources
func (h Handler) UpdateDaemonSetContainerResources(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("daemonset")
	containerName := req.PathParameter("container")

	spec := new(container.UpdateContainerResourceRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	daemonset, err := UpdateContainerResource(k8sClient, namespace, name, containerName, *spec)
	logger.Debug("get daemonset detail end", log.Err(err))

	h.WriteResponse(daemonset, err, req, res)
}

//UpdateDaemonSetContainerEnv
func (h Handler) UpdateDaemonSetContainerEnv(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("daemonset")
	containerName := req.PathParameter("container")

	spec := new(container.UpdateContainerEnvRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	daemonset, err := UpdateContainerEnv(k8sClient, namespace, name, containerName, *spec)
	logger.Debug("get daemonset detail end", log.Err(err))

	h.WriteResponse(daemonset, err, req, res)
}

//CreateDaemonSetVolumeMount
func (h Handler) CreateDaemonSetVolumeMount(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("daemonset")
	containerName := req.PathParameter("container")

	spec := new(common.VolumeInfo)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	daemonset, err := CreateDaemonSetVolumeMount(k8sClient, namespace, name, containerName, *spec)
	logger.Debug("get daemonset detail end", log.Err(err))

	h.WriteResponse(daemonset, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
