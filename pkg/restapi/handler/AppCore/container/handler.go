package container

import (
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/log"
	"strconv"

	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/logs"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/emicklei/go-restful"
	"net/http"
)

// Handler handler for container log
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
}

// New builder method for container log.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("container log related APIs").ApiVersion("v1").Path("/api/v1/log")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "container log")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}/{pod}").
					Filter(clientFilter.SecureFilter).
					Doc(`get pod log `).
					To(handler.GetLogs).
					Writes(logs.LogDetails{}).
					Returns(http.StatusOK, "OK", logs.LogDetails{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{pod}/{container}").
				Filter(clientFilter.SecureFilter).
				Doc(`get pod log `).
				To(handler.GetLogs).
				Writes(logs.LogDetails{}).
				Returns(http.StatusOK, "OK", logs.LogDetails{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/file/{namespace}/{pod}/{container}").
				Filter(clientFilter.SecureFilter).
				Doc(`get pod log file`).
				To(handler.GetLogsFile).
				Writes(logs.LogDetails{}).
				Returns(http.StatusOK, "OK", logs.LogDetails{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetStorageClassList get container log instances list
func (h Handler) GetLogs(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	podID := req.PathParameter("pod")
	containerID := req.PathParameter("container")

	refTimestamp := req.QueryParameter("referenceTimestamp")
	if refTimestamp == "" {
		refTimestamp = logs.NewestTimestamp
	}

	refLineNum, err := strconv.Atoi(req.QueryParameter("referenceLineNum"))
	if err != nil {
		refLineNum = 0
	}
	usePreviousLogs := req.QueryParameter("previous") == "true"
	offsetFrom, err1 := strconv.Atoi(req.QueryParameter("offsetFrom"))
	offsetTo, err2 := strconv.Atoi(req.QueryParameter("offsetTo"))
	logFilePosition := req.QueryParameter("logFilePosition")

	logSelector := logs.DefaultSelection
	if err1 == nil && err2 == nil {
		logSelector = &logs.Selection{
			ReferencePoint: logs.LogLineId{
				LogTimestamp: logs.LogTimestamp(refTimestamp),
				LineNum:      refLineNum,
			},
			OffsetFrom:      offsetFrom,
			OffsetTo:        offsetTo,
			LogFilePosition: logFilePosition,
		}
	}

	result, err := GetLogDetails(k8sClient, namespace, podID, containerID, logSelector, usePreviousLogs)

	logger.Debug("get pod log end", log.Err(err))
	h.WriteResponse(result, err, req, res)
}

// GetStorageClassList get container log instances detail
func (h Handler) GetLogsFile(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())
	namespace := req.PathParameter("namespace")
	podID := req.PathParameter("pod")
	containerID := req.PathParameter("container")

	usePreviousLogs := req.QueryParameter("previous") == "true"

	result, err := GetLogFile(k8sClient, namespace, podID, containerID, usePreviousLogs)

	logger.Debug("get deployment detail end", log.Err(err))
	h.WriteResponse(result, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
