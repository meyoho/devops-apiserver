package canI

import (
	"errors"
	"net/http"

	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	authv1 "k8s.io/api/authorization/v1"

	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/secret"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	//"text/template"
)

// Handler handler for container log
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
}

// New builder method for container log.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("canI log related APIs").ApiVersion("v1").Path("")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "can I")))

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/api/v1/cani").
				Filter(clientFilter.SecureFilter).
				Doc(`Validates access for user`).
				To(handler.CanI).
				Writes(CanIResponse{}).
				Returns(http.StatusOK, "OK", CanIResponse{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/api/v1/caniadmin").
				Filter(clientFilter.SecureFilter).
				Doc(`get pod log file`).
				To(handler.CanIadmin).
				Writes(CanIResponse{}).
				Returns(http.StatusOK, "OK", CanIResponse{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/api/callback/oauth/{namespace}/secret/{secretNamespace}/{secretName}/codereposervice/{serviceName}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("oauth callback").
				Param(ws.PathParameter("secretNamespace", "secretNamespace of secret")).
				Param(ws.PathParameter("secretName", "secretName")).
				Param(ws.PathParameter("serviceName", "serviceName")).
				Param(ws.PathParameter("namespace", "namespace of secret")).
				To(handler.OAuthCallback).
				Returns(http.StatusOK, "callback oauth ", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/api/appConfig.json").
				Filter(clientFilter.SecureFilter).
				Doc(`get pod log file`).
				To(handler.ConfigHandler).
				Writes(CanIResponse{}).
				Returns(http.StatusOK, "OK", CanIResponse{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetStorageClassList get container log instances list
func (h Handler) CanI(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	var allow bool
	payload := new(authv1.SelfSubjectAccessReviewSpec)
	if err := req.ReadEntity(payload); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	accessReview := &authv1.SelfSubjectAccessReview{Spec: *payload}
	cani, err := k8sClient.AuthorizationV1().SelfSubjectAccessReviews().Create(accessReview)

	if err != nil {
		logger.Error("get can I  detail end", log.Err(err))
		allow = false
	} else {
		allow = cani.Status.Allowed
	}
	allowed := CanIResponse{Allowed: allow}
	res.WriteHeaderAndEntity(http.StatusOK, allowed)
	h.WriteResponse(nil, err, req, res)
}

// GetStorageClassList get container log instances detail
func (h Handler) CanIadmin(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	var allow bool
	accessReview := &authv1.SelfSubjectAccessReview{
		Spec: authv1.SelfSubjectAccessReviewSpec{
			ResourceAttributes: &authv1.ResourceAttributes{
				Verb:     "create",
				Group:    "auth.alauda.io",
				Version:  "v1alpha1",
				Resource: "projects",
			},
		},
	}
	cani, err := k8sClient.AuthorizationV1().SelfSubjectAccessReviews().Create(accessReview)

	if err != nil {
		logger.Error("get can I  detail end", log.Err(err))
		allow = false
	} else {
		allow = cani.Status.Allowed
	}
	allowed := CanIResponse{Allowed: allow}
	res.WriteHeaderAndEntity(http.StatusOK, allowed)
	h.WriteResponse(nil, err, req, res)
}

func (h Handler) OAuthCallback(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	namespace := req.PathParameter("namespace")
	secretNamespace := req.PathParameter("secretNamespace")
	secretName := req.PathParameter("secretName")
	serviceName := req.PathParameter("serviceName")
	code := req.QueryParameter("code")

	errorDescription := req.QueryParameter("error_description")
	var err error
	if code != "" {
		err = secret.OAuthCallback(devopsClient, k8sClient, namespace, secretNamespace, secretName, serviceName, code)
	} else if errorDescription != "" {
		err = errors.New(errorDescription)
	} else {
		err = errors.New("except 'code' from third party, but not")
	}
	logger.Error("oauthcallback end", log.Err(err))

	h.WriteResponse(nil, err, req, res)

}

//GetStorageClassList get container log instances detail
func (h Handler) ConfigHandler(req *restful.Request, res *restful.Response) {
	//configTemplate, err := template.New(ConfigTemplateName).Parse(ConfigTemplate)
	//if err != nil{
	//	h.WriteResponse(nil, err, req, res)
	//}
	//write := new(http.ResponseWriter)

	//configTemplate.Execute(*write, GetAppConfigJSON())
	res.AddHeader("Content-Type", "application/json")
	h.WriteResponse(GetAppConfigJSON(), nil, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
