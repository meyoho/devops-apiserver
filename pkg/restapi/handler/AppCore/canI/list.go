package canI

import (
	"encoding/json"
	"log"
	"net/http"
	"text/template"
	"time"
)

// CanIResponse response for a can I request
type CanIResponse struct {
	Allowed bool `json:"allowed"`
}

// AppConfig is a global configuration of application.
type AppConfig struct {
	// ServerTime is current server time.
	ServerTime int64 `json:"serverTime"`
}

const (
	// ConfigTemplateName is a name of config template
	ConfigTemplateName = "appConfig"
	// ConfigTemplate is a template of a config
	ConfigTemplate = "{{.}}"
)

func GetAppConfigJSON() string {
	log.Println("Getting application global configuration")

	config := &AppConfig{
		ServerTime: time.Now().UTC().UnixNano() / 1e6,
	}

	jsonConfig, _ := json.Marshal(config)
	log.Printf("Application configuration %s", jsonConfig)
	return string(jsonConfig)
}

func ConfigHandler(w http.ResponseWriter, r *http.Request) (int, error) {
	configTemplate, err := template.New(ConfigTemplateName).Parse(ConfigTemplate)
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		return http.StatusInternalServerError, err
	}
	return http.StatusOK, configTemplate.Execute(w, GetAppConfigJSON())
}
