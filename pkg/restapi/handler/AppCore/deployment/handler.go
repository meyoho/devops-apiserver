package deployment

import (
	appCore "alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/container"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/event"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/handlecommon"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/network"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/pod"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/replicaset"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	v1 "k8s.io/api/apps/v1"
	"k8s.io/client-go/kubernetes"
	"net/http"
	"strings"
)

// Handler handler for Deployment
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	GetDeploymentDetail(ctx context.Context, appCoreClient *appCore.ApplicationClient, k8sclient kubernetes.Interface, namespace, name string) (detail *DeploymentDetail, err error)
	UpdateDeployment(ctx context.Context, appCoreClient *appCore.ApplicationClient, k8sclient kubernetes.Interface, namespace, name string, spec DeploymentSpec, isDryRun bool) (*DeploymentResult, error)
	UpdateDeploymentNetwork(ctx context.Context, appCoreClient *appCore.ApplicationClient, k8sclient kubernetes.Interface, namespace, name string, spec *network.UpdateNetworkSpec) error
	GetDeploymentOldReplicaSets(ctx context.Context, k8sclient kubernetes.Interface, namespace, name string, dsQuery *dataselect.Query) (*replicaset.ReplicaSetList, error)
	RollBackToSpecialRevision(ctx context.Context, k8sclient kubernetes.Interface, namespace, name string, spec common.RevisionDetail) (*v1.Deployment, error)
	UpdateDeploymentDetailYaml(ctx context.Context, k8sclient kubernetes.Interface, namespace string, deploymentName string, deploy *v1.Deployment) (*v1.Deployment, error)
	UpdateDeploymentReplica(ctx context.Context, k8sclient kubernetes.Interface, namespace string, deploymentName string, deploy DeploymentReplica) (*v1.Deployment, error)
}

// New builder method for application.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	//queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("Deployment related APIs").ApiVersion("v1").Path("/api/v1/deployment")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "deployment")))

	//ws.Route(
	//	// queryFilter adds all parameters for documentation
	//	// and injects a Query Builder filter as a middleware to enable
	//	// parsing req data and building a *dataselect.Query object into Context
	//	queryFilter.Build(
	//		// this method will add a few response types according to different http status
	//		// to the documentation
	//		abdecorator.WithAuth(
	//			ws.GET("").
	//				Filter(clientFilter.SecureFilter).
	//				Doc(`List Deployment instance`).
	//				To(handler.ListDeployment).
	//				Writes(DeploymentList{}).
	//				Returns(http.StatusOK, "OK", DeploymentList{}),
	//		),
	//	),
	//)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{deployment}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get Deployment instance").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.GetDeploymentDetail).
				Returns(http.StatusOK, "Create Deployment Complete", DeploymentDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{deployment}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Update Deployment instance").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.UpdateDeployment).
				Returns(http.StatusOK, "Update Deployment Complete", DeploymentResult{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{deployment}/network").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Update Deployment instance").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.UpdateDeploymentNetwork).
				Returns(http.StatusOK, "Update Deployment Network Complete", DeploymentResult{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{deployment}/oldreplicaset").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get Deployment old replicaset").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.GetDeploymentOldReplicaSets).
				Returns(http.StatusOK, "Get Deployment Old Replicaset", replicaset.ReplicaSetList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}/{deployment}/actions/rollback").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Roll Back To Speial Revision").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.RollBackToSpecialRevision).
				Returns(http.StatusOK, "Roll Back To Speial Revision", v1.Deployment{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{deployment}/start").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("start deployment").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.StartStopDeployment).
				Returns(http.StatusOK, "start deployment Complete", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{deployment}/stop").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("start deployment").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.StartStopDeployment).
				Returns(http.StatusOK, "start deployment Complete", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{deployment}/yaml").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update deployment yaml").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.UpdateDeploymentDetailYaml).
				Returns(http.StatusOK, "update deployment yaml", v1.Deployment{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{deployment}/replicas").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update deployment yaml").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.UpdateDeploymentReplica).
				Returns(http.StatusOK, "update deployment replica", v1.Deployment{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{deployment}/event").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("get deployment event").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.GetDeploymentEvents).
				Returns(http.StatusOK, "get deployment event", common.EventList{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{deployment}/pods").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("get deployment pods").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.GetDeploymentPods).
				Returns(http.StatusOK, "get deployment pods", pod.PodList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{deployment}/container/{container}/").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update deployment container").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.PutDeploymentContainer).
				Returns(http.StatusOK, "update deployment container", v1.Deployment{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{deployment}/container/{container}/image").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update deployment container").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.UpdateDeploymentContainerImage).
				Returns(http.StatusOK, "update deployment container", v1.Deployment{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{deployment}/container/{container}/resources").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update deployment resources").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.UpdateDeploymentContainerResources).
				Returns(http.StatusOK, "update deployment resources", v1.Deployment{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{deployment}/container/{container}/env").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update deployment env").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.UpdateDeploymentContainerEnv).
				Returns(http.StatusOK, "update deployment env", v1.Deployment{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}/{deployment}/container/{container}/volumeMount").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update deployment volumeMount").
				Param(ws.PathParameter("namespace", "namespace of deployment")).
				Param(ws.PathParameter("deployment", "name of deployment")).
				To(handler.CreateDeploymentVolumeMount).
				Returns(http.StatusOK, "update deployment volumeMount", v1.Deployment{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListDeployment list application instances
//func (h Handler) ListDeployment(req *restful.Request, res *restful.Response) {
//	k8sClient := abcontext.Client(req.Request.Context())
//
//	dataSelect := common.ParseDataSelectPathParameter(req)
//	namespace := common.ParseNamespacePathParameter(req)
//
//	result, err := h.Processor.ListDeployment(req.Request.Context(), k8sClient, namespace, dataSelect)
//
//	h.WriteResponse(result, err, req, res)
//}

func (h Handler) GetDeploymentDetail(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")

	result, err := h.Processor.GetDeploymentDetail(req.Request.Context(), appCoreClient, k8sClient, namespace, name)

	h.WriteResponse(result, err, req, res)
}

func (h Handler) UpdateDeployment(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	spec := new(DeploymentSpec)

	isDryRun := (strings.ToLower(req.QueryParameter("isDryRun")) == "true")
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)

	}

	result, err := h.Processor.UpdateDeployment(req.Request.Context(), appCoreClient, k8sClient, namespace, name, *spec, isDryRun)

	h.WriteResponse(result, err, req, res)
}

func (h Handler) UpdateDeploymentNetwork(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	spec := &network.UpdateNetworkSpec{}

	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)

	}
	err := h.Processor.UpdateDeploymentNetwork(req.Request.Context(), appCoreClient, k8sClient, namespace, name, spec)

	h.WriteResponse(nil, err, req, res)
}

func (h Handler) GetDeploymentOldReplicaSets(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	dataSelect := common.ParseDataSelectPathParameter(req)

	replicasetlist, err := h.Processor.GetDeploymentOldReplicaSets(req.Request.Context(), k8sClient, namespace, name, dataSelect)

	h.WriteResponse(replicasetlist, err, req, res)
}

func (h Handler) RollBackToSpecialRevision(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	spec := new(common.RevisionDetail)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)

	}

	deployment, err := h.Processor.RollBackToSpecialRevision(req.Request.Context(), k8sClient, namespace, name, *spec)

	h.WriteResponse(deployment, err, req, res)
}

func (h Handler) StartStopDeployment(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	spec := new(common.RevisionDetail)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)

	}

	deploy, err := k8sClient.Apps().Deployments(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	var list []v1.Deployment
	list = append(list, *deploy)
	isStop := strings.HasSuffix(req.Request.URL.Path, "stop")
	if !isStop {
		err = handlecommon.StartResources(k8sClient, list, nil)
	} else {
		err = handlecommon.StopResources(k8sClient, list, nil)
	}

	if err != nil {
		h.WriteResponse(nil, err, req, res)
	} else {
		res.WriteHeader(http.StatusNoContent)
		h.WriteResponse(nil, err, req, res)
	}

}

// UpdateDeploymentDetailYaml
func (h Handler) UpdateDeploymentDetailYaml(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	spec := new(v1.Deployment)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	deployment, err := h.Processor.UpdateDeploymentDetailYaml(req.Request.Context(), k8sClient, namespace, name, spec)

	h.WriteResponse(deployment, err, req, res)
}

//UpdateDeploymentReplica
func (h Handler) UpdateDeploymentReplica(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	spec := new(DeploymentReplica)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	deployment, err := h.Processor.UpdateDeploymentReplica(req.Request.Context(), k8sClient, namespace, name, *spec)

	h.WriteResponse(deployment, err, req, res)
}

//GetDeploymentEvents
func (h Handler) GetDeploymentEvents(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	dataSelect := common.ParseDataSelectPathParameter(req)

	result, err := event.GetResourceEvents(k8sClient, dataSelect, namespace, name)

	h.WriteResponse(result, err, req, res)
}

//GetDeploymentPods
func (h Handler) GetDeploymentPods(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	podList, err := GetDeploymentPods(k8sClient, dataselect.DefaultDataSelect, namespace, name)

	h.WriteResponse(podList, err, req, res)
}

//PutDeploymentContainer
func (h Handler) PutDeploymentContainer(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	containerName := req.PathParameter("container")
	isDryRun := (strings.ToLower(req.QueryParameter("isDryRun")) == "true")

	spec := new(container.UpdateContainerRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	deployment, err := PutDeploymentContainer(k8sClient, namespace, name, containerName, isDryRun, *spec)

	h.WriteResponse(deployment, err, req, res)
}

//UpdateDeploymentContainerImage
func (h Handler) UpdateDeploymentContainerImage(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	containerName := req.PathParameter("container")

	spec := new(container.UpdateContainerImageRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	deployment, err := UpdateContainerImage(k8sClient, namespace, name, containerName, *spec)

	h.WriteResponse(deployment, err, req, res)
}

//UpdateDeploymentContainerResources
func (h Handler) UpdateDeploymentContainerResources(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	containerName := req.PathParameter("container")

	spec := new(container.UpdateContainerResourceRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	deployment, err := UpdateContainerResource(k8sClient, namespace, name, containerName, *spec)

	h.WriteResponse(deployment, err, req, res)
}

//UpdateDeploymentContainerEnv
func (h Handler) UpdateDeploymentContainerEnv(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	containerName := req.PathParameter("container")

	spec := new(container.UpdateContainerEnvRequest)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	deployment, err := UpdateContainerEnv(k8sClient, namespace, name, containerName, *spec)
	h.WriteResponse(deployment, err, req, res)
}

//CreateDeploymentVolumeMount
func (h Handler) CreateDeploymentVolumeMount(req *restful.Request, res *restful.Response) {

	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("deployment")
	containerName := req.PathParameter("container")

	spec := new(common.VolumeInfo)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	deployment, err := CreateDeploymentVolumeMount(k8sClient, namespace, name, containerName, *spec)

	h.WriteResponse(deployment, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
