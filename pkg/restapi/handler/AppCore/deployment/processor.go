package deployment

import (
	appCore "alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/ingress"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/network"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/replicaset"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/service"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	//extensionsv1beta1 "k8s.io/api/extensions/v1beta1"

	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	apps "k8s.io/api/apps/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"strconv"

	hpa "alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/horizontalpodautoscaler"
	"context"
	goErrors "errors"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListApplication list application instances

func (p processor) GetDeploymentDetail(ctx context.Context, appCoreClient *appCore.ApplicationClient, k8sclient kubernetes.Interface, namespace, deploymentName string) (detail *DeploymentDetail, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("get deployment detail end", log.Err(err))
	}()
	deployment, err := GetDeploymentDetailOriginal(k8sclient, namespace, deploymentName)
	if err != nil {
		return nil, err
	}

	// 取pod值
	matchLabels := deployment.Spec.Selector.MatchLabels
	var labelname string
	for key, value := range matchLabels {
		labelname += key + "=" + value + ","
	}
	if len(labelname) > 0 {
		labelname = string(labelname[0 : len(labelname)-1])
	}
	options := metaV1.ListOptions{LabelSelector: labelname}
	podListStruct, _ := k8sclient.CoreV1().Pods(namespace).List(options)

	isSidecar := sidecarContainer(*podListStruct)
	var podAnnotations string
	if isSidecar {
		podAnnotations = "true"
	} else {
		podAnnotations = "false"
	}

	res, err := common.ConvertResourceToUnstructured(deployment)
	if err != nil {
		return nil, err
	}

	app, err := appCoreClient.FindApplication(common.LocalBaseDomain(), res)
	if err != nil {
		return nil, err
	}
	if app == nil {
		return nil, goErrors.New("Find App failed")
	}

	ingresses, err := ingress.GetFormCore(*app)
	if err != nil {
		return nil, err
	}

	services, err := service.GetFormCore(*app)
	if err != nil {
		return nil, err
	}

	rc, nonCriticalErrors, err := common.GetRelationResource(k8sclient, namespace)
	if err != nil {
		return nil, err
	}

	_, err, rc.HorizontalPodAutoscalerList = hpa.GetHorizontalPodAutoscalerListForResource(k8sclient, namespace, api.ResourceKindDeployment, deploymentName)
	if err != nil {
		return nil, err
	}

	rc.Services = services
	rc.Ingresses = ingresses

	return toDeploymentDetail(deployment, rc, nonCriticalErrors, podAnnotations), nil

}

func (p processor) UpdateDeployment(ctx context.Context, appCoreClient *appCore.ApplicationClient, k8sclient kubernetes.Interface, namespace, name string, spec DeploymentSpec, isDryRun bool) (*DeploymentResult, error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("update deployment end")

	}()
	deployment, err := GetDeploymentDetailOriginal(k8sclient, namespace, name)
	if err != nil {
		return nil, err
	}

	deployUnstr, err := common.ConvertResourceToUnstructured(deployment)
	if err != nil {
		return nil, err
	}

	app, err := appCoreClient.FindApplication(common.LocalBaseDomain(), deployUnstr)
	if err != nil {
		return nil, err
	}
	if app == nil {
		return nil, goErrors.New("Find App failed")
	}

	ingresses, err := ingress.GetFormCore(*app)
	if err != nil {
		return nil, err
	}

	services, err := service.GetFormCore(*app)
	if err != nil {
		return nil, err
	}
	oldResources, err := network.GetNetworkResources(deployment.Spec.Template.Spec.Containers, ingresses, services, namespace, deployment.Spec.Selector.MatchLabels)
	if err != nil {
		return nil, err
	}
	oldResources = append(oldResources, *deployUnstr)
	newYamlList, err := GenerateYaml(namespace, spec, deployment)
	if err != nil {
		return nil, err
	}
	// combine the two resource list, when the resource exist in both list
	// use the old resource metadata
	combinedList := common.CombineResourceList(oldResources, newYamlList)
	// remove old deployment resources
	RemovedList := common.RemoveSubList(app.Resources, oldResources)
	// add merged resourcelist to app
	finalList := common.AddSubList(RemovedList, combinedList)
	if isDryRun {
		return &DeploymentResult{
			Resources: &combinedList,
			Result:    nil,
		}, nil
	}
	_, result := appCoreClient.UpdateApplication(namespace, app.GetAppCrd().GetName(), &finalList)
	return &DeploymentResult{
		Resources: &combinedList,
		Result:    result,
	}, nil
}

func (p processor) UpdateDeploymentNetwork(ctx context.Context, appCoreClient *appCore.ApplicationClient, k8sclient kubernetes.Interface, namespace, name string, updateSpec *network.UpdateNetworkSpec) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("update deployment network end", log.Err(err))

	}()
	deployment, err := GetDeploymentDetailOriginal(k8sclient, namespace, name)
	if err != nil {
		return err
	}

	utr, err := common.ConvertResourceToUnstructured(deployment)
	if err != nil {
		return err
	}

	app, err := appCoreClient.FindApplication(common.LocalBaseDomain(), utr)
	if err != nil {
		return err
	}

	is, err := ingress.GetFormCore(*app)
	if err != nil {
		return err
	}

	ss, err := service.GetFormCore(*app)
	if err != nil {
		return err
	}

	var ingressName, serviceName string
	if updateSpec.Type == network.TypeExternal && updateSpec.OldExternalNetworkInfo != nil {
		ingressName = updateSpec.OldExternalNetworkInfo.IngressName
		serviceName = updateSpec.OldExternalNetworkInfo.ServiceName
	} else if updateSpec.Type == network.TypeInternal && updateSpec.OldInternalNetworkInfo != nil {
		serviceName = updateSpec.OldInternalNetworkInfo.ServiceName
	} else if updateSpec.Type == network.TypeNodePort && updateSpec.OldExternalNodePortInfo != nil {
		serviceName = updateSpec.OldExternalNodePortInfo.ServiceName
	}

	matchService := getMatchService(serviceName, ss)
	matchIngress := getMatchIngress(ingressName, is)

	yamlList, err := network.UpdateNetworkInfo(matchIngress, matchService, updateSpec, namespace, deployment.Spec.Template.Labels, "")
	if err != nil {
		return err
	}
	return handleUpdateYamlList(yamlList, appCoreClient, namespace, app.GetAppCrd().GetName())
}

func (p processor) GetDeploymentOldReplicaSets(ctx context.Context, client kubernetes.Interface, namespace, deploymentName string, dsQuery *dataselect.Query) (oldReplicaSetList *replicaset.ReplicaSetList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("get deploymentreplicasets end", log.Err(err))

	}()

	oldReplicaSetList = &replicaset.ReplicaSetList{
		ReplicaSets: make([]replicaset.ReplicaSet, 0),
		ListMeta:    api.ListMeta{TotalItems: 0},
	}

	deployment, err := client.AppsV1().Deployments(namespace).Get(deploymentName, api.GetOptionsInCache)
	if err != nil {
		return oldReplicaSetList, err
	}

	selector, err := metaV1.LabelSelectorAsSelector(deployment.Spec.Selector)
	if err != nil {
		return oldReplicaSetList, err
	}
	options := metaV1.ListOptions{LabelSelector: selector.String(), ResourceVersion: "0"}

	channels := &common.ResourceChannels{
		ReplicaSetList: common.GetReplicaSetListChannelWithOptions(client,
			common.NewSameNamespaceQuery(namespace), options, 1),
		PodList: common.GetPodListChannelWithOptions(client,
			common.NewSameNamespaceQuery(namespace), options, 1),
		EventList: common.GetEventListChannelWithOptions(client,
			common.NewSameNamespaceQuery(namespace), options, 1),
	}

	rawRs := <-channels.ReplicaSetList.List
	if err := <-channels.ReplicaSetList.Error; err != nil {
		return oldReplicaSetList, err
	}

	rawPods := <-channels.PodList.List
	if err := <-channels.PodList.Error; err != nil {
		return oldReplicaSetList, err
	}

	rawEvents := <-channels.EventList.List
	err = <-channels.EventList.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return oldReplicaSetList, criticalError
	}

	rawRepSets := make([]*apps.ReplicaSet, 0)
	for i := range rawRs.Items {
		rawRepSets = append(rawRepSets, &rawRs.Items[i])
	}
	oldRs, _, err := FindOldReplicaSets(deployment, rawRepSets)
	if err != nil {
		return oldReplicaSetList, err
	}

	oldReplicaSets := make([]apps.ReplicaSet, len(oldRs))
	for i, replicaSet := range oldRs {
		oldReplicaSets[i] = *replicaSet
	}

	if dsQuery == nil {
		dsQuery = dataselect.NewDataSelectQuery(dataselect.NoPagination, dataselect.NoSort, dataselect.NoFilter)
	}

	oldReplicaSetList = replicaset.ToReplicaSetList(oldReplicaSets, rawPods.Items, rawEvents.Items,
		nonCriticalErrors, dsQuery)
	return oldReplicaSetList, nil
}

func (p processor) RollBackToSpecialRevision(ctx context.Context, client kubernetes.Interface, namespace, deploymentName string, ucrv common.RevisionDetail) (deployment *apps.Deployment, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("roll back to sepcoal revisionend", log.Err(err))

	}()
	deployment, err = GetDeploymentDetailOriginal(client, namespace, deploymentName)
	if err != nil {
		return
	}

	//not deal paused
	if deployment.Spec.Paused {
		err = goErrors.New("you cannot rollback a paused deployment")
		return
	}

	replicaSetList, err := GetDeploymentOldReplicaSets(client, nil, namespace, deploymentName)
	if err != nil {
		return
	}
	var result replicaset.ReplicaSet
	//if Revision is set to -1, get the last revision
	if ucrv.Revision == -1 {

		for index, rs := range replicaSetList.ReplicaSets {
			var intRevision int64
			intRevision, err = strconv.ParseInt(replicaSetList.ReplicaSets[index].Revision, 10, 0)
			if err != nil {
				return
			}
			if intRevision >= ucrv.Revision {
				ucrv.Revision = intRevision
				result = rs
			}
		}
	} else {
		isExist := false
		//check if the revision exist
		for index, rs := range replicaSetList.ReplicaSets {
			var intRevision int64
			intRevision, err = strconv.ParseInt(replicaSetList.ReplicaSets[index].Revision, 10, 0)
			if err != nil {
				return
			}
			if intRevision == ucrv.Revision {
				isExist = true
				result = rs
				break
			}
		}
		if !isExist {
			err = goErrors.New("input revision is not valid")
			return
		}
	}

	olddeployment, err := client.AppsV1().Deployments(namespace).Get(deploymentName, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}
	if olddeployment == nil {
		err = goErrors.New("get deployment is nil, can not update deployment")
		return
	}
	olddeployment.Spec.Template = result.Spec.Template

	deployment, err = client.AppsV1().Deployments(namespace).Update(olddeployment)
	return

}

func (p processor) UpdateDeploymentReplica(ctx context.Context, client kubernetes.Interface, namespace string, deploymentName string, replicas DeploymentReplica) (deploy *apps.Deployment, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("update deployment deatil yaml")

	}()
	deploy, err = GetDeploymentDetailOriginal(client, namespace, deploymentName)
	if err != nil {
		return
	}

	deploy.Spec.Replicas = &replicas.Replicas
	deploy, err = client.AppsV1().Deployments(namespace).Update(deploy)
	setTypeMeta(deploy)
	return
}

//UpdateDeploymentDetailYaml
func (p processor) UpdateDeploymentDetailYaml(ctx context.Context, client kubernetes.Interface, namespace string, deploymentName string, deploy *apps.Deployment) (*apps.Deployment, error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("update deployment deatil yaml")

	}()
	old, err := GetDeploymentDetailOriginal(client, namespace, deploymentName)
	if err != nil {
		return nil, err
	}
	deploy.ObjectMeta.ResourceVersion = old.ObjectMeta.ResourceVersion
	deploy.ObjectMeta.Generation = old.ObjectMeta.Generation
	deploy.ObjectMeta.UID = old.ObjectMeta.UID
	deploy, err = client.AppsV1().Deployments(namespace).Update(deploy)
	setTypeMeta(deploy)
	return deploy, err

}
