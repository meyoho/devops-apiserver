package storageclass

import (
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"

	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"net/http"
)

// Handler handler for storageclass
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
}

// New builder method for storageclass.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("storageclass related APIs").ApiVersion("v1").Path("/api/v1/storageclass")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "storageclass")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.SecureFilter).
					Doc(`List storageclass instance`).
					To(handler.GetStorageClassList).
					Writes(StorageClassList{}).
					Returns(http.StatusOK, "OK", StorageClassList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get storageclass deatil").
				Param(ws.PathParameter("name", "name of storageclass")).
				To(handler.GetStorageClass).
				Returns(http.StatusOK, "Get StorageClass Detail Complete", StorageClass{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetStorageClassList get storageclass instances list
func (h Handler) GetStorageClassList(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())

	dataSelect := common.ParseDataSelectPathParameter(req)
	result, err := GetStorageClassList(k8sClient, dataSelect)
	logger.Debug("get deployment detail end", log.Err(err))

	h.WriteResponse(result, err, req, res)
}

// GetStorageClassList get storageclass instances detail
func (h Handler) GetStorageClass(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())
	name := req.PathParameter("name")

	result, err := GetStorageClass(k8sClient, name)
	logger.Debug("get deployment detail end", log.Err(err))
	h.WriteResponse(result, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
