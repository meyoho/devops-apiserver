package configmap

import (
	"context"
	"net/http"

	"alauda.io/app-core/pkg/app"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
)

// Handler handler for configmap
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	GetConfigMapList(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, nsQuery *common.NamespaceQuery, dsQuery *dataselect.Query) (*ConfigMapList, error)
	GetConfigMapDetail(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string) (*ConfigMapDetail, error, *v1.ConfigMap)
	CreateConfigMap(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace string, spec *ConfigMapDetail) (*ConfigMapDetail, error)
	UpdateConfigMap(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace string, spec *ConfigMapDetail) (*ConfigMapDetail, error)
	DeleteConfigMap(ctx context.Context, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string) error
}

// New builder method for configmap.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("configmap related APIs").ApiVersion("v1").Path("/api/v1/configmap")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "configmap")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.SecureFilter).
					Filter(clientFilter.AppClientFilter).
					Doc(`List configmap instance`).
					To(handler.GetConfigMapList).
					Writes(ConfigMapList{}).
					Returns(http.StatusOK, "OK", ConfigMapList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Get ConifigMap deatil").
				Param(ws.PathParameter("name", "name of configmap")).
				Param(ws.PathParameter("namespace", "namespace of configmap")).
				To(handler.GetConfigMapDetail).
				Returns(http.StatusOK, "Get ConfigMap Detail Complete", ConfigMapDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("Create ConifigMap ").
				Param(ws.PathParameter("name", "name of configmap")).
				Param(ws.PathParameter("namespace", "namespace of configmap")).
				To(handler.CreateConfigMap).
				Returns(http.StatusOK, "Create ConfigMap ", ConfigMapDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{configmap}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update ConifigMap instance").
				Param(ws.PathParameter("name", "name of configmap")).
				Param(ws.PathParameter("namespace", "namespace of configmap")).
				To(handler.UpdateConfigMap).
				Returns(http.StatusOK, "Update ConfigMap Complete", ConfigMapDetail{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{configmap}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("delete ConifigMap instance").
				Param(ws.PathParameter("name", "name of configmap")).
				Param(ws.PathParameter("namespace", "namespace of configmap")).
				To(handler.DeleteConfigMap).
				Returns(http.StatusOK, "Delete ConfigMap Complete", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("{namespace}/{name}/actions/tradeapp").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("update ConifigMap Belong app").
				Param(ws.PathParameter("name", "name of configmap")).
				Param(ws.PathParameter("namespace", "namespace of configmap")).
				To(handler.UpdateConfigMapBelongApp).
				Returns(http.StatusOK, "Update ConfigMap belong app ", ConfigMapDetail{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetConfigMapList get configmap instances list
func (h Handler) GetConfigMapList(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	appCoreClient := localcontext.AppClient(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	dataSelect := common.ParseDataSelectPathParameter(req)

	result, err := h.Processor.GetConfigMapList(req.Request.Context(), k8sClient, appCoreClient, namespace, dataSelect)
	h.WriteResponse(result, err, req, res)
}

// GetConfigMapList get configmap instances detail
func (h Handler) GetConfigMapDetail(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())
	appCoreClient := localcontext.AppClient(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	result, err, _ := h.Processor.GetConfigMapDetail(req.Request.Context(), k8sClient, appCoreClient, namespace, name)
	h.WriteResponse(result, err, req, res)
}

// CreateConfigMap create configmap instances
func (h Handler) CreateConfigMap(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	spec := new(ConfigMapDetail)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	extra := localcontext.ExtraConfig(req.Request.Context())
	curNamespace := common.GetCurNamespace(namespace, extra.CredentialsNamespace)

	result, err := h.Processor.CreateConfigMap(req.Request.Context(), k8sClient, appCoreClient, curNamespace, spec)
	h.WriteResponse(result, err, req, res)
}

//UpdateConfigMap update configmap instance

func (h Handler) UpdateConfigMap(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	spec := new(ConfigMapDetail)
	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	result, err := h.Processor.UpdateConfigMap(req.Request.Context(), k8sClient, appCoreClient, namespace, spec)
	h.WriteResponse(result, err, req, res)
}

//DeleteConfigMap delete configmap instance
func (h Handler) DeleteConfigMap(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("configmap")
	err := h.Processor.DeleteConfigMap(req.Request.Context(), k8sClient, appCoreClient, namespace, name)
	h.WriteResponse(nil, err, req, res)
}

// UpdateConfigMapBelongApp create configmap instances
func (h Handler) UpdateConfigMapBelongApp(req *restful.Request, res *restful.Response) {
	appCoreClient := localcontext.AppClient(req.Request.Context())
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	spec := new(common.AppNameDetail)

	if err := req.ReadEntity(spec); err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	result, err, originResource := h.Processor.GetConfigMapDetail(req.Request.Context(), k8sClient, appCoreClient, namespace, name)
	if err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	err = common.UpdateResourceBelongApplication(appCoreClient, originResource,
		namespace, result.AppName, spec.AppName)
	if err != nil {
		h.WriteResponse(nil, err, req, res)
	}

	h.WriteResponse(result, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
