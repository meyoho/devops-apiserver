package configmap

import (
	appCore "alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"fmt"
	v1 "k8s.io/api/core/v1"
	k8serror "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"

	"alauda.io/devops-apiserver/pkg/restapi/handler/common"

	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/log"
	"context"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/client-go/kubernetes"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListApplication list configmap instances
func (p processor) GetConfigMapList(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, nsQuery *common.NamespaceQuery, dsQuery *dataselect.Query) (configmaplist *ConfigMapList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetConfigMapDetail end", log.Err(err))

	}()

	channels := &common.ResourceChannels{
		ConfigMapList: common.GetConfigMapListChannel(client, nsQuery, 1),
	}

	return GetConfigMapListFromChannels(channels, dsQuery, appCoreClient)
}

func (p processor) GetConfigMapDetail(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace, name string) (cmdetail *ConfigMapDetail, err error, rawConfigMap *v1.ConfigMap) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("GetConfigMapDetail end", log.String("name", name), log.String("namespace", namespace), log.Err(err))

	}()

	rawConfigMap, err = client.CoreV1().ConfigMaps(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err, nil
	}
	setTypeMeta(rawConfigMap)

	cmdetail, err = getConfigMapDetailAndAppName(rawConfigMap, appCoreClient)
	if err != nil {
		return nil, err, nil
	}

	return cmdetail, nil, rawConfigMap
}

func (p processor) CreateConfigMap(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace string, spec *ConfigMapDetail) (details *ConfigMapDetail, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create configmap end", log.Err(err), log.String("namespace", namespace))

	}()

	configmap := &v1.ConfigMap{
		ObjectMeta: metaV1.ObjectMeta{
			Name:        spec.ObjectMeta.Name,
			Namespace:   namespace,
			Annotations: spec.ObjectMeta.Annotations,
		},
		Data: spec.Data,
	}
	setTypeMeta(configmap)

	var newConfigMap *v1.ConfigMap

	if spec.AppName == "" {
		newConfigMap, err = client.CoreV1().ConfigMaps(namespace).Create(configmap)
		if err != nil {
			return nil, err
		}
	} else {
		err = common.CreateResourceAndIpmortToApplication(appCoreClient, configmap,
			namespace, spec.AppName)
		if err != nil {
			return nil, err
		}
		_, err, newConfigMap = GetConfigMapDetail(client, appCoreClient, namespace, spec.ObjectMeta.Name)
		if err != nil {
			return nil, err
		}
	}

	details = &ConfigMapDetail{
		ObjectMeta: api.NewObjectMeta(newConfigMap.ObjectMeta),
		TypeMeta:   api.NewTypeMeta(api.ResourceKindConfigMap),
		AppName:    spec.AppName,
		Data:       newConfigMap.Data,
	}

	return details, err
}

func (p processor) UpdateConfigMap(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace string, spec *ConfigMapDetail) (details *ConfigMapDetail, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create configmap end", log.Err(err))

	}()
	oldConfigMapDetail, err, configmap := GetConfigMapDetail(client, appCoreClient, namespace, spec.ObjectMeta.Name)
	if err != nil {
		return nil, err
	}

	newMeta := api.NewRawObjectMeta(spec.ObjectMeta)
	configmap.ObjectMeta = api.CompleteMeta(newMeta, configmap.ObjectMeta)
	configmap.Data = spec.Data
	setTypeMeta(configmap)

	if oldConfigMapDetail.AppName != "" && spec.AppName != "" && spec.AppName == oldConfigMapDetail.AppName {
		//old hava val,new have val, and same,update use appcore api
		err = common.UpdateResourceWithApplication(appCoreClient, configmap, namespace, spec.AppName)
	} else if oldConfigMapDetail.AppName != "" && spec.AppName != "" && spec.AppName != oldConfigMapDetail.AppName {
		//appName change ,remove old ,update use appcore api
		err = updateConfigMapWithAppNameChange(appCoreClient, oldConfigMapDetail.AppName, spec.AppName, namespace, configmap)

	} else if oldConfigMapDetail.AppName != "" && spec.AppName == "" {
		// old have val,new no val ,remove old ,origin update new
		err = updateConfigMapWithNoNewAppName(client, appCoreClient, oldConfigMapDetail.AppName, namespace, configmap)

	} else if oldConfigMapDetail.AppName == "" && spec.AppName != "" {
		//old no value,new have val
		err = common.UpdateResourceWithApplication(appCoreClient, configmap,
			namespace, spec.AppName)
	} else if oldConfigMapDetail.AppName == "" && spec.AppName == "" {
		//old no val,new no val,use origin api
		_, err = client.CoreV1().ConfigMaps(namespace).Update(configmap)
	} else {
		err = nil
	}

	if err != nil {
		return nil, err
	}

	_, err, newConfigMap := GetConfigMapDetail(client, appCoreClient, namespace, spec.ObjectMeta.Name)
	if err != nil {
		return nil, err
	}

	details = &ConfigMapDetail{
		ObjectMeta: api.NewObjectMeta(newConfigMap.ObjectMeta),
		TypeMeta:   api.NewTypeMeta(api.ResourceKindConfigMap),
		Data:       newConfigMap.Data,
		AppName:    spec.AppName,
	}

	return details, err

}

func (p processor) DeleteConfigMap(ctx context.Context, client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("create configmap end", log.Err(err))

	}()
	detailConfigMap, err, originConfigMap := GetConfigMapDetail(client, appCoreClient, namespace, name)
	if err != nil {
		logger.Error("can't get configmap detail err is %v", log.Err(err))
		return err
	}
	setTypeMeta(originConfigMap)

	if detailConfigMap.AppName != "" {
		return k8serror.NewForbidden(schema.GroupResource{}, originConfigMap.GetName(),
			fmt.Errorf("configmap '%s' is not allowed to delete if it is relate by app", originConfigMap.GetName()))
	}

	err = client.CoreV1().ConfigMaps(namespace).Delete(name, &metaV1.DeleteOptions{})
	if err != nil {
		return err
	}

	return err

}
