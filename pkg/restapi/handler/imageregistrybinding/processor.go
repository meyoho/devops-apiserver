package imageregistrybinding

import (
	"context"
	"strings"

	"alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/imageregistry"
	"alauda.io/devops-apiserver/pkg/restapi/handler/secret"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListImageRegistry list imageregistry instances
func (p processor) ListImageRegistryBinding(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *ImageRegistryBindingList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list imageregistrybinding end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	// fetch data from list
	imageregistrylist, err := client.DevopsV1alpha1().ImageRegistryBindings(namespace.ToRequestParam()).List(v1alpha1.ListOptions())

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &ImageRegistryBindingList{
		Items:    make([]v1alpha1.ImageRegistryBinding, 0),
		ListMeta: api.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(imageregistrylist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	provider := localcontext.ExtraConfig(ctx).AnnotationProvider
	data.Items = ConvertToImageRegistrySlice(result, provider)
	data.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeleteImageRegistry delete imageregistrybinding instance
func (p processor) DeleteImageRegistryBinding(ctx context.Context, client versioned.Interface, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete imageregistrybinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().ImageRegistryBindings(namespace).Delete(name, &metav1.DeleteOptions{})
	return
}

// GetImageRegistryBinding retrieve imageregistrybinding instance
func (p processor) GetImageRegistryBinding(ctx context.Context, client versioned.Interface, namespace, name string) (data *v1alpha1.ImageRegistryBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get imageregistrybinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().ImageRegistryBindings(namespace).Get(name, common.GetOptionsInCache)
	data.Kind = devops.ResourceKindImageRegistryBinding
	return
}

//CreateImageRegistryBinding create imageregistrybinding instance
func (p processor) CreateImageRegistryBinding(ctx context.Context, client versioned.Interface, imageregistrybinding *v1alpha1.ImageRegistryBinding, namespace string) (data *v1alpha1.ImageRegistryBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create imageregistrybinding end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().ImageRegistryBindings(namespace).Create(imageregistrybinding)
	return
}

//UpdateImageRegistryBinding update a imageregistrybinding instance
func (p processor) UpdateImageRegistryBinding(ctx context.Context, client versioned.Interface, imageregistrybinding, oldimageregistrybinding *v1alpha1.ImageRegistryBinding, namespace, name string) (data *v1alpha1.ImageRegistryBinding, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update imageregistrybinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	binding := oldimageregistrybinding.DeepCopy()
	binding.SetAnnotations(imageregistrybinding.GetAnnotations())
	binding.Spec = imageregistrybinding.Spec

	data, err = client.DevopsV1alpha1().ImageRegistryBindings(namespace).Update(binding)
	return
}

// GetImageRepositoryListInBinding  get repo list in this binding
func (p processor) GetImageRepositoryListInBinding(ctx context.Context, client versioned.Interface, namespace, name string, query *dataselect.Query) (repolist *api.ImageRepositoryList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update imageregistrybinding end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	binding, err := client.DevopsV1alpha1().ImageRegistryBindings(namespace).Get(name, common.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	irList, err := client.DevopsV1alpha1().ImageRepositories(namespace).List(api.ListEverything)
	if err != nil {
		log.Error("get imagerepository err", log.Err(err))
	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	annotationprovider := localcontext.ExtraConfig(ctx).AnnotationProvider
	var repos []v1alpha1.ImageRepository
	for _, item := range irList.Items {
		for _, condition := range binding.Status.Conditions {
			if item.GetName() == condition.Name {
				repos = append(repos, item)
			}
		}
	}
	return toList(repos, nonCriticalErrors, query, annotationprovider), nil
}

// GetImageOriginRepositoryList get imageoriginrepository
func (p processor) GetImageOriginRepositoryList(ctx context.Context, client versioned.Interface, namespace, name string) (*api.ImageRegistryBindingRepositoriesDetails, error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update imageregistrybinding end", log.String("namespace", namespace), log.String("name", name))
	}()

	result := client.DevopsV1alpha1().RESTClient().Get().Namespace(namespace).
		Name(name).Resource("imageregistrybindings").SubResource("repositories").Do()
	if result.Error() != nil {
		return nil, result.Error()
	}

	obj, err := result.Get()
	if err != nil {
		return nil, err
	}

	bindingRepositories := obj.(*v1alpha1.ImageRegistryBindingRepositories)
	return &api.ImageRegistryBindingRepositoriesDetails{
		ImageRegistryBindingRepositories: bindingRepositories,
	}, nil
}

func (p processor) GetImageRepositorySecretList(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string, query *dataselect.Query) (secretlist secret.SecretList, err error) {
	extra := localcontext.ExtraConfig(ctx)
	credentialsNamespace := extra.CredentialsNamespace
	systemNamespace := extra.SystemNamespace
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create imageregistry end", log.Err(err))
	}()

	binding, err := GetImageRegistryBinding(client, namespace, name)
	if err != nil {
		logger.Error("get coderepobinding err", log.Err(err))
	}

	namespaceQuery := common.NewSameNamespaceQuery(namespace)
	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider

	secretList, err := secret.GetSecretList(k8sclient, appclient, namespaceQuery, query, false, credentialsNamespace, systemNamespace, annotationProvider)
	if err != nil {
		logger.Error("get secret List err ", log.Err(err))
		return
	}
	var secrets []secret.Secret
	for _, se := range secretList.Secrets {
		if binding.Spec.Secret.Name == se.ObjectMeta.Name && binding.ObjectMeta.Namespace == se.ObjectMeta.Namespace {
			secrets = append(secrets, se)
			break
		}
	}

	secretlist = secret.SecretList{
		ListMeta: api.ListMeta{TotalItems: 1},
		Secrets:  secrets,
	}
	return
}

func (p processor) GetImageOriginRepositoryProjectList(ctx context.Context, devopsClient versioned.Interface, namespace, name string, query *dataselect.Query) (result *api.ImageRegistryBindingRepositoriesDetails, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create imageregistry end", log.Err(err))
	}()

	logName := "handleGetImageOriginRepositoryProjectList"

	result, err = p.GetImageOriginRepositoryList(ctx, devopsClient, namespace, name)
	if err != nil {
		return
	}

	//start with project no repository

	binding, err := p.GetImageRegistryBinding(ctx, devopsClient, namespace, name)
	log.Infof("%v binding is %#v", logName, binding)
	if err != nil {
		return
	}

	if registry, getRegistryErr := devopsClient.DevopsV1alpha1().ImageRegistries().Get(binding.Spec.ImageRegistry.Name, v1alpha1.GetOptions()); getRegistryErr != nil {
		return nil, getRegistryErr
	} else if registry.Spec.Type != "Harbor" {
		return
	}

	projectList, err := GetProjectList(devopsClient, binding.Spec.ImageRegistry.Name, binding.Spec.Secret.Name, binding.Spec.Secret.Namespace)
	log.Infof("%s projectList is %#v", logName, projectList)
	if err != nil {
		log.Infof("%s projectList error %#v", logName, err)
		return
	}

	vmap := make(map[string]string)

	for _, project := range projectList.Items {
		vmap[project.ObjectMeta.Name] = project.ObjectMeta.Name
	}
	log.Infof("%s vmap is %#v", logName, vmap)

	for _, repo := range result.Items {
		delete(vmap, strings.Split(repo, "/")[0])
	}
	log.Infof("%s vmap is %#v", logName, vmap)

	for k := range vmap {
		result.Items = append(result.Items, k)
	}
	return
}

func toList(imageRepositories []v1alpha1.ImageRepository, nonCriticalErrors []error, dsQuery *dataselect.Query, provider devops.AnnotationProvider) *api.ImageRepositoryList {
	irList := &api.ImageRepositoryList{
		Items:    make([]v1alpha1.ImageRepository, 0),
		ListMeta: api.ListMeta{TotalItems: len(imageRepositories)},
	}

	irCells, filteredTotal := dataselect.GenericDataSelectWithFilter(imageregistry.ToCells(imageRepositories, provider), dsQuery)
	imageRepositories = imageregistry.FromCells(irCells)
	irList.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	irList.Errors = nonCriticalErrors

	for _, repo := range imageRepositories {
		irList.Items = append(irList.Items, toDetailsInList(repo))
	}

	return irList
}

func toDetailsInList(imageRepository v1alpha1.ImageRepository) v1alpha1.ImageRepository {
	crs := v1alpha1.ImageRepository{
		ObjectMeta: imageRepository.ObjectMeta,
		TypeMeta:   metav1.TypeMeta{Kind: api.ResourceKindImageRepository},
		Spec:       imageRepository.Spec,
		Status:     imageRepository.Status,
	}
	return crs
}

func GetImageRegistryBinding(client versioned.Interface, namespace, name string) (*v1alpha1.ImageRegistryBinding, error) {
	irb, err := client.DevopsV1alpha1().ImageRegistryBindings(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	return irb, nil
}

// ConvertToImageRegistrySlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToImageRegistrySlice(filtered []metav1.Object, provider devops.AnnotationProvider) (items []v1alpha1.ImageRegistryBinding) {
	items = make([]v1alpha1.ImageRegistryBinding, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.ImageRegistryBinding); ok {
			cm.ObjectMeta.Annotations[provider.AnnotationsKeyToolType()] = v1alpha1.ToolChainArtifactRepositoryName
			cm.Kind = common.ResourceKindImageRegistryBinding
			items = append(items, *cm)
		}
	}
	return
}

func GetProjectList(client versioned.Interface, name string, secretName, secretNamespace string) (*v1alpha1.ProjectDataList, error) {
	logName := "GetProjectList"

	result := client.DevopsV1alpha1().RESTClient().Get().
		Name(name).Resource("imageregistries").SubResource("projects").Param("secretname", secretName).Param("namespace", secretNamespace).Do()
	log.Infof("%s result is %#v", logName, result)
	if result.Error() != nil {
		log.Infof("%s result error %#v", logName, result.Error())
		return nil, result.Error()
	}

	obj, err := result.Get()
	log.Infof("obj %v err %v", obj, err)
	if err != nil {
		log.Infof("%s obj error %#v", logName, result.Error())
		return nil, err
	}

	projectList := obj.(*v1alpha1.ProjectDataList)
	log.Infof("%s projectList is %#v", logName, projectList)
	return projectList, nil
}
