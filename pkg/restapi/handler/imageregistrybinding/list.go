package imageregistrybinding

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/api"
)

// ImageRegistryBindingList contains a list of ImageRegistryBinding in the cluster.
type ImageRegistryBindingList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of ImageRegistryBinding.
	Items []v1alpha1.ImageRegistryBinding `json:"imageregistrybindings"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type CronCheckResult struct {
	Next     string `json:"next"`
	Previous string `json:"previous"`
	SanityZh string `json:"sanity_zh_cn"`
	SanityEn string `json:"sanity_en"`
	Error    string `json:"error"`
}

type APIResponse struct {
	Data   CronCheckResult `json:"data"`
	Status string          `json:"status"`
}
