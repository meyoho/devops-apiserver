package imageregistrybinding

import (
	"context"
	"net/http"

	"alauda.io/devops-apiserver/pkg/restapi/api"

	"alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/secret"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for ImageRegistry
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListImageRegistryBinding(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*ImageRegistryBindingList, error)
	DeleteImageRegistryBinding(ctx context.Context, client versioned.Interface, namespace, name string) error
	GetImageRegistryBinding(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.ImageRegistryBinding, error)
	CreateImageRegistryBinding(ctx context.Context, client versioned.Interface, imageregistrybinding *v1alpha1.ImageRegistryBinding, namespace string) (*v1alpha1.ImageRegistryBinding, error)
	UpdateImageRegistryBinding(ctx context.Context, client versioned.Interface, imageregistrybinding, oldimageregistrybinding *v1alpha1.ImageRegistryBinding, namespace, name string) (*v1alpha1.ImageRegistryBinding, error)
	GetImageRepositoryListInBinding(ctx context.Context, client versioned.Interface, namespace, name string, query *dataselect.Query) (*api.ImageRepositoryList, error)
	GetImageOriginRepositoryList(ctx context.Context, client versioned.Interface, namespace, name string) (*api.ImageRegistryBindingRepositoriesDetails, error)
	GetImageRepositorySecretList(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace, name string, query *dataselect.Query) (secret.SecretList, error)
	GetImageOriginRepositoryProjectList(ctx context.Context, client versioned.Interface, namespace, name string, query *dataselect.Query) (*api.ImageRegistryBindingRepositoriesDetails, error)
}

// New builder method for imageregistrybinding.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ImageRegistryBinding related APIs").ApiVersion("v1").Path("/api/v1/imageregistrybinding")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "imageregistrybinding")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List ImageRegistryBinding instances`).
					To(handler.ListImageRegistryBinding).
					Writes(ImageRegistryBindingList{}).
					Returns(http.StatusOK, "OK", ImageRegistryBindingList{}),
			),
		),
	)
	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List ImageRegistryBinding instances`).
					To(handler.ListImageRegistryBinding).
					Writes(ImageRegistryBindingList{}).
					Returns(http.StatusOK, "OK", ImageRegistryBindingList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create ImageRegistryBinding instance").
				To(handler.CreateImageRegistryBinding).
				Returns(http.StatusOK, "Create ImageRegistryBinding instance", v1alpha1.ImageRegistryBinding{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ImageRegistrybinding details").
				Param(ws.PathParameter("name", "name of the imageregistrybinding")).
				Param(ws.PathParameter("namespace", "namespace of the imageregistrybinding")).
				To(handler.GetImageRegistryBinding).
				Returns(http.StatusOK, "Get ImageRegistryBinding details Complete", v1alpha1.ImageRegistryBinding{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete ImageRegistry instance").
				Param(ws.PathParameter("name", "name of the imageregistrybinding")).
				Param(ws.PathParameter("namespace", "namespace of the imageregistrybinding")).
				To(handler.DeleteImageRegistryBinding).
				Returns(http.StatusOK, "Retrieve ImageRegistrybinding instance", struct{}{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update ImageRegistrybinding instance").
				Param(ws.PathParameter("name", "name of the imageregistry")).
				Param(ws.PathParameter("namespace", "namespace of the imageregistrybinding")).
				To(handler.UpdateImageRegistryBinding).
				Returns(http.StatusOK, "Update ImageRegistrybinding instance", v1alpha1.ImageRegistryBinding{}),
		),
	)

	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("/{namespace}/{name}/repositories").
					Filter(clientFilter.DevOpsClientFilter).
					Doc("Get ImageRegistrybinding Repository List").
					Param(ws.PathParameter("name", "name of the imageregistrybinding")).
					Param(ws.PathParameter("namespace", "namespace of the imageregistrybinding")).
					To(handler.GetImageRepositoryListInBinding).
					Returns(http.StatusOK, "Update ImageRegistrybinding instance", api.ImageRepositoryList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/remote-repositories").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ImageRegistrybinding Repository List").
				Param(ws.PathParameter("name", "name of the imageregistrybinding")).
				Param(ws.PathParameter("namespace", "namespace of the imageregistrybinding")).
				To(handler.GetImageOriginRepositoryList).
				Returns(http.StatusOK, "Update ImageRegistrybinding instance", api.ImageRepositoryList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/secrets").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ImageRegistrybinding Secret List").
				Param(ws.PathParameter("name", "name of the imageregistrybinding")).
				Param(ws.PathParameter("namespace", "namespace of the imageregistrybinding")).
				To(handler.GetImageRepositorySecretList).
				Returns(http.StatusOK, "Get ImageRegistrybinding secret list ", secret.SecretList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/remote-repositories-project").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ImageRegistrybinding remote repositories project").
				Param(ws.PathParameter("name", "name of the imageregistrybinding")).
				Param(ws.PathParameter("namespace", "namespace of the imageregistrybinding")).
				To(handler.GetImageOriginRepositoryProjectList).
				Returns(http.StatusOK, "Get ImageRegistrybinding remote repositories project ", api.ImageRepositoryList{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListImageRegistry list imageregistry instances
func (h Handler) ListImageRegistryBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	imageregistrybinding, err := h.Processor.ListImageRegistryBinding(req.Request.Context(), client, query, common.ParseNamespacePathParameter(req))
	common.WriteResponseBySort(imageregistrybinding, err, req, res, h)
}

//DeleteImageRegistry Delete imageregistry instance
func (h Handler) DeleteImageRegistryBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteImageRegistryBinding(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//GetImageRegistryBinding Get imageregistrybinding instance
func (h Handler) GetImageRegistryBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.GetImageRegistryBinding(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//CreateImageRegistryBinding create imageregistrybinding instance
func (h Handler) CreateImageRegistryBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newImageRegistryBinding := new(v1alpha1.ImageRegistryBinding)

	if err := req.ReadEntity(newImageRegistryBinding); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	if newImageRegistryBinding.GetSecretName() != "" {
		opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
			SecretName: newImageRegistryBinding.GetSecretName(),
			Namespace:  newImageRegistryBinding.GetSecretNamespace(),
		}
		authorizeResponse, err := client.DevopsV1alpha1().ImageRegistries().Authorize(newImageRegistryBinding.Spec.ImageRegistry.Name, &opts)
		if err != nil {
			log.Errorf("authorizeResponse %v", log.Any("authorizeResponse", authorizeResponse))
			h.WriteResponse(nil, err, req, res)
			return
		}
	}
	namespace := req.PathParameter("namespace")

	data, err := h.Processor.CreateImageRegistryBinding(req.Request.Context(), client, newImageRegistryBinding, namespace)
	h.WriteResponse(data, err, req, res)
}

// GetImageOriginRepositoryList get image origin repo list
func (h Handler) GetImageOriginRepositoryList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	name := req.PathParameter("name")

	namespace := req.PathParameter("namespace")
	list, err := h.Processor.GetImageOriginRepositoryList(req.Request.Context(), client, namespace, name)
	h.WriteResponse(list, err, req, res)
}

//UpdateImageRegistryBinding update imageregistrybinding instance
func (h Handler) UpdateImageRegistryBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newImageRegistryBinding := new(v1alpha1.ImageRegistryBinding)

	if err := req.ReadEntity(newImageRegistryBinding); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	if newImageRegistryBinding.GetSecretName() != "" {
		opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
			SecretName: newImageRegistryBinding.GetSecretName(),
			Namespace:  newImageRegistryBinding.GetSecretNamespace(),
		}
		authorizeResponse, err := client.DevopsV1alpha1().ImageRegistries().Authorize(newImageRegistryBinding.Spec.ImageRegistry.Name, &opts)
		if err != nil {
			log.Errorf("authorizeResponse %v and err is %v", log.Any("authorizeResponse", authorizeResponse), log.Err(err))
			h.WriteResponse(nil, err, req, res)
			return
		}
	}

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	oldImageRegistryBinding, err := h.Processor.GetImageRegistryBinding(req.Request.Context(), client, namespace, name)
	if err != nil {
		errors.HandleInternalError(res, err)
		return
	}

	data, err := h.Processor.UpdateImageRegistryBinding(req.Request.Context(), client, newImageRegistryBinding, oldImageRegistryBinding, namespace, name)
	h.WriteResponse(data, err, req, res)
}

func (h Handler) GetImageRepositoryListInBinding(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	name := req.PathParameter("name")
	namespace := req.PathParameter("namespace")
	list, err := h.Processor.GetImageRepositoryListInBinding(req.Request.Context(), client, namespace, name, query)
	h.WriteResponse(list, err, req, res)
}

func (h Handler) GetImageRepositorySecretList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	k8sclient := abcontext.Client(req.Request.Context())
	appclient := localcontext.AppClient(req.Request.Context())
	secretquery := common.ParseDataSelectPathParameter(req)
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	data, err := h.Processor.GetImageRepositorySecretList(req.Request.Context(), client, k8sclient, appclient, namespace, name, secretquery)
	h.WriteResponse(data, err, req, res)
}

//GetImageOriginRepositoryProjectList
func (h Handler) GetImageOriginRepositoryProjectList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := common.ParseDataSelectPathParameter(req)
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	data, err := h.Processor.GetImageOriginRepositoryProjectList(req.Request.Context(), client, namespace, name, query)
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
