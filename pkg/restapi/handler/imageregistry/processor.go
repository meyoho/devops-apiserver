package imageregistry

import (
	"context"

	"alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/coderepobinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	"alauda.io/devops-apiserver/pkg/restapi/handler/secret"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListImageRegistry list imageregistry instances
func (p processor) ListImageRegistry(ctx context.Context, client versioned.Interface, query *dataselect.Query) (data *ImageRegistryList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list imageregistry end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	imageregistrylist, err := client.DevopsV1alpha1().ImageRegistries().List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &ImageRegistryList{
		Items:    make([]v1alpha1.ImageRegistry, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(imageregistrylist.Items)

	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToImageRegistrySlice(result)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeleteImageRegistry delete imageregistry instance
func (p processor) DeleteImageRegistry(ctx context.Context, client versioned.Interface, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete ImageRegistry end", log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().ImageRegistries().Delete(name, &metav1.DeleteOptions{})
	return
}

// RetrieveImageRegistry retrieve imageregistry instance
func (p processor) RetrieveImageRegistry(ctx context.Context, client versioned.Interface, name string) (data *v1alpha1.ImageRegistry, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get imageregistry end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().ImageRegistries().Get(name, jenkins.GetOptionsInCache)
	return
}

func (p processor) GetImageRegistrySecretList(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace *common.NamespaceQuery, name string, query *dataselect.Query, credentialsNamespace, systemNamespace string) (secretlist secret.SecretList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create imageregistry end", log.Err(err))
	}()

	bindingQuery := dataselect.GeSimpleLabelQuery(common.CodeRepoServiceProperty, name)
	bindingList, err := coderepobinding.NewProcessor().ListCodeRepoBinding(ctx, client, bindingQuery, namespace)

	if err != nil {
		logger.Error("get coderepobinding err", log.Err(err))
	}
	provider := localcontext.ExtraConfig(ctx).AnnotationProvider
	secretList, err := secret.GetSecretList(k8sclient, appclient, namespace, query, false, credentialsNamespace, systemNamespace, provider)
	if err != nil {
		logger.Error("get secret List err ", log.Err(err))
		return
	}
	var secrets []secret.Secret
	for _, binding := range bindingList.Items {
		for _, se := range secretList.Secrets {
			if binding.Spec.Account.Secret.Name == se.ObjectMeta.Name && binding.ObjectMeta.Namespace == se.ObjectMeta.Namespace {
				secrets = append(secrets, se)
			}
		}
	}
	secretlist = secret.SecretList{
		ListMeta: api.ListMeta{TotalItems: len(secrets)},
		Secrets:  secrets,
	}
	return
}

//CreateImageRegistry create imageregistry instance
func (p processor) CreateImageRegistry(ctx context.Context, client versioned.Interface, imageregistry *v1alpha1.ImageRegistry) (data *v1alpha1.ImageRegistry, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create imageregistry end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().ImageRegistries().Create(imageregistry)
	return
}

//UpdateImageRegistry update a imageregistry instance
func (p processor) UpdateImageRegistry(ctx context.Context, client versioned.Interface, imageregistry *v1alpha1.ImageRegistry, name string) (data *v1alpha1.ImageRegistry, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update ImageRegistry end", log.Err(err), log.String("name", name))
	}()
	oldImageRegistry := new(v1alpha1.ImageRegistry)
	oldImageRegistry, err = client.DevopsV1alpha1().ImageRegistries().Get(name, jenkins.GetOptionsInCache)

	if err != nil {
		return
	}

	// if binded secret, do validation
	if len(imageregistry.Spec.Secret.Name) > 0 {
		if err = p.AuthorizeBySecret(ctx, client, name, imageregistry.Spec.Secret.Namespace, imageregistry.Spec.Secret.Name); err != nil {
			return
		}
	}

	oldImageRegistry.SetAnnotations(imageregistry.GetAnnotations())
	oldImageRegistry.Spec = imageregistry.Spec

	data, err = client.DevopsV1alpha1().ImageRegistries().Update(oldImageRegistry)
	return
}

func (p processor) GetImageOriginRepositoryProjectList(ctx context.Context, client versioned.Interface, name, secretNamespace, secret string) (result *api.ImageRegistryBindingRepositoriesDetails, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get imageregistry remote repositories end", log.Err(err))
	}()

	opts := &v1alpha1.CodeRepoServiceAuthorizeOptions{
		SecretName: secret,
		Namespace:  secretNamespace,
	}
	repositories, err := client.DevopsV1alpha1().ImageRegistries().GetImageRepos(name, opts)
	if err != nil {
		return nil, err
	}
	return &api.ImageRegistryBindingRepositoriesDetails{
		ImageRegistryBindingRepositories: repositories,
	}, nil

}

func (p processor) AuthorizeBySecret(ctx context.Context, client versioned.Interface, name, secretNamespace, secret string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Check imageregistry authorization by secret end", log.Err(err))
	}()

	opts := &v1alpha1.CodeRepoServiceAuthorizeOptions{
		SecretName: secret,
		Namespace:  secretNamespace,
	}

	_, err = client.DevopsV1alpha1().ImageRegistries().Authorize(name, opts)
	if err != nil {
		return err
	}

	return nil
}

// ConvertToImageRegistrySlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToImageRegistrySlice(filtered []metav1.Object) (items []v1alpha1.ImageRegistry) {
	items = make([]v1alpha1.ImageRegistry, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.ImageRegistry); ok {
			cm.Kind = v1alpha1.ResourceKindImageRegistry
			items = append(items, *cm)
		}
	}
	return
}
