package imageregistry

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"strings"
)

type imageRepositoryCell struct {
	*devopsv1alpha1.ImageRepository
	annotation devops.AnnotationProvider
}

func (self imageRepositoryCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case common.NameProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Name)
	case common.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.ObjectMeta.CreationTimestamp.Time)
	case common.LatestCommitAt:
		return dataselect.StdComparableTime(self.Status.LatestTag.GetCreatedAt())
	case common.NamespaceProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Namespace)
	case common.DisplayNameProperty:
		if len(self.ObjectMeta.Annotations) > 0 {
			if self.ObjectMeta.Annotations[self.annotation.AnnotationsKeyDisplayName()] != "" {
				return dataselect.StdLowerComparableString(self.ObjectMeta.Annotations[self.annotation.AnnotationsKeyDisplayName()])
			}
		}
		return dataselect.StdLowerComparableString(self.ObjectMeta.Name)
	case common.ImageRegistryBindingProperty:
		return dataselect.StdLowerComparableString(self.Spec.ImageRegistryBinding.Name)
	case common.LabelProperty:
		if len(self.ObjectMeta.Labels) > 0 {
			values := []string{}
			for k, v := range self.ObjectMeta.Labels {
				values = append(values, k+":"+v)
			}
			return dataselect.StdComparableLabel(strings.Join(values, ","))
		}
	}
	// if name is not supported then just return a constant dummy value, sort will have no effect.
	return nil
}

func ToCells(std []devopsv1alpha1.ImageRepository, provider devops.AnnotationProvider) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = imageRepositoryCell{&std[i], provider}
	}
	return cells
}

func FromCells(cells []dataselect.DataCell) []devopsv1alpha1.ImageRepository {
	std := make([]devopsv1alpha1.ImageRepository, len(cells))
	for i := range std {
		std[i] = *cells[i].(imageRepositoryCell).ImageRepository.DeepCopy()
	}
	return std
}
