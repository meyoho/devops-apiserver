package imageregistry_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/imageregistry"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Imageregistry", func() {

	var (
		processor         imageregistry.Processor
		ctx               context.Context
		client            *fake.Clientset
		query             *dataselect.Query
		err               error
		imageregistrylist *imageregistry.ImageRegistryList
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 1,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		client = fake.NewSimpleClientset()
		processor = imageregistry.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("get imageregistry in ns default", func() {
		client = fake.NewSimpleClientset(
			GetImageregistryList("imageregistry"),
		)
		imageregistrylist, err = processor.ListImageRegistry(ctx, client, query)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(imageregistrylist).ToNot(BeNil(), "should return a list")
		Expect(imageregistrylist.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(imageregistrylist.Items).To(HaveLen(1), "should have one items")
		Expect(imageregistrylist.Items[0].Name).To(Equal("imageregistry"))

	})

})

func GetImageregistryList(name string) *v1alpha1.ImageRegistryList {
	imageregistrylist := &v1alpha1.ImageRegistryList{
		Items: []v1alpha1.ImageRegistry{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
				},
				Spec: v1alpha1.ImageRegistrySpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name + "1",
				},
				Spec: v1alpha1.ImageRegistrySpec{},
			},
		},
	}
	return imageregistrylist
}
