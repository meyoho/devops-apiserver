package imageregistry

import (
	"context"
	"net/http"

	"alauda.io/app-core/pkg/app"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/secret"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for ImageRegistry
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListImageRegistry(ctx context.Context, client versioned.Interface, query *dataselect.Query) (*ImageRegistryList, error)
	DeleteImageRegistry(ctx context.Context, client versioned.Interface, name string) error
	RetrieveImageRegistry(ctx context.Context, client versioned.Interface, name string) (*v1alpha1.ImageRegistry, error)
	CreateImageRegistry(ctx context.Context, client versioned.Interface, ImageRegistry *v1alpha1.ImageRegistry) (*v1alpha1.ImageRegistry, error)
	UpdateImageRegistry(ctx context.Context, client versioned.Interface, ImageRegistry *v1alpha1.ImageRegistry, name string) (*v1alpha1.ImageRegistry, error)
	GetImageRegistrySecretList(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, appclient *app.ApplicationClient, namespace *common.NamespaceQuery, name string, query *dataselect.Query, credentialsNamespace, systemNamespace string) (secret.SecretList, error)
	GetImageOriginRepositoryProjectList(ctx context.Context, client versioned.Interface, name, secretNamespace, secret string) (*api.ImageRegistryBindingRepositoriesDetails, error)
	AuthorizeBySecret(ctx context.Context, client versioned.Interface, name, secretNamespace, secret string) error
}

// New builder method for imageregistry.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ImageRegistry related APIs").ApiVersion("v1").Path("/api/v1/imageregistry")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "imageregistry")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List ImageRegistry instance`).
					To(handler.ListImageRegistry).
					Writes(v1alpha1.ImageRegistryList{}).
					Returns(http.StatusOK, "OK", ImageRegistryList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create ImageRegistry instance").
				To(handler.CreateImageRegistry).
				Returns(http.StatusOK, "Create ImageRegistry Instance", v1alpha1.Jenkins{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete ImageRegistry instance").
				Param(ws.PathParameter("name", "name of the ImageRegistry")).
				To(handler.DeleteImageRegistry).
				Returns(http.StatusOK, "Delete ImageRegistry Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve ImageRegistry instance").
				Param(ws.PathParameter("name", "name of the ImageRegistry")).
				To(handler.RetrieveImageRegistry).
				Returns(http.StatusOK, "Retrieve ImageRegistry instance", v1alpha1.ImageRegistry{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}/secrets").
				Filter(clientFilter.DevOpsClientFilter).
				Filter(clientFilter.InsecureFilter).
				Filter(clientFilter.AppClientFilter).
				Doc("get imageregistry secret list").
				Param(ws.PathParameter("namespace", "namespace of the ImageRegistry")).
				Param(ws.PathParameter("name", "name of the ImageRegistry")).
				To(handler.GetImageRegistrySecretList).
				Returns(http.StatusOK, "get ImageRegistry secret list", secret.SecretList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update ImageRegistry instance").
				Param(restful.PathParameter("name", "ImageRegistry name to filter scope")).
				To(handler.UpdateImageRegistry).
				Returns(http.StatusOK, "Update ImageRegistry instance", v1alpha1.ImageRegistry{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}/remote-repositories-project").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ImageRegistry remote repositories project").
				Param(ws.PathParameter("name", "name of the imageregistry")).
				Param(ws.QueryParameter("namespace", "namespace of the secret")).
				Param(ws.QueryParameter("secretName", "name of the secret")).
				To(handler.GetImageOriginRepositoryList).
				Returns(http.StatusOK, "Get ImageRegistry remote repositories project ", api.ImageRepositoryList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}/authorize").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get ImageRegistry remote repositories project").
				Param(ws.PathParameter("name", "name of the imageregistry")).
				Param(ws.QueryParameter("namespace", "namespace of the secret")).
				Param(ws.QueryParameter("secretName", "name of the secret")).
				To(handler.AuthorizeBySecret).
				Returns(http.StatusOK, "Check ImageRegistry authorize by secret", struct{}{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListImageRegistry list imageregistry instances
func (h Handler) ListImageRegistry(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListImageRegistry(req.Request.Context(), client, query)
	h.WriteResponse(list, err, req, res)
}

//DeleteImageRegistry Delete ImageRegistry instance
func (h Handler) DeleteImageRegistry(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteImageRegistry(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//RetrieveImageRegistry Retrieve ImageRegistry instance
func (h Handler) RetrieveImageRegistry(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrieveImageRegistry(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// GetImageRegistrySecretlist get imageregistry secret list
func (h Handler) GetImageRegistrySecretList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	k8sclient := abcontext.Client(req.Request.Context())
	appclient := localcontext.AppClient(req.Request.Context())
	secretquery := common.ParseDataSelectPathParameter(req)
	namespace := common.ParseNamespacePathParameter(req)
	name := req.PathParameter("name")
	extra := localcontext.ExtraConfig(req.Request.Context())
	data, err := h.Processor.GetImageRegistrySecretList(req.Request.Context(), client, k8sclient, appclient, namespace, name, secretquery, extra.CredentialsNamespace, extra.SystemNamespace)
	h.WriteResponse(data, err, req, res)
}

//CreateImageRegistry create ImageRegistry instance
func (h Handler) CreateImageRegistry(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newImageRegistry := new(v1alpha1.ImageRegistry)

	if err := req.ReadEntity(newImageRegistry); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.CreateImageRegistry(req.Request.Context(), client, newImageRegistry)
	h.WriteResponse(data, err, req, res)
}

//UpdateJenkins update ImageRegistry instance
func (h Handler) UpdateImageRegistry(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newImageRegistry := new(v1alpha1.ImageRegistry)

	if err := req.ReadEntity(newImageRegistry); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.UpdateImageRegistry(req.Request.Context(), client, newImageRegistry, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// GetImageOriginRepositoryList get image origin repo list
func (h Handler) GetImageOriginRepositoryList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	name := req.PathParameter("name")

	list, err := h.Processor.GetImageOriginRepositoryProjectList(req.Request.Context(), client, name, req.QueryParameter("namespace"), req.QueryParameter("secretName"))
	h.WriteResponse(list, err, req, res)
}

// AuthorizeBySecret check authorization by secret
func (h Handler) AuthorizeBySecret(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	name := req.PathParameter("name")

	err := h.Processor.AuthorizeBySecret(req.Request.Context(), client, name, req.QueryParameter("namespace"), req.QueryParameter("secretName"))
	h.WriteResponse(struct{}{}, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
