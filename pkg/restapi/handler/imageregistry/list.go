package imageregistry

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
)

// ImageRegistryList contains a list of ImageRegistry in the cluster.
type ImageRegistryList struct {
	ListMeta jenkins.ListMeta `json:"listMeta"`

	// Unordered list of ImageRegistry.
	Items []v1alpha1.ImageRegistry `json:"imageregistries"`

	// list of non-critical errors, that occurred during resource retrieval
	Errors []error `json:"errors"`
}

// RetryRequest request a retry for pipeline
type RetryRequest struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`

	// empty for now
}

// LogDetails log details
type LogDetails struct {
	*v1alpha1.PipelineLog
}

// TaskDetails jenkins step details
type TaskDetails struct {
	*v1alpha1.PipelineTask
}
