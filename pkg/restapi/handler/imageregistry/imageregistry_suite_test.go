package imageregistry_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestImageregistry(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("imageregistry.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/imageregistry", []Reporter{junitReporter})
}
