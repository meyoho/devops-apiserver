package pipelinetasktemplate_test

import (
	"testing"

	"github.com/onsi/ginkgo/reporters"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestPipelineTaskTemplate(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("pipelinetasktempalte.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/pipelinetasktemplate", []Reporter{junitReporter})
}
