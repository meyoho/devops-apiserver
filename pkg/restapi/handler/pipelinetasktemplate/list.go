package pipelinetasktemplate

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
)

// PipelineTaskTemplateList contains a list of PipelineTaskTemplate
type PipelineTaskTemplateList struct {
	ListMeta jenkins.ListMeta                `json:"listMeta"`
	Items    []v1alpha1.PipelineTaskTemplate `json:"pipelinetasktemplates"`
	Errors   []error                         `json:"errors"`
}
