package pipelinetasktemplate

import (
	"context"
	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for PipelineTaskTemplate
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListPipelineTaskTemplate(ctx context.Context, client versioned.Interface, namespace string, query *dataselect.Query) (*PipelineTaskTemplateList, error)
	RetrievePipelineTaskTemplate(ctx context.Context, client versioned.Interface, namespace string, name string) (*v1alpha1.PipelineTaskTemplate, error)
}

// New builder method for pipelinetasktemplate.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("PipelineTaskTemplate related APIs").ApiVersion("v1").Path("/api/v1/pipelinetasktemplate")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "pipelinetasktemplate")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List PipelineTaskTemplate instance`).
					Param(ws.PathParameter("namespace", "namespace of the PipelineTaskTemplate")).
					To(handler.ListPipelineTaskTemplate).
					Writes(v1alpha1.PipelineTaskTemplateList{}).
					Returns(http.StatusOK, "OK", PipelineTaskTemplateList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve PipelineTaskTemplate instance").
				Param(ws.PathParameter("name", "name of the PipelineTaskTemplate")).
				Param(ws.PathParameter("namespace", "namespace of the PipelineTaskTemplate")).
				To(handler.RetrievePipelineTaskTemplate).
				Returns(http.StatusOK, "Retrieve PipelineTaskTemplate instance", v1alpha1.PipelineTaskTemplate{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListPipelineTaskTemplate list pipelinetasktemplate instances
func (h Handler) ListPipelineTaskTemplate(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListPipelineTaskTemplate(req.Request.Context(), client, req.PathParameter("namespace"), query)
	h.WriteResponse(list, err, req, res)
}

//RetrievePipelineTaskTemplate Retrieve PipelineTaskTemplate instance
func (h Handler) RetrievePipelineTaskTemplate(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrievePipelineTaskTemplate(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
