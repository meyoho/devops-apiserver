package pipelinetasktemplate_test

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelinetasktemplate"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("pipelinetasktemplate", func() {

	var (
		processor                pipelinetasktemplate.Processor
		ctx                      context.Context
		client                   *fake.Clientset
		query                    *dataselect.Query
		namespace                string
		err                      error
		pipelinetasktemplatelist *pipelinetasktemplate.PipelineTaskTemplateList
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 1,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		namespace = "default"
		client = fake.NewSimpleClientset()
		processor = pipelinetasktemplate.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("get all pipelinetempalte", func() {
		client = fake.NewSimpleClientset(
			GetPipelineTasktemplateList("testtasktemplate", namespace),
		)
		pipelinetasktemplatelist, err = processor.ListPipelineTaskTemplate(ctx, client, namespace, query)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(pipelinetasktemplatelist).ToNot(BeNil(), "should return a list")
		Expect(pipelinetasktemplatelist.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(pipelinetasktemplatelist.Items).To(HaveLen(1), "should have one items")
		Expect(pipelinetasktemplatelist.Items[0].Name).To(Equal("testtasktemplate"))

	})

})

func GetPipelineTasktemplateList(name, namespace string) *v1alpha1.PipelineTaskTemplateList {
	pipelinetasktempaltelist := &v1alpha1.PipelineTaskTemplateList{

		Items: []v1alpha1.PipelineTaskTemplate{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha1.PipelineTaskTemplateSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name + "1",
					Namespace: namespace,
				},
				Spec: v1alpha1.PipelineTaskTemplateSpec{},
			},
		},
	}
	return pipelinetasktempaltelist
}
