package pipelinetasktemplate

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListPipelineTaskTemplate list pipelinetasktemplate instances
func (p processor) ListPipelineTaskTemplate(ctx context.Context, client versioned.Interface, namespace string, query *dataselect.Query) (data *PipelineTaskTemplateList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list pipelinetasktemplate end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	pipelinetasktemplatelist, err := client.DevopsV1alpha1().PipelineTaskTemplates(namespace).List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &PipelineTaskTemplateList{
		Items:    make([]v1alpha1.PipelineTaskTemplate, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}
	itemCells := toCells(pipelinetasktemplatelist.Items)

	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := fromCells(itemCells)

	data.Items = ConvertToPipelineTaskTemplateSlice(result)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// RetrievePipelineTaskTemplate retrieve pipelinetasktemplate instance
func (p processor) RetrievePipelineTaskTemplate(ctx context.Context, client versioned.Interface, namespace, name string) (data *v1alpha1.PipelineTaskTemplate, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipelinetasktemplate end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().PipelineTaskTemplates(namespace).Get(name, jenkins.GetOptionsInCache)
	data.Kind = devops.TypePipelineTaskTemplate
	return
}

// ConvertToPipelineTaskTemplateSlice
// without having to do it manually
func ConvertToPipelineTaskTemplateSlice(filtered []v1alpha1.PipelineTaskTemplate) (items []v1alpha1.PipelineTaskTemplate) {
	for _, item := range filtered {
		item.Kind = common.ResourceKindPipelineTaskTemplate
		items = append(items, item)
	}
	return
}
