package toolchain

import (
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	"k8s.io/client-go/kubernetes"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

func (p processor) GetToolChainList(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, query *dataselect.Query, toolType string) (toolChainList *ToolChainList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list pipeline end", log.Any("query", query), log.Any("list", toolChainList), log.Err(err))
	}()

	return GetToolChainList(ctx, client, k8sclient, query, toolType)

}
