package toolchain

import (
	"context"
	"fmt"
	"log"
	"reflect"
	"sync"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codequalitybinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codequalitytool"
	"alauda.io/devops-apiserver/pkg/restapi/handler/coderepobinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codereposervice"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/imageregistry"
	"alauda.io/devops-apiserver/pkg/restapi/handler/imageregistrybinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkinsbinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/projectmanagement"
	"alauda.io/devops-apiserver/pkg/restapi/handler/projectmanagementbinding"
	"alauda.io/devops-apiserver/pkg/toolchain"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	bitbucketlog "bitbucket.org/mathildetech/log"
	"k8s.io/client-go/kubernetes"
)

// ToolChainList contains a list of tool.
type ToolChainList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of ToolChain.
	Items []interface{} `json:"items"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// ToolChainBindingList contains a list of binding in toolchain.
type ToolChainBindingList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of ToolChain.
	Items []interface{} `json:"items"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// GetToolChainBindingList get binding list in toolchain
func GetToolChainBindingList(
	ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, dsQuery *dataselect.Query,
	namespaces *common.NamespaceQuery, toolType string,
) (toolChainBindingList *ToolChainBindingList, err error) {
	log.Println("Getting list of binding")
	toolChainBindingList = &ToolChainBindingList{}

	toolChainList, err := GetToolChainList(ctx, client, k8sclient, dsQuery, toolType)
	if err != nil || toolChainList == nil || len(toolChainList.Items) == 0 {
		return
	}

	var (
		jenkinsBindingList           *jenkinsbinding.JenkinsBindingList
		codeRepoBindingList          *coderepobinding.CodeRepoBindingList
		imageRegistryBindingList     *imageregistrybinding.ImageRegistryBindingList
		codeQualityBindingList       *codequalitybinding.CodeQualityBindingList
		projectManagementBindingList *projectmanagementbinding.ProjectManagementBindingList
		jenkinsBindingCritical, codeRepoBindingCritical, codeQualityBindingCritical,
		imageRegistryBindingCritical, projectManagementBindingCritical error
		nonCriticals, jenkinsBindingNonCritical, codeRepoBindingNonCritical, codeQualityBindingNonCritical,
		imageRegistryBindingNonCritical, projectManagementBindingNonCritical []error
		wg sync.WaitGroup
	)
	wg.Add(5)

	go func() {
		jenkinsBindingList, jenkinsBindingCritical = jenkinsbinding.NewProcessor().ListJenkinsBinding(ctx, client, dsQuery, namespaces)
		jenkinsBindingNonCritical, jenkinsBindingCritical = errors.HandleError(jenkinsBindingCritical)
		if jenkinsBindingCritical != nil {
			log.Println("error while listing jenkinsbindings", jenkinsBindingCritical)
		}
		wg.Done()
	}()

	go func() {
		codeRepoBindingList, codeRepoBindingCritical = coderepobinding.NewProcessor().ListCodeRepoBinding(ctx, client, dsQuery, namespaces)
		codeRepoBindingNonCritical, codeRepoBindingCritical = errors.HandleError(codeRepoBindingCritical)
		if codeRepoBindingCritical != nil {
			log.Println("error while listing coderepobindings", codeRepoBindingCritical)
		}
		wg.Done()
	}()

	go func() {
		imageRegistryBindingList, imageRegistryBindingCritical = imageregistrybinding.NewProcessor().ListImageRegistryBinding(ctx, client, dsQuery, namespaces)
		imageRegistryBindingNonCritical, imageRegistryBindingCritical = errors.HandleError(imageRegistryBindingCritical)
		if imageRegistryBindingCritical != nil {
			log.Println("error while listing imageregistrybindings", imageRegistryBindingCritical)
		}
		wg.Done()
	}()

	go func() {
		codeQualityBindingList, codeQualityBindingCritical = codequalitybinding.NewProcessor().ListCodeQualityBinding(ctx, client, dsQuery, namespaces)
		codeQualityBindingNonCritical, codeQualityBindingCritical = errors.HandleError(codeQualityBindingCritical)
		if codeQualityBindingCritical != nil {
			log.Println("error while listing codequalitybindings", codeQualityBindingCritical)

		}
		wg.Done()
	}()

	go func() {
		projectManagementBindingList, projectManagementBindingCritical = projectmanagementbinding.NewProcessor().ListProjectManagementBinding(ctx, client, dsQuery, namespaces)
		projectManagementBindingNonCritical, projectManagementBindingCritical = errors.HandleError(projectManagementBindingCritical)
		if projectManagementBindingCritical != nil {
			log.Println("error while listing projectmanagementbindings", projectManagementBindingCritical)

		}
		wg.Done()
	}()

	wg.Wait()

	if jenkinsBindingCritical != nil {
		return nil, jenkinsBindingCritical
	}
	if codeRepoBindingCritical != nil {
		return nil, codeRepoBindingCritical
	}
	if imageRegistryBindingCritical != nil {
		return nil, imageRegistryBindingCritical
	}
	if codeQualityBindingCritical != nil {
		return nil, codeRepoBindingCritical
	}

	if jenkinsBindingNonCritical != nil {
		nonCriticals = append(nonCriticals, jenkinsBindingNonCritical...)
	}
	if codeRepoBindingNonCritical != nil {
		nonCriticals = append(nonCriticals, codeRepoBindingNonCritical...)
	}
	if imageRegistryBindingNonCritical != nil {
		nonCriticals = append(nonCriticals, imageRegistryBindingNonCritical...)
	}
	if codeQualityBindingNonCritical != nil {
		nonCriticals = append(nonCriticals, codeQualityBindingNonCritical...)
	}

	if projectManagementBindingNonCritical != nil {
		nonCriticals = append(nonCriticals, projectManagementBindingNonCritical...)
	}

	for _, toolChain := range toolChainList.Items {
		if toolChain == nil {
			continue
		}

		switch value := toolChain.(type) {
		case v1alpha1.Jenkins:
			if jenkinsBindingList == nil || jenkinsBindingList.Items == nil {
				continue
			}
			for _, item := range jenkinsBindingList.Items {
				item.Kind = v1alpha1.ResourceKindJenkinsBinding
				if item.Spec.Jenkins.Name == value.ObjectMeta.Name {
					toolChainBindingList.Items = append(toolChainBindingList.Items, item)
				}
			}

		case v1alpha1.CodeRepoService:
			if codeRepoBindingList == nil || codeRepoBindingList.Items == nil {
				continue
			}
			for _, item := range codeRepoBindingList.Items {
				item.Kind = v1alpha1.ResourceKindCodeRepoBinding
				if item.Spec.CodeRepoService.Name == value.ObjectMeta.Name {
					toolChainBindingList.Items = append(toolChainBindingList.Items, item)
				}
			}

		case v1alpha1.ImageRegistry:
			if imageRegistryBindingList == nil || imageRegistryBindingList.Items == nil {
				continue
			}
			for _, item := range imageRegistryBindingList.Items {
				item.Kind = v1alpha1.ResourceKindImageRegistryBinding
				if item.Spec.ImageRegistry.Name == value.ObjectMeta.Name {
					toolChainBindingList.Items = append(toolChainBindingList.Items, item)
				}
			}
		case v1alpha1.CodeQualityTool:
			if codeQualityBindingList == nil || codeQualityBindingList.Items == nil {
				continue
			}
			for _, item := range codeQualityBindingList.Items {
				item.Kind = v1alpha1.ResourceKindCodeQualityBinding
				if item.Spec.CodeQualityTool.Name == value.ObjectMeta.Name {
					toolChainBindingList.Items = append(toolChainBindingList.Items, item)
				}
			}
		case v1alpha1.ProjectManagement:
			if projectManagementBindingList == nil || projectManagementBindingList.Items == nil {
				continue
			}
			for _, item := range projectManagementBindingList.Items {
				item.Kind = v1alpha1.ResourceKindProjectManagementBinding
				if item.Spec.ProjectManagement.Name == value.ObjectMeta.Name {
					toolChainBindingList.Items = append(toolChainBindingList.Items, item)
				}
			}

		default:
			log.Println(fmt.Sprintf("value %v, type %s is not defined in this method.", value, reflect.TypeOf(value)))
		}
	}

	return toChainBindingList(toolChainBindingList.Items, nonCriticals), nil
}

func toChainBindingList(items []interface{}, nonCriticalErrors []error) *ToolChainBindingList {
	bindingList := &ToolChainBindingList{
		Items:    items,
		ListMeta: api.ListMeta{TotalItems: len(items)},
	}
	bindingList.Errors = nonCriticalErrors
	return bindingList
}

// GetToolChainList returns a list of toolchain
func GetToolChainList(ctx context.Context,
	client versioned.Interface, k8sclient kubernetes.Interface, query *dataselect.Query,
	toolType string,
) (toolChainList *ToolChainList, err error) {

	methodName := "GetToolChainList"

	toolChainList = &ToolChainList{}

	elements, err1 := GetCategoryFromToolCategory(client)

	if err1 != nil {
		err = err1
		return
	}

	if len(elements) == 0 {
		bitbucketlog.Errorf("%s No element defined in configmap", methodName)
		return
	}

	var toolTypes []string
	if toolType != "" {
		toolTypes = append(toolTypes, toolType)
	} else {
		for _, element := range elements {
			toolTypes = append(toolTypes, element.Name)
		}
	}

	items, err := getToolItemsByToolTypes(ctx, client, k8sclient, query, elements, toolTypes)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	return toChainList(items, nonCriticalErrors), nil
}

func getToolItemsByToolTypes(ctx context.Context,
	client versioned.Interface, k8sclient kubernetes.Interface, dsQuery *dataselect.Query,
	elements []*toolchain.Category, toolTypes []string,
) (items []interface{}, err error) {
	log.Println("Get tool items from ", toolTypes)

	var (
		codeRepoServiceList   *codereposervice.CodeRepoServiceList
		jenkinsList           *jenkins.JenkinsList
		imageRegistryList     *imageregistry.ImageRegistryList
		projectManagementList *projectmanagement.ProjectManagementList
		//testToolList          *testtool.TestToolList
		codeQualityToolList *codequalitytool.CodeQualityToolList
		err1                error
	)

	items = []interface{}{}
	for i, toolType := range toolTypes {
		log.Println(fmt.Sprintf("%d. Get tool items from %s", i, toolType))

		switch toolType {
		case v1alpha1.ToolChainCodeRepositoryName:
			codeRepoServiceList, err1 = codereposervice.NewProcessor().ListCodeRepoService(ctx, client, dsQuery)
			if err1 != nil {
				err = err1
				return
			}
		case v1alpha1.ToolChainContinuousIntegrationName:
			jenkinsList, err1 = jenkins.NewProcessor().ListJenkins(ctx, client, dsQuery)
			if err1 != nil {
				err = err1
				return
			}
		case v1alpha1.ToolChainArtifactRepositoryName:
			imageRegistryList, err1 = imageregistry.NewProcessor().ListImageRegistry(ctx, client, dsQuery)
			if err1 != nil {
				err = err1
				return
			}
		case v1alpha1.ToolChainProjectManagementName:
			projectManagementList, err1 = projectmanagement.NewProcessor().ListProjectManagement(ctx, client, dsQuery)
			if err1 != nil {
				err = err1
				return
			}
		//case v1alpha1.ToolChainTestToolName:
		//	testToolList, err1 = testtool.GetTestToolList(client, dsQuery)
		//	if err1 != nil {
		//		err = err1
		//		return
		//	}
		case v1alpha1.ToolChainCodeQualityToolName:
			codeQualityToolList, err1 = codequalitytool.NewProcessor().ListCodeQualityTool(ctx, client, dsQuery)
			if err1 != nil {
				err = err1
				return
			}
		}

		toolItems, err1 := getToolItemsByToolType(elements, toolType, codeRepoServiceList, jenkinsList, imageRegistryList,
			projectManagementList, codeQualityToolList)
		if err1 != nil {
			err = err1
			return
		}

		if len(toolItems) > 0 {
			items = append(items, toolItems...)
		}
	}

	return
}

func getToolItemsByToolType(
	elements []*toolchain.Category, toolType string,
	codeRepoServiceList *codereposervice.CodeRepoServiceList, jenkinsList *jenkins.JenkinsList,
	imageRegistryList *imageregistry.ImageRegistryList,
	projectManagementList *projectmanagement.ProjectManagementList,
	//testToolList *testtool.TestToolList,
	codeQualityToolList *codequalitytool.CodeQualityToolList,
) (items []interface{}, err error) {

	var (
		toolElement *toolchain.Category
	)

	for _, element := range elements {
		if element.Name == toolType {
			toolElement = element
		}
	}

	if toolElement == nil {
		return
	}

	if len(toolElement.Items) == 0 {
		return
	}

	toolchainMatcher := make(map[string]*toolchain.Item)
	for _, item := range toolElement.Items {
		if item != nil && item.Enabled {
			toolchainMatcher[item.Type] = item
		}
	}
	switch toolType {
	case devops.ToolChainProjectManagementName:
		if projectManagementList == nil {
			return
		}
		for _, value := range projectManagementList.Items {
			if _, ok := toolchainMatcher[value.Spec.Type.String()]; ok {
				if value.ObjectMeta.Annotations == nil {
					value.ObjectMeta.Annotations = make(map[string]string, 1)
				}
				items = append(items, value)
			}
		}
	case devops.ToolChainCodeRepositoryName:
		if codeRepoServiceList == nil {
			return
		}
		for _, value := range codeRepoServiceList.Items {
			if _, ok := toolchainMatcher[value.Spec.Type.String()]; ok {
				items = append(items, value)
			}
		}
	case devops.ToolChainCodeQualityToolName:
		if codeQualityToolList == nil {
			return
		}
		for _, value := range codeQualityToolList.Items {
			if _, ok := toolchainMatcher[value.Spec.Type.String()]; ok {
				if value.ObjectMeta.Annotations == nil {
					value.ObjectMeta.Annotations = make(map[string]string, 1)
				}
				items = append(items, value)
			}
		}
	case devops.ToolChainArtifactRepositoryName:
		if imageRegistryList == nil {
			return
		}
		for _, value := range imageRegistryList.Items {
			if _, ok := toolchainMatcher[value.Spec.Type.String()]; ok {
				items = append(items, value)
			}
		}
	case devops.ToolChainContinuousIntegrationName:
		if jenkinsList == nil {
			return
		}
		for _, value := range jenkinsList.Jenkins {
			items = append(items, value)
		}
	}
	return
}

func toChainList(items []interface{}, nonCriticalErrors []error) *ToolChainList {
	toolChainList := &ToolChainList{
		Items:    items,
		ListMeta: api.ListMeta{TotalItems: len(items)},
	}
	toolChainList.Errors = nonCriticalErrors
	return toolChainList
}

func GetCategoryFromToolCategory(client versioned.Interface) (elements []*toolchain.Category, err error) {
	methodName := "getCategory"

	settingResp, err := client.DevopsV1alpha1().ToolCategories().Setting()
	if err != nil {
		bitbucketlog.Errorf("%s toolChainStr err is %#v", methodName, err)
		return
	}

	bitbucketlog.Infof("%s toolChainStr is %#v", methodName, settingResp)

	for _, tc := range settingResp.ToolChains {
		items := make([]*toolchain.Item, 0)
		for _, tcItem := range tc.Items {

			item := toolchain.Item{
				DisplayName:          toolchain.DisplayName{English: tcItem.DisplayName.En, Chinese: tcItem.DisplayName.Zh},
				Name:                 tcItem.Name,
				Kind:                 tcItem.Kind,
				FeatureGate:          tcItem.FeatureGate,
				Type:                 tcItem.Type,
				Public:               tcItem.Public,
				Enterprise:           tcItem.Enterprise,
				Enabled:              tcItem.Enabled,
				APIPath:              tcItem.ApiPath,
				SupportedSecretTypes: []toolchain.ItemSecretType{},
				RoleSyncEnabled:      tcItem.RoleSyncEnabled,
			}
			items = append(items, &item)
		}
		newCategory := toolchain.Category{
			Name: tc.Name,
			DisplayName: toolchain.DisplayName{
				English: tc.DisplayName.En,
				Chinese: tc.DisplayName.Zh,
			},
			Enabled: tc.Enabled,
			APIPath: tc.ApiPath,
			Items:   items,
		}
		elements = append(elements, &newCategory)
	}

	bitbucketlog.Infof("%s elements is %#v", methodName, elements)

	return elements, err
}
