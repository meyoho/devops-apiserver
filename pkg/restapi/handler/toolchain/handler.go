package toolchain

import (
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/kubernetes"
	"net/http"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for ProjectManagement
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	GetToolChainList(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, query *dataselect.Query, tooltype string) (*ToolChainList, error)
}

// New builder method for projectmanagement.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ToolChain related APIs").ApiVersion("v1").Path("/api/v1/toolchain")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "toolchain")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Filter(clientFilter.InsecureFilter).
					Filter(clientFilter.AppClientFilter).
					Doc(`List ToolChain instance`).
					To(handler.GetToolChainList).
					Writes(ToolChainList{}).
					Returns(http.StatusOK, "OK", ToolChainList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/bindings").
				Filter(clientFilter.DevOpsClientFilter).
				Filter(clientFilter.InsecureFilter).
				Doc("Get toolchain binding ").
				To(handler.GetToolChainBindings).
				Returns(http.StatusOK, "Get ToolChain Binding Instance", ToolChainBindingList{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("bindings/{namespace}").
				Filter(clientFilter.DevOpsClientFilter).
				Filter(clientFilter.InsecureFilter).
				Doc("Get toolchain binding").
				To(handler.GetToolChainBindings).
				Returns(http.StatusOK, "Get ToolChain Binding Instance", ToolChainBindingList{}),
		),
	)
	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListProjectManagement list projectmanagement instances
func (h Handler) GetToolChainList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())

	toolType := req.QueryParameter("tool_type")
	k8sclient := abcontext.Client(req.Request.Context())
	list, err := h.Processor.GetToolChainList(req.Request.Context(), client, k8sclient, query, toolType)
	h.WriteResponse(list, err, req, res)
}

//DeleteProjectManagement Delete ProjectManagement instance
func (h Handler) GetToolChainBindings(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	k8sclient := abcontext.Client(req.Request.Context())

	toolType := req.QueryParameter("tool_type")
	namespace := req.PathParameter("namespace")
	namespaceQuery := common.NewSameNamespaceQuery(namespace)
	dataSelect := common.ParseDataSelectPathParameter(req)
	result, err := GetToolChainBindingList(req.Request.Context(), client, k8sclient, dataSelect, namespaceQuery, toolType)
	h.WriteResponse(result, err, req, res)

}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
