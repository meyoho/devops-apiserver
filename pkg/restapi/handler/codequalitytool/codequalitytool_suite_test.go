package codequalitytool_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestCodequalitytool(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("codequalitytool.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/codequalitytool", []Reporter{junitReporter})
}
