package codequalitytool

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for CodeQualityTool
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListCodeQualityTool(ctx context.Context, client versioned.Interface, query *dataselect.Query) (*CodeQualityToolList, error)
	DeleteCodeQualityTool(ctx context.Context, client versioned.Interface, name string) error
	RetrieveCodeQualityTool(ctx context.Context, client versioned.Interface, name string) (*v1alpha1.CodeQualityTool, error)
	CreateCodeQualityTool(ctx context.Context, client versioned.Interface, CodeQualityTool *v1alpha1.CodeQualityTool) (*v1alpha1.CodeQualityTool, error)
	UpdateCodeQualityTool(ctx context.Context, client versioned.Interface, CodeQualityTool *v1alpha1.CodeQualityTool, name string) (*v1alpha1.CodeQualityTool, error)
}

// New builder method for codequalitytool.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("CodeQualityTool related APIs").ApiVersion("v1").Path("/api/v1/codequalitytool")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "codequalitytool")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List CodeQualityTool instance`).
					To(handler.ListCodeQualityTool).
					Writes(v1alpha1.CodeQualityToolList{}).
					Returns(http.StatusOK, "OK", CodeQualityToolList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create CodeQualityTool instance").
				To(handler.CreateCodeQualityTool).
				Returns(http.StatusOK, "Create CodeQualityTool Instance", v1alpha1.CodeQualityTool{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete CodeQualityTool instance").
				Param(ws.PathParameter("name", "name of the CodeQualityTool")).
				To(handler.DeleteCodeQualityTool).
				Returns(http.StatusOK, "Delete CodeQualityTool Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve CodeQualityTool instance").
				Param(ws.PathParameter("name", "name of the CodeQualityTool")).
				To(handler.RetrieveCodeQualityTool).
				Returns(http.StatusOK, "Retrieve CodeQualityTool instance", v1alpha1.CodeQualityTool{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update CodeQualityTool instance").
				Param(restful.PathParameter("name", "CodeQualityTool name to filter scope")).
				To(handler.UpdateCodeQualityTool).
				Returns(http.StatusOK, "Update CodeQualityTool instance", v1alpha1.CodeQualityTool{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListCodeQualityTool list codequalitytool instances
func (h Handler) ListCodeQualityTool(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListCodeQualityTool(req.Request.Context(), client, query)
	h.WriteResponse(list, err, req, res)
}

//DeleteCodeQualityTool Delete CodeQualityTool instance
func (h Handler) DeleteCodeQualityTool(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteCodeQualityTool(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//RetrieveCodeQualityTool Retrieve CodeQualityTool instance
func (h Handler) RetrieveCodeQualityTool(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrieveCodeQualityTool(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//CreateCodeQualityTool create CodeQualityTool instance
func (h Handler) CreateCodeQualityTool(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newCodeQualityTool := new(v1alpha1.CodeQualityTool)

	if err := req.ReadEntity(newCodeQualityTool); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.CreateCodeQualityTool(req.Request.Context(), client, newCodeQualityTool)
	h.WriteResponse(data, err, req, res)
}

//UpdateJenkins update CodeQualityTool instance
func (h Handler) UpdateCodeQualityTool(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newCodeQualityTool := new(v1alpha1.CodeQualityTool)

	if err := req.ReadEntity(newCodeQualityTool); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.UpdateCodeQualityTool(req.Request.Context(), client, newCodeQualityTool, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
