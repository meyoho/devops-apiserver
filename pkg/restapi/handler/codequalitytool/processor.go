package codequalitytool

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListCodeQualityTool list codequalitytool instances
func (p processor) ListCodeQualityTool(ctx context.Context, client versioned.Interface, query *dataselect.Query) (data *CodeQualityToolList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list codequalitytool end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	codequalitytoollist, err := client.DevopsV1alpha1().CodeQualityTools().List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &CodeQualityToolList{
		Items:    make([]v1alpha1.CodeQualityTool, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(codequalitytoollist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToCodeQualityToolSlice(result)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeleteCodeQualityTool delete codequalitytool instance
func (p processor) DeleteCodeQualityTool(ctx context.Context, client versioned.Interface, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete CodeQualityTool end", log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().CodeQualityTools().Delete(name, &metav1.DeleteOptions{})
	return
}

// RetrieveCodeQualityTool retrieve codequalitytool instance
func (p processor) RetrieveCodeQualityTool(ctx context.Context, client versioned.Interface, name string) (data *v1alpha1.CodeQualityTool, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get codequalitytool end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().CodeQualityTools().Get(name, jenkins.GetOptionsInCache)
	return
}

//CreateCodeQualityTool create codequalitytool instance
func (p processor) CreateCodeQualityTool(ctx context.Context, client versioned.Interface, codequalitytool *v1alpha1.CodeQualityTool) (data *v1alpha1.CodeQualityTool, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create codequalitytool end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().CodeQualityTools().Create(codequalitytool)
	return
}

//UpdateCodeQualityTool update a codequalitytool instance
func (p processor) UpdateCodeQualityTool(ctx context.Context, client versioned.Interface, codequalitytool *v1alpha1.CodeQualityTool, name string) (data *v1alpha1.CodeQualityTool, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update CodeQualityTool end", log.Err(err), log.String("name", name))
	}()
	oldCodeQualityTool := new(v1alpha1.CodeQualityTool)
	oldCodeQualityTool, err = client.DevopsV1alpha1().CodeQualityTools().Get(name, jenkins.GetOptionsInCache)

	if err != nil {
		return
	}

	oldCodeQualityTool.SetAnnotations(codequalitytool.GetAnnotations())
	oldCodeQualityTool.Spec = codequalitytool.Spec

	data, err = client.DevopsV1alpha1().CodeQualityTools().Update(oldCodeQualityTool)
	return
}

// ConvertToCodeQualityToolSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToCodeQualityToolSlice(filtered []metav1.Object) (items []v1alpha1.CodeQualityTool) {
	items = make([]v1alpha1.CodeQualityTool, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.CodeQualityTool); ok {
			cm.Kind = v1alpha1.ResourceKindCodeQualityTool
			items = append(items, *cm)
		}
	}
	return
}
