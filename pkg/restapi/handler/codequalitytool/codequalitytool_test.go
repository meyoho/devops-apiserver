package codequalitytool_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codequalitytool"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Processor.Codequalitytool", func() {
	var (
		processor codequalitytool.Processor
		ctx       context.Context
		client    *fake.Clientset
		query     *dataselect.Query

		list *codequalitytool.CodeQualityToolList
		err  error
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 10,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		client = fake.NewSimpleClientset()
		processor = codequalitytool.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("should return codequalitytool", func() {
		client = fake.NewSimpleClientset(
			GetCodeQualityList("codequalitytool"),
		)
		list, err = processor.ListCodeQualityTool(ctx, client, query)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Items).To(HaveLen(2), "should not have any items")
	})

})

func GetJenkinsBinding(name, namespace string) *v1alpha1.JenkinsBinding {
	jenkinsbindinginstance := &v1alpha1.JenkinsBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: v1alpha1.JenkinsBindingSpec{
			Jenkins: v1alpha1.JenkinsInstance{
				Name: "jenkinsname",
			},
		},
	}
	return jenkinsbindinginstance
}

func GetCodeQualityList(name string) *v1alpha1.CodeQualityToolList {
	codequalitytoollist := &v1alpha1.CodeQualityToolList{
		TypeMeta: metav1.TypeMeta{
			Kind: "pipeline",
		},

		Items: []v1alpha1.CodeQualityTool{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
				},
				Spec: v1alpha1.CodeQualityToolSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name + "1",
				},
				Spec: v1alpha1.CodeQualityToolSpec{},
			},
		},
	}
	return codequalitytoollist
}
