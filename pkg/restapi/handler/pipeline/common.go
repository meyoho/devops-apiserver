package pipeline

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"strings"
	"time"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
)

// The code below allows to perform complex data section on []api.Namespace

type PipelineCell struct {
	pipeline *devopsv1alpha1.Pipeline
	devops.AnnotationProvider
}

func (self PipelineCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case common.NameProperty:
		return dataselect.StdComparableString(self.pipeline.ObjectMeta.Name)
	case common.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.pipeline.ObjectMeta.CreationTimestamp.Time)
	case common.StartedAtProperty:
		if self.pipeline.Status.StartedAt != nil {
			return dataselect.StdComparableTime(self.pipeline.Status.StartedAt.Time)
		}
		return dataselect.StdComparableTime(time.Time{})
	case common.NamespaceProperty:
		return dataselect.StdComparableString(self.pipeline.ObjectMeta.Namespace)
	case common.DisplayNameProperty:
		if len(self.pipeline.ObjectMeta.Annotations) > 0 {
			if self.pipeline.ObjectMeta.Annotations[self.AnnotationsKeyDisplayName()] != "" {
				return dataselect.StdLowerComparableString(self.pipeline.ObjectMeta.Annotations[self.AnnotationsKeyDisplayName()])
			}
		}
		return dataselect.StdLowerComparableString(self.pipeline.ObjectMeta.Name)
	case common.LabelsProperty:
		if len(self.pipeline.ObjectMeta.Labels) > 0 {
			values := []string{}
			for k, v := range self.pipeline.ObjectMeta.Labels {
				values = append(values, k+":"+v)
			}
			return dataselect.StdComparableLabel(strings.Join(values, ","))
		}
	case common.MultiBranchCategoryProperty:
		if comparableVal, ok := dataselect.MapStdComparable(self.pipeline.ObjectMeta.Annotations, self.AnnotationsKeyMultiBranchCategory()); ok {
			return comparableVal
		}
	case common.MultiBranchNameProperty:
		if comparableVal, ok := dataselect.MapStdComparable(self.pipeline.ObjectMeta.Annotations, self.AnnotationsKeyMultiBranchName()); ok {
			return comparableVal
		}
	case common.PipelineStatusProperty:
		if self.pipeline.Status.Jenkins != nil {
			return dataselect.StdComparableString(self.pipeline.Status.Jenkins.Status)
		}
	}
	// if name is not supported then just return a constant dummy value, sort will have no effect.
	return nil
}

func toCells(std []devopsv1alpha1.Pipeline, provider devops.AnnotationProvider) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = PipelineCell{&std[i], provider}
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []devopsv1alpha1.Pipeline {
	std := make([]devopsv1alpha1.Pipeline, len(cells))
	for i := range std {
		std[i] = *cells[i].(PipelineCell).pipeline.DeepCopy()
	}
	return std
}
