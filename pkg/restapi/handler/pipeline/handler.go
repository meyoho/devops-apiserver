package pipeline

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"alauda.io/devops-apiserver/pkg/registry/generic/download"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog"
	controllerruntimeclient "sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for Pipeline
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListPipeline(ctx context.Context, cacheClient controllerruntimeclient.Client, query *dataselect.Query, namespace *common.NamespaceQuery) (*PipelineList, error)
	DeletePipeline(ctx context.Context, client versioned.Interface, namespace, name string) error
	RetrievePipeline(ctx context.Context, cacheClient controllerruntimeclient.Client, client versioned.Interface, namespace, name string) (*v1alpha1.Pipeline, error)
	RetryPipelineDetail(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.Pipeline, error)
	AbortPipeline(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.Pipeline, error)
	GetPipelineLogs(ctx context.Context, client versioned.Interface, namespace, name string, start, stage, step int) (*LogDetails, error)
	GetPipelineTasks(ctx context.Context, client versioned.Interface, namespace, name string, stage int) (*TaskDetails, error)
	InputHandle(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, opts *InputOptions) (resp *InputResponse, err error)
	HandlePipelineTestReport(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, opt *PipelineTestReportsOptions) (report PipelineTestReports, err error)
	GetPipelineArtifacts(ctx context.Context, client versioned.Interface, namespace, name string) (artifactlist *v1alpha1.PipelineArtifactList, err error)
	DownloadArtifact(ctx context.Context, client versioned.Interface, opt *PipelineDownloadOptions) (data io.ReadCloser, contenttype string, err error)
	GetPipelineView(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.PipelineViewResult, error)
}

// New builder method for pipeline.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	accessFilter := decorator.AccessReviewFilter(srv)
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("Pipeline related APIs").ApiVersion("v1").Path("/api/v1/pipeline")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "pipeline")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(accessFilter.NewAccessReviewFilter(&v1alpha1.Pipeline{}, "list")).
					Filter(clientFilter.CacheClientFilter).
					Doc(`List Pipeline instance`).
					Param(ws.PathParameter("namespace", "namespace of the pipline")).
					To(handler.ListPipeline).
					Writes(v1alpha1.JenkinsList{}).
					Returns(http.StatusOK, "OK", PipelineList{}),
			),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete Pipeline instance").
				Param(ws.PathParameter("name", "name of the Pipeline")).
				Param(ws.PathParameter("namespace", "namespace of the Pipeline")).
				To(handler.DeletePipeline).
				Returns(http.StatusOK, "Delete Pipeline Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(accessFilter.NewAccessReviewFilter(&v1alpha1.Pipeline{}, "get")).
				Filter(clientFilter.CacheClientFilter).
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve Pipeline instance").
				Param(ws.PathParameter("namespace", "namespace of the Pipeline")).
				Param(ws.PathParameter("name", "name of the Pipeline")).
				Param(ws.QueryParameter("withFreshStages", "Whether to retrieve newest stages from Jenkins")).
				To(handler.RetrievePipeline).
				Returns(http.StatusOK, "Retrieve Pipeline instance", v1alpha1.Pipeline{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}/{name}/retry").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("retries a pipeline").
				Param(ws.PathParameter("namespace", "namespace of the Pipeline")).
				Param(ws.PathParameter("name", "name of the Pipeline")).
				To(handler.RetryPipelineDetail).
				Returns(http.StatusOK, "Retrieve Pipeline instance", v1alpha1.Pipeline{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}/abort").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("aborts a pipeline").
				Param(ws.PathParameter("namespace", "namespace of the Pipeline")).
				Param(ws.PathParameter("name", "name of the Pipeline")).
				To(handler.AbortPipeline).
				Returns(http.StatusOK, "abort Pipeline instance", v1alpha1.Pipeline{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/logs").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get logs for pipeline").
				Param(ws.PathParameter("namespace", "Namespace to use")).
				Param(ws.PathParameter("name", "Pipeline name to filter scope")).
				Param(ws.QueryParameter("start", "Start offset to fetch logs")).
				Param(ws.QueryParameter("stage", "Stage to fetch logs from")).
				Param(ws.QueryParameter("step", "Step to fetch logs from. Can be combined with stage")).
				To(handler.GetPipelineLogs).
				Returns(http.StatusOK, "get Pipeline log", v1alpha1.PipelineLog{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/tasks").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update Jenkins instance").
				Param(ws.PathParameter("namespace", "Namespace to use")).
				Param(ws.PathParameter("name", "Pipeline name to filter scope")).
				Param(ws.QueryParameter("stage", "Stage to fetch steps from. If not provided will return all stages")).
				To(handler.GetPipelineTasks).
				Returns(http.StatusOK, "Update Jenkins instance", v1alpha1.PipelineTask{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}/{name}/inputs").
				Filter(clientFilter.DevOpsClientFilter).
				Filter(clientFilter.SecureFilter).
				Doc("pipeline Input").
				Param(ws.PathParameter("namespace", "Namespace to use")).
				Param(ws.PathParameter("name", "Pipeline name to filter scope")).
				To(handler.PipelineInputs).
				Returns(http.StatusOK, "post to pipeline inputs", InputResponse{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/testreports").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get pipeline testreposts").
				Param(ws.PathParameter("namespace", "Namespace to use")).
				Param(ws.PathParameter("name", "Pipeline name to filter scope")).
				Param(ws.QueryParameter("start", "Start offset to fetch test report items")).
				Param(ws.QueryParameter("limit", "Limit of number to fetch test report items")).
				To(handler.GetPipelineTestReports).
				Returns(http.StatusOK, "get pipeline testreports", PipelineTestReports{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/artifacts").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get pipeline artifacts").
				Param(ws.PathParameter("namespace", "Namespace to use")).
				Param(ws.PathParameter("name", "Pipeline name to filter scope")).
				To(handler.GetPipelineArtifacts).
				Returns(http.StatusOK, "get pipeline artifact list", v1alpha1.PipelineArtifactList{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/download/{filename}").
				Filter(clientFilter.DevOpsClientFilter).
				Filter(clientFilter.SecureFilter).
				Doc("download pipeline artifacts").
				Param(ws.PathParameter("namespace", "Namespace to use")).
				Param(ws.PathParameter("name", "Pipeline name to filter scope")).
				Param(ws.PathParameter("filename", "download file name")).
				To(handler.DownloadArtifact).
				Returns(http.StatusOK, "download pipeline artifact with filename", download.ArtifactFile{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/download").
				Filter(clientFilter.DevOpsClientFilter).
				Filter(clientFilter.SecureFilter).
				Doc("download pipeline artifacts").
				Param(ws.PathParameter("namespace", "Namespace to use")).
				Param(ws.PathParameter("name", "Pipeline name to filter scope")).
				To(handler.DownloadArtifacts).
				Returns(http.StatusOK, "download all pipeline artifact", download.ArtifactFile{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/view").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get pipeline view").
				Param(ws.PathParameter("namespace", "Namespace to use")).
				Param(ws.PathParameter("name", "Pipeline name to filter scope")).
				To(handler.GetPipelineView).
				Returns(http.StatusOK, "get pipeline view", v1alpha1.PipelineViewResult{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListPipeline list pipeline instances
func (h Handler) ListPipeline(req *restful.Request, res *restful.Response) {
	cacheClient := localcontext.CacheClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	namespace := common.ParseNamespacePathParameter(req)
	list, err := h.Processor.ListPipeline(req.Request.Context(), cacheClient, query, namespace)
	h.WriteResponse(list, err, req, res)
}

//DeletePipeline Delete Pipeline instance
func (h Handler) DeletePipeline(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeletePipeline(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//RetrievePipeline Retrieve Pipeline instance
func (h Handler) RetrievePipeline(req *restful.Request, res *restful.Response) {
	devopsRESTClient := localcontext.DevOpsClient(req.Request.Context())
	cacheClient := localcontext.CacheClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	data, err := h.Processor.RetrievePipeline(req.Request.Context(), cacheClient, devopsRESTClient, namespace, name)

	withFreshStagesStr := req.QueryParameter("withFreshStages")
	if withFreshStages, err := strconv.ParseBool(withFreshStagesStr); err == nil && withFreshStages {
		if taskDetails, err := h.Processor.GetPipelineTasks(req.Request.Context(), devopsRESTClient, namespace, name, 0); err == nil {
			if stages, err := json.Marshal(taskDetails); err == nil {
				data.Status.Jenkins.Stages = string(stages)
			}
		}
	}
	h.WriteResponse(data, err, req, res)
}

//GetPipelineLogs get Pipeline logs
func (h Handler) GetPipelineLogs(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())

	start, err := strconv.Atoi(req.QueryParameter("start"))
	if err != nil {
		start = 0
	}
	stage, err := strconv.Atoi(req.QueryParameter("stage"))
	if err != nil {
		stage = 0
	}
	step, err := strconv.Atoi(req.QueryParameter("step"))
	if err != nil {
		step = 0
	}
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	data, err := h.Processor.GetPipelineLogs(req.Request.Context(), client, namespace, name, start, stage, step)

	h.WriteResponse(data, err, req, res)
}

//RetryPipelineDetail Retrieve Pipeline instance
func (h Handler) RetryPipelineDetail(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetryPipelineDetail(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//AbortPipeline abort a pipeline
func (h Handler) AbortPipeline(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.AbortPipeline(req.Request.Context(), client, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// GetPipelineTasks get pipeline task
func (h Handler) GetPipelineTasks(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	stage, err := strconv.Atoi(req.QueryParameter("stage"))
	if err != nil {
		stage = 0
	}
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	data, err := h.Processor.GetPipelineTasks(req.Request.Context(), client, namespace, name, stage)

	h.WriteResponse(data, err, req, res)
}

func (h Handler) PipelineInputs(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())

	devopsClient := localcontext.DevOpsClient(req.Request.Context())

	opt := new(InputOptions)
	if err := req.ReadEntity(opt); err != nil {
		h.WriteResponse(nil, err, req, res)
	}
	opt.Name = req.PathParameter("name")
	opt.Namespace = req.PathParameter("namespace")
	result, err := h.Processor.InputHandle(req.Request.Context(), devopsClient, k8sClient, opt)

	h.WriteResponse(result, err, req, res)
}

//GetPipelineTestReports
func (h Handler) GetPipelineTestReports(req *restful.Request, res *restful.Response) {
	k8sClient := abcontext.Client(req.Request.Context())

	devopsClient := localcontext.DevOpsClient(req.Request.Context())

	opt := &PipelineTestReportsOptions{}
	start, err := strconv.Atoi(req.QueryParameter("start"))
	if err != nil {
		start = 0
	}
	limit, err := strconv.Atoi(req.QueryParameter("limit"))
	if err != nil {
		log.Error("Error parsing start query parameter: original ", log.String("limit", req.QueryParameter("limit")), log.Err(err))
		limit = 100
	}
	opt.Start = int64(start)
	opt.Limit = int64(limit)
	opt.Name = req.PathParameter("name")
	opt.Namespace = req.PathParameter("namespace")
	result, err := h.Processor.HandlePipelineTestReport(req.Request.Context(), devopsClient, k8sClient, opt)
	h.WriteResponse(result, err, req, res)
}

//GetPipelineArtifacts  get pipeline artifact info
func (h Handler) GetPipelineArtifacts(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	result, err := h.Processor.GetPipelineArtifacts(req.Request.Context(), devopsClient, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(result, err, req, res)
}

//DownloadArtifact download specific artifact
func (h Handler) DownloadArtifact(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	filename := req.PathParameter("filename")
	opt := &PipelineDownloadOptions{}
	opt.Filename = filename
	opt.All = false
	opt.Name = req.PathParameter("name")
	opt.Namespace = req.PathParameter("namespace")

	result, _, err := h.Processor.DownloadArtifact(req.Request.Context(), devopsClient, opt)
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.AddHeader("Content-Type", "application/octet-stream")
	res.AddHeader("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", req.PathParameter("filename")))

	defer result.Close()
	_, err = io.Copy(res.ResponseWriter, result)
	if err != nil {
		klog.Errorf("Copy response body from result to res err err is %v", err)
	}
}

//DownloadArtifacts download all artifacts
func (h Handler) DownloadArtifacts(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())

	opt := &PipelineDownloadOptions{}
	opt.All = true
	opt.Name = req.PathParameter("name")
	opt.Namespace = req.PathParameter("namespace")

	result, _, err := h.Processor.DownloadArtifact(req.Request.Context(), devopsClient, opt)

	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	defer result.Close()
	res.AddHeader("Content-Type", "application/octet-stream")
	res.AddHeader("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", "archive.zip"))

	_, err = io.Copy(res.ResponseWriter, result)
	if err != nil {
		klog.Errorf("Copy response body from result to res err err is %v", err)
	}
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}

func (h Handler) GetPipelineView(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	result, err := h.Processor.GetPipelineView(req.Request.Context(), devopsClient, req.PathParameter("namespace"), req.PathParameter("name"))
	h.WriteResponse(result, err, req, res)
}
