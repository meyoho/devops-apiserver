package pipeline

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	"context"
	"encoding/json"
	"fmt"
	"github.com/appscode/jsonpatch"

	"strings"

	controllerruntimeclient "sigs.k8s.io/controller-runtime/pkg/client"

	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/errors"

	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"k8s.io/client-go/kubernetes"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListPipeline list pipeline instances
func (p processor) ListPipeline(ctx context.Context, cacheClient controllerruntimeclient.Client, query *dataselect.Query, namespace *common.NamespaceQuery) (data *PipelineList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list pipeline end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()

	pipelineList := &v1alpha1.PipelineList{}
	err = cacheClient.List(ctx, &controllerruntimeclient.ListOptions{
		LabelSelector: getLabelSelectorByDsQuery(query),
		Namespace:     namespace.ToRequestParam(),
	}, pipelineList)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &PipelineList{
		Items:    make([]v1alpha1.Pipeline, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}

	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider
	// filter using standard filters
	itemCells := toCells(pipelineList.Items, annotationProvider)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := fromCells(itemCells)

	data.Items = ConvertToPipelineSlice(result)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeletePipeline delete pipeline instance
func (p processor) DeletePipeline(ctx context.Context, client versioned.Interface, namespace, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete pipeline end", log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().Pipelines(namespace).Delete(name, &metav1.DeleteOptions{})
	return
}

// RetrievePipeline retrieve pipeline instance
func (p processor) RetrievePipeline(ctx context.Context, cacheClient controllerruntimeclient.Client, devopsRESTClient versioned.Interface, namespace, name string) (data *v1alpha1.Pipeline, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipeline end", log.String("name", name), log.Err(err))
	}()

	data = &v1alpha1.Pipeline{}
	err = cacheClient.Get(ctx, controllerruntimeclient.ObjectKey{Namespace: namespace, Name: name}, data)
	if errors.IsNotFoundError(err) {
		// if we cannot get Pipeline from local cache, we will use rest client to request it
		logger.Debug("Cannot get Pipeline from local cache, try to get it from server")
		data, err = devopsRESTClient.DevopsV1alpha1().Pipelines(namespace).Get(name, common.GetOptionsInCache)
	}
	return data, err
}

// RetryPipelineDetail retry pipeline
func (p processor) RetryPipelineDetail(ctx context.Context, client versioned.Interface, namespace, name string) (pipe *v1alpha1.Pipeline, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Retry pipeline end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	spec := new(RetryRequest)
	spec.Namespace = namespace
	spec.Name = name

	originalPipe, err := client.DevopsV1alpha1().Pipelines(namespace).Get(name, jenkins.GetOptionsInCache)
	if err != nil {
		return
	}
	pipe = originalPipe.DeepCopy()
	pipe.ObjectMeta = common.CloneMeta(pipe.ObjectMeta)
	if pipe.ObjectMeta.Labels != nil {
		if _, ok := pipe.ObjectMeta.Labels["created_by"]; ok {
			pipe.ObjectMeta.Labels["created_by"] = ""
		}
	} else {
		pipe.ObjectMeta.Labels = make(map[string]string, 0)
	}

	// mark this pipeline was replay from another one
	pipe.ObjectMeta.Labels[devops.LabelReplayedFrom] = originalPipe.Name

	// we should set Name to empty to enable to use generatename
	// https://github.com/kubernetes/apimachinery/blob/release-1.13/pkg/apis/meta/v1/types.go#L105
	pipe.Name = ""
	pipe.GenerateName = pipe.Spec.PipelineConfig.Name
	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider

	pipe.SetAnnotations(cleanupAnnotations(pipe.GetAnnotations(), annotationProvider))

	pipe, err = client.DevopsV1alpha1().Pipelines(spec.Namespace).Create(pipe)
	return
}

// AbortPipeline abort a pipeline
func (p processor) AbortPipeline(ctx context.Context, client versioned.Interface, namespace, name string) (pipe *v1alpha1.Pipeline, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("abort pipeline end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	oldPipeline, err := client.DevopsV1alpha1().Pipelines(namespace).Get(name, api.GetOptionsInCache)
	if err != nil {
		return nil, err
	}

	newPipeline := oldPipeline.DeepCopy()
	newPipeline.Spec.Cancel = true
	for i, cond := range newPipeline.Status.Conditions {
		if cond.Type == v1alpha1.PipelineConditionTypeCancelled {
			newPipeline.Status.Conditions[i] = v1alpha1.Condition{
				Type:   v1alpha1.PipelineConditionTypeCancelled,
				Status: v1alpha1.ConditionStatusUnknown,
			}
			break
		}
	}

	oldJson, err := json.Marshal(oldPipeline)
	if err != nil {
		return nil, err
	}

	newJson, err := json.Marshal(newPipeline)
	if err != nil {
		return nil, err
	}

	patchOperations, err := jsonpatch.CreatePatch(oldJson, newJson)
	if err != nil {
		return nil, err
	}

	patch, err := json.Marshal(patchOperations)
	if err != nil {
		return nil, err
	}

	pipe, err = client.DevopsV1alpha1().Pipelines(namespace).Patch(name, types.JSONPatchType, patch)

	return
}

// RetrievePipeline retrieve pipeline instance
func (p processor) GetPipelineLogs(ctx context.Context, client versioned.Interface, namespace, name string, start, stage, step int) (Log *LogDetails, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipeline end", log.String("namespace", namespace), log.String("name", name), log.Int("start", start), log.Int("stage", stage), log.Int("step", step), log.Err(err))
	}()

	logOpts := &v1alpha1.PipelineLogOptions{
		Start: int64(start),
		Stage: int64(stage),
		Step:  int64(step),
	}

	pipelineLog, err := client.DevopsV1alpha1().Pipelines(namespace).GetLogs(name, logOpts)
	if err != nil {
		return nil, err
	}
	return &LogDetails{
		PipelineLog: pipelineLog,
	}, nil
}

// GetPipelineTasks  get pipeline task
func (p processor) GetPipelineTasks(ctx context.Context, client versioned.Interface, namespace, name string, stage int) (taskdetail *TaskDetails, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipeline end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	taskOpts := &v1alpha1.PipelineTaskOptions{Stage: int64(stage)}

	pipelineTasks, err := client.DevopsV1alpha1().Pipelines(namespace).GetTasks(name, taskOpts)
	if err != nil {
		logger.Error("Error fetching logs: ", log.Err(err))
		return nil, err
	}
	return &TaskDetails{
		PipelineTask: pipelineTasks,
	}, nil

}

func (p processor) InputHandle(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, opts *InputOptions) (resp *InputResponse, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Input Handler end")
	}()

	resp = &InputResponse{
		Success: false,
	}
	pipelineInputOption := &v1alpha1.PipelineInputOptions{
		Approve: opts.Approve,
		InputID: opts.InputID,
		Stage:   opts.Stage,
		Step:    opts.Step,
	}
	if opts.Parameters != nil {
		params := make([]v1alpha1.PipelineParameter, len(opts.Parameters))
		for i, param := range opts.Parameters {
			params[i] = v1alpha1.PipelineParameter{
				Name:  param.Name,
				Value: param.Value,
			}
		}
		pipelineInputOption.Parameters = params
	}
	var response *v1alpha1.PipelineInputResponse
	if response, err = client.DevopsV1alpha1().Pipelines(opts.Namespace).Input(opts.Name, pipelineInputOption); err != nil {
		resp.Message = fmt.Sprintf("%v", err)
		return
	}

	resp.Message = "success"
	resp.Code = response.StatusCode
	return

}

func (p processor) HandlePipelineTestReport(ctx context.Context, client versioned.Interface, k8sclient kubernetes.Interface, opt *PipelineTestReportsOptions) (report PipelineTestReports, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Input Handler end")
	}()

	report = PipelineTestReports{
		Regression: make([]v1alpha1.PipelineTestReportItem, 0),
		Failed:     make([]v1alpha1.PipelineTestReportItem, 0),
		Skipped:    make([]v1alpha1.PipelineTestReportItem, 0),
		Fixed:      make([]v1alpha1.PipelineTestReportItem, 0),
		Passed:     make([]v1alpha1.PipelineTestReportItem, 0),
	}

	pipelineTestReportOption := &v1alpha1.PipelineTestReportOptions{
		Start: opt.Start,
		Limit: opt.Limit,
	}

	var (
		testResport *v1alpha1.PipelineTestReport
	)

	if testResport, err = client.DevopsV1alpha1().Pipelines(opt.Namespace).GetTestReports(opt.Name, pipelineTestReportOption); err == nil {
		if testResport.Summary != nil && testResport.Summary.Total > 0 {
			summary := testResport.Summary
			report.Summary = PipelineTestReportSummary{
				ExistingFailed: summary.ExistingFailed,
				Failed:         summary.Failed,
				Fixed:          summary.Fixed,
				Passed:         summary.Passed,
				Regressions:    summary.Regressions,
				Skipped:        summary.Skipped,
				Total:          summary.Total,
			}

			for _, item := range testResport.Items {
				if item.State == "REGRESSION" {
					report.Regression = append(report.Regression, item)
				} else if item.Status == "FAILED" && item.State != "REGRESSION" {
					report.Failed = append(report.Failed, item)
				} else if item.Status == "SKIPPED" {
					report.Skipped = append(report.Skipped, item)
				} else if item.Status == "PASSED" {
					report.Passed = append(report.Passed, item)
					if item.State == "FIXED" {
						report.Fixed = append(report.Fixed, item)
					}
				}
			}
		}
	}

	return
}

// ConvertToPipelineSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToPipelineSlice(filtered []v1alpha1.Pipeline) (items []v1alpha1.Pipeline) {
	items = make([]v1alpha1.Pipeline, 0, len(filtered))
	for _, item := range filtered {
		items = append(items, item)
	}
	return
}

func getLabelSelectorByDsQuery(dsQuery *dataselect.Query) labels.Selector {
	labelSelector := labels.NewSelector()
	if dsQuery == nil || dsQuery.FilterQuery == nil || len(dsQuery.FilterQuery.FilterByList) == 0 {
		return labelSelector
	}

	for _, filterBy := range dsQuery.FilterQuery.FilterByList {
		if filterBy.Property != "label" {
			continue
		}

		filterStrs := strings.Split(fmt.Sprintf("%s", filterBy.Value), ",")
		for _, filterStr := range filterStrs {
			keyAndValue := strings.Split(filterStr, ":")
			if len(keyAndValue) == 2 {
				req, _ := labels.NewRequirement(keyAndValue[0], selection.DoubleEquals, []string{keyAndValue[1]})
				labelSelector = labelSelector.Add(*req)
			}
		}
	}

	return labelSelector
}

func cleanupAnnotations(annotations map[string]string, provider devops.AnnotationProvider) (dest map[string]string) {

	var annotationsWhitelist = []string{
		provider.AnnotationsPipelineConfigName(),
		provider.AnnotationsKeyMultiBranchCategory(),
		provider.AnnotationsKeyMultiBranchName(),
	}

	dest = make(map[string]string)
	if len(annotations) == 0 {
		return
	}
	for _, k := range annotationsWhitelist {
		if val, ok := annotations[k]; ok {
			dest[k] = val
		}
	}
	return
}

// GetPipelineView
func (p processor) GetPipelineView(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.PipelineViewResult, error) {
	var err error
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("get pipeline view end", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()

	return client.DevopsV1alpha1().Pipelines(namespace).GetView(name)
}
