package pipeline

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/log"
	"context"
	"io"
)

type PipelineDownloadOptions struct {
	Namespace string
	Name      string
	All       bool
	Filename  string
}

// GetPipelineArtifacts get pipeline artifact instance
func (p processor) GetPipelineArtifacts(ctx context.Context, client versioned.Interface, namespace, name string) (artifactlist *v1alpha1.PipelineArtifactList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipeline artifact ", log.String("namespace", namespace), log.String("name", name), log.Err(err))
	}()
	result, err := client.DevopsV1alpha1().Pipelines(namespace).GetArtifacts(name)

	if err != nil {
		return nil, err
	}
	return result, nil
}

// DownloadArtifacts get pipeline artifact instance
func (p processor) DownloadArtifact(ctx context.Context, client versioned.Interface, opt *PipelineDownloadOptions) (data io.ReadCloser, contenttype string, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get pipeline artifact ", log.String("namespace", opt.Namespace), log.String("name", opt.Name), log.Err(err))
	}()

	opts := &v1alpha1.DownloadOption{
		FileName: opt.Filename,
		All:      opt.All,
	}
	data, err = client.DevopsV1alpha1().Pipelines(opt.Namespace).DownloadArtifact(opt.Name, opts)

	return
}
