package pipeline_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipeline"
	"alauda.io/devops-apiserver/pkg/restapi/handler/testtools"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Processor.JenkinsBinding", func() {
	var (
		processor   pipeline.Processor
		ctx         context.Context
		cacheClient client.Client
		restClient  *fake.Clientset
		query       *dataselect.Query

		list      *pipeline.PipelineList
		err       error
		namespace *common.NamespaceQuery
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 10,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		namespace = common.NewNamespaceQuery([]string{"default"})
		cacheClient = testtools.NewFakeClient()
		restClient = fake.NewSimpleClientset()
		processor = pipeline.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("should return pipeline", func() {
		cacheClient = testtools.NewFakeClient(
			GetPipelineList("pipelinename", "default"),
		)
		list, err = processor.ListPipeline(ctx, cacheClient, query, namespace)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Items).To(HaveLen(2), "should not have any items")
	})

	It("retrive a pipeline", func() {
		cacheClient = testtools.NewFakeClient(
			GetPipeline("pipelinename", "default"),
		)
		pipeline, err := processor.RetrievePipeline(ctx, cacheClient, restClient, "default", "pipelinename")
		Expect(err).To(BeNil())
		Expect(pipeline.Spec.PipelineConfig.Name).To(Equal("pipelineconifg"))

	})

})

func GetPipeline(name, namespace string) *v1alpha1.Pipeline {
	jenkinsbindinginstance := &v1alpha1.Pipeline{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: v1alpha1.PipelineSpec{
			PipelineConfig: v1alpha1.LocalObjectReference{
				Name: "pipelineconifg",
			},
		},
	}
	return jenkinsbindinginstance
}

func GetPipelineList(name, namespace string) *v1alpha1.PipelineList {
	pipelinelist := &v1alpha1.PipelineList{
		TypeMeta: metav1.TypeMeta{
			Kind: "pipeline",
		},

		Items: []v1alpha1.Pipeline{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha1.PipelineSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name + "1",
					Namespace: namespace,
				},
				Spec: v1alpha1.PipelineSpec{},
			},
		},
	}
	return pipelinelist
}
