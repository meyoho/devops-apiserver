package pipeline_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestPipeline(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("pipeline.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/pipeline", []Reporter{junitReporter})
}
