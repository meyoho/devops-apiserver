package commonresourcehandler

import (
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/commonresource"
	"alauda.io/devops-apiserver/pkg/restapi/handler/commonsubresource"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"net/http"
)

// Handler handler for artifactregistry
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
}

func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("common related APIs").ApiVersion("v1").Path("/api/v1/common")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "commonresource")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing req data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{resource}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`Get CommonResourceList`).
					To(handler.GetCommonResourceList).
					Returns(http.StatusOK, "OK", make(map[string]interface{})),
			),
		),
	)
	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("/namespace/{namespace}/{resource}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`Get CommonResourceList`).
					To(handler.GetCommonResourceList).
					Returns(http.StatusOK, "OK", make(map[string]interface{})),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{resource}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get common resource instance").
				To(handler.GetCommonResource).
				Returns(http.StatusOK, "get common resource Instance", make(map[string]interface{})),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/namespace/{namespace}/{resource}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get common resource instance").
				To(handler.GetCommonResource).
				Returns(http.StatusOK, "get common resource Instance", make(map[string]interface{})),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{resource}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete Common instance").
				Param(ws.PathParameter("name", "name of the common resource")).
				Param(ws.PathParameter("resource", "kind of the common resource")).
				To(handler.DeleteCommonResource).
				Returns(http.StatusOK, "Delete Common resource Complete", make(map[string]interface{})),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/namespace/{namespace}/{resource}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete Common instance").
				Param(ws.PathParameter("name", "name of the common resource")).
				Param(ws.PathParameter("resource", "kind of the common resource")).
				To(handler.DeleteCommonResource).
				Returns(http.StatusOK, "Delete Common resource Complete", make(map[string]interface{})),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{resource}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Post Create rescource instance").
				To(handler.PostCommonResource).
				Returns(http.StatusOK, "Retrieve CodeQualityProject instance", make(map[string]interface{})),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/namespace/{namespace}/{resource}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Post Create rescource instance").
				To(handler.PostCommonResource).
				Returns(http.StatusOK, "Retrieve CodeQualityProject instance", make(map[string]interface{})),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{resource}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update common resource instance").
				Param(restful.PathParameter("name", "ArtifactRegistry name to filter scope")).
				Param(ws.PathParameter("resource", "kind of the common resource")).
				To(handler.PutCommonResource).
				Returns(http.StatusOK, "Update common resource instance", make(map[string]interface{})),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/namespace/{namespace}/{resource}/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update common resource instance").
				Param(restful.PathParameter("name", "ArtifactRegistry name to filter scope")).
				Param(ws.PathParameter("resource", "kind of the common resource")).
				To(handler.PutCommonResource).
				Returns(http.StatusOK, "Update common resource instance", make(map[string]interface{})),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{resource}/{name}/sub/{sub}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update common resource instance").
				Param(restful.PathParameter("name", "ArtifactRegistry name to filter scope")).
				Param(ws.PathParameter("resource", "kind of the common resource")).
				Param(ws.PathParameter("sub", "subresource of the common resource")).
				To(handler.GetCommonResourceSub).
				Returns(http.StatusOK, "Get common resource instance", make(map[string]interface{})),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/namespace/{namespace}/{resource}/{name}/sub/{sub}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update common resource instance").
				Param(restful.PathParameter("name", "ArtifactRegistry name to filter scope")).
				Param(ws.PathParameter("resource", "kind of the common resource")).
				Param(ws.PathParameter("sub", "subresource of the common resource")).
				To(handler.GetCommonResourceSub).
				Returns(http.StatusOK, "Get common resource instance", make(map[string]interface{})),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{resource}/{name}/sub/{sub}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Post Create rescource instance").
				To(handler.PostCommonResourceSub).
				Returns(http.StatusOK, "Retrieve CodeQualityProject instance", make(map[string]interface{})),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/namespace/{namespace}/{resource}/{name}/sub/{sub}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Post Create rescource instance").
				To(handler.PostCommonResourceSub).
				Returns(http.StatusOK, "Retrieve CodeQualityProject instance", make(map[string]interface{})),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// GetCommonResourceList list common reource list
func (h Handler) GetCommonResourceList(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	resource := req.PathParameter("resource")
	namespace := req.PathParameter("namespace")
	query := req.Request.URL.Query()
	parameters := make(map[string]string)
	for k, v := range query {
		parameters[k] = v[0]
	}
	result, err := commonresource.GetResourceList(devopsClient, namespace, resource, parameters)
	h.WriteResponse(result, err, req, res)
}

//GetCommonResource get common resource
func (h Handler) GetCommonResource(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	resource := req.PathParameter("resource")
	name := req.PathParameter("name")
	namespace := req.PathParameter("namespace")
	query := req.Request.URL.Query()
	parameters := make(map[string]string)
	for k, v := range query {
		parameters[k] = v[0]
	}
	result, err := commonresource.GetResource(devopsClient, namespace, resource, name, parameters)
	h.WriteResponse(result, err, req, res)
}

func (h Handler) DeleteCommonResource(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())

	resource := req.PathParameter("resource")
	name := req.PathParameter("name")
	namespace := req.PathParameter("namespace")
	query := req.Request.URL.Query()
	parameters := make(map[string]string)
	for k, v := range query {
		parameters[k] = v[0]
	}

	_, err := commonresource.DeleteResource(devopsClient, namespace, resource, name, parameters)
	h.WriteResponse(nil, err, req, res)
}

func (h Handler) PostCommonResource(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())
	logName := "handlePostCommonResource"
	spec := req.Request.Body

	logger.Info(logName, log.Any("spec", spec))
	resource := req.PathParameter("resource")
	namespace := req.PathParameter("namespace")

	query := req.Request.URL.Query()
	parameters := make(map[string]string)
	for k, v := range query {
		parameters[k] = v[0]
	}

	result, err := commonresource.PostResource(devopsClient, namespace, resource, parameters, spec)
	h.WriteResponse(result, err, req, res)
}

//UpdateJenkins update CodeQualityProject instance
func (h Handler) PutCommonResource(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())
	logger := abcontext.Logger(req.Request.Context())
	logName := "handlePutCommonResource"
	spec := req.Request.Body

	logger.Info(logName, log.Any("spec", spec))
	resource := req.PathParameter("resource")
	name := req.PathParameter("name")
	namespace := req.PathParameter("namespace")

	query := req.Request.URL.Query()
	parameters := make(map[string]string)
	for k, v := range query {
		parameters[k] = v[0]
	}

	result, err := commonresource.PutResource(devopsClient, namespace, resource, name, parameters, spec)
	h.WriteResponse(result, err, req, res)
}

// GetCommonResourceSub get commonresource sub resource

func (h Handler) GetCommonResourceSub(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())

	resource := req.PathParameter("resource")
	name := req.PathParameter("name")
	sub := req.PathParameter("sub")
	namespace := req.PathParameter("namespace")
	query := req.Request.URL.Query()
	parameters := make(map[string]string)
	for k, v := range query {
		parameters[k] = v[0]
	}
	result, err := commonsubresource.GetResourceSub(devopsClient, namespace, resource, name, sub, parameters)
	h.WriteResponse(result, err, req, res)
}

func (h Handler) PostCommonResourceSub(req *restful.Request, res *restful.Response) {
	devopsClient := localcontext.DevOpsClient(req.Request.Context())

	body := req.Request.Body

	resource := req.PathParameter("resource")
	name := req.PathParameter("name")
	sub := req.PathParameter("sub")
	namespace := req.PathParameter("namespace")
	query := req.Request.URL.Query()
	parameters := make(map[string]string)
	for k, v := range query {
		parameters[k] = v[0]
	}

	result, err := commonsubresource.PostResourceSub(devopsClient, namespace, resource, name, sub, parameters, body)

	h.WriteResponse(result, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
