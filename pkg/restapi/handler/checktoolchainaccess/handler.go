package checktoolchainaccess

import (
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"context"
	"github.com/emicklei/go-restful"
	authorizationapi "k8s.io/api/authorization/v1"
	"k8s.io/client-go/kubernetes"
	"net/http"
)

// Handler handler for artifactregistry
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	CheckToolChainAccess(ctx context.Context, k8sclient kubernetes.Interface, resourceList []string, namespace string) (*authorizationapi.SelfSubjectAccessReview, error)
}

// New builder method for codequalityproject.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("toolchain related APIs").ApiVersion("v1").Path("/api/v1/checkalltoolchain")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Filter(clientFilter.SecureFilter).
					Doc(`List CodeRepoBinding instances`).
					To(handler.CheckToolChainAccess).
					Writes(authorizationapi.SelfSubjectAccessReview{}).
					Returns(http.StatusOK, "OK", authorizationapi.SelfSubjectAccessReview{}),
			),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

func (h Handler) CheckToolChainAccess(req *restful.Request, res *restful.Response) {

	resourceList := []string{"projectmanagements", "codereposervices", "jenkinses", "codequalitytools", "imageregistries", "artifactregistrymanagers", "artifactregistries"}

	namespace := req.PathParameter("namespace")

	k8sClient := abcontext.Client(req.Request.Context())

	selfSubjectAccessReview, err := h.Processor.CheckToolChainAccess(req.Request.Context(), k8sClient, resourceList, namespace)

	h.WriteResponse(selfSubjectAccessReview, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
