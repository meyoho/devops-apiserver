package checktoolchainaccess

import (
	"context"
	"strconv"

	authorizationapi "k8s.io/api/authorization/v1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"

	"k8s.io/client-go/kubernetes"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

func (p processor) CheckToolChainAccess(ctx context.Context, k8sclient kubernetes.Interface, resourceList []string, namespace string) (selfSubjectAccessReview *authorizationapi.SelfSubjectAccessReview, err error) {

	selfSubjectAccessReview = &authorizationapi.SelfSubjectAccessReview{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "authorization.k8s.io/v1",
			Kind:       "SelfSubjectAccessReview",
		},
		Spec: authorizationapi.SelfSubjectAccessReviewSpec{
			ResourceAttributes: &authorizationapi.ResourceAttributes{
				Group:     "devops.alauda.io",
				Namespace: namespace,
				Resource:  "",
				Verb:      "create",
			},
		},
	}

	resultBool := false

	logger := abcontext.Logger(ctx)

	for _, resource := range resourceList {

		selfSubjectAccessReview.Spec.ResourceAttributes.Resource = resource

		self, err := k8sclient.AuthorizationV1().SelfSubjectAccessReviews().Create(selfSubjectAccessReview)
		if err != nil {
			return selfSubjectAccessReview, err
		}

		logger.Debug("checkalltool resource " + resource + " is " + strconv.FormatBool(self.Status.Allowed))
		resultBool = resultBool || self.Status.Allowed
		if resultBool {
			break
		}
	}

	selfSubjectAccessReview.Status.Allowed = resultBool
	selfSubjectAccessReview.Spec.ResourceAttributes.Resource = "alltoolchain"

	return
}
