package codereposervice

import (
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/coderepository"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelineconfig"
	"context"
	"fmt"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"strings"
	"sync"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListCodeRepoService list codereposervice instances
func (p processor) ListCodeRepoService(ctx context.Context, client versioned.Interface, query *dataselect.Query) (data *CodeRepoServiceList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list codereposervice end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   getLabelSelectorByDsQuery(query).String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	codereposervicelist, err := client.DevopsV1alpha1().CodeRepoServices().List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &CodeRepoServiceList{
		Items:    make([]v1alpha1.CodeRepoService, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(codereposervicelist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToCodeRepoServiceSlice(result)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeleteCodeRepoService delete codereposervice instance
func (p processor) DeleteCodeRepoService(ctx context.Context, client versioned.Interface, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete codereposervice end", log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().CodeRepoServices().Delete(name, &metav1.DeleteOptions{})
	return
}

// RetrieveCodeRepoService retrieve codereposervice instance
func (p processor) RetrieveCodeRepoService(ctx context.Context, client versioned.Interface, name string) (data *v1alpha1.CodeRepoService, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get codereposervice end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().CodeRepoServices().Get(name, jenkins.GetOptionsInCache)
	return
}

// CodeRepoServiceResourceList
func (p processor) CodeRepoServiceResourceList(ctx context.Context, client versioned.Interface, namespace *common.NamespaceQuery, query *dataselect.Query) (resourcelist *common.ResourceList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get codereposervice resoure list end", log.Err(err))
	}()
	resourcelist = &common.ResourceList{}
	repositoryList, err := coderepository.NewProcessor().ListCodeRepository(ctx, client, namespace, query)
	if err != nil {
		return
	}

	annotationProvider := localcontext.ExtraConfig(ctx).AnnotationProvider
	wait := sync.WaitGroup{}
	for _, r := range repositoryList.Items {
		go func(namespace, name string) {
			dsQuery := dataselect.GeSimpleFieldQuery(common.CodeRepositoryProperty, name)
			items := pipelineconfig.GetPipelineConfigListAsResourceList(client, namespace, dsQuery, annotationProvider)
			resourcelist.Items = append(resourcelist.Items, items...)
			wait.Done()
		}(r.ObjectMeta.Namespace, r.ObjectMeta.Name)
		wait.Add(1)
	}
	wait.Wait()

	return
}

//CreateCodeRepoService create codereposervice instance
func (p processor) CreateCodeRepoService(ctx context.Context, client versioned.Interface, codereposervice *v1alpha1.CodeRepoService) (data *v1alpha1.CodeRepoService, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create codereposervice end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().CodeRepoServices().Create(codereposervice)
	return
}

//UpdateCodeRepoService update a codereposervice instance
func (p processor) UpdateCodeRepoService(ctx context.Context, client versioned.Interface, codereposervice *v1alpha1.CodeRepoService, name string) (data *v1alpha1.CodeRepoService, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update codereposervice end", log.Err(err), log.String("name", name))
	}()
	oldCodeRepoService := new(v1alpha1.CodeRepoService)
	oldCodeRepoService, err = client.DevopsV1alpha1().CodeRepoServices().Get(name, jenkins.GetOptionsInCache)

	if err != nil {
		return
	}

	oldCodeRepoService.SetAnnotations(codereposervice.GetAnnotations())
	oldCodeRepoService.Spec = codereposervice.Spec

	data, err = client.DevopsV1alpha1().CodeRepoServices().Update(oldCodeRepoService)
	return
}

// ConvertToCodeRepoServiceSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToCodeRepoServiceSlice(filtered []metav1.Object) (items []v1alpha1.CodeRepoService) {
	items = make([]v1alpha1.CodeRepoService, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.CodeRepoService); ok {
			cm.Kind = v1alpha1.ResourceKindCodeRepoService
			items = append(items, *cm)
		}
	}
	return
}

func AuthorizeService(client versioned.Interface, codereposerviceName, secretName, namespace string) (*v1alpha1.CodeRepoServiceAuthorizeResponse, error) {
	opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
		SecretName: secretName,
		Namespace:  namespace,
	}
	authorizeResponse, err := client.DevopsV1alpha1().Jenkinses().Authorize(codereposerviceName, &opts)
	fmt.Println("authorizeResponse:", authorizeResponse, " err:", err)
	return authorizeResponse, err
}

func getLabelSelectorByDsQuery(dsQuery *dataselect.Query) labels.Selector {
	labelSelector := labels.NewSelector()
	if dsQuery == nil || dsQuery.FilterQuery == nil || len(dsQuery.FilterQuery.FilterByList) == 0 {
		return labelSelector
	}

	for _, filterBy := range dsQuery.FilterQuery.FilterByList {
		if filterBy.Property != "label" {
			continue
		}

		filterStrs := strings.Split(fmt.Sprintf("%s", filterBy.Value), ",")
		for _, filterStr := range filterStrs {
			keyAndValue := strings.Split(filterStr, ":")
			if len(keyAndValue) == 2 {
				req, _ := labels.NewRequirement(keyAndValue[0], selection.DoubleEquals, []string{keyAndValue[1]})
				labelSelector = labelSelector.Add(*req)
			}
		}
	}

	return labelSelector
}
