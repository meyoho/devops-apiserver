package codereposervice

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
)

// CodeRepoServiceList contains a list of CodeRepoService in the cluster.
type CodeRepoServiceList struct {
	ListMeta jenkins.ListMeta `json:"listMeta"`

	// Unordered list of CodeRepoService.
	Items []v1alpha1.CodeRepoService `json:"codereposervices"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// RetryRequest request a retry for pipeline
type RetryRequest struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`

	// empty for now
}

// LogDetails log details
type LogDetails struct {
	*v1alpha1.PipelineLog
}

// TaskDetails jenkins step details
type TaskDetails struct {
	*v1alpha1.PipelineTask
}
