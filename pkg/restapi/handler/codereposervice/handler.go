package codereposervice

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for CoderepoService
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListCodeRepoService(ctx context.Context, client versioned.Interface, query *dataselect.Query) (*CodeRepoServiceList, error)
	DeleteCodeRepoService(ctx context.Context, client versioned.Interface, name string) error
	RetrieveCodeRepoService(ctx context.Context, client versioned.Interface, name string) (*v1alpha1.CodeRepoService, error)
	CreateCodeRepoService(ctx context.Context, client versioned.Interface, CoderepoService *v1alpha1.CodeRepoService) (*v1alpha1.CodeRepoService, error)
	UpdateCodeRepoService(ctx context.Context, client versioned.Interface, CoderepoService *v1alpha1.CodeRepoService, name string) (*v1alpha1.CodeRepoService, error)
	CodeRepoServiceResourceList(ctx context.Context, client versioned.Interface, namespace *common.NamespaceQuery, query *dataselect.Query) (*common.ResourceList, error)
}

// New builder method for codereposervice.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("CoderepoService related APIs").ApiVersion("v1").Path("/api/v1/codereposervice")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "codereposervice")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List CoderepoService instance`).
					To(handler.ListCoderepoService).
					Writes(CodeRepoServiceList{}).
					Returns(http.StatusOK, "OK", CodeRepoServiceList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create CodeRepoService instance").
				To(handler.CreateCodeRepoService).
				Returns(http.StatusOK, "Create CodeRepoService instance", v1alpha1.CodeRepoService{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete CoderepoService instance").
				Param(ws.PathParameter("name", "name of the CoderepoService")).
				To(handler.DeleteCodeRepoService).
				Returns(http.StatusOK, "Delete CoderepoService Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Get CoderepoService Instance Detail").
				Param(ws.PathParameter("name", "name of the CoderepoService")).
				To(handler.RetrieveCodeRepoService).
				Returns(http.StatusOK, "Retrieve CoderepoService instance", v1alpha1.CodeRepoService{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("update a codereposervice").
				Param(ws.PathParameter("name", "name of the CoderepoService")).
				To(handler.UpdateCodeRepoService).
				Returns(http.StatusOK, "Retrieve CoderepoService instance", v1alpha1.CodeRepoService{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}/resources").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get codereposervice resource").
				Param(ws.PathParameter("namespace", "namespace of the CoderepoService")).
				Param(ws.PathParameter("name", "name of the CoderepoService")).
				To(handler.CodeRepoServiceResourceList).
				Returns(http.StatusOK, "abort CoderepoService instance", common.ResourceList{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListCoderepoService list codereposervice instances
func (h Handler) ListCoderepoService(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListCodeRepoService(req.Request.Context(), client, query)
	h.WriteResponse(list, err, req, res)
}

//DeleteCodeRepoService Delete CoderepoService instance
func (h Handler) DeleteCodeRepoService(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteCodeRepoService(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//RetrieveCoderepoService Retrieve CoderepoService instance
func (h Handler) RetrieveCodeRepoService(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrieveCodeRepoService(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// CodeRepoServiceResourceList get reposervice resource list
func (h Handler) CodeRepoServiceResourceList(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())

	namespace := common.ParseNamespacePathParameter(req)
	name := req.PathParameter("name")
	dsQuery := dataselect.GeSimpleLabelQuery(common.CodeRepoServiceProperty, name)
	data, err := h.Processor.CodeRepoServiceResourceList(req.Request.Context(), client, namespace, dsQuery)

	h.WriteResponse(data, err, req, res)
}

//CreateCodeRepoService create CoderepoService instance
func (h Handler) CreateCodeRepoService(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newCodeRepoService := new(v1alpha1.CodeRepoService)

	if err := req.ReadEntity(newCodeRepoService); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.CreateCodeRepoService(req.Request.Context(), client, newCodeRepoService)
	h.WriteResponse(data, err, req, res)
}

//UpdateCodeRepoService update CoderepoService instance
func (h Handler) UpdateCodeRepoService(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newCodeRepoService := new(v1alpha1.CodeRepoService)

	if err := req.ReadEntity(newCodeRepoService); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.UpdateCodeRepoService(req.Request.Context(), client, newCodeRepoService, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
