package codereposervice_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codereposervice"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Processor.codereposervice", func() {
	var (
		processor codereposervice.Processor
		ctx       context.Context
		client    *fake.Clientset
		query     *dataselect.Query

		list *codereposervice.CodeRepoServiceList
		err  error
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 10,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		client = fake.NewSimpleClientset()
		processor = codereposervice.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("should return pipeline", func() {
		client = fake.NewSimpleClientset(
			GetCodeServiceList("pipelinename", "default"),
		)
		list, err = processor.ListCodeRepoService(ctx, client, query)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Items).To(HaveLen(2), "should not have any items")
	})

})

func GetCodeServiceList(name, namespace string) *v1alpha1.CodeRepoServiceList {
	codereposervicelist := &v1alpha1.CodeRepoServiceList{
		TypeMeta: metav1.TypeMeta{
			Kind: "pipelineconfig",
		},

		Items: []v1alpha1.CodeRepoService{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: v1alpha1.CodeRepoServiceSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name + "1",
					Namespace: namespace,
				},
				Spec: v1alpha1.CodeRepoServiceSpec{},
			},
		},
	}
	return codereposervicelist
}
