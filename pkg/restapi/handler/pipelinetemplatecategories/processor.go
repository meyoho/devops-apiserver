package pipelinetemplatecategories

import (
	"context"

	"sort"

	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListProjectManagement list projectmanagement instances
func (p processor) ListPipelineTemplateFilters(ctx context.Context, client versioned.Interface, namespace string) (filter PipelineTemplateFilter, err error) {

	clusterTemplates, err := client.DevopsV1alpha1().ClusterPipelineTemplates().List(api.ListEverything)
	if err != nil {
		return
	}

	templates, err := client.DevopsV1alpha1().PipelineTemplates(namespace).List(api.ListEverything)
	if err != nil {
		return
	}

	var objects = make([]metav1.Object, 0, len(clusterTemplates.Items)+len(templates.Items))
	for _, _item := range clusterTemplates.Items {
		item := _item
		objects = append(objects, &item)
	}
	for _, _item := range templates.Items {
		item := _item
		objects = append(objects, &item)
	}

	groupedLabels := groupLabels(objects, "category", "lang")

	filter = PipelineTemplateFilter{}
	filter.Items = groupedLabels["category"]
	filter.Lang = groupedLabels["lang"]

	return
}

func groupLabels(objects []metav1.Object, labelKeys ...string) map[string][]FilterItem {

	var result = map[string][]FilterItem{}
	// resultIndex will used to ensure not add duplicated label value into result
	var resultIndex = map[string]map[string]struct{}{}
	for _, key := range labelKeys {
		result[key] = []FilterItem{}
		resultIndex[key] = map[string]struct{}{}
	}

	for _, item := range objects {
		if len(item.GetLabels()) == 0 {
			continue
		}

		labels := item.GetLabels()

		for _, key := range labelKeys {
			value, ok := labels[key]
			if !ok {
				continue
			}

			if _, ok := resultIndex[key][value]; !ok {
				result[key] = append(result[key], FilterItem{Name: value})
				resultIndex[key][value] = struct{}{}
			}
		}
	}

	// we should sort all filter values in order to permanent position
	for key := range result {
		items := result[key]
		sort.Slice(items, func(i, j int) bool {
			return items[i].Name < items[j].Name
		})
	}

	return result
}
