package pipelinetemplatecategories

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("pipelinetemplatecategories.GroupLabels", func() {

	It("Should Grouped Labels", func() {
		var items1 = []metav1.Object{
			&v1alpha1.PipelineTemplate{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"category": "Build",
						"lang":     "Java",
					},
				},
			},
			&v1alpha1.PipelineTemplate{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"category": "Build",
					},
				},
			},
			&v1alpha1.PipelineTemplate{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"lang": "Golang",
					},
				},
			},
			&v1alpha1.PipelineTemplate{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"category": "Sync",
						"lang":     "Golang",
					},
				},
			},
			&v1alpha1.PipelineTemplate{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{},
				},
			},
			&v1alpha1.PipelineTemplate{
				ObjectMeta: metav1.ObjectMeta{},
			},
		}

		grouped := groupLabels(items1, "category", "lang")
		category := grouped["category"]
		Expect(len(category)).To(Equal(2), "category length should be 2")
		lang := grouped["lang"]
		Expect(len(lang)).To(Equal(2), "lang length should be 2")

		Expect(category).Should(BeEquivalentTo([]FilterItem{
			{
				Name: "Build",
			},
			{
				Name: "Sync",
			},
		}))

		Expect(lang).Should(BeEquivalentTo([]FilterItem{
			{
				Name: "Golang",
			},
			{
				Name: "Java",
			},
		}))
	})
})
