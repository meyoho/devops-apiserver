package pipelinetemplatecategories

import (
	"testing"

	"github.com/onsi/ginkgo/reporters"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestPipelinetemplatecategories(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("Pipelinetemplatecategories.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/pipelinetemplatecategories", []Reporter{junitReporter})
}
