package pipelinetemplatecategories

import (
	"context"
	"net/http"

	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for PipelineTemplateCategory
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListPipelineTemplateFilters(ctx context.Context, client versioned.Interface, namespace string) (filter PipelineTemplateFilter, err error)
}

// New builder method for projectmanagement.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("PipelineTemplateCategory related APIs").ApiVersion("v1").Path("/api/v1/pipelinetemplatecategories")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "pipelinetemplatecategories")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List PipelineTemplateCategory instance`).
					To(handler.ListPipelineTemplateFilters).
					Writes(PipelineTemplateFilter{}).
					Returns(http.StatusOK, "OK", PipelineTemplateFilter{}),
			),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListPipelineTemplateFilters list projectmanagement instances
func (h Handler) ListPipelineTemplateFilters(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	namespace := req.PathParameter("namespace")

	filters, err := h.Processor.ListPipelineTemplateFilters(req.Request.Context(), client, namespace)

	if err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}

	h.WriteResponse(filters, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
