package pipelinetemplatecategories

// PipelineTemplateCategory is category for PipelineTemplate
type FilterItem struct {
	Name string `json:"name"`
}

type PipelineTemplateFilter struct {
	Items []FilterItem `json:"items"`
	Lang  []FilterItem `json:"lang"`
}
