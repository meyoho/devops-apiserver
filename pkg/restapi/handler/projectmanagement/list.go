package projectmanagement

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
)

// ProjectManagementList contains a list of ProjectManagement in the cluster.
type ProjectManagementList struct {
	ListMeta jenkins.ListMeta `json:"listMeta"`

	// Unordered list of ProjectManagement.
	Items []v1alpha1.ProjectManagement `json:"jiras"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// RetryRequest request a retry for pipeline
type RetryRequest struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`

	// empty for now
}

// LogDetails log details
type LogDetails struct {
	*v1alpha1.PipelineLog
}

// TaskDetails jenkins step details
type TaskDetails struct {
	*v1alpha1.PipelineTask
}
