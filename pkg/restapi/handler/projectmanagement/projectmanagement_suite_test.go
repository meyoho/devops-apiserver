package projectmanagement_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestProjectmanagement(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("projectmanagement.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/restapi/handler/projectmanagement", []Reporter{junitReporter})
}
