package projectmanagement

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	localcontext "alauda.io/devops-apiserver/pkg/restapi/context"
	"alauda.io/devops-apiserver/pkg/restapi/decorator"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

// WARNING: this handler is an example. must be changed to the appropriate logic

// Handler handler for ProjectManagement
type Handler struct {
	Server    server.Server
	Processor Processor
}

// NewHandler constructor
func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		Server:    svr,
		Processor: processor,
	}
}

// Processor basic interface for data manipulation object
type Processor interface {
	ListProjectManagement(ctx context.Context, client versioned.Interface, query *dataselect.Query) (*ProjectManagementList, error)
	DeleteProjectManagement(ctx context.Context, client versioned.Interface, name string) error
	RetrieveProjectManagement(ctx context.Context, client versioned.Interface, name string) (*v1alpha1.ProjectManagement, error)
	CreateProjectManagement(ctx context.Context, client versioned.Interface, ProjectManagement *v1alpha1.ProjectManagement) (*v1alpha1.ProjectManagement, error)
	UpdateProjectManagement(ctx context.Context, client versioned.Interface, ProjectManagement *v1alpha1.ProjectManagement, name string) (*v1alpha1.ProjectManagement, error)
	GetProjects(ctx context.Context, client versioned.Interface, name, secret, secretnamespace, page, pagesize string) (*v1alpha1.ProjectDataList, error)
}

// New builder method for projectmanagement.Handler
// to be used while injecting the api in the registry
func New(srv server.Server) (ws *restful.WebService, err error) {
	// handler
	handler := NewHandler(srv, NewProcessor())

	// middlewares
	clientFilter := decorator.ClientDecorator(srv)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(srv)

	// webservice routes
	ws = abdecorator.NewWebService(srv)
	ws.Doc("ProjectManagement related APIs").ApiVersion("v1").Path("/api/v1/projectmanagement")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	// adds some specific context to the logger
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "projectmanagement")))

	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("").
					Filter(clientFilter.DevOpsClientFilter).
					Doc(`List ProjectManagement instance`).
					To(handler.ListProjectManagement).
					Writes(v1alpha1.ProjectManagementList{}).
					Returns(http.StatusOK, "OK", ProjectManagementList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Create ProjectManagement instance").
				To(handler.CreateProjectManagement).
				Returns(http.StatusOK, "Create ProjectManagement Instance", v1alpha1.Jenkins{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Delete ProjectManagement instance").
				Param(ws.PathParameter("name", "name of the ProjectManagement")).
				To(handler.DeleteProjectManagement).
				Returns(http.StatusOK, "Delete ProjectManagement Complete", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Retrieve ProjectManagement instance").
				Param(ws.PathParameter("name", "name of the ProjectManagement")).
				To(handler.RetrieveProjectManagement).
				Returns(http.StatusOK, "Retrieve ProjectManagement instance", v1alpha1.ProjectManagement{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{name}").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("Update ProjectManagement instance").
				Param(restful.PathParameter("name", "ProjectManagement name to filter scope")).
				To(handler.UpdateProjectManagement).
				Returns(http.StatusOK, "Update ProjectManagement instance", v1alpha1.ProjectManagement{}),
		),
	)

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{name}/remoteprojects").
				Filter(clientFilter.DevOpsClientFilter).
				Doc("get ProjectManagement projects").
				Param(restful.PathParameter("name", "ProjectManagement name to filter scope")).
				Param(ws.QueryParameter("secretName", "Name of secret")).
				Param(ws.QueryParameter("namespace", "namespace of secret")).
				Param(ws.QueryParameter("page", "page")).
				Param(ws.QueryParameter("pageSize", "pageSize")).
				To(handler.GetProjects).
				Returns(http.StatusOK, "Get ProjectManagement projects", v1alpha1.ProjectDataList{}),
		),
	)

	ws.Filter(clientFilter.DevOpsClientFilter)
	return
}

// ListProjectManagement list projectmanagement instances
func (h Handler) ListProjectManagement(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	query := abcontext.Query(req.Request.Context())
	list, err := h.Processor.ListProjectManagement(req.Request.Context(), client, query)
	h.WriteResponse(list, err, req, res)
}

//DeleteProjectManagement Delete ProjectManagement instance
func (h Handler) DeleteProjectManagement(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	err := h.Processor.DeleteProjectManagement(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(struct{}{}, err, req, res)
}

//RetrieveProjectManagement Retrieve ProjectManagement instance
func (h Handler) RetrieveProjectManagement(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.RetrieveProjectManagement(req.Request.Context(), client, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//CreateProjectManagement create ProjectManagement instance
func (h Handler) CreateProjectManagement(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newProjectManagement := new(v1alpha1.ProjectManagement)

	if err := req.ReadEntity(newProjectManagement); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.CreateProjectManagement(req.Request.Context(), client, newProjectManagement)
	h.WriteResponse(data, err, req, res)
}

//UpdateJenkins update ProjectManagement instance
func (h Handler) UpdateProjectManagement(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	newProjectManagement := new(v1alpha1.ProjectManagement)

	if err := req.ReadEntity(newProjectManagement); err != nil {
		h.WriteResponse(nil, err, req, res)
		return
	}
	data, err := h.Processor.UpdateProjectManagement(req.Request.Context(), client, newProjectManagement, req.PathParameter("name"))
	h.WriteResponse(data, err, req, res)
}

//GetProject update ProjectManagement instance
func (h Handler) GetProjects(req *restful.Request, res *restful.Response) {
	client := localcontext.DevOpsClient(req.Request.Context())
	data, err := h.Processor.GetProjects(req.Request.Context(), client, req.PathParameter("name"), req.QueryParameter("secretName"),
		req.QueryParameter("namespace"), req.QueryParameter("page"), req.QueryParameter("pageSize"))

	h.WriteResponse(data, err, req, res)
}

// WriteResponse writes a response
func (h Handler) WriteResponse(data interface{}, err error, req *restful.Request, res *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(data)
}
