package projectmanagement

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"strconv"
)

// WARNING: this processor is an example. must be changed to the appropriate logic

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

var _ Processor = processor{}

// ListProjectManagement list projectmanagement instances
func (p processor) ListProjectManagement(ctx context.Context, client versioned.Interface, query *dataselect.Query) (data *ProjectManagementList, err error) {

	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list projectmanagement end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}

	// fetch data from list
	projectmanagementlist, err := client.DevopsV1alpha1().ProjectManagements().List(listOptions)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	data = &ProjectManagementList{
		Items:    make([]v1alpha1.ProjectManagement, 0),
		ListMeta: jenkins.ListMeta{TotalItems: 0},
	}
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(projectmanagementlist.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToProjectManagementSlice(result)
	data.ListMeta = jenkins.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

// DeleteProjectManagement delete projectmanagement instance
func (p processor) DeleteProjectManagement(ctx context.Context, client versioned.Interface, name string) (err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Delete ProjectManagement end", log.String("name", name), log.Err(err))
	}()
	err = client.DevopsV1alpha1().ProjectManagements().Delete(name, &metav1.DeleteOptions{})
	return
}

// RetrieveProjectManagement retrieve projectmanagement instance
func (p processor) RetrieveProjectManagement(ctx context.Context, client versioned.Interface, name string) (data *v1alpha1.ProjectManagement, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get projectmanagement end", log.String("name", name), log.Err(err))
	}()
	data, err = client.DevopsV1alpha1().ProjectManagements().Get(name, jenkins.GetOptionsInCache)
	return
}

//CreateProjectManagement create projectmanagement instance
func (p processor) CreateProjectManagement(ctx context.Context, client versioned.Interface, projectmanagement *v1alpha1.ProjectManagement) (data *v1alpha1.ProjectManagement, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Create projectmanagement end", log.Err(err))
	}()

	data, err = client.DevopsV1alpha1().ProjectManagements().Create(projectmanagement)
	return
}

//UpdateProjectManagement update a projectmanagement instance
func (p processor) UpdateProjectManagement(ctx context.Context, client versioned.Interface, projectmanagement *v1alpha1.ProjectManagement, name string) (data *v1alpha1.ProjectManagement, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Update ProjectManagement end", log.Err(err), log.String("name", name))
	}()
	oldProjectManagement := new(v1alpha1.ProjectManagement)
	oldProjectManagement, err = client.DevopsV1alpha1().ProjectManagements().Get(name, jenkins.GetOptionsInCache)

	if err != nil {
		return
	}

	oldProjectManagement.SetAnnotations(projectmanagement.GetAnnotations())
	oldProjectManagement.Spec = projectmanagement.Spec

	data, err = client.DevopsV1alpha1().ProjectManagements().Update(oldProjectManagement)
	return
}

// GetProjects get the projects in projectmanagement
func (p processor) GetProjects(ctx context.Context, client versioned.Interface, name, secret, secretnamespace, page, pagesize string) (data *v1alpha1.ProjectDataList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("Get ProjectManagement Project end", log.Err(err), log.String("name", name))
	}()

	if pagesize == "" || page == "" {
		pagesize = strconv.Itoa(200)
		page = strconv.Itoa(1)
	}

	listoption := v1alpha1.ListProjectOptions{
		SecretName: secret,
		Namespace:  secretnamespace,
		Page:       page,
		PageSize:   pagesize,
	}
	data, err = client.DevopsV1alpha1().ProjectManagements().ListProjects(name, listoption)
	if err != nil {
		return nil, err
	}
	return
}

// ConvertToProjectManagementSlice TODO: need to find a better way to convert items
// without having to do it manually
func ConvertToProjectManagementSlice(filtered []metav1.Object) (items []v1alpha1.ProjectManagement) {
	items = make([]v1alpha1.ProjectManagement, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.ProjectManagement); ok {
			cm.Kind = v1alpha1.ResourceKindProjectManagement
			items = append(items, *cm)
		}
	}
	return
}

func AuthorizeService(client versioned.Interface, pmName, secretName, namespace string) (*v1alpha1.CodeRepoServiceAuthorizeResponse, error) {
	opts := v1alpha1.CodeRepoServiceAuthorizeOptions{
		SecretName: secretName,
		Namespace:  namespace,
	}
	authorizeResponse, err := client.DevopsV1alpha1().ProjectManagements().Authorize(pmName, &opts)
	fmt.Printf("authorizeResponse: %#v, err:%#v", authorizeResponse, err)
	return authorizeResponse, err
}
