package projectmanagement_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/fake"
	"alauda.io/devops-apiserver/pkg/restapi/handler/projectmanagement"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func InjectLogger() context.Context {
	return abcontext.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Processor.ProjectMangement", func() {
	var (
		processor projectmanagement.Processor
		ctx       context.Context
		client    *fake.Clientset
		query     *dataselect.Query

		list *projectmanagement.ProjectManagementList
		err  error
	)

	BeforeEach(func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 10,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query = dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		client = fake.NewSimpleClientset()
		processor = projectmanagement.NewProcessor()
	})

	JustBeforeEach(func() {
		Expect(ctx).ToNot(BeNil())
	})

	It("should return pipeline", func() {
		client = fake.NewSimpleClientset(
			GetProjecetmanagementList("pipelinename"),
		)
		list, err = processor.ListProjectManagement(ctx, client, query)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(list).ToNot(BeNil(), "should return a list")
		Expect(list.Items).ToNot(BeNil(), "should return a slice of items")
		Expect(list.Items).To(HaveLen(2), "should not have any items")
	})

})

func GetProjecetmanagementList(name string) *v1alpha1.ProjectManagementList {
	pipelineconfiglist := &v1alpha1.ProjectManagementList{
		TypeMeta: metav1.TypeMeta{
			Kind: "projectmanagement",
		},

		Items: []v1alpha1.ProjectManagement{
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
				},
				Spec: v1alpha1.ProjectManagementSpec{},
			},
			{
				ObjectMeta: metav1.ObjectMeta{
					Name: name + "1",
				},
				Spec: v1alpha1.ProjectManagementSpec{},
			},
		},
	}
	return pipelineconfiglist
}
