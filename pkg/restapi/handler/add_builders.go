package handler

import (
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/application"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/canI"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/configmap"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/configuration"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/container"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/daemonset"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/deployment"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/horizontalpodautoscaler"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/node"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/persistentvolumeclaim"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/pod"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/secret"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/statefulset"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/statistics"
	"alauda.io/devops-apiserver/pkg/restapi/handler/AppCore/storageclass"
	"alauda.io/devops-apiserver/pkg/restapi/handler/artifactregistry"
	"alauda.io/devops-apiserver/pkg/restapi/handler/artifactregistrybinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/artifactregistrymanager"
	"alauda.io/devops-apiserver/pkg/restapi/handler/checktoolchainaccess"
	"alauda.io/devops-apiserver/pkg/restapi/handler/clusterpipelinetasktemplate"
	"alauda.io/devops-apiserver/pkg/restapi/handler/clusterpipelinetemplate"
	"alauda.io/devops-apiserver/pkg/restapi/handler/clusterpipelinetemplatesync"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codequalitybinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codequalityproject"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codequalitytool"
	"alauda.io/devops-apiserver/pkg/restapi/handler/coderepobinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/codereposervice"
	"alauda.io/devops-apiserver/pkg/restapi/handler/coderepository"
	"alauda.io/devops-apiserver/pkg/restapi/handler/commonresourcehandler"
	"alauda.io/devops-apiserver/pkg/restapi/handler/imageregistry"
	"alauda.io/devops-apiserver/pkg/restapi/handler/imageregistrybinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/imagerepository"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkins"
	"alauda.io/devops-apiserver/pkg/restapi/handler/jenkinsbinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipeline"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelineconfig"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelinetasktemplate"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelinetemplate"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelinetemplatecategories"
	"alauda.io/devops-apiserver/pkg/restapi/handler/pipelinetemplatesync"
	"alauda.io/devops-apiserver/pkg/restapi/handler/projectmanagement"
	"alauda.io/devops-apiserver/pkg/restapi/handler/projectmanagementbinding"
	"alauda.io/devops-apiserver/pkg/restapi/handler/settings"
	"alauda.io/devops-apiserver/pkg/restapi/handler/toolchain"
	"bitbucket.org/mathildetech/alauda-backend/pkg/registry"
)

func init() {
	registry.AddBuilder(jenkins.New)
	registry.AddBuilder(jenkinsbinding.New)
	registry.AddBuilder(pipeline.New)
	registry.AddBuilder(codereposervice.New)
	registry.AddBuilder(imageregistry.New)
	registry.AddBuilder(imagerepository.New)
	registry.AddBuilder(projectmanagement.New)
	registry.AddBuilder(codequalitytool.New)
	registry.AddBuilder(clusterpipelinetemplatesync.New)
	registry.AddBuilder(clusterpipelinetemplate.New)
	registry.AddBuilder(clusterpipelinetasktemplate.New)
	registry.AddBuilder(pipelinetemplate.New)
	registry.AddBuilder(pipelinetasktemplate.New)
	registry.AddBuilder(pipelinetemplatesync.New)
	registry.AddBuilder(pipelineconfig.New)
	registry.AddBuilder(projectmanagementbinding.New)
	registry.AddBuilder(codequalitybinding.New)
	registry.AddBuilder(imageregistrybinding.New)
	registry.AddBuilder(coderepository.New)
	registry.AddBuilder(coderepobinding.New)
	registry.AddBuilder(pipelinetemplatecategories.New)
	registry.AddBuilder(toolchain.New)
	registry.AddBuilder(application.New)
	registry.AddBuilder(deployment.New)
	registry.AddBuilder(configmap.New)
	registry.AddBuilder(horizontalpodautoscaler.New)
	registry.AddBuilder(persistentvolumeclaim.New)
	registry.AddBuilder(secret.New)
	registry.AddBuilder(secret.NewAppSecret)
	registry.AddBuilder(storageclass.New)
	registry.AddBuilder(configuration.New)
	registry.AddBuilder(daemonset.New)
	registry.AddBuilder(statefulset.New)
	registry.AddBuilder(canI.New)
	registry.AddBuilder(statistics.New)
	registry.AddBuilder(settings.New)
	registry.AddBuilder(codequalityproject.New)
	registry.AddBuilder(container.New)
	registry.AddBuilder(pod.New)
	registry.AddBuilder(artifactregistry.New)
	registry.AddBuilder(artifactregistrymanager.New)
	registry.AddBuilder(artifactregistrybinding.New)
	registry.AddBuilder(commonresourcehandler.New)
	registry.AddBuilder(checktoolchainaccess.New)
	registry.AddBuilder(node.New)
	registry.AddHttpHandler("/api/sockjs/", pod.CreateAttachHandler("/api/sockjs"))

}
