// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package secret

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"strings"

	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	api "k8s.io/api/core/v1"
)

// The code below allows to perform complex data section on []api.Secret

type SecretCell struct {
	secret *api.Secret
	devops.AnnotationProvider
}

func (self SecretCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case common.NameProperty:
		return dataselect.StdCaseInSensitiveComparableString(self.secret.ObjectMeta.Name)
	case common.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.secret.ObjectMeta.CreationTimestamp.Time)
	case common.NamespaceProperty:
		return dataselect.StdComparableString(self.secret.ObjectMeta.Namespace)
	case common.DisplayNameProperty:
		if len(self.secret.ObjectMeta.Annotations) > 0 {
			if self.secret.ObjectMeta.Annotations[self.AnnotationsKeyDisplayName()] != "" {
				return dataselect.StdLowerComparableString(self.secret.ObjectMeta.Annotations[self.AnnotationsKeyDisplayName()])
			}
		}
		return dataselect.StdLowerComparableString(self.secret.ObjectMeta.Name)
	case common.SecretTypeProperty:
		return dataselect.StdComparableStringIn(self.secret.Type)
	case common.ProductNameProperty:
		if len(self.secret.ObjectMeta.Annotations) > 0 {
			if self.secret.ObjectMeta.Annotations[self.AnnotationsKeyProduct()] != "" {
				return dataselect.StdComparableString(self.secret.ObjectMeta.Annotations[self.AnnotationsKeyProduct()])
			}
		}
	case common.LabelProperty:
		if len(self.secret.ObjectMeta.Labels) > 0 {
			values := []string{}
			for k, v := range self.secret.ObjectMeta.Labels {
				values = append(values, k+":"+v)
			}
			return dataselect.StdComparableLabel(strings.Join(values, ","))
		}
	default:
	}
	// if name is not supported then just return a constant dummy value, sort will have no effect.
	return nil
}

func toCells(std []api.Secret, provider devops.AnnotationProvider) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = SecretCell{&std[i], provider}
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []api.Secret {
	std := make([]api.Secret, len(cells))
	for i := range std {
		secret := cells[i].(SecretCell).secret
		std[i] = *secret
	}
	return std
}
