// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package secret

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"log"

	appCore "alauda.io/app-core/pkg/app"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/restapi/api"
	//"alauda.io/diablo/src/backend/errors"
	"alauda.io/devops-apiserver/pkg/restapi/errors"
	"alauda.io/devops-apiserver/pkg/restapi/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
)

// SecretSpec is a common interface for the specification of different secrets.
type SecretSpec interface {
	GetName() string
	GetType() v1.SecretType
	GetNamespace() string
	GetData() map[string][]byte
}

// ImagePullSecretSpec is a specification of an image pull secret implements SecretSpec
type ImagePullSecretSpec struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`

	// The value of the .dockercfg property. It must be Base64 encoded.
	Data []byte `json:"data"`
}

// GetName returns the name of the ImagePullSecret
func (spec *ImagePullSecretSpec) GetName() string {
	return spec.Name
}

// GetType returns the type of the ImagePullSecret, which is always api.SecretTypeDockercfg
func (spec *ImagePullSecretSpec) GetType() v1.SecretType {
	return v1.SecretTypeDockercfg
}

// GetNamespace returns the namespace of the ImagePullSecret
func (spec *ImagePullSecretSpec) GetNamespace() string {
	return spec.Namespace
}

// GetData returns the data the secret carries, it is a single key-value pair
func (spec *ImagePullSecretSpec) GetData() map[string][]byte {
	return map[string][]byte{v1.DockerConfigKey: spec.Data}
}

// GenericSecretSpec is a specification of an generic secret spec
type GenericSecretSpec struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`

	// Data property to store values
	Data map[string][]byte `json:"data"`
	Type v1.SecretType     `json:"type"`
}

// GetName returns the name of the ImagePullSecret
func (spec *GenericSecretSpec) GetName() string {
	return spec.Name
}

// GetType returns the type of the ImagePullSecret, which is always api.SecretTypeDockercfg
func (spec *GenericSecretSpec) GetType() v1.SecretType {
	return spec.Type
}

// GetNamespace returns the namespace of the ImagePullSecret
func (spec *GenericSecretSpec) GetNamespace() string {
	return spec.Namespace
}

// GetData returns the data the secret carries, it is a single key-value pair
func (spec *GenericSecretSpec) GetData() map[string][]byte {
	return spec.Data
}

// Secret is a single secret returned to the frontend.
type Secret struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`
	Type       v1.SecretType  `json:"type"`
	Keys       []string       `json:"keys"`
	AppName    string         `json:"appName"`
}

func (ing Secret) GetObjectMeta() api.ObjectMeta {
	return ing.ObjectMeta
}

// SecretsList is a response structure for a queried secrets list.
type SecretList struct {
	api.ListMeta `json:"listMeta"`

	// Unordered list of Secrets.
	Secrets []Secret `json:"secrets"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

var supportedSecretTypes = []v1.SecretType{v1.SecretTypeBasicAuth, v1.SecretTypeDockerConfigJson, v1.SecretTypeOpaque, v1.SecretTypeTLS, v1.SecretTypeSSHAuth, devopsv1alpha1.SecretTypeOAuth2}

// GetSecretList returns all secrets in the given namespace.
func GetSecretList(client kubernetes.Interface, appCoreClient *appCore.ApplicationClient, namespace *common.NamespaceQuery,
	dsQuery *dataselect.Query, includePublic bool, credentialsNamespace string, systemNamespace string, provider devops.AnnotationProvider) (*SecretList, error) {
	curNamespace := namespace.ToRequestParam()
	log.Printf("Getting list of secrets in namespace %s", curNamespace)

	secretList, err := client.CoreV1().Secrets(curNamespace).List(api.ListEverything)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	if curNamespace != "" && includePublic {
		publicSecretList, err := client.CoreV1().Secrets(credentialsNamespace).List(api.ListEverything)
		nonCriticalErrors, criticalError = errors.HandleError(err)
		if criticalError != nil {
			return nil, criticalError
		}

		if publicSecretList != nil && len(publicSecretList.Items) > 0 {
			secretList.Items = append(secretList.Items, publicSecretList.Items...)
		}
	}

	// secret type list
	items := func(secrets []v1.Secret) []v1.Secret {
		filtered := make([]v1.Secret, 0, len(secrets))
		for _, s := range secrets {

			if len(s.ObjectMeta.Labels) > 0 && s.ObjectMeta.Labels[provider.AnnotationGeneratorName()] != "" {
				continue
			}
			for _, t := range supportedSecretTypes {
				if s.Type == t && !isHiddenNamespace(s.Namespace, systemNamespace) {
					filtered = append(filtered, s)
					break
				}
			}
		}
		return filtered
	}(secretList.Items)

	return toSecretList(items, nonCriticalErrors, dsQuery, appCoreClient, credentialsNamespace, provider), nil
}

var hiddenNamespaces = []string{devopsv1alpha1.NamespaceKubeSystem, devopsv1alpha1.NamespaceDefault}

func isHiddenNamespace(namespace string, systemNamespace string) bool {
	for _, n := range hiddenNamespaces {
		if n == namespace {
			return true
		}
	}
	if namespace == systemNamespace {
		return true
	}
	return false
}

func toSecret(secret *v1.Secret, appName string, credentialsNamespace string) *Secret {
	keys := make([]string, 0, len(secret.Data))
	for k := range secret.Data {
		keys = append(keys, k)
	}
	result := &Secret{
		ObjectMeta: api.NewObjectMeta(secret.ObjectMeta),
		TypeMeta:   api.NewTypeMeta(api.ResourceKindSecret),
		Type:       secret.Type,
		Keys:       keys,
		AppName:    appName,
	}
	if result.ObjectMeta.Annotations == nil {
		result.ObjectMeta.Annotations = make(map[string]string, 0)
	}

	if secret.GetNamespace() == credentialsNamespace {

		result.ObjectMeta.Annotations[common.GetDevopsGlobal(common.LocalBaseDomain(), devopsv1alpha1.LabelDevopsAlaudaIOGlobalKey)] = devopsv1alpha1.TrueString
	}
	return result
}

func toSecretList(secrets []v1.Secret, nonCriticalErrors []error, dsQuery *dataselect.Query, appCoreClient *appCore.ApplicationClient, credentialsNamespace string, provider devops.AnnotationProvider) *SecretList {
	newSecretList := &SecretList{
		ListMeta: api.ListMeta{TotalItems: len(secrets)},
		Secrets:  make([]Secret, 0),
		Errors:   nonCriticalErrors,
	}

	secretCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(secrets, provider), dsQuery)
	secrets = fromCells(secretCells)
	newSecretList.ListMeta = api.ListMeta{TotalItems: filteredTotal}

	for i := range secrets {
		setTypeMeta(&secrets[i])
	}

	appNameList := common.GetAppNameListFromAppcore(appCoreClient, secrets, filteredTotal)
	if appNameList != nil {
		for i := range secrets {
			newSecretList.Secrets = append(newSecretList.Secrets, *toSecret(&secrets[i], appNameList[i], credentialsNamespace))
		}
	}

	return newSecretList
}
