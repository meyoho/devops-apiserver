package secret

import (
	//"alauda.io/diablo/src/backend/resource/common"
	//"alauda.io/diablo/src/backend/resource/dataselect"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func setTypeMeta(cm *v1.Secret) {
	cm.TypeMeta.SetGroupVersionKind(schema.GroupVersionKind{
		Group:   "",
		Version: "v1",
		Kind:    "Secret",
	})
}
