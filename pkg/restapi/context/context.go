package context

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"bitbucket.org/mathildetech/log"
	"context"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/app-core/pkg/app"
	"k8s.io/client-go/rest"

	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
)

// Context simple pointer to context.Context interface
type Context context.Context

type contextKey struct{ Name string }

// ExtraOptions extra configuration for the server
type ExtraOptions struct {
	// Place you custom config here.
	AnnotationProvider   devops.AnnotationProvider
	SystemNamespace      string
	CredentialsNamespace string
}

var (
	devopsClientKey = contextKey{Name: "devops.Interface"}
	appclientKey    = contextKey{Name: "appcore.Interface"}
	configKey       = contextKey{Name: "config"}
	extraKey        = contextKey{Name: "extra"}
	cacheClientKey  = contextKey{Name: "cacheClient"}
)

// WithDevOpsClient inserts a client into the context
func WithDevOpsClient(ctx context.Context, client versioned.Interface) context.Context {
	return context.WithValue(ctx, devopsClientKey, client)
}

func WithAppClient(ctx context.Context, client *app.ApplicationClient) context.Context {
	return context.WithValue(ctx, appclientKey, client)
}

func WithConfig(ctx context.Context, config *rest.Config) context.Context {
	return context.WithValue(ctx, configKey, config)
}

func WithExtraConfig(ctx context.Context, extra *ExtraOptions) context.Context {
	return context.WithValue(ctx, extraKey, extra)
}

func WithCacheClient(ctx context.Context, cacheClient client.Client) context.Context {
	return context.WithValue(ctx, cacheClientKey, cacheClient)
}

// DevOpsClient fetches a client from a context if existing.
// will return nil if the context doesnot have the client
func DevOpsClient(ctx context.Context) versioned.Interface {
	val := ctx.Value(devopsClientKey)
	if val != nil {
		return val.(versioned.Interface)
	}
	log.Errorf("Context can't get devopsclient")
	return nil
}

func AppClient(ctx context.Context) *app.ApplicationClient {
	val := ctx.Value(appclientKey)
	if val != nil {
		return val.(*app.ApplicationClient)
	}
	log.Errorf("Context can't get appclient ")
	return nil
}

func ConfigClient(ctx context.Context) *rest.Config {
	val := ctx.Value(configKey)
	if val != nil {
		return val.(*rest.Config)
	}
	log.Errorf("Context can't get configclient")
	return nil
}

func ExtraConfig(ctx context.Context) ExtraOptions {
	point := ctx.Value(extraKey)
	if point != nil {
		val := point.(*ExtraOptions)
		return *val
	}
	log.Errorf("Context can't get extraoption")
	return ExtraOptions{}
}

func CacheClient(ctx context.Context) client.Client {
	val := ctx.Value(cacheClientKey)
	if val != nil {
		return val.(client.Client)
	}
	log.Errorf("Context can't get cacheclient")
	return nil
}
