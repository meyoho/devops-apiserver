/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package devopsinitializer_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"testing"
	"time"

	"alauda.io/devops-apiserver/pkg/dependency"

	"alauda.io/devops-apiserver/pkg/admission/devopsinitializer"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/fake"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	"k8s.io/apiserver/pkg/admission"
	k8sinformers "k8s.io/client-go/informers"

	"k8s.io/client-go/kubernetes"
	k8sfake "k8s.io/client-go/kubernetes/fake"
)

// TestWantsInternalDevopsInformerFactory ensures that the informer factory is injected
// when the WantsInternalDevopsInformerFactory interface is implemented by a plugin.
func TestWantsInternalDevopsInformerFactory(t *testing.T) {
	cs := &fake.Clientset{}
	sf := informers.NewSharedInformerFactory(cs, time.Duration(1)*time.Second)
	k8sclient := &k8sfake.Clientset{}
	kf := k8sinformers.NewSharedInformerFactory(k8sclient, 1*time.Second)
	provider := devops.AnnotationProvider{
		BaseDomain: devops.UsedBaseDomain,
	}
	target := devopsinitializer.New(sf, kf, cs, k8sclient, dependency.DefaultManager, provider)
	wantInformerFactory := &wantInternalInformerFactory{}
	target.Initialize(wantInformerFactory)
	if wantInformerFactory.sf != sf {
		t.Errorf("expected informer factory to be initialized")
	}
	if wantInformerFactory.kf != kf {
		t.Errorf("expected k8s informer factory to be initialized")
	}
}

// wantInternalInformerFactory is a test stub that fulfills the WantsInternalDevopsInformerFactory interface
type wantInternalInformerFactory struct {
	sf informers.SharedInformerFactory
	kf k8sinformers.SharedInformerFactory
	cs internalversion.Interface
	kc kubernetes.Interface
}

func (self *wantInternalInformerFactory) SetInternalDevopsInformerFactory(sf informers.SharedInformerFactory) {
	self.sf = sf
}
func (self *wantInternalInformerFactory) SetKubernetesInformerFactory(kf k8sinformers.SharedInformerFactory) {
	self.kf = kf
}
func (self *wantInternalInformerFactory) SetDevopsClient(c internalversion.Interface) {
	self.cs = c
}
func (self *wantInternalInformerFactory) SetInternalKubeClientSet(c kubernetes.Interface) {
	self.kc = c
}

func (self *wantInternalInformerFactory) SetProvider(provider devops.AnnotationProvider) {
}

func (self *wantInternalInformerFactory) Admit(a admission.Attributes) error      { return nil }
func (self *wantInternalInformerFactory) Handles(o admission.Operation) bool      { return false }
func (self *wantInternalInformerFactory) ValidateInitialization() error           { return nil }
func (self *wantInternalInformerFactory) SetDependencyManager(dependency.Manager) {}

var _ admission.Interface = &wantInternalInformerFactory{}
var _ devopsinitializer.WantsInternalDevopsInformerFactory = &wantInternalInformerFactory{}
