/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package devopsinitializer

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	"k8s.io/apiserver/pkg/admission"
	k8sinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
)

// WantsInternalDevopsInformerFactory defines a function which sets InformerFactory for admission plugins that need it
type WantsInternalDevopsInformerFactory interface {
	SetInternalDevopsInformerFactory(informers.SharedInformerFactory)
	SetKubernetesInformerFactory(k8sinformers.SharedInformerFactory)
	SetInternalKubeClientSet(kubernetes.Interface)
	SetDevopsClient(c devopsclient.Interface)
	SetDependencyManager(m dependency.Manager)
	SetProvider(provider devops.AnnotationProvider)
	admission.InitializationValidator
}
