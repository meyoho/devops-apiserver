package admission

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/banproject"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components"
	"k8s.io/apiserver/pkg/admission"
)

// RegisterAllPlugins register all devops projects
func RegisterAllPlugins(plugins *admission.Plugins) {
	banproject.Register(plugins)
	components.Register(plugins)
}
