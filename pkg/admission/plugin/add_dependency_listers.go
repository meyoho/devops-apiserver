package plugin

import (
	"context"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	listers "alauda.io/devops-apiserver/pkg/client/listers/devops/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	metainternalversion "k8s.io/apimachinery/pkg/apis/meta/internalversion"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	k8sinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

func AddListers(manager dependency.Manager, informers informers.SharedInformerFactory, k8sinformers k8sinformers.SharedInformerFactory, k kubernetes.Interface) {
	addJenkinsList(manager, informers.Devops().InternalVersion().Jenkinses().Lister())
	addJenkinsBindingList(manager, informers.Devops().InternalVersion().JenkinsBindings().Lister())
	addPipelineConfigList(manager, informers.Devops().InternalVersion().PipelineConfigs().Lister())
	addPipelineTemplateList(manager, informers.Devops().InternalVersion().PipelineTemplates().Lister())
	addClusterPipelineTemplateList(manager, informers.Devops().InternalVersion().ClusterPipelineTemplates().Lister())

	addCodeRepoServiceList(manager, informers.Devops().InternalVersion().CodeRepoServices().Lister())
	addCodeRepoBindingList(manager, informers.Devops().InternalVersion().CodeRepoBindings().Lister())

	addImageRegistryList(manager, informers.Devops().InternalVersion().ImageRegistries().Lister())
	addImageRegistryBindingList(manager, informers.Devops().InternalVersion().ImageRegistryBindings().Lister())

	addProjectManagementList(manager, informers.Devops().InternalVersion().ProjectManagements().Lister())
	addCodeQualityProjectList(manager, informers.Devops().InternalVersion().CodeQualityProjects().Lister())

	addTestToolList(manager, informers.Devops().InternalVersion().TestTools().Lister())

	addCodeQualityToolList(manager, informers.Devops().InternalVersion().CodeQualityTools().Lister())
	addDocumentManagementList(manager, informers.Devops().InternalVersion().DocumentManagements().Lister())
	addDocumentManagementBindingList(manager, informers.Devops().InternalVersion().DocumentManagementBindings().Lister())
	addCodeQualityBindingList(manager, informers.Devops().InternalVersion().CodeQualityBindings().Lister())
	addArtifactRegistryList(manager, informers.Devops().InternalVersion().ArtifactRegistries().Lister())
	addArtifactRegistryBindingList(manager, informers.Devops().InternalVersion().ArtifactRegistryBindings().Lister())
	addCodeQualityProjectList(manager, informers.Devops().InternalVersion().CodeQualityProjects().Lister())
	addPipelineList(manager, informers.Devops().InternalVersion().Pipelines().Lister())

	addSecretList(manager, k)
	addNamespacesList(manager, k8sinformers.Core().V1().Namespaces().Lister())
}

func addJenkinsList(mgr dependency.Manager, lister listers.JenkinsLister) {
	mgr.Kind(devops.TypeJenkins).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		return lister.Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		var slice []*devops.Jenkins
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.List(labelSelector)
		if err == nil {
			targetlist := &devops.JenkinsList{
				Items: make([]devops.Jenkins, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addJenkinsBindingList(mgr dependency.Manager, lister listers.JenkinsBindingLister) {
	mgr.Kind(devops.TypeJenkinsBinding).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.JenkinsBindings(namespace).Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var slice []*devops.JenkinsBinding
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.JenkinsBindings(namespace).List(labelSelector)
		if err == nil {
			targetlist := &devops.JenkinsBindingList{
				Items: make([]devops.JenkinsBinding, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addSecretList(mgr dependency.Manager, lister kubernetes.Interface) {
	mgr.Kind(devops.TypeSecret).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.CoreV1().Secrets(namespace).Get(name, metav1.GetOptions{ResourceVersion: "0"})
	})
}

func addNamespacesList(mgr dependency.Manager, lister corev1listers.NamespaceLister) {
	mgr.Kind(devops.TypeNamespace).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		return lister.Get(name)
	})
}

func addArtifactRegistryBindingList(mgr dependency.Manager, lister listers.ArtifactRegistryBindingLister) {
	mgr.Kind(devops.TypeArtifactRegistryBinding).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.ArtifactRegistryBindings(namespace).Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var slice []*devops.ArtifactRegistryBinding
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.ArtifactRegistryBindings(namespace).List(labelSelector)
		if err == nil {
			targetlist := &devops.ArtifactRegistryBindingList{
				Items: make([]devops.ArtifactRegistryBinding, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addArtifactRegistryList(mgr dependency.Manager, lister listers.ArtifactRegistryLister) {
	mgr.Kind(devops.TypeArtifactRegistry).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		return lister.Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		var slice []*devops.ArtifactRegistry
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.List(labelSelector)
		if err == nil {
			targetlist := &devops.ArtifactRegistryList{
				Items: make([]devops.ArtifactRegistry, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}
func addPipelineConfigList(mgr dependency.Manager, lister listers.PipelineConfigLister) {
	mgr.Kind(devops.TypePipelineConfig).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.PipelineConfigs(namespace).Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var slice []*devops.PipelineConfig
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.PipelineConfigs(namespace).List(labelSelector)
		if err == nil {
			targetlist := &devops.PipelineConfigList{
				Items: make([]devops.PipelineConfig, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addPipelineList(mgr dependency.Manager, lister listers.PipelineLister) {
	mgr.Kind(devops.TypePipeline).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.Pipelines(namespace).Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var slice []*devops.Pipeline
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.Pipelines(namespace).List(labelSelector)
		if err == nil {
			targetlist := &devops.PipelineList{
				Items: make([]devops.Pipeline, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addPipelineTemplateList(mgr dependency.Manager, lister listers.PipelineTemplateLister) {
	mgr.Kind(devops.TypePipelineTemplate).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.PipelineTemplates(namespace).Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var slice []*devops.PipelineTemplate
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.PipelineTemplates(namespace).List(labelSelector)
		if err == nil {
			targetlist := &devops.PipelineTemplateList{
				Items: make([]devops.PipelineTemplate, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addClusterPipelineTemplateList(mgr dependency.Manager, lister listers.ClusterPipelineTemplateLister) {
	mgr.Kind(devops.TypeClusterPipelineTemplate).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		return lister.Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		var slice []*devops.ClusterPipelineTemplate
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.List(labelSelector)
		if err == nil {
			targetlist := &devops.ClusterPipelineTemplateList{
				Items: make([]devops.ClusterPipelineTemplate, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addCodeRepoServiceList(mgr dependency.Manager, lister listers.CodeRepoServiceLister) {
	mgr.Kind(devops.TypeCodeRepoService).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		return lister.Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		var slice []*devops.CodeRepoService
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.List(labelSelector)
		if err == nil {
			targetlist := &devops.CodeRepoServiceList{
				Items: make([]devops.CodeRepoService, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addCodeRepoBindingList(mgr dependency.Manager, lister listers.CodeRepoBindingLister) {
	mgr.Kind(devops.TypeCodeRepoBinding).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.CodeRepoBindings(namespace).Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var slice []*devops.CodeRepoBinding
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.CodeRepoBindings(namespace).List(labelSelector)
		if err == nil {
			targetlist := &devops.CodeRepoBindingList{
				Items: make([]devops.CodeRepoBinding, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addImageRegistryList(mgr dependency.Manager, lister listers.ImageRegistryLister) {
	mgr.Kind(devops.TypeImageRegistry).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		return lister.Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		var slice []*devops.ImageRegistry
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.List(labelSelector)
		if err == nil {
			targetlist := &devops.ImageRegistryList{
				Items: make([]devops.ImageRegistry, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addImageRegistryBindingList(mgr dependency.Manager, lister listers.ImageRegistryBindingLister) {
	mgr.Kind(devops.TypeImageRegistryBinding).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.ImageRegistryBindings(namespace).Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var slice []*devops.ImageRegistryBinding
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.ImageRegistryBindings(namespace).List(labelSelector)
		if err == nil {
			targetlist := &devops.ImageRegistryBindingList{
				Items: make([]devops.ImageRegistryBinding, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addImageRepositoryList(mgr dependency.Manager, lister listers.ImageRepositoryLister) {
	mgr.Kind(devops.TypeImageRepository).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.ImageRepositories(namespace).Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var slice []*devops.ImageRepository
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.ImageRepositories(namespace).List(labelSelector)
		if err == nil {
			targetlist := &devops.ImageRepositoryList{
				Items: make([]devops.ImageRepository, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addProjectManagementList(mgr dependency.Manager, lister listers.ProjectManagementLister) {
	mgr.Kind(devops.TypeProjectManagement).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		return lister.Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		var slice []*devops.ProjectManagement
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.List(labelSelector)
		if err == nil {
			targetlist := &devops.ProjectManagementList{
				Items: make([]devops.ProjectManagement, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addTestToolList(mgr dependency.Manager, lister listers.TestToolLister) {
	mgr.Kind(devops.TypeTestTool).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		return lister.Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		var slice []*devops.TestTool
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.List(labelSelector)
		if err == nil {
			targetlist := &devops.TestToolList{
				Items: make([]devops.TestTool, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addCodeQualityToolList(mgr dependency.Manager, lister listers.CodeQualityToolLister) {
	mgr.Kind(devops.TypeCodeQualityTool).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		return lister.Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		var slice []*devops.CodeQualityTool
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.List(labelSelector)
		if err == nil {
			targetlist := &devops.CodeQualityToolList{
				Items: make([]devops.CodeQualityTool, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addCodeQualityBindingList(mgr dependency.Manager, lister listers.CodeQualityBindingLister) {
	mgr.Kind(devops.TypeCodeQualityBinding).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.CodeQualityBindings(namespace).Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var slice []*devops.CodeQualityBinding
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.CodeQualityBindings(namespace).List(labelSelector)
		if err == nil {
			targetlist := &devops.CodeQualityBindingList{
				Items: make([]devops.CodeQualityBinding, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addCodeQualityProjectList(mgr dependency.Manager, lister listers.CodeQualityProjectLister) {
	mgr.Kind(devops.TypeCodeQualityProject).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.CodeQualityProjects(namespace).Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var slice []*devops.CodeQualityProject
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.CodeQualityProjects(namespace).List(labelSelector)
		if err == nil {
			targetlist := &devops.CodeQualityProjectList{
				Items: make([]devops.CodeQualityProject, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addDocumentManagementList(mgr dependency.Manager, lister listers.DocumentManagementLister) {
	mgr.Kind(devops.TypeDocumentManagement).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		return lister.Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		var slice []*devops.DocumentManagement
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.List(labelSelector)
		if err == nil {
			targetlist := &devops.DocumentManagementList{
				Items: make([]devops.DocumentManagement, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}

func addDocumentManagementBindingList(mgr dependency.Manager, lister listers.DocumentManagementBindingLister) {
	mgr.Kind(devops.TypeDocumentManagementBinding).UnshiftGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return lister.DocumentManagementBindings(namespace).Get(name)
	}).UnshiftListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var slice []*devops.DocumentManagementBinding
		var labelSelector labels.Selector
		if opts != nil && opts.LabelSelector != nil {
			labelSelector = opts.LabelSelector
		} else {
			labelSelector = labels.Everything()
		}
		slice, err = lister.DocumentManagementBindings(namespace).List(labelSelector)
		if err == nil {
			targetlist := &devops.DocumentManagementBindingList{
				Items: make([]devops.DocumentManagementBinding, 0, len(slice)),
			}
			for _, s := range slice {
				targetlist.Items = append(targetlist.Items, *s)
			}
			list = targetlist
		}
		return
	})
}
