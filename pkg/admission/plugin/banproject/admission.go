package banproject

import (
	"io"

	"alauda.io/devops-apiserver/pkg/admission/devopsinitializer"
	"alauda.io/devops-apiserver/pkg/admission/plugin"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	"k8s.io/apiserver/pkg/admission"
	k8sinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
)

// PluginName plugin name
// must be unique in the API server
const PluginName = "BanProject"

// Register registers a plugin
func Register(plugins *admission.Plugins) {
	plugins.Register(PluginName, func(config io.Reader) (admission.Interface, error) {
		plugin.ReadConfigInPlugin(config, PluginName)

		return New([]string{
			devops.NamespaceKubeSystem,
			devops.NamespaceDefault,
		})
	})
}

// DisallowProject type to control validation logic
type DisallowProject struct {
	*admission.Handler
	blacklist []string
}

var _ = devopsinitializer.WantsInternalDevopsInformerFactory(&DisallowProject{})

// Admit ensures that the object in-flight is of kind Project.
// In addition checks that the Name is not on the banned list.
func (d *DisallowProject) Admit(_ admission.Attributes) error {
	return nil
}

// SetInternalDevopsInformerFactory gets Lister from SharedInformerFactory.
func (d *DisallowProject) SetInternalDevopsInformerFactory(f informers.SharedInformerFactory) {
	glog.V(3).Infof("got internal informer factory: %v", f)
}

// SetKubernetesInformerFactory set kubernetes informer factory.
func (d *DisallowProject) SetKubernetesInformerFactory(f k8sinformers.SharedInformerFactory) {
	glog.V(3).Infof("got kubernetes informer factory: %v", f)
}

// SetDevopsClient set Devops client
func (d *DisallowProject) SetDevopsClient(c devopsclient.Interface) {
	glog.V(3).Infof("got devops client: %v", c)
}

// SetInternalKubeClientSet set kubernetes client
func (d *DisallowProject) SetInternalKubeClientSet(c kubernetes.Interface) {
	glog.V(3).Infof("got kubernetes client: %v", c)
}

// ValidateInitialization checks whether the plugin was correctly initialized.
func (d *DisallowProject) ValidateInitialization() error {
	return nil
}

func (d *DisallowProject) SetDependencyManager(m dependency.Manager) {
}

func (d *DisallowProject) SetProvider(provider devops.AnnotationProvider) {

}

// New creates a new ban flunder admission plugin
func New(blacklist []string) (*DisallowProject, error) {
	return &DisallowProject{
		Handler:   admission.NewHandler(admission.Create),
		blacklist: blacklist,
	}, nil
}
