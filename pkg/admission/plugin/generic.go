package plugin

import (
	"io"
	"io/ioutil"
	"reflect"

	glog "k8s.io/klog"
)

// ReadConfigInPlugin read a configuration about a plugin
func ReadConfigInPlugin(config io.Reader, pluginName string) (configBytes []byte, err error) {
	if config == nil || reflect.ValueOf(config).IsNil() {
		glog.V(5).Infof("Config in plugin %s is nil", pluginName)
	} else {
		configBytes, err := ioutil.ReadAll(config)
		if err != nil {
			glog.V(5).Infof("Read config in plugin %s, err: %v", pluginName, err)
			return nil, err
		}
		glog.V(5).Infof("Config in plugin %s is %s", pluginName, configBytes)
	}
	return
}
