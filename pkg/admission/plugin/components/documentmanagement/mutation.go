package documentmanagement

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

var _ base.Middleware = &DocumentManagementBindingMutation{}

type DocumentManagementBindingMutation struct {
}

func (c *DocumentManagementBindingMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	binding := attr.GetObject().(*devops.DocumentManagementBinding)
	documentManagement := &devops.DocumentManagement{}
	secret := &corev1.Secret{}
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), binding.Namespace)
	dependency := d.GetDependencyManager().GetWithObject(ctx, devops.TypeDocumentManagementBinding, binding)
	if err = dependency.Validate(); err != nil {
		return
	}
	dependency.GetInto(devops.TypeDocumentManagement, documentManagement).GetInto(devops.TypeSecret, secret)

	// annotation for public start

	structNameForLog := "DocumentManagementBindingMutation"

	methodNameForLog := "Handle"

	err = common.ProcessBindingLabelForToolPublic(&binding.ObjectMeta, documentManagement, d.GetProvider().BaseDomain, structNameForLog, methodNameForLog)
	if err != nil {
		return false, err
	}

	binding.Labels[devops.LabelDocumentManagementType] = documentManagement.Spec.Type.String()
	binding.Labels[devops.LabelDocumentManagement] = documentManagement.GetName()
	if len(binding.OwnerReferences) == 0 {
		binding.OwnerReferences = []metav1.OwnerReference{
			*common.OwnerReference(documentManagement.GetObjectMeta(), documentManagement.GetKind()),
		}
	}

	common.MutateBindingResource(devops.TypeDocumentManagementBinding, binding, documentManagement, secret, d.GetProvider())

	err = common.MutateNamespaceResource(d.GetDependencyManager(), binding.Namespace, binding.Labels, binding.Annotations)
	return
}
