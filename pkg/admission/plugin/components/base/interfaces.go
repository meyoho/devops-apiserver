package base

import (
	"k8s.io/apiserver/pkg/admission"
)

type Middleware interface {
	Handle(PluginRequire, admission.Attributes) (bool, error)
}
