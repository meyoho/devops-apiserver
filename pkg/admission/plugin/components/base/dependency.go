package base

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
)

var (
	DefaultPluginRequire = NewPluginRequire()
)

type PluginRequire interface {
	GetDevopsClient() devopsclient.Interface
	GetDependencyManager() dependency.Manager
	SetDevopsClient(devopsclient.Interface)
	SetDependencyManager(dependency.Manager)
	SetProvider(devops.AnnotationProvider)
	GetProvider() devops.AnnotationProvider
}

var _ PluginRequire = &pluginRequire{}

func NewPluginRequire() PluginRequire {
	return &pluginRequire{}
}

type pluginRequire struct {
	devopsclient       devopsclient.Interface
	manager            dependency.Manager
	annotationprovider devops.AnnotationProvider
}

func (d *pluginRequire) GetDevopsClient() devopsclient.Interface {
	return d.devopsclient
}

func (d *pluginRequire) GetDependencyManager() dependency.Manager {
	return d.manager
}

func (d *pluginRequire) GetProvider() devops.AnnotationProvider {
	return d.annotationprovider
}

func (d *pluginRequire) SetDevopsClient(client devopsclient.Interface) {
	d.devopsclient = client
}

func (d *pluginRequire) SetDependencyManager(manager dependency.Manager) {
	d.manager = manager
}

func (d *pluginRequire) SetProvider(provider devops.AnnotationProvider) {
	d.annotationprovider = provider
}
