package toolbindingreplica

import (
	"fmt"
	"sync"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	glog "k8s.io/klog"
)

var _ base.Middleware = &ToolBindingReplicaMutation{}

type ToolBindingReplicaMutation struct {
	client         devopsclient.Interface
	manager        dependency.Manager
	initVerifyFunc sync.Once
	verifyFuncMap  map[string]verifyFunc
}

func (c *ToolBindingReplicaMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	c.client = d.GetDevopsClient()
	c.manager = d.GetDependencyManager()
	secret := &corev1.Secret{}
	tbr := attr.GetObject().(*devops.ToolBindingReplica)
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), tbr.Namespace)
	manager := c.manager.GetWithObject(ctx, devops.TypeToolBindingReplica, tbr)
	if err = manager.Validate(); err != nil {
		return
	}
	manager.GetInto(devops.TypeSecret, secret)

	if tbr.Spec.Selector.Kind != "" || tbr.Spec.Selector.Name != "" {
		tool, err := c.verifyToolExists(tbr.Spec.Selector.ObjectReference)
		if err != nil {
			glog.V(5).Infof("tool named %s|%s is not exists \n", tbr.Spec.Selector.Kind, tbr.Spec.Selector.Name)
			return false, err
		}

		secretRef := tool.(devops.ToolInterface).GetToolSpec().Secret
		// Copy secret on tool to ToolBindingReplica if secret is coming from tool
		if tbr.Spec.Secret == nil || tbr.Spec.Secret.Namespace == "" {
			tbr.Spec.Secret = &secretRef
		}

		if tbr.Spec.Secret.Namespace == secretRef.Namespace && tbr.Spec.Secret.Name != secretRef.Name {
			tbr.Spec.Secret.Name = secretRef.Name
		}

		// 填充默认值
		glog.V(5).Infof("mutate toolbindingreplica %s/%s ", tbr.Namespace, tbr.Name)
		err = c.mutateToolBindingReplica(tbr, tool, secret, d.GetProvider())
		if err != nil {
			return false, err
		}
	}
	return false, nil
}

func (d *ToolBindingReplicaMutation) mutateToolBindingReplica(tbr *devops.ToolBindingReplica, tool metav1.Object, secret *corev1.Secret, provider devops.AnnotationProvider) error {
	toolInterface := tool.(devops.ToolInterface)

	tbr.OwnerReferences = []metav1.OwnerReference{*common.OwnerReference(toolInterface.GetObjectMeta(), toolInterface.GetKind())}

	mutateToolBindingReplicaLabelsAndAnnotations(tbr, toolInterface, provider)

	if secret != nil {
		tbr.Annotations[provider.AnnotationsSecretType()] = string(secret.Type)
	}

	// assign coderepoBinding
	if _, ok := tool.(*devops.CodeRepoService); ok {
		mutateCodeRepoServiceBindingReplicaTmpl(tbr)
		return nil
	}

	// assign jenkinsBinding
	if _, ok := tool.(*devops.Jenkins); ok {
		mutateJenkinsBindingReplicaTmpl(tbr)
		return nil
	}

	//  assign imageregistry
	if _, ok := tool.(*devops.ImageRegistry); ok {
		mutateImageRegistryBindingReplicaTmpl(tbr)
		return nil
	}

	// assign projectmanagement
	if _, ok := tool.(*devops.ProjectManagement); ok {
		mutateProjectManagementBindingReplicaTmpl(tbr)
		return nil
	}

	// assign documentmanagement
	if _, ok := tool.(*devops.DocumentManagement); ok {
		mutateDocumentManagementBindingReplicaTmpl(tbr)
		return nil
	}

	// assign codequalitytool
	if _, ok := tool.(*devops.CodeQualityTool); ok {
		mutateCodeQualityToolBindingReplicaTmpl(tbr)
		return nil
	}

	// assign artifactregistry
	if _, ok := tool.(*devops.ArtifactRegistry); ok {
		mutateArtifactRegistryBindingReplicaTmpl(tbr)
		return nil
	}
	// TODO: - need add project logic

	glog.Errorf("unknown tool %#v when  mutate ToolBindingReplica %s \n", tool, tbr.Name)
	return nil
}

func mutateArtifactRegistryBindingReplicaTmpl(tbr *devops.ToolBindingReplica) {
	if tbr.Spec.ArtifactRegistry == nil || tbr.Spec.ArtifactRegistry.Template == nil {
		return
	}
	if len(tbr.Spec.ArtifactRegistry.Template.Bindings) > 0 {
		for index := range tbr.Spec.ArtifactRegistry.Template.Bindings {
			var binding = &tbr.Spec.ArtifactRegistry.Template.Bindings[index]
			binding.Spec.ArtifactRegistry = devops.LocalObjectReference{Name: tbr.Spec.Selector.Name}
			if binding.Spec.Secret.Name == "" {
				binding.Spec.Secret = *tbr.Spec.Secret.DeepCopy()
			}
		}
	}

	glog.V(9).Infof("mutate toolbindreplica[%s/%s] artifactregistry bindings: %#v", tbr.Namespace, tbr.Name, tbr.Spec.ArtifactRegistry.Template.Bindings)
}

func mutateCodeRepoServiceBindingReplicaTmpl(tbr *devops.ToolBindingReplica) {
	if tbr.Spec.CodeRepoService == nil || tbr.Spec.CodeRepoService.Template == nil {
		return
	}

	owners := make(map[string]struct{})
	for _, owner := range tbr.Spec.CodeRepoService.Owners {
		owners[owner.Name] = struct{}{}
	}

	if len(tbr.Spec.CodeRepoService.Template.Bindings) > 0 {
		validBindings := make([]devops.CodeRepoBindingTemplate, 0, len(tbr.Spec.CodeRepoService.Template.Bindings))
		for index := range tbr.Spec.CodeRepoService.Template.Bindings {
			var binding = &tbr.Spec.CodeRepoService.Template.Bindings[index]
			binding.Spec.CodeRepoService = devops.LocalObjectReference{Name: tbr.Spec.Selector.Name}
			if binding.Spec.Account.Secret.Name == "" {
				binding.Spec.Account.Secret = *tbr.Spec.Secret.DeepCopy()
			}

			for _, owner := range binding.Spec.Account.Owners {
				if _, exist := owners[owner.Name]; exist {
					validBindings = append(validBindings, *binding)
					break
				}
			}
		}
		tbr.Spec.CodeRepoService.Template.Bindings = validBindings
	}

	glog.V(9).Infof("mutate toolbindreplica[%s/%s] codereposervice bindings: %#v", tbr.Namespace, tbr.Name, tbr.Spec.CodeRepoService.Template.Bindings)
}

func mutateProjectManagementBindingReplicaTmpl(tbr *devops.ToolBindingReplica) {
	if tbr.Spec.ProjectManagement == nil || tbr.Spec.ProjectManagement.Template == nil {
		return
	}

	projects := make(map[string]struct{})
	for _, project := range tbr.Spec.ProjectManagement.Projects {
		projects[project.Name] = struct{}{}
	}

	if len(tbr.Spec.ProjectManagement.Template.Bindings) > 0 {
		validBindings := make([]devops.ProjectManagementBindingTemplate, 0, len(tbr.Spec.ProjectManagement.Template.Bindings))
		for index := range tbr.Spec.ProjectManagement.Template.Bindings {
			var binding = &tbr.Spec.ProjectManagement.Template.Bindings[index]
			binding.Spec.ProjectManagement = devops.LocalObjectReference{Name: tbr.Spec.Selector.Name}
			if binding.Spec.Secret.Name == "" {
				binding.Spec.Secret = *tbr.Spec.Secret.DeepCopy()
			}

			for _, project := range binding.Spec.ProjectManagementProjectInfos {
				if _, exist := projects[project.Name]; exist {
					validBindings = append(validBindings, *binding)
					break
				}
			}
		}
		tbr.Spec.ProjectManagement.Template.Bindings = validBindings
	}

	glog.V(9).Infof("mutate toolbindreplica[%s/%s] projectmanagement bindings: %#v", tbr.Namespace, tbr.Name, tbr.Spec.ProjectManagement.Template.Bindings)
}

func mutateDocumentManagementBindingReplicaTmpl(tbr *devops.ToolBindingReplica) {
	if tbr.Spec.DocumentManagement == nil || tbr.Spec.DocumentManagement.Template == nil {
		return
	}

	documents := make(map[string]struct{})
	for _, document := range tbr.Spec.DocumentManagement.Spaces {
		documents[document.Name] = struct{}{}
	}

	if len(tbr.Spec.DocumentManagement.Template.Bindings) > 0 {
		validBindings := make([]devops.DocumentManagementBindingTemplate, 0, len(tbr.Spec.DocumentManagement.Template.Bindings))
		for index := range tbr.Spec.DocumentManagement.Template.Bindings {
			var binding = &tbr.Spec.DocumentManagement.Template.Bindings[index]
			binding.Spec.DocumentManagement = devops.LocalObjectReference{Name: tbr.Spec.Selector.Name}
			if binding.Spec.Secret.Name == "" {
				binding.Spec.Secret = *tbr.Spec.Secret.DeepCopy()
			}

			for _, document := range binding.Spec.DocumentManagementSpaceRefs {
				if _, exist := documents[document.Name]; exist {
					validBindings = append(validBindings, *binding)
					break
				}
			}
		}
		tbr.Spec.DocumentManagement.Template.Bindings = validBindings
	}

	glog.Infof("mutate toolbindreplica[%s/%s] documentmanagement bindings: %v", tbr.Namespace, tbr.Name, tbr.Spec.DocumentManagement.Template.Bindings)
}

func mutateCodeQualityToolBindingReplicaTmpl(tbr *devops.ToolBindingReplica) {
	if tbr.Spec.CodeQuality == nil || tbr.Spec.CodeQuality.Template == nil {
		return
	}

	if len(tbr.Spec.CodeQuality.Template.Bindings) > 0 {
		for index := range tbr.Spec.CodeQuality.Template.Bindings {
			var binding = &tbr.Spec.CodeQuality.Template.Bindings[index]
			binding.Spec.CodeQualityTool = devops.LocalObjectReference{Name: tbr.Spec.Selector.Name}
			if binding.Spec.Secret.Name == "" {
				binding.Spec.Secret = *tbr.Spec.Secret.DeepCopy()
			}
		}
	}

	glog.Infof("mutate toolbindreplica[%s/%s] codequality bindings: %v", tbr.Namespace, tbr.Name, tbr.Spec.CodeQuality.Template.Bindings)
}

func mutateJenkinsBindingReplicaTmpl(tbr *devops.ToolBindingReplica) {

	if tbr.Spec.ContinuousIntegration == nil || tbr.Spec.ContinuousIntegration.Template == nil {
		return
	}
	if len(tbr.Spec.ContinuousIntegration.Template.Bindings) > 0 {
		for index := range tbr.Spec.ContinuousIntegration.Template.Bindings {
			var binding = &tbr.Spec.ContinuousIntegration.Template.Bindings[index]
			binding.Spec.Jenkins = devops.JenkinsInstance{Name: tbr.Spec.Selector.Name}
			if binding.Spec.Account.Secret.Name == "" {
				binding.Spec.Account.Secret = *tbr.Spec.Secret.DeepCopy()
			}
		}
	}
	glog.V(9).Infof("mutate toolbindreplica[%s/%s] continuousIntegration bindings: %#v", tbr.Namespace, tbr.Name, tbr.Spec.ContinuousIntegration.Template.Bindings)
}

func mutateImageRegistryBindingReplicaTmpl(tbr *devops.ToolBindingReplica) {

	if tbr.Spec.ImageRegistry == nil || tbr.Spec.ImageRegistry.Template == nil {
		return
	}

	repos := make(map[string]struct{})
	for _, repo := range tbr.Spec.ImageRegistry.RepoInfo.Repositories {
		repos[repo] = struct{}{}
	}

	if len(tbr.Spec.ImageRegistry.Template.Bindings) > 0 {
		validBindings := make([]devops.ImageRegistryBindingTemplate, 0, len(tbr.Spec.ImageRegistry.Template.Bindings))
		for index := range tbr.Spec.ImageRegistry.Template.Bindings {
			var binding = &tbr.Spec.ImageRegistry.Template.Bindings[index]
			binding.Spec.ImageRegistry = devops.LocalObjectReference{Name: tbr.Spec.Selector.Name}
			if binding.Spec.Secret.Name == "" {
				binding.Spec.Secret = *tbr.Spec.Secret.DeepCopy()
			}

			for _, repo := range binding.Spec.RepoInfo.Repositories {
				if _, exist := repos[repo]; exist {
					validBindings = append(validBindings, *binding)
					break
				}
			}
		}
		tbr.Spec.ImageRegistry.Template.Bindings = validBindings
	}
	glog.V(9).Infof("mutate toolbindreplica[%s/%s] imageregistry bindings: %#v", tbr.Namespace, tbr.Name, tbr.Spec.ImageRegistry.Template.Bindings)
}

func mutateToolBindingReplicaLabelsAndAnnotations(toolbindingreplica *devops.ToolBindingReplica, tool devops.ToolInterface, provider devops.AnnotationProvider) {

	annotations := toolbindingreplica.Annotations
	toolAnnotations := devops.AnnotateToolBindingReplica(tool, provider)

	for k, v := range toolAnnotations {
		annotations[k] = v
	}

	// add to labels as well
	labels := toolbindingreplica.Labels
	for k, v := range annotations {
		if _, ok := labels[k]; !ok {
			labels[k] = v
		}
	}
	labels = devops.CurateLabels(labels)
	toolbindingreplica.Labels = labels
}

func (d *ToolBindingReplicaMutation) verifyToolExists(toolSelector corev1.ObjectReference) (tool metav1.Object, err error) {

	verifyFuncMap := d.getVerifyFuncMap()

	var (
		verifyF verifyFunc
		ok      bool
	)
	verifyF, ok = verifyFuncMap[toolSelector.Kind]
	if !ok {
		glog.Errorf("cannot verify if tool exists, the kind `%s` is unknown", toolSelector.Kind)
		return nil, fmt.Errorf("kind of %s is unknown", toolSelector.Kind)
	}

	interf, err := verifyF(&devops.LocalObjectReference{Name: toolSelector.Name})
	glog.V(5).Infof("verify toolbindingreplica: %s|%s exists, err:%#v", toolSelector.Kind, toolSelector.Name, err)
	return interf, err
}

type verifyFunc func(*devops.LocalObjectReference) (metav1.Object, error)

func (d *ToolBindingReplicaMutation) getVerifyFuncMap() map[string]verifyFunc {
	d.initVerifyFunc.Do(func() {
		d.verifyFuncMap = map[string]verifyFunc{
			devops.TypeCodeRepoService: func(lor *devops.LocalObjectReference) (metav1.Object, error) {
				var obj *devops.CodeRepoService
				m := d.manager.Get(genericapirequest.NewContext(), devops.TypeCodeRepoService, lor.Name)
				m.GetInto(devops.TypeCodeRepoService, obj)
				if err := m.Validate(); err != nil {
					return nil, err
				}
				return obj, nil
			},
			devops.TypeJenkins: func(lor *devops.LocalObjectReference) (metav1.Object, error) {
				var obj *devops.Jenkins
				m := d.manager.Get(genericapirequest.NewContext(), devops.TypeCodeRepoService, lor.Name)
				m.GetInto(devops.TypeJenkins, obj)
				if err := m.Validate(); err != nil {
					return nil, err
				}
				return obj, nil
			},
			devops.TypeImageRegistry: func(lor *devops.LocalObjectReference) (metav1.Object, error) {
				var obj *devops.ImageRegistry
				m := d.manager.Get(genericapirequest.NewContext(), devops.TypeCodeRepoService, lor.Name)
				m.GetInto(devops.TypeImageRegistry, obj)
				if err := m.Validate(); err != nil {
					return nil, err
				}
				return obj, nil
			},
			devops.TypeProjectManagement: func(lor *devops.LocalObjectReference) (metav1.Object, error) {
				var obj *devops.ProjectManagement
				m := d.manager.Get(genericapirequest.NewContext(), devops.TypeCodeRepoService, lor.Name)
				m.GetInto(devops.TypeProjectManagement, obj)
				if err := m.Validate(); err != nil {
					return nil, err
				}
				return obj, nil
			},
			devops.TypeDocumentManagement: func(lor *devops.LocalObjectReference) (metav1.Object, error) {
				var obj *devops.DocumentManagement
				m := d.manager.Get(genericapirequest.NewContext(), devops.TypeCodeRepoService, lor.Name)
				m.GetInto(devops.TypeDocumentManagement, obj)
				if err := m.Validate(); err != nil {
					return nil, err
				}
				return obj, nil
			},
			devops.TypeCodeQualityTool: func(lor *devops.LocalObjectReference) (metav1.Object, error) {
				var obj *devops.CodeQualityTool
				m := d.manager.Get(genericapirequest.NewContext(), devops.TypeCodeRepoService, lor.Name)
				m.GetInto(devops.TypeCodeQualityTool, obj)
				if err := m.Validate(); err != nil {
					return nil, err
				}
				return obj, nil
			},
			devops.TypeArtifactRegistry: func(lor *devops.LocalObjectReference) (metav1.Object, error) {
				var obj *devops.ArtifactRegistry
				m := d.manager.Get(genericapirequest.NewContext(), devops.TypeCodeRepoService, lor.Name)
				m.GetInto(devops.TypeArtifactRegistry, obj)
				if err := m.Validate(); err != nil {
					return nil, err
				}
				return obj, nil
			},
			//TODO: cjt - Add other tools here
		}
	})
	return d.verifyFuncMap
}
