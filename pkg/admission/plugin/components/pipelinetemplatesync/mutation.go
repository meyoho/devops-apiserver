package pipelinetemplatesync

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

var _ base.Middleware = &PipelineTemplateSyncMutation{}

type PipelineTemplateSyncMutation struct {
}

func (c *PipelineTemplateSyncMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	sync := attr.GetObject().(*devops.PipelineTemplateSync)
	namespace := sync.GetNamespace()
	if sync.Spec.Source.CodeRepository == nil {
		return
	}

	var codeRepository *devops.CodeRepository
	codeRepoBinding := &devops.CodeRepoBinding{}
	if sync.Spec.Source.CodeRepository != nil {
		codeRepository, err = d.GetDevopsClient().Devops().CodeRepositories(namespace).Get(sync.Spec.Source.CodeRepository.Name, metav1.GetOptions{ResourceVersion: "0"})
		if err != nil {
			return
		}
		if codeRepository != nil {
			ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), codeRepository.Namespace)
			dependency := d.GetDependencyManager().GetWithObject(ctx, devops.TypeCodeRepository, codeRepository)
			if err = dependency.Validate(); err != nil {
				return
			}
			dependency.GetInto(devops.TypeCodeRepoBinding, codeRepoBinding)
		}
	}
	mutatePipelineTemplateSync(sync, codeRepository, codeRepoBinding)
	return
}

func mutatePipelineTemplateSync(sync *devops.PipelineTemplateSync, codeRepository *devops.CodeRepository, codeRepoBinding *devops.CodeRepoBinding) {
	sync.Spec.Source.Git = &devops.PipelineSourceGit{
		URI: codeRepository.Spec.Repository.CloneURL,
		Ref: sync.Spec.Source.CodeRepository.Ref,
	}
	sync.Spec.Source.Secret = &codeRepoBinding.Spec.Account.Secret
}
