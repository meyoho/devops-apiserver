package coderepository

import (
	"fmt"
	"reflect"
	"strconv"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	utilgeneric "alauda.io/devops-apiserver/pkg/util/generic"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	glog "k8s.io/klog"
)

var _ base.Middleware = &CodeRepoBindingMutation{}
var _ base.Middleware = &CodeRepositoryMutation{}

type CodeRepoBindingMutation struct {
}

func (c *CodeRepoBindingMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {

	shouldContinue = true
	binding := attr.GetObject().(*devops.CodeRepoBinding)
	codeRepoService := &devops.CodeRepoService{}
	secret := &corev1.Secret{}
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), binding.Namespace)
	dependency := d.GetDependencyManager().GetWithObject(ctx, devops.TypeCodeRepoBinding, binding)
	if err = dependency.Validate(); err != nil {
		return
	}
	dependency.GetInto(devops.TypeSecret, secret).GetInto(devops.TypeCodeRepoService, codeRepoService)
	if binding.Spec.Account.Secret.Namespace == "" {
		binding.Spec.Account.Secret.Namespace = binding.Namespace
	}

	// annotation for public start

	structNameForLog := "CodeRepoBindingMutation"

	methodNameForLog := "Handle"

	err = common.ProcessBindingLabelForToolPublic(&binding.ObjectMeta, codeRepoService, d.GetProvider().BaseDomain, structNameForLog, methodNameForLog)
	if err != nil {
		return false, err
	}

	binding.Labels[devops.LabelCodeRepoServiceType] = codeRepoService.Spec.Type.String()
	binding.Labels[devops.LabelCodeRepoService] = codeRepoService.GetName()
	binding.Labels[devops.LabelCodeRepoServicePublic] = strconv.FormatBool(codeRepoService.IsPublic())
	if len(binding.OwnerReferences) == 0 {
		binding.OwnerReferences = []metav1.OwnerReference{
			*common.OwnerReference(codeRepoService.GetObjectMeta(), codeRepoService.GetKind()),
		}
	}
	common.MutateBindingResource(devops.TypeCodeRepoBinding, binding, codeRepoService, secret, d.GetProvider())
	err = common.MutateNamespaceResource(d.GetDependencyManager(), binding.Namespace, binding.Labels, binding.Annotations)
	return
}

type CodeRepositoryMutation struct {
}

func (c *CodeRepositoryMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	repository := attr.GetObject().(*devops.CodeRepository)
	binding := &devops.CodeRepoBinding{}

	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), repository.Namespace)
	dependency := d.GetDependencyManager().GetWithObject(ctx, devops.TypeCodeRepository, repository)
	if err = dependency.Validate(); err != nil {
		glog.Errorf("[coderepository] mutation error: %s", err.Error())
	}

	dependency.GetInto(devops.TypeCodeRepoBinding, binding)
	if reflect.DeepEqual(binding, &devops.CodeRepoBinding{}) && err != nil {
		return false, fmt.Errorf("[coderepository] op: %s, CodeRepository: %v, mutation error: %s", attr.GetOperation(), repository, err.Error())
	}
	repository.Labels[devops.LabelCodeRepoBinding] = binding.GetName()
	repository.Labels[devops.LabelCodeRepoService] = binding.Spec.CodeRepoService.Name

	repository.Annotations[devops.LabelsSecretName] = binding.GetSecretName()
	repository.Annotations[devops.LabelsSecretNamespace] = binding.GetSecretNamespace()

	if err = common.MutateNamespaceResource(d.GetDependencyManager(), repository.Namespace, repository.Labels, repository.Annotations); err != nil {
		return
	}

	ownerReference := common.OwnerReference(binding, devops.TypeCodeRepoBinding)
	ownerReference.BlockOwnerDeletion = utilgeneric.Bool(true)
	repository.OwnerReferences = []metav1.OwnerReference{*ownerReference}
	return
}
