package coderepository_test

import (
	"testing"
	"time"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/coderepository"

	"alauda.io/devops-apiserver/pkg/admission/plugin"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/fake"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apiserver/pkg/admission"
	k8sinformer "k8s.io/client-go/informers"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	clienttesting "k8s.io/client-go/testing"
)

var _ = Describe("Verify", func() {
	var (
		err            error
		shouldContinue bool
		admissionInput runtime.Object
		pluginOutput   *devops.CodeRepoServiceList
		verify         coderepository.CodeRepoServiceVerify
		require        base.PluginRequire
		input          admission.Attributes
	)

	BeforeEach(func() {
		verify = coderepository.CodeRepoServiceVerify{}
	})

	JustBeforeEach(func() {
		dependency := dependency.NewManager()
		require = base.NewPluginRequire()
		cs := &fake.Clientset{}
		if pluginOutput != nil {
			cs.AddReactor("list", "codereposervices", func(action clienttesting.Action) (bool, runtime.Object, error) {
				return true, pluginOutput, nil
			})
		}
		informersFactory := informers.NewSharedInformerFactory(cs, 5*time.Minute)
		k8sclient := &k8sfake.Clientset{}
		k8sInformerFactory := k8sinformer.NewSharedInformerFactory(k8sclient, 5*time.Minute)
		plugin.AddListers(dependency, informersFactory, k8sInformerFactory, k8sclient)
		require.SetDependencyManager(dependency)
		require.SetDevopsClient(cs)
		input = admission.NewAttributesRecord(
			admissionInput,
			nil,
			devops.Kind("CodeRepoService").WithVersion("version"),
			"default",
			"",
			devops.Resource("CodeRepoService").WithVersion("version"),
			"",
			admission.Create,
			false,
			nil)
		stop := make(chan struct{})
		defer close(stop)
		informersFactory.Start(stop)
		informersFactory.WaitForCacheSync(stop)
		k8sInformerFactory.Start(stop)
		k8sInformerFactory.WaitForCacheSync(stop)
		shouldContinue, err = verify.Handle(require, input)

	})
	Context("No error", func() {
		BeforeEach(func() {
			pluginOutput = &devops.CodeRepoServiceList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.CodeRepoService{
					devops.CodeRepoService{
						ObjectMeta: metav1.ObjectMeta{
							Name: "codereposervice",
						},
						Spec: devops.CodeRepoServiceSpec{
							ToolSpec: devops.ToolSpec{
								HTTP: devops.HostPort{
									Host: "https://mayun.sparrow.host",
								},
							},
							Type: devops.CodeRepoServiceTypeGitee,
						},
					},
				},
			}
			admissionInput = &devops.CodeRepoService{
				ObjectMeta: metav1.ObjectMeta{
					Name: "codereposervice1",
				},
				Spec: devops.CodeRepoServiceSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "https://mayun.sparrow1.host",
						},
					},
					Type: devops.CodeRepoServiceTypeGitee,
				},
			}
		})

		It("Get reponse", func() {
			Expect(err).To(BeNil())
			Expect(shouldContinue).To(BeTrue())
		})
	})

	Context("Same host", func() {
		BeforeEach(func() {
			pluginOutput = &devops.CodeRepoServiceList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.CodeRepoService{
					devops.CodeRepoService{
						ObjectMeta: metav1.ObjectMeta{
							Name: "codereposervice",
						},
						Spec: devops.CodeRepoServiceSpec{
							ToolSpec: devops.ToolSpec{
								HTTP: devops.HostPort{
									Host: "https://mayun.sparrow.host/",
								},
							},
							Type: devops.CodeRepoServiceTypeGitee,
						},
					},
				},
			}
			admissionInput = &devops.CodeRepoService{
				ObjectMeta: metav1.ObjectMeta{
					Name: "codereposervice1",
				},
				Spec: devops.CodeRepoServiceSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "https://mayun.sparrow.host",
						},
					},
					Type: devops.CodeRepoServiceTypeGitee,
				},
			}
		})

		It("Get reponse", func() {
			Expect(err).ToNot(BeNil())
			Expect(shouldContinue).To(BeFalse())
		})
	})
})

func TestSo(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Coderepository")
}
