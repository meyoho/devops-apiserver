package pipelinetemplate_test

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/pipelinetemplate"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("Pipelinetemplate", func() {
	Context("test k8s Compatiable", func() {
		It("annotation and label should delete alauda.io", func() {
			template := devops.PipelineTemplate{
				ObjectMeta: v1.ObjectMeta{
					Annotations: map[string]string{
						"alauda.io/version": "0.1",
					},
					Labels: map[string]string{
						"alauda.io/latest": "true",
					},
				},
			}

			result := pipelinetemplate.MutatePipelineTemplate(&template)
			gomega.Expect(result.GetAnnotations()).To(gomega.Equal(map[string]string{
				"version": "0.1",
			}))
			gomega.Expect(result.GetLabels()).To(gomega.Equal(map[string]string{
				"latest": "true",
			}))
		})
	})
})
