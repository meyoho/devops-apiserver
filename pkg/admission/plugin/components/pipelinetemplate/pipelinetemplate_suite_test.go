package pipelinetemplate_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestPipelinetemplate(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Pipelinetemplate Suite")
}
