package pipelinetemplate

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"k8s.io/apiserver/pkg/admission"
)

var _ base.Middleware = &PipelineTemplateMutation{}
var _ base.Middleware = &ClusterPipelineTemplateMutation{}

type PipelineTemplateMutation struct {
}

func (c *PipelineTemplateMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	template := attr.GetObject().(*devops.PipelineTemplate)
	MutatePipelineTemplate(template)
	return
}

type ClusterPipelineTemplateMutation struct {
}

func (c *ClusterPipelineTemplateMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	template := attr.GetObject().(*devops.ClusterPipelineTemplate)
	MutatePipelineTemplate(template)
	return
}

func MutatePipelineTemplate(template devops.PipelineTemplateInterface) (result devops.PipelineTemplateInterface) {

	k8s.CompatiableBaseDomain(template)
	// set finalizers if no exist
	if len(template.GetFinalizers()) > 0 {
		return
	}
	if template.GetDeletionTimestamp() == nil || template.GetDeletionTimestamp().IsZero() {
		template.SetFinalizers([]string{devops.FinalizerPipelineConfigReferenced})
	}
	return template
}
