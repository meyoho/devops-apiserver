package imagerepository

import (
	"reflect"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	utilgeneric "alauda.io/devops-apiserver/pkg/util/generic"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

var _ base.Middleware = &ImageRegistryBindingMutation{}
var _ base.Middleware = &ImageRepositoryMutation{}

type ImageRegistryBindingMutation struct {
}

func (c *ImageRegistryBindingMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	binding := attr.GetObject().(*devops.ImageRegistryBinding)
	imageRegistry := &devops.ImageRegistry{}
	secret := &corev1.Secret{}
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), binding.Namespace)
	dependency := d.GetDependencyManager().GetWithObject(ctx, devops.TypeImageRegistryBinding, binding)
	if err = dependency.Validate(); err != nil {
		return
	}
	dependency.GetInto(devops.TypeImageRegistry, imageRegistry).GetInto(devops.TypeSecret, secret)

	// annotation for public start

	structNameForLog := "ImageRegistryBindingMutation"

	methodNameForLog := "Handle"

	err = common.ProcessBindingLabelForToolPublic(&binding.ObjectMeta, imageRegistry, d.GetProvider().BaseDomain, structNameForLog, methodNameForLog)
	if err != nil {
		return false, err
	}

	binding.Labels[devops.LabelImageRegistryType] = imageRegistry.Spec.Type.String()
	binding.Labels[devops.LabelImageRegistry] = imageRegistry.GetName()
	if len(binding.OwnerReferences) == 0 {
		binding.OwnerReferences = []metav1.OwnerReference{
			*common.OwnerReference(imageRegistry.GetObjectMeta(), imageRegistry.GetKind()),
		}
	}

	common.MutateBindingResource(devops.TypeImageRegistryBinding, binding, imageRegistry, secret, d.GetProvider())

	err = common.MutateNamespaceResource(d.GetDependencyManager(), binding.Namespace, binding.Labels, binding.Annotations)
	return
}

type ImageRepositoryMutation struct {
}

func (c *ImageRepositoryMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	repository := attr.GetObject().(*devops.ImageRepository)
	binding := &devops.ImageRegistryBinding{}
	imageRegistry := &devops.ImageRegistry{}
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), repository.Namespace)
	dependency := d.GetDependencyManager().GetWithObject(ctx, devops.TypeImageRepository, repository)
	if err = dependency.Validate(); err != nil {
		return
	}
	dependency.GetInto(devops.TypeImageRegistryBinding, binding).GetInto(devops.TypeImageRegistry, imageRegistry)
	if !reflect.DeepEqual(binding, &devops.ImageRegistryBinding{}) && !reflect.DeepEqual(imageRegistry, &devops.ImageRegistry{}) {
		repository.Labels[devops.LabelImageRegistryBinding] = binding.GetName()
		repository.Labels[devops.LabelImageRegistry] = imageRegistry.Name

		repository.Annotations[devops.LabelImageRegistryType] = imageRegistry.Spec.Type.String()
		repository.Annotations[devops.LabelsSecretName] = binding.GetDockerCfgSecretName()
		repository.Annotations[devops.LabelsSecretNamespace] = binding.GetNamespace()
		if imageRegistry.GetType() == devops.RegistryTypeDockerHub || imageRegistry.GetType() == devops.RegistryTypeAlauda {
			if imageRegistry.Spec.Data != nil && imageRegistry.Spec.Data["endpoint"] != "" {
				repository.Annotations[devops.LabelImageRegistryEndpoint] = imageRegistry.Spec.Data["endpoint"]
			}
		} else {
			repository.Annotations[devops.LabelImageRegistryEndpoint] = utilgeneric.GetHostInUrl(imageRegistry.Spec.HTTP.Host)
		}

		if err = common.MutateNamespaceResource(d.GetDependencyManager(), repository.Namespace, repository.Labels, repository.Annotations); err != nil {
			return
		}

		ownerReference := common.OwnerReference(binding, devops.TypeImageRegistryBinding)
		ownerReference.BlockOwnerDeletion = utilgeneric.Bool(true)
		repository.OwnerReferences = []metav1.OwnerReference{*ownerReference}
	}
	return
}
