package components

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"io"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"

	"alauda.io/devops-apiserver/pkg/dependency"

	"alauda.io/devops-apiserver/pkg/admission/plugin"

	// "sort"

	"alauda.io/devops-apiserver/pkg/admission/devopsinitializer"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	"k8s.io/apiserver/pkg/admission"
	k8sinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
)

// PluginName plugin name
// must be unique in the API server

const (
	PluginName = "VerifyDependency"
)

var (
	DefaultMiddlewares = []base.Middleware{}
)

// Register registers a plugin
func Register(plugins *admission.Plugins) {
	plugins.Register(PluginName, func(config io.Reader) (admission.Interface, error) {
		plugin.ReadConfigInPlugin(config, PluginName)

		return New()
	})
}

// Components type to control validation logic
// this plugin main purpose is to validate entry on create and update
// and avoid creating JenkinsBinding for non-existing Jenkins instances
type Components struct {
	*admission.Handler
	Middlewares []base.Middleware
}

var _ = devopsinitializer.WantsInternalDevopsInformerFactory(&Components{})

// Admit ensures that the object in-flight is of kind JenkinsBinding.
// In addition checks that the Jenkins instance used does exist
func (d *Components) Admit(a admission.Attributes) error {

	if len(d.Middlewares) > 0 {
		for _, f := range d.Middlewares {
			if shouldContinue, err := f.Handle(base.DefaultPluginRequire, a); !shouldContinue || err != nil {
				return err
			}
		}
	}

	return nil
}

// SetInternalDevopsInformerFactory gets Lister from SharedInformerFactory.
func (d *Components) SetInternalDevopsInformerFactory(f informers.SharedInformerFactory) {
}

// SetKubernetesInformerFactory set kubernetes informer factory
func (d *Components) SetKubernetesInformerFactory(f k8sinformers.SharedInformerFactory) {
	glog.V(3).Infof("got kubernetes informer factory: %v", f)
}

// SetDependencyManager set kubernetes informer factory
func (d *Components) SetDependencyManager(m dependency.Manager) {
	base.DefaultPluginRequire.SetDependencyManager(m)
}

// SetDevopsClient set client for devops
func (d *Components) SetDevopsClient(c devopsclient.Interface) {
	glog.V(3).Infof("got devops client: %v", c)
	base.DefaultPluginRequire.SetDevopsClient(c)
}

func (d *Components) SetProvider(provider devops.AnnotationProvider) {
	glog.V(3).Infof("got annotation provider: %v", provider)
	base.DefaultPluginRequire.SetProvider(provider)
}

// SetInternalKubeClientSet set client for kube
func (d *Components) SetInternalKubeClientSet(c kubernetes.Interface) {
}

// ValidateInitialization checks whether the plugin was correctly initialized.
func (d *Components) ValidateInitialization() error {
	return nil
}

// New creates a new jenkins verify admission plugin
func New() (*Components, error) {
	return &Components{
		Handler:     admission.NewHandler(admission.Create, admission.Update),
		Middlewares: DefaultMiddlewares,
	}, nil
}
