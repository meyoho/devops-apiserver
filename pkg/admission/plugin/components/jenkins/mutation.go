package jenkins

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

var _ base.Middleware = &JenkinsBindingMutation{}

type JenkinsBindingMutation struct {
}

func (c *JenkinsBindingMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	jenkinsBinding := attr.GetObject().(*devops.JenkinsBinding)
	secret := &corev1.Secret{}
	jenkins := &devops.Jenkins{}
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), jenkinsBinding.Namespace)
	dependency := d.GetDependencyManager().GetWithObject(ctx, devops.TypeJenkinsBinding, jenkinsBinding)
	if err = dependency.Validate(); err != nil {
		return
	}
	dependency.GetInto(devops.TypeJenkins, jenkins).GetInto(devops.TypeSecret, secret)

	// annotation for public start

	structNameForLog := "JenkinsBindingMutation"

	methodNameForLog := "Handle"

	err = common.ProcessBindingLabelForToolPublic(&jenkinsBinding.ObjectMeta, jenkins, d.GetProvider().BaseDomain, structNameForLog, methodNameForLog)
	if err != nil {
		return false, err
	}

	if len(jenkinsBinding.OwnerReferences) == 0 {
		jenkinsBinding.OwnerReferences = []metav1.OwnerReference{
			*common.OwnerReference(jenkins.GetObjectMeta(), devops.TypeJenkins),
		}
	}
	jenkinsBinding.Labels[devops.LabelJenkins] = jenkins.Name

	common.MutateBindingResource(devops.TypeJenkinsBinding, jenkinsBinding, jenkins, secret, d.GetProvider())

	err = common.MutateNamespaceResource(d.GetDependencyManager(), jenkinsBinding.Namespace, jenkinsBinding.Labels, jenkinsBinding.Annotations)
	return
}
