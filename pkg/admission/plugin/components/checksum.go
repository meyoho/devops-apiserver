package components

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/admission"
)

var _ base.Middleware = &Checksum{}

type Checksum struct {
}

func (c *Checksum) Handle(d base.PluginRequire, attr admission.Attributes) (bool, error) {
	obj := attr.GetObject()

	metaObj, ok := obj.(metav1.Object)
	// skip if this object doesn't contain object metadata
	if !ok {
		return true, nil
	}

	hashCodeObj, ok := metaObj.(devops.Hashcode)
	// skip calculating checksum if this kind of object doesn't implement Hashcode
	if !ok {
		return true, nil
	}

	checksumKey := d.GetProvider().AnnotationsKeyChecksum()
	checksum := hashCodeObj.Hashcode()

	annotations := metaObj.GetAnnotations()
	if annotations == nil {
		annotations = make(map[string]string)
		metaObj.SetAnnotations(annotations)
	}

	annotations[checksumKey] = checksum
	return true, nil
}
