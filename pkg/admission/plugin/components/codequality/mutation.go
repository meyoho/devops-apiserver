package codequality

import (
	"fmt"
	"strings"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	utilgeneric "alauda.io/devops-apiserver/pkg/util/generic"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

var _ base.Middleware = &CodeQualityBindingMutation{}
var _ base.Middleware = &CodeQualityProjectMutation{}

type CodeQualityBindingMutation struct {
}

func (c *CodeQualityBindingMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	binding := attr.GetObject().(*devops.CodeQualityBinding)
	codeQualityTool := &devops.CodeQualityTool{}
	secret := &corev1.Secret{}
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), binding.Namespace)
	dependency := d.GetDependencyManager().GetWithObject(ctx, devops.TypeCodeQualityBinding, binding)
	if err = dependency.Validate(); err != nil {
		return
	}
	dependency.GetInto(devops.TypeCodeQualityTool, codeQualityTool).GetInto(devops.TypeSecret, secret)

	// annotation for public start

	structNameForLog := "CodeQualityBindingMutation"

	methodNameForLog := "Handle"

	err = common.ProcessBindingLabelForToolPublic(&binding.ObjectMeta, codeQualityTool, d.GetProvider().BaseDomain, structNameForLog, methodNameForLog)
	if err != nil {
		return false, err
	}

	binding.Labels[devops.LabelCodeQualityToolType] = codeQualityTool.Spec.Type.String()
	binding.Labels[devops.LabelCodeQualityTool] = codeQualityTool.GetName()
	if len(binding.OwnerReferences) == 0 {
		binding.OwnerReferences = []metav1.OwnerReference{
			*common.OwnerReference(codeQualityTool.GetObjectMeta(), codeQualityTool.GetKind()),
		}
	}

	common.MutateBindingResource(devops.TypeCodeQualityBinding, binding, codeQualityTool, secret, d.GetProvider())

	common.MutateNamespaceResource(d.GetDependencyManager(), binding.Namespace, binding.Labels, binding.Annotations)
	return
}

type CodeQualityProjectMutation struct {
}

func (c *CodeQualityProjectMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	project := attr.GetObject().(*devops.CodeQualityProject)
	codeQualityTool := &devops.CodeQualityTool{}
	binding := &devops.CodeQualityBinding{}
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), project.Namespace)
	dependency := d.GetDependencyManager().GetWithObject(ctx, devops.TypeCodeQualityProject, project)
	if err = dependency.Validate(); err != nil {
		return
	}
	dependency.GetInto(devops.TypeCodeQualityBinding, binding).GetInto(devops.TypeCodeQualityTool, codeQualityTool)
	project.Labels[devops.LabelCodeQualityTool] = codeQualityTool.Name
	project.Labels[devops.LabelCodeQualityBinding] = binding.GetName()

	if codeQualityTool.Spec.HTTP.AccessURL != "" {
		project.Annotations[d.GetProvider().AnnotationsSonarQubeProjectLink()] =
			fmt.Sprintf("%s/dashboard?id=%s", strings.TrimRight(codeQualityTool.Spec.HTTP.AccessURL, "/"), project.Spec.Project.ProjectKey)
	} else {
		project.Annotations[d.GetProvider().AnnotationsSonarQubeProjectLink()] = ""
	}
	project.Annotations[devops.LabelCodeQualityToolType] = codeQualityTool.Spec.Type.String()
	project.Annotations[devops.LabelsSecretName] = binding.GetSecretName()
	project.Annotations[devops.LabelsSecretNamespace] = binding.GetNamespace()
	ownerReference := common.OwnerReference(binding, devops.TypeCodeQualityBinding)
	ownerReference.BlockOwnerDeletion = utilgeneric.Bool(true)
	project.OwnerReferences = []metav1.OwnerReference{*ownerReference}
	return
}
