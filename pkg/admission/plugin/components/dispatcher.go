package components

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"k8s.io/apiserver/pkg/admission"
)

var _ base.Middleware = &Dispatcher{}

type Dispatcher map[string][]base.Middleware

func (k *Dispatcher) Handle(d base.PluginRequire, attr admission.Attributes) (bool, error) {
	var ok bool
	self := map[string][]base.Middleware(*k)
	var middlewares []base.Middleware
	kind := attr.GetKind().GroupKind().Kind
	if middlewares, ok = self[kind]; !ok || len(middlewares) == 0 {
		return true, nil
	}
	for _, f := range middlewares {
		if shouldContinue, err := f.Handle(d, attr); !shouldContinue || err != nil {
			return false, err
		}
	}
	return true, nil
}
