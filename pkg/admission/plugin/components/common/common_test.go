package common_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestAppend(t *testing.T) {
	trueV := true
	falseV := false

	o1 := metav1.OwnerReference{
		Kind:       "k1",
		Name:       "n1",
		Controller: &falseV,
	}

	empty := []metav1.OwnerReference{}

	o2 := []metav1.OwnerReference{
		metav1.OwnerReference{
			Kind:       "k2",
			Name:       "n2",
			Controller: &trueV,
		},
	}

	o3 := []metav1.OwnerReference{
		metav1.OwnerReference{
			Kind:       "k3",
			Name:       "n3",
			Controller: &trueV,
		},
	}

	alreadyOwners := []metav1.OwnerReference{
		metav1.OwnerReference{
			Kind:       "k1",
			Name:       "n1",
			Controller: &trueV,
		},
	}

	result := common.AppendOwnerReference(empty, o1)
	if len(result) != 1 {
		t.Errorf("it should appended")
	}
	if *result[0].Controller != false {
		t.Errorf("owners[0]'s controller should be false")
	}

	result = common.AppendOwnerReference(o2, o1)
	if len(result) != 2 {
		t.Errorf("it should appended")
	}
	if *result[0].Controller != true {
		t.Errorf("owners[0]'s controller should be true")
	}
	if *result[1].Controller != false {
		t.Errorf("owners[1]'s controller should be false")
	}

	result = common.AppendOwnerReference(o3, o2[0])
	if len(result) != 2 {
		t.Errorf("it should appended")
	}
	if *result[0].Controller != true {
		t.Errorf("owners[0]'s controller should be true")
	}
	if *result[1].Controller != false {
		t.Errorf("owners[1]'s controller should be false")
	}

	result = common.AppendOwnerReference(alreadyOwners, o1)
	if len(result) != 1 || result[0].Kind != "k1" || result[0].Name != "n1" {
		t.Errorf("it should noe appended")
	}
}
