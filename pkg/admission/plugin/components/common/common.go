package common

import (
	"fmt"
	"k8s.io/apimachinery/pkg/api/errors"

	"k8s.io/klog"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

const (
	currentVersion = "v1alpha1"
)

func MutateBindingResource(kind string, binding metav1.Object, tool devops.ToolInterface, secret *corev1.Secret, provider devops.AnnotationProvider) {
	annotations := binding.GetAnnotations()
	if annotations == nil {
		annotations = map[string]string{}
	}
	annotations[provider.LabelToolItemKind()] = kind
	annotations[provider.LabelToolItemType()] = tool.GetKindType()
	annotations[provider.AnnotationsToolAccessURL()] = tool.GetToolSpec().HTTP.GetAccessURL()
	if secret != nil {
		annotations[provider.AnnotationsSecretType()] = string(secret.Type)
	}
	binding.SetAnnotations(annotations)
}

func MutateNamespaceResource(d dependency.Manager, namespaceName string, labels map[string]string, annotations map[string]string) (err error) {
	namespace := &v1.Namespace{}
	manager := d.Get(genericapirequest.NewContext(), devops.TypeNamespace, namespaceName)
	if err = manager.Validate(); err != nil {
		return
	}
	manager.GetInto(devops.TypeNamespace, namespace)
	if namespace.Labels != nil {
		for key, value := range namespace.Labels {
			if _, ok := labels[key]; !ok {
				labels[key] = value
			}
		}
	}
	if namespace.Annotations != nil {
		for key, value := range namespace.Annotations {
			if _, ok := annotations[key]; !ok {
				annotations[key] = value
			}
		}
	}
	return nil
}

func OwnerReference(object metav1.Object, kind string) *metav1.OwnerReference {
	return metav1.NewControllerRef(object, schema.GroupVersionKind{
		Group:   devops.GroupName,
		Version: currentVersion,
		Kind:    kind,
	})
}

// CompareNoSuffix is to clear the url suffix slash and then compare
func CompareNoSuffix(x, y string) bool {
	return strings.TrimRight(x, "/") == strings.TrimRight(y, "/")
}

func InitAnnotationsAndLabels(obj metav1.Object) {
	annotations := obj.GetAnnotations()
	if annotations == nil {
		annotations = make(map[string]string)
		obj.SetAnnotations(annotations)
	}
	labels := obj.GetLabels()
	if labels == nil {
		labels = make(map[string]string)
		obj.SetLabels(labels)
	}
}

func AppendOwnerReference(owners []metav1.OwnerReference, owner metav1.OwnerReference) []metav1.OwnerReference {
	if owners == nil {
		owners = []metav1.OwnerReference{}
	}

	already := false
	alreadyHasController := false
	for _, item := range owners {
		if item.Kind == owner.Kind && item.Name == owner.Name {
			already = true
		}

		if item.Controller != nil && *item.Controller {
			alreadyHasController = true
		}
	}

	falseV := false
	if alreadyHasController && *owner.Controller {
		owner.Controller = &falseV
	}

	result := owners
	if !already {
		result = append(owners, owner)
	}

	return result
}

func ProcessBindingLabelForToolPublic(bindingMeta *metav1.ObjectMeta, tool devops.ToolInterface, baseDomain, structNameForLog, methodNameForLog string) error {
	toolItemPublicString := "toolItemPublic"
	toolTypeString := "toolType"

	_, err := processLabel(bindingMeta, tool, []string{toolItemPublicString, toolTypeString}, baseDomain, structNameForLog)

	return err
}

func processLabel(bindingObjectMeta *metav1.ObjectMeta, tool devops.ToolInterface, labelKeys []string, baseDomain, logName string) (isUpdate bool, err error) {

	processLabel := "processLabel"

	for _, labelKey := range labelKeys {
		klog.V(9).Infof("[%s/%s] binding: [%s/%s] process label: %s", logName, processLabel, bindingObjectMeta.Namespace, bindingObjectMeta.Name, labelKey)

		//checkout annotationKey
		if _, ok := (*bindingObjectMeta).Labels[labelKey]; !ok {
			klog.V(9).Infof("[%s/%s] binding: [%s/%s],the label: %s does not exist", logName, processLabel, bindingObjectMeta.Namespace, bindingObjectMeta.Name, labelKey)

			if value, ok := (tool.GetObjectMeta().GetLabels())[baseDomain+"/"+labelKey]; ok {
				(*bindingObjectMeta).Labels[labelKey] = value
				isUpdate = isUpdate || true
				klog.V(9).Infof("[%s/%s] binding: [%s/%s],set label: %s value is: %s", logName, processLabel, bindingObjectMeta.Namespace, bindingObjectMeta.Name, labelKey, value)

			} else {
				klog.V(3).Infof("[%s/%s] binding :[%s/%s],the label %s does not exist,the label does not exist in kind %s tool %s", logName, processLabel, bindingObjectMeta.Namespace, bindingObjectMeta.Name, labelKey, tool.GetKind(), tool.GetObjectMeta().GetName())

				return isUpdate, errors.NewBadRequest(fmt.Sprintf("[%s/%s] process binding: [%s/%s] with kind %s tool %s has not set %s", logName, processLabel, bindingObjectMeta.Namespace, bindingObjectMeta.Name, tool.GetKind(), tool.GetObjectMeta().GetName(), labelKey))
			}

		}
	}

	return isUpdate, nil
}
