package components

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apiserver/pkg/admission"
	glog "k8s.io/klog"
)

const MaxConditionLen = 5000

var _ base.Middleware = &Annotations{}

type Annotations struct {
}

func (i *Annotations) Handle(d base.PluginRequire, attr admission.Attributes) (bool, error) {
	glog.V(7).Infof("Admit kind %v", attr.GetKind().GroupKind())
	obj := attr.GetObject()
	// if this is a toolchain object we should annotate it
	if tool, ok := obj.(devops.ToolInterface); ok {
		i.annotateToolChain(tool, d.GetProvider())
	}

	if metaObj, ok := obj.(metav1.Object); ok {
		i.initAnnotationsAndLabels(metaObj)
	}

	i.conditionsLengthProtect(attr.GetKind().GroupKind(), obj)
	return true, nil
}

func (i *Annotations) annotateToolChain(tool devops.ToolInterface, provider devops.AnnotationProvider) {
	i.initAnnotationsAndLabels(tool.GetObjectMeta())
	annotations := tool.GetObjectMeta().GetAnnotations()
	toolAnnotations := devops.AnnotateTool(tool, provider)

	for k, v := range toolAnnotations {
		annotations[k] = v
	}
	tool.GetObjectMeta().SetAnnotations(annotations)
	// add to labels as well
	labels := tool.GetObjectMeta().GetLabels()
	for k, v := range annotations {
		if _, ok := labels[k]; !ok {
			labels[k] = v
		}
	}
	labels = devops.CurateLabels(labels)
	tool.GetObjectMeta().SetLabels(labels)
}

func (i *Annotations) initAnnotationsAndLabels(obj metav1.Object) {
	annotations := obj.GetAnnotations()
	if annotations == nil {
		annotations = make(map[string]string)
		obj.SetAnnotations(annotations)
	}
	labels := obj.GetLabels()
	if labels == nil {
		labels = make(map[string]string)
		obj.SetLabels(labels)
	}
}

func (i *Annotations) conditionsLengthProtect(groupKind schema.GroupKind, obj runtime.Object) {
	status, ok := obj.(devops.StatusAccessor)
	if !ok {
		return
	}

	if len(status.GetStatus().Conditions) <= MaxConditionLen {
		return
	}

	resName := fmt.Sprintf("%#v", obj)
	res := obj.(metav1.Object)
	if res != nil {
		resName = fmt.Sprintf("%s/%s/%s", groupKind.String(), res.GetNamespace(), res.GetName())
	}

	glog.Errorf("!!!!! TO BE VERY CAREFUL !!!! length of CONDITIONs on resource %s is over the threshold:%d, will reset it to empty. current conditions is:%#v", resName, MaxConditionLen, status.GetStatus().Conditions)
	status.GetStatus().Conditions = []devops.BindingCondition{}
	return
}
