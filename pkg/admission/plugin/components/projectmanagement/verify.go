package projectmanagement

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/api/errors"
	metainternalversion "k8s.io/apimachinery/pkg/apis/meta/internalversion"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

var _ base.Middleware = &ProjectManagementVerify{}

type ProjectManagementVerify struct {
}

func (c *ProjectManagementVerify) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	service := attr.GetObject().(*devops.ProjectManagement)
	var (
		serviceList []devops.ProjectManagement
	)
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), service.GetNamespace())
	obj, err := d.GetDependencyManager().List(ctx, devops.TypeProjectManagement, &metainternalversion.ListOptions{
		LabelSelector: labels.Everything()})
	if err != nil {
		return shouldContinue, err
	}
	if list, ok := obj.(*devops.ProjectManagementList); ok {
		serviceList = list.Items
	}
	if serviceList != nil && len(serviceList) > 0 {
		for _, s := range serviceList {
			if s.GetName() != service.GetName() && common.CompareNoSuffix(s.Spec.HTTP.Host, service.Spec.HTTP.Host) {
				return shouldContinue, errors.NewBadRequest(fmt.Sprintf("the host '%s' has been used by service '%s'", service.Spec.HTTP.Host, s.Name))
			}
		}
	}
	return shouldContinue, err
}
