package projectmanagement

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

var _ base.Middleware = &ProjectManagementBindingMutation{}

type ProjectManagementBindingMutation struct {
}

func (c *ProjectManagementBindingMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	binding := attr.GetObject().(*devops.ProjectManagementBinding)
	projectManagement := &devops.ProjectManagement{}
	secret := &corev1.Secret{}
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), binding.Namespace)
	dependency := d.GetDependencyManager().GetWithObject(ctx, devops.TypeProjectManagementBinding, binding)
	dependency.GetInto(devops.TypeSecret, secret).GetInto(devops.TypeProjectManagement, projectManagement)
	if err = dependency.Validate(); err != nil {
		return
	}

	// annotation for public start

	structNameForLog := "ProjectManagementBindingMutation"

	methodNameForLog := "Handle"

	err = common.ProcessBindingLabelForToolPublic(&binding.ObjectMeta, projectManagement, d.GetProvider().BaseDomain, structNameForLog, methodNameForLog)
	if err != nil {
		return false, err
	}

	if len(binding.OwnerReferences) == 0 {
		binding.OwnerReferences = []metav1.OwnerReference{
			*common.OwnerReference(projectManagement, devops.TypeProjectManagement),
		}
	}
	MutateBindingResource(devops.TypeProjectManagementBinding, binding, projectManagement, secret, d.GetProvider())

	err = common.MutateNamespaceResource(d.GetDependencyManager(), binding.Namespace, binding.Labels, binding.Annotations)
	return
}

func MutateBindingResource(kind string, binding metav1.Object, tool devops.ToolInterface, secret *corev1.Secret, provider devops.AnnotationProvider) {
	annotations := binding.GetAnnotations()
	if annotations == nil {
		annotations = map[string]string{}
	}
	annotations[provider.LabelToolItemKind()] = kind
	annotations[provider.LabelToolItemType()] = tool.GetKindType()
	annotations[provider.AnnotationsToolAccessURL()] = tool.GetToolSpec().HTTP.AccessURL
	if secret != nil {
		annotations[provider.AnnotationsSecretType()] = string(secret.Type)
	}
	binding.SetAnnotations(annotations)
}
