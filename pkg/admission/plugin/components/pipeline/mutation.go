package pipeline

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"reflect"
)

var _ base.Middleware = &PipelineMutation{}

type PipelineMutation struct {
}

func (c *PipelineMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	pipeline := attr.GetObject().(*devops.Pipeline)
	namespace := pipeline.GetNamespace()
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), namespace)
	dependencies := d.GetDependencyManager().GetWithObject(ctx, devops.TypePipeline, pipeline)
	if err = dependencies.Validate(); err != nil {
		return
	}
	config := &devops.PipelineConfig{}
	jenkins := &devops.Jenkins{}
	dependencies.GetInto(devops.TypePipelineConfig, config).
		GetInto(devops.TypeJenkins, jenkins)

	pipeline.Labels[devops.LabelJenkins] = jenkins.Name

	// already have a number.. quit
	if pipeline.Annotations[d.GetProvider().AnnotationsKeyPipelineNumber()] != "" {
		return
	}

	pipeline.Labels[devops.LabelPipelineConfig] = pipeline.Spec.PipelineConfig.Name
	pipeline.Annotations[d.GetProvider().AnnotationsKeyPipelineConfig()] = pipeline.Spec.PipelineConfig.Name
	if !reflect.DeepEqual(config, &devops.PipelineConfig{}) {
		// Adding owner reference to delete all pipelines
		// when the PipelineConfig is deleted
		pipeline.OwnerReferences = []metav1.OwnerReference{
			*common.OwnerReference(config, devops.TypePipelineConfig),
		}
	}
	return
}
