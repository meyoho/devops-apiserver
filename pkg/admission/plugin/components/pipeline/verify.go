package pipeline

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	k8sutil "alauda.io/devops-apiserver/pkg/util/k8s"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

var _ base.Middleware = &PipelineVerify{}

type PipelineVerify struct {
	client  devopsclient.Interface
	manager dependency.Manager
}

func (c *PipelineVerify) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	c.client = d.GetDevopsClient()
	c.manager = d.GetDependencyManager()
	pipeline := attr.GetObject().(*devops.Pipeline)
	resource := attr.GetResource()

	config := &devops.PipelineConfig{}
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), pipeline.Namespace)
	dependency := c.manager.GetWithObject(ctx, devops.TypePipeline, pipeline)
	if err = dependency.Validate(); err != nil {
		return
	}
	dependency.GetInto(devops.TypePipelineConfig, config)

	// do not allow create (running PipelineConfig) if the PipelineConfig was disabled
	if attr.GetOperation() == admission.Create {
		if err = c.verifyIfRunnable(config, &resource); err != nil {
			return
		}
	}

	if err = c.verifyPipelineParams(pipeline, config, &resource); err != nil {
		return
	}
	return
}

func (d *PipelineVerify) verifyIfRunnable(config *devops.PipelineConfig, resource *schema.GroupVersionResource) (err error) {
	if config.Spec.Disabled {
		err = errors.NewForbidden(
			resource.GroupResource(),
			config.Name,
			fmt.Errorf("%s is disabled, it's not runnable", config.Name),
		)
	}
	return
}

func (d *PipelineVerify) verifyPipelineParams(pipeline *devops.Pipeline, config *devops.PipelineConfig, resource *schema.GroupVersionResource) (err error) {
	// do not verify the params when the pipeline was replayed from another one
	// it might not matches with the PipelineConfig
	skip := k8sutil.HasMultibranchLabel(config.Labels) ||
		(pipeline.Labels != nil && pipeline.Labels[devops.LabelReplayedFrom] != "")
	if !skip {
		err = d.pipelineParamsProcess(pipeline, config, resource)
	}

	return
}

func (d *PipelineVerify) pipelineParamsProcess(pipeline *devops.Pipeline, config *devops.PipelineConfig, resource *schema.GroupVersionResource) (err error) {
	cfgParams := config.Spec.Parameters
	pipParams := pipeline.Spec.Parameters
	var lackParams []devops.PipelineParameter
	if len(cfgParams) == 0 {
		// no params exists when a pipeline have not been triggered
		return
	}

	for _, pipeParam := range pipParams {
		var fond bool

		for _, param := range cfgParams {
			if param.Name == pipeParam.Name && param.Type == pipeParam.Type {
				fond = true
				break
			}
		}

		if !fond {
			lackParams = append(lackParams, pipeParam)
		}
	}

	if len(lackParams) > 0 {
		pipelineName := pipeline.GetName()
		err = errors.NewForbidden(
			resource.GroupResource(),
			pipelineName,
			fmt.Errorf("can not found params %v in PipelineConfig \"%s\"", lackParams, config.Name),
		)
	}

	return
}
