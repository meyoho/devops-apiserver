package components_test

import (
	"fmt"
	"testing"
	"time"

	"alauda.io/devops-apiserver/pkg/dependency"

	"alauda.io/devops-apiserver/pkg/admission/devopsinitializer"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/fake"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apiserver/pkg/admission"
	k8sinformer "k8s.io/client-go/informers"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	clienttesting "k8s.io/client-go/testing"
)

func TestVerifyDependencyAdmissionPlugin(t *testing.T) {
	var scenarios = []struct {
		jenkinsListOutput              *devops.JenkinsList
		jenkinsBindingListOutput       *devops.JenkinsBindingList
		pipelineConfigListOutput       *devops.PipelineConfigList
		codeRepoServiceListOutput      *devops.CodeRepoServiceList
		codeRepoBindingListOutput      *devops.CodeRepoBindingList
		imageRegistryListOutput        *devops.ImageRegistryList
		imageRegistryBindingListOutput *devops.ImageRegistryBinding
		namespaceOutput                *corev1.NamespaceList
		secretOutput                   *corev1.SecretList
		admissionInput                 runtime.Object
		inputNamespace                 string
		admissionInputKind             schema.GroupVersionKind
		admissionInputResource         schema.GroupVersionResource
		secretDependency               *devops.SecretKeySetRef
		admissionOperation             admission.Operation
		admissionMustFail              bool
		manager                        dependency.Manager
	}{
		// scenario 0:
		// create codereposervice has same host
		{
			codeRepoServiceListOutput: &devops.CodeRepoServiceList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.CodeRepoService{
					devops.CodeRepoService{
						ObjectMeta: metav1.ObjectMeta{
							Name: "codereposervice",
						},
						Spec: devops.CodeRepoServiceSpec{
							ToolSpec: devops.ToolSpec{
								HTTP: devops.HostPort{
									Host: "https://mayun.sparrow.host",
								},
							},
							Type: devops.CodeRepoServiceTypeGitee,
						},
					},
				},
			},
			admissionInput: &devops.CodeRepoService{
				ObjectMeta: metav1.ObjectMeta{
					Name: "codereposervice1",
				},
				Spec: devops.CodeRepoServiceSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "https://mayun.sparrow.host",
						},
					},
					Type: devops.CodeRepoServiceTypeGitee,
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("CodeRepoService").WithVersion("version"),
			admissionInputResource: devops.Resource("CodeRepoService").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
		// scenario 1:
		// update codereposervice has same host
		{
			codeRepoServiceListOutput: &devops.CodeRepoServiceList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.CodeRepoService{
					devops.CodeRepoService{
						ObjectMeta: metav1.ObjectMeta{
							Name: "codereposervice",
						},
						Spec: devops.CodeRepoServiceSpec{
							ToolSpec: devops.ToolSpec{
								HTTP: devops.HostPort{
									Host: "https://mayun.sparrow.host",
								},
							},
							Type: devops.CodeRepoServiceTypeGitee,
						},
					},
					devops.CodeRepoService{
						ObjectMeta: metav1.ObjectMeta{
							Name: "codereposervice1",
						},
						Spec: devops.CodeRepoServiceSpec{
							ToolSpec: devops.ToolSpec{
								HTTP: devops.HostPort{
									Host: "https://mayun.sparrow.host1",
								},
							},
							Type: devops.CodeRepoServiceTypeGitee,
						},
					},
				},
			},
			admissionInput: &devops.CodeRepoService{
				ObjectMeta: metav1.ObjectMeta{
					Name: "codereposervice",
				},
				Spec: devops.CodeRepoServiceSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "https://mayun.sparrow.host1",
						},
					},
					Type: devops.CodeRepoServiceTypeGitee,
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("CodeRepoService").WithVersion("version"),
			admissionInputResource: devops.Resource("CodeRepoService").WithVersion("version"),
			admissionOperation:     admission.Update,
			admissionMustFail:      true,
		},
		// scenario 3:
		// ok
		{
			codeRepoServiceListOutput: &devops.CodeRepoServiceList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.CodeRepoService{
					devops.CodeRepoService{
						ObjectMeta: metav1.ObjectMeta{
							Name: "codereposervice",
						},
						Spec: devops.CodeRepoServiceSpec{
							ToolSpec: devops.ToolSpec{
								HTTP: devops.HostPort{
									Host: "https://mayun.sparrow.host",
								},
							},
							Type: devops.CodeRepoServiceTypeGitee,
						},
					},
				},
			},
			admissionInput: &devops.CodeRepoService{
				ObjectMeta: metav1.ObjectMeta{
					Name: "codereposervice1",
				},
				Spec: devops.CodeRepoServiceSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "https://mayun.sparrow.host1",
						},
					},
					Type: devops.CodeRepoServiceTypeGitee,
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("CodeRepoService").WithVersion("version"),
			admissionInputResource: devops.Resource("CodeRepoService").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      false,
		},
		// scenario 4:
		// create codereposervice has same host
		{
			codeRepoServiceListOutput: &devops.CodeRepoServiceList{
				ListMeta: metav1.ListMeta{},
				Items: []devops.CodeRepoService{
					devops.CodeRepoService{
						ObjectMeta: metav1.ObjectMeta{
							Name: "codereposervice",
						},
						Spec: devops.CodeRepoServiceSpec{
							ToolSpec: devops.ToolSpec{
								HTTP: devops.HostPort{
									Host: "https://mayun.sparrow.host/",
								},
							},
							Type: devops.CodeRepoServiceTypeGitee,
						},
					},
				},
			},
			admissionInput: &devops.CodeRepoService{
				ObjectMeta: metav1.ObjectMeta{
					Name: "codereposervice1",
				},
				Spec: devops.CodeRepoServiceSpec{
					ToolSpec: devops.ToolSpec{
						HTTP: devops.HostPort{
							Host: "https://mayun.sparrow.host",
						},
					},
					Type: devops.CodeRepoServiceTypeGitee,
				},
			},
			inputNamespace:         "default",
			admissionInputKind:     devops.Kind("CodeRepoService").WithVersion("version"),
			admissionInputResource: devops.Resource("CodeRepoService").WithVersion("version"),
			admissionOperation:     admission.Create,
			admissionMustFail:      true,
		},
	}

	for index, scenario := range scenarios {
		func() {
			// prepare
			cs := &fake.Clientset{}
			if scenario.jenkinsListOutput != nil {
				cs.AddReactor("list", "jenkinses", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.jenkinsListOutput, nil
				})
			}
			if scenario.jenkinsBindingListOutput != nil {
				cs.AddReactor("list", "jenkinsbindings", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.jenkinsBindingListOutput, nil
				})
			}
			if scenario.pipelineConfigListOutput != nil {
				cs.AddReactor("list", "pipelineconfigs", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.pipelineConfigListOutput, nil
				})
			}
			if scenario.codeRepoServiceListOutput != nil {
				cs.AddReactor("list", "codereposervices", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.codeRepoServiceListOutput, nil
				})
			}
			if scenario.codeRepoBindingListOutput != nil {
				cs.AddReactor("list", "coderepobindings", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.codeRepoBindingListOutput, nil
				})
			}
			if scenario.imageRegistryListOutput != nil {
				cs.AddReactor("list", "imageregistries", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.imageRegistryListOutput, nil
				})
			}
			if scenario.imageRegistryBindingListOutput != nil {
				cs.AddReactor("list", "imageregistrybindings", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.imageRegistryBindingListOutput, nil
				})
			}
			informersFactory := informers.NewSharedInformerFactory(cs, 5*time.Minute)

			k8sclient := &k8sfake.Clientset{}
			if scenario.namespaceOutput != nil {
				k8sclient.AddReactor("list", "namespaces", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.namespaceOutput, nil
				})
			}
			if scenario.secretOutput != nil {
				k8sclient.AddReactor("list", "secrets", func(action clienttesting.Action) (bool, runtime.Object, error) {
					return true, scenario.secretOutput, nil
				})
			}
			if scenario.secretDependency != nil {
				k8sclient.AddReactor("get", "secrets", func(action clienttesting.Action) (bool, runtime.Object, error) {
					secretFullName := fmt.Sprintf("%s/%s", scenario.secretDependency.Namespace, scenario.secretDependency.Name)
					if scenario.secretOutput == nil || len(scenario.secretOutput.Items) == 0 {
						return true, nil, errors.NewNotFound(corev1.Resource("secrets"), secretFullName)
					}

					for _, secret := range scenario.secretOutput.Items {
						if secret.Name == scenario.secretDependency.Name {
							if scenario.secretDependency.Namespace == "" {
								return true, &secret, nil
							}

							if scenario.secretDependency.Namespace == secret.Namespace {
								return true, &secret, nil
							}
						}
					}

					return true, nil, errors.NewNotFound(corev1.Resource("secrets"), secretFullName)
				})
			}

			k8sInformerFactory := k8sinformer.NewSharedInformerFactory(k8sclient, 5*time.Minute)

			target, err := components.New()
			if err != nil {
				t.Fatalf("scenario %d: failed to create banflunder admission plugin due to = %v", index, err)
			}
			targetInitializer := devopsinitializer.New(informersFactory, k8sInformerFactory, cs, k8sclient, dependency.NewManager(), devops.NewAnnotationProvider(devops.UsedBaseDomain))
			targetInitializer.Initialize(target)

			err = admission.ValidateInitialization(target)
			if err != nil {
				t.Fatalf("scenario %d: failed to initialize banflunder admission plugin due to =%v", index, err)
			}

			stop := make(chan struct{})
			defer close(stop)
			informersFactory.Start(stop)
			informersFactory.WaitForCacheSync(stop)
			k8sInformerFactory.Start(stop)
			k8sInformerFactory.WaitForCacheSync(stop)

			// act
			err = target.Admit(admission.NewAttributesRecord(
				scenario.admissionInput,
				nil,
				scenario.admissionInputKind,
				scenario.inputNamespace,
				"",
				scenario.admissionInputResource,
				"",
				scenario.admissionOperation,
				false,
				nil),
			)

			// validate
			if scenario.admissionMustFail && err == nil {
				t.Errorf("scenario %d: expected an error but got nothing", index)
			}
			// t.Logf("index: %d  scenario: %v", index, scenario)
			if !scenario.admissionMustFail && err != nil {
				t.Errorf("scenario %d: verify dependency admission plugin returned unexpected error = %v", index, err)
			}
		}()
	}
}
