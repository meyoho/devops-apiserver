package pipelineconfig

import (
	"fmt"
	"strings"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	devopsclient "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	glog "k8s.io/klog"
)

var _ base.Middleware = &PipelineConfigBindingMutation{}

type PipelineConfigBindingMutation struct {
	client  devopsclient.Interface
	manager dependency.Manager
}

func (c *PipelineConfigBindingMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	c.client = d.GetDevopsClient()
	c.manager = d.GetDependencyManager()

	pipelineConfig := attr.GetObject().(*devops.PipelineConfig)
	namespace := pipelineConfig.GetNamespace()

	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), namespace)
	dependencies := c.manager.GetWithObject(ctx, devops.TypePipelineConfig, pipelineConfig)
	if err = dependencies.Validate(); err != nil {
		return
	}

	jenkinsBinding := &devops.JenkinsBinding{}
	jenkins := &devops.Jenkins{}
	dependencies.GetInto(devops.TypeJenkinsBinding, jenkinsBinding).
		GetInto(devops.TypeJenkins, jenkins)

	pipelineConfig.Labels[devops.LabelJenkins] = jenkins.Name

	var codeRepository *devops.CodeRepository
	codeRepoBinding := &devops.CodeRepoBinding{}
	if pipelineConfig.Spec.Source.CodeRepository != nil {
		codeRepository, err = c.client.Devops().CodeRepositories(namespace).Get(pipelineConfig.Spec.Source.CodeRepository.Name, metav1.GetOptions{ResourceVersion: "0"})
		if err != nil {
			return
		}
		if codeRepository != nil {
			ctx = genericapirequest.WithNamespace(ctx, codeRepository.Namespace)
			dependencies := c.manager.GetWithObject(ctx, devops.TypeCodeRepository, codeRepository)
			if err = dependencies.Validate(); err != nil {
				return
			}
			dependencies.GetInto(devops.TypeCodeRepoBinding, codeRepoBinding)
		}
	} else {
		codeRepoBinding = nil
	}
	err = c.mutatePipelineConfig(pipelineConfig, jenkinsBinding, codeRepository, codeRepoBinding)
	return
}

func (d *PipelineConfigBindingMutation) mutatePipelineConfig(pipelineConfig *devops.PipelineConfig, jenkinsBinding *devops.JenkinsBinding, codeRepository *devops.CodeRepository, codeRepoBinding *devops.CodeRepoBinding) error {
	pipelineConfig.OwnerReferences = common.AppendOwnerReference(pipelineConfig.OwnerReferences, *common.OwnerReference(jenkinsBinding, devops.TypeJenkinsBinding))

	if codeRepository != nil && codeRepoBinding != nil {
		pipelineConfig.Spec.Source.Git = &devops.PipelineSourceGit{
			URI: codeRepository.Spec.Repository.CloneURL,
			Ref: pipelineConfig.Spec.Source.CodeRepository.Ref,
		}
		pipelineConfig.Spec.Source.Secret = &codeRepoBinding.Spec.Account.Secret
	}
	if len(pipelineConfig.Spec.Triggers) > 0 {
		for i, t := range pipelineConfig.Spec.Triggers {
			pipelineConfig.Spec.Triggers[i] = d.mutatePipelineConfigTrigger(t)
		}
	}

	template, err := d.versionedPipelineTemplateRef(pipelineConfig)
	if err != nil {
		return err
	}
	appendTemplateLabel(pipelineConfig, template)

	return err
}

func (d *PipelineConfigBindingMutation) mutatePipelineConfigTrigger(trigger devops.PipelineTrigger) devops.PipelineTrigger {
	if trigger.Type != devops.PipelineTriggerTypeCron || trigger.Cron == nil || trigger.Cron.Schedule == nil {
		return trigger
	}

	schedule := trigger.Cron.Schedule
	weeks, minutes, hours := "", "", ""

	// conbine the weeks and times (minutes, hours)
	for _, week := range schedule.Weeks {
		weeks += string(week) + ","
	}
	for _, time := range schedule.Times {
		array := strings.Split(time, ":")
		if len(array) < 2 {
			continue
		}

		hours += array[0] + ","
		minutes += array[1] + ","
	}
	// trim redundant characters
	trigger.Cron.Rule = transformCronRule(minutes, hours, weeks)

	return trigger
}

func transformCronRule(minutes, hours, weeks string) string {
	minutes = trimAsCron(minutes)
	hours = trimAsCron(hours)
	weeks = trimAsCron(transformWeek(weeks))

	return fmt.Sprintf("%s %s * * %s", minutes, hours, weeks)
}

func trimAsCron(text string) string {
	if len(text) > 1 {
		return strings.TrimSuffix(text, ",")
	} else {
		return "*"
	}
}

// TransformWeek transform week expression to number
func transformWeek(text string) string {
	text = strings.Replace(text, "mon", "1", -1)
	text = strings.Replace(text, "tue", "2", -1)
	text = strings.Replace(text, "wed", "3", -1)
	text = strings.Replace(text, "thu", "4", -1)
	text = strings.Replace(text, "fri", "5", -1)
	text = strings.Replace(text, "sat", "6", -1)
	text = strings.Replace(text, "sun", "7", -1)
	return text
}

func (d *PipelineConfigBindingMutation) versionedPipelineTemplateRef(pipelineConfig *devops.PipelineConfig) (template devops.PipelineTemplateInterface, err error) {

	if pipelineConfig.Spec.Template.PipelineTemplateRef.Name == "" {
		// templateRef.Name is still empty, it must not created by template
		return nil, nil
	}

	templateRef := pipelineConfig.Spec.Template.PipelineTemplateRef
	var pipelineTemplate devops.PipelineTemplateInterface
	if templateRef.Kind == devops.TypeClusterPipelineTemplate {
		pipelineTemplate, err = d.client.Devops().ClusterPipelineTemplates().Get(templateRef.Name, metav1.GetOptions{ResourceVersion: "0"})
	} else if templateRef.Kind == devops.TypePipelineTemplate {
		pipelineTemplate, err = d.client.Devops().PipelineTemplates(templateRef.Namespace).Get(templateRef.Name, metav1.GetOptions{ResourceVersion: "0"})
	} else {
		glog.Errorf("template kind '%s' referenced by pipeline config '%s/%s' is not supported.", templateRef.Kind, pipelineConfig.Namespace, pipelineConfig.Name)
		return nil, templateKindNotAccept(pipelineConfig)
	}

	if err != nil {
		glog.Errorf("Get template %v error:%s", templateRef, err)
		return nil, err
	}

	versionedName, err := pipelineTemplate.GetVersionedName()
	if err != nil {
		glog.Errorf("Cannot get version name of %s", pipelineTemplate)
		return nil, err
	}

	_, err = clientset.NewExpansion(d.client).DevopsExpansion().PipelineTemplateInterface().Get(pipelineTemplate.GetKind(), pipelineTemplate.GetNamespace(), versionedName, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		if errors.IsNotFound(err) {
			glog.Errorf("Versioned template %s is not found, will use unversioned template %s", versionedName, pipelineTemplate)
			versionedName = pipelineTemplate.GetName()
		} else {
			glog.Errorf("Get template %s/%s error:%s", pipelineTemplate.GetKind(), versionedName, err.Error())
			return nil, err
		}
	}

	oldRefName := pipelineConfig.Spec.Template.PipelineTemplateRef.Name
	pipelineConfig.Spec.Template.PipelineTemplateRef.Name = versionedName
	pipelineConfig.Spec.Parameters = pipelineTemplate.GetPiplineTempateSpec().Parameters

	glog.V(5).Infof("mutate PipelineConfig %s/%s templateRef.Name from %s to %s", pipelineConfig.Namespace, pipelineConfig.Name, oldRefName, versionedName)
	return pipelineTemplate, nil
}

func templateKindNotAccept(pipelineConfig *devops.PipelineConfig) error {
	return errors.NewInvalid(
		pipelineConfig.GetObjectKind().GroupVersionKind().GroupKind(),
		pipelineConfig.Name, field.ErrorList{
			field.Invalid(
				field.NewPath("spec").Child("strategy", "template", "kind"),
				pipelineConfig.Spec.Template.PipelineTemplateRef.Kind,
				"Template Kind should be ClusterPipelineTemplate or PipelineTemplate",
			),
		},
	)
}

func appendTemplateLabel(config *devops.PipelineConfig, template devops.PipelineTemplateInterface) {
	if config.ObjectMeta.Labels == nil {
		config.ObjectMeta.Labels = make(map[string]string)
	}

	if config.ObjectMeta.Annotations == nil {
		config.ObjectMeta.Annotations = make(map[string]string)
	}

	if template != nil {
		// template name with out version
		templateName := template.GetAnnotations()[devops.AnnotationsTemplateName]
		if templateName == "" {
			glog.Infof("Append templateName=%s to pipelineconfig labels, because template %s has no templateName annotation, it must be a unversioned template", template.GetName(), template)
			templateName = template.GetName()
		}
		config.ObjectMeta.Labels[devops.LabelTemplateCategory] = template.GetLabels()[devops.LabelTemplateCategory]
		config.ObjectMeta.Labels[devops.LabelTemplateKind] = template.GetKind()
		config.ObjectMeta.Labels[devops.LabelTemplateName] = templateName
		config.ObjectMeta.Labels[devops.LabelTemplateVersion] = template.GetAnnotations()[devops.AnnotationsTemplateVersion]
		return
	}
}
