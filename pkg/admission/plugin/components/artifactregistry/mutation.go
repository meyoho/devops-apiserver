package artifactregistry

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/common"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/admission"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

type ArtifactRegistryBindingMutation struct {
}

func (c *ArtifactRegistryBindingMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	binding := attr.GetObject().(*devops.ArtifactRegistryBinding)
	artifactRegistry := &devops.ArtifactRegistry{}
	secret := &corev1.Secret{}
	ctx := genericapirequest.WithNamespace(genericapirequest.NewContext(), binding.Namespace)
	dependency := d.GetDependencyManager().GetWithObject(ctx, devops.TypeArtifactRegistryBinding, binding)
	if err = dependency.Validate(); err != nil {
		return
	}
	dependency.GetInto(devops.TypeArtifactRegistry, artifactRegistry).GetInto(devops.TypeSecret, secret)

	// annotation for public start

	structNameForLog := "ArtifactRegistryBindingMutation"

	methodNameForLog := "Handle"

	err = common.ProcessBindingLabelForToolPublic(&binding.ObjectMeta, artifactRegistry, d.GetProvider().BaseDomain, structNameForLog, methodNameForLog)
	if err != nil {
		return false, err
	}

	binding.Labels[devops.LabelArtifactRegistryType] = artifactRegistry.Spec.Type
	binding.Labels[devops.LabelArtifactRegistry] = artifactRegistry.GetName()
	if len(binding.OwnerReferences) == 0 {
		binding.OwnerReferences = []metav1.OwnerReference{
			*common.OwnerReference(artifactRegistry.GetObjectMeta(), artifactRegistry.GetKind()),
		}
	}

	common.MutateBindingResource(devops.TypeArtifactRegistry, binding, artifactRegistry, secret, d.GetProvider())
	err = common.MutateNamespaceResource(d.GetDependencyManager(), binding.Namespace, binding.Labels, binding.Annotations)
	return
}

type ArtifactRegistryMutation struct {
}

func (c *ArtifactRegistryMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	artifactRegistry := attr.GetObject().(*devops.ArtifactRegistry)
	if len(artifactRegistry.GetOwnerReferences()) == 0 {
		return
	}
	artifactRegistryManager, err := d.GetDevopsClient().Devops().ArtifactRegistryManagers().Get(artifactRegistry.OwnerReferences[0].Name, v1alpha1.GetOptions())

	if err != nil {
		return
	}

	artifactRegistry.Labels[devops.LabelArtifactRegistryManager] = artifactRegistryManager.GetName()

	return
}
