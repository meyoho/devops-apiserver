package components

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/artifactregistry"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/artifactregistrymanager"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/codequality"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/coderepository"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/documentmanagement"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/imageregistry"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/jenkins"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/pipeline"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/pipelineconfig"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/pipelinetasktemplate"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/pipelinetemplate"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/pipelinetemplatesync"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/projectmanagement"
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/toolbindingreplica"
	"alauda.io/devops-apiserver/pkg/apis/devops"
)

func init() {
	DefaultMiddlewares = append(DefaultMiddlewares,
		&CheckStatus{},
		&Annotations{},
		&Checksum{},
		&Dispatcher{
			// ArtifactRegistryManager
			devops.TypeArtifactRegistryManager: {
				&artifactregistrymanager.ArtifactRegistryManagerVerify{}},
			// ArtifactRegistryBinding
			devops.TypeArtifactRegistry: {
				&artifactregistry.ArtifactRegistryMutation{}},
			// ArtifactRegistryBinding
			devops.TypeArtifactRegistryBinding: {
				&artifactregistry.ArtifactRegistryBindingMutation{}},
			//devops.TypeArtifactRegistry: {
			//	&artifactregistry.ArtifactRegistryVerify{}},
			// CodeQualityBinding
			devops.TypeCodeQualityBinding: {
				&codequality.CodeQualityBindingMutation{}},
			// CodeQualityTool
			devops.TypeCodeQualityTool: {
				&codequality.CodeQualityToolVerify{}},
			// CodeQualityProject
			devops.TypeCodeQualityProject: {
				&codequality.CodeQualityProjectMutation{}},
			// CodeRepoBinding
			devops.TypeCodeRepoBinding: {
				&coderepository.CodeRepoBindingMutation{}},
			// CodeRepository
			devops.TypeCodeRepository: {
				&coderepository.CodeRepositoryMutation{}},
			// CodeRepoService
			devops.TypeCodeRepoService: {
				&coderepository.CodeRepoServiceVerify{}},
			// DocumentManagementBinding
			devops.TypeDocumentManagementBinding: {
				&documentmanagement.DocumentManagementBindingMutation{}},
			// ImageRegistryBinding
			devops.TypeImageRegistryBinding: {
				&imageregistry.ImageRegistryBindingMutation{}},
			// ImageRepository
			devops.TypeImageRepository: {
				&imageregistry.ImageRepositoryMutation{}},
			// ImageRegistry
			devops.TypeImageRegistry: {
				&imageregistry.ImageRegistryVerify{}},
			// JenkinsBinding
			devops.TypeJenkinsBinding: {
				&jenkins.JenkinsBindingMutation{}},
			// Jenkins
			devops.TypeJenkins: {
				&jenkins.JenkinsVerify{}},
			// Pipeline
			devops.TypePipeline: {
				&pipeline.PipelineVerify{},
				&pipeline.PipelineMutation{}},
			// PipelineConfig
			devops.TypePipelineConfig: {
				&pipelineconfig.PipelineConfigBindingMutation{}},
			// PipelineTemplate
			devops.TypePipelineTemplate: {
				&pipelinetemplate.PipelineTemplateMutation{}},
			// ClusterPipelineTemplate
			devops.TypeClusterPipelineTemplate: {
				&pipelinetemplate.ClusterPipelineTemplateMutation{}},
			// PipelineTemplate
			devops.TypePipelineTaskTemplate: {
				&pipelinetasktemplate.PipelineTaskTemplateMutation{}},
			// ClusterPipelineTemplate
			devops.TypeClusterPipelineTaskTemplate: {
				&pipelinetasktemplate.ClusterPipelineTaskTemplateMutation{}},
			// PipelineTemplateSync
			devops.TypePipelineTemplateSync: {
				&pipelinetemplatesync.PipelineTemplateSyncMutation{}},
			// ProjectManagementBinding
			devops.TypeProjectManagementBinding: {
				&projectmanagement.ProjectManagementBindingMutation{}},
			devops.TypeProjectManagement: {
				&projectmanagement.ProjectManagementVerify{}},
			// TypeToolBindingReplica
			devops.TypeToolBindingReplica: {
				&toolbindingreplica.ToolBindingReplicaMutation{}},
		},
	)
}
