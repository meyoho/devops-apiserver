package components

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apiserver/pkg/admission"
	glog "k8s.io/klog"
)

var _ base.Middleware = &CheckStatus{}

type CheckStatus struct {
}

func (i *CheckStatus) Handle(d base.PluginRequire, attr admission.Attributes) (bool, error) {
	_, err := meta.Accessor(attr.GetObject())
	if err != nil {
		if devops.IgnoredKinds.Has(attr.GetKind().Kind) {
			return false, nil
		}

		glog.Errorf("Admit Accessor err: %v", err)
		return false, err
	}

	// we don't need to verify dependency when update status
	if attr.GetSubresource() == "status" {
		return false, nil
	}
	return true, nil
}
