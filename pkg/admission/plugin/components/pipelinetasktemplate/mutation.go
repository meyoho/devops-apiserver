package pipelinetasktemplate

import (
	"alauda.io/devops-apiserver/pkg/admission/plugin/components/base"
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"k8s.io/apiserver/pkg/admission"
)

var _ base.Middleware = &PipelineTaskTemplateMutation{}
var _ base.Middleware = &ClusterPipelineTaskTemplateMutation{}

type PipelineTaskTemplateMutation struct {
}

func (c *PipelineTaskTemplateMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	template := attr.GetObject().(*devops.PipelineTaskTemplate)
	mutatePipelineTaskTemplate(template)
	return
}

type ClusterPipelineTaskTemplateMutation struct {
}

func (c *ClusterPipelineTaskTemplateMutation) Handle(d base.PluginRequire, attr admission.Attributes) (shouldContinue bool, err error) {
	shouldContinue = true
	template := attr.GetObject().(*devops.ClusterPipelineTaskTemplate)
	mutatePipelineTaskTemplate(template)
	return
}

func mutatePipelineTaskTemplate(template devops.PipelineTaskTemplateInterface) {
	//test this in devops-apiserver/pkg/admission/plugin/components/pipelinetemplate/pipelinetemplate_test
	k8s.CompatiableBaseDomain(template)

	// set finalizers if no exist
	if len(template.GetFinalizers()) > 0 {
		return
	}
	if template.GetDeletionTimestamp() == nil || template.GetDeletionTimestamp().IsZero() {
		template.SetFinalizers([]string{devops.FinalizerPipelineTemplateReferenced})
	}
}
