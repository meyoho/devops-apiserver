// Code generated by MockGen. DO NOT EDIT.
// Source: alauda.io/devops-apiserver/pkg/client/clientset/versioned/typed/devops/v1alpha1 (interfaces: ProjectManagementBindingExpansion)

// Package controller is a generated GoMock package.
package controller

import (
	v1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	gomock "github.com/golang/mock/gomock"
	reflect "reflect"
)

// MockProjectManagementBindingExpansion is a mock of ProjectManagementBindingExpansion interface
type MockProjectManagementBindingExpansion struct {
	ctrl     *gomock.Controller
	recorder *MockProjectManagementBindingExpansionMockRecorder
}

// MockProjectManagementBindingExpansionMockRecorder is the mock recorder for MockProjectManagementBindingExpansion
type MockProjectManagementBindingExpansionMockRecorder struct {
	mock *MockProjectManagementBindingExpansion
}

// NewMockProjectManagementBindingExpansion creates a new mock instance
func NewMockProjectManagementBindingExpansion(ctrl *gomock.Controller) *MockProjectManagementBindingExpansion {
	mock := &MockProjectManagementBindingExpansion{ctrl: ctrl}
	mock.recorder = &MockProjectManagementBindingExpansionMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockProjectManagementBindingExpansion) EXPECT() *MockProjectManagementBindingExpansionMockRecorder {
	return m.recorder
}

// IssueDetail mocks base method
func (m *MockProjectManagementBindingExpansion) IssueDetail(arg0 string, arg1 *v1alpha1.IssueKeyOptions) (*v1alpha1.IssueDetail, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IssueDetail", arg0, arg1)
	ret0, _ := ret[0].(*v1alpha1.IssueDetail)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// IssueDetail indicates an expected call of IssueDetail
func (mr *MockProjectManagementBindingExpansionMockRecorder) IssueDetail(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IssueDetail", reflect.TypeOf((*MockProjectManagementBindingExpansion)(nil).IssueDetail), arg0, arg1)
}

// IssueList mocks base method
func (m *MockProjectManagementBindingExpansion) IssueList(arg0 string, arg1 *v1alpha1.ListIssuesOptions) (*v1alpha1.IssueDetailList, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IssueList", arg0, arg1)
	ret0, _ := ret[0].(*v1alpha1.IssueDetailList)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// IssueList indicates an expected call of IssueList
func (mr *MockProjectManagementBindingExpansionMockRecorder) IssueList(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IssueList", reflect.TypeOf((*MockProjectManagementBindingExpansion)(nil).IssueList), arg0, arg1)
}

// IssueOptions mocks base method
func (m *MockProjectManagementBindingExpansion) IssueOptions(arg0 string, arg1 *v1alpha1.IssueSearchOptions) (*v1alpha1.IssueFilterDataList, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IssueOptions", arg0, arg1)
	ret0, _ := ret[0].(*v1alpha1.IssueFilterDataList)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// IssueOptions indicates an expected call of IssueOptions
func (mr *MockProjectManagementBindingExpansionMockRecorder) IssueOptions(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IssueOptions", reflect.TypeOf((*MockProjectManagementBindingExpansion)(nil).IssueOptions), arg0, arg1)
}
