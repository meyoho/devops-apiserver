// Code generated by MockGen. DO NOT EDIT.
// Source: alauda.io/devops-apiserver/pkg/registry/devops/pipeline (interfaces: URLBuilder)

// Package registry is a generated GoMock package.
package registry

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	generic "alauda.io/devops-apiserver/pkg/registry/generic"
	context "context"
	gomock "github.com/golang/mock/gomock"
	v1 "k8s.io/api/core/v1"
	v10 "k8s.io/client-go/listers/core/v1"
	url "net/url"
	reflect "reflect"
)

// MockURLBuilder is a mock of URLBuilder interface
type MockURLBuilder struct {
	ctrl     *gomock.Controller
	recorder *MockURLBuilderMockRecorder
}

// MockURLBuilderMockRecorder is the mock recorder for MockURLBuilder
type MockURLBuilderMockRecorder struct {
	mock *MockURLBuilder
}

// NewMockURLBuilder creates a new mock instance
func NewMockURLBuilder(ctrl *gomock.Controller) *MockURLBuilder {
	mock := &MockURLBuilder{ctrl: ctrl}
	mock.recorder = &MockURLBuilderMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockURLBuilder) EXPECT() *MockURLBuilderMockRecorder {
	return m.recorder
}

// GetPipelineDependencies mocks base method
func (m *MockURLBuilder) GetPipelineDependencies(arg0, arg1, arg2 generic.ResourceGetter, arg3 context.Context, arg4 string) (*devops.Pipeline, *devops.JenkinsBinding, *devops.Jenkins, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetPipelineDependencies", arg0, arg1, arg2, arg3, arg4)
	ret0, _ := ret[0].(*devops.Pipeline)
	ret1, _ := ret[1].(*devops.JenkinsBinding)
	ret2, _ := ret[2].(*devops.Jenkins)
	ret3, _ := ret[3].(error)
	return ret0, ret1, ret2, ret3
}

// GetPipelineDependencies indicates an expected call of GetPipelineDependencies
func (mr *MockURLBuilderMockRecorder) GetPipelineDependencies(arg0, arg1, arg2, arg3, arg4 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetPipelineDependencies", reflect.TypeOf((*MockURLBuilder)(nil).GetPipelineDependencies), arg0, arg1, arg2, arg3, arg4)
}

// GetSecret mocks base method
func (m *MockURLBuilder) GetSecret(arg0 v10.SecretLister, arg1 devops.UserAccount, arg2 string) (*v1.Secret, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSecret", arg0, arg1, arg2)
	ret0, _ := ret[0].(*v1.Secret)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetSecret indicates an expected call of GetSecret
func (mr *MockURLBuilderMockRecorder) GetSecret(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSecret", reflect.TypeOf((*MockURLBuilder)(nil).GetSecret), arg0, arg1, arg2)
}

// GetTestReportURL mocks base method
func (m *MockURLBuilder) GetTestReportURL(arg0 *devops.Pipeline, arg1 *devops.JenkinsBinding, arg2 *devops.Jenkins, arg3 *devops.PipelineTestReportOptions) (*url.URL, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetTestReportURL", arg0, arg1, arg2, arg3)
	ret0, _ := ret[0].(*url.URL)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetTestReportURL indicates an expected call of GetTestReportURL
func (mr *MockURLBuilderMockRecorder) GetTestReportURL(arg0, arg1, arg2, arg3 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetTestReportURL", reflect.TypeOf((*MockURLBuilder)(nil).GetTestReportURL), arg0, arg1, arg2, arg3)
}
