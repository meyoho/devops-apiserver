// Code generated by MockGen. DO NOT EDIT.
// Source: alauda.io/devops-apiserver/pkg/client/thirdparty/ace (interfaces: Interface)

// Package ace is a generated GoMock package.
package ace

import (
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty"
	ace "alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	context "context"
	gomock "github.com/golang/mock/gomock"
	http "net/http"
	reflect "reflect"
)

// MockInterface is a mock of Interface interface
type MockInterface struct {
	ctrl     *gomock.Controller
	recorder *MockInterfaceMockRecorder
}

// MockInterfaceMockRecorder is the mock recorder for MockInterface
type MockInterfaceMockRecorder struct {
	mock *MockInterface
}

// NewMockInterface creates a new mock instance
func NewMockInterface(ctrl *gomock.Controller) *MockInterface {
	mock := &MockInterface{ctrl: ctrl}
	mock.recorder = &MockInterfaceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockInterface) EXPECT() *MockInterfaceMockRecorder {
	return m.recorder
}

// CreateK8SResource mocks base method
func (m *MockInterface) CreateK8SResource(arg0 context.Context, arg1, arg2 string, arg3 interface{}, arg4 ...ace.RequestOptions) (*ace.KubernetesResource, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0, arg1, arg2, arg3}
	for _, a := range arg4 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "CreateK8SResource", varargs...)
	ret0, _ := ret[0].(*ace.KubernetesResource)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateK8SResource indicates an expected call of CreateK8SResource
func (mr *MockInterfaceMockRecorder) CreateK8SResource(arg0, arg1, arg2, arg3 interface{}, arg4 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0, arg1, arg2, arg3}, arg4...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateK8SResource", reflect.TypeOf((*MockInterface)(nil).CreateK8SResource), varargs...)
}

// CustomIterate mocks base method
func (m *MockInterface) CustomIterate(arg0 thirdparty.ListFunc, arg1 thirdparty.ListIterator) thirdparty.ListFunc {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CustomIterate", arg0, arg1)
	ret0, _ := ret[0].(thirdparty.ListFunc)
	return ret0
}

// CustomIterate indicates an expected call of CustomIterate
func (mr *MockInterfaceMockRecorder) CustomIterate(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CustomIterate", reflect.TypeOf((*MockInterface)(nil).CustomIterate), arg0, arg1)
}

// CustomMultipleIterate mocks base method
func (m *MockInterface) CustomMultipleIterate(arg0 thirdparty.ListFunc, arg1 ace.ContextGenerator, arg2 thirdparty.List, arg3 ace.MultipleIterator) (map[thirdparty.ListItem]thirdparty.List, []error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CustomMultipleIterate", arg0, arg1, arg2, arg3)
	ret0, _ := ret[0].(map[thirdparty.ListItem]thirdparty.List)
	ret1, _ := ret[1].([]error)
	return ret0, ret1
}

// CustomMultipleIterate indicates an expected call of CustomMultipleIterate
func (mr *MockInterfaceMockRecorder) CustomMultipleIterate(arg0, arg1, arg2, arg3 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CustomMultipleIterate", reflect.TypeOf((*MockInterface)(nil).CustomMultipleIterate), arg0, arg1, arg2, arg3)
}

// DeleteK8SResource mocks base method
func (m *MockInterface) DeleteK8SResource(arg0 context.Context, arg1, arg2, arg3, arg4 string, arg5 interface{}, arg6 ...ace.RequestOptions) error {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0, arg1, arg2, arg3, arg4, arg5}
	for _, a := range arg6 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "DeleteK8SResource", varargs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteK8SResource indicates an expected call of DeleteK8SResource
func (mr *MockInterfaceMockRecorder) DeleteK8SResource(arg0, arg1, arg2, arg3, arg4, arg5 interface{}, arg6 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0, arg1, arg2, arg3, arg4, arg5}, arg6...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteK8SResource", reflect.TypeOf((*MockInterface)(nil).DeleteK8SResource), varargs...)
}

// DoRequest mocks base method
func (m *MockInterface) DoRequest(arg0 *http.Request, arg1 interface{}) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DoRequest", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// DoRequest indicates an expected call of DoRequest
func (mr *MockInterfaceMockRecorder) DoRequest(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DoRequest", reflect.TypeOf((*MockInterface)(nil).DoRequest), arg0, arg1)
}

// GetK8SResource mocks base method
func (m *MockInterface) GetK8SResource(arg0 context.Context, arg1, arg2, arg3, arg4 string, arg5 ...ace.RequestOptions) (*ace.KubernetesResource, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0, arg1, arg2, arg3, arg4}
	for _, a := range arg5 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "GetK8SResource", varargs...)
	ret0, _ := ret[0].(*ace.KubernetesResource)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetK8SResource indicates an expected call of GetK8SResource
func (mr *MockInterfaceMockRecorder) GetK8SResource(arg0, arg1, arg2, arg3, arg4 interface{}, arg5 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0, arg1, arg2, arg3, arg4}, arg5...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetK8SResource", reflect.TypeOf((*MockInterface)(nil).GetK8SResource), varargs...)
}

// GetOrgAccountByName mocks base method
func (m *MockInterface) GetOrgAccountByName(arg0 context.Context, arg1 ...ace.RequestOptions) (*ace.Account, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0}
	for _, a := range arg1 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "GetOrgAccountByName", varargs...)
	ret0, _ := ret[0].(*ace.Account)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetOrgAccountByName indicates an expected call of GetOrgAccountByName
func (mr *MockInterfaceMockRecorder) GetOrgAccountByName(arg0 interface{}, arg1 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0}, arg1...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetOrgAccountByName", reflect.TypeOf((*MockInterface)(nil).GetOrgAccountByName), varargs...)
}

// GetOrgAccountByNameAsList mocks base method
func (m *MockInterface) GetOrgAccountByNameAsList(arg0 context.Context, arg1 ...ace.RequestOptions) (thirdparty.List, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0}
	for _, a := range arg1 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "GetOrgAccountByNameAsList", varargs...)
	ret0, _ := ret[0].(thirdparty.List)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetOrgAccountByNameAsList indicates an expected call of GetOrgAccountByNameAsList
func (mr *MockInterfaceMockRecorder) GetOrgAccountByNameAsList(arg0 interface{}, arg1 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0}, arg1...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetOrgAccountByNameAsList", reflect.TypeOf((*MockInterface)(nil).GetOrgAccountByNameAsList), varargs...)
}

// GetRegion mocks base method
func (m *MockInterface) GetRegion(arg0 context.Context, arg1 string, arg2 ...ace.RequestOptions) (ace.Region, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0, arg1}
	for _, a := range arg2 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "GetRegion", varargs...)
	ret0, _ := ret[0].(ace.Region)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetRegion indicates an expected call of GetRegion
func (mr *MockInterfaceMockRecorder) GetRegion(arg0, arg1 interface{}, arg2 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0, arg1}, arg2...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetRegion", reflect.TypeOf((*MockInterface)(nil).GetRegion), varargs...)
}

// GetRoles mocks base method
func (m *MockInterface) GetRoles(arg0 context.Context, arg1 ...ace.RequestOptions) (*ace.RoleList, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0}
	for _, a := range arg1 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "GetRoles", varargs...)
	ret0, _ := ret[0].(*ace.RoleList)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetRoles indicates an expected call of GetRoles
func (mr *MockInterfaceMockRecorder) GetRoles(arg0 interface{}, arg1 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0}, arg1...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetRoles", reflect.TypeOf((*MockInterface)(nil).GetRoles), varargs...)
}

// GetRolesWithUsers mocks base method
func (m *MockInterface) GetRolesWithUsers(arg0 context.Context, arg1 ...ace.RequestOptions) (ace.RoleUserAssignmentList, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0}
	for _, a := range arg1 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "GetRolesWithUsers", varargs...)
	ret0, _ := ret[0].(ace.RoleUserAssignmentList)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetRolesWithUsers indicates an expected call of GetRolesWithUsers
func (mr *MockInterfaceMockRecorder) GetRolesWithUsers(arg0 interface{}, arg1 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0}, arg1...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetRolesWithUsers", reflect.TypeOf((*MockInterface)(nil).GetRolesWithUsers), varargs...)
}

// GetUsersOfRole mocks base method
func (m *MockInterface) GetUsersOfRole(arg0 context.Context, arg1 ...ace.RequestOptions) (*ace.RoleUserList, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0}
	for _, a := range arg1 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "GetUsersOfRole", varargs...)
	ret0, _ := ret[0].(*ace.RoleUserList)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetUsersOfRole indicates an expected call of GetUsersOfRole
func (mr *MockInterfaceMockRecorder) GetUsersOfRole(arg0 interface{}, arg1 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0}, arg1...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetUsersOfRole", reflect.TypeOf((*MockInterface)(nil).GetUsersOfRole), varargs...)
}

// Iterate mocks base method
func (m *MockInterface) Iterate(arg0 thirdparty.ListFunc) thirdparty.ListFunc {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Iterate", arg0)
	ret0, _ := ret[0].(thirdparty.ListFunc)
	return ret0
}

// Iterate indicates an expected call of Iterate
func (mr *MockInterfaceMockRecorder) Iterate(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Iterate", reflect.TypeOf((*MockInterface)(nil).Iterate), arg0)
}

// ListK8SResources mocks base method
func (m *MockInterface) ListK8SResources(arg0 context.Context, arg1, arg2, arg3 string, arg4 ...ace.RequestOptions) (*ace.KubernetesResourceList, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0, arg1, arg2, arg3}
	for _, a := range arg4 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "ListK8SResources", varargs...)
	ret0, _ := ret[0].(*ace.KubernetesResourceList)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListK8SResources indicates an expected call of ListK8SResources
func (mr *MockInterfaceMockRecorder) ListK8SResources(arg0, arg1, arg2, arg3 interface{}, arg4 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0, arg1, arg2, arg3}, arg4...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListK8SResources", reflect.TypeOf((*MockInterface)(nil).ListK8SResources), varargs...)
}

// ListProjects mocks base method
func (m *MockInterface) ListProjects(arg0 context.Context, arg1 ...ace.RequestOptions) (*ace.ProjectList, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0}
	for _, a := range arg1 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "ListProjects", varargs...)
	ret0, _ := ret[0].(*ace.ProjectList)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListProjects indicates an expected call of ListProjects
func (mr *MockInterfaceMockRecorder) ListProjects(arg0 interface{}, arg1 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0}, arg1...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListProjects", reflect.TypeOf((*MockInterface)(nil).ListProjects), varargs...)
}

// ListRegions mocks base method
func (m *MockInterface) ListRegions(arg0 context.Context, arg1 ...ace.RequestOptions) ([]ace.Region, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0}
	for _, a := range arg1 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "ListRegions", varargs...)
	ret0, _ := ret[0].([]ace.Region)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListRegions indicates an expected call of ListRegions
func (mr *MockInterfaceMockRecorder) ListRegions(arg0 interface{}, arg1 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0}, arg1...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListRegions", reflect.TypeOf((*MockInterface)(nil).ListRegions), varargs...)
}

// ListSpaces mocks base method
func (m *MockInterface) ListSpaces(arg0 context.Context, arg1 string, arg2 ...ace.RequestOptions) (*ace.SpaceList, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0, arg1}
	for _, a := range arg2 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "ListSpaces", varargs...)
	ret0, _ := ret[0].(*ace.SpaceList)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ListSpaces indicates an expected call of ListSpaces
func (mr *MockInterfaceMockRecorder) ListSpaces(arg0, arg1 interface{}, arg2 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0, arg1}, arg2...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ListSpaces", reflect.TypeOf((*MockInterface)(nil).ListSpaces), varargs...)
}

// MultipleIteration mocks base method
func (m *MockInterface) MultipleIteration(arg0 thirdparty.ListFunc, arg1 ace.ContextGenerator, arg2 thirdparty.List) (map[thirdparty.ListItem]thirdparty.List, []error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "MultipleIteration", arg0, arg1, arg2)
	ret0, _ := ret[0].(map[thirdparty.ListItem]thirdparty.List)
	ret1, _ := ret[1].([]error)
	return ret0, ret1
}

// MultipleIteration indicates an expected call of MultipleIteration
func (mr *MockInterfaceMockRecorder) MultipleIteration(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "MultipleIteration", reflect.TypeOf((*MockInterface)(nil).MultipleIteration), arg0, arg1, arg2)
}

// UpdateK8SResource mocks base method
func (m *MockInterface) UpdateK8SResource(arg0 context.Context, arg1, arg2, arg3, arg4 string, arg5 interface{}, arg6 ...ace.RequestOptions) error {
	m.ctrl.T.Helper()
	varargs := []interface{}{arg0, arg1, arg2, arg3, arg4, arg5}
	for _, a := range arg6 {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "UpdateK8SResource", varargs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// UpdateK8SResource indicates an expected call of UpdateK8SResource
func (mr *MockInterfaceMockRecorder) UpdateK8SResource(arg0, arg1, arg2, arg3, arg4, arg5 interface{}, arg6 ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{arg0, arg1, arg2, arg3, arg4, arg5}, arg6...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateK8SResource", reflect.TypeOf((*MockInterface)(nil).UpdateK8SResource), varargs...)
}
