package toolchain

import (
	"encoding/json"
	"sync"
)

const (
	// VersionGateGA general availabilty
	VersionGateGA = "ga"
	// VersionGateAlpha alpha functionality
	VersionGateAlpha = "alpha"
	// VersionGateBeta beta functionality
	VersionGateBeta = "beta"
)

// Scheme tool chain elements registration and control
type Scheme struct {
	Categories  []*Category     `json:"toolChains"`
	VersionGate string          `json:"versionGate"`
	Installed   map[string]bool `json:"installed"`
	Panel       *Panel          `json:"panel"`
	initOnce    sync.Once
	hasChanges  bool
}

func (sc *Scheme) init() *Scheme {
	sc.initOnce.Do(func() {
		if sc.Categories == nil {
			sc.Categories = []*Category{}
		}
		switch sc.VersionGate {
		case VersionGateAlpha, VersionGateBeta, VersionGateGA:
			// do nothing
		default:
			sc.VersionGate = VersionGateGA
		}
		if sc.Installed == nil {
			sc.Installed = map[string]bool{}
		}
		sc.hasChanges = false
		if sc.Panel == nil {
			sc.Panel = NewPanel()
		}
		sc.Panel.sort()
	})
	return sc
}

// Add known types
func (sc *Scheme) Add(category *Category, items ...*Item) *Scheme {
	found := false
	for _, c := range sc.init().Categories {
		if c.GetName() == category.GetName() {
			found = true
			category = c
			break
		}
	}
	if !found {
		sc.Categories = append(sc.Categories, category)
	}
	category.Add(items...)
	return sc
}

// GetCategories return scheme categories
func (sc *Scheme) GetCategories() []*Category {
	return sc.Categories
}

// SetVersionGate sets a version gate
func (sc *Scheme) SetVersionGate(gate string) *Scheme {
	switch gate {
	case VersionGateGA, VersionGateAlpha, VersionGateBeta:
		sc.VersionGate = gate
	}
	return sc
}

// IsInstalled returns true if the object is installed
func (sc *Scheme) IsInstalled(name string) bool {
	return sc.init().Installed[name]
}

// SetInstalled set as installed
func (sc *Scheme) SetInstalled(name string) {
	sc.init().Installed[name] = true
	sc.hasChanges = true
}

// AddPanel add panel
func (sc *Scheme) AddPanel(item ...PanelItem) {
	sc.Panel.AddPanel(item...)
}

// HasChanges returns true if there was any change to the scheme
func (sc *Scheme) HasChanges() bool {
	return sc.hasChanges
}

// Marshal prints a JSON representation of the scheme
func (sc *Scheme) Marshal() (content []byte, err error) {
	sc.Panel.sort()
	content, err = json.Marshal(sc)
	return
}

func (sc *Scheme) MarshalIndent() (content []byte, err error) {
	sc.Panel.sort()
	content, err = json.MarshalIndent(sc, "", "  ")
	return
}

// Merge will merge two schemes
// By default will add any non-existing categories
// will ignore Installed, and all Enabled flags
func (sc *Scheme) Merge(other *Scheme) *Scheme {
	if other == nil {
		return sc
	}
	if sc == nil {
		return other
	}
	for _, cat := range other.Categories {
		found := false
		for _, ourCat := range sc.Categories {
			if cat.Name != ourCat.Name {
				continue
			}
			found = true
			ourCat.Merge(cat)
			break
		}
		if !found {
			sc.hasChanges = true
			sc.Add(cat)
		}
	}
	sc.Panel.Merge(other.Panel)
	return sc
}

// NewScheme constructor function for scheme
func NewScheme() *Scheme {
	sc := &Scheme{}
	return sc.init()
}

// NewFromString starts a scheme from string
func NewFromString(content string) (sc *Scheme, err error) {
	sc = &Scheme{}
	err = json.Unmarshal([]byte(content), sc)
	sc.init()
	return
}
