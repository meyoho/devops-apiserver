package toolchain_test

import (
	"encoding/json"
	"testing"

	"alauda.io/devops-apiserver/pkg/toolchain"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

// NewItem constructs a new Item
func NewItem(
	name, english, chinese, itemType, kind, apiPath, apiURI, webURI string,
	enabled, public, enterprise bool,
) *toolchain.Item {
	return &toolchain.Item{
		Name: name,
		DisplayName: toolchain.DisplayName{
			English: english,
			Chinese: chinese,
		},
		Kind:         kind,
		Type:         itemType,
		APIAccessURI: apiURI,
		WebAccessURI: webURI,
		APIPath:      apiPath,
		Enabled:      enabled,
		Enterprise:   enterprise,
		Public:       public,
	}
}

// NewCategory constructs a new Category
func NewCategory(
	name, english, chinese, apiPath string,
	enabled bool,
) *toolchain.Category {
	return &toolchain.Category{
		Name: name,
		DisplayName: toolchain.DisplayName{
			English: english,
			Chinese: chinese,
		},
		APIPath: apiPath,
		Enabled: enabled,
	}
}

func TestToolchain(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("toolchain.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/toolchain", []Reporter{junitReporter})
}

var _ = Describe("Scheme.Add", func() {
	var (
		scheme  *toolchain.Scheme
		payload = map[*toolchain.Category][]*toolchain.Item{}
		content []byte
		err     error
	)

	BeforeEach(func() {
		scheme = toolchain.NewScheme()
		payload = map[*toolchain.Category][]*toolchain.Item{
			NewCategory("category", "en", "zh", "apipath", true): []*toolchain.Item{
				NewItem(
					"item", "i-en", "i-zh", "Type1", "Kind1",
					"itempath", "apiurl", "weburl", true, true, false,
				),
			},
			NewCategory("category", "en", "zh", "apipath", true): []*toolchain.Item{
				NewItem(
					"item2", "i2-en", "i2-zh", "Type2", "Kind2",
					"itempath", "apiurl2", "weburl2", false, false, true,
				),
			},
			// new category
			NewCategory("category2", "en1", "zh1", "apipath1", true): []*toolchain.Item{
				NewItem(
					"item3", "i3-en", "i3-zh", "Type3", "Kind3",
					"itempath2", "apiurl3", "weburl3", false, false, true,
				),
			},
		}
	})

	JustBeforeEach(func() {
		for k, v := range payload {
			scheme.Add(k, v...)
		}
		content, err = scheme.Marshal()
	})

	Context("Add multiple categories and items", func() {
		It("should print correct json", func() {
			Expect(err).To(BeNil())
			Expect(scheme.Categories).To(HaveLen(2))
			Expect(scheme.GetCategories()[0].GetItems()).ToNot(BeEmpty())
			Expect(scheme.GetCategories()[1].GetItems()).ToNot(BeEmpty())
			Expect(content).ToNot(BeEmpty())
		})
	})
})

var _ = Describe("NewFromString", func() {
	var (
		scheme      *toolchain.Scheme
		content     string
		jsonContent []byte
		jsonErr     error
		err         error
	)

	BeforeEach(func() {
		content = `{"toolChains":[{"name":"codeRepository","displayName":{"en":"Code Repository","zh":"代码仓库"},"enabled":true,"apiPath":"codereposervices","items":[{"displayName":{"en":"Github","zh":"Github"},"host":"https://api.github.com","html":"https://github.com","name":"github","kind":"codereposervice","type":"Github","public":true,"enterprise":false,"enabled":true,"apiPath":"codereposervices","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""},{"displayName":{"en":"Gitlab","zh":"Gitlab"},"host":"https://gitlab.com","html":"https://gitlab.com","name":"gitlab","kind":"codereposervice","type":"Gitlab","public":true,"enterprise":false,"enabled":true,"apiPath":"codereposervices","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""},{"displayName":{"en":"Gitlab Enterprise","zh":"Gitlab 企业版"},"host":"","html":"","name":"gitlab-enterprise","kind":"codereposervice","type":"Gitlab","public":false,"enterprise":true,"enabled":true,"apiPath":"codereposervices","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""},{"displayName":{"en":"Gitee","zh":"码云"},"host":"https://gitee.com","html":"https://gitee.com","name":"gitee","kind":"codereposervice","type":"Gitee","public":true,"enterprise":false,"enabled":true,"apiPath":"codereposervices","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""},{"displayName":{"en":"Gitee Enterprise","zh":"码云企业版"},"host":"","html":"","name":"gitee-enterprise","kind":"codereposervice","type":"Gitee","public":false,"enterprise":true,"enabled":true,"apiPath":"codereposervices","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""},{"displayName":{"en":"Bitbucket","zh":"Bitbucket"},"host":"https://api.bitbucket.org","html":"https://bitbucket.org","name":"bitbucket","kind":"codereposervice","type":"Bitbucket","public":true,"enterprise":false,"enabled":true,"apiPath":"codereposervices","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""}]},{"name":"continuousIntegration","displayName":{"en":"Continuous Integration","zh":"持续集成"},"enabled":true,"apiPath":"","items":[{"displayName":{"en":"Jenkins","zh":"Jenkins"},"host":"","html":"","name":"jenkins","kind":"jenkins","type":"","public":false,"enterprise":false,"enabled":true,"apiPath":"jenkinses","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""}]},{"name":"artifactRepository","displayName":{"en":"Artifact Repository","zh":"制品仓库"},"enabled":true,"apiPath":"","items":[{"displayName":{"en":"Docker Registry","zh":"Docker Registry"},"host":"","html":"","name":"docker-registry","kind":"imageregistry","type":"Docker","public":false,"enterprise":false,"enabled":true,"apiPath":"imageregistries","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""},{"displayName":{"en":"Harbor Registry","zh":"Harbor Registry"},"host":"","html":"","name":"harbor-registry","kind":"imageregistry","type":"Harbor","public":false,"enterprise":false,"enabled":true,"apiPath":"imageregistries","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""},{"displayName":{"en":"Alauda Registry","zh":"Alauda Registry"},"host":"","html":"","name":"alauda-registry","kind":"imageregistry","type":"Alauda","public":false,"enterprise":false,"enabled":true,"apiPath":"imageregistries","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""},{"displayName":{"en":"DockerHub Registry","zh":"DockerHub Registry"},"host":"https://hub.docker.com","html":"https://hub.docker.com","name":"dockerhub-registry","kind":"imageregistry","type":"DockerHub","public":true,"enterprise":false,"enabled":true,"apiPath":"imageregistries","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""}]},{"name":"testTool","displayName":{"en":"Test tool","zh":"测试工具"},"enabled":true,"apiPath":"testtools","items":[{"displayName":{"en":"RedwoodHQ","zh":"RedwoodHQ"},"host":"","html":"","name":"redwoodhq","kind":"testtool","type":"RedwoodHQ","public":false,"enterprise":false,"enabled":true,"apiPath":"testtools","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""}]},{"name":"projectManagement","displayName":{"en":"Project Management","zh":"项目管理"},"enabled":true,"apiPath":"projectmanagements","items":[{"displayName":{"en":"Jira","zh":"Jira"},"host":"","html":"","name":"jira","kind":"projectmanagement","type":"Jira","public":false,"enterprise":false,"enabled":true,"apiPath":"projectmanagements","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""},{"displayName":{"en":"Taiga","zh":"Taiga"},"host":"","html":"","name":"taiga","kind":"projectmanagement","type":"Taiga","public":false,"enterprise":false,"enabled":true,"apiPath":"projectmanagements","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""}]},{"name":"codeQualityTool","displayName":{"en":"Code Quality","zh":"代码检查"},"enabled":true,"apiPath":"codequalitytools","items":[{"displayName":{"en":"SonarQube","zh":"SonarQube"},"host":"","html":"","name":"sonarqube","kind":"codequalitytool","type":"Sonarqube","public":false,"enterprise":false,"enabled":true,"apiPath":"codequalitytools","supportedSecretTypes":null,"roleSyncEnabled":false,"featureGate": ""}]}],"versionGate":"ga","installed":{},"panel":{"panels":[],"type":"3x3"}}`
	})

	JustBeforeEach(func() {
		scheme, err = toolchain.NewFromString(content)
		jsonContent, jsonErr = json.Marshal(scheme)
	})

	Context("Old format from configmap", func() {
		It("should work, but installed is empty", func() {
			Expect(err).To(BeNil())
			Expect(scheme).ToNot(BeNil())
			Expect(scheme.Categories).To(HaveLen(6))
			Expect(scheme.VersionGate).To(BeEquivalentTo(toolchain.VersionGateGA))
			Expect(jsonErr).To(BeNil())
			Expect(content).To(MatchJSON(jsonContent))
		})
	})

	Context("Empty string", func() {
		BeforeEach(func() {
			content = ""
		})
		It("should return err", func() {
			Expect(err).ToNot(BeNil())
			Expect(scheme).ToNot(BeNil())
			Expect(jsonContent).To(MatchJSON(`{"installed":{},"toolChains":[],"versionGate":"ga","panel":{"panels":[],"type":"3x3"}}`))
			Expect(jsonErr).To(BeNil())
		})
	})
})

var _ = Describe("Scheme.SetVersionGate", func() {
	var (
		scheme      *toolchain.Scheme
		versionGate string
	)

	BeforeEach(func() {
		scheme = toolchain.NewScheme()
	})

	JustBeforeEach(func() {
		scheme.SetVersionGate(versionGate)
	})

	It("defaults to ga", func() {
		Expect(scheme.VersionGate).To(Equal(toolchain.VersionGateGA))
	})

	Context("Set alpha", func() {
		BeforeEach(func() {
			versionGate = toolchain.VersionGateAlpha
		})
		It("should set correctly", func() {
			Expect(scheme.VersionGate).To(Equal(toolchain.VersionGateAlpha))
		})
	})

	Context("Set beta", func() {
		BeforeEach(func() {
			versionGate = toolchain.VersionGateBeta
		})
		It("should set correctly", func() {
			Expect(scheme.VersionGate).To(Equal(toolchain.VersionGateBeta))
		})
	})

	Context("Set invalid", func() {
		BeforeEach(func() {
			versionGate = "invalid"
		})
		It("should set correctly", func() {
			Expect(scheme.VersionGate).To(Equal(toolchain.VersionGateGA))
		})
	})
})

var _ = Describe("Scheme.Merge", func() {
	var (
		scheme *toolchain.Scheme
		merged *toolchain.Scheme
	)

	BeforeEach(func() {
		scheme = toolchain.NewScheme().
			Add(
				NewCategory("cat1", "", "", "", false),
				NewItem("i2", "", "", "", "", "", "", "", false, false, true),
			).
			Add(
				NewCategory("cat2", "", "", "", true),
				NewItem("i1", "", "", "", "", "", "", "", true, false, true),
			)

		merged = toolchain.NewScheme().
			// same as the first one, but enabled
			Add(
				NewCategory("cat1", "english", "chinese", "apipath", true),
				NewItem("i2", "i2-en", "i2-zh", "type", "kind", "apipath", "api", "web", true, false, true),
				NewItem("i3", "", "", "", "", "", "", "", true, false, true),
			).
			Add(NewCategory("cat3", "cat3", "cat3", "cat3", false))
	})

	JustBeforeEach(func() {
		scheme = scheme.Merge(merged)
	})
	// GetCategori := func(cat *toolchain.Category) {}

	It("defaults to ga", func() {
		Expect(scheme.Categories).To(HaveLen(3))
		Expect(scheme.Categories).To(ContainElement(
			NewCategory("cat1", "english", "chinese", "apipath", false).
				Add(NewItem("i2", "i2-en", "i2-zh", "type", "kind", "apipath", "api", "web", false, false, true)).
				Add(NewItem("i3", "", "", "", "", "", "", "", true, false, true))))
		Expect(scheme.Categories).To(ContainElement(
			NewCategory("cat2", "", "", "", true).Add(NewItem("i1", "", "", "", "", "", "", "", true, false, true)),
		))
		Expect(scheme.Categories).To(ContainElement(NewCategory("cat3", "cat3", "cat3", "cat3", false)))
	})

	Context("merged is nil", func() {
		BeforeEach(func() {
			merged = nil
		})
		It("does nothing", func() {
			Expect(scheme.Categories).To(HaveLen(2))
			Expect(scheme.Categories).To(ContainElement(
				NewCategory("cat1", "", "", "", false).
					Add(NewItem("i2", "", "", "", "", "", "", "", false, false, true))))

			Expect(scheme.Categories).To(ContainElement(
				NewCategory("cat2", "", "", "", true).
					Add(NewItem("i1", "", "", "", "", "", "", "", true, false, true)),
			))
		})
	})

	Context("scheme is nil", func() {
		BeforeEach(func() {
			scheme = nil
		})
		It("returns scheme", func() {
			Expect(scheme.Categories).To(HaveLen(2))

			Expect(scheme.Categories).To(ContainElement(
				NewCategory("cat1", "english", "chinese", "apipath", true).Add(
					NewItem("i2", "i2-en", "i2-zh", "type", "kind", "apipath", "api", "web", true, false, true),
					NewItem("i3", "", "", "", "", "", "", "", true, false, true),
				),
			))

			Expect(scheme.Categories).To(ContainElement(
				NewCategory("cat3", "cat3", "cat3", "cat3", false),
			))
		})
	})

})
