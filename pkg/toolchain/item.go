package toolchain

import (
	corev1 "k8s.io/api/core/v1"
)

// Item item for a category
type Item struct {
	// DisplayName is a set of humanized and translated names
	DisplayName DisplayName `json:"displayName"`
	// APIAccessURI is api address
	APIAccessURI string `json:"host"`
	// WebAccessURI is web address
	WebAccessURI string `json:"html"`
	// Name is the name of the item
	Name string `json:"name"`
	// Kind is the kind of the item
	Kind string `json:"kind"`
	// Type is the type of the item
	Type string `json:"type"`
	// Public specifies the visibility of the item.
	Public bool `json:"public"`
	// Enterprise specifies whether the item is enterprise or not.
	Enterprise bool `json:"enterprise"`
	// Enabled specifies the enabled of the item.
	Enabled bool `json:"enabled"`
	// APIPath is a specification of the toolchain type
	APIPath string `json:"apiPath"`
	// SupportSecretTypes defines secret types supported by this tool
	SupportedSecretTypes []ItemSecretType `json:"supportedSecretTypes"`
	// RoleSyncEnabled defines if this tool support role sync
	RoleSyncEnabled bool `json:"roleSyncEnabled"`
	// FeatureGate defines tooltype related featuregate
	FeatureGate string `json:"featureGate"`
}

type ItemSecretType struct {
	Type        corev1.SecretType `json:"type"`
	Description Description       `json:"description"`
}

// DisplayName defines a set of readable names
type DisplayName struct {
	// English is a human readable English name.
	English string `json:"en"`
	// Chinese is a human readable Chinese name.
	Chinese string `json:"zh"`
}

type Description struct {
	// English is a human readable English name.
	English string `json:"en"`
	// Chinese is a human readable Chinese name.
	Chinese string `json:"zh"`
}

// GetName return name
func (i *Item) GetName() string {
	return i.Name
}

// Merge merge one item to another but keep its Enabled field
func (i *Item) Merge(merge *Item) *Item {
	enabled := i.Enabled
	*i = *merge
	i.Enabled = enabled

	return i
}
