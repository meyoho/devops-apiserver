package toolchain

import "sort"

const (
	// PanelTypeThreeByThree 3 by 3 square presentation type
	PanelTypeThreeByThree = "3x3"
)

// Panel panel for all items
type Panel struct {
	Panels []PanelItem `json:"panels"`
	Type   string      `json:"type"`
}

// NewPanel panel constructor
func NewPanel() *Panel {
	return &Panel{
		Type:   PanelTypeThreeByThree,
		Panels: make([]PanelItem, 0, 9),
	}
}

// Merge merges another panel into this one. It will only accept other panel items if this is empty
func (panel *Panel) Merge(other *Panel) {
	if other == nil {
		return
	}
	if len(panel.Panels) == 0 && len(other.Panels) > 0 {
		panel.AddPanel(other.Panels...)
	}
}

func (panel *Panel) sort() {
	if len(panel.Panels) > 0 {
		sort.Slice(panel.Panels, func(i, j int) bool {
			return panel.Panels[i].Index < panel.Panels[j].Index
		})
	}
}

// AddPanel add panels
func (panel *Panel) AddPanel(panels ...PanelItem) *Panel {
	panel.Panels = append(panel.Panels, panels...)
	panel.sort()
	return panel
}

// PanelItem panel item demonstration for end user
type PanelItem struct {
	Name       DisplayName `json:"name"`
	Index      int         `json:"index"`
	Categories []string    `json:"categories"`
}

// NewPanelItem constructor for PanelItem
func NewPanelItem(en, zh string, index int, categories ...string) PanelItem {
	return PanelItem{
		Name: DisplayName{
			English: en,
			Chinese: zh,
		},
		Index:      index,
		Categories: categories,
	}
}
