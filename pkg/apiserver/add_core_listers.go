package apiserver

import (
	"context"

	"k8s.io/apimachinery/pkg/labels"
	corev1listers "k8s.io/client-go/listers/core/v1"

	corev1 "k8s.io/api/core/v1"
	metainternalversion "k8s.io/apimachinery/pkg/apis/meta/internalversion"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

// AddSecretList add secret lister
func AddSecretList(secretLister corev1listers.SecretLister) {
	dependency.DefaultManager.Kind(devops.TypeSecret).AddGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return secretLister.Secrets(namespace).Get(name)
	}).AddListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var secretSlice []*corev1.Secret
		var labelSelector labels.Selector
		if opts != nil {
			labelSelector = opts.LabelSelector
		}
		if labelSelector == nil {
			labelSelector = labels.Everything()
		}
		secretSlice, err = secretLister.Secrets(namespace).List(labelSelector)
		if err == nil {
			secretList := &corev1.SecretList{
				Items: make([]corev1.Secret, 0, len(secretSlice)),
			}
			for _, s := range secretSlice {
				secretList.Items = append(secretList.Items, *s)
			}
			list = secretList
		}
		return
	})
}

// AddConfigMapList add configmap lister
func AddConfigMapList(configMapLister corev1listers.ConfigMapLister) {
	dependency.DefaultManager.Kind(devops.TypeConfigMap).AddGetFunc(func(ctx context.Context, name string, _ *metav1.GetOptions) (runtime.Object, error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		return configMapLister.ConfigMaps(namespace).Get(name)
	}).AddListFunc(func(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
		namespace := genericapirequest.NamespaceValue(ctx)
		var cmSlice []*corev1.ConfigMap
		var labelSelector labels.Selector
		if opts != nil {
			labelSelector = opts.LabelSelector
		}
		if labelSelector == nil {
			labelSelector = labels.Everything()
		}
		cmSlice, err = configMapLister.ConfigMaps(namespace).List(labelSelector)
		if err == nil {
			cmList := &corev1.ConfigMapList{
				Items: make([]corev1.ConfigMap, 0, len(cmSlice)),
			}
			for _, s := range cmSlice {
				cmList.Items = append(cmList.Items, *s)
			}
			list = cmList
		}
		return
	})
}
