/*
Copyright 2016 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package apiserver

import (
	"net/http"
	"os"
	"strings"

	"alauda.io/devops-apiserver/pkg/registry/devops/setting"
	"alauda.io/devops-apiserver/pkg/registry/devops/toolcategory"
	"alauda.io/devops-apiserver/pkg/registry/devops/tooltype"

	"alauda.io/devops-apiserver/pkg/registry/devops/artifactregistry"
	artifactregistryrest "alauda.io/devops-apiserver/pkg/registry/devops/artifactregistry/rest"
	"alauda.io/devops-apiserver/pkg/registry/devops/artifactregistrybinding"
	"alauda.io/devops-apiserver/pkg/registry/devops/artifactregistrymanager"
	artifactregistrymanagerrest "alauda.io/devops-apiserver/pkg/registry/devops/artifactregistrymanager/rest"
	toolcategoryrest "alauda.io/devops-apiserver/pkg/registry/devops/toolcategory/rest"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/install"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	"alauda.io/devops-apiserver/pkg/dependency"
	devopsregistry "alauda.io/devops-apiserver/pkg/registry"
	"alauda.io/devops-apiserver/pkg/registry/devops/clusterpipelinetasktemplate"
	"alauda.io/devops-apiserver/pkg/registry/devops/clusterpipelinetemplate"
	clusterpipelinetemplaterest "alauda.io/devops-apiserver/pkg/registry/devops/clusterpipelinetemplate/rest"
	"alauda.io/devops-apiserver/pkg/registry/devops/clusterpipelinetemplatesync"
	codequalitybindingstorage "alauda.io/devops-apiserver/pkg/registry/devops/codequalitybinding"
	codequalitybindingstoragerest "alauda.io/devops-apiserver/pkg/registry/devops/codequalitybinding/rest"
	codequalityprojectstorage "alauda.io/devops-apiserver/pkg/registry/devops/codequalityproject"
	codequalityprojectstoragerest "alauda.io/devops-apiserver/pkg/registry/devops/codequalityproject/rest"
	codequalitytoolstorage "alauda.io/devops-apiserver/pkg/registry/devops/codequalitytool"
	codequalitytoolstoragerest "alauda.io/devops-apiserver/pkg/registry/devops/codequalitytool/rest"
	coderepobindingstorage "alauda.io/devops-apiserver/pkg/registry/devops/coderepobinding"
	coderepobindingstoragerest "alauda.io/devops-apiserver/pkg/registry/devops/coderepobinding/rest"
	codereposervicestorage "alauda.io/devops-apiserver/pkg/registry/devops/codereposervice"
	codereposervicestoragerest "alauda.io/devops-apiserver/pkg/registry/devops/codereposervice/rest"
	coderepositorystorage "alauda.io/devops-apiserver/pkg/registry/devops/coderepository"
	coderepositorystoragerest "alauda.io/devops-apiserver/pkg/registry/devops/coderepository/rest"
	documentmanagementstorage "alauda.io/devops-apiserver/pkg/registry/devops/documentmanagement"
	documentmanagementrest "alauda.io/devops-apiserver/pkg/registry/devops/documentmanagement/rest"
	documentmanagementbindingstorage "alauda.io/devops-apiserver/pkg/registry/devops/documentmanagementbinding"
	documentmanagementbindingrest "alauda.io/devops-apiserver/pkg/registry/devops/documentmanagementbinding/rest"
	imageregistrystorage "alauda.io/devops-apiserver/pkg/registry/devops/imageregistry"
	imageregistryrest "alauda.io/devops-apiserver/pkg/registry/devops/imageregistry/rest"
	imageregistrybindingstorage "alauda.io/devops-apiserver/pkg/registry/devops/imageregistrybinding"
	imageregistrybindingstoragerest "alauda.io/devops-apiserver/pkg/registry/devops/imageregistrybinding/rest"
	imagerepositorystorage "alauda.io/devops-apiserver/pkg/registry/devops/imagerepository"
	imagerepositorystoragerest "alauda.io/devops-apiserver/pkg/registry/devops/imagerepository/rest"
	jenkinsstorage "alauda.io/devops-apiserver/pkg/registry/devops/jenkins"
	jenkinsrest "alauda.io/devops-apiserver/pkg/registry/devops/jenkins/rest"
	jenkinsbindingstorage "alauda.io/devops-apiserver/pkg/registry/devops/jenkinsbinding"
	jenkinsbindingstoragerest "alauda.io/devops-apiserver/pkg/registry/devops/jenkinsbinding/rest"
	pipelinestorage "alauda.io/devops-apiserver/pkg/registry/devops/pipeline"
	pipelinestoragerest "alauda.io/devops-apiserver/pkg/registry/devops/pipeline/rest"
	pipelineconfigstorage "alauda.io/devops-apiserver/pkg/registry/devops/pipelineconfig"
	pipelineconfigstoragerest "alauda.io/devops-apiserver/pkg/registry/devops/pipelineconfig/rest"
	"alauda.io/devops-apiserver/pkg/registry/devops/pipelinetasktemplate"
	"alauda.io/devops-apiserver/pkg/registry/devops/pipelinetemplate"
	pipelinetemplaterest "alauda.io/devops-apiserver/pkg/registry/devops/pipelinetemplate/rest"
	"alauda.io/devops-apiserver/pkg/registry/devops/pipelinetemplatesync"
	projectmanagementstorage "alauda.io/devops-apiserver/pkg/registry/devops/projectmanagement"
	projectmanagementset "alauda.io/devops-apiserver/pkg/registry/devops/projectmanagement/rest"
	projectmanagementbindingstorage "alauda.io/devops-apiserver/pkg/registry/devops/projectmanagementbinding"
	projectmanagementbindingset "alauda.io/devops-apiserver/pkg/registry/devops/projectmanagementbinding/rest"
	"alauda.io/devops-apiserver/pkg/util/generic"

	toolsapi "alauda.io/devops-apiserver/pkg/registry/devops/tools"

	testtoolstorage "alauda.io/devops-apiserver/pkg/registry/devops/testtool"
	toolbindingreplicastorage "alauda.io/devops-apiserver/pkg/registry/devops/toolbindingreplica"
	toolbindingreplicarest "alauda.io/devops-apiserver/pkg/registry/devops/toolbindingreplica/rest"
	devopsgeneric "alauda.io/devops-apiserver/pkg/registry/generic"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/apimachinery/pkg/version"
	"k8s.io/apiserver/pkg/endpoints/openapi"
	"k8s.io/apiserver/pkg/registry/rest"
	genericapiserver "k8s.io/apiserver/pkg/server"
	"k8s.io/client-go/kubernetes"
	restclient "k8s.io/client-go/rest"
	glog "k8s.io/klog"
)

var (
	// Scheme object holding server scheme
	Scheme = runtime.NewScheme()
	// Codecs codec factory for the server
	Codecs = serializer.NewCodecFactory(Scheme)
)

func init() {
	// install.Install(groupFactoryRegistry, registry, Scheme)
	install.Install(Scheme)

	// we need to add the options to empty v1
	// TODO fix the server code to avoid this
	metav1.AddToGroupVersion(Scheme, schema.GroupVersion{Version: "v1"})

	// TODO: keep the generic API server from wanting this
	unversioned := schema.GroupVersion{Group: "", Version: "v1"}
	Scheme.AddUnversionedTypes(unversioned,
		&metav1.Status{},
		&metav1.APIVersions{},
		&metav1.APIGroupList{},
		&metav1.APIGroup{},
		&metav1.APIResourceList{},
		// adding parameters types
		// we will use them as ParametersCodec
		&metav1.ListOptions{},
		&metav1.ExportOptions{},
		&metav1.GetOptions{},
		&metav1.DeleteOptions{},
	)
	// register defaults for metav1
	metav1.RegisterDefaults(Scheme)
}

// ExtraConfig extra configuration for the server
type ExtraConfig struct {
	// Place you custom config here.
	SystemNamespace      string
	CredentialsNamespace string
	BaseDomain           string
}

// Config this is the general configuration for the server
type Config struct {
	GenericConfig *genericapiserver.RecommendedConfig
	ExtraConfig   ExtraConfig
}

// DevopsServer contains state for a Kubernetes cluster master/api server.
type DevopsServer struct {
	GenericAPIServer *genericapiserver.GenericAPIServer
}

type completedConfig struct {
	GenericConfig genericapiserver.CompletedConfig
	ExtraConfig   *ExtraConfig
	ClientConfig  *restclient.Config
	Transport     http.RoundTripper
}

// CompletedConfig complete configuration for server
type CompletedConfig struct {
	// Embed a private pointer that cannot be instantiated outside of this package.
	*completedConfig
}

func getDefaultEnv(envKey, defaultVal string) (val string) {
	val = os.Getenv(envKey)
	if val == "" {
		val = defaultVal
	}
	return
}

// Complete fills in any fields not set that are required to have valid data. It's mutating the receiver.
func (cfg *Config) Complete() CompletedConfig {
	appVersion := getDefaultEnv("APP_VERSION", "v0.3")
	split := strings.Split(
		strings.TrimPrefix(
			appVersion,
			"v",
		),
		".",
	)
	major, minor := "0", "3"
	if len(split) > 0 {
		major = split[0]
		if len(split) > 1 {
			minor = split[1]
		}
	}

	// Adding swagger
	cfg.GenericConfig.SwaggerConfig = genericapiserver.DefaultSwaggerConfig()
	// Adding OpenAPI
	cfg.GenericConfig.OpenAPIConfig = genericapiserver.DefaultOpenAPIConfig(v1alpha1.GetOpenAPIDefinitions, openapi.NewDefinitionNamer(Scheme))
	cfg.GenericConfig.OpenAPIConfig.Info.Title = "DevOps"
	cfg.GenericConfig.OpenAPIConfig.Info.Version = appVersion

	cfg.GenericConfig.Version = &version.Info{
		Major:     major,
		Minor:     minor,
		GitCommit: getDefaultEnv("COMMIT_ID", "v0.3"),
		BuildDate: getDefaultEnv("BUILD_DATE", ""),
	}

	c := completedConfig{
		cfg.GenericConfig.Complete(),
		&cfg.ExtraConfig,
		cfg.GenericConfig.ClientConfig,
		generic.GetDefaultTransport(),
	}

	return CompletedConfig{&c}
}

// New returns a new instance of DevopsServer from the given config.
func (c completedConfig) New() (*DevopsServer, error) {
	provider := devops.NewAnnotationProvider(c.ExtraConfig.BaseDomain)

	genericServer, err := c.GenericConfig.New("devops-apiserver", genericapiserver.NewEmptyDelegate())
	if err != nil {
		return nil, err
	}

	s := &DevopsServer{
		GenericAPIServer: genericServer,
	}

	k8sclient, err := kubernetes.NewForConfig(c.ClientConfig)
	if err != nil {
		glog.Errorf("Error generating kubernetes client. err %v", err)
		return nil, err
	}
	devopsclient, err := clientset.NewForConfig(c.ClientConfig)
	if err != nil {
		glog.Errorf("Error generating kubernetes client. err %v", err)
		return nil, err
	}

	secretLister := c.GenericConfig.SharedInformerFactory.Core().V1().Secrets().Lister()
	AddSecretList(secretLister)

	configMapLister := c.GenericConfig.SharedInformerFactory.Core().V1().ConfigMaps().Lister()
	AddConfigMapList(configMapLister)

	// round tripper
	roundTripper := c.Transport

	// bootstraping API Sever
	apiGroupInfo := genericapiserver.NewDefaultAPIGroupInfo(
		devops.GroupName,                  // API GroupName fixed as devops.alauda.io
		Scheme,                            // API Scheme
		runtime.NewParameterCodec(Scheme), // ParameterCodec will regulate all parameter types
		Codecs,                            // Basic codecs for all types
	)
	// apiGroupInfo.GroupMeta.GroupVersion = v1alpha1.SchemeGroupVersion
	v1alpha1storage := map[string]rest.Storage{}
	// Adding Jenkins REST API
	jenkinsStore := devopsregistry.RESTInPeace(jenkinsstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	dependency.AddStore(devops.TypeJenkins, jenkinsStore)

	jenkinsBindingStore := devopsregistry.RESTInPeace(jenkinsbindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	dependency.AddStore(devops.TypeJenkinsBinding, jenkinsBindingStore)

	pipelineconfigsStore := devopsregistry.RESTInPeace(pipelineconfigstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore, provider))
	dependency.AddStore(devops.TypePipelineConfig, pipelineconfigsStore)

	pipelineStore := devopsregistry.RESTInPeace(pipelinestorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore, provider))
	dependency.AddStore(devops.TypePipeline, pipelineStore)

	codereposerviceStore, codeRepoServiceStatusStore, err := codereposervicestorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	codeRepoServiceStore := devopsregistry.RESTInPeace(codereposerviceStore, err)
	dependency.AddStore(devops.TypeCodeRepoService, codeRepoServiceStore)

	coderepositoryStore, codeRepositoryStatusStore, err := coderepositorystorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	codeRepositoryStore := devopsregistry.RESTInPeace(coderepositoryStore, err)
	dependency.AddStore(devops.TypeCodeRepository, codeRepositoryStore)

	coderepobindingStore, codeRepoBindingStatusStore, err := coderepobindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	codeRepoBindingStore := devopsregistry.RESTInPeace(coderepobindingStore, err)
	dependency.AddStore(devops.TypeCodeRepoBinding, codeRepoBindingStore)

	imageregistryStore, imageRegistryStatusStore, err := imageregistrystorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	imageRegistryStore := devopsregistry.RESTInPeace(imageregistryStore, err)
	dependency.AddStore(devops.TypeImageRegistry, imageRegistryStore)

	imageRepositoryStore := devopsregistry.RESTInPeace(imagerepositorystorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	dependency.AddStore(devops.TypeImageRepository, imageRepositoryStore)

	imageregistrybindingStore, imageRegistryBindingStatusStore, err := imageregistrybindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	imageRegistryBindingStore := devopsregistry.RESTInPeace(imageregistrybindingStore, err)
	imageRegistryBindingStatusStore = devopsregistry.RESTInPeace(imageRegistryBindingStatusStore, err)
	dependency.AddStore(devops.TypeImageRegistryBinding, imageRegistryBindingStore)

	documentManagementStore := devopsregistry.RESTInPeace(documentmanagementstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	dependency.AddStore(devops.TypeDocumentManagement, documentManagementStore)

	documentManagementBindingStore := devopsregistry.RESTInPeace(documentmanagementbindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	dependency.AddStore(devops.TypeDocumentManagementBinding, documentManagementBindingStore)

	projectmanagementStore, projectmanagementStatusStore, err := projectmanagementstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	projectManagementStore := devopsregistry.RESTInPeace(projectmanagementStore, err)
	projectManagementStatusStore := devopsregistry.RESTInPeace(projectmanagementStatusStore, err)
	dependency.AddStore(devops.TypeProjectManagement, projectManagementStore)

	projectManagementBindingStore := devopsregistry.RESTInPeace(projectmanagementbindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	dependency.AddStore(devops.TypeProjectManagementBinding, projectManagementBindingStore)

	codequalitytoolStore, codeQualityToolStautsStore, err := codequalitytoolstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	codeQualityToolStore := devopsregistry.RESTInPeace(codequalitytoolStore, err)
	dependency.AddStore(devops.TypeCodeQualityTool, codeQualityToolStore)

	codeQualityBindingStore := devopsregistry.RESTInPeace(codequalitybindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	dependency.AddStore(devops.TypeCodeQualityBinding, codeQualityBindingStore)

	codeQualityProjectStore := devopsregistry.RESTInPeace(codequalityprojectstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	dependency.AddStore(devops.TypeCodeQualityProject, codeQualityProjectStore)

	clusterpipelinetemplateStore, clusterpipelinetemplateStatusStore, err := clusterpipelinetemplate.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	dependency.AddStore(devops.TypeClusterPipelineTemplate, clusterpipelinetemplateStore)
	clusterPipelinetemplate := devopsregistry.RESTInPeace(clusterpipelinetemplateStore, err)

	clusterpipelinetemplatetaskStore, clusterpipelinetemplatetaskStatusStore, err := clusterpipelinetasktemplate.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	dependency.AddStore(devops.TypeClusterPipelineTaskTemplate, clusterpipelinetemplatetaskStore)
	clusterPipelineTasktemplate := devopsregistry.RESTInPeace(clusterpipelinetemplatetaskStore, err)

	pipelinetemplateStore, pipelinetemplateStatusStore, err := pipelinetemplate.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	dependency.AddStore(devops.TypePipelineTemplate, pipelinetemplateStore)
	pipelinetemplate := devopsregistry.RESTInPeace(pipelinetemplateStore, err)

	pipelinetasktemplateStore, pipelinetasktemplateStatusStore, err := pipelinetasktemplate.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	dependency.AddStore(devops.TypePipelineTaskTemplate, pipelinetasktemplateStore)
	pipelinetasktemplate := devopsregistry.RESTInPeace(pipelinetasktemplateStore, err)

	artifactRegistry := devopsregistry.RESTInPeace(artifactregistry.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	dependency.AddStore(devops.TypeArtifactRegistry, artifactRegistry)

	artifactRegistryBinding := devopsregistry.RESTInPeace(artifactregistrybinding.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	dependency.AddStore(devops.TypeArtifactRegistryBinding, artifactRegistryBinding)

	artifactRegistryManager := devopsregistry.RESTInPeace(artifactregistrymanager.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	artifactRegistryManagerCreate := artifactregistrymanagerrest.NewCreateREST(artifactRegistry, artifactRegistryManager, devopsclient, secretLister, roundTripper, c.ExtraConfig.SystemNamespace, provider)
	dependency.AddStore(devops.TypeArtifactRegistryManager, artifactRegistryManager)

	toolCategoryStore, toolCategoryStatusStore, err := toolcategory.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	toolCategoryStoreInPeace := devopsregistry.RESTInPeace(toolCategoryStore, err)
	dependency.AddStore(devops.TypeToolCategory, toolCategoryStoreInPeace)

	toolTypeStore, toolTypeStatusStore, err := tooltype.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	toolTypeStoreInPeace := devopsregistry.RESTInPeace(toolTypeStore, err)
	dependency.AddStore(devops.TypeToolType, toolTypeStoreInPeace)

	//projectmanagementfunc := projectmanagementsyncrole.NewProjectManagementRolesOperation(roundTripper)
	//projectgetrolefunc := projectmanagementfunc.GetRolesMappingFunc
	//projectapplyrolefunc := projectmanagementfunc.ApplyRolesMappingFunc
	//imageregistryRoles := imageregistryrest.NewRolesREST(roundTripper)
	//codequalitytoolRoles := codequalitytoolrest.NewRolesREST(roundTripper, c.ExtraConfig.SystemNamespace)
	//codereposerviceRoles := codereposervicestoragerest.NewRolesREST(roundTripper)

	v1alpha1storage["jenkinses"] = jenkinsStore
	v1alpha1storage["jenkinses/authorize"] = jenkinsrest.NewSecretREST(jenkinsStore, jenkinsBindingStore, secretLister, configMapLister, roundTripper)
	v1alpha1storage["jenkinsbindings"] = jenkinsBindingStore
	v1alpha1storage["jenkinsbindings/proxy"] = jenkinsbindingstoragerest.NewProxyREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["jenkinsbindings/info"] = jenkinsbindingstoragerest.NewInfoREST(dependency.DefaultManager, roundTripper)

	// adding PipelineConfig REST API

	v1alpha1storage["pipelineconfigs"] = pipelineconfigsStore
	v1alpha1storage["pipelineconfigs/preview"] = pipelineconfigstoragerest.NewPreviewREST(dependency.DefaultManager)
	v1alpha1storage["pipelineconfigs/scan"] = pipelineconfigstoragerest.NewScanREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["pipelineconfigs/logs"] = pipelineconfigstoragerest.NewLogREST(dependency.DefaultManager, roundTripper)

	// adding Pipeline REST API
	v1alpha1storage["pipelines"] = pipelineStore
	v1alpha1storage["pipelines/input"] = pipelinestoragerest.NewInputREST(pipelineStore, jenkinsStore, jenkinsBindingStore, secretLister, roundTripper, pipelinestorage.URLBuilderImp{}, provider)
	v1alpha1storage["pipelines/logs"] = pipelinestoragerest.NewLogREST(dependency.DefaultManager, roundTripper, provider)
	v1alpha1storage["pipelines/tasks"] = pipelinestoragerest.NewTaskREST(dependency.DefaultManager, roundTripper, provider)
	v1alpha1storage["pipelines/testreports"] = pipelinestoragerest.NewTestReportREST(pipelineStore, jenkinsStore, jenkinsBindingStore, secretLister, roundTripper, pipelinestorage.URLBuilderImp{}, provider)
	v1alpha1storage["pipelines/artifacts"] = pipelinestoragerest.NewArtiFactREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["pipelines/download"] = pipelinestoragerest.NewDownLoadREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["pipelines/view"] = pipelinestoragerest.NewViewREST(dependency.DefaultManager, roundTripper, c.ExtraConfig.SystemNamespace)

	// adding RepoService REST API
	v1alpha1storage["codereposervices"] = codeRepoServiceStore
	v1alpha1storage["codereposervices/status"] = codeRepoServiceStatusStore
	v1alpha1storage["codereposervices/authorize"] = codereposervicestoragerest.NewSecretREST(codeRepoServiceStore, codeRepoBindingStore, secretLister, configMapLister, k8sclient, roundTripper, provider)
	v1alpha1storage["codereposervices/projects"] = codereposervicestoragerest.NewProjectREST(codeRepoServiceStore, secretLister, roundTripper)
	//v1alpha1storage["codereposervices/roles"] = devopsgeneric.NewRolesREST(devops.TypeCodeRepoService, dependency.DefaultManager, codereposerviceRoles.GetRolesMappingFunc, codereposerviceRoles.ApplyRolesMappingFunc)
	v1alpha1storage["coderepobindings"] = codeRepoBindingStore
	v1alpha1storage["coderepobindings/status"] = codeRepoBindingStatusStore
	v1alpha1storage["coderepobindings/remoterepositories"] = coderepobindingstoragerest.NewRepositoryREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["coderepositories"] = codeRepositoryStore
	v1alpha1storage["coderepositories/branches"] = coderepositorystoragerest.NewRepoBranchREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["coderepositories/status"] = codeRepositoryStatusStore

	// adding PipelineTaskTemplate REST API

	v1alpha1storage["pipelinetasktemplates"] = pipelinetasktemplate
	v1alpha1storage["pipelinetasktemplates/status"] = pipelinetasktemplateStatusStore
	v1alpha1storage["pipelinetemplates"] = pipelinetemplate
	v1alpha1storage["pipelinetemplates/status"] = pipelinetemplateStatusStore
	v1alpha1storage["pipelinetemplates/preview"] = pipelinetemplaterest.NewPreviewREST(dependency.DefaultManager)
	v1alpha1storage["pipelinetemplates/exports"] = pipelinetemplaterest.NewExportRest(dependency.DefaultManager)
	// adding PipelineTemplateSync REST API
	v1alpha1storage["pipelinetemplatesyncs"] = devopsregistry.RESTInPeace(pipelinetemplatesync.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	// adding ClusterPipelineTemplateSync REST API
	v1alpha1storage["clusterpipelinetemplatesyncs"] = devopsregistry.RESTInPeace(clusterpipelinetemplatesync.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))

	// cluster template

	v1alpha1storage["clusterpipelinetemplates"] = clusterPipelinetemplate
	v1alpha1storage["clusterpipelinetemplates/status"] = clusterpipelinetemplateStatusStore
	v1alpha1storage["clusterpipelinetasktemplates"] = clusterPipelineTasktemplate
	v1alpha1storage["clusterpipelinetasktemplates/status"] = clusterpipelinetemplatetaskStatusStore
	v1alpha1storage["clusterpipelinetemplates/preview"] = clusterpipelinetemplaterest.NewPreviewREST(dependency.DefaultManager)
	v1alpha1storage["clusterpipelinetemplates/exports"] = clusterpipelinetemplaterest.NewExportRest(dependency.DefaultManager)

	// adding ImageRegistry REST API
	v1alpha1storage["imageregistries"] = imageRegistryStore
	v1alpha1storage["imageregistries/status"] = imageRegistryStatusStore

	v1alpha1storage["imageregistries/authorize"] = imageregistryrest.NewSecretREST(imageRegistryStore, imageRegistryBindingStore, secretLister, configMapLister, roundTripper)
	v1alpha1storage["imageregistries/projects"] = imageregistryrest.NewProjectREST(imageRegistryStore, secretLister, roundTripper)
	v1alpha1storage["imageregistries/repositories"] = imageregistryrest.NewRepositoryREST(dependency.DefaultManager, secretLister, roundTripper)
	// v1alpha1storage["imageregistries/roles"] = devopsgeneric.NewRolesREST(devops.TypeImageRegistry, dependency.DefaultManager, imageregistryRoles.GetRoleMapping, imageregistryRoles.ApplyRoleMapping)
	v1alpha1storage["imageregistrybindings"] = imageRegistryBindingStore
	v1alpha1storage["imageregistrybindings/status"] = imageRegistryBindingStatusStore
	v1alpha1storage["imageregistrybindings/repositories"] = imageregistrybindingstoragerest.NewRepositoryREST(imageRegistryStore, imageRegistryBindingStore, secretLister, roundTripper)
	v1alpha1storage["imageregistrybindings/projects"] = imageregistrybindingstoragerest.NewProjectREST(imageRegistryStore, imageRegistryBindingStore, secretLister, roundTripper)
	v1alpha1storage["imagerepositories"] = imageRepositoryStore
	v1alpha1storage["imagerepositories/security"] = imagerepositorystoragerest.NewImageScanREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["imagerepositories/tags"] = imagerepositorystoragerest.NewImageTagREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["imagerepositories/remote"] = imagerepositorystoragerest.NewAvailableREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["imagerepositories/link"] = imagerepositorystoragerest.NewLinkREST(dependency.DefaultManager, roundTripper)

	// projectManagement
	v1alpha1storage["projectmanagements/authorize"] = projectmanagementset.NewSecretREST(projectManagementStore, projectManagementBindingStore, secretLister, configMapLister, roundTripper)
	v1alpha1storage["projectmanagementbindings/authorize"] = projectmanagementbindingset.NewSecretREST(projectManagementStore, projectManagementBindingStore, secretLister, configMapLister, roundTripper)
	v1alpha1storage["projectmanagementbindings/issueoptions"] = projectmanagementbindingset.NewIssueOptionREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["projectmanagementbindings/issueslist"] = projectmanagementbindingset.NewIssueListREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["projectmanagementbindings/issue"] = projectmanagementbindingset.NewIssueREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["projectmanagements/remoteprojects"] = projectmanagementset.NewProjectREST(projectManagementStore, projectManagementBindingStore, secretLister, configMapLister, roundTripper)
	v1alpha1storage["projectmanagements/users"] = projectmanagementset.NewUserREST(projectManagementStore, projectManagementBindingStore, secretLister, configMapLister, roundTripper)
	v1alpha1storage["projectmanagements"] = projectManagementStore
	v1alpha1storage["projectmanagements/status"] = projectManagementStatusStore
	v1alpha1storage["projectmanagementbindings"] = projectManagementBindingStore
	//v1alpha1storage["projectmanagements/roles"] = devopsgeneric.NewRolesREST(devops.TypeProjectManagement, dependency.DefaultManager, projectgetrolefunc, projectapplyrolefunc)

	// documentManagement
	v1alpha1storage["documentmanagements/authorize"] = documentmanagementrest.NewSecretREST(documentManagementStore, documentManagementBindingStore, secretLister, configMapLister, roundTripper)
	v1alpha1storage["documentmanagementbindings/authorize"] = documentmanagementbindingrest.NewSecretREST(documentManagementStore, documentManagementBindingStore, secretLister, configMapLister, roundTripper)
	v1alpha1storage["documentmanagements/projects"] = documentmanagementrest.NewDocumentREST(documentManagementStore, documentManagementBindingStore, secretLister, configMapLister, roundTripper)
	v1alpha1storage["documentmanagements"] = documentManagementStore
	v1alpha1storage["documentmanagementbindings"] = documentManagementBindingStore

	// testTool
	testToolStore := devopsregistry.RESTInPeace(testtoolstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	// testToolBindingStore := devopsregistry.RESTInPeace(testtoolbindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore))
	v1alpha1storage["testtools"] = testToolStore
	// v1alpha1storage["testtoolbindings"] = testToolBindingStore

	// codeQualityTool
	v1alpha1storage["codequalitytools"] = codeQualityToolStore
	v1alpha1storage["codequalitytools/status"] = codeQualityToolStautsStore
	//v1alpha1storage["codequalitytools/roles"] = devopsgeneric.NewRolesREST(devops.TypeCodeQualityTool, dependency.DefaultManager, codequalitytoolRoles.GetRolesMappingFunc, codequalitytoolRoles.ApplyRolesMappingFunc)
	v1alpha1storage["codequalitytools/authorize"] = codequalitytoolstoragerest.NewSecretREST(codeQualityToolStore, codeQualityBindingStore, secretLister, configMapLister, roundTripper)
	v1alpha1storage["codequalitybindings"] = codeQualityBindingStore
	v1alpha1storage["codequalitybindings/projects"] = codequalitybindingstoragerest.NewProjectsREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["codequalityprojects"] = codeQualityProjectStore
	v1alpha1storage["codequalityprojects/date"] = codequalityprojectstoragerest.NewLastAnalysisDateREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["codequalityprojects/remote"] = codequalityprojectstoragerest.NewReoteREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["codequalityprojects/reports"] = codequalityprojectstoragerest.NewReportsREST(dependency.DefaultManager, roundTripper)

	// toolBindingReplica
	toolbindingreplicaStore, toolbindingreplicaStatusStore, err := toolbindingreplicastorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	toolBindingReplicaStore := devopsregistry.RESTInPeace(toolbindingreplicaStore, err)
	toolBindingReplicaStatusStore := devopsregistry.RESTInPeace(toolbindingreplicaStatusStore, err)
	v1alpha1storage["toolbindingreplicas"] = toolBindingReplicaStore
	v1alpha1storage["toolbindingreplicas/projects"] = toolbindingreplicarest.NewProjectRest(toolBindingReplicaStore, devopsclient)
	v1alpha1storage["toolbindingreplicas/status"] = toolBindingReplicaStatusStore

	// irbREST := imageRegistryBindingStore.(*devopsregistry.REST)
	toolsBinding := devopsregistry.RESTInPeaceStorage(toolsapi.NewBindingREST(dependency.DefaultManager, c.ExtraConfig.SystemNamespace))
	v1alpha1storage["toolbindings"] = toolsBinding

	v1alpha1storage["artifactregistries"] = artifactRegistry
	artifactRegistryAuthorize := artifactregistryrest.NewSecretREST(dependency.DefaultManager, secretLister, roundTripper, c.ExtraConfig.SystemNamespace)
	v1alpha1storage["artifactregistries/authorize"] = artifactRegistryAuthorize
	//artifactRegistryRoles := artifactregistryrest.NewArtifactRegistryRolesOperation(roundTripper, c.ExtraConfig.SystemNamespace)
	//v1alpha1storage["artifactregistries/roles"] = devopsgeneric.NewRolesREST(devops.TypeArtifactRegistry, dependency.DefaultManager, artifactRegistryRoles.GetRolesMappingFunc, artifactRegistryRoles.ApplyRolesMappingFunc)

	v1alpha1storage["artifactregistrybindings"] = artifactRegistryBinding

	v1alpha1storage["artifactregistrymanagers"] = artifactRegistryManager
	artifactRegistryManagerInit := artifactregistrymanagerrest.NewInitREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["artifactregistrymanagers/init"] = artifactRegistryManagerInit
	v1alpha1storage["artifactregistrymanagers/project"] = artifactRegistryManagerCreate
	artifactRegistryManagerRepository := artifactregistrymanagerrest.NewRepositoryREST(dependency.DefaultManager, roundTripper, c.ExtraConfig.SystemNamespace)
	v1alpha1storage["artifactregistrymanagers/repository"] = artifactRegistryManagerRepository
	artifactRegistryManagerAuthorize := artifactregistrymanagerrest.NewSecretREST(dependency.DefaultManager, secretLister, roundTripper, c.ExtraConfig.SystemNamespace)
	v1alpha1storage["artifactregistrymanagers/authorize"] = artifactRegistryManagerAuthorize
	artifactRegistryManagerBlobStore := artifactregistrymanagerrest.NewBlobStoreREST(dependency.DefaultManager, roundTripper, c.ExtraConfig.SystemNamespace)
	v1alpha1storage["artifactregistrymanagers/blobstore"] = artifactRegistryManagerBlobStore
	artifactRegistryManagerArtifactRegistry := artifactregistrymanagerrest.NewLocalRepositoryREST(dependency.DefaultManager, roundTripper)
	v1alpha1storage["artifactregistrymanagers/artifactregistry"] = artifactRegistryManagerArtifactRegistry

	v1alpha1storage["toolcategories"] = toolCategoryStore
	v1alpha1storage["toolcategories/status"] = toolCategoryStatusStore
	v1alpha1storage["toolcategories/settings"] = toolcategoryrest.NewSettingsREST(dependency.DefaultManager)

	v1alpha1storage["tooltypes"] = toolTypeStore
	v1alpha1storage["tooltypes/status"] = toolTypeStatusStore

	settingStore, settingStoreStatusStore, err := setting.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter, devopsgeneric.NewStandardStore)
	settingStoreInPeace := devopsregistry.RESTInPeace(settingStore, err)
	dependency.AddStore(devops.TypeSetting, settingStoreInPeace)

	v1alpha1storage["settings"] = settingStore
	v1alpha1storage["settings/status"] = settingStoreStatusStore

	glog.V(5).Infof("Health checks %v", s.GenericAPIServer.HealthzChecks())
	apiGroupInfo.VersionedResourcesStorageMap["v1alpha1"] = v1alpha1storage

	if err := s.GenericAPIServer.InstallAPIGroup(&apiGroupInfo); err != nil {
		return nil, err
	}

	return s, nil
}
