/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by lister-gen. DO NOT EDIT.

package devops

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// PipelineTemplateLister helps list PipelineTemplates.
type PipelineTemplateLister interface {
	// List lists all PipelineTemplates in the indexer.
	List(selector labels.Selector) (ret []*devops.PipelineTemplate, err error)
	// PipelineTemplates returns an object that can list and get PipelineTemplates.
	PipelineTemplates(namespace string) PipelineTemplateNamespaceLister
	PipelineTemplateListerExpansion
}

// pipelineTemplateLister implements the PipelineTemplateLister interface.
type pipelineTemplateLister struct {
	indexer cache.Indexer
}

// NewPipelineTemplateLister returns a new PipelineTemplateLister.
func NewPipelineTemplateLister(indexer cache.Indexer) PipelineTemplateLister {
	return &pipelineTemplateLister{indexer: indexer}
}

// List lists all PipelineTemplates in the indexer.
func (s *pipelineTemplateLister) List(selector labels.Selector) (ret []*devops.PipelineTemplate, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*devops.PipelineTemplate))
	})
	return ret, err
}

// PipelineTemplates returns an object that can list and get PipelineTemplates.
func (s *pipelineTemplateLister) PipelineTemplates(namespace string) PipelineTemplateNamespaceLister {
	return pipelineTemplateNamespaceLister{indexer: s.indexer, namespace: namespace}
}

// PipelineTemplateNamespaceLister helps list and get PipelineTemplates.
type PipelineTemplateNamespaceLister interface {
	// List lists all PipelineTemplates in the indexer for a given namespace.
	List(selector labels.Selector) (ret []*devops.PipelineTemplate, err error)
	// Get retrieves the PipelineTemplate from the indexer for a given namespace and name.
	Get(name string) (*devops.PipelineTemplate, error)
	PipelineTemplateNamespaceListerExpansion
}

// pipelineTemplateNamespaceLister implements the PipelineTemplateNamespaceLister
// interface.
type pipelineTemplateNamespaceLister struct {
	indexer   cache.Indexer
	namespace string
}

// List lists all PipelineTemplates in the indexer for a given namespace.
func (s pipelineTemplateNamespaceLister) List(selector labels.Selector) (ret []*devops.PipelineTemplate, err error) {
	err = cache.ListAllByNamespace(s.indexer, s.namespace, selector, func(m interface{}) {
		ret = append(ret, m.(*devops.PipelineTemplate))
	})
	return ret, err
}

// Get retrieves the PipelineTemplate from the indexer for a given namespace and name.
func (s pipelineTemplateNamespaceLister) Get(name string) (*devops.PipelineTemplate, error) {
	obj, exists, err := s.indexer.GetByKey(s.namespace + "/" + name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(devops.Resource("pipelinetemplate"), name)
	}
	return obj.(*devops.PipelineTemplate), nil
}
