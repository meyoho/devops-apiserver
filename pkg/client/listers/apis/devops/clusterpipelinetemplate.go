/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by lister-gen. DO NOT EDIT.

package devops

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// ClusterPipelineTemplateLister helps list ClusterPipelineTemplates.
type ClusterPipelineTemplateLister interface {
	// List lists all ClusterPipelineTemplates in the indexer.
	List(selector labels.Selector) (ret []*devops.ClusterPipelineTemplate, err error)
	// Get retrieves the ClusterPipelineTemplate from the index for a given name.
	Get(name string) (*devops.ClusterPipelineTemplate, error)
	ClusterPipelineTemplateListerExpansion
}

// clusterPipelineTemplateLister implements the ClusterPipelineTemplateLister interface.
type clusterPipelineTemplateLister struct {
	indexer cache.Indexer
}

// NewClusterPipelineTemplateLister returns a new ClusterPipelineTemplateLister.
func NewClusterPipelineTemplateLister(indexer cache.Indexer) ClusterPipelineTemplateLister {
	return &clusterPipelineTemplateLister{indexer: indexer}
}

// List lists all ClusterPipelineTemplates in the indexer.
func (s *clusterPipelineTemplateLister) List(selector labels.Selector) (ret []*devops.ClusterPipelineTemplate, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*devops.ClusterPipelineTemplate))
	})
	return ret, err
}

// Get retrieves the ClusterPipelineTemplate from the index for a given name.
func (s *clusterPipelineTemplateLister) Get(name string) (*devops.ClusterPipelineTemplate, error) {
	obj, exists, err := s.indexer.GetByKey(name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(devops.Resource("clusterpipelinetemplate"), name)
	}
	return obj.(*devops.ClusterPipelineTemplate), nil
}
