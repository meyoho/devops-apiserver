/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by lister-gen. DO NOT EDIT.

package internalversion

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// ClusterPipelineTaskTemplateLister helps list ClusterPipelineTaskTemplates.
type ClusterPipelineTaskTemplateLister interface {
	// List lists all ClusterPipelineTaskTemplates in the indexer.
	List(selector labels.Selector) (ret []*devops.ClusterPipelineTaskTemplate, err error)
	// Get retrieves the ClusterPipelineTaskTemplate from the index for a given name.
	Get(name string) (*devops.ClusterPipelineTaskTemplate, error)
	ClusterPipelineTaskTemplateListerExpansion
}

// clusterPipelineTaskTemplateLister implements the ClusterPipelineTaskTemplateLister interface.
type clusterPipelineTaskTemplateLister struct {
	indexer cache.Indexer
}

// NewClusterPipelineTaskTemplateLister returns a new ClusterPipelineTaskTemplateLister.
func NewClusterPipelineTaskTemplateLister(indexer cache.Indexer) ClusterPipelineTaskTemplateLister {
	return &clusterPipelineTaskTemplateLister{indexer: indexer}
}

// List lists all ClusterPipelineTaskTemplates in the indexer.
func (s *clusterPipelineTaskTemplateLister) List(selector labels.Selector) (ret []*devops.ClusterPipelineTaskTemplate, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*devops.ClusterPipelineTaskTemplate))
	})
	return ret, err
}

// Get retrieves the ClusterPipelineTaskTemplate from the index for a given name.
func (s *clusterPipelineTaskTemplateLister) Get(name string) (*devops.ClusterPipelineTaskTemplate, error) {
	obj, exists, err := s.indexer.GetByKey(name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(devops.Resource("clusterpipelinetasktemplate"), name)
	}
	return obj.(*devops.ClusterPipelineTaskTemplate), nil
}
