package externalversions

import (
	"fmt"
	"net/http"
	"strings"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type GiteaClient struct {
	*ServiceClient
	devopsClient devopsclient.Interface
}

func NewGiteaClient(serviceClient *ServiceClient, roundTripper http.RoundTripper) (*GiteaClient, error) {
	var devopsClient devopsclient.Interface
	if serviceClient.isBasicAuth() {
		devopsClient = NewDevOpsClient("gitea", "", serviceClient.getHost(),
			devopsclient.NewBasicAuth(serviceClient.BasicAuthInfo.Username, serviceClient.BasicAuthInfo.Password),
			devopsclient.NewTransport(roundTripper))
	} else {
		// oauth2
		serviceClient.OAuth2Info.AccessTokenKey = k8s.GiteaAccessTokenKey
		token := serviceClient.OAuth2Info.AccessToken
		devopsClient = NewDevOpsClient("gitea", "", serviceClient.getHost(),
			devopsclient.NewAPIKey("Authorization", "header", fmt.Sprintf("token %s", token)),
			devopsclient.NewTransport(roundTripper))
	}

	return &GiteaClient{ServiceClient: serviceClient, devopsClient: devopsClient}, nil
}

func (c *GiteaClient) GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error) {
	return GetRemoteRepos(c.devopsClient)
}

func (c *GiteaClient) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository) {
	// actually we do not need it when use devops cient
	return devops.OriginCodeRepository{}
}

func (c *GiteaClient) CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckAvailable(c.devopsClient)
}

func (c *GiteaClient) CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckAuthentication(c.devopsClient)
}

func (c *GiteaClient) GetLatestRepoCommit(repoID, owner, repoName, repoFullName string) (commit *devops.RepositoryCommit, status *devops.HostPortStatus) {
	return GetLatestRepoCommit(c.devopsClient, repoID, owner, repoName, repoFullName)
}

func (c *GiteaClient) AccessToken(redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessToken()
		response       = &AccessTokenResponseGitea{}
	)

	return c.accessToken(response, redirectUrl, accessTokenUrl)
}

func (c *GiteaClient) getAccessToken() string {
	return fmt.Sprintf("%s/login/oauth/access_token", strings.TrimRight(c.getHtml(), "/"))
}

func (c *GiteaClient) GetAuthorizeUrl(redirectUrl string) string {
	return fmt.Sprintf("%s/login/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s&state=",
		strings.TrimRight(c.getHtml(), "/"), c.getClientID(), redirectUrl, c.getScope())
}

func (c *GiteaClient) GetBranches(owner, repoName, repoFullName string) (branches []devops.CodeRepoBranch, err error) {
	return GetBranches(c.devopsClient, owner, repoName, repoFullName)
}

func (c *GiteaClient) CreateProject(options devops.CreateProjectOptions) (*devops.ProjectData, error) {
	return CreateCodeRepoProject(c.devopsClient, options)
}

func (c *GiteaClient) ListProjectDataList(options devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	return ListCodeRepoProjects(c.devopsClient, options)
}
