package externalversions

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"sync"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/go-gitee"
	"alauda.io/devops-apiserver/pkg/util/generic"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

const (
	MaxGiteeOrganizationPerPage = 100
)

type GiteeClient struct {
	*ServiceClient
	Client       *gitee.Client
	devopsClient devopsclient.Interface
}

func NewGiteeClient(serviceClient *ServiceClient, roundTripper http.RoundTripper) (*GiteeClient, error) {
	var (
		client = &gitee.Client{}
		err    error
		token  string

		devopsClient devopsclient.Interface
	)

	if serviceClient.isBasicAuth() {
		client = gitee.NewClient(newHTTPCient(roundTripper), serviceClient.getPassword())
		token = serviceClient.getPassword()
	} else {
		serviceClient.OAuth2Info.AccessTokenKey = k8s.GiteeAccessTokenKey
		serviceClient.OAuth2Info.Scope = k8s.GiteeAccessTokenScope
		client = gitee.NewOAuthClient(newHTTPCient(roundTripper), serviceClient.OAuth2Info.AccessToken)
		token = serviceClient.OAuth2Info.AccessToken
	}

	devopsClient = NewDevOpsClient("gitee", "", serviceClient.getHost(),
		devopsclient.NewAPIKey("access_token", "query", token),
		devopsclient.NewTransport(roundTripper))
	err = client.SetBaseURL(serviceClient.Service.GetHost())
	if err != nil {
		return nil, err
	}
	return &GiteeClient{ServiceClient: serviceClient, Client: client, devopsClient: devopsClient}, nil
}

func (c *GiteeClient) CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getHost(), nil)
}

func (c *GiteeClient) CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	url := fmt.Sprintf("%suser?%s=%s", c.getBaseUrl(), gitee.AccessToken, c.getAccessToken())
	return CheckService(url, c.getHeader(lastModifyAt))
}

func (c *GiteeClient) GetLatestRepoCommit(repoID, owner, repoName, repoFullName string) (commit *devops.RepositoryCommit, status *devops.HostPortStatus) {
	return GetLatestRepoCommit(c.devopsClient, repoID, owner, repoName, repoFullName)
}

func (c *GiteeClient) getAllRepos() (repositories []*gitee.Project, err error) {
	var (
		wg      = sync.WaitGroup{}
		perPage = 5000
	)

	glog.V(5).Infof("fetch data in page %d", 1)
	firstPageResult, response, err := c.Client.Projects.List("",
		&gitee.ListProjectsOptions{
			ListOptions: gitee.ListOptions{
				Page:    1,
				PerPage: perPage,
			},
		})
	if err != nil {
		glog.Errorf("error list project in gitee: %v", err)
		return
	}

	if firstPageResult != nil {
		repositories = append(repositories, firstPageResult...)
	}

	if response == nil || response.TotalPages < 2 {
		glog.V(5).Infof("No next page, return current result")
		return
	}

	for page := 2; page < response.TotalPages+1; page++ {
		glog.V(5).Infof("fetch data in page %d", page)
		wg.Add(1)
		go func(page, perPage int) {
			defer wg.Done()

			pagedResult, _, err := c.Client.Projects.List("",
				&gitee.ListProjectsOptions{
					ListOptions: gitee.ListOptions{
						Page:    page,
						PerPage: k8s.PerPage,
					},
				})
			if err != nil {
				return
			}
			if pagedResult != nil {
				repositories = append(repositories, pagedResult...)
			}
		}(page, k8s.PerPage)
	}
	wg.Wait()

	return
}

func (c *GiteeClient) GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error) {
	return GetRemoteRepos(c.devopsClient)
}

func (c *GiteeClient) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if giteeRepo, ok := remoteRepo.(*gitee.Project); ok {
		codeRepo = devops.OriginCodeRepository{
			CodeRepoServiceType: devops.CodeRepoServiceTypeGitee,
			ID:                  strconv.Itoa(giteeRepo.ID),
			Name:                giteeRepo.Name,
			FullName:            giteeRepo.FullName,
			Description:         giteeRepo.Description,
			HTMLURL:             giteeRepo.HtmlURL,
			CloneURL:            giteeRepo.GetCloneURL(),
			SSHURL:              giteeRepo.GetSSHURL(),
			Language:            giteeRepo.Language,
			CreatedAt:           ConvertToMetaTime(giteeRepo.CreatedAt),
			PushedAt:            ConvertToMetaTime(giteeRepo.PushedAt),
			UpdatedAt:           ConvertToMetaTime(giteeRepo.UpdatedAt),

			Private:      giteeRepo.Private,
			Size:         0,
			SizeHumanize: "0",
		}
	}

	return
}

func (c *GiteeClient) GetAuthorizeUrl(redirectUrl string) string {
	return fmt.Sprintf("%s/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		strings.TrimRight(c.getHtml(), "/"), c.getClientID(), redirectUrl, c.getScope())
}

func (c *GiteeClient) AccessToken(redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessTokenUrl()
		response       = &AccessTokenResponseGitee{}
	)

	return c.accessToken(response, redirectUrl, accessTokenUrl)
}

func (c *GiteeClient) getAccessToken() string {
	if c.isBasicAuth() {
		return c.getPassword()
	} else {
		return c.ServiceClient.getAccessToken()
	}
}

func (c *GiteeClient) getAccessTokenUrl() string {
	return fmt.Sprintf("%s/oauth/token", strings.TrimRight(c.getHtml(), "/"))
}

func (c *GiteeClient) getRepoUrl(repoName string) string {
	return fmt.Sprintf("%srepos/%s?%s=%s", c.getBaseUrl(), repoName, gitee.AccessToken, c.getAccessToken())
}

func (c *GiteeClient) getBaseUrl() string {
	return c.Client.BaseURL().String()
}

func (c *GiteeClient) CreateProject(options devops.CreateProjectOptions) (*devops.ProjectData, error) {
	// return c.createOrganization(options)
	return CreateCodeRepoProject(c.devopsClient, options)
}
func (c *GiteeClient) ListProjectDataList(options devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	// return c.listOrganizations(options)
	return ListCodeRepoProjects(c.devopsClient, options)
}

func (c *GiteeClient) createOrganization(option devops.CreateProjectOptions) (*devops.ProjectData, error) {
	giteeCreateOrgOption := createProjectOptionsAsGiteeCreateOrgOptions(option)

	org, resp, err := c.Client.Organizations.Create(giteeCreateOrgOption)
	if err != nil {
		glog.Errorf("create gitee organization error, options:%#v, error:%s", giteeCreateOrgOption, err.Error())
		return nil, err
	}

	if org == nil {
		err := fmt.Errorf("create gitee organization got unknow error")
		glog.Errorf("%s, options %#v , response: %#v", err.Error(), option, resp)
		return nil, errors.NewInternalError(err)
	}

	return giteeOrgAsProjectData(*org)
}

func giteeOrgAsProjectData(giteeOrg gitee.Organization) (*devops.ProjectData, error) {
	data, err := generic.MarshalToMapString(giteeOrg)
	if err != nil {
		glog.Errorf("mashall gitee organization data %#v error:%#v", giteeOrg, err)
		return nil, err
	}

	var projectData = &devops.ProjectData{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.ResourceKindProjectData,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: giteeOrg.Login,
			Annotations: map[string]string{
				devops.AnnotationsProjectDataAvatarURL:   giteeOrg.AvatarURL,
				devops.AnnotationsProjectDataAccessPath:  giteeOrg.Login,
				devops.AnnotationsProjectDataDescription: giteeOrg.Description,
				devops.AnnotationsProjectDataType:        string(devops.OriginCodeRepoRoleTypeOrg),
			},
		},
		Data: data,
	}
	return projectData, nil
}

func createProjectOptionsAsGiteeCreateOrgOptions(option devops.CreateProjectOptions) gitee.CreateOrganizationOptions {
	return gitee.CreateOrganizationOptions{
		Name:        option.Name,
		Org:         option.Name,
		Description: option.Name,
	}
}

func asGiteeListOrganization(options devops.ListProjectOptions) gitee.ListOrganizationsOptions {
	return gitee.ListOrganizationsOptions{
		ListOptions: gitee.ListOptions{
			PerPage: MaxGiteeOrganizationPerPage,
			Page:    1,
		},
	}
}

func giteeOrganizationListAsProjectDataList(orgs []*gitee.Organization) (*devops.ProjectDataList, error) {
	items := []devops.ProjectData{}

	for _, org := range orgs {
		projectData, err := giteeOrgAsProjectData(*org)
		if err != nil {
			glog.Errorf(err.Error())
			return nil, err
		}

		items = append(items, *projectData)
	}

	return &devops.ProjectDataList{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.ResourceKindProjectDataList,
		},
		Items: items,
	}, nil
}

func (c *GiteeClient) listOrganizations(options devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	giteeListOrgOptions := asGiteeListOrganization(options)
	orgs, resp, err := c.Client.Organizations.List("", &giteeListOrgOptions)
	if err != nil {
		glog.Errorf("list gitee organization error, options:%#v, error:%s", giteeListOrgOptions, err.Error())
		return nil, err
	}
	if orgs == nil {
		err := fmt.Errorf("list gitee orgs got unknow error")
		glog.Errorf("%s, options %#v , response: %#v", err.Error(), giteeListOrgOptions, resp)
		return nil, errors.NewInternalError(err)
	}

	return giteeOrganizationListAsProjectDataList(orgs)
}

const (
	MaxGiteeBranchPerPage = 100
)

func (c *GiteeClient) listBranches(owner, repo string) ([]gitee.Branch, error) {
	var branches []gitee.Branch
	hasNextPage := true
	// per_page is at most 100
	for page := 1; hasNextPage; page++ {
		options := &gitee.ListOptions{Page: page, PerPage: MaxGiteeBranchPerPage}
		items, response, err := c.Client.Projects.Branches(fmt.Sprintf("%s/%s", owner, repo), options)
		if err != nil {
			glog.Errorf(err.Error())
			return []gitee.Branch{}, err
		}
		if items == nil {
			err := fmt.Errorf("list gitee branch got unknow error")
			return []gitee.Branch{}, errors.NewInternalError(err)
		}
		if response.NextPage <= page {
			hasNextPage = false
		}
		branches = append(branches, items...)
	}
	return branches, nil
}

func (c *GiteeClient) GetBranches(owner, repoName, repoFullName string) ([]devops.CodeRepoBranch, error) {
	return GetBranches(c.devopsClient, owner, repoName, repoFullName)
}
