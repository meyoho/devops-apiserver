package externalversions_test

import (
	ext "alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"github.com/google/go-github/github"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
	"time"
)

func TestAccessTokenResponseGithub_ConvertToSecretDataOAuth2Details(t *testing.T) {
	response := ext.AccessTokenResponseGithub{
		AccessTokenResponse: ext.AccessTokenResponse{
			TokenType:   "bearer",
			AccessToken: "xxx",
		},
		Scope: "",
	}
	details := response.ConvertToSecretDataOAuth2Details()
	assert.NotEqual(t, details.CreatedAt, "")
}

func TestConvertToMetaTime(t *testing.T) {
	type Table struct {
		name     string
		input    interface{}
		expected *metav1.Time
	}
	now := time.Now()

	tests := []Table{
		{
			name:     "time is invalid value",
			input:    "xxxxx",
			expected: nil,
		},
		{
			name:     "time is nil",
			input:    nil,
			expected: nil,
		},
		{
			name:     "time is github.Timestamp",
			input:    github.Timestamp{Time: now},
			expected: &metav1.Time{Time: now},
		},
		{
			name:     "time is time.Time",
			input:    &now,
			expected: &metav1.Time{Time: now},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.expected, ext.ConvertToMetaTime(test.input))
		})
	}
}
