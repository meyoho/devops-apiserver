package externalversions

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
	"alauda.io/devops-apiserver/pkg/role"
	"alauda.io/devops-apiserver/pkg/util"
	"alauda.io/devops-apiserver/pkg/util/parallel"
	authclientset "bitbucket.org/mathildetech/auth-controller2/pkg/client/clientset/versioned"
	"fmt"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	clusterregistryclientset "k8s.io/cluster-registry/pkg/client/clientset/versioned"
	glog "k8s.io/klog"
)

// ACPClient acp client for product
type ACPClient struct {
	// current k8s cluster client of acp
	client                kubernetes.Interface
	authClient            authclientset.Interface
	clusterRegistryClient clusterregistryclientset.Interface
	restConfig            rest.Config
	// mulitClusterHost multi cluster host of acp
	mulitClusterHost string
	Filter           ProjectsFilter

	acpSystemNamespace string
	provider           v1alpha1.AnnotationProvider
}

type ACPOptions struct {
	Client             kubernetes.Interface
	RestConfig         *rest.Config
	MultiClusterHost   string
	AcpSystemNamespace string
	Provider           v1alpha1.AnnotationProvider
}

// NewACPClient constructor method for ACPClient
func NewACPClient(options ACPOptions) (ACPClient, error) {

	authClient, err := authclientset.NewForConfig(options.RestConfig)
	if err != nil {
		return ACPClient{}, err
	}

	clusterregistryclient, err := clusterregistryclientset.NewForConfig(options.RestConfig)
	if err != nil {
		return ACPClient{}, err
	}
	return ACPClient{
		client:                options.Client,
		authClient:            authClient,
		clusterRegistryClient: clusterregistryclient,
		restConfig:            *options.RestConfig,
		mulitClusterHost:      options.MultiClusterHost,
		Filter:                StandardProjectsFilter{},
		acpSystemNamespace:    options.AcpSystemNamespace,
		provider:              options.Provider,
	}, nil
}

var _ AlaudaProductClient = ACPClient{}

// GetProjectUserRoles fetch user roles based on namespaces that are used
// for a specific project
func (a ACPClient) GetProjectUserRoles(list *corev1.NamespaceList) (projects []*role.ProjectUserRoleAssignment, err error) {
	projects = a.Filter.FilterProjects(list, a.provider)
	var roleBindingList *rbacv1.RoleBindingList
	for i, project := range projects {
		roleBindingList, err = a.client.RbacV1().RoleBindings(project.Namespace).List(metav1.ListOptions{ResourceVersion: "0"})
		if err != nil {
			glog.Errorf("Error fetching ACP RoleBindings for namespace \"%s\": %v", project.Namespace, err)
			return
		}
		for _, rb := range roleBindingList.Items {
			project.UserRoles = append(project.UserRoles, role.NewUserAssignmentsFromRoleBinding(rb)...)
		}
		projects[i] = project
	}
	return
}

func (a ACPClient) GetClusterManagement() ClusterManagementInterface {

	return &acpMultiClusterManagement{
		k8sclient:             a.client,
		multClusterHost:       a.mulitClusterHost,
		authclient:            a.authClient,
		clusterRegistryClient: a.clusterRegistryClient,
		restConfig:            a.restConfig,
		acpSystemNamespace:    a.acpSystemNamespace,
	}
}

// GetProjectNamespaces will range all clusters that owned by current project, and range clusters to retrieve project namespace
// and construct to ClusterNamespaceInterface
func (a ACPClient) GetProjectNamespaces(namespace *corev1.Namespace) ([]ClusterNamespaceInterface, error) {
	if namespace == nil {
		glog.Infof("namespace is nil when get acp client")
		return []ClusterNamespaceInterface{}, nil
	}

	projectName := namespace.Name // ACP Project name will equal to it's devops namespace

	multiCluster, err := a.GetClusterManagement().ListClusters(projectName)

	if multiCluster == nil || (multiCluster != nil && len(multiCluster.Clusters()) == 0) {
		return nil, err
	}

	list, listErr := multiCluster.Parallel(func(clusterInterface ClusterInterface) parallel.Task {

		return func() (i interface{}, e error) {
			return listACPNamespaces(projectName, clusterInterface, a.provider)
		}

	})

	clusterNamespaceList := []ClusterNamespaceInterface{}
	for _, item := range list {
		arr := item.([]*acpClusterNamespace)
		for _, clusterNs := range arr {
			clusterNamespaceList = append(clusterNamespaceList, clusterNs)
			glog.V(5).Infof("Build cluster camespace client %s", *clusterNs)
		}
	}

	errs := util.MultiErrors{}
	if err != nil {
		errs = append(errs, err)
	}
	if listErr != nil {
		errs = append(errs, listErr)
	}

	if len(errs) == 0 {
		return clusterNamespaceList, nil
	}

	return clusterNamespaceList, &errs
}

func listACPNamespaces(projectName string, clusterInterface ClusterInterface, provider v1alpha1.AnnotationProvider) ([]*acpClusterNamespace, error) {
	projectannotation := provider.LabelAlaudaIOProjectKey()

	labelSelector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{
		MatchLabels: map[string]string{
			projectannotation: projectName,
		},
	})

	if err != nil {
		glog.Errorf("ERROR to transfer selector by labels %s=%s, error:%s", projectannotation, projectName, err.Error())
		return nil, err
	}

	namespaces := &corev1.NamespaceList{}
	err = clusterInterface.List(v1alpha1.ResourceNamespaces, metav1.ListOptions{LabelSelector: labelSelector.String()}, namespaces)
	if err != nil {
		glog.Errorf("ERROR to list namespaces from cluster:%s, error:%s", clusterInterface, err.Error())
		return nil, err
	}

	namespaceClients := []*acpClusterNamespace{}
	for _, namespace := range namespaces.Items {
		namespaceClients = append(namespaceClients, newACPClusterNamespace(namespace.Name, clusterInterface.(*ACPCluster)))
	}

	return namespaceClients, nil
}

// ProjectExists will return true constantly, for acp, we should not use this logic
func (a ACPClient) ProjectExists(namespace *corev1.Namespace) (bool, error) {
	return true, nil
}

var _ ClusterManagementInterface = &acpMultiClusterManagement{}

// ListClusters will list clusters in clusterregistry according project name,
// if project name is empty, will return all clusters
func (clusterM *acpMultiClusterManagement) ListClusters(projectName string) (ParallelClusterInterface, error) {

	clusterNames, err := clusterM.getClusterNames(projectName)
	if err != nil {
		return NewDefaultParallelCluster([]ClusterInterface{}), err
	}

	errs := util.MultiErrors{}

	items := []ClusterInterface{}
	for i, clusterName := range clusterNames {

		clusterClient, err := BuildACPClusterClient(clusterM.k8sclient, clusterM.restConfig, clusterM.clusterRegistryClient, clusterM.multClusterHost, clusterName, clusterM.acpSystemNamespace)
		if err != nil {
			glog.Errorf("ERROR build cluster client error:%s, cluster=%s, systemNamespace=%s", err.Error(), clusterName, clusterM.acpSystemNamespace)
			errs = append(errs, fmt.Errorf("Cluster %s: %s", clusterName, err.Error()))
			continue
		}

		glog.V(5).Infof("Build ACP Cluster Client %d:%s ", i, clusterName)

		items = append(items, NewACPCluster(clusterName, clusterClient))
	}

	if len(errs) != 0 {
		// may be some cluster has error ,but some are correctly
		return NewDefaultParallelCluster(items), &errs
	}

	return NewDefaultParallelCluster(items), nil
}

func (clusterM *acpMultiClusterManagement) getClusterNames(projectName string) ([]string, error) {
	clusterNameList := []string{}

	// if project name is empty, we will find all clusters in acp
	if projectName == "" {

		clusterList, err := clusterM.clusterRegistryClient.ClusterregistryV1alpha1().Clusters(clusterM.acpSystemNamespace).List(metav1.ListOptions{ResourceVersion: "0"})
		if err != nil {
			glog.Errorf("List ACP Cluster error:%s", err.Error())
			return nil, err
		}

		for _, cluster := range clusterList.Items {
			clusterNameList = append(clusterNameList, cluster.Name)
		}

		return clusterNameList, nil
	}

	project, err := clusterM.authclient.AuthV1().Projects().Get(projectName, metav1.GetOptions{
		ResourceVersion: "0",
	})
	if err != nil {
		glog.Errorf("ERROR to get project '%s', error:%s", projectName, err.Error())
		return nil, err
	}

	if len(project.Spec.Clusters) == 0 {
		glog.Infof("Project '%s' has no clusters", projectName)
		return []string{}, nil
	}

	for _, cluster := range project.Spec.Clusters {
		clusterNameList = append(clusterNameList, cluster.Name)
	}
	return clusterNameList, nil
}

func (clusterM *acpMultiClusterManagement) GetCluster(identity string) (ClusterInterface, error) {

	clusterClient, err := BuildACPClusterClient(clusterM.k8sclient, clusterM.restConfig, clusterM.clusterRegistryClient, clusterM.multClusterHost, identity, clusterM.acpSystemNamespace)
	if err != nil {
		glog.Errorf("can not get client of cluster '%s', systemNamespace '%s', error:%s", identity, clusterM.acpSystemNamespace, err.Error())
		return nil, err
	}

	return NewACPCluster(identity, clusterClient), nil
}

type acpMultiClusterManagement struct {
	k8sclient             kubernetes.Interface
	authclient            authclientset.Interface
	clusterRegistryClient clusterregistryclientset.Interface

	restConfig rest.Config

	multClusterHost    string
	acpSystemNamespace string
}

var _ ClusterInterface = &ACPCluster{}

type ACPCluster struct {
	name   string
	client rest.Interface
}

// NewDevOpsCluster will create DevOps Manager Cluster
func NewDevOpsCluster(client rest.Interface) *ACPCluster {
	return &ACPCluster{
		name:   "ACP",
		client: client,
	}
}

func NewACPCluster(name string, client rest.Interface) *ACPCluster {
	return &ACPCluster{
		name:   name,
		client: client,
	}
}

func (c *ACPCluster) List(kind string, opts metav1.ListOptions, result runtime.Object) error {
	err := c.client.Get().
		Resource(kind).
		VersionedParams(&opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return err
}
func (c *ACPCluster) Get(kind string, name string, options metav1.GetOptions, result runtime.Object) (err error) {
	err = c.client.Get().
		Resource(kind).
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
func (c *ACPCluster) Delete(kind string, name string, options *metav1.DeleteOptions) error {
	return c.client.Delete().
		Resource(kind).
		Name(name).
		Body(options).
		Do().
		Error()
}
func (c *ACPCluster) DeleteCollection(kind string, options *metav1.DeleteOptions, listOptions metav1.ListOptions) error {
	return c.client.Delete().
		Resource(kind).
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Body(options).
		Do().
		Error()
}
func (c *ACPCluster) Create(kind string, object runtime.Object, result runtime.Object) (err error) {
	err = c.client.Post().
		Resource(kind).
		Body(object).
		Do().
		Into(result)
	return
}
func (c *ACPCluster) Update(kind string, object metav1.Object) (err error) {
	err = c.client.Put().
		Resource(kind).
		Name(object.GetName()).
		Body(object).
		Do().Error()
	return
}

func (c *ACPCluster) GetClusterIdentity() string {
	return c.name
}

func (c *ACPCluster) Namespace(namespace string) ClusterNamespaceInterface {
	return newACPClusterNamespace(namespace, c)
}

func (c *ACPCluster) GetClusterName() string {
	return c.name
}

func (c *ACPCluster) String() string {
	return fmt.Sprintf("ACP Cluster:%s", c.name)
}

type acpClusterNamespace struct {
	Namespace string
	Cluster   ClusterInterface

	client rest.Interface
}

func newACPClusterNamespace(namespace string, cluster *ACPCluster) *acpClusterNamespace {
	return &acpClusterNamespace{
		Namespace: namespace,
		client:    cluster.client,
		Cluster:   cluster,
	}
}

func (c *acpClusterNamespace) List(kind string, opts metav1.ListOptions, result runtime.Object) (err error) {
	err = c.client.Get().
		Namespace(c.CurrentNamespace()).
		Resource(kind).
		VersionedParams(&opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
func (c *acpClusterNamespace) Get(kind string, name string, options metav1.GetOptions, result runtime.Object) (err error) {
	err = c.client.Get().
		Namespace(c.CurrentNamespace()).
		Resource(kind).
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
func (c *acpClusterNamespace) Delete(kind string, name string, options *metav1.DeleteOptions) error {
	return c.client.Delete().
		Namespace(c.CurrentNamespace()).
		Resource(kind).
		Name(name).
		Body(options).
		Do().
		Error()
}
func (c *acpClusterNamespace) DeleteCollection(kind string, options *metav1.DeleteOptions, listOptions metav1.ListOptions) error {
	return c.client.Delete().
		Namespace(c.CurrentNamespace()).
		Resource(kind).
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Body(options).
		Do().
		Error()
}
func (c *acpClusterNamespace) Create(kind string, object runtime.Object, result runtime.Object) (err error) {
	err = c.client.Post().
		Namespace(c.CurrentNamespace()).
		Resource(kind).
		Body(object).
		Do().
		Into(result)
	return
}
func (c *acpClusterNamespace) Update(kind string, object metav1.Object) (err error) {
	err = c.client.Put().
		Namespace(c.CurrentNamespace()).
		Resource(kind).
		Name(object.GetName()).
		Body(object).
		Do().Error()
	return
}
func (c *acpClusterNamespace) GetClusterIdentity() string {
	return c.Cluster.GetClusterIdentity()
}
func (c *acpClusterNamespace) CurrentNamespace() string {
	return c.Namespace
}
func (c *acpClusterNamespace) CurrentCluster() ClusterInterface {
	return c.Cluster
}

func (c *acpClusterNamespace) String() string {
	return fmt.Sprintf("ACP Cluster Namespace:%s/%s", c.Cluster.GetClusterName(), c.Namespace)
}
