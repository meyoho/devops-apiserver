package externalversions

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/api/errors"
	glog "k8s.io/klog"
	"net/http"
	"strconv"
	"sync"
)

var SearchOptionsMap = map[string]bool{
	"status":    true,
	"priority":  true,
	"issuetype": true,
	"all":       true,
}

type Postdata struct {
	Jql        string   `json:"jql"`
	StartAt    int      `json:"startAt"`
	MaxResults int      `json:"maxResults"`
	Fields     []string `json:"fields"`
	Expand     []string `json:"expand"`
}

var AllOptions = []string{"status", "issuetype", "priority"}

type JiraClient struct {
	ProjectManagementClient
	devopsClient devopsclient.Interface
}

func NewJiraClient(roundTripper http.RoundTripper, endpoint string, username string, credential string, timeoutsecond int) *JiraClient {
	serviceClient := NewProjectManagementClient(roundTripper, endpoint, username, credential, timeoutsecond)
	devopsClient := NewDevOpsClient("jira", "", endpoint, devopsclient.NewBasicAuth(username, credential),
		devopsclient.NewTransport(serviceClient.Client.Transport))
	return &JiraClient{ProjectManagementClient: serviceClient, devopsClient: devopsClient}
}

func (c *JiraClient) SetTransport(trans http.RoundTripper) {
	c.ProjectManagementClient.Client.Transport = trans
}

func (c *JiraClient) GetProjectList(listoptions *devops.RoleMappingListOptions) ([]project, error) {

	projects := new([]project)
	urlSuffix := fmt.Sprintf("/rest/api/2/project")
	resp, err := c.RequestProjectManagement(urlSuffix, JiraType)
	if err != nil {
		return *projects, err
	}
	_ = json.Unmarshal([]byte(resp.Body), &projects)
	if listoptions == nil {
		return *projects, nil
	}
	projectsArray := listoptions.Projects
	if len(projectsArray) != 0 {
		newprojects := []project{}
		for _, project := range *projects {
			for _, projectname := range projectsArray {
				if project.Name == projectname {
					newprojects = append(newprojects, project)
				}
			}
		}
		projects = &newprojects

	}
	return *projects, nil
}

func (c *JiraClient) getoptiondata(optiontype string, result *devops.IssueFilterDataList, errlist chan error, wg *sync.WaitGroup) {

	urlSuffix := fmt.Sprintf("/rest/api/2/%s", optiontype)
	optionvalues := make([]issueoption, 0)

	resp, err := c.RequestProjectManagement(urlSuffix, JiraType)

	if err != nil {
		errlist <- err
	}
	_ = json.Unmarshal([]byte(resp.Body), &optionvalues)

	for _, item := range optionvalues {
		option := devops.IssueFilterData{}
		option.Name = item.Name
		option.ID = item.ID
		result.SetFilterData(optiontype, option)
	}
	wg.Done()
}

func (c *JiraClient) GetPermissionNameIdMap() (map[string]int, error) {
	permissionNameIDmap := make(map[string]int)
	//get all Permission Scheme name and ID, and make a map for name:id
	permissionschemes := new([]permissionscheme)
	urlSuffix := fmt.Sprintf("/rest/api/2/role")
	resp, err := c.RequestProjectManagement(urlSuffix, JiraType)
	if err != nil {
		return permissionNameIDmap, err
	}
	_ = json.Unmarshal([]byte(resp.Body), &permissionschemes)
	for _, permissionscheme := range *permissionschemes {
		permissionNameIDmap[permissionscheme.Name] = permissionscheme.ID

	}
	return permissionNameIDmap, nil

}

func (c *JiraClient) GetRoleMappping(projects []project, permissionNameIDmap map[string]int) (*devops.RoleMapping, error) {
	rolemapping := new(devops.RoleMapping)
	//range for projects
	for _, project := range projects {
		projectscheme := new(devops.ProjectUserRoleOperation)
		projectscheme.Project.Name = project.Name

		projectscheme.Project.Data = make(map[string]string)

		//range for permission in one project
		for name, id := range permissionNameIDmap {
			RoleActors := new(roleactors)
			urlSuffix := fmt.Sprintf("rest/api/2/project/%s/role/%d", project.ID, id)
			resp, _ := c.RequestProjectManagement(urlSuffix, JiraType)
			_ = json.Unmarshal([]byte(resp.Body), &RoleActors)

			//range for actor in a permission that in one project
			for _, actor := range RoleActors.Actors {
				userinfo := new(UserInfo)
				urlSuffix := fmt.Sprintf("/rest/api/2/user?username=%s", actor.Name)
				resp, _ = c.RequestProjectManagement(urlSuffix, JiraType)
				_ = json.Unmarshal([]byte(resp.Body), &userinfo)

				userroleoperation := new(devops.UserRoleOperation)
				userroleoperation.Role.Name = name
				userroleoperation.User.Username = userinfo.Name
				userroleoperation.User.Email = userinfo.EmailAddress
				projectscheme.UserRoleOperations = append(projectscheme.UserRoleOperations, *userroleoperation)
			}
		}

		rolemapping.Spec = append(rolemapping.Spec, *projectscheme)
	}
	return rolemapping, nil
}

func (c *JiraClient) AddUserforRole(userandoperation devops.UserRoleOperation, projectIdNameMap map[string]string, operation devops.ProjectUserRoleOperation, permissionNameIDmap map[string]int) {
	data := make(map[string][]string, 0)
	data["user"] = []string{}
	data["user"] = append(data["user"], userandoperation.User.Username)
	bodyJson, _ := json.Marshal(data)

	addurl := fmt.Sprintf("%s/rest/api/2/project/%s/role/%s", c.Endpoint, projectIdNameMap[operation.Project.Name], strconv.Itoa(permissionNameIDmap[userandoperation.Role.Name]))
	req, _ := http.NewRequest(http.MethodPost, addurl, bytes.NewBuffer(bodyJson))
	req.SetBasicAuth(c.Username, c.Credential)
	req.Header.Set("Content-Type", "application/json")
	resp, _ := c.DoRequest(req)
	if resp.StatusCode != 200 {
		glog.Errorf("resp code is %v and response is %v", resp.StatusCode, string(resp.Body))
	}
}

func (c *JiraClient) RemoveUserforRole(userandoperation devops.UserRoleOperation, projectIdNameMap map[string]string, operation devops.ProjectUserRoleOperation, permissionNameIDmap map[string]int) {
	removeurl := fmt.Sprintf("%s/rest/api/2/project/%s/role/%s?user=%s", c.Endpoint, projectIdNameMap[operation.Project.Name], strconv.Itoa(permissionNameIDmap[userandoperation.Role.Name]), userandoperation.User.Username)
	req, _ := http.NewRequest(http.MethodDelete, removeurl, nil)
	req.SetBasicAuth(c.Username, c.Credential)
	resp, err := c.Client.Do(req)
	if err != nil {
		glog.Errorf("remove user %s error: %v", userandoperation.User.Username, err)
	}
	body, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 204 {
		glog.Errorf("remove Error and info is %v", string(body))
	}
}

func (c *JiraClient) Authenticate() (status *devops.HostPortStatus, err error) {

	urlSuffix := fmt.Sprintf("/rest/api/2/user/search?username=%s", c.Username)
	resp, err := c.RequestProjectManagement(urlSuffix, JiraType)

	status = &devops.HostPortStatus{}

	if resp.StatusCode == 200 {
		status.StatusCode = 200
		status.Response = fmt.Sprintf("correct username and password")
	} else {
		status.StatusCode = resp.StatusCode
		status.Response = string(resp.Body)
	}
	return
}

func (c *JiraClient) SearchforUsers(name string) (result *devops.ProjectmanagementUser, err error) {
	glog.V(6).Infof("Search for  %s projectmanagement users.", c.Endpoint)
	result = &devops.ProjectmanagementUser{}
	urlSuffix := fmt.Sprintf("/rest/api/2/user/search?username=%s", name)
	resp, err := c.RequestProjectManagement(urlSuffix, JiraType)
	if err != nil {
		glog.Errorf("search for porjectmangement user failed err: %v", err)
		return
	}
	userInfoList := make([]UserInfo, 0)
	err = json.Unmarshal([]byte(resp.Body), &userInfoList)
	if err != nil {
		return nil, fmt.Errorf("Json unmarshal for userInfoList failed and error is %v", err)
	}
	for _, user := range userInfoList {
		User := devops.User{}
		User.Username = user.Name
		User.Email = user.EmailAddress
		User.ID = user.Key
		result.Items = append(result.Items, User)
	}
	return result, nil
}

func (c *JiraClient) GetProjects(page, pagesize string) (result *devops.ProjectDataList, err error) {
	return GetProjects(c.devopsClient, page, pagesize)
}

func (c *JiraClient) CreateProject(projectName, projectDescription, projectLead, projectKey string) (result *devops.ProjectData, err error) {
	return CreateProject(c.devopsClient, projectName, projectDescription, projectLead, projectKey)
}

func (c *JiraClient) GetIssueOptionValue(issuetype string) (result *devops.IssueFilterDataList, err error) {
	return GetIssueOption(c.devopsClient, issuetype)
}

func (c *JiraClient) GetIssueList(listoption *devopsclient.ListIssuesOptions) (*devops.IssueDetailList, error) {
	return GetIssuseList(c.devopsClient, listoption)
}

func (c *JiraClient) GetIssue(Issuekey string) (*devops.IssueDetail, error) {
	return GetIssue(c.devopsClient, Issuekey)
}

func (c *JiraClient) GetUserCount() (usercount int, err error) {
	glog.V(6).Infof("Get %s projectmanagement usercount.", c.Endpoint)
	urlSuffix := fmt.Sprintf("/rest/api/2/user/search?username=.&startAt=0&maxResults=2000")
	resp, err := c.RequestProjectManagement(urlSuffix, JiraType)
	if err != nil {
		return 0, fmt.Errorf("Error happned when get usercount and error is : %v", err)
	}
	Users := make([]UserInfo, 0)
	err = json.Unmarshal([]byte(resp.Body), &Users)
	if err != nil {
		err = errors.NewBadRequest("Can't get usercount, please check url or secret of your service")
		glog.Errorf("get user count failed: %v", err)
		return 0, err
	}
	return len(Users), nil
}
