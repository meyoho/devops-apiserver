package externalversions_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestExternalVersions(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("externalversions.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/client/thirdparty/devops/externalversions", []Reporter{junitReporter})
}
