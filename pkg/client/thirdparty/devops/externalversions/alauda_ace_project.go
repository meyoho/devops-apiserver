package externalversions

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	"alauda.io/devops-apiserver/pkg/role"
)

// ACEProject emulates a ACE Project structure
type ACEProject struct {
	// For ACE projects lots of information should be consolidated:
	// in DevOps projects there are a few things to consider:
	// 1. projects can be pure projects only and it should be the parent one
	// 2. projects can be a subproject kind, where users are divided between sub groups (subprojects).
	// In case the project has a subgroup it will probably will have namespaces groups as well
	//  - if the project has namespaces and subproject all namespaces should be ignored
	//  - if the project only has subprojects then all parent project users should be associated in the subproject
	// 3. projects can be a namespace kind. In a standard project only namespaces and the parent project are present.
	// in this case the namespace projects should be ignored and all users migrated to the parent project

	// Type specifies if this project is a space/subproject or a namespace project or a pure project
	Type ProjectType

	// ProjectUserRoleAssignment integrates the original project data
	*role.ProjectUserRoleAssignment

	// SubProjects its own subprojects. Only project type should have subprojects
	SubProjects []*ACEProject

	UserRoles ace.RoleUserAssignmentList

	// this is used mainly to cache the subprojects data instead of looping all over every time
	hasSpaces bool
}

// NewACEProject inits a ACEProject based on the namespace data of the project
func NewACEProject(project *role.ProjectUserRoleAssignment) (result *ACEProject) {
	result = &ACEProject{
		ProjectUserRoleAssignment: project,
		SubProjects:               make([]*ACEProject, 0, 10),
		Type:                      ProjectTypeProject,
	}
	if project.Project.SubProject != "" {
		result.Type = ProjectTypeSpace
		result.SubProjects = nil
	}
	return
}

// ProjectType determines the kind of project
type ProjectType string

const (
	// ProjectTypeProject this is a real project type and not its sub classifications
	ProjectTypeProject ProjectType = "project"
	// ProjectTypeSpace this is a sub classification of a project, known as subproject/space
	ProjectTypeSpace ProjectType = "space"
	// ProjectTypeNamespace this is
	ProjectTypeNamespace ProjectType = "namespace"
)

// process used after fetching user roles to process its data and return its user's role assignment
func (proj *ACEProject) process() (result []*role.ProjectUserRoleAssignment) {
	result = make([]*role.ProjectUserRoleAssignment, 0, 1+len(proj.SubProjects))
	proj.hasSpaces = len(proj.SubProjects) > 0
	switch proj.Type {
	case ProjectTypeProject:
		// for projects there are two conditions that should be checked:
		// if there are only project scoped and namespace scoped roles
		// all of the roles should be given to all spaces
		// otherwise namespace scoped roles should be ignored
		onlyNamespaces := !proj.UserRoles.HasSpaceRoles()
		// fetch all project scoped roles
		projectScopedRoles := make(ace.RoleUserAssignmentList, 0, len(proj.UserRoles))
		// and index scope related roles
		spaceScopedRoles := make(map[string]ace.RoleUserAssignmentList)
		for _, userRole := range proj.UserRoles {
			if userRole.IsProjectScopedRole() || (userRole.GetNamespaceUUID() != "" && onlyNamespaces) {
				// this is a project scoped role
				projectScopedRoles = append(projectScopedRoles, userRole)
			} else if userRole.GetSpace() != "" {
				// space scoped role, add to the index
				spaceRoleAssignments, ok := spaceScopedRoles[userRole.GetSpace()]
				if !ok || spaceRoleAssignments == nil {
					spaceRoleAssignments = make(ace.RoleUserAssignmentList, 0, 10)
				}
				spaceRoleAssignments = append(spaceRoleAssignments, userRole)
				spaceScopedRoles[userRole.GetSpace()] = spaceRoleAssignments
			}
		}
		// after the projects scoped roles and space scoped roles are organized
		// we should loop all the spaces and assign their roles
		// if no spaces available on the project, then just return it as project roles
		if proj.hasSpaces {
			// having spaces we should not return project roles, but only space roles
			// for that we add a project with no roles
			// and loop through spaces adding its specific roles + project scoped roles
			result = append(result, proj.ProjectUserRoleAssignment)
			for _, space := range proj.SubProjects {
				spaceRoleAssignments := spaceScopedRoles[space.ProjectUserRoleAssignment.SubProject]
				if spaceRoleAssignments == nil {
					spaceRoleAssignments = make(ace.RoleUserAssignmentList, 0, 0)
				}
				space.UserRoles = append(projectScopedRoles, spaceRoleAssignments...)
				result = append(result, space.process()...)
			}
		} else {
			// return as project roles for pure project cases
			// this is when there are no subprojects or namespaces
			proj.ProjectUserRoleAssignment.UserRoles = convertRoleUserAssignmentToUserAssignment(projectScopedRoles)
			result = append(result, proj.ProjectUserRoleAssignment)
		}
	case ProjectTypeSpace:
		proj.ProjectUserRoleAssignment.UserRoles = convertRoleUserAssignmentToUserAssignment(proj.UserRoles)
		result = append(result, proj.ProjectUserRoleAssignment)
	}
	return
}

// processProjectData in order to start correctly all data should be processed and orgnized as ACEProjects
func processProjectData(projects []*role.ProjectUserRoleAssignment) (calculatedProjects map[string]*ACEProject) {
	// need to calculate all namespace project data into a ACE specific project structure
	// project: the real project
	// - subprojects: all space projects
	calculatedProjects = map[string]*ACEProject{}
	unprocessed := map[string][]*ACEProject{}
	for _, project := range projects {
		aceProject := NewACEProject(project)
		switch aceProject.Type {
		case ProjectTypeProject:
			calculatedProjects[aceProject.Name] = aceProject
		case ProjectTypeSpace:
			// add all subprojects to a queue and add to its respective parents later
			subprojects, ok := unprocessed[aceProject.Name]
			if !ok || subprojects == nil {
				subprojects = make([]*ACEProject, 0, 10)
			}
			subprojects = append(subprojects, aceProject)
			unprocessed[aceProject.Name] = subprojects
		}
	}
	// if the unprocessed queue we need to add to the parent project
	if len(unprocessed) > 0 {
		for key, children := range unprocessed {
			if parent, ok := calculatedProjects[key]; ok && parent != nil {
				parent.SubProjects = append(parent.SubProjects, children...)
			} else if len(children) > 0 {
				// this is a project with only children. The project namespace was not added
				// so we need to add this project manually
				aceProject := NewACEProject(children[0].ProjectUserRoleAssignment)
				subProjectUA := aceProject.ProjectUserRoleAssignment
				// make a copy of the data and get its pointer. We dont want to affect the child pointer
				aceProject.ProjectUserRoleAssignment = role.NewProject(subProjectUA.Project.Namespace).
					SetName(subProjectUA.Project.Name).
					SetSubProject("")
				aceProject.ProjectUserRoleAssignment.Namespace = ""
				aceProject.Type = ProjectTypeProject
				aceProject.SubProjects = children
				calculatedProjects[key] = aceProject
			}
		}
	}
	return
}

func convertRoleUserAssignmentToUserAssignment(roleUserAssignment ace.RoleUserAssignmentList) (result []*role.UserAssignment) {
	result = make([]*role.UserAssignment, 0, len(roleUserAssignment))
	if len(roleUserAssignment) > 0 {
		for _, user := range roleUserAssignment {
			result = append(result, role.NewUserAssignment(
				user.GetUsername(), user.GetEmail(), user.GetRole(), user.GetSpace(),
			))
		}
	}
	return
}
