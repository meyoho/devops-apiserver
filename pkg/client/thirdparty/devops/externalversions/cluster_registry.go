package externalversions

import (
	"fmt"
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	clusterregistryv1alpha1 "k8s.io/cluster-registry/pkg/apis/clusterregistry/v1alpha1"
	clusterregistryclientset "k8s.io/cluster-registry/pkg/client/clientset/versioned"
	glog "k8s.io/klog"
)

func BuildACPClusterClient(devopsK8Sclient kubernetes.Interface, restConfig rest.Config, clusterregistryclient clusterregistryclientset.Interface,
	multiClusterHost string, clusterName string, acpSystemNamespace string) (client rest.Interface, err error) {

	cluster, err := clusterregistryclient.ClusterregistryV1alpha1().Clusters(acpSystemNamespace).Get(clusterName, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("Get Cluster '%s' error:%s", clusterName, err.Error())
		return nil, err
	}

	return buildACPClusterClient(devopsK8Sclient, restConfig, multiClusterHost, *cluster)
}

func buildACPClusterClient(devopsK8Sclient kubernetes.Interface, restConfig rest.Config, multiClusterHost string, cluster clusterregistryv1alpha1.Cluster) (client rest.Interface, err error) {
	if multiClusterHost == "" {
		return nil, fmt.Errorf("multiClusterHost should not be empty")
	}

	config := rest.CopyConfig(&restConfig)

	err = appendConfigAuthInfo(devopsK8Sclient, cluster, config)
	if err != nil {
		return nil, err
	}

	config.Host = fmtMultiClusterHost(multiClusterHost, cluster.Name)
	config.Insecure = true
	config.CAData = nil
	config.CAFile = ""
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		glog.Errorf("Error to Create client from config:%#v, error:%s", config, err.Error())
		return nil, err
	}

	return clientset.CoreV1().RESTClient(), nil
}

func fmtMultiClusterHost(host, clusterName string) string {
	return fmt.Sprintf("%s/kubernetes/%s", host, clusterName)
}

func appendConfigAuthInfo(devopsK8Sclient kubernetes.Interface, cluster clusterregistryv1alpha1.Cluster, config *rest.Config) (err error) {

	if cluster.Spec.AuthInfo.Controller == nil {
		glog.Infof("AuthIno.Controller is nil , cluster=%s", cluster.Name)
		return
	}

	kind := cluster.Spec.AuthInfo.Controller.Kind

	switch kind {
	case "Secret":
		return appendSecretAuthInfo(devopsK8Sclient, cluster, config)
	default:
		return fmt.Errorf("Unknown Kind '%s' in cluster '%s''s authinfo", kind, cluster.Name)
	}
}

func appendSecretAuthInfo(k8sclient kubernetes.Interface, cluster clusterregistryv1alpha1.Cluster, config *rest.Config) error {

	secretRef := cluster.Spec.AuthInfo.Controller
	secret, err := k8sclient.CoreV1().Secrets(secretRef.Namespace).Get(secretRef.Name, metav1.GetOptions{ResourceVersion: "0"})
	if err != nil {
		glog.Errorf("error to get secret %s/%s, err:%s", secretRef.Namespace, secretRef.Name, err.Error())
		return err
	}

	data, ok := secret.Data["token"]
	if !ok {
		err := fmt.Errorf("cannot get token from cluster '%s', token is not exists in secret data. secret:%s/%s", cluster.Name, secretRef.Namespace, secretRef.Name)
		glog.Errorf(err.Error())
		return err
	}

	config.BearerToken = strings.TrimSuffix(string(data), "\n")
	return nil
}
