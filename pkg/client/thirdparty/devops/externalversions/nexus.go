package externalversions

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"

	corev1 "k8s.io/api/core/v1"
	"net/http"
	"strconv"
	"strings"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	logger "k8s.io/klog"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	thirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/role"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	k8srequest "k8s.io/apiserver/pkg/endpoints/request"
)

var (
	nexusFilename              = "nexusclient"
	nexusApiPrefix             = "service/rest"
	nexusApiVersion            = "v1"
	isFirstTimeInitNexusScript = true
)

type NexusClient struct {
	Endpoint        string
	BasicAuthInfo   *k8s.SecretDataBasicAuth
	Timeoutsecond   int
	devopsClient    devopsclient.Interface
	systemNamespace string
}

func NewNexusClientForARM(endpoint string, basicAuthInfo *k8s.SecretDataBasicAuth, timeoutsecond int, systemNamespace string) ArtifactRegistryManagerClientInterface {
	devopsClient := NewDevOpsClient("nexus", "v3", endpoint, devopsclient.NewBasicAuth(basicAuthInfo.Username, basicAuthInfo.Password))
	return &NexusClient{Endpoint: endpoint, BasicAuthInfo: basicAuthInfo, Timeoutsecond: timeoutsecond, devopsClient: devopsClient, systemNamespace: systemNamespace}
}

func NewNexusClientForAR(endpoint string, basicAuthInfo *k8s.SecretDataBasicAuth, timeoutsecond int, systemNamespace string) ArtifactRegistryClientInterface {
	devopsClient := NewDevOpsClient("nexus", "v3", endpoint, devopsclient.NewBasicAuth(basicAuthInfo.Username, basicAuthInfo.Password))
	return &NexusClient{Endpoint: endpoint, BasicAuthInfo: basicAuthInfo, Timeoutsecond: timeoutsecond, devopsClient: devopsClient, systemNamespace: systemNamespace}
}

func (nc *NexusClient) InitManager() (err []error) {
	return Init(nc.devopsClient)
}

func (nc *NexusClient) CreateScript(scriptName string) error {

	methodName := "CreateScript"

	logger.V(9).Infof("%s %s create script %s", nexusFilename, methodName, scriptName)

	script, err := GetFileContentAsStringLines("./groovy/nexus/" + scriptName + ".groovy")
	if err != nil {
		return err
	}

	createRepoScriptObj := map[string]string{
		"name":    scriptName,
		"type":    "groovy",
		"content": script,
	}

	createRepoScriptJson, err := json.Marshal(createRepoScriptObj)
	if err != nil {
		return err
	}

	_, err = nc.httpClient(http.MethodPost, nc.Url(stringJoin(nexusApiPrefix, nexusApiVersion, "script")), bytes.NewReader(createRepoScriptJson), 204, "application/json")
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return err
	}

	return nil
}

func (nc *NexusClient) UpdateScript(scriptName string) error {

	methodName := "UpdateScript"

	logger.V(9).Infof("%s %s update script %s", nexusFilename, methodName, scriptName)

	script, err := GetFileContentAsStringLines("./groovy/nexus/" + scriptName + ".groovy")
	if err != nil {
		return err
	}

	createRepoScriptObj := map[string]string{
		"name":    scriptName,
		"type":    "groovy",
		"content": script,
	}

	createRepoScriptJson, err := json.Marshal(createRepoScriptObj)
	if err != nil {
		return err
	}

	_, err = nc.httpClient(http.MethodPut, nc.Url(stringJoin(nexusApiPrefix, nexusApiVersion, "script", scriptName)), bytes.NewReader(createRepoScriptJson), 204, "application/json")
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return err
	}

	return nil
}

func (nc *NexusClient) CheckAuthentication(username string, password string) (status *devops.HostPortStatus, err error) {

	apiPath := nc.Url(stringJoin(nexusApiPrefix, nexusApiVersion, "repositories"))

	status, err = BasicAuth(apiPath, username, password, nil)
	return status, err
}

func (nc *NexusClient) Url(suffix string) string {
	path := stringJoin(strings.TrimRight(nc.Endpoint, "/"),
		strings.TrimLeft(suffix, "/"))
	return path
}

func stringJoin(ss ...string) string {
	return strings.Join(ss, "/")
}

func (nc *NexusClient) ListRegistry() (devops.ArtifactRegistryList, error) {
	return ListRegistry(nc.devopsClient)
}

func (nc *NexusClient) CreateRegistry(artifactType string, v map[string]string) error {
	return CreateRegistry(nc.devopsClient, artifactType, v)
}

func (nc *NexusClient) GetRepository(artifactRepositoryName string) (registry devops.ArtifactRegistry, err error) {

	methodName := "GetRepository"

	body := map[string]string{"name": artifactRepositoryName}

	bodyJson, err := json.Marshal(body)
	if err != nil {
		logger.V(9).Infof("%s %s err is %#v", nexusFilename, methodName, err)
		return registry, err
	}

	scriptName := thirdparty.ScriptMapAll["get_repository"]

	logger.V(9).Infof("%s %s scriptName is %s", nexusFilename, methodName, scriptName)

	if scriptName == "" {
		return registry, errors.New("not found scriptName")
	}

	apiPath := nc.Url(stringJoin(nexusApiPrefix, nexusApiVersion, "script", scriptName, "run"))

	logger.V(9).Infof("%s %s api path is %s and artifactRepositoryName is %s and request body is %s", nexusFilename, methodName, apiPath, artifactRepositoryName, bodyJson)

	respBytes, err := nc.httpClient(http.MethodPost, apiPath, bytes.NewBuffer(bodyJson), 200, "text/plain")
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return
	}

	responseBody := responseBody{}

	err = json.Unmarshal(respBytes, &responseBody)
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return registry, err
	}
	logger.V(9).Infof("%s %s responseBody is %#v", nexusFilename, methodName, responseBody)

	logger.V(9).Infof("%s %s responseBody result is %#v", nexusFilename, methodName, responseBody.Result)

	repository := Repository{}

	err = json.Unmarshal([]byte(responseBody.Result), &repository)
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return registry, err
	}
	logger.V(9).Infof("%s %s repository is %#v", nexusFilename, methodName, repository)

	result := devops.ArtifactRegistry{
		ObjectMeta: metav1.ObjectMeta{
			Name: repository.Name,
		},
		Spec: devops.ArtifactRegistrySpec{
			Type:                 repository.Format,
			ArtifactRegistryName: repository.Name,
			ArtifactRegistryArgs: map[string]string{
				"type": repository.Type,
			},
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: repository.Url,
				},
			},
		},
	}

	logger.V(9).Infof("%s %s result is %#v", nexusFilename, methodName, result)

	return result, err
}

type responseBody struct {
	Name   string `json:"name"`
	Result string `json:"result"`
}

type Repository struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Format string `json:"format"`
	Url    string `json:"url"`
	Config string `json:"config"`
}

func GetFileContentAsStringLines(filePath string) (string, error) {
	logger.Infof("get file content as lines: %v", filePath)
	result := []string{}
	b, err := ioutil.ReadFile(filePath)
	if err != nil {
		logger.Errorf("read file: %v error: %v", filePath, err)
		return "", err
	}
	s := string(b)
	s = strings.Replace(s, "\"", "'", -1)
	//s = strings.Replace(s, "\n", "\\n", -1)

	logger.Infof("get file content as lines: %v, size: %v", filePath, len(result))
	return s, nil
}

func (nc *NexusClient) ListBlobStore() (result devops.BlobStoreOptionList, err error) {
	return ListBlobStore(nc.devopsClient)
}

func (nc *NexusClient) GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error) {

	methodName := "GetRoleMapping"

	logger.V(9).Infof("%s %s  opts is %#v and serviceName is %s", nexusFilename, methodName, opts, serviceName)

	result = &devops.RoleMapping{Spec: make([]devops.ProjectUserRoleOperation, 0, len(opts.Projects))}
	for _, project := range opts.Projects {

		logger.V(9).Infof("%s %s in projects loop,project is %s and serviceName is %s", nexusFilename, methodName, project, serviceName)

		if err != nil {
			logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
			return result, err
		}

		projectData := devops.ProjectData{
			ObjectMeta: metav1.ObjectMeta{
				Name: project,
			},
		}

		userRoleOperations, err := nc.getUserRoleOperationRequest(serviceName, project)
		if err != nil {
			logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
			return result, err
		}

		toolProject := devops.ProjectUserRoleOperation{
			Project:            projectData,
			UserRoleOperations: userRoleOperations,
		}

		result.Spec = append(result.Spec, toolProject)

	}

	logger.V(9).Infof("%s %s  result is %#v", nexusFilename, methodName, result)
	return
}

func (nc *NexusClient) getUserRoleOperationRequest(serviceName string, project string) (result []devops.UserRoleOperation, err error) {
	methodName := "getUserRoleOperationRequest"
	scriptName := thirdparty.ScriptMapAll["map_roleuser"]

	logger.V(9).Infof("%s %s scriptName is %s", nexusFilename, methodName, scriptName)

	if scriptName == "" {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return result, err
	}

	apiPath := nc.Url(stringJoin(nexusApiPrefix, nexusApiVersion, "script", scriptName, "run"))

	logger.V(9).Infof("%s %s api path is %s ", nexusFilename, methodName, apiPath)

	body := map[string]string{"id": project + "-" + serviceName}

	bodyJson, err := json.Marshal(body)
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return result, err
	}

	logger.V(9).Infof("%s %s request body is %s ", nexusFilename, methodName, bodyJson)

	bodyBytes := bytes.NewBuffer(bodyJson)

	successCode := 200

	respBody := responseBody{}

	method := http.MethodPost

	respBytes, err := nc.httpClient(method, apiPath, bodyBytes, successCode, "text/plain")
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return result, err
	}

	err = json.Unmarshal(respBytes, &respBody)
	if err != nil {
		logger.Errorf("%s %s unmarshal respBytes to &respBody err is %#v", nexusFilename, methodName, err)
		return nil, err
	}
	logger.V(9).Infof("%s %s respBody is %#v", nexusFilename, methodName, respBody)

	roleUserMap := make(map[string][]string)

	err = json.Unmarshal([]byte(respBody.Result), &roleUserMap)
	if err != nil {
		logger.Errorf("%s %s Unmarshal respBody.Result to roleUserMap err is %#v", nexusFilename, methodName, err)
		return
	}

	for r, us := range roleUserMap {
		role, error := nc.convertNexusRole2PlatformRole(r)
		if error != nil {
			return result, error
		}
		for _, u := range us {
			uro := devops.UserRoleOperation{
				User: devops.UserMeta{
					Username: u,
				},
				Role: devops.RoleMeta{
					Name: role,
				},
			}
			result = append(result, uro)
		}
	}

	logger.V(9).Infof("%s %s  result is %#v", nexusFilename, methodName, result)

	return
}

func (nc *NexusClient) convertNexusRole2PlatformRole(nexusRole string) (role string, err error) {
	splitString := strings.Split(nexusRole, "-")
	size := len(splitString)
	if size <= 0 {
		return role, errors.New("role name error.")
	}
	role = splitString[size-1]
	return
}

func (nc *NexusClient) httpClient(method string, apiPath string, bodyBytes io.Reader, successCode int, contentType string) ([]byte, error) {

	methodName := "httpClient"

	req, err := http.NewRequest(method, apiPath, bodyBytes)
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return nil, err
	}

	req.SetBasicAuth(nc.BasicAuthInfo.Username, nc.BasicAuthInfo.Password)
	req.Header.Set("accept", "application/json")
	req.Header.Set("Content-Type", contentType)
	client := http.DefaultClient
	client.Timeout = time.Duration(nc.Timeoutsecond) * time.Second
	resp, err := client.Do(req)
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return nil, err
	}

	defer func() {
		closeError := resp.Body.Close()
		if closeError != nil {
			logger.Errorf(closeError.Error())
		}
	}()

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return nil, err
	}

	statusCode := resp.StatusCode
	if statusCode != successCode {
		return nil, errors.New("Response code is " + strconv.Itoa(resp.StatusCode) + " and body is " + string(respBytes))
	}

	logger.V(9).Infof("%s %s respBytes is %#v", nexusFilename, methodName, respBytes)

	return respBytes, err

}

func (nc *NexusClient) AddUserRoleWithActions(project string, registry string, users []string, roleType string, artifactType string, getter dependency.Manager) (err error) {
	methodName := "AddUserRoleWithActions"
	scriptName := thirdparty.ScriptMapAll["add_userrolewithactions"]

	if scriptName == "" {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return
	}

	logger.V(9).Infof("%s %s scriptName is %s", nexusFilename, methodName, scriptName)

	apiPath := nc.Url(stringJoin(nexusApiPrefix, nexusApiVersion, "script", scriptName, "run"))

	logger.V(9).Infof("%s %s api path is %s ", nexusFilename, methodName, apiPath)

	devopsConfigMap, err := nc.getDevOpsConfigMap(getter)

	logger.V(9).Infof("%s %s get devopsConfigMap is %#v", nexusFilename, methodName, devopsConfigMap)

	devopsConfigMapRoleMapping, err := nc.newPlatformFromConfigMap(devopsConfigMap)

	logger.V(9).Infof("%s %s get devopsConfigMapRoleMapping is %#v", nexusFilename, methodName, devopsConfigMapRoleMapping)

	maven2Platform := devopsConfigMapRoleMapping.Platform("Maven2")

	logger.V(9).Infof("%s %s get maven2Platform is %#v", nexusFilename, methodName, maven2Platform)

	customRoleMap := maven2Platform.CustomMapping

	logger.V(9).Infof("%s %s get customRoleMap is %#v", nexusFilename, methodName, customRoleMap)

	rolePrivilegesMap := make(map[string]map[string][]string)

	for roleType, v := range customRoleMap {

		privilegesMap := make(map[string][]string)

		for privilegeType, actions := range v {
			actionsList := strings.Split(actions, ",")
			privilegesMap[privilegeType] = actionsList
		}

		rolePrivilegesMap[roleType] = privilegesMap
	}

	logger.V(9).Infof("%s %s get rolePrivilegesMap is %#v", nexusFilename, methodName, rolePrivilegesMap)

	body := map[string]interface{}{"project": project, "registry": registry, "users": users, "roleType": roleType, "artifactType": artifactType, "rolePrivilegesMap": rolePrivilegesMap[roleType]}

	bodyJson, err := json.Marshal(body)
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return
	}

	logger.V(9).Infof("%s %s request body is %s ", nexusFilename, methodName, bodyJson)

	bodyBytes := bytes.NewBuffer(bodyJson)

	successCode := 200

	respBody := responseBody{}

	method := http.MethodPost

	respBytes, err := nc.httpClient(method, apiPath, bodyBytes, successCode, "text/plain")
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return
	}

	err = json.Unmarshal(respBytes, &respBody)
	if err != nil {
		logger.Errorf("%s %s unmarshal respBytes to &respBody err is %#v", nexusFilename, methodName, err)
		return
	}
	logger.V(9).Infof("%s %s respBody is %#v", nexusFilename, methodName, respBody)

	return
}

func (nc *NexusClient) AddUserRole(project string, registry string, users []string, roleType string, artifactType string) (err error) {
	methodName := "AddUserRole"
	scriptName := thirdparty.ScriptMapAll["add_userrole"]

	if scriptName == "" {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return
	}

	logger.V(9).Infof("%s %s scriptName is %s", nexusFilename, methodName, scriptName)

	apiPath := nc.Url(stringJoin(nexusApiPrefix, nexusApiVersion, "script", scriptName, "run"))

	logger.V(9).Infof("%s %s api path is %s ", nexusFilename, methodName, apiPath)

	body := map[string]interface{}{"project": project, "registry": registry, "users": users, "roleType": roleType, "artifactType": artifactType}

	bodyJson, err := json.Marshal(body)
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return
	}

	logger.V(9).Infof("%s %s request body is %s ", nexusFilename, methodName, bodyJson)

	bodyBytes := bytes.NewBuffer(bodyJson)

	successCode := 200

	respBody := responseBody{}

	method := http.MethodPost

	respBytes, err := nc.httpClient(method, apiPath, bodyBytes, successCode, "text/plain")
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return
	}

	err = json.Unmarshal(respBytes, &respBody)
	if err != nil {
		logger.Errorf("%s %s unmarshal respBytes to &respBody err is %#v", nexusFilename, methodName, err)
		return
	}
	logger.V(9).Infof("%s %s respBody is %#v", nexusFilename, methodName, respBody)

	return
}

func (nc *NexusClient) RemoveUserRole(project string, registry string, users []string, roleType string) (err error) {
	methodName := "RemoveUserRole"
	scriptName := thirdparty.ScriptMapAll["remove_userrole"]

	if scriptName == "" {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return
	}

	logger.V(9).Infof("%s %s scriptName is %s", nexusFilename, methodName, scriptName)

	apiPath := nc.Url(stringJoin(nexusApiPrefix, nexusApiVersion, "script", scriptName, "run"))

	logger.V(9).Infof("%s %s api path is %s ", nexusFilename, methodName, apiPath)

	body := map[string]interface{}{"project": project, "registry": registry, "users": users, "roleType": roleType}

	bodyJson, err := json.Marshal(body)
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return
	}

	logger.V(9).Infof("%s %s request body is %s ", nexusFilename, methodName, bodyJson)

	bodyBytes := bytes.NewBuffer(bodyJson)

	successCode := 200

	respBody := responseBody{}

	method := http.MethodPost

	respBytes, err := nc.httpClient(method, apiPath, bodyBytes, successCode, "text/plain")
	if err != nil {
		logger.Errorf("%s %s err is %#v", nexusFilename, methodName, err)
		return
	}

	err = json.Unmarshal(respBytes, &respBody)
	if err != nil {
		logger.Errorf("%s %s unmarshal respBytes to &respBody err is %#v", nexusFilename, methodName, err)
		return
	}
	logger.V(9).Infof("%s %s respBody is %#v", nexusFilename, methodName, respBody)

	return
}

func (nc *NexusClient) getDevOpsConfigMap(getter dependency.Manager) (*corev1.ConfigMap, error) {
	context := k8srequest.NewContext()
	context = k8srequest.WithNamespace(context, nc.systemNamespace)
	depList := getter.Get(context, devops.TypeConfigMap, devops.SettingsConfigMapName)
	if err := depList.Validate(); err != nil {
		return nil, err
	}
	configMap := corev1.ConfigMap{}
	depList.GetInto(devops.TypeConfigMap, &configMap)
	// GetInto will not return error if error happened, trick it here
	if configMap.Name == "" {
		return nil, fmt.Errorf("Error to get config map '%s/%s'", nc.systemNamespace, devops.SettingsConfigMapName)
	}

	return &configMap, nil
}

// NewFromConfigMap starts a scheme from devops-config configmap
func (nc *NexusClient) newPlatformFromConfigMap(configMap *corev1.ConfigMap) (s *role.Scheme, err error) {

	//get role_mapping
	roleSyncSchemaString, ok := configMap.Data[devops.SettingsKeyRoleMapping]
	if !ok || len(roleSyncSchemaString) == 0 {
		err = fmt.Errorf("Key of '%s' in ConfigMap '%s' is not found or content is empty", devops.SettingsKeyRoleMapping, devops.SettingsConfigMapName)
		logger.Errorf(err.Error())
		return nil, err
	}

	//
	s, err = role.NewFromString(roleSyncSchemaString)
	if err != nil {
		logger.Errorf("Cannot init RoleSyncSchema from roleSyncSchemaString: %s, error:%s", roleSyncSchemaString, err)
		return nil, err
	}
	return
}
