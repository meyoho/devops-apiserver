package externalversions

import (
	"context"
	"fmt"
	"net/http"

	"k8s.io/apimachinery/pkg/api/errors"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/go-bitbucket"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"github.com/dustin/go-humanize"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

type BitbucketClient struct {
	*ServiceClient
	Client       *bitbucket.Client
	devopsClient devopsclient.Interface
}

func NewBitbucketClient(serviceClient *ServiceClient, roundTripper http.RoundTripper) (*BitbucketClient, error) {
	var (
		err    error
		client = &bitbucket.Client{}

		devopsClient devopsclient.Interface
	)

	if serviceClient.isBasicAuth() {
		client, err = bitbucket.NewBasicAuthClient(newHTTPCient(roundTripper), "", serviceClient.getUsername(), serviceClient.getPassword())
		devopsClient = NewDevOpsClient("bitbucket", "", serviceClient.getHost(),
			devopsclient.NewBasicAuth(serviceClient.getUsername(), serviceClient.getPassword()),
			devopsclient.NewTransport(roundTripper))
	} else {
		serviceClient.OAuth2Info.AccessTokenKey = k8s.BitbucketAccessTokenKey
		serviceClient.OAuth2Info.Scope = k8s.BitbucketAccessTokenScope

		client = bitbucket.NewClient(newHTTPCient(roundTripper), serviceClient.OAuth2Info.AccessToken)
		devopsClient = NewDevOpsClient("bitbucket", "", serviceClient.getHost(),
			devopsclient.NewBearerToken(serviceClient.OAuth2Info.AccessToken),
			devopsclient.NewTransport(roundTripper))
	}
	if err != nil {
		return nil, err
	}
	return &BitbucketClient{ServiceClient: serviceClient, Client: client, devopsClient: devopsClient}, nil
}

func (c *BitbucketClient) CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getHost(), nil)
}

func (c *BitbucketClient) CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	url := fmt.Sprintf("%suser", c.getBaseUrl())
	return CheckService(url, c.getHeader(lastModifyAt))
}

func (c *BitbucketClient) GetLatestRepoCommit(repoID, owner, repoName, repoFullName string) (commit *devops.RepositoryCommit, status *devops.HostPortStatus) {
	return GetLatestRepoCommit(c.devopsClient, repoID, owner, repoName, repoFullName)
}

func (c *BitbucketClient) getAllRepos() (repositories []*bitbucket.Repository, err error) {
	var (
		perPage = 5000
		page    = 0
	)

	for {
		page = page + 1

		glog.V(7).Infof("fetch data in page %d", page)
		firstPageResult, _, err1 := c.Client.Repositories.List("",
			&bitbucket.ListRepositoriesOptions{Role: bitbucket.Role(bitbucket.RoleValueMember),
				ListOptions: bitbucket.ListOptions{
					Page:    page,
					PageLen: perPage,
				},
			})
		if err1 != nil {
			err = err1
			glog.Errorf("error list project in bitbucket: %v", err)
			return
		}

		if firstPageResult != nil && firstPageResult.Values != nil {
			repositories = append(repositories, firstPageResult.Values...)
		}

		if firstPageResult == nil || firstPageResult.Next == "" {
			glog.V(7).Infof("No next page, return current result")
			return
		}
	}
}

func (c *BitbucketClient) GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error) {
	return GetRemoteRepos(c.devopsClient)
}

func (c *BitbucketClient) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if bitbucketRepo, ok := remoteRepo.(*bitbucket.Repository); ok {
		codeRepo = devops.OriginCodeRepository{
			CodeRepoServiceType: devops.CodeRepoServiceTypeBitbucket,
			ID:                  bitbucketRepo.UUID,
			Name:                bitbucketRepo.Name,
			FullName:            bitbucketRepo.FullName,
			Description:         bitbucketRepo.Description,
			HTMLURL:             bitbucketRepo.Links.HTML.Href,
			CloneURL:            bitbucketRepo.GetCloneURL(),
			SSHURL:              bitbucketRepo.GetSSHURL(),
			Language:            bitbucketRepo.Language,
			Owner: devops.OwnerInRepository{
				ID:   bitbucketRepo.Owner.UUID,
				Name: bitbucketRepo.Owner.Username,
				Type: devops.GetOwnerType(bitbucketRepo.Owner.Type),
			},

			CreatedAt: ConvertToMetaTime(bitbucketRepo.CreatedOn),
			// UpdatedOn on bitbuckect will reflect of pushedAt
			PushedAt:  ConvertToMetaTime(bitbucketRepo.UpdatedOn),
			UpdatedAt: ConvertToMetaTime(bitbucketRepo.UpdatedOn),

			Private:      bitbucketRepo.IsPrivate,
			Size:         int64(bitbucketRepo.GetSize() * 1000),
			SizeHumanize: humanize.Bytes(uint64(bitbucketRepo.GetSize())),
		}
	}

	return
}

func (c *BitbucketClient) getRepoUrl(repoName string) string {
	return fmt.Sprintf("%srepositories/%s", c.getBaseUrl(), repoName)
}

func (c *BitbucketClient) GetAuthorizeUrl(redirectUrl string) string {
	return fmt.Sprintf("%s/site/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		c.getHtml(), c.getClientID(), redirectUrl, c.getScope())
}

func (c *BitbucketClient) getAccessTokenUrl() string {
	return fmt.Sprintf("%s/site/oauth2/access_token", c.getHtml())
}

func (c *BitbucketClient) AccessToken(redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessTokenUrl()
		response       = &AccessTokenResponseBitbucket{}
	)

	return c.accessToken(response, redirectUrl, accessTokenUrl)
}

func (c *BitbucketClient) getBaseUrl() string {
	return c.Client.BaseURL().String()
}

// CreateProject is abstract of create team for bitbucket
func (c *BitbucketClient) CreateProject(options devops.CreateProjectOptions) (*devops.ProjectData, error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("ProjectData"), http.MethodPost)
}

// ListProjectDataList will list all teams or bitbucket
func (c *BitbucketClient) ListProjectDataList(options devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	// cxt := context.Background()
	// return c.listTeams(cxt, options)
	return ListCodeRepoProjects(c.devopsClient, options)
}

func (c *BitbucketClient) listTeams(ctx context.Context, options devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	bitbucketTeamsListOptions := &bitbucket.TeamsListOptions{
		Role: bitbucket.Role(bitbucket.RoleValueMember),
	}

	teams, response, err := c.Client.Teams.ListAll(ctx, bitbucketTeamsListOptions, nil)
	if err != nil {
		glog.Errorf("list bitbucket teams error, bitbucket listTeamsOptions:%#v, error:%s", bitbucketTeamsListOptions, sprintBitbucketErrorResponse(err))
		return nil, err
	}
	if teams == nil {
		err := fmt.Errorf("list bitbucket teams got unknow error")
		glog.Errorf("%s, options %#v, response:%#v", err.Error(), bitbucketTeamsListOptions, response)
		return nil, errors.NewInternalError(err)
	}

	projectDataList := &devops.ProjectDataList{
		TypeMeta: metav1.TypeMeta{
			Kind:       devops.ResourceKindProjectDataList,
			APIVersion: devops.APIVersionV1Alpha1,
		},
		Items: []devops.ProjectData{},
	}

	for _, team := range teams.Values {
		projectData := devops.ProjectData{
			TypeMeta: metav1.TypeMeta{
				Kind:       devops.ResourceKindProjectData,
				APIVersion: devops.APIVersionV1Alpha1,
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: team.Username,
				Annotations: map[string]string{
					"avatarURL":   team.Links.Avatar.Href,
					"accessPath":  team.Links.Html.Href,
					"description": team.DisplayName,
					"type":        team.Type_,
				},
			},
		}
		projectDataList.Items = append(projectDataList.Items, projectData)
	}

	return projectDataList, nil
}

func sprintBitbucketErrorResponse(errorResponse error) string {
	if errorResponse == nil {
		return ""
	}

	if errResp, ok := errorResponse.(*bitbucket.ErrorResponse); ok {
		return fmt.Sprintf("ErrorResponse:%s, origin response body:%s", errResp.Error(), string(errResp.Body))
	}
	return errorResponse.Error()
}

const (
	MaxBitbucketBranchPerPage = 100
)

func (c *BitbucketClient) GetBranches(owner, repoName, repoFullName string) ([]devops.CodeRepoBranch, error) {
	return GetBranches(c.devopsClient, owner, repoName, repoFullName)
}

func (c *BitbucketClient) listBranches(owner, repo string) ([]bitbucket.Branch, error) {
	var branches []bitbucket.Branch
	hasNextPage := true
	// per_page is at most 100
	for page := 1; hasNextPage; page++ {
		options := &bitbucket.ListOptions{Page: page, PageLen: MaxBitbucketBranchPerPage}
		items, _, err := c.Client.Branches.List(fmt.Sprintf("%s/%s", owner, repo), options)
		if err != nil {
			return []bitbucket.Branch{}, err
		}
		if items == nil {
			err := fmt.Errorf("list bitbucket branch got unknow error")
			return []bitbucket.Branch{}, errors.NewInternalError(err)
		}
		if items.Size <= page {
			hasNextPage = false
		}
		branches = append(branches, items.Branches...)
	}
	return branches, nil
}
