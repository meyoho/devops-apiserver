package externalversions_test

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	mace "alauda.io/devops-apiserver/pkg/mock/ace"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"alauda.io/devops-apiserver/pkg/role"
	"alauda.io/devops-apiserver/pkg/util/parallel"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("ACEClient.GetProjectUserRoles", func() {
	var (
		mockCtrl *gomock.Controller
		client   externalversions.ACEClient
		aceMock  *mace.MockInterface
		nslist   *corev1.NamespaceList
		result   []*role.ProjectUserRoleAssignment
		err      error
		provider devops.AnnotationProvider
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		aceMock = mace.NewMockInterface(mockCtrl)
		provider = devops.NewAnnotationProvider(devops.UsedBaseDomain)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should return a list of projects with user assignments", func() {
		nslist = &corev1.NamespaceList{
			Items: []corev1.Namespace{
				newNamespace("project1", "project1", ""),
				newNamespace("project2", "project2", ""),
			},
		}
		// first project
		aceMock.EXPECT().
			GetRolesWithUsers(context.TODO(), ace.ProjectOpts("project1")).
			Return(ace.RoleUserAssignmentList{
				&ace.RoleUserAssignment{RoleName: "admin", User: "admin", Email: "admin@example"},
				&ace.RoleUserAssignment{RoleName: "auditor", User: "auditor", Email: "auditor@example"},
				&ace.RoleUserAssignment{RoleName: "developer", User: "dev1", Email: "dev1@example"},
				&ace.RoleUserAssignment{RoleName: "developer", User: "test1", Email: "test1@example"},
			}, nil)

		aceMock.EXPECT().
			GetRolesWithUsers(context.TODO(), ace.ProjectOpts("project2")).
			Return(ace.RoleUserAssignmentList{}, nil)

		client = externalversions.NewACEClient(aceMock, provider)
		result, err = client.GetProjectUserRoles(nslist)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(result).ToNot(BeEmpty(), "should return a project data list with data")
		Expect(result).To(HaveLen(2), "should only return two projects")
		Expect(result).To(ContainElement(
			role.NewProject("project1").
				SetName("project1").
				SetSubProject("").
				AddUserRole(
					role.NewUserAssignment("admin", "admin@example", "admin", ""),
					role.NewUserAssignment("auditor", "auditor@example", "auditor", ""),
					role.NewUserAssignment("dev1", "dev1@example", "developer", ""),
					role.NewUserAssignment("test1", "test1@example", "developer", ""),
				),
		))
		Expect(result).To(ContainElement(
			role.NewProject("project2").
				SetName("project2").
				SetSubProject(""),
		))
	})

	It("should return a list of sub projects with user assignments", func() {
		nslist = &corev1.NamespaceList{
			Items: []corev1.Namespace{
				newNamespace("project-dev", "project", "dev"),
				newNamespace("project-test", "project", "test"),
			},
		}
		// first project
		aceMock.EXPECT().
			GetRolesWithUsers(context.TODO(), ace.ProjectOpts("project")).
			Return(ace.RoleUserAssignmentList{
				&ace.RoleUserAssignment{RoleName: "admin", User: "admin", Email: "admin@example"},
				&ace.RoleUserAssignment{RoleName: "auditor", User: "auditor", Email: "auditor@example"},
				&ace.RoleUserAssignment{RoleName: "developer", User: "dev1", Email: "dev1@example", SpaceName: "dev"},
				&ace.RoleUserAssignment{RoleName: "developer", User: "test1", Email: "test1@example", SpaceName: "dev"},
				&ace.RoleUserAssignment{RoleName: "developer", User: "dev2", Email: "dev2@example", SpaceName: "test"},
				&ace.RoleUserAssignment{RoleName: "developer", User: "test2", Email: "test2@example", SpaceName: "test"},
			}, nil)

		client = externalversions.NewACEClient(aceMock, provider)
		result, err = client.GetProjectUserRoles(nslist)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(result).ToNot(BeEmpty(), "should return a project data list with data")
		Expect(result).To(HaveLen(3), "should only return three projects: one without subproject and both with subproject")
		// main project
		project := getProject(result, "project", "")
		Expect(project).ToNot(BeNil(), "should contain the main project but empty")
		Expect(project.SubProject).To(BeEmpty(), "should not be a subproject")
		Expect(project.UserRoles).To(HaveLen(0), "main project should not contain any roles")

		// project-dev
		project = getProject(result, "project", "dev")
		Expect(project).ToNot(BeNil(), "should contain the project-dev subproject")
		Expect(project.SubProject).To(Equal("dev"))
		Expect(project.UserRoles).To(HaveLen(4), "should have project scoped roles and subproject scoped roles")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("dev1", "dev1@example", "developer", "dev")), "project-dev should have dev1 as developer")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("test1", "test1@example", "developer", "dev")), "project-dev should have test1 as developer")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("admin", "admin@example", "admin", "")), "project-dev should have admin as admin")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("auditor", "auditor@example", "auditor", "")), "project-dev should have auditor as auditor")

		// project-test
		project = getProject(result, "project", "test")
		Expect(project).ToNot(BeNil(), "should contain the project-test subproject")
		Expect(project.SubProject).To(Equal("test"))
		Expect(project.UserRoles).To(HaveLen(4), "should have project scoped roles and subproject scoped roles")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("dev2", "dev2@example", "developer", "test")), "project-test should have dev2 as developer")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("test2", "test2@example", "developer", "test")), "project-test should have test2 as developer")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("admin", "admin@example", "admin", "")), "project-test should have admin as admin")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("auditor", "auditor@example", "auditor", "")), "project-test should have auditor as auditor")
	})

	It("should return a list of projects and subprojects with user assignments", func() {
		nslist = &corev1.NamespaceList{
			Items: []corev1.Namespace{
				newNamespace("project", "project"),
				newNamespace("project-dev", "project", "dev"),
				newNamespace("project-test", "project", "test"),
				newNamespace("project2", "project2"), // only project level
				newNamespace("project3", "project3"), // only project level
			},
		}
		// first project
		aceMock.EXPECT().
			GetRolesWithUsers(context.TODO(), ace.ProjectOpts("project")).
			Return(ace.RoleUserAssignmentList{
				&ace.RoleUserAssignment{RoleName: "admin", User: "admin", Email: "admin@example"},
				&ace.RoleUserAssignment{RoleName: "auditor", User: "auditor", Email: "auditor@example"},
				&ace.RoleUserAssignment{RoleName: "developer", User: "dev1", Email: "dev1@example", SpaceName: "dev"},
				&ace.RoleUserAssignment{RoleName: "developer", User: "test1", Email: "test1@example", SpaceName: "dev"},
				&ace.RoleUserAssignment{RoleName: "developer", User: "dev2", Email: "dev2@example", SpaceName: "test"},
				&ace.RoleUserAssignment{RoleName: "developer", User: "test2", Email: "test2@example", SpaceName: "test"},
			}, nil)

		// second project, with empty list
		aceMock.EXPECT().
			GetRolesWithUsers(context.TODO(), ace.ProjectOpts("project2")).
			Return(ace.RoleUserAssignmentList{}, nil)

		aceMock.EXPECT().
			GetRolesWithUsers(context.TODO(), ace.ProjectOpts("project3")).
			Return(ace.RoleUserAssignmentList{
				&ace.RoleUserAssignment{RoleName: "admin", User: "admin", Email: "admin@example"},
				&ace.RoleUserAssignment{RoleName: "auditor", User: "auditor", Email: "auditor@example"},
			}, nil)

		client = externalversions.NewACEClient(aceMock, provider)
		result, err = client.GetProjectUserRoles(nslist)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(result).ToNot(BeEmpty(), "should return a project data list with data")
		Expect(result).To(HaveLen(5), "should only return five projects: three without subproject and two with subproject")

		// main project
		project := getProject(result, "project", "")
		Expect(project).ToNot(BeNil(), "should contain project but empty")
		Expect(project.SubProject).To(BeEmpty(), "should not be a subproject")
		Expect(project.UserRoles).To(HaveLen(0), "main project should not contain any roles")

		// project-dev
		project = getProject(result, "project", "dev")
		Expect(project).ToNot(BeNil(), "should contain the project-dev subproject")
		Expect(project.SubProject).To(Equal("dev"))
		Expect(project.UserRoles).To(HaveLen(4), "should have project scoped roles and subproject scoped roles")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("dev1", "dev1@example", "developer", "dev")))
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("test1", "test1@example", "developer", "dev")), "project-dev should have test1 as developer")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("admin", "admin@example", "admin", "")), "project-dev should have admin as admin")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("auditor", "auditor@example", "auditor", "")), "project-dev should have auditor as auditor")

		// project-test
		project = getProject(result, "project", "test")
		Expect(project).ToNot(BeNil(), "should contain the project-test subproject")
		Expect(project.SubProject).To(Equal("test"))
		Expect(project.UserRoles).To(HaveLen(4), "should have project scoped roles and subproject scoped roles")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("dev2", "dev2@example", "developer", "test")), "project-test should have dev2 as developer")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("test2", "test2@example", "developer", "test")), "project-test should have test2 as developer")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("admin", "admin@example", "admin", "")), "project-test should have admin as admin")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("auditor", "auditor@example", "auditor", "")), "project-test should have auditor as auditor")

		// main project2
		project = getProject(result, "project2", "")
		Expect(project).ToNot(BeNil(), "should contain the project2 but empty")
		Expect(project.SubProject).To(BeEmpty(), "project2 should not be a subproject")
		Expect(project.UserRoles).To(HaveLen(0), "project2 should not contain any roles")

		// main project
		project = getProject(result, "project3", "")
		Expect(project).ToNot(BeNil(), "should contain the project3 with roles")
		Expect(project.SubProject).To(BeEmpty(), "should not be a subproject")
		Expect(project.UserRoles).To(HaveLen(2), "project3 should contain 2 roles")
		// project3 roles
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("admin", "admin@example", "admin", "")))
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("auditor", "auditor@example", "auditor", "")))

	})

	It("should distribute namespace users to default groups when the project is namespaced project", func() {

		nslist = &corev1.NamespaceList{
			Items: []corev1.Namespace{
				newNamespace("project1-dev", "project1", "dev"),
				newNamespace("project1-ops", "project1", "ops"),
				newNamespace("project2-dev", "project2", "dev"), // only project level
				newNamespace("project1", "project1", ""),        // only project level
			},
		}
		// users in first project are all project level user
		aceMock.EXPECT().
			GetRolesWithUsers(context.TODO(), ace.ProjectOpts("project1")).
			Return(ace.RoleUserAssignmentList{
				&ace.RoleUserAssignment{RoleName: "admin", User: "admin", Email: "admin@example"},
				&ace.RoleUserAssignment{RoleName: "auditor", User: "namespace_auditor", Email: "auditor@example", NamespaceUUID: "whatever"},
			}, nil)

		// second project, with empty list
		aceMock.EXPECT().
			GetRolesWithUsers(context.TODO(), ace.ProjectOpts("project2")).
			Return(ace.RoleUserAssignmentList{}, nil)

		client = externalversions.NewACEClient(aceMock, provider)
		result, err = client.GetProjectUserRoles(nslist)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(result).ToNot(BeEmpty(), "should return a project data list with data")
		Expect(result).To(HaveLen(5), "should only return five projects: two without subproject and three with subproject")

		// project1
		project := getProject(result, "project1", "")
		Expect(project).ToNot(BeNil(), "should contain project1 but empty")
		Expect(project.SubProject).To(BeEmpty(), "should not be a subproject")
		Expect(project.UserRoles).To(HaveLen(0), "main project should not contain any roles")

		// project1-dev
		project = getProject(result, "project1", "dev")
		Expect(project).ToNot(BeNil(), "should contain project1-dev with roles")
		Expect(project.SubProject).To(Equal("dev"), "should be a subproject")
		Expect(project.UserRoles).To(HaveLen(2), "project1-dev should have two project scoped roles")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("admin", "admin@example", "admin", "")))
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("namespace_auditor", "auditor@example", "auditor", "")))

		// project1-ops
		project = getProject(result, "project1", "ops")
		Expect(project).ToNot(BeNil(), "should contain project1-ops with roles")
		Expect(project.SubProject).To(Equal("ops"), "should be a subproject")
		Expect(project.UserRoles).To(HaveLen(2), "project1-ops should have two project scoped roles")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("admin", "admin@example", "admin", "")))
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("namespace_auditor", "auditor@example", "auditor", "")))

		// project2
		project = getProject(result, "project2", "")
		Expect(project).ToNot(BeNil(), "should contain project2 but empty")
		Expect(project.SubProject).To(BeEmpty(), "should not be a subproject")
		Expect(project.UserRoles).To(HaveLen(0), "project2 should not contain any roles")

		// project2-
		project = getProject(result, "project2", "dev")
		Expect(project).ToNot(BeNil(), "should contain project2-dev but empty")
		Expect(project.SubProject).To(Equal("dev"), "should be a subproject")
		Expect(project.UserRoles).To(HaveLen(0), "project2-dev should not contain any roles")
	})

	It("should distribute project level user to groups", func() {
		nslist = &corev1.NamespaceList{
			Items: []corev1.Namespace{
				newNamespace("project1", "project1", "dev"),
				newNamespace("project1", "project1", "ops"),
				newNamespace("project2", "project2", "dev"), // only project level
			},
		}
		// first project
		aceMock.EXPECT().
			GetRolesWithUsers(context.TODO(), ace.ProjectOpts("project1")).
			Return(ace.RoleUserAssignmentList{
				&ace.RoleUserAssignment{RoleName: "admin", User: "admin", Email: "admin@example"},
				&ace.RoleUserAssignment{RoleName: "auditor", User: "auditor", Email: "auditor@example"},
			}, nil)

		// second project, with empty list
		aceMock.EXPECT().
			GetRolesWithUsers(context.TODO(), ace.ProjectOpts("project2")).
			Return(ace.RoleUserAssignmentList{}, nil)

		client = externalversions.NewACEClient(aceMock, provider)
		result, err = client.GetProjectUserRoles(nslist)

		Expect(err).To(BeNil(), "should not return an error")
		Expect(result).ToNot(BeEmpty(), "should return a project data list with data")
		Expect(result).To(HaveLen(5), "should only return five projects: two without subproject and three with subproject")

		// project1
		project := getProject(result, "project1", "")
		Expect(project).ToNot(BeNil(), "should contain project1 but empty")
		Expect(project.SubProject).To(BeEmpty(), "should not be a subproject")
		Expect(project.UserRoles).To(HaveLen(0), "project1 should not contain any roles")

		// project1-dev
		project = getProject(result, "project1", "dev")
		Expect(project).ToNot(BeNil(), "should contain project1-dev with roles")
		Expect(project.SubProject).To(Equal("dev"), "should be a subproject")
		Expect(project.UserRoles).To(HaveLen(2), "project1-dev should have two project scoped roles")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("admin", "admin@example", "admin", "")))
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("auditor", "auditor@example", "auditor", "")))

		// project1-ops
		project = getProject(result, "project1", "ops")
		Expect(project).ToNot(BeNil(), "should contain project1-ops with roles")
		Expect(project.SubProject).To(Equal("ops"), "should be a subproject")
		Expect(project.UserRoles).To(HaveLen(2), "project1-ops should have two project scoped roles")
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("admin", "admin@example", "admin", "")))
		Expect(project.UserRoles).To(ContainElement(role.NewUserAssignment("auditor", "auditor@example", "auditor", "")))

		// project2
		project = getProject(result, "project2", "")
		Expect(project).ToNot(BeNil(), "should contain project2 but empty")
		Expect(project.SubProject).To(BeEmpty(), "should not be a subproject")
		Expect(project.UserRoles).To(HaveLen(0), "project2 should not contain any roles")

		// project2-dev
		project = getProject(result, "project2", "dev")
		Expect(project).ToNot(BeNil(), "should contain project2-dev but empty")
		Expect(project.SubProject).To(Equal("dev"), "should not be a subproject")
		Expect(project.UserRoles).To(HaveLen(0), "project2-dev should not contain any roles")

	})

})

func getProject(projects []*role.ProjectUserRoleAssignment, project, subproject string) *role.ProjectUserRoleAssignment {
	for _, proj := range projects {
		if proj.Project.Name == project && proj.Project.SubProject == subproject {
			return proj
		}
	}
	return nil
}

var _ = Describe("ACEClient", func() {

	var (
		aceMock  *mace.MockInterface
		mockCtrl *gomock.Controller
		client   externalversions.ACEClient

		roundTripper *mhttp.MockRoundTripper
		provider     devops.AnnotationProvider
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		aceMock = mace.NewMockInterface(mockCtrl)
		roundTripper = mhttp.NewMockRoundTripper(mockCtrl)
		provider = devops.NewAnnotationProvider(devops.UsedBaseDomain)

	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Describe("GetNamespaces", func() {

		Context("when none spaces", func() {
			It("Should return empty namesapces", func() {
				aceMock.EXPECT().ListSpaces(context.TODO(), "", ace.ProjectOpts("a9")).Return(&ace.SpaceList{}, nil)
				client = externalversions.NewACEClient(aceMock, provider)
				nsInterfaceList, err := client.GetProjectNamespaces(devopsNamespace("a9", "a9-biz"))
				Expect(err).To(BeNil())
				Expect(len(nsInterfaceList)).To(BeEquivalentTo(0))
			})
		})

		Context("when got space list", func() {

			JustBeforeEach(func() {
				aceMock.EXPECT().ListSpaces(context.TODO(), "", ace.ProjectOpts("a9")).Return(spaceList, nil)
				client = externalversions.NewACEClient(aceMock, provider)
			})

			It("Should return namesapces when subproject is exist", func() {
				nsInterfaceList, err := client.GetProjectNamespaces(devopsNamespace("a9", "a9-biz"))
				Expect(err).To(BeNil())
				Expect(len(nsInterfaceList)).To(BeEquivalentTo(1))
				Expect(nsInterfaceList[0].CurrentNamespace()).To(BeEquivalentTo("a9-biz"))
				Expect(nsInterfaceList[0].CurrentCluster().GetClusterIdentity()).To(BeEquivalentTo("cluster-1"))
			})

			It("should return empty namespace when subproject is not exist", func() {
				nsInterfaceList, err := client.GetProjectNamespaces(devopsNamespace("a9", "a9-dev"))
				Expect(err).To(BeNil())
				Expect(len(nsInterfaceList)).To(BeEquivalentTo(0))
			})

			It("Should return namesapces when subproject is exist", func() {
				nsInterfaceList, err := client.GetProjectNamespaces(devopsNamespace("a9", ""))
				Expect(err).To(BeNil())
				Expect(len(nsInterfaceList)).To(BeEquivalentTo(3))
				Expect(nsInterfaceList[0].CurrentNamespace()).To(BeEquivalentTo("a9-biz"))
				Expect(nsInterfaceList[0].CurrentCluster().GetClusterIdentity()).To(BeEquivalentTo("cluster-1"))

				Expect(nsInterfaceList[2].CurrentNamespace()).To(BeEquivalentTo("a9-ops-2"))
				Expect(nsInterfaceList[2].CurrentCluster().GetClusterIdentity()).To(BeEquivalentTo("cluster-1"))
			})
		})
	})

	Describe("ClusterManagement.ListClusters", func() {

		It("Should return MultiClustersInterface", func() {
			request := newHttpRequest(http.MethodGet, "http://address/v2/regions/alauda/", nil)
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, regionListResponseJson), nil)

			client = externalversions.NewACEClient(ace.New(newACEOptions(roundTripper)), provider)
			multiClusters, err := client.GetClusterManagement().ListClusters("")
			Expect(err).To(BeNil())
			Expect(len(multiClusters.Clusters())).To(BeEquivalentTo(2))
			Expect(multiClusters.Namespace("default")).ToNot(BeNil())

			names, errs := multiClusters.Parallel(func(clusterInterface externalversions.ClusterInterface) parallel.Task {
				return func() (i interface{}, e error) {
					return clusterInterface.GetClusterName(), nil
				}
			})

			Expect(errs).To(BeNil())
			Expect(len(names)).To(BeEquivalentTo(2))
			Expect(names).To(ContainElement(interface{}("federal1")))
			Expect(names).To(ContainElement(interface{}("ace")))

			names, errs = multiClusters.Namespace("ss").Parallel(func(namespaceInterface externalversions.ClusterNamespaceInterface) parallel.Task {
				return func() (i interface{}, e error) {
					return namespaceInterface.CurrentCluster().GetClusterIdentity(), nil
				}
			})

			Expect(errs).To(BeNil())
			Expect(len(names)).To(BeEquivalentTo(2))
			Expect(names).To(ContainElement(interface{}("federal1")))
			Expect(names).To(ContainElement(interface{}("ace")))
		})
	})

	Describe("ClusterManagement.GetCluster", func() {
		It("Should return ClusterInterface", func() {
			request := newHttpRequest(http.MethodGet, "http://address/v2/regions/alauda/ace/", nil)
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, region), nil)
			client = externalversions.NewACEClient(ace.New(newACEOptions(roundTripper)), provider)
			cluster, err := client.GetClusterManagement().GetCluster("ace")
			Expect(err).To(BeNil())
			Expect(cluster.GetClusterName()).To(BeEquivalentTo("ace"))
		})
	})

	Describe("ACECluster", func() {
		var (
			cluster externalversions.ClusterInterface
			err     error
		)

		BeforeEach(func() {
			request := newHttpRequest(http.MethodGet, "http://address/v2/regions/alauda/ace/", nil)
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, region), nil)
		})

		Describe("ACECluster.List", func() {
			It("Should return k8s runtime list object", func() {
				request := newHttpRequest(http.MethodGet, "http://address/v2/kubernetes/clusters/ace/nodes/", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, nodeListJson), nil)

				client = externalversions.NewACEClient(ace.New(newACEOptions(roundTripper)), provider)
				cluster, err = client.GetClusterManagement().GetCluster("ace")
				Expect(err).To(BeNil())

				nodes := &corev1.NodeList{}
				err := cluster.List("nodes", v1.ListOptions{}, nodes)
				Expect(err).To(BeNil())
				for _, node := range nodes.Items {
					Expect(node.Name).ToNot(BeEmpty())
				}
			})
		})

		Describe("ACECluster.Get", func() {
			It("Should return k8s runtime object", func() {
				request := newHttpRequest(http.MethodGet, "http://address/v2/kubernetes/clusters/ace/nodes/devops-int-master3", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, master3NodeJson), nil)

				client = externalversions.NewACEClient(ace.New(newACEOptions(roundTripper)), provider)
				cluster, err = client.GetClusterManagement().GetCluster("ace")
				Expect(err).To(BeNil())

				node := &corev1.Node{}
				err = cluster.Get("nodes", "devops-int-master3", v1.GetOptions{}, node)
				Expect(err).To(BeNil())
				Expect(node.Name).To(BeEquivalentTo("devops-int-master3"))
			})

			Context("When resource is not found", func() {
				It("Should return k8s NotFound error", func() {
					request := newHttpRequest(http.MethodGet, "http://address/v2/kubernetes/clusters/ace/nodes/aaa", nil)
					roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusNotFound, request, aceNotFoundErrorJson), nil)

					client = externalversions.NewACEClient(ace.New(newACEOptions(roundTripper)), provider)
					cluster, err = client.GetClusterManagement().GetCluster("ace")
					Expect(err).To(BeNil())

					node := &corev1.Node{}
					err = cluster.Get("nodes", "aaa", v1.GetOptions{}, node)
					Expect(err).ToNot(BeNil())
					Expect(errors.IsNotFound(err)).To(BeTrue())
				})
			})
		})

		Describe("ACECluster.DeleteCollection", func() {
			It("Should delete k8s runtime object collection", func() {
				request := newHttpRequest(http.MethodGet, "http://address/v2/kubernetes/clusters/ace/nodes/?labelSelector=global%3Dtrue", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, nodeListJson), nil)
				requestDelete1 := newHttpRequest(http.MethodDelete, "http://address/v2/kubernetes/clusters/ace/nodes/devops-int-master3", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(requestDelete1)).Return(getResponseWithBody(http.StatusNoContent, requestDelete1, ""), nil)
				requestDelete2 := newHttpRequest(http.MethodDelete, "http://address/v2/kubernetes/clusters/ace/nodes/devops-int-slave1", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(requestDelete2)).Return(getResponseWithBody(http.StatusNoContent, requestDelete2, ""), nil)

				client = externalversions.NewACEClient(ace.New(newACEOptions(roundTripper)), provider)
				cluster, err = client.GetClusterManagement().GetCluster("ace")
				Expect(err).To(BeNil())

				s := &v1.LabelSelector{MatchLabels: map[string]string{"global": "true"}}
				selector, _ := v1.LabelSelectorAsSelector(s)
				err = cluster.DeleteCollection("nodes", nil, v1.ListOptions{
					LabelSelector: selector.String(),
				})
				Expect(err).To(BeNil())
			})
		})
	})

	Describe("ProjectExists", func() {
		BeforeEach(func() {
			request, err := http.NewRequest(http.MethodGet, "http://address/v1/projects/alauda/", nil)
			Expect(err).To(BeNil())
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request).WithQuery()).Return(getResponseWithBody(http.StatusOK, request, projectsListResponseJson), nil).AnyTimes()

			requestSpaceA8, err := http.NewRequest(http.MethodGet, "http://address/v2/spaces/alauda/?project_name=a8", nil)
			Expect(err).To(BeNil())
			requestSpaceA8.Header.Add("Authorization", "Token token")
			requestSpaceA8.Header.Add("Content-type", "application/json")
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(requestSpaceA8).WithQuery()).Return(getResponseWithBody(http.StatusOK, requestSpaceA8, spaceEmptyJson), nil).AnyTimes()

			requestSpace, err := http.NewRequest(http.MethodGet, "http://address/v2/spaces/alauda/?project_name=a9", nil)
			Expect(err).To(BeNil())
			requestSpace.Header.Add("Authorization", "Token token")
			requestSpace.Header.Add("Content-type", "application/json")
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(requestSpace).WithQuery()).Return(getResponseWithBody(http.StatusOK, requestSpace, spaceListResponseJson), nil).AnyTimes()

			client = externalversions.NewACEClient(ace.New(newACEOptions(roundTripper)), provider)
		})

		Context("When namespace is invalid", func() {
			It("Should return error", func() {
				namespace := &corev1.Namespace{
					ObjectMeta: metav1.ObjectMeta{
						Name: "sub1",
					},
				}
				res, err := client.ProjectExists(namespace)
				Expect(err).ToNot(BeNil())
				Expect(res).To(BeTrue())
			})
		})

		Context("When project is not exists", func() {
			It("Should return fasle", func() {
				namespace := &corev1.Namespace{
					ObjectMeta: metav1.ObjectMeta{
						Name: "sub1",
						Annotations: map[string]string{
							"alauda.io/project": "a6",
						},
					},
				}
				res, err := client.ProjectExists(namespace)
				Expect(err).To(BeNil())
				Expect(res).To(BeFalse())
			})
		})

		Context("When project&subproject is not exists", func() {
			It("Should return false", func() {
				namespace := &corev1.Namespace{
					ObjectMeta: metav1.ObjectMeta{
						Name: "sub1",
						Annotations: map[string]string{
							"alauda.io/project":    "a6",
							"alauda.io/subProject": "sub1",
						},
					},
				}
				res, err := client.ProjectExists(namespace)
				Expect(err).To(BeNil())
				Expect(res).To(BeFalse())
			})
		})

		Context("When subproject is not exists", func() {
			It("Should return false", func() {
				namespace := &corev1.Namespace{
					ObjectMeta: metav1.ObjectMeta{
						Name: "sub1",
						Annotations: map[string]string{
							"alauda.io/project":    "a9",
							"alauda.io/subProject": "sub1",
						},
					},
				}
				res, err := client.ProjectExists(namespace)
				Expect(err).To(BeNil())
				Expect(res).To(BeFalse())
			})
		})

		Context("When project/subproject is exists", func() {
			It("Should return true", func() {
				namespace := &corev1.Namespace{
					ObjectMeta: metav1.ObjectMeta{
						Name: "sub1",
						Annotations: map[string]string{
							"alauda.io/project":    "a9",
							"alauda.io/subProject": "a9-biz",
						},
					},
				}
				res, err := client.ProjectExists(namespace)
				Expect(err).To(BeNil())
				Expect(res).To(BeTrue())
			})
		})
	})
})

var spaceEmptyJson = `[
]`

var spaceListResponseJson = `[
    {
        "status": "running",
        "resource_actions": [
            "space:bind_namespace",
            "space:consume",
            "space:create",
            "space:delete",
            "space:unbind_namespace",
            "space:update",
            "space:view"
        ],
        "uuid": "1c96776f-db9d-4b80-a296-d6c482dbdcbe",
        "display_name": "",
        "description": "",
        "project_uuid": "5ac119ae-1d4a-40b2-8f29-99e824c154a8",
        "created_at": "2019-02-22T03:05:15Z",
        "updated_at": "2019-02-22T03:05:15Z",
        "created_by": "jtcheng",
        "devops": {
            "namespace": {
                "name": "devops-a9-a9-biz"
            }
        },
        "namespaces": [
            {
                "cluster_uuid": "5f32006b-50f9-4805-a7b6-7cb675996ec6",
                "created_at": "2019-02-22T03:05:04Z",
                "name": "a9-biz",
                "uuid": "a6e3c9f1-364e-11e9-9b67-5254000cec49"
            }
        ],
        "name": "a9-biz"
    }
]`

var projectsListResponseJson = `[
        {
            "status": "success",
            "template_uuid": "f1cd6c60-25e1-4e6b-88ad-8c1cc7b929d9",
            "uuid": "47ed0463-ac2c-4d58-abdc-dd95465aa295",
            "display_name": "a8",
            "description": "",
            "created_at": "2019-04-04T03:12:36Z",
            "namespace": "alauda",
            "updated_at": "2019-04-04T03:12:36Z",
            "token": "5bf2da12c470aab7eae14343c47513ae5604ddc5",
            "resource_actions": [
                "project:create",
                "project:delete",
                "project:update",
                "project:view"
            ],
            "template": "empty-template",
            "service_type": [
                "kubernetes"
            ],
            "clusters": [
                {
                    "display_name": "ACE",
                    "uuid": "5f32006b-50f9-4805-a7b6-7cb675996ec6",
                    "project_uuid": "47ed0463-ac2c-4d58-abdc-dd95465aa295",
                    "created_at": "2019-04-04T03:12:36Z",
                    "quota": {
                        "storage": -1,
                        "pvc_num": -1,
                        "pods": -1,
                        "cpu": -1,
                        "memory": -1
                    },
                    "updated_at": "2019-04-04T03:12:36Z",
                    "mirror": {},
                    "service_type": "kubernetes",
                    "total_quota": "{'pods': 440, 'storage': 180963, 'cpu': 28, 'memory': 53}",
                    "name": "ace"
                }
            ],
            "admin_list": [
                {
                    "name": "jyhan",
                    "realname": ""
                }
            ],
            "name": "a8"
        },
        {
            "status": "success",
            "template_uuid": "f1cd6c60-25e1-4e6b-88ad-8c1cc7b929d9",
            "uuid": "148f090b-1a3e-4fb5-8239-02b7255f4e9d",
            "display_name": "a9",
            "description": "",
            "created_at": "2019-04-02T10:09:07Z",
            "namespace": "alauda",
            "updated_at": "2019-04-02T10:09:07Z",
            "token": "5bf2da12c470aab7eae14343c47513ae5604ddc5",
            "resource_actions": [
                "project:update",
                "project:view",
                "project:create",
                "project:delete"
            ],
            "template": "empty-template",
            "service_type": [
                "kubernetes"
            ],
            "clusters": [
                {
                    "display_name": "ACE",
                    "uuid": "5f32006b-50f9-4805-a7b6-7cb675996ec6",
                    "project_uuid": "148f090b-1a3e-4fb5-8239-02b7255f4e9d",
                    "created_at": "2019-04-02T10:09:07Z",
                    "quota": {
                        "storage": -1,
                        "pvc_num": -1,
                        "pods": -1,
                        "cpu": -1,
                        "memory": -1
                    },
                    "updated_at": "2019-04-02T10:09:07Z",
                    "mirror": {},
                    "service_type": "kubernetes",
                    "total_quota": "{'pods': 440, 'storage': 180963, 'cpu': 28, 'memory': 53}",
                    "name": "ace"
                }
            ],
            "admin_list": [
                {
                    "name": "liangzhao",
                    "realname": ""
                }
            ],
            "name": "a9"
        }
    ]`

var aceNotFoundErrorJson = `{
    "errors": [
        {
            "source": 1044,
            "message": "nodes \"aaa\" not found",
            "code": "kubernetes_error"
        }
    ]
}`

var nodeListJson = `[
  {
      "resource_actions": [
          "k8s_others:create",
          "k8s_others:delete",
          "k8s_others:update",
          "k8s_others:view"
      ],
      "kubernetes": {
          "status": {
              "capacity": {
                  "ephemeral-storage": "51474044Ki",
                  "hugepages-1Gi": "0",
                  "hugepages-2Mi": "0",
                  "memory": "16300156Ki",
                  "pods": "110",
                  "cpu": "8"
              },
              "addresses": [
                  {
                      "type": "InternalIP",
                      "address": "10.0.128.50"
                  },
                  {
                      "type": "Hostname",
                      "address": "devops-int-master3"
                  }
              ],
              "nodeInfo": {
                  "kernelVersion": "3.10.0-862.11.6.el7.x86_64",
                  "kubeletVersion": "v1.11.2",
                  "containerRuntimeVersion": "docker://1.12.6",
                  "machineID": "735fbcd856b697dbc8d6deb1a11dd712",
                  "kubeProxyVersion": "v1.11.2",
                  "bootID": "25057187-e667-4577-a8a3-cc1fc7c40c66",
                  "osImage": "CentOS Linux 7 (Core)",
                  "architecture": "amd64",
                  "systemUUID": "F954F1EC-D45A-4119-A2AD-B5BC400D52FF",
                  "operatingSystem": "linux"
              },
              "allocatable": {
                  "ephemeral-storage": "47438478872",
                  "hugepages-1Gi": "0",
                  "hugepages-2Mi": "0",
                  "memory": "16197756Ki",
                  "pods": "110",
                  "cpu": "8"
              },
              "daemonEndpoints": {
                  "kubeletEndpoint": {
                      "Port": 10250
                  }
              },
              "images": [
              ],
              "conditions": [
              ]
          },
          "kind": "Node",
          "spec": {
              "podCIDR": "10.199.2.0/24"
          },
          "apiVersion": "v1",
          "metadata": {
              "name": "devops-int-master3",
              "labels": {
                  "redis": "true",
                  "log": "true",
                  "beta.kubernetes.io/os": "linux",
                  "node-role.kubernetes.io/master": "",
                  "ip": "10.0.128.50",
                  "global": "true",
                  "kubernetes.io/hostname": "devops-int-master3",
                  "master": "true",
                  "test": "false",
                  "beta.kubernetes.io/arch": "amd64"
              },
              "resourceVersion": "26327357",
              "creationTimestamp": "2018-11-22T09:19:22Z",
              "annotations": {
                  "node.alpha.kubernetes.io/ttl": "0",
                  "flannel.alpha.coreos.com/public-ip": "10.0.128.50",
                  "flannel.alpha.coreos.com/backend-data": "{\"VtepMAC\":\"0a:d3:24:de:b3:ad\"}",
                  "kubeadm.alpha.kubernetes.io/cri-socket": "/var/run/dockershim.sock",
                  "flannel.alpha.coreos.com/backend-type": "vxlan",
                  "volumes.kubernetes.io/controller-managed-attach-detach": "true",
                  "flannel.alpha.coreos.com/kube-subnet-manager": "true"
              },
              "selfLink": "/api/v1/nodes/devops-int-master3",
              "uid": "b310f50e-ee37-11e8-9985-52540052ef9b"
          }
      }
  },
  {
      "resource_actions": [
          "k8s_others:delete",
          "k8s_others:update",
          "k8s_others:view",
          "k8s_others:create"
      ],
      "kubernetes": {
          "status": {
              "capacity": {
                  "ephemeral-storage": "51474024Ki",
                  "hugepages-1Gi": "0",
                  "hugepages-2Mi": "0",
                  "memory": "8010172Ki",
                  "pods": "110",
                  "cpu": "4"
              },
              "addresses": [
                  {
                      "type": "InternalIP",
                      "address": "10.0.129.99"
                  },
                  {
                      "type": "Hostname",
                      "address": "devops-int-slave1"
                  }
              ],
              "nodeInfo": {
                  "kernelVersion": "3.10.0-693.el7.x86_64",
                  "kubeletVersion": "v1.11.2",
                  "containerRuntimeVersion": "docker://1.12.6",
                  "machineID": "735fbcd856b697dbc8d6deb1a11dd712",
                  "kubeProxyVersion": "v1.11.2",
                  "bootID": "bbdc5f8c-3e80-4b51-a178-a786b7dfee9d",
                  "osImage": "CentOS Linux 7 (Core)",
                  "architecture": "amd64",
                  "systemUUID": "53BF42AB-63E2-4FFE-A4F9-8B8BEE71056B",
                  "operatingSystem": "linux"
              },
              "allocatable": {
                  "ephemeral-storage": "47438460440",
                  "hugepages-1Gi": "0",
                  "hugepages-2Mi": "0",
                  "memory": "7907772Ki",
                  "pods": "110",
                  "cpu": "4"
              },
              "daemonEndpoints": {
                  "kubeletEndpoint": {
                      "Port": 10250
                  }
              },
              "images": [
              ],
              "conditions": [
              ]
          },
          "kind": "Node",
          "spec": {
              "podCIDR": "10.199.3.0/24"
          },
          "apiVersion": "v1",
          "metadata": {
              "name": "devops-int-slave1",
              "labels": {
                  "kubernetes.io/hostname": "devops-int-slave1",
                  "beta.kubernetes.io/os": "linux",
                  "beta.kubernetes.io/arch": "amd64"
              },
              "resourceVersion": "26327358",
              "creationTimestamp": "2018-12-27T08:06:02Z",
              "annotations": {
                  "node.alpha.kubernetes.io/ttl": "0",
                  "flannel.alpha.coreos.com/public-ip": "10.0.129.99",
                  "flannel.alpha.coreos.com/backend-data": "{\"VtepMAC\":\"de:40:6c:ce:54:72\"}",
                  "flannel.alpha.coreos.com/kube-subnet-manager": "true",
                  "flannel.alpha.coreos.com/backend-type": "vxlan",
                  "volumes.kubernetes.io/controller-managed-attach-detach": "true"
              },
              "selfLink": "/api/v1/nodes/devops-int-slave1",
              "uid": "40937d48-09ae-11e9-b160-52540052ef9b"
          }
      }
  }
]`

var master3NodeJson = `{
      "resource_actions": [
          "k8s_others:create",
          "k8s_others:delete",
          "k8s_others:update",
          "k8s_others:view"
      ],
      "kubernetes": {
          "status": {
              "capacity": {
                  "ephemeral-storage": "51474044Ki",
                  "hugepages-1Gi": "0",
                  "hugepages-2Mi": "0",
                  "memory": "16300156Ki",
                  "pods": "110",
                  "cpu": "8"
              },
              "addresses": [
                  {
                      "type": "InternalIP",
                      "address": "10.0.128.50"
                  },
                  {
                      "type": "Hostname",
                      "address": "devops-int-master3"
                  }
              ],
              "nodeInfo": {
                  "kernelVersion": "3.10.0-862.11.6.el7.x86_64",
                  "kubeletVersion": "v1.11.2",
                  "containerRuntimeVersion": "docker://1.12.6",
                  "machineID": "735fbcd856b697dbc8d6deb1a11dd712",
                  "kubeProxyVersion": "v1.11.2",
                  "bootID": "25057187-e667-4577-a8a3-cc1fc7c40c66",
                  "osImage": "CentOS Linux 7 (Core)",
                  "architecture": "amd64",
                  "systemUUID": "F954F1EC-D45A-4119-A2AD-B5BC400D52FF",
                  "operatingSystem": "linux"
              },
              "allocatable": {
                  "ephemeral-storage": "47438478872",
                  "hugepages-1Gi": "0",
                  "hugepages-2Mi": "0",
                  "memory": "16197756Ki",
                  "pods": "110",
                  "cpu": "8"
              },
              "daemonEndpoints": {
                  "kubeletEndpoint": {
                      "Port": 10250
                  }
              },
              "images": [
              ],
              "conditions": [
              ]
          },
          "kind": "Node",
          "spec": {
              "podCIDR": "10.199.2.0/24"
          },
          "apiVersion": "v1",
          "metadata": {
              "name": "devops-int-master3",
              "labels": {
                  "redis": "true",
                  "log": "true",
                  "beta.kubernetes.io/os": "linux",
                  "node-role.kubernetes.io/master": "",
                  "ip": "10.0.128.50",
                  "global": "true",
                  "kubernetes.io/hostname": "devops-int-master3",
                  "master": "true",
                  "test": "false",
                  "beta.kubernetes.io/arch": "amd64"
              },
              "resourceVersion": "26327357",
              "creationTimestamp": "2018-11-22T09:19:22Z",
              "annotations": {
                  "node.alpha.kubernetes.io/ttl": "0",
                  "flannel.alpha.coreos.com/public-ip": "10.0.128.50",
                  "flannel.alpha.coreos.com/backend-data": "{\"VtepMAC\":\"0a:d3:24:de:b3:ad\"}",
                  "kubeadm.alpha.kubernetes.io/cri-socket": "/var/run/dockershim.sock",
                  "flannel.alpha.coreos.com/backend-type": "vxlan",
                  "volumes.kubernetes.io/controller-managed-attach-detach": "true",
                  "flannel.alpha.coreos.com/kube-subnet-manager": "true"
              },
              "selfLink": "/api/v1/nodes/devops-int-master3",
              "uid": "b310f50e-ee37-11e8-9985-52540052ef9b"
          }
      }
  }`

var spaceList = &ace.SpaceList{
	Items: []*ace.Space{
		&ace.Space{
			Name: "a9-biz",
			Namespaces: []ace.SpaceRelatedNamespace{
				ace.SpaceRelatedNamespace{
					ClusterUUID: "cluster-1",
					Name:        "a9-biz",
					UUID:        "ns-1",
				},
			},
		},
		&ace.Space{
			Name: "a9-ops",
			Namespaces: []ace.SpaceRelatedNamespace{
				ace.SpaceRelatedNamespace{
					ClusterUUID: "cluster-1",
					Name:        "a9-ops",
					UUID:        "ns-2",
				},
				ace.SpaceRelatedNamespace{
					ClusterUUID: "cluster-1",
					Name:        "a9-ops-2",
					UUID:        "ns-3",
				},
			},
		},
	},
}

var region = `{
    "resource_actions": [
        "cluster:clear",
        "cluster:create",
        "cluster:delete",
        "cluster:deploy",
        "cluster:update",
        "cluster:view"
    ],
    "display_name": "ACE",
    "name": "ace",
    "container_manager": "KUBERNETES",
    "created_at": "2018-11-05T10:31:26.117115Z",
    "namespace": "alauda",
    "updated_at": "2019-02-15T08:45:00.879283Z",
    "platform_version": "v4",
    "state": "RUNNING",
    "features": {
        "metric": {
            "type": "prometheus",
            "integration_uuid": "281301bf-c643-436d-8269-114513b7eca4"
        },
        "log": {
            "application_name": "official-log",
            "storage": {
                "write_log_source": "default",
                "read_log_source": "default"
            },
            "type": "official",
            "application_namespace": "default"
        }
    },
    "mirror": {},
    "id": "ace",
    "attr": {
        "cluster": {
            "nic": "eth0"
        },
        "docker": {
            "path": "/var/lib/docker",
            "version": "1.12.6"
        },
        "feature_namespace": "default",
        "kubernetes": {
            "endpoint": "https://10.0.96.34:6443",
            "version": "1.11.2",
            "type": "original",
            "token": "eyJhbeAAg",
            "cni": {
                "network_policy": "",
                "cidr": "10.199.0.0/16",
                "type": "flannel",
                "backend": "vxlan"
            }
        },
        "cloud": {
            "name": "PRIVATE"
        }
    }
}`

var regionListResponseJson = `[
  {
      "resource_actions": [
          "cluster:clear",
          "cluster:create",
          "cluster:delete",
          "cluster:deploy",
          "cluster:update",
          "cluster:view"
      ],
      "display_name": "ACE",
      "name": "ace",
      "namespace": "alauda",
      "created_at": "2018-11-05T10:31:26.117Z",
      "container_manager": "KUBERNETES",
      "updated_at": "2019-02-15T08:45:00.879Z",
      "platform_version": "v4",
      "state": "RUNNING",
      "mirror": {},
      "attr": {
          "cluster": {
              "nic": "eth0"
          },
          "docker": {
              "path": "/var/lib/docker",
              "version": "1.12.6"
          },
          "feature_namespace": "default",
          "kubernetes": {
              "endpoint": "https://10.0.96.34:6443",
              "version": "1.11.2",
              "type": "original",
              "token": "eyJhbkVyQPW91cO6DeAAg",
              "cni": {
                  "network_policy": "",
                  "cidr": "10.199.0.0/16",
                  "type": "flannel",
                  "backend": "vxlan"
              }
          },
          "cloud": {
              "name": "PRIVATE"
          }
      },
      "id": "ace",
      "features": {
          "metric": {
              "type": "prometheus",
              "integration_uuid": "281301bf-c643-436d-8269-114513b7eca4"
          },
          "log": {
              "application_name": "official-log",
              "storage": {
                  "write_log_source": "default",
                  "read_log_source": "default"
              },
              "type": "official",
              "application_namespace": "default"
          }
      }
  },
  {
      "resource_actions": [
          "cluster:clear",
          "cluster:create",
          "cluster:delete",
          "cluster:deploy",
          "cluster:update",
          "cluster:view"
      ],
      "display_name": "联邦一",
      "name": "federal1",
      "namespace": "alauda",
      "created_at": "2018-11-09T08:42:44.254Z",
      "container_manager": "KUBERNETES",
      "updated_at": "2019-01-21T11:03:09.760Z",
      "platform_version": "v4",
      "state": "RUNNING",
      "mirror": {
          "regions": [
              {
                  "created_at": "2018-12-13T10:57:35.754Z",
                  "display_name": "federation22",
                  "id": "01390bd7-4b46-4ee3-b81e-6e1c068ef27d",
                  "name": "federation2"
              },
              {
                  "created_at": "2018-11-09T08:42:44.254Z",
                  "display_name": "联邦一",
                  "id": "2d4046f8-e636-4c68-bd1b-a4a834c27ca5",
                  "name": "federal1"
              }
          ],
          "flag": "#37d9f0",
          "display_name": "联邦集群",
          "id": "9821f263-e7d9-4eef-bef6-74eabd3ed039",
          "name": "federation"
      },
      "attr": {
          "cluster": {
              "nic": "eth0"
          },
          "docker": {
              "path": "/var/lib/docker",
              "version": "1.12.6"
          },
          "feature_namespace": "default",
          "kubernetes": {
              "endpoint": "https://10.0.96.17:6443",
              "version": "1.11.2",
              "type": "original",
              "token": "eyJhbGciOiJSUzI1G_A0-nPw",
              "cni": {
                  "network_policy": "",
                  "cidr": "10.22.0.0/16",
                  "type": "flannel",
                  "backend": "vxlan"
              }
          },
          "cloud": {
              "name": "PRIVATE"
          }
      },
      "id": "federal1",
      "features": {}
  }
]`

func devopsNamespace(project string, subproject string) *corev1.Namespace {
	ns := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: "",
			Name:      fmt.Sprintf("devops-%s-%s", project, subproject),
			Labels: map[string]string{
				"alauda.io/project": project,
			},
		},
	}
	if subproject != "" {
		ns.Labels["alauda.io/subProject"] = subproject
	}
	return ns
}

func newHttpRequest(method string, url string, body io.Reader) *http.Request {
	request, err := http.NewRequest(method, url, body)
	Expect(err).To(BeNil())
	request.Header.Add("Authorization", "Token token")
	request.Header.Add("Content-type", "application/json")
	return request
}

func getResponseWithBody(status int, request *http.Request, body string) *http.Response {
	return &http.Response{
		StatusCode: status,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 0,
		Header:     http.Header{},
		Request:    request,
		Body:       ioutil.NopCloser(bytes.NewBufferString(body)),
	}
}

func newACEOptions(roundTripper http.RoundTripper) ace.Options {
	return ace.Options{Transport: roundTripper, RootAccount: "alauda", APIAddress: "http://address"}
}
