package externalversions

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	glog "k8s.io/klog"
)

const (
	ImageDigestSignature       = "sha256:"
	ImageRepositoryAvailable   = "available"
	ImageRepositoryNotFound    = "notFound"
	ImageRepositoryUnAvailable = "unavailable"
	AlaudaPublicRegistry       = "alauda_public_registry"
	ImageDecimal               = 1024
	ImageSizeB                 = 1
	ImageSizeKB                = 2
	ImageSizeMB                = 3
	ImageSizeGB                = 4
	DockerHubPageSize          = 100
)

type RegistryRepositoriesResponse struct {
	Repositories []string `json:"repositories"`
}

type RegistryRepositoryTagResponse struct {
	Tags []string `json:"tags"`
}

type TagDetails struct {
	History []History `json:"history"`
}

type History struct {
	V1Compatibility string `json:"v1Compatibility"`
}

type V1Compatibility struct {
	Created string `json:"created"`
}

type RegistryResponse struct {
	StatusCode int
	Header     RegistryHeader
	HTTPHeader http.Header
	Body       []byte
	*http.Response
}

type RegistryHeader struct {
	Digest       string
	Authenticate string
}

type AuthOpts struct {
	Realm   string
	Scope   string
	Service string
}

type ImageScanTagList struct {
	Tags []ImageScanTag
}

type ImageScanTag struct {
	Digest       string        `json:"digest"`
	Author       string        `json:"author"`
	Name         string        `json:"name"`
	Size         int           `json:"size"`
	Created      string        `json:"created"`
	ScanOverview *ScanOverview `json:"scan_overview"`
}

type ScanOverview struct {
	ImageDigest string                    `json:"image_digest"`
	ScanStatus  devops.ImageTagScanStatus `json:"scan_status"`
	Components  Components                `json:"components"`
	JobID       int                       `json:"job_id"`
	Severity    int                       `json:"severity"`
	Updated     string                    `json:"update_time"`
}

type Components struct {
	Total   int       `json:"total"`
	Summary []Summary `json:"summary"`
}

type Summary struct {
	Severity int `json:"severity"`
	Count    int `json:"count"`
}

type AlaudaRegistry struct {
	Endpoint string `json:"endpoint"`
	Name     string `json:"name"`
}

type AlaudaRepository struct {
	Name string `json:"name"`
}

type AlaudaTagDetail struct {
	Created string          `json:"created"`
	Config  AlaudaTagConfig `json:"config"`
}

type AlaudaTagConfig struct {
	Image string `json:"Image"`
}

type DockerHubToken struct {
	Token string `json:"token"`
}

type DockerHubOrganizationResponse struct {
	Count   int                     `json:"count"`
	Results []DockerHubOrganization `json:"results"`
}

type DockerHubOrganization struct {
	Orgname string `json:"orgname"`
}

type DockerHubRepositoryResponse struct {
	Count   int                         `json:"count"`
	Results []DockerHubRepositoryResult `json:"results"`
}

type DockerHubRepositoryResult struct {
	Name        string `json:"name"`
	Namespace   string `json:"namespace"`
	Description string `json:"description"`
	StarCount   int    `json:"star_count"`
	PullCount   int    `json:"pull_count"`
	LastUpdated string `json:"last_updated"`
}

type DockerHubTagResponse struct {
	Results []DockerHubTag `json:"results"`
}

type DockerHubTag struct {
	Name        string `json:"name"`
	FullSize    int64  `json:"full_size"`
	LastUpdated string `json:"last_updated"`
	ImageID     string `json:"image_id"`
}

// region ImageRegistryClient
type ImageRegistryClient struct {
	Endpoint   string
	Username   string
	Credential string
	// TODO: replace with http.RoundTripper and add unit test
	Client *http.Client
	Cxt    context.Context
}

// TODO(jiajie): add comments to all public functions

func NewImageRegistryClient(endpoint, username, credential string) ImageRegistryClient {
	// TODO: Add support to an injected http.RoundTripper
	httpClient := &http.Client{}
	ctx := context.Background()
	httpClient.Timeout = time.Second * 30
	client := ImageRegistryClient{
		endpoint,
		username,
		credential,
		httpClient,
		ctx,
	}
	return client
}

func (c *ImageRegistryClient) GetImageToken(opts AuthOpts) (result string, err error) {
	logName := "GetImageToken"
	glog.V(5).Infof("Get registry token with %#v", opts)
	req, err1 := http.NewRequest(http.MethodGet, opts.Realm, nil)
	if err1 != nil {
		err = err1
		glog.Errorf("%s error is %s", logName, err.Error())
		return
	}
	query := req.URL.Query()
	query.Add("scope", opts.Scope)
	query.Add("service", opts.Service)
	req.URL.RawQuery = query.Encode()
	req.SetBasicAuth(c.Username, c.Credential)
	resp, err := c.DoRequest(req)
	if err != nil {
		glog.Errorf("Get registry token from %s failed, error is: %s", opts.Realm, err)
		return "", err
	}
	err = CheckResponse(resp.Response)
	if err != nil {
		glog.Errorf("Get registry token from %s failed, error is: %s", opts.Realm, err)
		return "", err
	}

	resMap := make(map[string]interface{})
	glog.V(9).Infof("%s resp body is %#v", logName, string(resp.Body))
	err = json.Unmarshal([]byte(resp.Body), &resMap)
	if err != nil {
		glog.Errorf("%s error is %s", logName, err.Error())
		return
	}
	return (resMap["token"]).(string), nil
}

func (c *ImageRegistryClient) Url(suffix string) string {
	path := fmt.Sprintf("%s/%s", strings.TrimRight(c.Endpoint, "/"),
		strings.TrimLeft(suffix, "/"))
	return path
}

func (c *ImageRegistryClient) RequestRegistry(suffix string) (resp *RegistryResponse, err error) {
	logName := "RequestRegistry"
	var (
		req   *http.Request
		token string
	)

	path := c.Url(suffix)
	glog.V(9).Infof("%s path is %#v", logName, path)
	req, err1 := http.NewRequest(http.MethodGet, path, nil)
	if err1 != nil {
		err = err1
		glog.Errorf("%s %s error is %s", logName, path, err.Error())
		return
	}
	if c.Username != "" && c.Credential != "" {
		req.SetBasicAuth(c.Username, c.Credential)
	}
	resp, err = c.DoRequest(req)

	glog.V(9).Infof("%s %s resp body is %#v", logName, path, string(resp.Body))

	if err != nil {
		glog.Errorf("%s %s error is %s", logName, path, err.Error())
		return nil, err
	}

	if resp.StatusCode == http.StatusUnauthorized {
		glog.V(6).Infof("%s %s Get registry response return 401 error, response is: %v", logName, path, resp)
		auth := resp.Header.Authenticate
		opts, err := parseAuthenticateString(auth)
		if err != nil {
			glog.Errorf("Error to parse authenticate in header: %s, response header:%#v", err.Error(), resp.Response.Header)
			return nil, fmt.Errorf("Error to parse authenticate in header: %s", err.Error())
		}
		token, err = c.GetImageToken(opts)
		glog.V(9).Infof("%s %s  token is %#v", logName, path, token)
		if err != nil {
			glog.Errorf("%s %s error is %s", logName, path, err.Error())
			return nil, err
		}
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		glog.V(9).Infof("%s %s  req is %#v", logName, path, req)
		resp, err = c.DoRequest(req)

		glog.V(9).Infof("%s %s  resp body is %#v", logName, path, string(resp.Body))
		if err != nil {
			glog.Errorf("%s %s error is %s", logName, path, err.Error())
			return nil, err
		}
		err = CheckResponse(resp.Response)
		if err != nil {
			glog.Errorf("%s %s error is %s", logName, path, err.Error())
			return nil, err
		}
		return resp, nil
	}

	err = CheckResponse(resp.Response)
	if err != nil {
		glog.Errorf("%s %s error is %s", logName, path, err.Error())
		return nil, err
	}

	return resp, nil
}

// CheckResponse checks the API response for errors, and returns them if present.
func CheckResponse(r *http.Response) error {
	switch r.StatusCode {
	case 200, 201, 202, 204, 304:
		return nil
	}

	errorResponse := &ErrorResponse{Response: r}
	data, err := ioutil.ReadAll(r.Body)
	if err == nil && data != nil {
		errorResponse.Message = string(data)
	}

	return errorResponse
}

type ErrorResponse struct {
	Response *http.Response `json:"-"`
	Message  string
}

func (e *ErrorResponse) Error() string {
	path, _ := url.QueryUnescape(e.Response.Request.URL.Opaque)
	u := fmt.Sprintf("%s://%s%s", e.Response.Request.URL.Scheme, e.Response.Request.URL.Host, path)
	return fmt.Sprintf("%s %s: %d %s", e.Response.Request.Method, u, e.Response.StatusCode, e.Message)
}

func (c *ImageRegistryClient) RequestRegistryWithToken(suffix, token string) (resp *RegistryResponse, err error) {
	var req *http.Request
	path := c.Url(suffix)
	req, err1 := http.NewRequest(http.MethodGet, path, nil)
	if err1 != nil {
		err = err1
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	resp, err = c.DoRequest(req)
	if err != nil {
		glog.Errorf("Get %s failed", suffix)
		return resp, err
	}
	return resp, nil
}

func (c *ImageRegistryClient) DoRequest(req *http.Request) (registryResponse *RegistryResponse, err error) {
	glog.V(6).Infof("Request data is: %v", req)
	registryResponse = new(RegistryResponse)
	resp, err := c.Client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return registryResponse, err
	}

	registryResponse.StatusCode = resp.StatusCode
	registryResponse.Header.Digest = resp.Header.Get("docker-content-digest")
	registryResponse.Header.Authenticate = resp.Header.Get("www-authenticate")
	registryResponse.HTTPHeader = resp.Header
	body, _ := ioutil.ReadAll(resp.Body)
	registryResponse.Body = body
	registryResponse.Response = resp
	return registryResponse, nil
}

func (c *ImageRegistryClient) ConvertSizeToString(intSize int64) (stringSize string) {
	result, times := float32(intSize), 1
	for result > ImageDecimal {
		result = result / ImageDecimal
		times += 1
	}
	switch times {
	case ImageSizeB:
		return fmt.Sprintf("%dB", intSize)
	case ImageSizeKB:
		return fmt.Sprintf("%.2fKB", result)
	case ImageSizeMB:
		return fmt.Sprintf("%.2fMB", result)
	case ImageSizeGB:
		return fmt.Sprintf("%.2fGB", result)
	default:
		return fmt.Sprintf("%d out of size", intSize)
	}
}

func parseAuthenticateString(auth string) (opts AuthOpts, err error) {
	authSplit := strings.SplitN(auth, " ", 2)
	if len(authSplit) != 2 {
		return AuthOpts{}, fmt.Errorf("format of authenticate '%s' is not correct", auth)
	}
	parts := strings.Split(authSplit[1], ",")
	for _, part := range parts {
		valueList := strings.SplitN(part, "=", 2)
		if len(valueList) == 2 {
			value := strings.Trim(valueList[1], "\",")
			switch valueList[0] {
			case "realm":
				opts.Realm = value
			case "scope":
				opts.Scope = value
			case "service":
				opts.Service = value
			}
		}
	}

	if opts.Realm == "" {
		return AuthOpts{}, fmt.Errorf("realm in authenticate '%s' should not be empty", auth)
	}
	return
}
