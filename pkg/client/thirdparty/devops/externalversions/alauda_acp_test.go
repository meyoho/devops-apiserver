package externalversions_test

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions"
	"alauda.io/devops-apiserver/pkg/role"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sfake "k8s.io/client-go/kubernetes/fake" // k8stesting "k8s.io/client-go/testing"
	"k8s.io/client-go/rest"
)

var _ = Describe("ACPClient.GetProjectUserRoles", func() {
	var (
		client          externalversions.ACPClient
		k8sclient       *k8sfake.Clientset
		nslist          *corev1.NamespaceList
		result          []*role.ProjectUserRoleAssignment
		err             error
		systemNamespace = "alauda-system"
	)

	BeforeEach(func() {
		nslist = &corev1.NamespaceList{
			Items: []corev1.Namespace{
				newNamespace("ns", "project"),
				newNamespace("ns-1", "project"),
				newNamespace("ns-3", "project2"),
			},
		}

		k8sclient = k8sfake.NewSimpleClientset(
			newRoleBinding("ns", "role1", "user1", "user2"),
			newRoleBinding("ns-3", "role2", "user2", "user3"),
		)
	})

	JustBeforeEach(func() {

		provider := devops.NewAnnotationProvider(devops.UsedBaseDomain)

		option := externalversions.ACPOptions{
			Client:             k8sclient,
			RestConfig:         &rest.Config{},
			MultiClusterHost:   "",
			AcpSystemNamespace: systemNamespace,
			Provider:           provider}
		client, _ = externalversions.NewACPClient(option)
		result, err = client.GetProjectUserRoles(nslist)
	})

	It("should return a list of projects with user assignments", func() {
		Expect(err).To(BeNil(), "should not return an error")
		Expect(result).ToNot(BeEmpty(), "should return a project data list with data")
		Expect(result).To(HaveLen(2), "should only return two projects")
		Expect(result).To(ContainElement(
			role.NewProject("ns").
				SetName("project").
				AddUserRole(
					role.NewUserAssignment("user1", "user1", "role1", ""),
					role.NewUserAssignment("user2", "user2", "role1", ""),
				),
		))

		Expect(result).To(ContainElement(
			role.NewProject("ns-3").
				SetName("project2").
				AddUserRole(
					role.NewUserAssignment("user2", "user2", "role2", ""),
					role.NewUserAssignment("user3", "user3", "role2", ""),
				),
		))

	})

})

func newNamespace(name, projectName string, subproject ...string) corev1.Namespace {
	ns := corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
			Annotations: map[string]string{
				"alauda.io/project": projectName,
			},
		},
	}
	if len(subproject) > 0 {
		ns.ObjectMeta.Annotations["alauda.io/subProject"] = subproject[0]
	}
	return ns
}

func newRoleBinding(namespace, name string, users ...string) *rbacv1.RoleBinding {
	rb := &rbacv1.RoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		RoleRef: rbacv1.RoleRef{
			Name: name,
		},
		Subjects: []rbacv1.Subject{},
	}
	for _, u := range users {
		rb.Subjects = append(rb.Subjects, rbacv1.Subject{
			Kind: rbacv1.UserKind,
			Name: u,
		})
	}
	return rb
}
