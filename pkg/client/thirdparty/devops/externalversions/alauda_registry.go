package externalversions

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

// region alauda
type AlaudaClient struct {
	ImageRegistryClient
}

func NewAlaudaClient(endpoint, username, credential string) *AlaudaClient {
	serviceClient := NewImageRegistryClient(endpoint, username, credential)
	return &AlaudaClient{serviceClient}
}

func (c *AlaudaClient) SetTransport(trans http.RoundTripper) {
	c.ImageRegistryClient.Client.Transport = trans
}

func (c *AlaudaClient) CheckRegistryAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	path := fmt.Sprintf("%s/_ping", c.Endpoint)
	return CheckService(path, nil)
}

func (c *AlaudaClient) CheckRepositoryAvailable(name string, lastModigyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	glog.V(6).Infof("Check alauda image repository: %s is available or not", name)
	var (
		start       = time.Now()
		duration    time.Duration
		lastAttempt = metav1.NewTime(start)
	)
	urlSuffix := fmt.Sprintf("v1/registries/%s/alauda_public_registry/repositories/%s", c.GetAlaudaNamespace(), c.GetRepositoryName(name))
	status = &devops.HostPortStatus{
		LastAttempt: &lastAttempt,
	}
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		status.StatusCode = 500
		status.Response = err.Error()
	} else {
		switch resp.StatusCode {
		case http.StatusOK:
			status.Response = ImageRepositoryAvailable
		case http.StatusNotFound:
			status.Response = ImageRepositoryNotFound
		default:
			status.Response = ImageRepositoryUnAvailable
		}
		status.StatusCode = resp.StatusCode
	}
	duration = time.Since(start)
	status.Delay = &duration
	return
}

func (c *AlaudaClient) GetImageRepos() (result *devops.ImageRegistryBindingRepositories, err error) {
	glog.V(6).Infof("Get %s public registry repo.", c.Endpoint)
	result = &devops.ImageRegistryBindingRepositories{}
	urlSuffix := fmt.Sprintf("v1/registries/%s/alauda_public_registry/repositories", c.GetAlaudaNamespace())
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		glog.Errorf("List alauda public registry repo failed: %s", err)
		return
	}
	repositoryList := make([]AlaudaRepository, 0)
	err = json.Unmarshal([]byte(resp.Body), &repositoryList)
	for _, repo := range repositoryList {
		name := fmt.Sprintf("%s/%s", c.GetAlaudaNamespace(), repo.Name)
		result.Items = append(result.Items, name)
	}
	return result, nil
}

func (c *AlaudaClient) GetImageTags(name string) (result []devops.ImageTag, err error) {
	glog.V(6).Infof("Get %s repository: %s tags", c.Endpoint, name)
	repository := c.GetRepositoryName(name)
	urlSuffix := fmt.Sprintf("v1/registries/%s/alauda_public_registry/repositories/%s/tags", c.GetAlaudaNamespace(), repository)
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		glog.Errorf("Can't get repository: %s tag list", repository)
		return result, err
	}
	tags := make([]string, 0)
	json.Unmarshal([]byte(resp.Body), &tags)
	var mutex sync.Mutex
	wait := sync.WaitGroup{}
	wait.Add(len(tags))
	for _, tag := range tags {
		go func(tagStr string) {
			defer wait.Done()
			tagDetail, err := c.GetImageTagDetail(repository, tagStr)
			if err != nil {
				glog.Errorf("Get repository: (%s), tag: (%s) detail failed, err: %v", repository, tagStr, err)
			} else {
				mutex.Lock()
				result = append(result, tagDetail)
				mutex.Unlock()
			}
		}(tag)
	}
	wait.Wait()
	return result, nil
}

func (c *AlaudaClient) GetAlaudaNamespace() (namespace string) {
	namespaceSplit := strings.Split(c.Username, "/")
	if len(namespaceSplit) == 2 {
		namespace = namespaceSplit[0]
	} else {
		namespace = c.Username
	}
	return
}

func (c *AlaudaClient) GetRepositoryName(name string) (repository string) {
	repository = name
	nameSplit := strings.Split(name, "/")
	if len(nameSplit) == 2 {
		repository = nameSplit[1]
	}
	return
}

func (c *AlaudaClient) GetImageTagDetail(repository, reference string) (result devops.ImageTag, err error) {
	glog.V(6).Infof("Get repository(%s) tag(%s) detail.", repository, reference)
	urlSuffix := fmt.Sprintf("v1/registries/%s/alauda_public_registry/repositories/%s/tags/%s", c.GetAlaudaNamespace(), repository, reference)
	result = devops.ImageTag{}
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil || resp.StatusCode != 200 {
		glog.Errorf("Get tag (%s-%s) detail failed.", repository, reference)
		result.Name = reference
		result.CreatedAt = &metav1.Time{}
		return result, err
	}
	tagDetail := new(AlaudaTagDetail)
	json.Unmarshal([]byte(resp.Body), tagDetail)
	t, err := time.Parse(time.RFC3339Nano, tagDetail.Created)
	if err != nil {
		glog.Errorf("Tag %s:%s convert time failed: %s", repository, reference, err)
	}
	result.CreatedAt = &metav1.Time{Time: t}
	result.Name = reference
	result.Digest = strings.TrimPrefix(tagDetail.Config.Image, ImageDigestSignature)
	return result, nil
}

func (c *AlaudaClient) CheckAuthentication() (status *devops.HostPortStatus, err error) {
	urlSuffix := fmt.Sprintf("v1/registries/%s", c.GetAlaudaNamespace())
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil || resp == nil {
		glog.Errorf("Check alauda registry auth failed: %s", err)
		return
	}
	status = &devops.HostPortStatus{
		StatusCode: resp.StatusCode,
		Response:   string(resp.Body),
	}
	return
}

func (c *AlaudaClient) GetRegistryEndpoint() (endpoint string, err error) {
	// Get alauda public registry endpoint
	glog.V(6).Infof("Get %s public registry endpoint.", c.Endpoint)
	urlSuffix := fmt.Sprintf("v1/registries/%s", c.GetAlaudaNamespace())
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		glog.Errorf("Get alauda registry endpoint failed: %s", err)
		return
	}
	if resp.StatusCode != 200 {
		glog.Errorf("Unexpected response code: %d, response is:%s", resp.StatusCode, string(resp.Body))
		err = fmt.Errorf("Unexpected response code: %d", resp.StatusCode)
		return
	}

	registryList := make([]AlaudaRegistry, 0)
	err = json.Unmarshal([]byte(resp.Body), &registryList)
	if err != nil {
		glog.Errorf("Parse alauda registry failed: %s", err)
	}
	for _, registry := range registryList {
		if registry.Name == AlaudaPublicRegistry {
			endpoint = registry.Endpoint
		}
	}
	return
}

func (c *AlaudaClient) TriggerScanImage(repositoryName, tag string) (result *devops.ImageResult) {
	return
}

func (c *AlaudaClient) GetVulnerability(repositoryName, tag string) (result *devops.VulnerabilityList, err error) {
	return
}

func (c *AlaudaClient) GetImageRepoLink(name string) (link string) {
	return
}

func (c *AlaudaClient) GetImageProjects() (result *devops.ProjectDataList, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("projectdatalist"), "get")
}

func (c *AlaudaClient) CreateImageProject(name string) (result *devops.ProjectData, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("porjectdata"), "create")
}

// end region
