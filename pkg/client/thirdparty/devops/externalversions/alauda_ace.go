package externalversions

import (
	"context"
	goerrors "errors"
	"fmt"
	"net/url"
	"sort"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	"alauda.io/devops-apiserver/pkg/role"
	"alauda.io/devops-apiserver/pkg/util/parallel"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	k8sschema "k8s.io/apimachinery/pkg/runtime/schema"
	glog "k8s.io/klog"
)

// ACEClient Alauda Cloud abstraction for AlaudaClient interface
type ACEClient struct {
	ace.Interface
	Filter ProjectsFilter

	// local memory cache, it is expensive, do not put data with abandon
	cache    map[string]interface{}
	provider v1alpha1.AnnotationProvider
}

const (
	CacheProjectListKey = "projects/"
)

// NewACEClient constructor for ACE Client
func NewACEClient(aceClient ace.Interface, provider v1alpha1.AnnotationProvider) ACEClient {
	return ACEClient{
		Interface: aceClient,
		Filter:    StandardProjectsFilter{},
		cache:     map[string]interface{}{},
		provider:  provider,
	}
}

const (
	pageOne  = 1
	pageSize = 50
)

// GetProjectUserRoles fetches user roles for list of projects here abstracted as
// namespaces
func (a ACEClient) GetProjectUserRoles(list *corev1.NamespaceList) (result []*role.ProjectUserRoleAssignment, err error) {
	// original project data collected from namespace data
	projects := a.Filter.FilterProjects(list, a.provider)
	// to make sure all projects comes before subprojects
	// first sort
	sort.Slice(projects, func(i, j int) bool {
		return projects[i].Name+projects[i].SubProject < projects[j].Name+projects[j].SubProject
	})
	// organize data as project and its children to keep it as close to the real project structure as possible
	// returns a map[string]*ACEProject where the key is the project name, and the value is the parent project object
	processedProjects := processProjectData(projects)

	// if no processedProjects then just return an empty slice
	result = make([]*role.ProjectUserRoleAssignment, 0, len(processedProjects))
	if len(processedProjects) == 0 {
		return
	}
	// otherwise should fetch all users for all projects
	var roleUserList ace.RoleUserAssignmentList
	for name, project := range processedProjects {
		roleUserList, err = a.GetRolesWithUsers(context.TODO(), ace.ProjectOpts(name), ace.PageOpts(pageOne, pageSize))
		// an error occured and we should abort this operation
		if err != nil {
			result = nil
			return
		}
		project.UserRoles = roleUserList
		result = append(result, project.process()...)
	}
	return
}

func isProjectLevelUser(user *ace.RoleUserAssignment) bool {
	return user.GetSpace() == "" && user.NamespaceUUID == ""
}

func getUserAssingments(list ace.RoleUserAssignmentList) (ua []*role.UserAssignment) {
	ua = make([]*role.UserAssignment, 0, len(list))
	if len(list) == 0 {
		return
	}
	for _, u := range list {
		ua = append(ua, role.NewUserAssignment(u.GetUsername(), u.GetEmail(), u.GetRole(), u.GetSpace()))
	}
	return
}

func (a ACEClient) GetClusterManagement() ClusterManagementInterface {
	return &aceClusterManagement{
		aceClient: a,
	}
}

func (a ACEClient) GetProjectNamespaces(namespace *corev1.Namespace) ([]ClusterNamespaceInterface, error) {
	ctx := context.TODO()

	spaceList, err := a.ListSpaces(ctx, "", getRequestOptionsFromTenantInfo(namespace.Labels, a.provider)...)
	if err != nil {
		return []ClusterNamespaceInterface{}, err
	}

	result := []ClusterNamespaceInterface{}
	subProject := getSubProjectFromTenantInfo(namespace.Labels, a.provider)

	// filter all bi namespace in current space
	for _, space := range spaceList.Items {
		if subProject != "" && space.Name != subProject {
			continue
		}

		for _, namespace := range space.Namespaces {
			aceNamespace := &aceClusterNamespace{
				name:        namespace.Name,
				clusterID:   namespace.ClusterUUID,
				clusterName: namespace.ClusterName,
				aceClient:   a,
			}
			result = append(result, aceNamespace)
		}
	}

	return result, nil
}

// ProjectExists  will judge whether the project/subproject exists that specified by namespace
func (a ACEClient) ProjectExists(namespace *corev1.Namespace) (bool, error) {
	if namespace == nil {
		err := goerrors.New("namespace should not be nil")
		glog.Errorf("%s when invoke ProjectExists", err.Error())
		return true, errors.NewInternalError(err)
	}

	aceDevOpsNamespace := ACEDevOpsNamespace((*namespace))

	if !aceDevOpsNamespace.IsValid(a.provider) {
		glog.V(5).Infof("namespace '%s' is not a valid ace devops namespace, namespace=%#v", namespace.Name, *namespace)
		return true, errors.NewBadRequest(fmt.Sprintf("namespace '%s' is not a valid ace devops namespace", namespace.Name))
	}

	if _, ok := a.cache[CacheProjectListKey]; !ok {
		err := a.retrieveProjectsAndSubprojects()
		if err != nil {
			return true, err
		}
	}

	cache := a.cache[CacheProjectListKey]
	data, ok := cache.(map[string]map[string]struct{})
	if !ok {
		return true, errors.NewInternalError(fmt.Errorf("cache type %T is not expected", cache))
	}

	projectCacheValue, ok := data[aceDevOpsNamespace.Project(a.provider)]
	if !ok {
		glog.V(8).Infof("ACE Project %s is not exists, current projects cache is :%#v", aceDevOpsNamespace.Project(a.provider), data)
		return false, nil
	}

	if aceDevOpsNamespace.SubProject(a.provider) == "" {
		return true, nil
	}

	_, ok = projectCacheValue[aceDevOpsNamespace.SubProject(a.provider)]
	glog.V(8).Infof("ACE Project %s/%s exists? %t, current projects cache is :%#v",
		aceDevOpsNamespace.Project(a.provider), aceDevOpsNamespace.SubProject(a.provider), ok, data)
	return ok, nil
}

func (a *ACEClient) retrieveProjectsAndSubprojects() error {
	ctx := context.TODO()

	projects, err := a.ListProjects(ctx)
	if err != nil {
		return err
	}

	// < project, <subproject, struct{}> >
	var cache = map[string]map[string]struct{}{}

	for _, project := range projects.Items {
		cache[project.Name] = map[string]struct{}{}

		spaceList, err := a.ListSpaces(ctx, project.Name)
		if err != nil {
			return err
		}

		for _, space := range spaceList.Items {
			cache[project.Name][space.Name] = struct{}{}
		}
	}

	a.cache[CacheProjectListKey] = cache
	return nil
}

var _ ClusterManagementInterface = &aceClusterManagement{}

type aceClusterManagement struct {
	aceClient ace.Interface
}

// ListClusters will list clusters in ace, now will return all clusters
func (clusterM *aceClusterManagement) ListClusters(projectName string) (ParallelClusterInterface, error) {
	ctx := context.TODO()

	regions, err := clusterM.aceClient.ListRegions(ctx)
	if err != nil {
		return NewDefaultParallelCluster([]ClusterInterface{}), err
	}

	clusterInterfaces := []ClusterInterface{}
	for _, cluster := range regions {
		clusterInterfaces = append(clusterInterfaces, &aceCluster{
			Cluster:   cluster,
			aceClient: clusterM.aceClient,
		})
	}
	return NewDefaultParallelCluster(clusterInterfaces), nil
}

func (clusterM *aceClusterManagement) GetCluster(identity string) (ClusterInterface, error) {
	ctx := context.TODO()

	region, err := clusterM.aceClient.GetRegion(ctx, identity)
	if err != nil {
		return nil, err
	}
	return &aceCluster{
		Cluster:   region,
		aceClient: clusterM.aceClient,
	}, nil
}

var _ ClusterInterface = &aceCluster{}

type aceCluster struct {
	ace.Cluster

	aceClient ace.Interface
}

func NewACECluster(clusterID string, clusteName string, aceClient ace.Interface) *aceCluster {
	return &aceCluster{
		Cluster: ace.Cluster{
			ID:   clusterID,
			Name: clusteName,
		},
		aceClient: aceClient,
	}
}

func (cluster *aceCluster) GetClusterIdentity() string {
	if cluster.Name != "" {
		return cluster.Name
	}
	return cluster.ID
}

func (cluster *aceCluster) GetClusterName() string {
	return cluster.Name
}

func (cluster *aceCluster) String() string {
	return fmt.Sprintf("name=%s(id=%s)", cluster.Name, cluster.ID)
}

func (cluster *aceCluster) Namespace(namespace string) ClusterNamespaceInterface {
	return &aceClusterNamespace{
		name:        namespace,
		clusterID:   cluster.ID,
		clusterName: cluster.Name,
		aceClient:   cluster.aceClient,
	}
}

func (c *aceCluster) List(kind string, options metav1.ListOptions, result runtime.Object) (err error) {
	ctx := context.TODO()

	urlValues, err := scheme.ParameterCodec.EncodeParameters(&options, v1alpha1.SchemeGroupVersion) //TODO: how to get schemeGroupVersion ?
	if err != nil {
		glog.Errorf("cannot EncodeParameters options:%#v, error:%s", options, err)
		return err
	}

	list, err := c.aceClient.ListK8SResources(ctx, c.GetClusterIdentity(), "", kind, ace.NewUrlValuesOptions(urlValues))
	if err != nil {
		return err
	}

	err = list.Into(result)
	if err != nil {
		glog.Errorf("cannot transfer ace k8s response %#v to result %T", list, result)
		return err
	}
	return nil
}

func (c *aceCluster) Get(kind string, name string, options metav1.GetOptions, result runtime.Object) (err error) {
	defer func() {
		err = tryAsK8sErrors(kind, name, err)
	}()

	ctx := context.TODO()
	var urlValues url.Values
	urlValues, err = scheme.ParameterCodec.EncodeParameters(&options, v1alpha1.SchemeGroupVersion) //TODO: how to get schemeGroupVersion ?
	if err != nil {
		return err
	}

	var k8sResource *ace.KubernetesResource
	k8sResource, err = c.aceClient.GetK8SResource(ctx, c.GetClusterIdentity(), "", kind, name, ace.NewUrlValuesOptions(urlValues))
	if err != nil {
		return err
	}

	err = k8sResource.Kubernetes.Into(result)
	if err != nil {
		glog.Errorf("resource %#v  cannot transfer to %T, error:%#v", k8sResource.Kubernetes, result, err)
		return err
	}
	return nil
}

func (c *aceCluster) Delete(kind string, name string, options *metav1.DeleteOptions) (err error) {
	defer func() {
		err = tryAsK8sErrors(kind, name, err)
	}()

	ctx := context.TODO()
	err = c.aceClient.DeleteK8SResource(ctx, c.GetClusterIdentity(), "", kind, name, options)
	glog.Infof("deleted %s/%s in ace cluster:%s , err:%#v", kind, name, c, err)
	return err
}

func (c *aceCluster) DeleteCollection(kind string, options *metav1.DeleteOptions, listOptions metav1.ListOptions) error {

	ctx := context.TODO()

	urlValues, err := scheme.ParameterCodec.EncodeParameters(&listOptions, v1alpha1.SchemeGroupVersion) //TODO: how to get schemeGroupVersion ?
	if err != nil {
		glog.Errorf("cannot EncodeParameters options:%#v, error:%s", options, err)
		return err
	}

	list, err := c.aceClient.ListK8SResources(ctx, c.GetClusterIdentity(), "", kind, ace.NewUrlValuesOptions(urlValues))
	if err != nil {
		return err
	}

	runtimeObj := &struct {
		metav1.ObjectMeta `json:"metadata"`
	}{}

	// will delete all selected resources parallel
	p := parallel.P(fmt.Sprintf("Delete Collection %s", kind))
	for _, kubernetesResource := range list.Items {
		err = kubernetesResource.Kubernetes.Into(runtimeObj)
		if err != nil {
			glog.Errorf("cannot transfer %#v to current runtimeObj, will ignore to delete current data", kubernetesResource.Kubernetes)
			continue
		}
		name := runtimeObj.GetName()
		glog.V(5).Infof("will delete %s/%s in ace cluster:%s", kind, name, c)
		p.Add(func() (i interface{}, e error) {
			return nil, c.Delete(kind, name, options)
		})
	}
	_, err = p.Do().Wait()
	return err
}

func (c *aceCluster) Create(kind string, object runtime.Object, result runtime.Object) (err error) {
	ctx := context.TODO()
	aceK8sResource, err := c.aceClient.CreateK8SResource(ctx, c.GetClusterIdentity(), kind, object)
	if err != nil {
		return err
	}
	err = aceK8sResource.Kubernetes.Into(result)
	if err != nil {
		glog.Errorf("cannot transfer %#v to %T", aceK8sResource.Kubernetes, result)
		return err
	}
	return nil
}

func (c *aceCluster) Update(kind string, object metav1.Object) (err error) {
	defer func() {
		err = tryAsK8sErrors(kind, object.GetName(), err)
	}()

	ctx := context.TODO()
	err = c.aceClient.UpdateK8SResource(ctx, c.GetClusterIdentity(), "", kind, object.GetName(), object)
	return err
}

var _ ClusterNamespaceInterface = &aceClusterNamespace{}

type aceClusterNamespace struct {
	name        string
	clusterID   string
	clusterName string

	aceClient ace.Interface
}

func (c *aceClusterNamespace) String() string {
	return fmt.Sprintf("%s(cluster:%s)", c.name, c.CurrentCluster())
}

func (c *aceClusterNamespace) CurrentNamespace() string {
	return c.name
}

// CurrentCluster return aceCluster, but not contains full ace cluster info
// it contains clusterID and clusterName , but we cannot ensure which field will be setted.
// may be it was clusterID (in old version)
// may be it was clusterName (in new version)
func (c *aceClusterNamespace) CurrentCluster() ClusterInterface {
	return NewACECluster(c.clusterID, c.clusterName, c.aceClient)
}

func (c *aceClusterNamespace) List(kind string, options metav1.ListOptions, result runtime.Object) (err error) {
	ctx := context.TODO()
	urlValues, err := scheme.ParameterCodec.EncodeParameters(&options, v1alpha1.SchemeGroupVersion) //TODO: how to get schemeGroupVersion ?
	if err != nil {
		return err
	}
	opts := ace.NewUrlValuesOptions(urlValues)
	aceK8sResourceList, err := c.aceClient.ListK8SResources(ctx, c.CurrentCluster().GetClusterIdentity(), c.CurrentNamespace(), kind, opts)
	err = aceK8sResourceList.Into(result)
	if err != nil {
		glog.Errorf("resource %#v  cannot transfer to %T, error:%#v", aceK8sResourceList, result, err)
		return err
	}
	return nil
}

func (c *aceClusterNamespace) Get(kind string, name string, options metav1.GetOptions, result runtime.Object) (err error) {
	defer func() {
		err = tryAsK8sErrors(kind, name, err)
	}()

	ctx := context.TODO()
	var urlValues url.Values
	urlValues, err = scheme.ParameterCodec.EncodeParameters(&options, v1alpha1.SchemeGroupVersion) //TODO: how to get schemeGroupVersion ?
	if err != nil {
		return err
	}

	var aceK8sResource *ace.KubernetesResource
	aceK8sResource, err = c.aceClient.GetK8SResource(ctx, c.CurrentCluster().GetClusterIdentity(), c.CurrentNamespace(), kind, name, ace.NewUrlValuesOptions(urlValues))
	if err != nil {
		return err
	}
	err = aceK8sResource.Kubernetes.Into(result)
	if err != nil {
		glog.Errorf("Cannot transfer %#v to %T", aceK8sResource.Kubernetes, result)
		return err
	}
	return err
}

func (c *aceClusterNamespace) Delete(kind string, name string, options *metav1.DeleteOptions) (err error) {
	defer func() {
		err = tryAsK8sErrors(kind, name, err)
	}()

	ctx := context.TODO()
	err = c.aceClient.DeleteK8SResource(ctx, c.CurrentCluster().GetClusterIdentity(), c.CurrentNamespace(), kind, name, options)
	glog.Infof("deleted %s/%s/%s in ace cluster:%s , err:%#v", kind, c.CurrentNamespace(), name, c, err)
	return err
}

func (c *aceClusterNamespace) DeleteCollection(kind string, options *metav1.DeleteOptions, listOptions metav1.ListOptions) error {

	ctx := context.TODO()

	urlValues, err := scheme.ParameterCodec.EncodeParameters(&listOptions, v1alpha1.SchemeGroupVersion) //TODO: how to get schemeGroupVersion ?
	if err != nil {
		glog.Errorf("cannot EncodeParameters options:%#v, error:%s", options, err)
		return err
	}

	list, err := c.aceClient.ListK8SResources(ctx, c.CurrentCluster().GetClusterIdentity(), c.CurrentNamespace(), kind, ace.NewUrlValuesOptions(urlValues))
	if err != nil {
		return err
	}

	runtimeObj := &struct {
		metav1.ObjectMeta `json:"metadata"`
	}{}

	// will delete all selected resources parallel
	p := parallel.P(fmt.Sprintf("Delete %s Collection in ns %s", kind, c.CurrentNamespace()))
	for _, kubernetesResource := range list.Items {

		err = kubernetesResource.Kubernetes.Into(runtimeObj)
		if err != nil {
			glog.Errorf("cannot transfer %#v to current runtimeObj, will ignore to delete current data", kubernetesResource.Kubernetes)
			continue
		}

		name := runtimeObj.GetName()

		p.Add(func() (i interface{}, e error) {
			err := c.Delete(kind, name, options)
			if err != nil {
				glog.Errorf("deleted '%s/%s/%s' in cluster '%s' error:%#v", kind, c.CurrentNamespace(), name, c.CurrentCluster().String(), err)
				return nil, err
			}

			glog.Infof("deleted '%s/%s/%s' in cluster '%s'", kind, c.CurrentNamespace(), name, c.CurrentCluster().String())
			return nil, nil
		})

	}
	_, err = p.Do().Wait()
	return err
}

func (c *aceClusterNamespace) Create(kind string, object runtime.Object, result runtime.Object) (err error) {

	ctx := context.TODO()
	var aceK8sResource *ace.KubernetesResource
	aceK8sResource, err = c.aceClient.CreateK8SResource(ctx, c.CurrentCluster().GetClusterIdentity(), kind, object)
	if err != nil {
		return err
	}
	err = aceK8sResource.Kubernetes.Into(result)
	if err != nil {
		glog.Errorf("Cannot transfer %#v to %T", aceK8sResource.Kubernetes, result)
		return err
	}
	return err
}

func (c *aceClusterNamespace) Update(kind string, object metav1.Object) (err error) {
	defer func() {
		err = tryAsK8sErrors(kind, object.GetName(), err)
	}()

	ctx := context.TODO()
	err = c.aceClient.UpdateK8SResource(ctx, c.CurrentCluster().GetClusterIdentity(), c.CurrentNamespace(), kind, object.GetName(), object)
	return err
}

func getRequestOptionsFromTenantInfo(tenantInfo map[string]string, provider v1alpha1.AnnotationProvider) []ace.RequestOptions {
	project, ok := tenantInfo[provider.AnnotationsKeyProject()]
	if !ok {
		return []ace.RequestOptions{}
	}
	return []ace.RequestOptions{
		ace.ProjectOpts(project),
	}
}

func getSubProjectFromTenantInfo(tenantInfo map[string]string, provider v1alpha1.AnnotationProvider) string {
	subProject, ok := tenantInfo[provider.AnnotationsKeySubProject()]
	if !ok {
		glog.Errorf("not contains subProject in %#v", tenantInfo)
		return ""
	}
	return subProject
}

func GetFormatedAnnotation(basedomain, annotation string) string {
	return fmt.Sprintf("%s/%s", basedomain, annotation)
}
func tryAsK8sErrors(kind string, name string, err error) error {
	if err == nil {
		return nil
	}

	aceErrList, ok := err.(*ace.ErrorList)
	if !ok {
		return err
	}
	if len(aceErrList.Errors) == 0 {
		return err
	}

	for _, errItem := range aceErrList.Errors {
		if ace.IsK8sResourceNotFound(aceErrList.StatusCode, errItem) {
			return errors.NewNotFound(k8sschema.GroupResource{Resource: kind}, name)
		}
	}
	return err
}

type ACEDevOpsNamespace corev1.Namespace

func (namespace ACEDevOpsNamespace) IsValid(provider v1alpha1.AnnotationProvider) bool {
	return namespace.Project(provider) != ""
}

func (namespace ACEDevOpsNamespace) Project(provider v1alpha1.AnnotationProvider) string {
	if namespace.Annotations == nil {
		return ""
	}
	return namespace.Annotations[provider.AnnotationsKeyProject()]
}

func (namespace ACEDevOpsNamespace) SubProject(provider v1alpha1.AnnotationProvider) string {
	if namespace.Annotations == nil {
		return ""
	}
	return namespace.Annotations[provider.AnnotationsKeySubProject()]
}
