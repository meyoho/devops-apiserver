package externalversions

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	glog "k8s.io/klog"
)

type TaigaClient struct {
	ProjectManagementClient
	devopsClient devopsclient.Interface
}

type TaigaProject struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Slug        string `json:"slug"`
	Description string `json:"description"`
}

type TaigaUser struct {
	ID    int    `json:"id"`
	Token string `json:"auth_token"`
}

type TaigaUserCount struct {
	Name     string `json:"username"`
	FullName string `json:"full_name"`
}

type TaigaCreateInfo struct {
	ID   int    `json:"id"`
	Slug string `json:"slug"`
}

func NewTaigaClient(endpoint, username, credential string, timeoutsecond int) *TaigaClient {
	serviceClient := NewProjectManagementClient(nil, endpoint, username, credential, timeoutsecond)
	devopsClient := NewDevOpsClient("taiga", "", endpoint, devopsclient.NewBasicAuth(username, credential),
		devopsclient.NewTransport(serviceClient.Client.Transport))
	return &TaigaClient{serviceClient, devopsClient}
}

func (c *TaigaClient) SetTransport(trans http.RoundTripper) {
	c.ProjectManagementClient.Client.Transport = trans
}

func (t *TaigaClient) GetIssueOptionValue(optiontype string) (*devops.IssueFilterDataList, error) {
	return nil, nil
}

func (t *TaigaClient) Authenticate() (status *devops.HostPortStatus, err error) {
	path := t.Url("/api/v1/auth")
	data := map[string]string{}
	data["username"] = t.Username
	data["password"] = t.Credential
	data["type"] = "normal"

	status = &devops.HostPortStatus{}

	bodyJson, _ := json.Marshal(data)
	req, err := http.NewRequest(http.MethodPost, path, bytes.NewBuffer(bodyJson))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		glog.Errorf("Error happend when create Taiga request, Endpoint is %s", path)
		return
	}
	resp, err := t.DoRequest(req)
	if resp.StatusCode == 200 {
		status.StatusCode = 200
		status.Response = fmt.Sprintf("correct username and password")
	} else {
		status.StatusCode = resp.StatusCode
		status.Response = string(resp.Body)
	}
	return
}

func (t *TaigaClient) GetIssueList(*devopsclient.ListIssuesOptions) (*devops.IssueDetailList, error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("ProjectManagement"), http.MethodGet)
}

func (c *TaigaClient) GetIssue(Issuekey string) (*devops.IssueDetail, error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("ProjectManagement"), http.MethodGet)
}

func (t *TaigaClient) CreateProject(projectName, projectDescription, projectLead, projectKey string) (result *devops.ProjectData, err error) {
	return CreateProject(t.devopsClient, projectName, projectDescription, projectLead, projectKey)
}

func (t *TaigaClient) SearchforUsers(name string) (result *devops.ProjectmanagementUser, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("ProjectManagement"), http.MethodGet)
}

func (t *TaigaClient) GetUserCount() (usercount int, err error) {
	glog.V(6).Infof("Get %s Type projectmanagement usercount, EndPoint is %s ", TaigaType, t.Endpoint)
	urlSuffix := fmt.Sprintf("/api/v1/users")
	resp, err := t.RequestProjectManagement(urlSuffix, TaigaType)
	if err != nil {
		return 0, fmt.Errorf("Error happned when get usercount and error is : %v", err)
	}
	Users := make([]TaigaUserCount, 0)
	err = json.Unmarshal([]byte(resp.Body), &Users)
	if err != nil {
		err = errors.NewBadRequest("Can't get usercount, please check url or secret of your service")
		glog.Errorf("get user count failed: %v", err)
		return 0, err
	}
	return len(Users), nil
}

func (t *TaigaClient) GetProjects(page, pagesize string) (result *devops.ProjectDataList, err error) {
	return GetProjects(t.devopsClient, page, pagesize)
}

func (c *TaigaClient) GetProjectList(listoptions *devops.RoleMappingListOptions) ([]project, error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("ProjectManagement"), http.MethodGet)
}

func (c *TaigaClient) GetPermissionNameIdMap() (map[string]int, error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("ProjectManagement"), http.MethodGet)
}

func (c *TaigaClient) GetRoleMappping([]project, map[string]int) (*devops.RoleMapping, error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("ProjectManagement"), http.MethodGet)
}

func (c *TaigaClient) AddUserforRole(userandoperation devops.UserRoleOperation, projectIdNameMap map[string]string, operation devops.ProjectUserRoleOperation, permissionNameIDmap map[string]int) {

}

func (c *TaigaClient) RemoveUserforRole(userandoperation devops.UserRoleOperation, projectIdNameMap map[string]string, operation devops.ProjectUserRoleOperation, permissionNameIDmap map[string]int) {
}
