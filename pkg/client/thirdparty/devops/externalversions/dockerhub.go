package externalversions

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

// region dockerhub
type DockerHubClient struct {
	ImageRegistryClient
	AccessUrl string
}

func NewDockerHubClient(endpoint, username, credential, accessUrl string) *DockerHubClient {
	serviceClient := NewImageRegistryClient(endpoint, username, credential)
	return &DockerHubClient{serviceClient, accessUrl}
}

func (c *DockerHubClient) SetTransport(trans http.RoundTripper) {
	c.ImageRegistryClient.Client.Transport = trans
}

func (c *DockerHubClient) CheckRegistryAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.Endpoint, nil)
}

func (c *DockerHubClient) CheckRepositoryAvailable(repositoryName string, lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	glog.V(6).Infof("Check dockerhub image repository: %s is available or not", repositoryName)
	var (
		start       = time.Now()
		duration    time.Duration
		lastAttempt = metav1.NewTime(start)
		token       string
	)
	status = &devops.HostPortStatus{
		LastAttempt: &lastAttempt,
	}
	urlSuffix := fmt.Sprintf("v2/repositories/%s", repositoryName)
	token, err = c.GetDockerHubToken()
	if err != nil {
		status.StatusCode = 500
		status.Response = err.Error()
	} else {
		resp, err := c.RequestRegistryWithToken(urlSuffix, token)
		if err != nil {
			status.StatusCode = 500
			status.Response = err.Error()
		} else {
			switch resp.StatusCode {
			case http.StatusOK:
				status.Response = ImageRepositoryAvailable
			case http.StatusNotFound:
				status.Response = ImageRepositoryNotFound
			default:
				status.Response = ImageRepositoryUnAvailable
			}
			status.StatusCode = resp.StatusCode
		}
	}
	duration = time.Since(start)
	status.Delay = &duration
	return
}

// TODO: add token to session for avoiding frequently request
func (c *DockerHubClient) GetDockerHubToken() (token string, err error) {
	glog.V(6).Infof("Get DockerHub token from %s", c.Endpoint)
	if c.Credential == "" || c.Username == "" {
		return
	}
	var req *http.Request
	urlSuffix := fmt.Sprintf("v2/users/login")
	path := c.Url(urlSuffix)
	data := make(map[string]string, 0)
	data["username"] = c.Username
	data["password"] = c.Credential
	bodyJson, _ := json.Marshal(data)
	req, err = http.NewRequest(http.MethodPost, path, bytes.NewBuffer(bodyJson))
	if err != nil {
		glog.Errorf("Init http request error: %v", err)
		return
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := c.DoRequest(req)
	if err != nil {
		glog.Errorf("Request dockerhub to get token failed: %v", err)
		return
	}
	if resp.StatusCode != 200 {
		glog.Errorf("Get dockerhub token failed: %v", resp.StatusCode)
		err = fmt.Errorf("%d: %v", resp.StatusCode, resp.Body)
		return
	}
	dockerHubToken := new(DockerHubToken)
	err = json.Unmarshal([]byte(resp.Body), dockerHubToken)
	if err != nil {
		glog.Errorf("DockerHubClient parse user token error %v", err)
		return
	}
	token = dockerHubToken.Token
	return
}

func (c *DockerHubClient) GetImageRepos() (result *devops.ImageRegistryBindingRepositories, err error) {
	glog.V(6).Infof("Get DockerHub repositories from %s", c.Endpoint)
	result = &devops.ImageRegistryBindingRepositories{}
	token, err := c.GetDockerHubToken()
	if err != nil {
		glog.Errorf("Get DockerHub repositories failed as get token error: %v", err)
		return
	}
	orgUrlSuffix := fmt.Sprintf("v2/user/orgs?page_size=%d", DockerHubPageSize)
	resp, err := c.RequestRegistryWithToken(orgUrlSuffix, token)
	if err != nil {
		glog.Errorf("Get dockerhub registry token failed: %s", err)
		return
	}
	// Get all organization belong to the user
	orgResp := new(DockerHubOrganizationResponse)
	json.Unmarshal([]byte(resp.Body), orgResp)
	orgResults := orgResp.Results
	if orgResp.Count > DockerHubPageSize {
		count := orgResp.Count / DockerHubPageSize
		for i := 0; i < count; i++ {
			orgUrlSuffix = fmt.Sprintf("v2/user/orgs?page_size=%d&page=%d", DockerHubPageSize, i+2)
			resp, err = c.RequestRegistryWithToken(orgUrlSuffix, token)
			if err != nil {
				glog.Errorf("List dockerhub registry organization failed: %s", err)
				return
			}
			json.Unmarshal([]byte(resp.Body), orgResp)
			orgResults = append(orgResults, orgResp.Results...)
		}
	}
	orgNameList := make([]string, 0)
	for _, r := range orgResults {
		if r.Orgname != c.Username {
			orgNameList = append(orgNameList, r.Orgname)
		}
	}
	orgNameList = append(orgNameList, c.Username)
	// Get all repos
	dockerHubResp := new(DockerHubRepositoryResponse)
	repoList := dockerHubResp.Results
	for _, orgName := range orgNameList {
		urlSuffix := fmt.Sprintf("v2/repositories/%s?page_size=%d", orgName, DockerHubPageSize)
		glog.Infof("List dockerhub org: %s repo url: %s", orgName, urlSuffix)
		resp, err = c.RequestRegistryWithToken(urlSuffix, token)
		if err != nil {
			glog.Errorf("List dockerhub org: %s repo failed: %s", orgName, err)
			return
		}
		err = json.Unmarshal([]byte(resp.Body), dockerHubResp)
		if err != nil {
			glog.Errorf("Parse dockerhub repo response error: %s", err)
			continue
		}
		repoList = append(repoList, dockerHubResp.Results...)
		if dockerHubResp.Count > DockerHubPageSize {
			glog.V(6).Infof("Dockerhub org: %s repo count over pagesize", orgName)
			count := dockerHubResp.Count / DockerHubPageSize
			for i := 0; i < count; i++ {
				urlSuffix = fmt.Sprintf("v2/repositories/%s?page_size=%d&page=%d", orgName, DockerHubPageSize, i+2)
				resp, err = c.RequestRegistryWithToken(urlSuffix, token)
				if err != nil {
					glog.Errorf("List dockerhub org: %s repo page: %d failed: %s", orgName, i+2, err)
					return
				}
				err = json.Unmarshal([]byte(resp.Body), dockerHubResp)
				if err != nil {
					glog.Errorf("Parse dockerhub repo response error: %s", err)
					continue
				}
				repoList = append(repoList, dockerHubResp.Results...)
			}
		}
	}
	for _, repo := range repoList {
		name := fmt.Sprintf("%s/%s", repo.Namespace, repo.Name)
		result.Items = append(result.Items, name)
	}
	return result, nil
}

func (c *DockerHubClient) GetImageTags(repositoryName string) (result []devops.ImageTag, err error) {
	glog.V(6).Infof("Get DockerHub tags %s/%s", c.Endpoint, repositoryName)
	urlSuffix := fmt.Sprintf("v2/repositories/%s/tags", repositoryName)
	token, err := c.GetDockerHubToken()
	if err != nil {
		glog.Errorf("Get DockerHub repositories failed as get token error: %v", err)
		return
	}
	resp, err := c.RequestRegistryWithToken(urlSuffix, token)
	if err != nil {
		glog.Errorf("Can't get repository: %s tag list", repositoryName)
		return
	}
	dockerHubTagResp := new(DockerHubTagResponse)
	err = json.Unmarshal([]byte(resp.Body), dockerHubTagResp)
	if err != nil {
		glog.Errorf("Parse dockerhub tag data failed: %s", err)
		return
	}
	tags := dockerHubTagResp.Results
	for _, tag := range tags {
		tmpTag := devops.ImageTag{}
		tmpTag.Name = tag.Name
		tmpTag.Size = c.ConvertSizeToString(tag.FullSize)
		t, err := time.Parse(time.RFC3339Nano, tag.LastUpdated)
		if err != nil {
			glog.Errorf("Tag %s:%s convert time failed: %s", repositoryName, tag.Name, err)
		}
		tmpTag.CreatedAt = &metav1.Time{Time: t}
		result = append(result, tmpTag)
	}
	return
}

func (c *DockerHubClient) CheckAuthentication() (status *devops.HostPortStatus, err error) {
	token, err := c.GetDockerHubToken()
	if err != nil || token == "" {
		glog.Errorf("Check dockerhub auth failed: %s", err)
		return
	}
	status = &devops.HostPortStatus{
		StatusCode: 200,
		Response:   "Check authentication successful.",
	}
	return
}

func (c *DockerHubClient) GetRegistryEndpoint() (endpoint string, err error) {
	// Get dockerhub registry endpoint
	return "index.docker.io", nil
}

func (c *DockerHubClient) GetImageRepoLink(name string) (link string) {
	return fmt.Sprintf("%s/r/%s", strings.TrimRight(c.AccessUrl, "/"), name)
}

func (c *DockerHubClient) TriggerScanImage(repositoryName, tag string) (result *devops.ImageResult) {
	return
}

func (c *DockerHubClient) GetVulnerability(repositoryName, tag string) (result *devops.VulnerabilityList, err error) {
	return
}

func (c *DockerHubClient) GetImageProjects() (result *devops.ProjectDataList, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("projectdatalist"), "get")
}

func (c *DockerHubClient) CreateImageProject(name string) (result *devops.ProjectData, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("porjectdata"), "create")
}

// end region
