package externalversions

import (
	"context"
	"fmt"
	"net/http"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/util/generic"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"github.com/pkg/errors"
	v1 "k8s.io/api/core/v1"
	"k8s.io/klog"
)

type JenkinsClient struct {
	Service      *devops.Jenkins
	devopsClient devopsclient.Interface
	Cxt          context.Context
}

func NewJenkinsClient(service *devops.Jenkins, secret *v1.Secret, roundTripper http.RoundTripper) (*JenkinsClient, error) {
	if secret.Type != v1.SecretTypeBasicAuth {
		return nil, errors.New("Invalid secret type, secret type for Jenkins must be kubernetes.io/basic-auth")

	}

	if roundTripper == nil {
		roundTripper = generic.GetDefaultTransport()
	}

	host := service.GetHostPort().Host
	basicAuth := k8s.GetDataBasicAuthFromSecret(secret)

	devopsClient := NewDevOpsClient("jenkins",
		"",
		host,
		devopsclient.NewBasicAuth(basicAuth.Username, basicAuth.Password),
		devopsclient.NewTransport(roundTripper))

	return &JenkinsClient{
		Service:      service,
		devopsClient: devopsClient,
		Cxt:          context.Background(),
	}, nil
}

func (c *JenkinsClient) GetJobProgressiveLog(pipelineNamespace, pipelineName string, buildNumber, start int64) (*devops.PipelineLog, error) {
	log, err := c.devopsClient.GetJobProgressiveLog(context.Background(), pipelineNamespace, pipelineName, buildNumber, start)
	if err != nil {
		klog.Errorf("Unable to get job '%s/%s' progressive log: reason %s", pipelineNamespace, pipelineName, err)
		return nil, err
	}

	return &devops.PipelineLog{
		HasMore:   log.HasMore,
		NextStart: log.NextStart,
		Text:      log.Text,
	}, nil
}

func (c *JenkinsClient) GetBranchJobProgressiveLog(pipelineNamespace, pipelineName, branchName string, buildNumber, start int64) (*devops.PipelineLog, error) {
	log, err := c.devopsClient.GetBranchJobProgressiveLog(context.Background(), pipelineNamespace, pipelineName, branchName, buildNumber, start)
	if err != nil {
		klog.Errorf("Unable to get branch job '%s/%s' progressive log: reason %s", pipelineNamespace, pipelineName, err)
		return nil, err
	}

	return &devops.PipelineLog{
		HasMore:   log.HasMore,
		NextStart: log.NextStart,
		Text:      log.Text,
	}, nil
}

func (c *JenkinsClient) GetJobNodeLog(pipelineNamespace, pipelineName, organization, runId, nodeId string, start int64) (*devops.PipelineLog, error) {
	log, err := c.devopsClient.GetJobNodeLog(context.Background(), pipelineNamespace, pipelineName, organization, runId, nodeId, start)
	if err != nil {
		klog.Errorf("Unable to get job '%s/%s' node log: reason %s", pipelineNamespace, pipelineName, err)
		return nil, err
	}

	return &devops.PipelineLog{
		HasMore:   log.HasMore,
		NextStart: log.NextStart,
		Text:      log.Text,
	}, nil
}

func (c *JenkinsClient) GetBranchJobNodeLog(pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string, start int64) (*devops.PipelineLog, error) {
	log, err := c.devopsClient.GetBranchJobNodeLog(context.Background(), pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, start)
	if err != nil {
		klog.Errorf("Unable to get branch job '%s/%s' node log: reason %s", pipelineNamespace, pipelineName, err)
		return nil, err
	}

	return &devops.PipelineLog{
		HasMore:   log.HasMore,
		NextStart: log.NextStart,
		Text:      log.Text,
	}, nil
}

func (c *JenkinsClient) GetJobStepLog(pipelineNamespace, pipelineName, organization, runId, nodeId, stepId string, start int64) (*devops.PipelineLog, error) {
	log, err := c.devopsClient.GetJobStepLog(context.Background(), pipelineNamespace, pipelineName, organization, runId, nodeId, stepId, start)
	if err != nil {
		klog.Errorf("Unable to get job '%s/%s' step log: reason %s", pipelineNamespace, pipelineName, err)
		return nil, err
	}

	return &devops.PipelineLog{
		HasMore:   log.HasMore,
		NextStart: log.NextStart,
		Text:      log.Text,
	}, nil
}

func (c *JenkinsClient) GetBranchJobStepLog(pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, stepId string, start int64) (*devops.PipelineLog, error) {
	log, err := c.devopsClient.GetBranchJobStepLog(context.Background(), pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, stepId, start)
	if err != nil {
		klog.Errorf("Unable to get branch job '%s/%s' step log: reason %s", pipelineNamespace, pipelineName, err)
		return nil, err
	}

	return &devops.PipelineLog{
		HasMore:   log.HasMore,
		NextStart: log.NextStart,
		Text:      log.Text,
	}, nil
}

func (c *JenkinsClient) GetBranchJobProgressiveScanLog(pipelineConfigNamespace, pipelineConfigName string, start int64) (*devops.PipelineConfigLog, error) {
	log, err := c.devopsClient.GetBranchProgressiveScanLog(context.Background(), pipelineConfigNamespace, pipelineConfigName, start)
	if err != nil {
		klog.Errorf("Unable to get branch job '%s/%s' scan log: reason %s", pipelineConfigNamespace, pipelineConfigName, err)
		return nil, err
	}

	return &devops.PipelineConfigLog{
		HasMore:   log.HasMore,
		NextStart: log.NextStart,
		Text:      log.Text,
	}, nil
}

func (c *JenkinsClient) ScanMultiBranchJob(pipelineConfigNamespace, pipelineConfigName string) error {
	err := c.devopsClient.ScanMultiBranchJob(context.Background(), pipelineConfigNamespace, pipelineConfigName)
	if err != nil {
		klog.Errorf("Unable to trigger scan of multi-branch job '%s/%s' reason %s", pipelineConfigNamespace, pipelineConfigName, err)
	}

	return err
}

func (c *JenkinsClient) GetJobNodes(pipelineNamespace, pipelineName, organization, runId string) ([]devops.PipelineBlueOceanTask, error) {
	nodes, err := c.devopsClient.GetJobNodes(context.Background(), pipelineNamespace, pipelineName, organization, runId)
	if err != nil {
		klog.Errorf("Unable to get job '%s/%s' nodes", pipelineNamespace, pipelineName)
		return nil, err
	}

	var tasks []devops.PipelineBlueOceanTask
	for _, node := range nodes {
		tasks = append(tasks, c.convertPipelineBlueOceanTask(node))
	}
	return tasks, nil
}

func (c *JenkinsClient) GetBranchJobNodes(pipelineNamespace, pipelineName, branchName, organization, runId string) ([]devops.PipelineBlueOceanTask, error) {
	nodes, err := c.devopsClient.GetBranchJobNodes(context.Background(), pipelineNamespace, pipelineName, branchName, organization, runId)
	if err != nil {
		klog.Errorf("Unable to get branch job '%s/%s' nodes", pipelineNamespace, pipelineName)
		return nil, err
	}

	var tasks []devops.PipelineBlueOceanTask
	for _, node := range nodes {
		tasks = append(tasks, c.convertPipelineBlueOceanTask(node))
	}
	return tasks, nil
}

func (c *JenkinsClient) GetJobSteps(pipelineNamespace, pipelineName, organization, runId, nodeId string) ([]devops.PipelineBlueOceanTask, error) {
	steps, err := c.devopsClient.GetJobSteps(context.Background(), pipelineNamespace, pipelineName, organization, runId, nodeId)
	if err != nil {
		klog.Errorf("Unable to get job '%s/%s' steps", pipelineNamespace, pipelineName)
		return nil, err
	}

	var tasks []devops.PipelineBlueOceanTask
	for _, node := range steps {
		tasks = append(tasks, c.convertPipelineBlueOceanTask(node))
	}
	return tasks, nil
}

func (c *JenkinsClient) GetBranchJobSteps(pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string) ([]devops.PipelineBlueOceanTask, error) {
	steps, err := c.devopsClient.GetBranchJobSteps(context.Background(), pipelineNamespace, pipelineName, branchName, organization, runId, nodeId)
	if err != nil {
		klog.Errorf("Unable to get job '%s/%s' steps", pipelineNamespace, pipelineName)
		return nil, err
	}

	var tasks []devops.PipelineBlueOceanTask
	for _, node := range steps {
		tasks = append(tasks, c.convertPipelineBlueOceanTask(node))
	}
	return tasks, nil
}

func (c *JenkinsClient) SubmitBranchInputStep(pipelineNamespace, pipelineName, branch, organization, runId string, inputOptions devops.PipelineInputOptions) error {
	parameters := make([]devopsclient.PipelineParameter, 0)
	if inputOptions.Parameters != nil {
		for _, param := range inputOptions.Parameters {
			parameters = append(parameters, devopsclient.PipelineParameter{
				Name:  param.Name,
				Type:  devopsclient.PipelineParameterType(param.Type),
				Value: param.Value,
			})
		}
	}

	options := devopsclient.PipelineInputOptions{
		Approve:          inputOptions.Approve,
		InputID:          inputOptions.InputID,
		PlatformApprover: inputOptions.PlatformApprover,
		Parameters:       parameters,
	}

	nodeID, stepID := inputOptions.Stage, inputOptions.Step
	return c.devopsClient.SubmitBranchInputStep(context.Background(), pipelineNamespace, pipelineName, branch, organization, runId,
		fmt.Sprintf("%d", nodeID), fmt.Sprintf("%d", stepID), options)
}

func (c *JenkinsClient) SubmitInputStep(pipelineNamespace, pipelineName, organization, runId string, inputOptions devops.PipelineInputOptions) error {
	parameters := make([]devopsclient.PipelineParameter, 0)
	if inputOptions.Parameters != nil {
		for _, param := range inputOptions.Parameters {
			parameters = append(parameters, devopsclient.PipelineParameter{
				Name:  param.Name,
				Type:  devopsclient.PipelineParameterType(param.Type),
				Value: param.Value,
			})
		}
	}

	options := devopsclient.PipelineInputOptions{
		Approve:          inputOptions.Approve,
		InputID:          inputOptions.InputID,
		PlatformApprover: inputOptions.PlatformApprover,
		Parameters:       parameters,
	}

	nodeId, stepId := inputOptions.Stage, inputOptions.Step
	return c.devopsClient.SubmitInputStep(context.Background(), pipelineNamespace, pipelineName, organization, runId,
		fmt.Sprintf("%d", nodeId), fmt.Sprintf("%d", stepId), options)
}

func (c *JenkinsClient) convertPipelineBlueOceanRef(ref devopsclient.PipelineBlueOceanRef) devops.PipelineBlueOceanRef {
	return devops.PipelineBlueOceanRef{
		Href:        ref.Href,
		ID:          ref.ID,
		Type:        ref.Type,
		URLName:     ref.URLName,
		Description: ref.Description,
		Name:        ref.Name,
		Value:       ref.Value.Value,
	}
}

func (c *JenkinsClient) convertPipelineBlueOceanRefs(refs []devopsclient.PipelineBlueOceanRef) []devops.PipelineBlueOceanRef {
	var results []devops.PipelineBlueOceanRef
	for _, ref := range refs {
		results = append(results, c.convertPipelineBlueOceanRef(ref))
	}
	return results
}

func (c *JenkinsClient) convertPipelineBlueOceanInput(input *devopsclient.PipelineBlueOceanInput) *devops.PipelineBlueOceanInput {
	if input == nil {
		return nil
	}

	return &devops.PipelineBlueOceanInput{
		PipelineBlueOceanRef: c.convertPipelineBlueOceanRef(input.PipelineBlueOceanRef),
		Message:              input.Message,
		OK:                   input.OK,
		Parameters:           c.convertPipelineBlueOceanParameters(input.Parameters),
		Submitter:            input.Submitter,
	}
}

func (c *JenkinsClient) convertPipelineBlueOceanParameters(params []devopsclient.PipelineBlueOceanParameter) []devops.PipelineBlueOceanParameter {
	var results []devops.PipelineBlueOceanParameter
	for _, param := range params {
		results = append(results, devops.PipelineBlueOceanParameter{
			PipelineBlueOceanRef:  c.convertPipelineBlueOceanRef(param.PipelineBlueOceanRef),
			DefaultParameterValue: c.convertPipelineBlueOceanRef(param.PipelineBlueOceanRef),
		})
	}
	return results
}

func (c *JenkinsClient) convertPipelineBlueOceanTask(task devopsclient.PipelineBlueOceanTask) devops.PipelineBlueOceanTask {
	return devops.PipelineBlueOceanTask{
		PipelineBlueOceanRef: c.convertPipelineBlueOceanRef(task.PipelineBlueOceanRef),
		DisplayDescription:   task.DisplayDescription,
		DisplayName:          task.DisplayName,
		DurationInMillis:     task.DurationInMillis,
		Input:                c.convertPipelineBlueOceanInput(task.Input),
		Result:               task.Result,
		State:                task.State,
		StartTime:            task.StartTime,
		Edges:                c.convertPipelineBlueOceanRefs(task.Edges),
		Actions:              c.convertPipelineBlueOceanRefs(task.Actions),
	}
}

// Jenkins pipeline test report

func (c *JenkinsClient) GetTestReportSummary(pcNamespace, pcName, organization, runId string) (
	summary *devops.PipelineTestReportSummary, err error) {
	var rawSummary *devopsclient.TestReportSummary
	if rawSummary, err = c.devopsClient.GetTestReportSummary(context.Background(), pcNamespace,
		pcName, organization, runId); err == nil {
		summary = convertTestReportSummary(rawSummary)
	}
	return
}

func (c *JenkinsClient) GetBranchTestReportSummary(pcNamespace, pcName, branchName, organization, runId string) (
	summary *devops.PipelineTestReportSummary, err error) {
	var rawSummary *devopsclient.TestReportSummary
	if rawSummary, err = c.devopsClient.GetBranchTestReportSummary(context.Background(), pcNamespace,
		pcName, branchName, organization, runId); err == nil {
		summary = convertTestReportSummary(rawSummary)
	}
	return
}

func convertTestReportSummary(rawSummary *devopsclient.TestReportSummary) (summary *devops.PipelineTestReportSummary) {
	summary = &devops.PipelineTestReportSummary{
		ExistingFailed: rawSummary.ExistingFailed,
		Failed:         rawSummary.Failed,
		Fixed:          rawSummary.Fixed,
		Passed:         rawSummary.Passed,
		Regressions:    rawSummary.Regressions,
		Skipped:        rawSummary.Skipped,
		Total:          rawSummary.Total,
	}
	return
}

func (c *JenkinsClient) GetTestReports(pcNamespace, pcName, organization, runId, status,
	state, stateBang string, start, limit int64) (report *devops.PipelineTestReport, err error) {
	var (
		rawReport *devopsclient.PipelineTestReport
	)

	if rawReport, err = c.devopsClient.GetTestReports(context.Background(), pcNamespace,
		pcName, organization, runId, status, state, stateBang, start, limit); err == nil {
		report = convertPipelineTestReport(rawReport)
	}
	return
}

func (c *JenkinsClient) GetBranchTestReports(pcNamespace, pcName, branchName, organization, runId, status,
	state, stateBang string, start, limit int64) (report *devops.PipelineTestReport, err error) {
	var (
		rawReport *devopsclient.PipelineTestReport
	)

	if rawReport, err = c.devopsClient.GetBranchTestReports(context.Background(), pcNamespace,
		pcName, branchName, organization, runId, status, state, stateBang, start, limit); err == nil {
		report = convertPipelineTestReport(rawReport)
	}
	return
}

func convertPipelineTestReport(rawReport *devopsclient.PipelineTestReport) (report *devops.PipelineTestReport) {
	reportItems := make([]devops.PipelineTestReportItem, 0)
	if len(rawReport.Items) > 0 {
		for _, item := range rawReport.Items {
			reportItems = append(reportItems, devops.PipelineTestReportItem{
				Age:             int(item.Age),
				Duration:        float32(item.Duration),
				ErrorDetails:    item.ErrorDetails,
				ErrorStackTrace: item.ErrorStackTrace,
				HasStdLog:       item.HasStdLog,
				ID:              item.ID,
				Name:            item.Name,
				State:           item.State,
				Status:          item.Status,
			})
		}
	}

	report = &devops.PipelineTestReport{Items: reportItems}
	return
}

// End with Jenkins pipeline test report
