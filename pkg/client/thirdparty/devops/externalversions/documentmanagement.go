package externalversions

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"

	"context"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
)

const (
	ConfluenceType = "Confluence"
)

type DocumentManagementResponse struct {
	StatusCode int
	Body       []byte
}

type DocumentManagementClient struct {
	Endpoint      string
	Cxt           context.Context
	Secret        *corev1.Secret
	AuthType      authType
	BasicAuthInfo *k8s.SecretDataBasicAuth
	OAuth2Info    *k8s.SecretDataOAuth2
}

type DocumentProject struct {
	Expand         string `json:"expand"`
	Self           string `json:"self"`
	ID             string `json:"id"`
	Key            string `json:"key"`
	Name           string `json:"name"`
	ProjectTypeKey string `json:"projectTypeKey"`
	AvatarUrls     string `json:"avatarUrls"`
	Description    string `json:"description"`
}

func NewDocumentManagementClient(endpoint string, secret *corev1.Secret, timeoutsecond int) DocumentManagementClient {
	client := DocumentManagementClient{
		Endpoint: endpoint,
		Cxt:      context.Background(),
		Secret:   secret,
	}
	client.AuthType = basicAuth
	client.BasicAuthInfo = k8s.GetDataBasicAuthFromSecret(secret)
	if secret != nil && secret.Type == devops.SecretTypeOAuth2 {
		client.AuthType = oAuthToken
		client.OAuth2Info = k8s.GetDataOAuth2FromSecret(secret)
	}
	return client
}
