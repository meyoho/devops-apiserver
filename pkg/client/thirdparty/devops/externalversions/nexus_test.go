package externalversions

import (
	. "github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
)

var _ = Describe("Nexus", func() {
	var (
		cli *NexusClient
	)

	JustBeforeEach(func() {
		cli = &NexusClient{}
	})

	Describe("nexus splice url", func() {
		Context("when endpoint without / ", func() {
			It("should return right url", func() {
				cli.Endpoint = "http://testnexus.io"
				result := cli.Url(stringJoin(nexusApiPrefix, nexusApiVersion, "path1", "path2"))
				gomega.Expect(result).To(gomega.BeEquivalentTo("http://testnexus.io/service/rest/v1/path1/path2"))
			})
		})

		Context("when endpoint with / ", func() {
			It("should return right url", func() {
				cli.Endpoint = "http://testnexus.io/"
				result := cli.Url(stringJoin(nexusApiPrefix, nexusApiVersion, "path1", "path2"))
				gomega.Expect(result).To(gomega.BeEquivalentTo("http://testnexus.io/service/rest/v1/path1/path2"))
			})
		})
	})
})
