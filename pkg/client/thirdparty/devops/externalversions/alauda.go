package externalversions

import (
	"fmt"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/util/parallel"
	"k8s.io/apimachinery/pkg/runtime"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	"alauda.io/devops-apiserver/pkg/role"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// AlaudaClientFactory factory interface
type AlaudaClientFactory interface {
	Client(kind string, config *corev1.ConfigMap) (AlaudaProductClient, error)
}

// NewAlaudaClientFactory constructor for AlaudaClientFactory
func NewAlaudaClientFactory(opts Options) AlaudaClientFactory {
	return alaudaClientFactory{opts: opts}
}

// internal implementation of the factory
type alaudaClientFactory struct {
	opts Options
}

var _ AlaudaClientFactory = alaudaClientFactory{}

// Client returns a client for AlaudaProduct
func (af alaudaClientFactory) Client(kind string, config *corev1.ConfigMap) (AlaudaProductClient, error) {
	if kind == "" {
		kind = config.Data[v1alpha1.SettingsKeyProduct]
	}

	switch strings.ToLower(kind) {
	case "ace":
		opts := ace.Options{
			Transport:          af.opts.Transport,
			Timeout:            30 * time.Second,
			Token:              config.Data[v1alpha1.SettingsKeyACEToken],
			RootAccount:        config.Data[v1alpha1.SettingsKeyACERootAccount],
			APIAddress:         config.Data[v1alpha1.SettingsKeyACEAPIEndpoint],
			AnnotationProvider: af.opts.AnnotationProvider,
		}

		return NewACEClient(ace.New(opts), opts.AnnotationProvider), nil
	case "acp":
		fallthrough
	default:
		opts := ACPOptions{
			af.opts.KubernetesClient, af.opts.RestConfig, af.opts.MultiClusterHost, af.opts.SystemNamespace, af.opts.AnnotationProvider,
		}
		return NewACPClient(opts)
	}
}

// AlaudaProductClient main alauda client interface
type AlaudaProductClient interface {
	// GetProjets according to a specific list of namespaces provide a list of projects
	GetProjectUserRoles(list *corev1.NamespaceList) ([]*role.ProjectUserRoleAssignment, error)

	GetClusterManagement() ClusterManagementInterface

	// GetProjectNamespaces will get project biz namespaces according to a namespace in devops cluster.
	GetProjectNamespaces(namespace *corev1.Namespace) ([]ClusterNamespaceInterface, error)

	// ProjectExists will judge whether the project/subproject exists that specified by namespace
	ProjectExists(namespace *corev1.Namespace) (bool, error)
}

// ProjectsFilter a unified interface for filtering projects
type ProjectsFilter interface {
	FilterProjects(list *corev1.NamespaceList, provider v1alpha1.AnnotationProvider) (projects []*role.ProjectUserRoleAssignment)
}

// StandardProjectsFilter common filter for projects
type StandardProjectsFilter struct{}

var _ ProjectsFilter = StandardProjectsFilter{}

// FilterProjects filter namespaces for specific projects
func (StandardProjectsFilter) FilterProjects(list *corev1.NamespaceList, provider v1alpha1.AnnotationProvider) (projects []*role.ProjectUserRoleAssignment) {
	projects = make([]*role.ProjectUserRoleAssignment, 0, len(list.Items))
	if list == nil || len(list.Items) == 0 {
		return
	}
	// creates an index to prevent adding the same project
	// over and over again with different namespaces
	index := map[string]struct{}{}
	for _, n := range list.Items {
		annotations := n.GetAnnotations()
		project := role.NewProject(n.GetName())
		if len(annotations) > 0 {
			if value, ok := annotations[provider.AnnotationsKeyProject()]; ok {
				project.Name = value
			}
			if value, ok := annotations[provider.AnnotationsKeySubProject()]; ok {
				project.SubProject = value
			}
		}
		if _, ok := index[project.Name+project.SubProject]; !ok {
			projects = append(projects, project)
			index[project.Name+project.SubProject] = struct{}{}
		}
	}
	return
}

type ClusterManagementInterface interface {
	// ListClusters will return clusters that owned by projectName,
	// if projectName is empty, will return all clusters
	ListClusters(projectName string) (ParallelClusterInterface, error)
	GetCluster(identity string) (ClusterInterface, error)
}

var _ ParallelClusterInterface = &defaultParallelCluster{}

type defaultParallelCluster struct {
	Items []ClusterInterface
}

func NewDefaultParallelCluster(Items []ClusterInterface) ParallelClusterInterface {
	return &defaultParallelCluster{
		Items: Items,
	}
}

func (clusters *defaultParallelCluster) AppendCluster(items ...ClusterInterface) {
	clusters.Items = append(clusters.Items, items...)
}

func (clusters *defaultParallelCluster) String() string {
	s := strings.Builder{}
	for i, c := range clusters.Items {
		s.WriteString(fmt.Sprintf("cluster%d:%s; ", i+1, c.String()))
	}
	return s.String()
}

func (clusters *defaultParallelCluster) Clusters() []ClusterInterface {
	return clusters.Items
}

// Namespace return parallel cluster interface in the namespace
func (clusters *defaultParallelCluster) Namespace(namespace string) ParallelClusterNamespaceInterface {
	return NewDefaultParallelClusterNamespaceList(clusters.Items, namespace)
}

func (clusters *defaultParallelCluster) Parallel(task func(ClusterInterface) parallel.Task) ([]interface{}, error) {

	p := parallel.P("").SetConcurrent(3)
	for _, _cluster := range clusters.Items {
		cluster := _cluster
		p.Add(task(cluster))
	}

	result, err := p.Do().Wait()
	return result, err
}

var _ ParallelClusterNamespaceInterface = &defaultParallelClusterNamespace{}

type defaultParallelClusterNamespace struct {
	Items     []ClusterInterface
	Namespace string
}

func NewDefaultParallelClusterNamespaceList(Items []ClusterInterface, namespace string) ParallelClusterNamespaceInterface {
	return &defaultParallelClusterNamespace{
		Items:     Items,
		Namespace: namespace,
	}
}

func (clusters *defaultParallelClusterNamespace) CurrentNamespace() string {
	return clusters.Namespace
}

func (clustersNamespace *defaultParallelClusterNamespace) CurrentClusters() ParallelClusterInterface {
	return NewDefaultParallelCluster(clustersNamespace.Items)
}

func (clustersNamespace *defaultParallelClusterNamespace) String() string {
	s := strings.Builder{}
	for i, c := range clustersNamespace.Items {
		s.WriteString(fmt.Sprintf("cluster%d:%s; ", i+1, c.String()))
	}
	return s.String()
}

func (clustersNamespace *defaultParallelClusterNamespace) Parallel(task func(namespaceInterface ClusterNamespaceInterface) parallel.Task) ([]interface{}, error) {

	p := parallel.P("")
	for _, cluster := range clustersNamespace.CurrentClusters().Clusters() {
		var clusterTmp = cluster
		p.Add(task(clusterTmp.Namespace(clustersNamespace.CurrentNamespace())))
	}

	results, err := p.Do().Wait()
	return results, err
}

// K8sInterface operate k8s resource
type K8sInterface interface {
	List(kind string, options metav1.ListOptions, result runtime.Object) (err error)
	Get(kind string, name string, options metav1.GetOptions, result runtime.Object) (err error)
	Delete(kind string, name string, options *metav1.DeleteOptions) error
	DeleteCollection(kind string, options *metav1.DeleteOptions, listOptions metav1.ListOptions) error
	Create(kind string, object runtime.Object, result runtime.Object) (err error)
	Update(kind string, object metav1.Object) (err error)
}

// ClusterInterface operate k8s resource in cluster scope
type ClusterInterface interface {
	K8sInterface

	GetClusterIdentity() string
	GetClusterName() string
	// Namespace return cluster interface that will operate in the namespace
	Namespace(namespace string) ClusterNamespaceInterface

	fmt.Stringer
}

// ClusterNamespaceInterface operate k8s resource in namespaced scope
type ClusterNamespaceInterface interface {
	K8sInterface

	CurrentNamespace() string
	// CurrentCluster return cluster interface that the namespace in
	// if cluster interface is parallel cluster interface, it will return parallel cluster namespace interface
	CurrentCluster() ClusterInterface

	fmt.Stringer
}

// ParallelClusterInterface operate multi k8s resource in cluster scope
type ParallelClusterInterface interface {
	// Parallel will operate in cluster parallel
	Parallel(task func(ClusterInterface) parallel.Task) ([]interface{}, error)
	// Clusters return cluster item array
	Clusters() []ClusterInterface

	AppendCluster(...ClusterInterface)

	// Namespace return cluster interface that will operate in the same namespace accross multi cluster
	Namespace(namespace string) ParallelClusterNamespaceInterface

	fmt.Stringer
}

// ParallelClusterNamespaceInterface operate multi k8s resource in namespaced scope
type ParallelClusterNamespaceInterface interface {
	Parallel(task func(ClusterNamespaceInterface) parallel.Task) ([]interface{}, error)
	CurrentNamespace() string
	// CurrentClusters return multi cluster interface
	CurrentClusters() ParallelClusterInterface
	fmt.Stringer
}
