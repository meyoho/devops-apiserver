package externalversions

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
)

type ConfluenceClient struct {
	DocumentManagementClient
	Client       *http.Client
	devopsClient devopsclient.Interface
}

type ConfluenceSpaceCreateInfo struct {
	Name string `json:"name"`
	ID   int    `json:"id"`
	Key  string `json:"key"`
	Type string `json:"type"`
}

type SpaceResult struct {
	Spaces []Space `json:"results"`
	Start  int     `json:"start"`
	Limit  int     `json:"limit"`
	Size   int     `json:"size"`
}

type Space struct {
	Type       string          `json:"type"`
	ID         int             `json:"id"`
	Key        string          `json:"key"`
	Name       string          `json:"name"`
	Link       SpaceLink       `json:"_links"`
	Expandable SpaceExpandable `json:"_expandable"`
}

type SpaceLink struct {
	Self  string `json:"self"`
	WebUI string `json:"webui"`
}

type SpaceExpandable struct {
	Metadata    string `json:"metadata"`
	Icon        string `json:"icon"`
	Description string `json:"description"`
	Homepage    string `json:"homepage"`
}

func NewConfluenceClient(endpoint string, secret *corev1.Secret, timeoutsecond int) *ConfluenceClient {
	serviceClient := NewDocumentManagementClient(endpoint, secret, timeoutsecond)
	timeout := time.Second * time.Duration(timeoutsecond)
	client := http.DefaultClient
	client.Timeout = timeout

	basicAuth := k8s.GetDataBasicAuthFromSecret(secret)
	devopsClient := NewDevOpsClient("confluence", "", endpoint, devopsclient.NewBasicAuth(basicAuth.Username, basicAuth.Password),
		devopsclient.NewTransport(client.Transport))
	return &ConfluenceClient{
		serviceClient,
		client,
		devopsClient,
	}
}

func (c *ConfluenceClient) SetTransport(trans http.RoundTripper) {
	c.Client.Transport = trans
}

func (c *ConfluenceClient) GetProjects(page, pagesize string) (result *devops.ProjectDataList, err error) {
	return GetProjects(c.devopsClient, page, pagesize)
}

func (c *ConfluenceClient) CreateProject(projectName, projectKey, projectDescription string) (result *devops.ProjectData, err error) {
	return CreateProject(c.devopsClient, projectName, projectDescription, "", projectKey)
}

func (c *ConfluenceClient) CheckAuthentication() (status *devops.HostPortStatus, err error) {
	var (
		resp        *http.Response
		req         *http.Request
		start       = time.Now()
		duration    time.Duration
		lastAttempt = metav1.NewTime(start)
	)
	// prepare
	status = &devops.HostPortStatus{
		LastAttempt: &lastAttempt,
	}
	http.DefaultClient.Timeout = time.Second * 30
	req, err = http.NewRequest(http.MethodGet, c.Endpoint, nil)
	if err != nil {
		return
	}
	if c.BasicAuthInfo.Username != "" && c.BasicAuthInfo.Password != "" {
		req.SetBasicAuth(c.BasicAuthInfo.Username, c.BasicAuthInfo.Password)
	}

	resp, err = http.DefaultClient.Do(req)
	if resp != nil {
		status.StatusCode = int(resp.StatusCode)
		data, err := ioutil.ReadAll(resp.Body)
		if data != nil {
			status.Response = string(data)
		} else {
			status.Response = ""
		}
		if err != nil {
			runtime.HandleError(err)
		}
	}
	duration = time.Since(start)
	status.Delay = &duration
	return
}

func (c *ConfluenceClient) Url(suffix string) string {
	path := fmt.Sprintf("%s/%s", strings.TrimRight(c.Endpoint, "/"),
		strings.TrimLeft(suffix, "/"))
	return path
}

func (c *ConfluenceClient) DoRequest(req *http.Request) (documentManagementResponse *DocumentManagementResponse, err error) {
	glog.V(6).Infof("Request data is: %v", req)
	documentManagementResponse = new(DocumentManagementResponse)
	resp, err := c.Client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return documentManagementResponse, err
	}
	documentManagementResponse.StatusCode = resp.StatusCode
	body, _ := ioutil.ReadAll(resp.Body)
	documentManagementResponse.Body = body
	return documentManagementResponse, nil
}
