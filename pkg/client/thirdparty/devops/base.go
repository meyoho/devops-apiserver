package devops

import (
	corev1 "k8s.io/api/core/v1"
)

type BaseOpts struct {
	// ConfigMap is devops-config ConfigMap
	ConfigMap *corev1.ConfigMap
}
