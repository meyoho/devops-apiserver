package internalversion

import (
	"context"

	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	devopsthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops"
	sonar "alauda.io/devops-apiserver/pkg/client/thirdparty/go-sonarqube"
	"alauda.io/devops-apiserver/pkg/util"
	"alauda.io/devops-apiserver/pkg/util/parallel"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"fmt"
	"net/http"
	"regexp"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	glog "k8s.io/klog"
)

type CodeQualityServiceClient struct {
	Service *devops.CodeQualityTool
	Secret  *corev1.Secret
	Client  *http.Client
	Ctx     context.Context
	devopsthirdparty.BaseOpts
}

func NewCodeQualityServiceClient(roundTripper http.RoundTripper, service *devops.CodeQualityTool, secret *corev1.Secret, opts devopsthirdparty.BaseOpts) *CodeQualityServiceClient {
	httpClient := http.DefaultClient
	if roundTripper != nil {
		httpClient.Transport = roundTripper
	}

	ctx := context.Background()
	httpClient.Timeout = time.Second * 30
	client := CodeQualityServiceClient{
		Service:  service,
		Secret:   secret,
		Client:   httpClient,
		Ctx:      ctx,
		BaseOpts: opts,
	}
	return &client
}

type SonarQubeClient struct {
	*sonar.Client
	codeQualityServiceClient *CodeQualityServiceClient
	devopsClient             devopsclient.Interface
}

func NewSonarQubeClient(c *CodeQualityServiceClient) (*SonarQubeClient, error) {
	basicAuth := k8s.GetDataBasicAuthFromSecret(c.Secret)
	password := basicAuth.Password
	return &SonarQubeClient{
		Client:                   sonar.NewClient(c.Client, c.Service.GetEndpoint(), password),
		codeQualityServiceClient: c,
		devopsClient: NewDevOpsClient("sonarqube", "", c.Service.GetEndpoint(), devopsclient.NewBasicAuth(basicAuth.Password, ""),
			devopsclient.NewTransport(c.Client.Transport)),
	}, nil
}

func (c *SonarQubeClient) String() string {
	return fmt.Sprintf("%s(%s)", c.codeQualityServiceClient.Service.Name, c.BaseURL().Host)
}

func (c *SonarQubeClient) CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.codeQualityServiceClient.Service.GetEndpoint(), nil)
}

func (c *SonarQubeClient) CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	status = &devops.HostPortStatus{
		LastAttempt: lastModifyAt,
	}

	_, valid, err := c.Permissions.CheckAuthentication()
	if err != nil {
		status.StatusCode = http.StatusInternalServerError
		status.Response = err.Error()
		return
	}

	if !valid {
		status.StatusCode = http.StatusUnauthorized
	} else {
		status.StatusCode = http.StatusOK
	}
	status.Response = fmt.Sprintf("Validate authentication result: %v", valid)

	return
}

func (c *SonarQubeClient) GetProjectReport(projectKey string) (conditions []devops.CodeQualityCondition, err error) {
	return GetProjectReport(c.devopsClient, projectKey)
}

// return error if we cannot ensure whether project existed in sonarqube
func (c *SonarQubeClient) CheckProjectExist(projectKey string) (bool, error) {
	return CheckProjectExist(c.devopsClient, projectKey)
}

func (c *SonarQubeClient) GetLastAnalysisDate(projectKey string) (*time.Time, error) {
	return GetLastAnalysisDate(c.devopsClient, projectKey)
}

func (c *SonarQubeClient) GetCorrespondCodeQualityProjects(repositoryList *devops.CodeRepositoryList, binding *devops.CodeQualityBinding) ([]devops.CodeQualityProject, error) {
	return GetCorrespondCodeQualityProjects(c.devopsClient, repositoryList, binding)
}

func (c *SonarQubeClient) getProjects() (sonar.ProjectList, error) {
	glog.V(5).Infof("Getting all projects from sonarqube %s", c.BaseURL().Host)
	const (
		page     = 1
		pageSize = 100
	)

	ctx := context.TODO()
	allList, err := thirdparty.NewListIterator(sonar.PageOpts).Iterate(func(ctx context.Context, opts ...thirdparty.ListOptions) (list thirdparty.List, e error) {
		_, projectList, err := c.Projects.GetProjects(page, pageSize)
		if err != nil {
			return nil, err
		}

		return projectList, nil
	}, ctx)

	if err != nil {
		glog.Errorf("Cannot get projects from sonarqube %s, error: %s", c.BaseURL().Host, err.Error())
		return sonar.ProjectList{}, err
	}

	projects := sonar.ProjectList{}
	for _, item := range allList.GetItems() {
		p, ok := item.(*sonar.Project)
		if !ok {
			continue
		}
		projects.Projects = append(projects.Projects, *p)
	}

	glog.V(5).Infof("Got all projects from sonarqube %s, total projects %d", c.BaseURL().Host, len(projects.Projects))
	return projects, nil
}

var domainWithPathsRegex = regexp.MustCompile("(.*://)?(.*)")

func (c *SonarQubeClient) getCorrespondProjectKey(repository *devops.CodeRepository) string {
	repoUrl := repository.Spec.Repository.HTMLURL
	r := domainWithPathsRegex.FindStringSubmatch(repoUrl)

	domainWithPaths := r[2]
	return strings.Replace(domainWithPaths, "/", "-", -1)
}

func (c *SonarQubeClient) generateCodeQualityProject(project *sonar.Project, repository *devops.CodeRepository, binding *devops.CodeQualityBinding) devops.CodeQualityProject {
	codeQualityProject := devops.CodeQualityProject{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.TypeCodeQualityProject,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      binding.GetName() + "-" + project.Key,
			Namespace: binding.GetNamespace(),
		},
		Spec: devops.CodeQualityProjectSpec{
			CodeQualityBinding: devops.LocalObjectReference{Name: binding.GetName()},
			CodeQualityTool:    devops.LocalObjectReference{Name: binding.Spec.CodeQualityTool.Name},
			CodeRepository:     devops.LocalObjectReference{Name: repository.GetName()},
			Project: devops.CodeQualityProjectInfo{
				CodeAddress: repository.Spec.Repository.FullName,
				ProjectKey:  project.Key,
				ProjectName: project.Name,
			},
		},
	}

	lastAnalysisDate, err := project.GetLastAnalysisDate()
	if err == nil {
		codeQualityProject.Spec.Project.LastAnalysis = &metav1.Time{Time: *lastAnalysisDate}
	}

	glog.Errorf("Error when get analysis date")
	return codeQualityProject
}

func (c *SonarQubeClient) getMetricData(projectKey, branch string) (metric map[string]string, err error) {
	// url: api/measures/component?componentKey&metricKeys&branch, return fields
	glog.V(5).Infof("Get project: %s metric data with branch: %s", projectKey, branch)
	metricKeys := []string{
		"alert_status",
		"code_smells",
		"new_code_smells",
		"sqale_rating",
		"new_maintainability_rating",
		"bugs",
		"new_bugs",
		"reliability_rating",
		"new_reliability_rating",
		"vulnerabilities",
		"new_vulnerabilities",
		"security_rating",
		"new_security_rating",
		"coverage",
		"new_coverage",
		"duplicated_lines_density",
		"new_duplicated_lines_density",
		"ncloc_language_distribution",
	}

	_, projectMetric, err := c.Projects.GetProjectMetrics(projectKey, branch, metricKeys)
	if err != nil {
		glog.Errorf("Failed to get project: %s metrics, err: %s", projectKey, err)
		return
	}

	metric = make(map[string]string, 0)
	for _, m := range projectMetric.Component.Measures {
		key := m.Metric
		value := m.Value
		if value == nil {
			value = &m.Periods[0].Value
		}
		metric[key] = *value
	}
	return
}

func (c *SonarQubeClient) genericReport(projectKey string, branch sonar.Branch) (condition *devops.CodeQualityCondition, err error) {
	condition = &devops.CodeQualityCondition{}
	condition.Name = branch.Name
	condition.Type = devops.TypeCodeQualityReport
	condition.Branch = branch.Name
	condition.IsMain = branch.IsMain

	glog.V(5).Infof("Generic report project: %s, branch: %s", projectKey, branch.Name)
	_, projectQualityGate, err := c.Projects.GetProjectQualityGateWithBranch(projectKey, branch.Name)
	if err != nil {
		return
	}
	glog.V(9).Infof("gateName: %s", projectQualityGate.QualityGate.Name)

	_, basicData, err := c.Projects.GetProjectWithBranch(projectKey, branch.Name)
	if err != nil {
		return
	}
	glog.V(9).Infof("basicData: %v", basicData)

	metric, err := c.getMetricData(projectKey, branch.Name)
	if err != nil {
		return
	}
	glog.V(9).Infof("metricData: %v", metric)

	analyzeTime, err := basicData.GetLastAnalysisDate()
	if err != nil {
		glog.Errorf("project analyze time parse failed: %s", err)
	}
	condition.LastAttempt = &metav1.Time{Time: *analyzeTime}
	condition.Visibility = basicData.Visibility
	condition.QualityGate = projectQualityGate.QualityGate.Name
	condition.Status = metric["alert_status"]

	c.exactAnalysisDataToMetrics(condition, metric)
	glog.V(9).Infof("condition is: %v", condition)
	return
}

func (c *SonarQubeClient) exactAnalysisDataToMetrics(condition *devops.CodeQualityCondition, metric map[string]string) {
	condition.Metrics = make(map[string]devops.CodeQualityAnalyzeMetric)
	condition.Metrics[devops.SonarQubeLanguages] = devops.CodeQualityAnalyzeMetric{
		Name:  devops.SonarQubeLanguages,
		Value: metric["ncloc_language_distribution"],
	}
	condition.Metrics[devops.SonarQubeNewBugs] = devops.CodeQualityAnalyzeMetric{
		Name:  devops.SonarQubeNewBugs,
		Value: metric["new_bugs"],
		Level: c.valueToLevel(metric["new_reliability_rating"]),
	}
	condition.Metrics[devops.SonarQubeNewVulnerabilities] = devops.CodeQualityAnalyzeMetric{
		Name:  devops.SonarQubeNewVulnerabilities,
		Value: metric["new_vulnerabilities"],
		Level: c.valueToLevel(metric["new_security_rating"]),
	}
	condition.Metrics[devops.SonarQubeNewCodeSmells] = devops.CodeQualityAnalyzeMetric{
		Name:  devops.SonarQubeNewCodeSmells,
		Value: metric["new_code_smells"],
		Level: c.valueToLevel(metric["new_maintainability_rating"]),
	}
	condition.Metrics[devops.SonarQubeNewCoverage] = devops.CodeQualityAnalyzeMetric{
		Name: devops.SonarQubeNewCoverage,

		Value: metric["new_coverage"],
	}
	condition.Metrics[devops.SonarQubeNewDuplication] = devops.CodeQualityAnalyzeMetric{
		Name:  devops.SonarQubeNewDuplication,
		Value: metric["new_duplicated_lines_density"],
	}
	// old metric
	condition.Metrics[devops.SonarQubeBugs] = devops.CodeQualityAnalyzeMetric{
		Name:  devops.SonarQubeBugs,
		Value: metric["bugs"],
		Level: c.valueToLevel(metric["reliability_rating"]),
	}
	condition.Metrics[devops.SonarQubeVulnerabilities] = devops.CodeQualityAnalyzeMetric{
		Name:  devops.SonarQubeVulnerabilities,
		Value: metric["vulnerabilities"],
		Level: c.valueToLevel(metric["security_rating"]),
	}
	condition.Metrics[devops.SonarQubeCodeSmells] = devops.CodeQualityAnalyzeMetric{
		Name:  devops.SonarQubeCodeSmells,
		Value: metric["code_smells"],
		Level: c.valueToLevel(metric["sqale_rating"]),
	}
	condition.Metrics[devops.SonarQubeCoverage] = devops.CodeQualityAnalyzeMetric{
		Name:  devops.SonarQubeCoverage,
		Value: metric["coverage"],
	}
	condition.Metrics[devops.SonarQubeDuplication] = devops.CodeQualityAnalyzeMetric{
		Name:  devops.SonarQubeDuplication,
		Value: metric["duplicated_lines_density"],
	}
}

func (c *SonarQubeClient) valueToLevel(key string) (level string) {
	levelMap := map[string]string{
		"1.0": "A",
		"2.0": "B",
		"3.0": "C",
		"4.0": "D",
		"5.0": "E",
	}
	if level, ok := levelMap[key]; ok {
		return level
	}
	return ""
}

func (c *SonarQubeClient) GetRolesMapping(listOptions *devops.RoleMappingListOptions) (roleMapping devops.RoleMapping, err error) {
	glog.Infof("Begining GetRolesMapping, options:%#v , sonar:%s", listOptions, c.codeQualityServiceClient.Service.GetEndpoint())
	defer func() {
		if err != nil {
			glog.Errorf("Ended GetRolesMapping, err:%#v,  sonar:%s", err.Error(), c.codeQualityServiceClient.Service.GetEndpoint())
		} else {
			glog.Infof("Ended GetRolesMapping, sonar:%s", c.codeQualityServiceClient.Service.GetEndpoint())
		}
	}()

	var platform *SonarPlatform
	platform, err = NewSonarPlatformFromConfigMap(c.codeQualityServiceClient.ConfigMap)
	if err != nil {
		glog.Errorf("Error NewSonarPlatform from configmap, configMap:%#v", c.codeQualityServiceClient.ConfigMap)
		return devops.RoleMapping{}, err
	}

	groups := c.newSonarUsergroupService()

	bindingNamespaces := listOptions.Projects

	groupNames := platform.UsergroupNames(bindingNamespaces)
	var usersList sonar.UserList
	usersList, err = groups.getAllUsersInMultiGroup(groupNames)
	if err != nil {
		return devops.RoleMapping{}, err
	}

	roleMapping, err = platform.UserListToRoleMapping(listOptions.Projects, usersList)
	if err != nil {
		glog.Errorf("convert userslist to roleMapping error: %s, listOptins.Projects:%#v, uersList:%#v", err.Error(), listOptions.Projects, usersList)
		return devops.RoleMapping{}, err
	}

	return roleMapping, nil
}

func (c *SonarQubeClient) ApplyRoleMapping(roleMapping *devops.RoleMapping) (err error) {
	glog.Infof("Begining ApplyRoleMapping, sonar:%s", c.codeQualityServiceClient.Service.GetEndpoint())
	glog.V(9).Infof("roleMapping:%#v , sonar:%s", roleMapping, c.codeQualityServiceClient.Service.GetEndpoint())
	defer func() {
		if err != nil {
			glog.Errorf("Ended ApplyRoleMapping, err:%#v,  sonar:%s", err.Error(), c.codeQualityServiceClient.Service.GetEndpoint())
		} else {
			glog.Infof("Ended ApplyRoleMapping, sonar:%s", c.codeQualityServiceClient.Service.GetEndpoint())
		}
	}()

	var platform *SonarPlatform
	platform, err = NewSonarPlatformFromConfigMap(c.codeQualityServiceClient.ConfigMap)
	if err != nil {
		glog.Errorf("Error NewSonarPlatform from configmap, configMap:%#v", c.codeQualityServiceClient.ConfigMap)
		return
	}

	sonarProjectsPermissions := platform.RoleMappingToSonarProjectPermissions(roleMapping)

	// Prepare to create all users's group
	// sonar has some bugs when creating user group in parallelly, so just do it one by one :(
	err = c.prepareSonarUserGroups(sonarProjectsPermissions)
	if err != nil {
		return err
	}

	// Set user to it's sonar group
	sonarUserListOps := platform.RoleMappingToSonarUserList(roleMapping)
	errGroup := c.newSonarUsergroupService().setUsers(sonarUserListOps)

	// Set all sonar projects permissions
	errProjects := c.newSonarPermissionsService().setMultiProjects(sonarProjectsPermissions)

	if errGroup != nil {
		err = errGroup
		return
	}

	if errProjects != nil {
		err = errProjects
		return
	}

	return nil
}

func (c *SonarQubeClient) newSonarPermissionsService() *sonarPermissionsService {
	return &sonarPermissionsService{
		Client: c.Client,
	}
}

func (c *SonarQubeClient) newSonarUsergroupService() *sonarUsergroupService {
	return &sonarUsergroupService{
		Client: c.Client,
	}
}

func isInSonarGroups(groupList *sonar.GroupList, groupName string) bool {
	if groupList == nil {
		return false
	}

	if len(groupList.Groups) == 0 {
		return false
	}

	for _, group := range groupList.Groups {
		if group.GetName() == groupName {
			return true
		}
	}

	return false
}

func (c *SonarQubeClient) prepareSonarUserGroups(projectsPermissions map[string]*[]SonarGroupPermissions) error {
	const pageSize = 1000

	if len(projectsPermissions) == 0 {
		return nil
	}

	_, alreadyUserGroups, err := c.Groups.Search(sonar.SearchGroupRequest{
		PageOptions: sonar.PageOptions{
			PageSize: pageSize,
		},
	})

	if err != nil {
		glog.Errorf("Search all user groups error:%s, sonar:%s", err.Error(), c.BaseURL())
		return err
	}

	errs := util.MultiErrors{}
	// do it in synchronization to avoid sonar bugs of creating user group
	for _, groupPermissions := range projectsPermissions {
		for _, group := range *groupPermissions {

			if isInSonarGroups(alreadyUserGroups, group.GroupName) {
				glog.V(7).Infof("Sonar user group %s is already exists, sonar:%s", group.GroupName, c.BaseURL())
				continue
			}

			glog.V(7).Infof("Creating Sonar user group %s, sonar:%s", group.GroupName, c.BaseURL())
			// not exists in sonar, we will create it
			_, err := c.Groups.Create(sonar.CreateGroupRequest{
				GroupName: group.GroupName,
			})

			if err == nil {
				glog.Infof("Created Sonar user group %s, sonar:%s", group.GroupName, c.BaseURL())
				continue
			}

			if err != nil && sonar.IsGroupAlreadyExistError(err) {
				glog.V(7).Infof("Sonar user group %s is already exists, sonar:%s", group.GroupName, c.BaseURL())
				continue
			}

			glog.Errorf("try to create group error: %s, sonar:%s", err.Error(), c.BaseURL())
			errs = append(errs, err)
		}
	}

	if len(errs) == 0 {
		return nil
	}

	return &errs
}

// sonarPermissionsService provides some parallely func to operate Sonar Permissions
type sonarPermissionsService struct {
	*sonar.Client
}

func (c *sonarPermissionsService) setOneGroup(projectKey string, groupPermissions SonarGroupPermissions) error {

	errs := util.MultiErrors{}
	glog.V(7).Infof("will set sonar project:%s to permission:%#v, sonar:%s", projectKey, groupPermissions, c.BaseURL())
	for _, permission := range groupPermissions.Permissions {
		var err error

		var op = c.Client.Permissions.AddGroup
		if !permission.Allow {
			op = c.Client.Permissions.RemoveGroup
		}

		_, err = op(sonar.AddGroupPermissionRequest{
			GroupName:  groupPermissions.GroupName,
			ProjectKey: projectKey,
			Permission: string(permission.Key),
		})

		if err != nil {
			errs = append(errs, err)
			if sonar.IsGroupNotExistError(err) {
				break // there is no need to set other permissions
			}
		}
	}

	if len(errs) == 0 {
		glog.V(5).Infof("Setted sonar project '%s' permissions, permissions:%#v, sonar:%s", projectKey, groupPermissions, c.BaseURL().Host)
		return nil
	}
	glog.Errorf("Error set sonar project '%s' permissions, error:%s, permissions:%#v, sonar:%s", projectKey, errs.Error(), groupPermissions, c.BaseURL().Host)
	return &errs
}

func (c *sonarPermissionsService) setProject(projectKey string, groupPermissions *[]SonarGroupPermissions) error {
	errs := util.MultiErrors{}
	for _, groupPermission := range *groupPermissions {
		err := c.setOneGroup(projectKey, groupPermission)
		if err != nil {
			errs = append(errs, err)
		}
	}

	if len(errs) == 0 {
		return nil
	}

	return &errs
}

func (c *sonarPermissionsService) setMultiProjects(projectsPermissions map[string]*[]SonarGroupPermissions) error {
	p := parallel.P("SetMultiSonarProjectPermissions")

	glog.V(5).Infof("Setting sonar projects permissions, sonar: %s", c.BaseURL().Host)
	for projectKey, groupsPermissions := range projectsPermissions {

		p.Add(func(key string, groupPermissions *[]SonarGroupPermissions) parallel.Task {

			return func() (i interface{}, e error) {
				err := c.setProject(key, groupPermissions)
				return nil, err
			}

		}(projectKey, groupsPermissions))

	}

	_, errs := p.SetConcurrent(5).Do().Wait()
	if errs != nil {
		glog.V(5).Infof("Error Setted sonar projects permissions,  errors:%s , projectPermissions:%#v, sonar: %s", errs.Error(), projectsPermissions, c.BaseURL().Host)
		return errs
	}

	glog.V(5).Infof("Setted sonar projects permissions, sonar: %s", c.BaseURL().Host)
	return nil
}

// sonarUsergroupService provides some parallely func to operate Sonar Usergroup
type sonarUsergroupService struct {
	*sonar.Client
}

// setUser will add user into a group or remove a user from a group
func (groupService *sonarUsergroupService) setUsers(userListOps []SonarUserOperation) error {

	glog.V(3).Infof("Setting users to Sonar Usergroups")
	p := parallel.P("SetSonarUsersToGroups")

	for _, userOps := range userListOps {
		p.Add(func(userOps SonarUserOperation) parallel.Task {

			return func() (i interface{}, e error) {
				err := groupService.setUser(userOps)
				return nil, err
			}

		}(userOps))
	}

	_, err := p.SetConcurrent(10).Do().Wait()

	if err != nil {
		glog.Errorf("Error Setted users to Sonar Usergroups, err:%s, sonar:%s", err.Error(), groupService.BaseURL().Host)
		return err
	}

	glog.V(5).Infof("Setted users to Sonar Usergroups success, sonar:%s", groupService.BaseURL().Host)
	return nil
}

// setUser will add a user into a group or remove a user from a group
func (s *sonarUsergroupService) setUser(userOps SonarUserOperation) (err error) {
	if len(userOps.User.GroupNames) == 0 {
		glog.Infof("Skip set user to sonar group, UserOps:%#v not contains groupname, sonar:%s", userOps, s.BaseURL().Host)
		return
	}

	groupName := userOps.User.GroupNames[0]

	maxRetry := 2

FORLOOP:
	for i := 0; i < maxRetry; i = i + 1 {
		switch userOps.Operation {
		case devops.UserRoleOperationAdd, devops.UserRoleOperationUpdate:
			glog.V(5).Infof("Adding user %s to sonar group '%s', sonar:%s", userOps.User.Name, groupName, s.BaseURL().Host)
			_, err = s.Groups.AddUser(sonar.AddUserToGroupRequest{
				GroupName: groupName,
				Login:     userOps.User.Name,
			})
		case devops.UserRoleOperationRemove:
			glog.V(5).Infof("Removing user %s in sonar group '%s', sonar:%s", userOps.User.Name, groupName, s.BaseURL().Host)
			_, err = s.Groups.RemoveUser(sonar.RemoveUserInGroupRequest{
				GroupName: groupName,
				Login:     userOps.User.Name,
			})
		default:
			err = fmt.Errorf("Skip set User '%s' in sonar group '%s', operation is not recognized, useroperation:%#v,  sonar:%s", userOps.User.Name, groupName, userOps, s.BaseURL().Host)
			glog.Error(err.Error())
		}

		if err == nil {
			break
		}
		if sonar.IsGroupNotExistError(err) {
			glog.Errorf("Sonar group '%s' is not exists when create user %s, will ignore it, next loop should be created, err:%s ", groupName, userOps.User.Name, err.Error())
			// we should not create group here, it will be faced at bugs of sonar
			// _, err = s.Groups.Create(sonar.CreateGroupRequest{
			// 	GroupName: groupName,
			// })
			continue
		}

		if sonar.IsUserNotExistError(err) {
			glog.Infof("Skip set User '%s' in sonar group '%s', the user is not exists, err:%s ", userOps.User.Name, groupName, err.Error())
			break FORLOOP
		}

		glog.Errorf("Added/Removing user %s to sonar group '%s', error:%s, sonar:%s", userOps.User.Name, groupName, err.Error(), s.BaseURL().Host)
		return err
	}

	glog.V(5).Infof("Added/Removing user %s to sonar group '%s' success, sonar:%s", userOps.User.Name, groupName, s.BaseURL().Host)
	return nil
}

// getAllUsersInMultiGroup will get users in groups parallely
func (s *sonarUsergroupService) getAllUsersInMultiGroup(groupsNames []string) (sonar.UserList, error) {
	glog.V(7).Infof("Getting users in groups:%#v", groupsNames)

	tasks := parallel.P("GetSonarGroupUsers")
	for _, groupName := range groupsNames {
		// we must care about the variable referenced by Closure
		var groupNameTmp = groupName
		tasks.Add(func() (i interface{}, e error) {
			return s.getAllUsers(groupNameTmp)
		})
	}

	userListArray, err := tasks.Do().Wait()
	if err != nil {
		return sonar.UserList{}, err
	}

	result := sonar.UserList{Users: []sonar.User{}}
	for _, item := range userListArray {
		users := item.(sonar.UserList)
		result.Users = append(result.Users, users.Users...)
	}

	result.Paging.PageIndex = 1
	result.Paging.Total = len(result.Users)
	result.Paging.PageSize = len(result.Users)
	glog.V(7).Infof("Got all users in groups:%#v, result:%#v", groupsNames, result)
	return result, nil
}

// getAllUsers will get all users according pagged info that responsed by server
func (s *sonarUsergroupService) getAllUsers(groupName string) (sonar.UserList, error) {
	const (
		page     = 1
		pageSize = 50
	)

	ctx := context.TODO()
	glog.V(7).Infof("Getting sonarqube Users by group:%s, sonar:%s", groupName, s.BaseURL().Host)
	allList, err := thirdparty.NewListIterator(sonar.PageOpts).Iterate(func(ctx context.Context, opts ...thirdparty.ListOptions) (list thirdparty.List, e error) {
		_, userList, err := s.Groups.GetUsers(sonar.GetUsersRequest{GroupName: groupName}, page, pageSize)
		if err != nil {
			if sonar.IsGroupNotExistError(err) {
				glog.Infof("WARNING, sonar usergroup '%s' is not exist, will return empty user list", groupName)
				return &sonar.UserList{}, nil
			}
			return nil, err
		}

		return &userList, nil
	}, ctx)

	if err != nil {
		glog.Errorf("List sonarqube users in group '%s' error, sonarqube:%s, error:%s", groupName, s.BaseURL().Host, err.Error())
		return sonar.UserList{}, err
	}

	users := sonar.UserList{}
	for _, item := range allList.GetItems() {
		u, ok := item.(*sonar.User)
		if !ok {
			continue
		}
		users.Users = append(users.Users, *u)
	}

	glog.V(7).Infof("Got sonarqube Users by group:%s, users:%#v,  sonar:%s", groupName, users, s.BaseURL().Host)

	return users, nil
}
