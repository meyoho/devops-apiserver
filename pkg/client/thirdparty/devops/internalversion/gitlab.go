package internalversion

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/generic"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"alauda.io/devops-apiserver/pkg/util/parallel"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"github.com/dustin/go-humanize"
	"github.com/xanzy/go-gitlab"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

var rootGroupID *int = nil

type GitlabClient struct {
	*ServiceClient
	Client       *gitlab.Client
	devopsClient devopsclient.Interface
}

func NewGitlabClient(serviceClient *ServiceClient, roundTripper http.RoundTripper) (*GitlabClient, error) {
	var (
		client = &gitlab.Client{}
		err    error
		token  string

		devopsClient devopsclient.Interface
	)

	if serviceClient.isBasicAuth() {
		client = gitlab.NewClient(newHTTPCient(roundTripper), serviceClient.getPassword())
		token = serviceClient.getPassword()

		devopsClient = NewDevOpsClient("gitlab", "", serviceClient.getHost(),
			devopsclient.NewAPIKey("PRIVATE-TOKEN", "header", token),
			devopsclient.NewTransport(roundTripper))
	} else {
		client = gitlab.NewOAuthClient(newHTTPCient(roundTripper), serviceClient.OAuth2Info.AccessToken)
		token = serviceClient.OAuth2Info.AccessToken
		serviceClient.OAuth2Info.AccessTokenKey = k8s.GitlabAccessTokenKey
		serviceClient.OAuth2Info.Scope = k8s.GitlabAccessTokenScope

		devopsClient = NewDevOpsClient("gitlab", "", serviceClient.getHost(),
			devopsclient.NewBearerToken(token),
			devopsclient.NewTransport(roundTripper))
	}

	err = client.SetBaseURL(serviceClient.getHost())
	if err != nil {
		return nil, err
	}
	return &GitlabClient{ServiceClient: serviceClient, Client: client, devopsClient: devopsClient}, nil
}

func newHTTPCient(roundTripper http.RoundTripper) *http.Client {

	if roundTripper == nil {
		return DefaultClient()
	}

	client := &http.Client{
		Transport: roundTripper,
	}
	SetClient(client)
	return client
}

func (c *GitlabClient) CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getHost(), nil)
}

func (c *GitlabClient) CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	url := fmt.Sprintf("%suser", c.getBaseUrl())
	return CheckService(url, c.getHeader(lastModifyAt))
}

func (c *GitlabClient) GetLatestRepoCommit(repoID, owner, repoName, repoFullName string) (commit *devops.RepositoryCommit, status *devops.HostPortStatus) {
	return GetLatestRepoCommit(c.devopsClient, repoID, owner, repoName, repoFullName)
}

func (c *GitlabClient) getAllRepos() (repositories []*gitlab.Project, err error) {
	var (
		wg = sync.WaitGroup{}
	)

	glog.V(5).Infof("fetch data in page %d", 1)
	firstPageResult, response, err := c.Client.Projects.ListProjects(
		&gitlab.ListProjectsOptions{
			Statistics: gitlab.Bool(true),
			ListOptions: gitlab.ListOptions{
				Page:    1,
				PerPage: k8s.PerPage,
			},
		})
	if err != nil {
		glog.Errorf("error list project in gitlab: %v", err)
		return
	}

	if firstPageResult != nil {
		repositories = append(repositories, firstPageResult...)
	}

	if response == nil || response.TotalPages < 2 {
		glog.V(5).Infof("No next page, return current result")
		return
	}

	for page := 2; page < response.TotalPages+1; page++ {
		glog.V(5).Infof("fetch data in page %d", page)
		wg.Add(1)
		go func(page, perPage int) {
			defer wg.Done()

			pagedResult, _, err := c.Client.Projects.ListProjects(
				&gitlab.ListProjectsOptions{
					Statistics: gitlab.Bool(true),
					ListOptions: gitlab.ListOptions{
						Page:    page,
						PerPage: k8s.PerPage,
					},
				})
			if err != nil {
				return
			}
			if pagedResult != nil {
				repositories = append(repositories, pagedResult...)
			}
		}(page, k8s.PerPage)
	}
	wg.Wait()

	return
}

func (c *GitlabClient) GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error) {
	return GetRemoteRepos(c.devopsClient)
}

func (c *GitlabClient) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if gitlabRepo, ok := remoteRepo.(*gitlab.Project); ok {
		codeRepo = devops.OriginCodeRepository{
			CodeRepoServiceType: devops.CodeRepoServiceTypeGitlab,
			ID:                  strconv.Itoa(gitlabRepo.ID),
			Name:                gitlabRepo.Name,
			FullName:            gitlabRepo.PathWithNamespace,
			Description:         gitlabRepo.Description,
			HTMLURL:             gitlabRepo.WebURL,
			CloneURL:            gitlabRepo.HTTPURLToRepo,
			SSHURL:              gitlabRepo.SSHURLToRepo,
			Language:            "",
			Owner: devops.OwnerInRepository{
				ID:   strconv.Itoa(gitlabRepo.Namespace.ID),
				Name: gitlabRepo.Namespace.FullPath,
				Type: devops.GetOwnerType(gitlabRepo.Namespace.Kind),
			},

			CreatedAt: ConvertToMetaTime(gitlabRepo.CreatedAt),
			// LastActivityAt on gitlab will reflect of pushedAt
			PushedAt:  ConvertToMetaTime(gitlabRepo.LastActivityAt),
			UpdatedAt: ConvertToMetaTime(gitlabRepo.LastActivityAt),

			Private:      c.isPrivate(gitlabRepo.Visibility),
			Size:         int64(getRepoSize(gitlabRepo) * 1000),
			SizeHumanize: humanize.Bytes(uint64(getRepoSize(gitlabRepo))),
		}
	}

	return
}

func (c *GitlabClient) getRepoUrl(repoID string) string {
	return fmt.Sprintf("%sprojects/%s", c.getBaseUrl(), repoID)
}

func (c *GitlabClient) getHeader(lastModifyAt *metav1.Time) (header map[string]string) {
	header = c.ServiceClient.getHeader(lastModifyAt)
	if c.isBasicAuth() {
		header[k8s.GitlabPrivateTokenKey] = c.getPassword()
	}
	return
}

func (c *GitlabClient) getOwnerAvatarURL(project *gitlab.Project) (ownerAvatarURL string) {
	if project == nil || project.Owner == nil {
		return
	}

	return project.Owner.AvatarURL
}

func (c *GitlabClient) getOwnerWebURL(project *gitlab.Project) (ownerAvatarURL string) {
	if project == nil || project.Owner == nil {
		return
	}

	return project.Owner.WebsiteURL
}

func (c *GitlabClient) isPrivate(value gitlab.VisibilityValue) bool {
	return value != gitlab.PublicVisibility
}

func (c *GitlabClient) GetAuthorizeUrl(redirectUrl string) string {
	return fmt.Sprintf("%s/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		strings.TrimRight(c.getHtml(), "/"), c.getClientID(), redirectUrl, c.getScope())
}

func (c *GitlabClient) getAccessTokenUrl() string {
	return fmt.Sprintf("%s/oauth/token", strings.TrimRight(c.getHtml(), "/"))
}

func (c *GitlabClient) AccessToken(redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessTokenUrl()
		response       = &AccessTokenResponseGitlab{}
	)

	return c.accessToken(response, redirectUrl, accessTokenUrl)
}

func (c *GitlabClient) getBaseUrl() string {
	return c.Client.BaseURL().String()
}

// CreateProject is abstract of create group and sub group for gitlab
func (c *GitlabClient) CreateProject(options devops.CreateProjectOptions) (*devops.ProjectData, error) {
	// return c.createGroupsAndSubGroups(options)
	return CreateCodeRepoProject(c.devopsClient, options)
}

// ListProjectDataList will list all groups or subgroups
func (c *GitlabClient) ListProjectDataList(options devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	return ListCodeRepoProjects(c.devopsClient, options)
}

func (c *GitlabClient) createGroupsAndSubGroups(options devops.CreateProjectOptions) (*devops.ProjectData, error) {

	var groupPath = options.Name
	groupName, subGroupName, err := splitGroup(groupPath)
	if err != nil {
		return nil, err
	}

	gitlabGroupData, err := c.getGitlabGroup(groupName)
	if err != nil {
		return nil, err
	}

	if gitlabGroupData != nil && subGroupName == "" {
		glog.Errorf("Gitlab group '%s' is alreay exists", groupName)
		return nil, errors.NewAlreadyExists(corev1.Resource("ProjectData"), groupName)
	}

	// not exists , we will create it
	if gitlabGroupData == nil {

		gitlabCreateGroupOptions := buildGitLabCreateGroupOptions(groupName, rootGroupID, options)
		glog.V(5).Infof("Creating gitlab group '%s', options:%#v", groupName, gitlabCreateGroupOptions)
		gitlabGroupData, err = c.createGitLabGroup(gitlabCreateGroupOptions)
		if err != nil {
			glog.V(5).Infof("Created gitlab group '%s' error:%s", groupName, sprintErrorResponse(err))
			return nil, err
		}
		glog.V(5).Infof("Created gitlab group '%s' ", groupName)
	}

	if subGroupName == "" {
		return gitlabGroupAsProjectData(*gitlabGroupData)
	}

	// sub group name is not empty, we should try to create it
	gitlabSubGroupData, err := c.getGitlabGroup(fmt.Sprintf("%s/%s", groupName, subGroupName))
	if err != nil {
		return nil, err
	}
	if gitlabSubGroupData != nil {
		glog.Errorf("Gitlab group '%s/%s' is alreay exists", groupName, subGroupName)
		return nil, errors.NewAlreadyExists(corev1.Resource("ProjectData"), fmt.Sprintf("%s/%s", groupName, subGroupName))
	}
	gitlabCreateGroupOptions := buildGitLabCreateGroupOptions(subGroupName, &gitlabGroupData.ID, options)
	glog.V(5).Infof("Creating gitlab group %s/%s, options:%#v", groupName, subGroupName, gitlabCreateGroupOptions)
	gitlabSubGroupData, err = c.createGitLabGroup(gitlabCreateGroupOptions)
	if err != nil {
		return nil, err
	}
	glog.V(5).Infof("Created gitlab group %s/%s", groupName, subGroupName)

	return gitlabGroupAsProjectData(*gitlabSubGroupData)
}

func getRepoSize(project *gitlab.Project) int64 {
	if project == nil || project.Statistics == nil {
		return 0
	}
	return project.Statistics.RepositorySize
}

func buildGitLabCreateGroupOptions(groupName string, parentGroupID *int, options devops.CreateProjectOptions) *gitlab.CreateGroupOptions {
	var groupDescription = ""
	gitlabCreateGroupOptions := &gitlab.CreateGroupOptions{
		Name:        &groupName,
		Path:        &groupName,
		Description: &groupDescription,
		ParentID:    parentGroupID,
	}
	glog.V(9).Infof("build gitlab create group options, name:%s, path:%s, parentID:%d", groupName, groupName, parentGroupID)
	return gitlabCreateGroupOptions
}

func gitlabGroupAsProjectData(gitlabGroup gitlab.Group) (*devops.ProjectData, error) {
	data, err := generic.MarshalToMapString(gitlabGroup)
	if err != nil {
		glog.Errorf("mashall gitlab group data %#v error:%#v", gitlabGroup, err)
		return nil, err
	}

	var projectData = &devops.ProjectData{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.ResourceKindProjectData,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: gitlabGroup.FullPath,
			Annotations: map[string]string{
				"avatarURL":   gitlabGroup.AvatarURL,
				"webURL":      gitlabGroup.WebURL,
				"description": gitlabGroup.Description,
				"type":        string(devops.OriginCodeRepoRoleTypeOrg),
			},
		},
		Data: data,
	}
	return projectData, nil
}

func splitGroup(path string) (groupName, subGroupName string, err error) {
	if path == "" {
		return "", "", fmt.Errorf("error format of path, it cannot be empty")
	}

	segments := strings.Split(path, "/")
	if len(segments) > 2 {
		err := fmt.Errorf("error format of group path '%s', segments should not be more than 2", path)
		glog.Error(err)
		return "", "", err
	}

	groupName = segments[0]
	if len(segments) == 2 {
		subGroupName = segments[1]
	}
	return
}

// getGitlabGroup will get group from gitlab server, it will return nil when group is not exists.
func (c *GitlabClient) getGitlabGroup(gitLabGroupFullName string) (*gitlab.Group, error) {
	gitlabGroup, response, err := c.Client.Groups.GetGroup(gitLabGroupFullName)

	if err != nil {
		if isGitLabGroupNotFoudError(err) {
			glog.V(7).Infof("Group '%s' is not exists", gitLabGroupFullName)
			return nil, nil
		}
		glog.Errorf("Get group '%s' error:%s", gitLabGroupFullName, sprintErrorResponse(err))
		return nil, err
	}
	if gitlabGroup == nil {
		glog.Errorf("Get group '%s' error, response is:%#v", gitLabGroupFullName, response.Response)
		return nil, errors.NewInternalError(fmt.Errorf("Error to get gitlab group %s", gitLabGroupFullName))
	}

	return gitlabGroup, nil
}

func (c *GitlabClient) createGitLabGroup(createOptions *gitlab.CreateGroupOptions) (*gitlab.Group, error) {
	gitlabgroup, response, err := c.Client.Groups.CreateGroup(createOptions)
	if err != nil {
		glog.Errorf("Create gitlab group '%s' error, options %#v, error:%s", *(createOptions.Name), createOptions, sprintErrorResponse(err))
		return nil, err
	}
	if gitlabgroup == nil {
		err := fmt.Errorf("Create gitlab group '%s' got unknow error", *(createOptions.Name))
		glog.Errorf("%s, options %#v, response:%#v", err.Error(), createOptions, response)
		return nil, errors.NewInternalError(err)
	}
	return gitlabgroup, nil
}

func (c *GitlabClient) listGroupsAndSubGroups(options devops.ListProjectOptions) (*devops.ProjectDataList, error) {

	gitlabListGroupOptions, err := buildGitlabListGroupOptions(options)
	if err != nil {
		return nil, err
	}

	gitlabGroups, err := c.listGitlabGroups(gitlabListGroupOptions)
	if err != nil {
		return nil, err
	}

	return gitlabGroupsAsProjectDataList(gitlabGroups)
}

func (c *GitlabClient) listGitlabGroups(options *gitlab.ListGroupsOptions) ([]*gitlab.Group, error) {
	gitlabGroups, response, err := c.Client.Groups.ListGroups(options)
	if err != nil {
		glog.Errorf("list gitlab groups error, gitlab listGroupOptions:%#v, error:%s", options, sprintErrorResponse(err))
		return []*gitlab.Group{}, err
	}
	if gitlabGroups == nil {
		err := fmt.Errorf("list gitlab groups got unknow error")
		glog.Errorf("%s, options %#v, response:%#v", err.Error(), options, response)
		return nil, errors.NewInternalError(err)
	}
	return gitlabGroups, nil
}

func buildGitlabListGroupOptions(options devops.ListProjectOptions) (*gitlab.ListGroupsOptions, error) {
	var gitlabListOptions = &gitlab.ListGroupsOptions{
		Search: &options.Filter,
	}
	return gitlabListOptions, nil
}

func gitlabGroupsAsProjectDataList(groups []*gitlab.Group) (*devops.ProjectDataList, error) {
	items := []devops.ProjectData{}

	for _, group := range groups {
		projectData, err := gitlabGroupAsProjectData(*group)
		if err != nil {
			glog.Errorf("")
			return nil, err
		}

		items = append(items, *projectData)
	}

	return &devops.ProjectDataList{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.ResourceKindProjectDataList,
		},
		Items: items,
	}, nil
}

func sprintErrorResponse(errorResponse error) string {
	if errorResponse == nil {
		return ""
	}

	if errResp, ok := errorResponse.(*gitlab.ErrorResponse); ok {
		return fmt.Sprintf("ErrorResponse:%s, origin response body:%s", errResp.Error(), string(errResp.Body))
	}
	return errorResponse.Error()
}

func isGitLabGroupNotFoudError(errorResponse error) bool {
	if errorResponse == nil {
		return false
	}
	if errResp, ok := errorResponse.(*gitlab.ErrorResponse); ok {
		if errResp.Response.StatusCode == http.StatusNotFound && strings.Contains(errResp.Message, "Group") {
			return true
		}
	}
	return false
}

func (c *GitlabClient) currentUser() (*devops.ProjectData, error) {
	user, _, err := c.Client.Users.CurrentUser()
	if err != nil {
		return nil, err
	}

	return gitlabUserAsProjectData(*user), nil
}

func gitlabUserAsProjectData(gitlabUser gitlab.User) *devops.ProjectData {

	var projectData = &devops.ProjectData{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.ResourceKindProjectData,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: gitlabUser.Username,
			Annotations: map[string]string{
				devops.AnnotationsProjectDataAvatarURL:   gitlabUser.AvatarURL,
				devops.AnnotationsProjectDataAccessPath:  "/" + gitlabUser.Username,
				devops.AnnotationsProjectDataDescription: "",
				devops.AnnotationsProjectDataType:        string(devops.OriginCodeRepoOwnerTypeUser),
			},
		},
		Data: map[string]string{
			"username": gitlabUser.Username,
			"id":       fmt.Sprint(gitlabUser.ID),
			"name":     gitlabUser.Name,
			"company":  gitlabUser.Organization,
			"email":    gitlabUser.Email,
		},
	}
	return projectData
}

func (gitlabClient *GitlabClient) GetRolesMapping(options *devops.RoleMappingListOptions) (roleMapping devops.RoleMapping, err error) {
	glog.Infof("Begining GetRolesMapping, options:%#v , gitlab:%s", options, gitlabClient.Service.GetHostPort().Host)
	defer func() {
		if err != nil {
			glog.Errorf("Ended GetRolesMapping, err:%#v,  gitlab:%s", err.Error(), gitlabClient.Service.GetHostPort().Host)
		} else {
			glog.Infof("Ended GetRolesMapping, gitlab:%s", gitlabClient.Service.GetHostPort().Host)
		}
	}()

	p := parallel.P("GetGitLabGroupMembers").SetConcurrent(3)

	for _, group := range options.Projects {
		p.Add(func(groupName string) parallel.Task {

			return func() (i interface{}, e error) {
				return gitlabClient.getGroupRolesMapping(groupName)
			}

		}(group))
	}

	var results []interface{}
	results, err = p.Do().Wait()
	if err != nil {
		return devops.RoleMapping{}, err
	}

	items := make([]devops.ProjectUserRoleOperation, 0, len(results))
	for _, item := range results {
		project := item.(devops.ProjectUserRoleOperation)
		items = append(items, project)
	}

	return devops.RoleMapping{Spec: items}, nil
}

func (gitlabClient *GitlabClient) getGroupRolesMapping(groupName string) (devops.ProjectUserRoleOperation, error) {
	members, _, err := gitlabClient.Client.Groups.ListGroupMembers(groupName, nil)
	if err != nil {
		return devops.ProjectUserRoleOperation{}, err
	}

	emailsMap := gitlabClient.getGroupMemebersEmails(members)

	userRoleOperations := make([]devops.UserRoleOperation, 0, len(members))
	for _, member := range members {

		item := devops.UserRoleOperation{
			User: devops.UserMeta{
				Username: member.Username,
				Email:    emailsMap[member.ID], // Don't be deceived by `member.Email` in gitlab client, gitlab api wont return email of user by api of list groups member.
			},
			Role: devops.RoleMeta{
				Name: gitlabAccessLevelToRoleName(member.AccessLevel),
			},
		}

		userRoleOperations = append(userRoleOperations, item)
	}

	return devops.ProjectUserRoleOperation{
		Project: devops.ProjectData{
			ObjectMeta: metav1.ObjectMeta{
				Name: groupName,
			},
		},
		UserRoleOperations: userRoleOperations,
	}, nil
}

// getGroupMemebersEmails will try to get email of user, if got errors, it won't return errors, just return empty email
func (gitlabClient *GitlabClient) getGroupMemebersEmails(members []*gitlab.GroupMember) (userIdMapEmail map[int]string) {

	p := parallel.P("GetGitlabUserEmail").SetConcurrent(3)

	for _, item := range members {

		p.Add(func(member gitlab.GroupMember) parallel.Task {

			return func() (i interface{}, e error) {
				user, _, err := gitlabClient.Client.Users.GetUser(member.ID)
				if err != nil {
					glog.Errorf("Skip get email, because error get gitlab user:%s, userid=%d, err=%s", member.Username, member.ID, err.Error())
					return nil, err
				}
				return user, nil
			}

		}(*item))
	}

	users, _ := p.Do().Wait() //ignore errors

	userIdMapEmail = make(map[int]string, len(users))
	for _, item := range users {
		user := item.(*gitlab.User)
		userIdMapEmail[user.ID] = user.Email
	}

	return
}

func (gitlabClient *GitlabClient) ApplyRoleMapping(roleMapping *devops.RoleMapping) (err error) {
	logPrefix := "ApplyRoleMapping"
	begin := time.Now()

	glog.Infof("[%s] Beginning ApplyRoleMapping, gitlab:%s", logPrefix, gitlabClient.Service.GetHostPort().Host)
	glog.V(9).Infof("[%s] roleMapping:%#v , gitlab:%s", logPrefix, roleMapping, gitlabClient.Service.GetHostPort().Host)

	defer func() {
		if err != nil {
			glog.Errorf("[%s]Ended ApplyRoleMapping, err:%#v,  gitlab:%s, elapsed %fs", logPrefix, err.Error(), gitlabClient.Service.GetHostPort().Host, time.Now().Sub(begin).Seconds())
		} else {
			glog.Infof("[%s] Ended ApplyRoleMapping, gitlab:%s, elapsed %fs", logPrefix, gitlabClient.Service.GetHostPort().Host, time.Now().Sub(begin).Seconds())
		}
	}()

	p := parallel.P("GitlabApplyRoleMapping").SetConcurrent(1)

	for _, project := range roleMapping.Spec {
		for _, item := range project.UserRoleOperations {
			p.Add(func(groupName string, userRole devops.UserRoleOperation) parallel.Task {

				return func() (i interface{}, e error) {
					e = gitlabClient.assignUserInGitlabGroup(groupName, userRole)
					return
				}

			}(project.Project.Name, item))
		}
	}

	_, err = p.Do().Wait()
	if err != nil {
		return err
	}

	return nil
}

// getGitlabUserId will get gitlab user by gitlab username , it will return user=nil, error=nil when username is not found on gitlab server
func (gitlabClient *GitlabClient) getGitlabUser(username string) (*gitlab.User, error) {
	users, _, err := gitlabClient.Client.Users.ListUsers(&gitlab.ListUsersOptions{
		Username: &username,
	})
	if err != nil {
		glog.Errorf("Error list users by username:%s, error:%s,  gitlab=%s", username, err, gitlabClient.Service.GetHostPort().Host)
		return nil, err
	}

	var targetUser *gitlab.User
	for _, user := range users {
		if user.Username == username {
			targetUser = user
			break
		}
	}

	if targetUser == nil {
		glog.Errorf("Cannot find Gitlab User %s, it is not exists on remote server, gitlab:%s", username, gitlabClient.Service.GetHostPort().Host)
		return nil, nil
	}

	return targetUser, nil
}

func (gitlabClient *GitlabClient) assignUserInGitlabGroup(groupName string, userRole devops.UserRoleOperation) error {
	switch userRole.Operation {
	case devops.UserRoleOperationAdd:
		return gitlabClient.addUserToGitlabGroup(groupName, userRole)
	case devops.UserRoleOperationUpdate:
		return gitlabClient.updateUserInGitlabGroup(groupName, userRole)
	case devops.UserRoleOperationRemove:
		return gitlabClient.removeUserInGitlabGroup(groupName, userRole)
	default:
		glog.Errorf("Unknown user role operation:%s, username=%s, group=%s, gitlab=%s", userRole.Operation, userRole.User.Name, groupName, gitlabClient.Service.GetHostPort().Host)
		return errors.NewInternalError(fmt.Errorf("Unknow user role operation:%s", userRole.Operation))
	}
}

func (gitlabClient *GitlabClient) removeUserInGitlabGroup(groupName string, userRole devops.UserRoleOperation) error {

	targetUser, err := gitlabClient.getGitlabUser(userRole.User.Username)
	if err != nil {
		return err
	}

	if targetUser == nil {
		glog.V(5).Infof("Skip remove gitlab group member %s, it is not exists on gitlab server, group=%s, gitlab=%s", userRole.User.Username, groupName, gitlabClient.Service.GetHostPort().Host)
		return nil
	}

	glog.V(5).Infof("Removing gitlab group member %s, group=%s, gitlab=%s", userRole.User.Username, groupName, gitlabClient.Service.GetHostPort().Host)
	resp, err := gitlabClient.Client.GroupMembers.RemoveGroupMember(groupName, targetUser.ID)
	if err != nil {
		glog.Errorf("Error remove gitlab group member %s, error=%s, group=%s, gitlab=%s", userRole.User.Username, err.Error(), groupName, gitlabClient.Service.GetHostPort().Host)
		if resp != nil && resp.StatusCode == http.StatusNotFound {
			glog.V(5).Infof("Skip remove gitlab group member %s, group=%s, gitlab=%s", userRole.User.Username, groupName, gitlabClient.Service.GetHostPort().Host)
			return nil
		}
		return err
	}

	glog.V(3).Infof("Removed gitlab group member %s, group=%s, gitlab=%s", userRole.User.Username, groupName, gitlabClient.Service.GetHostPort().Host)
	return nil
}

func (gitlabClient *GitlabClient) updateUserInGitlabGroup(groupName string, userRole devops.UserRoleOperation) error {
	accessLevel := roleNameToGitlabAccessLevel(userRole.Role.Name)

	targetUser, err := gitlabClient.getGitlabUser(userRole.User.Username)
	if err != nil {
		return err
	}

	if targetUser == nil {
		glog.V(5).Infof("Skip Gitlab User %s to update to group:%s, it is not exists on remote server, gitlab:%s", userRole.User.Username, groupName, gitlabClient.Service.GetHostPort().Host)
		return nil
	}

	glog.V(5).Infof("Updating Group member %s, accessLevel=%v, group=%s, gitlab=%s", targetUser.Username, accessLevel, groupName, gitlabClient.Service.GetHostPort().Host)
	_, _, err = gitlabClient.Client.GroupMembers.EditGroupMember(groupName, targetUser.ID, &gitlab.EditGroupMemberOptions{
		AccessLevel: &accessLevel,
	})

	if err != nil {
		glog.Errorf("Error Update Group member %s, error:%s,  accessLevel=%v, group=%s, gitlab=%s",
			targetUser.Username, err.Error(), accessLevel, groupName, gitlabClient.Service.GetHostPort().Host)
		return err
	}

	glog.V(3).Infof("Updated Group member %s, accessLevel=%v, group=%s, gitlab=%s",
		targetUser.Username, accessLevel, groupName, gitlabClient.Service.GetHostPort().Host)
	return nil
}

func (gitlabClient *GitlabClient) addUserToGitlabGroup(groupName string, userRole devops.UserRoleOperation) error {
	accessLevel := roleNameToGitlabAccessLevel(userRole.Role.Name)

	targetUser, err := gitlabClient.getGitlabUser(userRole.User.Username)
	if err != nil {
		return err
	}

	if targetUser == nil {
		glog.V(5).Infof("Skip Gitlab User %s, it is not exists on remote server, gitlab:%s", userRole.User.Username, gitlabClient.Service.GetHostPort().Host)
		return nil
	}

	glog.V(5).Infof("Adding Group member %s, accessLevel=%v, group=%s, gitlab=%s", targetUser.Username, accessLevel, groupName, gitlabClient.Service.GetHostPort().Host)
	_, _, err = gitlabClient.Client.GroupMembers.AddGroupMember(groupName, &gitlab.AddGroupMemberOptions{
		UserID:      &targetUser.ID,
		AccessLevel: &accessLevel,
	})

	if err != nil {
		if IsGitlabMemberAlreadyExistError(err) {
			glog.V(5).Infof("Skip gitlab user %s, it is already in gitlab group %s, err=%s, gitlab=%s",
				targetUser.Username, groupName, err.Error(), gitlabClient.Service.GetHostPort().Host)
			return nil
		}

		glog.Errorf("Error Add Group member %s, error:%s,  accessLevel=%v, group=%s, gitlab=%s",
			targetUser.Username, err.Error(), accessLevel, groupName, gitlabClient.Service.GetHostPort().Host)
		return err
	}

	glog.V(3).Infof("Added Group member %s, accessLevel=%v, group=%s, gitlab=%s",
		targetUser.Username, accessLevel, groupName, gitlabClient.Service.GetHostPort().Host)
	return nil
}

var GitLabAccessLevelMap = map[gitlab.AccessLevelValue]string{
	gitlab.NoPermissions:        "No",
	gitlab.GuestPermissions:     "Guest",
	gitlab.ReporterPermissions:  "Reporter",
	gitlab.DeveloperPermissions: "Developer",
	gitlab.MasterPermissions:    "Master",
	gitlab.OwnerPermission:      "Owner",
}

func gitlabAccessLevelToRoleName(level gitlab.AccessLevelValue) string {

	if name, ok := GitLabAccessLevelMap[level]; ok {
		return name
	}
	return GitLabAccessLevelMap[gitlab.NoPermissions]
}

func roleNameToGitlabAccessLevel(roleName string) gitlab.AccessLevelValue {

	levelMap := overturnMap(GitLabAccessLevelMap)

	if level, ok := levelMap[roleName]; ok {
		return level
	}

	glog.Errorf("Cannot find Gitlab AccessLevel by roleName: %s, will set AccessLevel to NoPermissions, avaiable rolename: %#v", roleName, GitLabAccessLevelMap)

	return gitlab.NoPermissions
}

func overturnMap(accessLevelMap map[gitlab.AccessLevelValue]string) map[string]gitlab.AccessLevelValue {
	result := map[string]gitlab.AccessLevelValue{}

	for key, val := range accessLevelMap {
		result[val] = key
	}

	return result
}

func IsGitlabMemberAlreadyExistError(err error) bool {
	if _, ok := err.(*gitlab.ErrorResponse); !ok {
		return false
	}

	resErr := err.(*gitlab.ErrorResponse)
	if resErr.Response.StatusCode != http.StatusConflict {
		return false
	}

	if strings.Contains(resErr.Message, "Member already exists") {
		return true
	}

	return false
}

const (
	MaxGitLabBranchPerPage = 100
)

func (c *GitlabClient) GetBranches(owner, repoName, repoFullName string) ([]devops.CodeRepoBranch, error) {
	return GetBranches(c.devopsClient, owner, repoName, repoFullName)
}

func (c *GitlabClient) listBranches(owner, repo string) ([]*gitlab.Branch, error) {
	var branches []*gitlab.Branch
	hasNextPage := true
	// per_page is at most 100
	for page := 1; hasNextPage; page++ {
		options := &gitlab.ListBranchesOptions{Page: page, PerPage: MaxGitLabBranchPerPage}
		items, response, err := c.Client.Branches.ListBranches(fmt.Sprintf("%s/%s", owner, repo), options)
		if err != nil {
			return []*gitlab.Branch{}, err
		}
		if items == nil {
			err := fmt.Errorf("list gitlab branch got unknow error")
			return []*gitlab.Branch{}, errors.NewInternalError(err)
		}
		if response.NextPage <= page {
			hasNextPage = false
		}
		branches = append(branches, items...)
	}
	return branches, nil
}
