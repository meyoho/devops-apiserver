package internalversion

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	glog "k8s.io/klog"
)

const (
	JiraType  = "Jira"
	TaigaType = "Taiga"
)

type ProjectManagementResponse struct {
	StatusCode int
	Body       []byte
}

type ProjectManagementClient struct {
	Endpoint   string
	Client     *http.Client
	Cxt        context.Context
	Username   string
	Credential string
}

type Project struct {
	Expand         string `json:"expand"`
	Self           string `json:"self"`
	ID             string `json:"id"`
	Key            string `json:"key"`
	Name           string `json:"name"`
	ProjectTypeKey string `json:"projectTypeKey"`
	AvatarUrls     string `json:"avatarUrls"`
	Description    string `json:"description"`
}

type ProjectCreateInfo struct {
	Self string `json:"self"`
	ID   int    `json:"id"`
	Key  string `json:"key"`
}
type UserInfo struct {
	Name         string `json:"name"`
	EmailAddress string `json:"emailAddress"`
	Key          string `json:"key"`
}

type project struct {
	ID   string `json:"id"`
	Key  string `json:"key"`
	Name string `json:"name"`
}

type issueoption struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type permissionscheme struct {
	Name string `json:"name"`
	ID   int    `json:"id"`
}

type roleactors struct {
	Name   string  `json:"name"`
	ID     int     `json:"id"`
	Actors []actor `json:"actors"`
}

type actor struct {
	ID          int    `json:"id"`
	DisplayName string `json:"displayname"`
	Name        string `json:"name"`
}

func NewProjectManagementClient(roundTripper http.RoundTripper, endpoint, username, credential string, timeoutsecond int) ProjectManagementClient {

	httpClient := http.DefaultClient
	if roundTripper != nil {
		httpClient.Transport = roundTripper
	}

	timeout := time.Second * time.Duration(timeoutsecond)
	httpClient.Timeout = timeout
	ctx := context.Background()
	httpClient.Timeout = timeout
	client := ProjectManagementClient{
		endpoint,
		httpClient,
		ctx,
		username,
		credential,
	}
	return client
}

func (c *ProjectManagementClient) RequestProjectManagement(suffix string, clientType string) (resp *ProjectManagementResponse, err error) {
	var (
		req *http.Request
	)
	path := c.Url(suffix)
	req, err1 := http.NewRequest(http.MethodGet, path, nil)
	if err1 != nil {
		err = err1
		return
	}
	if clientType == JiraType {
		if c.Username != "" && c.Credential != "" {
			glog.V(6).Infof("ProjectmangementClient username: %s, Credential: %s", c.Username, c.Credential)
			req.SetBasicAuth(c.Username, c.Credential)
		}
	} else if clientType == TaigaType {
		if c.Username != "" && c.Credential != "" {
			_, Token, err := c.GetTaigaUserIDBearerToken()
			if err != nil {
				return nil, fmt.Errorf("Get Taiga User Token err, Error: %v", err)
			}
			req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", Token))
		}
	}

	resp, err = c.DoRequest(req)

	switch resp.StatusCode {
	case http.StatusOK:
		glog.V(6).Infof("Get %s is successful", suffix)
		return resp, nil
	case http.StatusUnauthorized:
		glog.V(4).Infof("Get projectmanagement response return 401 error, response is: %v", resp)
		return resp, nil
	default:
		return resp, err
	}
}

func (c *ProjectManagementClient) GetTaigaUserIDBearerToken() (UserId int, Token string, err error) {
	path := c.Url("/api/v1/auth")
	data := map[string]string{}
	data["username"] = c.Username
	data["password"] = c.Credential
	data["type"] = "normal"
	bodyJson, _ := json.Marshal(data)
	req, err := http.NewRequest(http.MethodPost, path, bytes.NewBuffer(bodyJson))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		glog.Errorf("Error happend when create Taiga request, Endpoint is %s", path)
		return
	}
	resp, err := c.DoRequest(req)
	if err != nil {
		return 0, "", err
	}
	if resp.StatusCode != 200 {
		return 0, "", fmt.Errorf("Get User Token err , StatusCode is %d, and body is %v", resp.StatusCode, string(resp.Body))
	}
	taigauser := new(TaigaUser)
	json.Unmarshal([]byte(resp.Body), &taigauser)
	Token = taigauser.Token
	UserId = taigauser.ID
	return UserId, Token, nil

}

func (c *ProjectManagementClient) Url(suffix string) string {
	path := fmt.Sprintf("%s/%s", strings.TrimRight(c.Endpoint, "/"),
		strings.TrimLeft(suffix, "/"))
	return path
}

func (c *ProjectManagementClient) DoRequest(req *http.Request) (projectmanagementResponse *ProjectManagementResponse, err error) {
	glog.V(6).Infof("Request data is: %v", req)
	projectmanagementResponse = new(ProjectManagementResponse)
	resp, err := c.Client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return projectmanagementResponse, err
	}
	projectmanagementResponse.StatusCode = resp.StatusCode
	body, _ := ioutil.ReadAll(resp.Body)
	projectmanagementResponse.Body = body
	return projectmanagementResponse, nil
}
