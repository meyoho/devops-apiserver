package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http"
)

var searchissueslist = `
{
    "expand": "names,schema",
    "startAt": 0,
    "maxResults": 4,
    "total": 1,
    "issues": [
        {
            "expand": "operations,versionedRepresentations,editmeta,changelog,renderedFields",
            "id": "10000",
            "self": "http://127.0.0.1:30000/rest/api/2/issue/10000",
            "key": "TEST-1",
            "fields": {
                "summary": "不错吧还",
                "issuetype": {
                    "self": "http://127.0.0.1:30000/rest/api/2/issuetype/10003",
                    "id": "10003",
                    "description": "A task that needs to be done.",
                    "iconUrl": "http://127.0.0.1:30000/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype",
                    "name": "Task",
                    "subtask": false,
                    "avatarId": 10318
                },
                "creator": {
                    "self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
                    "name": "admin",
                    "key": "admin",
                    "emailAddress": "yuzp1996@qq.com",
                    "avatarUrls": {
                        "48x48": "http://127.0.0.1:30000/secure/useravatar?avatarId=10334",
                        "24x24": "http://127.0.0.1:30000/secure/useravatar?size=small&avatarId=10334",
                        "16x16": "http://127.0.0.1:30000/secure/useravatar?size=xsmall&avatarId=10334",
                        "32x32": "http://127.0.0.1:30000/secure/useravatar?size=medium&avatarId=10334"
                    },
                    "displayName": "yuzp1996@qq.com",
                    "active": true,
                    "timeZone": "GMT"
                },
                "created": "2019-09-09T05:57:04.000+0000",
                "description": "description",
                "project": {
                    "self": "http://127.0.0.1:30000/rest/api/2/project/10000",
                    "id": "10000",
                    "key": "TEST",
                    "name": "TEST",
                    "projectTypeKey": "business",
                    "avatarUrls": {
                        "48x48": "http://127.0.0.1:30000/secure/projectavatar?avatarId=10324",
                        "24x24": "http://127.0.0.1:30000/secure/projectavatar?size=small&avatarId=10324",
                        "16x16": "http://127.0.0.1:30000/secure/projectavatar?size=xsmall&avatarId=10324",
                        "32x32": "http://127.0.0.1:30000/secure/projectavatar?size=medium&avatarId=10324"
                    }
                },
                "assignee": {
                    "self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
                    "name": "admin",
                    "key": "admin",
                    "emailAddress": "yuzp1996@qq.com",
                    "avatarUrls": {
                        "48x48": "http://127.0.0.1:30000/secure/useravatar?avatarId=10334",
                        "24x24": "http://127.0.0.1:30000/secure/useravatar?size=small&avatarId=10334",
                        "16x16": "http://127.0.0.1:30000/secure/useravatar?size=xsmall&avatarId=10334",
                        "32x32": "http://127.0.0.1:30000/secure/useravatar?size=medium&avatarId=10334"
                    },
                    "displayName": "yuzp1996@qq.com",
                    "active": true,
                    "timeZone": "GMT"
                },
                "priority": {
                    "self": "http://127.0.0.1:30000/rest/api/2/priority/3",
                    "iconUrl": "http://127.0.0.1:30000/images/icons/priorities/medium.svg",
                    "name": "Medium",
                    "id": "3"
                },
                "updated": "2019-09-09T05:59:25.000+0000",
                "status": {
                    "self": "http://127.0.0.1:30000/rest/api/2/status/3",
                    "description": "此问题正在被经办人积极处理。",
                    "iconUrl": "http://127.0.0.1:30000/images/icons/statuses/inprogress.png",
                    "name": "处理中",
                    "id": "3",
                    "statusCategory": {
                        "self": "http://127.0.0.1:30000/rest/api/2/statuscategory/4",
                        "id": 4,
                        "key": "indeterminate",
                        "colorName": "yellow",
                        "name": "处理中"
                    }
                }
            }
        }
    ]
}
`

var issuedetailJson = `
{
    "expand": "renderedFields,names,schema,operations,editmeta,changelog,versionedRepresentations",
    "id": "10000",
    "self": "http://127.0.0.1:30000/rest/api/2/issue/10000",
    "key": "PROJECT16-1",
    "fields": {
        "issuetype": {
            "self": "http://127.0.0.1:30000/rest/api/2/issuetype/10003",
            "id": "10003",
            "description": "A task that needs to be done.",
            "iconUrl": "http://127.0.0.1:30000/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype",
            "name": "Task",
            "subtask": false,
            "avatarId": 10318
        },
        "creator": {
            "self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
            "name": "admin",
            "key": "JIRAUSER10000",
            "emailAddress": "yuzp1996@qq.com",
            "avatarUrls": {
                "48x48": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=48",
                "24x24": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=24",
                "16x16": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=16",
                "32x32": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=32"
            },
            "displayName": "yuzp1996@qq.com",
            "active": true,
            "timeZone": "GMT"
        },
        "created": "2019-09-17T03:56:15.534+0000",
        "description": "good issue  I Like it ",
        "project": {
            "self": "http://127.0.0.1:30000/rest/api/2/project/10001",
            "id": "10001",
            "key": "PROJECT16",
            "name": "PROJECT16",
            "projectTypeKey": "business",
            "avatarUrls": {
                "48x48": "http://127.0.0.1:30000/secure/projectavatar?avatarId=10324",
                "24x24": "http://127.0.0.1:30000/secure/projectavatar?size=small&avatarId=10324",
                "16x16": "http://127.0.0.1:30000/secure/projectavatar?size=xsmall&avatarId=10324",
                "32x32": "http://127.0.0.1:30000/secure/projectavatar?size=medium&avatarId=10324"
            }
        },
        "comment": {
            "comments": [
                {
                    "self": "http://127.0.0.1:30000/rest/api/2/issue/10000/comment/10000",
                    "id": "10000",
                    "author": {
                        "self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
                        "name": "admin",
                        "key": "JIRAUSER10000",
                        "emailAddress": "yuzp1996@qq.com",
                        "avatarUrls": {
                            "48x48": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=48",
                            "24x24": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=24",
                            "16x16": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=16",
                            "32x32": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=32"
                        },
                        "displayName": "yuzp1996@qq.com",
                        "active": true,
                        "timeZone": "GMT"
                    },
                    "body": "test  gogogo  good",
                    "updateAuthor": {
                        "self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
                        "name": "admin",
                        "key": "JIRAUSER10000",
                        "emailAddress": "yuzp1996@qq.com",
                        "avatarUrls": {
                            "48x48": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=48",
                            "24x24": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=24",
                            "16x16": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=16",
                            "32x32": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=32"
                        },
                        "displayName": "yuzp1996@qq.com",
                        "active": true,
                        "timeZone": "GMT"
                    },
                    "created": "2019-09-17T04:00:17.836+0000",
                    "updated": "2019-09-17T04:00:17.836+0000"
                }
            ],
            "maxResults": 1,
            "total": 1,
            "startAt": 0
        },
        "issuelinks": [
            {
                "id": "10000",
                "self": "http://127.0.0.1:30000/rest/api/2/issueLink/10000",
                "type": {
                    "id": "10000",
                    "name": "Blocks",
                    "inward": "is blocked by",
                    "outward": "blocks",
                    "self": "http://127.0.0.1:30000/rest/api/2/issueLinkType/10000"
                },
                "outwardIssue": {
                    "id": "10001",
                    "key": "PROJECT16-2",
                    "self": "http://127.0.0.1:30000/rest/api/2/issue/10001",
                    "fields": {
                        "summary": "TEST2",
                        "status": {
                            "self": "http://127.0.0.1:30000/rest/api/2/status/10000",
                            "description": "",
                            "iconUrl": "http://127.0.0.1:30000/images/icons/status_generic.gif",
                            "name": "To Do",
                            "id": "10000",
                            "statusCategory": {
                                "self": "http://127.0.0.1:30000/rest/api/2/statuscategory/2",
                                "id": 2,
                                "key": "new",
                                "colorName": "blue-gray",
                                "name": "To Do"
                            }
                        },
                        "priority": {
                            "self": "http://127.0.0.1:30000/rest/api/2/priority/3",
                            "iconUrl": "http://127.0.0.1:30000/images/icons/priorities/medium.svg",
                            "name": "Medium",
                            "id": "3"
                        },
                        "issuetype": {
                            "self": "http://127.0.0.1:30000/rest/api/2/issuetype/10003",
                            "id": "10003",
                            "description": "A task that needs to be done.",
                            "iconUrl": "http://127.0.0.1:30000/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype",
                            "name": "Task",
                            "subtask": false,
                            "avatarId": 10318
                        }
                    }
                }
            }
        ],
        "assignee": {
            "self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
            "name": "admin",
            "key": "JIRAUSER10000",
            "emailAddress": "yuzp1996@qq.com",
            "avatarUrls": {
                "48x48": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=48",
                "24x24": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=24",
                "16x16": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=16",
                "32x32": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=32"
            },
            "displayName": "yuzp1996@qq.com",
            "active": true,
            "timeZone": "GMT"
        },
        "priority": {
            "self": "http://127.0.0.1:30000/rest/api/2/priority/3",
            "iconUrl": "http://127.0.0.1:30000/images/icons/priorities/medium.svg",
            "name": "Medium",
            "id": "3"
        },
        "updated": "2019-09-17T04:03:16.139+0000",
        "status": {
            "self": "http://127.0.0.1:30000/rest/api/2/status/3",
            "description": "This issue is being actively worked on at the moment by the assignee.",
            "iconUrl": "http://127.0.0.1:30000/images/icons/statuses/inprogress.png",
            "name": "In Progress",
            "id": "3",
            "statusCategory": {
                "self": "http://127.0.0.1:30000/rest/api/2/statuscategory/4",
                "id": 4,
                "key": "indeterminate",
                "colorName": "yellow",
                "name": "In Progress"
            }
        }
    }
}
`

var statusResponseJson = `
[
    {
        "self": "http://localhost:8090/jira/rest/api/2.0/status/10000",
        "description": "The issue is currently being worked on.",
        "iconUrl": "http://localhost:8090/jira/images/icons/progress.gif",
        "name": "In Progress",
        "id": "10000",
        "statusCategory": {
            "self": "http://localhost:8090/jira/rest/api/2.0/statuscategory/1",
            "id": 1,
            "key": "in-flight",
            "colorName": "yellow",
            "name": "In Progress"
        }
    },
    {
        "self": "http://localhost:8090/jira/rest/api/2.0/status/5",
        "description": "The issue is closed.",
        "iconUrl": "http://localhost:8090/jira/images/icons/closed.gif",
        "name": "Closed",
        "id": "5",
        "statusCategory": {
            "self": "http://localhost:8090/jira/rest/api/2.0/statuscategory/9",
            "id": 9,
            "key": "completed",
            "colorName": "green"
        }
    }
]
`

var priorityResponseJson = `
[
    {
        "self": "http://127.0.0.1:30000/rest/api/2/priority/1",
        "statusColor": "#ff7452",
        "description": "This problem will block progress.",
        "iconUrl": "http://127.0.0.1:30000/images/icons/priorities/highest.svg",
        "name": "Highest",
        "id": "1"
    },
    {
        "self": "http://127.0.0.1:30000/rest/api/2/priority/2",
        "statusColor": "#ff8f73",
        "description": "Serious problem that could block progress.",
        "iconUrl": "http://127.0.0.1:30000/images/icons/priorities/high.svg",
        "name": "High",
        "id": "2"
    },
    {
        "self": "http://127.0.0.1:30000/rest/api/2/priority/3",
        "statusColor": "#ffab00",
        "description": "Has the potential to affect progress.",
        "iconUrl": "http://127.0.0.1:30000/images/icons/priorities/medium.svg",
        "name": "Medium",
        "id": "3"
    },
    {
        "self": "http://127.0.0.1:30000/rest/api/2/priority/4",
        "statusColor": "#0065ff",
        "description": "Minor problem or easily worked around.",
        "iconUrl": "http://127.0.0.1:30000/images/icons/priorities/low.svg",
        "name": "Low",
        "id": "4"
    },
    {
        "self": "http://127.0.0.1:30000/rest/api/2/priority/5",
        "statusColor": "#2684ff",
        "description": "Trivial problem with little or no impact on progress.",
        "iconUrl": "http://127.0.0.1:30000/images/icons/priorities/lowest.svg",
        "name": "Lowest",
        "id": "5"
    }
]
`

var issuetypeResponseJson = `
[
    {
        "self": "http://localhost:8090/jira/rest/api/2.0/issueType/3",
        "id": "3",
        "description": "A task that needs to be done.",
        "iconUrl": "http://localhost:8090/jira/images/icons/issuetypes/task.png",
        "name": "Task",
        "subtask": false,
        "avatarId": 1
    },
    {
        "self": "http://localhost:8090/jira/rest/api/2.0/issueType/1",
        "id": "1",
        "description": "A problem with the software.",
        "iconUrl": "http://localhost:8090/jira/images/icons/issuetypes/bug.png",
        "name": "Bug",
        "subtask": false,
        "avatarId": 10002
    }
]
`

var _ = Describe("Projectmanagement work", func() {

	var (
		ctl            *gomock.Controller
		roundTripper   *mhttp.MockRoundTripper
		jiraClient     *JiraClient
		filterdatalist *devops.IssueFilterDataList
	)

	BeforeEach(func() {
		ctl = gomock.NewController(GinkgoT())

		roundTripper = mhttp.NewMockRoundTripper(ctl)
		jiraClient = NewJiraClient(roundTripper, "http://127.0.0.1:30000", "admin", "123456", 30)

	})

	Context("Jira client can work well", func() {
		It("Jira client get type option", func() {

			liststatusRequest, _ := http.NewRequest(http.MethodGet, "/rest/api/2/status", nil)
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(liststatusRequest)).Return(newHttpResponse(http.StatusOK, statusResponseJson), nil)

			listPriorityRequest, _ := http.NewRequest(http.MethodGet, "/rest/api/2/priority", nil)
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listPriorityRequest)).Return(newHttpResponse(http.StatusOK, priorityResponseJson), nil)

			listIssuetypeRequest, _ := http.NewRequest(http.MethodGet, "/rest/api/2/issuetype", nil)
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listIssuetypeRequest)).Return(newHttpResponse(http.StatusOK, issuetypeResponseJson), nil)

			filterdatalist, _ = jiraClient.GetIssueOptionValue("all")

			Expect(filterdatalist).ToNot(BeNil())
			Expect(len(filterdatalist.Status)).To(Equal(2))
			Expect(filterdatalist.Status).To(Equal([]devops.IssueFilterData{
				{
					Name: "In Progress",
					ID:   "10000",
					Data: nil,
				},
				{
					Name: "Closed",
					ID:   "5",
					Data: nil,
				},
			}))
		})

		It("Jira get issuelist", func() {
			postdata := Postdata{
				Jql:        "summary ~ '错吧还' and issuekey = TEST-1 and status = 3 and type = task and project = TEST",
				StartAt:    1,
				MaxResults: 4,
				Fields: []string{
					"created",
					"priority",
					"status",
					"project",
					"updated",
					"creator",
					"assignee",
					"summary",
				},
				Expand: []string{},
			}
			bodyJson, _ := json.Marshal(postdata)

			listissuesRequest, _ := http.NewRequest(http.MethodPost, "/rest/api/2/search", bytes.NewBuffer(bodyJson))
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listissuesRequest)).Return(newHttpResponse(http.StatusOK, searchissueslist), nil)

			listoption := devopsclient.ListIssuesOptions{
				Page:     1,
				PageSize: 4,
				Type:     "task",
				Priority: "Medium",
				Status:   "3",
				Summary:  "错吧还",
				IssueKey: "TEST-1",
				OrderBy:  "updated",
				Sort:     "DESC",
				Projects: []string{"TEST"},
			}
			issuelist, _ := jiraClient.GetIssueList(&listoption)
			Expect(issuelist).NotTo(BeNil())
			Expect(len(issuelist.Items)).To(Equal(1))
			Expect(issuelist.Items[0].Description).To(Equal("description"))
			Expect(issuelist.Items[0].Key).To(Equal("TEST-1"))

		})

		It("Jira get issue detail", func() {
			issueKey := "PROJECT16-1"

			url := fmt.Sprintf("/rest/api/2/issue/%s?fields=issuetype,project,summary,creator,priority,comment,issuelinks,assignee,updated,status,created,description", issueKey)
			issueRequest, _ := http.NewRequest(http.MethodGet, url, nil)
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(issueRequest)).Return(newHttpResponse(http.StatusOK, issuedetailJson), nil)

			issuedetail, _ := jiraClient.GetIssue(issueKey)
			Expect(issuedetail).NotTo(BeNil())
			Expect(issuedetail.IssueLinks[0].Summary).To(Equal("TEST2"))

		})

	})
})
