package internalversion

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	devopsthirdparty "alauda.io/devops-apiserver/pkg/client/thirdparty/devops"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	jenkinsmodle "bitbucket.org/mathildetech/devops-client/pkg/jenkins/v1/models"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

type ThirdParty struct {
	CodeRepoServiceClientFactory         CodeRepoServiceClientFactory
	ImageRegistryClientFactory           ImageRegistryClientFactory
	ProjectManagementClientFactory       ProjectManagementClientFactory
	DocumentManagementClientFactory      DocumentManagementClientFactory
	CodeQualityClientFactory             CodeQualityClientFactory
	ArtifactRegistryManagerClientFactory ArtifactRegistryManagerClientFactory
}

func NewThirdParty(codeRepoServiceClientFactory CodeRepoServiceClientFactory, imageRegistryClientFactory ImageRegistryClientFactory, projectManagementClientFactory ProjectManagementClientFactory, documentManagementClientFactory DocumentManagementClientFactory, codeQualityClientFactory CodeQualityClientFactory) ThirdParty {
	return ThirdParty{
		CodeRepoServiceClientFactory:    codeRepoServiceClientFactory,
		ImageRegistryClientFactory:      imageRegistryClientFactory,
		ProjectManagementClientFactory:  projectManagementClientFactory,
		DocumentManagementClientFactory: documentManagementClientFactory,
		CodeQualityClientFactory:        codeQualityClientFactory,
	}
}

type CodeRepoServiceClientFactory interface {
	GetCodeRepoServiceClient(service *devops.CodeRepoService, secret *corev1.Secret) (CodeRepoServiceClientInterface, error)
}

type CodeRepoServiceClientInterface interface {
	GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error)
	ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository)
	CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error)
	CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error)
	GetLatestRepoCommit(repoID, owner, repoName, repoFullName string) (commit *devops.RepositoryCommit, status *devops.HostPortStatus)
	AccessToken(redirectUrl string) (secret *corev1.Secret, err error)
	GetAuthorizeUrl(redirectUrl string) string
	GetBranches(owner, repoName, repoFullName string) (branches []devops.CodeRepoBranch, err error)

	CreateProject(options devops.CreateProjectOptions) (*devops.ProjectData, error)
	ListProjectDataList(options devops.ListProjectOptions) (*devops.ProjectDataList, error)

	GetRolesMapping(*devops.RoleMappingListOptions) (devops.RoleMapping, error)
	ApplyRoleMapping(roleMapping *devops.RoleMapping) (err error)
}

type AccessTokenResponseInterface interface {
	ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details
	GetAccessToken() string
}

var DefaultCodeRepoServiceClientFactory = NewCodeRepoServiceClientFactory(nil)

// NewCodeRepoServiceClientFactory if set roundTripper to nil, will use default roundtripper
func NewCodeRepoServiceClientFactory(roundTripper http.RoundTripper) CodeRepoServiceClientFactory {
	return codeRepoServiceClientFactory{
		RoundTripper: roundTripper,
	}
}

type codeRepoServiceClientFactory struct {
	RoundTripper http.RoundTripper
}

func (f codeRepoServiceClientFactory) GetCodeRepoServiceClient(service *devops.CodeRepoService, secret *corev1.Secret) (CodeRepoServiceClientInterface, error) {
	if service == nil {
		return nil, fmt.Errorf("CodeRepoService must not be empty")
	}
	var (
		err           error
		client        CodeRepoServiceClientInterface
		serviceClient = NewServiceClient(service, secret)
	)
	serviceClient.SetTransport(f.RoundTripper)
	switch service.Spec.Type {
	case devops.CodeRepoServiceTypeGithub:
		client, err = NewGithubClient(serviceClient, f.RoundTripper)
	case devops.CodeRepoServiceTypeGitlab:
		client, err = NewGitlabClient(serviceClient, f.RoundTripper)
	case devops.CodeRepoServiceTypeGitee:
		client, err = NewGiteeClient(serviceClient, f.RoundTripper)
	case devops.CodeRepoServiceTypeBitbucket:
		client, err = NewBitbucketClient(serviceClient, f.RoundTripper)
	case devops.CodeRepoServiceTypeGogs:
		client, err = NewGogsClient(serviceClient, f.RoundTripper)
	case devops.CodeRepoServiceTypeGitea:
		client, err = NewGiteaClient(serviceClient, f.RoundTripper)
	default:
		return nil, fmt.Errorf("invalid service type %s", service.Spec.Type)
	}
	return client, err
}

type ImageRegistryClientFactory interface {
	SetTransport(http.RoundTripper)
	GetImageRegistryClient(registryType devops.ImageRegistryType, endpoint, username, credential string) ImageRegistryClientInterface
}

type ImageRegistryClientInterface interface {
	GetImageRepos() (result *devops.ImageRegistryBindingRepositories, err error)
	GetImageTags(repository string) (result []devops.ImageTag, err error)
	CheckRegistryAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error)
	CheckAuthentication() (status *devops.HostPortStatus, err error)
	CheckRepositoryAvailable(repository string, lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error)
	TriggerScanImage(repositoryName, tag string) (result *devops.ImageResult)
	GetVulnerability(repositoryName, tag string) (result *devops.VulnerabilityList, err error)
	GetRegistryEndpoint() (endpoint string, err error)
	GetImageRepoLink(name string) (link string)
	GetImageProjects() (result *devops.ProjectDataList, err error)
	CreateImageProject(name string) (result *devops.ProjectData, err error)

	// role sync methods
	GetPermissionNameIDMap() (map[string]int, error)
	GetRoleMapping(opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error)
	UserProjectOperation(project devops.ProjectData, user devops.UserRoleOperation) (err error)
	SetTransport(http.RoundTripper)
}

func NewImageRegistryClientFactory() ImageRegistryClientFactory {
	return &imageRegistryClientFactory{}
}

type imageRegistryClientFactory struct {
	transport http.RoundTripper
}

func (f *imageRegistryClientFactory) SetTransport(transport http.RoundTripper) {
	f.transport = transport
}

func (f imageRegistryClientFactory) GetImageRegistryClient(registryType devops.ImageRegistryType, endpoint, username, credential string) ImageRegistryClientInterface {
	var client ImageRegistryClientInterface
	switch registryType {
	case devops.RegistryTypeDocker:
		client = NewDockerClient(endpoint, username, credential)
	case devops.RegistryTypeHarbor:
		client = NewHarborClient(endpoint, username, credential)
	case devops.RegistryTypeAlauda:
		client = NewAlaudaClient(endpoint, username, credential)
	case devops.RegistryTypeDockerHub:
		client = NewDockerHubClient(endpoint, username, credential)
	default:
		log.Println("Invalid Registry type: ", registryType)
		return nil
	}
	if f.transport != nil && client != nil {
		client.SetTransport(f.transport)
	}
	return client
}

func NewProjectManagementClientFactory() ProjectManagementClientFactory {
	return &projectManagementClientFactory{}
}

type ProjectManagementClientFactory interface {
	SetTransport(http.RoundTripper)
	GetProjectmanageClient(projectmanagementType devops.ProjectManagementType, endpoint, name, credential string, timeoutsecond int) ProjectmanagementClientInterface
}

type ProjectmanagementClientInterface interface {
	GetProjects(page, pagesize string) (result *devops.ProjectDataList, err error)
	CreateProject(projectName, projectDescription, projectLead, projectKey string) (result *devops.ProjectData, err error)
	SearchforUsers(name string) (result *devops.ProjectmanagementUser, err error)
	GetUserCount() (usercount int, err error)
	Authenticate() (status *devops.HostPortStatus, err error)
	GetProjectList(listoptions *devops.RoleMappingListOptions) ([]project, error)
	GetPermissionNameIdMap() (map[string]int, error)
	GetRoleMappping([]project, map[string]int) (*devops.RoleMapping, error)
	AddUserforRole(devops.UserRoleOperation, map[string]string, devops.ProjectUserRoleOperation, map[string]int)
	RemoveUserforRole(devops.UserRoleOperation, map[string]string, devops.ProjectUserRoleOperation, map[string]int)
	SetTransport(http.RoundTripper)
	GetIssueOptionValue(optiontype string) (*devops.IssueFilterDataList, error)
	GetIssueList(*devopsclient.ListIssuesOptions) (*devops.IssueDetailList, error)
	GetIssue(Issuekey string) (*devops.IssueDetail, error)
}

type projectManagementClientFactory struct {
	transport http.RoundTripper
}

func (f *projectManagementClientFactory) SetTransport(transport http.RoundTripper) {
	f.transport = transport
}

func (f projectManagementClientFactory) GetProjectmanageClient(projectmanagementType devops.ProjectManagementType, endpoint, name, credential string, timeoutsecond int) ProjectmanagementClientInterface {
	var client ProjectmanagementClientInterface
	switch projectmanagementType {
	case devops.ProjectManageTypeJira:
		client = NewJiraClient(nil, endpoint, name, credential, timeoutsecond)
	case devops.ProjectManageTypeTaiga:
		client = NewTaigaClient(endpoint, name, credential, timeoutsecond)
	default:
		glog.Errorf("Invalid Projectmanagement type: %s", projectmanagementType)
		return nil
	}
	if f.transport != nil && client != nil {
		client.SetTransport(f.transport)
	}
	return client
}

func NewDocumentManagementClientFactory(roundTripper http.RoundTripper) DocumentManagementClientFactory {
	return &documentManagementClientFactory{
		RoundTripper: roundTripper,
	}
}

type DocumentManagementClientFactory interface {
	SetTransport(http.RoundTripper)
	GetDocumentManagementClient(documentManagementType devops.DocumentManagementType, endpoint string, secret *corev1.Secret, timeoutsecond int) DocumentManagementClientInterface
}

type DocumentManagementClientInterface interface {
	GetProjects(page, pagesize string) (result *devops.ProjectDataList, err error)
	CreateProject(projectName, projectKey, projectDescription string) (result *devops.ProjectData, err error)
	CheckAuthentication() (status *devops.HostPortStatus, err error)
	SetTransport(http.RoundTripper)
}

type documentManagementClientFactory struct {
	RoundTripper http.RoundTripper
}

func (f *documentManagementClientFactory) SetTransport(transport http.RoundTripper) {
	f.RoundTripper = transport
}

func (f documentManagementClientFactory) GetDocumentManagementClient(documentManagementType devops.DocumentManagementType, endpoint string, secret *corev1.Secret, timeoutsecond int) DocumentManagementClientInterface {
	var client DocumentManagementClientInterface
	switch documentManagementType {
	case devops.DocumentManageTypeConfluence:
		client = NewConfluenceClient(endpoint, secret, timeoutsecond, f.RoundTripper)
	default:
		glog.Errorf("Invalid DocumentManagement type: %s", documentManagementType)
		return nil
	}
	if f.RoundTripper != nil && client != nil {
		client.SetTransport(f.RoundTripper)
	}
	return client
}

func NewCodeQualityClientFactory() CodeQualityClientFactory {
	return codeQualityClientFactory{}
}

type CodeQualityClientInterface interface {
	//GetProjects() (result *devops.CodeQualityBindingProjects, err error)
	CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error)
	CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error)
	GetProjectReport(projectKey string) (conditions []devops.CodeQualityCondition, err error)
	GetCorrespondCodeQualityProjects(repositoryList *devops.CodeRepositoryList, binding *devops.CodeQualityBinding) ([]devops.CodeQualityProject, error)
	CheckProjectExist(projectKey string) (bool, error)
	GetLastAnalysisDate(projectKey string) (*time.Time, error)

	GetRolesMapping(*devops.RoleMappingListOptions) (devops.RoleMapping, error)
	ApplyRoleMapping(roleMapping *devops.RoleMapping) (err error)
}

type CodeQualityClientFactory interface {
	GetCodeQualityClient(service *devops.CodeQualityTool, secret *corev1.Secret, opts devopsthirdparty.BaseOpts) (CodeQualityClientInterface, error)
	SetTransport(http.RoundTripper)
}

type codeQualityClientFactory struct {
	transport http.RoundTripper
}

func (f codeQualityClientFactory) GetCodeQualityClient(service *devops.CodeQualityTool, secret *corev1.Secret, opts devopsthirdparty.BaseOpts) (CodeQualityClientInterface, error) {
	if service == nil {
		return nil, fmt.Errorf("CodeQualityService can't be empty")
	}
	var (
		err           error
		client        CodeQualityClientInterface
		serviceClient = NewCodeQualityServiceClient(nil, service, secret, opts)
	)

	switch service.Spec.Type {
	case devops.CodeQualityToolTypeSonarqube:
		client, err = NewSonarQubeClient(serviceClient)
	default:
		return nil, fmt.Errorf("invalide codequality service type: %s", service.Spec.Type)
	}
	return client, err
}

func (f codeQualityClientFactory) SetTransport(transport http.RoundTripper) {
	f.transport = transport
}

func NewContinuousIntegrationClientFactory(transport http.RoundTripper) ContinuousIntegrationClientFactory {
	return continuousIntegrationClientFactory{
		transport: transport,
	}
}

type ContinuousIntegrationClientInterface interface {
	GetJobProgressiveLog(pipelineNamespace, pipelineName string, buildNumber, start int64) (*devops.PipelineLog, error)
	GetBranchJobProgressiveLog(pipelineNamespace, pipelineName, branchName string, buildNumber, start int64) (*devops.PipelineLog, error)
	GetJobNodeLog(pipelineNamespace, pipelineName, organization, runId, nodeId string, start int64) (*devops.PipelineLog, error)
	GetBranchJobNodeLog(pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string, start int64) (*devops.PipelineLog, error)
	GetJobStepLog(pipelineNamespace, pipelineName, organization, runId, nodeId, stepId string, start int64) (*devops.PipelineLog, error)
	GetBranchJobStepLog(pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, stepId string, start int64) (*devops.PipelineLog, error)
	GetBranchJobProgressiveScanLog(pipelineNamespace, pipelineName string, start int64) (*devops.PipelineConfigLog, error)
	ScanMultiBranchJob(pipelineNamespace, pipelineName string) error
	GetJobNodes(pipelineNamespace, pipelineName, organization, runId string) ([]devops.PipelineBlueOceanTask, error)
	GetBranchJobNodes(pipelineNamespace, pipelineName, branchName, organization, runId string) ([]devops.PipelineBlueOceanTask, error)
	GetJobSteps(pipelineNamespace, pipelineName, organization, runId, nodeId string) ([]devops.PipelineBlueOceanTask, error)
	GetBranchJobSteps(pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string) ([]devops.PipelineBlueOceanTask, error)

	SubmitInputStep(pipelineNamespace, pipelineName, organization, runId string, inputOptions devops.PipelineInputOptions) error
	SubmitBranchInputStep(pipelineNamespace, pipelineName, branchName, organization, runId string, inputOptions devops.PipelineInputOptions) error

	GetTestReportSummary(pipelineNamespace, pipelineName, organization, runId string) (*devops.PipelineTestReportSummary, error)
	GetBranchTestReportSummary(pipelineNamespace, pipelineName, branchName, organization, runId string) (*devops.PipelineTestReportSummary, error)

	GetTestReports(pipelineNamespace, pipelineName, organization, runId, status, state, stateBang string, start, limit int64) ([]devops.PipelineTestReportItem, error)
	GetBranchTestReports(pipelineNamespace, pipelineName, branchName, organization, runId, status, state, stateBang string, start, limit int64) ([]devops.PipelineTestReportItem, error)
	GetPipelineArtifacts(pcNamespace, pcName, organization, runId string) (jenkinsmodle.ArtifactList, error)
}

type ContinuousIntegrationClientFactory interface {
	GetContinuousIntegrationClient(service *devops.Jenkins, secret *corev1.Secret) (ContinuousIntegrationClientInterface, error)
	SetTransport(http.RoundTripper)
}

type continuousIntegrationClientFactory struct {
	transport http.RoundTripper
}

func (f continuousIntegrationClientFactory) GetContinuousIntegrationClient(service *devops.Jenkins, secret *corev1.Secret) (ContinuousIntegrationClientInterface, error) {
	if service == nil {
		return nil, fmt.Errorf("JenkinsService can't be empty")
	}

	return NewJenkinsClient(service, secret, f.transport)
}

func (f continuousIntegrationClientFactory) SetTransport(transport http.RoundTripper) {
	f.transport = transport
}

type ArtifactRegistryManagerClientFactory interface {
	SetTransport(http.RoundTripper)
	GetArtifactRegistryManagerClient(managerType string, endpoint string, basicAuthInfo *k8s.SecretDataBasicAuth, timeoutsecond int, systemNamespace string) ArtifactRegistryManagerClientInterface
}

type artifactRegistryManagerClientFactory struct {
	transport http.RoundTripper
}

func (armf *artifactRegistryManagerClientFactory) GetArtifactRegistryManagerClient(managerType string, endpoint string, basicAuthInfo *k8s.SecretDataBasicAuth, Timeoutsecond int, systemNamespace string) (clientInterface ArtifactRegistryManagerClientInterface) {
	var client ArtifactRegistryManagerClientInterface
	switch strings.ToLower(managerType) {
	case "nexus":

		client = NewNexusClientForARM(endpoint, basicAuthInfo, Timeoutsecond, systemNamespace)

	default:
		glog.Errorf("Invalid manager type: %v", managerType)
		return nil
	}

	return client
}

type ArtifactRegistryManagerClientInterface interface {
	InitManager() []error
	CheckAuthentication(username string, password string) (status *devops.HostPortStatus, err error)
	ListRegistry() (devops.ArtifactRegistryList, error)
	CreateRegistry(artifactType string, v map[string]string) error
	ListBlobStore() (devops.BlobStoreOptionList, error)
}

func (armf *artifactRegistryManagerClientFactory) SetTransport(tripper http.RoundTripper) {
	armf.transport = tripper
}

func NewArtifactRegistryManagerClientFactory() ArtifactRegistryManagerClientFactory {
	return &artifactRegistryManagerClientFactory{}
}

type ArtifactRegistryClientFactory interface {
	SetTransport(http.RoundTripper)
	GetArtifactRegistryClient(managerType string, endpoint string, basicAuthInfo *k8s.SecretDataBasicAuth, timeoutsecond int, systemNamespace string) ArtifactRegistryClientInterface
}

type artifactRegistryClientFactory struct {
	transport http.RoundTripper
}

func (armf *artifactRegistryClientFactory) GetArtifactRegistryClient(artifactType string, endpoint string, basicAuthInfo *k8s.SecretDataBasicAuth, Timeoutsecond int, systemNamespace string) (clientInterface ArtifactRegistryClientInterface) {
	var client ArtifactRegistryClientInterface
	switch strings.ToLower(artifactType) {
	case "nexus":

		client = NewNexusClientForAR(endpoint, basicAuthInfo, Timeoutsecond, systemNamespace)

	default:
		glog.Errorf("Invalid artifactType: %v", artifactType)
		return nil
	}

	return client
}

func (armf *artifactRegistryClientFactory) SetTransport(tripper http.RoundTripper) {
	armf.transport = tripper
}

func NewArtifactRegistryClientFactory() ArtifactRegistryClientFactory {
	return &artifactRegistryClientFactory{}
}

type ArtifactRegistryClientInterface interface {
	CheckAuthentication(username string, password string) (status *devops.HostPortStatus, err error)
	ListRegistry() (devops.ArtifactRegistryList, error)
	CreateRegistry(artifactType string, v map[string]string) error
	ListBlobStore() (devops.BlobStoreOptionList, error)
	GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error)
	AddUserRole(project string, registry string, users []string, roleType string, artifactType string) (err error)
	AddUserRoleWithActions(project string, registry string, users []string, roleType string, artifactType string, getter dependency.Manager) (err error)
	RemoveUserRole(project string, registry string, users []string, roleType string) (err error)
}
