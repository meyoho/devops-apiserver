package internalversion

import (
	"bytes"
	"fmt"
	"io/ioutil"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"

	"net/http"

	"github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
)

var bitbucketCodeRepoService = &devops.CodeRepoService{
	Spec: devops.CodeRepoServiceSpec{
		ToolSpec: devops.ToolSpec{
			HTTP: devops.HostPort{
				Host: "https://api.bitbucket.com",
			},
		},
		Type:   devops.CodeRepoServiceTypeBitbucket,
		Public: true,
	},
}
var bitbucketSecret = &corev1.Secret{
	Type: corev1.SecretTypeBasicAuth,
	Data: map[string][]byte{
		"username": []byte("--"),
		"password": []byte("--"),
	},
}

var listTeamsResponseJson = `{
    "pagelen": 10,
    "values": [
        {
            "username": "mathildetech",
            "website": "",
            "display_name": "Mathilde Technology Corp",
            "uuid": "{946ce0ec-3f05-47b4-a58c-0b098e85d7b9}",
            "links": {
                "hooks": {
                    "href": "https://api.bitbucket.org/2.0/teams/mathildetech/hooks"
                },
                "self": {
                    "href": "https://api.bitbucket.org/2.0/teams/mathildetech"
                },
                "repositories": {
                    "href": "https://api.bitbucket.org/2.0/repositories/mathildetech"
                },
                "html": {
                    "href": "https://bitbucket.org/mathildetech/"
                },
                "followers": {
                    "href": "https://api.bitbucket.org/2.0/teams/mathildetech/followers"
                },
                "avatar": {
                    "href": "https://bitbucket.org/account/mathildetech/avatar/"
                },
                "members": {
                    "href": "https://api.bitbucket.org/2.0/teams/mathildetech/members"
                },
                "following": {
                    "href": "https://api.bitbucket.org/2.0/teams/mathildetech/following"
                },
                "projects": {
                    "href": "https://api.bitbucket.org/2.0/teams/mathildetech/projects/"
                },
                "snippets": {
                    "href": "https://api.bitbucket.org/2.0/snippets/mathildetech"
                }
            },
            "created_on": "2014-08-11T16:00:03.999549+00:00",
            "location": null,
            "type": "team"
        },
        {
            "username": "jtcheng",
            "website": null,
            "display_name": "jtcheng",
            "uuid": "{2dcbfc03-30ec-46a1-9bb5-b47c8c6c3039}",
            "links": {
                "hooks": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtcheng/hooks"
                },
                "self": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtcheng"
                },
                "repositories": {
                    "href": "https://api.bitbucket.org/2.0/repositories/jtcheng"
                },
                "html": {
                    "href": "https://bitbucket.org/jtcheng/"
                },
                "followers": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtcheng/followers"
                },
                "avatar": {
                    "href": "https://bitbucket.org/account/jtcheng/avatar/"
                },
                "members": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtcheng/members"
                },
                "following": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtcheng/following"
                },
                "projects": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtcheng/projects/"
                },
                "snippets": {
                    "href": "https://api.bitbucket.org/2.0/snippets/jtcheng"
                }
            },
            "created_on": "2018-11-05T15:02:08.375973+00:00",
            "location": null,
            "type": "team"
        },
        {
            "username": "jtchengteam",
            "website": null,
            "display_name": "jtcheng-team",
            "uuid": "{43805acd-92d5-4d59-b816-5b4e160c698a}",
            "links": {
                "hooks": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtchengteam/hooks"
                },
                "self": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtchengteam"
                },
                "repositories": {
                    "href": "https://api.bitbucket.org/2.0/repositories/jtchengteam"
                },
                "html": {
                    "href": "https://bitbucket.org/jtchengteam/"
                },
                "followers": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtchengteam/followers"
                },
                "avatar": {
                    "href": "https://bitbucket.org/account/jtchengteam/avatar/"
                },
                "members": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtchengteam/members"
                },
                "following": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtchengteam/following"
                },
                "projects": {
                    "href": "https://api.bitbucket.org/2.0/teams/jtchengteam/projects/"
                },
                "snippets": {
                    "href": "https://api.bitbucket.org/2.0/snippets/jtchengteam"
                }
            },
            "created_on": "2018-12-12T07:09:53.637105+00:00",
            "location": null,
            "type": "team"
        }
    ],
    "page": 1,
    "size": 3
}`

var listBitbucketEmptyTeamsResponseJson = `{
  "pagelen": 10,
  "values":[],
  "page": 1,
  "size": 0
}`

var _ = Describe("BitbucketClient", func() {

	var (
		ctl             *gomock.Controller
		bitbucketClient *BitbucketClient
		roundTripper    *mhttp.MockRoundTripper
		serviceClient   *ServiceClient
	)

	BeforeEach(func() {
		serviceClient = NewServiceClient(bitbucketCodeRepoService, bitbucketSecret)
		ctl = gomock.NewController(GinkgoT())
		roundTripper = mhttp.NewMockRoundTripper(ctl)
		bitbucketClient, _ = NewBitbucketClient(serviceClient, roundTripper)
	})

	Describe("BitbucketClient.listTeams", func() {

		Context("When got teams from bitbucket", func() {

			It("Should return projectDataList", func() {
				listTeamsRequest, _ := http.NewRequest(http.MethodGet, "/2.0/teams", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listTeamsRequest)).Return(newBitbucketHttpResponse(http.StatusOK, listTeamsResponseJson), nil)

				listUserRequest, _ := http.NewRequest(http.MethodGet, "/2.0/user", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(listUserRequest)).Return(newHttpResponse(http.StatusOK, bitUserResponseJson), nil)

				projectDataList, err := bitbucketClient.ListProjectDataList(devops.ListProjectOptions{})
				gomega.Expect(err).To(gomega.BeNil())
				gomega.Expect(len(projectDataList.Items)).To(gomega.BeEquivalentTo(4))
				gomega.Expect(projectDataList.Items[0].Name).To(gomega.BeEquivalentTo("mathildetech"))
				gomega.Expect(projectDataList.Items[3].Name).To(gomega.BeEquivalentTo("chengjingtao"))
			})
		})

		Context("When got none teams from bitbucket", func() {

			It("Should return projectDataList with empty items", func() {
				listTeamsRequest, _ := http.NewRequest(http.MethodGet, "/2.0/teams", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listTeamsRequest)).Return(newBitbucketHttpResponse(http.StatusOK, listBitbucketEmptyTeamsResponseJson), nil)

				listUserRequest, _ := http.NewRequest(http.MethodGet, "/2.0/user", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(listUserRequest)).Return(newHttpResponse(http.StatusOK, bitUserResponseJson), nil)

				projectDataList, err := bitbucketClient.ListProjectDataList(devops.ListProjectOptions{})
				gomega.Expect(err).To(gomega.BeNil())
				gomega.Expect(len(projectDataList.Items)).To(gomega.BeEquivalentTo(1))

				gomega.Expect(projectDataList.Items[0].Name).To(gomega.BeEquivalentTo("chengjingtao"))
			})
		})

	})

})

var bitUserResponseJson = `{"type":"user","links":{"self":{"href":"https://api.bitbucket.org/2.0/users/chengjingtao"},"html":{"href":"https://bitbucket.org/chengjingtao/"},"avatar":{"href":"https://bitbucket.org/account/chengjingtao/avatar/"},"followers":{"href":"https://api.bitbucket.org/2.0/users/chengjingtao/followers"},"following":{"href":"https://api.bitbucket.org/2.0/users/chengjingtao/following"},"repositories":{"href":"https://api.bitbucket.org/2.0/repositories/chengjingtao"}},"username":"chengjingtao","nickname":"chengjingtao","account_status":"active","display_name":"chengjt","created_on":"2017-06-19T04:41:34.38587Z","uuid":"{93b88a89-bbf8-4527-a875-d8d7f153af88}","account_id":"557058:38c8b1b1-ad26-40e8-9e45-3ce8af3de3c3"}`

func newBitbucketHttpResponse(statusCode int, body string) *http.Response {
	return &http.Response{
		Status: fmt.Sprintf("%d", statusCode),
		Header: http.Header{
			"Content-Type": []string{
				"application/json",
			},
		},
		StatusCode: statusCode,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 0,
		// Request:    request,
		Body: ioutil.NopCloser(bytes.NewBufferString(fmt.Sprintf(body))),
	}
}
