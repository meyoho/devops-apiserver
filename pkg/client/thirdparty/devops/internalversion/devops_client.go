package internalversion

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	factory "bitbucket.org/mathildetech/devops-client/pkg"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
	"k8s.io/klog/klogr"
)

func NewDevOpsClient(tool, version, baseURL string, opts ...devopsclient.Option) devopsclient.Interface {
	// split url
	var (
		host, scheme string
	)

	url := strings.Split(baseURL, "://")
	if len(url) == 2 {
		scheme, host = url[0], url[1]
		host = strings.TrimRight(host, "/")
	}

	options := devopsclient.NewOptions(opts...)
	// adapter basic config
	options.BasicConfig = &devopsclient.BasicConfig{
		Host:     host,
		BasePath: "",
		Schemes:  []string{scheme},
	}
	options.Logger = klogr.New()
	client, err := factory.NewClient(tool, version, options)
	if err != nil {
		glog.Errorf("New devops client err: %s", err.Error())
	}
	return client
}

func GetRemoteRepos(client devopsclient.Interface) (result *devops.CodeRepoBindingRepositories, err error) {
	repos, err := client.GetRemoteRepos(context.Background())
	if err != nil {
		glog.Errorf("Get remote repos err: %s", err.Error())
		return result, err
	}

	result = &devops.CodeRepoBindingRepositories{}
	for _, owner := range repos.Owners {
		codeOwner := devops.CodeRepositoryOwner{
			Type:      devops.OriginCodeRepoOwnerType(owner.Type),
			ID:        owner.ID,
			Name:      owner.Name,
			Email:     owner.Email,
			HTMLURL:   owner.HTMLURL,
			AvatarURL: owner.AvatarURL,
			DiskUsage: owner.DiskUsage,
		}

		for _, repo := range owner.Repositories {
			codeOwner.Repositories = append(codeOwner.Repositories, devops.OriginCodeRepository{
				CodeRepoServiceType: devops.CodeRepoServiceType(repo.CodeRepoServiceType),
				ID:                  repo.ID,
				Name:                repo.Name,
				FullName:            repo.FullName,
				Description:         repo.Description,
				HTMLURL:             repo.HTMLURL,
				CloneURL:            repo.CloneURL,
				SSHURL:              repo.SSHURL,
				Language:            repo.Language,
				Owner: devops.OwnerInRepository{
					Type: devops.OriginCodeRepoOwnerType(repo.Owner.Type),
					ID:   repo.Owner.ID,
					Name: repo.Owner.Name,
				},
				CreatedAt:    &metav1.Time{Time: repo.CreatedAt},
				PushedAt:     &metav1.Time{Time: repo.PushedAt},
				UpdatedAt:    &metav1.Time{Time: repo.UpdatedAt},
				Private:      repo.Private,
				Size:         repo.Size,
				SizeHumanize: repo.SizeHumanize,
				Data:         repo.Data,
			})
		}

		result.Owners = append(result.Owners, codeOwner)
	}

	return result, nil
}

func GetBranches(client devopsclient.Interface, owner, repo, fullrepoName string) ([]devops.CodeRepoBranch, error) {
	glog.V(6).Infof("Get code repo %s/%s branches", owner, repo)
	result := make([]devops.CodeRepoBranch, 0)
	branches, err := client.GetBranches(context.Background(), owner, repo, fullrepoName)
	if err != nil {
		glog.Errorf("Get code repo %s/%s branches err: %s", owner, repo, err.Error())
		return result, err
	}

	for _, branch := range branches {
		result = append(result, devops.CodeRepoBranch{
			Name:   branch.Name,
			Commit: branch.Commit,
		})
	}

	return result, nil
}

func GetLatestRepoCommit(client devopsclient.Interface, repoID, owner, repoName, repoFullName string) (commit *devops.RepositoryCommit, status *devops.HostPortStatus) {
	glog.V(6).Infof("Get code repo %s latest commit", repoFullName)
	cm, stat := client.GetLatestRepoCommit(context.Background(), repoID, owner, repoName, repoFullName)
	if cm != nil {
		commit = &devops.RepositoryCommit{
			CommitID:       cm.CommitID,
			CommitAt:       &metav1.Time{Time: cm.CommitAt},
			CommitterName:  cm.CommitterName,
			CommitterEmail: cm.CommitterEmail,
			CommitMessage:  cm.CommitMessage,
		}
	}

	if stat != nil {
		status = &devops.HostPortStatus{
			StatusCode:  stat.StatusCode,
			Response:    stat.Response,
			LastAttempt: &metav1.Time{Time: stat.LastAttempt},
		}
	}
	return commit, status
}

func ListCodeRepoProjects(client devopsclient.Interface, options devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	glog.V(6).Infof("Get project data, options: %#v", options)
	data, err := client.ListCodeRepoProjects(context.Background(), devopsclient.ListProjectOptions{
		Page:     options.Page,
		PageSize: options.PageSize,
		Filter:   options.Filter,
		IsRemote: options.IsRemote,
	})
	if err != nil {
		glog.Errorf("Get project data err: %s", err.Error())
		return nil, err
	}

	projects := make([]devops.ProjectData, 0, len(data.Items))
	for _, project := range data.Items {
		projects = append(projects, devops.ProjectData{
			TypeMeta: metav1.TypeMeta{
				Kind: devops.ResourceKindProjectData,
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: project.Name,
			},
			Data: project.Data,
		})
	}
	return &devops.ProjectDataList{
		Items: projects,
	}, nil
}

func CreateCodeRepoProject(client devopsclient.Interface, options devops.CreateProjectOptions) (*devops.ProjectData, error) {
	glog.V(6).Infof("Create code repo project, options: %#v", options)
	data, err := client.CreateCodeRepoProject(context.Background(), devopsclient.CreateProjectOptions{
		Name:     options.Name,
		IsRemote: options.IsRemote,
		Data:     options.Data,
	})
	if err != nil {
		glog.Errorf("Create code repo project err: %s", err.Error())
		return nil, err
	}

	return &devops.ProjectData{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.ResourceKindProjectData,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:        data.Name,
			Annotations: data.Annotations,
		},
		Data: data.Data,
	}, nil
}

func GetImageRepos(client devopsclient.Interface) (*devops.ImageRegistryBindingRepositories, error) {
	glog.V(6).Info("Get image repos")
	repos, err := client.GetImageRepos(context.Background())
	if err != nil {
		glog.Errorf("Get image repos err: %s", err.Error())
		return nil, err
	}
	return &devops.ImageRegistryBindingRepositories{
		Items: repos.Items,
	}, nil
}

func GetImageTags(client devopsclient.Interface, repositoryName string) (result []devops.ImageTag, err error) {
	glog.V(6).Infof("Get image %s tags", repositoryName)
	tags, err := client.GetImageTags(context.Background(), repositoryName)
	if err != nil {
		glog.Errorf("Get image %s tags err: %s", repositoryName, err.Error())
		return result, err
	}
	for _, tag := range tags {
		imageTag := &devops.ImageTag{
			Name:       tag.Name,
			Author:     tag.Author,
			Digest:     tag.Digest,
			CreatedAt:  &metav1.Time{Time: tag.CreatedAt},
			Size:       tag.Size,
			ScanStatus: devops.ImageTagScanStatus(tag.ScanStatus),
			Level:      tag.Level,
			Message:    tag.Message,
		}

		if len(tag.Summary) > 0 {
			for _, summary := range tag.Summary {
				imageTag.Summary = append(imageTag.Summary,
					devops.Summary{Severity: summary.Severity, Count: summary.Count},
				)
			}
		}
		result = append(result, *imageTag)
	}
	return result, nil
}

func TriggerScanImage(client devopsclient.Interface, repositoryName, tag string) (result *devops.ImageResult) {
	res := client.TriggerScanImage(context.Background(), repositoryName, tag)
	glog.V(6).Infof("Trigger scan image %s/%s done, result: %#v", repositoryName, tag, res)
	return &devops.ImageResult{
		StatusCode: res.StatusCode,
		Message:    res.Message,
	}
}

func GetVulnerability(client devopsclient.Interface, repositoryName, tag string) (result *devops.VulnerabilityList, err error) {
	glog.V(6).Infof("Get image %s/%s vulnerability", repositoryName, tag)
	result = &devops.VulnerabilityList{}
	vuls, err := client.GetVulnerability(context.Background(), repositoryName, tag)
	if err != nil {
		glog.Errorf("Get image %s/%s vulnerability err: %s", repositoryName, tag, err.Error())
		return result, err
	}

	for _, vul := range vuls.Items {
		result.Items = append(result.Items, devops.Vulnerability{
			ID:          vul.ID,
			Severity:    vul.Severity,
			Package:     vul.Package,
			Version:     vul.Version,
			Description: vul.Description,
			Link:        vul.Link,
		})
	}
	return result, nil
}

func GetImageProjects(client devopsclient.Interface) (result *devops.ProjectDataList, err error) {
	glog.V(6).Info("Get image projects")
	result = &devops.ProjectDataList{}
	data, err := client.GetImageProjects(context.Background())
	if err != nil {
		glog.Errorf("Get image projects err: %s", err.Error())
		return result, err
	}

	for _, project := range data.Items {
		result.Items = append(result.Items, devops.ProjectData{
			ObjectMeta: metav1.ObjectMeta{
				Name: project.Name,
				Labels: map[string]string{
					harborProjectIDKey: strconv.Itoa(project.ProjectID),
				},
			},
			Data: project.Data,
		})
	}
	return result, nil
}

func CreateImageProject(client devopsclient.Interface, name string) (result *devops.ProjectData, err error) {
	glog.V(6).Infof("Create image project %s", name)
	result = &devops.ProjectData{}
	data, err := client.CreateImageProject(context.Background(), name)
	if err != nil {
		glog.Errorf("Create image project %s err: %s", name, err.Error())
		return result, err
	}

	result.ObjectMeta.Name = data.Name
	return result, nil
}

func GetProjects(client devopsclient.Interface, page, pagesize string) (result *devops.ProjectDataList, err error) {
	glog.V(6).Infof("Get projects, page: %s, pagesie: %s", page, pagesize)
	data, err := client.GetProjects(context.Background(), page, pagesize)
	if err != nil {
		glog.Errorf("Get projects err: %s", err.Error())
		return nil, err
	}

	projects := make([]devops.ProjectData, 0, len(data.Items))
	for _, item := range data.Items {
		projects = append(projects, devops.ProjectData{
			ObjectMeta: metav1.ObjectMeta{
				Name:        item.Name,
				Annotations: item.Annotations,
			},
			Data: item.Data,
		})
	}
	return &devops.ProjectDataList{
		Items: projects,
	}, nil
}

func CreateProject(client devopsclient.Interface, projectName, projectDescription, projectLead, projectKey string) (result *devops.ProjectData, err error) {
	glog.V(6).Infof("Create project %s", projectName)
	data, err := client.CreateProject(context.Background(), projectName, projectDescription, projectLead, projectKey)
	if err != nil {
		glog.Errorf("Create project %s err: %s", projectName, err.Error())
		return nil, err
	}
	return &devops.ProjectData{
		ObjectMeta: metav1.ObjectMeta{
			Name:        data.Name,
			Annotations: data.Annotations,
		},
	}, nil
}

func GetIssuseList(client devopsclient.Interface, listoption *devopsclient.ListIssuesOptions) (result *devops.IssueDetailList, err error) {
	issuelist, err := client.GetIssueList(context.Background(), *listoption)
	if err != nil {
		return nil, err
	}
	result = &devops.IssueDetailList{}
	result.Total = int(issuelist.Total)
	for _, issue := range issuelist.Issues {
		itemissue := ParseIssue(&issue, false)
		result.Items = append(result.Items, *itemissue)
	}

	return result, err
}

func GetIssue(client devopsclient.Interface, issuekey string) (result *devops.IssueDetail, err error) {
	issuedetail, err := client.GetIssueDetail(context.Background(), issuekey)
	if err != nil {
		return nil, err
	}
	result = ParseIssue(issuedetail, true)
	return result, nil
}

func ParseIssue(issuedetail *devopsclient.IssueDetail, detail bool) *devops.IssueDetail {

	result := &devops.IssueDetail{}
	result.Summary = issuedetail.Summary
	result.Key = issuedetail.Key
	result.SelfLink = fmt.Sprintf("/browse/%s", issuedetail.Key)
	result.Description = issuedetail.Description
	result.Updated = issuedetail.Updated
	result.Created = issuedetail.Created

	result.Creator.Username = issuedetail.Creator.Username
	result.Creator.ID = issuedetail.Creator.ID
	result.Creator.Email = issuedetail.Creator.Email

	result.Assignee.Username = issuedetail.Assignee.Username
	result.Assignee.ID = issuedetail.Assignee.ID
	result.Assignee.Email = issuedetail.Assignee.Email

	result.Status.ID = issuedetail.Status.ID
	result.Status.Name = issuedetail.Status.Name

	result.Priority.Name = issuedetail.Priority.Name

	result.Project.Name = issuedetail.Project.Name
	result.Issuetype.Name = issuedetail.Issuetype.Name
	if detail {
		comments := []devops.Comment{}
		for _, detailcomment := range issuedetail.Comments {
			comment := devops.Comment{}
			comment.Author = detailcomment.Author
			comment.Time = detailcomment.Time
			comment.Content = detailcomment.Content
			comments = append(comments, comment)
		}
		result.Comments = comments
		issuelinks := []devops.IssueLink{}
		for _, detailissuelink := range issuedetail.IssueLinks {
			issuelink := devops.IssueLink{}
			issuelink.SelfLink = detailissuelink.SelfLink
			issuelink.Key = detailissuelink.Key
			issuelink.Summary = detailissuelink.Summary
			issuelinks = append(issuelinks, issuelink)
		}
		result.IssueLinks = issuelinks
	}
	return result
}

func GetIssueOption(client devopsclient.Interface, issuetype string) (result *devops.IssueFilterDataList, err error) {

	statuslist, err := client.GetIssueOptions(context.Background(), issuetype)
	if err != nil {
		return nil, err
	}

	result = &devops.IssueFilterDataList{}
	statusdata := []devops.IssueFilterData{}
	prioritydata := []devops.IssueFilterData{}
	issuetypedata := []devops.IssueFilterData{}

	for _, priority := range statuslist.Priority {
		prioritydata = append(prioritydata, devops.IssueFilterData{
			ID:   priority.ID,
			Name: priority.Name,
		})
	}
	for _, status := range statuslist.Status {
		statusdata = append(statusdata, devops.IssueFilterData{
			ID:   status.ID,
			Name: status.Name,
		})
	}
	for _, issuetype := range statuslist.Type {
		issuetypedata = append(issuetypedata, devops.IssueFilterData{
			ID:   issuetype.ID,
			Name: issuetype.Name,
		})
	}
	result.Status = statusdata
	result.Priority = prioritydata
	result.IssueType = issuetypedata

	return result, nil
}

func ListRegistry(client devopsclient.Interface) (devops.ArtifactRegistryList, error) {
	result := devops.ArtifactRegistryList{}
	arList, err := client.ListRegistry(context.Background())
	if err != nil {
		glog.Errorf("List registry err: %s", err.Error())
		return result, err
	}

	for _, ar := range arList.Items {
		result.Items = append(result.Items, devops.ArtifactRegistry{
			ObjectMeta: metav1.ObjectMeta{
				Name: ar.Name,
			},
			Spec: devops.ArtifactRegistrySpec{
				Type:                 ar.Spec.Type,
				ArtifactRegistryName: ar.Spec.ArtifactRegistryName,
				ArtifactRegistryArgs: ar.Spec.ArtifactRegistryArgs,
				ToolSpec: devops.ToolSpec{
					HTTP: devops.HostPort{
						Host: ar.Spec.URL,
					},
				},
			},
		})
	}
	return result, nil
}

func ListBlobStore(client devopsclient.Interface) (result devops.BlobStoreOptionList, err error) {
	blobStores, err := client.ListBlobStore(context.Background())
	if err != nil {
		glog.Errorf("List blob store err: %s", err.Error())
		return result, err
	}

	for _, blob := range blobStores.Items {
		result.Items = append(result.Items, devops.BlobStoreOption{
			Name: blob.Name,
			Type: blob.Type,
		})
	}

	return result, nil
}

func Init(client devopsclient.Interface) []error {
	return client.Init(context.Background())
}

func CreateRegistry(client devopsclient.Interface, artifactType string, v map[string]string) error {
	return client.CreateRegistry(context.Background(), artifactType, v)
}

func GetProjectReport(client devopsclient.Interface, projectKey string) (conditions []devops.CodeQualityCondition, err error) {
	conds, err := client.GetProjectReport(context.Background(), projectKey)
	if err != nil {
		glog.Errorf("Get project report err: %s", err.Error())
		return nil, err
	}

	for _, cond := range conds {
		newCond := devops.CodeQualityCondition{
			BindingCondition: devops.BindingCondition{
				Name:        cond.BindingCondition.Name,
				Namespace:   cond.BindingCondition.Namespace,
				Type:        cond.BindingCondition.Type,
				LastAttempt: &metav1.Time{Time: cond.BindingCondition.LastAttempt},
				Reason:      cond.BindingCondition.Reason,
				Message:     cond.BindingCondition.Message,
				Status:      cond.BindingCondition.Status,
				Owner:       cond.BindingCondition.Owner,
			},
			Branch:      cond.Branch,
			IsMain:      cond.IsMain,
			QualityGate: cond.QualityGate,
			Visibility:  cond.Visibility,
		}

		metrics := make(map[string]devops.CodeQualityAnalyzeMetric)
		for key, value := range cond.Metrics {
			metrics[key] = devops.CodeQualityAnalyzeMetric{
				Name:  value.Name,
				Level: value.Level,
				Value: value.Value,
			}
		}
		newCond.Metrics = metrics
		conditions = append(conditions, newCond)
	}

	return conditions, nil
}

func GetCorrespondCodeQualityProjects(client devopsclient.Interface, repositoryList *devops.CodeRepositoryList, binding *devops.CodeQualityBinding) ([]devops.CodeQualityProject, error) {
	repoList := &devopsclient.CodeRepositoryList{
		Items: make([]devopsclient.CodeRepository, 0, len(repositoryList.Items)),
	}
	for _, coderepo := range repositoryList.Items {
		repoList.Items = append(repoList.Items, devopsclient.CodeRepository{
			Name: coderepo.Name,
			Spec: devopsclient.CodeRepositorySpec{
				Repository: devopsclient.OriginCodeRepository{
					FullName: coderepo.GetRepoFullName(),
					HTMLURL:  coderepo.Spec.Repository.HTMLURL,
				},
			},
		})
	}

	bind := &devopsclient.CodeQualityBinding{
		Name:      binding.Name,
		Namespace: binding.Namespace,
		Spec: devopsclient.CodeQualityBindingSpec{
			CodeQualityTool: devopsclient.CodeQualityTool{
				Name: binding.Spec.CodeQualityTool.Name,
			},
		},
	}

	projects, err := client.GetCorrespondCodeQualityProjects(context.Background(), repoList, bind)
	if err != nil {
		glog.Errorf("Get CorrespondCodeQualityProjects err: %s", err.Error())
		return nil, err
	}

	results := make([]devops.CodeQualityProject, 0, len(projects))
	for _, project := range projects {
		results = append(results, devops.CodeQualityProject{
			TypeMeta: metav1.TypeMeta{
				Kind: devops.TypeCodeQualityProject,
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      project.Name,
				Namespace: project.Namespace,
			},
			Spec: devops.CodeQualityProjectSpec{
				CodeQualityTool: devops.LocalObjectReference{
					Name: project.Spec.CodeQualityTool.Name,
				},
				CodeQualityBinding: devops.LocalObjectReference{
					Name: project.Spec.CodeQualityBinding.Name,
				},
				CodeRepository: devops.LocalObjectReference{
					Name: project.Spec.CodeRepository.Name,
				},
				Project: devops.CodeQualityProjectInfo{
					ProjectKey:   project.Spec.Project.ProjectKey,
					ProjectName:  project.Spec.Project.ProjectName,
					CodeAddress:  project.Spec.Project.CodeAddress,
					LastAnalysis: &metav1.Time{Time: project.Spec.Project.LastAnalysis},
				},
			},
		})
	}
	return results, nil
}

func CheckProjectExist(client devopsclient.Interface, projectKey string) (bool, error) {
	return client.CheckProjectExist(context.Background(), projectKey)
}

func GetLastAnalysisDate(client devopsclient.Interface, projectKey string) (*time.Time, error) {
	return client.GetLastAnalysisDate(context.Background(), projectKey)
}

func CheckAvailable(client devopsclient.Interface) (*devops.HostPortStatus, error) {
	status, err := client.Available(context.Background())
	if err != nil {
		return &devops.HostPortStatus{
			StatusCode: 500,
			Response:   err.Error(),
		}, err
	}

	return &devops.HostPortStatus{
		StatusCode:   status.StatusCode,
		Response:     status.Response,
		Version:      status.Version,
		Delay:        &status.Delay,
		LastAttempt:  &metav1.Time{Time: status.LastAttempt},
		ErrorMessage: status.ErrorMessage,
	}, nil
}

func CheckAuthentication(client devopsclient.Interface) (*devops.HostPortStatus, error) {
	status, err := client.Authenticate(context.Background())
	if err != nil {
		return &devops.HostPortStatus{
			StatusCode: 500,
			Response:   err.Error(),
		}, err
	}

	return &devops.HostPortStatus{
		StatusCode:   status.StatusCode,
		Response:     status.Response,
		Version:      status.Version,
		Delay:        &status.Delay,
		LastAttempt:  &metav1.Time{Time: status.LastAttempt},
		ErrorMessage: status.ErrorMessage,
	}, nil
}
