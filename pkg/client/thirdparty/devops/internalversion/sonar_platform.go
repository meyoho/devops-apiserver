package internalversion

import (
	"fmt"
	"strconv"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	sonar "alauda.io/devops-apiserver/pkg/client/thirdparty/go-sonarqube"
	"alauda.io/devops-apiserver/pkg/role"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

type SonarPlatform struct {
	role.Platform
}

func NewSonarPlatform(platform role.Platform) *SonarPlatform {
	return &SonarPlatform{
		platform,
	}
}

func NewSonarPlatformFromConfigMap(configMap *corev1.ConfigMap) (*SonarPlatform, error) {
	schema, err := newPlatformFromConfigMap(configMap)
	if err != nil {
		return nil, err
	}

	p := schema.Platform(devops.CodeQualityToolTypeSonarqube.String())
	if p == nil {
		err = fmt.Errorf("platform \"%s\" was not found or is not enabled", devops.CodeQualityToolTypeSonarqube.String())
		return nil, err
	}

	return NewSonarPlatform(*p), nil
}

// NewFromConfigMap starts a scheme from devops-config configmap
func newPlatformFromConfigMap(configMap *corev1.ConfigMap) (s *role.Scheme, err error) {
	roleSyncSchemaString, ok := configMap.Data[devops.SettingsKeyRoleMapping]
	if !ok || len(roleSyncSchemaString) == 0 {
		err = fmt.Errorf("Key of '%s' in ConfigMap '%s' is not found or content is empty", devops.SettingsKeyRoleMapping, devops.SettingsConfigMapName)
		glog.Errorf(err.Error())
		return nil, err
	}

	s, err = role.NewFromString(roleSyncSchemaString)
	if err != nil {
		glog.Errorf("Cannot init RoleSyncSchema from roleSyncSchemaString: %s, error:%s", roleSyncSchemaString, err)
		return nil, err
	}
	return
}

// UsergroupName construct binding namespace and sonarRoleName to usergroup name
// eg. bindingNamespace=devops-a6-dev sonarRoleName=admin , groupname willbe  devops-a6-dev-admin
func (p *SonarPlatform) UsergroupName(bindingNamespace, sonarRoleName string) string {
	return fmt.Sprintf("%s-%s", bindingNamespace, sonarRoleName)
}

// UsergroupNames will construct bidning namespaces [ "devops-a6-dev", "devops-a6-ops" ] to
// usergroup on sonar is devops-a6-a6-dev-admin,devops-a6-a6-dev-user, devops-a6-a6-ops-admin,devops-a6-a6-ops-user
// according the role name on sonar
func (p *SonarPlatform) UsergroupNames(bindingNamespaces []string) []string {
	var sonarUsergroups = []string{}
	for _, bindingNamespace := range bindingNamespaces {
		groups := p.UsergroupNamesOfOneBindingNamespace(bindingNamespace)
		sonarUsergroups = append(sonarUsergroups, groups...)
	}
	return sonarUsergroups
}

func (p *SonarPlatform) UsergroupNamesOfOneBindingNamespace(bindingNamespace string) []string {
	var sonarUsergroups = []string{}
	already := map[string]bool{}
	for _, sonarRoleName := range p.RoleMapping {
		var groupName = p.UsergroupName(bindingNamespace, sonarRoleName)
		if !already[groupName] {
			sonarUsergroups = append(sonarUsergroups, groupName)
			already[groupName] = true
		}
	}
	return sonarUsergroups
}

// BindingNamespaceAndSonarRole will parse 'devops-a6-dev-admin' to  isValidSonarUsergroup=true bindingNamespace=devops-a6-dev sonarRoleName=admin
// is the groupName is not our expected, will return isValidaSonarUsergroup=false
func (p *SonarPlatform) BindingNamespaceAndSonarRole(groupName string, bindingNamespaces []string) (
	isValidSonarUsergroup bool, bindingNamespace, sonarRoleName string) {

	for _, bindingNamespace := range bindingNamespaces {
		for _, sonarRoleName := range p.RoleMapping {
			if p.UsergroupName(bindingNamespace, sonarRoleName) == groupName {
				return true, bindingNamespace, sonarRoleName
			}
		}
	}

	return false, "", ""
}

// UserListToRoleMapping will convert UserList from SonarUserList to  RoleMapping structure
// referenced doc http://confluence.alaudatech.com/pages/viewpage.action?pageId=39322740
func (p *SonarPlatform) UserListToRoleMapping(bindingNamespaces []string, userList sonar.UserList) (devops.RoleMapping, error) {

	var bindingNamespaceMapUserRoles = map[string][]devops.UserRoleOperation{}

	for _, user := range userList.Users {

		if user.GroupNames == nil {
			continue
		}

		for _, groupName := range user.GroupNames {
			valid, bindingNamespace, sonarRoleName := p.BindingNamespaceAndSonarRole(groupName, bindingNamespaces)
			if !valid {
				glog.V(7).Infof("skip sonar usergroup '%s'", groupName)
				continue
			}

			userRole := p.NewUserRoleOperation(user, sonarRoleName)
			if _, ok := bindingNamespaceMapUserRoles[bindingNamespace]; !ok {
				bindingNamespaceMapUserRoles[bindingNamespace] = []devops.UserRoleOperation{}
			}
			bindingNamespaceMapUserRoles[bindingNamespace] = append(bindingNamespaceMapUserRoles[bindingNamespace], userRole)
		}
	}

	// convert projectMapUserRoles to []ProjectUserRoleOperation
	projectUserRoles := make([]devops.ProjectUserRoleOperation, 0, len(bindingNamespaces))
	for _, bindingNamespace := range bindingNamespaces {
		item := devops.ProjectUserRoleOperation{
			Project: devops.ProjectData{
				ObjectMeta: metav1.ObjectMeta{
					Name:      bindingNamespace,
					Namespace: bindingNamespace,
				},
			},
		}
		if userRoleOperations, ok := bindingNamespaceMapUserRoles[bindingNamespace]; ok {
			item.UserRoleOperations = userRoleOperations
		}
		projectUserRoles = append(projectUserRoles, item)
	}

	return devops.RoleMapping{
		Spec: projectUserRoles,
	}, nil
}

// New a UserRoleOperation instance by sonar user and role name on sonar
func (p *SonarPlatform) NewUserRoleOperation(user sonar.User, sonarRoleName string) devops.UserRoleOperation {
	return devops.UserRoleOperation{
		User: devops.UserMeta{
			Username: user.Login,
			Email:    user.Email,
		},
		Role: devops.RoleMeta{
			Name: sonarRoleName,
		},
	}
}

func (p *SonarPlatform) RoleMappingToSonarUserList(roleMapping *devops.RoleMapping) []SonarUserOperation {

	sonarUserListOps := []SonarUserOperation{}

	if roleMapping.Spec == nil {
		return sonarUserListOps
	}

	for _, projectUsersRoleOperation := range roleMapping.Spec {
		for _, userRoleOperation := range projectUsersRoleOperation.UserRoleOperations {
			sonarUserOp := p.newSonarUserOperation(projectUsersRoleOperation.Project.Name, userRoleOperation)
			sonarUserListOps = append(sonarUserListOps, sonarUserOp)
		}
	}

	return sonarUserListOps
}

// RoleMappingToSonarProjectPermissions convert rolemapping to sonar project permissions
// map[string][]SonarGroupPermissions is  projectKey:[]SonarGroupPermission{}
// it represet all group permissions settings on a sonar project
// eg.  "github.com/example/demo1":[]{  {GroupName:"devops-a6-a6-dev",Permissions:[]{}},   }
func (p *SonarPlatform) RoleMappingToSonarProjectPermissions(roleMapping *devops.RoleMapping) map[string]*[]SonarGroupPermissions {

	// some sonar's projects's []SonarGroupPermissions{} is the same ,so we use pointer to save it.
	result := map[string]*[]SonarGroupPermissions{}

	if roleMapping.Spec == nil {
		return result
	}

	for _, projectUsersRoleOperation := range roleMapping.Spec {

		permissions := p.toSonarProjectPermissions(projectUsersRoleOperation)
		for projectKey, item := range permissions {
			if _, ok := result[projectKey]; !ok {
				result[projectKey] = item
				continue
			}

			sonarGroupPermissions := append(*result[projectKey], (*item)...)
			result[projectKey] = &sonarGroupPermissions
		}
	}

	return result
}

// toSonarProjectPermissions return sonarprojectkey map to sonargroup permissions of this project
// eg.  "github.com/example/demo1": []{  {GroupName:"devops-a6-a6-dev",Permissions:[]{} }
func (p *SonarPlatform) toSonarProjectPermissions(projectUsersRoleOperation devops.ProjectUserRoleOperation) map[string]*[]SonarGroupPermissions {
	sonarProjectKeyStr := projectUsersRoleOperation.Project.Annotations["codequalityProjectKeys"]
	if len(sonarProjectKeyStr) == 0 {
		return map[string]*[]SonarGroupPermissions{}
	}
	sonarProjectKeys := strings.Split(sonarProjectKeyStr, ",")

	// get group permissions
	alreadyAdd := map[string]bool{}
	groupPermissions := []SonarGroupPermissions{}

	// we should range all role name , but not only in userRoleOperations, becuase, somar sonar project will be added without any user add.
	for standRoleName, sonarRoleName := range p.RoleMapping {
		var groupName = p.UsergroupName(projectUsersRoleOperation.Project.Name, sonarRoleName)

		// remove duplicate groupName
		if _, exist := alreadyAdd[groupName]; exist {
			continue
		}

		groupPermissions = append(groupPermissions,
			SonarGroupPermissions{
				GroupName:   groupName,
				Permissions: customToSonarPermissionSettings(p.Platform.CustomMapping[standRoleName]),
			})
		alreadyAdd[groupName] = true
	}

	permissions := map[string]*[]SonarGroupPermissions{}

	// copy groupPermissions to all sonar projects
	// because they has same permissions setting if they are in the same bindingnamespaces
	for _, projectKey := range sonarProjectKeys {
		permissions[projectKey] = &groupPermissions
	}
	return permissions
}

func customToSonarPermissionSettings(roleCustomPermission map[string]string) []SonarPermissionSetting {
	items := []SonarPermissionSetting{}
	for permissionKey, allowFlag := range roleCustomPermission {
		if allow, _ := strconv.ParseBool(allowFlag); allow {
			items = append(items, SonarPermissionSetting{
				Key:   sonar.PermissionItem(permissionKey),
				Allow: allow,
			})
		}
	}
	return items
}

type SonarUserOperation struct {
	sonar.User
	Operation devops.RoleOperation
}

type SonarGroupPermissions struct {
	GroupName   string
	GroupID     int
	Permissions []SonarPermissionSetting
}

type SonarPermissionSetting struct {
	Key   sonar.PermissionItem
	Allow bool
}

func (p *SonarPlatform) newSonarUserOperation(bindingNamespace string, userRoleOperation devops.UserRoleOperation) SonarUserOperation {
	groupName := p.UsergroupName(bindingNamespace, userRoleOperation.Role.Name)

	sonarUser := SonarUserOperation{
		User: sonar.User{
			Name:       userRoleOperation.User.Username,
			Login:      userRoleOperation.User.Username,
			Email:      userRoleOperation.User.Email,
			GroupNames: []string{groupName},
		},

		Operation: userRoleOperation.Operation,
	}
	return sonarUser
}
