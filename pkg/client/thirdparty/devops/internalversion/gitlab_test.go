package internalversion_test

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/devops/internalversion"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("GitlabClient", func() {

	var (
		ctl          *gomock.Controller
		gitlabClient *internalversion.GitlabClient
		roundTripper *mhttp.MockRoundTripper
	)

	JustBeforeEach(func() {
		ctl = gomock.NewController(GinkgoT())
		roundTripper = mhttp.NewMockRoundTripper(ctl)
	})

	Describe("GitlabClient.GetRolesMapping", func() {
		Context("When no users in gitlab group", func() {

			It("Should return empty rolesmapping", func() {
				g1MembersReq, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:8000/api/v4/groups/g1/members", nil)
				g2MembersReq, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:8000/api/v4/groups/g2/members", nil)

				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(g1MembersReq)).Return(newGitlabHttpResponse(200, emptyGroupMembersJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(g2MembersReq)).Return(newGitlabHttpResponse(200, emptyGroupMembersJson), nil)

				gitlabClient = getGitlabClient(roundTripper)
				roleMapping, err := gitlabClient.GetRolesMapping(&devops.RoleMappingListOptions{
					Projects: []string{
						"g1", "g2",
					},
				})

				Expect(err).To(BeNil())
				Expect(len(roleMapping.Spec)).To(BeEquivalentTo(2))
				Expect([]string{"g1", "g2"}).To(ContainElement(roleMapping.Spec[0].Project.Name))
				Expect([]string{"g1", "g2"}).To(ContainElement(roleMapping.Spec[1].Project.Name))

				Expect(len(roleMapping.Spec[0].UserRoleOperations)).To(BeEquivalentTo(0))
				Expect(len(roleMapping.Spec[1].UserRoleOperations)).To(BeEquivalentTo(0))

			})
		})

		Context("When has users in gitlab group", func() {
			It("Should return rolesmapping", func() {
				g1MembersReq, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:8000/api/v4/groups/g1/members", nil)
				g2MembersReq, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:8000/api/v4/groups/g2/members", nil)
				u1Req, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:8000/api/v4/users/1", nil)
				u2Req, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:8000/api/v4/users/2", nil)

				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(g1MembersReq)).Return(newGitlabHttpResponse(200, emptyGroupMembersJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(g2MembersReq)).Return(newGitlabHttpResponse(200, groupMembersJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(u1Req)).Return(newGitlabHttpResponse(200, u1Json), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(u2Req)).Return(newGitlabHttpResponse(200, u2Json), nil)

				gitlabClient = getGitlabClient(roundTripper)
				roleMapping, err := gitlabClient.GetRolesMapping(&devops.RoleMappingListOptions{
					Projects: []string{
						"g1", "g2",
					},
				})

				Expect(err).To(BeNil())
				Expect(len(roleMapping.Spec)).To(BeEquivalentTo(2))
				Expect([]string{"g1", "g2"}).To(ContainElement(roleMapping.Spec[0].Project.Name))
				Expect([]string{"g1", "g2"}).To(ContainElement(roleMapping.Spec[1].Project.Name))

				var g2 devops.ProjectUserRoleOperation
				var g1 devops.ProjectUserRoleOperation

				if roleMapping.Spec[1].Project.Name == "g2" {
					g2 = roleMapping.Spec[1]
					g1 = roleMapping.Spec[0]
				} else {
					g2 = roleMapping.Spec[0]
					g1 = roleMapping.Spec[1]
				}

				Expect(len(g1.UserRoleOperations)).To(BeEquivalentTo(0))
				Expect(len(g2.UserRoleOperations)).To(BeEquivalentTo(2))

				var g2User1Op = g2.UserRoleOperations[0]
				Expect(g2User1Op.User.Username).To(BeEquivalentTo("root"))
				Expect(g2User1Op.Role.Name).To(BeEquivalentTo("Owner"))
				Expect(g2User1Op.User.Email).To(BeEquivalentTo("u1@example.com"))

				var g2User2Op = g2.UserRoleOperations[1]
				Expect(g2User2Op.User.Username).To(BeEquivalentTo("cjt"))
				Expect(g2User2Op.Role.Name).To(BeEquivalentTo("Guest"))
				Expect(g2User2Op.User.Email).To(BeEquivalentTo("u2@example.com"))

			})
		})
	})

	Describe("GitlabClient.ApplyRolesMapping", func() {
		Context("when has add/update/remove", func() {
			It("should return add/update/remove user in gitlab group", func() {

				listUsers, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:8000/api/v4/users", nil)
				addUser1ToGroup, _ := http.NewRequest(http.MethodPost, "http://18.24.199.4:8000/api/v4/groups/g1/members", nil)
				updateUser2ToGroup, _ := http.NewRequest(http.MethodPut, "http://18.24.199.4:8000/api/v4/groups/g1/members/2", nil)
				deleteUser3ToGroup, _ := http.NewRequest(http.MethodDelete, "http://18.24.199.4:8000/api/v4/groups/g1/members/3", nil)

				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listUsers)).Return(newGitlabHttpResponse(200, listUserJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listUsers)).Return(newGitlabHttpResponse(200, listUserJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listUsers)).Return(newGitlabHttpResponse(200, listUserJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(addUser1ToGroup)).Return(newGitlabHttpResponse(200, u1GrouMember), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(updateUser2ToGroup)).Return(newGitlabHttpResponse(200, u2GrouMember), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(deleteUser3ToGroup)).Return(newGitlabHttpResponse(200, ``), nil)

				gitlabClient = getGitlabClient(roundTripper)
				err := gitlabClient.ApplyRoleMapping(gitlabRoleMapping)
				Expect(err).To(BeNil())
			})
		})
	})

})

var gitlabRoleMapping = &devops.RoleMapping{
	Spec: []devops.ProjectUserRoleOperation{
		{
			Project: devops.ProjectData{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "g1",
					Namespace: "",
				},
			},
			UserRoleOperations: []devops.UserRoleOperation{
				{
					User: devops.UserMeta{
						Username: "user1",
					},
					Role: devops.RoleMeta{
						Name: "Developer",
					},
					Operation: devops.UserRoleOperationAdd,
				},
				{
					User: devops.UserMeta{
						Username: "user2",
					},
					Role: devops.RoleMeta{
						Name: "Master",
					},
					Operation: devops.UserRoleOperationUpdate,
				},
				{
					User: devops.UserMeta{
						Username: "user3",
					},
					Role: devops.RoleMeta{
						Name: "Guest",
					},
					Operation: devops.UserRoleOperationRemove,
				},
			},
		},

		{
			Project: devops.ProjectData{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "g2",
					Namespace: "",
				},
			},
			UserRoleOperations: []devops.UserRoleOperation{},
		},
	},
}

var listUserJson = `[
  {
    "id": 1,
    "username": "user1",
    "name": "user 1",
    "state": "active",
    "avatar_url": "http://localhost:3000/uploads/user/avatar/1/cd8.jpeg",
    "web_url": "http://localhost:3000/john_smith"
  },
  {
    "id": 2,
    "username": "user2",
    "name": "user 2",
    "state": "active",
    "avatar_url": "http://gravatar.com/../e32131cd8.jpeg",
    "web_url": "http://localhost:3000/jack_smith"
  },
  {
    "id": 3,
    "username": "user3",
    "name": "user 3",
    "state": "active",
    "avatar_url": "http://gravatar.com/../e32131cd8.jpeg",
    "web_url": "http://localhost:3000/jack_smith"
  }
]
`

var u1GrouMember = `{
        "id": 1,
        "name": "user1",
        "username": "user1",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/2fc21a2583339390e802a0f250dbbd4b?s=80&d=identicon",
        "web_url": "http://119.28.18.18:31101/user1",
        "access_level": 10,
        "expires_at": null
}`

var u2GrouMember = `{
        "id": 2,
        "name": "user2",
        "username": "user2",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/2fc21a2583339390e802a0f250dbbd4b?s=80&d=identicon",
        "web_url": "http://119.28.18.18:31101/user2",
        "access_level": 10,
        "expires_at": null
}`

var u3GrouMember = `{
        "id": 3,
        "name": "user3",
        "username": "user3",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/2fc21a2583339390e802a0f250dbbd4b?s=80&d=identicon",
        "web_url": "http://119.28.18.18:31101/user3",
        "access_level": 10,
        "expires_at": null
}`

var u1Json = `{
  "id": 1,
  "username": "u1",
  "name": "John Smith",
  "state": "active",
  "avatar_url": "http://localhost:3000/uploads/user/avatar/1/cd8.jpeg",
  "web_url": "http://localhost:3000/john_smith",
  "created_at": "2012-05-23T08:00:58Z",
  "bio": null,
  "location": null,
  "email": "u1@example.com",
  "public_email": "u1@example.com",
  "skype": "",
  "linkedin": "",
  "twitter": "",
  "website_url": "",
  "organization": ""
}
`
var u2Json = `{
  "id": 2,
  "username": "u2",
  "name": "John Smith",
  "state": "active",
  "avatar_url": "http://localhost:3000/uploads/user/avatar/1/cd8.jpeg",
  "web_url": "http://localhost:3000/john_smith",
  "created_at": "2012-05-23T08:00:58Z",
  "bio": null,
  "location": null,
  "email": "u2@example.com",
  "public_email": "u2@example.com",
  "skype": "",
  "linkedin": "",
  "twitter": "",
  "website_url": "",
  "organization": ""
}
`

var emptyGroupMembersJson = `[]`

var groupMembersJson = `[
    {
        "id": 1,
        "name": "Administrator",
        "username": "root",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
        "web_url": "http://119.28.18.18:31101/root",
        "access_level": 50,
        "expires_at": null
    },
    {
        "id": 2,
        "name": "cjt",
        "username": "cjt",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/2fc21a2583339390e802a0f250dbbd4b?s=80&d=identicon",
        "web_url": "http://119.28.18.18:31101/cjt",
        "access_level": 10,
        "expires_at": null
    }
]`

func newGitlabHttpResponse(statusCode int, body string) *http.Response {
	return &http.Response{
		Status: fmt.Sprintf("%d", statusCode),
		Header: http.Header{
			"Content-Type": []string{
				"application/json",
			},
		},
		StatusCode: statusCode,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 0,
		Body:       ioutil.NopCloser(bytes.NewBufferString(fmt.Sprintf(body))),
	}
}

func getGitlabClient(rountTripper http.RoundTripper) *internalversion.GitlabClient {
	service := internalversion.NewServiceClient(codereposervice, gitlabSecret)
	gitlabClient, err := internalversion.NewGitlabClient(service, rountTripper)
	Expect(err).To(BeNil())
	return gitlabClient
}

var gitlabSecret = &corev1.Secret{
	Type: corev1.SecretTypeBasicAuth,
	Data: map[string][]byte{
		"username": []byte("--"),
		"password": []byte("08c703a875d70055874dabaa04620286b9cb2a5e"),
	},
}

var codereposervice = &devops.CodeRepoService{
	Spec: devops.CodeRepoServiceSpec{
		ToolSpec: devops.ToolSpec{
			HTTP: devops.HostPort{
				Host: "http://18.24.199.4:8000",
			},
		},
		Type: devops.CodeRepoServiceTypeGitlab,
	},
}
