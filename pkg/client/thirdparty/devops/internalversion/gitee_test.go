package internalversion

import (
	"bytes"
	"fmt"
	"io/ioutil"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"

	"net/http"

	"github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
)

var giteeCodeRepoService = &devops.CodeRepoService{
	Spec: devops.CodeRepoServiceSpec{
		ToolSpec: devops.ToolSpec{
			HTTP: devops.HostPort{
				Host: "https://api.gitee.com",
			},
		},
		Type:   devops.CodeRepoServiceTypeGitee,
		Public: true,
	},
}
var giteeSecret = &corev1.Secret{
	Type: corev1.SecretTypeBasicAuth,
	Data: map[string][]byte{
		"username": []byte("--"),
		"password": []byte("--"),
	},
}

var newGiteeOrgJson = func(org string) string {
	return fmt.Sprintf(`{
        "id": 2411132,
        "login": "%s",
        "url": "https://gitee.com/api/v5/orgs/%s",
        "avatar_url": "",
        "repos_url": "https://gitee.com/api/v5/orgs/%s/repos",
        "events_url": "https://gitee.com/api/v5/orgs/%s/events",
        "members_url": "https://gitee.com/api/v5/orgs/%s/members{/member}",
        "description": ""
	}`, org, org, org, org, org)
}

var listGiteeOrgsResponseJson = `[
    {
        "id": 2411132,
        "login": "jtchengarch",
        "url": "https://gitee.com/api/v5/orgs/jtchengarch",
        "avatar_url": "",
        "repos_url": "https://gitee.com/api/v5/orgs/jtchengarch/repos",
        "events_url": "https://gitee.com/api/v5/orgs/jtchengarch/events",
        "members_url": "https://gitee.com/api/v5/orgs/jtchengarch/members{/member}",
        "description": ""
    },
    {
        "id": 2411134,
        "login": "jtcheng-data",
        "url": "https://gitee.com/api/v5/orgs/jtcheng-data",
        "avatar_url": "",
        "repos_url": "https://gitee.com/api/v5/orgs/jtcheng-data/repos",
        "events_url": "https://gitee.com/api/v5/orgs/jtcheng-data/events",
        "members_url": "https://gitee.com/api/v5/orgs/jtcheng-data/members{/member}",
        "description": ""
    },
    {
        "id": 2411631,
        "login": "jtcheng-org",
        "url": "https://gitee.com/api/v5/orgs/jtcheng-org",
        "avatar_url": "",
        "repos_url": "https://gitee.com/api/v5/orgs/jtcheng-org/repos",
        "events_url": "https://gitee.com/api/v5/orgs/jtcheng-org/events",
        "members_url": "https://gitee.com/api/v5/orgs/jtcheng-org/members{/member}",
        "description": ""
    },
    {
        "id": 3043896,
        "login": "chengjt-debug",
        "url": "https://gitee.com/api/v5/orgs/chengjt-debug",
        "avatar_url": "",
        "repos_url": "https://gitee.com/api/v5/orgs/chengjt-debug/repos",
        "events_url": "https://gitee.com/api/v5/orgs/chengjt-debug/events",
        "members_url": "https://gitee.com/api/v5/orgs/chengjt-debug/members{/member}",
        "description": "chengjt-debug"
    }
]`

var listGiteeEmptyOrgsResponseJson = `[]`

var _ = Describe("GiteeClient", func() {

	var (
		ctl           *gomock.Controller
		giteeClient   *GiteeClient
		roundTripper  *mhttp.MockRoundTripper
		serviceClient *ServiceClient
	)

	BeforeEach(func() {
		serviceClient = NewServiceClient(giteeCodeRepoService, giteeSecret)
		ctl = gomock.NewController(GinkgoT())
		roundTripper = mhttp.NewMockRoundTripper(ctl)
		giteeClient, _ = NewGiteeClient(serviceClient, roundTripper)
	})

	Describe("GiteeClient.ListProjectDataList", func() {

		Context("When got orgs from gitee", func() {

			It("Should return projectDataList", func() {
				listOrgsRequest, _ := http.NewRequest(http.MethodGet, "/api/v5/user/orgs", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(listOrgsRequest)).Return(newGiteeHttpResponse(http.StatusOK, listGiteeOrgsResponseJson), nil)

				listUserRequest, _ := http.NewRequest(http.MethodGet, "/api/v5/user", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(listUserRequest)).Return(newHttpResponse(http.StatusOK, giteeUserResponseJson), nil)

				projectDataList, err := giteeClient.ListProjectDataList(devops.ListProjectOptions{})
				gomega.Expect(err).To(gomega.BeNil())
				gomega.Expect(len(projectDataList.Items)).To(gomega.BeEquivalentTo(5))
				gomega.Expect(projectDataList.Items[0].Name).To(gomega.BeEquivalentTo("jtchengarch"))

				gomega.Expect(projectDataList.Items[4].Name).To(gomega.BeEquivalentTo("chengjt"))
			})
		})

		Context("When got none org from github", func() {

			It("Should return projectDataList with empty items", func() {
				listOrgsRequest, _ := http.NewRequest(http.MethodGet, "/api/v5/user/orgs", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listOrgsRequest)).Return(newGiteeHttpResponse(http.StatusOK, listGiteeEmptyOrgsResponseJson), nil)

				listUserRequest, _ := http.NewRequest(http.MethodGet, "/api/v5/user", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(listUserRequest)).Return(newHttpResponse(http.StatusOK, giteeUserResponseJson), nil)

				projectDataList, err := giteeClient.ListProjectDataList(devops.ListProjectOptions{})
				gomega.Expect(err).To(gomega.BeNil())
				gomega.Expect(len(projectDataList.Items)).To(gomega.BeEquivalentTo(1))
				gomega.Expect(projectDataList.Items[0].Name).To(gomega.BeEquivalentTo("chengjt"))
			})
		})
	})

	Describe("CreateProjectData", func() {
		It("Should create ok", func() {
			createOrgRequest, _ := http.NewRequest(http.MethodPost, "/api/v5/users/organization", nil)
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(createOrgRequest)).Return(newGiteeHttpResponse(http.StatusCreated, newGiteeOrgJson("created-org")), nil)

			projectData, err := giteeClient.CreateProject(devops.CreateProjectOptions{
				Name: "created-org",
			})

			gomega.Expect(err).To(gomega.BeNil())
			gomega.Expect(projectData.Name).To(gomega.BeEquivalentTo("created-org"))
		})
	})
})

var giteeUserResponseJson = `{
    "id": 303622,
    "login": "chengjt",
    "name": "程静涛",
    "avatar_url": "https://gitee.com/assets/no_portrait.png",
    "url": "https://gitee.com/api/v5/users/chengjt",
    "html_url": "https://gitee.com/chengjt",
    "followers_url": "https://gitee.com/api/v5/users/chengjt/followers",
    "following_url": "https://gitee.com/api/v5/users/chengjt/following_url{/other_user}",
    "gists_url": "https://gitee.com/api/v5/users/chengjt/gists{/gist_id}",
    "starred_url": "https://gitee.com/api/v5/users/chengjt/starred{/owner}{/repo}",
    "subscriptions_url": "https://gitee.com/api/v5/users/chengjt/subscriptions",
    "organizations_url": "https://gitee.com/api/v5/users/chengjt/orgs",
    "repos_url": "https://gitee.com/api/v5/users/chengjt/repos",
    "events_url": "https://gitee.com/api/v5/users/chengjt/events{/privacy}",
    "received_events_url": "https://gitee.com/api/v5/users/chengjt/received_events",
    "type": "User",
    "site_admin": false,
    "blog": null,
    "weibo": null,
    "bio": null,
    "public_repos": 3,
    "public_gists": 1,
    "followers": 0,
    "following": 0,
    "stared": 2,
    "watched": 9,
    "created_at": "2015-01-15T13:02:20+08:00",
    "updated_at": "2019-03-22T12:42:10+08:00",
    "email": "1016890794@qq.com",
    "phone": "15101680849",
    "private_token": "RZosqtKMSN23VKEMGMi9",
    "total_repos": 1000,
    "owned_repos": 9,
    "total_private_repos": 1000,
    "owned_private_repos": 6,
    "private_gists": 0,
    "address": {
        "name": null,
        "tel": null,
        "address": null,
        "province": null,
        "city": null,
        "zip_code": null,
        "comment": null
    }
}`

func newGiteeHttpResponse(statusCode int, body string) *http.Response {
	return &http.Response{
		Status: fmt.Sprintf("%d", statusCode),
		Header: http.Header{
			"Content-Type": []string{
				"application/json",
			},
		},
		StatusCode: statusCode,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 0,
		Body:       ioutil.NopCloser(bytes.NewBufferString(fmt.Sprintf(body))),
	}
}
