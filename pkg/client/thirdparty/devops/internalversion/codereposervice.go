/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package internalversion

import (
	"reflect"
	"time"

	"alauda.io/devops-apiserver/pkg/util/generic"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"

	"github.com/google/go-github/github"
)

func ConvertToMetaTime(metaTime interface{}) *metav1.Time {
	glog.V(7).Infof("metatime: %v; type: %s", metaTime, reflect.TypeOf(metaTime))
	if metaTime == nil {
		return nil
	}

	switch x := metaTime.(type) {
	case github.Timestamp:
		return &metav1.Time{Time: x.Time}
	case *time.Time:
		if x == nil {
			return nil
		}
		return &metav1.Time{Time: *x}
	}
	return nil
}

type AccessTokenResponse struct {
	TokenType   string `json:"token_type"` // is always 'bearer'
	AccessToken string `json:"access_token"`
}

func (a *AccessTokenResponse) GetAccessToken() string {
	return a.AccessToken
}

type AccessTokenResponseGithub struct {
	AccessTokenResponse
	Scope string `json:"scope"`
}

func (a *AccessTokenResponseGithub) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken: a.AccessToken,
		Scope:       a.Scope,
		CreatedAt:   generic.ConvertTimeToStr(time.Now()),
	}
}

type AccessTokenResponseGitea struct {
	AccessTokenResponse
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int64  `json:"expires_in"`
	CreatedAt    int64  `json:"created_at"`
}

func (a *AccessTokenResponseGitea) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken:  a.AccessToken,
		RefreshToken: a.RefreshToken,
		ExpiresIn:    a.ExpiresIn,
		CreatedAt:    generic.ConvertTimeToStr(time.Now()),
	}
}

type AccessTokenResponseGitlab struct {
	AccessTokenResponse
	Scope string `json:"scope"`

	RefreshToken string `json:"refresh_token"`
	CreatedAt    int64  `json:"created_at"`
}

func (a *AccessTokenResponseGitlab) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken:  a.AccessToken,
		Scope:        a.Scope,
		RefreshToken: a.RefreshToken,
		CreatedAt:    generic.ConvertTimeToStr(time.Now()),
	}
}

type AccessTokenResponseGitee struct {
	AccessTokenResponse
	Scope string `json:"scope"`

	RefreshToken string `json:"refresh_token"`
	CreatedAt    int64  `json:"created_at"`

	IDToken string `json:"id_token,omitempty"` // 'gitee-private' has this field
}

func (a *AccessTokenResponseGitee) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken:  a.AccessToken,
		Scope:        a.Scope,
		RefreshToken: a.RefreshToken,
		CreatedAt:    generic.ConvertTimeToStr(time.Now()),
	}
}

type AccessTokenResponseBitbucket struct {
	AccessTokenResponse
	Scopes string `json:"scopes"`

	RefreshToken string `json:"refresh_token"`
}

func (a *AccessTokenResponseBitbucket) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken:  a.AccessToken,
		Scope:        a.Scopes,
		RefreshToken: a.RefreshToken,
		CreatedAt:    generic.ConvertTimeToStr(time.Now()),
	}
}
