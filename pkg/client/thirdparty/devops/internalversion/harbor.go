package internalversion

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

type HarborProjectList struct {
	Projects []HarborProject
}

type HarborProject struct {
	ProjectID         int             `json:"project_id"`
	OwnerID           int             `json:"owner_id"`
	Name              string          `json:"name"`
	CreationTime      string          `json:"creation_time"`
	UpdateTime        string          `json:"update_time"`
	Deleted           bool            `json:"deleted"`
	OwnerName         string          `json:"owner_name"`
	Togglable         bool            `json:"togglable"`
	CurrentUserRoleID int             `json:"current_user_role_id"`
	RepoCount         int             `json:"repo_count"`
	Metadata          ProjectMetadata `json:"metadata"`
}

// HarborUserList a list of harbor users
type HarborUserList struct {
	Users []HarborUser
}

// HarborUser a harbor user
type HarborUser struct {
	// The ID of the user.
	UserID       int32  `json:"user_id,omitempty"`
	Username     string `json:"username,omitempty"`
	Email        string `json:"email,omitempty"`
	Password     string `json:"password,omitempty"`
	Realname     string `json:"realname,omitempty"`
	Comment      string `json:"comment,omitempty"`
	Deleted      bool   `json:"deleted,omitempty"`
	RoleName     string `json:"role_name,omitempty"`
	RoleID       int32  `json:"role_id,omitempty"`
	HasAdminRole bool   `json:"has_admin_role,omitempty"`
	ResetUUID    string `json:"reset_uuid,omitempty"`
	Salt         string `json:"Salt,omitempty"`
	CreationTime string `json:"creation_time,omitempty"`
	UpdateTime   string `json:"update_time,omitempty"`
}

// HarborProjectMember project member
type HarborProjectMember struct {
	// The role id 1 for projectAdmin, 2 for developer, 3 for guest
	RoleID      int32             `json:"role_id,omitempty"`
	MemberUser  *HarborUserEntity `json:"member_user,omitempty"`
	MemberGroup *HarborUserGroup  `json:"member_group,omitempty"`
}

// HarborUserEntity user entity
type HarborUserEntity struct {
	// The ID of the user.
	UserID int32 `json:"user_id,omitempty"`
	// The name of the user.
	Username string `json:"username,omitempty"`
}

// HarborUserGroup usergroup
type HarborUserGroup struct {
	// The ID of the user group
	ID int32 `json:"id,omitempty"`
	// The name of the user group
	GroupName string `json:"group_name,omitempty"`
	// The group type, 1 for LDAP group.
	GroupType int32 `json:"group_type,omitempty"`
	// The DN of the LDAP group if group type is 1 (LDAP group).
	LdapGroupDn string `json:"ldap_group_dn,omitempty"`
}

// HarborProjectMemberEntityList a list of project members
type HarborProjectMemberEntityList struct {
	Members []HarborProjectMemberEntity
}

// HarborProjectMemberEntity project memeber entity
type HarborProjectMemberEntity struct {
	// the project member id
	ID int32 `json:"id,omitempty"`
	// the project id
	ProjectID int32 `json:"project_id,omitempty"`
	// the name of the group member.
	EntityName string `json:"entity_name,omitempty"`
	// the name of the role
	RoleName string `json:"role_name,omitempty"`
	// the role id
	RoleID int32 `json:"role_id,omitempty"`
	// the id of entity, if the member is an user, it is user_id in user table. if the member is an user group, it is the user group's ID in user_group table.
	EntityID int32 `json:"entity_id,omitempty"`
	// the entity's type, u for user entity, g for group entity.
	EntityType string `json:"entity_type,omitempty"`
}

// HarborRoleRequest harbor role request
type HarborRoleRequest struct {
	// The role id 1 for projectAdmin, 2 for developer, 3 for guest
	RoleID int32 `json:"role_id,omitempty"`
}

type ProjectMetadata struct {
	Public             string  `json:"public"`
	AutoScan           *string `json:"auto_scan,omitempty"`
	EnableContentTrust *string `json:"enable_content_trust,omitempty"`
	PreventVul         *string `json:"prevent_vul,omitempty"`
	Severity           *string `json:"severity,omitempty"`
}

type HarborRepository struct {
	// The ID of repository.
	Id int32 `json:"id,omitempty"`
	// The name of repository.
	Name string `json:"name,omitempty"`
	// The project ID of repository.
	ProjectId int32 `json:"project_id,omitempty"`
	// The description of repository.
	Description string `json:"description,omitempty"`
	// The pull count of repository.
	PullCount int32 `json:"pull_count,omitempty"`
	// The star count of repository.
	StarCount int32 `json:"star_count,omitempty"`
	// The tags count of repository.
	TagsCount int32 `json:"tags_count,omitempty"`
	// The creation time of repository.
	CreationTime string `json:"creation_time,omitempty"`
	// The update time of repository.
	UpdateTime string `json:"update_time,omitempty"`
}

/*
{"id":30,"name":"aml-test/base-modelserver","project_id":8,"description":"","pull_count":12,"star_count":0,"tags_count":3,"labels":[],"creation_time":"2019-05-07T11:27:55.613876Z","update_time":"2019-05-16T04:09:54.847817Z"}
*/

const (
	harborProjectAdmin       = "projectAdmin"
	harborProjectAdminRoleID = 1
	harborDeveloper          = "developer"
	harborDeveloperRoleID    = 2
	harborGuest              = "guest"
	harborGuestRoleID        = 3
)

const (
	harborUserIDKey      = "user_id"
	harborMemberIDKey    = "member_id"
	harborRoleIDKey      = "role_id"
	harborProjectIDKey   = "project_id"
	harborProjectNameKey = "project_name"
)

var harborRolesIDMap = map[string]int{
	harborProjectAdmin: harborProjectAdminRoleID,
	harborDeveloper:    harborDeveloperRoleID,
	harborGuest:        harborGuestRoleID,
}

// region Harbor
type HarborClient struct {
	ImageRegistryClient
	devopsClient       devopsclient.Interface
	projectMemberCache map[int]*HarborProjectMemberEntityList
	projectListCache   *HarborProjectList
	usersListCache     *HarborUserList
}

func NewHarborClient(endpoint, username, credential string) *HarborClient {
	harborClient := NewImageRegistryClient(endpoint, username, credential)
	devopsClient := NewDevOpsClient("harbor", "", endpoint, devopsclient.NewBasicAuth(username, credential),
		devopsclient.NewTransport(harborClient.Client.Transport))
	return &HarborClient{ImageRegistryClient: harborClient, projectMemberCache: map[int]*HarborProjectMemberEntityList{}, devopsClient: devopsClient}
}

func (c *HarborClient) SetTransport(trans http.RoundTripper) {
	c.ImageRegistryClient.Client.Transport = trans
}

func (c *HarborClient) CheckAuthentication() (status *devops.HostPortStatus, err error) {
	return CheckAuthentication(c.devopsClient)
}

func (c *HarborClient) CheckRegistryAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckAvailable(c.devopsClient)
}

func (c *HarborClient) CheckRepositoryAvailable(repositoryName string, lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	glog.V(6).Infof("Check harbor image repository: %s is available or not", repositoryName)
	var (
		start       = time.Now()
		duration    time.Duration
		lastAttempt = metav1.NewTime(start)
	)
	urlSuffix := fmt.Sprintf("api/repositories/%s/tags", repositoryName)
	status = &devops.HostPortStatus{
		LastAttempt: &lastAttempt,
	}
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		status.StatusCode = 500
		status.Response = err.Error()
	} else {
		status.StatusCode = resp.StatusCode
		status.Response = ImageRepositoryAvailable
	}
	duration = time.Since(start)
	status.Delay = &duration
	return
}

func (c *HarborClient) GetImageReposByRegistry() (result *devops.ImageRegistryBindingRepositories, err error) {
	logName := "GetImageRepos"
	result = &devops.ImageRegistryBindingRepositories{}

	path := "v2/_catalog"

	resp, err := c.RequestRegistry(path)
	if err != nil {
		glog.Errorf("%s %s error is %s", logName, path, err.Error())
		return
	}
	glog.V(9).Infof("%s %s resp is %#v", logName, path, resp)
	glog.V(9).Infof("%s %s resp body is %s", logName, path, string([]byte(resp.Body)))
	response := new(RegistryRepositoriesResponse)
	err = json.Unmarshal([]byte(resp.Body), response)
	if err != nil {
		glog.Errorf("%s %s error is %s", logName, path, err.Error())
		return
	}
	result.Items = response.Repositories
	glog.V(9).Infof("%s %s result is %#v", logName, path, result)

	return result, nil
}

func (c *HarborClient) GetImageRepos() (result *devops.ImageRegistryBindingRepositories, err error) {
	return GetImageRepos(c.devopsClient)
}

// GetProjectRepositories fetch repositories for a projectID
func (c *HarborClient) GetProjectRepositories(projectID int) (repos []HarborRepository, err error) {
	logName := "GetProjectRepositories"
	currentPage := 1
	repos = make([]HarborRepository, 0, 100)
	totalString := ""
	totalInt := 0
	for {
		glog.V(9).Infof("[%s] fetch project %d repositories page %d", logName, projectID, currentPage)
		var pageRepos []HarborRepository
		var response *RegistryResponse
		url := fmt.Sprintf("api/repositories?project_id=%d&page_size=100&page=%d", projectID, currentPage)
		if response, err = c.RequestRegistry(url); err != nil {
			glog.Errorf("[%s] Error while fetching project %d page %d: %#v", logName, projectID, currentPage, err)
			return
		}
		if err = json.Unmarshal(response.Body, &pageRepos); err != nil {
			glog.Errorf("[%s] Error unmashalling response %s from project %d page %d: %#v", logName, response.Body, projectID, currentPage, err)
			return
		}
		if len(pageRepos) > 0 {
			repos = append(repos, pageRepos...)
		}
		currentPage++
		// the total number of items is added on the header

		totalString = response.HTTPHeader.Get("X-Total-Count")
		totalInt, _ = strconv.Atoi(totalString)
		glog.V(9).Infof("[%s] repo list for project %d total count is %s == %d ", logName, projectID, totalString, totalInt)

		// did we got it all already?
		if len(repos) >= totalInt {
			break
		}
	}
	return
}

func (c *HarborClient) GetImageTags(repositoryName string) (result []devops.ImageTag, err error) {
	return GetImageTags(c.devopsClient, repositoryName)
}

func (c *HarborClient) TriggerScanImage(repositoryName, tag string) (result *devops.ImageResult) {
	return TriggerScanImage(c.devopsClient, repositoryName, tag)
}

func (c *HarborClient) GetVulnerability(repositoryName, tag string) (result *devops.VulnerabilityList, err error) {
	return GetVulnerability(c.devopsClient, repositoryName, tag)
}

func (c *HarborClient) ConvertScanTag(scanTags []ImageScanTag) (tags []devops.ImageTag, err error) {
	for _, scanTag := range scanTags {
		tmpTag := devops.ImageTag{}
		tmpTag.Name = scanTag.Name
		tmpTag.Author = scanTag.Author
		tmpTag.Digest = strings.TrimPrefix(scanTag.Digest, ImageDigestSignature)
		t, err := time.Parse(time.RFC3339Nano, scanTag.Created)
		if err != nil {
			glog.Errorf("Tag(%s) time convert string to time.Time is failed: %s", scanTag.Name, err)
		}
		tmpTag.CreatedAt = &metav1.Time{Time: t}
		tmpTag.Size = c.ConvertSizeToString(int64(scanTag.Size))
		tmpTag.ScanStatus = devops.ImageTagScanStatusNotScan

		if scanTag.ScanOverview != nil {
			if scanTag.ScanOverview.ScanStatus == devops.ImageTagScanStatusError {
				tmpTag.ScanStatus = devops.ImageTagScanStatusError
				tmpTag.Message = c.GetScanJobLog(scanTag.ScanOverview.JobID)
			} else {
				tmpTag.ScanStatus = scanTag.ScanOverview.ScanStatus
				tmpTag.Level = scanTag.ScanOverview.Severity
				for _, s := range scanTag.ScanOverview.Components.Summary {
					summary := devops.Summary{Severity: s.Severity, Count: s.Count}
					tmpTag.Summary = append(tmpTag.Summary, summary)
				}
				t, err := time.Parse(time.RFC3339Nano, scanTag.ScanOverview.Updated)
				if err != nil {
					glog.Errorf("Tag(%s) time convert string to time.Time is failed: %s", scanTag.Name, err)
				}
				tmpTag.UpdatedAt = &metav1.Time{Time: t}
			}
		}
		tags = append(tags, tmpTag)
	}
	return
}

func (c *HarborClient) GetScanJobLog(jobID int) (message string) {
	glog.V(6).Infof("Scan image failed, get the log with jobID: %d", jobID)
	urlSuffix := fmt.Sprintf("api/jobs/scan/%d/log", jobID)
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		glog.Errorf("Get ScanJobLog failed: %v", err)
		return err.Error()
	}
	return fmt.Sprintf("%s", resp.Body)
}

func (c *HarborClient) GetRegistryEndpoint() (endpoint string, err error) {
	return
}

func (c *HarborClient) GetImageRepoLink(name string) (link string) {
	glog.V(6).Infof("Get harbor repository: %s link", name)
	var projectID int
	nameList := strings.Split(name, "/")
	nameListLength := len(nameList)
	if nameListLength < 2 {
		glog.Errorf("Harbor repository name: %s illegal", name)
		return
	}
	projectList, err := c.GetProjects()
	if err != nil {
		glog.Errorf("Get harbor projects failed: %v", err)
	}
	for _, project := range projectList.Projects {
		if project.Name == nameList[0] {
			projectID = project.ProjectID
			break
		}
	}
	if projectID != 0 {
		link = fmt.Sprintf("%s/harbor/projects/%d/repositories/%s",
			strings.TrimRight(c.Endpoint, "/"), projectID, strings.Replace(name, "/", "%2f", nameListLength-1))
	}
	return
}

func (c *HarborClient) GetImageProjects() (result *devops.ProjectDataList, err error) {
	return GetImageProjects(c.devopsClient)
}

func (c *HarborClient) prepareRequest(method, path string, body interface{}) (req *http.Request, err error) {
	var reader io.Reader
	if body != nil {
		bodyJSON, _ := json.Marshal(body)
		reader = bytes.NewBuffer(bodyJSON)
	}
	url := c.Url(path)
	req, err = http.NewRequest(method, url, reader)
	if err != nil {
		glog.Errorf("Init http request error: %v", err)
		return
	}
	if c.Username != "" && c.Credential != "" {
		req.SetBasicAuth(c.Username, c.Credential)
	}
	req.Header.Set("Content-Type", "application/json")
	return
}

func (c *HarborClient) CreateImageProject(name string) (result *devops.ProjectData, err error) {
	return CreateImageProject(c.devopsClient, name)
}

func (c *HarborClient) GetProjects(forceRemote ...bool) (projects *HarborProjectList, err error) {
	logName := "GetProjects"
	if c.projectListCache != nil && len(forceRemote) == 0 {
		projects = c.projectListCache
		glog.V(9).Infof("[%s] go cache", logName)
		return
	}

	projects = &HarborProjectList{
		Projects: make([]HarborProject, 0, 10),
	}

	var page = 1
	var totalString = ""
	var total int

	for {
		tempHarborProjectList := make([]HarborProject, 0, 10)
		var resp *RegistryResponse
		urlSuffix := "api/projects?page_size=100&page=" + strconv.Itoa(page)
		glog.V(9).Infof("[%s] loop: %d urlSuffix: %s", logName, page, urlSuffix)
		resp, err = c.RequestRegistry(urlSuffix)
		glog.V(9).Infof("[%s] loop: %d resp: %#v", logName, page, resp)
		if err != nil {
			glog.Errorf("List harbor projects failed: %s", err)
			return
		}

		glog.V(9).Infof("[%s] loop: %d resp body: %s", logName, page, resp.Body)

		totalString = resp.HTTPHeader.Get("X-Total-Count")
		glog.V(9).Infof("[%s] loop: %d totalString: %#v", logName, page, totalString)

		total, err = strconv.Atoi(totalString)
		glog.V(9).Infof("[%s] loop: %d total: %#v", logName, page, total)
		if err != nil {
			glog.Error(err.Error())
			return
		}

		err = json.Unmarshal([]byte(resp.Body), &tempHarborProjectList)
		glog.V(9).Infof("[%s] loop: %d tempHarborProjectList: %#v", logName, page, tempHarborProjectList)
		if err != nil {
			glog.Errorf("Parse harbor projects failed: %s", err)
			return
		} else {
			projects.Projects = append(projects.Projects, tempHarborProjectList...)
		}

		if len(projects.Projects) >= total {
			glog.V(9).Infof("[%s] loop: %d total: %#v project len:%#v", logName, page, total, len(projects.Projects))
			break
		}

		page++
	}
	c.projectListCache = projects

	glog.V(9).Infof("[%s] projects:%#v", logName, projects)

	return
}

// GetPermissionNameIDMap harbor roles are fixed so we can return a simple list
func (c *HarborClient) GetPermissionNameIDMap() (map[string]int, error) {
	return harborRolesIDMap, nil
}

// GetUsers returns a list of users
func (c *HarborClient) GetUsers() (users *HarborUserList, err error) {
	// make a small cache to improve speed
	// this only works because the client is always created
	// in case the client is a longlived instance, this approached is not usable
	if c.usersListCache != nil {
		users = c.usersListCache
		return
	}
	users = &HarborUserList{
		Users: make([]HarborUser, 0, 10),
	}
	urlSuffix := "api/users"
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		glog.Errorf("List harbor users failed: %s", err)
		return
	}
	glog.V(5).Infof("response body is: %s", resp.Body)
	err = json.Unmarshal([]byte(resp.Body), &users.Users)
	if err != nil {
		glog.Errorf("Parse harbor users failed: %s", err)
	}
	c.usersListCache = users
	return
}

// GetRoleMapping returns a list of role mappings
func (c *HarborClient) GetRoleMapping(opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error) {
	// list users
	var (
		funcName = "GetRoleMapping"
		users    *HarborUserList
		projects *devops.ProjectDataList
	)
	// get users
	if users, err = c.GetUsers(); err != nil {
		glog.Errorf("[%s] Harbor list users err: %s", funcName, err)
		return
	}
	// create an index for the list
	userMap := map[string]HarborUser{}
	if len(users.Users) > 0 {
		for _, user := range users.Users {
			userMap[user.Username] = user
		}
	}

	// list projects
	if projects, err = c.GetImageProjects(); err != nil {
		glog.Errorf("[%s] Harbor list projects err: %s", funcName, err)
		return
	}

	// filtering users according to the options list
	result = &devops.RoleMapping{Spec: make([]devops.ProjectUserRoleOperation, 0, len(projects.Items))}
	for i := 0; i < len(projects.Items); i++ {
		project := projects.Items[i]
		if opts.ContainsProject(project.GetName()) {
			toolProject := devops.ProjectUserRoleOperation{
				Project:            project,
				UserRoleOperations: make([]devops.UserRoleOperation, 0, len(users.Users)),
			}
			// fetching the members of the project
			if toolProject, err = c.getProjectMemberRoles(toolProject, userMap); err != nil {
				glog.Errorf("Project %s could not fetch user roles. err: %s", toolProject.Project.Name, err)
				continue
			}
			result.Spec = append(result.Spec, toolProject)
		}
	}
	return
}

func (c *HarborClient) getProjectMemberRoles(project devops.ProjectUserRoleOperation, users map[string]HarborUser) (result devops.ProjectUserRoleOperation, err error) {
	id := c.getProjectID(project.Project)
	var members *HarborProjectMemberEntityList
	members, err = c.GetHarborProjectMembers(id)
	if err != nil {
		err = fmt.Errorf("Project id %d could not retrieve users: %s", id, err)
		return
	}
	if members != nil && len(members.Members) > 0 {
		for _, mem := range members.Members {
			// u for user and g for group. For now only user is supported
			switch mem.EntityType {
			case "u":
				userRole := c.getUserRoleOperationFromMemberEntity(mem, users)
				project.UserRoleOperations = append(project.UserRoleOperations, userRole)
			}
		}
	}
	result = project
	return
}

func (c *HarborClient) getUserRoleOperationFromMemberEntity(member HarborProjectMemberEntity, users map[string]HarborUser) (userRole devops.UserRoleOperation) {
	// entityData := map[string]string{}
	userRole = devops.UserRoleOperation{
		User: devops.UserMeta{
			ObjectMeta: metav1.ObjectMeta{
				Labels: map[string]string{
					harborUserIDKey:   strconv.Itoa(int(member.EntityID)),
					harborMemberIDKey: strconv.Itoa(int(member.ID)),
				},
			},
			Username: member.EntityName,
		},
		Role: devops.RoleMeta{
			ObjectMeta: metav1.ObjectMeta{
				Labels: map[string]string{
					harborRoleIDKey: strconv.Itoa(int(member.RoleID)),
				},
			},
			Name: member.RoleName,
		},
	}
	if user, ok := users[member.EntityName]; ok {
		userRole.User.Email = user.Email
	}
	return
}

func (c *HarborClient) getRoleID(roleName string) int {
	roleMap, _ := c.GetPermissionNameIDMap()
	return roleMap[roleName]
}

func (c *HarborClient) getProjectID(project devops.ProjectData) int {
	if project.Labels == nil {
		project.Labels = map[string]string{}
	}
	value, _ := strconv.Atoi(project.Labels[harborProjectIDKey])
	if value == 0 {
		if project.Data == nil {
			project.Data = map[string]string{}
		}
		value, _ = strconv.Atoi(project.Data[harborProjectIDKey])
	}
	if value == 0 {
		list, _ := c.GetProjects()
		if list != nil && len(list.Projects) > 0 {
			for _, proj := range list.Projects {
				if proj.Name == project.Name {
					value = proj.ProjectID
					project.Data[harborProjectIDKey] = strconv.Itoa(value)
					project.Labels[harborProjectIDKey] = strconv.Itoa(value)
					break
				}
			}
		}
	}
	return value
}

// getHarborUser this method is to simplify fetching users from harbor when syncing permissions
// in some cases the username or email could have some difference, therefore
// copying and keeping the username/email just like when stored in harbor is necessary
func (c *HarborClient) getHarborUser(user devops.UserRoleOperation) devops.UserRoleOperation {
	users, _ := c.GetUsers()
	if users == nil || len(users.Users) == 0 {
		return user
	}
	for _, us := range users.Users {
		if us.Email == user.User.Email || us.Username == user.User.Username {
			user.User.Email = us.Email
			user.User.Username = us.Username
			break
		}
	}
	return user
}

// getProjectMemberID gets a user's memberID for a specific project
func (c *HarborClient) getProjectMemberID(projectID int, user devops.UserRoleOperation) (memberID int, err error) {
	if user.User.Labels == nil {
		user.User.Labels = map[string]string{}
	}
	// if the member id is already in the object we can return it
	if memberIDStr, ok := user.User.Labels[harborMemberIDKey]; ok && memberIDStr != "" {
		memberID, err = strconv.Atoi(memberIDStr)
		return
	}
	// if it is not in the user information, it is necessary to fetch from harbor
	var members *HarborProjectMemberEntityList
	if members, err = c.GetHarborProjectMembers(projectID); err != nil {
		return
	}
	if members != nil && len(members.Members) > 0 {
		for _, member := range members.Members {
			if member.EntityType == "u" && member.EntityName == user.User.Username {
				memberID = int(member.ID)
				user.User.Labels[harborMemberIDKey] = strconv.Itoa(memberID)
				user.User.Labels[harborUserIDKey] = strconv.Itoa(int(member.EntityID))
				break
			}
		}
	}
	if memberID == 0 {
		err = errors.NewBadRequest(fmt.Sprintf("User %s is not a member of project %d", user.User.Username, projectID))
	}
	return
}

// UserProjectOperation operates a user/role on a project
func (c *HarborClient) UserProjectOperation(project devops.ProjectData, user devops.UserRoleOperation) (err error) {
	roleID := int32(c.getRoleID(user.Role.Name))
	projectID := c.getProjectID(project)
	// make sure the user data is equal to harbor's
	user = c.getHarborUser(user)
	var memberID int
	switch user.Operation {
	case devops.UserRoleOperationAdd:
		request := HarborProjectMember{
			RoleID: roleID,
			MemberUser: &HarborUserEntity{
				Username: user.User.Username,
			},
		}
		err = c.CreateProjectMember(projectID, request)
	case devops.UserRoleOperationRemove:
		if memberID, err = c.getProjectMemberID(projectID, user); err != nil {
			return
		}
		err = c.DeleteProjectMember(projectID, memberID)
	case devops.UserRoleOperationUpdate:
		if memberID, err = c.getProjectMemberID(projectID, user); err != nil {
			return
		}
		request := HarborRoleRequest{RoleID: roleID}
		err = c.UpdateProjectMember(projectID, memberID, request)
	}
	return
}

// GetHarborProjectMembers returns a list of member of a project
func (c *HarborClient) GetHarborProjectMembers(projectID int, forceRemote ...bool) (users *HarborProjectMemberEntityList, err error) {
	// make a simple cache
	var ok bool
	if users, ok = c.projectMemberCache[projectID]; ok && users != nil && len(forceRemote) == 0 {
		return
	}
	users = &HarborProjectMemberEntityList{
		Members: make([]HarborProjectMemberEntity, 0, 10),
	}
	urlSuffix := fmt.Sprintf("api/projects/%d/members", projectID)
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		glog.Errorf("List harbor project members failed: %s", err)
		return
	}
	glog.V(5).Infof("response body is: %s", resp.Body)
	err = json.Unmarshal([]byte(resp.Body), &users.Members)
	if err != nil {
		glog.Errorf("Parse harbor project members failed: %s", err)
	}
	if users != nil {
		c.projectMemberCache[projectID] = users
	}
	return
}

// CreateProjectMember creates a member in a project
func (c *HarborClient) CreateProjectMember(projectID int, request HarborProjectMember) (err error) {
	urlSuffix := fmt.Sprintf("api/projects/%d/members", projectID)
	var req *http.Request
	var resp *RegistryResponse
	req, err = c.prepareRequest(http.MethodPost, urlSuffix, request)
	if err != nil {
		glog.Errorf("Create harbor project memeber request failed: %s", err)
		return
	}
	resp, err = c.DoRequest(req)
	if err != nil {
		glog.Errorf("Create harbor project memeber failed: %s", err)
		return
	}
	switch resp.StatusCode {
	case http.StatusOK:
		err = nil
	default:
		err = fmt.Errorf("Harbor response: %s", resp.Body)
	}
	return
}

// UpdateProjectMember updates a role of a member in a project
func (c *HarborClient) UpdateProjectMember(projectID, memberID int, request HarborRoleRequest) (err error) {
	urlSuffix := fmt.Sprintf("api/projects/%d/members/%d", projectID, memberID)
	var req *http.Request
	var resp *RegistryResponse
	req, err = c.prepareRequest(http.MethodPut, urlSuffix, request)
	if err != nil {
		glog.Errorf("Create update harbor project memeber request failed: %s", err)
		return
	}
	resp, err = c.DoRequest(req)
	if err != nil {
		glog.Errorf("Update harbor project memeber failed: %s", err)
		return
	}
	switch resp.StatusCode {
	case http.StatusOK:
		err = nil
	default:
		err = fmt.Errorf("Harbor response: %s", resp.Body)
	}
	return
}

// DeleteProjectMember removes a project member
func (c *HarborClient) DeleteProjectMember(projectID, memberID int) (err error) {
	urlSuffix := fmt.Sprintf("api/projects/%d/members/%d", projectID, memberID)
	var req *http.Request
	var resp *RegistryResponse
	req, err = c.prepareRequest(http.MethodDelete, urlSuffix, nil)
	if err != nil {
		glog.Errorf("Create delete harbor project memeber request failed: %s", err)
		return
	}
	resp, err = c.DoRequest(req)
	if err != nil {
		glog.Errorf("Delete harbor project memeber failed: %s", err)
		return
	}
	switch resp.StatusCode {
	case http.StatusOK:
		err = nil
	default:
		err = fmt.Errorf("Harbor response: %s", resp.Body)
	}
	return
}
