package internalversion

import (
	"bytes"
	"fmt"
	"io/ioutil"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"

	"net/http"

	"github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
)

var githubCodeRepoService = &devops.CodeRepoService{
	Spec: devops.CodeRepoServiceSpec{
		ToolSpec: devops.ToolSpec{
			HTTP: devops.HostPort{
				Host: "https://api.github.com",
			},
		},
		Type:   devops.CodeRepoServiceTypeGithub,
		Public: true,
	},
}
var githubSecret = &corev1.Secret{
	Type: corev1.SecretTypeBasicAuth,
	Data: map[string][]byte{
		"username": []byte("--"),
		"password": []byte("--"),
	},
}

var userResponseJson = `{
    "login": "chengjingtao",
    "id": 1991282,
    "node_id": "MDQ6VXNlcjE5OTEyODI=",
    "avatar_url": "https://avatars0.githubusercontent.com/u/1991282?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/chengjingtao",
    "html_url": "https://github.com/chengjingtao",
    "followers_url": "https://api.github.com/users/chengjingtao/followers",
    "following_url": "https://api.github.com/users/chengjingtao/following{/other_user}",
    "gists_url": "https://api.github.com/users/chengjingtao/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/chengjingtao/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/chengjingtao/subscriptions",
    "organizations_url": "https://api.github.com/users/chengjingtao/orgs",
    "repos_url": "https://api.github.com/users/chengjingtao/repos",
    "events_url": "https://api.github.com/users/chengjingtao/events{/privacy}",
    "received_events_url": "https://api.github.com/users/chengjingtao/received_events",
    "type": "User",
    "site_admin": false,
    "name": "chengjt",
    "company": "Alauda",
    "blog": "",
    "location": null,
    "email": "cjt0616@hotmail.com",
    "hireable": null,
    "bio": null,
    "public_repos": 41,
    "public_gists": 1,
    "followers": 11,
    "following": 13,
    "created_at": "2012-07-17T12:39:07Z",
    "updated_at": "2018-12-12T06:49:13Z",
    "private_gists": 0,
    "total_private_repos": 0,
    "owned_private_repos": 0,
    "disk_usage": 16223,
    "collaborators": 0,
    "two_factor_authentication": false,
    "plan": {
        "name": "free",
        "space": 976562499,
        "collaborators": 0,
        "private_repos": 10000
    }
}`

var listOrgsResponseJson = `[
    {
        "login": "jenkinsci",
        "id": 107424,
        "node_id": "MDEyOk9yZ2FuaXphdGlvbjEwNzQyNA==",
        "url": "https://api.github.com/orgs/jenkinsci",
        "repos_url": "https://api.github.com/orgs/jenkinsci/repos",
        "events_url": "https://api.github.com/orgs/jenkinsci/events",
        "hooks_url": "https://api.github.com/orgs/jenkinsci/hooks",
        "issues_url": "https://api.github.com/orgs/jenkinsci/issues",
        "members_url": "https://api.github.com/orgs/jenkinsci/members{/member}",
        "public_members_url": "https://api.github.com/orgs/jenkinsci/public_members{/member}",
        "avatar_url": "https://avatars0.githubusercontent.com/u/107424?v=4",
        "description": "Jenkins is an open source automation server with an unparalleled plugin ecosystem to support practically every tool as part of your delivery pipelines"
    },
    {
        "login": "alauda",
        "id": 22073292,
        "node_id": "MDEyOk9yZ2FuaXphdGlvbjIyMDczMjky",
        "url": "https://api.github.com/orgs/alauda",
        "repos_url": "https://api.github.com/orgs/alauda/repos",
        "events_url": "https://api.github.com/orgs/alauda/events",
        "hooks_url": "https://api.github.com/orgs/alauda/hooks",
        "issues_url": "https://api.github.com/orgs/alauda/issues",
        "members_url": "https://api.github.com/orgs/alauda/members{/member}",
        "public_members_url": "https://api.github.com/orgs/alauda/public_members{/member}",
        "avatar_url": "https://avatars1.githubusercontent.com/u/22073292?v=4",
        "description": ""
    },
    {
        "login": "alauda-ci-test",
        "id": 31463068,
        "node_id": "MDEyOk9yZ2FuaXphdGlvbjMxNDYzMDY4",
        "url": "https://api.github.com/orgs/alauda-ci-test",
        "repos_url": "https://api.github.com/orgs/alauda-ci-test/repos",
        "events_url": "https://api.github.com/orgs/alauda-ci-test/events",
        "hooks_url": "https://api.github.com/orgs/alauda-ci-test/hooks",
        "issues_url": "https://api.github.com/orgs/alauda-ci-test/issues",
        "members_url": "https://api.github.com/orgs/alauda-ci-test/members{/member}",
        "public_members_url": "https://api.github.com/orgs/alauda-ci-test/public_members{/member}",
        "avatar_url": "https://avatars3.githubusercontent.com/u/31463068?v=4",
        "description": null
    },
    {
        "login": "jtcheng-org",
        "id": 44767115,
        "node_id": "MDEyOk9yZ2FuaXphdGlvbjQ0NzY3MTE1",
        "url": "https://api.github.com/orgs/jtcheng-org",
        "repos_url": "https://api.github.com/orgs/jtcheng-org/repos",
        "events_url": "https://api.github.com/orgs/jtcheng-org/events",
        "hooks_url": "https://api.github.com/orgs/jtcheng-org/hooks",
        "issues_url": "https://api.github.com/orgs/jtcheng-org/issues",
        "members_url": "https://api.github.com/orgs/jtcheng-org/members{/member}",
        "public_members_url": "https://api.github.com/orgs/jtcheng-org/public_members{/member}",
        "avatar_url": "https://avatars2.githubusercontent.com/u/44767115?v=4",
        "description": null
    },
    {
        "login": "jtcheng-test",
        "id": 45840882,
        "node_id": "MDEyOk9yZ2FuaXphdGlvbjQ1ODQwODgy",
        "url": "https://api.github.com/orgs/jtcheng-test",
        "repos_url": "https://api.github.com/orgs/jtcheng-test/repos",
        "events_url": "https://api.github.com/orgs/jtcheng-test/events",
        "hooks_url": "https://api.github.com/orgs/jtcheng-test/hooks",
        "issues_url": "https://api.github.com/orgs/jtcheng-test/issues",
        "members_url": "https://api.github.com/orgs/jtcheng-test/members{/member}",
        "public_members_url": "https://api.github.com/orgs/jtcheng-test/public_members{/member}",
        "avatar_url": "https://avatars3.githubusercontent.com/u/45840882?v=4",
        "description": null
    }
]`

var listEmptyTeamsResponseJson = `[]`

var _ = Describe("GitHubClient", func() {

	var (
		ctl           *gomock.Controller
		githubClient  *GithubClient
		roundTripper  *mhttp.MockRoundTripper
		serviceClient *ServiceClient
	)

	BeforeEach(func() {
		serviceClient = NewServiceClient(githubCodeRepoService, githubSecret)
		ctl = gomock.NewController(GinkgoT())
		roundTripper = mhttp.NewMockRoundTripper(ctl)
		githubClient, _ = NewGithubClient(serviceClient, roundTripper)
	})

	Describe("GitHubClient.ListProjectDataList", func() {

		Context("When got orgs from github", func() {

			It("Should return projectDataList", func() {
				listTeamsRequest, _ := http.NewRequest(http.MethodGet, "/user/orgs", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listTeamsRequest)).Return(newHttpResponse(http.StatusOK, listOrgsResponseJson), nil)
				listUserRequest, _ := http.NewRequest(http.MethodGet, "/user", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewVerboseRequestMatcher(listUserRequest)).Return(newHttpResponse(http.StatusOK, userResponseJson), nil)

				projectDataList, err := githubClient.ListProjectDataList(devops.ListProjectOptions{})
				gomega.Expect(err).To(gomega.BeNil())
				gomega.Expect(len(projectDataList.Items)).To(gomega.BeEquivalentTo(6))
				gomega.Expect(projectDataList.Items[0].Name).To(gomega.BeEquivalentTo("jenkinsci"))

				gomega.Expect(projectDataList.Items[5].Name).To(gomega.BeEquivalentTo("chengjingtao"))

			})
		})

		Context("When got none org from github", func() {

			It("Should return projectDataList with empty items", func() {
				listTeamsRequest, _ := http.NewRequest(http.MethodGet, "/user/orgs", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listTeamsRequest)).Return(newHttpResponse(http.StatusOK, listEmptyTeamsResponseJson), nil)

				listUserRequest, _ := http.NewRequest(http.MethodGet, "/user", nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(listUserRequest)).Return(newHttpResponse(http.StatusOK, userResponseJson), nil)

				projectDataList, err := githubClient.ListProjectDataList(devops.ListProjectOptions{})
				gomega.Expect(err).To(gomega.BeNil())
				gomega.Expect(len(projectDataList.Items)).To(gomega.BeEquivalentTo(1))

				gomega.Expect(projectDataList.Items[0].Name).To(gomega.BeEquivalentTo("chengjingtao"))

			})
		})

	})

})

func newHttpResponse(statusCode int, body string) *http.Response {
	resp := &http.Response{
		Status: fmt.Sprintf("%d", statusCode),
		Header: http.Header{
			"Content-Type": []string{
				"application/json",
			},
		},
		StatusCode: statusCode,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 0,
		Body:       ioutil.NopCloser(bytes.NewBufferString(fmt.Sprintf(body))),
	}
	resp.Header.Set("X-RateLimit-Limit", "5000")
	resp.Header.Set("X-RateLimit-Remaining", "4999")
	resp.Header.Set("X-RateLimit-Reset", "1576733956")
	return resp
}
