package internalversion

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/generic"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"github.com/dustin/go-humanize"
	"github.com/google/go-github/github"
	"golang.org/x/oauth2"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

var (
	DefaultGitHubTeamPermission = "push"
	defaultBaseURL              = "https://api.github.com/"
)

type GithubClient struct {
	*ServiceClient
	Client       *github.Client
	devopsClient devopsclient.Interface
	//TODO cxt should not be reused
	Cxt context.Context
}

func NewGithubClient(serviceClient *ServiceClient, roundTripper http.RoundTripper) (*GithubClient, error) {
	var (
		cxt          = context.Background()
		err          error
		httpClient   *http.Client
		devopsClient devopsclient.Interface
	)

	if roundTripper == nil {
		roundTripper = generic.GetDefaultTransport()
	}

	if serviceClient.isBasicAuth() {
		tp := github.BasicAuthTransport{
			Username:  serviceClient.BasicAuthInfo.Username,
			Password:  serviceClient.BasicAuthInfo.Password,
			Transport: roundTripper,
		}
		httpClient = tp.Client()
		devopsClient = NewDevOpsClient("github", "", defaultBaseURL, devopsclient.NewBasicAuth(serviceClient.BasicAuthInfo.Username, serviceClient.BasicAuthInfo.Password),
			devopsclient.NewTransport(roundTripper))
	} else {
		serviceClient.OAuth2Info.AccessTokenKey = k8s.GithubAccessTokenKey
		serviceClient.OAuth2Info.Scope = k8s.GithubAccessTokenScope

		ts := oauth2.StaticTokenSource(
			&oauth2.Token{AccessToken: serviceClient.OAuth2Info.AccessToken},
		)

		httpClient = oauth2.NewClient(context.WithValue(
			cxt, oauth2.HTTPClient,
			&http.Client{
				Transport: roundTripper,
			}), ts)
		devopsClient = NewDevOpsClient("github", "", defaultBaseURL, devopsclient.NewAPIKey("Authorization", "header", fmt.Sprintf("token %s", serviceClient.OAuth2Info.AccessToken)),
			devopsclient.NewTransport(roundTripper))
	}

	host, err := url.Parse(serviceClient.getHost())
	if err != nil {
		return nil, err
	}
	if fmt.Sprintf("%s://%s/", "https", host.Host) != defaultBaseURL {
		client, err := github.NewEnterpriseClient(
			fmt.Sprintf("%s://%s/api/v3/", host.Scheme, host.Host),
			fmt.Sprintf("%s://%s/upload/", host.Scheme, host.Host),
			httpClient)
		if err != nil {
			return nil, err
		}
		return &GithubClient{ServiceClient: serviceClient, Client: client, Cxt: cxt, devopsClient: devopsClient}, err
	}
	return &GithubClient{ServiceClient: serviceClient, Client: github.NewClient(httpClient), Cxt: cxt, devopsClient: devopsClient}, err
}

func (c *GithubClient) CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.getHost(), nil)
}

func (c *GithubClient) CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	url := fmt.Sprintf("%suser", c.getBaseUrl())
	return CheckService(url, c.getHeader(lastModifyAt))
}

func (c *GithubClient) GetLatestRepoCommit(repoID, owner, repoName, repoFullName string) (commit *devops.RepositoryCommit, status *devops.HostPortStatus) {
	return GetLatestRepoCommit(c.devopsClient, repoID, owner, repoName, repoFullName)
}

func (c *GithubClient) getAllRepos() (githubRepositories []*github.Repository, err error) {
	var (
		wg = sync.WaitGroup{}
	)

	glog.V(5).Infof("fetch data in page %d", 1)
	firstPageResult, response, err1 := c.Client.Repositories.List(c.Cxt, "", &github.RepositoryListOptions{ListOptions: github.ListOptions{Page: 1, PerPage: k8s.PerPage}})
	if err1 != nil {
		err = err1
		glog.Errorf("Error list repositories for the user %s. err: %v", c.getUsername(), err)
		return
	}

	if firstPageResult != nil {
		githubRepositories = append(githubRepositories, firstPageResult...)
	}

	if response == nil || response.LastPage < 2 {
		glog.V(5).Infof("No next page, return current result")
		return
	}

	for page := 2; page < response.LastPage+1; page++ {
		glog.V(5).Infof("fetch data in page %d", page)
		wg.Add(1)
		go func(page, perPage int) {
			defer wg.Done()

			pagedResult, _, err := c.Client.Repositories.List(c.Cxt, "", &github.RepositoryListOptions{ListOptions: github.ListOptions{Page: page, PerPage: perPage}})
			if err != nil {
				return
			}
			if pagedResult != nil {
				githubRepositories = append(githubRepositories, pagedResult...)
			}
		}(page, k8s.PerPage)
	}
	wg.Wait()

	return
}

func (c *GithubClient) GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error) {
	return GetRemoteRepos(c.devopsClient)
}

func (c *GithubClient) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if githubRepo, ok := remoteRepo.(*github.Repository); ok {
		codeRepo = devops.OriginCodeRepository{
			CodeRepoServiceType: devops.CodeRepoServiceTypeGithub,
			ID:                  strconv.FormatInt(githubRepo.GetID(), 10),
			Name:                githubRepo.GetName(),
			FullName:            githubRepo.GetFullName(),
			Description:         githubRepo.GetDescription(),
			HTMLURL:             githubRepo.GetHTMLURL(),
			CloneURL:            githubRepo.GetCloneURL(),
			SSHURL:              githubRepo.GetSSHURL(),
			Language:            githubRepo.GetLanguage(),
			Owner: devops.OwnerInRepository{
				ID:   strconv.FormatInt(githubRepo.Owner.GetID(), 10),
				Name: githubRepo.GetOwner().GetLogin(),
				Type: devops.GetOwnerType(githubRepo.GetOwner().GetType()),
			},

			CreatedAt: ConvertToMetaTime(githubRepo.GetCreatedAt()),
			PushedAt:  ConvertToMetaTime(githubRepo.GetPushedAt()),
			UpdatedAt: ConvertToMetaTime(githubRepo.GetUpdatedAt()),

			Private:      githubRepo.GetPrivate(),
			Size:         int64(githubRepo.GetSize() * 1000),
			SizeHumanize: humanize.Bytes(uint64(githubRepo.GetSize() * 1000)),
		}
	}

	return
}

// getRateLimitUrl access this url will not waste the rate limit
func (c *GithubClient) getRateLimitUrl() string {
	return fmt.Sprintf("%srate_limit", c.Client.BaseURL)
}

func (c *GithubClient) getRepoUrl(repoName string) string {
	return fmt.Sprintf("%srepos/%s", c.getBaseUrl(), repoName)
}

func (c *GithubClient) GetAuthorizeUrl(redirectUrl string) string {
	return fmt.Sprintf("%s/login/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		c.getHtml(), c.getClientID(), redirectUrl, c.getScope())
}

func (c *GithubClient) getAccessTokenUrl() string {
	return fmt.Sprintf("%s/login/oauth/access_token", c.getHtml())
}

func (c *GithubClient) AccessToken(redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessTokenUrl()
		response       = &AccessTokenResponseGithub{}
	)

	return c.accessToken(response, redirectUrl, accessTokenUrl)
}

func (c *GithubClient) getBaseUrl() string {
	return c.Client.BaseURL.String()
}

func (c *GithubClient) CreateProject(options devops.CreateProjectOptions) (*devops.ProjectData, error) {
	return nil, errors.NewMethodNotSupported(corev1.Resource("ProjectData"), http.MethodPost)
}

// ListProjectDataList will list all groups or teams
func (c *GithubClient) ListProjectDataList(options devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	return ListCodeRepoProjects(c.devopsClient, options)
}

// createOrgTeams  create team for github
// Limit by github, we are not support create org
// Up to now, the method will not be used
func (c *GithubClient) createOrgTeams(options devops.CreateProjectOptions) (*devops.ProjectData, error) {

	var teamPath = options.Name
	orgName, teamName, err := splitOrgTeam(teamPath)
	if err != nil {
		return nil, err
	}

	if teamName == "" {
		// teamName is required
		return nil, errors.NewBadRequest(fmt.Sprintf("format of '%s' is invalid", teamPath))
	}

	githubOrg, err := c.getGithubOrg(orgName)
	if err != nil {
		return nil, err
	}

	if githubOrg == nil {
		glog.Errorf("GitHub Org '%s' does not exists", orgName)
		return nil, errors.NewNotFound(corev1.Resource("ProjectData"), orgName)
	}

	// team name is not empty, we should try to create it
	githubTeam, err := c.getGithubTeam(orgName, teamName)
	if err != nil {
		return nil, err
	}
	if githubTeam != nil {
		glog.Errorf("GitHub team '%s/%s' is alreay exists", orgName, teamName)
		return nil, errors.NewAlreadyExists(corev1.Resource("ProjectData"), fmt.Sprintf("%s/%s", orgName, teamName))
	}
	glog.V(5).Infof("Creating github team %s/%s", orgName, teamName)
	githubTeam, err = c.createGitHubTeam(orgName, buildGitHubNewTeam(teamName, options))
	if err != nil {
		return nil, err
	}
	glog.V(5).Infof("Created github team %s/%s", orgName, teamName)

	return githubTeamAsProjectData(orgName, *githubTeam)
}

func (c *GithubClient) createGitHubTeam(orgName string, team github.NewTeam) (*github.Team, error) {
	githubTeam, response, err := c.Client.Organizations.CreateTeam(c.Cxt, orgName, &team)
	if err != nil {
		glog.Errorf("Create github org team %s/%s error:%s", orgName, team.Name, err.Error())
		return nil, err
	}
	if githubTeam == nil {
		err := fmt.Errorf("Create github org team %s/%s got unknown response", orgName, team.Name)
		glog.Errorf("%s :%#v", err.Error(), response)
		return nil, err
	}

	return githubTeam, nil
}

func buildGitHubNewTeam(teamName string, options devops.CreateProjectOptions) github.NewTeam {
	githubNewTeam := github.NewTeam{
		Name:       teamName,
		Permission: &DefaultGitHubTeamPermission,
	}

	glog.V(9).Infof("build github create team options, name:%s", teamName)
	return githubNewTeam
}

func githubOrgAsProjectData(gitHubOrg github.Organization) (*devops.ProjectData, error) {
	data, err := generic.MarshalToMapString(gitHubOrg)
	if err != nil {
		glog.Errorf("mashall github org data %#v error:%#v", gitHubOrg, err)
		return nil, err
	}

	var projectData = &devops.ProjectData{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.ResourceKindProjectData,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: gitHubOrg.GetLogin(),
			Annotations: map[string]string{
				devops.AnnotationsProjectDataAvatarURL:   gitHubOrg.GetAvatarURL(),
				devops.AnnotationsProjectDataAccessPath:  "/" + gitHubOrg.GetLogin(),
				devops.AnnotationsProjectDataDescription: gitHubOrg.GetDescription(),
				devops.AnnotationsProjectDataType:        string(devops.OriginCodeRepoRoleTypeOrg),
			},
		},
		Data: data,
	}
	return projectData, nil
}

func githubUserAsProjectData(gitHubUser github.User) *devops.ProjectData {

	var projectData = &devops.ProjectData{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.ResourceKindProjectData,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: gitHubUser.GetLogin(),
			Annotations: map[string]string{
				devops.AnnotationsProjectDataAvatarURL:   gitHubUser.GetAvatarURL(),
				devops.AnnotationsProjectDataAccessPath:  "/" + gitHubUser.GetLogin(),
				devops.AnnotationsProjectDataDescription: "",
				devops.AnnotationsProjectDataType:        string(devops.OriginCodeRepoOwnerTypeUser),
			},
		},
		Data: map[string]string{
			"login":   gitHubUser.GetLogin(),
			"id":      fmt.Sprint(gitHubUser.GetID()),
			"name":    gitHubUser.GetName(),
			"company": gitHubUser.GetCompany(),
			"type":    gitHubUser.GetType(),
			"email":   gitHubUser.GetEmail(),
		},
	}
	return projectData
}

func githubTeamAsProjectData(orgName string, gitHubTeam github.Team) (*devops.ProjectData, error) {
	data, err := generic.MarshalToMapString(gitHubTeam)
	if err != nil {
		glog.Errorf("marshall github team data %#v error:%#v", gitHubTeam, err)
		return nil, err
	}

	var projectData = &devops.ProjectData{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.ResourceKindProjectData,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: orgName + "/" + gitHubTeam.GetName(),
			Annotations: map[string]string{
				devops.AnnotationsProjectDataAvatarURL:   "",
				devops.AnnotationsProjectDataAccessPath:  githubAccessPath(orgName, gitHubTeam.GetName()),
				devops.AnnotationsProjectDataDescription: gitHubTeam.GetDescription(),
			},
		},
		Data: data,
	}
	return projectData, nil
}

func splitOrgTeam(path string) (orgName, teamName string, err error) {
	if path == "" {
		return "", "", fmt.Errorf("error format of path, it cannot be empty")
	}

	segments := strings.Split(path, "/")
	if len(segments) > 2 {
		err := fmt.Errorf("error format of orgteam path '%s', segments should not be more than 2", path)
		glog.Error(err)
		return "", "", err
	}

	orgName = segments[0]
	if len(segments) == 2 {
		teamName = segments[1]
	}
	return
}

// listGithubTeam will get org teams from github server, it will return nil when team is not exists.
func (c *GithubClient) listGithubTeams(githubOrgName string) ([]*github.Team, error) {
	githubTeams, response, err := c.Client.Organizations.ListTeams(c.Cxt, githubOrgName, &github.ListOptions{})

	if err != nil {
		glog.Errorf("List GitHub '%s' teams error:%s", githubOrgName, err.Error())
		return nil, err
	}

	if githubTeams == nil {
		glog.Errorf("List GitHub '%s' teams error, response is:%#v", githubOrgName, response)
		return nil, errors.NewInternalError(fmt.Errorf("Error to get github %s org teams", githubOrgName))
	}

	return githubTeams, nil
}

// getGithubTeam will get org team from github server, it will return nil when team is not exists.
func (c *GithubClient) getGithubTeam(githubOrgName string, teamName string) (*github.Team, error) {
	githubTeams, err := c.listGithubTeams(githubOrgName)
	if err != nil {
		return nil, err
	}

	for _, team := range githubTeams {
		if team.GetName() == teamName {
			return team, nil
		}
	}

	return nil, nil
}

// getGithubOrg will get org from github server, it will return nil when org is not exists.
func (c *GithubClient) getGithubOrg(githubOrgName string) (*github.Organization, error) {
	githubOrg, response, err := c.Client.Organizations.Get(c.Cxt, githubOrgName)

	if err != nil {
		if response.StatusCode == http.StatusNotFound {
			glog.V(7).Infof("GitHub Org '%s' does not exists", githubOrgName)
			return nil, nil
		}
		glog.Errorf("Get GitHub org '%s' error:%s", githubOrgName, err.Error())
		return nil, err
	}

	if githubOrg == nil {
		glog.Errorf("Get GitHub org '%s' error, response is:%#v", githubOrgName, response)
		return nil, errors.NewInternalError(fmt.Errorf("Error to get github org %s", githubOrgName))
	}

	return githubOrg, nil
}

func (c *GithubClient) currentUser() (*devops.ProjectData, error) {
	cxt := context.TODO()
	user, _, err := c.Client.Users.Get(cxt, "")
	if err != nil {
		return nil, err
	}

	return githubUserAsProjectData(*user), nil

}

func (c *GithubClient) listOrgs(options devops.ListProjectOptions) (*devops.ProjectDataList, error) {

	githubListOrgOptions, err := buildGitHubListOrgOptions(options)
	if err != nil {
		return nil, err
	}

	githubOrgs, err := c.listGitHubOrgs(githubListOrgOptions)
	if err != nil {
		return nil, err
	}

	projectDataList, err := githubOrgsAsProjectDataList(githubOrgs)
	if err != nil {
		return nil, err
	}

	return projectDataList, err
}

func (c *GithubClient) listGitHubOrgs(options *github.ListOptions) ([]*github.Organization, error) {
	githubOrgs, response, err := c.Client.Organizations.List(c.Cxt, "", options)
	if err != nil {
		glog.Errorf("list github orgs error, github listOrgsOptions:%#v, error:%s", options, err.Error())
		return []*github.Organization{}, err
	}
	if githubOrgs == nil {
		err := fmt.Errorf("list github orgs got unknow error")
		glog.Errorf("%s, options %#v , response: %#v", err.Error(), options, response)
		return nil, errors.NewInternalError(err)
	}
	return githubOrgs, nil
}

// listTeams will list all teams of orgs as ProjectData, it not be invoked up to now
func (c *GithubClient) listTeams(githubOrgs []*github.Organization) ([]devops.ProjectData, error) {
	waitGroup := sync.WaitGroup{}
	allDone := make(chan struct{})
	failFast := make(chan error)
	items := []devops.ProjectData{}
	var err error

	waitGroup.Add(len(githubOrgs))

	for _, org := range githubOrgs {
		var githubOrg = org
		go func() {
			glog.V(5).Infof("listing GitHub %s's teams", githubOrg.GetLogin())
			defer waitGroup.Done()
			teams, err := c.listGithubTeams(githubOrg.GetLogin())
			if err != nil {
				failFast <- err
				return
			}
			glog.V(5).Infof("Listed GitHub %s's teams %d", githubOrg.GetLogin(), len(teams))
			list, err := teamsAsProjectDataList(githubOrg.GetLogin(), teams)
			if err != nil {
				failFast <- err
				return
			}
			items = append(items, list.Items...)
		}()
	}

	go func() {
		waitGroup.Wait()
		close(allDone)
	}()

	select {
	case <-allDone:
	case err = <-failFast:
		close(failFast)
	}

	return items, err
}

func buildGitHubListOrgOptions(options devops.ListProjectOptions) (*github.ListOptions, error) {
	if options.Page == "" {
		options.Page = "0"
	}
	if options.PageSize == "" {
		options.PageSize = fmt.Sprint(k8s.PerPage)
	}

	page, err := strconv.Atoi(options.Page)
	if err != nil {
		glog.Errorf("page %s in List project options is invalid", options.Page)
		return nil, err
	}
	pageSize, err := strconv.Atoi(options.PageSize)
	if err != nil {
		glog.Errorf("pageSize %s in List project options is invalid", options.Page)
		return nil, err
	}

	var githubListOptions = &github.ListOptions{
		Page:    page,
		PerPage: pageSize,
	}
	return githubListOptions, nil
}

func githubOrgsAsProjectDataList(orgs []*github.Organization) (*devops.ProjectDataList, error) {
	items := []devops.ProjectData{}

	for _, org := range orgs {
		projectData, err := githubOrgAsProjectData(*org)
		if err != nil {
			glog.Errorf(err.Error())
			return nil, err
		}

		items = append(items, *projectData)
	}

	return &devops.ProjectDataList{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.ResourceKindProjectDataList,
		},
		Items: items,
	}, nil
}

func teamsAsProjectDataList(orgName string, teams []*github.Team) (*devops.ProjectDataList, error) {
	items := []devops.ProjectData{}

	for _, team := range teams {
		projectData, err := githubTeamAsProjectData(orgName, *team)
		if err != nil {
			glog.Errorf(err.Error())
			return nil, err
		}

		items = append(items, *projectData)
	}

	return &devops.ProjectDataList{
		TypeMeta: metav1.TypeMeta{
			Kind: devops.ResourceKindProjectDataList,
		},
		Items: items,
	}, nil
}

func githubAccessPath(orgName string, teamName string) string {
	return fmt.Sprintf("/orgs/%s/teams/%s", orgName, teamName)
}

func (*GithubClient) GetRolesMapping(*devops.RoleMappingListOptions) (devops.RoleMapping, error) {
	return devops.RoleMapping{}, errors.NewMethodNotSupported(corev1.Resource("RoleMapping"), http.MethodGet)
}
func (*GithubClient) ApplyRoleMapping(roleMapping *devops.RoleMapping) (err error) {
	return errors.NewMethodNotSupported(corev1.Resource("RoleMapping"), http.MethodPost)
}

const (
	MaxGithubBranchPerPage = 100
)

func (c *GithubClient) GetBranches(owner, repoName, repoFullName string) ([]devops.CodeRepoBranch, error) {
	return GetBranches(c.devopsClient, owner, repoName, repoFullName)
}

func (c *GithubClient) listBranches(owner, repo string) ([]*github.Branch, error) {
	var branches []*github.Branch
	hasNextPage := true
	// per_page is at most 100
	for page := 1; hasNextPage; page++ {
		options := &github.ListOptions{Page: page, PerPage: MaxGithubBranchPerPage}
		items, response, err := c.Client.Repositories.ListBranches(c.Cxt, owner, repo, options)
		if err != nil {
			return []*github.Branch{}, err
		}
		if items == nil {
			err := fmt.Errorf("list github branch got unknow error")
			return []*github.Branch{}, errors.NewInternalError(err)
		}
		if response.NextPage <= page {
			hasNextPage = false
		}
		branches = append(branches, items...)
	}
	return branches, nil
}
