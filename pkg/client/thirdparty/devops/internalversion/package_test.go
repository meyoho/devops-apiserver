package internalversion

import (
	. "github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
	"testing"
)

func TestInternalversion(t *testing.T) {
	gomega.RegisterFailHandler(Fail)
	RunSpecs(t, "pkg/client/thirdparty/devops/internalversion")
}
