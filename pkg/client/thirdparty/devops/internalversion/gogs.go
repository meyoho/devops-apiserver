package internalversion

import (
	"errors"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devopsclient "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	corev1 "k8s.io/api/core/v1"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type GogsClient struct {
	*ServiceClient
	devopsClient devopsclient.Interface
}

func NewGogsClient(serviceClient *ServiceClient, roundTripper http.RoundTripper) (*GogsClient, error) {
	if serviceClient.isBasicAuth() {
		token := serviceClient.getUsername()
		// user secret field username needs to use token due to Gogs issue: https://github.com/gogs/gogs/issues/3866
		// otherwise, pipeline can not clone code.
		devopsClient := NewDevOpsClient("gogs", "", serviceClient.getHost(),
			devopsclient.NewAPIKey("Authorization", "header", fmt.Sprintf("token %s", token)),
			devopsclient.NewTransport(roundTripper))

		return &GogsClient{ServiceClient: serviceClient, devopsClient: devopsClient}, nil
	}

	return nil, errors.New("gogs configuration error, unknown authorization")
}

func (c *GogsClient) GetRemoteRepos() (result *devops.CodeRepoBindingRepositories, err error) {
	return GetRemoteRepos(c.devopsClient)
}

func (c *GogsClient) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo devops.OriginCodeRepository) {
	// actually we do not need it when use devops cient
	return devops.OriginCodeRepository{}
}

func (c *GogsClient) CheckAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckAvailable(c.devopsClient)
}

func (c *GogsClient) CheckAuthentication(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckAuthentication(c.devopsClient)
}

func (c *GogsClient) GetLatestRepoCommit(repoID, owner, repoName, repoFullName string) (commit *devops.RepositoryCommit, status *devops.HostPortStatus) {
	return GetLatestRepoCommit(c.devopsClient, repoID, owner, repoName, repoFullName)
}

func (c *GogsClient) AccessToken(redirectUrl string) (secret *corev1.Secret, err error) {
	return nil, errors.New("gogs does not support oauth2")
}

func (c *GogsClient) GetAuthorizeUrl(redirectUrl string) string {
	return ""
}

func (c *GogsClient) GetBranches(owner, repoName, repoFullName string) (branches []devops.CodeRepoBranch, err error) {
	return GetBranches(c.devopsClient, owner, repoName, repoFullName)
}

func (c *GogsClient) CreateProject(options devops.CreateProjectOptions) (*devops.ProjectData, error) {
	return CreateCodeRepoProject(c.devopsClient, options)
}

func (c *GogsClient) ListProjectDataList(options devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	return ListCodeRepoProjects(c.devopsClient, options)
}

func (c *GogsClient) GetRolesMapping(*devops.RoleMappingListOptions) (devops.RoleMapping, error) {
	return devops.RoleMapping{}, kerrors.NewMethodNotSupported(corev1.Resource("RoleMapping"), http.MethodGet)
}

func (c *GogsClient) ApplyRoleMapping(roleMapping *devops.RoleMapping) (err error) {
	return kerrors.NewMethodNotSupported(corev1.Resource("RoleMapping"), http.MethodPost)
}
