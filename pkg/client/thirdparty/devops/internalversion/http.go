package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/generic"

	"io/ioutil"
	"net/http"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	glog "k8s.io/klog"
)

func CheckService(url string, header map[string]string) (status *devops.HostPortStatus, err error) {
	var req *http.Request
	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}

	if header == nil {
		header = map[string]string{}
	}

	if len(header) > 0 {
		for key, val := range header {
			req.Header.Add(key, val)
		}
	}

	glog.V(3).Infof("Check Service url: %s; req.Header: %v", url, req.Header)
	client := generic.NewHTTPClient()
	return ConvertResponseToHostPortStatus(client.Do(req))
}

func ConvertResponseToHostPortStatus(response *http.Response, err error) (*devops.HostPortStatus, error) {
	var data []byte
	lastAttempt := metav1.Now()
	status := &devops.HostPortStatus{
		StatusCode:  -1,
		LastAttempt: &lastAttempt,
	}

	if err != nil {
		status.ErrorMessage = err.Error()
	}

	if response != nil {
		status.StatusCode = response.StatusCode
		if !response.Close {
			defer response.Body.Close()
			data, err = ioutil.ReadAll(response.Body)
			if err != nil {
				return status, err
			}
			status.Response = string(data)
		}
	}
	return status, err
}

func BasicAuth(url, username, password string) (status *devops.HostPortStatus, err error) {
	var (
		resp        *http.Response
		req         *http.Request
		start       = time.Now()
		duration    time.Duration
		lastAttempt = metav1.NewTime(start)
	)
	// prepare
	status = &devops.HostPortStatus{
		LastAttempt: &lastAttempt,
	}
	http.DefaultClient.Timeout = time.Second * 30
	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}
	if username != "" && password != "" {
		req.SetBasicAuth(username, password)
	}

	resp, err = http.DefaultClient.Do(req)
	if resp != nil {
		status.StatusCode = int(resp.StatusCode)
		data, err := ioutil.ReadAll(resp.Body)
		if data != nil {
			status.Response = string(data)
		} else {
			status.Response = ""
		}
		if err != nil {
			runtime.HandleError(err)
		}
	}
	duration = time.Since(start)
	status.Delay = &duration
	return
}

func BasicAuthRoundTripper(url, username, password string, roundTripper http.RoundTripper) (status *devops.HostPortStatus, err error) {
	var (
		resp        *http.Response
		req         *http.Request
		start       = time.Now()
		duration    time.Duration
		lastAttempt = metav1.NewTime(start)
	)
	// prepare
	status = &devops.HostPortStatus{
		LastAttempt: &lastAttempt,
	}
	http.DefaultClient.Timeout = time.Second * 30
	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}
	if username != "" && password != "" {
		req.SetBasicAuth(username, password)
	}

	resp, err = roundTripper.RoundTrip(req)
	if resp != nil {
		status.StatusCode = int(resp.StatusCode)
		data, err := ioutil.ReadAll(resp.Body)
		if data != nil {
			status.Response = string(data)
		} else {
			status.Response = ""
		}
		if err != nil {
			runtime.HandleError(err)
		}
	}
	duration = time.Since(start)
	status.Delay = &duration
	return
}
