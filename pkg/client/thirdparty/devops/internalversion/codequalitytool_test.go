package internalversion_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	devops2 "alauda.io/devops-apiserver/pkg/client/thirdparty/devops"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/devops/internalversion"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var jsonDevOpsConfigMap = `{
    "apiVersion": "v1",
    "data": {
        "_domain": "{\"toolChains\":[{\"name\":\"codeRepository\",\"displayName\":{\"en\":\"Code Repository\",\"zh\":\"代码仓库\"},\"enabled\":true,\"apiPath\":\"\",\"items\":[{\"displayName\":{\"en\":\"Github\",\"zh\":\"Github\"},\"host\":\"https://api.github.com\",\"html\":\"https://github.com\",\"name\":\"github\",\"kind\":\"codereposervice\",\"type\":\"Github\",\"public\":true,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"codereposervices\",\"supportedSecretTypes\":[{\"type\":\"devops.alauda.io/oauth2\",\"description\":{\"en\":\"Input respectively OAuth App's Client ID and Client Secret\",\"zh\":\"分别为设置页面 OAuth Apps 的 Client ID 和 Client Secret\"}},{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Username is same as used on login and Password is same as Personal Access Token generated in Settings page\",\"zh\":\"用户名输入登录时的用户名，密码为设置页面的 Personal Access Token\"}}]},{\"displayName\":{\"en\":\"Gitlab\",\"zh\":\"Gitlab\"},\"host\":\"https://gitlab.com\",\"html\":\"https://gitlab.com\",\"name\":\"gitlab\",\"kind\":\"codereposervice\",\"type\":\"Gitlab\",\"public\":true,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"codereposervices\",\"supportedSecretTypes\":[{\"type\":\"devops.alauda.io/oauth2\",\"description\":{\"en\":\"Input respectively OAuth Application's Application ID in the Client ID field and Secret in the Client Secret\",\"zh\":\"分别为设置页面 Application 的 Application ID 和 Secret\"}},{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Username is same as used on login and Password is same as Personal Access Token generated in Settings page\",\"zh\":\"用户名输入登录时的用户名，密码为设置页面的 Personal Access Token\"}}]},{\"displayName\":{\"en\":\"Gitlab Enterprise\",\"zh\":\"Gitlab 企业版\"},\"host\":\"\",\"html\":\"\",\"name\":\"gitlab-enterprise\",\"kind\":\"codereposervice\",\"type\":\"Gitlab\",\"public\":false,\"enterprise\":true,\"enabled\":true,\"apiPath\":\"codereposervices\",\"supportedSecretTypes\":[{\"type\":\"devops.alauda.io/oauth2\",\"description\":{\"en\":\"Input respectively OAuth Application's Application ID in the Client ID field and Secret in the Client Secret\",\"zh\":\"分别为设置页面 Application 的 Application ID 和 Secret\"}},{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Username is same as used on login and Password is same as Personal Access Token generated in Settings page\",\"zh\":\"用户名输入登录时的用户名，密码为设置页面的 Personal Access Token\"}}]},{\"displayName\":{\"en\":\"Gitee\",\"zh\":\"码云\"},\"host\":\"https://gitee.com\",\"html\":\"https://gitee.com\",\"name\":\"gitee\",\"kind\":\"codereposervice\",\"type\":\"Gitee\",\"public\":true,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"codereposervices\",\"supportedSecretTypes\":[{\"type\":\"devops.alauda.io/oauth2\",\"description\":{\"en\":\"Input respectively OAuth Application's Application ID in the Client ID field and Secret in the Client Secret\",\"zh\":\"分别为设置页面应用的 Client ID 和 Client Secret\"}},{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Username is same as used on login and Password is same as Personal Access Token generated in Settings page\",\"zh\":\"用户名输入登录时的用户名，密码为设置页面的私人令牌\"}}]},{\"displayName\":{\"en\":\"Gitee Enterprise\",\"zh\":\"码云企业版\"},\"host\":\"\",\"html\":\"\",\"name\":\"gitee-enterprise\",\"kind\":\"codereposervice\",\"type\":\"Gitee\",\"public\":false,\"enterprise\":true,\"enabled\":true,\"apiPath\":\"codereposervices\",\"supportedSecretTypes\":[{\"type\":\"devops.alauda.io/oauth2\",\"description\":{\"en\":\"Input respectively OAuth Application's Application ID in the Client ID field and Secret in the Client Secret\",\"zh\":\"分别为设置页面应用的 Client ID 和 Client Secret\"}},{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Username is same as used on login and Password is same as Personal Access Token generated in Settings page\",\"zh\":\"用户名输入登录时的用户名，密码为设置页面的私人令牌\"}}]},{\"displayName\":{\"en\":\"Bitbucket\",\"zh\":\"Bitbucket\"},\"host\":\"https://api.bitbucket.org\",\"html\":\"https://bitbucket.org\",\"name\":\"bitbucket\",\"kind\":\"codereposervice\",\"type\":\"Bitbucket\",\"public\":true,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"codereposervices\",\"supportedSecretTypes\":[{\"type\":\"devops.alauda.io/oauth2\",\"description\":{\"en\":\"Input respectively OAuth Consumer's Key in the Client ID field and Secret in the Client Secret field\",\"zh\":\"分别为设置页面 OAuth consumers 的 Key 和 Secret\"}},{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Username is same as used on login and Password is same as App passwords generated in Settings page\",\"zh\":\"用户名输入登录时的用户名，密码为设置页面的 App passwords\"}}]}]},{\"name\":\"continuousIntegration\",\"displayName\":{\"en\":\"Continuous Integration\",\"zh\":\"持续集成\"},\"enabled\":true,\"apiPath\":\"\",\"items\":[{\"displayName\":{\"en\":\"Jenkins\",\"zh\":\"Jenkins\"},\"host\":\"\",\"html\":\"\",\"name\":\"jenkins\",\"kind\":\"jenkins\",\"type\":\"\",\"public\":false,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"jenkinses\",\"supportedSecretTypes\":[{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Input username as used on login in the Username field and the user token in the Password field\",\"zh\":\"用户名输入登录时的用户名，密码为在 Jenkins 配置页面的 Token\"}}]}]},{\"name\":\"artifactRepository\",\"displayName\":{\"en\":\"Artifact Repository\",\"zh\":\"制品仓库\"},\"enabled\":true,\"apiPath\":\"\",\"items\":[{\"displayName\":{\"en\":\"Docker Registry\",\"zh\":\"Docker Registry\"},\"host\":\"\",\"html\":\"\",\"name\":\"docker-registry\",\"kind\":\"imageregistry\",\"type\":\"Docker\",\"public\":false,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"imageregistries\",\"supportedSecretTypes\":[{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Input Username and Password as used on login\",\"zh\":\"用户名和密码均为登录时的用户名和密码\"}}]},{\"displayName\":{\"en\":\"Harbor Registry\",\"zh\":\"Harbor Registry\"},\"host\":\"\",\"html\":\"\",\"name\":\"harbor-registry\",\"kind\":\"imageregistry\",\"type\":\"Harbor\",\"public\":false,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"imageregistries\",\"supportedSecretTypes\":[{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Input Username and Password as used on login\",\"zh\":\"用户名和密码均为登录时的用户名和密码\"}}]},{\"displayName\":{\"en\":\"Alauda Registry\",\"zh\":\"Alauda Registry\"},\"host\":\"\",\"html\":\"\",\"name\":\"alauda-registry\",\"kind\":\"imageregistry\",\"type\":\"Alauda\",\"public\":false,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"imageregistries\",\"supportedSecretTypes\":[{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Username is [ RootAccount/Username ] and Password is your login password\",\"zh\":\"用户名输入 [ 根账号/用户名 ]，密码输入登录时的密码\"}}]},{\"displayName\":{\"en\":\"ockerHub Registry\",\"zh\":\"DockerHub Registry\"},\"host\":\"https://hub.docker.com\",\"html\":\"https://hub.docker.com\",\"name\":\"dockerhub-registry\",\"kind\":\"imageregistry\",\"type\":\"DockerHub\",\"public\":true,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"imageregistries\",\"supportedSecretTypes\":[{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Input Username and Password as used on login\",\"zh\":\"用户名和密码均为登录时的用户名和密码\"}}]}]},{\"name\":\"testTool\",\"displayName\":{\"en\":\"Test Tool\",\"zh\":\"测试工具\"},\"enabled\":true,\"apiPath\":\"\",\"items\":[{\"displayName\":{\"en\":\"RedwoodHQ\",\"zh\":\"RedwoodHQ\"},\"host\":\"\",\"html\":\"\",\"name\":\"redwoodhq\",\"kind\":\"testtool\",\"type\":\"RedwoodHQ\",\"public\":false,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"testtools\",\"supportedSecretTypes\":[]}]},{\"name\":\"projectManagement\",\"displayName\":{\"en\":\"Project Management\",\"zh\":\"项目管理\"},\"enabled\":true,\"apiPath\":\"\",\"items\":[{\"displayName\":{\"en\":\"Jira\",\"zh\":\"Jira\"},\"host\":\"\",\"html\":\"\",\"name\":\"jira\",\"kind\":\"projectmanagement\",\"type\":\"Jira\",\"public\":false,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"projectmanagements\",\"supportedSecretTypes\":[{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Input Username and Password as used on login\",\"zh\":\"用户名和密码均为登录时的用户名和密码\"}}]},{\"displayName\":{\"en\":\"Taiga\",\"zh\":\"Taiga\"},\"host\":\"\",\"html\":\"\",\"name\":\"taiga\",\"kind\":\"projectmanagement\",\"type\":\"Taiga\",\"public\":false,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"projectmanagements\",\"supportedSecretTypes\":[{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Input Username and Password as used on login\",\"zh\":\"用户名和密码均为登录时的用户名和密码\"}}]}]},{\"name\":\"codeQualityTool\",\"displayName\":{\"en\":\"Code Quality\",\"zh\":\"代码检查\"},\"enabled\":true,\"apiPath\":\"\",\"items\":[{\"displayName\":{\"en\":\"SonarQube\",\"zh\":\"SonarQube\"},\"host\":\"\",\"html\":\"\",\"name\":\"sonarqube\",\"kind\":\"codequalitytool\",\"type\":\"Sonarqube\",\"public\":false,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"codequalitytools\",\"supportedSecretTypes\":[{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Input Username and Password as used on login\",\"zh\":\"用户名和密码均为登录时的用户名和密码\"}}]}]},{\"name\":\"documentManagement\",\"displayName\":{\"en\":\"Document Management\",\"zh\":\"文档库管理\"},\"enabled\":true,\"apiPath\":\"\",\"items\":[{\"displayName\":{\"en\":\"Confluence\",\"zh\":\"Confluence\"},\"host\":\"\",\"html\":\"\",\"name\":\"confluence\",\"kind\":\"DocumentManagement\",\"type\":\"Confluence\",\"public\":false,\"enterprise\":false,\"enabled\":true,\"apiPath\":\"documentmanagements\",\"supportedSecretTypes\":[{\"type\":\"kubernetes.io/basic-auth\",\"description\":{\"en\":\"Input Username and Password as used on login\",\"zh\":\"用户名和密码均为登录时的用户名和密码\"}}]}]}],\"versionGate\":\"ga\",\"installed\":{\"bitbucket\":true,\"dockerhub-registry\":true,\"gitee\":true,\"github\":true,\"gitlab\":true},\"panel\":{\"panels\":[{\"name\":{\"en\":\"Project Management\",\"zh\":\"项目管理\"},\"index\":0,\"categories\":[\"ProjectManagementBinding\"]},{\"name\":{\"en\":\"Document Management\",\"zh\":\"文档管理\"},\"index\":1,\"categories\":[\"DocumentManagementBinding\"]},{\"name\":{\"en\":\"Code Repository\",\"zh\":\"代码仓库\"},\"index\":2,\"categories\":[\"CodeRepoBinding\"]},{\"name\":{\"en\":\"Continuous Integration\",\"zh\":\"持续集成\"},\"index\":3,\"categories\":[\"JenkinsBinding\"]},{\"name\":{\"en\":\"Code Quality Analysis\",\"zh\":\"代码质量分析\"},\"index\":4,\"categories\":[\"CodeQualityBinding\"]},{\"name\":{\"en\":\"Artifacts\",\"zh\":\"制品仓库\"},\"index\":5,\"categories\":[\"ImageRegistryBinding\"]}],\"type\":\"3x3\"}}",
        "ace_api_endpoint": "http://18.24.24.11:32001",
        "ace_root_account": "alauda",
        "ace_token": "11f2da12c470aab7eae14343c47513ae5604dd11",
        "ace_ui_endpoint": "http://18.24.24.11:32005/console",
        "role_mapping": "{\"source\":\"ace\",\"platforms\":[{\"name\":\"ace\",\"sync\":\"rbac\",\"enabled\":true,\"roles\":{\"namespace_admin\":\"namespace_admin\",\"namespace_auditor\":\"namespace_auditor\",\"namespace_developer\":\"namespace_developer\",\"project_admin\":\"project_admin\",\"project_auditor\":\"project_auditor\",\"space_admin\":\"space_admin\",\"space_auditor\":\"space_auditor\",\"space_developer\":\"space_developer\"},\"custom\":{}},{\"name\":\"acp\",\"sync\":\"rbac\",\"enabled\":true,\"roles\":{\"namespace_admin\":\"alauda_namespace_admin\",\"namespace_auditor\":\"alauda_namespace_auditor\",\"namespace_developer\":\"alauda_namespace_developer\",\"project_admin\":\"alauda_project_admin\",\"project_auditor\":\"alauda_project_auditor\",\"space_admin\":\"alauda_space_admin\",\"space_auditor\":\"alauda_space_auditor\",\"space_developer\":\"alauda_space_developer\"},\"custom\":{}},{\"name\":\"bitbucket\",\"sync\":\"rbac-mutex\",\"enabled\":true,\"roles\":{\"namespace_admin\":\"Developer\",\"namespace_auditor\":\"Developer\",\"namespace_developer\":\"Developer\",\"project_admin\":\"Administrator\",\"project_auditor\":\"Developer\",\"space_admin\":\"Developer\",\"space_auditor\":\"Developer\",\"space_developer\":\"Developer\"},\"custom\":{}},{\"name\":\"gitee\",\"sync\":\"rbac-mutex\",\"enabled\":true,\"roles\":{\"namespace_admin\":\"Developer\",\"namespace_auditor\":\"Developer\",\"namespace_developer\":\"Developer\",\"project_admin\":\"Admin\",\"project_auditor\":\"Developer\",\"space_admin\":\"Developer\",\"space_auditor\":\"Developer\",\"space_developer\":\"Developer\"},\"custom\":{}},{\"name\":\"github\",\"sync\":\"rbac-mutex\",\"enabled\":true,\"roles\":{\"namespace_admin\":\"Member\",\"namespace_auditor\":\"Member\",\"namespace_developer\":\"Member\",\"project_admin\":\"Owner\",\"project_auditor\":\"Member\",\"space_admin\":\"Member\",\"space_auditor\":\"Member\",\"space_developer\":\"Member\"},\"custom\":{}},{\"name\":\"gitlab\",\"sync\":\"rbac-mutex\",\"enabled\":true,\"roles\":{\"namespace_admin\":\"Master\",\"namespace_auditor\":\"Reporter\",\"namespace_developer\":\"Developer\",\"project_admin\":\"Owner\",\"project_auditor\":\"Guester\",\"space_admin\":\"Master\",\"space_auditor\":\"Reporter\",\"space_developer\":\"Developer\"},\"custom\":{}},{\"name\":\"harbor\",\"sync\":\"rbac-mutex\",\"enabled\":true,\"roles\":{\"namespace_admin\":\"Developer\",\"namespace_auditor\":\"Guest\",\"namespace_developer\":\"Developer\",\"project_admin\":\"Project Admin\",\"project_auditor\":\"Guest\",\"space_admin\":\"Developer\",\"space_auditor\":\"Guest\",\"space_developer\":\"Developer\"},\"custom\":{}},{\"name\":\"Jira\",\"sync\":\"rbac\",\"enabled\":true,\"roles\":{\"namespace_admin\":\"ALaudaSpaceAdmin\",\"namespace_auditor\":\"AlaudaSpaceAuditor\",\"namespace_developer\":\"AlaudaSpaceDeveloper\",\"project_admin\":\"AlaudaProjectAdmin\",\"project_auditor\":\"ALaudaProjectAuditor\",\"space_admin\":\"ALaudaSpaceAdmin\",\"space_auditor\":\"AlaudaSpaceAuditor\",\"space_developer\":\"AlaudaSpaceDeveloper\"},\"custom\":{}},{\"name\":\"Sonarqube\",\"sync\":\"rbac\",\"enabled\":true,\"roles\":{\"namespace_admin\":\"admin\",\"namespace_auditor\":\"user\",\"namespace_developer\":\"user\",\"project_admin\":\"admin\",\"project_auditor\":\"user\",\"space_admin\":\"admin\",\"space_auditor\":\"user\",\"space_developer\":\"user\"},\"custom\":{\"namespace_admin\":{\"admin\":\"true\",\"codeviewer\":\"true\",\"issueadmin\":\"true\",\"scan\":\"true\",\"user\":\"true\"},\"namespace_auditor\":{\"admin\":\"false\",\"codeviewer\":\"true\",\"issueadmin\":\"false\",\"scan\":\"true\",\"user\":\"true\"},\"namespace_developer\":{\"admin\":\"false\",\"codeviewer\":\"true\",\"issueadmin\":\"false\",\"scan\":\"true\",\"user\":\"true\"},\"project_admin\":{\"admin\":\"true\",\"codeviewer\":\"true\",\"issueadmin\":\"true\",\"scan\":\"true\",\"user\":\"true\"},\"project_auditor\":{\"admin\":\"false\",\"codeviewer\":\"true\",\"issueadmin\":\"false\",\"scan\":\"true\",\"user\":\"true\"},\"space_admin\":{\"admin\":\"true\",\"codeviewer\":\"true\",\"issueadmin\":\"true\",\"scan\":\"true\",\"user\":\"true\"},\"space_auditor\":{\"admin\":\"false\",\"codeviewer\":\"true\",\"issueadmin\":\"false\",\"scan\":\"true\",\"user\":\"true\"},\"space_developer\":{\"admin\":\"false\",\"codeviewer\":\"true\",\"issueadmin\":\"false\",\"scan\":\"true\",\"user\":\"true\"}}}],\"roles\":[{\"name\":\"namespace_admin\",\"priority\":20},{\"name\":\"namespace_auditor\",\"priority\":22},{\"name\":\"namespace_developer\",\"priority\":21},{\"name\":\"project_admin\",\"priority\":0},{\"name\":\"project_auditor\",\"priority\":100},{\"name\":\"space_admin\",\"priority\":10},{\"name\":\"space_auditor\",\"priority\":12},{\"name\":\"space_developer\",\"priority\":11}]}"
    },
    "kind": "ConfigMap",
    "metadata": {
        "name": "devops-config",
        "namespace": "alauda-system"
    }
}
`

var devopsConfigMap = corev1.ConfigMap{}

var codequalityTool = &devops.CodeQualityTool{
	Spec: devops.CodeQualityToolSpec{
		ToolSpec: devops.ToolSpec{
			HTTP: devops.HostPort{
				Host: "http://18.24.199.4:29000",
			},
		},
		Type: devops.CodeQualityToolTypeSonarqube,
	},
}

var sonarSecret = &corev1.Secret{
	Type: corev1.SecretTypeBasicAuth,
	Data: map[string][]byte{
		"username": []byte("--"),
		"password": []byte("08c703a875d70055874dabaa04620286b9cb2a5e"),
	},
}

var _ = Describe("SonarQubeClient", func() {

	var (
		ctl          *gomock.Controller
		sonarClient  *internalversion.SonarQubeClient
		roundTripper *mhttp.MockRoundTripper
	)

	JustBeforeEach(func() {
		unmarshall(jsonDevOpsConfigMap, &devopsConfigMap)
		ctl = gomock.NewController(GinkgoT())
		roundTripper = mhttp.NewMockRoundTripper(ctl)
	})

	Describe("GetRolesMapping", func() {

		Context("When no users in project", func() {

			It("Should return empty rolesmapping", func() {
				userRequest, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:29000/api/user_groups/users?name=devops-a6-a6-dev-user", nil)
				adminRequest, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:29000/api/user_groups/users?name=devops-a6-a6-dev-admin", nil)
				opsUserRequest, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:29000/api/user_groups/users?name=devops-a6-a6-ops-user", nil)
				opsAdminRequest, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:29000/api/user_groups/users?name=devops-a6-a6-ops-admin", nil)

				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(userRequest).WithQuery()).Return(newSonarHttpResponse(http.StatusOK, emptyUsersJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(adminRequest).WithQuery()).Return(newSonarHttpResponse(http.StatusOK, emptyUsersJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(opsUserRequest).WithQuery()).Return(newSonarHttpResponse(http.StatusOK, emptyUsersJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(opsAdminRequest).WithQuery()).Return(newSonarHttpResponse(http.StatusOK, emptyUsersJson), nil)
				sonarClient = getSonarClient(roundTripper)

				roleMapping, err := sonarClient.GetRolesMapping(&devops.RoleMappingListOptions{
					Projects: []string{"devops-a6-a6-dev", "devops-a6-a6-ops"},
				})
				gomega.Expect(err).To(gomega.BeNil())
				gomega.Expect(len(roleMapping.Spec)).To(gomega.BeEquivalentTo(2))
			})
		})

		Context("When has users in project", func() {

			It("Should return empty rolesmapping", func() {
				devUserRequest, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:29000/api/user_groups/users?name=devops-a6-a6-dev-user", nil)
				devAdminRequest, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:29000/api/user_groups/users?name=devops-a6-a6-dev-admin", nil)
				opsUserRequest, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:29000/api/user_groups/users?name=devops-a6-a6-ops-user", nil)
				opsAdminRequest, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:29000/api/user_groups/users?name=devops-a6-a6-ops-admin", nil)
				a7UserRequest, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:29000/api/user_groups/users?name=devops-a7-a7-biz-user", nil)
				a7AdminRequest, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:29000/api/user_groups/users?name=devops-a7-a7-biz-admin", nil)

				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(devUserRequest).WithQuery()).Return(newSonarHttpResponse(http.StatusOK, devUserJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(devAdminRequest).WithQuery()).Return(newSonarHttpResponse(http.StatusOK, emptyUsersJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(opsUserRequest).WithQuery()).Return(newSonarHttpResponse(http.StatusOK, emptyUsersJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(opsAdminRequest).WithQuery()).Return(newSonarHttpResponse(http.StatusOK, opsAdminUserJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(a7UserRequest).WithQuery()).Return(newSonarHttpResponse(http.StatusOK, emptyUsersJson), nil)
				roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(a7AdminRequest).WithQuery()).Return(newSonarHttpResponse(http.StatusNotFound, groupNotFoundJson), nil)
				sonarClient = getSonarClient(roundTripper)

				roleMapping, err := sonarClient.GetRolesMapping(&devops.RoleMappingListOptions{
					Projects: []string{"devops-a6-a6-dev", "devops-a6-a6-ops", "devops-a7-a7-biz"},
				})
				gomega.Expect(err).To(gomega.BeNil())
				gomega.Expect(len(roleMapping.Spec)).To(gomega.BeEquivalentTo(3))
				for _, project := range roleMapping.Spec {
					gomega.Expect([]string{"devops-a6-a6-dev", "devops-a6-a6-ops", "devops-a7-a7-biz"}).To(gomega.ContainElement(project.Project.Name))

					switch project.Project.Name {
					case "devops-a6-a6-dev":
						{
							gomega.Expect(len(project.UserRoleOperations)).To(gomega.Equal(2))
							userNames := []string{}
							for _, user := range project.UserRoleOperations {
								gomega.Expect([]string{"a6-dev-user1", "a6-dev-user2"}).To(gomega.ContainElement(user.User.Username))
								userNames = append(userNames, user.User.Username)
								gomega.Expect("user").To(gomega.BeEquivalentTo(user.Role.Name))
							}
							gomega.Expect(userNames).To(gomega.BeEquivalentTo([]string{"a6-dev-user1", "a6-dev-user2"}))
						}
					case "devops-a6-a6-ops":
						{
							gomega.Expect(len(project.UserRoleOperations)).To(gomega.Equal(2))
							userNames := []string{}
							for _, user := range project.UserRoleOperations {
								gomega.Expect([]string{"a6-ops-user1", "a6-ops-user2"}).To(gomega.ContainElement(user.User.Username))
								userNames = append(userNames, user.User.Username)
								gomega.Expect("admin").To(gomega.BeEquivalentTo(user.Role.Name))
							}
							gomega.Expect(userNames).To(gomega.BeEquivalentTo([]string{"a6-ops-user1", "a6-ops-user2"}))
						}
					default:
						gomega.Expect(len(project.UserRoleOperations)).To(gomega.Equal(0))
					}
				}
			})
		})

	})

	// we can run case base on a real sonarqube.
	// we need to prepare thease data on sonarqube
	//   users: a6-dev-user1, a6-dev-user2, a6-ops-user1, a6-ops-user2, a6-dev-admin1
	//   sonarprojects: a6-dev-demo1, a6-dev-demo2, a6-dev-demo3, a6-ops-demo1, a6-ops-demo2
	Describe("ApplyRolesMapping", func() {
		It("Should create group auto and set user and permissions", func() {
			addGroupRequest, _ := http.NewRequest(http.MethodPost, "http://18.24.199.4:29000/api/user_groups/create", nil)
			addUserToGroupRequest, _ := http.NewRequest(http.MethodPost, "http://18.24.199.4:29000/api/user_groups/add_user", nil)
			setPermissionRequest, _ := http.NewRequest(http.MethodPost, "http://18.24.199.4:29000/api/permissions/add_group", nil)
			searchGroupRequest, _ := http.NewRequest(http.MethodGet, "http://18.24.199.4:29000/api/user_groups/search", nil)
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(addGroupRequest)).Return(newSonarHttpResponse(http.StatusOK, ""), nil).AnyTimes()
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(addUserToGroupRequest)).Return(newSonarHttpResponse(http.StatusOK, devUserJson), nil).AnyTimes()
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(setPermissionRequest)).Return(newSonarHttpResponse(http.StatusOK, devUserJson), nil).AnyTimes()
			roundTripper.EXPECT().RoundTrip(mhttp.NewRequestMatcher(searchGroupRequest)).Return(newSonarHttpResponse(http.StatusOK, searchGroupJson), nil).AnyTimes()

			sonarClient = getSonarClient(roundTripper)
			err := sonarClient.ApplyRoleMapping(&roleMapping)
			gomega.Expect(err).To(gomega.BeNil())
		})
	})

})

var roleMapping = devops.RoleMapping{
	Spec: []devops.ProjectUserRoleOperation{
		{
			Project: devops.ProjectData{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "devops-a6-a6-dev",
					Namespace: "devops-a6-a6-dev",
					Annotations: map[string]string{
						"codequalityProjectKeys": "a6-dev-demo1,a6-dev-demo2,a6-dev-demo3",
					},
				},
			},
			UserRoleOperations: []devops.UserRoleOperation{
				{
					User: devops.UserMeta{
						Username: "a6-dev-user1",
					},
					Role: devops.RoleMeta{
						Name:   "user",
						Custom: userPermission,
					},
					Operation: devops.UserRoleOperationAdd,
				},
				{
					User: devops.UserMeta{
						Username: "a6-dev-user2",
					},
					Role: devops.RoleMeta{
						Name:   "user",
						Custom: userPermission,
					},
					Operation: devops.UserRoleOperationAdd,
				},
				{
					User: devops.UserMeta{
						Username: "a6-dev-admin1",
					},
					Role: devops.RoleMeta{
						Name:   "admin",
						Custom: adminPermission,
					},
					Operation: devops.UserRoleOperationAdd,
				},
			},
		},

		{
			Project: devops.ProjectData{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "devops-a6-a6-ops",
					Namespace: "devops-a6-a6-ops",
					Annotations: map[string]string{
						"codequalityProjectKeys": "a6-ops-demo1,a6-ops-demo2,a6-dev-demo3",
					},
				},
			},
			UserRoleOperations: []devops.UserRoleOperation{
				{
					User: devops.UserMeta{
						Username: "a6-ops-user1",
					},
					Role: devops.RoleMeta{
						Name:   "user",
						Custom: adminPermission,
					},
					Operation: devops.UserRoleOperationAdd,
				},
				{
					User: devops.UserMeta{
						Username: "a6-ops-user2",
					},
					Role: devops.RoleMeta{
						Name:   "user",
						Custom: userPermission,
					},
					Operation: devops.UserRoleOperationAdd,
				},
			},
		},
	},
}

//user:true;codeviewer:true;issueadmin:false;admin:false;scan:true
var adminPermission = map[string]string{
	"user":       "true",
	"codeviewer": "true",
	"issueadmin": "true",
	"admin":      "true",
	"scan":       "true",
}

var userPermission = map[string]string{
	"user":       "true",
	"codeviewer": "true",
	"issueadmin": "false",
	"admin":      "false",
	"scan":       "true",
}

func getSonarClient(rountTripper http.RoundTripper) *internalversion.SonarQubeClient {
	codequalityClient := internalversion.NewCodeQualityServiceClient(rountTripper, codequalityTool, sonarSecret, devops2.BaseOpts{ConfigMap: &devopsConfigMap})
	sonarClient, _ := internalversion.NewSonarQubeClient(codequalityClient)
	return sonarClient
}

func newSonarHttpResponse(statusCode int, body string) *http.Response {
	return &http.Response{
		Status: fmt.Sprintf("%d", statusCode),
		Header: http.Header{
			"Content-Type": []string{
				"application/json",
			},
		},
		StatusCode: statusCode,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 0,
		Body:       ioutil.NopCloser(bytes.NewBufferString(fmt.Sprintf(body))),
	}
}

var groupNotFoundJson = `{
    "errors": [
        {
            "msg": "No group with name 'devops-a7-a7-biz-admin' in organization 'default-organization'"
        }
    ]
}`

var devUserJson = `{
    "users": [
        {
            "login": "a6-dev-user1",
            "name": "a6-dev-user1",
            "selected": true
        },
        {
            "login": "a6-dev-user2",
            "name": "a6-dev-user2",
            "selected": true
        }
    ],
    "p": 1,
    "ps": 25,
    "total": 2
}`

var searchGroupJson = `{
	"paging": {
			"pageIndex": 1,
			"pageSize": 1000,
			"total": 5
	},
	"groups": [
			{
					"id": 32,
					"name": "devops-jtcheng-jtcheng-arch-admin",
					"description": "Auto created by devops",
					"membersCount": 0,
					"default": false
			}
	]
}`

var opsAdminUserJson = `{
    "users": [
        {
            "login": "a6-ops-user1",
            "name": "a6-opssss-user1",
            "selected": true
        },
        {
            "login": "a6-ops-user2",
            "name": "a6-opsss-user2",
            "selected": true
        }
    ],
    "p": 1,
    "ps": 25,
    "total": 2
}`

var emptyUsersJson = `{
    "users": [],
    "p": 1,
    "ps": 25,
    "total": 0
}`

func unmarshall(str string, obj interface{}) {
	err := json.Unmarshal([]byte(str), obj)

	if err != nil {
		fmt.Printf("str:%s, obj:%#v", str, obj)
	}
	gomega.Expect(err).To(gomega.BeNil())
}
