package internalversion

import (
	"net/http"
	"time"
)

var (
	HttpTimeout       = 30 * time.Second
	defaultHttpClient = http.DefaultClient
)

func SetClient(client *http.Client) {
	client.Timeout = HttpTimeout
}

func DefaultClient() *http.Client {
	SetClient(defaultHttpClient)
	return defaultHttpClient
}
