package internalversion

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

// region docker
type DockerClient struct {
	ImageRegistryClient
}

func NewDockerClient(endpoint, username, credential string) *DockerClient {
	serviceClient := NewImageRegistryClient(endpoint, username, credential)
	return &DockerClient{serviceClient}
}

func (c *DockerClient) SetTransport(trans http.RoundTripper) {
	c.ImageRegistryClient.Client.Transport = trans
}

func (c *DockerClient) CheckAuthentication() (status *devops.HostPortStatus, err error) {
	resp, err := c.RequestRegistry("v2/_catalog")
	if err != nil || resp == nil {
		return
	}
	status = &devops.HostPortStatus{
		StatusCode: resp.StatusCode,
		Response:   string(resp.Body),
	}
	return
}

func (c *DockerClient) CheckRegistryAvailable(lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	return CheckService(c.Endpoint, nil)
}

func (c *DockerClient) CheckRepositoryAvailable(repositoryName string, lastModifyAt *metav1.Time) (status *devops.HostPortStatus, err error) {
	glog.V(6).Infof("Check image repository: %s is available or not", repositoryName)
	var (
		start       = time.Now()
		duration    time.Duration
		lastAttempt = metav1.NewTime(start)
	)
	repository := strings.TrimLeft(repositoryName, "/")
	urlSuffix := fmt.Sprintf("v2/%s/tags/list", repository)
	status = &devops.HostPortStatus{
		LastAttempt: &lastAttempt,
	}
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		status.StatusCode = 500
		status.Response = err.Error()
	} else {
		status.StatusCode = resp.StatusCode
		status.Response = ImageRepositoryAvailable
	}
	duration = time.Since(start)
	status.Delay = &duration
	return
}

func (c *DockerClient) GetImageRepos() (result *devops.ImageRegistryBindingRepositories, err error) {
	result = &devops.ImageRegistryBindingRepositories{}
	resp, err := c.RequestRegistry("v2/_catalog")
	if err != nil {
		return
	}
	response := new(RegistryRepositoriesResponse)
	json.Unmarshal([]byte(resp.Body), response)
	result.Items = response.Repositories

	return result, nil
}

func (c *DockerClient) GetImageTags(repositoryName string) (result []devops.ImageTag, err error) {
	repository := strings.TrimLeft(repositoryName, "/")
	urlSuffix := fmt.Sprintf("v2/%s/tags/list", repository)
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil {
		glog.Errorf("Can't get repository: %s tag list", repository)
		return result, err
	}
	tagResp := new(RegistryRepositoryTagResponse)
	json.Unmarshal([]byte(resp.Body), tagResp)
	tags := tagResp.Tags
	glog.V(6).Infof("Repository(%s) tags is: %s", repositoryName, tags)
	wait := sync.WaitGroup{}
	wait.Add(len(tags))
	// TODO: support worker pool
	for _, tag := range tags {
		go func(tagStr string) {
			defer wait.Done()
			tagDetail, err := c.GetImageTagDetail(repository, tagStr)
			if err != nil {
				glog.Errorf("Can't get repository: (%s), tag: (%s) detail.", repository, tagStr)
			} else {
				result = append(result, tagDetail)
			}
		}(tag)
	}
	wait.Wait()

	return result, nil
}

func (c *DockerClient) GetImageTagDetail(repository, reference string) (result devops.ImageTag, err error) {
	urlSuffix := fmt.Sprintf("v2/%s/manifests/%s", repository, reference)
	result = devops.ImageTag{}
	resp, err := c.RequestRegistry(urlSuffix)
	if err != nil || resp.StatusCode != 200 {
		glog.Errorf("Cat't get repository: (%s) tag: (%s) manifest.", repository, reference)
		result.Name = reference
		result.CreatedAt = &metav1.Time{}
		return result, err
	}
	glog.V(6).Infof("Get repository (%s) tag (%s) manifests", repository, reference)
	tagDetailResp := new(TagDetails)
	json.Unmarshal([]byte(resp.Body), tagDetailResp)
	firstV1Compatibility := tagDetailResp.History[0].V1Compatibility
	v1Compatibility := new(V1Compatibility)
	json.Unmarshal([]byte(firstV1Compatibility), v1Compatibility)
	t, err := time.Parse(time.RFC3339Nano, v1Compatibility.Created)
	if err != nil {
		glog.Errorf("Tag create time convert string to time.Time is failed: %s", err)
	}
	result.CreatedAt = &metav1.Time{Time: t}
	result.Name = reference
	result.Digest = strings.TrimPrefix(resp.Header.Digest, ImageDigestSignature)
	return result, nil
}

func (c *DockerClient) TriggerScanImage(repositoryName, tag string) (result *devops.ImageResult) {
	return
}

func (c *DockerClient) GetVulnerability(repositoryName, tag string) (result *devops.VulnerabilityList, err error) {
	return
}

func (c *DockerClient) GetRegistryEndpoint() (endpoint string, err error) {
	return
}

func (c *DockerClient) GetImageRepoLink(name string) (link string) {
	return
}

func (c *DockerClient) GetImageProjects() (result *devops.ProjectDataList, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("ProjectDataList"), "get")
}

func (c *DockerClient) CreateImageProject(name string) (result *devops.ProjectData, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("ProjectData"), "create")
}

func (c *DockerClient) GetPermissionNameIDMap() (map[string]int, error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("ImageRegistry"), "list")
}

func (c *DockerClient) UserProjectOperation(project devops.ProjectData, user devops.UserRoleOperation) error {
	return errors.NewMethodNotSupported(devops.Resource("ImageRegistry"), "update")
}

func (c *DockerClient) GetRoleMapping(opts *devops.RoleMappingListOptions) (*devops.RoleMapping, error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("ImageRegistry"), "list")
}
