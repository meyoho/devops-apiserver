package devops

var ScriptMap = map[string](map[string]string){
	"create_Maven2": {"hosted": "alauda_devops_create_maven2_hosted"},
}

var ScriptMapAll = map[string]string{
	"map_roleuser":            "alauda_devops_map_roleuser",
	"get_repository":          "alauda_devops_get_repository",
	"list_blobstore":          "alauda_devops_list_blobstore",
	"add_userrole":            "alauda_devops_add_userrole",
	"remove_userrole":         "alauda_devops_remove_userrole",
	"add_userrolewithactions": "alauda_devops_add_userrole_actions",
}
