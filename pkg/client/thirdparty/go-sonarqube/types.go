package sonarqube

import "alauda.io/devops-apiserver/pkg/client/thirdparty"

type Paging struct {
	PageIndex int `json:"pageIndex"`
	PageSize  int `json:"pageSize"`
	Total     int `json:"total"`
}

// CompatibilityPaging is for some sonar api not use pagging
type CompatibilityPaging struct {
	PageIndex int `json:"p"`
	PageSize  int `json:"ps"`
	Total     int `json:"total"`
}

type User struct {
	Login       string   `json:"login"`
	Name        string   `json:"name"`
	Active      bool     `json:"active"`
	Email       string   `json:"email"`
	ScmAccounts []string `json:"scmAccounts"`
	Selected    bool     `json:"selected"`
	GroupNames  []string `json:"groups"`
}

func (u *User) GetName() string {
	return u.Name
}

type PermissionItem string

const (
	ProjectPermissionAdmin      = "admin"
	ProjectPermissionCodeViewer = "codeviewer"
	ProjectPermissionIssueAdmin = "issueadmin"
	ProjectPermissionScan       = "scan"
	ProjectPermissionUser       = "user"
	//admin, codeviewer, issueadmin, scan, user
)

type Group struct {
	ID           int    `json:"id"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	MembersCount int    `json:"membersCount"`
	Default      bool   `json:"default"`
}

func (u *Group) GetName() string {
	return u.Name
}

var _ thirdparty.List = &UserList{}

type UserList struct {
	Users  []User `json:"users"`
	Paging Paging `json:"paging"`

	CompatibilityPaging `json:",inline"`
}

func (list *UserList) GetPagging() (count, pageSize, pageNumber int) {
	return list.Paging.Total, list.Paging.PageSize, list.Paging.PageIndex
}

func (list *UserList) GetItems() []thirdparty.ListItem {
	items := []thirdparty.ListItem{}
	for _, user := range list.Users {
		userCopy := user
		items = append(items, &userCopy)
	}

	return items
}

func (list *UserList) AddItems(items thirdparty.List) {

	if list.Users == nil {
		list.Users = make([]User, 0, len(items.GetItems()))
	}

	for _, item := range items.GetItems() {
		if u, ok := item.(*User); ok {
			list.Users = append(list.Users, *u)
		}
	}

	return
}

type GroupList struct {
	Groups []Group `json:"groups"`
	Paging Paging  `json:"paging"`
}
