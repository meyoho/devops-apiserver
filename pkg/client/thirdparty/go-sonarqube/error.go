package sonarqube

import (
	"net/http"
	"strings"
)

func IsGroupNotExistError(err error) bool {
	if _, ok := err.(*ErrorResponse); !ok {
		return false
	}

	resErr := err.(*ErrorResponse)
	if resErr.Response.StatusCode != http.StatusNotFound {
		return false
	}

	if strings.Contains(resErr.Message, "group") {
		return true
	}

	for _, item := range resErr.Errors {
		if strings.Contains(item.Message, "group") {
			return true
		}
	}

	return false
}

func IsUserNotExistError(err error) bool {
	if _, ok := err.(*ErrorResponse); !ok {
		return false
	}

	resErr := err.(*ErrorResponse)
	if resErr.Response.StatusCode != http.StatusNotFound {
		return false
	}

	if strings.Contains(resErr.Message, "user") {
		return true
	}

	for _, item := range resErr.Errors {
		if strings.Contains(item.Message, "user") {
			return true
		}
	}

	return false
}

func IsGroupAlreadyExistError(err error) bool {
	if _, ok := err.(*ErrorResponse); !ok {
		return false
	}

	resErr := err.(*ErrorResponse)

	if resErr.Response.StatusCode != http.StatusBadRequest {
		return false
	}

	for _, item := range resErr.Errors {
		if strings.Contains(item.Message, "already exists") {
			return true
		}
	}
	return false
}
