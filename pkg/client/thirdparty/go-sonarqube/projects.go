package sonarqube

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"net/http"
	"strings"
	"time"
)

const (
	PathComponentDetails   = "components/show"
	PathProjectBranches    = "project_branches/list"
	PathSearchProjects     = "projects/search"
	PathComponentMeasures  = "measures/component"
	PathProjectQualityGate = "qualitygates/get_by_project"
	CodeQualityTime        = "2006-01-02T15:04:05+0000"
)

type ProjectService struct {
	client *Client
}

type Project struct {
	Name             string `json:"name"`
	Key              string `json:"key"`
	LastAnalysisDate string `json:"lastAnalysisDate"`
	// Used for keep Compatibility
	AnalysisDate string `json:"analysisDate"`
	Visibility   string `json:"visibility"`
}

func (p *Project) GetName() string {
	return p.Key
}

func (p *Project) GetLastAnalysisDate() (*time.Time, error) {
	if p.LastAnalysisDate == "" {
		return p.parseTime(p.AnalysisDate)
	}
	return p.parseTime(p.LastAnalysisDate)
}

func (p *Project) parseTime(s string) (*time.Time, error) {
	t, err := time.Parse(CodeQualityTime, s)
	if err != nil {
		return nil, err
	}

	return &t, nil
}

type ProjectList struct {
	Projects []Project `json:"components"`
	Paging   Paging    `json:"paging"`
}

func (list *ProjectList) GetPagging() (count, pageSize, pageNumber int) {
	return list.Paging.Total, list.Paging.PageSize, list.Paging.PageIndex
}

func (list *ProjectList) GetItems() []thirdparty.ListItem {
	var items []thirdparty.ListItem
	for _, project := range list.Projects {
		projectCopy := project
		items = append(items, &projectCopy)
	}

	return items
}

func (list *ProjectList) AddItems(items thirdparty.List) {

	if list.Projects == nil {
		list.Projects = make([]Project, 0, len(items.GetItems()))
	}

	for _, item := range items.GetItems() {
		if u, ok := item.(*Project); ok {
			list.Projects = append(list.Projects, *u)
		}
	}

	return
}

func (p *ProjectService) GetProjects(page, pageSize int) (*http.Response, *ProjectList, error) {
	req, err := p.client.NewRequest(http.MethodGet, PathSearchProjects, PageOpts(page, pageSize), nil)
	if err != nil {
		return nil, nil, err
	}

	projectList := ProjectList{}
	resp, err := p.client.Do(req, &projectList)

	if err != nil {
		return resp, nil, err
	}

	return resp, &projectList, nil
}

type ProjectDetails struct {
	Project Project `json:"component"`
}

func (p *ProjectService) GetProject(projectKey string) (*http.Response, *Project, error) {
	return p.GetProjectWithBranch(projectKey, "")
}

type Branches struct {
	Branches []Branch `json:"branches"`
}

type Branch struct {
	Name         string `json:"name"`
	IsMain       bool   `json:"isMain"`
	AnalysisDate string `json:"analysisDate"`
}

func (b *Branch) GetLastAnalysisDate() (*time.Time, error) {
	t, err := time.Parse(CodeQualityTime, b.AnalysisDate)
	return &t, err
}

func (p *ProjectService) GetProjectWithBranch(projectKey, branch string) (*http.Response, *Project, error) {
	req, err := p.client.NewRequest(http.MethodGet, PathComponentDetails, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	q := req.URL.Query()
	q.Add("component", projectKey)
	if branch != "" {
		q.Add("branch", branch)
	}
	req.URL.RawQuery = q.Encode()

	projectDetail := ProjectDetails{}
	resp, err := p.client.Do(req, &projectDetail)
	if err != nil {
		return resp, nil, err
	}

	return resp, &projectDetail.Project, nil
}

func (p *ProjectService) GetProjectBranches(projectKey string) (*http.Response, *Branches, error) {
	req, err := p.client.NewRequest(http.MethodGet, PathProjectBranches, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	q := req.URL.Query()
	q.Add("project", projectKey)
	req.URL.RawQuery = q.Encode()

	branches := Branches{}
	resp, err := p.client.Do(req, &branches)
	if err != nil {
		return resp, nil, err
	}

	return resp, &branches, err

}

type ProjectQualityGate struct {
	QualityGate QualityGate `json:"qualityGate"`
}

type QualityGate struct {
	ID      int64  `json:"id"`
	Name    string `json:"name"`
	Default bool   `json:"default"`
}

func (p *ProjectService) GetProjectQualityGate(projectKey string) (*http.Response, *ProjectQualityGate, error) {
	return p.GetProjectQualityGateWithBranch(projectKey, "")
}

func (p *ProjectService) GetProjectQualityGateWithBranch(projectKey, branch string) (*http.Response, *ProjectQualityGate, error) {
	req, err := p.client.NewRequest(http.MethodGet, PathProjectQualityGate, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	q := req.URL.Query()
	q.Add("project", projectKey)
	if branch != "" {
		q.Add("branch", branch)
	}
	req.URL.RawQuery = q.Encode()

	projectQualityGate := ProjectQualityGate{}
	resp, err := p.client.Do(req, &projectQualityGate)
	if err != nil {
		return resp, nil, err
	}

	return resp, &projectQualityGate, nil
}

type ProjectMetrics struct {
	Component Component `json:"component"`
}

type Component struct {
	Name     string         `json:"name"`
	Measures []SonarMeasure `json:"measures"`
	Key      string         `json:"key"`
}

type SonarMeasure struct {
	Metric  string   `json:"metric"`
	Periods []Period `json:"periods"`
	Value   *string  `json:"value,omitempty"`
}

type Period struct {
	Index int64  `json:"index"`
	Value string `json:"value"`
}

func (p *ProjectService) GetProjectMetrics(projectKey, branch string, metricKeys []string) (*http.Response, *ProjectMetrics, error) {
	req, err := p.client.NewRequest(http.MethodGet, PathComponentMeasures, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	q := req.URL.Query()
	q.Add("component", projectKey)
	q.Add("branch", branch)
	q.Add("metricKeys", strings.Join(metricKeys, ","))

	req.URL.RawQuery = q.Encode()

	projectMetrics := ProjectMetrics{}
	resp, err := p.client.Do(req, &projectMetrics)
	if err != nil {
		return resp, nil, err
	}

	return resp, &projectMetrics, nil
}
