package sonarqube

import (
	"net/http"
)

type GroupService struct {
	client *Client
}

type SearchGroupRequest struct {
	PageOptions
	Field string `json:"f,omitempty"`
	Query string `json:"q,omitempty"`
}

type AddUserToGroupRequest struct {
	GroupID   int    `url:"id,omitempty"`
	GroupName string `url:"name,omitempty"`
	Login     string `url:"login,omitempty"`
}

type CreateGroupRequest struct {
	GroupName   string `url:"name,omitempty"`
	Description string `url:"description,omitempty"`
}

func (s *GroupService) Create(createOptions CreateGroupRequest, options ...OptionFunc) (*http.Response, error) {
	var path = "user_groups/create"
	if createOptions.Description == "" {
		createOptions.Description = "Auto created by devops"
	}

	req, err := s.client.NewRequest(http.MethodPost, path, createOptions, options)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(req, nil)

	if err != nil {
		return resp, err
	}

	return resp, err
}

func (s *GroupService) Search(searchOptions SearchGroupRequest, options ...OptionFunc) (*http.Response, *GroupList, error) {
	var path = "user_groups/search"

	req, err := s.client.NewRequest(http.MethodGet, path, searchOptions, options)
	if err != nil {
		return nil, nil, err
	}

	groups := GroupList{}
	resp, err := s.client.Do(req, &groups)

	if err != nil {
		return resp, nil, err
	}

	return resp, &groups, nil
}

func (s *GroupService) AddUser(createOptions AddUserToGroupRequest, options ...OptionFunc) (*http.Response, error) {
	var path = "user_groups/add_user"

	req, err := s.client.NewRequest(http.MethodPost, path, createOptions, options)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(req, nil)

	if err != nil {
		return resp, err
	}

	return resp, err
}

type RemoveUserInGroupRequest = AddUserToGroupRequest

func (s *GroupService) RemoveUser(removeOptions RemoveUserInGroupRequest, options ...OptionFunc) (*http.Response, error) {
	var path = "user_groups/remove_user"

	req, err := s.client.NewRequest(http.MethodPost, path, removeOptions, options)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(req, nil)

	if err != nil {
		return resp, err
	}

	return resp, err
}

type GetUsersRequest struct {
	PageOptions
	GroupName string `url:"name,omitempty"`
}

func (s *GroupService) GetUsers(query GetUsersRequest, page, pageSize int) (*http.Response, UserList, error) {
	var path = "user_groups/users"

	req, err := s.client.NewRequest(http.MethodGet, path, query, nil)
	if err != nil {
		return nil, UserList{}, err
	}

	userList := UserList{}
	resp, err := s.client.Do(req, &userList)

	if err != nil {
		return resp, userList, err
	}

	for i, _ := range userList.Users {
		(&userList.Users[i]).GroupNames = []string{query.GroupName}
	}

	userList.Paging.Total = userList.CompatibilityPaging.Total
	userList.Paging.PageSize = userList.CompatibilityPaging.PageSize
	userList.Paging.PageIndex = userList.CompatibilityPaging.PageIndex

	return resp, userList, err
}
