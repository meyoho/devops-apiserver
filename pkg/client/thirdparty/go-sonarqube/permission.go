package sonarqube

import "net/http"

const (
	PathValidate = "authentication/validate"
)

type PermissionService struct {
	client *Client
}

type AddGroupPermissionRequest struct {
	GroupID    int    `url:"groupId,omitempty"`
	GroupName  string `url:"groupName,omitempty"`
	Permission string `url:"permission,omitempty"`
	ProjectID  string `url:"projectId,omitempty"`
	ProjectKey string `url:"projectKey,omitempty"`
}

func (s *PermissionService) AddGroup(addOptions AddGroupPermissionRequest, options ...OptionFunc) (*http.Response, error) {
	var path = "permissions/add_group"

	req, err := s.client.NewRequest(http.MethodPost, path, addOptions, options)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(req, nil)

	if err != nil {
		return resp, err
	}

	return resp, err
}

type RemoveGroupPermissionRequest = AddGroupPermissionRequest

func (s *PermissionService) RemoveGroup(removeOptions RemoveGroupPermissionRequest, options ...OptionFunc) (*http.Response, error) {
	var path = "permissions/remove_group"

	req, err := s.client.NewRequest(http.MethodPost, path, removeOptions, options)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(req, nil)

	if err != nil {
		return resp, err
	}

	return resp, err
}

type ValidateResult struct {
	Valid bool `json:"valid"`
}

func (s *PermissionService) CheckAuthentication() (*http.Response, bool, error) {
	req, err := s.client.NewRequest(http.MethodGet, PathValidate, nil, nil)
	if err != nil {
		return nil, false, err
	}

	validateResult := ValidateResult{}
	resp, err := s.client.Do(req, &validateResult)
	if err != nil {
		return resp, false, err
	}

	return resp, validateResult.Valid, nil
}
