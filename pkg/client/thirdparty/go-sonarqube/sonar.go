//
// Copyright 2018, sywang
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package sonarqube

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/google/go-querystring/query"
	glog "k8s.io/klog"
)

const (
	apiVersionPath = "api/"
	userAgent      = "go-sonarqube-devops"
	defaultBaseUrl = "sonarqube.com"
)

// authType represents an authentication type within Gitee.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
type authType int

// List of available authentication types.
//
const (
	basicAuth authType = iota
	sonarToken
)

// A Client manages communication with the Gitee API.
type Client struct {
	// HTTP client used to communicate with the API.
	client *http.Client

	// Base URL for API requests. Defaults to the public Gitee API, but can be
	// set to a domain endpoint to use with a self hosted Gitee server. baseURL
	// should always be specified with a trailing slash.
	baseURL *url.URL

	// Token type used to make authenticated API calls.
	authType authType

	// Token used to make authenticated API calls.
	token string

	// Username and password used for basix authentication.
	username, password string

	// User agent used when communicating with the Gitee API.
	UserAgent string

	Users       *UserService
	Groups      *GroupService
	Permissions *PermissionService
	Projects    *ProjectService
}

// NewClient returns a new Gitee API client. If a nil httpClient is
// provided, http.DefaultClient will be used. To use API methods which require
// authentication, provide a valid private or personal token.
func NewClient(httpClient *http.Client, baseUrl, token string) *Client {
	client := newClient(httpClient)
	client.authType = sonarToken
	err := client.SetBaseURL(baseUrl)
	if err != nil {
		glog.Errorf("Sonar base url :%s is invalid:%s", baseUrl, err)
		client.SetBaseURL(defaultBaseUrl)
	}
	client.token = token
	return client
}

func newClient(httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
		httpClient.Timeout = 30 * time.Second
	}

	c := &Client{client: httpClient, UserAgent: userAgent}

	c.Users = &UserService{client: c}
	c.Groups = &GroupService{client: c}
	c.Permissions = &PermissionService{client: c}
	c.Projects = &ProjectService{client: c}

	return c
}

// BaseURL return a copy of the baseURL.
func (c *Client) BaseURL() *url.URL {
	u := *c.baseURL
	return &u
}

// SetBaseURL sets the base URL for API requests to a custom endpoint. urlStr
// should always be specified with a trailing slash.
func (c *Client) SetBaseURL(urlStr string) error {
	// Make sure the given URL end with a slash
	if !strings.HasSuffix(urlStr, "/") {
		urlStr += "/"
	}

	baseURL, err := url.Parse(urlStr)
	if err != nil {
		return err
	}

	if !strings.HasSuffix(baseURL.Path, apiVersionPath) {
		baseURL.Path += apiVersionPath
	}

	// Update the base URL of the client.
	c.baseURL = baseURL

	return nil
}

// NewRequest creates an API request. A relative URL path can be provided in
// urlStr, in which case it is resolved relative to the base URL of the Client.
// Relative URL paths should always be specified without a preceding slash. If
// specified, the value pointed to by body is JSON encoded and included as the
// request body.
func (c *Client) NewRequest(method, path string, opt interface{}, options []OptionFunc) (*http.Request, error) {
	u := *c.baseURL
	// Set the encoded opaque data
	u.Opaque = c.baseURL.Path + path
	u.Path = c.baseURL.Path + path

	urlValues := url.Values{}
	if opt != nil {
		q, err := query.Values(opt)
		if err != nil {
			return nil, err
		}
		urlValues = q
	}

	if method == http.MethodGet {
		u.RawQuery = urlValues.Encode()
	}

	req := &http.Request{
		Method:     method,
		URL:        &u,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 1,
		Header:     make(http.Header),
		Host:       u.Host,
	}

	for _, fn := range options {
		if fn == nil {
			continue
		}

		if err := fn(req); err != nil {
			return nil, err
		}
	}

	if method == http.MethodPost || method == http.MethodPut || method == http.MethodDelete {
		bodyBuffer := bytes.NewBufferString(urlValues.Encode())

		u.RawQuery = ""
		req.Body = ioutil.NopCloser(bodyBuffer)
		req.ContentLength = int64(bodyBuffer.Len())
		// sonarqube payload type is x-www-form-urlencoded
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
	}

	switch c.authType {
	case sonarToken:
		req.Header.Set("Authorization", fmt.Sprintf("Basic %s", encodeToken(c.token)))
	}

	if c.UserAgent != "" {
		req.Header.Set("User-Agent", c.UserAgent)
	}

	return req, nil
}

func encodeToken(token string) string {
	if !strings.HasSuffix(token, ":") {
		token = fmt.Sprintf("%s:", token)
	}
	encodeToken := base64.StdEncoding.EncodeToString([]byte(token))
	return encodeToken
}

// Do sends an API request and returns the API response. The API response is
// JSON decoded and stored in the value pointed to by v, or returned as an
// error if an API error has occurred. If v implements the io.Writer
// interface, the raw response body will be written to v, without attempting to
// first decode it.
func (c *Client) Do(req *http.Request, v interface{}) (*http.Response, error) {
	resp, err := c.client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return nil, err
	}

	err = CheckResponse(resp)
	if err != nil {
		// even though there was an error, we still return the response
		// in case the caller wants to inspect it further
		return resp, err
	}

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			_, err = io.Copy(w, resp.Body)
		} else {
			err = json.NewDecoder(resp.Body).Decode(v)
		}
	}

	return resp, err
}

// OptionFunc can be passed to all API requests
type OptionFunc func(*http.Request) error

// WithContext runs the request with the provided context
func WithContext(ctx context.Context) OptionFunc {
	return func(req *http.Request) error {
		*req = *req.WithContext(ctx)
		return nil
	}
}

// An ErrorResponse reports one or more errors caused by an API request.
//
type ErrorResponse struct {
	Body     string
	Response *http.Response
	Message  string

	Errors ErrorItems `json:"errors"`
}

type ErrorItems []ErrorItem

func (items *ErrorItems) Error() string {
	sb := strings.Builder{}
	if items == nil {
		return ""
	}

	for _, item := range *items {
		sb.WriteString(item.Error() + ";")
	}
	return sb.String()
}

type ErrorItem struct {
	Message string `json:"msg"`
}

func (item *ErrorItem) Error() string {
	return item.Message
}

func (e *ErrorResponse) Error() string {
	path, _ := url.QueryUnescape(e.Response.Request.URL.Opaque)
	u := fmt.Sprintf("%s://%s%s", e.Response.Request.URL.Scheme, e.Response.Request.URL.Host, path)
	return fmt.Sprintf("%s %s: %d %s %#v", e.Response.Request.Method, u, e.Response.StatusCode, e.Message, e.Errors)
}

// CheckResponse checks the API response for errors, and returns them if present.
func CheckResponse(r *http.Response) error {
	switch r.StatusCode {
	case 200, 201, 202, 204, 304:
		return nil
	}

	errorResponse := &ErrorResponse{}
	data, err := ioutil.ReadAll(r.Body)
	if err == nil && data != nil {

		if err := json.Unmarshal(data, &errorResponse); err != nil {
			errorResponse.Message = "failed to parse unknown error format"
		}
	}

	errorResponse.Body = string(data)
	errorResponse.Response = r

	return errorResponse
}
