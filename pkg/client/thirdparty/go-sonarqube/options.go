package sonarqube

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"strconv"
)

// PageOptions page options for list
type PageOptions struct {
	Page     int `url:"p,omitempty"`
	PageSize int `url:"ps,omitempty"`
}

// Get options for pagging
func (po PageOptions) Get() map[string][]string {
	return map[string][]string{
		"p":  []string{strconv.Itoa(po.Page)},
		"ps": []string{strconv.Itoa(po.PageSize)},
	}
}

// PageOpts constructor for PageOptions
func PageOpts(page, pageSize int) thirdparty.ListOptions {
	return PageOptions{Page: page, PageSize: pageSize}
}
