package sonarqube

import (
	"net/http"
)

type UserService struct {
	client *Client
}

func (s *UserService) GetUsers(page, pageSize int) (*http.Response, UserList, error) {
	var path = "users/search"

	req, err := s.client.NewRequest(http.MethodGet, path, PageOpts(page, pageSize), nil)
	if err != nil {
		return nil, UserList{}, err
	}

	userList := UserList{}
	resp, err := s.client.Do(req, &userList)

	if err != nil {
		return resp, UserList{}, err
	}

	return resp, userList, err
}
