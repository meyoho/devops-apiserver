package thirdparty

import (
	"context"
	"strconv"
	"sync"
)

// IteratorExtension extension for the client
type IteratorExtension interface {
	Iterate(listFunc ListFunc) ListFunc
	CustomIterate(listFunc ListFunc, iterator ListIterator) ListFunc
}

// List interface for lists
type List interface {
	GetPagging() (count, pageSize, pageNumber int)
	GetItems() []ListItem
	AddItems(List)
}

// ListItem item in a list
type ListItem interface {
	GetName() string
}

// ListFunc function to list options
type ListFunc func(ctx context.Context, opts ...ListOptions) (List, error)

// ListIterator interface for list iterator that are responsible for
// fetching a entire list abstracting pagging logic for client
type ListIterator interface {
	Iterate(ListFunc, context.Context, ...ListOptions) (List, error)
}

// DefaultListIterator default list iterator
var DefaultListIterator = NewDefaultListIterator()

func NewDefaultListIterator() ListIterator {
	return StandardListIterator{
		PageOptsFunc: PageOpts,
	}
}

func NewListIterator(pageOptsFunc func(page, pageSize int) ListOptions) ListIterator {
	if pageOptsFunc == nil {
		pageOptsFunc = PageOpts
	}

	return StandardListIterator{
		PageOptsFunc: pageOptsFunc,
	}
}

// StandardListIterator simple standard list iterator
type StandardListIterator struct {
	PageOptsFunc func(page, pageSize int) ListOptions
}

// Iterate through a list of items using a standard abstract logic
func (s StandardListIterator) Iterate(listFunc ListFunc, ctx context.Context, opts ...ListOptions) (list List, err error) {
	if opts == nil {
		opts = []ListOptions{}
	}
	list, err = listFunc(ctx, opts...)
	if err != nil {
		return
	}
	_, pageSize, numberOfPages := list.GetPagging()
	if numberOfPages > 1 {
		// iterate all pages one by one.
		// to do this a nil opts is added to opts
		// and added to every request to iterate
		// over the whole list
		pageIndex := len(opts)
		opts = append(opts, nil)
		lists := make([]List, numberOfPages)
		errList := make([]error, numberOfPages)
		var wait sync.WaitGroup
		for page := 2; page <= numberOfPages; page++ {
			wait.Add(1)
			go func(page int, pageSize int, ctx context.Context) {
				defer wait.Done()
				pageOpts := s.PageOptsFunc(page, pageSize)
				opts[pageIndex] = pageOpts
				thisList, err := listFunc(ctx, opts...)
				if err != nil {
					errList[page-1] = err
					return
				}
				lists[page-1] = thisList
			}(page, pageSize, ctx)
		}
		wait.Wait()
		// adding all items from the list back to the first list
		for index, l := range lists {
			if errList[index] != nil {
				return nil, errList[index]
			}
			if l != nil {
				list.AddItems(l)
			}
		}
	}
	return
}

// RemoveDuplicatedListItems remove any duplicated items from the list
// by using its name
func RemoveDuplicatedListItems(itemList List) []ListItem {
	items := itemList.GetItems()
	index := map[string]ListItem{}
	newItems := make([]ListItem, 0, len(items))
	for _, li := range items {
		name := li.GetName()
		if _, ok := index[name]; !ok {
			index[name] = li
			newItems = append(newItems, li)
		}
	}
	return newItems
}

// ListOptions list options
type ListOptions interface {
	Get() map[string][]string
}

// PageOptions page options for list
type PageOptions struct {
	Page     int
	PageSize int
}

// Get options for pagging
func (po PageOptions) Get() map[string][]string {
	return map[string][]string{
		"page":      []string{strconv.Itoa(po.Page)},
		"page_size": []string{strconv.Itoa(po.PageSize)},
	}
}

// PageOpts constructor for PageOptions
func PageOpts(page, pageSize int) ListOptions {
	return PageOptions{Page: page, PageSize: pageSize}
}
