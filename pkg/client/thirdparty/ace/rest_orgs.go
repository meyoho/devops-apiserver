package ace

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"context"
	"net/http"

	acectx "alauda.io/devops-apiserver/pkg/client/thirdparty/ace/context"
)

const resourceOrgs = "orgs"
const subresourceAccounts = "accounts"

// OrgExtension org related methods on AlaudaCloudClient
type OrgExtension interface {
	GetOrgAccountByName(ctx context.Context, opts ...RequestOptions) (result *Account, err error)
	GetOrgAccountByNameAsList(ctx context.Context, opts ...RequestOptions) (list thirdparty.List, err error)
}

// GetOrgAccountByName get one org account details by name
// /v1/orgs/{org_name}/accounts/{username}
func (ace *AlaudaCloudClient) GetOrgAccountByName(ctx context.Context, opts ...RequestOptions) (result *Account, err error) {
	var request *http.Request
	request, err = ace.NewReq().
		Get().
		JSON().
		Path(
			ace.opts.APIAddress,
			apiVersionV1,
			resourceOrgs,
			ace.opts.RootAccount,
			subresourceAccounts,
			acectx.Name(ctx),
		).
		Params(opts...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return
	}
	result = &Account{}
	err = ace.DoRequest(request, result)
	return
}

func (ace *AlaudaCloudClient) GetOrgAccountByNameAsListInterator(ctx context.Context, opts ...thirdparty.ListOptions) (list thirdparty.List, err error) {
	return ace.GetOrgAccountByNameAsList(ctx, AsRequestOptions(opts)...)
}

// GetOrgAccountByNameAsList get one role as list to use iterators
func (ace *AlaudaCloudClient) GetOrgAccountByNameAsList(ctx context.Context, opts ...RequestOptions) (list thirdparty.List, err error) {
	var account *Account
	if account, err = ace.GetOrgAccountByName(ctx, opts...); err != nil {
		return
	}
	list = &AccountList{Items: []*Account{account}, StandardList: StandardList{PageSize: 1, Count: 1, NumberOfPages: 1}}
	return
}
