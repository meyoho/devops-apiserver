package ace

type Region struct {
	Name             string `json:"name"`
	DisplayName      string `json:"display_name"`
	RootAccount      string `json:"namespace"`
	ContainerManager string `json:"container_manager"`
	State            string `json:"state"`
	ID               string `json:"id"`
}

type Cluster = Region
