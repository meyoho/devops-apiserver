package ace

// StandardList common list
type StandardList struct {
	Count         int `json:"count"`
	PageSize      int `json:"page_size"`
	NumberOfPages int `json:"num_pages"`
}

// GetPagging return pagging related data
func (std StandardList) GetPagging() (int, int, int) {
	return std.Count, std.PageSize, std.NumberOfPages
}
