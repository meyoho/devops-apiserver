package ace_test

import (
	"context"
	"net/http"

	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	acectx "alauda.io/devops-apiserver/pkg/client/thirdparty/ace/context"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("AlaudaContainerEnterprise.GetOrgAccountByName", func() {
	var (
		ctrl      *gomock.Controller
		transport *mhttp.MockRoundTripper
		client    *ace.AlaudaCloudClient
		ctx       context.Context
		name      string
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		transport = mhttp.NewMockRoundTripper(ctrl)
		client = ace.New(ace.Options{
			RootAccount: "alauda",
			Token:       "token",
			APIAddress:  "http://address",
			Transport:   transport,
		})
		name = "someuser"
		ctx = acectx.WithName(context.TODO(), name)
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	It("should send a request with parameters and return an account instance", func() {
		// adding request and response expectations
		request, _ := http.NewRequest(http.MethodGet, "http://address/v1/orgs/alauda/accounts/someuser/", nil)
		request.Header.Add("Authorization", "Token token")
		request.Header.Add("Content-type", "application/json")

		transport.EXPECT().
			RoundTrip(mhttp.NewRequestMatcher(request)).
			Return(getResponseWithBody(http.StatusOK, request, `{
				"username": "someuser",
				"created_at": "2018-11-26T02:56:45Z",
				"updated_at": "2018-11-26T02:56:45Z",
				"permission": null,
				"type": "organizations.LDAPAccount",
				"realname": "Real Name",
				"department": "Department",
				"position": "Position",
				"mobile": "Mobile",
				"email": "Email",
				"office_address": "Office",
				"landline_phone": "Landline",
				"extra_info": "",
				"sub_type": null
			}`), nil)

		account, err := client.GetOrgAccountByName(ctx)
		Expect(err).To(BeNil(), "should not return error")
		Expect(account).ToNot(BeNil(), "should return subaccount details")

		Expect(account.Username).To(Equal("someuser"))
		Expect(account.Realname).To(Equal("Real Name"))
		Expect(account.Department).To(Equal("Department"))
		Expect(account.Position).To(Equal("Position"))
		Expect(account.Mobile).To(Equal("Mobile"))
		Expect(account.Email).To(Equal("Email"))
		Expect(account.OfficeAddress).To(Equal("Office"))
		Expect(account.LandlinePhone).To(Equal("Landline"))
	})
})
