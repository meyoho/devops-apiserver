package ace_test

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	acectx "alauda.io/devops-apiserver/pkg/client/thirdparty/ace/context"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"context"
	"fmt"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http"
)

var _ = Describe("AlaudaCloudClient.rest_regions", func() {
	var (
		ctrl      *gomock.Controller
		transport *mhttp.MockRoundTripper
		client    *ace.AlaudaCloudClient
		ctx       context.Context
		name      string
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		transport = mhttp.NewMockRoundTripper(ctrl)
		client = ace.New(ace.Options{
			RootAccount: "alauda",
			Token:       "token",
			APIAddress:  "http://address",
			Transport:   transport,
		})
		name = "someuser"
		ctx = acectx.WithName(context.TODO(), name)
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	Describe("ListRegions", func() {
		It("should return region list", func() {
			request, err := http.NewRequest(http.MethodGet, "http://address/v2/regions/alauda/", nil)
			Expect(err).To(BeNil())
			request.Header.Add("Authorization", "Token token")
			request.Header.Add("Content-type", "application/json")

			transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, regionListResponseJson), nil)
			regions, err := client.ListRegions(ctx)
			Expect(err).To(BeNil())
			Expect(len(regions)).To(BeEquivalentTo(1))
			Expect(regions[0].Name).To(BeEquivalentTo("ace"))
		})
	})

	Describe("GetRegion", func() {
		It("should return region", func() {
			request, err := http.NewRequest(http.MethodGet, "http://address/v2/regions/alauda/ace/", nil)
			Expect(err).To(BeNil())
			request.Header.Add("Authorization", "Token token")
			request.Header.Add("Content-type", "application/json")

			transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, regionACE), nil)
			region, err := client.GetRegion(ctx, "ace")
			Expect(err).To(BeNil())
			Expect(region.Name).To(BeEquivalentTo("ace"))
		})

		Context("when region is not exist", func() {
			It("should return not found error", func() {
				request, err := http.NewRequest(http.MethodGet, "http://address/v2/regions/alauda/ace1/", nil)
				Expect(err).To(BeNil())
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")

				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusNotFound, request, notFoundJson), nil)
				_, err = client.GetRegion(ctx, "ace1")
				Expect(err).ToNot(BeNil())
				errList := err.(*ace.ErrorList)
				Expect(errList).ToNot(BeNil())
				Expect(errList.Errors[0].Code).To(BeEquivalentTo("resource_not_exist"))
				Expect(errList.Errors[0].Message).ToNot(BeEmpty())
				Expect(fmt.Sprint(errList.Errors[0].Source)).ToNot(BeEmpty())
			})
		})
	})
})

var notFoundJson = `{
    "errors": [
        {
            "source": "1007",
            "message": "The requested resource does not exist.",
            "code": "resource_not_exist"
        }
    ]
}`

var regionACE = `{
    "resource_actions": [
        "cluster:delete",
        "cluster:deploy",
        "cluster:update",
        "cluster:view",
        "cluster:clear",
        "cluster:create"
    ],
    "display_name": "ACE",
    "name": "ace",
    "container_manager": "KUBERNETES",
    "created_at": "2018-11-22T09:26:51.696050Z",
    "namespace": "alauda",
    "updated_at": "2019-02-18T03:40:11.408628Z",
    "platform_version": "v4",
    "state": "RUNNING",
    "features": {
        "devops": {}
    },
    "mirror": {},
    "id": "5f32006b-50f9-4805-a7b6-7cb675996ec6",
    "attr": {
        "cluster": {
            "nic": "eth0"
        },
        "docker": {
            "path": "/var/lib/docker",
            "version": "1.12.6"
        },
        "feature_namespace": "default",
        "kubernetes": {
            "endpoint": "https://10.0.129.161:6443",
            "version": "1.11.2",
            "type": "original",
            "token": "eyJaW8vc2VydmljZWFjY291bnQv0C_0-FGTAmIFJy4JBQ",
            "cni": {
                "network_policy": "",
                "cidr": "10.199.0.0/16",
                "type": "flannel",
                "backend": "vxlan"
            }
        },
        "cloud": {
            "name": "PRIVATE"
        }
    }
}`

var regionListResponseJson = `[
    {
        "resource_actions": [
            "cluster:delete",
            "cluster:deploy",
            "cluster:update",
            "cluster:view",
            "cluster:clear",
            "cluster:create"
        ],
        "display_name": "ACE",
        "features": {
            "devops": {}
        },
        "namespace": "alauda",
        "container_manager": "KUBERNETES",
        "attr": {
            "cluster": {
                "nic": "eth0"
            },
            "docker": {
                "path": "/var/lib/docker",
                "version": "1.12.6"
            },
            "feature_namespace": "default",
            "kubernetes": {
                "endpoint": "https://10.0.129.161:6443",
                "version": "1.11.2",
                "type": "original",
                "token": "eyJhbGciOFJy4JBQ",
                "cni": {
                    "network_policy": "",
                    "cidr": "10.199.0.0/16",
                    "type": "flannel",
                    "backend": "vxlan"
                }
            },
            "cloud": {
                "name": "PRIVATE"
            }
        },
        "platform_version": "v4",
        "mirror": {},
        "id": "5f32006b-50f9-4805-a7b6-7cb675996ec6",
        "name": "ace"
    }
]`
