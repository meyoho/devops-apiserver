package ace

import (
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/client/thirdparty"
)

// RoleUser user assigned to a role in ACE
type RoleUser struct {
	TemplateUUID        string    `json:"template_uuid"`
	TemplateDisplayName string    `json:"template_display_name"`
	TemplateName        string    `json:"template_name"`
	Namespace           string    `json:"namespace"`
	User                string    `json:"user"`
	AssignedAt          time.Time `json:"assigned_at"`
	RoleName            string    `json:"role_name"`
	RoleUUID            string    `json:"role_uuid"`
	Email               string    `json:"email"`
}

var _ thirdparty.ListItem = &RoleUser{}

// GetName returns a name of an item
func (r *RoleUser) GetName() string {
	return r.User
}

// RoleUserList List of role user
type RoleUserList struct {
	StandardList
	Items []*RoleUser `json:"results"`
}

var _ thirdparty.List = &RoleUserList{}

// GetItems return items of the list
func (rl *RoleUserList) GetItems() (items []thirdparty.ListItem) {
	items = make([]thirdparty.ListItem, len(rl.Items))
	if len(rl.Items) > 0 {
		for i, r := range rl.Items {
			items[i] = r
		}
	}
	return
}

// AddItems add items from another list
func (rl *RoleUserList) AddItems(list thirdparty.List) {
	items := list.GetItems()
	if len(items) > 0 {
		for _, i := range items {
			if role, ok := i.(*RoleUser); ok {
				rl.Items = append(rl.Items, role)
			}
		}
	}
}

// CleanupRoleName remove all necessary prefixes to restore
func CleanupRoleName(role *Role) string {
	baseName := role.Name
	switch {
	case role.SpaceName != "":
		baseName = strings.TrimPrefix(baseName, role.SpaceName+"-")
	case role.NamespaceUUID != "":
		slice := strings.Split(baseName, "-")
		baseName = slice[len(slice)-1]
	case role.ProjectName != "":
		baseName = strings.TrimPrefix(baseName, role.ProjectName+"-")
	}
	return baseName
}

// RoleUserAssignment role assignment
type RoleUserAssignment struct {
	RoleName           string `json:"role_name"`
	RoleUUID           string `json:"role_uuid"`
	IsRoleFromTemplate bool   `json:"is_template_role"`
	User               string `json:"username"`
	Email              string `json:"email"`
	SpaceName          string `json:"space_name"`
	// when the user is in k8s namespace, this filed will not be empty
	NamespaceUUID string `json:"namespace_uuid"`
}

// GetName return username
func (rua *RoleUserAssignment) GetName() string {
	return rua.User
}

// GetUsername returns the username
func (rua *RoleUserAssignment) GetUsername() string {
	return rua.GetName()
}

// GetEmail returns the email of the user
func (rua *RoleUserAssignment) GetEmail() string {
	return rua.Email
}

// GetRole returns the role of the user
func (rua *RoleUserAssignment) GetRole() string {
	return rua.RoleName
}

// GetSpace returns the space of the user
func (rua *RoleUserAssignment) GetSpace() string {
	return rua.SpaceName
}

// GetNamespaceUUID returns the namespace uuid of the user
func (rua *RoleUserAssignment) GetNamespaceUUID() string {
	return rua.NamespaceUUID
}

// IsProjectScopedRole returns true if the role assignment is of project scope (not space and not namespace)
func (rua *RoleUserAssignment) IsProjectScopedRole() bool {
	return rua.SpaceName == "" && rua.NamespaceUUID == ""
}

// RoleUserAssignmentList list abstraction for slice of *RoleUserAssignment
type RoleUserAssignmentList []*RoleUserAssignment

// GetPagging get pagging related data, here will only return list data
func (rl RoleUserAssignmentList) GetPagging() (int, int, int) {
	return len(rl), len(rl), 1
}

// GetItems return its content as []ListItem
func (rl RoleUserAssignmentList) GetItems() (items []thirdparty.ListItem) {
	items = make([]thirdparty.ListItem, len(rl))
	if len(rl) > 0 {
		for i, r := range rl {
			items[i] = r
		}
	}
	return
}

// HasSpaceRoles returns true if there is any space role assigned
func (rl RoleUserAssignmentList) HasSpaceRoles() bool {
	for _, role := range rl {
		if role.SpaceName != "" {
			return true
		}
	}
	return false
}

// AddItems add items from another list
func (rl RoleUserAssignmentList) AddItems(list thirdparty.List) {
	rl.AddListItems(list.GetItems())
}

// AddListItems add list items as []ListItem slice
func (rl RoleUserAssignmentList) AddListItems(items []thirdparty.ListItem) RoleUserAssignmentList {
	if len(items) > 0 {
		for _, i := range items {
			if role, ok := i.(*RoleUserAssignment); ok {
				rl = append(rl, role)
			}
		}
	}
	return rl
}
