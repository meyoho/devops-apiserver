package ace

import (
	"bytes"
	"encoding/json"
	glog "k8s.io/klog"
	"net/http"
)

const (
	contentTypeHeader   = "Content-type"
	HeaderContentType   = contentTypeHeader
	HeaderAuthorization = "Authorization"
	applicationJSON     = "application/json"
	ContentTypeJSON     = "application/json"
)

// Renderer for response body
type Renderer interface {
	Render(*http.Response, interface{}) error
	IsContentType(*http.Request) bool
}

// DefaultRenderer default API json renderer
var DefaultRenderer Renderer = JSONRenderer{}

// JSONRenderer renders reponse objects as json
type JSONRenderer struct{}

// Render render response object as json
func (JSONRenderer) Render(response *http.Response, data interface{}) (err error) {
	// no need to render result to data
	if data == nil {
		return nil
	}
	body := new(bytes.Buffer)
	if _, err = body.ReadFrom(response.Body); err != nil {
		return
	}
	byts := body.Bytes()
	glog.V(9).Infof("%s %s %d , Response Body:%s", response.Request.Method, response.Request.URL.String(), response.StatusCode, string(byts))
	err = json.Unmarshal(byts, data)
	if err != nil {
		glog.Errorf("Unmarshal to %T error:%s, Response: %s", data, err, string(byts))
		return err
	}
	return
}

// IsContentType returns true if it can render the content type
func (JSONRenderer) IsContentType(req *http.Request) bool {
	return req.Header.Get(contentTypeHeader) == applicationJSON
}
