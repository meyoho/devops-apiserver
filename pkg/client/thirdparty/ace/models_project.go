package ace

import "time"

type ProjectList struct {
	StandardList
	Items []*Project `json:"results"`
}

// Project a project of ace
type Project struct {
	UUID        string           `json:"uuid"`
	DisplayName string           `json:"display_name"`
	Description string           `json:"description"`
	CreatedAt   time.Time        `json:"created_at"`
	UpdatedAt   time.Time        `json:"updated_at"`
	Namespace   string           `json:"namespace"`
	Token       string           `json:"token"`
	Name        string           `json:"name"`
	Clusters    []ProjectCluster `json:"clusters"`
	AdminList   []ProjectAdmin   `json:"admin_list"`
}

type ProjectAdmin struct {
	Name     string `json:"name"`
	RealName string `json:"realname"`
}

type ProjectCluster struct {
	DisplayName string `json:"display_name"`
	UUID        string `json:"uuid"`
	ProjectUUID string `json:"project_uuid"`
	ServiceType string `json:"service_type"`
	Name        string `json:"name"`
}
