package ace

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	acectx "alauda.io/devops-apiserver/pkg/client/thirdparty/ace/context"
	"context"
	"fmt"
	"net/http"
)

const apiVersionV1 = "v1"
const resourceRoles = "roles"
const subresourceUsers = "users"
const resourceRoleusers = "role-users"

// RoleExtension extension for roles
type RoleExtension interface {
	GetRoles(ctx context.Context, opts ...RequestOptions) (list *RoleList, err error)
	GetUsersOfRole(ctx context.Context, opts ...RequestOptions) (list *RoleUserList, err error)
	GetRolesWithUsers(ctx context.Context, opts ...RequestOptions) (roleUserList RoleUserAssignmentList, err error)
}

var _ RoleExtension = &AlaudaCloudClient{}

// GetRoles get roles of the specified root account using provided RequestOptions
// /v1/roles/{org_name}/
func (ace *AlaudaCloudClient) GetRoles(ctx context.Context, opts ...RequestOptions) (list *RoleList, err error) {
	var request *http.Request
	request, err = ace.NewReq().
		Get().
		JSON().
		Path(ace.opts.APIAddress, apiVersionV2, resourceRoles, ace.opts.RootAccount).
		Params(opts...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return
	}
	list = &RoleList{}
	err = ace.DoRequest(request, list)
	return
}

func (ace *AlaudaCloudClient) GetRolesUsers(ctx context.Context, opts ...RequestOptions) (list *RoleUsersList, err error) {
	var request *http.Request
	request, err = ace.NewReq().
		Get().
		JSON().
		Path(ace.opts.APIAddress, apiVersionV1, resourceRoleusers, ace.opts.RootAccount).
		Params(opts...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return
	}
	list = &RoleUsersList{}
	err = ace.DoRequest(request, list)

	return
}

// GetRolesUsersIterator encapsulates GetRolesUsers functions in a signature used by ListIterator
func (ace *AlaudaCloudClient) GetRolesUsersIterator(ctx context.Context, opts ...thirdparty.ListOptions) (thirdparty.List, error) {
	return ace.GetRolesUsers(ctx, AsRequestOptions(opts)...)
}

// GetRolesIterator encapsulates GetRoles functions in a signature used by ListIterator
func (ace *AlaudaCloudClient) GetRolesIterator(ctx context.Context, opts ...thirdparty.ListOptions) (thirdparty.List, error) {
	return ace.GetRoles(ctx, AsRequestOptions(opts)...)
}

// GetUsersOfRole fetch users using a particular role. The role name should be given in the context
// using ace/context.WithName
// // /v1/roles/{org_name}/{role_name}/users/
func (ace *AlaudaCloudClient) GetUsersOfRole(ctx context.Context, opts ...RequestOptions) (list *RoleUserList, err error) {
	var request *http.Request
	request, err = ace.NewReq().
		Get().
		JSON().
		Path(
			ace.opts.APIAddress,
			apiVersionV1,
			resourceRoles,
			ace.opts.RootAccount,
			acectx.Name(ctx),
			subresourceUsers,
		).
		Params(opts...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return
	}
	list = &RoleUserList{}
	err = ace.DoRequest(request, list)
	return
}

// GetUsersOfRolesIterator encapsulates GetUsersOfRole functions in a signature used by ListIterator
func (ace *AlaudaCloudClient) GetUsersOfRolesIterator(ctx context.Context, opts ...thirdparty.ListOptions) (thirdparty.List, error) {
	return ace.GetUsersOfRole(ctx, AsRequestOptions(opts)...)
}

// fetches all roles from a project (using RequestOptions) and fetch all the users for all roles
// and returns a simplified role-user relationship table
func (ace *AlaudaCloudClient) GetRolesWithUsers(ctx context.Context, opts ...RequestOptions) (result RoleUserAssignmentList, err error) {

	var roleUsersList thirdparty.List
	roleUsersList, err = ace.Iterate(ace.GetRolesUsersIterator)(ctx, AsListOptions(opts)...)
	if err != nil {
		return
	}

	result = ace.getListItemMapAsRoleUserAssignmentList(roleUsersList.(*RoleUsersList))
	return
}

// getListItemMapAsRoleUserAssignmentList aggregate a multi dimension list to a plain list
// and do some treatment on the
func (ace *AlaudaCloudClient) getListItemMapAsRoleUserAssignmentList(roleList *RoleUsersList) (result RoleUserAssignmentList) {
	result = make(RoleUserAssignmentList, 0, len(result)*2)

	index := map[string]struct{}{} // space/template/username : exists

	for _, role := range roleList.Items {
		for _, user := range role.UserList {

			// we will hava many namespace_admin if current user in many biz namespace, we should just keep one namespace_admin
			key := fmt.Sprintf("%s/%s/%s", role.SpaceName, user.TemplateName, user.User)
			if _, ok := index[key]; ok {
				continue // this user in this space with this role  has already append, so skip it
			}

			index[key] = struct{}{}
			item := &RoleUserAssignment{
				RoleName:           user.RoleName,
				RoleUUID:           user.RoleUUID,
				User:               user.User,
				IsRoleFromTemplate: user.TemplateName != "",
				SpaceName:          role.SpaceName,
				Email:              user.Email,
				NamespaceUUID:      role.NamespaceUUID,
			}
			if item.IsRoleFromTemplate {
				item.RoleName = CleanupRoleName(&role.Role)
			}
			result = append(result, item)
		}
	}
	return result
}

func (ace *AlaudaCloudClient) assignDataToUserList(roleUsersList RoleUserAssignmentList, userDetails map[thirdparty.ListItem]thirdparty.List) {
	userIndex := map[string]*Account{}
	for _, list := range userDetails {
		items := list.GetItems()
		if len(items) > 0 {
			account := items[0].(*Account)
			userIndex[account.Username] = account
		}
	}
	for i, roleUser := range roleUsersList {
		if account, ok := userIndex[roleUser.User]; ok {
			roleUser.Email = account.Email
			roleUsersList[i] = roleUser
		}
	}
}
