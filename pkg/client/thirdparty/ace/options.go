package ace

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"net/http"
	"time"
)

// Options options for constructor
type Options struct {
	Transport          http.RoundTripper
	Timeout            time.Duration
	Token              string
	RootAccount        string
	APIAddress         string
	Renderers          []Renderer
	ErrorHandlers      []ErrorHandler
	ListIterator       thirdparty.ListIterator
	MultipleIterator   MultipleIterator
	AnnotationProvider devops.AnnotationProvider
}

func (opts Options) init() Options {
	if opts.Timeout < time.Second {
		opts.Timeout = time.Second * 30
	}
	if opts.Transport == nil {
		opts.Transport = http.DefaultTransport
	}
	if opts.APIAddress == "" {
		opts.APIAddress = "https://api-cloud.alauda.cn"
	}
	if len(opts.Renderers) == 0 {
		opts.Renderers = []Renderer{DefaultRenderer}
	}
	if len(opts.ErrorHandlers) == 0 {
		opts.ErrorHandlers = []ErrorHandler{DefaultErrorHandler}
	}
	if opts.ListIterator == nil {
		opts.ListIterator = thirdparty.DefaultListIterator
	}
	if opts.MultipleIterator == nil {
		opts.MultipleIterator = DefaultMultipleIterator
	}
	return opts
}
