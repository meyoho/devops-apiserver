package ace_test

import (
	"context"
	"encoding/json"
	"fmt"
	"k8s.io/api/core/v1"
	"net/http"

	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	acectx "alauda.io/devops-apiserver/pkg/client/thirdparty/ace/context"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var (
	clusterName = "devops"
	clusterID   = "1"
)

var _ = Describe("AlaudaCloudClient.rest_cluster", func() {
	var (
		ctrl      *gomock.Controller
		transport *mhttp.MockRoundTripper
		client    *ace.AlaudaCloudClient
		ctx       context.Context
		name      string
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		transport = mhttp.NewMockRoundTripper(ctrl)
		client = ace.New(ace.Options{
			RootAccount: "alauda",
			Token:       "token",
			APIAddress:  "http://address",
			Transport:   transport,
		})
		name = "someuser"
		ctx = acectx.WithName(context.TODO(), name)
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	Describe("ListK8SResources", func() {
		Context("list cluster scope resources", func() {
			It("should return cluster resource list", func() {
				request, err := http.NewRequest(http.MethodGet, fmt.Sprintf("http://address/v2/kubernetes/clusters/%s/nodes/", clusterID), nil)
				Expect(err).To(BeNil())
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, nodesResponseJson), nil)
				nodes, err := client.ListK8SResources(ctx, clusterID, "", "nodes", ace.PageOpts(1, 10))
				Expect(err).To(BeNil())
				Expect(nodes.Count).To(BeEquivalentTo(2))
				Expect(len(nodes.Items)).To(BeEquivalentTo(2))
			})
		})

		Context("list namespaced resources", func() {
			It("should return namespaced resource list", func() {
				request, err := http.NewRequest(http.MethodGet, fmt.Sprintf("http://address/v2/kubernetes/clusters/%s/pods/ns1/", clusterID), nil)
				Expect(err).To(BeNil())
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, podsResponseJson), nil)

				pods, err := client.ListK8SResources(ctx, clusterID, "ns1", "pods", ace.PageOpts(1, 10))
				Expect(err).To(BeNil())
				Expect(pods.Count).To(BeEquivalentTo(2))
				Expect(len(pods.Items)).To(BeEquivalentTo(2))
			})
		})
	})

	Describe("GetK8SResource", func() {
		Context("get cluster scope resource", func() {
			It("should return cluster resource", func() {
				request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("http://address/v2/kubernetes/clusters/%s/nodes/master1", clusterID), nil)
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, masterResponseJson), nil)

				node, err := client.GetK8SResource(ctx, clusterID, "", "nodes", "master1")
				Expect(err).To(BeNil())
				metadata := node.Kubernetes["metadata"].(map[string]interface{})
				Expect(metadata["name"]).To(BeEquivalentTo("devops-int-master1"))
			})
		})

		Context("get namespaced resources", func() {
			It("should return namespaced resource", func() {
				request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("http://address/v2/kubernetes/clusters/%s/pods/ns1/devops-api", clusterID), nil)
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, podResponseJson), nil)

				pod, err := client.GetK8SResource(ctx, clusterID, "ns1", "pods", "devops-api")
				Expect(err).To(BeNil())
				Expect(pod).NotTo(BeNil())
				metadata := pod.Kubernetes["metadata"].(map[string]interface{})
				Expect(metadata["name"]).To(BeEquivalentTo("pod1"))
			})
		})
	})

	Describe("UpdateK8SResource", func() {
		Context("update cluster scope resource", func() {
			It("should return cluster updated resource", func() {
				request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf("http://address/v2/kubernetes/clusters/%s/nodes/master1", clusterID), nil)
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusNoContent, request, masterResponseJson), nil)

				err := client.UpdateK8SResource(ctx, clusterID, "", "nodes", "master1", &v1.Node{})
				Expect(err).To(BeNil())
			})
		})

		Context("update namespaced resources", func() {
			It("should return namespaced updated resource", func() {
				request, _ := http.NewRequest(http.MethodPut, fmt.Sprintf("http://address/v2/kubernetes/clusters/%s/pods/ns1/devops-api", clusterID), nil)
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusNoContent, request, podResponseJson), nil)

				err := client.UpdateK8SResource(ctx, clusterID, "ns1", "pods", "devops-api", &v1.Node{})
				Expect(err).To(BeNil())
			})
		})
	})

	Describe("CreateK8SResource", func() {
		Context("create cluster scope resource", func() {
			It("should return cluster created resource", func() {
				request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("http://address/v2/kubernetes/clusters/%s/nodes/", clusterID), nil)
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, masterResponseJson), nil)

				node, err := client.CreateK8SResource(ctx, clusterID, "nodes", &v1.Node{})
				Expect(err).To(BeNil())
				metadata := node.Kubernetes["metadata"].(map[string]interface{})
				Expect(metadata["name"]).To(BeEquivalentTo("devops-int-master1"))
			})
		})

		Context("created namespaced resources", func() {
			It("should return namespaced created resource", func() {
				request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("http://address/v2/kubernetes/clusters/%s/pods/", clusterID), nil)
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, podResponseJson), nil)

				pod, err := client.CreateK8SResource(ctx, clusterID, "pods", &v1.Pod{})
				Expect(err).To(BeNil())
				Expect(pod).NotTo(BeNil())
				metadata := pod.Kubernetes["metadata"].(map[string]interface{})
				Expect(metadata["name"]).To(BeEquivalentTo("pod1"))
			})
		})
	})

	Describe("DeleteK8SResource", func() {
		Context("delete cluster scope resource", func() {
			It("should delete cluster resource", func() {
				request, _ := http.NewRequest(http.MethodDelete, fmt.Sprintf("http://address/v2/kubernetes/clusters/%s/nodes/devops-int-master1", clusterID), nil)
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusNoContent, request, masterResponseJson), nil)

				err := client.DeleteK8SResource(ctx, clusterID, "", "nodes", "devops-int-master1", nil)
				Expect(err).To(BeNil())
			})
		})

		Context("delete namespaced resources", func() {
			It("should return delete namespaced resource", func() {
				request, _ := http.NewRequest(http.MethodDelete, fmt.Sprintf("http://address/v2/kubernetes/clusters/%s/pods/ns1/pod1", clusterID), nil)
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusNoContent, request, podResponseJson), nil)

				err := client.DeleteK8SResource(ctx, clusterID, "ns1", "pods", "pod1", nil)
				Expect(err).To(BeNil())
			})
		})
	})

	Describe("IsK8sResourceNotFound", func() {
		Context("when it is k8s resource not found error", func() {
			It("", func() {
				var err = &ace.ErrorList{StatusCode: 404}
				unmarshal([]byte(aceNotFoundErrorJson), err)
				Expect(ace.IsK8sResourceNotFound(err.StatusCode, (*err).Errors[0])).To(BeTrue())
			})
		})
		Context("when it is not k8s resource not found error", func() {
			It("", func() {
				var err = &ace.ErrorList{StatusCode: 400}
				unmarshal([]byte(aceAnyErrorJson), err)
				Expect(ace.IsK8sResourceNotFound(err.StatusCode, (*err).Errors[0])).To(BeFalse())
			})
		})
	})

})

func unmarshal(jsonBts []byte, obj interface{}) {
	err := json.Unmarshal(jsonBts, obj)
	Expect(err).To(BeNil())
}

var aceNotFoundErrorJson = `{
    "errors": [
        {
            "source": 1044,
            "message": "nodes \"aaa\" not found",
            "code": "kubernetes_error"
        }
    ]
}`

var aceAnyErrorJson = `{
    "errors": [
        {
            "source": 1044,
            "message": "what ever",
            "code": "kubernetes_error"
        }
    ]
}`

var node = v1.Node{}

var nodesResponseJson = `{
    "count": 2,
    "next": false,
    "previous": false,
    "page_size": 20,
    "num_pages": 1,
    "results": [
      {
        "kubernetes": {}
      },
      {
        "kubernetes": {}
      }
    ]
  }
`

var masterResponseJson = `{
    "kubernetes": {
			"apiVersion": "v1",
				"kind": "Node",
				"metadata": {
						"annotations": {
						},
						"creationTimestamp": "2018-11-22T09:18:56Z",
						"labels": {
								"beta.kubernetes.io/arch": "amd64"
						},
						"name": "devops-int-master1",
						"resourceVersion": "22607296",
						"selfLink": "/api/v1/nodes/devops-int-master1",
						"uid": "a327e18f-ee37-11e8-9985-52540052ef9b"
				}
			}
    }
`
var createdPodRespJson = fmt.Sprintf("[%s]", podResponseJson)
var podResponseJson = `{
    "kubernetes": {
			"apiVersion": "v1",
				"kind": "Pod",
				"metadata": {
						"annotations": {
						},
						"creationTimestamp": "2018-11-22T09:18:56Z",
						"labels": {
								"beta.kubernetes.io/arch": "amd64"
						},
						"name": "pod1",
						"resourceVersion": "22607296",
						"selfLink": "/api/v1/nodes/devops-int-master1",
						"uid": "a327e18f-ee37-11e8-9985-52540052ef9b"
				}
			}
    }
`

var podsResponseJson = `{
    "count": 2,
    "next": false,
    "previous": false,
    "page_size": 20,
    "num_pages": 1,
    "results": [
      {
        "kubernetes": {}
      },
      {
        "kubernetes": {}
      }
    ]
  }
`
