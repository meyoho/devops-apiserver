package ace

import (
	"context"
	"net/http"
)

const resourceSpaces = "spaces"

// OrgExtension org related methods on AlaudaCloudClient
type SpaceExtension interface {
	ListSpaces(ctx context.Context, projectName string, opts ...RequestOptions) (result *SpaceList, err error)
}

// ListSpaces list spaces
// /v1/spaces/{org_name}/
func (ace *AlaudaCloudClient) ListSpaces(ctx context.Context, projectName string, opts ...RequestOptions) (result *SpaceList, err error) {
	if projectName != "" {
		opts = append(opts, ProjectOpts(projectName))
	}

	var request *http.Request
	request, err = ace.NewReq().
		Get().
		JSON().
		Path(
			ace.opts.APIAddress,
			apiVersionV2,
			resourceSpaces,
			ace.opts.RootAccount,
		).
		Params(opts...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return
	}

	paged := false
	for _, opt := range opts {
		if _, ok := opt.(PageOptions); ok {
			paged = true
		}
	}

	result = &SpaceList{}
	if paged {
		err = ace.DoRequest(request, result)
		return result, err
	}

	items := &[]*Space{}
	err = ace.DoRequest(request, items)
	if err != nil {
		return nil, err
	}
	result.Items = *items
	result.Count = len(*items)
	result.PageSize = len(*items)
	result.NumberOfPages = len(*items)
	return result, err
}
