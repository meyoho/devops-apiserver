package ace_test

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"context"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http"
)

var _ = Describe("ListProjects", func() {

	var (
		ctrl      *gomock.Controller
		transport *mhttp.MockRoundTripper
		client    *ace.AlaudaCloudClient
		ctx       context.Context
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		transport = mhttp.NewMockRoundTripper(ctrl)
		client = ace.New(ace.Options{
			RootAccount: "alauda",
			Token:       "token",
			APIAddress:  "http://address",
			Transport:   transport,
		})
		ctx = context.TODO()
	})

	Context("When request without page", func() {
		It("Should return all projects", func() {
			request, err := http.NewRequest(http.MethodGet, "http://address/v1/projects/alauda/", nil)
			Expect(err).To(BeNil())
			transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, projectsListResponseJson), nil)
			projects, err := client.ListProjects(ctx)
			Expect(err).To(BeNil())
			Expect(projects.Count).To(BeEquivalentTo(2))
			Expect(len(projects.Items)).To(BeEquivalentTo(2))
			Expect(projects.Items[0].Name).To(BeEquivalentTo("figer"))
			Expect(projects.Items[1].Name).To(BeEquivalentTo("liangzhao"))
		})
	})

	Context("When request with page", func() {
		It("Should return paged projects", func() {
			request, err := http.NewRequest(http.MethodGet, "http://address/v1/projects/alauda/?page=1&page_size=20", nil)
			Expect(err).To(BeNil())
			transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, projectsPagedJson), nil)
			projects, err := client.ListProjects(ctx, ace.PageOpts(1, 20))
			Expect(err).To(BeNil())
			Expect(projects.Count).To(BeEquivalentTo(22))
			Expect(len(projects.Items)).To(BeEquivalentTo(2))
			Expect(projects.Items[0].Name).To(BeEquivalentTo("figer"))
			Expect(projects.Items[1].Name).To(BeEquivalentTo("liangzhao"))
		})
	})

})

var projectsListResponseJson = `[
        {
            "status": "success",
            "template_uuid": "f1cd6c60-25e1-4e6b-88ad-8c1cc7b929d9",
            "uuid": "47ed0463-ac2c-4d58-abdc-dd95465aa295",
            "display_name": "figer",
            "description": "",
            "created_at": "2019-04-04T03:12:36Z",
            "namespace": "alauda",
            "updated_at": "2019-04-04T03:12:36Z",
            "token": "5bf2da12c470aab7eae14343c47513ae5604ddc5",
            "resource_actions": [
                "project:create",
                "project:delete",
                "project:update",
                "project:view"
            ],
            "template": "empty-template",
            "service_type": [
                "kubernetes"
            ],
            "clusters": [
                {
                    "display_name": "ACE",
                    "uuid": "5f32006b-50f9-4805-a7b6-7cb675996ec6",
                    "project_uuid": "47ed0463-ac2c-4d58-abdc-dd95465aa295",
                    "created_at": "2019-04-04T03:12:36Z",
                    "quota": {
                        "storage": -1,
                        "pvc_num": -1,
                        "pods": -1,
                        "cpu": -1,
                        "memory": -1
                    },
                    "updated_at": "2019-04-04T03:12:36Z",
                    "mirror": {},
                    "service_type": "kubernetes",
                    "total_quota": "{'pods': 440, 'storage': 180963, 'cpu': 28, 'memory': 53}",
                    "name": "ace"
                }
            ],
            "admin_list": [
                {
                    "name": "jyhan",
                    "realname": ""
                }
            ],
            "name": "figer"
        },
        {
            "status": "success",
            "template_uuid": "f1cd6c60-25e1-4e6b-88ad-8c1cc7b929d9",
            "uuid": "148f090b-1a3e-4fb5-8239-02b7255f4e9d",
            "display_name": "liangzhao",
            "description": "",
            "created_at": "2019-04-02T10:09:07Z",
            "namespace": "alauda",
            "updated_at": "2019-04-02T10:09:07Z",
            "token": "5bf2da12c470aab7eae14343c47513ae5604ddc5",
            "resource_actions": [
                "project:update",
                "project:view",
                "project:create",
                "project:delete"
            ],
            "template": "empty-template",
            "service_type": [
                "kubernetes"
            ],
            "clusters": [
                {
                    "display_name": "ACE",
                    "uuid": "5f32006b-50f9-4805-a7b6-7cb675996ec6",
                    "project_uuid": "148f090b-1a3e-4fb5-8239-02b7255f4e9d",
                    "created_at": "2019-04-02T10:09:07Z",
                    "quota": {
                        "storage": -1,
                        "pvc_num": -1,
                        "pods": -1,
                        "cpu": -1,
                        "memory": -1
                    },
                    "updated_at": "2019-04-02T10:09:07Z",
                    "mirror": {},
                    "service_type": "kubernetes",
                    "total_quota": "{'pods': 440, 'storage': 180963, 'cpu': 28, 'memory': 53}",
                    "name": "ace"
                }
            ],
            "admin_list": [
                {
                    "name": "liangzhao",
                    "realname": ""
                }
            ],
            "name": "liangzhao"
        }
    ]`
var projectsPagedJson = `{
    "count": 22,
    "num_pages": 2,
    "results": [
        {
            "status": "success",
            "template_uuid": "f1cd6c60-25e1-4e6b-88ad-8c1cc7b929d9",
            "uuid": "47ed0463-ac2c-4d58-abdc-dd95465aa295",
            "display_name": "figer",
            "description": "",
            "created_at": "2019-04-04T03:12:36Z",
            "namespace": "alauda",
            "updated_at": "2019-04-04T03:12:36Z",
            "token": "5bf2da12c470aab7eae14343c47513ae5604ddc5",
            "resource_actions": [
                "project:create",
                "project:delete",
                "project:update",
                "project:view"
            ],
            "template": "empty-template",
            "service_type": [
                "kubernetes"
            ],
            "clusters": [
                {
                    "display_name": "ACE",
                    "uuid": "5f32006b-50f9-4805-a7b6-7cb675996ec6",
                    "project_uuid": "47ed0463-ac2c-4d58-abdc-dd95465aa295",
                    "created_at": "2019-04-04T03:12:36Z",
                    "quota": {
                        "storage": -1,
                        "pvc_num": -1,
                        "pods": -1,
                        "cpu": -1,
                        "memory": -1
                    },
                    "updated_at": "2019-04-04T03:12:36Z",
                    "mirror": {},
                    "service_type": "kubernetes",
                    "total_quota": "{'pods': 440, 'storage': 180963, 'cpu': 28, 'memory': 53}",
                    "name": "ace"
                }
            ],
            "admin_list": [
                {
                    "name": "jyhan",
                    "realname": ""
                }
            ],
            "name": "figer"
        },
        {
            "status": "success",
            "template_uuid": "f1cd6c60-25e1-4e6b-88ad-8c1cc7b929d9",
            "uuid": "148f090b-1a3e-4fb5-8239-02b7255f4e9d",
            "display_name": "liangzhao",
            "description": "",
            "created_at": "2019-04-02T10:09:07Z",
            "namespace": "alauda",
            "updated_at": "2019-04-02T10:09:07Z",
            "token": "5bf2da12c470aab7eae14343c47513ae5604ddc5",
            "resource_actions": [
                "project:update",
                "project:view",
                "project:create",
                "project:delete"
            ],
            "template": "empty-template",
            "service_type": [
                "kubernetes"
            ],
            "clusters": [
                {
                    "display_name": "ACE",
                    "uuid": "5f32006b-50f9-4805-a7b6-7cb675996ec6",
                    "project_uuid": "148f090b-1a3e-4fb5-8239-02b7255f4e9d",
                    "created_at": "2019-04-02T10:09:07Z",
                    "quota": {
                        "storage": -1,
                        "pvc_num": -1,
                        "pods": -1,
                        "cpu": -1,
                        "memory": -1
                    },
                    "updated_at": "2019-04-02T10:09:07Z",
                    "mirror": {},
                    "service_type": "kubernetes",
                    "total_quota": "{'pods': 440, 'storage': 180963, 'cpu': 28, 'memory': 53}",
                    "name": "ace"
                }
            ],
            "admin_list": [
                {
                    "name": "liangzhao",
                    "realname": ""
                }
            ],
            "name": "liangzhao"
        }
    ],
    "page_size": 20,
    "next": "?page=2",
    "previous": null
}`
