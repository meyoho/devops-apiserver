package ace

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"net/http"
)

// Interface interface for ace client
type Interface interface {
	DoRequest(request *http.Request, data interface{}) (err error)
	thirdparty.IteratorExtension
	MultipleIteratorExtension
	RoleExtension
	OrgExtension

	ClusterExtension
	SpaceExtension
	RegionExtension
	ProjectExtension
}
