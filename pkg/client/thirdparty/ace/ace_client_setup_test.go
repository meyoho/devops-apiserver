package ace_test

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"

	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestACEClient(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("ace.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/client/thirdparty/ace", []Reporter{junitReporter})
}

var _ = Describe("New", func() {
	var (
		opts      ace.Options
		aceClient *ace.AlaudaCloudClient
	)

	BeforeEach(func() {
		opts = ace.Options{}
	})

	JustBeforeEach(func() {
		aceClient = ace.New(opts)
	})

	It("cont", func() {
		Expect(aceClient).ToNot(BeNil())
	})
})

func getResponseWithBody(status int, request *http.Request, body string) *http.Response {
	return &http.Response{
		StatusCode: status,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 0,
		Header:     http.Header{},
		Request:    request,
		Body:       ioutil.NopCloser(bytes.NewBufferString(body)),
	}
}
