package ace

import (
	"encoding/json"
	"fmt"
	"k8s.io/apimachinery/pkg/runtime"
	glog "k8s.io/klog"
	"reflect"
)

// KubernetesResource represent Kubernetes resource response of ace
type KubernetesResource struct {
	Kubernetes kubernetesObject `json:"kubernetes"`
}

type kubernetesObject map[string]interface{}

//Into will transfer current interface{} to  transferObj
// eg. kObj.Into(&CodeRepoService{})
func (acek8sObj *kubernetesObject) Into(transferObj interface{}) error {

	bts, err := json.Marshal(acek8sObj)
	if err != nil {
		return err
	}
	err = json.Unmarshal(bts, transferObj)
	return err
}

type KubernetesResourceList struct {
	StandardList
	Items []*KubernetesResource `json:"results"`
}

// Into will transfer aceK8sList.Items to k8s list object
// eg. aceK8sList.Into(&v1.CodeRepoServiceList{})
func (aceK8sList *KubernetesResourceList) Into(k8sListObject runtime.Object) error {
	if aceK8sList.Count == 0 {
		return nil
	}

	resultItemsField, found := reflect.TypeOf(k8sListObject).Elem().FieldByName("Items")
	if !found {
		return fmt.Errorf("%T not contains a field named 'Items'", k8sListObject)
	}

	resultItemType := resultItemsField.Type.Elem()
	itemsSlice := reflect.MakeSlice(reflect.SliceOf(resultItemType), 0, aceK8sList.Count) // itemsSlice := make([]Item, 0, list.Count)

	for _, k8sRes := range aceK8sList.Items {
		itemValuePointer := reflect.New(resultItemType) // itemValuePointer := &Item{}

		err := k8sRes.Kubernetes.Into(itemValuePointer.Interface())
		if err != nil {
			glog.Errorf("cannot transfer k8s resource %#v to %T, error:%s", k8sRes.Kubernetes, itemValuePointer, err)
			return err
		}

		itemsSlice = reflect.Append(itemsSlice, itemValuePointer.Elem()) // itemsSlice = append(itemsSlice, *itemValuePointer)
	}

	//set items to result
	reflect.ValueOf(k8sListObject).Elem().FieldByName("Items").Set(itemsSlice)

	return nil
}
