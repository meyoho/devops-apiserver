package ace

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"context"
	"sync"

	acectx "alauda.io/devops-apiserver/pkg/client/thirdparty/ace/context"
)

// MultipleIteratorExtension multiple iterator extension for ACE client
type MultipleIteratorExtension interface {
	MultipleIteration(listFunc thirdparty.ListFunc, ctxGen ContextGenerator, list thirdparty.List) (map[thirdparty.ListItem]thirdparty.List, []error)
	CustomMultipleIterate(listFunc thirdparty.ListFunc, ctxGen ContextGenerator, list thirdparty.List, multiIterator MultipleIterator) (map[thirdparty.ListItem]thirdparty.List, []error)
}

// ContextGenerator used with the multiple iterator
// to generate the context for each search
type ContextGenerator func(item thirdparty.ListItem) (context.Context, []RequestOptions)

// MultipleIterator is a iterator that can do multiple requests
// using one item as a dependency each time
type MultipleIterator interface {
	// its only function takes a ListFunc (which could be an already iterated)
	// a context generator to generate context for each request
	// and a list of items to be feeded to the context generator
	IterateList(thirdparty.ListFunc, ContextGenerator, thirdparty.List) (map[thirdparty.ListItem]thirdparty.List, []error)
}

type multipleIteratorItem struct {
	Item thirdparty.ListItem
	List thirdparty.List
}

// DefaultMultipleIterator default multiple iterator for ace client
var DefaultMultipleIterator = StandardMultipleIterator{}

// StandardMultipleIterator simple standard concurrent multiple iterator
type StandardMultipleIterator struct{}

var _ MultipleIterator = StandardMultipleIterator{}

// MultipleIterate iterates through a list and
func (StandardMultipleIterator) IterateList(listFunc thirdparty.ListFunc, ctxGen ContextGenerator, list thirdparty.List) (result map[thirdparty.ListItem]thirdparty.List, errs []error) {
	var wait sync.WaitGroup
	listItems := list.GetItems()
	errs = make([]error, len(listItems))
	channel := make(chan multipleIteratorItem, len(listItems))
	result = make(map[thirdparty.ListItem]thirdparty.List)
	for i, item := range listItems {
		wait.Add(1)
		outerContext, options := ctxGen(item)
		go func(index int, item thirdparty.ListItem, ctx context.Context, opts []RequestOptions) {
			defer wait.Done()
			items, err := listFunc(ctx, AsListOptions(opts)...)
			errs[index] = err
			channel <- multipleIteratorItem{Item: item, List: items}
		}(i, item, outerContext, options)
	}
	wait.Wait()
	for len(channel) > 0 {
		i := <-channel
		if i.List != nil {
			result[i.Item] = i.List
		}
	}
	errs = ClearErrorList(errs)
	return
}

// GetStandardContextGenerator standard context generator for ListItem
func GetStandardContextGenerator(ctx context.Context, opts ...RequestOptions) ContextGenerator {
	return func(item thirdparty.ListItem) (inncontext context.Context, reqOpts []RequestOptions) {
		inncontext = acectx.WithName(ctx, item.GetName())
		reqOpts = opts
		return
	}
}
