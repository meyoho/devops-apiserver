package ace_test

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	acectx "alauda.io/devops-apiserver/pkg/client/thirdparty/ace/context"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"context"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http"
)

var _ = Describe("AlaudaCloudClient.rest_spaces", func() {
	var (
		ctrl      *gomock.Controller
		transport *mhttp.MockRoundTripper
		client    *ace.AlaudaCloudClient
		ctx       context.Context
		name      string
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		transport = mhttp.NewMockRoundTripper(ctrl)
		client = ace.New(ace.Options{
			RootAccount: "alauda",
			Token:       "token",
			APIAddress:  "http://address",
			Transport:   transport,
		})
		name = "someuser"
		ctx = acectx.WithName(context.TODO(), name)
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	Describe("ListSpaces", func() {
		Context("list spaces without pages", func() {
			It("should return space list", func() {
				request, err := http.NewRequest(http.MethodGet, "http://address/v2/spaces/alauda/?projcet_name=a9", nil)
				Expect(err).To(BeNil())
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, spaceListResponseJson), nil)
				spaces, err := client.ListSpaces(ctx, "a9")
				Expect(err).To(BeNil())
				Expect(spaces.Count).To(BeEquivalentTo(1))
				Expect(len(spaces.Items)).To(BeEquivalentTo(1))
				Expect(spaces.Items[0].Name).To(BeEquivalentTo("a9-biz"))
				Expect(spaces.Items[0].Namespaces[0].ClusterName).To(BeEquivalentTo("ace"))
			})
		})

		Context("list spaces with pages", func() {
			It("should return space list", func() {
				request, err := http.NewRequest(http.MethodGet, "http://address/v2/spaces/alauda/?projcet_name=a9&page=1&page_size=10", nil)
				Expect(err).To(BeNil())
				request.Header.Add("Authorization", "Token token")
				request.Header.Add("Content-type", "application/json")
				transport.EXPECT().RoundTrip(mhttp.NewRequestMatcher(request)).Return(getResponseWithBody(http.StatusOK, request, spacePagedResponseJson), nil)
				spaces, err := client.ListSpaces(ctx, "a9", ace.PageOpts(1, 10))
				Expect(err).To(BeNil())
				Expect(spaces.Count).To(BeEquivalentTo(1))
				Expect(spaces.PageSize).To(BeEquivalentTo(10))
				Expect(len(spaces.Items)).To(BeEquivalentTo(1))
				Expect(spaces.Items[0].Name).To(BeEquivalentTo("a9-biz"))
				Expect(spaces.Items[0].Namespaces[0].ClusterName).To(BeEquivalentTo("ace"))
			})
		})
	})
})

var spaceListResponseJson = `[
    {
        "status": "running",
        "resource_actions": [
            "space:bind_namespace",
            "space:consume",
            "space:create",
            "space:delete",
            "space:unbind_namespace",
            "space:update",
            "space:view"
        ],
        "uuid": "1c96776f-db9d-4b80-a296-d6c482dbdcbe",
        "display_name": "",
        "description": "",
        "project_uuid": "5ac119ae-1d4a-40b2-8f29-99e824c154a8",
        "created_at": "2019-02-22T03:05:15Z",
        "updated_at": "2019-02-22T03:05:15Z",
        "created_by": "jtcheng",
        "devops": {
            "namespace": {
                "name": "devops-a9-a9-biz"
            }
        },
        "namespaces": [
            {
                "cluster_uuid": "5f32006b-50f9-4805-a7b6-7cb675996ec6",
                "cluster_name": "ace",
                "created_at": "2019-02-22T03:05:04Z",
                "name": "a9-biz",
                "uuid": "a6e3c9f1-364e-11e9-9b67-5254000cec49"
            }
        ],
        "name": "a9-biz"
    }
]`

var spacePagedResponseJson = `{
    "count": 1,
    "num_pages": 1,
    "results": [
        {
            "status": "running",
            "resource_actions": [
                "space:bind_namespace",
                "space:consume",
                "space:create",
                "space:delete",
                "space:unbind_namespace",
                "space:update",
                "space:view"
            ],
            "uuid": "1c96776f-db9d-4b80-a296-d6c482dbdcbe",
            "display_name": "",
            "description": "",
            "project_uuid": "5ac119ae-1d4a-40b2-8f29-99e824c154a8",
            "created_at": "2019-02-22T03:05:15Z",
            "updated_at": "2019-02-22T03:05:15Z",
            "created_by": "jtcheng",
            "devops": {
                "namespace": {
                    "name": "devops-a9-a9-biz"
                }
            },
            "namespaces": [
                {
                    "cluster_uuid": "5f32006b-50f9-4805-a7b6-7cb675996ec6",
                    "cluster_name": "ace",
                    "created_at": "2019-02-22T03:05:04Z",
                    "name": "a9-biz",
                    "uuid": "a6e3c9f1-364e-11e9-9b67-5254000cec49"
                }
            ],
            "name": "a9-biz"
        }
    ],
    "page_size": 10,
    "next": null,
    "previous": null
}`
