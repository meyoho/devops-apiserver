package ace

import (
	"context"
	"net/http"

	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"github.com/moul/http2curl"
	glog "k8s.io/klog"
)

// AlaudaCloudClient http client for Alauda Container Enterprise
type AlaudaCloudClient struct {
	*http.Client
	opts Options
}

var _ Interface = &AlaudaCloudClient{}

// DoRequest does request and marshals response using a renderer
func (ace *AlaudaCloudClient) DoRequest(request *http.Request, data interface{}) (err error) {
	var response *http.Response
	if response, err = ace.Do(request); err != nil {
		return
	}

	curl, _ := http2curl.GetCurlCommand(request)
	if curl != nil {
		glog.V(5).Infof("request: %s\tresponse: %s", curl.String(), response.Status)
	}

	renderer := ace.opts.Renderers[0]
	for _, r := range ace.opts.Renderers {
		if r.IsContentType(request) {
			renderer = r
			break
		}
	}
	for _, errorHandler := range ace.opts.ErrorHandlers {
		if !errorHandler.IsError(response) {
			continue
		}
		if err = errorHandler.HandleError(response, renderer); err != nil {
			return
		}
	}
	err = renderer.Render(response, data)
	return
}

// NewReq creates a new request
func (ace *AlaudaCloudClient) NewReq() RequestBuilder {
	return NewRequest()
}

// Iterate iterates a list API with the default list iterator
func (ace *AlaudaCloudClient) Iterate(listFunc thirdparty.ListFunc) thirdparty.ListFunc {
	return ace.CustomIterate(listFunc, ace.opts.ListIterator)
}

// CustomIterate iterates a list with a provided list iterator
func (ace *AlaudaCloudClient) CustomIterate(listFunc thirdparty.ListFunc, iterator thirdparty.ListIterator) thirdparty.ListFunc {
	return func(ctx context.Context, opts ...thirdparty.ListOptions) (thirdparty.List, error) {
		return iterator.Iterate(listFunc, ctx, opts...)
	}
}

// MultipleIteration iterates through a list to fetch another list using default MultipleIterator
func (ace *AlaudaCloudClient) MultipleIteration(listFunc thirdparty.ListFunc, ctxGen ContextGenerator, list thirdparty.List) (map[thirdparty.ListItem]thirdparty.List, []error) {
	return ace.CustomMultipleIterate(listFunc, ctxGen, list, ace.opts.MultipleIterator)
}

// CustomMultipleIterate uses a custom MultiIterator to fetch multiple iterations
func (ace *AlaudaCloudClient) CustomMultipleIterate(listFunc thirdparty.ListFunc, ctxGen ContextGenerator, list thirdparty.List, multiIterator MultipleIterator) (map[thirdparty.ListItem]thirdparty.List, []error) {
	return multiIterator.IterateList(listFunc, ctxGen, list)
}

func (ace *AlaudaCloudClient) GetOptions() Options {
	return ace.opts
}

// New constructor for ACE Client
func New(opts Options) *AlaudaCloudClient {
	opts = opts.init()
	client := *http.DefaultClient
	client.Transport = opts.Transport
	client.Timeout = opts.Timeout
	return &AlaudaCloudClient{
		Client: &client,
		opts:   opts,
	}
}
