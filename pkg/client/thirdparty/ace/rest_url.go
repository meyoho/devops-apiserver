package ace

import (
	"net/url"
	"strings"
)

// URL generates a url
func (ace *AlaudaCloudClient) URL(urls ...string) URIBuilder {
	if urls == nil {
		urls = []string{}
	}
	urls = append(urls, "")
	urls = append([]string{ace.opts.APIAddress}, urls...)
	return URIBuilder{url: strings.Join(urls, "/")}
}

// URIBuilder builds urls
type URIBuilder struct {
	url    string
	params map[string][]string
}

// Params adds parameters
func (uri URIBuilder) Params(params map[string][]string) URIBuilder {
	uri.params = params
	return uri
}

// Done renders the url
func (uri URIBuilder) Done() string {
	if len(uri.params) > 0 {
		uri.url += "?" + url.Values(uri.params).Encode()
	}
	return uri.url
}
