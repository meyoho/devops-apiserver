package context

import "context"

var nameContextKey = struct{}{}

// WithName adds a name to a context
func WithName(ctx context.Context, name string) context.Context {
	return context.WithValue(ctx, nameContextKey, name)
}

// Name gets a name from a context
func Name(ctx context.Context) string {
	return getStringValue(ctx, nameContextKey)
}

func getStringValue(ctx context.Context, key interface{}) (str string) {
	value := ctx.Value(key)
	if value != nil {
		str = value.(string)
	}
	return
}
