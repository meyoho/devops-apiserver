package ace_test

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("AlaudaContainerEnterprise.URL", func() {
	var (
		client *ace.AlaudaCloudClient
		url    string
		path   []string
		params map[string][]string
	)

	BeforeEach(func() {
		client = ace.New(ace.Options{
			RootAccount: "alauda",
			Token:       "token",
			APIAddress:  "http://address",
		})
		path = []string{"v1", "roles"}
	})

	JustBeforeEach(func() {
		url = client.URL(path...).Params(params).Done()
	})

	It("should add / between parameters", func() {
		Expect(url).To(Equal("http://address/v1/roles/"))
	})

	Context("Path is nil", func() {
		BeforeEach(func() {
			path = nil
		})
		It("should work when path is nil", func() {
			Expect(url).To(Equal("http://address/"))
		})
	})

	Context("Params is not nil", func() {
		BeforeEach(func() {
			params = map[string][]string{
				"project_name": []string{"devops"},
			}
		})
		It("should work when params contains values", func() {
			Expect(url).To(Equal("http://address/v1/roles/?project_name=devops"))
		})
	})

	Context("Params has multi vlaues", func() {
		BeforeEach(func() {
			params = map[string][]string{
				"project_name": []string{"devops"},
				"space_name":   []string{"dev"},
			}
		})
		It("should work when params contains values", func() {
			Expect(url).To(Equal("http://address/v1/roles/?project_name=devops&space_name=dev"))
		})
	})

})
