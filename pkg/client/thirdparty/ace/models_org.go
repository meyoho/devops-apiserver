package ace

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"time"
)

// Account a sub account record
type Account struct {
	Username      string    `json:"username"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
	Type          string    `json:"type"`
	Realname      string    `json:"realname"`
	Department    string    `json:"department"`
	Position      string    `json:"position"`
	Mobile        string    `json:"mobile"`
	Email         string    `json:"email"`
	OfficeAddress string    `json:"office_address"`
	LandlinePhone string    `json:"landline_phone"`
}

var _ thirdparty.ListItem = &Account{}

// GetName get a name of a username
func (a *Account) GetName() string {
	return a.Username
}

// AccountList a list of sub accounts
type AccountList struct {
	StandardList
	Items []*Account `json:"results"`
}

var _ thirdparty.List = &AccountList{}

// GetItems return items from the list
func (al *AccountList) GetItems() (items []thirdparty.ListItem) {
	items = make([]thirdparty.ListItem, len(al.Items))
	if len(al.Items) > 0 {
		for i, r := range al.Items {
			items[i] = r
		}
	}
	return
}

// AddItems add items from another list
func (al *AccountList) AddItems(list thirdparty.List) {
	items := list.GetItems()
	if len(items) > 0 {
		for _, i := range items {
			if role, ok := i.(*Account); ok {
				al.Items = append(al.Items, role)
			}
		}
	}
}
