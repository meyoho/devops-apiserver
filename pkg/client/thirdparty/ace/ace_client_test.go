package ace_test

import (
	"context"
	"log"

	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

// WARNNING: This is to use with a real environment
// SHOULD NOT MAKE THIS TESTS GENERALLY AVAILABLE
var _ = PDescribe("INT.test", func() {
	var (
		client = ace.New(ace.Options{
			APIAddress:  "http://118.24.224.101:32001",
			RootAccount: "alauda",
			Token:       "default",
		})
		opts = []ace.RequestOptions{}
		list *ace.RoleList
		err  error
	)

	BeforeEach(func() {
		opts = []ace.RequestOptions{
			ace.ProjectOpts("devops"),
			ace.PageOpts(1, 5),
		}
	})

	JustBeforeEach(func() {
		list, err = client.GetRoles(context.TODO(), opts...)
	})

	It("should return some data", func() {
		log.Println(err)
		Expect(err).To(BeNil())
		// log.Println(list)
		for _, i := range list.Items {
			log.Println(i.Name)
		}
	})
})

var _ = PDescribe("INT.test.with.iterator", func() {
	var (
		client = ace.New(ace.Options{
			APIAddress:  "http://118.24.224.101:32001",
			RootAccount: "alauda",
			Token:       "default",
		})
		opts = []ace.RequestOptions{}
		list thirdparty.List
		err  error
	)

	BeforeEach(func() {
		opts = []ace.RequestOptions{
			ace.ProjectOpts("devops"),
		}
	})

	JustBeforeEach(func() {
		list, err = client.Iterate(client.GetRolesIterator)(context.TODO(), ace.AsListOptions(opts)...)
		// list, err = client..Iterator(GetRoles(context.TODO(), opts...)
	})

	It("should return some data", func() {
		log.Println(err)
		Expect(err).To(BeNil())
		// log.Println(list)
		rolList := list.(*ace.RoleList)
		for _, i := range rolList.Items {
			log.Println(i.Name)
		}
	})
})

var _ = PDescribe("INT.getusersandroles", func() {
	var (
		client = ace.New(ace.Options{
			APIAddress:  "http://118.24.224.101:32001",
			RootAccount: "alauda",
			Token:       "default",
		})
		opts = []ace.RequestOptions{}
		list []*ace.RoleUserAssignment
		err  error
	)

	BeforeEach(func() {
		opts = []ace.RequestOptions{
			ace.ProjectOpts("devops"),
			ace.PageOpts(1, 20),
		}
	})

	JustBeforeEach(func() {
		list, err = client.GetRolesWithUsers(context.TODO(), opts...)
	})

	It("should return some data", func() {
		log.Println(err)
		Expect(err).To(BeNil())
		for _, r := range list {
			log.Println("user", r.User, "email", r.Email, "role", r.RoleName, "isTemplate", r.IsRoleFromTemplate, "space", r.SpaceName)
		}
	})
})
