package ace

import (
	"context"
	"net/http"
)

const resourceProject = "projects"

type ProjectExtension interface {
	ListProjects(ctx context.Context, opts ...RequestOptions) (result *ProjectList, err error)
}

func (ace *AlaudaCloudClient) ListProjects(ctx context.Context, opts ...RequestOptions) (result *ProjectList, err error) {
	var request *http.Request
	request, err = ace.NewReq().Get().JSON().
		Path(ace.opts.APIAddress, apiVersionV1, resourceProject, ace.opts.RootAccount).
		Params(opts...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()

	if err != nil {
		return
	}

	paged := false
	for _, opt := range opts {
		if _, ok := opt.(PageOptions); ok {
			paged = true
		}
	}

	result = &ProjectList{}
	if paged {
		err = ace.DoRequest(request, result)
		return result, err
	}

	items := &[]*Project{}
	err = ace.DoRequest(request, items)
	if err != nil {
		return nil, err
	}

	result.Items = *items
	result.Count = len(*items)
	result.PageSize = len(*items)
	return result, err
}
