package ace

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"net/url"
	"strconv"
)

// RequestOptions request options
type RequestOptions interface {
	Get() map[string][]string
}

func AsRequestOptions(opts []thirdparty.ListOptions) []RequestOptions {
	res := []RequestOptions{}
	for _, opt := range opts {
		res = append(res, opt)
	}
	return res
}

func AsListOptions(opts []RequestOptions) []thirdparty.ListOptions {
	res := []thirdparty.ListOptions{}
	for _, opt := range opts {
		res = append(res, opt)
	}
	return res
}

// PageOptions page options for requests
type PageOptions struct {
	Page     int
	PageSize int
}

// Get options for pagging
func (po PageOptions) Get() map[string][]string {
	return map[string][]string{
		"page":      []string{strconv.Itoa(po.Page)},
		"page_size": []string{strconv.Itoa(po.PageSize)},
	}
}

// PageOpts constructor for PageOptions
func PageOpts(page, pageSize int) RequestOptions {
	return PageOptions{Page: page, PageSize: pageSize}
}

// ProjectOptions options for projects
type ProjectOptions struct {
	Name string
}

// Get get project request options
func (po ProjectOptions) Get() map[string][]string {
	return map[string][]string{"project_name": []string{po.Name}}
}

// ProjectOpts constructor for ProjectOptions
func ProjectOpts(name string) RequestOptions {
	return ProjectOptions{Name: name}
}

func getParams(opts ...RequestOptions) (result map[string][]string) {
	result = make(map[string][]string)
	if len(opts) > 0 {
		for _, o := range opts {
			for k, v := range o.Get() {
				result[k] = v
			}
		}
	}
	return
}

type UrlValuesOptions struct {
	Values url.Values
}

func NewUrlValuesOptions(values url.Values) UrlValuesOptions {
	return UrlValuesOptions{Values: values}
}

func (values UrlValuesOptions) Get() map[string][]string {
	return map[string][]string(values.Values)
}
