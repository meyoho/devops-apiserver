package ace

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/runtime"
	"net/http"
	"reflect"
	"strings"
)

// RequestBuilder builds requests cascaded
type RequestBuilder struct {
	opts          []RequestOptions
	host          string
	method        string
	path          string
	noSlashEnding bool
	body          interface{}

	headers map[string]string
	context context.Context
}

// NewRequest constructor for request builder
func NewRequest() RequestBuilder {
	return RequestBuilder{}
}

// Get sets as GET request
func (rb RequestBuilder) Get() RequestBuilder {
	return rb.Method(http.MethodGet)
}

func (rb RequestBuilder) Post() RequestBuilder {
	return rb.Method(http.MethodPost)
}

func (rb RequestBuilder) Put() RequestBuilder {
	return rb.Method(http.MethodPut)
}

func (rb RequestBuilder) Delete() RequestBuilder {
	return rb.Method(http.MethodDelete)
}

// Method sets a method like GET, POST, DELETE etc
func (rb RequestBuilder) Method(method string) RequestBuilder {
	rb.method = method
	return rb
}

// Path adds multiple strings and joins them with / to generate a url
func (rb RequestBuilder) Path(path ...string) RequestBuilder {
	rb.path = strings.Join(path, "/")
	return rb
}

// Path adds multiple strings and joins them with / to generate a url
func (rb RequestBuilder) NoSlashEnding() RequestBuilder {
	rb.noSlashEnding = true
	return rb
}

func (rb RequestBuilder) PathIgnoreEmpty(path ...string) RequestBuilder {
	validPath := []string{}
	for _, p := range path {
		if len(p) == 0 {
			continue
		}
		validPath = append(validPath, p)
	}

	return rb.Path(validPath...)
}

// func (rb RequestBuilder) AppendPath(path string) RequestBuilder {
// 	return rb.Path(rb.path, path)
// }

// Params add options as parameters for the request
func (rb RequestBuilder) Params(opts ...RequestOptions) RequestBuilder {
	rb.opts = opts
	return rb
}

func (rb RequestBuilder) initHeaders() RequestBuilder {
	if rb.headers == nil {
		rb.headers = map[string]string{}
	}
	return rb
}

// JSON sets content type as JSON for the request
func (rb RequestBuilder) JSON() RequestBuilder {
	rb = rb.initHeaders()
	rb.headers[HeaderContentType] = ContentTypeJSON
	return rb
}

// TokenAuth adds a token as a Token Authorization header
func (rb RequestBuilder) TokenAuth(token string) RequestBuilder {
	rb = rb.initHeaders()
	rb.headers[HeaderAuthorization] = fmt.Sprintf("Token %s", token)
	return rb
}

// Context sets the context for the requests
func (rb RequestBuilder) Context(ctx context.Context) RequestBuilder {
	rb.context = ctx
	return rb
}

// Body sets the body for the requests
func (rb RequestBuilder) Body(obj interface{}) RequestBuilder {
	rb.body = obj
	return rb
}

// Done processes and generates the request
func (rb RequestBuilder) Done() (req *http.Request, err error) {
	if !rb.noSlashEnding && rb.path[len(rb.path)-1] != '/' {
		rb.path += "/"
	}

	urlbuilder := URIBuilder{url: rb.path}
	if rb.opts != nil {
		urlbuilder = urlbuilder.Params(getParams(rb.opts...))
	}
	body, err := rb.getBody()
	if err != nil {
		return nil, err
	}
	req, err = http.NewRequest(rb.method, urlbuilder.Done(), body)
	if err != nil {
		return
	}
	if len(rb.headers) > 0 {
		for k, v := range rb.headers {
			req.Header.Add(k, v)
		}
	}
	req = req.WithContext(rb.context)
	return
}

func (rb RequestBuilder) getBody() (io.Reader, error) {
	if rb.body == nil {
		return nil, nil
	}

	switch t := rb.body.(type) {
	case string:
		if t == "" {
			return nil, nil
		}
		data, err := ioutil.ReadFile(t)
		if err != nil {
			return nil, err
		}
		return bytes.NewReader(data), nil
	case []byte:
		return bytes.NewReader(t), nil
	case io.Reader:
		return rb.body.(io.Reader), nil
	case runtime.Object:
		// callers may pass typed interface pointers, therefore we must check nil with reflection
		if reflect.ValueOf(t).IsNil() {
			return nil, nil
		}
		data, err := json.Marshal(rb.body) // runtime.Encode(json.Encoder{}, t)
		if err != nil {
			return nil, err
		}
		return bytes.NewReader(data), nil
	default:
		return nil, fmt.Errorf("unknown type used for body: %+v", rb.body)
	}
}
