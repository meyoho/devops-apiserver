package ace_test

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("CleanupRoleName", func() {
	var (
		role     *ace.Role
		expected string
	)

	BeforeEach(func() {
		role = &ace.Role{
			ProjectName: "devops",
			Name:        "devops-project_admin",
		}
	})

	JustBeforeEach(func() {
		expected = ace.CleanupRoleName(role)
	})

	Context("Standard project role", func() {
		It("should return role without project", func() {
			Expect(expected).To(Equal("project_admin"))
		})
	})

	Context("Space name project role", func() {
		BeforeEach(func() {
			role.SpaceName = "devops-dev"
			role.Name = "devops-dev-space_admin"
		})
		It("should return role without space name", func() {
			Expect(expected).To(Equal("space_admin"))
		})
	})

	Context("Namespace role", func() {
		BeforeEach(func() {
			role = &ace.Role{
				ProjectName:   "project_name",
				Namespace:     "alauda",
				SpaceName:     "",
				NamespaceUUID: "7f40c795-f795-11e8-a9cb-a2441bae370c",
				Name:          "devops-ace-devops-features-namespace_admin",
			}
		})
		It("should return role without namespace or project name", func() {
			Expect(expected).To(Equal("namespace_admin"))
		})
	})
})
