package ace_test

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"context"

	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("StandardMultipleIterator.MultipleIterate", func() {
	var (
		stdIterator ace.StandardMultipleIterator
		listFunc    thirdparty.ListFunc
		genCtxFunc  ace.ContextGenerator
		opts        []ace.RequestOptions
		ctx         context.Context
		list        thirdparty.List
		requests    int
		result      map[thirdparty.ListItem]thirdparty.List
		errs        []error
	)

	BeforeEach(func() {
		requests = 0
		ctx = context.TODO()
		listFunc = func(ctx context.Context, opts ...thirdparty.ListOptions) (funclist thirdparty.List, err error) {
			requests++
			listObj := &ace.RoleUserList{
				StandardList: ace.StandardList{
					Count: 10, PageSize: 5, NumberOfPages: 2,
				},
				Items: []*ace.RoleUser{
					&ace.RoleUser{User: "username"},
				},
			}
			return listObj, nil
		}
		genCtxFunc = ace.GetStandardContextGenerator(ctx, opts...)
		list = &ace.RoleList{
			Items: []*ace.Role{
				&ace.Role{
					Name: "role1",
				},
				&ace.Role{
					Name: "role2",
				},
				&ace.Role{
					Name: "role3",
				},
			},
		}
	})

	JustBeforeEach(func() {
		result, errs = stdIterator.IterateList(listFunc, genCtxFunc, list)
	})

	Context("Get three requests", func() {
		It("should get successfully two three roles, with one user in each", func() {
			Expect(errs).ToNot(BeNil(), "no error occurred")
			Expect(errs).To(BeEmpty(), "error list should be empty")
			Expect(result).ToNot(BeNil(), "map is not nil")
			Expect(result).To(HaveLen(3), "have 3 roles as key")
			Expect(requests).To(Equal(3), "only three requests in total, one for each role")
		})
	})
})
