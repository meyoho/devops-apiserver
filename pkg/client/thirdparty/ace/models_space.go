package ace

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"time"
)

// Space for ACE
type Space struct {
	Status      string                  `json:"status"`
	CreatedBy   string                  `json:"created_by"`
	CreatedAt   time.Time               `json:"created_at"`
	Description string                  `json:"description"`
	DisplayName string                  `json:"display_name"`
	Name        string                  `json:"name"`
	DevOps      SpaceRelatedDevops      `json:"devops"`
	Namespaces  []SpaceRelatedNamespace `json:"namespaces"`
	UpdatedAt   time.Time               `json:"updated_at"`
	UUID        string                  `json:"uuid"`
}

type SpaceRelatedDevops struct {
	Namespace devopsNamespace `json:"namespace"`
}

type devopsNamespace struct {
	Name string `json:"name"`
}

type SpaceRelatedNamespace struct {
	ClusterUUID string    `json:"cluster_uuid"`
	ClusterName string    `json:"cluster_name"`
	CreatedAt   time.Time `json:"created_at"`
	Name        string    `json:"name"`
	UUID        string    `json:"uuid"`
}

var _ thirdparty.ListItem = &Space{}

// GetName returns a name of an item
func (r *Space) GetName() string {
	return r.Name
}

// RoleList List of roles
type SpaceList struct {
	StandardList
	Items []*Space `json:"results"`
}

var _ thirdparty.List = &SpaceList{}

// GetItems return items of the list
func (rl *SpaceList) GetItems() (items []thirdparty.ListItem) {
	items = make([]thirdparty.ListItem, len(rl.Items))
	if len(rl.Items) > 0 {
		for i, r := range rl.Items {
			items[i] = r
		}
	}
	return
}

// AddItems add items from another list
func (rl *SpaceList) AddItems(list thirdparty.List) {
	items := list.GetItems()
	if len(items) > 0 {
		for _, i := range items {
			if space, ok := i.(*Space); ok {
				rl.Items = append(rl.Items, space)
			}
		}
	}
}
