package ace

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"time"
)

// Role role for ACE
type Role struct {
	UUID          string    `json:"uuid"`
	Name          string    `json:"name"`
	Namespace     string    `json:"namespace"`
	NamespaceUUID string    `json:"namespace_uuid"`
	AdminRole     bool      `json:"admin_role"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
	ProjectName   string    `json:"project_name"`
	ProjectUUID   string    `json:"project_uuid"`
	SpaceName     string    `json:"space_name"`
	SpaceUUID     string    `json:"space_uuid"`
	Type          string    `json:"type"`
	ID            int       `json:"id"`
}

var _ thirdparty.ListItem = &Role{}

// GetName returns a name of an item
func (r *Role) GetName() string {
	return r.Name
}

// RoleList List of roles
type RoleList struct {
	StandardList
	Items []*Role `json:"results"`
}

var _ thirdparty.List = &RoleList{}

// GetItems return items of the list
func (rl *RoleList) GetItems() (items []thirdparty.ListItem) {
	items = make([]thirdparty.ListItem, len(rl.Items))
	if len(rl.Items) > 0 {
		for i, r := range rl.Items {
			items[i] = r
		}
	}
	return
}

// AddItems add items from another list
func (rl *RoleList) AddItems(list thirdparty.List) {
	items := list.GetItems()
	if len(items) > 0 {
		for _, i := range items {
			if role, ok := i.(*Role); ok {
				rl.Items = append(rl.Items, role)
			}
		}
	}
}

type RoleUsers struct {
	Role     `json:",inline"`
	UserList []RoleUser `json:"user_data"`
}

var _ thirdparty.ListItem = &RoleUsers{}

// GetName returns a name of an role name
func (r *RoleUsers) GetName() string {
	return r.Name
}

// RoleUsersList List of roleUses
type RoleUsersList struct {
	StandardList
	Items []*RoleUsers `json:"results"`
}

var _ thirdparty.List = &RoleUsersList{}

// GetItems return items of the list
func (rl *RoleUsersList) GetItems() (items []thirdparty.ListItem) {
	items = make([]thirdparty.ListItem, len(rl.Items))
	if len(rl.Items) > 0 {
		for i, r := range rl.Items {
			items[i] = r
		}
	}
	return
}

// AddItems add items from another list
func (rl *RoleUsersList) AddItems(list thirdparty.List) {
	items := list.GetItems()
	if len(items) > 0 {
		for _, i := range items {
			if role, ok := i.(*RoleUsers); ok {
				rl.Items = append(rl.Items, role)
			}
		}
	}
}
