package ace_test

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	"encoding/json"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"k8s.io/api/core/v1"
)

var _ = Describe("models_cluster", func() {
	Describe("[models_cluster] KubernetesObject.Into", func() {

		It("Should not return error", func() {
			kuberRes := &ace.KubernetesResource{}
			err := json.Unmarshal([]byte(nodeJsonResp), kuberRes)
			Expect(err).To(BeNil())

			node := &v1.Node{}
			err = kuberRes.Kubernetes.Into(node)
			Expect(err).To(BeNil())
			Expect(node.Name).To(BeEquivalentTo("devops-int-master1"))
		})
	})

	Describe("[models_cluster] KubernetesResourceList.Into", func() {

		Context("When data is not list", func() {
			It("Should not return error", func() {
				kuberResList := &ace.KubernetesResourceList{}
				err := json.Unmarshal([]byte(nodeList), kuberResList)
				Expect(err).To(BeNil())

				nodes := &v1.NodeList{}
				err = kuberResList.Into(nodes)
				Expect(err).To(BeNil())
				Expect(len(nodes.Items)).To(Equal(2))
				for _, node := range nodes.Items {
					Expect(node.Name).To(HavePrefix("devops-int-master"))
				}
			})
		})

	})
})

var nodeList = `{
"count": 4,
"num_pages": 2,
"results":[
    {
        "resource_actions": [
            "k8s_others:create",
            "k8s_others:delete",
            "k8s_others:update",
            "k8s_others:view"
        ],
        "kubernetes": {
            "status": {
                "capacity": {
                    "ephemeral-storage": "51474044Ki",
                    "hugepages-1Gi": "0",
                    "hugepages-2Mi": "0",
                    "memory": "16299576Ki",
                    "pods": "110",
                    "cpu": "8"
                },
                "addresses": [
                    {
                        "type": "InternalIP",
                        "address": "10.0.128.109"
                    },
                    {
                        "type": "Hostname",
                        "address": "devops-int-master1"
                    }
                ],
                "nodeInfo": {
                    "kernelVersion": "3.10.0-957.1.3.el7.x86_64",
                    "kubeletVersion": "v1.11.2",
                    "containerRuntimeVersion": "docker://1.12.6",
                    "machineID": "735fbcd856b697dbc8d6deb1a11dd712",
                    "kubeProxyVersion": "v1.11.2",
                    "bootID": "1d8970bf-6686-475b-af75-abd023ce29bf",
                    "osImage": "CentOS Linux 7 (Core)",
                    "architecture": "amd64",
                    "systemUUID": "A97AE2A0-E58A-409F-81B2-3626FC6D4349",
                    "operatingSystem": "linux"
                },
                "allocatable": {
                    "ephemeral-storage": "47438478872",
                    "hugepages-1Gi": "0",
                    "hugepages-2Mi": "0",
                    "memory": "16197176Ki",
                    "pods": "110",
                    "cpu": "8"
                },
                "daemonEndpoints": {
                    "kubeletEndpoint": {
                        "Port": 10250
                    }
                },
                "images": [
                ],
                "conditions": [
                ]
            },
            "kind": "Node",
            "spec": {
                "podCIDR": "10.199.0.0/24"
            },
            "apiVersion": "v1",
            "metadata": {
                "name": "devops-int-master1",
                "labels": {
                    "redis": "true",
                    "log": "true",
                    "beta.kubernetes.io/os": "linux",
                    "node-role.kubernetes.io/master": "",
                    "ip": "10.0.128.109",
                    "global": "true",
                    "kubernetes.io/hostname": "devops-int-master1",
                    "master": "true",
                    "test": "false",
                    "beta.kubernetes.io/arch": "amd64"
                },
                "resourceVersion": "26213986",
                "creationTimestamp": "2018-11-22T09:18:56Z",
                "annotations": {
                    "node.alpha.kubernetes.io/ttl": "0",
                    "flannel.alpha.coreos.com/public-ip": "10.0.128.109",
                    "flannel.alpha.coreos.com/backend-data": "{\"VtepMAC\":\"0a:75:9b:0b:36:65\"}",
                    "kubeadm.alpha.kubernetes.io/cri-socket": "/var/run/dockershim.sock",
                    "flannel.alpha.coreos.com/backend-type": "vxlan",
                    "volumes.kubernetes.io/controller-managed-attach-detach": "true",
                    "flannel.alpha.coreos.com/kube-subnet-manager": "true"
                },
                "selfLink": "/api/v1/nodes/devops-int-master1",
                "uid": "a327e18f-ee37-11e8-9985-52540052ef9b"
            }
        }
    },
    {
        "resource_actions": [
            "k8s_others:delete",
            "k8s_others:update",
            "k8s_others:view",
            "k8s_others:create"
        ],
        "kubernetes": {
            "status": {
                "capacity": {
                    "ephemeral-storage": "51474044Ki",
                    "hugepages-1Gi": "0",
                    "hugepages-2Mi": "0",
                    "memory": "16300164Ki",
                    "pods": "110",
                    "cpu": "8"
                },
                "addresses": [
                    {
                        "type": "InternalIP",
                        "address": "10.0.129.126"
                    },
                    {
                        "type": "Hostname",
                        "address": "devops-int-master2"
                    }
                ],
                "nodeInfo": {
                    "kernelVersion": "3.10.0-862.11.6.el7.x86_64",
                    "kubeletVersion": "v1.11.2",
                    "containerRuntimeVersion": "docker://1.12.6",
                    "machineID": "735fbcd856b697dbc8d6deb1a11dd712",
                    "kubeProxyVersion": "v1.11.2",
                    "bootID": "21ac5715-266b-4138-b994-34e4e502712c",
                    "osImage": "CentOS Linux 7 (Core)",
                    "architecture": "amd64",
                    "systemUUID": "84A43966-E646-47AC-98C4-C0E3C8C17DB1",
                    "operatingSystem": "linux"
                },
                "allocatable": {
                    "ephemeral-storage": "47438478872",
                    "hugepages-1Gi": "0",
                    "hugepages-2Mi": "0",
                    "memory": "16197764Ki",
                    "pods": "110",
                    "cpu": "8"
                },
                "daemonEndpoints": {
                    "kubeletEndpoint": {
                        "Port": 10250
                    }
                },
                "images": [
                ],
                "conditions": [
                ]
            },
            "kind": "Node",
            "spec": {
                "podCIDR": "10.199.1.0/24"
            },
            "apiVersion": "v1",
            "metadata": {
                "name": "devops-int-master2",
                "labels": {
                    "redis": "true",
                    "log": "true",
                    "beta.kubernetes.io/os": "linux",
                    "node-role.kubernetes.io/master": "",
                    "ip": "10.0.129.126",
                    "global": "true",
                    "kubernetes.io/hostname": "devops-int-master2",
                    "master": "true",
                    "test": "false",
                    "beta.kubernetes.io/arch": "amd64"
                },
                "resourceVersion": "26213984",
                "creationTimestamp": "2018-11-22T09:19:22Z",
                "annotations": {
                    "node.alpha.kubernetes.io/ttl": "0",
                    "flannel.alpha.coreos.com/public-ip": "10.0.129.126",
                    "flannel.alpha.coreos.com/backend-data": "{\"VtepMAC\":\"1e:cf:42:d1:b1:7e\"}",
                    "kubeadm.alpha.kubernetes.io/cri-socket": "/var/run/dockershim.sock",
                    "flannel.alpha.coreos.com/backend-type": "vxlan",
                    "volumes.kubernetes.io/controller-managed-attach-detach": "true",
                    "flannel.alpha.coreos.com/kube-subnet-manager": "true"
                },
                "selfLink": "/api/v1/nodes/devops-int-master2",
                "uid": "b30b6ec4-ee37-11e8-9985-52540052ef9b"
            }
        }
    }
]}`

var nodeJsonResp = `{
  "resource_actions": [
      "k8s_others:create",
      "k8s_others:delete",
      "k8s_others:update",
      "k8s_others:view"
  ],
  "kubernetes": {
      "status": {
          "capacity": {
              "ephemeral-storage": "51474044Ki",
              "hugepages-1Gi": "0",
              "hugepages-2Mi": "0",
              "memory": "16299576Ki",
              "pods": "110",
              "cpu": "8"
          },
          "addresses": [
              {
                  "type": "InternalIP",
                  "address": "10.0.128.109"
              },
              {
                  "type": "Hostname",
                  "address": "devops-int-master1"
              }
          ],
          "nodeInfo": {
              "kernelVersion": "3.10.0-957.1.3.el7.x86_64",
              "kubeletVersion": "v1.11.2",
              "containerRuntimeVersion": "docker://1.12.6",
              "machineID": "735fbcd856b697dbc8d6deb1a11dd712",
              "kubeProxyVersion": "v1.11.2",
              "bootID": "1d8970bf-6686-475b-af75-abd023ce29bf",
              "osImage": "CentOS Linux 7 (Core)",
              "architecture": "amd64",
              "systemUUID": "A97AE2A0-E58A-409F-81B2-3626FC6D4349",
              "operatingSystem": "linux"
          },
          "allocatable": {
              "ephemeral-storage": "47438478872",
              "hugepages-1Gi": "0",
              "hugepages-2Mi": "0",
              "memory": "16197176Ki",
              "pods": "110",
              "cpu": "8"
          },
          "daemonEndpoints": {
              "kubeletEndpoint": {
                  "Port": 10250
              }
          }
      },
      "kind": "Node",
      "spec": {
          "podCIDR": "10.199.0.0/24"
      },
      "apiVersion": "v1",
      "metadata": {
          "name": "devops-int-master1",
          "labels": {
              "redis": "true",
              "log": "true",
              "beta.kubernetes.io/os": "linux",
              "node-role.kubernetes.io/master": "",
              "ip": "10.0.128.109",
              "global": "true",
              "kubernetes.io/hostname": "devops-int-master1",
              "master": "true",
              "test": "false",
              "beta.kubernetes.io/arch": "amd64"
          },
          "resourceVersion": "26201102",
          "creationTimestamp": "2018-11-22T09:18:56Z",
          "annotations": {
              "node.alpha.kubernetes.io/ttl": "0",
              "flannel.alpha.coreos.com/public-ip": "10.0.128.109",
              "flannel.alpha.coreos.com/backend-data": "{\"VtepMAC\":\"0a:75:9b:0b:36:65\"}",
              "kubeadm.alpha.kubernetes.io/cri-socket": "/var/run/dockershim.sock",
              "flannel.alpha.coreos.com/backend-type": "vxlan",
              "volumes.kubernetes.io/controller-managed-attach-detach": "true",
              "flannel.alpha.coreos.com/kube-subnet-manager": "true"
          },
          "selfLink": "/api/v1/nodes/devops-int-master1",
          "uid": "a327e18f-ee37-11e8-9985-52540052ef9b"
      }
  }
}`
