package ace

import (
	"context"
	"net/http"
)

const apiVersionV2 = "v2"
const resourceRegions = "regions"
const resourceKubernetesCluster = "kubernetes/clusters"

var (
	ErrorCodeKubernetes = "kubernetes_error"
	NotFoundKey         = "not found"
)

// ClusterExtension /kubernetes/clusters/{cluster_id_name}/{resource_type}
type ClusterExtension interface {
	ListK8SResources(ctx context.Context, clusterID string, namespace string, resourceType string, options ...RequestOptions) (result *KubernetesResourceList, err error)
	GetK8SResource(ctx context.Context, clusterID string, namespace string, resourceType string, name string, options ...RequestOptions) (result *KubernetesResource, err error)
	UpdateK8SResource(ctx context.Context, clusterID string, namespace string, resourceType string, name string, object interface{}, options ...RequestOptions) (err error)
	DeleteK8SResource(ctx context.Context, clusterID string, namespace string, resourceType string, name string, deleteOptions interface{}, options ...RequestOptions) error
	// CreateK8SResource will create resource in k8s, if you want create namespaced scoped resource, just set namespace in object
	CreateK8SResource(ctx context.Context, clusterID string, resourceType string, object interface{}, options ...RequestOptions) (result *KubernetesResource, err error)
}

func (ace *AlaudaCloudClient) ListK8SResources(ctx context.Context, clusterNameOrID string, namespace string, resourceType string, options ...RequestOptions) (result *KubernetesResourceList, err error) {
	request, err := ace.NewReq().
		Get().
		JSON().
		PathIgnoreEmpty(
			ace.opts.APIAddress,
			apiVersionV2,
			resourceKubernetesCluster,
			clusterNameOrID,
			resourceType,
			namespace,
		).
		Params(options...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return nil, err
	}

	paged := false
	for _, opt := range options {
		if _, ok := opt.(PageOptions); ok {
			paged = true
		}
	}

	result = &KubernetesResourceList{}
	if paged {
		err = ace.DoRequest(request, result)
		return result, err
	}

	items := &[]*KubernetesResource{}
	err = ace.DoRequest(request, items)
	if err != nil {
		return result, nil
	}
	result.Items = *items
	result.Count = len(*items)
	result.PageSize = len(*items)
	result.NumberOfPages = len(*items)
	return result, err
}

func (ace *AlaudaCloudClient) GetK8SResource(ctx context.Context, clusterNameOrID string, namespace string, resourceType string, name string, options ...RequestOptions) (result *KubernetesResource, err error) {
	request, err := ace.NewReq().
		Get().
		JSON().
		PathIgnoreEmpty(
			ace.opts.APIAddress,
			apiVersionV2,
			resourceKubernetesCluster,
			clusterNameOrID,
			resourceType,
			namespace,
			name,
		).
		NoSlashEnding(). // /kubernetes/clusters/{cluster_id_name}/{resource_type}/{name} is different with /kubernetes/clusters/{cluster_id_name}/{resource_type}/{namespace}/
		Params(options...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return nil, err
	}
	result = &KubernetesResource{}
	err = ace.DoRequest(request, result)
	return result, err
}

func (ace *AlaudaCloudClient) CreateK8SResource(ctx context.Context, clusterNameOrID string, resourceType string, object interface{}, options ...RequestOptions) (result *KubernetesResource, err error) {
	request, err := ace.NewReq().
		Post().
		JSON().
		PathIgnoreEmpty(
			ace.opts.APIAddress,
			apiVersionV2,
			resourceKubernetesCluster,
			clusterNameOrID,
			resourceType,
		).
		Body(object).
		Params(options...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return nil, err
	}
	k8sRes := &KubernetesResource{}
	err = ace.DoRequest(request, k8sRes)
	if err != nil {
		return nil, err
	}

	return k8sRes, nil
}

func (ace *AlaudaCloudClient) DeleteK8SResource(ctx context.Context, clusterNameOrID string, namespace string, resourceType string, name string, deleteOptions interface{}, options ...RequestOptions) error {
	request, err := ace.NewReq().
		Delete().
		JSON().
		PathIgnoreEmpty(
			ace.opts.APIAddress,
			apiVersionV2,
			resourceKubernetesCluster,
			clusterNameOrID,
			resourceType,
			namespace,
			name,
		).
		NoSlashEnding().
		Body(deleteOptions).
		Params(options...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return err
	}
	err = ace.DoRequest(request, nil)
	return err
}

func (ace *AlaudaCloudClient) UpdateK8SResource(ctx context.Context, clusterNameOrID string, namespace string, resourceType string, name string, object interface{}, options ...RequestOptions) (err error) {
	request, err := ace.NewReq().
		Put().
		JSON().
		PathIgnoreEmpty(
			ace.opts.APIAddress,
			apiVersionV2,
			resourceKubernetesCluster,
			clusterNameOrID,
			resourceType,
			namespace,
			name,
		).
		NoSlashEnding().
		Body(object).
		Params(options...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return err
	}
	err = ace.DoRequest(request, nil)
	return err
}

func IsK8sResourceNotFound(statusCode int, err Error) bool {
	// update to now, we just judge status code, but some days , we really need more certain error code in err
	_ = err
	return statusCode == http.StatusNotFound
}
