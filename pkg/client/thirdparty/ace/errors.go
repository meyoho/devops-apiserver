package ace

import (
	"bytes"
	"fmt"
	"net/http"
)

// Error specific error instance
type Error struct {
	Source  interface{} `json:"source"` // source in jakiro response is int or string
	Message string      `json:"message"`
	Code    string      `json:"code"`
}

func (e Error) Error() string {
	return fmt.Sprintf("[%d] %s: \"%s\"", e.Source, e.Code, e.Message)
}

// ErrorList ace standard error list
type ErrorList struct {
	StatusCode int     `json:"statusCode"`
	Errors     []Error `json:"errors"`
}

func (e ErrorList) Error() (err string) {
	var errBuff bytes.Buffer
	if e.StatusCode > 0 {
		errBuff.WriteString(fmt.Sprintf("StatusCode: %d. ", e.StatusCode))
	}
	if len(e.Errors) > 0 {
		errBuff.WriteString("Errors: [")
		for i, err := range e.Errors {
			errBuff.WriteString(fmt.Sprintf("{%d: \"%s\"},", i, err.Error()))
		}
		errBuff.Truncate(errBuff.Len() - 1)
		errBuff.WriteString("]")
	}
	return errBuff.String()
}

// IsErrorList returns true if it is a pointer to ErrorList
func IsErrorList(err error) bool {
	_, ok := err.(*ErrorList)
	return ok
}

// ErrorHandler handle errors in a response
type ErrorHandler interface {
	IsError(response *http.Response) bool
	HandleError(response *http.Response, renderer Renderer) error
}

// DefaultErrorHandler default error handler for ACE client
var DefaultErrorHandler ErrorHandler = StandardErrorHandler{}

// StandardErrorHandler standard handler uses status codes to return a ACE standard error format
type StandardErrorHandler struct{}

// HandleError handles the response using standard jakiro error response
func (StandardErrorHandler) HandleError(response *http.Response, renderer Renderer) (err error) {
	err = &ErrorList{StatusCode: response.StatusCode}
	renderErr := renderer.Render(response, err)
	if renderErr != nil {
		err = renderErr
	}

	return
}

// IsError returns true if the response is an error response
func (StandardErrorHandler) IsError(response *http.Response) bool {
	return response.StatusCode >= http.StatusBadRequest
}

// ClearErrorList will clear all nil results from an error list
func ClearErrorList(errs []error) (result []error) {
	result = make([]error, 0, len(errs))
	for _, e := range errs {
		if e != nil {
			result = append(result, e)
		}
	}
	return
}
