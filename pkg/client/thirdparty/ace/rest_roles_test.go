package ace_test

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	acectx "alauda.io/devops-apiserver/pkg/client/thirdparty/ace/context"
	"alauda.io/devops-apiserver/pkg/mock/mhttp"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("AlaudaContainerEnterprise.GetRoles", func() {
	var (
		ctrl      *gomock.Controller
		transport *mhttp.MockRoundTripper
		client    *ace.AlaudaCloudClient
		ctx       context.Context
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		transport = mhttp.NewMockRoundTripper(ctrl)
		client = ace.New(ace.Options{
			RootAccount: "alauda",
			Token:       "token",
			APIAddress:  "http://address",
			Transport:   transport,
		})
		ctx = context.TODO()
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	It("should send a request with parameters and return a list with one item", func() {
		// adding request and response expectations
		request, _ := http.NewRequest(http.MethodGet, "http://address/v2/roles/alauda/?project_name=projecta&page=1&page_size=20", nil)
		request.Header.Add("Authorization", "Token token")
		request.Header.Add("Content-type", "application/json")
		projectParam := ace.ProjectOpts("projecta")
		pageParam := ace.PageOpts(1, 20)

		transport.EXPECT().
			RoundTrip(mhttp.NewRequestMatcher(request)).
			Return(getResponseWithBody(http.StatusOK, request, `{
					"count": 2,"page_size": 20,"num_pages": 1,
					"results": [{
						"uuid": "1234-12354-123",
						"name": "administrator",
						"namespace": "org",
						"admin_role": true,
						"created_at":"2016-01-01T10:10:10Z",
						"updated_at":"2016-01-01T10:10:10Z",
						"project_name": "project_a",
						"project_uuid": "2123123-123123-123123"}]
				}`), nil)

		list, err := client.GetRoles(ctx, pageParam, projectParam)
		Expect(err).To(BeNil())
		Expect(list).ToNot(BeNil())
		Expect(list.Count).To(Equal(2))
		Expect(list.PageSize).To(Equal(20))
		Expect(list.NumberOfPages).To(Equal(1))
		Expect(list.Items).To(HaveLen(1))
		createdTime, _ := time.Parse(time.RFC3339, "2016-01-01T10:10:10Z")
		Expect(list.Items).To(ContainElement(&ace.Role{
			UUID:        "1234-12354-123",
			Name:        "administrator",
			Namespace:   "org",
			AdminRole:   true,
			ProjectName: "project_a",
			ProjectUUID: "2123123-123123-123123",
			CreatedAt:   createdTime,
			UpdatedAt:   createdTime,
		}))
	})
	It("should send a request with parameters and return an error", func() {
		// adding request and response expectations
		request, _ := http.NewRequest(http.MethodGet, "http://address/v2/roles/alauda/?project_name=projecta", nil)
		request.Header.Add("Authorization", "Token token")
		request.Header.Add("Content-type", "application/json")
		projectParam := ace.ProjectOpts("projecta")

		transport.EXPECT().
			RoundTrip(mhttp.NewRequestMatcher(request)).
			Return(getResponseWithBody(http.StatusForbidden, request, `{
				"errors": [
					{
							"source": 123,
							"message": "Permissions partially accepted",
							"code": "permissions_partially_accepted"
					}
				]}`), nil)

		list, err := client.GetRoles(ctx, projectParam)
		Expect(err).ToNot(BeNil(), "returns an error for request")
		Expect(list).ToNot(BeNil(), "returns a empty list")
		Expect(ace.IsErrorList(err)).To(BeTrue(), "is a standard ace error")
		aceErr := err.(*ace.ErrorList)
		Expect(aceErr.StatusCode).To(Equal(http.StatusForbidden), "should have 403 status code")
		Expect(aceErr.Errors).To(HaveLen(1), "have one item")
		Expect(fmt.Sprint(aceErr.Errors[0].Source)).To(BeEquivalentTo("123"))
		Expect(fmt.Sprint(aceErr.Errors[0].Message)).To(BeEquivalentTo("Permissions partially accepted"))
		Expect(fmt.Sprint(aceErr.Errors[0].Code)).To(BeEquivalentTo("permissions_partially_accepted"))
		Expect(aceErr.Error()).ToNot(BeEmpty())
	})
})

var _ = Describe("AlaudaContainerEnterprise.GetUserOfRole", func() {
	var (
		ctrl      *gomock.Controller
		transport *mhttp.MockRoundTripper
		client    *ace.AlaudaCloudClient
		ctx       context.Context
		name      string
	)

	BeforeEach(func() {
		name = "somerole"
		ctrl = gomock.NewController(GinkgoT())
		transport = mhttp.NewMockRoundTripper(ctrl)
		client = ace.New(ace.Options{
			RootAccount: "alauda",
			Token:       "token",
			APIAddress:  "http://address",
			Transport:   transport,
		})
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	It("should send a request with pagging and return a list with one item", func() {
		// adding request and response expectations
		request, _ := http.NewRequest(http.MethodGet, "http://address/v1/roles/alauda/somerole/users/?project_name=projecta&page=1&page_size=20", nil)
		request.Header.Add("Authorization", "Token token")
		request.Header.Add("Content-type", "application/json")
		projectParam := ace.ProjectOpts("projecta")
		pageParam := ace.PageOpts(1, 20)
		ctx = acectx.WithName(context.TODO(), name)

		transport.EXPECT().
			RoundTrip(mhttp.NewRequestMatcher(request)).
			Return(getResponseWithBody(http.StatusOK, request, `{
					"count": 1,
					"num_pages": 1,
					"results": [
						{
							"template_uuid": "593647b5-dd9f-437a-b724-7e3f0614a974",
							"template_display_name": "项目管理员",
							"realname": "",
							"template_name": "project_admin",
							"namespace": "alauda",
							"assigned_at": "2018-11-25T10:18:33Z",
							"user": "daniel",
							"role_uuid": "66754647-795a-4ac8-a0ed-7cf7ef258228",
							"role_name": "devops-project_admin"
						}
					],
					"page_size": 20
				}`), nil)

		list, err := client.GetUsersOfRole(ctx, pageParam, projectParam)
		Expect(err).To(BeNil())
		Expect(list).ToNot(BeNil())
		Expect(list.Count).To(Equal(1))
		Expect(list.PageSize).To(Equal(20))
		Expect(list.NumberOfPages).To(Equal(1))
		Expect(list.Items).To(HaveLen(1))
		assignedAt, _ := time.Parse(time.RFC3339, "2018-11-25T10:18:33Z")
		Expect(list.Items).To(ContainElement(&ace.RoleUser{
			TemplateUUID:        "593647b5-dd9f-437a-b724-7e3f0614a974",
			TemplateDisplayName: "项目管理员",
			TemplateName:        "project_admin",
			Namespace:           "alauda",
			AssignedAt:          assignedAt,
			User:                "daniel",
			RoleUUID:            "66754647-795a-4ac8-a0ed-7cf7ef258228",
			RoleName:            "devops-project_admin",
		}))
	})

})

var _ = Describe("AlaudaContainerEnterprise.GetRolesUsers", func() {
	var (
		ctrl      *gomock.Controller
		transport *mhttp.MockRoundTripper
		client    *ace.AlaudaCloudClient
		ctx       context.Context
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		transport = mhttp.NewMockRoundTripper(ctrl)
		client = ace.New(ace.Options{
			RootAccount: "alauda",
			Token:       "token",
			APIAddress:  "http://address",
			Transport:   transport,
		})
		ctx = context.TODO()
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	It("should send a request with parameters and return a list with one item", func() {
		// adding request and response expectations
		request, _ := http.NewRequest(http.MethodGet, "http://address/v1/role-users/alauda/?page=1&page_size=20&project_name=projecta", nil)
		request.Header.Add("Authorization", "Token token")
		request.Header.Add("Content-type", "application/json")
		projectParam := ace.ProjectOpts("projecta")
		pageParam := ace.PageOpts(1, 20)

		transport.EXPECT().
			RoundTrip(mhttp.NewVerboseRequestMatcher(request).WithQuery()).
			Return(getResponseWithBody(http.StatusOK, request, `{
					"count": 1,"page_size": 20,"num_pages": 1,
					"results": [{
            "resource_actions": [
                "role:assign",
                "role:revoke",
                "role:view",
                "role:create",
                "role:delete",
                "role:update"
            ],
            "space_uuid": "44c86e96-eef1-4d16-b869-d381b92311fd",
            "project_name": "a6",
            "uuid": "e6b5f363-6c9d-415b-bddd-e93704f72d65",
            "project_uuid": "95e8c3fc-1776-4510-97a3-700cf64e882f",
            "namespace": "alauda",
            "created_at": "2019-01-29T07:38:39Z",
            "created_by": "alauda",
            "name": "a6-a6-hhh-space_admin",
            "space_name": "a6-a6-hhh",
            "namespace_uuid": "",
            "type": "ROLE",
            "id": 517,
            "region_id": null,
            "user_data": [
                {
                    "template_uuid": "3c691ea4-a924-44c7-8dd6-522e352f62d9",
                    "template_display_name": "组管理员",
                    "realname": "",
                    "template_name": "space_admin",
                    "namespace": "alauda",
                    "assigned_at": "2019-03-19T12:28:57Z",
                    "user": "alauda",
                    "role_uuid": "e6b5f363-6c9d-415b-bddd-e93704f72d65",
                    "role_name": "a6-a6-hhh-space_admin",
                    "email": "alauda@alauda.io"
                },
                {
                    "template_uuid": "3c691ea4-a924-44c7-8dd6-522e352f62d9",
                    "template_display_name": "组管理员",
                    "realname": "",
                    "template_name": "space_admin",
                    "namespace": "alauda",
                    "assigned_at": "2019-03-19T12:05:12Z",
                    "user": "zpyu",
                    "role_uuid": "e6b5f363-6c9d-415b-bddd-e93704f72d65",
                    "role_name": "a6-a6-hhh-space_admin",
                    "email": ""
                }
            ]
        }]
				}`), nil)

		list, err := client.GetRolesUsers(ctx, pageParam, projectParam)
		Expect(err).To(BeNil())
		Expect(list).ToNot(BeNil())
		Expect(list.Count).To(Equal(1))
		Expect(list.PageSize).To(Equal(20))
		Expect(list.NumberOfPages).To(Equal(1))
		Expect(list.Items).To(HaveLen(1))
		createdTime, _ := time.Parse(time.RFC3339, "2019-01-29T07:38:39Z")

		// assert userlist in one roleusers
		roleUsers := list.Items[0]
		Expect(roleUsers.CreatedAt).To(BeEquivalentTo(createdTime))
		Expect(roleUsers.Name).To(BeEquivalentTo("a6-a6-hhh-space_admin"))
		Expect(roleUsers.ProjectName).To(BeEquivalentTo("a6"))
		Expect(roleUsers.SpaceName).To(BeEquivalentTo("a6-a6-hhh"))
		Expect(roleUsers.Name).To(BeEquivalentTo("a6-a6-hhh-space_admin"))

		// assert userlist in one roleusers
		Expect(len(roleUsers.UserList)).To(BeEquivalentTo(2))
		user1 := roleUsers.UserList[0]
		Expect(user1.User).To(BeEquivalentTo("alauda"))
		Expect(user1.TemplateName).To(BeEquivalentTo("space_admin"))
		Expect(user1.Email).To(BeEquivalentTo("alauda@alauda.io"))
		Expect(user1.RoleName).To(BeEquivalentTo("a6-a6-hhh-space_admin"))
	})
	It("should send a request with parameters and return an error", func() {
		// adding request and response expectations
		request, _ := http.NewRequest(http.MethodGet, "http://address/v1/role-users/alauda/?project_name=projecta", nil)
		request.Header.Add("Authorization", "Token token")
		request.Header.Add("Content-type", "application/json")
		projectParam := ace.ProjectOpts("projecta")

		transport.EXPECT().
			RoundTrip(mhttp.NewRequestMatcher(request)).
			Return(getResponseWithBody(http.StatusForbidden, request, `{
				"errors": [
					{
							"source": 123,
							"message": "Permissions partially accepted",
							"code": "permissions_partially_accepted"
					}
				]}`), nil)

		list, err := client.GetRolesUsers(ctx, projectParam)
		Expect(err).ToNot(BeNil(), "returns an error for request")
		Expect(list).ToNot(BeNil(), "returns a empty list")
		Expect(ace.IsErrorList(err)).To(BeTrue(), "is a standard ace error")
		aceErr := err.(*ace.ErrorList)
		Expect(aceErr.StatusCode).To(Equal(http.StatusForbidden), "should have 403 status code")
		Expect(aceErr.Errors).To(HaveLen(1), "have one item")
		Expect(fmt.Sprint(aceErr.Errors[0].Source)).To(BeEquivalentTo("123"))
		Expect(fmt.Sprint(aceErr.Errors[0].Message)).To(BeEquivalentTo("Permissions partially accepted"))
		Expect(fmt.Sprint(aceErr.Errors[0].Code)).To(BeEquivalentTo("permissions_partially_accepted"))
		Expect(aceErr.Error()).ToNot(BeEmpty())
	})
})

var _ = Describe("AlaudaContainerEnterprise.GetRolesWithUsers", func() {
	var (
		ctrl      *gomock.Controller
		transport *mhttp.MockRoundTripper
		client    *ace.AlaudaCloudClient
		ctx       context.Context
	)

	BeforeEach(func() {
		ctrl = gomock.NewController(GinkgoT())
		transport = mhttp.NewMockRoundTripper(ctrl)
		client = ace.New(ace.Options{
			RootAccount: "alauda",
			Token:       "token",
			APIAddress:  "http://address",
			Transport:   transport,
		})
		ctx = context.TODO()
	})

	AfterEach(func() {
		ctrl.Finish()
	})

	It("should send a request with parameters and return a list with one item", func() {
		// adding request and response expectations
		request, _ := http.NewRequest(http.MethodGet, "http://address/v1/role-users/alauda/?page=1&page_size=20&project_name=projecta", nil)
		request.Header.Add("Authorization", "Token token")
		request.Header.Add("Content-type", "application/json")
		projectParam := ace.ProjectOpts("projecta")
		pageParam := ace.PageOpts(1, 20)

		transport.EXPECT().
			RoundTrip(mhttp.NewVerboseRequestMatcher(request).WithQuery()).
			Return(getResponseWithBody(http.StatusOK, request, `{
					"count": 1,"page_size": 20,"num_pages": 1,
					"results": [{
            "resource_actions": [
                "role:assign",
                "role:revoke",
                "role:view",
                "role:create",
                "role:delete",
                "role:update"
            ],
            "space_uuid": "44c86e96-eef1-4d16-b869-d381b92311fd",
            "project_name": "a6",
            "uuid": "e6b5f363-6c9d-415b-bddd-e93704f72d65",
            "project_uuid": "95e8c3fc-1776-4510-97a3-700cf64e882f",
            "namespace": "alauda",
            "created_at": "2019-01-29T07:38:39Z",
            "created_by": "alauda",
            "name": "a6-a6-hhh-space_admin",
            "space_name": "a6-a6-hhh",
            "namespace_uuid": "",
            "type": "ROLE",
            "id": 517,
            "region_id": null,
            "user_data": [
                {
                    "template_uuid": "3c691ea4-a924-44c7-8dd6-522e352f62d9",
                    "template_display_name": "组管理员",
                    "realname": "",
                    "template_name": "space_admin",
                    "namespace": "alauda",
                    "assigned_at": "2019-03-19T12:28:57Z",
                    "user": "alauda",
                    "role_uuid": "e6b5f363-6c9d-415b-bddd-e93704f72d65",
                    "role_name": "a6-a6-hhh-space_admin",
                    "email": "alauda@alauda.io"
                },
                {
                    "template_uuid": "3c691ea4-a924-44c7-8dd6-522e352f62d9",
                    "template_display_name": "组管理员",
                    "realname": "",
                    "template_name": "space_admin",
                    "namespace": "alauda",
                    "assigned_at": "2019-03-19T12:05:12Z",
                    "user": "zpyu",
                    "role_uuid": "e6b5f363-6c9d-415b-bddd-e93704f72d65",
                    "role_name": "a6-a6-hhh-space_admin",
                    "email": ""
                }
            ]
        }]
				}`), nil)

		list, err := client.GetRolesUsers(ctx, pageParam, projectParam)
		Expect(err).To(BeNil())
		Expect(list).ToNot(BeNil())
		Expect(list.Count).To(Equal(1))
		Expect(list.PageSize).To(Equal(20))
		Expect(list.NumberOfPages).To(Equal(1))
		Expect(list.Items).To(HaveLen(1))
		createdTime, _ := time.Parse(time.RFC3339, "2019-01-29T07:38:39Z")

		// assert userlist in one roleusers
		roleUsers := list.Items[0]
		Expect(roleUsers.CreatedAt).To(BeEquivalentTo(createdTime))
		Expect(roleUsers.Name).To(BeEquivalentTo("a6-a6-hhh-space_admin"))
		Expect(roleUsers.ProjectName).To(BeEquivalentTo("a6"))
		Expect(roleUsers.SpaceName).To(BeEquivalentTo("a6-a6-hhh"))
		Expect(roleUsers.Name).To(BeEquivalentTo("a6-a6-hhh-space_admin"))

		// assert userlist in one roleusers
		Expect(len(roleUsers.UserList)).To(BeEquivalentTo(2))
		user1 := roleUsers.UserList[0]
		Expect(user1.User).To(BeEquivalentTo("alauda"))
		Expect(user1.TemplateName).To(BeEquivalentTo("space_admin"))
		Expect(user1.Email).To(BeEquivalentTo("alauda@alauda.io"))
		Expect(user1.RoleName).To(BeEquivalentTo("a6-a6-hhh-space_admin"))
	})

	It("should remove duplicate role of namespace_admin ", func() {
		// adding request and response expectations
		request, _ := http.NewRequest(http.MethodGet, "http://address/v1/role-users/alauda/?page=1&page_size=20&project_name=projecta", nil)
		request.Header.Add("Authorization", "Token token")
		request.Header.Add("Content-type", "application/json")
		projectParam := ace.ProjectOpts("projecta")
		pageParam := ace.PageOpts(1, 20)

		transport.EXPECT().
			RoundTrip(mhttp.NewVerboseRequestMatcher(request).WithQuery()).
			Return(getResponseWithBody(http.StatusOK, request, `{
    "count": 7,
    "next": null,
    "previous": null,
    "page_size": 50,
    "num_pages": 1,
    "results": [
    {
      "resource_actions": [
        "role:assign",
        "role:create",
        "role:delete",
        "role:revoke",
        "role:update",
        "role:view"
      ],
      "space_uuid": "",
      "project_name": "a7",
      "uuid": "a8cc4904-e383-4dbf-8000-07f50f574c75",
      "project_uuid": "9e370ff2-1247-43dc-8160-239cdd86b450",
      "namespace": "alauda",
      "created_at": "2019-01-29T08:25:44Z",
      "created_by": "jtcheng",
      "name": "a7-project_admin",
      "space_name": "",
      "namespace_uuid": "",
      "type": "ROLE",
      "id": 529,
      "region_id": null,
      "user_data": [
        {
          "template_uuid": "593647b5-dd9f-437a-b724-7e3f0614a974",
          "template_display_name": "\u9879\u76ee\u7ba1\u7406\u5458",
          "realname": "",
          "template_name": "project_admin",
          "namespace": "alauda",
          "assigned_at": "2019-01-29T08:25:52Z",
          "user": "jtcheng",
          "role_uuid": "a8cc4904-e383-4dbf-8000-07f50f574c75",
          "role_name": "a7-project_admin",
          "email": ""
        }
      ]
    },
    {
      "space_uuid": "ec54ae04-2e4f-4431-9e4b-1c8ea3d8d9a4",
      "project_name": "a7",
      "uuid": "efe61b4f-ed5f-45e8-a01d-c918bd4b41e1",
      "project_uuid": "9e370ff2-1247-43dc-8160-239cdd86b450",
      "namespace": "alauda",
      "created_at": "2019-01-29T08:26:49Z",
      "created_by": "jtcheng",
      "name": "a7-arch-space_admin",
      "space_name": "a7-arch",
      "namespace_uuid": "",
      "type": "ROLE",
      "id": 545,
      "region_id": null,
      "user_data": [
        {
          "template_uuid": "593647b5-dd9f-437a-b724-7e3f0614a974",
          "template_display_name": "\u9879\u76ee\u7ba1\u7406\u5458",
          "realname": "",
          "template_name": "space_admin",
          "namespace": "alauda",
          "assigned_at": "2019-01-29T08:25:52Z",
          "user": "jtcheng",
          "role_uuid": "a8cc4904-e383-4dbf-8000-07f50f574c75",
          "role_name": "a7-arch-space_admin",
          "email": ""
        }
			]
    },
        {
            "resource_actions": [
                "role:delete",
                "role:update",
                "role:assign",
                "role:revoke",
                "role:view",
                "role:create"
            ],
            "space_uuid": "",
            "project_name": "a7",
            "uuid": "f552903a-0b52-495e-aebc-4e038f2c441e",
            "project_uuid": "9e370ff2-1247-43dc-8160-239cdd86b450",
            "namespace": "alauda",
            "created_at": "2019-01-29T08:26:37Z",
            "created_by": "jtcheng",
            "name": "a7-ace-a7-arch-namespace_admin",
            "space_name": "",
            "namespace_uuid": "9857bb8a-239f-11e9-a92d-5254000cec49",
            "type": "ROLE",
            "id": 533,
            "region_id": null,
            "user_data": [
                {
                    "template_uuid": "9985d18e-64c1-4533-8a45-225cdd03ded1",
                    "template_display_name": "命名空间管理员",
                    "realname": "",
                    "template_name": "namespace_admin",
                    "namespace": "alauda",
                    "assigned_at": "2019-04-04T11:40:20Z",
                    "user": "jtcheng",
                    "role_uuid": "f552903a-0b52-495e-aebc-4e038f2c441e",
                    "role_name": "a7-ace-a7-arch-namespace_admin",
                    "email": ""
                }
            ]
        },
        {
            "resource_actions": [
                "role:assign",
                "role:revoke",
                "role:view",
                "role:create",
                "role:delete",
                "role:update"
            ],
            "space_uuid": "",
            "project_name": "a7",
            "uuid": "cdbd00c3-b129-4d94-ae2c-684d978c4e74",
            "project_uuid": "9e370ff2-1247-43dc-8160-239cdd86b450",
            "namespace": "alauda",
            "created_at": "2019-01-29T08:26:37Z",
            "created_by": "jtcheng",
            "name": "a7-ace-a7-arch-namespace_auditor",
            "space_name": "",
            "namespace_uuid": "9857bb8a-239f-11e9-a92d-5254000cec49",
            "type": "ROLE",
            "id": 535,
            "region_id": null,
            "user_data": []
        },
        {
            "resource_actions": [
                "role:assign",
                "role:revoke",
                "role:view",
                "role:create",
                "role:delete",
                "role:update"
            ],
            "space_uuid": "",
            "project_name": "a7",
            "uuid": "a3e04934-5e37-4720-b0e5-daaf7c3f8d4f",
            "project_uuid": "9e370ff2-1247-43dc-8160-239cdd86b450",
            "namespace": "alauda",
            "created_at": "2019-01-29T08:26:37Z",
            "created_by": "jtcheng",
            "name": "a7-ace-a7-arch-namespace_developer",
            "space_name": "",
            "namespace_uuid": "9857bb8a-239f-11e9-a92d-5254000cec49",
            "type": "ROLE",
            "id": 537,
            "region_id": null,
            "user_data": [
                {
                    "template_uuid": "49219f26-76e7-485a-94cc-1bcb2ddb55d9",
                    "template_display_name": "命名空间开发人员",
                    "realname": "",
                    "template_name": "namespace_developer",
                    "namespace": "alauda",
                    "assigned_at": "2019-04-04T11:41:21Z",
                    "user": "jtcheng",
                    "role_uuid": "a3e04934-5e37-4720-b0e5-daaf7c3f8d4f",
                    "role_name": "a7-ace-a7-arch-namespace_developer",
                    "email": ""
                }
            ]
        },
        {
            "resource_actions": [
                "role:assign",
                "role:revoke",
                "role:view",
                "role:create",
                "role:delete",
                "role:update"
            ],
            "space_uuid": "",
            "project_name": "a7",
            "uuid": "3ce8dbe8-a1ef-4b10-8829-044c678cda23",
            "project_uuid": "9e370ff2-1247-43dc-8160-239cdd86b450",
            "namespace": "alauda",
            "created_at": "2019-03-08T03:08:48Z",
            "created_by": "alauda",
            "name": "a7-ace-a7-nn-namespace_admin",
            "space_name": "",
            "namespace_uuid": "7e20a064-414f-11e9-b335-52540052ef9b",
            "type": "ROLE",
            "id": 659,
            "region_id": null,
            "user_data": [
                {
                    "template_uuid": "9985d18e-64c1-4533-8a45-225cdd03ded1",
                    "template_display_name": "命名空间管理员",
                    "realname": "",
                    "template_name": "namespace_admin",
                    "namespace": "alauda",
                    "assigned_at": "2019-04-04T11:40:20Z",
                    "user": "jtcheng",
                    "role_uuid": "3ce8dbe8-a1ef-4b10-8829-044c678cda23",
                    "role_name": "a7-ace-a7-nn-namespace_admin",
                    "email": ""
                }
            ]
        },
        {
            "resource_actions": [
                "role:view",
                "role:create",
                "role:delete",
                "role:update",
                "role:assign",
                "role:revoke"
            ],
            "space_uuid": "",
            "project_name": "a7",
            "uuid": "f0177c39-400a-4274-8ef0-e6493e388a50",
            "project_uuid": "9e370ff2-1247-43dc-8160-239cdd86b450",
            "namespace": "alauda",
            "created_at": "2019-03-08T03:08:48Z",
            "created_by": "alauda",
            "name": "a7-ace-a7-nn-namespace_auditor",
            "space_name": "",
            "namespace_uuid": "7e20a064-414f-11e9-b335-52540052ef9b",
            "type": "ROLE",
            "id": 661,
            "region_id": null,
            "user_data": []
        },
        {
            "resource_actions": [
                "role:assign",
                "role:revoke",
                "role:view",
                "role:create",
                "role:delete",
                "role:update"
            ],
            "space_uuid": "",
            "project_name": "a7",
            "uuid": "b72aef60-5b6e-4c93-9cf1-5abfa896c5e2",
            "project_uuid": "9e370ff2-1247-43dc-8160-239cdd86b450",
            "namespace": "alauda",
            "created_at": "2019-03-08T03:08:48Z",
            "created_by": "alauda",
            "name": "a7-ace-a7-nn-namespace_developer",
            "space_name": "",
            "namespace_uuid": "7e20a064-414f-11e9-b335-52540052ef9b",
            "type": "ROLE",
            "id": 663,
            "region_id": null,
            "user_data": [
                {
                    "template_uuid": "49219f26-76e7-485a-94cc-1bcb2ddb55d9",
                    "template_display_name": "命名空间开发人员",
                    "realname": "",
                    "template_name": "namespace_developer",
                    "namespace": "alauda",
                    "assigned_at": "2019-04-04T11:41:21Z",
                    "user": "jtcheng",
                    "role_uuid": "b72aef60-5b6e-4c93-9cf1-5abfa896c5e2",
                    "role_name": "a7-ace-a7-nn-namespace_developer",
                    "email": ""
                }
            ]
        },
        {
            "resource_actions": [
                "role:assign",
                "role:revoke",
                "role:view",
                "role:create",
                "role:delete",
                "role:update"
            ],
            "space_uuid": "",
            "project_name": "a8",
            "uuid": "b72aef60-5b6e-4c93-9cf1-5abfa896c5e2",
            "project_uuid": "9e370ff2-1247-43dc-8160-239cdd86b450",
            "namespace": "alauda",
            "created_at": "2019-03-08T03:08:48Z",
            "created_by": "alauda",
            "name": "myrole",
            "space_name": "",
            "namespace_uuid": "",
            "type": "ROLE",
            "id": 663,
            "region_id": null,
            "user_data": [
                {
                    "template_uuid": "",
                    "template_display_name": "",
                    "realname": "",
                    "template_name": "",
                    "namespace": "alauda",
                    "assigned_at": "2019-04-04T11:41:21Z",
                    "user": "user1",
                    "role_uuid": "b72aef60-5b6e-4c93-9cf1-5abfa896c5e2",
                    "role_name": "myrole",
                    "email": ""
                }
            ]
        }
		]}`), nil)

		res, err := client.GetRolesWithUsers(ctx, pageParam, projectParam)
		Expect(err).To(BeNil())
		items := res.GetItems()
		Expect(len(items)).To(Equal(5))

		Expect(items).To(ContainElement(&ace.RoleUserAssignment{
			RoleName:           "project_admin",
			RoleUUID:           "a8cc4904-e383-4dbf-8000-07f50f574c75",
			IsRoleFromTemplate: true,
			User:               "jtcheng",
			Email:              "",
			NamespaceUUID:      "",
		}))
		Expect(items).To(ContainElement(&ace.RoleUserAssignment{
			RoleName:           "space_admin",
			RoleUUID:           "a8cc4904-e383-4dbf-8000-07f50f574c75",
			IsRoleFromTemplate: true,
			User:               "jtcheng",
			SpaceName:          "a7-arch",
			Email:              "",
			NamespaceUUID:      "",
		}))
		Expect(items).To(ContainElement(&ace.RoleUserAssignment{
			RoleName:           "namespace_admin",
			RoleUUID:           "f552903a-0b52-495e-aebc-4e038f2c441e",
			IsRoleFromTemplate: true,
			User:               "jtcheng",
			Email:              "",
			NamespaceUUID:      "9857bb8a-239f-11e9-a92d-5254000cec49",
		}))
		Expect(items).To(ContainElement(&ace.RoleUserAssignment{
			RoleName:           "namespace_developer",
			RoleUUID:           "a3e04934-5e37-4720-b0e5-daaf7c3f8d4f",
			IsRoleFromTemplate: true,
			User:               "jtcheng",
			Email:              "",
			NamespaceUUID:      "9857bb8a-239f-11e9-a92d-5254000cec49",
		}))
		Expect(items).To(ContainElement(&ace.RoleUserAssignment{
			RoleName:           "myrole",
			RoleUUID:           "b72aef60-5b6e-4c93-9cf1-5abfa896c5e2",
			IsRoleFromTemplate: false,
			User:               "user1",
			Email:              "",
			NamespaceUUID:      "",
		}))
	})
})
