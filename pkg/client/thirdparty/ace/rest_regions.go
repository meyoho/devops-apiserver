package ace

import (
	"context"
	"net/http"
)

type RegionExtension interface {
	ListRegions(ctx context.Context, opts ...RequestOptions) ([]Region, error)
	GetRegion(ctx context.Context, regionName string, opts ...RequestOptions) (Region, error)
}

func (ace *AlaudaCloudClient) ListRegions(ctx context.Context, opts ...RequestOptions) ([]Region, error) {

	var request *http.Request
	request, err := ace.NewReq().
		Get().
		JSON().
		Path(
			ace.opts.APIAddress,
			apiVersionV2,
			resourceRegions,
			ace.opts.RootAccount,
		).
		Params(opts...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return []Region{}, err
	}
	result := &[]Region{}
	err = ace.DoRequest(request, result)
	return *result, err
}

func (ace *AlaudaCloudClient) GetRegion(ctx context.Context, regionName string, opts ...RequestOptions) (Region, error) {

	var request *http.Request
	request, err := ace.NewReq().
		Get().
		JSON().
		Path(
			ace.opts.APIAddress,
			apiVersionV2,
			resourceRegions,
			ace.opts.RootAccount,
			regionName,
		).
		Params(opts...).
		Context(ctx).
		TokenAuth(ace.opts.Token).Done()
	if err != nil {
		return Region{}, err
	}
	result := &Region{}
	err = ace.DoRequest(request, result)
	return *result, err
}
