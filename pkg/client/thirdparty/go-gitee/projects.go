//
// Copyright 2018, sywang
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package gitee

import (
	"fmt"
	"strings"
	"time"
)

// ProjectsService handles communication with the repositories related methods
// of the Gitee API.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
type ProjectsService struct {
	client *Client
}

// Project represents a Gitee project.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
type Project struct {
	ID                  int                `json:"id"`
	FullName            string             `json:"full_name"`
	HumanName           string             `json:"human_name"`
	URL                 string             `json:"url"`
	Path                string             `json:"path"`
	Name                string             `json:"name"`
	Owner               *User              `json:"owner"`
	Description         string             `json:"description"`
	Private             bool               `json:"private"`
	Public              bool               `json:"public"`
	Internal            bool               `json:"internal"`
	Fork                bool               `json:"fork"`
	HtmlURL             string             `json:"html_url"`
	ForksURL            string             `json:"forks_url"`
	KeysURL             string             `json:"keys_url"`
	CollaboratorsURL    string             `json:"collaborators_url"`
	HooksURL            string             `json:"hooks_url"`
	BranchesURL         string             `json:"branches_url"`
	TagsURL             string             `json:"tags_url"`
	BlobsURL            string             `json:"blobs_url"`
	StargazersURL       string             `json:"stargazers_url"`
	ContributorsURL     string             `json:"contributors_url"`
	CommitsURL          string             `json:"commits_url"`
	CommentsURL         string             `json:"comments_url"`
	IssueCommentURL     string             `json:"issue_comment_url"`
	IssuesURL           string             `json:"issues_url"`
	PullsURL            string             `json:"pulls_url"`
	MilestonesURL       string             `json:"milestones_url"`
	NotificationsURL    string             `json:"notifications_url"`
	LabelsURL           string             `json:"labels_url"`
	ReleasesURL         string             `json:"releases_url"`
	Recommend           bool               `json:"recommend"`
	Homepage            string             `json:"homepage"`
	Language            string             `json:"language"`
	ForksCount          int                `json:"forks_count"`
	StargazersCount     int                `json:"stargazers_count"`
	WatchersCount       int                `json:"watchers_count"`
	DefaultBranch       string             `json:"default_branch"`
	OpenIssuesCount     int                `json:"open_issues_count"`
	HasIssues           bool               `json:"has_issues"`
	HasWiki             bool               `json:"has_wiki"`
	PullRequestsEnabled bool               `json:"pull_requests_enabled"`
	HasPage             bool               `json:"has_page"`
	License             string             `json:"license"`
	PushedAt            *time.Time         `json:"pushed_at"`
	CreatedAt           *time.Time         `json:"created_at"`
	UpdatedAt           *time.Time         `json:"updated_at"`
	Parent              *Project           `json:"parent"` // parent is nil when fork == false
	Paas                string             `json:"paas"`
	Stared              bool               `json:"stared"`
	Watched             bool               `json:"watched"`
	Permission          *ProjectPermission `json:"permission"`
	RELATION            string             `json:"relation"`
}

func (p *Project) GetOwnerAvatarURL() (ownerAvatarURL string) {
	if p.Owner == nil {
		return
	}

	return p.Owner.AvatarURL
}

func (p *Project) GetOwnerWebURL() (ownerAvatarURL string) {
	if p.Owner == nil {
		return
	}

	return p.HtmlURL
}

func (p *Project) GetOwnerLogin() (login string) {
	names := strings.Split(p.FullName, "/")
	if len(names) > 1 {
		return names[0]
	} else {
		return p.FullName
	}
}

func (p *Project) GetOwner() (ownerAvatarURL string) {
	if p.Owner == nil {
		return
	}

	return p.HtmlURL
}

func (p *Project) GetCloneURL() string {
	//https://gitee.com/sniperyen1/sy-prj1.git
	return fmt.Sprintf("%s.git", p.HtmlURL)
}

func (p *Project) GetSSHURL() string {
	// git@gitee.com:sniperyen1/sy-prj1.git
	cloneURL := p.GetCloneURL()
	urls := strings.Split(cloneURL, "//")
	if len(urls) > 1 {
		return fmt.Sprintf("git@%s", urls[1])
	}
	return cloneURL
}

type Branch struct {
	Name   string `json:"name"`
	Commit struct {
		SHA string `json:"sha"`
		URL string `json:"url"`
	} `json:"commit"`
	Protected     bool   `json:"protected"`
	ProtectionURL string `json:"protection_url"`
}

type ProjectPermission struct {
	Pull  bool `json:"pull"`
	Push  bool `json:"push"`
	Admin bool `json:"admin"`
}

func (s Project) String() string {
	return Stringify(s)
}

// ListProjectsOptions represents the available List() options.
//
// Gitee API docs: https://gitee.com/api/v5/swagger#/getV5UserRepos
type ListProjectsOptions struct {
	ListOptions
	Visibility  *VisibilityValue `url:"visibility,omitempty" json:"visibility,omitempty"`
	Affiliation string           `url:"affiliation,omitempty" json:"affiliation,omitempty"`
	Type        *TypeValue       `url:"type,omitempty" json:"type,omitempty"`
	Sort        *SortValue       `url:"sort,omitempty" json:"sort,omitempty"`
	Direction   *string          `url:"direction,omitempty" json:"direction,omitempty"`
}

// List gets a list of projects accessible by the authenticated user.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
func (s *ProjectsService) List(user string, opt *ListProjectsOptions, options ...OptionFunc) ([]*Project, *Response, error) {
	var u string
	if user != "" {
		u = fmt.Sprintf("users/%s/repos", user)
	} else {
		u = "user/repos"
	}
	req, err := s.client.NewRequest("GET", u, opt, options)
	if err != nil {
		return nil, nil, err
	}

	var p []*Project
	resp, err := s.client.Do(req, &p)
	if err != nil {
		return nil, resp, err
	}

	return p, resp, err
}

// Get gets a specific project, identified by OWNER_NAME/PROJECT_NAME, which is owned by the authenticated user.
//
// Gitee API docs: https://gitee.com/api/v5/swagger#/getV5ReposOwnerRepo
func (s *ProjectsService) Get(fullName string, options ...OptionFunc) (*Project, *Response, error) {
	u := fmt.Sprintf("repos/%s", fullName)

	req, err := s.client.NewRequest("GET", u, nil, options)
	if err != nil {
		return nil, nil, err
	}

	p := new(Project)
	resp, err := s.client.Do(req, p)
	if err != nil {
		return nil, resp, err
	}

	return p, resp, err
}

// Branches get branches
//
// Gitee API docs: https://gitee.com/api/v5/swagger#/getV5ReposOwnerRepoBranches
func (s *ProjectsService) Branches(fullName string, opt *ListOptions, options ...OptionFunc) ([]Branch, *Response, error) {
	u := fmt.Sprintf("repos/%s/branches", fullName)

	req, err := s.client.NewRequest("GET", u, opt, options)
	if err != nil {
		return nil, nil, err
	}
	var p []Branch
	resp, err := s.client.Do(req, &p)
	if err != nil {
		return nil, resp, err
	}
	return p, resp, err
}
