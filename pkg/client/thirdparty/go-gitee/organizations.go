//
// Copyright 2018, sywang
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package gitee

import (
	"fmt"
	"net/http"
)

// ProjectsService handles communication with the repositories related methods
// of the Gitee API.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
type OrganizationsService struct {
	client *Client
}

// Project represents a Gitee project.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
type Organization struct {
	ID          int    `json:"id"`
	Login       string `json:"login"`
	URL         string `json:"url"`
	AvatarURL   string `json:"avatar_url"`
	ReposURL    string `json:"repos_url"`
	EventsURL   string `json:"events_url"`
	MembersURL  string `json:"members_url"`
	Description string `json:"description"`
}

func (s Organization) String() string {
	return Stringify(s)
}

// ListProjectsOptions represents the available List() options.
//
// Gitee API docs: https://gitee.com/api/v5/swagger#/getV5UserOrgs
type ListOrganizationsOptions struct {
	ListOptions
}

// List gets a list of projects accessible by the authenticated user.
//
// Gitee API docs: https://gitee.com/api/v5/swagger#/getV5UserOrgs
func (s *OrganizationsService) List(user string, opt *ListOrganizationsOptions, options ...OptionFunc) ([]*Organization, *Response, error) {
	var u string
	if user != "" {
		u = fmt.Sprintf("users/%s/orgs", user)
	} else {
		u = "user/orgs"
	}
	req, err := s.client.NewRequest("GET", u, opt, options)
	if err != nil {
		return nil, nil, err
	}

	var p []*Organization
	resp, err := s.client.Do(req, &p)
	if err != nil {
		return nil, resp, err
	}

	return p, resp, err
}

type CreateOrganizationOptions struct {
	Name        string `json:"name"`
	Org         string `json:"org"`
	Description string `json:"description"`
}

func (s *OrganizationsService) Create(createOptions CreateOrganizationOptions, options ...OptionFunc) (*Organization, *Response, error) {
	var path = "users/organization"

	req, err := s.client.NewRequest(http.MethodPost, path, createOptions, options)
	if err != nil {
		return nil, nil, err
	}
	var org = &Organization{}
	resp, err := s.client.Do(req, org)

	if resp.StatusCode == http.StatusNotFound {
		// gitee will return 404 when organization is already existed ...
		// we should ensure it
		org, _, _ := s.Get(createOptions.Org)
		if org != nil {
			resp.Response.StatusCode = http.StatusBadRequest
			return org, nil, &ErrorResponse{
				Response: resp.Response,
				Message:  fmt.Sprintf("organization '%s' is already exists", createOptions.Name),
			}
		}
	}

	if err != nil {
		return nil, resp, err
	}

	return org, resp, err
}

func (s *OrganizationsService) Get(org string, options ...OptionFunc) (*Organization, *Response, error) {
	var path = fmt.Sprintf("orgs/%s", org)

	req, err := s.client.NewRequest(http.MethodGet, path, nil, options)
	if err != nil {
		return nil, nil, err
	}
	var organization = &Organization{}
	resp, err := s.client.Do(req, organization)
	if err != nil {
		return nil, resp, err
	}

	return organization, resp, err
}
