//
// Copyright 2018, sywang
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package gitee

import (
	"fmt"
	"time"
)

// ProjectsService handles communication with the repositories related methods
// of the Gitee API.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
type CommitsService struct {
	client *Client
}

// Project represents a Gitee project.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
// Generated by https://quicktype.io

type Commit struct {
	URL         string         `json:"url"`
	SHA         string         `json:"sha"`
	HTMLURL     string         `json:"html_url"`
	CommentsURL string         `json:"comments_url"`
	Commit      CommitClass    `json:"commit"`
	Author      CommitterClass `json:"author"`
	Committer   CommitterClass `json:"committer"`
	Parents     []interface{}  `json:"parents"`
	Stats       Stats          `json:"stats"`
}

func (s Commit) String() string {
	return Stringify(s)
}

type CommitterClass struct {
	ID                int64  `json:"id"`
	Login             string `json:"login"`
	Name              string `json:"name"`
	AvatarURL         string `json:"avatar_url"`
	URL               string `json:"url"`
	HTMLURL           string `json:"html_url"`
	FollowersURL      string `json:"followers_url"`
	FollowingURL      string `json:"following_url"`
	GistsURL          string `json:"gists_url"`
	StarredURL        string `json:"starred_url"`
	SubscriptionsURL  string `json:"subscriptions_url"`
	OrganizationsURL  string `json:"organizations_url"`
	ReposURL          string `json:"repos_url"`
	EventsURL         string `json:"events_url"`
	ReceivedEventsURL string `json:"received_events_url"`
	Type              string `json:"type"`
	SiteAdmin         bool   `json:"site_admin"`
}

type CommitClass struct {
	Author   CommitClassCommiter `json:"author"`
	Commiter CommitClassCommiter `json:"commiter"`
	Message  string              `json:"message"`
	Tree     Tree                `json:"tree"`
}

type CommitClassCommiter struct {
	Name  string     `json:"name"`
	Date  *time.Time `json:"date"`
	Email string     `json:"email"`
}

type Tree struct {
	SHA string `json:"sha"`
	URL string `json:"url"`
}

type Stats struct {
	ID        string `json:"id"`
	Additions int64  `json:"additions"`
	Deletions int64  `json:"deletions"`
	Total     int64  `json:"total"`
}

// ListCommitsOptions represents the available List() options.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
type ListCommitsOptions struct {
	ListOptions
	Sha    *string    `url:"sha,omitempty" json:"sha,omitempty"`
	Path   *string    `url:"path,omitempty" json:"path,omitempty"`
	Author *string    `url:"author,omitempty" json:"author,omitempty"`
	Since  *time.Time `url:"since,omitempty" json:"since,omitempty"`
	Until  *string    `url:"until,omitempty" json:"until,omitempty"`
}

// List gets a list of projects accessible by the authenticated user.
//
// Gitee API docs: https://gitee.com/api/v5/swagger
func (s *CommitsService) List(repoFullName string, opt *ListCommitsOptions, options ...OptionFunc) ([]*Commit, *Response, error) {
	u := fmt.Sprintf("repos/%s/commits", repoFullName)
	req, err := s.client.NewRequest("GET", u, opt, options)
	if err != nil {
		return nil, nil, err
	}

	var p []*Commit
	resp, err := s.client.Do(req, &p)
	if err != nil {
		return nil, resp, err
	}

	return p, resp, err
}
