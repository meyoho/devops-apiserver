//
// Copyright 2018, jtcheng
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package bitbucket

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/go-bitbucket/types"
	"context"
	"fmt"
)

// TeamsService provides access to the team related functions
// in the Bitbucket API.
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
type UsersService service

// Gets the public information associated with a team.
// If the team's profile is private, location, website and created_on elements are omitted.
// @username The team's username or UUID.
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
func (s *UsersService) Get(ctx context.Context, username string) (*types.User, *Response, error) {
	u := fmt.Sprintf("users/%s", username)
	if username == "" {
		u = "user"
	}

	req, err := s.client.NewRequest("GET", u, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	user := new(types.User)
	resp, err := s.client.Do(ctx, req, user)
	if err != nil {
		return nil, resp, err
	}

	return user, resp, nil
}
