//
// Copyright 2018, sywang
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package bitbucket

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"

	"github.com/google/go-querystring/query"
	"golang.org/x/oauth2"
)

const (
	defaultBaseURL = "https://api.bitbucket.org/"
	apiVersionPath = "2.0/"
	userAgent      = "go-bitbucket"
	AccessToken    = "access_token"
	// https://developer.atlassian.com/bitbucket/api/2/reference/meta/pagination
	DefaultMaxPagelen = 100
	MaxPagelen        = 3000
)

// VisibilityValue represents a visibility level within Bitbucket.
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
type VisibilityValue string

// List of available visibility levels
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
const (
	AllVisibility     VisibilityValue = "all"
	PrivateVisibility VisibilityValue = "private"
	PublicVisibility  VisibilityValue = "public"
)

type SortValue string

const (
	FullNameSortValue SortValue = "full_name"
	CreatedSortValue  SortValue = "created"
	UpdatedSortValue  SortValue = "updated"
	PushedSortValue   SortValue = "pushed"
)

type RoleValue string

const (
	RoleValueOwner      RoleValue = "owner"
	RoleValueAdmin      RoleValue = "admin"
	RoleValueCntributor RoleValue = "contributor"
	RoleValueMember     RoleValue = "member"
)

// authType represents an authentication type within Bitbucket.
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
type authType int

// List of available authentication types.
//
const (
	basicAuth authType = iota
	oAuthToken
)

// A Client manages communication with the Bitbucket API.
type Client struct {
	// HTTP client used to communicate with the API.
	client *http.Client

	// Base URL for API requests. Defaults to the public Bitbucket API, but can be
	// set to a domain endpoint to use with a self hosted Bitbucket server. baseURL
	// should always be specified with a trailing slash.
	baseURL *url.URL

	// Token type used to make authenticated API calls.
	authType authType

	// Username and password used for basix authentication.
	username, password string

	// Token used to make authenticated API calls.
	token string

	// User agent used when communicating with the Bitbucket API.
	UserAgent string

	Repositories *RepositoriesService
	Commits      *CommitsService
	Teams        *TeamsService
	Users        *UsersService
	Branches     *BranchesService
}

type service struct {
	client *Client
}

// NewClient returns a new Bitbucket API client. If a nil httpClient is
// provided, http.DefaultClient will be used. To use API methods which require
// authentication, provide a valid private or personal token.
func NewClient(httpClient *http.Client, token string) *Client {
	client := newClient(httpClient)
	client.authType = oAuthToken
	client.token = token
	return client
}

// NewOAuthTokenClient returns a new Bitbucket API client. If a nil httpClient is
// provided, http.DefaultClient will be used. To use API methods which require
// authentication, provide a valid username and password.
func NewOAuthTokenClient(httpClient *http.Client, endpoint, username, password string) (*Client, error) {
	client := newClient(httpClient)
	client.authType = oAuthToken
	client.username = username
	client.password = password
	if endpoint != "" {
		client.SetBaseURL(endpoint)
	}

	err := client.requestOAuthToken(context.TODO())
	if err != nil {
		return nil, err
	}

	return client, nil
}

func NewBasicAuthClient(httpClient *http.Client, endpoint, username, password string) (*Client, error) {
	client := newClient(httpClient)
	client.authType = basicAuth
	client.username = username
	client.password = password
	if endpoint != "" {
		client.SetBaseURL(endpoint)
	}

	return client, nil
}

func newClient(httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	c := &Client{client: httpClient, UserAgent: userAgent}
	if err := c.SetBaseURL(defaultBaseURL); err != nil {
		// Should never happen since defaultBaseURL is our constant.
		panic(err)
	}

	c.Repositories = &RepositoriesService{client: c}
	c.Commits = &CommitsService{client: c}
	c.Teams = &TeamsService{client: c}
	c.Users = &UsersService{client: c}
	c.Branches = &BranchesService{client: c}
	return c
}

func (c *Client) GetToken() string {
	return c.token
}

// BaseURL return a copy of the baseURL.
func (c *Client) BaseURL() *url.URL {
	u := *c.baseURL
	return &u
}

// SetBaseURL sets the base URL for API requests to a custom endpoint. urlStr
// should always be specified with a trailing slash.
func (c *Client) SetBaseURL(urlStr string) error {
	// Make sure the given URL end with a slash
	if !strings.HasSuffix(urlStr, "/") {
		urlStr += "/"
	}

	baseURL, err := url.Parse(urlStr)
	if err != nil {
		return err
	}

	if !strings.HasSuffix(baseURL.Path, apiVersionPath) {
		baseURL.Path += apiVersionPath
	}

	// Update the base URL of the client.
	c.baseURL = baseURL

	return nil
}

// NewRequest creates an API request. A relative URL path can be provided in
// urlStr, in which case it is resolved relative to the base URL of the Client.
// Relative URL paths should always be specified without a preceding slash. If
// specified, the value pointed to by body is JSON encoded and included as the
// request body.
func (c *Client) NewRequest(method, path string, opt interface{}, options []OptionFunc) (*http.Request, error) {
	u := *c.baseURL
	// Set the encoded opaque data
	u.Opaque = c.baseURL.Path + path

	if opt != nil {
		q, err := query.Values(opt)
		if err != nil {
			return nil, err
		}
		u.RawQuery = q.Encode()
	}

	req := &http.Request{
		Method:     method,
		URL:        &u,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 1,
		Header:     make(http.Header),
		Host:       u.Host,
	}

	for _, fn := range options {
		if fn == nil {
			continue
		}

		if err := fn(req); err != nil {
			return nil, err
		}
	}

	if method == "POST" || method == "PUT" {
		bodyBytes, err := json.Marshal(opt)
		if err != nil {
			return nil, err
		}
		bodyReader := bytes.NewReader(bodyBytes)

		u.RawQuery = ""
		req.Body = ioutil.NopCloser(bodyReader)
		req.ContentLength = int64(bodyReader.Len())
		req.Header.Set("Content-Type", "application/json")
	}

	req.Header.Set("Accept", "application/json")

	switch c.authType {
	case basicAuth:
		req.SetBasicAuth(c.username, c.password)
	case oAuthToken:
		req.Header.Set("Authorization", "Bearer "+c.token)
	}

	if c.UserAgent != "" {
		req.Header.Set("User-Agent", c.UserAgent)
	}

	return req, nil
}

// Response is a Bitbucket API response. This wraps the standard http.Response
// returned from Bitbucket and provides convenient access to things like
// pagination links.
type Response struct {
	*http.Response

	// These fields provide the page values for paginating through a set of
	// results. Any or all of these may be set to the zero value for
	// responses that are not part of a paginated set, or for which there
	// are no additional pages.
	TotalItems   int
	TotalPages   int
	ItemsPerPage int
	CurrentPage  int
	NextPage     int
	PreviousPage int
}

// newResponse creates a new Response for the provided http.Response.
func newResponse(r *http.Response) *Response {
	response := &Response{Response: r}
	response.populatePageValues()
	return response
}

const (
	xTotal      = "X-Total"
	xTotalPages = "X-Total-Pages"
	xPerPage    = "X-Per-Page"
	xPage       = "X-Page"
	xNextPage   = "X-Next-Page"
	xPrevPage   = "X-Prev-Page"
)

// populatePageValues parses the HTTP Link response headers and populates the
// various pagination link values in the Response.
func (r *Response) populatePageValues() {
	if totalItems := r.Response.Header.Get(xTotal); totalItems != "" {
		r.TotalItems, _ = strconv.Atoi(totalItems)
	}
	if totalPages := r.Response.Header.Get(xTotalPages); totalPages != "" {
		r.TotalPages, _ = strconv.Atoi(totalPages)
	}
	if itemsPerPage := r.Response.Header.Get(xPerPage); itemsPerPage != "" {
		r.ItemsPerPage, _ = strconv.Atoi(itemsPerPage)
	}
	if currentPage := r.Response.Header.Get(xPage); currentPage != "" {
		r.CurrentPage, _ = strconv.Atoi(currentPage)
	}
	if nextPage := r.Response.Header.Get(xNextPage); nextPage != "" {
		r.NextPage, _ = strconv.Atoi(nextPage)
	}
	if previousPage := r.Response.Header.Get(xPrevPage); previousPage != "" {
		r.PreviousPage, _ = strconv.Atoi(previousPage)
	}
}

// Do sends an API request and returns the API response. The API response is
// JSON decoded and stored in the value pointed to by v, or returned as an
// error if an API error has occurred. If v implements the io.Writer
// interface, the raw response body will be written to v, without attempting to
// first decode it.
func (c *Client) Do(ctx context.Context, req *http.Request, v interface{}) (*Response, error) {
	if ctx == nil {
		ctx = context.Background()
	}
	req.WithContext(ctx)

	resp, err := c.client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusUnauthorized && c.authType == basicAuth {
		err = c.requestOAuthToken(req.Context())
		if err != nil {
			return nil, err
		}
		return c.Do(ctx, req, v)
	}

	response := newResponse(resp)

	err = CheckResponse(resp)
	if err != nil {
		// even though there was an error, we still return the response
		// in case the caller wants to inspect it further
		return response, err
	}

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			_, err = io.Copy(w, resp.Body)
		} else {
			err = json.NewDecoder(resp.Body).Decode(v)
		}
	}

	return response, err
}

func (c *Client) requestOAuthToken(ctx context.Context) error {
	config := &oauth2.Config{
		Endpoint: oauth2.Endpoint{
			AuthURL:  fmt.Sprintf("%s://%s/oauth/authorize", c.BaseURL().Scheme, c.BaseURL().Host),
			TokenURL: fmt.Sprintf("%s://%s/oauth/token", c.BaseURL().Scheme, c.BaseURL().Host),
		},
	}
	ctx = context.WithValue(ctx, oauth2.HTTPClient, c.client)
	t, err := config.PasswordCredentialsToken(ctx, c.username, c.password)
	if err != nil {
		return err
	}
	c.token = t.AccessToken
	return nil
}

// OptionFunc can be passed to all API requests to make the API call as if you were
// another user, provided your private token is from an administrator account.
//
// Bitbucket docs: https://developer.atlassian.com/bitbucket/api/2/reference/
type OptionFunc func(*http.Request) error

// WithContext runs the request with the provided context
func WithContext(ctx context.Context) OptionFunc {
	return func(req *http.Request) error {
		*req = *req.WithContext(ctx)
		return nil
	}
}

// Helper function to accept and format both the project ID or name as project
// identifier for all API calls.
func parseID(id interface{}) (string, error) {
	switch v := id.(type) {
	case int:
		return strconv.Itoa(v), nil
	case string:
		return v, nil
	default:
		return "", fmt.Errorf("invalid ID type %#v, the ID must be an int or a string", id)
	}
}

// An ErrorResponse reports one or more errors caused by an API request.
//
// Bitbucket API docs:
// https://developer.atlassian.com/bitbucket/api/2/reference/
type ErrorResponse struct {
	Body     []byte
	Response *http.Response
	Message  string
}

func (e *ErrorResponse) Error() string {
	path, _ := url.QueryUnescape(e.Response.Request.URL.Opaque)
	u := fmt.Sprintf("%s://%s%s", e.Response.Request.URL.Scheme, e.Response.Request.URL.Host, path)
	return fmt.Sprintf("%s %s: %d %s", e.Response.Request.Method, u, e.Response.StatusCode, e.Message)
}

// CheckResponse checks the API response for errors, and returns them if present.
func CheckResponse(r *http.Response) error {
	switch r.StatusCode {
	case 200, 201, 202, 204, 304:
		return nil
	}

	errorResponse := &ErrorResponse{Response: r}
	data, err := ioutil.ReadAll(r.Body)
	if err == nil && data != nil {
		errorResponse.Body = data

		var raw interface{}
		if err := json.Unmarshal(data, &raw); err != nil {
			errorResponse.Message = "failed to parse unknown error format"
		} else {
			errorResponse.Message = parseError(raw)
		}
	}

	return errorResponse
}

func parseError(raw interface{}) string {
	switch raw := raw.(type) {
	case string:
		return raw

	case []interface{}:
		var errs []string
		for _, v := range raw {
			errs = append(errs, parseError(v))
		}
		return fmt.Sprintf("[%s]", strings.Join(errs, ", "))

	case map[string]interface{}:
		var errs []string
		for k, v := range raw {
			errs = append(errs, fmt.Sprintf("{%s: %s}", k, parseError(v)))
		}
		sort.Strings(errs)
		return strings.Join(errs, ", ")

	default:
		return fmt.Sprintf("failed to parse unexpected error type: %T", raw)
	}
}

// ListOptions specifies the optional parameters to various List methods that
// support pagination.
type ListOptions struct {
	// For paginated result sets, page of results to retrieve.
	Page int `url:"page,omitempty" json:"page,omitempty"`

	// For paginated result sets, the number of results to include per page.
	PageLen int `url:"pagelen,omitempty" json:"pagelen,omitempty"`
}

// Bool is a helper routine that allocates a new bool value
// to store v and returns a pointer to it.
func Bool(v bool) *bool {
	p := new(bool)
	*p = v
	return p
}

// Int is a helper routine that allocates a new int32 value
// to store v and returns a pointer to it, but unlike Int32
// its argument value is an int.
func Int(v int) *int {
	p := new(int)
	*p = v
	return p
}

// String is a helper routine that allocates a new string value
// to store v and returns a pointer to it.
func String(v string) *string {
	p := new(string)
	*p = v
	return p
}

// Role is a helper routine that allocates a new Role value
// to store v and returns a pointer to it.
func Role(v RoleValue) *RoleValue {
	p := new(RoleValue)
	*p = v
	return p
}
