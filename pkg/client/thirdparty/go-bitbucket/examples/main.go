package main

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/go-bitbucket"
	"context"
	"fmt"
	"log"
	"os"
)

// export BITBUCKET_USERNAME=xxx
// export BITBUCKET_PASSWORD=xxx
// go run main.go
func main() {
	ProjectExample()
	UserExample()
}

func UserExample() {
	git, err := bitbucket.NewBasicAuthClient(nil, "", os.Getenv("BITBUCKET_USERNAME"), os.Getenv("BITBUCKET_PASSWORD"))
	if err != nil {
		log.Fatal(err)
		return
	}

	u, _, err := git.Users.Get(context.TODO(), "")
	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Printf("%#v", u)
}

func ProjectExample() {
	git, err := bitbucket.NewBasicAuthClient(nil, "", os.Getenv("BITBUCKET_USERNAME"), os.Getenv("BITBUCKET_PASSWORD"))
	if err != nil {
		log.Fatal(err)
		return
	}

	opt := bitbucket.ListRepositoriesOptions{Role: bitbucket.Role(bitbucket.RoleValueCntributor)}
	repositoryList, _, err := git.Repositories.ListAll("", &opt)
	if err != nil {
		log.Fatal(err)
		return
	}
	for i, repository := range repositoryList {
		log.Printf("Found %d repository: %s; owner: %s; type: %s", i, repository.FullName, repository.Owner.Username, repository.Owner.Type)
	}
	repositoryList, _, err = git.Repositories.ListAll(os.Getenv("BITBUCKET_USERNAME"), &opt)
	if err != nil {
		log.Fatal(err)
		return
	}
	for i, repository := range repositoryList {
		log.Printf("Found %d repository: %s; owner: %s; type: %s", i, repository.FullName, repository.Owner.Username, repository.Owner.Type)
	}

	teams, _, err := git.Teams.List(nil, &bitbucket.TeamsListOptions{Role: bitbucket.Role(bitbucket.RoleValueMember)}, nil)
	if err != nil {
		log.Fatal(err)
		return
	}
	for i, team := range teams.Values {
		log.Printf("Found %d team: %s; uuid: %s", i, team.Username, team.Uuid)
		team, _, err := git.Teams.Get(nil, team.Username)
		if err != nil {
			log.Fatal(err)
			return
		}
		log.Printf("=> display_name: %s, created_on: %v", team.DisplayName, team.CreatedOn)
	}

	teams, _, err = git.Teams.ListAll(nil, &bitbucket.TeamsListOptions{Role: bitbucket.Role(bitbucket.RoleValueMember)}, nil)
	if err != nil {
		log.Fatal(err)
		return
	}
	for i, team := range teams.Values {
		log.Printf("Found %d team: %s; uuid: %s", i, team.Username, team.Uuid)
		team, _, err := git.Teams.Get(nil, team.Username)
		if err != nil {
			log.Fatal(err)
			return
		}
		log.Printf("=> display_name: %s, created_on: %v", team.DisplayName, team.CreatedOn)
	}
}
