//
// Copyright 2018, sywang
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package bitbucket

import (
	"fmt"
	"time"
)

// CommitsService handles communication with the commits related methods
// of the Bitbucket API.
//
// Bitbucket API docs: https://Bitbucket.com/api/v5/swagger
type CommitsService struct {
	client *Client
}

type CommitList struct {
	PageLen int       `json:"pagelen"`
	Values  []*Commit `json:"values"`
	Next    string    `json:"next"`
}

func (s CommitList) String() string {
	return Stringify(s)
}

type Commit struct {
	Hash       string           `json:"hash"`
	Repository CommitRepository `json:"repository"`
	Links      CommitLinks      `json:"links"`
	Author     Author           `json:"author"`
	Summary    Summary          `json:"summary"`
	Parents    []Parent         `json:"parents"`
	Date       *time.Time       `json:"date"`
	Message    string           `json:"message"`
	Type       string           `json:"type"`
}

func (s Commit) String() string {
	return Stringify(s)
}

type Author struct {
	Raw  string `json:"raw"`
	Type string `json:"type"`
	User User   `json:"user"`
}

type User struct {
	Username    string    `json:"username"`
	DisplayName string    `json:"display_name"`
	AccountID   string    `json:"account_id"`
	Links       UserLinks `json:"links"`
	Type        string    `json:"type"`
	UUID        string    `json:"uuid"`
}

type UserLinks struct {
	Self   Approve `json:"self"`
	HTML   Approve `json:"html"`
	Avatar Approve `json:"avatar"`
}

type Approve struct {
	Href string `json:"href"`
}

type CommitLinks struct {
	Self     Approve `json:"self"`
	Comments Approve `json:"comments"`
	Patch    Approve `json:"patch"`
	HTML     Approve `json:"html"`
	Diff     Approve `json:"diff"`
	Approve  Approve `json:"approve"`
	Statuses Approve `json:"statuses"`
}

type Parent struct {
	Hash  string      `json:"hash"`
	Type  string      `json:"type"`
	Links ParentLinks `json:"links"`
}

type ParentLinks struct {
	Self Approve `json:"self"`
	HTML Approve `json:"html"`
}

type CommitRepository struct {
	Links    UserLinks `json:"links"`
	Type     string    `json:"type"`
	Name     string    `json:"name"`
	FullName string    `json:"full_name"`
	UUID     string    `json:"uuid"`
}

type Summary struct {
	Raw    string `json:"raw"`
	Markup string `json:"markup"`
	HTML   string `json:"html"`
	Type   string `json:"type"`
}

// ListCommitsOptions represents the available List() options.
//
// Bitbucket API docs: https://Bitbucket.com/api/v5/swagger
type ListCommitsOptions struct {
	ListOptions
	Sha    *string    `url:"sha,omitempty" json:"sha,omitempty"`
	Path   *string    `url:"path,omitempty" json:"path,omitempty"`
	Author *string    `url:"author,omitempty" json:"author,omitempty"`
	Since  *time.Time `url:"since,omitempty" json:"since,omitempty"`
	Until  *string    `url:"until,omitempty" json:"until,omitempty"`
}

// List gets a list of commits accessible by the authenticated user.
//
// Bitbucket API docs: https://Bitbucket.com/api/v5/swagger
func (s *CommitsService) List(repoFullName string, opt *ListCommitsOptions, options ...OptionFunc) (*CommitList, *Response, error) {
	u := fmt.Sprintf("repositories/%s/commits", repoFullName)
	req, err := s.client.NewRequest("GET", u, opt, options)
	if err != nil {
		return nil, nil, err
	}

	var p *CommitList
	resp, err := s.client.Do(nil, req, &p)
	if err != nil {
		return nil, resp, err
	}

	return p, resp, err
}
