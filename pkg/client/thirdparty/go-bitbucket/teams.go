//
// Copyright 2018, jtcheng
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package bitbucket

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty/go-bitbucket/types"
	"context"
	"fmt"
)

// TeamsService provides access to the team related functions
// in the Bitbucket API.
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
type TeamsService service

// TeamsListOptions specifies the optional parameters to the
// TeamsService.ListAll method.
type TeamsListOptions struct {
	Role *RoleValue `url:"role,omitempty" json:"role,omitempty"`

	ListOptions
}

// Returns all the teams that the authenticated user is associated with.
// without page
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
func (s *TeamsService) ListAll(ctx context.Context, opt *TeamsListOptions, options ...OptionFunc) (*types.PaginatedTeams, *Response, error) {
	var (
		teams    = []types.Team{}
		response *Response
	)
	if opt == nil {
		opt = &TeamsListOptions{}
	}
	opt.PageLen = DefaultMaxPagelen

	for {
		teamList, response, err := s.List(ctx, opt, options...)
		if err != nil {
			return nil, response, err
		}
		if teamList == nil {
			return nil, nil, fmt.Errorf("next page result is nil")
		}
		if teamList.Values == nil {
			return nil, nil, fmt.Errorf("next page result has no values")
		}
		teams = append(teams, teamList.Values...)
		if teamList.Next == "" {
			break
		}
	}

	return &types.PaginatedTeams{Size: int32(len(teams)), Page: 1, Pagelen: int32(len(teams)), Values: teams}, response, nil
}

// Returns all the teams that the authenticated user is associated with.
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
func (s *TeamsService) List(ctx context.Context, opt *TeamsListOptions, options ...OptionFunc) (*types.PaginatedTeams, *Response, error) {
	var u = "teams"

	req, err := s.client.NewRequest("GET", u, opt, options)
	if err != nil {
		return nil, nil, err
	}

	var teams = &types.PaginatedTeams{}
	resp, err := s.client.Do(ctx, req, teams)
	if err != nil {
		return nil, resp, err
	}

	return teams, resp, nil
}

// Gets the public information associated with a team.
// If the team's profile is private, location, website and created_on elements are omitted.
// @username The team's username or UUID.
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
func (s *TeamsService) Get(ctx context.Context, username string) (*types.Team, *Response, error) {
	u := fmt.Sprintf("teams/%s", username)
	req, err := s.client.NewRequest("GET", u, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	team := new(types.Team)
	resp, err := s.client.Do(ctx, req, team)
	if err != nil {
		return nil, resp, err
	}

	return team, resp, nil
}
