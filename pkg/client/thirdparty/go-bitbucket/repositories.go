//
// Copyright 2018, sywang
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package bitbucket

import (
	"fmt"
	"time"
)

// RepositoriesService handles communication with the repositories related methods
// of the Bitbucket API.
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
type RepositoriesService struct {
	client *Client
}

// RepositoryList represents a Bitbucket repository list paged.
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
type RepositoryList struct {
	PageLen int `json:"pagelen"`
	Size    int `json:"size"` // "size" and "page" is missed when list repository by username
	Page    int `json:"page"`

	Values []*Repository `json:"values"`
	Next   string        `json:"next"`
}

// Repository represents a Bitbucket repository.
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
type Repository struct {
	SCM         string          `json:"scm"`
	Website     string          `json:"website"`
	HasWiki     bool            `json:"has_wiki"`
	UUID        string          `json:"uuid"`
	Links       RepositoryLinks `json:"links"`
	ForkPolicy  string          `json:"fork_policy"`
	Name        string          `json:"name"`
	Project     Project         `json:"project"`
	Language    string          `json:"language"`
	CreatedOn   *time.Time      `json:"created_on"`
	Mainbranch  Mainbranch      `json:"mainbranch"`
	FullName    string          `json:"full_name"`
	HasIssues   bool            `json:"has_issues"`
	Owner       Owner           `json:"owner"`
	UpdatedOn   *time.Time      `json:"updated_on"`
	Size        int             `json:"size"`
	Type        string          `json:"type"`
	Slug        string          `json:"slug"`
	IsPrivate   bool            `json:"is_private"`
	Description string          `json:"description"`
}

func (r *Repository) GetSize() int {
	return r.Size
}

type RepositoryLinks struct {
	Watchers     Link    `json:"watchers"`
	Branches     Link    `json:"branches"`
	Tags         Link    `json:"tags"`
	Commits      Link    `json:"commits"`
	Clone        []Clone `json:"clone"`
	Self         Link    `json:"self"`
	Source       Link    `json:"source"`
	HTML         Link    `json:"html"`
	Avatar       Link    `json:"avatar"`
	Hooks        Link    `json:"hooks"`
	Forks        Link    `json:"forks"`
	Downloads    Link    `json:"downloads"`
	Issues       Link    `json:"issues"`
	PullRequests Link    `json:"pullrequests"`
}

type Link struct {
	Href string `json:"href"`
}

type Clone struct {
	Href string `json:"href"`
	Name string `json:"name"`
}

type Mainbranch struct {
	Type string `json:"type"`
	Name string `json:"name"`
}

type Owner struct {
	Username    string     `json:"username"`
	DisplayName string     `json:"display_name"`
	Type        string     `json:"type"`
	UUID        string     `json:"uuid"`
	Links       OwnerLinks `json:"links"`
}

type OwnerLinks struct {
	Self   Link `json:"self"`
	HTML   Link `json:"html"`
	Avatar Link `json:"avatar"`
}

type Project struct {
	Key   string     `json:"key"`
	Type  string     `json:"type"`
	UUID  string     `json:"uuid"`
	Links OwnerLinks `json:"links"`
	Name  string     `json:"name"`
}

func (p *Repository) GetOwnerAvatarURL() (ownerAvatarURL string) {
	return p.Owner.Links.Avatar.Href
}

func (p *Repository) GetOwnerWebURL() (ownerAvatarURL string) {
	return p.Owner.Links.HTML.Href
}

func (p *Repository) GetOwnerLogin() (login string) {
	return p.Owner.Username
}

func (p *Repository) GetCloneURL() string {
	for _, link := range p.Links.Clone {
		if link.Name == "https" {
			return link.Href
		}
	}
	return ""
}

func (p *Repository) GetSSHURL() string {
	for _, link := range p.Links.Clone {
		if link.Name == "ssh" {
			return link.Href
		}
	}
	return ""
}

type ProjectPermission struct {
	Pull  bool `json:"pull"`
	Push  bool `json:"push"`
	Admin bool `json:"admin"`
}

func (s Repository) String() string {
	return Stringify(s)
}

// ListRepositoriesOptions represents the available List() options.
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
type ListRepositoriesOptions struct {
	ListOptions
	Role *RoleValue `url:"role,omitempty" json:"role,omitempty"`
}

func (s *RepositoriesService) ListAll(user string, opt *ListRepositoriesOptions, options ...OptionFunc) ([]*Repository, *Response, error) {
	var (
		repositories = make([]*Repository, 0)
		response     *Response
	)
	if opt == nil {
		opt = &ListRepositoriesOptions{}
	}
	if user == "" {
		opt.PageLen = MaxPagelen
	} else {
		opt.PageLen = DefaultMaxPagelen
	}

	for {
		repositoryList, response, err := s.List(user, opt, options...)
		if err != nil {
			return nil, response, err
		}
		if repositoryList == nil {
			return nil, nil, fmt.Errorf("next page result is nil")
		}
		if repositoryList.Values == nil {
			return nil, nil, fmt.Errorf("next page result has no values")
		}
		repositories = append(repositories, repositoryList.Values...)
		if repositoryList.Next == "" {
			break
		}
	}

	return repositories, response, nil
}

// List gets a list of projects accessible by the user.
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
func (s *RepositoriesService) List(user string, opt *ListRepositoriesOptions, options ...OptionFunc) (*RepositoryList, *Response, error) {
	u := "repositories"
	if user != "" {
		u = fmt.Sprintf("%s/%s", u, user)
	}
	req, err := s.client.NewRequest("GET", u, opt, options)
	if err != nil {
		return nil, nil, err
	}

	var p *RepositoryList
	resp, err := s.client.Do(nil, req, &p)
	if err != nil {
		return nil, resp, err
	}

	return p, resp, err
}

// Get gets a specific project, identified by OWNER_NAME/PROJECT_NAME, which is owned by the authenticated user.
//
// Bitbucket API docs: https://developer.atlassian.com/bitbucket/api/2/reference/
func (s *RepositoriesService) Get(fullName string, options ...OptionFunc) (*Repository, *Response, error) {
	u := fmt.Sprintf("repositories/%s", fullName)

	req, err := s.client.NewRequest("GET", u, nil, options)
	if err != nil {
		return nil, nil, err
	}

	p := new(Repository)
	resp, err := s.client.Do(nil, req, p)
	if err != nil {
		return nil, resp, err
	}

	return p, resp, err
}
