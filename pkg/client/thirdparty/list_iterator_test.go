package thirdparty_test

import (
	"alauda.io/devops-apiserver/pkg/client/thirdparty"
	"context"

	"alauda.io/devops-apiserver/pkg/client/thirdparty/ace"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("StandardLister.GetFullList", func() {
	var (
		stdLister thirdparty.StandardListIterator
		listFunc  thirdparty.ListFunc
		opts      []ace.RequestOptions
		ctx       context.Context
		list      thirdparty.List
		err       error
	)

	BeforeEach(func() {
		ctx = context.TODO()
		// opts = []ace.RequestOptions{
		// 	ace.ProjectOpts("project"),
		// }
		listFunc = func(ctx context.Context, opts ...thirdparty.ListOptions) (funclist thirdparty.List, err error) {
			listObj := &ace.RoleList{
				StandardList: ace.StandardList{
					Count: 10, PageSize: 5, NumberOfPages: 2,
				},
				Items: []*ace.Role{
					&ace.Role{UUID: "1"}, &ace.Role{UUID: "2"},
					&ace.Role{UUID: "3"}, &ace.Role{UUID: "4"},
					&ace.Role{UUID: "5"},
				},
			}
			for _, o := range opts {
				if pageOpts, ok := o.(ace.PageOptions); ok {
					if pageOpts.Page > 1 {
						listObj.Items = []*ace.Role{
							&ace.Role{UUID: "6"}, &ace.Role{UUID: "7"},
							&ace.Role{UUID: "8"}, &ace.Role{UUID: "9"},
							&ace.Role{UUID: "10"},
						}
					}
				}
			}
			return listObj, nil
		}
	})

	JustBeforeEach(func() {
		list, err = stdLister.Iterate(listFunc, ctx, ace.AsListOptions(opts)...)
	})

	Context("Get two pages from a list", func() {
		It("should get successfully two pages with total 10 items", func() {
			Expect(err).To(BeNil(), "no error occurred")
			Expect(list).ToNot(BeNil(), "list is not nil")

			Expect(list.GetItems()).To(HaveLen(10), "list have 10 items")
			count, pageSize, pages := list.GetPagging()
			Expect(count).To(Equal(10), "total count is 10")
			Expect(pageSize).To(Equal(5), "page size is 5")
			Expect(pages).To(Equal(2), "total pages equals 2")

			Expect(list.GetItems()).ToNot(ContainElement(
				WithTransform(func(item interface{}) bool {
					return item == nil
				}, BeTrue()),
			), "should not cointain nil item")
		})
	})
})

var _ = Describe("RemoveDuplicatedListItems", func() {
	var (
		list   thirdparty.List
		result []thirdparty.ListItem
	)
	BeforeEach(func() {
		list = ace.RoleUserAssignmentList{
			&ace.RoleUserAssignment{
				User: "user1",
			},
			&ace.RoleUserAssignment{
				User: "user2",
			},
			&ace.RoleUserAssignment{
				User: "user1",
			},
		}
	})
	JustBeforeEach(func() {
		result = thirdparty.RemoveDuplicatedListItems(list)
	})
	It("should only leave user1 and user2", func() {
		Expect(result).To(HaveLen(2))
		Expect(result).To(ContainElement(&ace.RoleUserAssignment{User: "user1"}))
		Expect(result).To(ContainElement(&ace.RoleUserAssignment{User: "user2"}))
	})
})
