package internalversion

import (
	devops "alauda.io/devops-apiserver/pkg/client/clientset/internalversion/typed/devops/internalversion"
)

type InterfaceExpansion interface {
	Interface
	DevopsExpansion() devops.DevopsInterfaceExpansion
}

type interfaceExpansion struct {
	Interface
}

func (expansion *interfaceExpansion) DevopsExpansion() devops.DevopsInterfaceExpansion {
	return devops.NewDevopsInterfaceExpansion(expansion.Interface.Devops())
}

func NewExpansion(inter Interface) InterfaceExpansion {
	return &interfaceExpansion{
		Interface: inter,
	}
}
