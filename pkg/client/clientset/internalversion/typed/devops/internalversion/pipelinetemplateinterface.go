package internalversion

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

func (expansion *devopsInterfaceExpansion) PipelineTemplateInterface() PipelineTemplateInterfaceInterface {
	return &pipelineTemplate{
		DevopsInterface: expansion.DevopsInterface,
	}
}

func (expansion *devopsInterfaceExpansion) PipelineTaskTemplateInterface() PipelineTaskTemplateInterfaceInterface {
	return &pipelineTaskTemplate{
		DevopsInterface: expansion.DevopsInterface,
	}
}

type PipelineTemplateInterfaceGetter interface {
	PipelineTemplateInterface() PipelineTemplateInterfaceInterface
}

type PipelineTaskTemplateInterfaceGetter interface {
	PipelineTaskTemplateInterface() PipelineTaskTemplateInterfaceInterface
}

type PipelineTemplateInterfaceInterface interface {
	Create(devops.PipelineTemplateInterface) (devops.PipelineTemplateInterface, error)
	Update(devops.PipelineTemplateInterface) (devops.PipelineTemplateInterface, error)
	Delete(kind, namespace, name string, options *v1.DeleteOptions) error
	Get(kind, namespace, name string, options v1.GetOptions) (devops.PipelineTemplateInterface, error)
	Preview(kind, namespace, name string, jenkinsfilePreviewOptions *devops.JenkinsfilePreviewOptions) (*devops.JenkinsfilePreview, error)
}

type PipelineTaskTemplateInterfaceInterface interface {
	Create(devops.PipelineTaskTemplateInterface) (devops.PipelineTaskTemplateInterface, error)
	Update(devops.PipelineTaskTemplateInterface) (devops.PipelineTaskTemplateInterface, error)
	Delete(kind, namespace, name string, options *v1.DeleteOptions) error
	Get(kind, namespace, name string, options v1.GetOptions) (devops.PipelineTaskTemplateInterface, error)
}

// pipelineTemplate is a pipelinetemplate  or clusterpipelinetemplate
type pipelineTemplate struct {
	DevopsInterface DevopsInterface
}

// pipelineTaskTemplate is a pipelinetasktemplate  or clustertaskpipelinetemplate
type pipelineTaskTemplate struct {
	DevopsInterface DevopsInterface
}

func unKnowTemplateError(name, kind string) error {
	return fmt.Errorf("template '%s' is not %s", name, kind)
}

func (t *pipelineTemplate) Create(template devops.PipelineTemplateInterface) (devops.PipelineTemplateInterface, error) {
	templateName := fmt.Sprintf("%s/%s", template.GetObjectMeta().GetNamespace(), template.GetObjectMeta())

	switch template.GetKind() {
	case devops.TypeClusterPipelineTemplate:
		clusterTemplate, ok := template.(*devops.ClusterPipelineTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypeClusterPipelineTemplate)
		}
		return t.DevopsInterface.ClusterPipelineTemplates().Create(clusterTemplate)
	case devops.TypePipelineTemplate:
		ptemplate, ok := template.(*devops.PipelineTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypePipelineTemplate)
		}
		return t.DevopsInterface.PipelineTemplates(ptemplate.Namespace).Create(ptemplate)
	default:
		glog.Errorf("template %#v is not a known kind", template)
		return nil, unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTemplate) Update(template devops.PipelineTemplateInterface) (devops.PipelineTemplateInterface, error) {
	templateName := fmt.Sprintf("%s/%s", template.GetObjectMeta().GetNamespace(), template.GetObjectMeta())

	switch template.GetKind() {
	case devops.TypeClusterPipelineTemplate:
		clusterTemplate, ok := template.(*devops.ClusterPipelineTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypeClusterPipelineTemplate)
		}
		return t.DevopsInterface.ClusterPipelineTemplates().Update(clusterTemplate)
	case devops.TypePipelineTemplate:
		ptemplate, ok := template.(*devops.PipelineTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypePipelineTemplate)
		}
		return t.DevopsInterface.PipelineTemplates(ptemplate.Namespace).Update(ptemplate)
	default:
		glog.Errorf("template %#v is not a known kind", template)
		return nil, unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTemplate) Delete(kind, namespace, name string, options *v1.DeleteOptions) error {
	templateName := fmt.Sprintf("%s/%s/%s", kind, namespace, name)

	switch kind {
	case devops.TypeClusterPipelineTemplate:
		return t.DevopsInterface.ClusterPipelineTemplates().Delete(name, options)
	case devops.TypePipelineTemplate:
		return t.DevopsInterface.PipelineTemplates(namespace).Delete(name, options)
	default:
		glog.Errorf("template %#v is not a known kind", kind)
		return unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTemplate) Get(kind, namespace, name string, options v1.GetOptions) (devops.PipelineTemplateInterface, error) {

	switch kind {
	case devops.TypeClusterPipelineTemplate:
		return t.DevopsInterface.ClusterPipelineTemplates().Get(name, options)
	case devops.TypePipelineTemplate:
		return t.DevopsInterface.PipelineTemplates(namespace).Get(name, options)
	default:
		glog.Errorf("template kind %s is not a known kind", kind)
		return nil, unKnowTemplateError(kind, "a known kind")
	}
}

func (t *pipelineTemplate) Preview(kind, namespace, name string, jenkinsfilePreviewOptions *devops.JenkinsfilePreviewOptions) (*devops.JenkinsfilePreview, error) {

	switch kind {
	case devops.TypeClusterPipelineTemplate:
		return t.DevopsInterface.ClusterPipelineTemplates().Preview(name, jenkinsfilePreviewOptions)
	case devops.TypePipelineTemplate:
		return t.DevopsInterface.PipelineTemplates(namespace).Preview(name, jenkinsfilePreviewOptions)
	default:
		glog.Errorf("template kind %s is not a known kind", kind)
		return nil, unKnowTemplateError(kind, "a known kind")
	}
}

func (t *pipelineTaskTemplate) Create(template devops.PipelineTaskTemplateInterface) (devops.PipelineTaskTemplateInterface, error) {
	templateName := fmt.Sprintf("%s/%s", template.GetObjectMeta().GetNamespace(), template.GetObjectMeta())

	switch template.GetKind() {
	case devops.TypeClusterPipelineTaskTemplate:
		clusterTaskTemplate, ok := template.(*devops.ClusterPipelineTaskTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypeClusterPipelineTaskTemplate)
		}
		return t.DevopsInterface.ClusterPipelineTaskTemplates().Create(clusterTaskTemplate)
	case devops.TypePipelineTaskTemplate:
		pTasktemplate, ok := template.(*devops.PipelineTaskTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypePipelineTaskTemplate)
		}
		return t.DevopsInterface.PipelineTaskTemplates(pTasktemplate.Namespace).Create(pTasktemplate)
	default:
		glog.Errorf("template %#v is not a known kind", template)
		return nil, unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTaskTemplate) Update(template devops.PipelineTaskTemplateInterface) (devops.PipelineTaskTemplateInterface, error) {
	templateName := fmt.Sprintf("%s/%s", template.GetObjectMeta().GetNamespace(), template.GetObjectMeta())

	switch template.GetKind() {
	case devops.TypeClusterPipelineTaskTemplate:
		clusterTaskTemplate, ok := template.(*devops.ClusterPipelineTaskTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypeClusterPipelineTaskTemplate)
		}
		return t.DevopsInterface.ClusterPipelineTaskTemplates().Update(clusterTaskTemplate)
	case devops.TypePipelineTaskTemplate:
		pTasktemplate, ok := template.(*devops.PipelineTaskTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypePipelineTaskTemplate)
		}
		return t.DevopsInterface.PipelineTaskTemplates(pTasktemplate.Namespace).Update(pTasktemplate)
	default:
		glog.Errorf("template %#v is not a known kind", template)
		return nil, unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTaskTemplate) Delete(kind, namespace, name string, options *v1.DeleteOptions) error {
	templateName := fmt.Sprintf("%s/%s/%s", kind, namespace, name)

	switch kind {
	case devops.TypeClusterPipelineTaskTemplate:
		return t.DevopsInterface.ClusterPipelineTaskTemplates().Delete(name, options)
	case devops.TypePipelineTaskTemplate:
		return t.DevopsInterface.PipelineTaskTemplates(namespace).Delete(name, options)
	default:
		glog.Errorf("template %#v is not a known kind", kind)
		return unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTaskTemplate) Get(kind, namespace, name string, options v1.GetOptions) (devops.PipelineTaskTemplateInterface, error) {

	switch kind {
	case devops.TypeClusterPipelineTaskTemplate:
		return t.DevopsInterface.ClusterPipelineTaskTemplates().Get(name, options)
	case devops.TypePipelineTaskTemplate:
		return t.DevopsInterface.PipelineTaskTemplates(namespace).Get(name, options)
	default:
		glog.Errorf("template kind %s is not a known kind", kind)
		return nil, unKnowTemplateError(kind, "a known kind")
	}
}
