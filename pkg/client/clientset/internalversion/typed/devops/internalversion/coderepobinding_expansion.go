package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

// The CodeRepoBindingExpansion interface allows manually adding extra methods to the CodeRepoBindingInterface.
type CodeRepoBindingExpansion interface {
	GetRemoteRepositories(name string, opts *devops.CodeRepoBindingRepositoryOptions) (*devops.CodeRepoBindingRepositories, error)
}

// Get takes name of the pipeline, and returns the corresponding pipeline object, and an error if there is any.
func (c *codeRepoBindings) GetRemoteRepositories(name string, opts *devops.CodeRepoBindingRepositoryOptions) (result *devops.CodeRepoBindingRepositories, err error) {
	result = &devops.CodeRepoBindingRepositories{}
	err = c.client.Get().
		Namespace(c.ns).
		Name(name).
		Resource("coderepobindings").
		SubResource("remoterepositories").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
