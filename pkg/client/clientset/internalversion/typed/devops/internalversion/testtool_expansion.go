package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
	"k8s.io/apimachinery/pkg/api/errors"
)

// The TestToolExpansion interface allows manually adding extra methods to the TestToolInterface.
type TestToolExpansion interface {
	Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error)
	ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error)
	CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error)
}

// Authorize will check whether the secret is available
func (c *testTools) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	result = &devops.CodeRepoServiceAuthorizeResponse{}
	err = c.client.Get().
		Resource("testtools").
		Name(serviceName).
		SubResource("authorize").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

// ListProjects will list projects in testTools
func (c *testTools) ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("testtools"), "ListProjects")

}

// CreateProject will create project in testTools
func (c *testTools) CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("testtools"), "CreateProject")
}
