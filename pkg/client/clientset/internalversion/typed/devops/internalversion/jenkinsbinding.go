/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package internalversion

import (
	"time"

	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	scheme "alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// JenkinsBindingsGetter has a method to return a JenkinsBindingInterface.
// A group's client should implement this interface.
type JenkinsBindingsGetter interface {
	JenkinsBindings(namespace string) JenkinsBindingInterface
}

// JenkinsBindingInterface has methods to work with JenkinsBinding resources.
type JenkinsBindingInterface interface {
	Create(*devops.JenkinsBinding) (*devops.JenkinsBinding, error)
	Update(*devops.JenkinsBinding) (*devops.JenkinsBinding, error)
	UpdateStatus(*devops.JenkinsBinding) (*devops.JenkinsBinding, error)
	Delete(name string, options *v1.DeleteOptions) error
	DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error
	Get(name string, options v1.GetOptions) (*devops.JenkinsBinding, error)
	List(opts v1.ListOptions) (*devops.JenkinsBindingList, error)
	Watch(opts v1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *devops.JenkinsBinding, err error)
	Proxy(jenkinsBindingName string, jenkinsBindingProxyOptions *devops.JenkinsBindingProxyOptions) (*devops.JenkinsBindingProxyResult, error)

	JenkinsBindingExpansion
}

// jenkinsBindings implements JenkinsBindingInterface
type jenkinsBindings struct {
	client rest.Interface
	ns     string
}

// newJenkinsBindings returns a JenkinsBindings
func newJenkinsBindings(c *DevopsClient, namespace string) *jenkinsBindings {
	return &jenkinsBindings{
		client: c.RESTClient(),
		ns:     namespace,
	}
}

// Get takes name of the jenkinsBinding, and returns the corresponding jenkinsBinding object, and an error if there is any.
func (c *jenkinsBindings) Get(name string, options v1.GetOptions) (result *devops.JenkinsBinding, err error) {
	result = &devops.JenkinsBinding{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("jenkinsbindings").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of JenkinsBindings that match those selectors.
func (c *jenkinsBindings) List(opts v1.ListOptions) (result *devops.JenkinsBindingList, err error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	result = &devops.JenkinsBindingList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("jenkinsbindings").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested jenkinsBindings.
func (c *jenkinsBindings) Watch(opts v1.ListOptions) (watch.Interface, error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	opts.Watch = true
	return c.client.Get().
		Namespace(c.ns).
		Resource("jenkinsbindings").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Watch()
}

// Create takes the representation of a jenkinsBinding and creates it.  Returns the server's representation of the jenkinsBinding, and an error, if there is any.
func (c *jenkinsBindings) Create(jenkinsBinding *devops.JenkinsBinding) (result *devops.JenkinsBinding, err error) {
	result = &devops.JenkinsBinding{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("jenkinsbindings").
		Body(jenkinsBinding).
		Do().
		Into(result)
	return
}

// Update takes the representation of a jenkinsBinding and updates it. Returns the server's representation of the jenkinsBinding, and an error, if there is any.
func (c *jenkinsBindings) Update(jenkinsBinding *devops.JenkinsBinding) (result *devops.JenkinsBinding, err error) {
	result = &devops.JenkinsBinding{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("jenkinsbindings").
		Name(jenkinsBinding.Name).
		Body(jenkinsBinding).
		Do().
		Into(result)
	return
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().

func (c *jenkinsBindings) UpdateStatus(jenkinsBinding *devops.JenkinsBinding) (result *devops.JenkinsBinding, err error) {
	result = &devops.JenkinsBinding{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("jenkinsbindings").
		Name(jenkinsBinding.Name).
		SubResource("status").
		Body(jenkinsBinding).
		Do().
		Into(result)
	return
}

// Delete takes name of the jenkinsBinding and deletes it. Returns an error if one occurs.
func (c *jenkinsBindings) Delete(name string, options *v1.DeleteOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("jenkinsbindings").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *jenkinsBindings) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	var timeout time.Duration
	if listOptions.TimeoutSeconds != nil {
		timeout = time.Duration(*listOptions.TimeoutSeconds) * time.Second
	}
	return c.client.Delete().
		Namespace(c.ns).
		Resource("jenkinsbindings").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Timeout(timeout).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched jenkinsBinding.
func (c *jenkinsBindings) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *devops.JenkinsBinding, err error) {
	result = &devops.JenkinsBinding{}
	err = c.client.Patch(pt).
		Namespace(c.ns).
		Resource("jenkinsbindings").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}

// Proxy takes the representation of a jenkinsBindingProxyOptions and creates it.  Returns the server's representation of the jenkinsBindingProxyResult, and an error, if there is any.
func (c *jenkinsBindings) Proxy(jenkinsBindingName string, jenkinsBindingProxyOptions *devops.JenkinsBindingProxyOptions) (result *devops.JenkinsBindingProxyResult, err error) {
	result = &devops.JenkinsBindingProxyResult{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("jenkinsbindings").
		Name(jenkinsBindingName).
		SubResource("proxy").
		Body(jenkinsBindingProxyOptions).
		Do().
		Into(result)
	return
}
