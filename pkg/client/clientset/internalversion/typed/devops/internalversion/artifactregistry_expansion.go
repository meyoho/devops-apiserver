package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
	glog "k8s.io/klog"
)

//ArtifactRegistryManagerExpansion expasion of ProjectManagement client
type ArtifactRegistryExpansion interface {
	Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error)
	GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error)
	ApplyRoleMapping(serviceName string, opts *devops.RoleMapping) (result *devops.RoleMapping, err error)
}

// Authorize will check whether the secret is available
func (c *artifactRegistries) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	result = &devops.CodeRepoServiceAuthorizeResponse{}
	glog.V(9).Infof("arm authorize opts is %#v", opts)
	err = c.client.Get().
		Resource("artifactregistries").
		Name(serviceName).
		SubResource("authorize").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}
func (p *artifactRegistries) GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error) {
	result = &devops.RoleMapping{}
	err = p.client.Get().
		Resource("artifactRegistries").
		Name(serviceName).
		SubResource("roles").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

func (p *artifactRegistries) ApplyRoleMapping(serviceName string, opts *devops.RoleMapping) (result *devops.RoleMapping, err error) {
	result = &devops.RoleMapping{}
	err = p.client.Post().
		Resource("artifactRegistries").
		Name(serviceName).
		SubResource("roles").
		Body(opts).
		Do().
		Into(result)
	return
}
