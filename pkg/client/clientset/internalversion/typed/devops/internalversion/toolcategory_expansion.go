package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"encoding/json"
	log "k8s.io/klog"
)

//ArtifactRegistryManagerExpansion expasion of artifactregistry manager client
type ToolCategoryExpansion interface {
	Setting() (result *devops.SettingResp, err error)
}

//get settings
func (c *toolCategories) Setting() (result *devops.SettingResp, err error) {
	result = new(devops.SettingResp)

	r := c.client.Get().
		Resource("toolcategories").
		Name("all").
		SubResource("settings").
		Do()

	//使用Into方法无法正常赋值，原因待查

	raw, err := r.Raw()
	if err != nil {
		log.Errorf("settings err is %#v", err)
		return
	}
	err = json.Unmarshal(raw, result)
	if err != nil {
		log.Errorf("settings err is %#v", err)
		return
	}
	log.V(9).Infof("Setting resutl is %#v", result)

	return
}
