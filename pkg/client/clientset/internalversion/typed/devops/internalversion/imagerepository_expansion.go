package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
)

// ImageRepositoryExpansion expansion methods for client
type ImageRepositoryExpansion interface {
	GetVulnerability(name string, opts *devops.ImageScanOptions) (result *devops.VulnerabilityList, err error)
	GetImageTags(name string, opts *devops.ImageTagOptions) (result *devops.ImageTagResult, err error)
	Remote(name string, opts *devops.ImageRepositoryOptions) (result *devops.ImageRepositoryRemoteStatus, err error)
	Link(name string, opts *devops.ImageRepositoryOptions) (result *devops.ImageRepositoryLink, err error)
}

// Get scan image vulnerability with repository and name, return the vulnerability list
func (c *imageRepositories) GetVulnerability(name string, opts *devops.ImageScanOptions) (result *devops.VulnerabilityList, err error) {
	result = &devops.VulnerabilityList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imagerepositories").
		Name(name).
		SubResource("security").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *imageRepositories) GetImageTags(name string, opts *devops.ImageTagOptions) (result *devops.ImageTagResult, err error) {
	result = &devops.ImageTagResult{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imagerepositories").
		Name(name).
		SubResource("tags").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *imageRepositories) Remote(name string, opts *devops.ImageRepositoryOptions) (result *devops.ImageRepositoryRemoteStatus, err error) {
	result = &devops.ImageRepositoryRemoteStatus{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imagerepositories").
		Name(name).
		SubResource("remote").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *imageRepositories) Link(name string, opts *devops.ImageRepositoryOptions) (result *devops.ImageRepositoryLink, err error) {
	result = &devops.ImageRepositoryLink{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imagerepositories").
		Name(name).
		SubResource("link").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
