package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

// The ImageRegistryExpansion interface allows manually adding extra methods to the ImageRegistryInterface.
type ImageRegistryExpansion interface {
	Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error)
	GetImageRepos(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.ImageRegistryBindingRepositories, error)
	ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error)
	CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error)
}

// Authorize will check whether the secret is available
func (c *imageRegistries) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	result = &devops.CodeRepoServiceAuthorizeResponse{}
	err = c.client.Get().
		Resource("imageregistries").
		Name(serviceName).
		SubResource("authorize").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

// GetImageRepos will list remote image repositories
func (c *imageRegistries) GetImageRepos(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.ImageRegistryBindingRepositories, err error) {
	result = &devops.ImageRegistryBindingRepositories{}
	err = c.client.Get().
		Resource("imageregistries").
		Name(serviceName).
		SubResource("repositories").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

// ListProjects will list projects in imageRegistries
func (c *imageRegistries) ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	result = &devops.ProjectDataList{}
	err = c.client.Get().
		Resource("imageregistries").
		Name(serviceName).
		SubResource("projects").
		VersionedParams(&opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// CreateProject will create project in imageRegistrie
func (c *imageRegistries) CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	result = &devops.ProjectData{}
	err = c.client.Post().
		Resource("imageregistries").
		Name(serviceName).
		SubResource("projects").
		Body(opts).
		Do().
		Into(result)
	return
}
