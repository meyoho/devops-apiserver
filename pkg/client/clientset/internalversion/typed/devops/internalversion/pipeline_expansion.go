package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	scheme "alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

// PipelineExpansion expansion methods for client
type PipelineExpansion interface {
	GetLogs(name string, opts *devops.PipelineLogOptions) (*devops.PipelineLog, error)
	GetTasks(name string, opts *devops.PipelineTaskOptions) (result *devops.PipelineTask, err error)
	GetTestReports(name string, opts *devops.PipelineTestReportOptions) (result *devops.PipelineTestReport, err error)
	GetView(name string) (*devops.PipelineViewResult, error)
}

// GetLogs takes name of the pipeline, and returns the corresponding pipeline object, and an error if there is any.
func (c *pipelines) GetLogs(name string, opts *devops.PipelineLogOptions) (result *devops.PipelineLog, err error) {
	result = &devops.PipelineLog{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("logs").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// GetTasks takes name of the pipeline and PipelineTaskOptions, and returns the corresponding pipeline object, and an error if there is any.
func (c *pipelines) GetTasks(name string, opts *devops.PipelineTaskOptions) (result *devops.PipelineTask, err error) {
	result = &devops.PipelineTask{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("tasks").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *pipelines) GetTestReports(name string, opts *devops.PipelineTestReportOptions) (result *devops.PipelineTestReport, err error) {
	result = &devops.PipelineTestReport{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("testreports").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *pipelines) GetView(name string) (*devops.PipelineViewResult, error) {
	result := &devops.PipelineViewResult{}
	err := c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("view").
		Do().
		Into(result)
	return result, err
}
