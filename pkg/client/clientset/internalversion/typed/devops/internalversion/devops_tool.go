package internalversion

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

type DevopsInterfaceExpansion interface {
	DevopsInterface
	DevOpsToolGetter
	PipelineTemplateInterfaceGetter
	PipelineTaskTemplateInterfaceGetter
}

func NewDevopsInterfaceExpansion(interf DevopsInterface) DevopsInterfaceExpansion {
	return &devopsInterfaceExpansion{
		DevopsInterface: interf,
	}
}

type devopsInterfaceExpansion struct {
	DevopsInterface
	DevOpsToolGetter
	PipelineTemplateInterfaceGetter
	PipelineTaskTemplateInterfaceGetter
}

type DevOpsToolGetter interface {
	DevOpsTool() DevOpsToolInterface
}

type DevOpsToolInterface interface {
	Create(devops.ToolInterface) (devops.ToolInterface, error)
	Update(devops.ToolInterface) (devops.ToolInterface, error)
	UpdateStatus(devops.ToolInterface) (devops.ToolInterface, error)
	Delete(kind, name string, options *metav1.DeleteOptions) error
	Get(kind, name string, options metav1.GetOptions) (devops.ToolInterface, error)

	Authorize(kind string, name string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error)

	CreateProject(kind string, name string, opts *devops.CreateProjectOptions) (*devops.ProjectData, error)
	ListProjects(kind string, name string, opts devops.ListProjectOptions) (*devops.ProjectDataList, error)
}

func (expansion *devopsInterfaceExpansion) DevOpsTool() DevOpsToolInterface {
	return &devopsTool{
		DevopsInterface: expansion.DevopsInterface,
	}
}

var _ DevOpsToolInterface = &devopsTool{}

type devopsTool struct {
	DevopsInterface DevopsInterface
}

func unKnowKindError(toolString, kind string) error {
	return fmt.Errorf("tool '%s' is not %s", toolString, kind)
}

func (devopsTool *devopsTool) Create(tool devops.ToolInterface) (devops.ToolInterface, error) {

	toolName := fmt.Sprintf("%s/%s", tool.GetObjectMeta().GetNamespace(), tool.GetObjectMeta())
	switch tool.GetKind() {
	case devops.TypeCodeRepoService:
		codeRepoService, ok := tool.(*devops.CodeRepoService)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeRepoService)
		}
		return devopsTool.DevopsInterface.CodeRepoServices().Create(codeRepoService)
	case devops.TypeJenkins:
		jenkins, ok := tool.(*devops.Jenkins)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeJenkins)
		}
		return devopsTool.DevopsInterface.Jenkinses().Create(jenkins)
	case devops.TypeDocumentManagement:
		documentManagement, ok := tool.(*devops.DocumentManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeDocumentManagement)
		}
		return devopsTool.DevopsInterface.DocumentManagements().Create(documentManagement)
	case devops.TypeProjectManagement:
		projectManagement, ok := tool.(*devops.ProjectManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeProjectManagement)
		}
		return devopsTool.DevopsInterface.ProjectManagements().Create(projectManagement)
	case devops.TypeCodeQualityTool:
		codeQualityTool, ok := tool.(*devops.CodeQualityTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeQualityTool)
		}
		return devopsTool.DevopsInterface.CodeQualityTools().Create(codeQualityTool)
	case devops.TypeImageRegistry:
		imageRegistry, ok := tool.(*devops.ImageRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeImageRegistry)
		}
		return devopsTool.DevopsInterface.ImageRegistries().Create(imageRegistry)
	case devops.TypeTestTool:
		testTool, ok := tool.(*devops.TestTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeTestTool)
		}
		return devopsTool.DevopsInterface.TestTools().Create(testTool)
	case devops.TypeArtifactRegistryManager:
		arm, ok := tool.(*devops.ArtifactRegistryManager)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistryManager)
		}
		return devopsTool.DevopsInterface.ArtifactRegistryManagers().Create(arm)
	case devops.TypeArtifactRegistry:
		ar, ok := tool.(*devops.ArtifactRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistry)
		}
		return devopsTool.DevopsInterface.ArtifactRegistries().Create(ar)
	default:
		glog.Errorf("tool %#v is not a known kind", tool)
		return nil, unKnowKindError(toolName, "a known kind")
	}
}

func (devopsTool *devopsTool) Update(tool devops.ToolInterface) (devops.ToolInterface, error) {
	toolName := fmt.Sprintf("%s/%s", tool.GetObjectMeta().GetNamespace(), tool.GetObjectMeta())

	switch tool.GetKind() {
	case devops.TypeCodeRepoService:
		codeRepoService, ok := tool.(*devops.CodeRepoService)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeRepoService)
		}
		return devopsTool.DevopsInterface.CodeRepoServices().Update(codeRepoService)
	case devops.TypeJenkins:
		jenkins, ok := tool.(*devops.Jenkins)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeJenkins)
		}
		return devopsTool.DevopsInterface.Jenkinses().Update(jenkins)
	case devops.TypeDocumentManagement:
		documentManagement, ok := tool.(*devops.DocumentManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeDocumentManagement)
		}
		return devopsTool.DevopsInterface.DocumentManagements().Update(documentManagement)
	case devops.TypeProjectManagement:
		projectManagement, ok := tool.(*devops.ProjectManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeProjectManagement)
		}
		return devopsTool.DevopsInterface.ProjectManagements().Update(projectManagement)
	case devops.TypeCodeQualityTool:
		codeQualityTool, ok := tool.(*devops.CodeQualityTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeQualityTool)
		}
		return devopsTool.DevopsInterface.CodeQualityTools().Update(codeQualityTool)
	case devops.TypeImageRegistry:
		imageRegistry, ok := tool.(*devops.ImageRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeImageRegistry)
		}
		return devopsTool.DevopsInterface.ImageRegistries().Update(imageRegistry)
	case devops.TypeTestTool:
		testTool, ok := tool.(*devops.TestTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeTestTool)
		}
		return devopsTool.DevopsInterface.TestTools().Update(testTool)
	case devops.TypeArtifactRegistryManager:
		arm, ok := tool.(*devops.ArtifactRegistryManager)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistryManager)
		}
		return devopsTool.DevopsInterface.ArtifactRegistryManagers().Update(arm)
	case devops.TypeArtifactRegistry:
		ar, ok := tool.(*devops.ArtifactRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistry)
		}
		return devopsTool.DevopsInterface.ArtifactRegistries().Update(ar)
	default:
		glog.Errorf("tool %#v is not a known kind", tool)
		return nil, unKnowKindError(toolName, "a known kind")
	}
}
func (devopsTool *devopsTool) UpdateStatus(tool devops.ToolInterface) (devops.ToolInterface, error) {
	toolName := fmt.Sprintf("%s/%s", tool.GetObjectMeta().GetNamespace(), tool.GetObjectMeta())

	switch tool.GetKind() {
	case devops.TypeCodeRepoService:
		codeRepoService, ok := tool.(*devops.CodeRepoService)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeRepoService)
		}
		return devopsTool.DevopsInterface.CodeRepoServices().UpdateStatus(codeRepoService)
	case devops.TypeJenkins:
		jenkins, ok := tool.(*devops.Jenkins)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeJenkins)
		}
		return devopsTool.DevopsInterface.Jenkinses().UpdateStatus(jenkins)
	case devops.TypeDocumentManagement:
		documentManagement, ok := tool.(*devops.DocumentManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeDocumentManagement)
		}
		return devopsTool.DevopsInterface.DocumentManagements().UpdateStatus(documentManagement)
	case devops.TypeProjectManagement:
		projectManagement, ok := tool.(*devops.ProjectManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeProjectManagement)
		}
		return devopsTool.DevopsInterface.ProjectManagements().UpdateStatus(projectManagement)
	case devops.TypeCodeQualityTool:
		codeQualityTool, ok := tool.(*devops.CodeQualityTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeQualityTool)
		}
		return devopsTool.DevopsInterface.CodeQualityTools().UpdateStatus(codeQualityTool)
	case devops.TypeImageRegistry:
		imageRegistry, ok := tool.(*devops.ImageRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeImageRegistry)
		}
		return devopsTool.DevopsInterface.ImageRegistries().UpdateStatus(imageRegistry)
	case devops.TypeTestTool:
		testTool, ok := tool.(*devops.TestTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeTestTool)
		}
		return devopsTool.DevopsInterface.TestTools().UpdateStatus(testTool)
	case devops.TypeArtifactRegistryManager:
		arm, ok := tool.(*devops.ArtifactRegistryManager)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistryManager)
		}
		return devopsTool.DevopsInterface.ArtifactRegistryManagers().UpdateStatus(arm)
	case devops.TypeArtifactRegistry:
		ar, ok := tool.(*devops.ArtifactRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistry)
		}
		return devopsTool.DevopsInterface.ArtifactRegistries().UpdateStatus(ar)
	default:
		glog.Errorf("tool %#v is not a known kind", tool)
		return nil, unKnowKindError(toolName, "a known kind")
	}
}

func notImplementError(kind, verb string) error {
	return errors.NewMethodNotSupported(devops.Resource(kind), verb)
}

func (devopsTool *devopsTool) Delete(kind, name string, options *metav1.DeleteOptions) error {
	switch kind {
	case devops.TypeCodeRepoService:
		return devopsTool.DevopsInterface.CodeRepoServices().Delete(name, options)
	case devops.TypeJenkins:
		return devopsTool.DevopsInterface.Jenkinses().Delete(name, options)
	case devops.TypeDocumentManagement:
		return devopsTool.DevopsInterface.DocumentManagements().Delete(name, options)
	case devops.TypeProjectManagement:
		return devopsTool.DevopsInterface.ProjectManagements().Delete(name, options)
	case devops.TypeCodeQualityTool:
		return devopsTool.DevopsInterface.CodeQualityTools().Delete(name, options)
	case devops.TypeImageRegistry:
		return devopsTool.DevopsInterface.ImageRegistries().Delete(name, options)
	case devops.TypeTestTool:
		return devopsTool.DevopsInterface.TestTools().Delete(name, options)
	case devops.TypeArtifactRegistryManager:
		return devopsTool.DevopsInterface.ArtifactRegistryManagers().Delete(name, options)
	case devops.TypeArtifactRegistry:
		return devopsTool.DevopsInterface.ArtifactRegistries().Delete(name, options)
	default:
		glog.Errorf("tool kind '%s' is not a known kind", kind)
		return unKnowKindError(kind, "a known kind")
	}
}
func (devopsTool *devopsTool) Get(kind, name string, options metav1.GetOptions) (devops.ToolInterface, error) {

	switch kind {
	case devops.TypeCodeRepoService:
		return devopsTool.DevopsInterface.CodeRepoServices().Get(name, options)
	case devops.TypeJenkins:
		return devopsTool.DevopsInterface.Jenkinses().Get(name, options)
	case devops.TypeDocumentManagement:
		return devopsTool.DevopsInterface.DocumentManagements().Get(name, options)
	case devops.TypeProjectManagement:
		return devopsTool.DevopsInterface.ProjectManagements().Get(name, options)
	case devops.TypeCodeQualityTool:
		return devopsTool.DevopsInterface.CodeQualityTools().Get(name, options)
	case devops.TypeImageRegistry:
		return devopsTool.DevopsInterface.ImageRegistries().Get(name, options)
	case devops.TypeTestTool:
		return devopsTool.DevopsInterface.TestTools().Get(name, options)
	case devops.TypeArtifactRegistryManager:
		return devopsTool.DevopsInterface.ArtifactRegistryManagers().Get(name, options)
	case devops.TypeArtifactRegistry:
		return devopsTool.DevopsInterface.ArtifactRegistries().Get(name, options)
	default:
		return nil, unKnowKindError(kind, "a known kind")
	}
}

func (devopsTool *devopsTool) Authorize(kind string, name string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error) {

	switch kind {
	case devops.TypeCodeRepoService:
		return devopsTool.DevopsInterface.CodeRepoServices().Authorize(name, opts)
	case devops.TypeJenkins:
		return devopsTool.DevopsInterface.Jenkinses().Authorize(name, opts)
	case devops.TypeDocumentManagement:
		return devopsTool.DevopsInterface.DocumentManagements().Authorize(name, opts)
	case devops.TypeProjectManagement:
		return devopsTool.DevopsInterface.ProjectManagements().Authorize(name, opts)
	case devops.TypeCodeQualityTool:
		return devopsTool.DevopsInterface.CodeQualityTools().Authorize(name, opts)
	case devops.TypeImageRegistry:
		return devopsTool.DevopsInterface.ImageRegistries().Authorize(name, opts)
	case devops.TypeTestTool:
		return devopsTool.DevopsInterface.TestTools().Authorize(name, opts)
	case devops.TypeArtifactRegistryManager:
		return devopsTool.DevopsInterface.ArtifactRegistryManagers().Authorize(name, opts)
	case devops.TypeArtifactRegistry:
		return devopsTool.DevopsInterface.ArtifactRegistries().Authorize(name, opts)
	default:
		return nil, unKnowKindError(kind, "a known kind")
	}
}

func (devopsTool *devopsTool) CreateProject(kind string, name string, opts *devops.CreateProjectOptions) (*devops.ProjectData, error) {
	switch kind {
	case devops.TypeCodeRepoService:
		return devopsTool.DevopsInterface.CodeRepoServices().CreateProject(name, opts)
	case devops.TypeJenkins:
		return devopsTool.DevopsInterface.Jenkinses().CreateProject(name, opts)
	case devops.TypeDocumentManagement:
		return devopsTool.DevopsInterface.DocumentManagements().CreateProject(name, opts)
	case devops.TypeProjectManagement:
		return devopsTool.DevopsInterface.ProjectManagements().CreateProject(name, opts)
	case devops.TypeCodeQualityTool:
		return devopsTool.DevopsInterface.CodeQualityTools().CreateProject(name, opts)
	case devops.TypeImageRegistry:
		return devopsTool.DevopsInterface.ImageRegistries().CreateProject(name, opts)
	case devops.TypeTestTool:
		return devopsTool.DevopsInterface.TestTools().CreateProject(name, opts)
	default:
		return nil, unKnowKindError(kind, "a known kind")
	}
}

func (devopsTool *devopsTool) ListProjects(kind string, name string, opts devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	switch kind {
	case devops.TypeCodeRepoService:
		return devopsTool.DevopsInterface.CodeRepoServices().ListProjects(name, opts)
	case devops.TypeJenkins:
		return devopsTool.DevopsInterface.Jenkinses().ListProjects(name, opts)
	case devops.TypeDocumentManagement:
		return devopsTool.DevopsInterface.DocumentManagements().ListProjects(name, opts)
	case devops.TypeProjectManagement:
		return devopsTool.DevopsInterface.ProjectManagements().ListProjects(name, opts)
	case devops.TypeCodeQualityTool:
		return devopsTool.DevopsInterface.CodeQualityTools().ListProjects(name, opts)
	case devops.TypeImageRegistry:
		return devopsTool.DevopsInterface.ImageRegistries().ListProjects(name, opts)
	case devops.TypeTestTool:
		return devopsTool.DevopsInterface.TestTools().ListProjects(name, opts)
	default:
		return nil, unKnowKindError(kind, "a known kind")
	}
}
