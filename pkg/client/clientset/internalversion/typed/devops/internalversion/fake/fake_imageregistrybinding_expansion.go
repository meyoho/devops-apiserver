package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	core "k8s.io/client-go/testing"
)

// GetImageRepos adding fake remote repository list method for testing
func (c *FakeImageRegistryBindings) GetImageRepos(name string) (result *devops.ImageRegistryBindingRepositories, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = imageregistrybindingsResource
	action.Subresource = "repositories"
	obj, err := c.Fake.Invokes(action, &devops.ImageRegistryBindingRepositories{})
	result = obj.(*devops.ImageRegistryBindingRepositories)
	return result, err
}

// CreateImageProject adding fake create project method for testing
func (c *FakeImageRegistryBindings) CreateImageProject(name string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "create"
	action.Namespace = c.ns
	action.Resource = imageregistrybindingsResource
	action.Subresource = "projects"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectData{})
	result = obj.(*devops.ProjectData)
	return result, err
}

// GetImageProjects adding fake get project list method for testing
func (c *FakeImageRegistryBindings) GetImageProjects(name string, opts *devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = imageregistrybindingsResource
	action.Subresource = "projects"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectDataList{})
	result = obj.(*devops.ProjectDataList)
	return
}
