/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakePipelineTaskTemplates implements PipelineTaskTemplateInterface
type FakePipelineTaskTemplates struct {
	Fake *FakeDevops
	ns   string
}

var pipelinetasktemplatesResource = schema.GroupVersionResource{Group: "devops.alauda.io", Version: "", Resource: "pipelinetasktemplates"}

var pipelinetasktemplatesKind = schema.GroupVersionKind{Group: "devops.alauda.io", Version: "", Kind: "PipelineTaskTemplate"}

// Get takes name of the pipelineTaskTemplate, and returns the corresponding pipelineTaskTemplate object, and an error if there is any.
func (c *FakePipelineTaskTemplates) Get(name string, options v1.GetOptions) (result *devops.PipelineTaskTemplate, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(pipelinetasktemplatesResource, c.ns, name), &devops.PipelineTaskTemplate{})

	if obj == nil {
		return nil, err
	}
	return obj.(*devops.PipelineTaskTemplate), err
}

// List takes label and field selectors, and returns the list of PipelineTaskTemplates that match those selectors.
func (c *FakePipelineTaskTemplates) List(opts v1.ListOptions) (result *devops.PipelineTaskTemplateList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(pipelinetasktemplatesResource, pipelinetasktemplatesKind, c.ns, opts), &devops.PipelineTaskTemplateList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &devops.PipelineTaskTemplateList{ListMeta: obj.(*devops.PipelineTaskTemplateList).ListMeta}
	for _, item := range obj.(*devops.PipelineTaskTemplateList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested pipelineTaskTemplates.
func (c *FakePipelineTaskTemplates) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(pipelinetasktemplatesResource, c.ns, opts))

}

// Create takes the representation of a pipelineTaskTemplate and creates it.  Returns the server's representation of the pipelineTaskTemplate, and an error, if there is any.
func (c *FakePipelineTaskTemplates) Create(pipelineTaskTemplate *devops.PipelineTaskTemplate) (result *devops.PipelineTaskTemplate, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(pipelinetasktemplatesResource, c.ns, pipelineTaskTemplate), &devops.PipelineTaskTemplate{})

	if obj == nil {
		return nil, err
	}
	return obj.(*devops.PipelineTaskTemplate), err
}

// Update takes the representation of a pipelineTaskTemplate and updates it. Returns the server's representation of the pipelineTaskTemplate, and an error, if there is any.
func (c *FakePipelineTaskTemplates) Update(pipelineTaskTemplate *devops.PipelineTaskTemplate) (result *devops.PipelineTaskTemplate, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(pipelinetasktemplatesResource, c.ns, pipelineTaskTemplate), &devops.PipelineTaskTemplate{})

	if obj == nil {
		return nil, err
	}
	return obj.(*devops.PipelineTaskTemplate), err
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().
func (c *FakePipelineTaskTemplates) UpdateStatus(pipelineTaskTemplate *devops.PipelineTaskTemplate) (*devops.PipelineTaskTemplate, error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateSubresourceAction(pipelinetasktemplatesResource, "status", c.ns, pipelineTaskTemplate), &devops.PipelineTaskTemplate{})

	if obj == nil {
		return nil, err
	}
	return obj.(*devops.PipelineTaskTemplate), err
}

// Delete takes name of the pipelineTaskTemplate and deletes it. Returns an error if one occurs.
func (c *FakePipelineTaskTemplates) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(pipelinetasktemplatesResource, c.ns, name), &devops.PipelineTaskTemplate{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakePipelineTaskTemplates) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(pipelinetasktemplatesResource, c.ns, listOptions)

	_, err := c.Fake.Invokes(action, &devops.PipelineTaskTemplateList{})
	return err
}

// Patch applies the patch and returns the patched pipelineTaskTemplate.
func (c *FakePipelineTaskTemplates) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *devops.PipelineTaskTemplate, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(pipelinetasktemplatesResource, c.ns, name, pt, data, subresources...), &devops.PipelineTaskTemplate{})

	if obj == nil {
		return nil, err
	}
	return obj.(*devops.PipelineTaskTemplate), err
}
