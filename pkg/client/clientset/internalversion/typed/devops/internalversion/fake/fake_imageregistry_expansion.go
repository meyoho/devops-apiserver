package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	core "k8s.io/client-go/testing"
)

// Authorize adding fake method for testing
func (c *FakeImageRegistries) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = imageregistriesResource
	action.Subresource = "authorize"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoServiceAuthorizeResponse{})
	result = obj.(*devops.CodeRepoServiceAuthorizeResponse)
	return result, err
}

func (c *FakeImageRegistries) GetImageRepos(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.ImageRegistryBindingRepositories, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = imageregistriesResource
	action.Subresource = "repositories"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoServiceAuthorizeOptions{})
	result = obj.(*devops.ImageRegistryBindingRepositories)
	return result, err
}

func (c *FakeImageRegistries) ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = imageregistriesResource
	action.Subresource = "projects"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectDataList{})
	result = obj.(*devops.ProjectDataList)
	return result, err
}

func (c *FakeImageRegistries) CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "create"
	action.Resource = imageregistriesResource
	action.Subresource = "projects"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectData{})
	result = obj.(*devops.ProjectData)
	return result, err
}
