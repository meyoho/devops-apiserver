package fake

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	testing "k8s.io/client-go/testing"
)

func (c *FakeCodeQualityProjects) Remote(serviceName string, opts *devops.CodeQualityProjectOptions) (*devops.CodeQualityProjectRemoteStatus, error) {
	action := testing.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = codequalityprojectsResource
	action.Subresource = "remote"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeQualityProjectRemoteStatus{})
	return obj.(*devops.CodeQualityProjectRemoteStatus), err
}

func (c *FakeCodeQualityProjects) Reports(serviceName string, opts *devops.CodeQualityProjectOptions) (*devops.CodeQualityProjectReportsCondition, error) {
	action := testing.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = codequalityprojectsResource
	action.Subresource = "reports"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeQualityProjectReportsCondition{})
	return obj.(*devops.CodeQualityProjectReportsCondition), err
}

func (c *FakeCodeQualityProjects) LastAnalysisDate(serviceName string, opts *devops.CodeQualityProjectOptions) (*devops.CodeQualityProjectLastAnalysisDate, error) {
	action := testing.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = codequalityprojectsResource
	action.Subresource = "date"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeQualityProjectLastAnalysisDate{})
	return obj.(*devops.CodeQualityProjectLastAnalysisDate), err
}
