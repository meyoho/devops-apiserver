package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	core "k8s.io/client-go/testing"
)

type JenkinsBindingExpansion interface {
	GetJenkinsInfo(jenkinsBindingName string, options *devops.JenkinsBindingInfoOptions) (*devops.JenkinsBindingInfoResult, error)
}

func (c *FakeJenkinsBindings) GetJenkinsInfo(jenkinsBindingName string, options *devops.JenkinsBindingInfoOptions) (*devops.JenkinsBindingInfoResult, error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = jenkinsbindingsResource
	action.Subresource = "info"
	action.Value = options
	obj, err := c.Fake.Invokes(action, &devops.JenkinsBindingInfoResult{})
	result := obj.(*devops.JenkinsBindingInfoResult)

	return result, err
}
