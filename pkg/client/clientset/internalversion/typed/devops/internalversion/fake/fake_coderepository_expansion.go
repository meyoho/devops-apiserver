package fake

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	core "k8s.io/client-go/testing"
)

// GetBranches adding fake get branches vulnerability for testing
func (c *FakeCodeRepositories) GetBranches(name string, opts *devops.CodeRepoBranchOptions) (result *devops.CodeRepoBranchResult, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = coderepositoriesResource
	action.Subresource = "branches"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoBranchResult{})
	result = obj.(*devops.CodeRepoBranchResult)
	return result, err
}
