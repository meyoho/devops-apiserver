package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/client-go/testing"
)

func (c *FakePipelineTemplates) Exports(name string, opts *v1alpha1.ExportShowOptions) (result *v1alpha1.PipelineExportedVariables, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetSubresourceAction(pipelinetemplatesResource, c.ns, "exports", name), &v1alpha1.PipelineExportedVariables{})

	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.PipelineExportedVariables), err
}
