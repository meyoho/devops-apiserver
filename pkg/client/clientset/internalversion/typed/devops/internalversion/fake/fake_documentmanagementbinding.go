/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeDocumentManagementBindings implements DocumentManagementBindingInterface
type FakeDocumentManagementBindings struct {
	Fake *FakeDevops
	ns   string
}

var documentmanagementbindingsResource = schema.GroupVersionResource{Group: "devops.alauda.io", Version: "", Resource: "documentmanagementbindings"}

var documentmanagementbindingsKind = schema.GroupVersionKind{Group: "devops.alauda.io", Version: "", Kind: "DocumentManagementBinding"}

// Get takes name of the documentManagementBinding, and returns the corresponding documentManagementBinding object, and an error if there is any.
func (c *FakeDocumentManagementBindings) Get(name string, options v1.GetOptions) (result *devops.DocumentManagementBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(documentmanagementbindingsResource, c.ns, name), &devops.DocumentManagementBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*devops.DocumentManagementBinding), err
}

// List takes label and field selectors, and returns the list of DocumentManagementBindings that match those selectors.
func (c *FakeDocumentManagementBindings) List(opts v1.ListOptions) (result *devops.DocumentManagementBindingList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(documentmanagementbindingsResource, documentmanagementbindingsKind, c.ns, opts), &devops.DocumentManagementBindingList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &devops.DocumentManagementBindingList{ListMeta: obj.(*devops.DocumentManagementBindingList).ListMeta}
	for _, item := range obj.(*devops.DocumentManagementBindingList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested documentManagementBindings.
func (c *FakeDocumentManagementBindings) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(documentmanagementbindingsResource, c.ns, opts))

}

// Create takes the representation of a documentManagementBinding and creates it.  Returns the server's representation of the documentManagementBinding, and an error, if there is any.
func (c *FakeDocumentManagementBindings) Create(documentManagementBinding *devops.DocumentManagementBinding) (result *devops.DocumentManagementBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(documentmanagementbindingsResource, c.ns, documentManagementBinding), &devops.DocumentManagementBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*devops.DocumentManagementBinding), err
}

// Update takes the representation of a documentManagementBinding and updates it. Returns the server's representation of the documentManagementBinding, and an error, if there is any.
func (c *FakeDocumentManagementBindings) Update(documentManagementBinding *devops.DocumentManagementBinding) (result *devops.DocumentManagementBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(documentmanagementbindingsResource, c.ns, documentManagementBinding), &devops.DocumentManagementBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*devops.DocumentManagementBinding), err
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().
func (c *FakeDocumentManagementBindings) UpdateStatus(documentManagementBinding *devops.DocumentManagementBinding) (*devops.DocumentManagementBinding, error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateSubresourceAction(documentmanagementbindingsResource, "status", c.ns, documentManagementBinding), &devops.DocumentManagementBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*devops.DocumentManagementBinding), err
}

// Delete takes name of the documentManagementBinding and deletes it. Returns an error if one occurs.
func (c *FakeDocumentManagementBindings) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(documentmanagementbindingsResource, c.ns, name), &devops.DocumentManagementBinding{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeDocumentManagementBindings) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(documentmanagementbindingsResource, c.ns, listOptions)

	_, err := c.Fake.Invokes(action, &devops.DocumentManagementBindingList{})
	return err
}

// Patch applies the patch and returns the patched documentManagementBinding.
func (c *FakeDocumentManagementBindings) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *devops.DocumentManagementBinding, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(documentmanagementbindingsResource, c.ns, name, pt, data, subresources...), &devops.DocumentManagementBinding{})

	if obj == nil {
		return nil, err
	}
	return obj.(*devops.DocumentManagementBinding), err
}
