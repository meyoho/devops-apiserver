package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	core "k8s.io/client-go/testing"
)

// GetVulnerability adding fake get image vulnerability for testing
func (c *FakeImageRepositories) GetVulnerability(name string, opts *devops.ImageScanOptions) (result *devops.VulnerabilityList, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = imagerepositoriesResource
	action.Subresource = "security"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.VulnerabilityList{})
	result = obj.(*devops.VulnerabilityList)
	return result, err
}

// GetImageTags adding fake get image vulnerability for testing
func (c *FakeImageRepositories) GetImageTags(name string, opts *devops.ImageTagOptions) (result *devops.ImageTagResult, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = imagerepositoriesResource
	action.Subresource = "tags"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ImageTagResult{})
	result = obj.(*devops.ImageTagResult)
	return result, err
}

// Authorize adding fake method for testing
func (c *FakeImageRepositories) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = imageregistriesResource
	action.Subresource = "authorize"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoServiceAuthorizeResponse{})
	result = obj.(*devops.CodeRepoServiceAuthorizeResponse)
	return result, err
}

func (c *FakeImageRepositories) Remote(name string, opts *devops.ImageRepositoryOptions) (result *devops.ImageRepositoryRemoteStatus, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = imageregistriesResource
	action.Subresource = "remote"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ImageRepositoryRemoteStatus{})
	result = obj.(*devops.ImageRepositoryRemoteStatus)
	return result, err
}

func (c *FakeImageRepositories) Link(name string, opts *devops.ImageRepositoryOptions) (result *devops.ImageRepositoryLink, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = imageregistriesResource
	action.Subresource = "remote"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ImageRepositoryLink{})
	result = obj.(*devops.ImageRepositoryLink)
	return result, err
}
