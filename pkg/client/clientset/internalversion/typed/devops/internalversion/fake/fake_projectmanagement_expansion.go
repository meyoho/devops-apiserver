package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	core "k8s.io/client-go/testing"
)

// Authorize adding fake method for testing
func (c *FakeProjectManagements) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = projectmanagementsResource
	action.Subresource = "authorize"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoServiceAuthorizeResponse{})
	result = obj.(*devops.CodeRepoServiceAuthorizeResponse)
	return result, err
}

func (c *FakeProjectManagements) ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = projectmanagementsResource
	action.Subresource = "projects"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectDataList{})
	result = obj.(*devops.ProjectDataList)
	return result, err
}

func (c *FakeProjectManagements) CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "create"
	action.Resource = projectmanagementsResource
	action.Subresource = "projects"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectData{})
	result = obj.(*devops.ProjectData)
	return result, err
}

func (p *FakeProjectManagements) GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = projectmanagementsResource
	action.Subresource = "roles"
	action.Value = opts
	obj, err := p.Fake.Invokes(action, &devops.RoleMappingListOptions{})
	result = obj.(*devops.RoleMapping)
	return result, err
}

func (p *FakeProjectManagements) ApplyRoleMapping(serviceName string, opts *devops.RoleMapping) (result *devops.RoleMapping, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "create"
	action.Resource = imageregistrybindingsResource
	action.Subresource = "roles"
	action.Value = opts
	obj, err := p.Fake.Invokes(action, &devops.RoleMapping{})
	result = obj.(*devops.RoleMapping)
	return result, err
}
