package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	testing "k8s.io/client-go/testing"
)

// Preview takes the representation of a jenkinsfilePreviewOptions and creates it.  Returns the server's representation of the jenkinsfilePreview, and an error, if there is any.
func (c *FakeClusterPipelineTemplates) Preview(clusterPipelineTemplateName string, jenkinsfilePreviewOptions *devops.JenkinsfilePreviewOptions) (result *devops.JenkinsfilePreview, err error) {
	action := testing.GenericActionImpl{}
	action.Verb = "create"
	action.Resource = clusterpipelinetemplatesResource
	action.Subresource = "preview"
	action.Value = jenkinsfilePreviewOptions
	obj, err := c.Fake.Invokes(action, &devops.JenkinsfilePreview{})
	if obj == nil {
		return nil, err
	}
	return obj.(*devops.JenkinsfilePreview), err
}
