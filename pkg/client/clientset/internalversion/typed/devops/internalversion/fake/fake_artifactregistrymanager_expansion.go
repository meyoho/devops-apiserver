package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	core "k8s.io/client-go/testing"
)

// Authorize will check whether the secret is available
func (c *FakeArtifactRegistryManagers) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = artifactregistrymanagersResource
	action.Subresource = "authorize"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoServiceAuthorizeResponse{})
	result = obj.(*devops.CodeRepoServiceAuthorizeResponse)
	return result, err
}

func (c *FakeArtifactRegistryManagers) InitManager(serviceName string, opts *devops.ArtifactRegistryManagerOptions) error {
	action := core.GenericActionImpl{}
	action.Verb = "post"
	action.Resource = artifactregistrymanagersResource
	action.Subresource = "init"
	action.Value = opts
	_, err := c.Fake.Invokes(action, nil)
	return err
}
