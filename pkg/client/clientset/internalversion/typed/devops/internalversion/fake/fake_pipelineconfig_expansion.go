package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	core "k8s.io/client-go/testing"
)

// GetLogs adding fake log method for testing
func (c *FakePipelineConfigs) GetLogs(name string, opts *devops.PipelineConfigLogOptions) (result *devops.PipelineConfigLog, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "logs"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.PipelineLog{})
	result = obj.(*devops.PipelineConfigLog)
	return result, err
}
