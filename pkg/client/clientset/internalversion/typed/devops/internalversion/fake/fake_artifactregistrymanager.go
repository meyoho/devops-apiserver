/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeArtifactRegistryManagers implements ArtifactRegistryManagerInterface
type FakeArtifactRegistryManagers struct {
	Fake *FakeDevops
}

var artifactregistrymanagersResource = schema.GroupVersionResource{Group: "devops.alauda.io", Version: "", Resource: "artifactregistrymanagers"}

var artifactregistrymanagersKind = schema.GroupVersionKind{Group: "devops.alauda.io", Version: "", Kind: "ArtifactRegistryManager"}

// Get takes name of the artifactRegistryManager, and returns the corresponding artifactRegistryManager object, and an error if there is any.
func (c *FakeArtifactRegistryManagers) Get(name string, options v1.GetOptions) (result *devops.ArtifactRegistryManager, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootGetAction(artifactregistrymanagersResource, name), &devops.ArtifactRegistryManager{})
	if obj == nil {
		return nil, err
	}
	return obj.(*devops.ArtifactRegistryManager), err
}

// List takes label and field selectors, and returns the list of ArtifactRegistryManagers that match those selectors.
func (c *FakeArtifactRegistryManagers) List(opts v1.ListOptions) (result *devops.ArtifactRegistryManagerList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootListAction(artifactregistrymanagersResource, artifactregistrymanagersKind, opts), &devops.ArtifactRegistryManagerList{})
	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &devops.ArtifactRegistryManagerList{ListMeta: obj.(*devops.ArtifactRegistryManagerList).ListMeta}
	for _, item := range obj.(*devops.ArtifactRegistryManagerList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested artifactRegistryManagers.
func (c *FakeArtifactRegistryManagers) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewRootWatchAction(artifactregistrymanagersResource, opts))
}

// Create takes the representation of a artifactRegistryManager and creates it.  Returns the server's representation of the artifactRegistryManager, and an error, if there is any.
func (c *FakeArtifactRegistryManagers) Create(artifactRegistryManager *devops.ArtifactRegistryManager) (result *devops.ArtifactRegistryManager, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootCreateAction(artifactregistrymanagersResource, artifactRegistryManager), &devops.ArtifactRegistryManager{})
	if obj == nil {
		return nil, err
	}
	return obj.(*devops.ArtifactRegistryManager), err
}

// Update takes the representation of a artifactRegistryManager and updates it. Returns the server's representation of the artifactRegistryManager, and an error, if there is any.
func (c *FakeArtifactRegistryManagers) Update(artifactRegistryManager *devops.ArtifactRegistryManager) (result *devops.ArtifactRegistryManager, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootUpdateAction(artifactregistrymanagersResource, artifactRegistryManager), &devops.ArtifactRegistryManager{})
	if obj == nil {
		return nil, err
	}
	return obj.(*devops.ArtifactRegistryManager), err
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().
func (c *FakeArtifactRegistryManagers) UpdateStatus(artifactRegistryManager *devops.ArtifactRegistryManager) (*devops.ArtifactRegistryManager, error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootUpdateSubresourceAction(artifactregistrymanagersResource, "status", artifactRegistryManager), &devops.ArtifactRegistryManager{})
	if obj == nil {
		return nil, err
	}
	return obj.(*devops.ArtifactRegistryManager), err
}

// Delete takes name of the artifactRegistryManager and deletes it. Returns an error if one occurs.
func (c *FakeArtifactRegistryManagers) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewRootDeleteAction(artifactregistrymanagersResource, name), &devops.ArtifactRegistryManager{})
	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeArtifactRegistryManagers) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewRootDeleteCollectionAction(artifactregistrymanagersResource, listOptions)

	_, err := c.Fake.Invokes(action, &devops.ArtifactRegistryManagerList{})
	return err
}

// Patch applies the patch and returns the patched artifactRegistryManager.
func (c *FakeArtifactRegistryManagers) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *devops.ArtifactRegistryManager, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootPatchSubresourceAction(artifactregistrymanagersResource, name, pt, data, subresources...), &devops.ArtifactRegistryManager{})
	if obj == nil {
		return nil, err
	}
	return obj.(*devops.ArtifactRegistryManager), err
}
