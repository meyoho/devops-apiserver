package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	scheme "alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
)

// ClusterPipelineTemplateExpansion expansion methods for client
type ClusterPipelineTemplateExpansion interface {
	Preview(name string, opts *devops.JenkinsfilePreviewOptions) (*devops.JenkinsfilePreview, error)
}

// Preview for ClusterPipelineTemplate
func (c *clusterPipelineTemplates) Preview(name string, opts *devops.JenkinsfilePreviewOptions) (result *devops.JenkinsfilePreview, err error) {
	result = &devops.JenkinsfilePreview{}
	err = c.client.Get().
		Resource("clusterpipelinetemplates").
		Name(name).
		SubResource("preview").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
