package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	scheme "alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

// PipelineConfigExpansion expansion methods for client
type PipelineConfigExpansion interface {
	GetLogs(name string, opts *devops.PipelineConfigLogOptions) (*devops.PipelineConfigLog, error)
}

// GetLogs get log from pipelineconfig
func (c *pipelineConfigs) GetLogs(name string, opts *devops.PipelineConfigLogOptions) (result *devops.PipelineConfigLog, err error) {
	result = &devops.PipelineConfigLog{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelineconfigs").
		Name(name).
		SubResource("logs").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
