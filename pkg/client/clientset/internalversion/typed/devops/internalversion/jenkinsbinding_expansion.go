package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

type JenkinsBindingExpansion interface {
	GetJenkinsInfo(jenkinsBindingName string, options *devops.JenkinsBindingInfoOptions) (*devops.JenkinsBindingInfoResult, error)
}

func (c *jenkinsBindings) GetJenkinsInfo(jenkinsBindingName string, options *devops.JenkinsBindingInfoOptions) (*devops.JenkinsBindingInfoResult, error) {
	result := &devops.JenkinsBindingInfoResult{}
	err := c.client.Get().
		Namespace(c.ns).
		Resource("jenkinsbindings").
		Name(jenkinsBindingName).
		SubResource("info").
		VersionedParams(options, scheme.ParameterCodec).
		Do().
		Into(result)

	return result, err
}
