/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package internalversion

import (
	"time"

	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	scheme "alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// PipelineTemplatesGetter has a method to return a PipelineTemplateInterface.
// A group's client should implement this interface.
type PipelineTemplatesGetter interface {
	PipelineTemplates(namespace string) PipelineTemplateInterface
}

// PipelineTemplateInterface has methods to work with PipelineTemplate resources.
type PipelineTemplateInterface interface {
	Create(*devops.PipelineTemplate) (*devops.PipelineTemplate, error)
	Update(*devops.PipelineTemplate) (*devops.PipelineTemplate, error)
	UpdateStatus(*devops.PipelineTemplate) (*devops.PipelineTemplate, error)
	Delete(name string, options *v1.DeleteOptions) error
	DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error
	Get(name string, options v1.GetOptions) (*devops.PipelineTemplate, error)
	List(opts v1.ListOptions) (*devops.PipelineTemplateList, error)
	Watch(opts v1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *devops.PipelineTemplate, err error)
	Preview(pipelineTemplateName string, jenkinsfilePreviewOptions *devops.JenkinsfilePreviewOptions) (*devops.JenkinsfilePreview, error)

	PipelineTemplateExpansion
}

// pipelineTemplates implements PipelineTemplateInterface
type pipelineTemplates struct {
	client rest.Interface
	ns     string
}

// newPipelineTemplates returns a PipelineTemplates
func newPipelineTemplates(c *DevopsClient, namespace string) *pipelineTemplates {
	return &pipelineTemplates{
		client: c.RESTClient(),
		ns:     namespace,
	}
}

// Get takes name of the pipelineTemplate, and returns the corresponding pipelineTemplate object, and an error if there is any.
func (c *pipelineTemplates) Get(name string, options v1.GetOptions) (result *devops.PipelineTemplate, err error) {
	result = &devops.PipelineTemplate{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelinetemplates").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of PipelineTemplates that match those selectors.
func (c *pipelineTemplates) List(opts v1.ListOptions) (result *devops.PipelineTemplateList, err error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	result = &devops.PipelineTemplateList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelinetemplates").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested pipelineTemplates.
func (c *pipelineTemplates) Watch(opts v1.ListOptions) (watch.Interface, error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	opts.Watch = true
	return c.client.Get().
		Namespace(c.ns).
		Resource("pipelinetemplates").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Watch()
}

// Create takes the representation of a pipelineTemplate and creates it.  Returns the server's representation of the pipelineTemplate, and an error, if there is any.
func (c *pipelineTemplates) Create(pipelineTemplate *devops.PipelineTemplate) (result *devops.PipelineTemplate, err error) {
	result = &devops.PipelineTemplate{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("pipelinetemplates").
		Body(pipelineTemplate).
		Do().
		Into(result)
	return
}

// Update takes the representation of a pipelineTemplate and updates it. Returns the server's representation of the pipelineTemplate, and an error, if there is any.
func (c *pipelineTemplates) Update(pipelineTemplate *devops.PipelineTemplate) (result *devops.PipelineTemplate, err error) {
	result = &devops.PipelineTemplate{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("pipelinetemplates").
		Name(pipelineTemplate.Name).
		Body(pipelineTemplate).
		Do().
		Into(result)
	return
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().

func (c *pipelineTemplates) UpdateStatus(pipelineTemplate *devops.PipelineTemplate) (result *devops.PipelineTemplate, err error) {
	result = &devops.PipelineTemplate{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("pipelinetemplates").
		Name(pipelineTemplate.Name).
		SubResource("status").
		Body(pipelineTemplate).
		Do().
		Into(result)
	return
}

// Delete takes name of the pipelineTemplate and deletes it. Returns an error if one occurs.
func (c *pipelineTemplates) Delete(name string, options *v1.DeleteOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("pipelinetemplates").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *pipelineTemplates) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	var timeout time.Duration
	if listOptions.TimeoutSeconds != nil {
		timeout = time.Duration(*listOptions.TimeoutSeconds) * time.Second
	}
	return c.client.Delete().
		Namespace(c.ns).
		Resource("pipelinetemplates").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Timeout(timeout).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched pipelineTemplate.
func (c *pipelineTemplates) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *devops.PipelineTemplate, err error) {
	result = &devops.PipelineTemplate{}
	err = c.client.Patch(pt).
		Namespace(c.ns).
		Resource("pipelinetemplates").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}

// Preview takes the representation of a jenkinsfilePreviewOptions and creates it.  Returns the server's representation of the jenkinsfilePreview, and an error, if there is any.
func (c *pipelineTemplates) Preview(pipelineTemplateName string, jenkinsfilePreviewOptions *devops.JenkinsfilePreviewOptions) (result *devops.JenkinsfilePreview, err error) {
	result = &devops.JenkinsfilePreview{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("pipelinetemplates").
		Name(pipelineTemplateName).
		SubResource("preview").
		Body(jenkinsfilePreviewOptions).
		Do().
		Into(result)
	return
}
