package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
)

// ImageRegistryBindingExpansion expansion methods for client
type ImageRegistryBindingExpansion interface {
	GetImageRepos(name string) (*devops.ImageRegistryBindingRepositories, error)
	CreateImageProject(name string, opts *devops.CreateProjectOptions) (*devops.ProjectData, error)
	GetImageProjects(name string, opts *devops.ListProjectOptions) (*devops.ProjectDataList, error)
}

// Get binding image repositories with name and returns the remote repository list, and an error if there is any
func (c *imageRegistryBindings) GetImageRepos(name string) (result *devops.ImageRegistryBindingRepositories, err error) {
	result = &devops.ImageRegistryBindingRepositories{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imageregistrybindings").
		Name(name).
		SubResource("repositories").
		Do().
		Into(result)
	return
}

// Create image project in registry binding with name
func (c *imageRegistryBindings) CreateImageProject(name string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	result = &devops.ProjectData{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("imageregistrybindings").
		Name(name).
		SubResource("projects").
		Body(opts).
		Do().
		Into(result)
	return
}

// Get image projects with registry binding name
func (c *imageRegistryBindings) GetImageProjects(name string, opts *devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	result = &devops.ProjectDataList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imageregistrybindings").
		Name(name).
		SubResource("projects").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
