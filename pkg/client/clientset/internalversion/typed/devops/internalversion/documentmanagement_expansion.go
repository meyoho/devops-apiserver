package internalversion

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

// The DocumentManagementExpansion interface allows manually adding extra methods to the DocumentManagementInterface.
type DocumentManagementExpansion interface {
	Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error)
	ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error)
	CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error)
}

// Authorize will check whether the secret is available
func (c *documentManagements) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	result = &devops.CodeRepoServiceAuthorizeResponse{}
	err = c.client.Get().
		Resource("documentmanagements").
		Name(serviceName).
		SubResource("authorize").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

// ListProjects will list projects in documentmanagement
func (c *documentManagements) ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	result = &devops.ProjectDataList{}
	err = c.client.Get().
		Resource("documentmanagements").
		Name(serviceName).
		SubResource("projects").
		VersionedParams(&opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// CreateProject will create project in documentmanagement
func (c *documentManagements) CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	result = &devops.ProjectData{}
	err = c.client.Post().
		Resource("documentmanagements").
		Name(serviceName).
		SubResource("projects").
		Body(opts).
		Do().
		Into(result)
	return
}
