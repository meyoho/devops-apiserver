package v1alpha1

import (
	"fmt"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ DevOpsToolBindingInterface = &devopsToolBinding{}

type devopsToolBinding struct {
	devopsV1alpha1Interface DevopsV1alpha1Interface
}

type DevOpsToolBindingGetter interface {
	DevOpsToolBinding() DevOpsToolBindingInterface
}

type DevOpsToolBindingInterface interface {
	Get(kind, namespace, name string, options metav1.GetOptions) (metav1.Object, error)
	Create(kind, namespace string, obj metav1.Object) (metav1.Object, error)
	Update(kind, namespace string, obj metav1.Object) (metav1.Object, error)
	Delete(kind, namespace, name string, options *metav1.DeleteOptions) error
	List(kind string, options metav1.ListOptions) (interface{}, error)
}

func (devopsToolBinding *devopsToolBinding) Get(kind, namespace, name string, options metav1.GetOptions) (metav1.Object, error) {
	switch kind {
	case devops.TypeCodeRepoService:
		return devopsToolBinding.devopsV1alpha1Interface.CodeRepoBindings(namespace).Get(name, options)
	case devops.TypeCodeQualityTool:
		return devopsToolBinding.devopsV1alpha1Interface.CodeQualityBindings(namespace).Get(name, options)
	case devops.TypeDocumentManagement:
		return devopsToolBinding.devopsV1alpha1Interface.DocumentManagementBindings(namespace).Get(name, options)
	case devops.TypeProjectManagement:
		return devopsToolBinding.devopsV1alpha1Interface.ProjectManagementBindings(namespace).Get(name, options)
	case devops.TypeJenkins:
		return devopsToolBinding.devopsV1alpha1Interface.JenkinsBindings(namespace).Get(name, options)
	case devops.TypeImageRegistry:
		return devopsToolBinding.devopsV1alpha1Interface.ImageRegistryBindings(namespace).Get(name, options)
	case devops.TypeArtifactRegistry:
		return devopsToolBinding.devopsV1alpha1Interface.ArtifactRegistryBindings(namespace).Get(name, options)
	}

	return nil, fmt.Errorf("unknown type: %s", kind)
}

func (devopsToolBinding *devopsToolBinding) Create(kind, namespace string, obj metav1.Object) (metav1.Object, error) {
	switch kind {
	case devops.TypeCodeRepoService:
		codeRepoBinding, ok := obj.(*devops.CodeRepoBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeCodeRepoService)
		}
		return devopsToolBinding.devopsV1alpha1Interface.CodeRepoBindings(namespace).Create(codeRepoBinding)
	case devops.TypeCodeQualityTool:
		codeQualityBinding, ok := obj.(*devops.CodeQualityBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeCodeQualityTool)
		}
		return devopsToolBinding.devopsV1alpha1Interface.CodeQualityBindings(namespace).Create(codeQualityBinding)
	case devops.TypeDocumentManagement:
		documentManagementBinding, ok := obj.(*devops.DocumentManagementBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeDocumentManagement)
		}
		return devopsToolBinding.devopsV1alpha1Interface.DocumentManagementBindings(namespace).Create(documentManagementBinding)
	case devops.TypeProjectManagement:
		projectManagementBinding, ok := obj.(*devops.ProjectManagementBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeProjectManagement)
		}
		return devopsToolBinding.devopsV1alpha1Interface.ProjectManagementBindings(namespace).Create(projectManagementBinding)
	case devops.TypeJenkins:
		jenkinsBinding, ok := obj.(*devops.JenkinsBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeJenkins)
		}
		return devopsToolBinding.devopsV1alpha1Interface.JenkinsBindings(namespace).Create(jenkinsBinding)
	case devops.TypeImageRegistry:
		imageRegistryBinding, ok := obj.(*devops.ImageRegistryBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeImageRegistry)
		}
		return devopsToolBinding.devopsV1alpha1Interface.ImageRegistryBindings(namespace).Create(imageRegistryBinding)
	case devops.TypeArtifactRegistry:
		artifactRegistryBinding, ok := obj.(*devops.ArtifactRegistryBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeArtifactRegistry)
		}
		return devopsToolBinding.devopsV1alpha1Interface.ArtifactRegistryBindings(namespace).Create(artifactRegistryBinding)
	}

	return nil, fmt.Errorf("unknown type: %s", kind)
}

func (devopsToolBinding *devopsToolBinding) Update(kind, namespace string, obj metav1.Object) (metav1.Object, error) {
	switch kind {
	case devops.TypeCodeRepoService:
		codeRepoBinding, ok := obj.(*devops.CodeRepoBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeCodeRepoService)
		}
		return devopsToolBinding.devopsV1alpha1Interface.CodeRepoBindings(namespace).Update(codeRepoBinding)
	case devops.TypeCodeQualityTool:
		codeQualityBinding, ok := obj.(*devops.CodeQualityBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeCodeQualityTool)
		}
		return devopsToolBinding.devopsV1alpha1Interface.CodeQualityBindings(namespace).Update(codeQualityBinding)
	case devops.TypeDocumentManagement:
		documentManagementBinding, ok := obj.(*devops.DocumentManagementBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeDocumentManagement)
		}
		return devopsToolBinding.devopsV1alpha1Interface.DocumentManagementBindings(namespace).Update(documentManagementBinding)
	case devops.TypeProjectManagement:
		projectManagementBinding, ok := obj.(*devops.ProjectManagementBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeProjectManagement)
		}
		return devopsToolBinding.devopsV1alpha1Interface.ProjectManagementBindings(namespace).Update(projectManagementBinding)
	case devops.TypeJenkins:
		jenkinsBinding, ok := obj.(*devops.JenkinsBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeJenkins)
		}
		return devopsToolBinding.devopsV1alpha1Interface.JenkinsBindings(namespace).Update(jenkinsBinding)
	case devops.TypeImageRegistry:
		imageRegistryBinding, ok := obj.(*devops.ImageRegistryBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeImageRegistry)
		}
		return devopsToolBinding.devopsV1alpha1Interface.ImageRegistryBindings(namespace).Update(imageRegistryBinding)
	case devops.TypeArtifactRegistry:
		artifactRegistryBinding, ok := obj.(*devops.ArtifactRegistryBinding)
		if !ok {
			return nil, fmt.Errorf("kind not match %s", devops.TypeArtifactRegistry)
		}
		return devopsToolBinding.devopsV1alpha1Interface.ArtifactRegistryBindings(namespace).Update(artifactRegistryBinding)
	}

	return nil, fmt.Errorf("unknown type: %s", kind)
}

func (devopsToolBinding *devopsToolBinding) Delete(kind, namespace, name string, options *metav1.DeleteOptions) error {
	switch kind {
	case devops.TypeCodeRepoService:
		return devopsToolBinding.devopsV1alpha1Interface.CodeRepoBindings(namespace).Delete(name, options)
	case devops.TypeCodeQualityTool:
		return devopsToolBinding.devopsV1alpha1Interface.CodeQualityBindings(namespace).Delete(name, options)
	case devops.TypeDocumentManagement:
		return devopsToolBinding.devopsV1alpha1Interface.DocumentManagementBindings(namespace).Delete(name, options)
	case devops.TypeProjectManagement:
		return devopsToolBinding.devopsV1alpha1Interface.ProjectManagementBindings(namespace).Delete(name, options)
	case devops.TypeJenkins:
		return devopsToolBinding.devopsV1alpha1Interface.JenkinsBindings(namespace).Delete(name, options)
	case devops.TypeImageRegistry:
		return devopsToolBinding.devopsV1alpha1Interface.ImageRegistryBindings(namespace).Delete(name, options)
	case devops.TypeArtifactRegistry:
		return devopsToolBinding.devopsV1alpha1Interface.ArtifactRegistryBindings(namespace).Delete(name, options)
	}

	return fmt.Errorf("unknown type: %s", kind)
}

func (devopsToolBinding *devopsToolBinding) List(kind string, options metav1.ListOptions) (interface{}, error) {
	switch kind {
	case devops.TypeCodeRepoService:
		return devopsToolBinding.devopsV1alpha1Interface.CodeRepoBindings("").List(options)
	case devops.TypeCodeQualityTool:
		return devopsToolBinding.devopsV1alpha1Interface.CodeQualityBindings("").List(options)
	case devops.TypeDocumentManagement:
		return devopsToolBinding.devopsV1alpha1Interface.DocumentManagementBindings("").List(options)
	case devops.TypeProjectManagement:
		return devopsToolBinding.devopsV1alpha1Interface.ProjectManagementBindings("").List(options)
	case devops.TypeJenkins:
		return devopsToolBinding.devopsV1alpha1Interface.JenkinsBindings("").List(options)
	case devops.TypeImageRegistry:
		return devopsToolBinding.devopsV1alpha1Interface.ImageRegistryBindings("").List(options)
	case devops.TypeArtifactRegistry:
		return devopsToolBinding.devopsV1alpha1Interface.ArtifactRegistryBindings("").List(options)
	}

	return nil, fmt.Errorf("unknown type: %s", kind)
}
