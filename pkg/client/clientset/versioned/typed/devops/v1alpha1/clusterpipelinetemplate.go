/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package v1alpha1

import (
	"time"

	v1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	scheme "alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// ClusterPipelineTemplatesGetter has a method to return a ClusterPipelineTemplateInterface.
// A group's client should implement this interface.
type ClusterPipelineTemplatesGetter interface {
	ClusterPipelineTemplates() ClusterPipelineTemplateInterface
}

// ClusterPipelineTemplateInterface has methods to work with ClusterPipelineTemplate resources.
type ClusterPipelineTemplateInterface interface {
	Create(*v1alpha1.ClusterPipelineTemplate) (*v1alpha1.ClusterPipelineTemplate, error)
	Update(*v1alpha1.ClusterPipelineTemplate) (*v1alpha1.ClusterPipelineTemplate, error)
	UpdateStatus(*v1alpha1.ClusterPipelineTemplate) (*v1alpha1.ClusterPipelineTemplate, error)
	Delete(name string, options *v1.DeleteOptions) error
	DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error
	Get(name string, options v1.GetOptions) (*v1alpha1.ClusterPipelineTemplate, error)
	List(opts v1.ListOptions) (*v1alpha1.ClusterPipelineTemplateList, error)
	Watch(opts v1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.ClusterPipelineTemplate, err error)
	ClusterPipelineTemplateExpansion
}

// clusterPipelineTemplates implements ClusterPipelineTemplateInterface
type clusterPipelineTemplates struct {
	client rest.Interface
}

// newClusterPipelineTemplates returns a ClusterPipelineTemplates
func newClusterPipelineTemplates(c *DevopsV1alpha1Client) *clusterPipelineTemplates {
	return &clusterPipelineTemplates{
		client: c.RESTClient(),
	}
}

// Get takes name of the clusterPipelineTemplate, and returns the corresponding clusterPipelineTemplate object, and an error if there is any.
func (c *clusterPipelineTemplates) Get(name string, options v1.GetOptions) (result *v1alpha1.ClusterPipelineTemplate, err error) {
	result = &v1alpha1.ClusterPipelineTemplate{}
	err = c.client.Get().
		Resource("clusterpipelinetemplates").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of ClusterPipelineTemplates that match those selectors.
func (c *clusterPipelineTemplates) List(opts v1.ListOptions) (result *v1alpha1.ClusterPipelineTemplateList, err error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	result = &v1alpha1.ClusterPipelineTemplateList{}
	err = c.client.Get().
		Resource("clusterpipelinetemplates").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested clusterPipelineTemplates.
func (c *clusterPipelineTemplates) Watch(opts v1.ListOptions) (watch.Interface, error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	opts.Watch = true
	return c.client.Get().
		Resource("clusterpipelinetemplates").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Watch()
}

// Create takes the representation of a clusterPipelineTemplate and creates it.  Returns the server's representation of the clusterPipelineTemplate, and an error, if there is any.
func (c *clusterPipelineTemplates) Create(clusterPipelineTemplate *v1alpha1.ClusterPipelineTemplate) (result *v1alpha1.ClusterPipelineTemplate, err error) {
	result = &v1alpha1.ClusterPipelineTemplate{}
	err = c.client.Post().
		Resource("clusterpipelinetemplates").
		Body(clusterPipelineTemplate).
		Do().
		Into(result)
	return
}

// Update takes the representation of a clusterPipelineTemplate and updates it. Returns the server's representation of the clusterPipelineTemplate, and an error, if there is any.
func (c *clusterPipelineTemplates) Update(clusterPipelineTemplate *v1alpha1.ClusterPipelineTemplate) (result *v1alpha1.ClusterPipelineTemplate, err error) {
	result = &v1alpha1.ClusterPipelineTemplate{}
	err = c.client.Put().
		Resource("clusterpipelinetemplates").
		Name(clusterPipelineTemplate.Name).
		Body(clusterPipelineTemplate).
		Do().
		Into(result)
	return
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().

func (c *clusterPipelineTemplates) UpdateStatus(clusterPipelineTemplate *v1alpha1.ClusterPipelineTemplate) (result *v1alpha1.ClusterPipelineTemplate, err error) {
	result = &v1alpha1.ClusterPipelineTemplate{}
	err = c.client.Put().
		Resource("clusterpipelinetemplates").
		Name(clusterPipelineTemplate.Name).
		SubResource("status").
		Body(clusterPipelineTemplate).
		Do().
		Into(result)
	return
}

// Delete takes name of the clusterPipelineTemplate and deletes it. Returns an error if one occurs.
func (c *clusterPipelineTemplates) Delete(name string, options *v1.DeleteOptions) error {
	return c.client.Delete().
		Resource("clusterpipelinetemplates").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *clusterPipelineTemplates) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	var timeout time.Duration
	if listOptions.TimeoutSeconds != nil {
		timeout = time.Duration(*listOptions.TimeoutSeconds) * time.Second
	}
	return c.client.Delete().
		Resource("clusterpipelinetemplates").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Timeout(timeout).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched clusterPipelineTemplate.
func (c *clusterPipelineTemplates) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.ClusterPipelineTemplate, err error) {
	result = &v1alpha1.ClusterPipelineTemplate{}
	err = c.client.Patch(pt).
		Resource("clusterpipelinetemplates").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}
