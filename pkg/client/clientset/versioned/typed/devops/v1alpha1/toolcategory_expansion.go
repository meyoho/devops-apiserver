package v1alpha1

import (
	"encoding/json"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	log "k8s.io/klog"
)

//ArtifactRegistryManagerExpansion expasion of ProjectManagement client
type ToolCategoryExpansion interface {
	Setting() (result *devops.SettingResp, err error)
}

// Authorize will check whether the secret is available
func (c *toolCategories) Setting() (result *devops.SettingResp, err error) {
	result = new(devops.SettingResp)

	r := c.client.Get().
		Resource("toolcategories").
		Name("all").
		SubResource("settings").
		Do()

	//使用Into方法无法正常赋值，原因待查

	raw, err := r.Raw()
	if err != nil {
		log.Errorf("settings err is %#v", err)
		return
	}
	err = json.Unmarshal(raw, result)
	if err != nil {
		log.Errorf("settings err is %#v", err)
		return
	}
	log.Infof("Setting resutl is %#v", result)

	return
}
