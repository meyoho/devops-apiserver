package v1alpha1

import (
	"io"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
)

// PipelineExpansion expansion methods for client
type PipelineExpansion interface {
	GetLogs(name string, opts *v1alpha1.PipelineLogOptions) (*v1alpha1.PipelineLog, error)
	GetTasks(name string, opts *v1alpha1.PipelineTaskOptions) (result *v1alpha1.PipelineTask, err error)
	GetTestReports(name string, opts *v1alpha1.PipelineTestReportOptions) (result *v1alpha1.PipelineTestReport, err error)
	GetArtifacts(name string) (result *v1alpha1.PipelineArtifactList, err error)
	DownloadArtifact(name string, opts *v1alpha1.DownloadOption) (stream io.ReadCloser, err error)
	GetView(name string) (*v1alpha1.PipelineViewResult, error)
}

// GetLogs takes name of the pipeline and get log options and return PipelineLog or any error if any
func (c *pipelines) GetLogs(name string, opts *v1alpha1.PipelineLogOptions) (result *v1alpha1.PipelineLog, err error) {
	result = &v1alpha1.PipelineLog{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("logs").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// GetTasks takes name of the pipeline, and returns the corresponding pipeline object, and an error if there is any.
func (c *pipelines) GetTasks(name string, opts *v1alpha1.PipelineTaskOptions) (result *v1alpha1.PipelineTask, err error) {
	result = &v1alpha1.PipelineTask{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("tasks").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *pipelines) GetTestReports(name string, opts *v1alpha1.PipelineTestReportOptions) (result *v1alpha1.PipelineTestReport, err error) {
	result = &v1alpha1.PipelineTestReport{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("testreports").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *pipelines) GetArtifacts(name string) (result *v1alpha1.PipelineArtifactList, err error) {
	result = &v1alpha1.PipelineArtifactList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("artifacts").
		Do().
		Into(result)
	return
}
func (c *pipelines) DownloadArtifact(name string, opts *v1alpha1.DownloadOption) (stream io.ReadCloser, err error) {
	stream, err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("download").
		VersionedParams(opts, scheme.ParameterCodec).Stream()
	return
}

func (c *pipelines) GetView(name string) (*v1alpha1.PipelineViewResult, error) {
	result := &v1alpha1.PipelineViewResult{}
	err := c.client.Get().
		Namespace(c.ns).
		Resource("pipelines").
		Name(name).
		SubResource("view").
		Do().
		Into(result)
	return result, err
}
