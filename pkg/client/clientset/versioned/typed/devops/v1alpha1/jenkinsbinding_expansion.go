package v1alpha1

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
)

type JenkinsBindingExpansion interface {
	GetJenkinsInfo(jenkinsBindingName string, options *v1alpha1.JenkinsBindingInfoOptions) (*v1alpha1.JenkinsBindingInfoResult, error)
}

func (c *jenkinsBindings) GetJenkinsInfo(jenkinsBindingName string, options *v1alpha1.JenkinsBindingInfoOptions) (*v1alpha1.JenkinsBindingInfoResult, error) {
	result := &v1alpha1.JenkinsBindingInfoResult{}
	err := c.client.Get().
		Namespace(c.ns).
		Resource("jenkinsbindings").
		Name(jenkinsBindingName).
		SubResource("info").
		VersionedParams(options, scheme.ParameterCodec).
		Do().
		Into(result)

	return result, err
}
