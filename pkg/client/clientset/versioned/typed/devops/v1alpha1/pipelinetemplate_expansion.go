package v1alpha1

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
)

// PipelineTemplateExpansion expansion methods for client
type PipelineTemplateExpansion interface {
	Exports(name string, opts *v1alpha1.ExportShowOptions) (*v1alpha1.PipelineExportedVariables, error)
}

// Exports for PipelineTemplate
func (c *pipelineTemplates) Exports(name string, opts *v1alpha1.ExportShowOptions) (result *v1alpha1.PipelineExportedVariables, err error) {
	result = &v1alpha1.PipelineExportedVariables{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelinetemplates").
		Name(name).
		SubResource("exports").
		Do().
		Into(result)
	return
}
