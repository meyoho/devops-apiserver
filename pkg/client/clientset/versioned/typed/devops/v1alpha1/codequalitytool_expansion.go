package v1alpha1

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
	"k8s.io/apimachinery/pkg/api/errors"
)

// The CodeQualityToolExpansion interface allows manually adding extra methods to the CodeQualityToolInterface.
type CodeQualityToolExpansion interface {
	Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error)
	CreateProject(serviceName string, opts *devops.CreateProjectOptions) (*devops.ProjectData, error)
	ListProjects(serviceName string, opts devops.ListProjectOptions) (*devops.ProjectDataList, error)
	GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error)
	ApplyRoleMapping(serviceName string, opts *devops.RoleMapping) (result *devops.RoleMapping, err error)
}

// Authorize will check whether the secret is available
func (c *codeQualityTools) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	result = &devops.CodeRepoServiceAuthorizeResponse{}
	err = c.client.Get().
		Resource("codequalitytools").
		Name(serviceName).
		SubResource("authorize").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

// ListProjects will list projects in codeQualityTools
func (c *codeQualityTools) ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("codequalitytools"), "ListProjects")
}

// CreateProject will create project in codeQualityTools
func (c *codeQualityTools) CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("codequalitytools"), "CreateProject")
}

func (c *codeQualityTools) GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error) {
	result = &devops.RoleMapping{}
	err = c.client.Get().
		Resource("codequalitytools").
		Name(serviceName).
		SubResource("roles").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

func (c *codeQualityTools) ApplyRoleMapping(serviceName string, opts *devops.RoleMapping) (result *devops.RoleMapping, err error) {
	result = &devops.RoleMapping{}
	err = c.client.Post().
		Resource("codequalitytools").
		Name(serviceName).
		SubResource("roles").
		Body(opts).
		Do().
		Into(result)
	return
}
