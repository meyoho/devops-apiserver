package v1alpha1

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	scheme "alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

//ProjectManagementExpansion expasion of ProjectManagement client
type ProjectManagementBindingExpansion interface {
	IssueOptions(name string, issuetype *v1alpha1.IssueSearchOptions) (result *v1alpha1.IssueFilterDataList, err error)
	IssueDetail(name string, issuekey *v1alpha1.IssueKeyOptions) (result *v1alpha1.IssueDetail, err error)
	IssueList(name string, issuelistoption *v1alpha1.ListIssuesOptions) (result *v1alpha1.IssueDetailList, err error)
}

func (c *projectManagementBindings) IssueOptions(name string, issuetype *v1alpha1.IssueSearchOptions) (result *v1alpha1.IssueFilterDataList, err error) {
	result = &v1alpha1.IssueFilterDataList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("projectmanagementbindings").
		Name(name).
		SubResource("issueoptions").
		VersionedParams(issuetype, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *projectManagementBindings) IssueDetail(name string, issuekey *v1alpha1.IssueKeyOptions) (result *v1alpha1.IssueDetail, err error) {
	result = &v1alpha1.IssueDetail{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("projectmanagementbindings").
		Name(name).
		SubResource("issue").
		VersionedParams(issuekey, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *projectManagementBindings) IssueList(name string, issuelistoption *v1alpha1.ListIssuesOptions) (result *v1alpha1.IssueDetailList, err error) {
	result = &v1alpha1.IssueDetailList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("projectmanagementbindings").
		Name(name).
		SubResource("issueslist").
		VersionedParams(issuelistoption, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
