package v1alpha1

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

// The CodeRepoServiceExpansion interface allows manually adding extra methods to the ImageRegistryInterface.
type ImageRegistryExpansion interface {
	Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error)
	GetImageRepos(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.ImageRegistryBindingRepositories, error)
	ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error)
	CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error)
	GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error)
	ApplyRoleMapping(serviceName string, opts *devops.RoleMapping) (result *devops.RoleMapping, err error)
}

// Authorize will check whether the secret is available
func (c *imageRegistries) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	result = &devops.CodeRepoServiceAuthorizeResponse{}
	err = c.client.Get().
		Resource("imageregistries").
		Name(serviceName).
		SubResource("authorize").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

// GetImageRepos will list remote image repositories
func (c *imageRegistries) GetImageRepos(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.ImageRegistryBindingRepositories, err error) {
	result = &devops.ImageRegistryBindingRepositories{}
	err = c.client.Get().
		Resource("imageregistries").
		Name(serviceName).
		SubResource("repositories").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

// ListProjects will list projects in imageRegistrie
func (c *imageRegistries) ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	result = &devops.ProjectDataList{}
	err = c.client.Get().
		Resource("imageregistries").
		Name(serviceName).
		SubResource("projects").
		VersionedParams(&opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// CreateProject will create project in imageRegistrie
func (c *imageRegistries) CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	result = &devops.ProjectData{}
	err = c.client.Post().
		Resource("imageregistries").
		Name(serviceName).
		SubResource("projects").
		Body(opts).
		Do().
		Into(result)
	return
}

func (p *imageRegistries) GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error) {
	result = &devops.RoleMapping{}
	err = p.client.Get().
		Resource("imageregistries").
		Name(serviceName).
		SubResource("roles").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

func (p *imageRegistries) ApplyRoleMapping(serviceName string, opts *devops.RoleMapping) (result *devops.RoleMapping, err error) {
	result = &devops.RoleMapping{}
	err = p.client.Post().
		Resource("imageregistries").
		Name(serviceName).
		SubResource("roles").
		Body(opts).
		Do().
		Into(result)
	return
}
