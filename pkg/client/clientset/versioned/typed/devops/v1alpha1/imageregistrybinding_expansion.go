package v1alpha1

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
)

// ImageRegistryBindingExpansion expansion methods for client
type ImageRegistryBindingExpansion interface {
	GetImageRepos(name string) (*v1alpha1.ImageRegistryBindingRepositories, error)
	CreateImageProject(name string, opts *v1alpha1.CreateProjectOptions) (*v1alpha1.ProjectData, error)
	GetImageProjects(name string, opts *v1alpha1.ListProjectOptions) (*v1alpha1.ProjectDataList, error)
}

// Get takes name of imageregistrybinding, and returns the remote repository list, and an error if there is any
func (c *imageRegistryBindings) GetImageRepos(name string) (result *v1alpha1.ImageRegistryBindingRepositories, err error) {
	result = &v1alpha1.ImageRegistryBindingRepositories{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imageregistrybindings").
		Name(name).
		SubResource("repositories").
		Do().
		Into(result)
	return
}

// Create image project in registry binding with name
func (c *imageRegistryBindings) CreateImageProject(name string, opts *v1alpha1.CreateProjectOptions) (result *v1alpha1.ProjectData, err error) {
	result = &v1alpha1.ProjectData{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("imageregistrybindings").
		Name(name).
		SubResource("projects").
		Body(opts).
		Do().
		Into(result)
	return
}

// Get image projects with registry binding name
func (c *imageRegistryBindings) GetImageProjects(name string, opts *v1alpha1.ListProjectOptions) (result *v1alpha1.ProjectDataList, err error) {
	result = &v1alpha1.ProjectDataList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imageregistrybindings").
		Name(name).
		SubResource("projects").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
