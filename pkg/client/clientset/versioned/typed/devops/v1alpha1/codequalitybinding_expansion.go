package v1alpha1

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

// CodeQualityBindingExpansion expansion of codequalitybinding client
type CodeQualityBindingExpansion interface {
	Projects(serviceName string, opts *devops.CorrespondCodeQualityProjectsOptions) (*devops.CodeQualityProjectList, error)
}

func (c *codeQualityBindings) Projects(serviceName string, opts *devops.CorrespondCodeQualityProjectsOptions) (result *devops.CodeQualityProjectList, err error) {
	result = &devops.CodeQualityProjectList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("codequalitybindings").
		Name(serviceName).
		SubResource("projects").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}
