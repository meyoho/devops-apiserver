package v1alpha1

import (
	"fmt"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

func (expansion *devopsV1alpha1InterfaceExpansion) PipelineTemplateInterface() PipelineTemplateInterfaceInterface {
	return &pipelineTemplate{
		DevopsV1alpha1Interface: expansion.DevopsV1alpha1Interface,
	}
}

func (expansion *devopsV1alpha1InterfaceExpansion) PipelineTaskTemplateInterface() PipelineTaskTemplateInterfaceInterface {
	return &pipelineTaskTemplate{
		DevopsV1alpha1Interface: expansion.DevopsV1alpha1Interface,
	}
}

type PipelineTemplateInterfaceGetter interface {
	PipelineTemplateInterface() PipelineTemplateInterfaceInterface
}

type PipelineTaskTemplateInterfaceGetter interface {
	PipelineTaskTemplateInterface() PipelineTaskTemplateInterfaceInterface
}

type PipelineTemplateInterfaceInterface interface {
	Create(devops.PipelineTemplateInterface) (devops.PipelineTemplateInterface, error)
	Update(devops.PipelineTemplateInterface) (devops.PipelineTemplateInterface, error)
	UpdateStatus(devops.PipelineTemplateInterface) (devops.PipelineTemplateInterface, error)
	Delete(kind, namespace, name string, options *v1.DeleteOptions) error
	Get(kind, namespace, name string, options v1.GetOptions) (devops.PipelineTemplateInterface, error)
	List(kind, namespace string, options v1.ListOptions) ([]devops.PipelineTemplateInterface, error)
	Preview(kind, namespace, name string, jenkinsfilePreviewOptions *devops.JenkinsfilePreviewOptions) (*devops.JenkinsfilePreview, error)
}

type PipelineTaskTemplateInterfaceInterface interface {
	Create(devops.PipelineTaskTemplateInterface) (devops.PipelineTaskTemplateInterface, error)
	Update(devops.PipelineTaskTemplateInterface) (devops.PipelineTaskTemplateInterface, error)
	UpdateStatus(devops.PipelineTaskTemplateInterface) (devops.PipelineTaskTemplateInterface, error)
	Delete(kind, namespace, name string, options *v1.DeleteOptions) error
	List(kind, namespace string, options v1.ListOptions) ([]devops.PipelineTaskTemplateInterface, error)
	Get(kind, namespace, name string, options v1.GetOptions) (devops.PipelineTaskTemplateInterface, error)
}

// pipelineTemplate is a pipelinetemplate  or clusterpipelinetemplate
type pipelineTemplate struct {
	DevopsV1alpha1Interface DevopsV1alpha1Interface
}

// pipelineTaskTemplate is a pipelinetasktemplate  or clustertaskpipelinetemplate
type pipelineTaskTemplate struct {
	DevopsV1alpha1Interface DevopsV1alpha1Interface
}

func unKnowTemplateError(name, kind string) error {
	return fmt.Errorf("template '%s' is not %s", name, kind)
}

func (t *pipelineTemplate) Create(template devops.PipelineTemplateInterface) (devops.PipelineTemplateInterface, error) {
	templateName := fmt.Sprintf("%s/%s", template.GetObjectMeta().GetNamespace(), template.GetObjectMeta())

	switch template.GetKind() {
	case devops.TypeClusterPipelineTemplate:
		clusterTemplate, ok := template.(*devops.ClusterPipelineTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypeClusterPipelineTemplate)
		}
		return t.DevopsV1alpha1Interface.ClusterPipelineTemplates().Create(clusterTemplate)
	case devops.TypePipelineTemplate:
		ptemplate, ok := template.(*devops.PipelineTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypePipelineTemplate)
		}
		return t.DevopsV1alpha1Interface.PipelineTemplates(ptemplate.Namespace).Create(ptemplate)
	default:
		glog.Errorf("template %#v is not a known kind", template)
		return nil, unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTemplate) Update(template devops.PipelineTemplateInterface) (devops.PipelineTemplateInterface, error) {
	templateName := fmt.Sprintf("%s/%s", template.GetObjectMeta().GetNamespace(), template.GetObjectMeta())

	switch template.GetKind() {
	case devops.TypeClusterPipelineTemplate:
		clusterTemplate, ok := template.(*devops.ClusterPipelineTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypeClusterPipelineTemplate)
		}
		return t.DevopsV1alpha1Interface.ClusterPipelineTemplates().Update(clusterTemplate)
	case devops.TypePipelineTemplate:
		ptemplate, ok := template.(*devops.PipelineTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypePipelineTemplate)
		}
		return t.DevopsV1alpha1Interface.PipelineTemplates(ptemplate.Namespace).Update(ptemplate)
	default:
		glog.Errorf("template %#v is not a known kind", template)
		return nil, unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTemplate) UpdateStatus(template devops.PipelineTemplateInterface) (devops.PipelineTemplateInterface, error) {
	templateName := fmt.Sprintf("%s/%s", template.GetNamespace(), template.GetName())
	switch template.GetKind() {
	case devops.TypeClusterPipelineTemplate:
		clusterTemplate, ok := template.(*devops.ClusterPipelineTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypeClusterPipelineTemplate)
		}
		return t.DevopsV1alpha1Interface.ClusterPipelineTemplates().UpdateStatus(clusterTemplate)
	case devops.TypePipelineTemplate:
		ptemplate, ok := template.(*devops.PipelineTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypePipelineTemplate)
		}
		return t.DevopsV1alpha1Interface.PipelineTemplates(ptemplate.Namespace).UpdateStatus(ptemplate)
	default:
		glog.Errorf("template %#v is not a known kind", template)
		return nil, unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTemplate) Delete(kind, namespace, name string, options *v1.DeleteOptions) error {
	templateName := fmt.Sprintf("%s/%s/%s", kind, namespace, name)

	switch kind {
	case devops.TypeClusterPipelineTemplate:
		return t.DevopsV1alpha1Interface.ClusterPipelineTemplates().Delete(name, options)
	case devops.TypePipelineTemplate:
		return t.DevopsV1alpha1Interface.PipelineTemplates(namespace).Delete(name, options)
	default:
		glog.Errorf("template %#v is not a known kind", kind)
		return unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTemplate) Get(kind, namespace, name string, options v1.GetOptions) (devops.PipelineTemplateInterface, error) {

	switch kind {
	case devops.TypeClusterPipelineTemplate:
		return t.DevopsV1alpha1Interface.ClusterPipelineTemplates().Get(name, options)
	case devops.TypePipelineTemplate:
		return t.DevopsV1alpha1Interface.PipelineTemplates(namespace).Get(name, options)
	default:
		glog.Errorf("template kind %s is not a known kind", kind)
		return nil, unKnowTemplateError(kind, "a known kind")
	}
}

func (t *pipelineTemplate) Preview(kind, namespace, name string, jenkinsfilePreviewOptions *devops.JenkinsfilePreviewOptions) (*devops.JenkinsfilePreview, error) {

	switch kind {
	case devops.TypeClusterPipelineTemplate:
		return t.DevopsV1alpha1Interface.ClusterPipelineTemplates().Preview(name, jenkinsfilePreviewOptions)
	case devops.TypePipelineTemplate:
		return t.DevopsV1alpha1Interface.PipelineTemplates(namespace).Preview(name, jenkinsfilePreviewOptions)
	default:
		glog.Errorf("template kind %s is not a known kind", kind)
		return nil, unKnowTemplateError(kind, "a known kind")
	}
}

func (t *pipelineTemplate) List(kind, namespace string, options v1.ListOptions) ([]devops.PipelineTemplateInterface, error) {

	data := []devops.PipelineTemplateInterface{}

	switch kind {
	case devops.TypeClusterPipelineTemplate:
		pipeline, err := t.DevopsV1alpha1Interface.ClusterPipelineTemplates().List(options)
		if err != nil {
			return nil, err
		}
		for _, _template := range pipeline.Items {
			template := _template
			data = append(data, &template)
		}
		return data, nil
	case devops.TypePipelineTemplate:
		pipeline, err := t.DevopsV1alpha1Interface.PipelineTemplates(namespace).List(options)
		if err != nil {
			return nil, err
		}
		for _, _template := range pipeline.Items {
			template := _template
			data = append(data, &template)
		}

		return data, nil
	default:
		glog.Errorf("can not find %v template", kind)
		return nil, unKnowTemplateError(kind, "a known kind")
	}
}

func (t *pipelineTaskTemplate) Create(template devops.PipelineTaskTemplateInterface) (devops.PipelineTaskTemplateInterface, error) {
	templateName := fmt.Sprintf("%s/%s", template.GetObjectMeta().GetNamespace(), template.GetObjectMeta())

	switch template.GetKind() {
	case devops.TypeClusterPipelineTaskTemplate:
		clusterTaskTemplate, ok := template.(*devops.ClusterPipelineTaskTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypeClusterPipelineTaskTemplate)
		}
		return t.DevopsV1alpha1Interface.ClusterPipelineTaskTemplates().Create(clusterTaskTemplate)
	case devops.TypePipelineTaskTemplate:
		pTasktemplate, ok := template.(*devops.PipelineTaskTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypePipelineTaskTemplate)
		}
		return t.DevopsV1alpha1Interface.PipelineTaskTemplates(pTasktemplate.Namespace).Create(pTasktemplate)
	default:
		glog.Errorf("template %#v is not a known kind", template)
		return nil, unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTaskTemplate) Update(template devops.PipelineTaskTemplateInterface) (devops.PipelineTaskTemplateInterface, error) {
	templateName := fmt.Sprintf("%s/%s", template.GetObjectMeta().GetNamespace(), template.GetObjectMeta())

	switch template.GetKind() {
	case devops.TypeClusterPipelineTaskTemplate:
		clusterTaskTemplate, ok := template.(*devops.ClusterPipelineTaskTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypeClusterPipelineTaskTemplate)
		}
		return t.DevopsV1alpha1Interface.ClusterPipelineTaskTemplates().Update(clusterTaskTemplate)
	case devops.TypePipelineTaskTemplate:
		pTasktemplate, ok := template.(*devops.PipelineTaskTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypePipelineTaskTemplate)
		}
		return t.DevopsV1alpha1Interface.PipelineTaskTemplates(pTasktemplate.Namespace).Update(pTasktemplate)
	default:
		glog.Errorf("template %#v is not a known kind", template)
		return nil, unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTaskTemplate) UpdateStatus(template devops.PipelineTaskTemplateInterface) (devops.PipelineTaskTemplateInterface, error) {
	templateName := fmt.Sprintf("%s/%s", template.GetNamespace(), template.GetName())

	switch template.GetKind() {
	case devops.TypeClusterPipelineTaskTemplate:
		clusterTaskTemplate, ok := template.(*devops.ClusterPipelineTaskTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypeClusterPipelineTaskTemplate)
		}
		return t.DevopsV1alpha1Interface.ClusterPipelineTaskTemplates().UpdateStatus(clusterTaskTemplate)
	case devops.TypePipelineTaskTemplate:
		pTasktemplate, ok := template.(*devops.PipelineTaskTemplate)
		if !ok {
			return nil, unKnowKindError(templateName, devops.TypePipelineTaskTemplate)
		}
		return t.DevopsV1alpha1Interface.PipelineTaskTemplates(pTasktemplate.Namespace).UpdateStatus(pTasktemplate)
	default:
		glog.Errorf("template %#v is not a known kind", template)
		return nil, unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTaskTemplate) Delete(kind, namespace, name string, options *v1.DeleteOptions) error {
	templateName := fmt.Sprintf("%s/%s/%s", kind, namespace, name)

	switch kind {
	case devops.TypeClusterPipelineTaskTemplate:
		return t.DevopsV1alpha1Interface.ClusterPipelineTaskTemplates().Delete(name, options)
	case devops.TypePipelineTaskTemplate:
		return t.DevopsV1alpha1Interface.PipelineTaskTemplates(namespace).Delete(name, options)
	default:
		glog.Errorf("template %#v is not a known kind", kind)
		return unKnowTemplateError(templateName, "a known kind")
	}
}

func (t *pipelineTaskTemplate) Get(kind, namespace, name string, options v1.GetOptions) (devops.PipelineTaskTemplateInterface, error) {

	switch kind {
	case devops.TypeClusterPipelineTaskTemplate:
		return t.DevopsV1alpha1Interface.ClusterPipelineTaskTemplates().Get(name, options)
	case devops.TypePipelineTaskTemplate:
		return t.DevopsV1alpha1Interface.PipelineTaskTemplates(namespace).Get(name, options)
	default:
		glog.Errorf("template kind %s is not a known kind", kind)
		return nil, unKnowTemplateError(kind, "a known kind")
	}
}

func (t *pipelineTaskTemplate) List(kind, namespace string, options v1.ListOptions) ([]devops.PipelineTaskTemplateInterface, error) {

	data := []devops.PipelineTaskTemplateInterface{}

	switch kind {
	case devops.TypeClusterPipelineTaskTemplate:
		taskpipeline, err := t.DevopsV1alpha1Interface.ClusterPipelineTaskTemplates().List(options)
		if err != nil {
			return nil, err
		}
		for _, _task := range taskpipeline.Items {
			task := _task
			data = append(data, &task)
		}
		return data, nil
	case devops.TypePipelineTaskTemplate:
		taskpipeline, err := t.DevopsV1alpha1Interface.PipelineTaskTemplates(namespace).List(options)
		if err != nil {
			return nil, err
		}
		for _, _task := range taskpipeline.Items {
			task := _task
			data = append(data, &task)
		}

		return data, nil
	default:
		glog.Errorf("can not find %v task template", kind)
		return nil, unKnowTemplateError(kind, "a known kind")
	}
}
