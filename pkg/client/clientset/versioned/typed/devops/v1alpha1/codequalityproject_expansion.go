package v1alpha1

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

// CodeQualityProjectExpansion expansion of codequalityproject client
type CodeQualityProjectExpansion interface {
	Remote(serviceName string, opts *devops.CodeQualityProjectOptions) (*devops.CodeQualityProjectRemoteStatus, error)
	Reports(serviceName string, opts *devops.CodeQualityProjectOptions) (*devops.CodeQualityProjectReportsCondition, error)
	LastAnalysisDate(serviceName string, opts *devops.CodeQualityProjectOptions) (*devops.CodeQualityProjectLastAnalysisDate, error)
}

func (c *codeQualityProjects) Remote(serviceName string, opts *devops.CodeQualityProjectOptions) (result *devops.CodeQualityProjectRemoteStatus, err error) {
	result = &devops.CodeQualityProjectRemoteStatus{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("codequalityprojects").
		Name(serviceName).
		SubResource("remote").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

func (c *codeQualityProjects) Reports(serviceName string, opts *devops.CodeQualityProjectOptions) (result *devops.CodeQualityProjectReportsCondition, err error) {
	result = &devops.CodeQualityProjectReportsCondition{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("codequalityprojects").
		Name(serviceName).
		SubResource("reports").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

func (c *codeQualityProjects) LastAnalysisDate(serviceName string, opts *devops.CodeQualityProjectOptions) (result *devops.CodeQualityProjectLastAnalysisDate, err error) {
	result = &devops.CodeQualityProjectLastAnalysisDate{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("codequalityprojects").
		Name(serviceName).
		SubResource("date").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}
