package v1alpha1

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
)

// ClusterPipelineTemplateExpansion expansion methods for client
type ClusterPipelineTemplateExpansion interface {
	Preview(name string, opts *v1alpha1.JenkinsfilePreviewOptions) (*v1alpha1.JenkinsfilePreview, error)
	Exports(name string, opts *v1alpha1.ExportShowOptions) (*v1alpha1.PipelineExportedVariables, error)
}

// Preview for ClusterPipelineTemplate
func (c *clusterPipelineTemplates) Preview(name string, opts *v1alpha1.JenkinsfilePreviewOptions) (result *v1alpha1.JenkinsfilePreview, err error) {
	result = &v1alpha1.JenkinsfilePreview{}
	err = c.client.Post().
		Resource("clusterpipelinetemplates").
		Name(name).
		SubResource("preview").
		Body(opts).
		Do().
		Into(result)
	return
}

// Exports for ClusterPipelineTemplate
func (c *clusterPipelineTemplates) Exports(name string, opts *v1alpha1.ExportShowOptions) (result *v1alpha1.PipelineExportedVariables, err error) {
	result = &v1alpha1.PipelineExportedVariables{}
	err = c.client.Get().
		Resource("clusterpipelinetemplates").
		Name(name).
		SubResource("exports").
		Do().
		Into(result)
	return
}
