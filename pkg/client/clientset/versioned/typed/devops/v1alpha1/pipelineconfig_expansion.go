package v1alpha1

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	scheme "alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

// PipelineConfigExpansion expansion methods for client
type PipelineConfigExpansion interface {
	GetLogs(name string, opts *v1alpha1.PipelineConfigLogOptions) (*v1alpha1.PipelineConfigLog, error)
}

// GetLogs get log from pipelineconfig
func (c *pipelineConfigs) GetLogs(name string, opts *v1alpha1.PipelineConfigLogOptions) (result *v1alpha1.PipelineConfigLog, err error) {
	result = &v1alpha1.PipelineConfigLog{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("pipelineconfigs").
		Name(name).
		SubResource("logs").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
