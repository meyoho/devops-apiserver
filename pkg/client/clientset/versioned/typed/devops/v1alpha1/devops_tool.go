package v1alpha1

import (
	"fmt"

	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

type DevopsV1alpha1InterfaceExpansion interface {
	DevopsV1alpha1Interface
	DevOpsToolGetter
	DevOpsToolBindingGetter
	PipelineTemplateInterfaceGetter
	PipelineTaskTemplateInterfaceGetter
}

func NewDevopsV1alpha1InterfaceExpansion(v1alpha1Interface DevopsV1alpha1Interface) DevopsV1alpha1InterfaceExpansion {
	return &devopsV1alpha1InterfaceExpansion{
		DevopsV1alpha1Interface: v1alpha1Interface,
	}
}

type devopsV1alpha1InterfaceExpansion struct {
	DevopsV1alpha1Interface
	DevOpsToolGetter
	DevOpsToolBindingGetter
}

type DevOpsToolGetter interface {
	DevOpsTool() DevOpsToolInterface
}

type DevOpsToolInterface interface {
	Create(devops.ToolInterface) (devops.ToolInterface, error)
	Update(devops.ToolInterface) (devops.ToolInterface, error)
	UpdateStatus(devops.ToolInterface) (devops.ToolInterface, error)
	Delete(kind, name string, options *metav1.DeleteOptions) error
	Get(kind, name string, options metav1.GetOptions) (devops.ToolInterface, error)

	Authorize(kind string, name string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error)

	CreateProject(kind string, name string, opts *devops.CreateProjectOptions) (*devops.ProjectData, error)
	ListProjects(kind string, name string, opts devops.ListProjectOptions) (*devops.ProjectDataList, error)
}

func (expansion *devopsV1alpha1InterfaceExpansion) DevOpsTool() DevOpsToolInterface {
	return &devopsTool{
		devopsV1alpha1Interface: expansion.DevopsV1alpha1Interface,
	}
}

func (expansion *devopsV1alpha1InterfaceExpansion) DevOpsToolBinding() DevOpsToolBindingInterface {
	return &devopsToolBinding{
		devopsV1alpha1Interface: expansion.DevopsV1alpha1Interface,
	}
}

var _ DevOpsToolInterface = &devopsTool{}

type devopsTool struct {
	devopsV1alpha1Interface DevopsV1alpha1Interface
}

func unKnowKindError(toolString, kind string) error {
	return fmt.Errorf("tool '%s' is not %s", toolString, kind)
}

func (devopsTool *devopsTool) Create(tool devops.ToolInterface) (devops.ToolInterface, error) {

	toolName := fmt.Sprintf("%s/%s", tool.GetObjectMeta().GetNamespace(), tool.GetObjectMeta())
	switch tool.GetKind() {
	case devops.TypeCodeRepoService:
		codeRepoService, ok := tool.(*devops.CodeRepoService)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeRepoService)
		}
		return devopsTool.devopsV1alpha1Interface.CodeRepoServices().Create(codeRepoService)
	case devops.TypeJenkins:
		jenkins, ok := tool.(*devops.Jenkins)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeJenkins)
		}
		return devopsTool.devopsV1alpha1Interface.Jenkinses().Create(jenkins)
	case devops.TypeDocumentManagement:
		documentManagement, ok := tool.(*devops.DocumentManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeDocumentManagement)
		}
		return devopsTool.devopsV1alpha1Interface.DocumentManagements().Create(documentManagement)
	case devops.TypeProjectManagement:
		projectManagement, ok := tool.(*devops.ProjectManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeProjectManagement)
		}
		return devopsTool.devopsV1alpha1Interface.ProjectManagements().Create(projectManagement)
	case devops.TypeCodeQualityTool:
		codeQualityTool, ok := tool.(*devops.CodeQualityTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeQualityTool)
		}
		return devopsTool.devopsV1alpha1Interface.CodeQualityTools().Create(codeQualityTool)
	case devops.TypeImageRegistry:
		imageRegistry, ok := tool.(*devops.ImageRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeImageRegistry)
		}
		return devopsTool.devopsV1alpha1Interface.ImageRegistries().Create(imageRegistry)
	case devops.TypeTestTool:
		testTool, ok := tool.(*devops.TestTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeTestTool)
		}
		return devopsTool.devopsV1alpha1Interface.TestTools().Create(testTool)
	case devops.TypeArtifactRegistryManager:
		arm, ok := tool.(*devops.ArtifactRegistryManager)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistryManager)
		}
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistryManagers().Create(arm)
	case devops.TypeArtifactRegistry:
		ar, ok := tool.(*devops.ArtifactRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistry)
		}
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistries().Create(ar)
	default:
		glog.Errorf("tool %#v is not a known kind", tool)
		return nil, unKnowKindError(toolName, "a known kind")
	}
}

func (devopsTool *devopsTool) Update(tool devops.ToolInterface) (devops.ToolInterface, error) {
	toolName := fmt.Sprintf("%s/%s", tool.GetObjectMeta().GetNamespace(), tool.GetObjectMeta())

	switch tool.GetKind() {
	case devops.TypeCodeRepoService:
		codeRepoService, ok := tool.(*devops.CodeRepoService)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeRepoService)
		}
		return devopsTool.devopsV1alpha1Interface.CodeRepoServices().Update(codeRepoService)
	case devops.TypeJenkins:
		jenkins, ok := tool.(*devops.Jenkins)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeJenkins)
		}
		return devopsTool.devopsV1alpha1Interface.Jenkinses().Update(jenkins)
	case devops.TypeDocumentManagement:
		documentManagement, ok := tool.(*devops.DocumentManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeDocumentManagement)
		}
		return devopsTool.devopsV1alpha1Interface.DocumentManagements().Update(documentManagement)
	case devops.TypeProjectManagement:
		projectManagement, ok := tool.(*devops.ProjectManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeProjectManagement)
		}
		return devopsTool.devopsV1alpha1Interface.ProjectManagements().Update(projectManagement)
	case devops.TypeCodeQualityTool:
		codeQualityTool, ok := tool.(*devops.CodeQualityTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeQualityTool)
		}
		return devopsTool.devopsV1alpha1Interface.CodeQualityTools().Update(codeQualityTool)
	case devops.TypeImageRegistry:
		imageRegistry, ok := tool.(*devops.ImageRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeImageRegistry)
		}
		return devopsTool.devopsV1alpha1Interface.ImageRegistries().Update(imageRegistry)
	case devops.TypeTestTool:
		testTool, ok := tool.(*devops.TestTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeTestTool)
		}
		return devopsTool.devopsV1alpha1Interface.TestTools().Update(testTool)
	case devops.TypeArtifactRegistryManager:
		arm, ok := tool.(*devops.ArtifactRegistryManager)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistryManager)
		}
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistryManagers().Update(arm)
	case devops.TypeArtifactRegistry:
		ar, ok := tool.(*devops.ArtifactRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistry)
		}
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistries().Update(ar)
	default:
		glog.Errorf("tool %#v is not a known kind", tool)
		return nil, unKnowKindError(toolName, "a known kind")
	}
}
func (devopsTool *devopsTool) UpdateStatus(tool devops.ToolInterface) (devops.ToolInterface, error) {
	toolName := fmt.Sprintf("%s/%s", tool.GetObjectMeta().GetNamespace(), tool.GetObjectMeta())

	switch tool.GetKind() {
	case devops.TypeCodeRepoService:
		codeRepoService, ok := tool.(*devops.CodeRepoService)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeRepoService)
		}
		return devopsTool.devopsV1alpha1Interface.CodeRepoServices().UpdateStatus(codeRepoService)
	case devops.TypeJenkins:
		jenkins, ok := tool.(*devops.Jenkins)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeJenkins)
		}
		return devopsTool.devopsV1alpha1Interface.Jenkinses().UpdateStatus(jenkins)
	case devops.TypeDocumentManagement:
		documentManagement, ok := tool.(*devops.DocumentManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeDocumentManagement)
		}
		return devopsTool.devopsV1alpha1Interface.DocumentManagements().UpdateStatus(documentManagement)
	case devops.TypeProjectManagement:
		projectManagement, ok := tool.(*devops.ProjectManagement)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeProjectManagement)
		}
		return devopsTool.devopsV1alpha1Interface.ProjectManagements().UpdateStatus(projectManagement)
	case devops.TypeCodeQualityTool:
		codeQualityTool, ok := tool.(*devops.CodeQualityTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeCodeQualityTool)
		}
		return devopsTool.devopsV1alpha1Interface.CodeQualityTools().UpdateStatus(codeQualityTool)
	case devops.TypeImageRegistry:
		imageRegistry, ok := tool.(*devops.ImageRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeImageRegistry)
		}
		return devopsTool.devopsV1alpha1Interface.ImageRegistries().UpdateStatus(imageRegistry)
	case devops.TypeTestTool:
		testTool, ok := tool.(*devops.TestTool)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeTestTool)
		}
		return devopsTool.devopsV1alpha1Interface.TestTools().UpdateStatus(testTool)
	case devops.TypeArtifactRegistryManager:
		arm, ok := tool.(*devops.ArtifactRegistryManager)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistryManager)
		}
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistryManagers().UpdateStatus(arm)
	case devops.TypeArtifactRegistry:
		ar, ok := tool.(*devops.ArtifactRegistry)
		if !ok {
			return nil, unKnowKindError(toolName, devops.TypeArtifactRegistry)
		}
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistries().UpdateStatus(ar)
	default:
		glog.Errorf("tool %#v is not a known kind", tool)
		return nil, unKnowKindError(toolName, "a known kind")
	}
}

func notImplementError(kind, verb string) error {
	return errors.NewMethodNotSupported(devops.Resource(kind), verb)
}

func (devopsTool *devopsTool) Delete(kind, name string, options *metav1.DeleteOptions) error {
	switch kind {
	case devops.TypeCodeRepoService:
		return devopsTool.devopsV1alpha1Interface.CodeRepoServices().Delete(name, options)
	case devops.TypeJenkins:
		return devopsTool.devopsV1alpha1Interface.Jenkinses().Delete(name, options)
	case devops.TypeDocumentManagement:
		return devopsTool.devopsV1alpha1Interface.DocumentManagements().Delete(name, options)
	case devops.TypeProjectManagement:
		return devopsTool.devopsV1alpha1Interface.ProjectManagements().Delete(name, options)
	case devops.TypeCodeQualityTool:
		return devopsTool.devopsV1alpha1Interface.CodeQualityTools().Delete(name, options)
	case devops.TypeImageRegistry:
		return devopsTool.devopsV1alpha1Interface.ImageRegistries().Delete(name, options)
	case devops.TypeTestTool:
		return devopsTool.devopsV1alpha1Interface.TestTools().Delete(name, options)
	case devops.TypeArtifactRegistryManager:
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistryManagers().Delete(name, options)
	case devops.TypeArtifactRegistry:
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistries().Delete(name, options)
	default:
		glog.Errorf("tool kind '%s' is not a known kind", kind)
		return unKnowKindError(kind, "a known kind")
	}
}
func (devopsTool *devopsTool) Get(kind, name string, options metav1.GetOptions) (devops.ToolInterface, error) {

	switch kind {
	case devops.TypeCodeRepoService:
		return devopsTool.devopsV1alpha1Interface.CodeRepoServices().Get(name, options)
	case devops.TypeJenkins:
		return devopsTool.devopsV1alpha1Interface.Jenkinses().Get(name, options)
	case devops.TypeDocumentManagement:
		return devopsTool.devopsV1alpha1Interface.DocumentManagements().Get(name, options)
	case devops.TypeProjectManagement:
		return devopsTool.devopsV1alpha1Interface.ProjectManagements().Get(name, options)
	case devops.TypeCodeQualityTool:
		return devopsTool.devopsV1alpha1Interface.CodeQualityTools().Get(name, options)
	case devops.TypeImageRegistry:
		return devopsTool.devopsV1alpha1Interface.ImageRegistries().Get(name, options)
	case devops.TypeTestTool:
		return devopsTool.devopsV1alpha1Interface.TestTools().Get(name, options)
	case devops.TypeArtifactRegistryManager:
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistryManagers().Get(name, options)
	case devops.TypeArtifactRegistry:
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistries().Get(name, options)
	default:
		return nil, unKnowKindError(kind, "a known kind")
	}
}

func (devopsTool *devopsTool) Authorize(kind string, name string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error) {

	switch kind {
	case devops.TypeCodeRepoService:
		return devopsTool.devopsV1alpha1Interface.CodeRepoServices().Authorize(name, opts)
	case devops.TypeJenkins:
		return devopsTool.devopsV1alpha1Interface.Jenkinses().Authorize(name, opts)
	case devops.TypeDocumentManagement:
		return devopsTool.devopsV1alpha1Interface.DocumentManagements().Authorize(name, opts)
	case devops.TypeProjectManagement:
		return devopsTool.devopsV1alpha1Interface.ProjectManagements().Authorize(name, opts)
	case devops.TypeCodeQualityTool:
		return devopsTool.devopsV1alpha1Interface.CodeQualityTools().Authorize(name, opts)
	case devops.TypeImageRegistry:
		return devopsTool.devopsV1alpha1Interface.ImageRegistries().Authorize(name, opts)
	case devops.TypeTestTool:
		return devopsTool.devopsV1alpha1Interface.TestTools().Authorize(name, opts)
	case devops.TypeArtifactRegistryManager:
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistryManagers().Authorize(name, opts)
	case devops.TypeArtifactRegistry:
		return devopsTool.devopsV1alpha1Interface.ArtifactRegistries().Authorize(name, opts)
	default:
		return nil, unKnowKindError(kind, "a known kind")
	}
}

func (devopsTool *devopsTool) CreateProject(kind string, name string, opts *devops.CreateProjectOptions) (*devops.ProjectData, error) {
	switch kind {
	case devops.TypeCodeRepoService:
		return devopsTool.devopsV1alpha1Interface.CodeRepoServices().CreateProject(name, opts)
	case devops.TypeJenkins:
		return devopsTool.devopsV1alpha1Interface.Jenkinses().CreateProject(name, opts)
	case devops.TypeDocumentManagement:
		return devopsTool.devopsV1alpha1Interface.DocumentManagements().CreateProject(name, opts)
	case devops.TypeProjectManagement:
		return devopsTool.devopsV1alpha1Interface.ProjectManagements().CreateProject(name, opts)
	case devops.TypeCodeQualityTool:
		return devopsTool.devopsV1alpha1Interface.CodeQualityTools().CreateProject(name, opts)
	case devops.TypeImageRegistry:
		return devopsTool.devopsV1alpha1Interface.ImageRegistries().CreateProject(name, opts)
	case devops.TypeTestTool:
		return devopsTool.devopsV1alpha1Interface.TestTools().CreateProject(name, opts)
	default:
		return nil, unKnowKindError(kind, "a known kind")
	}
}

func (devopsTool *devopsTool) ListProjects(kind string, name string, opts devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	switch kind {
	case devops.TypeCodeRepoService:
		return devopsTool.devopsV1alpha1Interface.CodeRepoServices().ListProjects(name, opts)
	case devops.TypeJenkins:
		return devopsTool.devopsV1alpha1Interface.Jenkinses().ListProjects(name, opts)
	case devops.TypeDocumentManagement:
		return devopsTool.devopsV1alpha1Interface.DocumentManagements().ListProjects(name, opts)
	case devops.TypeProjectManagement:
		return devopsTool.devopsV1alpha1Interface.ProjectManagements().ListProjects(name, opts)
	case devops.TypeCodeQualityTool:
		return devopsTool.devopsV1alpha1Interface.CodeQualityTools().ListProjects(name, opts)
	case devops.TypeImageRegistry:
		return devopsTool.devopsV1alpha1Interface.ImageRegistries().ListProjects(name, opts)
	case devops.TypeTestTool:
		return devopsTool.devopsV1alpha1Interface.TestTools().ListProjects(name, opts)
	default:
		return nil, unKnowKindError(kind, "a known kind")
	}
}
