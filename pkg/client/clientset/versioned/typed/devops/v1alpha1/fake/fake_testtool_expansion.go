package fake

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	core "k8s.io/client-go/testing"
)

// Authorize adding fake method for testing
func (c *FakeTestTools) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = testtoolsResource
	action.Subresource = "authorize"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoServiceAuthorizeResponse{})
	result = obj.(*devops.CodeRepoServiceAuthorizeResponse)
	return result, err
}

func (c *FakeTestTools) ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("testtools"), "ListProjects")
}

func (c *FakeTestTools) CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("testtools"), "CreateProject")
}
