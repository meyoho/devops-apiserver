package fake

import (
	"io"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	core "k8s.io/client-go/testing"
)

// GetLogs adding fake log method for testing
func (c *FakePipelines) GetLogs(name string, opts *v1alpha1.PipelineLogOptions) (result *v1alpha1.PipelineLog, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "logs"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.PipelineLog{})
	result = obj.(*v1alpha1.PipelineLog)
	return result, err
}

// GetTasks adding fake log method for testing
func (c *FakePipelines) GetTasks(name string, opts *v1alpha1.PipelineTaskOptions) (result *v1alpha1.PipelineTask, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "tasks"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.PipelineTask{})
	result = obj.(*v1alpha1.PipelineTask)
	return result, err
}

// GetTestReports fetch test reports from Jenkins
func (c *FakePipelines) GetTestReports(name string, opts *v1alpha1.PipelineTestReportOptions) (result *v1alpha1.PipelineTestReport, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "testReports"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.PipelineTestReport{})
	result = obj.(*v1alpha1.PipelineTestReport)
	return result, err
}

// GetTasks adding fake log method for testing
func (c *FakePipelines) GetArtifacts(name string) (result *v1alpha1.PipelineArtifactList, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "artifacts"
	obj, err := c.Fake.Invokes(action, &v1alpha1.PipelineArtifactList{})
	result = obj.(*v1alpha1.PipelineArtifactList)
	return result, err
}

// GetTestReports fetch download  from Jenkins
func (c *FakePipelines) DownloadArtifact(name string, opts *v1alpha1.DownloadOption) (stream io.ReadCloser, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "download"
	action.Value = opts
	return nil, err
}

// GetView get a view of process information
func (c *FakePipelines) GetView(name string) (*v1alpha1.PipelineViewResult, error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "view"
	obj, err := c.Fake.Invokes(action, &v1alpha1.PipelineViewResult{})
	result := obj.(*v1alpha1.PipelineViewResult)
	return result, err
}
