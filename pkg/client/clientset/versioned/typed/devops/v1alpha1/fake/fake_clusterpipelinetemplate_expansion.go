package fake

import (
	v1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	testing "k8s.io/client-go/testing"
)

// Preview takes the representation of a jenkinsfilePreviewOptions and creates it.  Returns the server's representation of the jenkinsfilePreview, and an error, if there is any.
func (c *FakeClusterPipelineTemplates) Preview(clusterPipelineTemplateName string, jenkinsfilePreviewOptions *v1alpha1.JenkinsfilePreviewOptions) (result *v1alpha1.JenkinsfilePreview, err error) {
	action := testing.GenericActionImpl{}
	action.Verb = "create"
	action.Resource = clusterpipelinetemplatesResource
	action.Subresource = "preview"
	action.Value = jenkinsfilePreviewOptions
	obj, err := c.Fake.Invokes(action, &v1alpha1.JenkinsfilePreview{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.JenkinsfilePreview), err
}

// Exports for ClusterPipelineTemplate
func (c *FakeClusterPipelineTemplates) Exports(name string, opts *v1alpha1.ExportShowOptions) (result *v1alpha1.PipelineExportedVariables, err error) {
	action := testing.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = clusterpipelinetemplatesResource
	action.Subresource = "exports"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.PipelineExportedVariables{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.PipelineExportedVariables), err
}
