package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	core "k8s.io/client-go/testing"
)

// GetLogs adding fake log method for testing
func (c *FakePipelineConfigs) GetLogs(name string, opts *v1alpha1.PipelineConfigLogOptions) (result *v1alpha1.PipelineConfigLog, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = pipelinesResource
	action.Subresource = "logs"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.PipelineLog{})
	result = obj.(*v1alpha1.PipelineConfigLog)
	return result, err
}
