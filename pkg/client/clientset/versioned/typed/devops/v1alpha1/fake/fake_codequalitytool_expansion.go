package fake

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	core "k8s.io/client-go/testing"
)

// Authorize adding fake method for testing
func (c *FakeCodeQualityTools) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = codequalitytoolsResource
	action.Subresource = "authorize"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoServiceAuthorizeResponse{})
	result = obj.(*devops.CodeRepoServiceAuthorizeResponse)
	return result, err
}

// ListProjects will list projects in codeQualityTools
func (c *FakeCodeQualityTools) ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("codequalitytools"), "ListProjects")
}

// CreateProject will create project in codeQualityTools
func (c *FakeCodeQualityTools) CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	return nil, errors.NewMethodNotSupported(devops.Resource("codequalitytools"), "CreateProject")
}

// CreateProject will create project in codeQualityTools
func (c *FakeCodeQualityTools) GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = codequalitytoolsResource
	action.Subresource = "roles"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectData{})
	result = obj.(*devops.RoleMapping)
	return result, err
}

// CreateProject will create project in codeQualityTools
func (c *FakeCodeQualityTools) ApplyRoleMapping(serviceName string, opts *devops.RoleMapping) (result *devops.RoleMapping, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "post"
	action.Resource = codequalitytoolsResource
	action.Subresource = "roles"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectData{})
	result = obj.(*devops.RoleMapping)
	return result, err
}
