package fake

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	core "k8s.io/client-go/testing"
)

// Authorize adding fake method for testing
func (c *FakeCodeRepoServices) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = codereposervicesResource
	action.Subresource = "authorize"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoServiceAuthorizeResponse{})
	result = obj.(*devops.CodeRepoServiceAuthorizeResponse)
	return result, err
}

// ProcessAuthorization adding fake method for testing
func (c *FakeCodeRepoServices) ProcessAuthorization(serviceName string, opts *devops.CodeRepoServiceAuthorizeCreate) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "create"
	action.Resource = codereposervicesResource
	action.Subresource = "authorize"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoServiceAuthorizeResponse{})
	result = obj.(*devops.CodeRepoServiceAuthorizeResponse)
	return result, err
}

func (c *FakeCodeRepoServices) ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = codereposervicesResource
	action.Subresource = "projects"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectDataList{})
	result = obj.(*devops.ProjectDataList)
	return result, err
}

func (c *FakeCodeRepoServices) CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "create"
	action.Resource = codereposervicesResource
	action.Subresource = "projects"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectData{})
	result = obj.(*devops.ProjectData)
	return result, err
}

// CreateProject will create project
func (c *FakeCodeRepoServices) GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = codereposervicesResource
	action.Subresource = "roles"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectData{})
	result = obj.(*devops.RoleMapping)
	return result, err
}

// CreateProject will create project
func (c *FakeCodeRepoServices) ApplyRoleMapping(serviceName string, opts *devops.RoleMapping) (result *devops.RoleMapping, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "post"
	action.Resource = codereposervicesResource
	action.Subresource = "roles"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.ProjectData{})
	result = obj.(*devops.RoleMapping)
	return result, err
}
