/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	v1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeJenkinses implements JenkinsInterface
type FakeJenkinses struct {
	Fake *FakeDevopsV1alpha1
}

var jenkinsesResource = schema.GroupVersionResource{Group: "devops.alauda.io", Version: "v1alpha1", Resource: "jenkinses"}

var jenkinsesKind = schema.GroupVersionKind{Group: "devops.alauda.io", Version: "v1alpha1", Kind: "Jenkins"}

// Get takes name of the jenkins, and returns the corresponding jenkins object, and an error if there is any.
func (c *FakeJenkinses) Get(name string, options v1.GetOptions) (result *v1alpha1.Jenkins, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootGetAction(jenkinsesResource, name), &v1alpha1.Jenkins{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.Jenkins), err
}

// List takes label and field selectors, and returns the list of Jenkinses that match those selectors.
func (c *FakeJenkinses) List(opts v1.ListOptions) (result *v1alpha1.JenkinsList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootListAction(jenkinsesResource, jenkinsesKind, opts), &v1alpha1.JenkinsList{})
	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &v1alpha1.JenkinsList{ListMeta: obj.(*v1alpha1.JenkinsList).ListMeta}
	for _, item := range obj.(*v1alpha1.JenkinsList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested jenkinses.
func (c *FakeJenkinses) Watch(opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewRootWatchAction(jenkinsesResource, opts))
}

// Create takes the representation of a jenkins and creates it.  Returns the server's representation of the jenkins, and an error, if there is any.
func (c *FakeJenkinses) Create(jenkins *v1alpha1.Jenkins) (result *v1alpha1.Jenkins, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootCreateAction(jenkinsesResource, jenkins), &v1alpha1.Jenkins{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.Jenkins), err
}

// Update takes the representation of a jenkins and updates it. Returns the server's representation of the jenkins, and an error, if there is any.
func (c *FakeJenkinses) Update(jenkins *v1alpha1.Jenkins) (result *v1alpha1.Jenkins, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootUpdateAction(jenkinsesResource, jenkins), &v1alpha1.Jenkins{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.Jenkins), err
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().
func (c *FakeJenkinses) UpdateStatus(jenkins *v1alpha1.Jenkins) (*v1alpha1.Jenkins, error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootUpdateSubresourceAction(jenkinsesResource, "status", jenkins), &v1alpha1.Jenkins{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.Jenkins), err
}

// Delete takes name of the jenkins and deletes it. Returns an error if one occurs.
func (c *FakeJenkinses) Delete(name string, options *v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewRootDeleteAction(jenkinsesResource, name), &v1alpha1.Jenkins{})
	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeJenkinses) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	action := testing.NewRootDeleteCollectionAction(jenkinsesResource, listOptions)

	_, err := c.Fake.Invokes(action, &v1alpha1.JenkinsList{})
	return err
}

// Patch applies the patch and returns the patched jenkins.
func (c *FakeJenkinses) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.Jenkins, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewRootPatchSubresourceAction(jenkinsesResource, name, pt, data, subresources...), &v1alpha1.Jenkins{})
	if obj == nil {
		return nil, err
	}
	return obj.(*v1alpha1.Jenkins), err
}
