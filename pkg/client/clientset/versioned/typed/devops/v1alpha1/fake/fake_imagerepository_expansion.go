package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	core "k8s.io/client-go/testing"
)

// GetVulnerability adding fake get image vulnerability for testing
func (c *FakeImageRepositories) GetVulnerability(name string, opts *v1alpha1.ImageScanOptions) (result *v1alpha1.VulnerabilityList, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = imagerepositoriesResource
	action.Subresource = "security"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.VulnerabilityList{})
	result = obj.(*v1alpha1.VulnerabilityList)
	return result, err
}

// GetImageTags adding fake get image vulnerability for testing
func (c *FakeImageRepositories) GetImageTags(name string, opts *v1alpha1.ImageTagOptions) (result *v1alpha1.ImageTagResult, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = imagerepositoriesResource
	action.Subresource = "tags"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.ImageTagResult{})
	result = obj.(*v1alpha1.ImageTagResult)
	return result, err
}

func (c *FakeImageRepositories) Remote(name string, opts *v1alpha1.ImageRepositoryOptions) (result *v1alpha1.ImageRepositoryRemoteStatus, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = imageregistriesResource
	action.Subresource = "remote"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.ImageRepositoryRemoteStatus{})
	result = obj.(*v1alpha1.ImageRepositoryRemoteStatus)
	return result, err
}

func (c *FakeImageRepositories) Link(name string, opts *v1alpha1.ImageRepositoryOptions) (result *v1alpha1.ImageRepositoryLink, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = imageregistriesResource
	action.Subresource = "remote"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.ImageRepositoryLink{})
	result = obj.(*v1alpha1.ImageRepositoryLink)
	return result, err
}
