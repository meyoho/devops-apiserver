package fake

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	testing "k8s.io/client-go/testing"
)

func (c *FakeCodeQualityBindings) Projects(serviceName string, opts *devops.CorrespondCodeQualityProjectsOptions) (*devops.CodeQualityProjectList, error) {
	action := testing.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = codequalitybindingsResource
	action.Subresource = "projects"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &devops.CodeQualityProjectList{})
	return obj.(*devops.CodeQualityProjectList), err
}
