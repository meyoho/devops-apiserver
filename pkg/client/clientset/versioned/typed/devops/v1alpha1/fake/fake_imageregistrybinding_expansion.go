package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	core "k8s.io/client-go/testing"
)

// GetImageRepos adding fake remote repository list method for testing
func (c *FakeImageRegistryBindings) GetImageRepos(name string) (result *v1alpha1.ImageRegistryBindingRepositories, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = imageregistrybindingsResource
	action.Subresource = "repositories"
	obj, err := c.Fake.Invokes(action, &v1alpha1.ImageRegistryBindingRepositories{})
	result = obj.(*v1alpha1.ImageRegistryBindingRepositories)
	return result, err
}

// CreateImageProject adding fake create project method for testing
func (c *FakeImageRegistryBindings) CreateImageProject(name string, opts *v1alpha1.CreateProjectOptions) (result *v1alpha1.ProjectData, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "create"
	action.Namespace = c.ns
	action.Resource = imageregistrybindingsResource
	action.Subresource = "projects"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.ProjectData{})
	result = obj.(*v1alpha1.ProjectData)
	return result, err
}

// GetImageProjects adding fake get project list method for testing
func (c *FakeImageRegistryBindings) GetImageProjects(name string, opts *v1alpha1.ListProjectOptions) (result *v1alpha1.ProjectDataList, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = imageregistrybindingsResource
	action.Subresource = "projects"
	action.Value = opts
	obj, err := c.Fake.Invokes(action, &v1alpha1.ProjectDataList{})
	result = obj.(*v1alpha1.ProjectDataList)
	return
}
