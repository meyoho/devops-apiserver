package fake

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	core "k8s.io/client-go/testing"
)

// Authorize will check whether the secret is available
func (c *FakeToolCategories) Setting() (result *devops.SettingResp, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = artifactregistrymanagersResource
	action.Subresource = "settings"
	obj, err := c.Fake.Invokes(action, &devops.CodeRepoServiceAuthorizeResponse{})
	result = obj.(*devops.SettingResp)
	return result, err
}
