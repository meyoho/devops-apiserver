package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	core "k8s.io/client-go/testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
)

// Authorize adding fake method for testing
func (c *FakeProjectManagementBindings) IssueOptions(name string, issuetype *v1alpha1.IssueSearchOptions) (result *v1alpha1.IssueFilterDataList, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = projectmanagementsResource
	action.Subresource = "issueoptions"
	action.Value = issuetype
	obj, err := c.Fake.Invokes(action, &devops.IssueSearchOptions{})
	result = obj.(*v1alpha1.IssueFilterDataList)
	return result, err
}
func (p *FakeProjectManagementBindings) IssueDetail(name string, issuekey *v1alpha1.IssueKeyOptions) (result *v1alpha1.IssueDetail, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = projectmanagementsResource
	action.Subresource = "issue"
	action.Value = issuekey
	obj, err := p.Fake.Invokes(action, &devops.IssueKeyOptions{})
	result = obj.(*v1alpha1.IssueDetail)
	return result, err
}

func (p *FakeProjectManagementBindings) IssueList(name string, issuelistoption *v1alpha1.ListIssuesOptions) (result *v1alpha1.IssueDetailList, err error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Resource = imageregistrybindingsResource
	action.Subresource = "issueslist"
	action.Value = issuelistoption
	obj, err := p.Fake.Invokes(action, &devops.ListIssuesOptions{})
	result = obj.(*v1alpha1.IssueDetailList)
	return result, err
}
