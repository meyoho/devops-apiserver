package fake

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	core "k8s.io/client-go/testing"
)

type JenkinsBindingExpansion interface {
	GetJenkinsInfo(jenkinsBindingName string, options *v1alpha1.JenkinsBindingInfoOptions) (*v1alpha1.JenkinsBindingInfoResult, error)
}

func (c *FakeJenkinsBindings) GetJenkinsInfo(jenkinsBindingName string, options *v1alpha1.JenkinsBindingInfoOptions) (*v1alpha1.JenkinsBindingInfoResult, error) {
	action := core.GenericActionImpl{}
	action.Verb = "get"
	action.Namespace = c.ns
	action.Resource = jenkinsbindingsResource
	action.Subresource = "info"
	action.Value = options
	obj, err := c.Fake.Invokes(action, &v1alpha1.JenkinsBindingInfoResult{})
	result := obj.(*v1alpha1.JenkinsBindingInfoResult)

	return result, err
}
