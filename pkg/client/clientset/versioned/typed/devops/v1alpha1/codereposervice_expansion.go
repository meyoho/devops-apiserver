package v1alpha1

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

// The CodeRepoServiceExpansion interface allows manually adding extra methods to the CodeRepoServiceInterface.
type CodeRepoServiceExpansion interface {
	Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error)
	ProcessAuthorization(serviceName string, opts *devops.CodeRepoServiceAuthorizeCreate) (*devops.CodeRepoServiceAuthorizeResponse, error)

	CreateProject(serviceName string, opts *devops.CreateProjectOptions) (*devops.ProjectData, error)
	ListProjects(serviceName string, opts devops.ListProjectOptions) (*devops.ProjectDataList, error)

	GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error)
	ApplyRoleMapping(serviceName string, opts *devops.RoleMapping) (result *devops.RoleMapping, err error)
}

// Authorize will check whether the secret is available
func (c *codeRepoServices) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	result = &devops.CodeRepoServiceAuthorizeResponse{}
	err = c.client.Get().
		Resource("codereposervices").
		Name(serviceName).
		SubResource("authorize").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

// ProcessAuthorization will process an authorization request for oAuth2
func (c *codeRepoServices) ProcessAuthorization(serviceName string, opts *devops.CodeRepoServiceAuthorizeCreate) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	result = &devops.CodeRepoServiceAuthorizeResponse{}
	err = c.client.Post().
		Resource("codereposervices").
		Name(serviceName).
		SubResource("authorize").
		Body(opts).
		Do().
		Into(result)
	return
}

// ListProjects will list projects in codereposervice
func (c *codeRepoServices) ListProjects(serviceName string, opts devops.ListProjectOptions) (result *devops.ProjectDataList, err error) {
	result = &devops.ProjectDataList{}
	err = c.client.Get().
		Resource("codereposervices").
		Name(serviceName).
		SubResource("projects").
		VersionedParams(&opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// CreateProject will create project(organization or group ) in codereposervice
func (c *codeRepoServices) CreateProject(serviceName string, opts *devops.CreateProjectOptions) (result *devops.ProjectData, err error) {
	result = &devops.ProjectData{}
	err = c.client.Post().
		Resource("codereposervices").
		Name(serviceName).
		SubResource("projects").
		Body(opts).
		Do().
		Into(result)
	return
}

func (c *codeRepoServices) GetRoleMapping(serviceName string, opts *devops.RoleMappingListOptions) (result *devops.RoleMapping, err error) {
	result = &devops.RoleMapping{}
	err = c.client.Get().
		Resource("codereposervices").
		Name(serviceName).
		SubResource("roles").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

func (c *codeRepoServices) ApplyRoleMapping(serviceName string, opts *devops.RoleMapping) (result *devops.RoleMapping, err error) {
	result = &devops.RoleMapping{}
	err = c.client.Post().
		Resource("codereposervices").
		Name(serviceName).
		SubResource("roles").
		Body(opts).
		Do().
		Into(result)
	return
}
