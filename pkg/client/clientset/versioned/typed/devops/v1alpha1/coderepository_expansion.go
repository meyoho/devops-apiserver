package v1alpha1

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
)

type CodeRepositoryExpansion interface {
	GetBranches(name string, opts *devops.CodeRepoBranchOptions) (result *devops.CodeRepoBranchResult, err error)
}

// Authorize will check whether the secret is available
func (c *codeRepositories) GetBranches(name string, opts *devops.CodeRepoBranchOptions) (result *devops.CodeRepoBranchResult, err error) {
	result = &devops.CodeRepoBranchResult{}
	err = c.client.Get().
		Resource("coderepositories").
		Name(name).
		Namespace(c.ns).
		SubResource("branches").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}
