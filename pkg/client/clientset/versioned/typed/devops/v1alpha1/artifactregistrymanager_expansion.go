package v1alpha1

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/internalversion/scheme"
	glog "k8s.io/klog"
)

//ArtifactRegistryManagerExpansion expasion of ProjectManagement client
type ArtifactRegistryManagerExpansion interface {
	Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (*devops.CodeRepoServiceAuthorizeResponse, error)
	InitManager(serviceName string, opts *devops.ArtifactRegistryManagerOptions) error
}

// Authorize will check whether the secret is available
func (c *artifactRegistryManagers) Authorize(serviceName string, opts *devops.CodeRepoServiceAuthorizeOptions) (result *devops.CodeRepoServiceAuthorizeResponse, err error) {
	result = &devops.CodeRepoServiceAuthorizeResponse{}
	glog.V(9).Infof("arm authorize opts is %#v", opts)
	err = c.client.Get().
		Resource("artifactregistrymanagers").
		Name(serviceName).
		SubResource("authorize").
		VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Into(result)
	return
}

// InitManager will post scripts to manager
func (c *artifactRegistryManagers) InitManager(serviceName string, opts *devops.ArtifactRegistryManagerOptions) error {
	return c.client.Post().
		Resource("artifactregistrymanagers").
		Name(serviceName).
		SubResource("init").
		//VersionedParams(opts, scheme.ParameterCodec).
		Body(opts).
		Do().
		Error()
}
