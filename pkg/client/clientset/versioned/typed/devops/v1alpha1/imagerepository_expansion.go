package v1alpha1

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned/scheme"
)

// ImageRepositoryExpansion expansion methods for client
type ImageRepositoryExpansion interface {
	GetVulnerability(name string, opts *v1alpha1.ImageScanOptions) (result *v1alpha1.VulnerabilityList, err error)
	GetImageTags(name string, opts *v1alpha1.ImageTagOptions) (result *v1alpha1.ImageTagResult, err error)
	Remote(name string, opts *v1alpha1.ImageRepositoryOptions) (result *v1alpha1.ImageRepositoryRemoteStatus, err error)
	Link(name string, opts *v1alpha1.ImageRepositoryOptions) (result *v1alpha1.ImageRepositoryLink, err error)
}

// Get scan image vulnerability with repository and name, return the vulnerability list
func (c *imageRepositories) GetVulnerability(name string, opts *v1alpha1.ImageScanOptions) (result *v1alpha1.VulnerabilityList, err error) {
	result = &v1alpha1.VulnerabilityList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imagerepositories").
		Name(name).
		SubResource("security").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *imageRepositories) GetImageTags(name string, opts *v1alpha1.ImageTagOptions) (result *v1alpha1.ImageTagResult, err error) {
	result = &v1alpha1.ImageTagResult{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imagerepositories").
		Name(name).
		SubResource("tags").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *imageRepositories) Remote(name string, opts *v1alpha1.ImageRepositoryOptions) (result *v1alpha1.ImageRepositoryRemoteStatus, err error) {
	result = &v1alpha1.ImageRepositoryRemoteStatus{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imagerepositories").
		Name(name).
		SubResource("remote").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

func (c *imageRepositories) Link(name string, opts *v1alpha1.ImageRepositoryOptions) (result *v1alpha1.ImageRepositoryLink, err error) {
	result = &v1alpha1.ImageRepositoryLink{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("imagerepositories").
		Name(name).
		SubResource("link").
		VersionedParams(opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}
