package versioned

import (
	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/client/clientset/versioned/typed/devops/v1alpha1"
)

type InterfaceExpansion interface {
	Interface
	DevopsV1alpha1Expansion() devopsv1alpha1.DevopsV1alpha1InterfaceExpansion
}

type interfaceExpansion struct {
	Interface
}

func (expansion *interfaceExpansion) DevopsV1alpha1Expansion() devopsv1alpha1.DevopsV1alpha1InterfaceExpansion {
	return devopsv1alpha1.NewDevopsV1alpha1InterfaceExpansion(expansion.Interface.DevopsV1alpha1())
}

func NewExpansion(inter Interface) InterfaceExpansion {
	return &interfaceExpansion{
		Interface: inter,
	}
}
