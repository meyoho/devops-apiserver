/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by informer-gen. DO NOT EDIT.

package internalversion

import (
	time "time"

	devops "alauda.io/devops-apiserver/pkg/apis/devops"
	clientsetinternalversion "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	internalinterfaces "alauda.io/devops-apiserver/pkg/client/informers/internalversion/internalinterfaces"
	internalversion "alauda.io/devops-apiserver/pkg/client/listers/devops/internalversion"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
	watch "k8s.io/apimachinery/pkg/watch"
	cache "k8s.io/client-go/tools/cache"
)

// CodeQualityBindingInformer provides access to a shared informer and lister for
// CodeQualityBindings.
type CodeQualityBindingInformer interface {
	Informer() cache.SharedIndexInformer
	Lister() internalversion.CodeQualityBindingLister
}

type codeQualityBindingInformer struct {
	factory          internalinterfaces.SharedInformerFactory
	tweakListOptions internalinterfaces.TweakListOptionsFunc
	namespace        string
}

// NewCodeQualityBindingInformer constructs a new informer for CodeQualityBinding type.
// Always prefer using an informer factory to get a shared informer instead of getting an independent
// one. This reduces memory footprint and number of connections to the server.
func NewCodeQualityBindingInformer(client clientsetinternalversion.Interface, namespace string, resyncPeriod time.Duration, indexers cache.Indexers) cache.SharedIndexInformer {
	return NewFilteredCodeQualityBindingInformer(client, namespace, resyncPeriod, indexers, nil)
}

// NewFilteredCodeQualityBindingInformer constructs a new informer for CodeQualityBinding type.
// Always prefer using an informer factory to get a shared informer instead of getting an independent
// one. This reduces memory footprint and number of connections to the server.
func NewFilteredCodeQualityBindingInformer(client clientsetinternalversion.Interface, namespace string, resyncPeriod time.Duration, indexers cache.Indexers, tweakListOptions internalinterfaces.TweakListOptionsFunc) cache.SharedIndexInformer {
	return cache.NewSharedIndexInformer(
		&cache.ListWatch{
			ListFunc: func(options v1.ListOptions) (runtime.Object, error) {
				if tweakListOptions != nil {
					tweakListOptions(&options)
				}
				return client.Devops().CodeQualityBindings(namespace).List(options)
			},
			WatchFunc: func(options v1.ListOptions) (watch.Interface, error) {
				if tweakListOptions != nil {
					tweakListOptions(&options)
				}
				return client.Devops().CodeQualityBindings(namespace).Watch(options)
			},
		},
		&devops.CodeQualityBinding{},
		resyncPeriod,
		indexers,
	)
}

func (f *codeQualityBindingInformer) defaultInformer(client clientsetinternalversion.Interface, resyncPeriod time.Duration) cache.SharedIndexInformer {
	return NewFilteredCodeQualityBindingInformer(client, f.namespace, resyncPeriod, cache.Indexers{cache.NamespaceIndex: cache.MetaNamespaceIndexFunc}, f.tweakListOptions)
}

func (f *codeQualityBindingInformer) Informer() cache.SharedIndexInformer {
	return f.factory.InformerFor(&devops.CodeQualityBinding{}, f.defaultInformer)
}

func (f *codeQualityBindingInformer) Lister() internalversion.CodeQualityBindingLister {
	return internalversion.NewCodeQualityBindingLister(f.Informer().GetIndexer())
}
