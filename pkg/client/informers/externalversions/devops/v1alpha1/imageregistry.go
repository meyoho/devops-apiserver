/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by informer-gen. DO NOT EDIT.

package v1alpha1

import (
	time "time"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	versioned "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	internalinterfaces "alauda.io/devops-apiserver/pkg/client/informers/externalversions/internalinterfaces"
	v1alpha1 "alauda.io/devops-apiserver/pkg/client/listers/devops/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
	watch "k8s.io/apimachinery/pkg/watch"
	cache "k8s.io/client-go/tools/cache"
)

// ImageRegistryInformer provides access to a shared informer and lister for
// ImageRegistries.
type ImageRegistryInformer interface {
	Informer() cache.SharedIndexInformer
	Lister() v1alpha1.ImageRegistryLister
}

type imageRegistryInformer struct {
	factory          internalinterfaces.SharedInformerFactory
	tweakListOptions internalinterfaces.TweakListOptionsFunc
}

// NewImageRegistryInformer constructs a new informer for ImageRegistry type.
// Always prefer using an informer factory to get a shared informer instead of getting an independent
// one. This reduces memory footprint and number of connections to the server.
func NewImageRegistryInformer(client versioned.Interface, resyncPeriod time.Duration, indexers cache.Indexers) cache.SharedIndexInformer {
	return NewFilteredImageRegistryInformer(client, resyncPeriod, indexers, nil)
}

// NewFilteredImageRegistryInformer constructs a new informer for ImageRegistry type.
// Always prefer using an informer factory to get a shared informer instead of getting an independent
// one. This reduces memory footprint and number of connections to the server.
func NewFilteredImageRegistryInformer(client versioned.Interface, resyncPeriod time.Duration, indexers cache.Indexers, tweakListOptions internalinterfaces.TweakListOptionsFunc) cache.SharedIndexInformer {
	return cache.NewSharedIndexInformer(
		&cache.ListWatch{
			ListFunc: func(options v1.ListOptions) (runtime.Object, error) {
				if tweakListOptions != nil {
					tweakListOptions(&options)
				}
				return client.DevopsV1alpha1().ImageRegistries().List(options)
			},
			WatchFunc: func(options v1.ListOptions) (watch.Interface, error) {
				if tweakListOptions != nil {
					tweakListOptions(&options)
				}
				return client.DevopsV1alpha1().ImageRegistries().Watch(options)
			},
		},
		&devopsv1alpha1.ImageRegistry{},
		resyncPeriod,
		indexers,
	)
}

func (f *imageRegistryInformer) defaultInformer(client versioned.Interface, resyncPeriod time.Duration) cache.SharedIndexInformer {
	return NewFilteredImageRegistryInformer(client, resyncPeriod, cache.Indexers{cache.NamespaceIndex: cache.MetaNamespaceIndexFunc}, f.tweakListOptions)
}

func (f *imageRegistryInformer) Informer() cache.SharedIndexInformer {
	return f.factory.InformerFor(&devopsv1alpha1.ImageRegistry{}, f.defaultInformer)
}

func (f *imageRegistryInformer) Lister() v1alpha1.ImageRegistryLister {
	return v1alpha1.NewImageRegistryLister(f.Informer().GetIndexer())
}
