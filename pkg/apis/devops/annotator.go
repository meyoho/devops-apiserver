package devops

import (
	"regexp"
	"strconv"

	"k8s.io/apimachinery/pkg/util/validation"
)

func init() {
	// add CodeRepoService
	addAnnotationToolFunc(TypeCodeRepoService, annotateCodeRepoService)
	addAnnotationToolFunc(TypeJenkins, annotateJenkins)
	addAnnotationToolFunc(TypeImageRegistry, annotateImageRegistry)
	addAnnotationToolFunc(TypeProjectManagement, annotateProjectManagement)
	addAnnotationToolFunc(TypeCodeQualityTool, annotateCodeQualityTool)
	addAnnotationToolFunc(TypeArtifactRegistry, annotateAr)
	addAnnotationToolFunc(TypeArtifactRegistryManager, annotateArm)
}

// AnnotateTool general internal function to annotate specific tools according to
// its own schema
func AnnotateTool(tool ToolInterface, provider AnnotationProvider) map[string]string {
	if tool == nil {
		return nil
	}

	annotations := defaultToolFunc(tool, provider)
	// if has a specific tool it should use it
	if function, ok := annotationsFunctions[tool.GetKind()]; ok {
		for k, v := range function(tool, provider) {
			annotations[k] = v
		}
	}
	return annotations
}

// AnnotateToolBindingReplica annotate ToolBindingReplica according the tool
func AnnotateToolBindingReplica(tool ToolInterface, provider AnnotationProvider) map[string]string {
	if tool == nil {
		return nil
	}

	annotations := map[string]string{}
	annotations[provider.AnnotationsToolHttpHost()] = tool.GetToolSpec().HTTP.Host
	annotations[provider.AnnotationsToolAccessURL()] = tool.GetToolSpec().HTTP.AccessURL
	annotations[provider.AnnotationsToolItemKind()] = tool.GetKind()
	annotations[provider.AnnotationsToolItemType()] = tool.GetKindType()
	annotations[provider.AnnotationsToolName()] = tool.GetObjectMeta().GetName()

	if tool.GetObjectMeta().GetAnnotations() != nil {
		if project, ok := tool.GetObjectMeta().GetAnnotations()[provider.AnnotationsKeyProject()]; ok {
			annotations[provider.AnnotationsToolItemProject()] = project
		}
	}

	return annotations
}

// CurateLabels removes all non valid label values
func CurateLabels(labels map[string]string) map[string]string {

	if labels == nil {
		return labels
	}
	toDelete := []string{}
	for k, v := range labels {
		err := validation.IsValidLabelValue(v)
		if len(err) != 0 {
			toDelete = append(toDelete, k)
		}
	}
	for _, k := range toDelete {
		delete(labels, k)
	}
	return labels
}

var labelValueRegex = regexp.MustCompile("(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])?")

// addAnnotationToolFunc internal method to add a annotation function for a kind
func addAnnotationToolFunc(kind string, function annotateToolFunc) {
	annotationsFunctions[kind] = function
}

// annotateToolFunc specific function to annotate a tool
type annotateToolFunc func(ToolInterface, AnnotationProvider) map[string]string

var annotationsFunctions = map[string]annotateToolFunc{}

// defaultToolFunc default no-op function for annotating tools
func defaultToolFunc(tool ToolInterface, provider AnnotationProvider) (annotations map[string]string) {
	annotations = tool.GetObjectMeta().GetAnnotations()
	if annotations == nil {
		annotations = map[string]string{}
	}
	// is subscription?
	if value, ok := annotations[provider.AnnotationsToolSubscription()]; !ok && value == "" {
		annotations[provider.AnnotationsToolSubscription()] = FalseString
	}
	// get kind
	if value, ok := annotations[provider.AnnotationsToolItemKind()]; !ok && value == "" {
		annotations[provider.AnnotationsToolItemKind()] = tool.GetKind()
	}
	// get kind type
	if value, ok := annotations[provider.AnnotationsToolItemType()]; !ok && value == "" {
		annotations[provider.AnnotationsToolItemType()] = tool.GetKindType()
	}
	return
}

// annotateCodeRepoService internal function to annotate CodeRepoService in admission process
func annotateCodeRepoService(tool ToolInterface, provider AnnotationProvider) (annotations map[string]string) {
	annotations = map[string]string{}
	if codeRepoService, ok := tool.(*CodeRepoService); ok {
		annotations[provider.AnnotationsToolItemPublic()] = strconv.FormatBool(codeRepoService.Spec.Public)
		annotations[provider.AnnotationsToolItemKind()] = codeRepoService.Spec.Type.String()
		annotations[provider.AnnotationsCreateAppUrl()] = codeRepoService.GetHtml()
		annotations[provider.AnnotationsToolType()] = ToolChainCodeRepositoryName
	}
	return
}

// annotateImageRegistry internal function to annotate ImageRegistry in admission process
func annotateImageRegistry(tool ToolInterface, provider AnnotationProvider) (annotations map[string]string) {
	annotations = map[string]string{}
	if imageRegistry, ok := tool.(*ImageRegistry); ok {
		annotations[provider.AnnotationsToolItemPublic()] = strconv.FormatBool(imageRegistry.Spec.Type == RegistryTypeDockerHub)
		annotations[provider.AnnotationsToolItemKind()] = imageRegistry.Spec.Type.String()
		annotations[provider.AnnotationsCreateAppUrl()] = imageRegistry.GetEndpoint()
		annotations[provider.AnnotationsToolType()] = ToolChainArtifactRepositoryName
	}
	return
}

// annotateJenkins internal function to annotate Jenkins in admission process
func annotateJenkins(tool ToolInterface, provider AnnotationProvider) (annotations map[string]string) {
	annotations = map[string]string{}
	if jenkins, ok := tool.(*Jenkins); ok {
		annotations[provider.AnnotationsToolItemPublic()] = FalseString
		annotations[provider.AnnotationsToolItemKind()] = TypeJenkins
		annotations[provider.AnnotationsCreateAppUrl()] = jenkins.GetHostPort().Host
		annotations[provider.AnnotationsToolType()] = ToolChainContinuousIntegrationName
	}
	return
}

// annotateProjectManagement internal function to annotate ProjectManagement objects in admission process
func annotateProjectManagement(tool ToolInterface, provider AnnotationProvider) (annotations map[string]string) {
	annotations = map[string]string{}
	if projectManagement, ok := tool.(*ProjectManagement); ok {
		annotations[provider.AnnotationsToolItemPublic()] = FalseString
		annotations[provider.AnnotationsToolItemKind()] = projectManagement.GetKindType()
		annotations[provider.AnnotationsCreateAppUrl()] = projectManagement.GetHostPort().Host
		annotations[provider.AnnotationsToolType()] = ToolChainProjectManagementName
	}
	return
}

// annotateCodeQualityTool internal function to annotate CodeQualityTool objects in admission process
func annotateCodeQualityTool(tool ToolInterface, provider AnnotationProvider) (annotations map[string]string) {
	annotations = map[string]string{}
	if codeQualityTool, ok := tool.(*CodeQualityTool); ok {
		annotations[provider.AnnotationsToolItemPublic()] = FalseString
		annotations[provider.AnnotationsToolItemKind()] = codeQualityTool.GetKindType()
		annotations[provider.AnnotationsCreateAppUrl()] = codeQualityTool.GetHostPort().Host
		annotations[provider.AnnotationsToolType()] = ToolChainCodeQualityToolName
	}
	return
}

func annotateAr(tool ToolInterface, provider AnnotationProvider) (annotations map[string]string) {
	annotations = map[string]string{}
	if ar, ok := tool.(*ArtifactRegistry); ok {
		annotations[provider.AnnotationsToolItemPublic()] = FalseString
		annotations[provider.AnnotationsToolItemKind()] = ""
		annotations[provider.AnnotationsCreateAppUrl()] = ar.GetHostPort().Host
		annotations[provider.AnnotationsToolType()] = ToolChainArtifactRegistryToolName
	}
	return
}

func annotateArm(tool ToolInterface, provider AnnotationProvider) (annotations map[string]string) {
	annotations = map[string]string{}
	if arm, ok := tool.(*ArtifactRegistryManager); ok {
		annotations[provider.AnnotationsToolItemPublic()] = FalseString
		annotations[provider.AnnotationsToolItemKind()] = ""
		annotations[provider.AnnotationsCreateAppUrl()] = arm.GetHostPort().Host
		annotations[provider.AnnotationsToolType()] = ToolChainArtifactRegistryManagerToolName
	}
	return
}
