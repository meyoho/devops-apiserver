package devops

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type ToolType struct {
	metav1.TypeMeta
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the Deployment.
	// +optional
	Spec ToolTypeSpec

	// Most recently observed status of the Deployment.
	// +optional
	Status ToolTypeStatus
}

type ToolTypeSpec struct {
	DisplayName ToolTypeSpecDisplayName
	// +optional
	Description ToolTypeSpecDisplayName
	// +optional
	RecommendedVersion string
	ToolCategoryRef    []ToolCategoryRef
	Enabled            bool
	//ToolRef              ToolTypeSpecToolRef
	SupportedSecretTypes []ToolTypeSpecSupportedSecretTypes
	Type                 string
	Kind                 string
	Public               bool
	Host                 string
	Html                 string
	Enterprise           bool
	ApiPath              string
	RoleSyncEnabled      bool
}

type ToolTypeStatus struct {
	Phase string
}

type ToolTypeSpecDisplayName struct {
	En string
	Zh string
}

type ToolTypeSpecToolRef struct {
	APIGroup string
	Kind     string
}

type ToolTypeSpecSupportedSecretTypes struct {
	SecretType  string
	Description ToolTypeSpecDisplayName
}

type ToolCategoryRef struct {
	APIGroup string
	Kind     string
	Name     string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ToolTypeList struct {
	metav1.TypeMeta
	// +optional
	metav1.ListMeta

	Items []ToolType
}
