package devops

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ArtifactRegistryType string
type ArtifactRegistryFormat string

const (
	// ArtifactRegistryTypeMaven2
	// +alauda:toolchain-gen:class=item,category=artifactRepository,name=Maven2,en=Maven2,zh=Maven2,apipath=artifactregistries,enabled=true,kind=artifactRegistry,type=Maven2
	// +alauda:toolchain-gen:class=secret,category=artifactRepository,itemName=Maven2,type=kubernetes.io/basic-auth,en=Input Username and Password as used on login,zh=用户名和密码均为登录时的用户名和密码
	// +alauda:rolesync-gen:class=platform,name=Maven2,sync=rbac,enabled=true,roles=project_admin:AlaudaProjectAdmin;project_auditor:AlaudaProjectAuditor;space_admin:AlaudaSpaceAdmin;space_developer:AlaudaSpaceDeveloper;space_auditor:AlaudaSpaceAuditor;namespace_admin:AlaudaSpaceAdmin;namespace_auditor:AlaudaSpaceAuditor;namespace_developer:AlaudaSpaceDeveloper

	ArtifactRegistryTypeMaven2 string = "Maven2"
)

func (c ArtifactRegistryType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// real artifact registry service
type ArtifactRegistry struct {
	// +optional
	metav1.TypeMeta
	// +optional
	metav1.ObjectMeta

	Spec ArtifactRegistrySpec
	// Read-only
	// +optional
	Status ServiceStatus
}

// real artifact registry service spec
type ArtifactRegistrySpec struct {
	// ArtifactRegistryArgs represent the argument of artifactregistryspec
	ArtifactRegistryArgs map[string]string
	// ArtifactRegistryName represent the name of artifactregistry
	ArtifactRegistryName string
	// Type represent the type of artifactregistry
	Type string
	// Type represent if this artifactregistory is public or not
	Public bool
	ToolSpec
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ArtifactRegistryList Get ArtifactRegistry list
type ArtifactRegistryList struct {
	metav1.TypeMeta
	// +optional
	metav1.ListMeta
	// Items represent all artifact in this list
	Items []ArtifactRegistry
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ArtifactRegistryCreateOptions for artifact registry create
type ArtifactRegistryCreateOptions struct {
	// +optional
	metav1.TypeMeta
	// +optional
	metav1.ObjectMeta

	Spec ArtifactRegistryCreateOptionSpec
}

type ArtifactRegistryCreateOptionSpec struct {
	ArtifactRegistryArgs map[string]string
	ArtifactRegistryName string
	Type                 string
	Public               bool
	ToolSpec
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Artifact registry create result
type ArtifactRegistryCreateResult struct {
	metav1.TypeMeta
}

var _ ToolInterface = &ArtifactRegistry{}

func (ar *ArtifactRegistry) GetKind() string {
	return TypeArtifactRegistry
}

func (ar *ArtifactRegistry) GetHostPort() HostPort {
	return ar.Spec.HTTP
}

func (ar *ArtifactRegistry) GetKindType() string {
	return ar.Spec.Type
}

func (ar *ArtifactRegistry) GetObjectMeta() metav1.Object {
	return &ar.ObjectMeta
}

func (ar *ArtifactRegistry) GetStatus() *ServiceStatus {
	return &ar.Status
}

func (ar *ArtifactRegistry) GetToolSpec() ToolSpec {
	return ar.Spec.ToolSpec
}
