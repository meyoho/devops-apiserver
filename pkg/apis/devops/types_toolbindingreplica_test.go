package devops_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestGetProjectsRef(t *testing.T) {
	type casedata struct {
		description        string
		toolBindingReplica devops.ToolBindingReplica
		projectsRef        []corev1.ObjectReference
	}

	var table = []casedata{
		{
			description: "when kind is codereposervice",
			toolBindingReplica: set(generateToolBindingReplica(devops.TypeCodeRepoService), func(replica *devops.ToolBindingReplica) {
				replica.Spec.CodeRepoService = codereposerviceTemplate()
			}),
			projectsRef: []corev1.ObjectReference{{Name: "org1"}, {Name: "org2"}},
		},
		{
			description: "when kind is imageregistry",
			toolBindingReplica: set(generateToolBindingReplica(devops.TypeImageRegistry), func(replica *devops.ToolBindingReplica) {
				replica.Spec.ImageRegistry = imageregistryTemplate()
			}),
			projectsRef: []corev1.ObjectReference{{Name: "project1"}, {Name: "project2"}},
		},
		{
			description: "when kind is jenkins",
			toolBindingReplica: set(generateToolBindingReplica(devops.TypeJenkins), func(replica *devops.ToolBindingReplica) {
				replica.Spec.ContinuousIntegration = jenkinsTemplate()
			}),
			projectsRef: []corev1.ObjectReference{},
		},
		{
			description: "when kind is codequality",
			toolBindingReplica: set(generateToolBindingReplica(devops.TypeCodeQualityTool), func(replica *devops.ToolBindingReplica) {
				replica.Spec.CodeQuality = codequalityTemplate()
			}),
			projectsRef: []corev1.ObjectReference{},
		},
		{
			description: "when kind is documentmanagement",
			toolBindingReplica: set(generateToolBindingReplica(devops.TypeDocumentManagement), func(replica *devops.ToolBindingReplica) {
				replica.Spec.DocumentManagement = documentmangementTemplate()
			}),
			projectsRef: []corev1.ObjectReference{{Name: "space1"}, {Name: "space2"}},
		},
		{
			description: "when kind is projectmanagement",
			toolBindingReplica: set(generateToolBindingReplica(devops.TypeDocumentManagement), func(replica *devops.ToolBindingReplica) {
				replica.Spec.ProjectManagement = projectmangementTemplate()
			}),
			projectsRef: []corev1.ObjectReference{},
		},
		{
			description: "when kind is artifactregistry",
			toolBindingReplica: set(generateToolBindingReplica(devops.TypeArtifactRegistry), func(replica *devops.ToolBindingReplica) {
				replica.Spec.ArtifactRegistry = artifactRegistryTemplate()
			}),
			projectsRef: []corev1.ObjectReference{},
		},
	}

	for _, item := range table {
		actual := item.toolBindingReplica.Spec.GetProjectsRef()
		assert.Equal(t, item.projectsRef, actual, item.description)
	}
}

func set(replica *devops.ToolBindingReplica, f func(replica *devops.ToolBindingReplica)) devops.ToolBindingReplica {
	f(replica)
	return *replica
}

func generateToolBindingReplica(selectorKind string) *devops.ToolBindingReplica {
	return &devops.ToolBindingReplica{
		ObjectMeta: metav1.ObjectMeta{
			Name: "case",
		},
		Spec: devops.ToolBindingReplicaSpec{
			Selector: devops.ToolSelector{
				ObjectReference: corev1.ObjectReference{
					Kind: selectorKind,
				},
			},
		},
	}
}

func codereposerviceTemplate() *devops.CodeRepoBindingReplicaTemplate {
	return &devops.CodeRepoBindingReplicaTemplate{
		Owners: []devops.OwnerInRepository{
			devops.OwnerInRepository{
				Name: "org1",
			},
			devops.OwnerInRepository{
				Name: "org2",
			},
		},
	}
}

func imageregistryTemplate() *devops.ImageRegistryBindingReplicaTemplate {
	return &devops.ImageRegistryBindingReplicaTemplate{
		RepoInfo: devops.ImageRegistryBindingRepo{
			Repositories: []string{
				"project1",
				"project2",
			},
		},
	}
}

func documentmangementTemplate() *devops.DocumentManagementBindingReplicaTemplate {
	return &devops.DocumentManagementBindingReplicaTemplate{
		Spaces: []devops.DocumentManagementSpaceRef{
			devops.DocumentManagementSpaceRef{
				Name: "space1",
			},
			devops.DocumentManagementSpaceRef{
				Name: "space2",
			},
		},
	}
}

func projectmangementTemplate() *devops.ProjectManagementBindingReplicaTemplate {
	return &devops.ProjectManagementBindingReplicaTemplate{
		Projects: []devops.ProjectManagementProjectInfo{
			devops.ProjectManagementProjectInfo{Name: "jira1"},
			devops.ProjectManagementProjectInfo{Name: "jira2"},
		},
	}
}

func jenkinsTemplate() *devops.CIBindingReplicaTemplate {
	return &devops.CIBindingReplicaTemplate{}
}

func codequalityTemplate() *devops.CodeQualityBindingReplicaTemplate {
	return &devops.CodeQualityBindingReplicaTemplate{}
}

func artifactRegistryTemplate() *devops.ArtifactRegistryBindingReplicaTemplate {
	return &devops.ArtifactRegistryBindingReplicaTemplate{}
}
