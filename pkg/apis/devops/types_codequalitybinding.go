package devops

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeQualityTool binding
type CodeQualityBinding struct {
	metav1.TypeMeta
	// +optional
	metav1.ObjectMeta

	Spec CodeQualityBindingSpec
	// Read-only
	// +optional
	Status ServiceStatus
}

func (b *CodeQualityBinding) GetSecretNamespace() string {
	namespace := b.GetNamespace()
	if b.Spec.Secret.Namespace != "" {
		namespace = b.Spec.Secret.Namespace
	}
	return namespace
}

func (b *CodeQualityBinding) GetSecretName() string {
	return b.Spec.Secret.Name
}

// CodeQualityBindingSpec represents CodeQualityBinding specs
type CodeQualityBindingSpec struct {
	CodeQualityTool LocalObjectReference
	// +optional
	Secret SecretKeySetRef
	// +optional
	CodeRepository CodeRepositoryInfo
	// +optional
	Pattern CodeRepositoryPattern
}

// CodeRepositoryInfo defines sync project model is devops binding codeRepository
type CodeRepositoryInfo struct {
	// +optional
	All bool
}

// CodeRepositoryInfo defines sync project model is prefix of name
type CodeRepositoryPattern struct {
	// +optional
	Prefix []string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeQualityBindingList is a list of CodeQualityBinding objects.
type CodeQualityBindingList struct {
	metav1.TypeMeta
	metav1.ListMeta

	Items []CodeQualityBinding
}

var _ ToolBindingLister = &CodeQualityBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *CodeQualityBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta.Kind = TypeCodeQualityBinding
	}
	return
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type CodeQualityBindingProjects struct {
	metav1.TypeMeta

	Items []string
}

func (binding *CodeQualityBinding) GetStatus() *ServiceStatus {
	return &binding.Status
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type CorrespondCodeQualityProjectsOptions struct {
	metav1.TypeMeta
	// code repository name
	Repositories string
}
