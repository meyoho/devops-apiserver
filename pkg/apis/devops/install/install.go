/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package install

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
)

/*
import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"k8s.io/apimachinery/pkg/apimachinery/announced"
	"k8s.io/apimachinery/pkg/apimachinery/registered"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/sets"
)
*/

// Install registers the API group and adds types to a scheme
/*
func Install(groupFactoryRegistry announced.APIGroupFactoryRegistry, registry *registered.APIRegistrationManager, scheme *runtime.Scheme) {
	if err := announced.NewGroupMetaFactory(
		&announced.GroupMetaFactoryArgs{
			GroupName: devops.GroupName,
			RootScopedKinds: sets.NewString(
				"Project",
				"ProjectList",
				"Jenkins",
				"JenkinsList",
				"CodeRepoService",
				"CodeRepoServiceList",
				"ImageRegistry",
				"ImageRegistryList",
				"ClusterPipelineTemplate",
				"ClusterPipelineTemplateList",
				"ClusterPipelineTaskTemplate",
				"ClusterPipelineTaskTemplateList",
				"ProjectManagement",
				"ProjectManagementList",
				"TestTool",
				"TestToolList",
				"CodeQualityTool",
				"CodeQualityToolList",
				"ArtifactRegistryManager",
				"ArtifactRegistry",
				"ArtifactRegistryBinding",
				"ToolCategory",
			),
			IgnoredKinds: sets.NewString(
				"PipelineLogOptions",
				"PipelineLog",
				"CodeRepoServiceAuthorizeOptions",
				"CodeRepoServiceAuthorizeResponse",
			),
			VersionPreferenceOrder:     []string{v1alpha1.SchemeGroupVersion.Version},
			AddInternalObjectsToScheme: devops.AddToScheme,
		},
		announced.VersionToSchemeFunc{
			v1alpha1.SchemeGroupVersion.Version: v1alpha1.AddToScheme,
		},
	).Announce(groupFactoryRegistry).RegisterAndEnable(registry, scheme); err != nil {
		panic(err)
	}
}
*/

// Install registers the API group and adds types to a scheme
func Install(scheme *runtime.Scheme) {
	utilruntime.Must(devops.AddToScheme(scheme))
	utilruntime.Must(v1alpha1.AddToScheme(scheme))
	// utilruntime.Must(v1alpha1.AddToScheme(scheme))
	// TODO: Once add a new version should set this prioritt
	// utilruntime.Must(scheme.SetVersionPriority(v1beta1.SchemeGroupVersion, v1alpha1.SchemeGroupVersion))
	utilruntime.Must(scheme.SetVersionPriority(v1alpha1.SchemeGroupVersion))
}
