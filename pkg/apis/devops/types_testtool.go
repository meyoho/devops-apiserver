package devops

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// TestToolType type for the TestTool
type TestToolType string

const (
	// TestToolTypeRedwoodHQ RedwoodHQ
	TestToolTypeRedwoodHQ TestToolType = "RedwoodHQ"
)

func (c TestToolType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TestTool struct holds TestTool data
type TestTool struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the TestTool.
	// +optional
	Spec TestToolSpec
	// Most recently observed status of the TestTool.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus
}

// TestToolSpec is the spec in TestTool
type TestToolSpec struct {
	ToolSpec
	// Type defines the service type.
	Type TestToolType
	// Public defines the public of the service.
	// +optional
	Public bool
	// Data defines the data.
	// +optional
	Data map[string]string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TestToolList is a list of TestTool objects.
type TestToolList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items is a list of TestTool
	Items []TestTool
}

var _ ToolInterface = &TestTool{}

func (testTool *TestTool) GetKind() string {
	return TypeTestTool
}

func (testTool *TestTool) GetKindType() string {
	return testTool.Spec.Type.String()
}

func (testTool *TestTool) GetHostPort() HostPort {
	return testTool.Spec.HTTP
}

func (testTool *TestTool) GetObjectMeta() metav1.Object {
	return &testTool.ObjectMeta
}

func (testTool *TestTool) GetStatus() *ServiceStatus {
	return &testTool.Status
}

func (testTool *TestTool) GetToolSpec() ToolSpec {
	return testTool.Spec.ToolSpec
}
