package devops

import (
	"time"

	corev1 "k8s.io/api/core/v1"
)

const (
	// StatusCreating creating
	StatusCreating = "Creating"
	// StatusReady can be reached
	StatusReady = "Ready"
	// StatusError service cannot be reached
	StatusError = "Error"
	// StatusSyncing Syncing
	StatusSyncing = "Syncing"
	// StatusDisabled Disabled
	StatusDisabled = "Disabled"
	//StatusWaitingToDelete WaitingToDelete
	StatusWaitingToDelete = "WaitingToDelete"
	//StatusListRepoError ListRepoError
	StatusListRepoError = "ListRepoError"
	//StatusListTagError ListTagError
	StatusListTagError = "ListTagError"
	// StatusNeedsAuthorization NeedsAuthorization
	StatusNeedsAuthorization = "NeedsAuthorization"

	AnnotationsStyleIcon = "style.icon"

	// AnnotationsKeyProduct product name key for annotations
	AnnotationsKeyProduct = "product"

	// AnnotationsJenkinsProgressiveLogURI annotations key that contains progressive log uri
	AnnotationsJenkinsProgressiveLogURI = "alauda.io/jenkins-progressive-log"
	//AnnotationsJenkinsMultiBranchName annotations key that contains branch name of pipeline if pipeline is a multi-branch job
	AnnotationsJenkinsMultiBranchName = "multiBranchName"

	// AnnotationsProjectDataAvatarURL the avatar of the "project" in tool
	AnnotationsProjectDataAvatarURL = "avatarURL"
	// AnnotationsProjectDataAccessPath the access path of the "project" in tool
	AnnotationsProjectDataAccessPath = "accessPath"
	// AnnotationsProjectDataDescription the description of the "project" in tool
	AnnotationsProjectDataDescription = "description"
	// AnnotationsProjectDataType the type of the "project" in tool, tg. Org, Team ,Group, SubGroup, enum by tool
	AnnotationsProjectDataType = "type"

	// AnnotationsTemplateName the original name of template
	AnnotationsTemplateName = "templateName"

	// endregion

	// region Type: Pascal nomenclature
	TypeProject                     = "Project"
	TypeNamespace                   = "Namespace"
	TypeJenkins                     = "Jenkins"
	TypeJenkinsBinding              = "JenkinsBinding"
	TypePipelineConfig              = "PipelineConfig"
	TypePipeline                    = "Pipeline"
	TypePipelineTemplate            = "PipelineTemplate"
	TypePipelineTaskTemplate        = "PipelineTaskTemplate"
	TypePipelineTemplateSync        = "PipelineTemplateSync"
	TypeClusterPipelineTemplateSync = "ClusterPipelineTemplateSync"
	TypeCodeRepoService             = "CodeRepoService"
	TypeCodeRepoBinding             = "CodeRepoBinding"
	TypeCodeRepository              = "CodeRepository"
	TypeImageRegistry               = "ImageRegistry"
	TypeImageRegistryBinding        = "ImageRegistryBinding"
	TypeImageRepository             = "ImageRepository"
	TypeProjectManagement           = "ProjectManagement"
	TypeProjectManagementBinding    = "ProjectManagementBinding"
	TypeTestTool                    = "TestTool"
	TypeToolBindingReplica          = "ToolBindingReplica"
	TypeSecret                      = "Secret"
	TypeConfigMap                   = "ConfigMap"
	TypeCodeQualityTool             = "CodeQualityTool"
	TypeDocumentManagement          = "DocumentManagement"
	TypeDocumentManagementBinding   = "DocumentManagementBinding"
	TypeCodeQualityBinding          = "CodeQualityBinding"
	TypeCodeQualityProject          = "CodeQualityProject"
	TypeCodeQualityProjectList      = "CodeQualityProjectList"
	TypeClusterPipelineTemplate     = "ClusterPipelineTemplate"
	TypeClusterPipelineTaskTemplate = "ClusterPipelineTaskTemplate"
	TypeToolBindingMap              = "ToolBindingMap"

	// endregion

	// region Kind: all words were lower case
	ResourceKindProject              = "project"
	ResourceKindJenkins              = "jenkins"
	ResourceKindPipeline             = "pipeline"
	ResourceKindCodeRepoService      = "codereposervice"
	ResourceKindImageRegistry        = "imageregistry"
	ResourceKindImageRegistryBinding = "imageregistrybinding"
	ResourceKindToolBindingReplica   = "toolbindingreplica"
	ResourceKindProjectData          = "projectdata"
	ResourceKindProjectDataList      = "projectdatalist"
	TypeArtifactRegistry             = "ArtifactRegistry"
	TypeArtifactRegistryBinding      = "ArtifactRegistryBinding"
	TypeArtifactRegistryManager      = "ArtifactRegistryManager"
	TypeToolCategory                 = "ToolCategory"
	TypeToolType                     = "ToolType"
	TypeSetting                      = "Setting"

	// endregion

	// region Label: Camel nomenclature, used as the key in labels or other place
	LabelPipelineConfig = "pipelineConfig"
	LabelJenkins        = "jenkins"
	//template label and annotataion
	LabelTemplateKind          = "templateKind"
	LabelTemplateName          = "templateName"
	LabelTemplateVersion       = "templateVersion"
	LabelTemplateCategory      = "category"
	AnnotationsTemplateVersion = "version"
	// AnnotationsKeyDisplayNameEn english displayName key for annotations
	AnnotationsKeyDisplayNameEn = "displayName.en"
	// AnnotationsKeyDisplayNameZhCN China displayName key for annotations
	AnnotationsKeyDisplayNameZhCN = "displayName.zh-CN"

	LabelPipelineKind            = "pipeline.kind"
	LabelPipelineKindMultiBranch = "multi-branch"
	LabelCodeRepoService         = "codeRepoService"
	LabelCodeRepoBinding         = "codeRepoBinding"
	LabelProjectManagement       = "projectManagement"
	LabelDocumentManagement      = "documentManagement"
	LabelDocumentManagementType  = "documentManagementType"
	LabelArtifactRegistry        = "artifactRegistry"
	LabelArtifactRegistryManager = "artifactRegistryManager"
	LabelArtifactRegistryType    = "artifactRegistryType"
	LabelTestTool                = "testTool"
	LabelImageRegistry           = "imageRegistry"
	LabelImageRegistryBinding    = "imageRegistryBinding"
	LabelToolItemType            = "toolItemType"
	// LabelToolItemKind Kind of the tool, uses a an API kind
	LabelToolItemKind = "toolItemKind"

	LabelToolBindingReplica          = "toolBindingReplica"
	LabelToolBindingReplicaNamespace = "toolBindingReplicaNamespace"
	LabelCodeQualityTool             = "codeQualityTool"
	LabelCodeQualityBinding          = "codeQualityBinding"
	LabelCodeRepoServiceType         = "codeRepoServiceType"
	// LabelCodeRepoServicePublic label key for codeRepoServicePublic
	LabelCodeRepoServicePublic = "codeRepoServicePublic"
	// LabelImageRegistryType label key for imageRegistryType
	LabelImageRegistryType = "imageRegistryType"
	// LabelImageRegistryEndpoint label key for imageRegistryEndpoint
	LabelImageRegistryEndpoint = "imageRegistryEndpoint"
	// LabelImageRepositoryLink label key for imageRepositoryLink
	LabelImageRepositoryLink = "imageRepositoryLink"
	// LabelsSecretName label key for specific secret name
	LabelsSecretName = "secretName"
	// LabelsSecretNamespace label key for specific secret namespace
	LabelsSecretNamespace = "secretNamespace"
	// LabelCodeQualityToolType label key for codeQualityToolType
	LabelCodeQualityToolType = "codeQualityToolType"
	// LabelReplayedFrom indicate a Pipeline was replayed from another one
	LabelReplayedFrom = "replayed-from"
	// endregion

	LabelBindingPolicyGenerated = "bindingPolicyGenerated"

	// region ToolChainItem
	// configmap
	SettingsConfigMapName  = "devops-config"
	SettingsKeyDomain      = "_domain"
	SettingsKeyRoleMapping = "role_mapping"

	// github
	GithubHost    = "https://api.github.com"
	GithubHTML    = "https://github.com"
	BitbucketHTML = "https://bitbucket.org"

	// endregion
	// region ToolChainElement
	// codeRepository
	ToolChainCodeRepositoryName = "codeRepository"
	// continuousIntegration
	ToolChainContinuousIntegrationName = "continuousIntegration"
	// artifactRepository
	ToolChainArtifactRepositoryName = "artifactRepository"
	// testTool
	ToolChainTestToolName = "testTool"
	// projectManagement
	ToolChainProjectManagementName = "projectManagement"
	// codeQualityTool
	ToolChainCodeQualityToolName             = "codeQualityTool"
	ToolChainDocumentManagementName          = "documentManagement"
	ToolChainArtifactRegistryToolName        = "artifactRegistry"
	ToolChainArtifactRegistryManagerToolName = "artifactRegistryManager"

	// endregion

	// TrueString true as string
	TrueString = "true"
	// FalseString false as string
	FalseString = "false"

	// APIVersionV1Alpha1 version of the api
	APIVersionV1Alpha1 = "devops.alauda.io/v1alpha1"

	// region TTL
	TTLSession = 1 * time.Minute

	// region OAuth2
	// SecretTypeOAuth2 contains data needed for oauth2 authentication.
	//
	// Required fields:
	// - Secret.Data["clientID"] - client id used for authentication
	// - Secret.Data["clientSecret"] - client secret used for authentication
	SecretTypeOAuth2 corev1.SecretType = "devops.alauda.io/oauth2"

	// OAuth2ClientIDKey is the key of the clientID for SecretTypeOAuth2 secrets
	OAuth2ClientIDKey = "clientID"
	// OAuth2ClientSecretKey is the key of the clientSecret for SecretTypeOAuth2 secrets
	OAuth2ClientSecretKey = "clientSecret"
	// OAuth2CodeKey is the key of the code for SecretTypeOAuth2 secrets
	OAuth2CodeKey = "code"
	// OAuth2AccessTokenKeyKey is the key of the accessTokenKey for SecretTypeOAuth2 secrets
	OAuth2AccessTokenKeyKey = "accessTokenKey"
	// OAuth2AccessTokenKey is the key of the accessToken for SecretTypeOAuth2 secrets
	OAuth2AccessTokenKey = "accessToken"
	// OAuth2ScopeKey is the key of the scope for SecretTypeOAuth2 secrets
	OAuth2ScopeKey = "scope"
	// OAuth2RefreshTokenKey is the key of the refreshToken for SecretTypeOAuth2 secrets
	OAuth2RefreshTokenKey = "refreshToken"
	// OAuth2ExpiresInKey is the key of the expiresIn for SecretTypeOAuth2 secrets
	OAuth2CreatedAtKey = "createdAt"
	// OAuth2ExpiresInKey is the key of the expiresIn for SecretTypeOAuth2 secrets
	OAuth2ExpiresInKey = "expiresIn"
	// OAuth2RedirectURLKey is the key of the redirectURL for SecretTypeOAuth2 secrets
	OAuth2RedirectURLKey = "redirectURL"
	// endregion

	NamespaceDefault    = "default"
	NamespaceKubeSystem = "kube-system"

	TypeCodeQualityReport = "CodeQualityReport"

	// SonarQube Metric Names
	SonarQubeBugs               = "bugs"
	SonarQubeVulnerabilities    = "vulnerabilities"
	SonarQubeCodeSmells         = "codeSmells"
	SonarQubeDuplication        = "duplications"
	SonarQubeCoverage           = "coverage"
	SonarQubeLanguages          = "languages"
	SonarQubeNewBugs            = "newBugs"
	SonarQubeNewCodeSmells      = "newCodeSmells"
	SonarQubeNewDuplication     = "newDuplications"
	SonarQubeNewVulnerabilities = "newVulnerabilities"
	SonarQubeNewCoverage        = "newCoverage"

	// Global Parameter
	// Sort
	ParameterSortModeAscending  = "asc"
	ParameterSortModeDescending = "desc"
	ParameterSortByNatural      = "natural"

	// Finalizers
	FinalizerPipelineConfigReferenced   = "pipelineconfig-referenced"
	FinalizerPipelineTemplateReferenced = "pipelinetemplate-referenced"

	UsedBaseDomain = "alauda.io"

	PreviousDeletedTimestamp = "previousDeletedTimestamp"
)
