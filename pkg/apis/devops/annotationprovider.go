package devops

import (
	"fmt"
)

type AnnotationProvider struct {
	BaseDomain string
}

func NewAnnotationProvider(baseDomain string) AnnotationProvider {
	return AnnotationProvider{
		BaseDomain: baseDomain,
	}
}

func (p AnnotationProvider) formated(data string) string {
	basedomain := p.BaseDomain

	if basedomain == "" {
		basedomain = "alauda.io"
	}

	return fmt.Sprintf("%s/%s", basedomain, data)
}

func (p AnnotationProvider) AnnotationsKeyCategories() string {
	return p.formated("categories")
}

func (p AnnotationProvider) AnnotationsKeyDisplayNameZh() string {
	return p.formated("displayName.zh-CN")
}
func (p AnnotationProvider) AnnotationsPipelineConfigName() string {
	return p.formated("pipelineConfig.name")
}

func (p AnnotationProvider) AnnotationsKeyMultiBranchBranchList() string {
	return p.formated("jenkins.branch")
}

func (p AnnotationProvider) AnnotationsKeyMultiBranchStalePRList() string {
	return p.formated("jenkins.stale.pr")
}

func (p AnnotationProvider) AnnotationsKeyMultiBranchPRList() string {
	return p.formated("jenkins.pr")
}

func (p AnnotationProvider) AnnotationsKeyMultiBranchStaleBranchList() string {
	return p.formated("jenkins.stale.branch")
}

func (p AnnotationProvider) AnnotationsCommit() string {
	return p.formated("commit")
}

func (p AnnotationProvider) AnnotationsKeyProductVersion() string {
	return p.formated("product.version")
}

func (p AnnotationProvider) AnnotationsKeyMultiBranchCategory() string {
	return p.formated("multiBranchCategory")
}

func (p AnnotationProvider) AnnotationsKeyMultiBranchName() string {
	return p.formated("multiBranchName")
}

func (p AnnotationProvider) AnnotationGeneratorName() string {
	return p.formated("generatorName")
}

func (p AnnotationProvider) AnnotationsKeyDisplayName() string {
	return p.formated("displayName")
}

func (p AnnotationProvider) AnnotationsKeyNotificationDisplayName() string {
	return p.formated("display-name")
}

func (p AnnotationProvider) LabelKeyNotificationType() string {
	return p.formated("type")
}

func (p AnnotationProvider) AnnotationsHeartbeatTouchTime() string {
	return p.formated("heartbeatTouchTime")
}

func (p AnnotationProvider) LabelAlaudaIOProjectKey() string {
	return p.formated("project")
}
func (p AnnotationProvider) AnnotationsKeyToolType() string {
	return p.formated("toolType")
}

func (p AnnotationProvider) AnnotationsKeyDisplayNameEn() string {
	return p.formated("displayName.en")
}
func (p AnnotationProvider) LabelToolItemType() string {
	return p.formated("toolItemType")
}

func (p AnnotationProvider) AnnotationsGeneratedBy() string {
	return p.formated("generatedBy")
}

func (p AnnotationProvider) AnnotationsGeneratorNamespace() string {
	return p.formated("generatorNamespace")
}

func (p AnnotationProvider) AnnotationsGeneratorName() string {
	return p.formated("generatorName")
}

func (p AnnotationProvider) AnnotationsSonarQubeProjectLink() string {
	return p.formated("sonarqubeProjectLink")
}

func (p AnnotationProvider) AnnotationsTemplateVersion() string {
	return p.formated("version")
}

func (p AnnotationProvider) AnnotationsTemplateName() string {
	return p.formated("templateName")
}

func (p AnnotationProvider) AnnotationsTemplateLatestVersion() string {
	return p.formated("templateLatestVersion")
}

func (p AnnotationProvider) AnnotationsImageRegistryEndpoint() string {
	return p.formated("imageRegistryEndpoint")
}

func (p AnnotationProvider) LabelToolItemKind() string {
	return p.formated("toolItemKind")

}

func (p AnnotationProvider) LabelToolBindingReplica() string {
	return p.formated("toolBindingReplica")

}

func (p AnnotationProvider) LabelToolBindingReplicaNamespace() string {
	return p.formated("toolBindingReplicaNamespace")

}

func (p AnnotationProvider) AnnotationsKeyDisplayNameZhCN() string {
	return p.formated("displayName.zh-CN")

}

func (p AnnotationProvider) AnnotationsStyleIcon() string {
	return p.formated("style.icon")
}

func (p AnnotationProvider) AnnotationsKeyProduct() string {
	return p.formated("product")

}

func (p AnnotationProvider) AnnotationsKeyProject() string {
	return p.formated("project")

}

func (p AnnotationProvider) AnnotationsKeySubProject() string {
	return p.formated("subProject")

}

func (p AnnotationProvider) AnnotationsKeyPipelineLastNumber() string {
	return p.formated("pipeline.last.number")

}

func (p AnnotationProvider) AnnotationsKeyPipelineNumber() string {
	return p.formated("pipeline.number")

}

func (p AnnotationProvider) AnnotationsKeyPipelineConfig() string {
	return p.formated("pipelineConfig.name")

}

func (p AnnotationProvider) AnnotationsKeyPipelineConfigScanLog() string {
	return p.formated("alauda.io/multi-branch-scan-log")

}

func (p AnnotationProvider) AnnotationsKeyPipelineConfigBranches() string {
	return p.formated("jenkins.branch")

}

func (p AnnotationProvider) AnnotationsKeyPipelineConfigPullRequests() string {
	return p.formated("jenkins.pr")

}

func (p AnnotationProvider) AnnotationsKeyPipelineConfigStaleBranches() string {
	return p.formated("jenkins.stale.branch")

}

func (p AnnotationProvider) AnnotationsKeyPipelineConfigStalePullRequests() string {
	return p.formated("jenkins.stale.pr")

}

func (p AnnotationProvider) AnnotationsJenkinsMultiBranchName() string {
	return p.formated("multiBranchName")

}

func (p AnnotationProvider) AnnotationsSecretType() string {
	return p.formated("secretType")

}

func (p AnnotationProvider) AnnotationsCreateAppUrl() string {
	return p.formated("createAppUrl")

}

func (p AnnotationProvider) AnnotationsToolHttpHost() string {
	return p.formated("toolHttpHost")

}

func (p AnnotationProvider) AnnotationsToolAccessURL() string {
	return p.formated("toolAccessUrl")

}

func (p AnnotationProvider) AnnotationsToolSubscription() string {
	return p.formated("subscription")

}

func (p AnnotationProvider) AnnotationsToolItemType() string {
	return p.formated("toolItemType")

}

func (p AnnotationProvider) AnnotationsToolName() string {
	return p.formated("toolName")

}

func (p AnnotationProvider) AnnotationsToolItemKind() string {
	return p.formated("toolItemKind")

}

func (p AnnotationProvider) AnnotationsToolItemPublic() string {
	return p.formated("toolItemPublic")

}

func (p AnnotationProvider) AnnotationsToolItemProject() string {
	return p.formated("toolItemProject")

}

func (p AnnotationProvider) AnnotationsToolType() string {
	return p.formated("toolType")

}

func (p AnnotationProvider) ArtifactRegistryManagerLabel() string {
	return p.formated("artifactRegistryManager")
}

func (p AnnotationProvider) AnnotationsKeyChecksum() string {
	return p.formated("checksum")
}
