package devops

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// region Jenkins

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Jenkins struct to hold the jenkins data
type Jenkins struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the Jenkins.
	// +optional
	Spec JenkinsSpec
	// Most recently observed status of the Jenkins.
	// Populated by the system.
	// Read-only.
	// +optional
	Status JenkinsStatus
}

// JenkinsSpec defines Jenkins' specs
type JenkinsSpec struct {
	ToolSpec
}

// JenkinsStatus defines Jenkins' status
type JenkinsStatus struct {
	// ServiceStatus defines the common status
	// +optional
	ServiceStatus
}

// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsList is a list of Jenkins objects.
type JenkinsList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items is a list of Jenkins objects.
	Items []Jenkins
}

// region ToolInterface

var _ ToolInterface = &Jenkins{}

func (j *Jenkins) GetKind() string {
	return TypeJenkins
}

func (jenkins *Jenkins) GetHostPort() HostPort {
	return jenkins.Spec.HTTP
}

func (jenkins *Jenkins) GetKindType() string {
	return TypeJenkins
}

func (jenkins *Jenkins) GetObjectMeta() metav1.Object {
	return &jenkins.ObjectMeta
}

func (jenkins *Jenkins) GetStatus() *ServiceStatus {
	return &jenkins.Status.ServiceStatus
}

func (jenkins *Jenkins) GetToolSpec() ToolSpec {
	return jenkins.Spec.ToolSpec
}
