package devops

import (
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ProjectManagementType type for the ProjectManagement
type ProjectManagementType string

const (
	// ProjectManageTypeJira Jira
	ProjectManageTypeJira ProjectManagementType = "Jira"
	// ProjectManageTypeTaiga Taiga
	ProjectManageTypeTaiga ProjectManagementType = "Taiga"
)

func (c ProjectManagementType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagement struct holds ProjectManagement data
type ProjectManagement struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the ProjectManagement.
	// +optional
	Spec ProjectManagementSpec
	// Most recently observed status of the ProjectManagement.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus
}

// GetType projectmanagement service type
func (c *ProjectManagement) GetType() ProjectManagementType {
	return c.Spec.Type
}

func (c *ProjectManagement) GetEndpoint() string {
	endpoint := c.Spec.HTTP.Host
	endpoint = strings.TrimRight(endpoint, "/")
	return endpoint
}

func (c *ProjectManagement) GetSecretNamespace() string {
	namespace := c.GetNamespace()
	if c.Spec.Secret.Namespace != "" {
		namespace = c.Spec.Secret.Namespace
	}
	return namespace
}

func (c *ProjectManagement) GetSecretName() string {
	return c.Spec.Secret.Name
}

// ProjectManagementSpec is the spec in ProjectManagement
type ProjectManagementSpec struct {
	ToolSpec
	// Type defines the service type.
	Type ProjectManagementType
	//Secret defines the secret type.
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type ProjectmanagementUser struct {
	metav1.TypeMeta
	Items []User
}
type User struct {
	Username string
	Email    string
	ID       string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
//UserSearchOptions indicates the options to search user
type UserSearchOptions struct {
	metav1.TypeMeta
	// Name specifies the name of the user.
	Name string
	// SecretName specifies the name of the secret.
	SecretName string
	// NameSpace specifies the namespace of the secret.
	NameSpace string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagementList is a list of ProjectManagement objects.
type ProjectManagementList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items is a list of ProjectManagement
	Items []ProjectManagement
}

var _ ToolInterface = &ProjectManagement{}

func (pm *ProjectManagement) GetKind() string {
	return TypeProjectManagement
}

func (pm *ProjectManagement) GetKindType() string {
	return pm.Spec.Type.String()
}

func (pm *ProjectManagement) GetObjectMeta() metav1.Object {
	return &pm.ObjectMeta
}

func (projectManagement *ProjectManagement) GetHostPort() HostPort {
	return projectManagement.Spec.HTTP
}

func (pm *ProjectManagement) GetStatus() *ServiceStatus {
	return &pm.Status
}

func (pm *ProjectManagement) GetToolSpec() ToolSpec {
	return pm.Spec.ToolSpec
}
