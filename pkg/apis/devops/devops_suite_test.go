package devops_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestDevops(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Devops Suite")
}
