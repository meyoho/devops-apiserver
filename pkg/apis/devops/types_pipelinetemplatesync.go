package devops

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

// PipelineTemplateSyncInterface is interface of PipelineTemplateSync and ClusterPipelineTemplateSync
type PipelineTemplateSyncInterface interface {
	runtime.Object
	GetSpec() *PipelineTemplateSyncSpec
	// GetTypeMeta() *metav1.TypeMeta{ // TypeMeta is not always has kind
	GetKind() string
	GetStatus() *PipelineTemplateSyncStatus
	metav1.ObjectMetaAccessor
	metav1.Object
	Phaser
}

// GetSpec will return ClusterPipelineTemplateSync spec
func (sync *ClusterPipelineTemplateSync) GetSpec() *PipelineTemplateSyncSpec {
	return &sync.Spec
}

// GetKind will return kind of resource
func (sync *ClusterPipelineTemplateSync) GetKind() string {
	return TypeClusterPipelineTemplateSync
}

// GetStatus will return status of resource
func (sync *ClusterPipelineTemplateSync) GetStatus() *PipelineTemplateSyncStatus {
	return sync.Status
}

// SetStatus will set status of resource
func (sync *ClusterPipelineTemplateSync) SetStatus(status *PipelineTemplateSyncStatus) {
	sync.Status = status
}

// GetSpec will return ClusterPipelineTemplateSync spec
func (sync *PipelineTemplateSync) GetSpec() *PipelineTemplateSyncSpec {
	return &sync.Spec
}

// GetKind will return kind of resource
func (sync *PipelineTemplateSync) GetKind() string {
	return TypePipelineTemplateSync
}

// GetStatus will return status of resource
func (sync *PipelineTemplateSync) GetStatus() *PipelineTemplateSyncStatus {
	return sync.Status
}

// SetStatus will set status of resource
func (sync *PipelineTemplateSync) SetStatus(status *PipelineTemplateSyncStatus) {
	sync.Status = status
}

var _ PipelineTemplateSyncInterface = &ClusterPipelineTemplateSync{}

var _ PipelineTemplateSyncInterface = &PipelineTemplateSync{}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterPipelineTemplateSyncList is a list of ClusterPipelineTemplateSync
type ClusterPipelineTemplateSyncList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items hold all templates for sync
	// +optional
	Items []ClusterPipelineTemplateSync
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterPipelineTemplateSync specified a ClusterPipelineTemplate sync setting
type ClusterPipelineTemplateSync struct {
	metav1.TypeMeta
	metav1.ObjectMeta

	// Spec specification for PipelineTemplateSync
	Spec PipelineTemplateSyncSpec
	// Status indicate status of PipelineTemplateSync
	// +optional
	Status *PipelineTemplateSyncStatus
}

// GetPhase returns current phase as string
func (p *ClusterPipelineTemplateSync) GetPhase() string {
	if p.Status == nil {
		return ""
	}
	return string(p.Status.Phase)
}

// region PipelineTemplate
// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTemplateSync specified a PipelineTemplate sync setting
type PipelineTemplateSync struct {
	metav1.TypeMeta
	metav1.ObjectMeta

	// Spec specification for PipelineTemplateSync
	Spec PipelineTemplateSyncSpec
	// Status indicate status of PipelineTemplateSync
	// +optional
	Status *PipelineTemplateSyncStatus
}

var _ Phaser = &PipelineTemplateSync{}

// GetPhase returns current phase as string
func (p *PipelineTemplateSync) GetPhase() string {
	if p.Status == nil {
		return ""
	}
	return string(p.Status.Phase)
}

// PipelineTemplateSyncSpec represents PipelineTemplateSync's specs
type PipelineTemplateSyncSpec struct {
	// Source git source
	Source PipelineSource
	// which strategy will be used
	Strategy TemplateSyncStrategy
}

// PipelineTemplateSyncStatus represents PipelineTemplateSync's status
type PipelineTemplateSyncStatus struct {
	// Phase describe phase of PipelineTemplateSync
	Phase PipelineTemplateSyncPhase
	// Message is message in the process of sync
	// +optional
	Message string
	// Error is a specific error message if any error occurred
	// +optional
	Error string
	// Conditions contains all file will be synced
	// +optional
	Conditions []PipelineTemplateSyncCondition
	// StartTime is the time of start sync process
	// +optional
	StartTime metav1.Time
	// EndTime is the time of end sync process
	// +optional
	EndTime metav1.Time
	// CommitID is git commit log id
	// +optional
	CommitID string
}

// PipelineTemplateSyncPhase a phase of PipelineTemplateSyncStatus
type PipelineTemplateSyncPhase string

// TemplateSyncStrategy a strategy for templatesync
type TemplateSyncStrategy string

const (
	// PipelineTemplateSyncPhaseDraft draft phase of PipelineTemplateSync
	PipelineTemplateSyncPhaseDraft PipelineTemplateSyncPhase = "Draft"
	// PipelineTemplateSyncPhasePending pending phase of PipelineTemplateSync
	PipelineTemplateSyncPhasePending PipelineTemplateSyncPhase = "Pending"
	// PipelineTemplateSyncPhaseSyncing syncing phase of PipelineTemplateSync
	PipelineTemplateSyncPhaseSyncing PipelineTemplateSyncPhase = "Syncing"
	// PipelineTemplateSyncPhaseReady ready phase of PipelineTemplateSync
	PipelineTemplateSyncPhaseReady PipelineTemplateSyncPhase = "Ready"
	// PipelineTemplateSyncPhaseError error phase of PipelineTemplateSync
	PipelineTemplateSyncPhaseError PipelineTemplateSyncPhase = "Error"
	// TemplateSyncForce force sync template strategy
	TemplateSyncForce TemplateSyncStrategy = "Force"
	// TemplateSyncVersionUpgrage version upgrade sync template strategy
	TemplateSyncVersionUpgrage TemplateSyncStrategy = "VersionUpgrade"
)

// IsValid PipelineTemplateSyncPhase valid check
func (phase PipelineTemplateSyncPhase) IsValid() bool {
	switch phase {
	case PipelineTemplateSyncPhaseDraft, PipelineTemplateSyncPhasePending, PipelineTemplateSyncPhaseSyncing,
		PipelineTemplateSyncPhaseReady, PipelineTemplateSyncPhaseError:
		return true
	}

	return false
}

// PipelineTemplateSyncCondition represent for one record for sync
type PipelineTemplateSyncCondition struct {
	// LastTransitionTime
	// +optional
	LastTransitionTime metav1.Time
	// LastUpdateTime
	// +optional
	LastUpdateTime metav1.Time
	// Message contains describe message for sync process
	// +optional
	Message string
	// Reason is the reason for success or failure
	// +optional
	Reason string
	// Status is status of sync process
	Status SyncStatus
	// Type is the type of template
	Type string
	// Target represent target template file relative path
	Target string
	// Name is the name template
	Name string
	// Version is the version of template file
	Version string
	// PreviousVersion is the version of previous template file
	PreviousVersion string
}

// SyncStatus represent status of sync
type SyncStatus string

// IsValid validates for SyncStatus
func (status SyncStatus) IsValid() bool {
	switch status {
	case SyncStatusSuccess:
	case SyncStatusFailure:
	case SyncStatusSkip:
		return true
	}

	return false
}

const (
	// SyncStatusSuccess show the process is ok
	SyncStatusSuccess SyncStatus = "Success"
	// SyncStatusFailure show the process is failure
	SyncStatusFailure SyncStatus = "Failure"
	// SyncStatusSkip show the process is kip
	SyncStatusSkip SyncStatus = "Skip"
	// SyncStatusDeleted show the process is deleted
	SyncStatusDeleted SyncStatus = "Deleted"
)

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PipelineTemplateSyncList is a list of PipelineTemplateSync
type PipelineTemplateSyncList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items hold all templates for sync
	// +optional
	Items []PipelineTemplateSync
}

// endregion
