package devops_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestTypes(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("types.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/apis/devops", []Reporter{junitReporter})
}
