package devops_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"sort"

	. "alauda.io/devops-apiserver/pkg/apis/devops"
)

var _ = Describe("Devops", func() {
	artifactlist := PipelineArtifactList{
		Length: 8,
		ArtifactList: []PipelineArtifact{
			{Name: "z.log"},
			{Name: "#df.log"},
			{Name: "b.log"},
			{Name: "a.log"},
			{Name: "&df.log"},
			{Name: "w.log"},
			{Name: "e.log"},
			{Name: "Readme.log"},
		},
	}
	It("sort should work as expected", func() {
		sort.Sort(artifactlist)
		Expect(artifactlist.ArtifactList).To(Equal([]PipelineArtifact{
			{Name: "#df.log"},
			{Name: "&df.log"},
			{Name: "a.log"},
			{Name: "b.log"},
			{Name: "e.log"},
			{Name: "Readme.log"},
			{Name: "w.log"},
			{Name: "z.log"},
		}))
	})

})
