package devops

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeQualityProject save CodeQualityTool Project info
type CodeQualityProject struct {
	metav1.TypeMeta
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the CodeQualityProject.
	// +optional
	Spec CodeQualityProjectSpec
	// Most recently observed status of the CodeQualityProject.
	// Populated by the system.
	// Read-only.
	// +optional
	Status CodeQualityProjectStatus
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// CodeQualityProjectOptions presets CodeQualityProject options
type CodeQualityProjectOptions struct {
	metav1.TypeMeta
	ProjectKey string
}

//+k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type CodeQualityProjectLastAnalysisDate struct {
	metav1.TypeMeta
	Date *metav1.Time
}

//+k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type CodeQualityProjectRemoteStatus struct {
	metav1.TypeMeta
	Exist bool
}

//+k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type CodeQualityProjectReportsCondition struct {
	metav1.TypeMeta
	Conditions []CodeQualityCondition
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeQualityProjectList is a list of CodeQualityProject objects.
type CodeQualityProjectList struct {
	metav1.TypeMeta
	metav1.ListMeta

	// Items contains all codequalityproject in this list
	Items []CodeQualityProject
}

// CodeQualityProject presents CodeQualityProject's spec
type CodeQualityProjectSpec struct {
	// CodeQualityTool defines the CodeQualityTool in spec
	CodeQualityTool LocalObjectReference
	// CodeQualityBinding defines the CodeQualityBinding in spec
	CodeQualityBinding LocalObjectReference
	// CodeRepository defines the CodeRepository in spec
	CodeRepository LocalObjectReference
	// Project defines CodeQualityProject info
	Project CodeQualityProjectInfo
}

// CodeQualityProjectInfo presents CodeQualityProject info
type CodeQualityProjectInfo struct {
	// ProjectKey defines key in CodeQualityProjectInfo
	ProjectKey string
	// ProjectName defines display name in CodeQualityProjectInfo
	ProjectName string
	// CodeAddress defines code address in CodeQualityProjectInfo
	CodeAddress string
	// LastAnalysis defines the last analysis date of this project
	LastAnalysis *metav1.Time `json:"LastAnalysisDate"`
}

// CodeQualityProjectInfo presents CodeQualityProject status
type CodeQualityProjectStatus struct {
	// Current condition of the service.
	// One of: "Creating" or "Ready" or "Error" or "WaitingToDelete".
	// +optional
	Phase ServiceStatusPhase
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string
	// Human-readable message indicating details about last transition.
	// +optional
	Message string
	// LastUpdate is the latest time when updated the service.
	// +optional
	LastUpdate *metav1.Time
	// HTTPStatus is http status of the service.
	// +optional
	HTTPStatus *HostPortStatus
	// Conditions defines analyze info
	CodeQualityConditions []CodeQualityCondition
}

// CodeQualityCondition presents CodeQualityProject analyze info
type CodeQualityCondition struct {
	BindingCondition
	// Branch defines analyze code branch, default is master
	Branch string
	// IsMain defines whether the branch is main branch
	IsMain bool
	// QualityGate defines project use which quality gate
	QualityGate string
	// Public defines project visible
	Visibility string
	// Metrics defines a series of metrics of this project
	Metrics map[string]CodeQualityAnalyzeMetric
}

// CodeQualityAnalyzeResult present CodeQualityProject analyze result
type CodeQualityAnalyzeMetric struct {
	// Name defines the name of this metric
	Name string
	// Value defines the value of this metric
	Value string
	// Level defines the level of the value
	// +optional
	Level string
}
