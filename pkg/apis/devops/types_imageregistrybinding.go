package devops

import (
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryBinding registry service binding
type ImageRegistryBinding struct {
	metav1.TypeMeta
	// +optional
	metav1.ObjectMeta

	Spec ImageRegistryBindingSpec
	// Read-only
	// +optional
	Status ServiceStatus
}

func (b *ImageRegistryBinding) GetSecretNamespace() string {
	namespace := b.GetNamespace()
	if b.Spec.Secret.Namespace != "" {
		namespace = b.Spec.Secret.Namespace
	}
	return namespace
}

func (b *ImageRegistryBinding) GetSecretName() string {
	return b.Spec.Secret.Name
}

func (b *ImageRegistryBinding) GetDockerCfgSecretName() string {
	if b.Spec.Secret.Name == "" {
		return ""
	}
	return fmt.Sprintf("dockercfg--%s--%s", b.Namespace, b.Name)
}

// ImageRegistryBindingSpec represents ImageRegistry specs
type ImageRegistryBindingSpec struct {
	ImageRegistry LocalObjectReference
	// +optional
	Secret SecretKeySetRef
	// +optional
	RepoInfo ImageRegistryBindingRepo
}

// ImageRegistryBindingRepo save the path of user choose
type ImageRegistryBindingRepo struct {
	// Repositories represent all repository in this imageregistorybinding
	// +optional
	Repositories []string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryBindingList is a list of ImageRegistryBinding objects.
type ImageRegistryBindingList struct {
	metav1.TypeMeta
	// +optional
	metav1.ListMeta

	Items []ImageRegistryBinding
}

var _ ToolBindingLister = &ImageRegistryBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *ImageRegistryBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta.Kind = TypeImageRegistryBinding
	}
	return
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryBindingRepositories used to retrieve repository from registry
type ImageRegistryBindingRepositories struct {
	metav1.TypeMeta
	// Items indicate all imagerepository in this registry
	Items []string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryBindingRepositoryOptions user to fetch registry binding options
type ImageRegistryBindingRepositoryOptions struct {
	metav1.TypeMeta

	// +optional
}

var _ StatusAccessor = &ImageRegistryBinding{}

// GetStatus implements StatusAccessor
func (b *ImageRegistryBinding) GetStatus() *ServiceStatus {
	return &b.Status
}
