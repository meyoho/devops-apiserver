package devops

import (
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ImageRegistry type of image registry
type ImageRegistryType string

const (
	// RegistryTypeDocker normal registry, deploy registry alone
	RegistryTypeDocker ImageRegistryType = "Docker"
	// RegistryTypeHarbor harbor
	RegistryTypeHarbor ImageRegistryType = "Harbor"
	// RegistryTypeAlauda alauda
	RegistryTypeAlauda ImageRegistryType = "Alauda"
	// RegistryTypeDockerHub dockerhub
	RegistryTypeDockerHub ImageRegistryType = "DockerHub"
)

func (c ImageRegistryType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// registry service
type ImageRegistry struct {
	// +optional
	metav1.TypeMeta
	// +optional
	metav1.ObjectMeta

	Spec ImageRegistrySpec
	// Read-only
	// +optional
	Status ServiceStatus
}

// Get registry service endpoint
func (c *ImageRegistry) GetEndpoint() string {
	endpoint := c.Spec.HTTP.Host
	endpoint = strings.TrimRight(endpoint, "/")
	return endpoint
}

// Get registry service type
func (c *ImageRegistry) GetType() ImageRegistryType {
	return c.Spec.Type
}

// Get registry service spec
type ImageRegistrySpec struct {
	ToolSpec
	Type ImageRegistryType
	// Public represent if this imageregistry is public or not
	Public bool

	// Data contains the data of imageregistry
	// +optional
	Data map[string]string
	// +optional
	// binding policy
	BindingPolicy *ImageRegistryBindingPolicy
}

type ImageRegistryBindingPolicy struct {
	// +optional
	// policy individual
	// it will create project on remote registry with same name of binding's namespace/projectName
	Individual *ImageRegistryBindingPolicyIndividual

	// +optional
	// policy share
	// all the allocated repositories shared by all projects
	Share *ImageRegistryBindingPolicyShare
}

// ImageRegistryBindingPolicyIndividual policy
// TODO
type ImageRegistryBindingPolicyIndividual struct{}

type ImageRegistryBindingPolicyShare struct {
	// allocated repos
	Repositories []string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryList Get image registry list
type ImageRegistryList struct {
	metav1.TypeMeta
	// +optional
	metav1.ListMeta

	Items []ImageRegistry
}

var _ ToolInterface = &ImageRegistry{}

func (ir *ImageRegistry) GetKind() string {
	return TypeImageRegistry
}

func (ir *ImageRegistry) GetKindType() string {
	return ir.GetType().String()
}

func (ir *ImageRegistry) GetObjectMeta() metav1.Object {
	return &ir.ObjectMeta
}

func (imageRegistry *ImageRegistry) GetHostPort() HostPort {
	return imageRegistry.Spec.HTTP
}

func (imageRegistry *ImageRegistry) GetStatus() *ServiceStatus {
	return &imageRegistry.Status
}

func (imageRegistry *ImageRegistry) GetToolSpec() ToolSpec {
	return imageRegistry.Spec.ToolSpec
}

// endregion ToolInterface
