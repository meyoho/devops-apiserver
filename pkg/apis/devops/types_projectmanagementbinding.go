package devops

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagementBinding is the binding referenced to ProjectManagement
type ProjectManagementBinding struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the ProjectManagementBinding.
	// +optional
	Spec ProjectManagementBindingSpec
	// Most recently observed status of the ProjectManagementBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus
}

func (b *ProjectManagementBinding) GetSecretNamespace() string {
	namespace := b.GetNamespace()
	if b.Spec.Secret.Namespace != "" {
		namespace = b.Spec.Secret.Namespace
	}
	return namespace
}

func (b *ProjectManagementBinding) GetSecretName() string {
	return b.Spec.Secret.Name
}

// ProjectManagementBindingSpec is the spec in ProjectManagementBinding
type ProjectManagementBindingSpec struct {
	ProjectManagement LocalObjectReference
	//Secret defines the secret type.
	//+optional
	Secret SecretKeySetRef
	// ProjectManagementProjectInfos contains all project info in projectmanagement instance
	//+optional
	ProjectManagementProjectInfos []ProjectManagementProjectInfo
}

// ProjectManagementProjectInfo contains  project info in projectmanagement
type ProjectManagementProjectInfo struct {
	// ID is the id of the project in projectmanagement
	ID string
	// Name is the name of project in projectmanagement
	Name string
	// Key is the key of project in projectmanagement
	Key string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
//IssueSearchOptions indicates the options to search Issue
type IssueSearchOptions struct {
	metav1.TypeMeta
	// Type of issue it could be all status prioritiy or issuetype
	Type string
}

type IssueFilterData struct {
	// Name of filterdata
	Name string
	// ID of filterdata
	ID string
	// Data about some special source
	Data map[string]string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type IssueFilterDataList struct {
	//+optional
	metav1.TypeMeta
	//Status defines the issue filter status
	//+optional
	Status []IssueFilterData
	//IssueType defines the issue type for a issue
	//+optional
	IssueType []IssueFilterData
	// Priority defines the priority of the issue
	//+optional
	Priority []IssueFilterData
}

func (issuefilterlist *IssueFilterDataList) SetFilterData(name string, data IssueFilterData) {
	switch name {
	case "status":
		issuefilterlist.Status = append(issuefilterlist.Status, data)
	case "issuetype":
		issuefilterlist.IssueType = append(issuefilterlist.IssueType, data)
	case "priority":
		issuefilterlist.Priority = append(issuefilterlist.Priority, data)
	default:
		return
	}
}

// ListIssuesOptions indicate the options to show issue
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ListIssuesOptions struct {
	//+optional
	metav1.TypeMeta
	//Page defines the page num
	Page int
	//PageSize defines the pagesize num
	PageSize int
	//Project defines the Project name
	//+optional
	Project string
	//Type defines the issue type
	//+optional
	Type string
	//Priority defines the priority of the issue list
	//+optional
	Priority string
	//Status defines the status of the issue list
	//+optional
	Status string
	//Summary defines the summary of the issue list
	//+optional
	Summary string
	//Issuekey defines the issue key of a issue
	//+optional
	IssueKey string
	//OrderBy defines the order
	OrderBy string
	//Sort chould be ASC or DESC
	Sort string
	//+optional
	Projects []string
}

// IssueKeyOptions indicates the key of issue
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type IssueKeyOptions struct {
	//+optional
	metav1.TypeMeta
	//key of the issue
	Key string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type IssueDetail struct {
	//+optional
	metav1.TypeMeta
	//link of the issue
	SelfLink string
	//key of the issue
	Key string
	//summay of the issue
	Summary string
	//description of the issue
	Description string
	//comments of the issue
	Comments []Comment
	//issuelinks of the issue
	IssueLinks []IssueLink
	//priority of the issue
	Priority IssuePriority
	//issuetype of the issue
	Issuetype IssueType
	//status of the issue
	Status IssueStatus
	//project which this issue belong to
	Project ProjectData
	//created time
	Created string
	//updated time
	Updated string
	//creator name
	Creator User
	//assigneer name
	Assignee User
}
type IssueType struct {
	//Issue type name
	Name string
}

type IssueStatus struct {
	//Issue Status name
	Name string
	//Issue Status ID
	ID string
}

type IssuePriority struct {
	//Issue Priority Name
	Name string
}

type Comment struct {
	//Comment author name
	Author string
	//Created Time
	Time string
	//Content of the comment
	Content string
}

type IssueLink struct {
	SelfLink string
	Summary  string
	Key      string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type IssueDetailList struct {
	metav1.TypeMeta
	// total num of the issue list
	Total int
	// items of the issuelist
	Items []IssueDetail
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagementBindingList is a list of ProjectManagementBinding objects.
type ProjectManagementBindingList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items is a list of ProjectManagementBinding
	Items []ProjectManagementBinding
}

var _ ToolBindingLister = &ProjectManagementBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *ProjectManagementBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta.Kind = TypeProjectManagementBinding
	}
	return
}

func (binding *ProjectManagementBinding) GetStatus() *ServiceStatus {
	return &binding.Status
}
