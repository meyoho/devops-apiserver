package devops

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// StatusAccessor get status of object
type StatusAccessor interface {
	GetStatus() *ServiceStatus
}

// ToolInterface is abstract of all tools
type ToolInterface interface {
	// GetHostPort returen field of Http in Tool, will abandon by GetToolSpec
	GetHostPort() HostPort
	// Object's TypeMeta is empty
	// https://github.com/kubernetes/client-go/issues/413
	// https://github.com/kubernetes/kubernetes/pull/63972
	// GetTypeMeta() metav1.TypeMeta

	GetKind() string
	// Get SubType of tool
	GetKindType() string
	StatusAccessor
	metav1.ObjectMetaAccessor
	GetToolSpec() ToolSpec
}

// ToolSpec contains custom field in tool spec
type ToolSpec struct {
	HTTP HostPort
	// +optional
	Secret SecretKeySetRef
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CreateProjectOptions work for create projectmanagement project
type CreateProjectOptions struct {
	// +optional
	metav1.ObjectMeta
	metav1.TypeMeta
	// SecretName represent the secret use to create project
	SecretName string
	// Namespace represent the Namespace of project
	Namespace string
	// Name represent the Name of project
	Name string
	// IsRemote represent if this project be remote or not
	IsRemote bool
	// Data represent the data needed to create projectmanagement project
	Data map[string]string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ListProjectOptions options to list Projects
type ListProjectOptions struct {
	metav1.TypeMeta
	// SecretName secret use to list project
	SecretName string
	// Namespace namespace which the secret in
	Namespace string
	// Page represent the page to show
	Page string
	// PageSize represent the pagesize
	PageSize string
	// Filter represent how to filter data
	Filter string
	// IsRemote decide show the remote project or not
	IsRemote bool
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectData project data
type ProjectData struct {
	metav1.TypeMeta
	metav1.ObjectMeta
	//Data represent all data needed to create project
	Data map[string]string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectDataList  fetch project infor
type ProjectDataList struct {
	metav1.TypeMeta
	metav1.ListMeta
	Items []ProjectData
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ToolBinding one ToolBinding data
type ToolBinding struct {
	metav1.TypeMeta
	metav1.ObjectMeta
	Data map[string]string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ToolBindingMap a map of ToolBindings
type ToolBindingMap struct {
	metav1.TypeMeta
	metav1.ObjectMeta
	Items []ToolBinding
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ToolBindingList tool binding list
type ToolBindingList struct {
	metav1.TypeMeta
	metav1.ListMeta
	Items []ToolBindingMap
}

// ToolBindingLister lists tool bindings
type ToolBindingLister interface {
	GetItems() []ToolBinding
}

// NewToolBinding creates a new tool binding based on a metav1.Object
func NewToolBinding(obj metav1.Object) (binding ToolBinding) {
	binding.ObjectMeta = newObjectMeta(obj)
	return
}

func newObjectMeta(obj metav1.Object) (meta metav1.ObjectMeta) {
	meta.Name = obj.GetName()
	meta.Namespace = obj.GetNamespace()
	meta.Initializers = obj.GetInitializers()
	meta.Labels = obj.GetLabels()
	meta.OwnerReferences = obj.GetOwnerReferences()
	meta.ResourceVersion = obj.GetResourceVersion()
	meta.SelfLink = obj.GetSelfLink()
	meta.UID = obj.GetUID()
	meta.Annotations = obj.GetAnnotations()
	meta.Generation = obj.GetGeneration()
	meta.ClusterName = obj.GetClusterName()
	return
}
