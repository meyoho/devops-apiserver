package devops

import (
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// region CodeQualityTool
// CodeQualityToolType for CodeQuality
type CodeQualityToolType string

const (
	// CodeQualityToolTypeSonarqube Sonarqube
	CodeQualityToolTypeSonarqube CodeQualityToolType = "Sonarqube"
)

func (c CodeQualityToolType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +alauda:rolesync-gen:class=platform,name=Sonarqube,sync=rbac,enabled=true,roles=project_admin:admin;project_auditor:user;space_admin:admin;space_developer:user;space_auditor:user;namespace_admin:admin;namespace_auditor:user;namespace_developer:user,custom=project_admin[user:true;codeviewer:true;issueadmin:true;admin:true;scan:true]project_auditor[user:true;codeviewer:true;issueadmin:false;admin:false;scan:true]space_admin[user:true;codeviewer:true;issueadmin:true;admin:true;scan:true]space_developer[user:true;codeviewer:true;issueadmin:false;admin:false;scan:true]space_auditor[user:true;codeviewer:true;issueadmin:false;admin:false;scan:true]namespace_admin[user:true;codeviewer:true;issueadmin:true;admin:true;scan:true]namespace_developer[user:true;codeviewer:true;issueadmin:false;admin:false;scan:true]namespace_auditor[user:true;codeviewer:true;issueadmin:false;admin:false;scan:true]

// CodeQualityTool struct holds CodeQualityTool data
type CodeQualityTool struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the CodeQualityTool
	Spec CodeQualityToolSpec
	// Most recently observed status of the CodeQualityTool.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus
}

// Get CodeQualityTool endpoint
func (c *CodeQualityTool) GetEndpoint() string {
	endpoint := c.Spec.HTTP.Host
	endpoint = strings.TrimRight(endpoint, "/")
	return endpoint
}

// Get CodeQualityTool type
func (c *CodeQualityTool) GetType() CodeQualityToolType {
	return c.Spec.Type
}

// CodeQualityToolSpec is the spec in CodeQualityTool
type CodeQualityToolSpec struct {
	ToolSpec
	// Type defines the code quality tool
	Type CodeQualityToolType
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// CodeQualityToolList is a list of CodeQualityTool
type CodeQualityToolList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items is a list of CodeQualityTool
	Items []CodeQualityTool
}

var _ ToolInterface = &CodeQualityTool{}

func (cqt *CodeQualityTool) GetKind() string {
	return TypeCodeQualityTool
}

func (cqt *CodeQualityTool) GetKindType() string {
	return cqt.Spec.Type.String()
}

func (codeQualityTool *CodeQualityTool) GetHostPort() HostPort {
	return codeQualityTool.Spec.HTTP
}

func (codeQualityTool *CodeQualityTool) GetObjectMeta() metav1.Object {
	return &codeQualityTool.ObjectMeta
}

func (codeQualityTool *CodeQualityTool) GetStatus() *ServiceStatus {
	return &codeQualityTool.Status
}

func (codeQualityTool *CodeQualityTool) GetToolSpec() ToolSpec {
	return codeQualityTool.Spec.ToolSpec
}
