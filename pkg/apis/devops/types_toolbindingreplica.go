package devops

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// region ToolBindingReplica

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type ToolBindingReplicaList struct {
	metav1.TypeMeta
	metav1.ListMeta

	// Items is a list of CodeQualityTool
	Items []ToolBindingReplica
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ToolBindingReplica is the tool binding of project
type ToolBindingReplica struct {
	metav1.TypeMeta
	// +optional
	metav1.ObjectMeta

	// +optional
	Spec ToolBindingReplicaSpec
	// +optional
	Status ToolBindingReplicaStatus
}

// Aggregate all status that rely on to ToolBindingReplicaStatus
func (tbr *ToolBindingReplica) AggregateStatus() (existError bool) {

	if tbr.Status.HTTPStatus != nil && tbr.Status.HTTPStatus.ErrorMessage != "" {
		tbr.Status.Phase = NewToolBindingReplicaPhase(ServiceStatusPhaseError)
		tbr.Status.Reason = tbr.Status.HTTPStatus.ErrorMessage
		tbr.Status.Message = tbr.Status.HTTPStatus.ErrorMessage
		return true
	}

	for _, cond := range tbr.Status.Conditions {
		// Any error condition will cause status of tbr changing to error
		if cond.Status == StatusError {
			tbr.Status.Phase = NewToolBindingReplicaPhase(ServiceStatusPhaseError)
			tbr.Status.Reason = cond.Reason
			tbr.Status.Message = cond.Message
			return true
		}
	}
	return false
}

// LabelSelectorIsNotEmpty checks label is empty
func LabelSelectorIsNotEmpty(labelSelector *metav1.LabelSelector) bool {
	return len(labelSelector.MatchLabels) > 0 || len(labelSelector.MatchExpressions) > 0
}

// GetLabelSelector gets label selector
func (tbr *ToolBindingReplica) GetLabelSelector() metav1.LabelSelector {
	labelSelector := metav1.LabelSelector{}

	switch tbr.Spec.Selector.Kind {
	case TypeCodeQualityTool:
		for _, binding := range tbr.Spec.CodeQuality.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeCodeRepoService:
		for _, binding := range tbr.Spec.CodeRepoService.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeDocumentManagement:
		for _, binding := range tbr.Spec.DocumentManagement.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeImageRegistry:
		for _, binding := range tbr.Spec.ImageRegistry.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeJenkins:
		for _, binding := range tbr.Spec.ContinuousIntegration.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeProjectManagement:
		for _, binding := range tbr.Spec.ProjectManagement.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeArtifactRegistry:
		for _, binding := range tbr.Spec.ArtifactRegistry.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}
	}

	return labelSelector
}

type ToolBindingReplicaPhase string

func NewToolBindingReplicaPhase(phase ServiceStatusPhase) ToolBindingReplicaPhase {
	return ToolBindingReplicaPhase(string(phase))
}

func (phase ToolBindingReplicaPhase) Is(s ServiceStatusPhase) bool {
	return string(phase) == string(s)
}

const (
	ToolBindingReplicaPhaseSyncing ToolBindingReplicaPhase = "Syncing"
)

type ToolBindingReplicaStatus struct {
	Phase ToolBindingReplicaPhase
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string
	// Human-readable message indicating details about last transition.
	// +optional
	Message string
	// LastUpdate is the latest time when updated the service.
	// +optional
	LastUpdate *metav1.Time
	// HTTPStatus is http status of the service.
	// +optional
	HTTPStatus *HostPortStatus
	// Conditions is a list of BindingCondition objects.
	// +optional
	Conditions []BindingCondition
	// Last time we sync the status
	// +optional
	LastAttempt *metav1.Time
}

// ToolSelector is selector to select tool,contains enough information to let you inspect or modify the referred object.
type ToolSelector struct {
	corev1.ObjectReference
}

// ToolBindingReplicaSpec is the spec in ToolBindingReplica
type ToolBindingReplicaSpec struct {
	Selector ToolSelector

	// +optional
	Secret *SecretKeySetRef

	// +optional
	CodeRepoService *CodeRepoBindingReplicaTemplate
	// +optional
	ContinuousIntegration *CIBindingReplicaTemplate
	// +optional
	ImageRegistry *ImageRegistryBindingReplicaTemplate
	// +optional
	ProjectManagement *ProjectManagementBindingReplicaTemplate
	// +optional
	DocumentManagement *DocumentManagementBindingReplicaTemplate

	// +optional
	CodeQuality *CodeQualityBindingReplicaTemplate
	// +optional
	ArtifactRegistry *ArtifactRegistryBindingReplicaTemplate
}

func (spec *ToolBindingReplicaSpec) GetProjectsRef() []corev1.ObjectReference {
	if spec == nil {
		return []corev1.ObjectReference{}
	}

	switch spec.Selector.Kind {
	case TypeCodeRepoService:
		if spec.CodeRepoService == nil {
			return []corev1.ObjectReference{}
		}
		return spec.CodeRepoService.GetProjectsRef()
	case TypeJenkins:
		if spec.ContinuousIntegration == nil {
			return []corev1.ObjectReference{}
		}
		return spec.ContinuousIntegration.GetProjectsRef()
	case TypeCodeQualityTool:
		if spec.CodeQuality == nil {
			return []corev1.ObjectReference{}
		}
		return spec.CodeQuality.GetProjectsRef()
	case TypeImageRegistry:
		if spec.ImageRegistry == nil {
			return []corev1.ObjectReference{}
		}
		return spec.ImageRegistry.GetProjectsRef()
	case TypeProjectManagement:
		if spec.ProjectManagement == nil {
			return []corev1.ObjectReference{}
		}
		return spec.ProjectManagement.GetProjectsRef()
	case TypeDocumentManagement:
		if spec.DocumentManagement == nil {
			return []corev1.ObjectReference{}
		}
		return spec.DocumentManagement.GetProjectsRef()
	default:
		return []corev1.ObjectReference{}
	}
}

type BindingReplicaNamespaceSelector struct {
	// +optional
	metav1.LabelSelector
	// +optional
	NamespacesRef []string
}

func (selector BindingReplicaNamespaceSelector) IsNotEmpty() bool {
	return len(selector.MatchLabels) > 0 || len(selector.MatchExpressions) > 0 || len(selector.NamespacesRef) > 0
}

type ToolBindingReplicaTemplateInterface interface {
	GetProjectsRef() []corev1.ObjectReference // object reference to ProjectData

	//GetBindTemplates() interface{} //[]bindingTemplateInterface
}

type bindingTemplateInterface interface {
	GetNamespaceSelector() BindingReplicaNamespaceSelector
	//GetBindingSpec() interface{} // ToolBindingSpec
}

type CodeQualityBindingReplicaTemplate struct {
	// +optional
	Template *CodeQualityBindingReplicaTemplateSpec
}

func (template *CodeQualityBindingReplicaTemplate) GetProjectsRef() []corev1.ObjectReference {
	if template == nil {
		return []corev1.ObjectReference{}
	}
	return []corev1.ObjectReference{}
}

type CodeQualityBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []CodeQualityTemplate
}

type CodeQualityTemplate struct {
	Selector BindingReplicaNamespaceSelector
	Spec     CodeQualityBindingSpec
}

type DocumentManagementBindingReplicaTemplate struct {
	// +optional
	Spaces []DocumentManagementSpaceRef
	// +optional
	Template *DocumentManagementBindingReplicaTemplateSpec
}

func (template *DocumentManagementBindingReplicaTemplate) GetProjectsRef() []corev1.ObjectReference {
	if template == nil || template.Spaces == nil {
		return []corev1.ObjectReference{}
	}
	result := []corev1.ObjectReference{}
	for _, space := range template.Spaces {
		result = append(result, corev1.ObjectReference{Name: space.Name})
	}

	return result
}

type DocumentManagementBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []DocumentManagementBindingTemplate
}

type DocumentManagementBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector
	Spec     DocumentManagementBindingSpec
}

type ProjectManagementBindingReplicaTemplate struct {
	// +optional
	Projects []ProjectManagementProjectInfo
	// +optional
	Template *ProjectManagementBindingReplicaTemplateSpec
}

func (template *ProjectManagementBindingReplicaTemplate) GetProjectsRef() []corev1.ObjectReference {
	if template == nil || template.Projects == nil {
		return []corev1.ObjectReference{}
	}
	result := []corev1.ObjectReference{}
	for _, project := range template.Projects {
		result = append(result, corev1.ObjectReference{Name: project.Name})
	}

	return result
}

type ProjectManagementBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []ProjectManagementBindingTemplate
}

type ProjectManagementBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector
	Spec     ProjectManagementBindingSpec
}
type CodeRepoBindingReplicaTemplate struct {
	// +optional
	Owners []OwnerInRepository // resource of code repo
	// +optional
	Template *CodeRepoBindingReplicaTemplateSpec
}

func (template *CodeRepoBindingReplicaTemplate) GetProjectsRef() []corev1.ObjectReference {
	if template == nil || template.Owners == nil {
		return []corev1.ObjectReference{}
	}
	result := []corev1.ObjectReference{}
	for _, owner := range template.Owners {
		result = append(result, corev1.ObjectReference{Name: owner.Name})
	}

	return result
}

type CodeRepoBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []CodeRepoBindingTemplate
}

type CodeRepoBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector
	Spec     CodeRepoBindingSpec
}

type CIBindingReplicaTemplate struct {
	// no resources here
	// +optional
	Template *CIBindingReplicaTemplateSpec
}

func (template *CIBindingReplicaTemplate) GetProjectsRef() []corev1.ObjectReference {
	if template == nil {
		return []corev1.ObjectReference{}
	}
	return []corev1.ObjectReference{}
}

type CIBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []CIBindingTemplate
}

type CIBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector
	Spec     JenkinsBindingSpec
}

type ImageRegistryBindingReplicaTemplate struct {
	// +optional
	RepoInfo ImageRegistryBindingRepo // Resource assign to project
	// +optional
	Template *ImageRegistryBindingReplicaTemplateSpec
}

func (template *ImageRegistryBindingReplicaTemplate) GetProjectsRef() []corev1.ObjectReference {
	if template == nil || template.RepoInfo.Repositories == nil {
		return []corev1.ObjectReference{}
	}
	result := []corev1.ObjectReference{}
	// repo project is saved in RepoInfo.Repositories
	for _, repoProject := range template.RepoInfo.Repositories {
		result = append(result, corev1.ObjectReference{Name: repoProject})
	}

	return result
}

type ImageRegistryBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []ImageRegistryBindingTemplate
}

type ImageRegistryBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector
	Spec     ImageRegistryBindingSpec
}

type ArtifactRegistryBindingReplicaTemplate struct {
	// +optional
	Template *ArtifactRegistryBindingReplicaTemplateSpec
}

type ArtifactRegistryBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []ArtifactRegistryBindingTemplate
}

type ArtifactRegistryBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector
	Spec     ArtifactRegistryBindingSpec
}

// endregion
