package devops

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type ToolCategory struct {
	metav1.TypeMeta
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the Deployment.
	// +optional
	Spec ToolCategorySpec

	// Most recently observed status of the Deployment.
	// +optional
	Status ToolCategoryStatus
}

type ToolCategorySpec struct {
	DisplayName ToolCategorySpecDisplayName
	Enabled     bool
	Kind        string
}

type ToolCategoryStatus struct {
	Phase string
}

type ToolCategorySpecDisplayName struct {
	En string
	Zh string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ToolCategoryList struct {
	metav1.TypeMeta
	// +optional
	metav1.ListMeta

	Items []ToolCategory
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type SettingResp struct {
	// +optional
	metav1.TypeMeta
	DefaultDomain string
	Integrations  string
	Portal_link   string
	ToolChains    []ToolCategoryResp
	VersionGate   string
}

type ToolCategoryResp struct {
	Name        string
	DisplayName ToolCategorySpecDisplayName
	Enabled     bool
	ApiPath     string
	Items       []ToolTypeResp
}

type ToolTypeResp struct {
	DisplayName ToolTypeSpecDisplayName
	// +optional
	Description ToolTypeSpecDisplayName
	// +optional
	RecommendedVersion   string
	Host                 string
	Html                 string
	Name                 string
	Kind                 string
	Type                 string
	Public               bool
	Enterprise           bool
	Enabled              bool
	ApiPath              string
	SupportedSecretTypes []ToolTypeSpecSupportedSecretTypes
	RoleSyncEnabled      bool
	FeatureGate          string
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type Setting struct {
	metav1.TypeMeta
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the Deployment.
	// +optional
	Spec SettingSpec

	// Most recently observed status of the Deployment.
	// +optional
	Status ToolCategoryStatus
}

type SettingSpec struct {
	DefaultDomain string
	VersionGate   string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type SettingList struct {
	metav1.TypeMeta
	// +optional
	metav1.ListMeta

	Items []Setting
}
