package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateDocumentManagementBindingUpdate validates an update over the DocumentManagementBinding
func ValidateDocumentManagementBindingUpdate(new, old *devops.DocumentManagementBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateDocumentManagementBindingSpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateDocumentManagementBindingSpec spec data from DocumentManagementBindingSpec
func ValidateDocumentManagementBindingSpecUpdate(newSpec, oldSpec *devops.DocumentManagementBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateDocumentManagementBindingSpec(newSpec, fldPath)...)
	return
}

// ValidateDocumentManagementBinding validates a DocumentManagementBinding config
func ValidateDocumentManagementBinding(new *devops.DocumentManagementBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateDocumentManagementBindingSpec(&new.Spec, specField)
	return
}

// ValidateDocumentManagementBindingSpec validates spec
func ValidateDocumentManagementBindingSpec(spec *devops.DocumentManagementBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	errs = append(errs, ValidateLocalObjectReference(devops.LabelDocumentManagement, &spec.DocumentManagement, fldPath.Child(devops.LabelDocumentManagement))...)
	errs = append(errs, ValidateSecretKeySetRef(spec.Secret, fldPath.Child("secret"))...)
	errs = append(errs, ValidateSpaceRefs(spec.DocumentManagementSpaceRefs, fldPath.Child("documentmanagementspacerefs"))...)
	return
}

func ValidateSpaceRefs(SpaceRefs []devops.DocumentManagementSpaceRef, fldPath *field.Path) (errs field.ErrorList) {
	for i, spaceRef := range SpaceRefs {
		if spaceRef.Name == "" {
			errs = append(errs, field.Required(fldPath.Index(i).Child("name"), "should not be empty"))
		}
	}
	return
}
