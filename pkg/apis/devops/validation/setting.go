package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateSettingUpdate validates an update over the ToolCategoryTool
func ValidateSettingUpdate(new, old *devops.Setting) (errs field.ErrorList) {
	specFiled := field.NewPath("spec")

	// spec
	errs = ValidateSettingSpecUpdate(&new.Spec, &new.Spec, specFiled)
	return
}

// ValidateToolCategoryToolSpecUpdate spec data from ToolCategoryToolSpec
func ValidateSettingSpecUpdate(newSpec, oldSpec *devops.SettingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateSettingSpec(newSpec, fldPath)...)
	return
}

// ValidateToolCategoryTool validates a ToolCategoryTool config
func ValidateSetting(new *devops.Setting) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateSettingSpec(&new.Spec, specField)
	return
}

// ValidateToolCategoryToolSpec spec data from ToolCategoryToolSpec
func ValidateSettingSpec(spec *devops.SettingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	return
}
