package validation_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidateProjectManageBinding(t *testing.T) {

	type Table struct {
		name     string
		input    *devops.ProjectManagementBinding
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	invalidInput, invalidErrs := getInvalidProjectManageBinding()
	validInput, validErrs := getValidProjectManageBinding()

	table := []Table{
		{
			name:     "create: invalid ProjectManagementBinding",
			input:    invalidInput,
			expected: invalidErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateProjectManageBinding(obj.(*devops.ProjectManagementBinding))
			},
		},
		{
			name:     "create: valid ProjectManagementBinding",
			input:    validInput,
			expected: validErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateProjectManageBinding(obj.(*devops.ProjectManagementBinding))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func getValidProjectManageBinding() (*devops.ProjectManagementBinding, field.ErrorList) {
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	Secret.Namespace = "global-credentials"
	ProjectInfo := devops.ProjectManagementProjectInfo{}
	ProjectInfo.ID = "ID"
	ProjectInfo.Name = "Name"
	List := []devops.ProjectManagementProjectInfo{}
	List = append(List, ProjectInfo)
	return &devops.ProjectManagementBinding{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.ProjectManagementBindingSpec{
			ProjectManagement: devops.LocalObjectReference{
				Name: "projectManagement",
			},
			Secret: devops.SecretKeySetRef{
				SecretReference: *Secret,
			},
			ProjectManagementProjectInfos: List,
		},
	}, field.ErrorList{}
}

func getInvalidProjectManageBinding() (pipe *devops.ProjectManagementBinding, errs field.ErrorList) {
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	Secret.Namespace = "global-credentials"
	ProjectInfo := devops.ProjectManagementProjectInfo{}
	ProjectInfo.ID = "ID"
	ProjectInfo.Name = "Name"
	List := []devops.ProjectManagementProjectInfo{}
	List = append(List, ProjectInfo)
	pipe = &devops.ProjectManagementBinding{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.ProjectManagementBindingSpec{
			ProjectManagement: devops.LocalObjectReference{},
			Secret: devops.SecretKeySetRef{
				SecretReference: *Secret,
			},
			ProjectManagementProjectInfos: List,
		},
	}

	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("projectManagement").Child("name"),
			"",
			"please provide a projectManagement name",
		),
	}

	return
}
