package validation_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidateDocumentManagementBinding(t *testing.T) {

	type Table struct {
		name     string
		input    *devops.DocumentManagementBinding
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	invalidInput, invalidErrs := getInvalidDocumentManagementBinding()
	validInput, validErrs := getValidDocumentManagementBinding()

	table := []Table{
		{
			name:     "create: invalid DocumentManagementBinding",
			input:    invalidInput,
			expected: invalidErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateDocumentManagementBinding(obj.(*devops.DocumentManagementBinding))
			},
		},
		{
			name:     "create: valid DocumentManagementBinding",
			input:    validInput,
			expected: validErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateDocumentManagementBinding(obj.(*devops.DocumentManagementBinding))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func getValidDocumentManagementBinding() (*devops.DocumentManagementBinding, field.ErrorList) {
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	Secret.Namespace = "global-credentials"
	SpaceRef := devops.DocumentManagementSpaceRef{}
	SpaceRef.Name = "Name"
	List := []devops.DocumentManagementSpaceRef{}
	List = append(List, SpaceRef)
	return &devops.DocumentManagementBinding{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.DocumentManagementBindingSpec{
			DocumentManagement: devops.LocalObjectReference{
				Name: "documentManagement",
			},
			Secret: devops.SecretKeySetRef{
				SecretReference: *Secret,
			},
			DocumentManagementSpaceRefs: List,
		},
	}, field.ErrorList{}
}

func getInvalidDocumentManagementBinding() (pipe *devops.DocumentManagementBinding, errs field.ErrorList) {
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	Secret.Namespace = "global-credentials"
	SpaceRef := devops.DocumentManagementSpaceRef{}
	SpaceRef.Name = "Name"
	List := []devops.DocumentManagementSpaceRef{}
	List = append(List, SpaceRef)
	pipe = &devops.DocumentManagementBinding{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.DocumentManagementBindingSpec{
			DocumentManagement: devops.LocalObjectReference{},
			Secret: devops.SecretKeySetRef{
				SecretReference: *Secret,
			},
			DocumentManagementSpaceRefs: List,
		},
	}

	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("documentManagement").Child("name"),
			"",
			"please provide a documentManagement name",
		),
	}

	return
}
