package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateCodeQualityToolUpdate validates an update over the CodeQualityTool
func ValidateCodeQualityToolUpdate(new, old *devops.CodeQualityTool) (errs field.ErrorList) {
	specFiled := field.NewPath("spec")

	// spec
	errs = ValidateCodeQualityToolSpecUpdate(&new.Spec, &new.Spec, specFiled)
	return
}

// ValidateCodeQualityToolSpecUpdate spec data from CodeQualityToolSpec
func ValidateCodeQualityToolSpecUpdate(newSpec, oldSpec *devops.CodeQualityToolSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateCodeQualityToolSpec(newSpec, fldPath)...)
	return
}

// ValidateCodeQualityTool validates a CodeQualityTool config
func ValidateCodeQualityTool(new *devops.CodeQualityTool) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateCodeQualityToolSpec(&new.Spec, specField)
	return
}

// ValidateCodeQualityToolSpec spec data from CodeQualityToolSpec
func ValidateCodeQualityToolSpec(spec *devops.CodeQualityToolSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// http
	errs = append(errs, ValidateHostPort(&spec.HTTP, fldPath.Child("http"))...)
	return
}
