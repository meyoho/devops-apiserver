package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// ValidatePipelineTemplateSync validates a pipelineTemplateSync config
func ValidatePipelineTemplateSync(pipelineTemplateSync *devops.PipelineTemplateSync) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	//spec
	errs = ValidatePipelineTemplateSyncSpec(&pipelineTemplateSync.Spec, specField)
	// status
	errs = append(errs, validateStatus(pipelineTemplateSync.Status, field.NewPath("status"))...)
	return
}

func ValidatePipelineTemplateSyncUpdate(pipelineTemplateSync *devops.PipelineTemplateSync, old *devops.PipelineTemplateSync) (errs field.ErrorList) {
	if old == nil {
		old = pipelineTemplateSync
	}

	return
}

// ValidatePipelineTemplateSyncSpec spec data from PipelineTemplateSyncSpec
func ValidatePipelineTemplateSyncSpec(spec *devops.PipelineTemplateSyncSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// source
	errs = append(errs, ValidatePipelineSource(&spec.Source, fldPath.Child("source"))...)
	return
}

func validateStatus(status *devops.PipelineTemplateSyncStatus, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	if status != nil && !status.Phase.IsValid() {
		errs = append(errs, field.Invalid(
			fldPath.Child("phase"),
			status.Phase,
			"please provide valid phase",
		))
	}

	return
}
