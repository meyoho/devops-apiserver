package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"fmt"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"regexp"
)

const (
	regexURL string = "^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$" // Ref: https://stackoverflow.com/questions/136505/searching-for-uuids-in-text-with-regex
)

var urlregexp = regexp.MustCompile(regexURL)

// ValidateHostPort validate HostPort
func ValidateHostPort(hostPort *devops.HostPort, fldPath *field.Path) (errs field.ErrorList) {
	if hostPort == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			hostPort,
			"please provide a http configuration",
		))

	} else if hostPort.Host == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("host"),
			hostPort.Host,
			"please provide a host",
		))
	}
	return
}

// ValidateLocalObjectReference validates LocalObjectReference in spec
func ValidateLocalObjectReference(objectType string, objectReference *devops.LocalObjectReference, fldPath *field.Path) (errs field.ErrorList) {
	if objectReference == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			objectReference,
			fmt.Sprintf("please provide a %s", objectType),
		))

	} else if objectReference.Name == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("name"),
			objectReference.Name,
			fmt.Sprintf("please provide a %s name", objectType),
		))
	}
	return
}

// ValidateSecretKeySetRef validates secret
func ValidateSecretKeySetRef(secret devops.SecretKeySetRef, fldPath *field.Path) (errs field.ErrorList) {
	if secret.Name == "" {
		errs = append(errs, field.Required(fldPath.Child("name"), "should not be empty"))
	}
	if secret.Namespace == "" {
		errs = append(errs, field.Required(fldPath.Child("namespace"), "should not be empty"))
	}
	return
}
