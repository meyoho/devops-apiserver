package validation_test

import (
	"testing"

	corev1 "k8s.io/api/core/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidateCodeRepoBinding(t *testing.T) {

	type Table struct {
		name     string
		input    *devops.CodeRepoBinding
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	errorInput, fullErrors := getInvalidCodeRepoBinding()
	correctInput, correctErrs := getValidCodeRepoBinding()

	table := []Table{
		{
			name:     "create: empty CodeRepoBinding",
			input:    errorInput,
			expected: fullErrors,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeRepoBinding(obj.(*devops.CodeRepoBinding))
			},
		},
		{
			name:     "create: correct CodeRepoBinding",
			input:    correctInput,
			expected: correctErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeRepoBinding(obj.(*devops.CodeRepoBinding))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func getValidCodeRepoBinding() (*devops.CodeRepoBinding, field.ErrorList) {
	return &devops.CodeRepoBinding{
		Spec: devops.CodeRepoBindingSpec{
			CodeRepoService: devops.LocalObjectReference{
				Name: "github",
			},
			Account: devops.CodeRepoBindingAccount{
				Secret: devops.SecretKeySetRef{
					SecretReference: corev1.SecretReference{Name: "secret"},
				},
			},
		},
	}, field.ErrorList{}
}

func getInvalidCodeRepoBinding() (pipe *devops.CodeRepoBinding, errs field.ErrorList) {
	return &devops.CodeRepoBinding{}, field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("codeRepoService").Child("name"),
			"",
			"please provide a CodeRepoService name",
		),
		field.Invalid(
			field.NewPath("spec").Child("account").Child("secret"),
			"",
			"please provide a secret",
		),
	}
}
