package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// ValidateClusterPipelineTemplateSync validates a ClusterPipelineTemplateSync config
func ValidateClusterPipelineTemplateSync(clusterPipelineTemplateSync *devops.ClusterPipelineTemplateSync) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	//spec
	errs = ValidatePipelineTemplateSyncSpec(&clusterPipelineTemplateSync.Spec, specField)
	// status
	errs = append(errs, validateStatus(clusterPipelineTemplateSync.Status, field.NewPath("status"))...)
	return
}
