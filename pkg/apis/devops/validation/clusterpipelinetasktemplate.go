package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateClusterPipelineTaskTemplate validate for all fields
func ValidateClusterPipelineTaskTemplate(taskTemplate *devops.ClusterPipelineTaskTemplate) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	errs = ValidatePipelineTaskTemplateSpec(&taskTemplate.Spec, specField)

	return
}

// ValidateClusterPipelineTaskTemplateUpdate validate PipelineTaskTemplate when execute update action
func ValidateClusterPipelineTaskTemplateUpdate(spec *devops.ClusterPipelineTaskTemplate, old *devops.ClusterPipelineTaskTemplate) (errs field.ErrorList) {
	glog.V(7).Infof("spec: %v; old: %v", spec, old)
	return
}
