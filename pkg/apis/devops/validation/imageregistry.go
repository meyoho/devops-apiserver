package validation

import (
	"net/url"
	"regexp"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

const (
	reURL string = "^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$" // Ref: https://stackoverflow.com/questions/136505/searching-for-uuids-in-text-with-regex
)

var urlRegexp = regexp.MustCompile(reURL)

// ValidateImageRegistry validates a image registry config
func ValidateImageRegistry(imageRegistry *devops.ImageRegistry) (errs field.ErrorList) {
	specField := field.NewPath("spec")
	// spec
	errs = ValidateImageRegistrySpec(&imageRegistry.Spec, specField)
	return
}

// ValidateImageRegistrySpec spec data from ImageRegistry
func ValidateImageRegistrySpec(spec *devops.ImageRegistrySpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// type
	errs = append(errs, validateImageRegistryType(spec.Type, fldPath.Child("type"))...)
	// http
	errs = append(errs, validateImageRegistryHttp(&spec.HTTP, fldPath.Child("http"))...)
	return
}

func validateImageRegistryType(registryType devops.ImageRegistryType, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	switch registryType {
	case devops.RegistryTypeDocker:
	case devops.RegistryTypeHarbor:
	case devops.RegistryTypeAlauda:
	case devops.RegistryTypeDockerHub:
	default:
		errs = append(errs, field.Invalid(
			fldPath,
			registryType,
			"please provide a valid registry type: Docker",
		))
	}
	return
}

func validateImageRegistryHttp(hostPort *devops.HostPort, fldPath *field.Path) (errs field.ErrorList) {
	_, err := url.Parse(hostPort.Host)
	if err != nil || hostPort.Host == "" || !urlRegexp.MatchString(hostPort.Host) {
		errs = append(errs, field.Invalid(
			fldPath.Child("host"),
			hostPort.Host,
			"provided hostname is not a valid url",
		))
	}
	return
}
