package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateDocumentManageUpdate validates an update over the DocumentManagement
func ValidateDocumentManageUpdate(new *devops.DocumentManagement, old *devops.DocumentManagement) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateDocumentManageSpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateDocumentManageSpec spec data from DocumentManagementSpec
func ValidateDocumentManageSpecUpdate(newSpec, oldSpec *devops.DocumentManagementSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateDocumentManageSpec(newSpec, fldPath)...)
	return
}

// ValidateDocumentManagement validates a DocumentManagement config
func ValidateDocumentManagement(new *devops.DocumentManagement) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateDocumentManageSpec(&new.Spec, specField)
	return
}

// ValidateDocumentManageSpec spec data from DocumentManagementSpec
func ValidateDocumentManageSpec(spec *devops.DocumentManagementSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	// http
	errs = append(errs, ValidateHostPort(&spec.HTTP, fldPath.Child("http"))...)
	errs = append(errs, ValidataDocumentType(&spec.Type, fldPath.Child("type"))...)
	return
}

// ValidateHostPort validate HostPort
func ValidataDocumentType(documentManagementType *devops.DocumentManagementType, fldPath *field.Path) (errs field.ErrorList) {
	if !documentManagementType.IsValid() {
		errs = append(errs, field.Invalid(
			fldPath,
			documentManagementType,
			"please provide a right documentManagement type",
		))
	}
	return
}
