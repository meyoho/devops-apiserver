package validation_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidateCodeQualityTool(t *testing.T) {
	type Table struct {
		name     string
		input    *devops.CodeQualityTool
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	errorInputWithoutHost, fullErrorsWithoutHost := getInvalidCodeQualityToolWithoutHost()
	errorInputWithoutNothing, fullErrorsWithoutNothing := getInvalidCodeQualityToolWithoutNothing()
	correctInput, correctErrs := getValidCodeQualityTool()

	table := []Table{
		{
			name:     "create: invalid CodeQualityTool without host",
			input:    errorInputWithoutHost,
			expected: fullErrorsWithoutHost,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeQualityTool(obj.(*devops.CodeQualityTool))
			},
		},
		{
			name:     "create: invalid CodeQualityTool with nothing",
			input:    errorInputWithoutNothing,
			expected: fullErrorsWithoutNothing,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeQualityTool(obj.(*devops.CodeQualityTool))
			},
		},
		{
			name:     "create: correct CodeQualityTool",
			input:    correctInput,
			expected: correctErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeQualityTool(obj.(*devops.CodeQualityTool))
			},
		},
	}

	for i, cqt := range table {
		res := cqt.method(cqt.input)
		if len(res) != len(cqt.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, cqt.name,
				len(cqt.expected), len(res),
				cqt.expected, res,
			)
		} else {
			for j, e := range cqt.expected {
				if e.Error() != res[j].Error() {
					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, cqt.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func getValidCodeQualityTool() (*devops.CodeQualityTool, field.ErrorList) {
	return &devops.CodeQualityTool{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.CodeQualityToolSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "https://www.sonarqube.com",
				},
			},
		},
	}, field.ErrorList{}
}

func getInvalidCodeQualityToolWithoutHost() (cqt *devops.CodeQualityTool, errs field.ErrorList) {
	cqt = &devops.CodeQualityTool{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.CodeQualityToolSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{},
			},
		},
	}

	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("http").Child("host"),
			"",
			"please provide a host",
		),
	}

	return
}

func getInvalidCodeQualityToolWithoutNothing() (cqt *devops.CodeQualityTool, errs field.ErrorList) {
	cqt = &devops.CodeQualityTool{
		ObjectMeta: metav1.ObjectMeta{},
		Spec:       devops.CodeQualityToolSpec{},
	}

	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("http").Child("host"),
			cqt.Spec.HTTP.Host,
			"please provide a host",
		),
	}

	return
}
