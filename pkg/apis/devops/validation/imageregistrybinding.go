package validation

import (
	"regexp"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"fmt"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

const (
	regexRepo             = "^[a-z0-9][a-z0-9\\.\\_\\-\\/]*"
	regexProject          = "^[a-z0-9][a-z0-9\\.\\_\\-]+"
	fieldNameRepoInfo     = "repoInfo"
	fieldNameRepositories = "repositories"
)

var (
	reporegexp    = regexp.MustCompile(regexRepo)
	projectregexp = regexp.MustCompile(regexProject)
)

// ValidateImageRegistryBinding validates a imageRegistryBinding config
func ValidateImageRegistryBinding(imageRegistryBinding *devops.ImageRegistryBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")
	// spec
	errs = ValidateImageRegistryBindingSpec(&imageRegistryBinding.Spec, specField)
	return
}

// ValidateImageRegistryBindingSpec validates an update over the ImageRegistryBinding
func ValidateImageRegistryBindingSpec(spec *devops.ImageRegistryBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// ImageRegistry
	errs = append(errs, validateSpecImageRegistry(&spec.ImageRegistry, fldPath.Child("imageRegistry"))...)
	// RepoInfo
	errs = append(errs, validateSpecRepoInfo(&spec.RepoInfo, fldPath.Child(fieldNameRepoInfo))...)

	return
}

func validateSpecImageRegistry(imageRegistry *devops.LocalObjectReference, fldPath *field.Path) (errs field.ErrorList) {
	if imageRegistry == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			imageRegistry,
			"please provide a ImageRegistry",
		))
	} else if imageRegistry.Name == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("name"),
			imageRegistry.Name,
			"please provide a ImageRegistry name",
		))
	}
	return
}

func validateSpecRepoInfo(repoInfo *devops.ImageRegistryBindingRepo, fldPath *field.Path) (errs field.ErrorList) {
	if repoInfo == nil || len(repoInfo.Repositories) == 0 {
		glog.Info("ImageRegistryBindingRepo repositories path is nil")
		return
	}
	for _, repo := range repoInfo.Repositories {
		if repo != "/" {
			if strings.HasPrefix(repo, "/") || strings.HasSuffix(repo, "/") {
				errs = append(errs, field.Invalid(
					fldPath.Child(fieldNameRepoInfo).Child(fieldNameRepositories),
					repoInfo.Repositories,
					"repositories path can not include / or \\",
				))
			} else if !reporegexp.MatchString(repo) {
				errs = append(errs, field.Invalid(
					fldPath.Child(fieldNameRepoInfo).Child(fieldNameRepositories),
					repoInfo.Repositories,
					"repositories element include illegal char, just support start or end with a-z0-9 and a-z0-9_-\\/",
				))
			}
		}
	}
	return
}

func ValidateImageCreateProject(name string) (err error) {
	if !projectregexp.MatchString(name) {
		return fmt.Errorf("name: %s can't match regex: %s", name, regexProject)
	}
	return
}
