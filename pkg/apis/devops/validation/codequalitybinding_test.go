package validation_test

import (
	corev1 "k8s.io/api/core/v1"
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidateCodeQualityBinding(t *testing.T) {
	type Table struct {
		name     string
		input    *devops.CodeQualityBinding
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	errorInput, fullErrors := getInvalidCodeQualityBinding()
	correctInput, correctErrs := getValidCodeQualityBinding()

	table := []Table{
		{
			name:     "create: error CodeQualityBinding",
			input:    errorInput,
			expected: fullErrors,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeQualityBinding(obj.(*devops.CodeQualityBinding))
			},
		},
		{
			name:     "create: correct CodeQualityBinding",
			input:    correctInput,
			expected: correctErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeQualityBinding(obj.(*devops.CodeQualityBinding))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {
					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func getValidCodeQualityBinding() (*devops.CodeQualityBinding, field.ErrorList) {
	return &devops.CodeQualityBinding{
		Spec: devops.CodeQualityBindingSpec{
			CodeQualityTool: devops.LocalObjectReference{
				Name: "Sonarqube",
			},
			Secret: devops.SecretKeySetRef{
				SecretReference: corev1.SecretReference{Name: "secret"},
			},
		},
	}, field.ErrorList{}
}

func getInvalidCodeQualityBinding() (pipe *devops.CodeQualityBinding, errs field.ErrorList) {
	return &devops.CodeQualityBinding{}, field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("codeQualityTool").Child("name"),
			"",
			"please provide a CodeQualityTool name",
		),
	}
}
