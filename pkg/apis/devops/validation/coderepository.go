package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateCodeRepository validates a CodeRepository config
func ValidateCodeRepository(codeRepository *devops.CodeRepository) (errs field.ErrorList) {
	specField := field.NewPath("spec")
	// spec
	errs = ValidateCodeRepositorySpec(&codeRepository.Spec, specField)
	return
}

// ValidateCodeRepositoryUpdate validates an update over the CodeRepository
func ValidateCodeRepositoryUpdate(new *devops.CodeRepository, old *devops.CodeRepository) (errs field.ErrorList) {
	glog.V(7).Infof("new: %v; old: %v", new, old)
	specField := field.NewPath("spec")
	// spec
	errs = ValidateCodeRepositorySpec(&new.Spec, specField)
	return
}

// ValidateCodeRepositorySpec spec data from CodeRepositorySpec
func ValidateCodeRepositorySpec(spec *devops.CodeRepositorySpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// codeRepoBinding
	errs = append(errs, validateCodeRepoBinding(&spec.CodeRepoBinding, fldPath.Child("codeRepoBinding"))...)
	// repository
	errs = append(errs, validateOriginCodeRepository(&spec.Repository, fldPath.Child("repository"))...)

	return
}

func validateOriginCodeRepository(repository *devops.OriginCodeRepository, fldPath *field.Path) (errs field.ErrorList) {
	if repository == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			repository,
			"please provide a repository configuration",
		))
		return
	}

	errs = append(errs, ValidateServiceType(repository.CodeRepoServiceType, fldPath.Child("codeRepoServiceType"))...)
	return
}

func validateCodeRepoBinding(codeRepoBinding *devops.LocalObjectReference, fldPath *field.Path) (errs field.ErrorList) {
	if codeRepoBinding == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			codeRepoBinding,
			"please provide a CodeRepoBinding configuration",
		))
	} else if codeRepoBinding.Name == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("name"),
			codeRepoBinding.Name,
			"please provide a CodeRepoBinding name",
		))
	}
	return
}
