package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateTestToolUpdate validates an update over the TestTool
func ValidateTestToolUpdate(new *devops.TestTool, old *devops.TestTool) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateTestToolSpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateTestToolSpec spec data from TestToolSpec
func ValidateTestToolSpecUpdate(newSpec, oldSpec *devops.TestToolSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateTestToolSpec(newSpec, fldPath)...)
	return
}

// ValidateTestTool validates a TestTool config
func ValidateTestTool(new *devops.TestTool) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateTestToolSpec(&new.Spec, specField)
	return
}

// ValidateTestToolSpec spec data from TestToolSpec
func ValidateTestToolSpec(spec *devops.TestToolSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// http
	errs = append(errs, ValidateHostPort(&spec.HTTP, fldPath.Child("http"))...)
	return
}
