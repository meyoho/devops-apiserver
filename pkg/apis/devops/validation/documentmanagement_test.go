package validation_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidateDocumentManagement(t *testing.T) {

	type Table struct {
		name     string
		input    *devops.DocumentManagement
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	errorInputWithoutHost, fullErrorsWithoutHost := getInvalidDocumentManageWithoutHost()
	errorInputWithNothing, fullErrorsWithNothing := getInvalidDocumentManageWithNothing()
	correctInput, correctErrs := getValidDocumentManagement()

	table := []Table{
		{
			name:     "create: invalid DocumentManagement without host",
			input:    errorInputWithoutHost,
			expected: fullErrorsWithoutHost,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateDocumentManagement(obj.(*devops.DocumentManagement))
			},
		},
		{
			name:     "create: invalid DocumentManagement with nothing",
			input:    errorInputWithNothing,
			expected: fullErrorsWithNothing,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateDocumentManagement(obj.(*devops.DocumentManagement))
			},
		},
		{
			name:     "create: correct DocumentManagement",
			input:    correctInput,
			expected: correctErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateDocumentManagement(obj.(*devops.DocumentManagement))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)

		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func getValidDocumentManagement() (*devops.DocumentManagement, field.ErrorList) {
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	return &devops.DocumentManagement{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.DocumentManagementSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "https://aaa.bbb.ccc",
				},
				Secret: devops.SecretKeySetRef{
					SecretReference: *Secret,
				},
			},
			Type: "Confluence",
		},
	}, field.ErrorList{}
}

func getInvalidDocumentManageWithoutHost() (pipe *devops.DocumentManagement, errs field.ErrorList) {
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	pipe = &devops.DocumentManagement{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.DocumentManagementSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{},
				Secret: devops.SecretKeySetRef{
					SecretReference: *Secret,
				},
			},
			Type: "Confluence",
		},
	}
	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("http").Child("host"),
			"",
			"please provide a host",
		),
	}
	return
}

func getInvalidDocumentManageWithNothing() (pipe *devops.DocumentManagement, errs field.ErrorList) {
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	pipe = &devops.DocumentManagement{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.DocumentManagementSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{},
				Secret: devops.SecretKeySetRef{
					SecretReference: *Secret,
				},
			},
		},
	}
	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("http").Child("host"),
			pipe.Spec.HTTP.Host,
			"please provide a host",
		),
		field.Invalid(
			field.NewPath("spec").Child("type"),
			pipe.Spec.Type,
			"please provide a right documentManagement type",
		),
	}
	return
}
