package validation_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidateTestTool(t *testing.T) {

	type Table struct {
		name     string
		input    *devops.TestTool
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	errorInputWithoutHost, fullErrorsWithoutHost := getInvalidTestToolWithoutHost()
	errorInputWithNothing, fullErrorsWithNothing := getInvalidTestToolWithNothing()
	correctInput, correctErrs := getValidTestTool()

	table := []Table{
		{
			name:     "create: invalid TestTool without host",
			input:    errorInputWithoutHost,
			expected: fullErrorsWithoutHost,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateTestTool(obj.(*devops.TestTool))
			},
		},
		{
			name:     "create: invalid TestTool with nothing",
			input:    errorInputWithNothing,
			expected: fullErrorsWithNothing,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateTestTool(obj.(*devops.TestTool))
			},
		},
		{
			name:     "create: correct TestTool",
			input:    correctInput,
			expected: correctErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateTestTool(obj.(*devops.TestTool))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func getValidTestTool() (*devops.TestTool, field.ErrorList) {
	return &devops.TestTool{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.TestToolSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "https://aaa.bbb.ccc",
				},
			},
		},
	}, field.ErrorList{}
}

func getInvalidTestToolWithoutHost() (pipe *devops.TestTool, errs field.ErrorList) {
	pipe = &devops.TestTool{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.TestToolSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{},
			},
		},
	}

	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("http").Child("host"),
			"",
			"please provide a host",
		),
	}

	return
}

func getInvalidTestToolWithNothing() (pipe *devops.TestTool, errs field.ErrorList) {
	pipe = &devops.TestTool{
		ObjectMeta: metav1.ObjectMeta{},
		Spec:       devops.TestToolSpec{},
	}

	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("http").Child("host"),
			pipe.Spec.HTTP.Host,
			"please provide a host",
		),
	}

	return
}
