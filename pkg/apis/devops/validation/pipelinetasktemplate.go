package validation

import (
	"fmt"
	"strings"

	glog "k8s.io/klog"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// ValidatePipelineTaskTemplate validate for all fields
func ValidatePipelineTaskTemplate(taskTemplate *devops.PipelineTaskTemplate) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	errs = ValidatePipelineTaskTemplateSpec(&taskTemplate.Spec, specField)

	return
}

// ValidatePipelineTaskTemplateSpec PipelineTaskTemplate spec
func ValidatePipelineTaskTemplateSpec(spec *devops.PipelineTaskTemplateSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// agent
	errs = append(errs, ValidateJenkinsAgent(spec.Agent, fldPath.Child("agent"))...)
	// dependencies
	errs = append(errs, ValidateDependencies(spec.Dependencies, fldPath.Child("dependencies"))...)
	return
}

// ValidateJenkinsAgent will validate the field of agent
func ValidateJenkinsAgent(agent *devops.JenkinsAgent, fldPath *field.Path) (errs field.ErrorList) {
	if agent != nil && strings.TrimSpace(agent.Label) == "" {
		// we only support label style of agent
		errs = append(errs, field.Invalid(
			fldPath,
			agent,
			"please provide a label for the agent",
		))
	}

	return
}

// ValidateDependencies validate jenkins plugin dependencies
func ValidateDependencies(dependencies *devops.PipelineDependency, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if dependencies == nil {
		return
	}

	plugins := dependencies.Plugins
	if len(plugins) == 0 {
		return
	}

	for index, plugin := range plugins {
		if plugin.Name == "" {
			errs = append(errs, field.Invalid(
				fldPath.Child("plugins"), // TODO-suren need validate items
				plugin,
				fmt.Sprintf("please provide a name, index %d", index),
			))
		}

		if plugin.Version == "" {
			errs = append(errs, field.Invalid(
				fldPath.Child("plugins"),
				plugin,
				fmt.Sprintf("please provide a version, index %d", index),
			))
		}
	}

	return
}

// ValidatePipelineTaskTemplateUpdate validate PipelineTaskTemplate when execute update action
func ValidatePipelineTaskTemplateUpdate(new *devops.PipelineTaskTemplate, old *devops.PipelineTaskTemplate) (errs field.ErrorList) {
	glog.V(7).Infof("new: %v; old: %v", new, old)
	return
}
