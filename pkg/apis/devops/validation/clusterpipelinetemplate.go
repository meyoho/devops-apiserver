package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateClusterPipelineTemplate validation for ClusterPipelineTemplate create action
func ValidateClusterPipelineTemplate(clusterpipelinetemplate *devops.ClusterPipelineTemplate) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// spec
	errs = append(errs, ValidatePipelineTemplateSpec(&clusterpipelinetemplate.Spec, field.NewPath("spec"))...)
	//  metadata
	errs = append(errs, ValidateTemplateMetadata(clusterpipelinetemplate.GetObjectMeta(), field.NewPath("metadata"))...)

	return
}

// ValidateClusterPipelineTemplateUpdate validation for ClusterPipelineTemplate update actioin
func ValidateClusterPipelineTemplateUpdate(obj *devops.ClusterPipelineTemplate, old *devops.ClusterPipelineTemplate) (errs field.ErrorList) {
	glog.V(7).Infof("obj: %v; old: %v", obj, old)
	errs = field.ErrorList{}
	return
}
