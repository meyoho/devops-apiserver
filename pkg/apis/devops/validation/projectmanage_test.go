package validation_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidateProjectManage(t *testing.T) {

	type Table struct {
		name     string
		input    *devops.ProjectManagement
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	errorInputWithoutHost, fullErrorsWithoutHost := getInvalidProjectManageWithoutHost()
	errorInputWithNothing, fullErrorsWithNothing := getInvalidProjectManageWithNothing()
	correctInput, correctErrs := getValidProjectManage()

	table := []Table{
		{
			name:     "create: invalid ProjectManagement without host",
			input:    errorInputWithoutHost,
			expected: fullErrorsWithoutHost,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateProjectManage(obj.(*devops.ProjectManagement))
			},
		},
		{
			name:     "create: invalid ProjectManagement with nothing",
			input:    errorInputWithNothing,
			expected: fullErrorsWithNothing,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateProjectManage(obj.(*devops.ProjectManagement))
			},
		},
		{
			name:     "create: correct ProjectManagement",
			input:    correctInput,
			expected: correctErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateProjectManage(obj.(*devops.ProjectManagement))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)

		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func getValidProjectManage() (*devops.ProjectManagement, field.ErrorList) {
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	return &devops.ProjectManagement{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.ProjectManagementSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "https://aaa.bbb.ccc",
				},
				Secret: devops.SecretKeySetRef{
					SecretReference: *Secret,
				},
			},
			Type: "Jira",
		},
	}, field.ErrorList{}
}

func getInvalidProjectManageWithoutHost() (pipe *devops.ProjectManagement, errs field.ErrorList) {
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	pipe = &devops.ProjectManagement{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.ProjectManagementSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{},
				Secret: devops.SecretKeySetRef{
					SecretReference: *Secret,
				},
			},
			Type: "Jira",
		},
	}
	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("http").Child("host"),
			"",
			"please provide a host",
		),
	}
	return
}

func getInvalidProjectManageWithNothing() (pipe *devops.ProjectManagement, errs field.ErrorList) {
	Secret := &corev1.SecretReference{}
	Secret.Name = "test"
	pipe = &devops.ProjectManagement{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.ProjectManagementSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{},
				Secret: devops.SecretKeySetRef{
					SecretReference: *Secret,
				},
			},
		},
	}
	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("http").Child("host"),
			pipe.Spec.HTTP.Host,
			"please provide a host",
		),
		field.Invalid(
			field.NewPath("spec").Child("type"),
			pipe.Spec.Type,
			"please provide a right type",
		),
	}
	return
}
