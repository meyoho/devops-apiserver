package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

const (
	shouldNotEmptyMsg = "should not be empty"
)

type tbrSpecTmplValidator func(*devops.ToolBindingReplicaSpec, *field.Path) field.ErrorList

var bindingReplicaTemplateValidatorMap = map[string]tbrSpecTmplValidator{
	devops.TypeJenkins:            validateJenkinsBindingReplicaTemplate,
	devops.TypeCodeRepoService:    validateCodeRepoBindingReplicaTemplate,
	devops.TypeImageRegistry:      validateImageRegistryBindingReplicaTemplate,
	devops.TypeDocumentManagement: validateDocumentManagementBindingReplicaTemplate,
	devops.TypeCodeQualityTool:    validateCodeQualityBindingReplicaTemplate,
	devops.TypeArtifactRegistry:   validateArtifactRegistryBindingReplicaTemplate,
}

func validateCodeRepoBindingReplicaTemplate(spec *devops.ToolBindingReplicaSpec, rootfldPath *field.Path) (errs field.ErrorList) {
	if spec.CodeRepoService == nil {
		// there is nothing need to validate
		return
	}

	rootfldPath = rootfldPath.Child("codeRepoService")

	if len(spec.CodeRepoService.Owners) == 0 {
		errs = append(errs, field.Required(rootfldPath.Child("owners"), shouldNotEmptyMsg))
		return errs
	}
	fldPath := rootfldPath.Child("owners")
	for index, owner := range spec.CodeRepoService.Owners {
		if owner.Name == "" {
			errs = append(errs, field.Required(fldPath.Index(index).Child("name"), shouldNotEmptyMsg))
		}
		if owner.Type == "" {
			errs = append(errs, field.Required(fldPath.Index(index).Child("type"), shouldNotEmptyMsg))
		}
	}

	if spec.CodeRepoService.Template != nil {
		fldPath = rootfldPath.Child("template")
		if len(spec.CodeRepoService.Template.Bindings) == 0 {
			errs = append(errs, field.Required(fldPath.Child("bindings"), shouldNotEmptyMsg))
			return errs
		}
		fldPath = fldPath.Child("bindings")
		for index, binding := range spec.CodeRepoService.Template.Bindings {
			fldPath = fldPath.Index(index)
			errs = append(errs, validateToolBindingNamespaceSelector(binding.Selector, fldPath.Child("selector"))...)
			errs = append(errs, ValidateCodeRepoBindingSpec(&binding.Spec, fldPath.Child("spec"))...)
		}
	}

	return errs
}

func validateImageRegistryBindingReplicaTemplate(spec *devops.ToolBindingReplicaSpec, rootfldPath *field.Path) (errs field.ErrorList) {
	if spec.ImageRegistry == nil {
		// there is nothing need to validate
		return
	}
	rootfldPath = rootfldPath.Child("imageRegistry")

	if len(spec.ImageRegistry.RepoInfo.Repositories) == 0 {
		errs = append(errs, field.Required(rootfldPath.Child("repoInfo").Child("repositories"), shouldNotEmptyMsg))
		return errs
	}
	if spec.ImageRegistry.Template == nil {
		return errs
	}
	// spec.ImageRegistry.Template != nil
	fldPath := rootfldPath.Child("template")
	if len(spec.ImageRegistry.Template.Bindings) == 0 {
		errs = append(errs, field.Required(fldPath.Child("bindings"), shouldNotEmptyMsg))
		return errs
	}
	fldPath = fldPath.Child("bindings")
	for index, binding := range spec.ImageRegistry.Template.Bindings {
		fldPath = fldPath.Index(index)
		errs = append(errs, validateToolBindingNamespaceSelector(binding.Selector, fldPath.Child("selector"))...)
		errs = append(errs, ValidateImageRegistryBindingSpec(&binding.Spec, fldPath.Child("spec"))...)
	}
	return errs
}

func validateDocumentManagementBindingReplicaTemplate(spec *devops.ToolBindingReplicaSpec, rootfldPath *field.Path) (errs field.ErrorList) {
	if spec.DocumentManagement == nil {
		// there is nothing need to validate
		return
	}
	rootfldPath = rootfldPath.Child("documentManagement")

	if len(spec.DocumentManagement.Spaces) == 0 {
		errs = append(errs, field.Required(rootfldPath.Child("repoInfo").Child("repositories"), shouldNotEmptyMsg))
		return errs
	}
	if spec.DocumentManagement.Template == nil {
		return errs
	}
	// spec.DocumentManagement.Template != nil
	fldPath := rootfldPath.Child("template")
	if len(spec.DocumentManagement.Template.Bindings) == 0 {
		errs = append(errs, field.Required(fldPath.Child("bindings"), shouldNotEmptyMsg))
		return errs
	}
	fldPath = fldPath.Child("bindings")
	for index, binding := range spec.DocumentManagement.Template.Bindings {
		fldPath = fldPath.Index(index)
		errs = append(errs, validateToolBindingNamespaceSelector(binding.Selector, fldPath.Child("selector"))...)
		errs = append(errs, ValidateDocumentManagementBindingSpec(&binding.Spec, fldPath.Child("spec"))...)
	}
	return errs
}

func validateCodeQualityBindingReplicaTemplate(spec *devops.ToolBindingReplicaSpec, rootfldPath *field.Path) (errs field.ErrorList) {
	if spec.CodeQuality == nil {
		// there is nothing need to validate
		return
	}
	fldPath := rootfldPath.Child("codeQualityTool").Child("template")

	// it should not be nil in jenkins
	if spec.CodeQuality.Template == nil {
		errs = append(errs, field.Required(fldPath.Child("template"), "should not be nil"))
		return nil
	}

	if len(spec.CodeQuality.Template.Bindings) == 0 {
		errs = append(errs, field.Required(fldPath.Child("bindings"), shouldNotEmptyMsg))
		return errs
	}
	fldPath = fldPath.Child("bindings")
	for index, binding := range spec.CodeQuality.Template.Bindings {
		fldPath = fldPath.Index(index)
		errs = append(errs, validateToolBindingNamespaceSelector(binding.Selector, fldPath.Child("selector"))...)
		errs = append(errs, ValidateCodeQualityBindingSpec(&binding.Spec, fldPath.Child("spec"))...)
	}
	return errs
}

func validateArtifactRegistryBindingReplicaTemplate(spec *devops.ToolBindingReplicaSpec, rootfldPath *field.Path) (errs field.ErrorList) {
	if spec.ArtifactRegistry == nil {
		// there is nothing need to validate
		return
	}
	fldPath := rootfldPath.Child("artifactRegistry").Child("template")

	// it should not be nil in jenkins
	if spec.ArtifactRegistry.Template == nil {
		errs = append(errs, field.Required(fldPath.Child("template"), "should not be nil"))
		return nil
	}

	if len(spec.ArtifactRegistry.Template.Bindings) == 0 {
		errs = append(errs, field.Required(fldPath.Child("bindings"), shouldNotEmptyMsg))
		return errs
	}
	fldPath = fldPath.Child("bindings")
	for index, binding := range spec.ArtifactRegistry.Template.Bindings {
		fldPath = fldPath.Index(index)
		errs = append(errs, validateToolBindingNamespaceSelector(binding.Selector, fldPath.Child("selector"))...)
		errs = append(errs, ValidateArtifactRegistryBindingSpec(&binding.Spec, fldPath.Child("spec"))...)
	}
	return errs
}

func validateJenkinsBindingReplicaTemplate(spec *devops.ToolBindingReplicaSpec, rootfldPath *field.Path) (errs field.ErrorList) {
	if spec.ContinuousIntegration == nil {
		// there is nothing need to validate
		return
	}
	fldPath := rootfldPath.Child("continuousIntegration").Child("template")

	// it should not be nil in jenkins
	if spec.ContinuousIntegration.Template == nil {
		errs = append(errs, field.Required(fldPath.Child("template"), "should not be nil"))
		return nil
	}

	if len(spec.ContinuousIntegration.Template.Bindings) == 0 {
		errs = append(errs, field.Required(fldPath.Child("bindings"), shouldNotEmptyMsg))
		return errs
	}
	fldPath = fldPath.Child("bindings")
	for index, binding := range spec.ContinuousIntegration.Template.Bindings {
		fldPath = fldPath.Index(index)
		errs = append(errs, validateToolBindingNamespaceSelector(binding.Selector, fldPath.Child("selector"))...)
		errs = append(errs, ValidateJenkinsBindingSpec(binding.Spec, fldPath.Child("spec"))...)
	}
	return errs
}

// ValidateToolBindingReplicaUpdate validates an update over the ToolBindingReplica
func ValidateToolBindingReplicaUpdate(new, old *devops.ToolBindingReplica) (errs field.ErrorList) {
	specField := field.NewPath("spec")
	// spec
	errs = ValidateToolBindingReplicaSpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateToolBindingReplicaSpecUpdate spec data from ToolBindingReplicaSpec
func ValidateToolBindingReplicaSpecUpdate(newSpec, oldSpec *devops.ToolBindingReplicaSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateToolBindingReplicaSpec(newSpec, fldPath)...)
	return
}

// ValidateToolBindingReplica validates a ToolBindingReplica config
func ValidateToolBindingReplica(new *devops.ToolBindingReplica) (errs field.ErrorList) {
	specField := field.NewPath("spec")
	// spec
	errs = ValidateToolBindingReplicaSpec(&new.Spec, specField)
	return
}

// ValidateToolBindingReplicaSpec validates spec
func ValidateToolBindingReplicaSpec(spec *devops.ToolBindingReplicaSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = append(errs, validateToolBindingReplicaSecret(spec.Secret, fldPath.Child("secret"))...)
	errs = append(errs, validateToolBindingReplicaSelector(spec.Selector, fldPath.Child("selector"))...)
	errs = append(errs, validToolBindingReplicaSpecTemplate(spec, fldPath)...)
	return
}

func validateToolBindingReplicaSecret(secret *devops.SecretKeySetRef, fldPath *field.Path) (errs field.ErrorList) {
	if secret == nil {
		errs = append(errs, field.Required(fldPath, "should not be empty"))
		return errs
	}

	return ValidateSecretKeySetRef(*secret, fldPath)
}

func validateToolBindingReplicaSelector(selector devops.ToolSelector, fldPath *field.Path) (errs field.ErrorList) {

	if selector.Name == "" {
		errs = append(errs, field.Required(
			fldPath.Child("name"),
			shouldNotEmptyMsg,
		))
	}

	if selector.Kind == "" {
		errs = append(errs, field.Required(
			fldPath.Child("kind"),
			shouldNotEmptyMsg,
		))
	}

	return
}

func validToolBindingReplicaSpecTemplate(spec *devops.ToolBindingReplicaSpec, rootfldPath *field.Path) (errs field.ErrorList) {

	validate, ok := bindingReplicaTemplateValidatorMap[spec.Selector.Kind]
	if !ok {
		glog.Errorf("Kind of ToolBindingReplica %s cannot validate, not find any function used to validate", spec.Selector.Kind)
		return
	}

	return validate(spec, rootfldPath)
}

func validateToolBindingReplicaTemplateMeta(fldPath *field.Path, metadata metav1.ObjectMeta) (errs field.ErrorList) {
	if metadata.Namespace == "" {
		errs = append(errs, field.Required(fldPath.Child("namespace"), shouldNotEmptyMsg))
	}
	if metadata.Name == "" {
		errs = append(errs, field.Required(fldPath.Child("name"), shouldNotEmptyMsg))
	}
	return errs
}

// ValidateJenkinsBindingSpec validate jenkinsBindingSpec
func ValidateJenkinsBindingSpec(spec devops.JenkinsBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = append(errs, ValidateLocalObjectReference("jenkinsInstance",
		&devops.LocalObjectReference{Name: spec.Jenkins.Name},
		fldPath.Child("jenkins"))...)
	errs = append(errs, ValidateSecretKeySetRef(spec.Account.Secret, fldPath.Child("account").Child("secret"))...)
	return
}

// ValidateToolBindingReplicaStatus will validate status of toolbindingreplica
func ValidateToolBindingReplicaStatus(tbr devops.ToolBindingReplica) (errs field.ErrorList) {
	rootField := field.NewPath("status")
	if string(tbr.Status.Phase) == "" {
		errs = append(errs, field.Required(rootField.Child("phase"), shouldNotEmptyMsg))
	}

	return errs
}

func validateToolBindingNamespaceSelector(namespaceSelector devops.BindingReplicaNamespaceSelector, fldPath *field.Path) (errs field.ErrorList) {
	// we allow the set empty namespaces on one tool project, so validate nothing
	return nil
}
