package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateProjectManageBindingUpdate validates an update over the ProjectManagementBinding
func ValidateProjectManageBindingUpdate(new, old *devops.ProjectManagementBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateProjectManageBindingSpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateProjectManageBindingSpec spec data from ProjectManagementBindingSpec
func ValidateProjectManageBindingSpecUpdate(newSpec, oldSpec *devops.ProjectManagementBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateProjectManageBindingSpec(newSpec, fldPath)...)
	return
}

// ValidateProjectManageBinding validates a ProjectManagementBinding config
func ValidateProjectManageBinding(new *devops.ProjectManagementBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateProjectManageBindingSpec(&new.Spec, specField)
	return
}

// ValidateProjectManageBindingSpec validates spec
func ValidateProjectManageBindingSpec(spec *devops.ProjectManagementBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	errs = append(errs, ValidateLocalObjectReference(devops.LabelProjectManagement, &spec.ProjectManagement, fldPath.Child(devops.LabelProjectManagement))...)
	errs = append(errs, ValidateSecretKeySetRef(spec.Secret, fldPath.Child("secret"))...)
	errs = append(errs, ValidateProjectInfos(spec.ProjectManagementProjectInfos, fldPath.Child("projectmanagementprojectinfos"))...)
	return
}

func ValidateProjectInfos(ProjectInfos []devops.ProjectManagementProjectInfo, fldPath *field.Path) (errs field.ErrorList) {
	if ProjectInfos == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			ProjectInfos,
			"please provide a ProjectManagementProjectInfo configuration",
		))
	}
	return
}
