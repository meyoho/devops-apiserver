package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateTestToolBindingUpdate validates an update over the TestToolBinding
func ValidateTestToolBindingUpdate(new, old *devops.TestToolBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateTestToolBindingSpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateTestToolBindingSpec spec data from TestToolBindingSpec
func ValidateTestToolBindingSpecUpdate(newSpec, oldSpec *devops.TestToolBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateTestToolBindingSpec(newSpec, fldPath)...)
	return
}

// ValidateTestToolBinding validates a TestToolBinding config
func ValidateTestToolBinding(new *devops.TestToolBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateTestToolBindingSpec(&new.Spec, specField)
	return
}

// ValidateTestToolBindingSpec validates spec
func ValidateTestToolBindingSpec(spec *devops.TestToolBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	errs = append(errs, ValidateLocalObjectReference(devops.LabelTestTool, &spec.TestTool, fldPath.Child(devops.LabelTestTool))...)
	return
}
