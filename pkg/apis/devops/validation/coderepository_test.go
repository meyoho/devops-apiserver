package validation_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidateCodeRepository(t *testing.T) {

	type Table struct {
		name     string
		input    *devops.CodeRepository
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	invalidInput, invalidFullErrs := getInvalidCodeRepository()
	validInput, validtErrs := getValidCodeRepository()

	table := []Table{
		{
			name:     "create: invalid CodeRepoService",
			input:    invalidInput,
			expected: invalidFullErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeRepository(obj.(*devops.CodeRepository))
			},
		},
		{
			name:     "create: valid CodeRepoService without valid type",
			input:    validInput,
			expected: validtErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeRepository(obj.(*devops.CodeRepository))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func getValidCodeRepository() (*devops.CodeRepository, field.ErrorList) {
	return &devops.CodeRepository{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.CodeRepositorySpec{
			CodeRepoBinding: devops.LocalObjectReference{
				Name: "github-binding-1",
			},
			Repository: devops.OriginCodeRepository{
				CodeRepoServiceType: devops.CodeRepoServiceTypeGithub,
				Name:                "repo1",
			},
		},
	}, field.ErrorList{}
}

func getInvalidCodeRepository() (repo *devops.CodeRepository, errs field.ErrorList) {
	repo = &devops.CodeRepository{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.CodeRepositorySpec{
			Repository: devops.OriginCodeRepository{
				CodeRepoServiceType: "xxx",
			},
		},
	}

	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("codeRepoBinding").Child("name"),
			repo.Spec.CodeRepoBinding.Name,
			"please provide a CodeRepoBinding name",
		),
		field.NotSupported(
			field.NewPath("spec").Child("repository").Child("codeRepoServiceType"),
			repo.Spec.Repository.CodeRepoServiceType,
			validation.GetValidCodeRepoServiceStringTypes(),
		),
	}

	return
}
