package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// Validatear validates an update over the ar
func ValidateArtifactRegistryUpdate(new *devops.ArtifactRegistry, old *devops.ArtifactRegistry) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateArtifactRegistrySpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateProjectManageSpec spec data from ProjectManagementSpec
func ValidateArtifactRegistrySpecUpdate(newSpec, oldSpec *devops.ArtifactRegistrySpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateArtifactRegistrySpec(newSpec, fldPath)...)
	return
}

// ValidateProjectManage validates a ProjectManagement config
func ValidateArtifactRegistry(new *devops.ArtifactRegistry) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateArtifactRegistrySpec(&new.Spec, specField)
	return
}

// ValidateProjectManageSpec spec data from ProjectManagementSpec
func ValidateArtifactRegistrySpec(spec *devops.ArtifactRegistrySpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	// http

	//errs = append(errs, ValidateFormat(&spec.Format, fldPath.Child("format"))...)
	errs = append(errs, ValidateType(&spec.Type, fldPath.Child("type"))...)
	//errs = append(errs, ValidateName(spec.DisplayName, fldPath.Child("displayname"))...)
	//errs = append(errs, ValidateManager(&spec.Manager, fldPath.Child("manager"))...)
	return
}

func ValidateFormat(format *devops.ArtifactRegistryFormat, fldPath *field.Path) (errs field.ErrorList) {
	if format == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			format,
			"please provide a format configuration",
		))
	} else if *format == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("format"),
			format,
			"please provide a format",
		))
	}
	return
}

func ValidateType(Type *string, fldPath *field.Path) (errs field.ErrorList) {
	if *Type == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("Type"),
			Type,
			"please provide a Type",
		))
	}
	return
}

func ValidateName(name string, fldPath *field.Path) (errs field.ErrorList) {
	if name == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("name"),
			name,
			"please provide a name",
		))
	}
	return
}

func ValidateManager(manager *devops.ArtifactRegistryManager, fldPath *field.Path) (errs field.ErrorList) {
	if manager == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			manager,
			"please provide a manager configuration",
		))
	}
	return
}
