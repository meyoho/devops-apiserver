package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// Validatear validates an update over the ar
func ValidateArtifactRegistryManagerUpdate(new *devops.ArtifactRegistryManager, old *devops.ArtifactRegistryManager) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateArtifactRegistryManagerSpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateProjectManageSpec spec data from ProjectManagementSpec
func ValidateArtifactRegistryManagerSpecUpdate(newSpec, oldSpec *devops.ArtifactRegistryManagerSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateArtifactRegistryManagerSpec(newSpec, fldPath)...)
	return
}

// ValidateProjectManage validates a ProjectManagement config
func ValidateArtifactRegistryManager(new *devops.ArtifactRegistryManager) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateArtifactRegistryManagerSpec(&new.Spec, specField)
	return
}

// ValidateProjectManageSpec spec data from ProjectManagementSpec
func ValidateArtifactRegistryManagerSpec(spec *devops.ArtifactRegistryManagerSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	// http
	errs = append(errs, ValidateARMType(&spec.Type, fldPath.Child("type"))...)
	errs = append(errs, ValidateARMHttp(&spec.HTTP, fldPath.Child("http"))...)

	return
}

func ValidateARMHttp(hp *devops.HostPort, fldPath *field.Path) (errs field.ErrorList) {
	if hp == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			hp,
			"please provide a hp configuration",
		))
	}
	return
}

func ValidateARMType(Type *devops.ArtifactRegistryManagerType, fldPath *field.Path) (errs field.ErrorList) {
	if Type == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			Type,
			"please provide a Type configuration",
		))
	} else if *Type == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("Type"),
			Type,
			"please provide a Type",
		))
	}
	return
}
