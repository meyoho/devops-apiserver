package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// ValidateImageRepository validates a ImageRepository config
func ValidateImageRepository(imageRepository *devops.ImageRepository) (errs field.ErrorList) {
	specField := field.NewPath("spec")
	// spec
	errs = ValidateImageRepositorySpec(&imageRepository.Spec, specField)
	return
}

// ValidateImageRepositorySpec spec data from ImageRepositorySpec
func ValidateImageRepositorySpec(spec *devops.ImageRepositorySpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// imageRegistry
	errs = append(errs, validateImageRegistry(&spec.ImageRegistry, fldPath.Child(""))...)
	// imageRegistryBinding
	errs = append(errs, validateImageRegistryBinding(&spec.ImageRegistryBinding, fldPath.Child("imageRegistryBinding"))...)
	// image
	errs = append(errs, validateImage(spec.Image, fldPath.Child("image"))...)
	return
}

func validateImageRegistry(imageRegistry *devops.LocalObjectReference, fldPath *field.Path) (errs field.ErrorList) {
	if imageRegistry == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			imageRegistry,
			"please provide a ImageRegistry configuration",
		))
	} else if imageRegistry.Name == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("name"),
			imageRegistry.Name,
			"please provide a registry instance name",
		))
	}
	return
}

func validateImageRegistryBinding(imageRegistryBinding *devops.LocalObjectReference, fldPath *field.Path) (errs field.ErrorList) {
	if imageRegistryBinding == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			imageRegistryBinding,
			"please provide a ImageRegistryBinding configuration",
		))
	} else if imageRegistryBinding.Name == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("name"),
			imageRegistryBinding.Name,
			"please provide a ImageRegistryBinding instance name",
		))
	}
	return
}

func validateImage(image string, fldPath *field.Path) (errs field.ErrorList) {
	if image == "" {
		errs = append(errs, field.Invalid(
			fldPath,
			image,
			"please provide a image path",
		))
	}
	return
}
