package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// Validatear validates an update over the ar
func ValidateArtifactRegistryBindingUpdate(new *devops.ArtifactRegistryBinding, old *devops.ArtifactRegistryBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateArtifactRegistryBindingSpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateProjectManageSpec spec data from ProjectManagementSpec
func ValidateArtifactRegistryBindingSpecUpdate(newSpec, oldSpec *devops.ArtifactRegistryBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateArtifactRegistryBindingSpec(newSpec, fldPath)...)
	return
}

// ValidateProjectManage validates a ProjectManagement config
func ValidateArtifactRegistryBinding(new *devops.ArtifactRegistryBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateArtifactRegistryBindingSpec(&new.Spec, specField)
	return
}

// ValidateProjectManageSpec spec data from ProjectManagementSpec
func ValidateArtifactRegistryBindingSpec(spec *devops.ArtifactRegistryBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	// http
	errs = append(errs, ValidateAR(&spec.ArtifactRegistry, fldPath.Child("url"))...)

	return
}

func ValidateAR(format *devops.LocalObjectReference, fldPath *field.Path) (errs field.ErrorList) {
	if format.Name == "" {
		errs = append(errs, field.Invalid(
			fldPath,
			format,
			"please provide a format configuration",
		))
	}
	return
}
