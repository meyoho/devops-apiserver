package validation

import (
	"fmt"
	"net/http"
	"reflect"
	"strings"

	glog "k8s.io/klog"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

const (
	fieldNameParameters = "parameters"
	fieldNameStrategy   = "strategy"
	fieldNameSource     = "source"

	msgCannotUpdateWhenCreatedError = "pipeline can not be update when it was created"
	msgIgnoreType                   = "please provide type"
	msgIgnoreName                   = "please provide name"
)

// ValidatePipelineConfig validates a pipeline config
func ValidatePipelineConfig(pipelineConfig *devops.PipelineConfig) (errs field.ErrorList) {
	specField := field.NewPath("spec")
	// spec
	errs = ValidatePipelineConfigSpec(&pipelineConfig.Spec, specField)
	return
}

// ValidatePipelineConfigUpdate validates an update over the PipelineConfig
func ValidatePipelineConfigUpdate(new *devops.PipelineConfig, old *devops.PipelineConfig) (errs field.ErrorList) {
	glog.V(7).Infof("new: %v; old: %v", new, old)
	errs = field.ErrorList{}
	return
}

// ValidatePipelineConfigSpec spec data from PipelineConfigSpec
func ValidatePipelineConfigSpec(spec *devops.PipelineConfigSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// jenkinsBinding
	errs = append(errs, validateJenkinsBinding(&spec.JenkinsBinding, fldPath.Child("jenkinsBinding"))...)
	// runPolicy
	errs = append(errs, validateRunPolicy(spec.RunPolicy, fldPath.Child("runPolicy"))...)
	// runLimits
	errs = append(errs, validateRunLimits(&spec.RunLimits, fldPath.Child("runLimits"))...)
	// parameters
	errs = append(errs, validatePipelineParameters(spec.Parameters, fldPath.Child(fieldNameParameters))...)
	// triggers
	errs = append(errs, validatePipelineTriggers(spec.Triggers, fldPath.Child("triggers"))...)
	// strategy
	errs = append(errs, validatePipelineConfigStrategy(spec, fldPath.Child(fieldNameStrategy))...)
	// hooks
	errs = append(errs, validatePipelineHooks(spec.Hooks, fldPath.Child("hooks"))...)
	// source
	errs = append(errs, ValidatePipelineSource(&spec.Source, fldPath.Child(fieldNameSource))...)
	//template
	errs = append(errs, validateSpecTemplateSource(spec.Template.PipelineTemplateSource, fldPath.Child("template"))...)
	return
}

func validateSpecTemplateSource(templateSource devops.PipelineTemplateSource, fldPath *field.Path) (errs field.ErrorList) {
	// created by new api, we should validate templateRef
	if (templateSource.PipelineTemplateRef.Name == "" && templateSource.PipelineTemplateRef.Kind != "") ||
		(templateSource.PipelineTemplateRef.Name != "" && templateSource.PipelineTemplateRef.Kind == "") {
		errs = append(errs, field.Invalid(fldPath.Child("pipelineTemplateRef"), templateSource.PipelineTemplateRef, "please provide the template kind or template name"))
		return
	}

	if templateSource.PipelineTemplate == nil {
		return
	}

	// it is created by graph template
	errs = ValidatePipelineTemplateSpec(&templateSource.PipelineTemplate.Spec, fldPath.Child("pipelineTemplate.spec"))
	return
}

// ValidatePipeline validates a pipeline data
func ValidatePipeline(pipeline *devops.Pipeline) (errs field.ErrorList) {
	specField := field.NewPath("spec")
	// spec
	errs = ValidatePipelineSpec(&pipeline.Spec, specField)
	// status
	errs = append(errs, ValidatePipelineStatus(&pipeline.Status, field.NewPath("status"))...)

	return
}

// ValidatePipelineStatus validate a pipeline status
func ValidatePipelineStatus(status *devops.PipelineStatus, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// phase
	errs = append(errs, validatePipelinePhase(status.Phase, status.Phase, fldPath.Child("phase"))...)

	return
}

// ValidatePipelineUpdate validates a pipeline data for an update
func ValidatePipelineUpdate(pipeline *devops.Pipeline, old *devops.Pipeline) (errs field.ErrorList) {
	if old == nil {
		old = pipeline
	}

	errs = ValidatePipelineSpecUpdate(&pipeline.Spec, &old.Spec, field.NewPath("spec"))

	errs = append(errs, ValidatePipelineStatusUpdate(&pipeline.Status, &old.Status, field.NewPath("status"))...)

	return
}

// ValidatePipelineSpecUpdate validate a pipeline spec for an update
func ValidatePipelineSpecUpdate(spec *devops.PipelineSpec, old *devops.PipelineSpec, fldPath *field.Path) (errs field.ErrorList) {
	if len(spec.Hooks) != len(old.Hooks) && !reflect.DeepEqual(spec.Hooks, old.Hooks) {
		errs = append(errs, field.Invalid(
			fldPath,
			"Hooks",
			msgCannotUpdateWhenCreatedError,
		))
	}

	if !reflect.DeepEqual(spec.JenkinsBinding, old.JenkinsBinding) {
		errs = append(errs, field.Invalid(
			fldPath,
			"JenkinsBinding",
			msgCannotUpdateWhenCreatedError,
		))
	}

	if len(spec.Parameters) != len(old.Parameters) && !reflect.DeepEqual(spec.Parameters, old.Parameters) {
		errs = append(errs, field.Invalid(
			fldPath,
			fieldNameParameters,
			msgCannotUpdateWhenCreatedError,
		))
	}

	if !reflect.DeepEqual(spec.PipelineConfig, old.PipelineConfig) {
		errs = append(errs, field.Invalid(
			fldPath,
			"PipelineConfig",
			msgCannotUpdateWhenCreatedError,
		))
	}

	if !reflect.DeepEqual(spec.RunPolicy, old.RunPolicy) {
		errs = append(errs, field.Invalid(
			fldPath,
			"RunPolicy",
			msgCannotUpdateWhenCreatedError,
		))
	}

	if !reflect.DeepEqual(spec.Source, old.Source) {
		errs = append(errs, field.Invalid(
			fldPath.Child(fieldNameSource),
			spec.Source,
			fmt.Sprintf("pipeline can not be update when it was created, old source: %v", old.Source),
		))
	}

	if !reflect.DeepEqual(spec.Strategy, old.Strategy) {
		errs = append(errs, field.Invalid(
			fldPath.Child(fieldNameStrategy),
			spec.Strategy,
			fmt.Sprintf("pipeline can not be update when it was created, old strategy: %v", old.Strategy),
		))
	}

	if len(spec.Triggers) != len(old.Triggers) && !reflect.DeepEqual(spec.Triggers, old.Triggers) {
		errs = append(errs, field.Invalid(
			fldPath,
			"Triggers",
			msgCannotUpdateWhenCreatedError,
		))
	}

	return
}

// ValidatePipelineStatusUpdate validates a pipeline status for an update
func ValidatePipelineStatusUpdate(status *devops.PipelineStatus, oldStatus *devops.PipelineStatus, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// phase
	errs = append(errs, validatePipelinePhase(status.Phase, oldStatus.Phase, fldPath.Child("phase"))...)

	return
}

func validatePipelinePhase(phase devops.PipelinePhase, oldPhase devops.PipelinePhase, fldPath *field.Path) (errs field.ErrorList) {
	if phase != oldPhase && oldPhase.IsFinalPhase() {
		errs = append(errs, field.Invalid(
			fldPath,
			phase,
			"pipeline already finished, can not update",
		))
	}

	if !phase.IsValid() {
		errs = append(errs, field.Invalid(
			fldPath,
			phase,
			"pipeline phase is invalid",
		))
	}

	return
}

// ValidatePipelineSpec validate all spec for pipeline
func ValidatePipelineSpec(spec *devops.PipelineSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// pipelineConfig
	errs = append(errs, validatePipelineConfigRef(&spec.PipelineConfig, fldPath.Child("pipelineConfig"))...)
	// jenkinsBinding
	errs = append(errs, validateJenkinsBinding(&spec.JenkinsBinding, fldPath.Child("jenkinsBinding"))...)
	// runPolicy
	errs = append(errs, validateRunPolicy(spec.RunPolicy, fldPath.Child("runPolicy"))...)
	// parameters
	errs = append(errs, validatePipelineParameters(spec.Parameters, fldPath.Child(fieldNameParameters))...)
	// triggers
	errs = append(errs, validatePipelineTriggers(spec.Triggers, fldPath.Child("triggers"))...)
	// strategy
	errs = append(errs, validatePipelineStrategy(spec.Strategy, fldPath.Child(fieldNameStrategy))...)
	// hooks
	errs = append(errs, validatePipelineHooks(spec.Hooks, fldPath.Child("hooks"))...)
	// source
	errs = append(errs, ValidatePipelineSource(&spec.Source, fldPath.Child(fieldNameSource))...)
	return
}

func validatePipelineConfigRef(pipelineConfig *devops.LocalObjectReference, fldPath *field.Path) (errs field.ErrorList) {
	if pipelineConfig == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			pipelineConfig,
			"please provide a PipelineConfig reference",
		))

	} else if pipelineConfig.Name == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("name"),
			pipelineConfig.Name,
			"please provide a PipelineConfig instance name",
		))
	}
	return
}

func validateJenkinsBinding(jenkinsBinding *devops.LocalObjectReference, fldPath *field.Path) (errs field.ErrorList) {
	if jenkinsBinding == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			jenkinsBinding,
			"please provide a JenkinsBinding configuration",
		))

	} else if jenkinsBinding.Name == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("name"),
			jenkinsBinding.Name,
			"please provide a JenkinsBinding instance name",
		))
	}
	return
}

func validateRunPolicy(runPolicy devops.PipelineRunPolicy, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	switch runPolicy {
	case devops.PipelinePolicySerial, devops.PipelinePolicyParallel:
		// pass
	default:
		errs = append(errs, field.Invalid(
			fldPath,
			runPolicy,
			"please provide a valid runPolicy: Serial or Parallel",
		))
	}

	return
}

func validateRunLimits(runPolicy *devops.PipelineRunLimits, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if runPolicy == nil {
		return
	}
	if runPolicy.SuccessCount < 0 {
		errs = append(errs, field.Invalid(
			fldPath.Child("successCount"),
			runPolicy.SuccessCount,
			"please provide a valid successCount, must be a positive number, or 0 for no limits",
		))
	}
	if runPolicy.FailureCount < 0 {
		errs = append(errs, field.Invalid(
			fldPath.Child("failureCount"),
			runPolicy.FailureCount,
			"please provide a valid failureCount, must be a positive number, or 0 for no limits",
		))
	}
	return
}

func validatePipelineParameters(parameters []devops.PipelineParameter, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if len(parameters) == 0 {
		return
	}

	nameMap := make(map[string]string)

	for i, p := range parameters {
		// Name
		if p.Name == "" {
			errs = append(errs, field.Invalid(
				fldPath.Index(i).Child("name"),
				p.Name,
				"please provide a valid parameter name",
			))
		}

		// unique check
		_, exists := nameMap[p.Name]
		if exists {
			errs = append(errs, field.Invalid(
				fldPath.Index(i).Child("name"),
				p.Name,
				"parameter must be unique",
			))
		} else {
			nameMap[p.Name] = ""
		}

		// Type
		switch p.Type {
		case devops.PipelineParameterTypeBoolean, devops.PipelineParameterTypeString,
			devops.StringParameterDefinition, devops.BooleanParameterDefinition:
			// pass
		default:
			errs = append(errs, field.Invalid(
				fldPath.Index(i).Child("type"),
				p.Type,
				"please provide a valid parameter type: string or boolean",
			))
		}
	}
	return
}

func validatePipelineTriggers(triggers []devops.PipelineTrigger, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if len(triggers) == 0 {
		return
	}
	for i, t := range triggers {
		// type
		switch t.Type {
		case devops.PipelineTriggerTypeCodeChange:
			if t.CodeChange == nil {
				errs = append(errs, field.Invalid(
					fldPath.Index(i).Child("codeChange"),
					t.CodeChange,
					"please provide a codeChange configuration",
				))
			}
		case devops.PipelineTriggerTypeCron:
			if t.Cron == nil {
				errs = append(errs, field.Invalid(
					fldPath.Index(i).Child("cron"),
					t.Cron,
					"please provide a cron configuration",
				))
			} else if t.Cron.Enabled && t.Cron.Rule == "" && t.Cron.Schedule == nil {
				errs = append(errs, field.Invalid(
					fldPath.Index(i).Child("cron").Child("rule"),
					t.Cron.Rule,
					"please provide a cron schedule rule or schedule for the cron trigger",
				))
			} else if t.Cron.Schedule != nil {
				schedule := t.Cron.Schedule
				if len(schedule.Times) == 0 && len(schedule.Weeks) == 0 {
					errs = append(errs, field.Invalid(
						fldPath.Index(i).Child("cron").Child("schedule"),
						t.Cron.Rule,
						"please provide a cron schedule rule or schedule for the cron trigger",
					))
				} else {
					for j, week := range schedule.Weeks {
						if !week.IsValid() {
							errs = append(errs, field.Invalid(
								fldPath.Index(i).Child("cron").Child("schedule").Child("weeks").Index(j),
								week,
								"please provide a valid week expression",
							))
						}
					}
				}
			}
			// TODO: Add cron validation logic

		default:
			errs = append(errs, field.Invalid(
				fldPath.Index(i).Child("type"),
				t.Type,
				"please provide a valid trigger type: cron or codeChange",
			))
		}
	}
	return
}

func validatePipelineStrategy(strategy devops.PipelineStrategy, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if strategy.Jenkins.Jenkinsfile == "" &&
		strategy.Jenkins.JenkinsfilePath == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("jenkins"),
			strategy.Jenkins.Jenkinsfile,
			"please provide the pipeline strategy: jenkinsfile or jenkinsfilePath",
		))
	}

	return
}

func validatePipelineConfigStrategy(spec *devops.PipelineConfigSpec, fldPath *field.Path) (errs field.ErrorList) {
	strategy := &spec.Strategy
	errs = field.ErrorList{}

	if spec.Template.PipelineTemplateRef.Name == "" && spec.Template.PipelineTemplate == nil {
		// it must be created by jenkinsfile

		if strategy.Jenkins.Jenkinsfile == "" && strategy.Jenkins.JenkinsfilePath == "" {
			errs = append(errs, field.Invalid(fldPath.Child("jenkins"), strategy.Jenkins.Jenkinsfile, "please provide the pipeline strategy: jenkinsfile or jenkinsfilePath"))
			return errs
		}
		// created by jenkinsfile, and everything is valid
		return
	}

	// created by new api, we should validate templateRef
	if (spec.Template.PipelineTemplateRef.Name == "" && spec.Template.PipelineTemplateRef.Kind != "") ||
		(spec.Template.PipelineTemplateRef.Name != "" && spec.Template.PipelineTemplateRef.Kind == "") {
		errs = append(errs, field.Invalid(fldPath.Child("template.pipelineTemplateRef"), spec.Template.PipelineTemplateRef, "please provide the template kind or template name"))
		return
	}

	return
}

// ValidatePipelineConfigTemplate validate pipelineconfigtemplate
func ValidatePipelineConfigTemplate(template *devops.PipelineConfigTemplate, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if template == nil {
		return
	}

	errs = append(errs, validatePipelineConfigTemplateSpec(&template.Spec, fldPath.Child("spec"))...)

	return
}

func validatePipelineConfigTemplateSpec(spec *devops.PipelineConfigTemplateSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// engine
	errs = append(errs, ValidatePipelineTemplateEngine(spec.Engine, fldPath.Child("engine"))...)
	// agent
	errs = append(errs, ValidateJenkinsAgent(spec.Agent, fldPath.Child("agent"))...)
	// stages
	errs = append(errs, ValidatePipelineStageInstances(spec.Stages, fldPath.Child("stages"))...)
	// parameters
	errs = append(errs, ValidatePipelineParameter(spec.Parameters, fldPath.Child(fieldNameParameters))...)
	// arguments
	errs = append(errs, ValidatePipelineTemplateArgumentGroup(spec.Arguments, fldPath.Child("arguments"))...)
	// dependencies
	errs = append(errs, ValidateDependencies(spec.Dependencies, fldPath.Child("dependencies"))...)
	return
}

// ValidatePipelineTemplateEngine validate pipeineTemplateEngine
func ValidatePipelineTemplateEngine(engine devops.PipelineTemplateEngine, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	if !engine.IsEmpty() && !engine.IsValid() {
		errs = append(errs, field.Invalid(fldPath, engine, fmt.Sprintf("support engine is %v", devops.PipelineTemplateEngineGraph)))
	}

	return
}

// ValidatePipelineStageInstances validate PipelineStageInstance
func ValidatePipelineStageInstances(stages []devops.PipelineStageInstance, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if len(stages) == 0 {
		return
	}

	for i, stage := range stages {
		errs = append(errs, ValidatePipelineStageInstance(stage, fldPath.Index(i))...)
	}

	return
}

// ValidatePipelineStageInstance validate PipelineStageInstance
func ValidatePipelineStageInstance(stage devops.PipelineStageInstance, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	if strings.TrimSpace(stage.Name) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("name"), stage.Name, "please provide name for stage"))
	}

	// We will not use PipelineStageInstance any more, so no need to validate it
	// Refactor doc:
	// for i, task := range stage.Tasks {
	// 	errs = append(errs, validatePipelineTemplateTaskInstance(task, fldPath.Index(i).Child("task"))...)
	// }

	return
}

func validatePipelineTemplateTaskInstance(task devops.PipelineTemplateTaskInstance, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	errs = append(errs, validatePipelineTemplateTaskInstanceSpec(task.Spec, fldPath.Child("spec"))...)
	return
}

func validatePipelineTemplateTaskInstanceSpec(spec devops.PipelineTemplateTaskInstanceSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// agent
	errs = append(errs, ValidateJenkinsAgent(spec.Agent, fldPath.Child("agent"))...)
	// engine
	if !spec.Engine.IsEmpty() && !spec.Engine.IsValid() {
		errs = append(errs, field.Invalid(fldPath.Child("engine"), spec.Engine, "please provide valid engine"))
	}
	// type
	if strings.TrimSpace(spec.Type) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("type"), spec.Type, msgIgnoreType))
	}
	// body
	if strings.TrimSpace(spec.Body) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("body"), spec.Body, "please provide body"))
	}
	// options
	errs = append(errs, ValidatePipelineTaskOption(spec.Options, fldPath.Child("options"))...)
	// approve
	errs = append(errs, ValidatePipelineTaskApprove(spec.Approve, fldPath.Child("approve"))...)
	// environments
	for i, env := range spec.Environments {
		errs = append(errs, ValidatePipelineEnvironment(env, fldPath.Child("environments").Index(i))...)
	}
	// exports
	for i, export := range spec.Exports {
		errs = append(errs, ValiateGlobalParameter(export, fldPath.Child("exports").Index(i))...)
	}
	// arguments
	for i, argument := range spec.Arguments {
		errs = append(errs, ValidatePipelineTemplateArgument(argument, fldPath.Child("arguments").Index(i))...)
	}
	return
}

// ValidatePipelineTaskOption check PipelineTaskOption
func ValidatePipelineTaskOption(options *devops.PipelineTaskOption, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if options == nil {
		return
	}

	if options.Timeout <= 0 {
		errs = append(errs, field.Invalid(fldPath.Child("timeout"), options.Timeout, "timeout value should be positive"))
	}
	return
}

// ValidatePipelineTaskApprove check PipelineTaskApprove
func ValidatePipelineTaskApprove(approve *devops.PipelineTaskApprove, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if approve == nil {
		return
	}

	if strings.TrimSpace(approve.Message) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("message"), approve.Message, "please provide message"))
	}
	if approve.Timeout <= 0 {
		errs = append(errs, field.Invalid(fldPath.Child("timeout"), approve.Timeout, "timeout value should be positive"))
	}
	return
}

// ValidatePipelineEnvironment check PipelineEnvironment
func ValidatePipelineEnvironment(env devops.PipelineEnvironment, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	if strings.TrimSpace(env.Name) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("name"), env.Name, msgIgnoreName))
	}
	if strings.TrimSpace(env.Value) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("value"), env.Value, "please provide value"))
	}
	return
}

// ValiateGlobalParameter check GlobalParameter
func ValiateGlobalParameter(export devops.GlobalParameter, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	if strings.TrimSpace(export.Name) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("name"), export.Name, msgIgnoreName))
	}
	// description
	errs = append(errs, ValidateI18nName(export.Description, fldPath.Child("description"))...)
	return
}

// ValidateI18nName check I18nName
func ValidateI18nName(name *devops.I18nName, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if name == nil {
		return
	}

	if strings.TrimSpace(name.Zh) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("zh-CN"), name.Zh, "provide Chinese name"))
	}
	if strings.TrimSpace(name.En) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("en"), name.En, "provide English name"))
	}
	return
}

// ValidatePipelineTemplateArgument checkout PipelineTemplateArgument
func ValidatePipelineTemplateArgument(argument devops.PipelineTemplateArgument, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// name
	if strings.TrimSpace(argument.Name) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("name"), argument.Name, msgIgnoreName))
	}
	// schema
	errs = append(errs, ValidatePipelineTaskArgumentSchema(argument.Schema, fldPath.Child("schema"))...)
	// display
	errs = append(errs, ValidatePipelineTaskArgumentDisplay(argument.Display, fldPath.Child("display"))...)
	// validation
	errs = append(errs, ValidatePipelineTaskArgumentValidation(argument.Validation, fldPath.Child("validation"))...)
	// relation
	for i, relation := range argument.Relation {
		errs = append(errs, ValidatePipelineTaskArgumentAction(relation, fldPath.Child("relation").Index(i))...)
	}
	return
}

// ValidatePipelineTaskArgumentSchema check PipelineTaskArgumentSchema
func ValidatePipelineTaskArgumentSchema(schema devops.PipelineTaskArgumentSchema, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if strings.TrimSpace(schema.Type) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("type"), schema.Type, msgIgnoreType))
	}
	return
}

// ValidatePipelineTaskArgumentDisplay check PipelineTaskArgumentDisplay
func ValidatePipelineTaskArgumentDisplay(display devops.PipelineTaskArgumentDisplay, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// type
	if strings.TrimSpace(display.Type) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("type"), display.Type, msgIgnoreType))
	}
	// name
	errs = append(errs, ValidateI18nName(&display.Name, fldPath.Child("name"))...)
	// description
	errs = append(errs, ValidateI18nName(&display.Description, fldPath.Child("description"))...)
	return
}

// ValidatePipelineTaskArgumentValidation check PipeineTaskArgumentValidation
func ValidatePipelineTaskArgumentValidation(validation *devops.PipelineTaskArgumentValidation, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if validation == nil {
		return
	}

	if strings.TrimSpace(validation.Pattern) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("pattern"), validation.Pattern, "please provide pattern"))
	}
	return
}

// ValidatePipelineTaskArgumentAction check PipelineTaskArgumentAction
func ValidatePipelineTaskArgumentAction(relation devops.PipelineTaskArgumentAction, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	if strings.TrimSpace(relation.Action) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("action"), relation.Action, "please provide action"))
	}
	errs = append(errs, ValidatePipelineTaskArgumentWhen(relation.When, fldPath.Child("when"))...)
	return
}

// ValidatePipelineTaskArgumentWhen check PipelineTaskArgumentWhen
func ValidatePipelineTaskArgumentWhen(when devops.PipelineTaskArgumentWhen, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	if strings.TrimSpace(when.Name) == "" {
		errs = append(errs, field.Invalid(fldPath.Child("name"), when.Name, msgIgnoreName))
	}
	return
}

func validatePipelineHooks(hooks []devops.PipelineHook, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if len(hooks) == 0 {
		return
	}
	for i, h := range hooks {
		// type
		switch h.Type {
		case devops.PipelineHookTypeHTTPRequest:
			errs = append(errs, validateHookHTTPRequest(h.HTTPRequest, fldPath.Index(i).Child("httpRequest"))...)

		default:
			errs = append(errs, field.Invalid(
				fldPath.Index(i).Child("type"),
				h.Type,
				"please provide a valid hook type: httpRequest",
			))
		}
		// events
		if len(h.Events) == 0 {
			errs = append(errs, field.Invalid(
				fldPath.Index(i).Child("events"),
				nil,
				"please provide at least one valid event: PipelineConfigCreated, PipelineConfigUpdated, PipelineConfigDeleted, PipelineStarted, PipelineStopped",
			))
			continue
		}
		for j, e := range h.Events {
			switch e {
			case devops.PipelineEventConfigCreated,
				devops.PipelineEventConfigDeleted,
				devops.PipelineEventConfigUpdated,
				devops.PipelineEventPipelineStarted,
				devops.PipelineEventPipelineStopped:
			default:
				errs = append(errs, field.Invalid(
					fldPath.Index(i).Child("events").Index(j),
					e,
					"please provide at least one valid event: PipelineConfigCreated, PipelineConfigUpdated, PipelineConfigDeleted, PipelineStarted, PipelineStopped",
				))
			}
		}
	}
	return
}

func validateHookHTTPRequest(httpRequest *devops.PipelineHookHTTPRequest, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if httpRequest == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			httpRequest,
			"please provide a httpRequest configuration",
		))
		return
	}
	// url
	if !urlregexp.MatchString(httpRequest.URI) {
		errs = append(errs, field.Invalid(
			fldPath.Child("uri"),
			httpRequest.URI,
			"please provide a valid http uri",
		))
	}
	// http method
	switch httpRequest.Method {
	case http.MethodPost,
		http.MethodGet,
		http.MethodPut,
		http.MethodDelete,
		http.MethodHead:
	default:
		errs = append(errs, field.Invalid(
			fldPath.Child("method"),
			httpRequest.Method,
			"please provide a valid http method: GET, POST, PUT, DELETE, HEAD",
		))
	}

	return
}

// ValidatePipelineSource validates pipline git source
func ValidatePipelineSource(source *devops.PipelineSource, fldPath *field.Path) (errs field.ErrorList) {
	// if source.Git == nil && source.CodeRepository == nil {
	// 	errs = append(errs, field.Invalid(
	// 		fldPath,
	// 		source,
	// 		"please provide git or codeRepository",
	// 	))
	// 	return
	// }

	if source.Git != nil {
		if source.Git.URI == "" {
			errs = append(errs, field.Invalid(
				fldPath.Child("git").Child("uri"),
				source.Git.URI,
				"please provide a git uri",
			))
		}
		if source.Git.Ref == "" {
			errs = append(errs, field.Invalid(
				fldPath.Child("git").Child("ref"),
				source.Git.Ref,
				"please provide a git branch name or * for all",
			))
		}
	}

	return
}
