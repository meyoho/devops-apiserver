package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateProjectManageUpdate validates an update over the ProjectManagement
func ValidateProjectManageUpdate(new *devops.ProjectManagement, old *devops.ProjectManagement) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateProjectManageSpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateProjectManageSpec spec data from ProjectManagementSpec
func ValidateProjectManageSpecUpdate(newSpec, oldSpec *devops.ProjectManagementSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateProjectManageSpec(newSpec, fldPath)...)
	return
}

// ValidateProjectManage validates a ProjectManagement config
func ValidateProjectManage(new *devops.ProjectManagement) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateProjectManageSpec(&new.Spec, specField)
	return
}

// ValidateProjectManageSpec spec data from ProjectManagementSpec
func ValidateProjectManageSpec(spec *devops.ProjectManagementSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	// http
	errs = append(errs, ValidateHostPort(&spec.HTTP, fldPath.Child("http"))...)
	errs = append(errs, ValidataType(&spec.Type, fldPath.Child("type"))...)
	return
}

// ValidateHostPort validate HostPort
func ValidataType(Type *devops.ProjectManagementType, fldPath *field.Path) (errs field.ErrorList) {

	if Type == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			Type,
			"please provide a Type configuration",
		))
	} else if !(*Type == devops.ProjectManageTypeTaiga || *Type == devops.ProjectManageTypeJira) {
		errs = append(errs, field.Invalid(
			fldPath,
			Type,
			"please provide a right type",
		))
	}
	return
}
