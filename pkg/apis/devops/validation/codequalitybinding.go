package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// ValidateCodeQualityBinding validates a codeQualityBinding config
func ValidateCodeQualityBinding(codeQualityBinding *devops.CodeQualityBinding) (errs field.ErrorList) {
	specField := field.NewPath("spec")
	// spec
	errs = ValidateCodeQualityBindingSpec(&codeQualityBinding.Spec, specField)
	return
}

// ValidateCodeQualityBindingSpec validates a CodeQualityBinding spec
func ValidateCodeQualityBindingSpec(spec *devops.CodeQualityBindingSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// CodeQualityTool
	errs = append(errs, validateSpecCodeQualityTool(&spec.CodeQualityTool, fldPath.Child("codeQualityTool"))...)

	return
}

func validateSpecCodeQualityTool(codeQualityTool *devops.LocalObjectReference, fldPath *field.Path) (errs field.ErrorList) {
	if codeQualityTool == nil {
		errs = append(errs, field.Invalid(
			fldPath,
			codeQualityTool,
			"please provide a CodeQualityTool",
		))
	} else if codeQualityTool.Name == "" {
		errs = append(errs, field.Invalid(
			fldPath.Child("name"),
			codeQualityTool.Name,
			"please provide a CodeQualityTool name",
		))
	}
	return
}
