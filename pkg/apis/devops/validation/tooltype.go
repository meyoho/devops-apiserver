package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateToolTypeToolUpdate validates an update over the ToolTypeTool
func ValidateToolTypeUpdate(new, old *devops.ToolType) (errs field.ErrorList) {
	specFiled := field.NewPath("spec")

	// spec
	errs = ValidateToolTypeSpecUpdate(&new.Spec, &new.Spec, specFiled)
	return
}

// ValidateToolTypeToolSpecUpdate spec data from ToolTypeToolSpec
func ValidateToolTypeSpecUpdate(newSpec, oldSpec *devops.ToolTypeSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateToolTypeSpec(newSpec, fldPath)...)
	return
}

// ValidateToolTypeTool validates a ToolTypeTool config
func ValidateToolType(new *devops.ToolType) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateToolTypeSpec(&new.Spec, specField)
	return
}

// ValidateToolTypeToolSpec spec data from ToolTypeToolSpec
func ValidateToolTypeSpec(spec *devops.ToolTypeSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	return
}
