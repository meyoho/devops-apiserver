package validation_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidateCodeRepoService(t *testing.T) {

	type Table struct {
		name     string
		input    *devops.CodeRepoService
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	errorInputWithoutHost, fullErrorsWithoutHost := getInvalidCodeRepoServiceWithoutHost()
	errorInputWithoutValidType, fullErrorsWithoutValidType := getInvalidCodeRepoServiceWithoutValidType()
	errorInputWithNothing, fullErrorsWithNothing := getInvalidCodeRepoServiceWithNothing()

	correctInput, correctErrs := getValidCodeRepoService()

	table := []Table{
		{
			name:     "create: invalid CodeRepoService without host",
			input:    errorInputWithoutHost,
			expected: fullErrorsWithoutHost,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeRepoService(obj.(*devops.CodeRepoService))
			},
		},
		{
			name:     "create: invalid CodeRepoService without valid type",
			input:    errorInputWithoutValidType,
			expected: fullErrorsWithoutValidType,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeRepoService(obj.(*devops.CodeRepoService))
			},
		},
		{
			name:     "create: empty CodeRepoService",
			input:    errorInputWithNothing,
			expected: fullErrorsWithNothing,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeRepoService(obj.(*devops.CodeRepoService))
			},
		},
		{
			name:     "create: correct CodeRepoService",
			input:    correctInput,
			expected: correctErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidateCodeRepoService(obj.(*devops.CodeRepoService))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func getValidCodeRepoService() (*devops.CodeRepoService, field.ErrorList) {
	return &devops.CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.CodeRepoServiceSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "https://api.github.com",
				},
			},
			Type:   "Github",
			Public: true,
		},
	}, field.ErrorList{}
}

func getInvalidCodeRepoServiceWithoutHost() (pipe *devops.CodeRepoService, errs field.ErrorList) {
	pipe = &devops.CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.CodeRepoServiceSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{},
			},
			Type:   "Github",
			Public: true,
		},
	}

	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("http").Child("host"),
			"",
			"please provide a host",
		),
	}

	return
}

func getInvalidCodeRepoServiceWithoutValidType() (pipe *devops.CodeRepoService, errs field.ErrorList) {
	pipe = &devops.CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.CodeRepoServiceSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "https://api.github.com",
				},
			},
			Type:   "Github1",
			Public: true,
		},
	}

	errs = field.ErrorList{
		field.NotSupported(
			field.NewPath("spec").Child("type"),
			pipe.Spec.Type,
			validation.GetValidCodeRepoServiceStringTypes(),
		),
	}

	return
}

func getInvalidCodeRepoServiceWithNothing() (pipe *devops.CodeRepoService, errs field.ErrorList) {
	pipe = &devops.CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{},
		Spec:       devops.CodeRepoServiceSpec{},
	}

	errs = field.ErrorList{
		field.NotSupported(
			field.NewPath("spec").Child("type"),
			pipe.Spec.Type,
			validation.GetValidCodeRepoServiceStringTypes(),
		),
		field.Invalid(
			field.NewPath("spec").Child("http").Child("host"),
			pipe.Spec.HTTP.Host,
			"please provide a host",
		),
	}

	return
}

func TestValidateCodeRepoServiceSpecUpdate(t *testing.T) {
	type Table struct {
		name     string
		old      *devops.CodeRepoService
		new      *devops.CodeRepoService
		expected field.ErrorList
		method   func(obj1, obj2 runtime.Object) field.ErrorList
	}

	invalidOld, invalidNew, invalidErrs := getInvalidCodeRepoServiceUpdate()
	validOld, validNew, validErrs := getValidCodeRepoServiceUpdate()

	tests := []Table{
		{
			name:     "invalid update",
			old:      invalidOld,
			new:      invalidNew,
			expected: invalidErrs,
			method: func(new, old runtime.Object) field.ErrorList {
				return validation.ValidateCodeRepoServiceUpdate(new.(*devops.CodeRepoService), old.(*devops.CodeRepoService))
			},
		},
		{
			name:     "valid update",
			old:      validOld,
			new:      validNew,
			expected: validErrs,
			method: func(new, old runtime.Object) field.ErrorList {
				return validation.ValidateCodeRepoServiceUpdate(new.(*devops.CodeRepoService), old.(*devops.CodeRepoService))
			}},
	}
	for _, tst := range tests {
		t.Run(tst.name, func(t *testing.T) {
			res := tst.method(tst.new, tst.old)
			if len(res) != len(tst.expected) {
				t.Errorf(
					"expected error lists with different size: len(%v) != len(%v)\n %v != %v",
					len(tst.expected), len(res),
					tst.expected, res,
				)
			} else {
				for j, e := range tst.expected {
					if e.Error() != res[j].Error() {
						t.Errorf(
							"actual error %d is different than expected: %v != %v",
							j, e.Error(), res[j].Error(),
						)
					}
				}
			}
		})
	}
}

func getInvalidCodeRepoServiceUpdate() (old, new *devops.CodeRepoService, errs field.ErrorList) {
	old = &devops.CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.CodeRepoServiceSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "https://api.github.com",
				},
			},
			Type:   "Github",
			Public: true,
		},
	}

	new = &devops.CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.CodeRepoServiceSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "https://api.github.com111",
				},
			},
			Type:   "Github111",
			Public: true,
		},
	}

	errs = append(errs,
		field.NotSupported(field.NewPath("spec").Child("type"), new.Spec.Type,
			validation.GetValidCodeRepoServiceStringTypes(),
		))
	errs = append(errs,
		field.Forbidden(field.NewPath("spec").Child("http").Child("host"),
			"update is forbidden for codeRepoService is public"),
	)

	return
}

func getValidCodeRepoServiceUpdate() (old, new *devops.CodeRepoService, errs field.ErrorList) {
	old = &devops.CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.CodeRepoServiceSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "https://gitlab.com",
				},
			},
			Type:   "Gitlab",
			Public: false,
		},
	}

	new = &devops.CodeRepoService{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.CodeRepoServiceSpec{
			ToolSpec: devops.ToolSpec{
				HTTP: devops.HostPort{
					Host: "https://alauda.gitlab.com",
				},
			},
			Type:   "Gitlab",
			Public: false,
		},
	}

	return
}
