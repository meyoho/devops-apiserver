package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
	glog "k8s.io/klog"
)

// ValidateToolCategoryToolUpdate validates an update over the ToolCategoryTool
func ValidateToolCategoryUpdate(new, old *devops.ToolCategory) (errs field.ErrorList) {
	specFiled := field.NewPath("spec")

	// spec
	errs = ValidateToolCategorySpecUpdate(&new.Spec, &new.Spec, specFiled)
	return
}

// ValidateToolCategoryToolSpecUpdate spec data from ToolCategoryToolSpec
func ValidateToolCategorySpecUpdate(newSpec, oldSpec *devops.ToolCategorySpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if oldSpec == nil {
		glog.V(7).Infof("old spec is nil")
	}

	// spec
	errs = append(errs, ValidateToolCategorySpec(newSpec, fldPath)...)
	return
}

// ValidateToolCategoryTool validates a ToolCategoryTool config
func ValidateToolCategory(new *devops.ToolCategory) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateToolCategorySpec(&new.Spec, specField)
	return
}

// ValidateToolCategoryToolSpec spec data from ToolCategoryToolSpec
func ValidateToolCategorySpec(spec *devops.ToolCategorySpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	return
}
