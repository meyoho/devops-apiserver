package validation

import (
	"fmt"
	"strings"

	glog "k8s.io/klog"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// ValidatePipelineTemplate validate all fields
func ValidatePipelineTemplate(pipelineTemplate *devops.PipelineTemplate) (errs field.ErrorList) {
	// spec
	errs = ValidatePipelineTemplateSpec(&pipelineTemplate.Spec, field.NewPath("spec"))
	//  metadata
	errs = append(errs, ValidateTemplateMetadata(pipelineTemplate.GetObjectMeta(), field.NewPath("metadata"))...)

	return
}

// ValidateTemplateMetadata validate template metadata
func ValidateTemplateMetadata(meta metav1.Object, fldPath *field.Path) (errs field.ErrorList) {
	labels := meta.GetLabels()
	if labels == nil {
		labels = make(map[string]string)
	}

	if val, ok := labels["category"]; !ok || "" == val {
		errs = append(errs, field.Invalid(
			fldPath.Child("labels"),
			val,
			"please provide category label",
		))
	}

	return
}

// ValidatePipelineTemplateSpec will validate PipelineTemplate specification
func ValidatePipelineTemplateSpec(pipelineTemplateSpec *devops.PipelineTemplateSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// stages
	errs = append(errs, validatePipelineTemplateStages(pipelineTemplateSpec.Stages, fldPath.Child("stages"))...)
	// parameters
	errs = append(errs, ValidatePipelineParameter(pipelineTemplateSpec.Parameters, fldPath.Child("parameters"))...)
	// arguments
	errs = append(errs, ValidatePipelineTemplateArgumentGroup(pipelineTemplateSpec.Arguments, fldPath.Child("arguments"))...)

	return
}

func validatePipelineTemplateStages(pipelineTemplateStages []devops.PipelineStage, fldPath *field.Path) (errs field.ErrorList) {
	glog.V(7).Infof("pipelineTemplateStages: %v; fldPath: %v", pipelineTemplateStages, fldPath)
	errs = field.ErrorList{}

	return
}

// ValidatePipelineParameter validate all parameters
func ValidatePipelineParameter(pipelineParameters []devops.PipelineParameter, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	if len(pipelineParameters) == 0 {
		return
	}

	for index, param := range pipelineParameters {
		if strings.TrimSpace(param.Name) == "" {
			errs = append(errs, field.Invalid(
				fldPath.Index(index).Child("name"),
				param,
				fmt.Sprintf("please provide name %d", index),
			))
		}

		if strings.TrimSpace(string(param.Type)) == "" {
			errs = append(errs, field.Invalid(
				fldPath.Index(index).Child("type"),
				param,
				fmt.Sprintf("please provide type %d", index),
			))
		}
	}

	return
}

// ValidatePipelineTemplateArgumentGroup valiate PipelineTemplateArgumentGroup
func ValidatePipelineTemplateArgumentGroup(pipelineTemplateArguments []devops.PipelineTemplateArgumentGroup, fldPath *field.Path) (errs field.ErrorList) {
	glog.V(7).Infof("pipelineTemplateArguments: %v; fldPath: %v", pipelineTemplateArguments, fldPath)

	errs = field.ErrorList{}
	return
}

// ValidatePipelineTemplateUpdate will validate when PipelineTemplate been update
func ValidatePipelineTemplateUpdate(new *devops.PipelineTemplate, old *devops.PipelineTemplate) (errs field.ErrorList) {
	glog.V(7).Infof("new: %v; old: %v", new, old)
	errs = field.ErrorList{}
	return
}
