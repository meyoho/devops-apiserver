package validation

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// ValidateCodeRepoServiceUpdate validates an update over the CodeRepoService
func ValidateCodeRepoServiceUpdate(new *devops.CodeRepoService, old *devops.CodeRepoService) (errs field.ErrorList) {
	specField := field.NewPath("spec")

	// spec
	errs = ValidateCodeRepoServiceSpecUpdate(&new.Spec, &old.Spec, specField)
	return
}

// ValidateCodeRepoServiceSpec spec data from CodeRepoServiceSpec
func ValidateCodeRepoServiceSpecUpdate(newSpec, oldSpec *devops.CodeRepoServiceSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// spec
	errs = append(errs, ValidateCodeRepoServiceSpec(newSpec, fldPath)...)
	// http
	errs = append(errs, ValidateCodeRepoServiceSpecHTTPUpdate(newSpec, oldSpec, fldPath.Child("http"))...)
	return
}

// ValidateCodeRepoServiceSpecHTTPUpdate validate http when update a CodeRepoService
func ValidateCodeRepoServiceSpecHTTPUpdate(newSpec, oldSpec *devops.CodeRepoServiceSpec, fldPath *field.Path) (errs field.ErrorList) {
	if newSpec.Public && oldSpec.HTTP.Host != newSpec.HTTP.Host {
		errs = append(errs, field.Forbidden(
			fldPath.Child("host"),
			"update is forbidden for codeRepoService is public",
		))
		return
	}

	errs = append(errs, ValidateHostPort(&newSpec.HTTP, fldPath)...)
	return
}

// ValidateCodeRepoService validates a codeRepoService config
func ValidateCodeRepoService(codeRepoService *devops.CodeRepoService) (errs field.ErrorList) {
	specField := field.NewPath("spec")
	// spec
	errs = ValidateCodeRepoServiceSpec(&codeRepoService.Spec, specField)
	return
}

// ValidateCodeRepoServiceSpec spec data from CodeRepoServiceSpec
func ValidateCodeRepoServiceSpec(spec *devops.CodeRepoServiceSpec, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}

	// type
	errs = append(errs, ValidateServiceType(spec.Type, fldPath.Child("type"))...)
	// http
	errs = append(errs, ValidateHostPort(&spec.HTTP, fldPath.Child("http"))...)
	return
}

var validCodeRepoServiceTypes = []devops.CodeRepoServiceType{devops.CodeRepoServiceTypeGithub, devops.CodeRepoServiceTypeGitlab, devops.CodeRepoServiceTypeGitee, devops.CodeRepoServiceTypeBitbucket, devops.CodeRepoServiceTypeGitea, devops.CodeRepoServiceTypeGogs}

// GetValidCodeRepoServiceStringTypes gets string types of valid code repo service
func GetValidCodeRepoServiceStringTypes() []string {
	validValues := make([]string, 0, len(validCodeRepoServiceTypes))
	for _, valid := range validCodeRepoServiceTypes {
		validValues = append(validValues, valid.String())
	}

	return validValues
}

func ValidateServiceType(serviceType devops.CodeRepoServiceType, fldPath *field.Path) (errs field.ErrorList) {
	errs = field.ErrorList{}
	var found bool
	for _, validCodeRepoServiceType := range validCodeRepoServiceTypes {
		if validCodeRepoServiceType == serviceType {
			found = true
			break
		}
	}

	if !found {
		errs = append(errs, field.NotSupported(
			fldPath,
			serviceType,
			GetValidCodeRepoServiceStringTypes(),
		))
	}

	return
}
