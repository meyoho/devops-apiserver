package validation_test

import (
	"testing"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/validation"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func TestValidatePipelineConfig(t *testing.T) {

	type Table struct {
		name     string
		input    *devops.PipelineConfig
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	errorInput, fullErrors := GetInvalidPipelineConfig()

	correctInput, correctErrs := GetValidPipelineConfig()

	templatePipelineConfig, emptyErrs := GetTemplatePipelineConfig()

	table := []Table{
		{
			name:     "create: empty PipelineConfig",
			input:    errorInput,
			expected: fullErrors,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidatePipelineConfig(obj.(*devops.PipelineConfig))
			},
		},
		{
			name:     "create: correct PipelineConfig",
			input:    correctInput,
			expected: correctErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidatePipelineConfig(obj.(*devops.PipelineConfig))
			},
		},
		{
			name:     "create: correct templated PipelineConfig",
			input:    templatePipelineConfig,
			expected: emptyErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidatePipelineConfig(obj.(*devops.PipelineConfig))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func GetTemplatePipelineConfig() (*devops.PipelineConfig, field.ErrorList) {
	config, err := GetValidPipelineConfig()
	config.Spec.Strategy.Jenkins.JenkinsfilePath = ""
	config.Spec.Template.PipelineTemplateRef.Kind = "ClusterPipelineTemplate"
	config.Spec.Template.PipelineTemplateRef.Name = "GolangBuilder"
	return config, err
}

func GetValidPipelineConfig() (*devops.PipelineConfig, field.ErrorList) {
	return &devops.PipelineConfig{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.PipelineConfigSpec{
			Source: devops.PipelineSource{
				Git: &devops.PipelineSourceGit{
					URI: "fake.uri",
					Ref: "fake",
				},
			},
			JenkinsBinding: devops.LocalObjectReference{
				Name: "jenkins",
			},
			RunLimits: devops.PipelineRunLimits{
				SuccessCount: 10,
				FailureCount: 10,
			},
			RunPolicy: devops.PipelinePolicySerial,
			Parameters: []devops.PipelineParameter{
				devops.PipelineParameter{
					Name: "parameter",
					Type: "string",
				},
			},
			Triggers: []devops.PipelineTrigger{
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCodeChange,
					CodeChange: &devops.PipelineTriggerCodeChange{
						PeriodicCheck: "* * * * *",
					},
				},
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCron,
					Cron: &devops.PipelineTriggerCron{
						Enabled: true,
						Rule:    "* * * * *",
					},
				},
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCron,
					Cron: &devops.PipelineTriggerCron{
						Schedule: &devops.PipelineTriggeSchedule{
							Weeks: []devops.Week{
								"mon", "1",
							},
						},
					},
				},
			},
			Strategy: devops.PipelineStrategy{
				Jenkins: devops.PipelineStrategyJenkins{
					JenkinsfilePath: "Jenkinsfile",
				},
			},
			Hooks: []devops.PipelineHook{
				devops.PipelineHook{
					Type: devops.PipelineHookTypeHTTPRequest,
					Events: []devops.PipelineEvent{
						devops.PipelineEventConfigCreated,
					},
					HTTPRequest: &devops.PipelineHookHTTPRequest{
						URI:    "http://some.com",
						Method: "POST",
					},
				},
			},
		},
	}, field.ErrorList{}
}

func GetInvalidPipelineConfig() (pipe *devops.PipelineConfig, errs field.ErrorList) {
	pipe = &devops.PipelineConfig{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.PipelineConfigSpec{
			Strategy: devops.PipelineStrategy{},
			RunLimits: devops.PipelineRunLimits{
				SuccessCount: -1,
				FailureCount: -1,
			},
			Parameters: []devops.PipelineParameter{
				devops.PipelineParameter{
					Name: "",
					Type: "",
				},
			},
			Triggers: []devops.PipelineTrigger{
				// missing type
				devops.PipelineTrigger{
					Type: "",
				},
				// missing cron
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCron,
				},
				// missing codechange
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCodeChange,
				},
				// missing cron.rule
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCron,
					Cron: &devops.PipelineTriggerCron{
						Enabled: true,
						Rule:    "",
					},
				},
				// wrong expression for week
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCron,
					Cron: &devops.PipelineTriggerCron{
						Schedule: &devops.PipelineTriggeSchedule{
							Weeks: []devops.Week{
								"x",
							},
						},
					},
				},
			},
			Hooks: []devops.PipelineHook{
				// missing type
				devops.PipelineHook{},
				// missing httpRequest
				devops.PipelineHook{
					Type:   devops.PipelineHookTypeHTTPRequest,
					Events: []devops.PipelineEvent{"aa"},
				},
				// invalid event
				devops.PipelineHook{
					Type: devops.PipelineHookTypeHTTPRequest,
					Events: []devops.PipelineEvent{
						devops.PipelineEventConfigCreated,
					},
					HTTPRequest: &devops.PipelineHookHTTPRequest{
						URI:    "aaa",
						Method: "some",
					},
				},
			},
			Source: devops.PipelineSource{
				Git:    &devops.PipelineSourceGit{},
				Secret: &devops.SecretKeySetRef{},
			},
		},
	}

	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("jenkinsBinding").Child("name"),
			"",
			"please provide a JenkinsBinding instance name",
		),
		field.Invalid(
			field.NewPath("spec").Child("runPolicy"),
			"",
			"please provide a valid runPolicy: Serial or Parallel",
		),
		field.Invalid(
			field.NewPath("spec").Child("runLimits").Child("successCount"),
			-1,
			"please provide a valid successCount, must be a positive number, or 0 for no limits",
		),
		field.Invalid(
			field.NewPath("spec").Child("runLimits").Child("failureCount"),
			-1,
			"please provide a valid failureCount, must be a positive number, or 0 for no limits",
		),
		field.Invalid(
			field.NewPath("spec").Child("parameters").Index(0).Child("name"),
			"",
			"please provide a valid parameter name",
		),
		field.Invalid(
			field.NewPath("spec").Child("parameters").Index(0).Child("type"),
			"",
			"please provide a valid parameter type: string or boolean",
		),
		field.Invalid(
			field.NewPath("spec").Child("triggers").Index(0).Child("type"),
			"",
			"please provide a valid trigger type: cron or codeChange",
		),
		field.Invalid(
			field.NewPath("spec").Child("triggers").Index(1).Child("cron"),
			nil,
			"please provide a cron configuration",
		),
		field.Invalid(
			field.NewPath("spec").Child("triggers").Index(2).Child("codeChange"),
			nil,
			"please provide a codeChange configuration",
		),
		field.Invalid(
			field.NewPath("spec").Child("triggers").Index(3).Child("cron").Child("rule"),
			"",
			"please provide a cron schedule rule or schedule for the cron trigger",
		),
		field.Invalid(
			field.NewPath("spec").Child("triggers").Index(4).Child("cron").Child("schedule").Child("weeks").Index(0),
			"x",
			"please provide a valid week expression",
		),
		field.Invalid(
			field.NewPath("spec").Child("strategy").Child("jenkins"),
			"",
			"please provide the pipeline strategy: jenkinsfile or jenkinsfilePath",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(0).Child("type"),
			"",
			"please provide a valid hook type: httpRequest",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(0).Child("events"),
			nil,
			"please provide at least one valid event: PipelineConfigCreated, PipelineConfigUpdated, PipelineConfigDeleted, PipelineStarted, PipelineStopped",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(1).Child("httpRequest"),
			nil,
			"please provide a httpRequest configuration",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(1).Child("events").Index(0),
			"aa",
			"please provide at least one valid event: PipelineConfigCreated, PipelineConfigUpdated, PipelineConfigDeleted, PipelineStarted, PipelineStopped",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(2).Child("httpRequest").Child("uri"),
			"aaa",
			"please provide a valid http uri",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(2).Child("httpRequest").Child("method"),
			"some",
			"please provide a valid http method: GET, POST, PUT, DELETE, HEAD",
		),
		field.Invalid(
			field.NewPath("spec").Child("source").Child("git").Child("uri"),
			"",
			"please provide a git uri",
		),
		field.Invalid(
			field.NewPath("spec").Child("source").Child("git").Child("ref"),
			"",
			"please provide a git branch name or * for all",
		),
	}

	return
}

func TestValidatePipeline(t *testing.T) {

	type Table struct {
		name     string
		input    *devops.Pipeline
		expected field.ErrorList
		method   func(obj runtime.Object) field.ErrorList
	}

	errorInput, fullErrors := GetInvalidPipeline()

	correctInput, correctErrs := GetValidPipeline()

	table := []Table{
		{
			name:     "create: empty Pipeline",
			input:    errorInput,
			expected: fullErrors,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidatePipeline(obj.(*devops.Pipeline))
			},
		},
		{
			name:     "create: correct Pipeline",
			input:    correctInput,
			expected: correctErrs,
			method: func(obj runtime.Object) field.ErrorList {
				return validation.ValidatePipeline(obj.(*devops.Pipeline))
			},
		},
	}

	for i, tst := range table {
		res := tst.method(tst.input)
		if len(res) != len(tst.expected) {
			t.Errorf(
				"Test %d: %v - expected error lists with different size: len(%v) != len(%v)\n %v != %v",
				i, tst.name,
				len(tst.expected), len(res),
				tst.expected, res,
			)
		} else {
			for j, e := range tst.expected {
				if e.Error() != res[j].Error() {

					t.Errorf(
						"Test %d: %v - actual error %d is different than expected: %v != %v",
						i, tst.name, j,
						e.Error(), res[j].Error(),
					)
				}
			}
		}
	}
}

func GetValidPipeline() (*devops.Pipeline, field.ErrorList) {
	return &devops.Pipeline{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.PipelineSpec{
			Source: devops.PipelineSource{
				Git: &devops.PipelineSourceGit{
					URI: "fake.uri",
					Ref: "fake",
				},
			},
			PipelineConfig: devops.LocalObjectReference{
				Name: "pipelineConfig",
			},
			JenkinsBinding: devops.LocalObjectReference{
				Name: "jenkins",
			},
			RunPolicy: devops.PipelinePolicySerial,
			Parameters: []devops.PipelineParameter{
				devops.PipelineParameter{
					Name: "parameter",
					Type: "string",
				},
			},
			Triggers: []devops.PipelineTrigger{
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCodeChange,
					CodeChange: &devops.PipelineTriggerCodeChange{
						PeriodicCheck: "* * * * *",
					},
				},
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCron,
					Cron: &devops.PipelineTriggerCron{
						Enabled: true,
						Rule:    "* * * * *",
					},
				},
			},
			Strategy: devops.PipelineStrategy{
				Jenkins: devops.PipelineStrategyJenkins{
					JenkinsfilePath: "Jenkinsfile",
				},
			},
			Hooks: []devops.PipelineHook{
				devops.PipelineHook{
					Type: devops.PipelineHookTypeHTTPRequest,
					Events: []devops.PipelineEvent{
						devops.PipelineEventConfigCreated,
					},
					HTTPRequest: &devops.PipelineHookHTTPRequest{
						URI:    "http://some.com",
						Method: "POST",
					},
				},
			},
		},
		Status: devops.PipelineStatus{
			Phase: devops.PipelinePhasePending,
		},
	}, field.ErrorList{}
}

func GetInvalidPipeline() (pipe *devops.Pipeline, errs field.ErrorList) {
	pipe = &devops.Pipeline{
		ObjectMeta: metav1.ObjectMeta{},
		Spec: devops.PipelineSpec{
			Parameters: []devops.PipelineParameter{
				devops.PipelineParameter{
					Name: "",
					Type: "",
				},
				devops.PipelineParameter{
					Name: "a",
					Type: "b",
				},
				devops.PipelineParameter{
					Name: "a",
					Type: "StringParameterDefinition",
				},
			},
			Triggers: []devops.PipelineTrigger{
				// missing type
				devops.PipelineTrigger{
					Type: "",
				},
				// missing cron
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCron,
				},
				// missing codechange
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCodeChange,
				},
				// missing cron.rule
				devops.PipelineTrigger{
					Type: devops.PipelineTriggerTypeCron,
					Cron: &devops.PipelineTriggerCron{
						Enabled: true,
						Rule:    "",
					},
				},
			},
			Hooks: []devops.PipelineHook{
				// missing type
				devops.PipelineHook{},
				// missing httpRequest
				devops.PipelineHook{
					Type:   devops.PipelineHookTypeHTTPRequest,
					Events: []devops.PipelineEvent{"aa"},
				},
				// invalid event
				devops.PipelineHook{
					Type: devops.PipelineHookTypeHTTPRequest,
					Events: []devops.PipelineEvent{
						devops.PipelineEventConfigCreated,
					},
					HTTPRequest: &devops.PipelineHookHTTPRequest{
						URI:    "aaa",
						Method: "some",
					},
				},
			},
			Source: devops.PipelineSource{
				Git:    &devops.PipelineSourceGit{},
				Secret: &devops.SecretKeySetRef{},
			},
		},
		Status: devops.PipelineStatus{
			Phase: "",
		},
	}

	errs = field.ErrorList{
		field.Invalid(
			field.NewPath("spec").Child("pipelineConfig").Child("name"),
			"",
			"please provide a PipelineConfig instance name",
		),
		field.Invalid(
			field.NewPath("spec").Child("jenkinsBinding").Child("name"),
			"",
			"please provide a JenkinsBinding instance name",
		),
		field.Invalid(
			field.NewPath("spec").Child("runPolicy"),
			"",
			"please provide a valid runPolicy: Serial or Parallel",
		),
		field.Invalid(
			field.NewPath("spec").Child("parameters").Index(0).Child("name"),
			"",
			"please provide a valid parameter name",
		),
		field.Invalid(
			field.NewPath("spec").Child("parameters").Index(0).Child("type"),
			"",
			"please provide a valid parameter type: string or boolean",
		),
		field.Invalid(
			field.NewPath("spec").Child("parameters").Index(1).Child("type"),
			"b",
			"please provide a valid parameter type: string or boolean",
		),
		field.Invalid(
			field.NewPath("spec").Child("parameters").Index(2).Child("name"),
			"a",
			"parameter must be unique",
		),
		field.Invalid(
			field.NewPath("spec").Child("triggers").Index(0).Child("type"),
			"",
			"please provide a valid trigger type: cron or codeChange",
		),
		field.Invalid(
			field.NewPath("spec").Child("triggers").Index(1).Child("cron"),
			nil,
			"please provide a cron configuration",
		),
		field.Invalid(
			field.NewPath("spec").Child("triggers").Index(2).Child("codeChange"),
			nil,
			"please provide a codeChange configuration",
		),
		field.Invalid(
			field.NewPath("spec").Child("triggers").Index(3).Child("cron").Child("rule"),
			"",
			"please provide a cron schedule rule or schedule for the cron trigger",
		),
		field.Invalid(
			field.NewPath("spec").Child("strategy").Child("jenkins"),
			"",
			"please provide the pipeline strategy: jenkinsfile or jenkinsfilePath",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(0).Child("type"),
			"",
			"please provide a valid hook type: httpRequest",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(0).Child("events"),
			nil,
			"please provide at least one valid event: PipelineConfigCreated, PipelineConfigUpdated, PipelineConfigDeleted, PipelineStarted, PipelineStopped",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(1).Child("httpRequest"),
			nil,
			"please provide a httpRequest configuration",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(1).Child("events").Index(0),
			"aa",
			"please provide at least one valid event: PipelineConfigCreated, PipelineConfigUpdated, PipelineConfigDeleted, PipelineStarted, PipelineStopped",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(2).Child("httpRequest").Child("uri"),
			"aaa",
			"please provide a valid http uri",
		),
		field.Invalid(
			field.NewPath("spec").Child("hooks").Index(2).Child("httpRequest").Child("method"),
			"some",
			"please provide a valid http method: GET, POST, PUT, DELETE, HEAD",
		),
		field.Invalid(
			field.NewPath("spec").Child("source").Child("git").Child("uri"),
			"",
			"please provide a git uri",
		),
		field.Invalid(
			field.NewPath("spec").Child("source").Child("git").Child("ref"),
			"",
			"please provide a git branch name or * for all",
		),
		field.Invalid(
			field.NewPath("status").Child("phase"),
			"",
			"pipeline phase is invalid",
		),
	}

	return
}
