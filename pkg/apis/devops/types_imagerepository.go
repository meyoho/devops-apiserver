package devops

import (
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ImageRepoPhase string

const (
	// ImageRepoPhaseCreating registry service binding phase is creating
	ImageRepoPhaseCreating ImageRepoPhase = StatusCreating
	// ImageRepoPhaseReady registry service binding phase is ready
	ImageRepoPhaseReady ImageRepoPhase = StatusReady
	// ImageRepoPhaseListRepoError could not list repo
	ImageRepoPhaseListRepoError ImageRepoPhase = StatusListRepoError
	// ImageRepoPhaseError connect registry service error
	ImageRepoPhaseError ImageRepoPhase = StatusError
	// ImageRepoPhaseWaitingToDelete will delete
	ImageRepoPhaseWaitingToDelete ImageRepoPhase = StatusWaitingToDelete
)

type ImageTagScanStatus string

const (
	ImageTagScanStatusNotScan   ImageTagScanStatus = "notScan"
	ImageTagScanStatusPending   ImageTagScanStatus = "pending"
	ImageTagScanStatusAnalyzing ImageTagScanStatus = "analyzing"
	ImageTagScanStatusFinished  ImageTagScanStatus = "finished"
	ImageTagScanStatusError     ImageTagScanStatus = "error"
)

// +genclient
// +genclient:method=ScanImage,verb=create,subresource=security,input=ImageScanOptions,result=ImageResult
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRepository save image info, include image path and tag info
type ImageRepository struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the ImageRepository.
	// +optional
	Spec ImageRepositorySpec
	// Most recently observed status of the ImageRepository.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ImageRepositoryStatus
}

// ImageRepositorySpec represents ImageRepository's specs
type ImageRepositorySpec struct {
	// ImageRegistry defines the imageRegistry in spec
	ImageRegistry LocalObjectReference
	// ImageRegistryBinding defines the imageRegistryBinding in spec
	ImageRegistryBinding LocalObjectReference
	// CodeRepoBinding defines the image in spec
	Image string
}

// ImageRepositoryStatus defines the status of the service, inherit ServiceStatus
type ImageRepositoryStatus struct {
	// ServiceStatus defines the common status
	// +optional
	ServiceStatus
	// Tags defines the tags in the image repository
	// +optional
	Tags []ImageTag
	// LatestTag defines the latest tag pushed
	// +optional
	LatestTag ImageTag
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// ImageResult
type ImageResult struct {
	metav1.TypeMeta

	StatusCode int
	Message    string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// ImageTagResult
type ImageTagResult struct {
	metav1.TypeMeta

	Tags []ImageTag
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// ImageTagOptions
type ImageTagOptions struct {
	metav1.TypeMeta
	// SortBy indicate sort by which element
	// +optional
	SortBy string
	// SortMode indicate which sort mode to use for example asc desc natural
	// +optional
	SortMode string
}

// ImageTag defines the image tag attributes
type ImageTag struct {
	// Name defines the tag name
	// +optional
	Name string
	// Digest defines the tag digest
	// +optional
	Digest string
	// Author defines the tag author
	// Support for Harbor
	// +optional
	Author string
	// CreatedAt defines the tag created_at
	// +optional
	CreatedAt *metav1.Time
	// UpdatedAt defines the completion time of Harbor scan tag
	// Support for Harbor
	// +optional
	UpdatedAt *metav1.Time
	// Size is the size of this tag
	Size string
	//Level is the level of this tag
	Level int
	// ScanStatus represent the scanstatus of this tag
	ScanStatus ImageTagScanStatus
	// Message represent the message of this tag
	Message string
	// Summary of this tag
	Summary []Summary
}

// Summary defines the Severity Count
type Summary struct {
	// Severity represent the severity level
	Severity int
	// Count represent the num of this severity
	Count int
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// Vulnerability
type Vulnerability struct {
	metav1.TypeMeta

	ID          string
	Severity    int
	Package     string
	Version     string
	Description string
	Link        string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// VulnerabilityList
type VulnerabilityList struct {
	metav1.TypeMeta
	// +optional
	metav1.ListMeta

	Items []Vulnerability
}

// GetCreatedAt get tag created_at
func (i *ImageTag) GetCreatedAt() time.Time {
	if i.CreatedAt != nil {
		return i.CreatedAt.Time
	}
	return time.Time{}
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRepositoryList is a list of ImageRepository objects.
type ImageRepositoryList struct {
	metav1.TypeMeta
	// +optional
	metav1.ListMeta

	Items []ImageRepository
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageScanOptions user to fetch scan image options
type ImageScanOptions struct {
	metav1.TypeMeta
	// Tag represent witch tag image will scan
	Tag string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// ImageRepositoryOptions presets image repository options
type ImageRepositoryOptions struct {
	metav1.TypeMeta
	Image string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ImageRepositoryRemoteStatus struct {
	metav1.TypeMeta
	Status *HostPortStatus
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ImageRepositoryLink struct {
	metav1.TypeMeta
	Link string
}

// GetRegistryName get service name
func (c *ImageRepository) GetRegistryName() string {
	repoLabels := c.GetLabels()
	if repoLabels == nil {
		return ""
	}

	if serviceName, ok := repoLabels[LabelImageRegistry]; ok {
		return serviceName
	}
	return ""
}

// endregion

var _ StatusAccessor = &ImageRepository{}

// GetStatus implements StatusAccessor
func (b *ImageRepository) GetStatus() *ServiceStatus {
	return &b.Status.ServiceStatus
}
