package devops

import (
	"fmt"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// region CodeRepository

// CodeRepoServiceType type for the repo service
type CodeRepoServiceType string

const (
	// CodeRepoServiceTypeGithub github

	CodeRepoServiceTypeGithub CodeRepoServiceType = "Github"
	// CodeRepoServiceTypeGitlab gitlab

	CodeRepoServiceTypeGitlab CodeRepoServiceType = "Gitlab"
	// CodeRepoServiceTypeGitee gitee

	CodeRepoServiceTypeGitee CodeRepoServiceType = "Gitee"
	// CodeRepoServiceTypeBitbucket bitbucket

	CodeRepoServiceTypeBitbucket CodeRepoServiceType = "Bitbucket"

	// CodeRepoServiceTypeGogs gogs
	CodeRepoServiceTypeGogs CodeRepoServiceType = "Gogs"
	// CodeRepoServiceTypeGitea gitea
	CodeRepoServiceTypeGitea CodeRepoServiceType = "Gitea"
)

func (c CodeRepoServiceType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoService struct holds a reference to a specific CodeRepoService configuration
// and some user data for access
type CodeRepoService struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the CodeRepoService.
	// +optional
	Spec CodeRepoServiceSpec
	// Most recently observed status of the CodeRepoService.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus
}

// GetHost get the host defined in the service.
func (c *CodeRepoService) GetHost() string {
	return c.Spec.HTTP.Host
}

// GetHost get the html defined in the service.
func (c *CodeRepoService) GetHtml() string {
	switch c.Spec.Type {
	case CodeRepoServiceTypeGitlab, CodeRepoServiceTypeGitee, CodeRepoServiceTypeGogs, CodeRepoServiceTypeGitea:
		return c.Spec.HTTP.Host
	case CodeRepoServiceTypeGithub:
		return GithubHTML
	case CodeRepoServiceTypeBitbucket:
		return BitbucketHTML
	default:
		return fmt.Sprintf("invalid codeRepoService type %s", c.Spec.Type)
	}
}

// GetType get the type of the service.
func (c *CodeRepoService) GetType() CodeRepoServiceType {
	return c.Spec.Type
}

// IsPublic defines whether the service is public.
func (c *CodeRepoService) IsPublic() bool {
	return c.Spec.Public
}

// CodeRepoServiceSpec defines CodeRepoService's specs
type CodeRepoServiceSpec struct {
	// ToolSpec defins custom fields of tool
	ToolSpec
	// Type defines the service type.
	Type CodeRepoServiceType
	// Public defines the public of the service.
	// +optional
	Public bool
	// Data defines the data.
	// +optional
	Data map[string]string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// CodeRepoBranchOptions
type CodeRepoBranchOptions struct {
	metav1.TypeMeta
	// SortBy represent how to sort data
	// +optional
	SortBy string
	// SortMode represent the sort mode for example asc desc natural
	// +optional
	SortMode string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// CodeRepoBranchResult
type CodeRepoBranchResult struct {
	metav1.TypeMeta
	Branches []CodeRepoBranch
}

var _ ToolInterface = &CodeRepoService{}

func (crs *CodeRepoService) GetKind() string {
	return TypeCodeRepoService
}

func (codeRepoService *CodeRepoService) GetHostPort() HostPort {
	return codeRepoService.Spec.HTTP
}

func (crs *CodeRepoService) GetKindType() string {
	return crs.GetType().String()
}

func (crs *CodeRepoService) GetObjectMeta() metav1.Object {
	return &crs.ObjectMeta
}

func (crs *CodeRepoService) GetStatus() *ServiceStatus {
	return &crs.Status
}

func (crs *CodeRepoService) GetToolSpec() ToolSpec {
	return crs.Spec.ToolSpec
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceList is a list of CodeRepoService objects.
type CodeRepoServiceList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items is a list of CodeRepoService.
	Items []CodeRepoService
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceAuthorizeResponse used to validate secret used in service
type CodeRepoServiceAuthorizeResponse struct {
	metav1.TypeMeta

	// AuthorizeUrl specifies the authorizeUrl used to authorize web page.
	// +optional
	AuthorizeUrl string
	// RedirectUrl specifies redirect URL for the authorization request
	// +optional
	RedirectUrl string

	// GotAccessToken true when the access token was generated successfully
	// +optional
	GotAccessToken bool
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceAuthorizeCreate used to update a secret using code response
type CodeRepoServiceAuthorizeCreate struct {
	metav1.TypeMeta
	// SecretName is a the name of the secret used for the oAuth2 authorization request
	SecretName string
	// Namespace is the namespace of the given secret defined by SecretName
	Namespace string
	// Authorization code used in the oAuth2 authorization flow
	// +optional
	Code string
	// RedirectURL used in the oAuth2 authorization flow
	// +optional
	RedirectURL string
	// Defines if the API server should request the oAuth2 access token and refresh token
	GetAccessToken bool
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceAuthorizeOptions used to fetch service options
type CodeRepoServiceAuthorizeOptions struct {
	metav1.TypeMeta

	// SecretName specifies the name of the secret.
	// +optional
	SecretName string
	// Namespace specifies the namespace of the secret.
	// +optional
	Namespace string
	// RedirectURL specifies the redirectURL of the secret.
	// +optional
	RedirectURL string
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBinding struct holds a reference to a specific CodeRepoBinding configuration
// and some user data for access
type CodeRepoBinding struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the CodeRepoBinding.
	// +optional
	Spec CodeRepoBindingSpec
	// Most recently observed status of the CodeRepoBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus
}

var _ StatusAccessor = &CodeRepoBinding{}

// GetStatus implements StatusAccessor
func (b *CodeRepoBinding) GetStatus() *ServiceStatus {
	return &b.Status
}

// CodeRepoBindingSpec defines CodeRepoBinding's spec
type CodeRepoBindingSpec struct {
	// CodeRepoService defines the codeRepoService.
	CodeRepoService LocalObjectReference
	// Account defines the account.
	Account CodeRepoBindingAccount
}

// CodeRepoBindingAccount defines CodeRepoBinding' status
type CodeRepoBindingAccount struct {
	// Secret defines the secret.
	// It should not be optional
	Secret SecretKeySetRef
	// Owners defines the owners in this account.
	// +optional
	Owners []CodeRepositoryOwnerSync
}

func (b *CodeRepoBinding) GetSecretNamespace() string {
	namespace := b.GetNamespace()
	if b.Spec.Account.Secret.Namespace != "" {
		namespace = b.Spec.Account.Secret.Namespace
	}
	return namespace
}

func (b *CodeRepoBinding) GetSecretName() string {
	return b.Spec.Account.Secret.Name
}

// CodeRepositoryOwnerSync defines owner which will be synced.
type CodeRepositoryOwnerSync struct {
	// Type defines the type.
	Type OriginCodeRepoOwnerType
	// Name defines the name.
	Name string
	// All defines whether sync all repositories in this owner.
	// +optional
	All bool
	// Repositories defines the repositories will be synced.
	// +optional
	Repositories []string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBindingList is a list of CodeRepoBinding objects.
type CodeRepoBindingList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items is a list of CodeRepoBinding objects
	Items []CodeRepoBinding
}

var _ ToolBindingLister = &CodeRepoBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *CodeRepoBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta.Kind = TypeCodeRepoBinding
	}
	return
}

const (
	GithubOrgKey    = "Organization"
	GitlabOrgKey    = "group"
	BitbucketOrgKey = "team"
	// gitee was not defined here because of no org key in response.
)

// GetOwnerType unify the owner type from different code repository service.
func GetOwnerType(strOwnerType string) OriginCodeRepoOwnerType {
	switch strOwnerType {
	case GithubOrgKey, GitlabOrgKey, BitbucketOrgKey:
		return OriginCodeRepoRoleTypeOrg
	default:
		return OriginCodeRepoOwnerTypeUser
	}
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepository struct holds a reference to a specific CodeRepository configuration
// and some user data for access
type CodeRepository struct {
	metav1.TypeMeta
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta

	// Specification of the desired behavior of the CodeRepository.
	// +optional
	Spec CodeRepositorySpec
	// Most recently observed status of the CodeRepository.
	// Populated by the system.
	// Read-only.
	// +optional
	Status CodeRepositoryStatus
}

type CodeRepoBranch struct {
	// Name defines the name in branch
	Name string
	// Commit defines the record was last commit by this branch
	Commit string
}

// CodeRepositorySpec defines CodeRepository's specs
type CodeRepositorySpec struct {
	// CodeRepoBinding defines the codeRepoBinding
	CodeRepoBinding LocalObjectReference
	// Repository defines the repository
	Repository OriginCodeRepository
}

// CodeRepositoryStatus represents CodeRepository's status
type CodeRepositoryStatus struct {
	// ServiceStatus defines the common status
	// +optional
	ServiceStatus
	// Repository defines the repository in status
	// +optional
	Repository CodeRepositoryStatusRepository
}

// CodeRepositoryStatusRepository represents CodeRepositoryStatusRepository's Repository
type CodeRepositoryStatusRepository struct {
	// LatestCommit defines the lastest commit
	// +optional
	LatestCommit RepositoryCommit
}

// GetServiceName get service name
func (c *CodeRepository) GetServiceName() string {
	repoLabels := c.GetLabels()
	if repoLabels == nil {
		return ""
	}

	if serviceName, ok := repoLabels[LabelCodeRepoService]; ok {
		return serviceName
	}
	return ""
}

// GetRepoFullName defines repository's fullname. eg: "alauda/devops/devops-1/sy-prj4"
func (c *CodeRepository) GetRepoFullName() string {
	return c.Spec.Repository.FullName
}

// GetRepoID defines repository's id
func (c *CodeRepository) GetRepoID() string {
	return c.Spec.Repository.ID
}

// GetRepoName defines repository's name
func (c *CodeRepository) GetRepoName() string {
	return c.Spec.Repository.Name
}

// GetOwnerName defines owner's name
func (c *CodeRepository) GetOwnerName() string {
	return c.Spec.Repository.Owner.Name
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepositoryList is a list of CodeRepository objects.
type CodeRepositoryList struct {
	metav1.TypeMeta
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items list of CodeRepository objects.
	Items []CodeRepository
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBindingRepositoryOptions used to fetch service options
type CodeRepoBindingRepositoryOptions struct {
	metav1.TypeMeta
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBindingRepositories used to retrieve logs from a pipeline
type CodeRepoBindingRepositories struct {
	metav1.TypeMeta

	// CodeRepoServiceType defines the service type.
	Type CodeRepoServiceType
	// Owners a list of owner can be fetched from a binding.
	// +optional
	Owners []CodeRepositoryOwner
}

// CodeRepositoryOwner defines the repository owner.
type CodeRepositoryOwner struct {
	// Type defines the owner type.
	Type OriginCodeRepoOwnerType
	// ID defines the id.
	ID string
	// Name defines the name.
	Name string
	// Email defines the email.
	// +optional
	Email string
	// HTMLURL defines the htmlURL.
	// +optional
	HTMLURL string
	// AvatarURL defines the avatarURL.
	// +optional
	AvatarURL string
	// DiskUsage defines the diskUsage.
	// +optional
	DiskUsage int
	// Repositories defines the repositories in this owner.
	// +optional
	Repositories []OriginCodeRepository
}

type OriginCodeRepository struct {
	// CodeRepoServiceType defines the service type.
	CodeRepoServiceType CodeRepoServiceType
	// ID defines the id.
	ID string
	// Name defines the name.
	Name string
	// FullName defines the full name
	// +optional
	FullName string
	// Description defines the description.
	// +optional
	Description string
	// HTMLURL defines the html url.
	// +optional
	HTMLURL string
	// CloneURL defines the clone url which begins with "https://" or "http://".
	CloneURL string
	// SSHURL defines ssh clone url which begins with "git@".
	SSHURL string
	// Language defines the language.
	// +optional
	Language string
	// Owner defines the owner.
	Owner OwnerInRepository
	// CreatedAt defines the created time.
	CreatedAt *metav1.Time
	// PushedAt defines the pushed time.
	// +optional
	PushedAt *metav1.Time
	// UpdatedAt defines the updated time.
	// +optional
	UpdatedAt *metav1.Time
	// Private defines the visibility of the repository.
	// +optional
	Private bool
	// Size defines the size of the repository used.
	// +optional
	Size int64
	// Size defines the size of the repository used which is humanize.
	// +optional
	SizeHumanize string
	// Data defines reserved fields for the repository
	// +optional
	Data map[string]string
}

type RepositoryCommit struct {
	// CommitID defines the commit id
	// +optional
	CommitID string
	// CommitMessage defines the commit id
	// +optional
	CommitMessage string
	// CommitAt defines the commit time
	// +optional
	CommitAt *metav1.Time
	// CommitterName defines the committer's name
	// +optional
	CommitterName string
	// CommitterEmail defines the committer's email
	// +optional
	CommitterEmail string
}

func (r *RepositoryCommit) GetCommitAt() time.Time {
	if r.CommitAt != nil {
		return r.CommitAt.Time
	}
	return time.Time{}
}

// OwnerInRepository defines the owner in repository which includes fewer properties.
type OwnerInRepository struct {
	// OriginCodeRepoOwnerType defines the type of the owner
	// +optional
	Type OriginCodeRepoOwnerType
	// ID defines the id of the owner
	// +optional
	ID string
	// Name defines the name of the owner
	// +optional
	Name string
}

// OriginCodeRepoOwnerType defines the owner type of the repository
type OriginCodeRepoOwnerType string

const (
	// OriginCodeRepoOwnerTypeUser defines the owner type of user
	OriginCodeRepoOwnerTypeUser OriginCodeRepoOwnerType = "User"
	// OriginCodeRepoRoleTypeOrg defines the owner type of org
	OriginCodeRepoRoleTypeOrg OriginCodeRepoOwnerType = "Org"
)

// endregion
