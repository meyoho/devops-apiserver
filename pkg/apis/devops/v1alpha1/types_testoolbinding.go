package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TestToolBinding is the binding referenced to TestTool
type TestToolBinding struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the TestToolBinding.
	// +optional
	Spec TestToolBindingSpec `json:"spec"`
	// Most recently observed status of the TestToolBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// TestToolBindingSpec is the spec in TestToolBinding
type TestToolBindingSpec struct {
	TestTool LocalObjectReference `json:"testTool"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TestToolBindingList is a list of TestToolBinding objects.
type TestToolBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of TestToolBinding
	Items []TestToolBinding `json:"items"`
}

// endregion
