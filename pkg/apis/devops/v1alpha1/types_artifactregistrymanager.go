package v1alpha1

import (
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ArtifactRegistryManagerType type
type ArtifactRegistryManagerType string

const (
	// ArtifactRegistryManagerTypeNexus
	// +alauda:toolchain-gen:class=item,category=artifactRepository,name=Nexus,en=Nexus,zh=Nexus,apipath=artifactregistrymanagers,enabled=true,kind=artifactRegistryManager,type=Nexus,index=5
	// +alauda:toolchain-gen:class=secret,category=artifactRepository,itemName=Nexus,type=kubernetes.io/basic-auth,en=Input Username and Password as used on login,zh=用户名和密码均为登录时的用户名和密码
	ArtifactRegistryManagerTypeNexus ArtifactRegistryManagerType = "Nexus"
	// ArtifactRegistryManagerTypeJfrog
	// +alauda:toolchain-gen:class=item,category=artifactRepository,name=Jfrog,en=Jfrog,zh=Jfrog,apipath=artifactregistrymanagers,enabled=false,kind=artifactRegistryManager,type=Jfrog,index=5
	// +alauda:toolchain-gen:class=secret,category=artifactRepository,itemName=Jfrog,type=kubernetes.io/basic-auth,en=Input Username and Password as used on login,zh=用户名和密码均为登录时的用户名和密码
	ArtifactRegistryManagerTypeJfrog ArtifactRegistryManagerType = "Jfrog"
)

func (c ArtifactRegistryManagerType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// real artifact pository manager service
type ArtifactRegistryManager struct {
	// +optional
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	Spec ArtifactRegistryManagerSpec `json:"spec"`
	// Read-only
	// +optional
	Status ServiceStatus `json:"status"`
}

// real artifact pository manager service spec
type ArtifactRegistryManagerSpec struct {
	ToolSpec `json:",inline"`
	Type     ArtifactRegistryManagerType `json:"type"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ArtifactRegistryManagerList Get ArtifactRegistryManager list
type ArtifactRegistryManagerList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []ArtifactRegistryManager `json:"items"`
}

var _ ToolInterface = &ArtifactRegistryManager{}

func (arm *ArtifactRegistryManager) GetKind() string {
	return TypeArtifactRegistryManager
}

func (arm *ArtifactRegistryManager) GetKindType() string {
	return arm.GetType().String()
}

func (arm *ArtifactRegistryManager) GetObjectMeta() metav1.Object {
	return &arm.ObjectMeta
}

func (arm *ArtifactRegistryManager) GetHostPort() HostPort {
	return arm.Spec.HTTP
}

func (arm *ArtifactRegistryManager) GetStatus() *ServiceStatus {
	return &arm.Status
}

func (arm *ArtifactRegistryManager) GetToolSpec() ToolSpec {
	return arm.Spec.ToolSpec
}

func (arm *ArtifactRegistryManager) GetEndpoint() string {
	endpoint := arm.Spec.HTTP.Host
	endpoint = strings.TrimRight(endpoint, "/")
	return endpoint
}

func (arm *ArtifactRegistryManager) GetType() ArtifactRegistryManagerType {
	return arm.Spec.Type
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type BlobStoreOption struct {
	// +optional
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	Name string

	Type string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type BlobStoreOptionList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []BlobStoreOption `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type ArtifactRegistryOption struct {
	// +optional
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	//like maven2,npm and so on
	ArtifactType string `json:"ArtifactType"`

	//with manager`s type,like hosted,group
	Type string `json:"Type"`

	//Filter out what already exists artifactregistry
	IsFilterAR string `json:"IsFilterAR"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// ArtifactRegistryManagerOptions
type ArtifactRegistryManagerOptions struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`
	// TODO more options
}

// endregion ToolInterface
