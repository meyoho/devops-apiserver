package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ArtifactRegistryType string
type ArtifactRegistryFormat string

const (
	// ArtifactRegistryTypeMaven2
	// +alauda:toolchain-gen:class=item,category=artifactRepository,name=Maven2,en=Maven2,zh=Maven2,apipath=artifactregistries,enabled=true,kind=artifactRegistry,type=Maven2,roleSyncEnabled=true
	// +alauda:toolchain-gen:class=secret,category=artifactRepository,itemName=Maven2,type=kubernetes.io/basic-auth,en=Input Username and Password as used on login,zh=用户名和密码均为登录时的用户名和密码
	// +alauda:rolesync-gen:class=platform,name=Maven2,sync=rbac,enabled=true,roles=project_admin:admin;project_auditor:developer;space_admin:admin;space_developer:developer;space_auditor:developer;namespace_admin:admin;namespace_auditor:developer;namespace_developer:developer,custom=admin[admin:*;view:*]developer[view:*]

	ArtifactRegistryTypeMaven2 string = "Maven2"
)

func (c ArtifactRegistryType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// real artifact registry service
type ArtifactRegistry struct {
	// +optional
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	Spec ArtifactRegistrySpec `json:"spec"`
	// Read-only
	// +optional
	Status ServiceStatus `json:"status"`
}

// real artifact registry service spec
type ArtifactRegistrySpec struct {
	ArtifactRegistryArgs map[string]string `json:"artifactRegistryArgs"`
	ArtifactRegistryName string            `json:"artifactRegistryName"`
	Type                 string            `json:"type"`
	Public               bool              `json:"public"`
	ToolSpec             `json:",inline"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ArtifactRegistryList Get ArtifactRegistry list
type ArtifactRegistryList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []ArtifactRegistry `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ArtifactRegistryCreateOptions for artifact registry create
type ArtifactRegistryCreateOptions struct {
	// +optional
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	Spec ArtifactRegistryCreateOptionSpec `json:"spec"`
}

type ArtifactRegistryCreateOptionSpec struct {
	ArtifactRegistryArgs map[string]string `json:"artifactRegistryArgs"`
	ArtifactRegistryName string            `json:"artifactRegistryName"`
	Type                 string            `json:"type"`
	Public               bool              `json:"public"`
	ToolSpec             `json:",inline"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Artifact registry create result
type ArtifactRegistryCreateResult struct {
	metav1.TypeMeta `json:",inline"`
}

var _ ToolInterface = &ArtifactRegistry{}

func (ar *ArtifactRegistry) GetKind() string {
	return TypeArtifactRegistry
}

func (ar *ArtifactRegistry) GetHostPort() HostPort {
	return ar.Spec.HTTP
}

func (ar *ArtifactRegistry) GetKindType() string {
	return ar.Spec.Type
}

func (ar *ArtifactRegistry) GetObjectMeta() metav1.Object {
	return &ar.ObjectMeta
}

func (ar *ArtifactRegistry) GetStatus() *ServiceStatus {
	return &ar.Status
}

func (ar *ArtifactRegistry) GetToolSpec() ToolSpec {
	return ar.Spec.ToolSpec
}
