package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type ToolCategory struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the Deployment.
	// +optional
	Spec ToolCategorySpec `json:"spec"`

	// Most recently observed status of the Deployment.
	// +optional
	Status ToolCategoryStatus `json:"status"`
}

type ToolCategorySpec struct {
	DisplayName ToolCategorySpecDisplayName `json:"displayName"`
	Enabled     bool                        `json:"enabled"`
	Kind        string                      `json:"kind"`
}

type ToolCategoryStatus struct {
	Phase string
}

type ToolCategorySpecDisplayName struct {
	En string `json:"en"`
	Zh string `json:"zh"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ToolCategoryList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []ToolCategory `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type SettingResp struct {
	// +optional
	metav1.TypeMeta `json:",inline"`
	DefaultDomain   string             `json:"defaultDomain"`
	Integrations    string             `json:"integrations"`
	Portal_link     string             `json:"portal_link"`
	ToolChains      []ToolCategoryResp `json:"toolChains"`
	VersionGate     string             `json:"versionGate"`
}

type ToolCategoryResp struct {
	Name        string                      `json:"name"`
	DisplayName ToolCategorySpecDisplayName `json:"displayName"`
	Enabled     bool                        `json:"enabled"`
	ApiPath     string                      `json:"apiPath"`
	Items       []ToolTypeResp              `json:"items"`
}

type ToolTypeResp struct {
	DisplayName ToolTypeSpecDisplayName `json:"displayName"`
	// +optional
	Description ToolTypeSpecDisplayName `json:"description"`
	// +optional
	RecommendedVersion   string                             `json:"recommendedVersion"`
	Host                 string                             `json:"host"`
	Html                 string                             `json:"html"`
	Name                 string                             `json:"name"`
	Kind                 string                             `json:"kind"`
	Type                 string                             `json:"type"`
	Public               bool                               `json:"public"`
	Enterprise           bool                               `json:"enterprise"`
	Enabled              bool                               `json:"enabled"`
	ApiPath              string                             `json:"apiPath"`
	SupportedSecretTypes []ToolTypeSpecSupportedSecretTypes `json:"supportedSecretTypes"`
	RoleSyncEnabled      bool                               `json:"roleSyncEnabled"`
	FeatureGate          string                             `json:"featureGate"`
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type Setting struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the Deployment.
	// +optional
	Spec SettingSpec `json:"spec"`

	// Most recently observed status of the Deployment.
	// +optional
	Status ToolCategoryStatus `json:"status"`
}

type SettingSpec struct {
	DefaultDomain string `json:"defaultDomain"`
	VersionGate   string `json:"versionGate"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type SettingList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []Setting `json:"items"`
}

func (in *ToolCategory) Merge(newT *ToolCategory) (result *ToolCategory, err error) {

	in.Spec.DisplayName = newT.Spec.DisplayName
	in.Spec.Kind = newT.Spec.Kind

	result = in

	return result, err
}
