/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// DocumentManagementBinding is the binding referenced to DocumentManagement
type DocumentManagementBinding struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the DocumentManagementBinding.
	// +optional
	Spec DocumentManagementBindingSpec `json:"spec"`
	// Most recently observed status of the DocumentManagementBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

func (b *DocumentManagementBinding) GetSecretNamespace() string {
	namespace := b.GetNamespace()
	if b.Spec.Secret.Namespace != "" {
		namespace = b.Spec.Secret.Namespace
	}
	return namespace
}

func (b *DocumentManagementBinding) GetSecretName() string {
	return b.Spec.Secret.Name
}

// DocumentManagementBindingSpec is the spec in DocumentManagementBinding
type DocumentManagementBindingSpec struct {
	DocumentManagement LocalObjectReference `json:"documentManagement"`
	//Secret defines the secret type.
	//+optional
	Secret SecretKeySetRef `json:"secret"`
	//+optional
	DocumentManagementSpaceRefs []DocumentManagementSpaceRef `json:"documentManagementSpaceRefs"`
}

//DocumentManagementSpaceRef define the documentmanagespace reference
type DocumentManagementSpaceRef struct {
	Name string `json:"name"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// DocumentManagementBindingList is a list of DocumentManagementBinding objects.
type DocumentManagementBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta

	// Items is a list of DocumentManagementBinding
	Items []DocumentManagementBinding `json:"items"`
}

var _ ToolBindingLister = &DocumentManagementBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *DocumentManagementBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta.Kind = TypeDocumentManagementBinding
	}
	return
}

func (binding *DocumentManagementBinding) GetStatus() *ServiceStatus {
	return &binding.Status
}
