package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// StatusAccessor get status of object
type StatusAccessor interface {
	GetStatus() *ServiceStatus
}

// ToolInterface is abstract of all tools
type ToolInterface interface {
	// GetHostPort returen field of Http in Tool, will abandon by GetToolSpec
	GetHostPort() HostPort
	// Object's TypeMeta is empty
	// https://github.com/kubernetes/client-go/issues/413
	// https://github.com/kubernetes/kubernetes/pull/63972
	// GetTypeMeta() metav1.TypeMeta

	GetKind() string
	// Get SubType of tool
	GetKindType() string
	StatusAccessor
	metav1.ObjectMetaAccessor
	GetToolSpec() ToolSpec
}

// ToolSpec contains custom field in tool spec
type ToolSpec struct {
	HTTP HostPort `json:"http"`
	// +optional
	Secret SecretKeySetRef `json:"secret"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CreateProjectOptions work for create projectmanagement project
type CreateProjectOptions struct {
	// +optional
	metav1.ObjectMeta `json:"metadata"`
	metav1.TypeMeta   `json:",inline"`
	SecretName        string            `json:"secretname"`
	Namespace         string            `json:"namespace"`
	Name              string            `json:"name"`
	IsRemote          bool              `json:"is_remote"`
	Data              map[string]string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ListProjectOptions list projects
type ListProjectOptions struct {
	metav1.TypeMeta `json:",inline"`
	SecretName      string `json:"secretname"`
	Namespace       string `json:"namespace"`
	Page            string `json:"page"`
	PageSize        string `json:"pagesize"`
	Filter          string `json:"filter"`
	IsRemote        bool   `json:"is_remote"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectData return projectmanagement project Info
type ProjectData struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`
	Data              map[string]string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectDataList  fetch project infor
type ProjectDataList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ProjectData `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ToolBinding one ToolBinding data
type ToolBinding struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`
	Data              map[string]string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ToolBindingMap a map of ToolBindings
type ToolBindingMap struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`
	Items             []ToolBinding `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ToolBindingList tool binding list
type ToolBindingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ToolBindingMap `json:"items,omitempty"`
}

// ToolBindingLister lists tool bindings
type ToolBindingLister interface {
	GetItems() []ToolBinding
}

// NewToolBinding creates a new tool binding based on a metav1.Object
func NewToolBinding(obj metav1.Object) (binding ToolBinding) {
	binding.ObjectMeta = newObjectMeta(obj)
	return
}

func newObjectMeta(obj metav1.Object) (meta metav1.ObjectMeta) {
	meta.Name = obj.GetName()
	meta.Namespace = obj.GetNamespace()
	meta.Initializers = obj.GetInitializers()
	meta.Labels = obj.GetLabels()
	meta.OwnerReferences = obj.GetOwnerReferences()
	meta.ResourceVersion = obj.GetResourceVersion()
	meta.SelfLink = obj.GetSelfLink()
	meta.UID = obj.GetUID()
	meta.Annotations = obj.GetAnnotations()
	meta.Generation = obj.GetGeneration()
	meta.ClusterName = obj.GetClusterName()
	return
}
