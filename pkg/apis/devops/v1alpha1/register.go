/*
Copyright 2016 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

// GroupName API group name
const GroupName = "devops.alauda.io"
const Version = "v1alpha1"

// SchemeGroupVersion is group version used to register these objects
var SchemeGroupVersion = schema.GroupVersion{Group: GroupName, Version: Version}

var (
	// SchemeBuilder helps toe build the scheme for the group
	// TODO: move SchemeBuilder with zz_generated.deepcopy.go to k8s.io/api.
	// localSchemeBuilder and AddToScheme will stay in k8s.io/kubernetes.
	SchemeBuilder      runtime.SchemeBuilder
	localSchemeBuilder = &SchemeBuilder
	// AddToScheme function from the localSchemeBuilder to add types to scheme
	AddToScheme = localSchemeBuilder.AddToScheme
)

func init() {
	// We only register manually written functions here. The registration of the
	// generated functions takes place in the generated files. The separation
	// makes the code compile even when the generated files are missing.
	localSchemeBuilder.Register(addKnownTypes)
}

// Adds the list of known types to the given scheme.
func addKnownTypes(scheme *runtime.Scheme) error {
	scheme.AddKnownTypes(SchemeGroupVersion,
		&Jenkins{},
		&JenkinsList{},
		&JenkinsBinding{},
		&JenkinsBindingList{},
		&JenkinsBindingProxyOptions{},
		&JenkinsBindingProxyResult{},
		&JenkinsBindingInfoOptions{},
		&JenkinsBindingInfoResult{},
		&PipelineConfig{},
		&PipelineConfigList{},
		&PipelineConfigScanOptions{},
		&PipelineConfigScanResult{},
		&PipelineConfigLogOptions{},
		&PipelineConfigLog{},
		&PipelineInputOptions{},
		&PipelineInputResponse{},
		&JenkinsfilePreviewOptions{},
		&JenkinsfilePreview{},
		&Pipeline{},
		&PipelineList{},
		&PipelineLogOptions{},
		&PipelineLog{},
		&PipelineTaskOptions{},
		&PipelineTask{},
		&PipelineTestReportOptions{},
		&PipelineTestReport{},
		&PipelineTaskTemplate{},
		&PipelineTaskTemplateList{},
		&PipelineViewResult{},
		&PipelineTemplate{},
		&PipelineTemplateList{},
		&PipelineTemplateSync{},
		&PipelineTemplateSyncList{},
		&ExportShowOptions{},
		&PipelineExportedVariables{},
		&ClusterPipelineTemplate{},
		&ClusterPipelineTemplateList{},
		&ClusterPipelineTaskTemplate{},
		&ClusterPipelineTaskTemplateList{},
		&ClusterPipelineTemplateSync{},
		&ClusterPipelineTemplateSyncList{},
		&CodeRepoService{},
		&CodeRepoServiceList{},
		&CodeRepoBinding{},
		&CodeRepoBindingList{},
		&CodeRepository{},
		&CodeRepositoryList{},
		&CodeRepoBindingRepositories{},
		&CodeRepoBindingRepositoryOptions{},
		&ImageRegistry{},
		&ImageRegistryList{},
		&ImageRegistryBinding{},
		&ImageRegistryBindingList{},
		&ImageRepository{},
		&ImageRepositoryList{},
		&ImageRegistryBindingRepositories{},
		&ImageRegistryBindingRepositoryOptions{},
		&ImageResult{},
		&ImageScanOptions{},
		&ImageRepositoryOptions{},
		&ImageRepositoryRemoteStatus{},
		&ImageRepositoryLink{},
		&Vulnerability{},
		&VulnerabilityList{},
		&ImageTagResult{},
		&ImageTagOptions{},
		&CodeRepoServiceAuthorizeResponse{},
		&CodeRepoServiceAuthorizeOptions{},
		&CodeRepoServiceAuthorizeCreate{},
		&ProjectManagement{},
		&ProjectManagementList{},
		&ProjectManagementBinding{},
		&ProjectManagementBindingList{},
		&IssueSearchOptions{},
		&ListProjectOptions{},
		&IssueFilterDataList{},
		&IssueDetailList{},
		&ListIssuesOptions{},
		&IssueDetail{},
		&IssueKeyOptions{},
		&CreateProjectOptions{},
		&ProjectDataList{},
		&ProjectmanagementUser{},
		&ProjectData{},
		&UserSearchOptions{},
		&DocumentManagement{},
		&DocumentManagementList{},
		&DocumentManagementBinding{},
		&DocumentManagementBindingList{},
		&TestTool{},
		&TestToolList{},
		//&TestToolBinding{},
		//&TestToolBindingList{},
		&CodeQualityTool{},
		&CodeQualityToolList{},
		&ToolBindingReplica{},
		&ToolBindingReplicaList{},
		&CodeQualityBinding{},
		&CodeQualityBindingList{},
		&CodeQualityProject{},
		&CodeQualityProjectList{},
		&CorrespondCodeQualityProjectsOptions{},
		&CodeQualityProjectOptions{},
		&CodeQualityProjectLastAnalysisDate{},
		&CodeQualityProjectRemoteStatus{},
		&CodeQualityProjectReportsCondition{},
		&CodeRepoBranchOptions{},
		&CodeRepoBranchResult{},
		&RoleMapping{},
		&RoleMappingListOptions{},
		&ToolBinding{},
		&ToolBindingList{},
		&ArtifactRegistryManager{},
		&ArtifactRegistryManagerList{},
		&ArtifactRegistry{},
		&ArtifactRegistryList{},
		&ArtifactRegistryBinding{},
		&ArtifactRegistryBindingList{},
		&ArtifactRegistryCreateOptions{},
		&ArtifactRegistryCreateResult{},
		&ArtifactRegistryManagerOptions{},
		&BlobStoreOption{},
		&BlobStoreOptionList{},
		&ArtifactRegistryOption{},
		&ToolCategory{},
		&ToolCategoryList{},
		&ToolType{},
		&ToolTypeList{},
		&SettingResp{},
		&Setting{},
		&SettingList{},
		&ArtifactOption{},
		&PipelineArtifactList{},
		&DownloadOption{},
	)
	metav1.AddToGroupVersion(scheme, SchemeGroupVersion)
	return nil
}

// Resource takes an unqualified resource and returns a Group qualified GroupResource
func Resource(resource string) schema.GroupResource {
	return SchemeGroupVersion.WithResource(resource).GroupResource()
}
