package v1alpha1

import (
	"net/http"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +genclient:method=Proxy,verb=create,subresource=proxy,input=JenkinsBindingProxyOptions,result=JenkinsBindingProxyResult
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsBinding struct holds a reference to a specific jenkins object
// and some user data for access
type JenkinsBinding struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// Specification of the desired behavior of the JenkinsBinding.
	// +optional
	Spec JenkinsBindingSpec `json:"spec,omitempty"`
	// Most recently observed status of the JenkinsBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status JenkinsBindingStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsBindingProxyOptions proxy interface payload
type JenkinsBindingProxyOptions struct {
	metav1.TypeMeta `json:",inline"`

	// Method http request method
	Method string `json:"method"`
	// Header http header
	// +optional
	Header http.Header `json:"header"`
	// Payload http request payload
	// +optional
	Payload string `json:"payload"`
	// URL http request url
	URL string `json:"url"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsBindingProxyResult proxy interface response
type JenkinsBindingProxyResult struct {
	metav1.TypeMeta `json:",inline"`

	// Code http response status code
	Code int
	// Header http response header
	Header http.Header
	// Data http response
	Data string
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsBindingInfoOptions info interface payload
type JenkinsBindingInfoOptions struct {
	metav1.TypeMeta `json:",inline"`

	Target string `json:"target"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsBindingInfoResult info interface response
type JenkinsBindingInfoResult struct {
	metav1.TypeMeta `json:",inline"`

	Result string `json:"result"`
}

func (b *JenkinsBinding) GetSecretNamespace() string {
	namespace := b.GetNamespace()
	if b.Spec.Account.Secret.Namespace != "" {
		namespace = b.Spec.Account.Secret.Namespace
	}
	return namespace
}

func (b *JenkinsBinding) GetSecretName() string {
	return b.Spec.Account.Secret.Name
}

// JenkinsBindingSpec defines JenkinsBinding's specs
type JenkinsBindingSpec struct {
	// Jenkins is the jenkins defined in the binding.
	// +optional
	Jenkins JenkinsInstance `json:"jenkins"`
	// Account is the account to access the jenkins.
	// +optional
	Account UserAccount `json:"account"`
}

// JenkinsInstance defines a Jenkins instance for JenkinsBinding
type JenkinsInstance struct {
	// Name is the name of the jenkins.
	// +optional
	Name string `json:"name"`
}

// UserAccount user account data for access
// currently only suports a secret as storage
type UserAccount struct {
	// Secret is the secret of the account.
	// +optional
	Secret SecretKeySetRef `json:"secret"`
}

// JenkinsBindingStatus defines JenkinsBinding' status
type JenkinsBindingStatus struct {
	// ServiceStatus defines the common status
	// +optional
	ServiceStatus `json:",inline"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsBindingList is a list of JenkinsBinding objects.
type JenkinsBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of JenkinsBinding objects.
	Items []JenkinsBinding `json:"items"`
}

var _ ToolBindingLister = &JenkinsBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *JenkinsBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta.Kind = TypeJenkinsBinding
	}
	return
}

func (binding *JenkinsBinding) GetStatus() *ServiceStatus {
	return &binding.Status.ServiceStatus
}
