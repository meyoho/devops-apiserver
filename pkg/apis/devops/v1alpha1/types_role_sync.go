package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// RoleMapping role mapping by project
type RoleMapping struct {
	metav1.TypeMeta `json:",inline"`

	// Spec operations for user roles
	Spec []ProjectUserRoleOperation `json:"spec"`
}

// ContainsProject returns true if the project name is present
func (opts *RoleMappingListOptions) ContainsProject(project string) (result bool) {
	if len(opts.Projects) > 0 {
		for _, proj := range opts.Projects {
			if proj == project {
				result = true
				break
			}
		}
	}
	return
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// RoleMappingListOptions options for listing
type RoleMappingListOptions struct {
	metav1.TypeMeta `json:",inline"`

	// Projects list of projects for tool to list user/roles
	// +optional
	Projects []string `json:"projects"`
}

// ProjectUserRoleOperation operation of user/role set on a specific project
type ProjectUserRoleOperation struct {
	// Project project data
	Project ProjectData `json:"project"`
	// UserRoleOperations operations on users' roles
	UserRoleOperations []UserRoleOperation `json:"userRoleOperations"`
}

// UserRoleOperation specific user/role/operation instruction
type UserRoleOperation struct {
	// User user data
	User UserMeta `json:"user"`
	// Role role data
	Role RoleMeta `json:"role"`
	// Operation operation on role/user
	// +optional
	Operation RoleOperation `json:"operation"`
}

// RoleOperation role operation on a user
type RoleOperation string

const (
	// UserRoleOperationAdd adds a role to a user
	UserRoleOperationAdd = "add"
	// UserRoleOperationRemove remove a role from a user
	UserRoleOperationRemove = "remove"
	// UserRoleOperationUpdate updates a role assignment on a user
	UserRoleOperationUpdate = "update"
)

// UserMeta metadata for user
type UserMeta struct {
	// Objects custom data
	// +optional
	metav1.ObjectMeta `json:"metadata"`
	// Username user's username
	// +optional
	Username string `json:"username"`
	// Email user's email address
	// +optional
	Email string `json:"email"`
}

// RoleMeta metadata for user
type RoleMeta struct {
	// Objects custom data
	// +optional
	metav1.ObjectMeta `json:"metadata"`
	// Name role's name
	// +optional
	Name string `json:"name"`
	// Custom custom permission set scheme
	// +optional
	Custom map[string]string `json:"custom"`
}
