/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// +k8s:deepcopy-gen=package
// +k8s:conversion-gen=alauda.io/devops-apiserver/pkg/apis/devops
// +k8s:openapi-gen=true
// +k8s:defaulter-gen=TypeMeta
//go:generate go run ../../../../main.go gen tool -o zz_generated.toolchain.go -p alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1
//go:generate go fmt zz_generated.toolchain.go

// +alauda:rolesync-gen:class=platform,name=acp,sync=rbac,enabled=true,roles=project_admin:alauda_project_admin;project_auditor:alauda_project_auditor;namespace_admin:alauda_namespace_admin;namespace_developer:alauda_namespace_developer;namespace_auditor:alauda_namespace_auditor;space_admin:alauda_space_admin;space_developer:alauda_space_developer;space_auditor:alauda_space_auditor
// +alauda:rolesync-gen:class=platform,name=ace,source=true,sync=rbac,enabled=true,roles=project_admin:project_admin;project_auditor:project_auditor;namespace_admin:namespace_admin;namespace_developer:namespace_developer;namespace_auditor:namespace_auditor;space_admin:space_admin;space_developer:space_developer;space_auditor:space_auditor
// +alauda:rolesync-gen:class=priority,name=project_admin,priority=0
// +alauda:rolesync-gen:class=priority,name=project_auditor,priority=100
// +alauda:rolesync-gen:class=priority,name=space_admin,priority=10
// +alauda:rolesync-gen:class=priority,name=space_developer,priority=11
// +alauda:rolesync-gen:class=priority,name=space_auditor,priority=12
// +alauda:rolesync-gen:class=priority,name=namespace_admin,priority=20
// +alauda:rolesync-gen:class=priority,name=namespace_developer,priority=21
// +alauda:rolesync-gen:class=priority,name=namespace_auditor,priority=22
//go:generate go run ../../../../main.go gen role-sync -o zz_generated.rolesync.go -p alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1
//go:generate go fmt zz_generated.rolesync.go

// Package v1alpha1 is the v1alpha1 version of the API.
// +groupName=devops.alauda.io
package v1alpha1
