package v1alpha1

import (
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ImageRepoPhase string

const (
	// ImageRepoPhaseCreating registry service binding phase is creating
	ImageRepoPhaseCreating ImageRepoPhase = StatusCreating
	// ImageRepoPhaseReady registry service binding phase is ready
	ImageRepoPhaseReady ImageRepoPhase = StatusReady
	// ImageRepoPhaseListRepoError could not list repo
	ImageRepoPhaseListRepoError ImageRepoPhase = StatusListRepoError
	// ImageRepoPhaseError connect registry service error
	ImageRepoPhaseError ImageRepoPhase = StatusError
	// ImageRepoPhaseWaitingToDelete will delete
	ImageRepoPhaseWaitingToDelete ImageRepoPhase = StatusWaitingToDelete
)

type ImageTagScanStatus string

const (
	ImageTagScanStatusNotScan   ImageTagScanStatus = "notScan"
	ImageTagScanStatusPending   ImageTagScanStatus = "pending"
	ImageTagScanStatusAnalyzing ImageTagScanStatus = "analyzing"
	ImageTagScanStatusFinished  ImageTagScanStatus = "finished"
	ImageTagScanStatusError     ImageTagScanStatus = "error"
)

// +genclient
// +genclient:method=ScanImage,verb=create,subresource=security,input=ImageScanOptions,result=ImageResult
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRepository save image info, include image path and tag info
type ImageRepository struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the ImageRepository.
	// +optional
	Spec ImageRepositorySpec `json:"spec"`
	// Most recently observed status of the ImageRepository.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ImageRepositoryStatus `json:"status"`
}

// ImageRepositorySpec represents ImageRepository's specs
type ImageRepositorySpec struct {
	// ImageRegistry defines the imageRegistry in spec
	ImageRegistry LocalObjectReference `json:"imageRegistry"`
	// ImageRegistryBinding defines the imageRegistryBinding in spec
	ImageRegistryBinding LocalObjectReference `json:"imageRegistryBinding"`
	// CodeRepoBinding defines the image in spec
	Image string `json:"image"`
}

// ImageRepositoryStatus defines the status of the service, inherit ServiceStatus
type ImageRepositoryStatus struct {
	// ServiceStatus defines the common status
	// +optional
	ServiceStatus `json:",inline"`
	// Tags defines the tags in the image repository
	// +optional
	Tags []ImageTag `json:"tags"`
	// LatestTag defines the latest tag pushed
	// +optional
	LatestTag ImageTag `json:"latestTag"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// ImageResult
type ImageResult struct {
	metav1.TypeMeta `json:",inline"`

	StatusCode int    `json:"status_code"`
	Message    string `json:"message"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// ImageTagResult
type ImageTagResult struct {
	metav1.TypeMeta `json:",inline"`
	Tags            []ImageTag `json:"tags"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// ImageTagOptions
type ImageTagOptions struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	SortBy string `json:"sortBy"`
	// +optional
	SortMode string `json:"sortMode"`
}

// ImageTag defines the image tag attributes
type ImageTag struct {
	// Name defines the tag name
	// +optional
	Name string `json:"name"`
	// Digest defines the tag digest
	// +optional
	Digest string `json:"digest"`
	// Author defines the tag author
	// Support for Harbor
	// +optional
	Author string `json:"author"`
	// CreatedAt defines the tag created_at
	// +optional
	CreatedAt *metav1.Time `json:"created_at"`
	// UpdatedAt defines the completion time of Harbor scan tag
	// Support for Harbor
	// +optional
	UpdatedAt  *metav1.Time       `json:"updated_at"`
	Size       string             `json:"size"`
	Level      int                `json:"level"`
	ScanStatus ImageTagScanStatus `json:"scanStatus"`
	Message    string             `json:"message"`
	// +optional
	Summary []Summary `json:"summary"`
}

// Summary defines the Severity Count
type Summary struct {
	Severity int `json:"severity"`
	Count    int `json:"count"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// Vulnerability
type Vulnerability struct {
	metav1.TypeMeta `json:",inline"`

	ID          string `json:"id"`
	Severity    int    `json:"severity"`
	Package     string `json:"package"`
	Version     string `json:"version"`
	Description string `json:"description"`
	Link        string `json:"link"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// VulnerabilityList
type VulnerabilityList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []Vulnerability
}

// GetCreatedAt get tag created_at
func (i *ImageTag) GetCreatedAt() time.Time {
	if i.CreatedAt != nil {
		return i.CreatedAt.Time
	}
	return time.Time{}
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRepositoryList is a list of ImageRepository objects.
type ImageRepositoryList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`
	// Items contains all imagerepository in this list
	Items []ImageRepository `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageScanOptions user to fetch scan image options
type ImageScanOptions struct {
	metav1.TypeMeta `json:",inline"`

	Tag string `json:"tag"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// ImageRepositoryOptions presets image repository options
type ImageRepositoryOptions struct {
	metav1.TypeMeta `json:",inline"`
	Image           string `json:"image"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ImageRepositoryRemoteStatus struct {
	metav1.TypeMeta `json:",inline"`
	Status          *HostPortStatus `json:"status"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ImageRepositoryLink struct {
	metav1.TypeMeta `json:",inline"`
	Link            string `json:"link"`
}

// GetRegistryName get service name
func (c *ImageRepository) GetRegistryName() string {
	repoLabels := c.GetLabels()
	if repoLabels == nil {
		return ""
	}

	if serviceName, ok := repoLabels[LabelImageRegistry]; ok {
		return serviceName
	}
	return ""
}

// endregion

var _ StatusAccessor = &ImageRepository{}

// GetStatus implements StatusAccessor
func (c *ImageRepository) GetStatus() *ServiceStatus {
	return &c.Status.ServiceStatus
}
