package v1alpha1

import (
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// artifactRegistryBinding
type ArtifactRegistryBinding struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	Spec ArtifactRegistryBindingSpec `json:"spec"`
	// Read-only
	// +optional
	Status ServiceStatus `json:"status"`
}

func (arb *ArtifactRegistryBinding) GetSecretNamespace() string {
	namespace := arb.GetNamespace()
	if arb.Spec.Secret.Namespace != "" {
		namespace = arb.Spec.Secret.Namespace
	}
	return namespace
}

func (b *ArtifactRegistryBinding) GetSecretName() string {
	return b.Spec.Secret.Name
}

func (b *ArtifactRegistryBinding) GetDockerCfgSecretName() string {
	if b.Spec.Secret.Name == "" {
		return ""
	}
	return fmt.Sprintf("dockercfg--%s--%s", b.Namespace, b.Name)
}

// ArtifactRegistryBindingSpec represents ArtifactRegistryBinding specs
type ArtifactRegistryBindingSpec struct {
	ArtifactRegistry LocalObjectReference `json:"artifactRegistry"`
	// +optional
	Secret SecretKeySetRef `json:"secret"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ArtifactRegistryBindingList is a list of ArtifactRegistryBinding objects.
type ArtifactRegistryBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []ArtifactRegistryBinding `json:"items"`
}

var _ ToolBindingLister = &ArtifactRegistryBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *ArtifactRegistryBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta.Kind = TypeArtifactRegistryBinding
	}
	return
}

var _ StatusAccessor = &ArtifactRegistryBinding{}

// GetStatus implements StatusAccessor
func (arb *ArtifactRegistryBinding) GetStatus() *ServiceStatus {
	return &arb.Status
}
