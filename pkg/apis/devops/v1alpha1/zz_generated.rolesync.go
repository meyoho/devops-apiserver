// ATTENTION!
// This file was autogenerated by pkg/cmd/gen_role_sync.go Do not edit it manually!
package v1alpha1

import (
	"alauda.io/devops-apiserver/pkg/role"
)

// RoleSyncScheme public global variable for schema
var RoleSyncScheme = GetRoleSyncScheme()

// GetRoleSyncScheme get role sync schema
func GetRoleSyncScheme() *role.Scheme {
	scheme := role.NewScheme()
	scheme.SetSource("ace")

	// Priorities
	scheme.AddPriority(
		// 0: namespace_admin
		role.Priority{
			Name:     "namespace_admin",
			Priority: 20,
		},
		// 1: namespace_auditor
		role.Priority{
			Name:     "namespace_auditor",
			Priority: 22,
		},
		// 2: namespace_developer
		role.Priority{
			Name:     "namespace_developer",
			Priority: 21,
		},
		// 3: project_admin
		role.Priority{
			Name:     "project_admin",
			Priority: 0,
		},
		// 4: project_auditor
		role.Priority{
			Name:     "project_auditor",
			Priority: 100,
		},
		// 5: space_admin
		role.Priority{
			Name:     "space_admin",
			Priority: 10,
		},
		// 6: space_auditor
		role.Priority{
			Name:     "space_auditor",
			Priority: 12,
		},
		// 7: space_developer
		role.Priority{
			Name:     "space_developer",
			Priority: 11,
		},
	)

	// Platforms
	scheme.AddPlatform(
		// 0: Bitbucket
		role.NewPlatform("Bitbucket").
			SetSyncTypeString("rbac-mutex").
			SetEnabled(true).
			SetRoleMapping(map[string]string{
				"namespace_admin":     "Developer",
				"namespace_auditor":   "Developer",
				"namespace_developer": "Developer",
				"project_admin":       "Administrator",
				"project_auditor":     "Developer",
				"space_admin":         "Developer",
				"space_auditor":       "Developer",
				"space_developer":     "Developer",
			}).
			SetCustom(map[string]map[string]string{}),
		// 1: Gitee
		role.NewPlatform("Gitee").
			SetSyncTypeString("rbac-mutex").
			SetEnabled(true).
			SetRoleMapping(map[string]string{
				"namespace_admin":     "Developer",
				"namespace_auditor":   "Developer",
				"namespace_developer": "Developer",
				"project_admin":       "Admin",
				"project_auditor":     "Developer",
				"space_admin":         "Developer",
				"space_auditor":       "Developer",
				"space_developer":     "Developer",
			}).
			SetCustom(map[string]map[string]string{}),
		// 2: Github
		role.NewPlatform("Github").
			SetSyncTypeString("rbac-mutex").
			SetEnabled(true).
			SetRoleMapping(map[string]string{
				"namespace_admin":     "Member",
				"namespace_auditor":   "Member",
				"namespace_developer": "Member",
				"project_admin":       "Owner",
				"project_auditor":     "Member",
				"space_admin":         "Member",
				"space_auditor":       "Member",
				"space_developer":     "Member",
			}).
			SetCustom(map[string]map[string]string{}),
		// 3: Gitlab
		role.NewPlatform("Gitlab").
			SetSyncTypeString("rbac-mutex").
			SetEnabled(true).
			SetRoleMapping(map[string]string{
				"namespace_admin":     "Master",
				"namespace_auditor":   "Reporter",
				"namespace_developer": "Developer",
				"project_admin":       "Owner",
				"project_auditor":     "Guest",
				"space_admin":         "Master",
				"space_auditor":       "Reporter",
				"space_developer":     "Developer",
			}).
			SetCustom(map[string]map[string]string{}),
		// 4: Harbor
		role.NewPlatform("Harbor").
			SetSyncTypeString("rbac-mutex").
			SetEnabled(true).
			SetRoleMapping(map[string]string{
				"namespace_admin":     "developer",
				"namespace_auditor":   "guest",
				"namespace_developer": "developer",
				"project_admin":       "projectAdmin",
				"project_auditor":     "guest",
				"space_admin":         "developer",
				"space_auditor":       "guest",
				"space_developer":     "developer",
			}).
			SetCustom(map[string]map[string]string{}),
		// 5: Jira
		role.NewPlatform("Jira").
			SetSyncTypeString("rbac").
			SetEnabled(true).
			SetRoleMapping(map[string]string{
				"namespace_admin":     "AlaudaSpaceAdmin",
				"namespace_auditor":   "AlaudaSpaceAuditor",
				"namespace_developer": "AlaudaSpaceDeveloper",
				"project_admin":       "AlaudaProjectAdmin",
				"project_auditor":     "AlaudaProjectAuditor",
				"space_admin":         "AlaudaSpaceAdmin",
				"space_auditor":       "AlaudaSpaceAuditor",
				"space_developer":     "AlaudaSpaceDeveloper",
			}).
			SetCustom(map[string]map[string]string{}),
		// 6: Maven2
		role.NewPlatform("Maven2").
			SetSyncTypeString("rbac").
			SetEnabled(true).
			SetRoleMapping(map[string]string{
				"namespace_admin":     "admin",
				"namespace_auditor":   "developer",
				"namespace_developer": "developer",
				"project_admin":       "admin",
				"project_auditor":     "developer",
				"space_admin":         "admin",
				"space_auditor":       "developer",
				"space_developer":     "developer",
			}).
			SetCustom(map[string]map[string]string{
				"admin": map[string]string{
					"admin": "*",
					"view":  "*",
				},
				"developer": map[string]string{
					"view": "*",
				},
			}),
		// 7: Sonarqube
		role.NewPlatform("Sonarqube").
			SetSyncTypeString("rbac").
			SetEnabled(true).
			SetRoleMapping(map[string]string{
				"namespace_admin":     "admin",
				"namespace_auditor":   "user",
				"namespace_developer": "user",
				"project_admin":       "admin",
				"project_auditor":     "user",
				"space_admin":         "admin",
				"space_auditor":       "user",
				"space_developer":     "user",
			}).
			SetCustom(map[string]map[string]string{
				"namespace_admin": map[string]string{
					"admin":      "true",
					"codeviewer": "true",
					"issueadmin": "true",
					"scan":       "true",
					"user":       "true",
				},
				"namespace_auditor": map[string]string{
					"admin":      "false",
					"codeviewer": "true",
					"issueadmin": "false",
					"scan":       "true",
					"user":       "true",
				},
				"namespace_developer": map[string]string{
					"admin":      "false",
					"codeviewer": "true",
					"issueadmin": "false",
					"scan":       "true",
					"user":       "true",
				},
				"project_admin": map[string]string{
					"admin":      "true",
					"codeviewer": "true",
					"issueadmin": "true",
					"scan":       "true",
					"user":       "true",
				},
				"project_auditor": map[string]string{
					"admin":      "false",
					"codeviewer": "true",
					"issueadmin": "false",
					"scan":       "true",
					"user":       "true",
				},
				"space_admin": map[string]string{
					"admin":      "true",
					"codeviewer": "true",
					"issueadmin": "true",
					"scan":       "true",
					"user":       "true",
				},
				"space_auditor": map[string]string{
					"admin":      "false",
					"codeviewer": "true",
					"issueadmin": "false",
					"scan":       "true",
					"user":       "true",
				},
				"space_developer": map[string]string{
					"admin":      "false",
					"codeviewer": "true",
					"issueadmin": "false",
					"scan":       "true",
					"user":       "true",
				},
			}),
		// 8: ace
		role.NewPlatform("ace").
			SetSyncTypeString("rbac").
			SetEnabled(true).
			SetRoleMapping(map[string]string{
				"namespace_admin":     "namespace_admin",
				"namespace_auditor":   "namespace_auditor",
				"namespace_developer": "namespace_developer",
				"project_admin":       "project_admin",
				"project_auditor":     "project_auditor",
				"space_admin":         "space_admin",
				"space_auditor":       "space_auditor",
				"space_developer":     "space_developer",
			}).
			SetCustom(map[string]map[string]string{}),
		// 9: acp
		role.NewPlatform("acp").
			SetSyncTypeString("rbac").
			SetEnabled(true).
			SetRoleMapping(map[string]string{
				"namespace_admin":     "alauda_namespace_admin",
				"namespace_auditor":   "alauda_namespace_auditor",
				"namespace_developer": "alauda_namespace_developer",
				"project_admin":       "alauda_project_admin",
				"project_auditor":     "alauda_project_auditor",
				"space_admin":         "alauda_space_admin",
				"space_auditor":       "alauda_space_auditor",
				"space_developer":     "alauda_space_developer",
			}).
			SetCustom(map[string]map[string]string{}),
	)

	return scheme
}
