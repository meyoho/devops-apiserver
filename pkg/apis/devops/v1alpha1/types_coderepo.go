package v1alpha1

import (
	"fmt"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// region CodeRepository

// CodeRepoServiceType type for the repo service
type CodeRepoServiceType string

const (
	// CodeRepoServiceTypeGithub github
	// +alauda:toolchain-gen:class=item,category=codeRepository,name=github,en=Github,zh=Github,apipath=codereposervices,enabled=true,kind=codereposervice,type=Github,api=https://api.github.com,web=https://github.com,public=true,roleSyncEnabled=false
	// +alauda:toolchain-gen:class=secret,category=codeRepository,itemName=github,type=devops.alauda.io/oauth2,en=Input respectively OAuth App's Client ID and Client Secret,zh=分别为设置页面 OAuth Apps 的 Client ID 和 Client Secret
	// +alauda:toolchain-gen:class=secret,category=codeRepository,itemName=github,type=kubernetes.io/basic-auth,en=Username is same as used on login and Password is same as Personal Access Token generated in Settings page,zh=用户名输入登录时的用户名，密码为设置页面的 Personal Access Token
	// +alauda:rolesync-gen:class=platform,name=Github,sync=rbac-mutex,enabled=true,roles=project_admin:Owner;project_auditor:Member;namespace_admin:Member;namespace_developer:Member;namespace_auditor:Member;space_admin:Member;space_developer:Member;space_auditor:Member
	CodeRepoServiceTypeGithub CodeRepoServiceType = "Github"
	// CodeRepoServiceTypeGitlab gitlab
	// +alauda:toolchain-gen:class=item,category=codeRepository,name=gitlab,en=Gitlab,zh=Gitlab,apipath=codereposervices,enabled=true,kind=codereposervice,type=Gitlab,api=https://gitlab.com,web=https://gitlab.com,public=true,roleSyncEnabled=false
	// +alauda:toolchain-gen:class=item,category=codeRepository,name=gitlab-enterprise,en=Gitlab Enterprise,zh=Gitlab 企业版,apipath=codereposervices,enabled=true,kind=codereposervice,type=Gitlab,public=false,enterprise=true,roleSyncEnabled=true
	// +alauda:toolchain-gen:class=secret,category=codeRepository,itemName=gitlab+gitlab-enterprise,type=kubernetes.io/basic-auth,en=Username is same as used on login and Password is same as Personal Access Token generated in Settings page,zh=用户名输入登录时的用户名，密码为设置页面的 Personal Access Token
	// +alauda:toolchain-gen:class=secret,category=codeRepository,itemName=gitlab+gitlab-enterprise,type=devops.alauda.io/oauth2,en=Input respectively OAuth Application's Application ID in the Client ID field and Secret in the Client Secret,zh=分别为设置页面 Application 的 Application ID 和 Secret
	// +alauda:rolesync-gen:class=platform,name=Gitlab,sync=rbac-mutex,enabled=true,roles=project_admin:Owner;project_auditor:Guest;namespace_admin:Master;namespace_developer:Developer;namespace_auditor:Reporter;space_admin:Master;space_developer:Developer;space_auditor:Reporter
	CodeRepoServiceTypeGitlab CodeRepoServiceType = "Gitlab"
	// CodeRepoServiceTypeGitee gitee
	// +alauda:toolchain-gen:class=item,category=codeRepository,name=gitee,en=Gitee,zh=码云,apipath=codereposervices,enabled=true,kind=codereposervice,type=Gitee,api=https://gitee.com,web=https://gitee.com,public=true,roleSyncEnabled=false
	// +alauda:toolchain-gen:class=item,category=codeRepository,name=gitee-enterprise,en=Gitee Enterprise,zh=码云企业版,apipath=codereposervices,enabled=true,kind=codereposervice,type=Gitee,public=false,enterprise=true,roleSyncEnabled=false
	// +alauda:toolchain-gen:class=secret,category=codeRepository,itemName=gitee+gitee-enterprise,type=kubernetes.io/basic-auth,en=Username is same as used on login and Password is same as Personal Access Token generated in Settings page,zh=用户名输入登录时的用户名，密码为设置页面的私人令牌
	// +alauda:toolchain-gen:class=secret,category=codeRepository,itemName=gitee+gitee-enterprise,type=devops.alauda.io/oauth2,en=Input respectively OAuth Application's Application ID in the Client ID field and Secret in the Client Secret,zh=分别为设置页面应用的 Client ID 和 Client Secret
	// +alauda:rolesync-gen:class=platform,name=Gitee,sync=rbac-mutex,enabled=true,roles=project_admin:Admin;project_auditor:Developer;namespace_admin:Developer;namespace_developer:Developer;namespace_auditor:Developer;space_admin:Developer;space_developer:Developer;space_auditor:Developer
	CodeRepoServiceTypeGitee CodeRepoServiceType = "Gitee"
	// CodeRepoServiceTypeBitbucket bitbucket
	// +alauda:toolchain-gen:class=item,category=codeRepository,name=bitbucket,en=Bitbucket,zh=Bitbucket,apipath=codereposervices,enabled=true,kind=codereposervice,type=Bitbucket,api=https://api.bitbucket.org,web=https://bitbucket.org,public=true,roleSyncEnabled=false
	// +alauda:toolchain-gen:class=secret,category=codeRepository,itemName=bitbucket,type=kubernetes.io/basic-auth,en=Username is same as used on login and Password is same as App passwords generated in Settings page,zh=用户名输入登录时的用户名，密码为设置页面的 App passwords
	// +alauda:toolchain-gen:class=secret,category=codeRepository,itemName=bitbucket,type=devops.alauda.io/oauth2,en=Input respectively OAuth Consumer's Key in the Client ID field and Secret in the Client Secret field,zh=分别为设置页面 OAuth consumers 的 Key 和 Secret
	// +alauda:rolesync-gen:class=platform,name=Bitbucket,sync=rbac-mutex,enabled=true,roles=project_admin:Administrator;project_auditor:Developer;namespace_admin:Developer;namespace_developer:Developer;namespace_auditor:Developer;space_admin:Developer;space_developer:Developer;space_auditor:Developer
	CodeRepoServiceTypeBitbucket CodeRepoServiceType = "Bitbucket"
	// CodeRepoServiceTypeGogs gogs
	CodeRepoServiceTypeGogs CodeRepoServiceType = "Gogs"
	// CodeRepoServiceTypeGitea gitea
	CodeRepoServiceTypeGitea CodeRepoServiceType = "Gitea"
)

func (c CodeRepoServiceType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +alauda:toolchain-gen:class=category,name=codeRepository,en=Code Repository,zh=代码仓库,enabled=true,index=2
// +alauda:toolchain-gen:class=panel,category=CodeRepoBinding,en=Code Repository,zh=代码仓库,index=2

// CodeRepoService struct holds a reference to a specific CodeRepoService configuration
// and some user data for access
type CodeRepoService struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the CodeRepoService.
	// +optional
	Spec CodeRepoServiceSpec `json:"spec"`
	// Most recently observed status of the CodeRepoService.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// GetHost get the host defined in the service.
func (c *CodeRepoService) GetHost() string {
	return c.Spec.HTTP.Host
}

// GetHost get the html defined in the service.
func (c *CodeRepoService) GetHtml() string {
	switch c.Spec.Type {
	case CodeRepoServiceTypeGitlab, CodeRepoServiceTypeGitee, CodeRepoServiceTypeGogs, CodeRepoServiceTypeGitea:
		return c.Spec.HTTP.Host
	case CodeRepoServiceTypeGithub:
		return GithubHTML
	case CodeRepoServiceTypeBitbucket:
		return BitbucketHTML
	default:
		return fmt.Sprintf("invalid codeRepoService type %s", c.Spec.Type)
	}
}

// GetType get the type of the service.
func (c *CodeRepoService) GetType() CodeRepoServiceType {
	return c.Spec.Type
}

// IsPublic defines whether the service is public.
func (c *CodeRepoService) IsPublic() bool {
	return c.Spec.Public
}

// CodeRepoServiceSpec defines CodeRepoService's specs
type CodeRepoServiceSpec struct {
	// ToolSpec defins custom fields of tool
	ToolSpec `json:",inline"`
	// Type defines the service type.
	Type CodeRepoServiceType `json:"type"`
	// Public defines the public of the service.
	// +optional
	Public bool `json:"public"`
	// Data defines the data.
	// +optional
	Data map[string]string `json:"data"`
}

var _ ToolInterface = &CodeRepoService{}

func (crs *CodeRepoService) GetKind() string {
	return TypeCodeRepoService
}

func (codeRepoService *CodeRepoService) GetHostPort() HostPort {
	return codeRepoService.Spec.HTTP
}

func (crs *CodeRepoService) GetKindType() string {
	return crs.GetType().String()
}

func (crs *CodeRepoService) GetObjectMeta() metav1.Object {
	return &crs.ObjectMeta
}

func (crs *CodeRepoService) GetStatus() *ServiceStatus {
	return &crs.Status
}

func (crs *CodeRepoService) GetToolSpec() ToolSpec {
	return crs.Spec.ToolSpec
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceList is a list of CodeRepoService objects.
type CodeRepoServiceList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata"`

	// Items is a list of CodeRepoService.
	Items []CodeRepoService `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceAuthorizeResponse used to validate secret used in service
type CodeRepoServiceAuthorizeResponse struct {
	metav1.TypeMeta `json:",inline"`

	// AuthorizeUrl specifies the authorizeUrl used to authorize web page.
	// +optional
	AuthorizeUrl string `json:"authorizeUrl"`
	// RedirectUrl specifies redirect URL for the authorization request
	// +optional
	RedirectUrl string `json:"redirectUrl"`

	// GotAccessToken true when the access token was generated successfully
	// +optional
	GotAccessToken bool `json:"gotAccessToken"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceAuthorizeCreate used to update a secret using code response
type CodeRepoServiceAuthorizeCreate struct {
	metav1.TypeMeta `json:",inline"`
	// SecretName is a the name of the secret used for the oAuth2 authorization request
	SecretName string `json:"secretName"`
	// Namespace is the namespace of the given secret defined by SecretName
	Namespace string `json:"namespace"`
	// Authorization code used in the oAuth2 authorization flow
	// +optional
	Code string `json:"code"`
	// redirectUrl used in the oAuth2 authorization flow
	// +optional
	RedirectURL string `json:"redirectUrl"`
	// Defines if the API server should request the oAuth2 access token and refresh token
	GetAccessToken bool `json:"getAccessToken"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceAuthorizeOptions used to fetch service options
type CodeRepoServiceAuthorizeOptions struct {
	metav1.TypeMeta `json:",inline"`

	// SecretName specifies the name of the secret.
	// +optional
	SecretName string `json:"secretName"`
	// Namespace specifies the namespace of the secret.
	// +optional
	Namespace string `json:"namespace"`
	// RedirectURL specifies the redirectURL of the secret.
	// +optional
	RedirectURL string `json:"redirectUrl"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBinding struct holds a reference to a specific CodeRepoBinding configuration
// and some user data for access
type CodeRepoBinding struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the CodeRepoBinding.
	// +optional
	Spec CodeRepoBindingSpec `json:"spec"`
	// Most recently observed status of the CodeRepoBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

var _ StatusAccessor = &CodeRepoBinding{}

// GetStatus implements StatusAccessor
func (b *CodeRepoBinding) GetStatus() *ServiceStatus {
	return &b.Status
}

// CodeRepoBindingSpec defines CodeRepoBinding's spec
type CodeRepoBindingSpec struct {
	// CodeRepoService defines the codeRepoService.
	CodeRepoService LocalObjectReference `json:"codeRepoService"`
	// Account defines the account.
	Account CodeRepoBindingAccount `json:"account"`
}

// CodeRepoBindingAccount defines CodeRepoBinding' status
type CodeRepoBindingAccount struct {
	// Secret defines the secret.
	// It should not be optional
	Secret SecretKeySetRef `json:"secret"`
	// Owners defines the owners in this account.
	// +optional
	Owners []CodeRepositoryOwnerSync `json:"owners"`
}

func (b *CodeRepoBinding) GetSecretNamespace() string {
	namespace := b.GetNamespace()
	if b.Spec.Account.Secret.Namespace != "" {
		namespace = b.Spec.Account.Secret.Namespace
	}
	return namespace
}

func (b *CodeRepoBinding) GetSecretName() string {
	return b.Spec.Account.Secret.Name
}

// CodeRepositoryOwnerSync defines owner which will be synced.
type CodeRepositoryOwnerSync struct {
	// Type defines the type.
	Type OriginCodeRepoOwnerType `json:"type"`
	// Name defines the name.
	Name string `json:"name"`
	// All defines whether sync all repositories in this owner.
	// +optional
	All bool `json:"all"`
	// Repositories defines the repositories will be synced.
	// +optional
	Repositories []string `json:"repositories"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBindingList is a list of CodeRepoBinding objects.
type CodeRepoBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata"`

	// Items is a list of CodeRepoBinding objects
	Items []CodeRepoBinding `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// CodeRepoBranchOptions
type CodeRepoBranchOptions struct {
	metav1.TypeMeta `json:",inline"`
	// SortBy represent how to sort data
	// +optional
	SortBy string `json:"sortBy"`
	// SortMode represent the sort mode for example asc desc natural
	// +optional
	SortMode string `json:"sortMode"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// CodeRepoBranchResult
type CodeRepoBranchResult struct {
	metav1.TypeMeta `json:",inline"`
	Branches        []CodeRepoBranch `json:"branches"`
}

var _ ToolBindingLister = &CodeRepoBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *CodeRepoBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta = item.TypeMeta
	}
	return
}

const (
	GithubOrgKey    = "Organization"
	GitlabOrgKey    = "group"
	BitbucketOrgKey = "team"
	// gitee was not defined here because of no org key in response.
)

// GetOwnerType unify the owner type from different code repository service.
func GetOwnerType(strOwnerType string) OriginCodeRepoOwnerType {
	switch strOwnerType {
	case GithubOrgKey, GitlabOrgKey, BitbucketOrgKey:
		return OriginCodeRepoRoleTypeOrg
	default:
		return OriginCodeRepoOwnerTypeUser
	}
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepository struct holds a reference to a specific CodeRepository configuration
// and some user data for access
type CodeRepository struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the CodeRepository.
	// +optional
	Spec CodeRepositorySpec `json:"spec"`
	// Most recently observed status of the CodeRepository.
	// Populated by the system.
	// Read-only.
	// +optional
	Status CodeRepositoryStatus `json:"status"`
}

type CodeRepoBranch struct {
	// Name defines the name in branch
	Name string `json:"name"`
	// Commit defines the record was last commit by this branch
	Commit string `json:"commit"`
}

// CodeRepositorySpec defines CodeRepository's specs
type CodeRepositorySpec struct {
	// CodeRepoBinding defines the codeRepoBinding
	CodeRepoBinding LocalObjectReference `json:"codeRepoBinding"`
	// Repository defines the repository
	Repository OriginCodeRepository `json:"repository"`
}

// CodeRepositoryStatus represents CodeRepository's status
type CodeRepositoryStatus struct {
	// ServiceStatus defines the common status
	// +optional
	ServiceStatus `json:",inline"`
	// Repository defines the repository in status
	// +optional
	Repository CodeRepositoryStatusRepository `json:"repository"`
}

// CodeRepositoryStatusRepository represents CodeRepositoryStatusRepository's Repository
type CodeRepositoryStatusRepository struct {
	// LatestCommit defines the lastest commit
	// +optional
	LatestCommit RepositoryCommit `json:"latestCommit"`
}

// GetServiceName get service name
func (c *CodeRepository) GetServiceName() string {
	repoLabels := c.GetLabels()
	if repoLabels == nil {
		return ""
	}

	if serviceName, ok := repoLabels[LabelCodeRepoService]; ok {
		return serviceName
	}
	return ""
}

// GetRepoFullName defines repository's fullname. eg: "alauda/devops/devops-1/sy-prj4"
func (c *CodeRepository) GetRepoFullName() string {
	return c.Spec.Repository.FullName
}

// GetRepoID defines repository's id
func (c *CodeRepository) GetRepoID() string {
	return c.Spec.Repository.ID
}

// GetRepoName defines repository's name
func (c *CodeRepository) GetRepoName() string {
	return c.Spec.Repository.Name
}

// GetOwnerName defines owner's name
func (c *CodeRepository) GetOwnerName() string {
	return c.Spec.Repository.Owner.Name
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepositoryList is a list of CodeRepository objects.
type CodeRepositoryList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata"`

	// Items list of CodeRepository objects.
	Items []CodeRepository `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBindingRepositoryOptions used to fetch service options
type CodeRepoBindingRepositoryOptions struct {
	metav1.TypeMeta `json:",inline"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBindingRepositories used to retrieve logs from a pipeline
type CodeRepoBindingRepositories struct {
	metav1.TypeMeta `json:",inline"`

	// CodeRepoServiceType defines the service type.
	Type CodeRepoServiceType `json:"type"`
	// Owners a list of owner can be fetched from a binding.
	// +optional
	Owners []CodeRepositoryOwner `json:"owners"`
}

// CodeRepositoryOwner defines the repository owner.
type CodeRepositoryOwner struct {
	// Type defines the owner type.
	Type OriginCodeRepoOwnerType `json:"type"`
	// ID defines the id.
	ID string `json:"id"`
	// Name defines the name.
	Name string `json:"name"`
	// Email defines the email.
	// +optional
	Email string `json:"email"`
	// HTMLURL defines the htmlURL.
	// +optional
	HTMLURL string `json:"htmlURL"`
	// AvatarURL defines the avatarURL.
	// +optional
	AvatarURL string `json:"avatarURL"`
	// DiskUsage defines the diskUsage.
	// +optional
	DiskUsage int `json:"diskUsage"`
	// Repositories defines the repositories in this owner.
	// +optional
	Repositories []OriginCodeRepository `json:"repositories"`
}

type OriginCodeRepository struct {
	// CodeRepoServiceType defines the service type.
	CodeRepoServiceType CodeRepoServiceType `json:"codeRepoServiceType"`
	// ID defines the id.
	ID string `json:"id"`
	// Name defines the name.
	Name string `json:"name"`
	// FullName defines the full name
	// +optional
	FullName string `json:"fullName"`
	// Description defines the description.
	// +optional
	Description string `json:"description"`
	// HTMLURL defines the html url.
	// +optional
	HTMLURL string `json:"htmlURL"`
	// CloneURL defines the clone url which begins with "https://" or "http://".
	CloneURL string `json:"cloneURL"`
	// SSHURL defines ssh clone url which begins with "git@".
	SSHURL string `json:"sshURL"`
	// Language defines the language.
	// +optional
	Language string `json:"language"`
	// Owner defines the owner.
	Owner OwnerInRepository `json:"owner"`
	// CreatedAt defines the created time.
	CreatedAt *metav1.Time `json:"createdAt"`
	// PushedAt defines the pushed time.
	// +optional
	PushedAt *metav1.Time `json:"pushedAt"`
	// UpdatedAt defines the updated time.
	// +optional
	UpdatedAt *metav1.Time `json:"updatedAt"`
	// Private defines the visibility of the repository.
	// +optional
	Private bool `json:"private"`
	// Size defines the size of the repository used.
	// +optional
	Size int64 `json:"size"`
	// Size defines the size of the repository used which is humanize.
	// +optional
	SizeHumanize string `json:"sizeHumanize"`
	// Data defines reserved fields for the repository
	// +optional
	Data map[string]string `json:"data"`
}

type RepositoryCommit struct {
	// CommitID defines the commit id
	// +optional
	CommitID string `json:"commitID,omitempty"`
	// CommitMessage defines the commit id
	// +optional
	CommitMessage string `json:"commitMessage,omitempty"`
	// CommitAt defines the commit time
	// +optional
	CommitAt *metav1.Time `json:"commitAt,omitempty"`
	// CommitterName defines the committer's name
	// +optional
	CommitterName string `json:"committerName,omitempty"`
	// CommitterEmail defines the committer's email
	// +optional
	CommitterEmail string `json:"committerEmail,omitempty"`
}

func (r *RepositoryCommit) GetCommitAt() time.Time {
	if r.CommitAt != nil {
		return r.CommitAt.Time
	}
	return time.Time{}
}

// OwnerInRepository defines the owner in repository which includes fewer properties.
type OwnerInRepository struct {
	// OriginCodeRepoOwnerType defines the type of the owner
	// +optional
	Type OriginCodeRepoOwnerType `json:"type"`
	// ID defines the id of the owner
	// +optional
	ID string `json:"id"`
	// Name defines the name of the owner
	// +optional
	Name string `json:"name"`
}

// OriginCodeRepoOwnerType defines the owner type of the repository
type OriginCodeRepoOwnerType string

const (
	// OriginCodeRepoOwnerTypeUser defines the owner type of user
	OriginCodeRepoOwnerTypeUser OriginCodeRepoOwnerType = "User"
	// OriginCodeRepoRoleTypeOrg defines the owner type of org
	OriginCodeRepoRoleTypeOrg OriginCodeRepoOwnerType = "Org"
)

// endregion
