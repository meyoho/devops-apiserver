package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// region Jenkins

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +alauda:toolchain-gen:class=category,name=continuousIntegration,en=Continuous Integration,zh=持续集成,enabled=true,index=3
// +alauda:toolchain-gen:class=item,category=continuousIntegration,name=jenkins,en=Jenkins,zh=Jenkins,apipath=jenkinses,enabled=true,kind=jenkins,type=Jenkins,roleSyncEnabled=false
// +alauda:toolchain-gen:class=secret,category=continuousIntegration,itemName=jenkins,type=kubernetes.io/basic-auth,en=Input username as used on login in the Username field and the user token in the Password field,zh=用户名输入登录时的用户名，密码为在 Jenkins 配置页面的 Token
// +alauda:toolchain-gen:class=panel,category=JenkinsBinding,en=Continuous Integration,zh=持续集成,index=3

// Jenkins struct to hold the jenkins data
type Jenkins struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// Specification of the desired behavior of the Jenkins.
	// +optional
	Spec JenkinsSpec `json:"spec,omitempty"`
	// Most recently observed status of the Jenkins.
	// Populated by the system.
	// Read-only.
	// +optional
	Status JenkinsStatus `json:"status,omitempty"`
}

// JenkinsSpec defines Jenkins' specs
type JenkinsSpec struct {
	ToolSpec `json:",inline"`
}

// JenkinsStatus defines Jenkins' status
type JenkinsStatus struct {
	// ServiceStatus defines the common status
	// +optional
	ServiceStatus `json:",inline"`
}

// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// JenkinsList is a list of Jenkins objects.
type JenkinsList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of Jenkins objects.
	Items []Jenkins `json:"items"`
}

// region ToolInterface

var _ ToolInterface = &Jenkins{}

func (j *Jenkins) GetKind() string {
	return TypeJenkins
}

func (jenkins *Jenkins) GetHostPort() HostPort {
	return jenkins.Spec.HTTP
}

func (jenkins *Jenkins) GetKindType() string {
	return TypeJenkins
}

func (jenkins *Jenkins) GetObjectMeta() metav1.Object {
	return &jenkins.ObjectMeta
}

func (jenkins *Jenkins) GetStatus() *ServiceStatus {
	return &jenkins.Status.ServiceStatus
}

func (jenkins *Jenkins) GetToolSpec() ToolSpec {
	return jenkins.Spec.ToolSpec
}
