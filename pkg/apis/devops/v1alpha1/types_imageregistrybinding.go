package v1alpha1

import (
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryBinding registry service binding
type ImageRegistryBinding struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	Spec ImageRegistryBindingSpec `json:"spec"`
	// Most recently observed status of the ImageRegistryBinding.
	// Populated by the system.
	// Read-only
	// +optional
	Status ServiceStatus `json:"status"`
}

func (b *ImageRegistryBinding) GetSecretNamespace() string {
	namespace := b.GetNamespace()
	if b.Spec.Secret.Namespace != "" {
		namespace = b.Spec.Secret.Namespace
	}
	return namespace
}

func (b *ImageRegistryBinding) GetSecretName() string {
	return b.Spec.Secret.Name
}

func (b *ImageRegistryBinding) GetDockerCfgSecretName() string {
	if b.Spec.Secret.Name == "" {
		return ""
	}
	return fmt.Sprintf("dockercfg--%s--%s", b.Namespace, b.Name)
}

// ImageRegistryBindingSpec represents ImageRegistry specs
type ImageRegistryBindingSpec struct {
	// ImageRegistry defines the LocalObjectReference in spec
	ImageRegistry LocalObjectReference `json:"imageRegistry"`
	// Secret is the secret of the account.
	// +optional
	Secret SecretKeySetRef `json:"secret"`
	// RepoInfo defines the ImageRegistryBindingRepo in spec
	// +optional
	RepoInfo ImageRegistryBindingRepo `json:"repoInfo"`
}

// ImageRegistryBindingRepo save the path of user choose
type ImageRegistryBindingRepo struct {
	// +optional
	Repositories []string `json:"repositories"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryBindingList is a list of ImageRegistryBinding objects.
type ImageRegistryBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	// Items is a list of ImageRegistryBinding objects
	Items []ImageRegistryBinding `json:"items"`
}

var _ ToolBindingLister = &ImageRegistryBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *ImageRegistryBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta.Kind = TypeImageRegistryBinding
	}
	return
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryBindingRepositories used to retrieve repository from registry
type ImageRegistryBindingRepositories struct {
	metav1.TypeMeta `json:",inline"`

	Items []string `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryBindingRepositoryOptions user to fetch registry binding options
type ImageRegistryBindingRepositoryOptions struct {
	metav1.TypeMeta `json:",inline"`

	// +optional
}

var _ StatusAccessor = &ImageRegistryBinding{}

// GetStatus implements StatusAccessor
func (b *ImageRegistryBinding) GetStatus() *ServiceStatus {
	return &b.Status
}
