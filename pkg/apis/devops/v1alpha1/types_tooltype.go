package v1alpha1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type ToolType struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the Deployment.
	// +optional
	Spec ToolTypeSpec `json:"spec"`

	// Most recently observed status of the Deployment.
	// +optional
	Status ToolTypeStatus `json:"status"`
}

type ToolTypeSpec struct {
	DisplayName ToolTypeSpecDisplayName `json:"displayName"`
	// +optional
	Description ToolTypeSpecDisplayName `json:"description"`
	// +optional
	RecommendedVersion string            `json:"recommendedVersion"`
	ToolCategoryRef    []ToolCategoryRef `json:"toolCategory"`
	Enabled            bool              `json:"enabled"`
	//ToolRef              ToolTypeSpecToolRef                `json:"toolRef"`
	SupportedSecretTypes []ToolTypeSpecSupportedSecretTypes `json:"supportedSecretTypes"`
	Type                 string                             `json:"type"`
	Kind                 string                             `json:"kind"`
	Public               bool                               `json:"public"`
	Host                 string                             `json:"host"`
	Html                 string                             `json:"html"`
	Enterprise           bool                               `json:"enterprise"`
	ApiPath              string                             `json:"apiPath"`
	RoleSyncEnabled      bool                               `json:"roleSyncEnabled"`
}

type ToolTypeStatus struct {
	Phase string
}

type ToolTypeSpecDisplayName struct {
	En string `json:"en"`
	Zh string `json:"zh"`
}

type ToolTypeSpecToolRef struct {
	APIGroup string `json:"apiGroup"`
	Kind     string `json:"kind"`
}

type ToolTypeSpecSupportedSecretTypes struct {
	SecretType  string                  `json:"secretType"`
	Description ToolTypeSpecDisplayName `json:"description"`
}

type ToolCategoryRef struct {
	APIGroup string `json:"apiGroup"`
	Kind     string `json:"kind"`
	Name     string `json:"name"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ToolTypeList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	Items []ToolType `json:"items"`
}

func (in *ToolType) Merge(newT *ToolType) (result *ToolType, err error) {
	// in  is  old one
	// newT  is new one
	//tmpEnabled := in.Spec.Enabled
	in.Spec = newT.Spec
	in.Annotations = newT.Annotations
	//in.Spec.Enabled = tmpEnabled
	result = in
	return result, err
}
