package v1alpha1

import (
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ImageRegistry type of image registry
type ImageRegistryType string

const (
	// RegistryTypeDocker normal registry, deploy registry alone
	// +alauda:toolchain-gen:class=panel,category=ImageRegistryBinding,en=Artifacts,zh=制品仓库,index=5
	// +alauda:toolchain-gen:class=item,category=artifactRepository,name=docker-registry,en=Docker Registry,zh=Docker Registry,apipath=imageregistries,enabled=true,kind=imageregistry,type=Docker,roleSyncEnabled=false
	RegistryTypeDocker ImageRegistryType = "Docker"
	// RegistryTypeHarbor harbor
	// +alauda:toolchain-gen:class=item,category=artifactRepository,name=harbor-registry,en=Harbor Registry,zh=Harbor Registry,apipath=imageregistries,enabled=true,kind=imageregistry,type=Harbor,roleSyncEnabled=true
	// +alauda:rolesync-gen:class=platform,name=Harbor,sync=rbac-mutex,enabled=true,roles=project_admin:projectAdmin;project_auditor:guest;namespace_admin:developer;namespace_developer:developer;namespace_auditor:guest;space_admin:developer;space_developer:developer;space_auditor:guest
	RegistryTypeHarbor ImageRegistryType = "Harbor"
	// RegistryTypeAlauda alauda
	// +alauda:toolchain-gen:class=item,category=artifactRepository,name=alauda-registry,en=Alauda Registry,zh=Alauda Registry,apipath=imageregistries,enabled=true,kind=imageregistry,type=Alauda,roleSyncEnabled=false
	RegistryTypeAlauda ImageRegistryType = "Alauda"
	// RegistryTypeDockerHub dockerhub
	// +alauda:toolchain-gen:class=item,category=artifactRepository,name=dockerhub-registry,en=ockerHub Registry,zh=DockerHub Registry,apipath=imageregistries,enabled=true,kind=imageregistry,type=DockerHub,public=true,api=https://hub.docker.com,web=https://hub.docker.com,roleSyncEnabled=false
	RegistryTypeDockerHub ImageRegistryType = "DockerHub"

	// +alauda:toolchain-gen:class=secret,category=artifactRepository,itemName=docker-registry+dockerhub-registry+harbor-registry,type=kubernetes.io/basic-auth,en=Input Username and Password as used on login,zh=用户名和密码均为登录时的用户名和密码
	// +alauda:toolchain-gen:class=secret,category=artifactRepository,itemName=alauda-registry,type=kubernetes.io/basic-auth,en=Username is [ RootAccount/Username ] and Password is your login password,zh=用户名输入 [ 根账号/用户名 ]，密码输入登录时的密码
)

func (c ImageRegistryType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +alauda:toolchain-gen:class=category,name=artifactRepository,en=Artifact Repository,zh=制品仓库,enabled=true,index=5

// registry service
type ImageRegistry struct {
	// +optional
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	Spec ImageRegistrySpec `json:"spec"`
	// Most recently observed status of the ImageRegistry.
	// Populated by the system.
	// Read-only
	// +optional
	Status ServiceStatus `json:"status"`
}

// Get registry service endpoint
func (c *ImageRegistry) GetEndpoint() string {
	endpoint := c.Spec.HTTP.Host
	endpoint = strings.TrimRight(endpoint, "/")
	return endpoint
}

// Get registry service type
func (c *ImageRegistry) GetType() ImageRegistryType {
	return c.Spec.Type
}

// Get registry service spec
type ImageRegistrySpec struct {
	ToolSpec `json:",inline"`
	// Type defines the ImageRegistryType in spec
	Type   ImageRegistryType `json:"type"`
	Public bool              `json:"public"`

	// +optional
	Data map[string]string `json:"data"`
	// +optional
	// binding policy
	BindingPolicy *ImageRegistryBindingPolicy `json:"bindingPolicy"`
}

type ImageRegistryBindingPolicy struct {
	// +optional
	// policy individual
	// it will create project on remote registry with same name of binding's namespace/projectName
	Individual *ImageRegistryBindingPolicyIndividual `json:"individual"`

	// +optional
	// policy share
	// all the allocated repositories shared by all projects
	Share *ImageRegistryBindingPolicyShare `json:"share"`
}

// ImageRegistryBindingPolicyIndividual policy
// TODO
type ImageRegistryBindingPolicyIndividual struct{}

type ImageRegistryBindingPolicyShare struct {
	// allocated repos
	Repositories []string `json:"repositories"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ImageRegistryList Get image registry list
type ImageRegistryList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata"`

	// Items is a list of ImageRegistry objects
	Items []ImageRegistry `json:"items"`
}

var _ ToolInterface = &ImageRegistry{}

func (ir *ImageRegistry) GetKind() string {
	return TypeImageRegistry
}

func (ir *ImageRegistry) GetKindType() string {
	return ir.GetType().String()
}

func (ir *ImageRegistry) GetObjectMeta() metav1.Object {
	return &ir.ObjectMeta
}

func (imageRegistry *ImageRegistry) GetHostPort() HostPort {
	return imageRegistry.Spec.HTTP
}

func (imageRegistry *ImageRegistry) GetStatus() *ServiceStatus {
	return &imageRegistry.Status
}

func (imageRegistry *ImageRegistry) GetToolSpec() ToolSpec {
	return imageRegistry.Spec.ToolSpec
}

// endregion ToolInterface
