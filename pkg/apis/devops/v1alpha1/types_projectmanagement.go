package v1alpha1

import (
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// region ProjectManagement

// ProjectManagementType type for the ProjectManagement
type ProjectManagementType string

const (
	// ProjectManageTypeJira Jira
	// +alauda:toolchain-gen:class=panel,category=ProjectManagementBinding,en=Project Management,zh=项目管理,index=0
	// +alauda:toolchain-gen:class=item,category=projectManagement,name=jira,en=Jira,zh=Jira,apipath=projectmanagements,enabled=true,kind=projectmanagement,type=Jira,roleSyncEnabled=true
	// +alauda:toolchain-gen:class=secret,category=projectManagement,itemName=jira,type=kubernetes.io/basic-auth,en=Input Username and Password as used on login,zh=用户名和密码均为登录时的用户名和密码
	ProjectManageTypeJira ProjectManagementType = "Jira"
	// ProjectManageTypeTaiga Taiga
	// +alauda:toolchain-gen:class=item,category=projectManagement,name=taiga,en=Taiga,zh=Taiga,apipath=projectmanagements,enabled=true,kind=projectmanagement,type=Taiga,roleSyncEnabled=false
	// +alauda:toolchain-gen:class=secret,category=projectManagement,itemName=taiga,type=kubernetes.io/basic-auth,en=Input Username and Password as used on login,zh=用户名和密码均为登录时的用户名和密码
	ProjectManageTypeTaiga ProjectManagementType = "Taiga"
)

func (c ProjectManagementType) String() string {
	return string(c)
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +alauda:toolchain-gen:class=category,name=projectManagement,en=Project Management,zh=项目管理,enabled=true,index=0
// +alauda:rolesync-gen:class=platform,name=Jira,sync=rbac,enabled=true,roles=project_admin:AlaudaProjectAdmin;project_auditor:AlaudaProjectAuditor;space_admin:AlaudaSpaceAdmin;space_developer:AlaudaSpaceDeveloper;space_auditor:AlaudaSpaceAuditor;namespace_admin:AlaudaSpaceAdmin;namespace_auditor:AlaudaSpaceAuditor;namespace_developer:AlaudaSpaceDeveloper

// ProjectManagement struct holds ProjectManagement data
type ProjectManagement struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the ProjectManagement.
	// +optional
	Spec ProjectManagementSpec `json:"spec"`
	// Most recently observed status of the ProjectManagement.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

// Get projectmanagement service type
func (c *ProjectManagement) GetType() ProjectManagementType {
	return c.Spec.Type
}

func (c *ProjectManagement) GetEndpoint() string {
	endpoint := c.Spec.HTTP.Host
	endpoint = strings.TrimRight(endpoint, "/")
	return endpoint
}

func (c *ProjectManagement) GetSecretNamespace() string {
	namespace := c.GetNamespace()
	if c.Spec.Secret.Namespace != "" {
		namespace = c.Spec.Secret.Namespace
	}
	return namespace
}

func (c *ProjectManagement) GetSecretName() string {
	return c.Spec.Secret.Name
}

// ProjectManagementSpec is the spec in ProjectManagement
type ProjectManagementSpec struct {
	ToolSpec `json:",inline"`
	// Type defines the service type.
	Type ProjectManagementType `json:"type"`
	//Secret defines the secret type.
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type ProjectmanagementUser struct {
	metav1.TypeMeta `json:",inline"`
	Items           []User `json:"users"`
}
type User struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	ID       string `json:"id"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type UserSearchOptions struct {
	metav1.TypeMeta `json:",inline"`
	// Name specifies the name of the user.
	Name       string `json:"name"`
	SecretName string `json:"secretname"`
	NameSpace  string `json:"namespace"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagementList is a list of ProjectManagement objects.
type ProjectManagementList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of ProjectManagement
	Items []ProjectManagement `json:"items"`
}

var _ ToolInterface = &ProjectManagement{}

func (pm *ProjectManagement) GetKind() string {
	return TypeProjectManagement
}

func (pm *ProjectManagement) GetKindType() string {
	return pm.Spec.Type.String()
}

func (pm *ProjectManagement) GetObjectMeta() metav1.Object {
	return &pm.ObjectMeta
}

func (projectManagement *ProjectManagement) GetHostPort() HostPort {
	return projectManagement.Spec.HTTP
}

func (pm *ProjectManagement) GetStatus() *ServiceStatus {
	return &pm.Status
}

func (pm *ProjectManagement) GetToolSpec() ToolSpec {
	return pm.Spec.ToolSpec
}
