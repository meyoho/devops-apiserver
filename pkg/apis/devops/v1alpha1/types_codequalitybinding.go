package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeQualityBinding is the binding referenced to CodeQuality
type CodeQualityBinding struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	Spec CodeQualityBindingSpec `json:"spec"`
	// Most recently observed status of the CodeQualityBinding.
	// Populated by the system.
	// Read-only
	// +optional
	Status ServiceStatus `json:"status"`
}

func (b *CodeQualityBinding) GetSecretNamespace() string {
	namespace := b.GetNamespace()
	if b.Spec.Secret.Namespace != "" {
		namespace = b.Spec.Secret.Namespace
	}
	return namespace
}

func (b *CodeQualityBinding) GetSecretName() string {
	return b.Spec.Secret.Name
}

func (binding *CodeQualityBinding) GetStatus() *ServiceStatus {
	return &binding.Status
}

// CodeQualityBindingSpec represents CodeQualityBinding specs
type CodeQualityBindingSpec struct {
	// CodeQualityTool defines the CodeQualityTool in spec
	CodeQualityTool LocalObjectReference `json:"codeQualityTool"`
	// Secret is the secret of the account.
	// +optional
	Secret SecretKeySetRef `json:"secret"`
	// CodeRepository defines the CodeRepository in spec
	// +optional
	CodeRepository CodeRepositoryInfo `json:"codeRepository"`
	// Pattern defines sync project model is prefix of name
	// +optional
	Pattern CodeRepositoryPattern `json:"pattern"`
}

// CodeRepositoryInfo defines sync project model is devops binding codeRepository
type CodeRepositoryInfo struct {
	// All represent use all code repository or not
	// +optional
	All bool `json:"all"`
}

// CodeRepositoryPattern defines sync project model is prefix of name
type CodeRepositoryPattern struct {
	// Prefix is a list of sync project model's prefix of name
	// +optional
	Prefix []string `json:"prefix"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeQualityBindingList is a list of CodeQualityBinding objects.
type CodeQualityBindingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	// Items is a list of CodeQualityBinding objects
	Items []CodeQualityBinding `json:"items"`
}

var _ ToolBindingLister = &CodeQualityBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *CodeQualityBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta.Kind = TypeCodeQualityBinding
	}
	return
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type CodeQualityBindingProjects struct {
	metav1.TypeMeta `json:",inline"`

	Items []string `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type CorrespondCodeQualityProjectsOptions struct {
	metav1.TypeMeta `json:",inline"`
	Repositories    string `json:"repositories"`
}
