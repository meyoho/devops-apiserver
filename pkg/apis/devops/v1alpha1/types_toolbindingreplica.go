package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// region ToolBindingReplica

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type ToolBindingReplicaList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	// Items is a list of ToolBindingReplica
	Items []ToolBindingReplica `json:"items"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ToolBindingReplica is the tool binding of project
type ToolBindingReplica struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Spec defines the ToolBindingReplicaSpec in ToolBindingReplica
	// +optional
	Spec ToolBindingReplicaSpec `json:"spec"`
	// Status defines the ToolBindingReplicaStatus in ToolBindingReplica
	// +optional
	Status ToolBindingReplicaStatus `json:"status"`
}

// ObjectReferenceLabel get labels of current object
func (tbr *ToolBindingReplica) ObjectReferenceLabel(provider AnnotationProvider) map[string]string {
	return map[string]string{
		provider.LabelToolBindingReplica():          tbr.Name,
		provider.LabelToolBindingReplicaNamespace(): tbr.Namespace,
	}
}

// Aggregate all status that rely on to ToolBindingReplicaStatus
func (tbr *ToolBindingReplica) AggregateStatus() (existError bool) {

	if tbr.Status.HTTPStatus != nil && tbr.Status.HTTPStatus.ErrorMessage != "" {
		tbr.Status.Phase = NewToolBindingReplicaPhase(ServiceStatusPhaseError)
		tbr.Status.Reason = tbr.Status.HTTPStatus.ErrorMessage
		tbr.Status.Message = tbr.Status.HTTPStatus.ErrorMessage
		return true
	}

	for _, cond := range tbr.Status.Conditions {
		// Any error condition will cause status of tbr changing to error
		if cond.Status == StatusError || cond.Status == ServiceStatusNeedsAuthorization.String() {
			tbr.Status.Phase = StatusError
			tbr.Status.Reason = cond.Reason
			tbr.Status.Message = cond.Message
			return true
		}
	}
	return false
}

// LabelSelectorIsNotEmpty checks label is empty
func LabelSelectorIsNotEmpty(labelSelector *metav1.LabelSelector) bool {
	return len(labelSelector.MatchLabels) > 0 || len(labelSelector.MatchExpressions) > 0
}

// GetLabelSelector gets label selector
func (tbr *ToolBindingReplica) GetLabelSelector() metav1.LabelSelector {
	labelSelector := metav1.LabelSelector{}

	switch tbr.Spec.Selector.Kind {
	case TypeCodeQualityTool:
		for _, binding := range tbr.Spec.CodeQuality.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeCodeRepoService:
		for _, binding := range tbr.Spec.CodeRepoService.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeDocumentManagement:
		for _, binding := range tbr.Spec.DocumentManagement.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeImageRegistry:
		for _, binding := range tbr.Spec.ImageRegistry.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeJenkins:
		for _, binding := range tbr.Spec.ContinuousIntegration.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeProjectManagement:
		for _, binding := range tbr.Spec.ProjectManagement.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}

	case TypeArtifactRegistry:
		for _, binding := range tbr.Spec.ArtifactRegistry.Template.Bindings {
			if LabelSelectorIsNotEmpty(&binding.Selector.LabelSelector) {
				labelSelector = binding.Selector.LabelSelector
			}
		}
	}

	return labelSelector
}

type ToolBindingReplicaPhase string

func NewToolBindingReplicaPhase(phase ServiceStatusPhase) ToolBindingReplicaPhase {
	return ToolBindingReplicaPhase(string(phase))
}

func (phase ToolBindingReplicaPhase) Is(s ServiceStatusPhase) bool {
	return string(phase) == string(s)
}

const (
	ToolBindingReplicaPhaseSyncing ToolBindingReplicaPhase = "Syncing"
)

type ToolBindingReplicaStatus struct {
	Phase ToolBindingReplicaPhase `json:"phase"`
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string `json:"reason,omitempty"`
	// Human-readable message indicating details about last transition.
	// +optional
	Message string `json:"message,omitempty"`
	// LastUpdate is the latest time when updated the service.
	// +optional
	LastUpdate *metav1.Time `json:"lastUpdated"`
	// HTTPStatus is http status of the service.
	// +optional
	HTTPStatus *HostPortStatus `json:"http,omitempty"`
	// Conditions is a list of BindingCondition objects.
	// +optional
	Conditions []BindingCondition `json:"conditions"`
	// Last time we sync the status
	// +optional
	LastAttempt *metav1.Time `json:"lastAttempt"`
}

// ToolSelector is selector to select tool,contains enough information to let you inspect or modify the referred object.
type ToolSelector struct {
	corev1.ObjectReference `json:",inline"`
}

// ToolBindingReplicaSpec is the spec in ToolBindingReplica
type ToolBindingReplicaSpec struct {
	// Selector defines the ToolSelector in spec
	Selector ToolSelector `json:"selector"`

	// Secret defines the pointer of SecretKeySetRef in spec
	// +optional
	Secret *SecretKeySetRef `json:"secret"`

	// CodeRepoService defines the pointer of CodeRepoBindingReplicaTemplate in spec
	// +optional
	CodeRepoService *CodeRepoBindingReplicaTemplate `json:"codeRepoService,omitempty"`
	// ContinuousIntegration defines the pointer of CIBindingReplicaTemplate in spec
	// +optional
	ContinuousIntegration *CIBindingReplicaTemplate `json:"continuousIntegration,omitempty"`
	// ImageRegistry defines the pointer of ImageRegistryBindingReplicaTemplate in spec
	// +optional
	ImageRegistry *ImageRegistryBindingReplicaTemplate `json:"imageRegistry,omitempty"`
	// ProjectManagement defines the pointer of ProjectManagementBindingReplicaTemplate in spec
	// +optional
	ProjectManagement *ProjectManagementBindingReplicaTemplate `json:"projectManagement,omitempty"`
	// DocumentManagement defines the pointer of DocumentManagementBindingReplicaTemplate in spec
	// +optional
	DocumentManagement *DocumentManagementBindingReplicaTemplate `json:"documentManagement,omitempty"`
	// CodeQuality defines the pointer of CodeQualityBindingReplicaTemplate in spec
	// +optional
	CodeQuality *CodeQualityBindingReplicaTemplate `json:"codeQualityTool,omitempty"`
	// ArtifactRegistry defines the pointer of ArtifactRegistryBindingReplicaTemplate in spec
	// +optional
	ArtifactRegistry *ArtifactRegistryBindingReplicaTemplate `json:"artifactRegistry,omitempty"`
}

type CodeQualityBindingReplicaTemplate struct {
	// +optional
	Template *CodeQualityBindingReplicaTemplateSpec `json:"template"`
}

type CodeQualityBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []CodeQualityTemplate `json:"bindings"`
}

type CodeQualityTemplate struct {
	Selector BindingReplicaNamespaceSelector `json:"selector"`
	Spec     CodeQualityBindingSpec          `json:"spec"`
}

type DocumentManagementBindingReplicaTemplate struct {
	// +optional
	Spaces []DocumentManagementSpaceRef `json:"spaces"`
	// +optional
	Template *DocumentManagementBindingReplicaTemplateSpec `json:"template"`
}

type DocumentManagementBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []DocumentManagementBindingTemplate `json:"bindings"`
}

type DocumentManagementBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector `json:"selector"`
	Spec     DocumentManagementBindingSpec   `json:"spec"`
}

type ProjectManagementBindingReplicaTemplate struct {
	// +optional
	Projects []ProjectManagementProjectInfo `json:"projects"`
	// +optional
	Template *ProjectManagementBindingReplicaTemplateSpec `json:"template"`
}

type ProjectManagementBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []ProjectManagementBindingTemplate `json:"bindings"`
}

type ProjectManagementBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector `json:"selector"`
	Spec     ProjectManagementBindingSpec    `json:"spec"`
}
type CodeRepoBindingReplicaTemplate struct {
	// +optional
	Owners []OwnerInRepository `json:"owners"` // resource of code repo
	// +optional
	Template *CodeRepoBindingReplicaTemplateSpec `json:"template"`
}

type CodeRepoBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []CodeRepoBindingTemplate `json:"bindings"`
}

type CodeRepoBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector `json:"selector"`
	Spec     CodeRepoBindingSpec             `json:"spec"`
}

type BindingReplicaNamespaceSelector struct {
	// +optional
	metav1.LabelSelector `json:",inline"`
	// +optional
	NamespacesRef []string `json:"namespaces"`
}

type CIBindingReplicaTemplate struct {
	// no resources here
	// +optional
	Template *CIBindingReplicaTemplateSpec `json:"template"`
}

type CIBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []CIBindingTemplate `json:"bindings"`
}

type CIBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector `json:"selector"`
	Spec     JenkinsBindingSpec              `json:"spec"`
}

type ImageRegistryBindingReplicaTemplate struct {
	// +optional
	RepoInfo ImageRegistryBindingRepo `json:"repoInfo"` // Resource assign to project
	// +optional
	Template *ImageRegistryBindingReplicaTemplateSpec `json:"template"`
}

type ImageRegistryBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []ImageRegistryBindingTemplate `json:"bindings"`
}

type ImageRegistryBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector `json:"selector"`
	Spec     ImageRegistryBindingSpec        `json:"spec"`
}

type ArtifactRegistryBindingReplicaTemplate struct {
	// +optional
	Template *ArtifactRegistryBindingReplicaTemplateSpec `json:"template"`
}

type ArtifactRegistryBindingReplicaTemplateSpec struct {
	// +optional
	Bindings []ArtifactRegistryBindingTemplate `json:"bindings"`
}

type ArtifactRegistryBindingTemplate struct {
	Selector BindingReplicaNamespaceSelector `json:"selector"`
	Spec     ArtifactRegistryBindingSpec     `json:"spec"`
}

// endregion
