package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagementBinding is the binding referenced to ProjectManagement
type ProjectManagementBinding struct {
	metav1.TypeMeta `json:",inline"`
	// Standard object's metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
	// +optional
	metav1.ObjectMeta `json:"metadata"`

	// Specification of the desired behavior of the ProjectManagementBinding.
	// +optional
	Spec ProjectManagementBindingSpec `json:"spec"`
	// Most recently observed status of the ProjectManagementBinding.
	// Populated by the system.
	// Read-only.
	// +optional
	Status ServiceStatus `json:"status"`
}

func (b *ProjectManagementBinding) GetSecretNamespace() string {
	namespace := b.GetNamespace()
	if b.Spec.Secret.Namespace != "" {
		namespace = b.Spec.Secret.Namespace
	}
	return namespace
}

func (b *ProjectManagementBinding) GetSecretName() string {
	return b.Spec.Secret.Name
}

// ProjectManagementBindingSpec is the spec in ProjectManagementBinding
type ProjectManagementBindingSpec struct {
	ProjectManagement LocalObjectReference `json:"projectManagement"`
	//Secret defines the secret type.
	//+optional
	Secret SecretKeySetRef `json:"secret"`
	//+optional
	ProjectManagementProjectInfos []ProjectManagementProjectInfo `json:"projectManagementProjectInfos"`
}

type ProjectManagementProjectInfo struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Key  string `json:"key"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type IssueSearchOptions struct {
	metav1.TypeMeta `json:",inline"`
	// Type of issue it could be all status prioritiy or issuetype
	Type string `json:"type"`
}

type IssueFilterData struct {
	// Name of filterdata
	Name string `json:"name"`
	// ID of filterdata
	ID string `json:"id"`
	// Data about some special source
	Data map[string]string `json:"data"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type IssueFilterDataList struct {
	//+optional
	metav1.TypeMeta `json:",inline"`
	//Status defines the issue filter status
	//+optional
	Status []IssueFilterData `json:"status"`
	//IssueType defines the issue type for a issue
	//+optional
	IssueType []IssueFilterData `json:"issuetype"`
	// Priority defines the priority of the issue
	//+optional
	Priority []IssueFilterData `json:"priority"`
}

func (issuefilterlist *IssueFilterDataList) SetFilterData(name string, data IssueFilterData) {
	switch name {
	case "status":
		issuefilterlist.Status = append(issuefilterlist.Status, data)
	case "issuetype":
		issuefilterlist.IssueType = append(issuefilterlist.IssueType, data)
	case "priority":
		issuefilterlist.Priority = append(issuefilterlist.Priority, data)
	default:
		return
	}
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type ListIssuesOptions struct {
	//+optional
	metav1.TypeMeta `json:",inline"`
	//Page defines the page num
	Page int `json:"page"`
	//PageSize defines the pagesize num
	PageSize int `json:"pagesize"`
	//Project defines the Project name
	//+optional
	Project string `json:"project"`
	//Type defines the issue type
	//+optional
	Type string `json:"type"`
	//Priority defines the priority of the issue list
	//+optional
	Priority string `json:"priority"`
	//Status defines the status of the issue list
	//+optional
	Status string `json:"status"`
	//Summary defines the summary of the issue list
	//+optional
	Summary string `json:"summary"`
	//Issuekey defines the issue key of a issue
	//+optional
	IssueKey string `json:"issuekey"`
	//OrderBy defines the order
	OrderBy string `json:"orderby"`
	//Sort chould be ASC or DESC
	Sort string `json:"sort"`
	//+optional
	Projects []string `json:"projects"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type IssueDetail struct {
	//+optional
	metav1.TypeMeta `json:",inline"`
	//link of the issue
	SelfLink string `json:"selflink"`
	//key of the issue
	Key string `json:"key"`
	//summay of the issue
	Summary string `json:"summary"`
	//description of the issue
	Description string `json:"description"`
	//comments of the issue
	Comments []Comment `json:"comments"`
	//issuelinks of the issue
	IssueLinks []IssueLink `json:"issuelinks"`
	//priority of the issue
	Priority IssuePriority `json:"priority"`
	//issuetype of the issue
	Issuetype IssueType `json:"issuetype"`
	//status of the issue
	Status IssueStatus `json:"status"`
	//project which this issue belong to
	Project ProjectData `json:"project"`
	//created time
	Created string `json:"created"`
	//updated time
	Updated string `json:"updated"`
	//creator name
	Creator User `json:"creator"`
	//assigneer name
	Assignee User `json:"assignee"`
}

type IssueType struct {
	//Issue type name
	Name string `json:"name"`
}

type IssueStatus struct {
	//Issue Status name
	Name string `json:"name"`
	//Issue Status ID
	ID string `json:"id"`
}

type IssuePriority struct {
	//Issue Priority Name
	Name string `json:"name"`
}

type Comment struct {
	//Comment author name
	Author string `json:"author"`
	//Created Time
	Time string `json:"time"`
	//Content of the comment
	Content string `json:"content"`
}

type IssueLink struct {
	//link of the issue
	SelfLink string `json:"selflink"`
	//Summary of the issue
	Summary string `json:"summary"`
	//Key of the issue
	Key string `json:"key"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type IssueKeyOptions struct {
	//+optional
	metav1.TypeMeta `json:",inline"`
	//key of the issue
	Key string `json:"key"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type IssueDetailList struct {
	metav1.TypeMeta `json:",inline"`
	// total num of the issue list
	Total int `json:"total"`
	// items of the issuelist
	Items []IssueDetail `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectManagementBindingList is a list of ProjectManagementBinding objects.
type ProjectManagementBindingList struct {
	metav1.TypeMeta `json:",inline"`
	// Standard list metadata.
	// More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	// Items is a list of ProjectManagementBinding
	Items []ProjectManagementBinding `json:"items"`
}

var _ ToolBindingLister = &ProjectManagementBindingList{}

// GetItems returns items as ToolBinding to satisfy ToolBindingLister interface
func (list *ProjectManagementBindingList) GetItems() (items []ToolBinding) {
	items = make([]ToolBinding, len(list.Items))
	for i, item := range list.Items {
		items[i] = NewToolBinding(&item)
		items[i].TypeMeta.Kind = TypeProjectManagementBinding
	}
	return
}

func (pm *ProjectManagementBinding) GetStatus() *ServiceStatus {
	return &pm.Status
}
