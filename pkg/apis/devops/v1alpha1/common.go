package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
)

func GetSimpleSelector(key, value string) (selector labels.Selector) {
	selector = labels.NewSelector()
	req, _ := labels.NewRequirement(key, selection.Equals, []string{value})
	selector.Add(*req)
	return
}

func GetRepoSelector(serviceName string) (selector labels.Selector) {
	return GetSimpleSelector(LabelCodeRepository, serviceName)
}

func GetServiceSelector(serviceName string) (selector labels.Selector) {
	return GetSimpleSelector(LabelCodeRepoService, serviceName)
}

// GetOptions return a standard metav1.GetOptions to use APIServer cache
func GetOptions() metav1.GetOptions {
	return metav1.GetOptions{ResourceVersion: "0"}
}

// ListOptions returns a standard metav1.ListOptions to use APIServer cache
func ListOptions() metav1.ListOptions {
	return ListEverything
}
