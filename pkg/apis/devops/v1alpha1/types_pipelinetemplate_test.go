package v1alpha1_test

import (
	devops "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("Types.PipelineTemplate", func() {

	Describe("PipelineTemplate.GetVersionedName", func() {

		var (
			template devops.PipelineTemplate
		)

		BeforeEach(func() {
			template = devops.PipelineTemplate{
				ObjectMeta: metav1.ObjectMeta{
					Name: "alaudaBuildImage",
					Annotations: map[string]string{
						"version": "1.0",
					},
				},
			}
		})

		Context("When has version", func() {
			It("Should get correct versioned name", func() {
				name, err := template.GetVersionedName()
				Expect(err).To(BeNil())
				Expect(name).To(BeEquivalentTo("alaudaBuildImage.1.0"))
			})
		})

		Context("When has no version", func() {
			It("Should get error", func() {
				template.Annotations = map[string]string{}
				_, err := template.GetVersionedName()
				Expect(err).NotTo(BeNil())
			})
		})

		Context("When has annotation", func() {
			It("Should get error", func() {
				template.Annotations = nil
				_, err := template.GetVersionedName()
				Expect(err).NotTo(BeNil())
			})
		})
	})

	Describe("ClusterPipelineTemplate.GetVersionedName", func() {
		var (
			template devops.ClusterPipelineTemplate
		)

		BeforeEach(func() {
			template = devops.ClusterPipelineTemplate{
				ObjectMeta: metav1.ObjectMeta{
					Name: "alaudaBuildImage",
					Annotations: map[string]string{
						"version": "1.0",
					},
				},
			}
		})

		Context("When has version", func() {
			It("Should get correct versioned name", func() {
				name, err := template.GetVersionedName()
				Expect(err).To(BeNil())
				Expect(name).To(BeEquivalentTo("alaudaBuildImage.1.0"))
			})
		})

		Context("When has no version", func() {
			It("Should get error", func() {
				template.Annotations = map[string]string{}
				_, err := template.GetVersionedName()
				Expect(err).NotTo(BeNil())
			})
		})

		Context("When has annotation", func() {
			It("Should get error", func() {
				template.Annotations = nil
				_, err := template.GetVersionedName()
				Expect(err).NotTo(BeNil())
			})
		})
	})

	Describe("ClusterPipelineTaskTemplate.GetVersionedName", func() {
		var (
			template devops.ClusterPipelineTaskTemplate
		)

		BeforeEach(func() {
			template = devops.ClusterPipelineTaskTemplate{
				ObjectMeta: metav1.ObjectMeta{
					Name: "alaudaBuildImage",
					Annotations: map[string]string{
						"version": "1.0",
					},
				},
			}
		})

		Context("When has version", func() {
			It("Should get correct versioned name", func() {
				name, err := template.GetVersionedName()
				Expect(err).To(BeNil())
				Expect(name).To(BeEquivalentTo("alaudaBuildImage.1.0"))
			})
		})

		Context("When has no version", func() {
			It("Should get error", func() {
				template.Annotations = map[string]string{}
				_, err := template.GetVersionedName()
				Expect(err).NotTo(BeNil())
			})
		})

		Context("When has annotation", func() {
			It("Should get error", func() {
				template.Annotations = nil
				_, err := template.GetVersionedName()
				Expect(err).NotTo(BeNil())
			})
		})
	})

	Describe("PipelineTaskTemplate.GetVersionedName", func() {
		var (
			template devops.PipelineTaskTemplate
		)

		BeforeEach(func() {
			template = devops.PipelineTaskTemplate{
				ObjectMeta: metav1.ObjectMeta{
					Name: "alaudaBuildImage",
					Annotations: map[string]string{
						"version": "1.0",
					},
				},
			}
		})

		Context("When has version", func() {
			It("Should get correct versioned name", func() {
				name, err := template.GetVersionedName()
				Expect(err).To(BeNil())
				Expect(name).To(BeEquivalentTo("alaudaBuildImage.1.0"))
			})
		})

		Context("When has no version", func() {
			It("Should get error", func() {
				template.Annotations = map[string]string{}
				_, err := template.GetVersionedName()
				Expect(err).NotTo(BeNil())
			})
		})

		Context("When has annotation", func() {
			It("Should get error", func() {
				template.Annotations = nil
				_, err := template.GetVersionedName()
				Expect(err).NotTo(BeNil())
			})
		})
	})
})
