package dependency

import (
	"context"
	"fmt"
	"sync"

	"k8s.io/apimachinery/pkg/api/errors"
	metainternalversion "k8s.io/apimachinery/pkg/apis/meta/internalversion"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/apiserver/pkg/registry/rest"
)

var (
	// DefaultManager default Dependency getter
	DefaultManager = NewManager()
)

// AddStore adds get and list methods of store to DefaultManager for a kind
func AddStore(kind string, store rest.StandardStorage) {
	DefaultManager.Kind(kind).AddGetFunc(store.Get).AddListFunc(store.List)
}

// manager default implement for Manager
type manager struct {
	Solvers map[string]Resolver
	init    sync.Once
	lock    *sync.RWMutex
}

var _ Manager = &manager{}

// NewManager constructs a dependency getter
func NewManager() Manager {
	return &manager{
		lock: new(sync.RWMutex),
	}
}

func (d *manager) get(ctx context.Context, kind, name string, obj runtime.Object) (depItemList ItemList) {
	solver := d.Kind(kind)
	depItemList = make(ItemList, 0, len(solver.Dependencies())+1)
	ownItem := &Item{Kind: kind, IntoFunc: solver.GetIntoFunc()}
	depItemList = append(depItemList, ownItem)
	currentNamespace := genericapirequest.NamespaceValue(ctx)
	if ownItem.IntoFunc == nil {
		ownItem.Error = errors.NewBadRequest(fmt.Sprintf("Dependecy resolver for kind \"%v\" did not initiate a IntoFunc. This is most likely a programming error", kind))
		return
	}

	if ownItem.Object, ownItem.Error = d.resolverGetFunc(solver, obj)(ctx, name, &metav1.GetOptions{ResourceVersion: "0"}); ownItem.Error != nil {
		return
	}
	if len(solver.Dependencies()) > 0 {
		for _, dep := range solver.Dependencies() {
			if dep.GetNameNamespace == nil || dep.Kind == "" {
				continue
			}
			apply, namespace, name := dep.GetNameNamespace(ownItem)
			if !apply {
				continue
			}
			dependencyContext := ctx
			if namespace != currentNamespace {
				dependencyContext = genericapirequest.WithNamespace(genericapirequest.NewContext(), namespace)
			}

			depItemList = append(depItemList, d.Get(dependencyContext, dep.Kind, name)...)
		}
	}
	return
}

// Get get one item and all its dependencies
// this will execute as recursion and could generate stack overflow if the dependency stack is too high
func (d *manager) Get(ctx context.Context, kind, name string) (depItemList ItemList) {
	return d.get(ctx, kind, name, nil)
}

// GetWithObject get one item and all its dependencies
func (d *manager) GetWithObject(ctx context.Context, kind string, obj runtime.Object) (depItemList ItemList) {
	return d.get(ctx, kind, "", obj)
}

// AddDependency add dependency request
func (d *manager) AddDependency(kind string, getFunc GetObjectFunc, dependencyRequests ...Request) Manager {
	solver := d.Kind(kind)
	solver.AddGetFunc(getFunc).AddDependency(dependencyRequests...)
	return d
}

// AddList adds a list func to the kind
func (d *manager) AddList(kind string, listFunc ListObjectFunc) Manager {
	solver := d.Kind(kind)
	solver.AddListFunc(listFunc)
	return d
}

// SetAsBinding set kind as binding
func (d *manager) SetAsBinding(kind string) Manager {
	d.Kind(kind).SetBinding(true)
	return d
}

// GetBindingKinds return all bindings kinds
func (d *manager) GetBindingKinds() (kinds []string) {
	kinds = make([]string, 0, len(d.Solvers))
	for k, v := range d.Solvers {
		if v.GetBinding() {
			kinds = append(kinds, k)
		}
	}
	return
}

// AddIntoFunc add copyInto function
func (d *manager) AddIntoFunc(kind string, intoFunc CopyIntoFunc) Manager {
	d.Kind(kind).SetIntoFunc(intoFunc)
	return d
}

// Kind gets or initiates a new ResourceDependencySolver for a kind
func (d *manager) Kind(kind string) Resolver {
	d.init.Do(func() {
		if d.Solvers == nil {
			d.Solvers = map[string]Resolver{}
		}
	})
	d.lock.RLock()
	solver, ok := d.Solvers[kind]
	d.lock.RUnlock()
	if !ok {
		solver = NewResolver()
		d.lock.Lock()
		d.Solvers[kind] = solver
		d.lock.Unlock()
	}
	return solver
}

// List list items
func (d *manager) List(ctx context.Context, kind string, opts *metainternalversion.ListOptions) (runtime.Object, error) {
	return d.Kind(kind).List(ctx, opts)
}

func (d *manager) resolverGetFunc(solver Resolver, obj runtime.Object) func(context.Context, string, *metav1.GetOptions) (runtime.Object, error) {
	if obj == nil {
		return solver.Get
	}

	return func(context.Context, string, *metav1.GetOptions) (runtime.Object, error) {
		return obj, nil
	}
}
