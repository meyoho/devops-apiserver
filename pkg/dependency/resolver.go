package dependency

import (
	"context"

	"k8s.io/apimachinery/pkg/api/errors"
	metainternalversion "k8s.io/apimachinery/pkg/apis/meta/internalversion"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilerrors "k8s.io/apimachinery/pkg/util/errors"
)

// resolver defines a resource dependency solution
type resolver struct {
	IntoFunc       CopyIntoFunc
	GetFunc        []GetObjectFunc
	ListFunc       []ListObjectFunc
	IsBinding      bool
	DependencyList RequestList
}

var _ Resolver = &resolver{}

// NewResolver constructor for Resolver
func NewResolver() Resolver {
	return &resolver{
		GetFunc:  make([]GetObjectFunc, 0, 2),
		ListFunc: make([]ListObjectFunc, 0, 2),
	}
}

// Get get object
func (r *resolver) Get(ctx context.Context, name string, opts *metav1.GetOptions) (obj runtime.Object, err error) {
	if len(r.GetFunc) == 0 {
		err = errors.NewBadRequest("No get functions added")
		return
	}
	errs := make([]error, 0, len(r.GetFunc))
	for _, get := range r.GetFunc {
		obj, err = get(ctx, name, opts)
		// if get succeeds at least once, should ignore all errors
		// and return the object
		if err == nil && obj != nil {
			errs = nil
			break
		}
		errs = append(errs, err)
	}
	// if errors were not ignored, aggregate all errors in one
	if len(errs) > 0 {
		err = utilerrors.NewAggregate(errs)
	}
	return
}

// SetIntoFunc sets specific function to add resource
func (r *resolver) SetIntoFunc(f CopyIntoFunc) Resolver {
	r.IntoFunc = f
	return r
}

// GetIntoFunc gets a function to copy instances of object
func (r *resolver) GetIntoFunc() CopyIntoFunc {
	return r.IntoFunc
}

// UnshiftGetFunc sets a get function to the beginning
func (r *resolver) UnshiftGetFunc(f GetObjectFunc) Resolver {
	r.GetFunc = append([]GetObjectFunc{f}, r.GetFunc...)
	return r
}

// AddGetFunc add functions to the end of the list
func (r *resolver) AddGetFunc(f GetObjectFunc) Resolver {
	r.GetFunc = append(r.GetFunc, f)
	return r
}

func (r *resolver) List(ctx context.Context, opts *metainternalversion.ListOptions) (list runtime.Object, err error) {
	if len(r.ListFunc) == 0 {
		err = errors.NewBadRequest("No list functions added")
		return
	}
	errs := make([]error, 0, len(r.ListFunc))
	for _, listFunc := range r.ListFunc {
		list, err = listFunc(ctx, opts)
		// if list succeeds at least once, should ignore all errors
		// and return the object
		if err == nil && list != nil {
			errs = nil
			break
		}
		errs = append(errs, err)
	}
	// if errors were not ignored, aggregate all errors in one
	if len(errs) > 0 {
		err = utilerrors.NewAggregate(errs)
	}
	return
}

// UnshiftListFunc sets a list function to the beginning
func (r *resolver) UnshiftListFunc(f ListObjectFunc) Resolver {
	r.ListFunc = append([]ListObjectFunc{f}, r.ListFunc...)
	return r
}

// AddListFunc adds a list function to the end
func (r *resolver) AddListFunc(f ListObjectFunc) Resolver {
	r.ListFunc = append(r.ListFunc, f)
	return r
}

// SetBinding set binding property
func (r *resolver) SetBinding(b bool) Resolver {
	r.IsBinding = b
	return r
}

// GetBinding get binding property
func (r *resolver) GetBinding() bool {
	return r.IsBinding
}

// AddDependency add dependency to item
func (r *resolver) AddDependency(deps ...Request) Resolver {
	r.DependencyList = append(r.DependencyList, deps...)
	return r
}

// Dependencies return list of dependencies
func (r *resolver) Dependencies() RequestList {
	return r.DependencyList
}
