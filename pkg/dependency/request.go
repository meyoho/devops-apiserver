package dependency

// Request represent a dependency request
type Request struct {
	Kind             string
	GetNameNamespace func(*Item) (apply bool, namespace, name string)
}

// NewRequest constructs new request
func NewRequest(kind string, getRequest func(*Item) (apply bool, namespace, name string)) Request {
	return Request{
		Kind:             kind,
		GetNameNamespace: getRequest,
	}
}

// RequestList represent a list of Request
type RequestList []Request
