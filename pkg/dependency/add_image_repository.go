package dependency

import "alauda.io/devops-apiserver/pkg/apis/devops"

func init() {

	// ImageRegistry
	DefaultManager.Kind(devops.TypeImageRegistry).AddDependency(
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.ImageRegistry)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	)

	// ImageRegistryBinding
	DefaultManager.Kind(devops.TypeImageRegistryBinding).AddDependency(
		// Jenkins
		NewRequest(
			devops.TypeImageRegistry,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.ImageRegistryBinding)
				if !ok {
					return false, "", ""
				}
				return true, "", target.Spec.ImageRegistry.Name
			},
		),
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.ImageRegistryBinding)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	).SetBinding(true)

	// ImageRepository
	DefaultManager.Kind(devops.TypeImageRepository).AddDependency(
		NewRequest(
			devops.TypeImageRegistryBinding,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.ImageRepository)
				if !ok {
					return false, "", ""
				}
				return true, target.Namespace, target.Spec.ImageRegistryBinding.Name
			},
		),
	)
}
