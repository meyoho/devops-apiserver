package dependency

import "alauda.io/devops-apiserver/pkg/apis/devops"

func init() {

	// TypeCodeRepoService
	DefaultManager.Kind(devops.TypeCodeRepoService).AddDependency(
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.CodeRepoService)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	)

	// CodeRepoBinding
	DefaultManager.Kind(devops.TypeCodeRepoBinding).AddDependency(
		// Jenkins
		NewRequest(
			devops.TypeCodeRepoService,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.CodeRepoBinding)
				if !ok {
					return false, "", ""
				}
				return true, "", target.Spec.CodeRepoService.Name
			},
		),
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.CodeRepoBinding)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Account.Secret.Name != "", target.Spec.Account.Secret.Namespace, target.Spec.Account.Secret.Name
			},
		),
	).SetBinding(true)

	// CodeRepository
	DefaultManager.Kind(devops.TypeCodeRepository).AddDependency(
		NewRequest(
			devops.TypeCodeRepoBinding,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.CodeRepository)
				if !ok {
					return false, "", ""
				}
				return true, target.Namespace, target.Spec.CodeRepoBinding.Name
			},
		),
	)
}
