package dependency_test

import (
	"context"
	"errors"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metainternalversion "k8s.io/apimachinery/pkg/apis/meta/internalversion"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

var _ = Describe("Resolver.Get", func() {

	var (
		resolver dependency.Resolver
		ctx      context.Context
		name     string
		opts     *metav1.GetOptions
		binding  bool

		obj runtime.Object
		err error
	)

	BeforeEach(func() {
		resolver = dependency.NewResolver()
		ctx = context.TODO()
		name = "name"
		opts = &metav1.GetOptions{}
		binding = true

	})

	JustBeforeEach(func() {
		resolver.SetBinding(binding)
		obj, err = resolver.Get(ctx, name, opts)
	})

	It("GetFunc is null, should return error", func() {
		Expect(err).ToNot(BeNil(), "should return error")
		Expect(obj).To(BeNil(), "no object should be returned")
		Expect(resolver.GetBinding()).To(BeTrue(), "should be set")
	})

	Context("Get returns object", func() {
		BeforeEach(func() {
			resolver.AddGetFunc(func(cont context.Context, str string, op *metav1.GetOptions) (runtime.Object, error) {
				return &devops.Pipeline{}, nil
			})
		})
		It("should return object", func() {
			Expect(err).To(BeNil(), "no error")
			Expect(obj).ToNot(BeNil(), "pipeline object should be returned")
			Expect(obj.(*devops.Pipeline)).ToNot(BeNil(), "object should be pipeline pointer")
		})
	})

	Context("Get returns error", func() {
		BeforeEach(func() {
			resolver.UnshiftGetFunc(func(cont context.Context, str string, op *metav1.GetOptions) (runtime.Object, error) {
				return nil, errors.New("some other error")
			})
		})
		It("should return specified error", func() {
			Expect(err).ToNot(BeNil())
			Expect(err.Error()).To(ContainSubstring("some other error"), "should be error")
			Expect(obj).To(BeNil(), "no object should be returned")
		})
	})

	Context("Add multiple get functions with success", func() {
		BeforeEach(func() {
			resolver.AddGetFunc(func(cont context.Context, str string, op *metav1.GetOptions) (runtime.Object, error) {
				return nil, errors.New("get error")
			})
			resolver.AddGetFunc(func(cont context.Context, str string, op *metav1.GetOptions) (runtime.Object, error) {
				return &devops.Pipeline{}, nil
			})
		})
		It("should ignore the first error and return object", func() {
			Expect(err).To(BeNil(), "no error")
			Expect(obj).ToNot(BeNil(), "pipeline object should be returned")
			Expect(obj.(*devops.Pipeline)).ToNot(BeNil(), "object should be pipeline pointer")
		})
	})

	Context("Add multiple get functions all with error", func() {
		BeforeEach(func() {
			resolver.AddGetFunc(func(cont context.Context, str string, op *metav1.GetOptions) (runtime.Object, error) {
				return nil, errors.New("first error")
			})
			resolver.AddGetFunc(func(cont context.Context, str string, op *metav1.GetOptions) (runtime.Object, error) {
				return nil, errors.New("second error")
			})
		})
		It("should aggregate all errors", func() {
			Expect(err).ToNot(BeNil(), "should return an error")
			Expect(err).To(HaveLen(2), "should return two errors")
			Expect(obj).To(BeNil(), "pipeline object should be nil")
		})
	})

})

var _ = Describe("Resolver.List", func() {

	var (
		resolver dependency.Resolver
		ctx      context.Context
		opts     *metainternalversion.ListOptions

		obj runtime.Object
		err error
	)

	BeforeEach(func() {
		resolver = dependency.NewResolver()
		ctx = context.TODO()
		opts = &metainternalversion.ListOptions{}

	})

	JustBeforeEach(func() {
		obj, err = resolver.List(ctx, opts)
	})

	It("GetFunc is null, should return error", func() {
		Expect(err).ToNot(BeNil(), "should return error")
		Expect(obj).To(BeNil(), "no object should be returned")
	})

	Context("Get returns object", func() {
		BeforeEach(func() {
			resolver.AddListFunc(func(cont context.Context, op *metainternalversion.ListOptions) (runtime.Object, error) {
				return &devops.PipelineList{}, nil
			})
		})
		It("should return object", func() {
			Expect(err).To(BeNil(), "no error")
			Expect(obj).ToNot(BeNil(), "pipeline object should be returned")
			Expect(obj.(*devops.PipelineList)).ToNot(BeNil(), "object should be pipeline pointer")
		})
	})

	Context("Get returns error", func() {
		BeforeEach(func() {
			resolver.UnshiftListFunc(func(cont context.Context, op *metainternalversion.ListOptions) (runtime.Object, error) {
				return nil, errors.New("some other error")
			})
		})
		It("should return specified error", func() {
			Expect(err).ToNot(BeNil())
			Expect(err.Error()).To(ContainSubstring("some other error"), "should be error")
			Expect(obj).To(BeNil(), "no object should be returned")
		})
	})

	Context("Add multiple list functions", func() {
		BeforeEach(func() {
			resolver.AddListFunc(func(cont context.Context, op *metainternalversion.ListOptions) (runtime.Object, error) {
				return nil, errors.New("some other error")
			})
			resolver.AddListFunc(func(cont context.Context, op *metainternalversion.ListOptions) (runtime.Object, error) {
				return &devops.PipelineList{}, nil
			})
		})
		It("should ignore first error and fetch the second", func() {
			Expect(err).To(BeNil(), "no error")
			Expect(obj).ToNot(BeNil(), "pipeline list should be returned")
			Expect(obj.(*devops.PipelineList)).ToNot(BeNil(), "object should be pipeline pointer")
		})
	})
	Context("Add multiple list functions all with error", func() {
		BeforeEach(func() {
			resolver.AddListFunc(func(cont context.Context, op *metainternalversion.ListOptions) (runtime.Object, error) {
				return nil, errors.New("first error")
			})
			resolver.AddListFunc(func(cont context.Context, op *metainternalversion.ListOptions) (runtime.Object, error) {
				return nil, errors.New("second error")
			})
		})
		It("should aggregate all errors", func() {
			Expect(err).ToNot(BeNil(), "no error")
			Expect(err).To(HaveLen(2), "should have two errors")
			Expect(obj).To(BeNil(), "pipeline list should be returned")
		})
	})
})
