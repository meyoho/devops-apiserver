package dependency_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestInterface(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("dependency.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/dependency", []Reporter{junitReporter})
}
