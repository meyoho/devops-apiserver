package dependency

import "alauda.io/devops-apiserver/pkg/apis/devops"

func init() {

	// TypeProjectManagement
	DefaultManager.Kind(devops.TypeProjectManagement).AddDependency(
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.ProjectManagement)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	)

	// TypeProjectManagementBinding
	DefaultManager.Kind(devops.TypeProjectManagementBinding).AddDependency(
		// ProjectManagement
		NewRequest(
			devops.TypeProjectManagement,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.ProjectManagementBinding)
				if !ok {
					return false, "", ""
				}
				return true, "", target.Spec.ProjectManagement.Name
			},
		),
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.ProjectManagementBinding)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	).SetBinding(true)
}
