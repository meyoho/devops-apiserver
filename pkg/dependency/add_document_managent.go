package dependency

import "alauda.io/devops-apiserver/pkg/apis/devops"

func init() {

	// TypeDocumentManagement
	DefaultManager.Kind(devops.TypeDocumentManagement).AddDependency(
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.DocumentManagement)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	)

	// CodeRepoBinding
	DefaultManager.Kind(devops.TypeDocumentManagementBinding).AddDependency(
		// Jenkins
		NewRequest(
			devops.TypeDocumentManagement,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.DocumentManagementBinding)
				if !ok {
					return false, "", ""
				}
				return true, "", target.Spec.DocumentManagement.Name
			},
		),
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.DocumentManagementBinding)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	).SetBinding(true)
}
