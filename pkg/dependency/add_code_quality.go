package dependency

import "alauda.io/devops-apiserver/pkg/apis/devops"

func init() {

	// CodeQualityTool
	DefaultManager.Kind(devops.TypeCodeQualityTool).AddDependency(
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.CodeQualityTool)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	)

	// CodeQualityBinding
	DefaultManager.Kind(devops.TypeCodeQualityBinding).AddDependency(
		// Jenkins
		NewRequest(
			devops.TypeCodeQualityTool,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.CodeQualityBinding)
				if !ok {
					return false, "", ""
				}
				return true, "", target.Spec.CodeQualityTool.Name
			},
		),
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.CodeQualityBinding)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	).SetBinding(true)

	// CodeQualityProject
	DefaultManager.Kind(devops.TypeCodeQualityProject).AddDependency(
		NewRequest(
			devops.TypeCodeQualityBinding,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.CodeQualityProject)
				if !ok {
					return false, "", ""
				}
				return true, target.Namespace, target.Spec.CodeQualityBinding.Name
			},
		),
	)
}
