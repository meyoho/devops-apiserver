package dependency

import (
	"context"

	metainternalversion "k8s.io/apimachinery/pkg/apis/meta/internalversion"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

// Manager common interface for getting a dependency chain
type Manager interface {
	Get(ctx context.Context, kind string, name string) ItemList
	GetWithObject(ctx context.Context, kind string, obj runtime.Object) ItemList
	AddDependency(kind string, storage GetObjectFunc, request ...Request) Manager
	AddList(kind string, listFunc ListObjectFunc) Manager
	AddIntoFunc(kind string, intoFunc CopyIntoFunc) Manager
	SetAsBinding(kind string) Manager
	GetBindingKinds() []string
	List(ctx context.Context, kind string, opts *metainternalversion.ListOptions) (runtime.Object, error)
	Kind(string) Resolver
}

// Resolver resolver for dependency of a kind
type Resolver interface {

	// SetIntoFunc sets a function for copying objects function
	SetIntoFunc(CopyIntoFunc) Resolver
	// GetIntoFunc gets a function to copy instances of object
	GetIntoFunc() CopyIntoFunc

	// Get executes Get instruction
	Get(context.Context, string, *metav1.GetOptions) (runtime.Object, error)
	// UnshiftGetFunc sets a get function to the beginning
	UnshiftGetFunc(GetObjectFunc) Resolver
	// AddGetFunc add functions to the end of the list
	AddGetFunc(GetObjectFunc) Resolver
	// List executes list function
	List(context.Context, *metainternalversion.ListOptions) (runtime.Object, error)
	// UnshiftListFunc sets a list function to the beginning
	UnshiftListFunc(ListObjectFunc) Resolver
	// AddListFunc adds a list function to the end
	AddListFunc(ListObjectFunc) Resolver
	// SetBinding set binding property
	SetBinding(bool) Resolver
	// GetBinding get binding property
	GetBinding() bool
	// AddDependency add dependency to item
	AddDependency(...Request) Resolver
	// Dependencies return list of dependencies
	Dependencies() RequestList
}

// CopyIntoFunc custom copyInto function
type CopyIntoFunc func(from, to runtime.Object) error

// GetObjectFunc a function to get one specific object
type GetObjectFunc func(context.Context, string, *metav1.GetOptions) (runtime.Object, error)

// ListObjectFunc list of objects
type ListObjectFunc func(context.Context, *metainternalversion.ListOptions) (runtime.Object, error)
