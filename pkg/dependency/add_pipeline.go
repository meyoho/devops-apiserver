package dependency

import "alauda.io/devops-apiserver/pkg/apis/devops"

func init() {

	// Jenkins
	DefaultManager.Kind(devops.TypeJenkins).AddDependency(
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.Jenkins)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	)

	// JenkinsBinding
	DefaultManager.Kind(devops.TypeJenkinsBinding).AddDependency(
		// Jenkins
		NewRequest(
			devops.TypeJenkins,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.JenkinsBinding)
				if !ok {
					return false, "", ""
				}
				return true, "", target.Spec.Jenkins.Name
			},
		),
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.JenkinsBinding)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Account.Secret.Name != "", target.Spec.Account.Secret.Namespace, target.Spec.Account.Secret.Name
			},
		),
	).SetBinding(true)

	// PipelineConfig
	DefaultManager.Kind(devops.TypePipelineConfig).AddDependency(
		NewRequest(
			devops.TypeJenkinsBinding,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.PipelineConfig)
				if !ok {
					return false, "", ""
				}
				return true, target.Namespace, target.Spec.JenkinsBinding.Name
			},
		),
	)

	// Pipeline
	DefaultManager.Kind(devops.TypePipeline).AddDependency(
		NewRequest(
			devops.TypePipelineConfig,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.Pipeline)
				if !ok {
					return false, "", ""
				}
				return true, target.Namespace, target.Spec.PipelineConfig.Name
			},
		),
	)
}
