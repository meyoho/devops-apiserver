package dependency

import "alauda.io/devops-apiserver/pkg/apis/devops"

func init() {

	// TypeArtifactRegistryManager
	DefaultManager.Kind(devops.TypeArtifactRegistryManager).AddDependency(
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.ArtifactRegistryManager)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	)

	// TypeArtifactRegistry
	DefaultManager.Kind(devops.TypeArtifactRegistry).AddDependency(
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.ArtifactRegistry)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	)

	// TypeArtifactRegistryBinding
	DefaultManager.Kind(devops.TypeArtifactRegistryBinding).AddDependency(
		// Jenkins
		NewRequest(
			devops.TypeArtifactRegistry,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.ArtifactRegistryBinding)
				if !ok {
					return false, "", ""
				}
				return true, target.Namespace, target.Spec.ArtifactRegistry.Name
			},
		),
		// Secret
		NewRequest(
			devops.TypeSecret,
			func(item *Item) (apply bool, namespace, name string) {
				target, ok := item.Object.(*devops.ArtifactRegistryBinding)
				if !ok {
					return false, "", ""
				}
				return target.Spec.Secret.Name != "", target.Spec.Secret.Namespace, target.Spec.Secret.Name
			},
		),
	).SetBinding(true)
}
