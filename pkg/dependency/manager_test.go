package dependency_test

import (
	// "net/rpc"
	"context"
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"
	mockregistry "alauda.io/devops-apiserver/pkg/mock/apiserver/registry"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	genericapirequest "k8s.io/apiserver/pkg/endpoints/request"
)

var _ = Describe("Manager", func() {

	var (
		mockCtrl              *gomock.Controller
		getter                dependency.Manager
		pipelineStorage       *mockregistry.MockStandardStorage
		pipelineConfigStorage *mockregistry.MockStandardStorage
		jenkinsBindingStorage *mockregistry.MockStandardStorage
		kind                  string
		ctx                   context.Context
		name                  string
		namespace             string
		pipelineConfigName    string
		jenkinsBindingName    string
		result                dependency.ItemList
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		getter = dependency.NewManager()

		pipelineStorage = mockregistry.NewMockStandardStorage(mockCtrl)
		pipelineConfigStorage = mockregistry.NewMockStandardStorage(mockCtrl)

		getter.Kind("PipelineConfig").SetIntoFunc(func(in, out runtime.Object) error {
			source := in.(*devops.PipelineConfig)
			target := out.(*devops.PipelineConfig)
			source.DeepCopyInto(target)
			return nil
		})
		getter.Kind("Pipeline").SetIntoFunc(func(in, out runtime.Object) error {
			source := in.(*devops.Pipeline)
			target := out.(*devops.Pipeline)
			source.DeepCopyInto(target)
			return nil
		})
		getter.AddDependency("PipelineConfig", func(ctx context.Context, name string, opts *metav1.GetOptions) (runtime.Object, error) {
			return pipelineConfigStorage.Get(ctx, name, opts)
		})
		getter.AddDependency("Pipeline", func(ctx context.Context, name string, opts *metav1.GetOptions) (runtime.Object, error) {
			return pipelineStorage.Get(ctx, name, opts)
		}, dependency.Request{
			Kind: "PipelineConfig",
			GetNameNamespace: func(item *dependency.Item) (apply bool, namespace, name string) {
				if item == nil || item.Object == nil {
					return false, "", ""
				}
				pipeline := item.Object.(*devops.Pipeline)
				return true, pipeline.Namespace, pipeline.Spec.PipelineConfig.Name
			},
		})
		namespace = "namespace"
		name = "pipeline"
		kind = "Pipeline"
		pipelineConfigName = "pipelineConfig"
		ctx = genericapirequest.WithNamespace(genericapirequest.NewContext(), namespace)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Context("Get existing item", func() {
		It("should succeed", func() {
			pipelineStorage.EXPECT().
				Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"}).
				Return(&devops.Pipeline{
					ObjectMeta: metav1.ObjectMeta{
						Name:      name,
						Namespace: namespace,
					},
					Spec: devops.PipelineSpec{
						PipelineConfig: devops.LocalObjectReference{
							Name: pipelineConfigName,
						},
					},
				}, nil)
			pipelineConfigStorage.EXPECT().
				Get(ctx, pipelineConfigName, &metav1.GetOptions{ResourceVersion: "0"}).
				Return(&devops.PipelineConfig{
					ObjectMeta: metav1.ObjectMeta{
						Name:      pipelineConfigName,
						Namespace: namespace,
					},
				}, nil)

			result = getter.Get(ctx, kind, name)
			Expect(result).ToNot(BeNil())
			Expect(result).To(HaveLen(2))
			Expect(result).ToNot(
				ContainElement(
					WithTransform(
						func(item *dependency.Item) bool {
							return item.Error != nil
						}, BeTrue(),
					),
				),
			)
		})
	})

	Context("Get existing item by object", func() {
		It("should succeed", func() {
			pipelineConfigStorage.EXPECT().
				Get(ctx, pipelineConfigName, &metav1.GetOptions{ResourceVersion: "0"}).
				Return(&devops.PipelineConfig{
					ObjectMeta: metav1.ObjectMeta{
						Name:      pipelineConfigName,
						Namespace: namespace,
					},
				}, nil)

			object := &devops.Pipeline{
				ObjectMeta: metav1.ObjectMeta{
					Name:      name,
					Namespace: namespace,
				},
				Spec: devops.PipelineSpec{
					PipelineConfig: devops.LocalObjectReference{
						Name: pipelineConfigName,
					},
				},
			}
			result = getter.GetWithObject(ctx, kind, object)
			Expect(result).ToNot(BeNil())
			Expect(result).To(HaveLen(2))
			Expect(result).ToNot(
				ContainElement(
					WithTransform(
						func(item *dependency.Item) bool {
							return item.Error != nil
						}, BeTrue(),
					),
				),
			)
		})
	})

	Context("Get dependency does not exist", func() {
		It("should fail on second item", func() {
			pipelineStorage.EXPECT().
				Get(ctx, name, &metav1.GetOptions{ResourceVersion: "0"}).
				Return(&devops.Pipeline{
					ObjectMeta: metav1.ObjectMeta{
						Name:      name,
						Namespace: namespace,
					},
					Spec: devops.PipelineSpec{
						PipelineConfig: devops.LocalObjectReference{
							Name: pipelineConfigName,
						},
					},
				}, nil)
			pipelineConfigStorage.EXPECT().
				Get(ctx, pipelineConfigName, &metav1.GetOptions{ResourceVersion: "0"}).
				Return(nil, fmt.Errorf("Does not exist"))

			result = getter.Get(ctx, kind, name)
			Expect(result).ToNot(BeNil())
			Expect(result).To(HaveLen(2))
			Expect(result).To(
				ContainElement(
					WithTransform(
						func(item *dependency.Item) bool {
							return item.Error != nil && item.Kind == "PipelineConfig" && item.Object == nil
						}, BeTrue(),
					),
				),
			)
		})
	})

	Context("Get non existing kind", func() {
		BeforeEach(func() {
			kind = "NonExisting"
		})
		It("should return one failure", func() {
			result = getter.Get(ctx, kind, name)
			Expect(result).ToNot(BeNil())
			Expect(result).To(HaveLen(1))
			Expect(result[0].Error).ToNot(BeNil())
		})
	})

	Context("Different namespace", func() {
		var (
			secretNamespace string
			secretName      string
			secretStorage   *mockregistry.MockStandardStorage
		)

		BeforeEach(func() {
			secretNamespace = "secretNamespace"
			secretName = "secretName"

			secretStorage = mockregistry.NewMockStandardStorage(mockCtrl)
			jenkinsBindingStorage = mockregistry.NewMockStandardStorage(mockCtrl)

			getter.Kind("Secret").SetIntoFunc(func(in, out runtime.Object) error {
				source := in.(*corev1.Secret)
				target := out.(*corev1.Secret)
				source.DeepCopyInto(target)
				return nil
			})
			getter.Kind("JenkinsBinding").SetIntoFunc(func(in, out runtime.Object) error {
				source := in.(*devops.JenkinsBinding)
				target := out.(*devops.JenkinsBinding)
				source.DeepCopyInto(target)
				return nil
			})
			getter.AddDependency(devops.TypeJenkinsBinding, func(ctx context.Context, name string, opts *metav1.GetOptions) (runtime.Object, error) {
				return jenkinsBindingStorage.Get(ctx, name, opts)
			}, dependency.Request{
				Kind: devops.TypeSecret,
				GetNameNamespace: func(item *dependency.Item) (apply bool, namespace, name string) {
					if item == nil || item.Object == nil {
						return false, "", ""
					}
					jenkinsBinding := item.Object.(*devops.JenkinsBinding)
					secret := jenkinsBinding.Spec.Account.Secret
					return true, secret.Namespace, secret.Name
				},
			})
			getter.AddDependency(devops.TypeSecret, func(ctx context.Context, name string, opts *metav1.GetOptions) (runtime.Object, error) {
				return secretStorage.Get(ctx, name, opts)
			})
		})
		It("should be success", func() {
			differentCtx := genericapirequest.WithNamespace(genericapirequest.NewContext(), secretNamespace)

			jenkinsBinding := &devops.JenkinsBinding{
				Spec: devops.JenkinsBindingSpec{
					Account: devops.UserAccount{
						Secret: devops.SecretKeySetRef{
							SecretReference: corev1.SecretReference{
								Namespace: secretNamespace,
								Name:      secretName,
							},
						},
					},
				},
			}
			jenkinsBindingStorage.EXPECT().
				Get(ctx, jenkinsBindingName, &metav1.GetOptions{ResourceVersion: "0"}).
				Return(jenkinsBinding, nil)

			secret := &corev1.Secret{}
			secretStorage.EXPECT().
				Get(differentCtx, secretName, &metav1.GetOptions{ResourceVersion: "0"}).
				Return(secret, nil)

			result = getter.Get(ctx, devops.TypeJenkinsBinding, jenkinsBindingName)
			Expect(result).ToNot(BeNil())
			Expect(result).To(HaveLen(2))
			Expect(result[0].Error).To(BeNil())
			Expect(result[0].Object).To(Equal(jenkinsBinding))
			Expect(result[1].Error).To(BeNil())
			Expect(result[1].Object).To(Equal(secret))
		})
	})
})
