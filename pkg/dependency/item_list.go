package dependency

import (
	"fmt"

	"k8s.io/apimachinery/pkg/runtime"
	glog "k8s.io/klog"
)

// Item represent a dependency item
type Item struct {
	Kind   string
	Object runtime.Object
	/**/ IntoFunc CopyIntoFunc
	Error         error
}

// ItemList represent a list of Item list
type ItemList []*Item

// Get get Dependency item from a kind
func (itemList ItemList) Get(kind string) (item *Item) {
	if itemList == nil {
		return
	}

	for _, i := range itemList {
		if i.Kind == kind {
			item = i
			break
		}
	}
	return
}

// Validate returns the first error found, otherwise will return nnil
func (itemList ItemList) Validate() (err error) {
	if itemList == nil {
		return
	}
	for _, i := range itemList {
		if i.Error != nil {
			err = i.Error
			break
		}
	}
	return
}

// GetInto get a dependency by kind and inserts values into the target object
// if the object is not found or there is no IntoFunc associated will not do anything
// returns itself for a easier chain operation
func (itemList ItemList) GetInto(kind string, target runtime.Object) ItemList {
	item := itemList.Get(kind)

	if item == nil || item.IntoFunc == nil || item.Object == nil {
		glog.Errorf("kind '%s' in ItemList  is invalid, itemList:%#v, item:%#v", kind, itemList, item)
		return itemList
	}

	if err := item.IntoFunc(item.Object, target); err != nil {
		item.Error = fmt.Errorf("Error happend when insert value into target object and err is %v", err)
		glog.Error(item.Error.Error())
	}

	return itemList
}
