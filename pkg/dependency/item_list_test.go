package dependency_test

import (
	"errors"
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/dependency"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

var _ = Describe("ItemList", func() {

	var (
		list   dependency.ItemList
		kind   string
		target = &devops.Pipeline{}
		result *dependency.Item
		err    error
	)

	BeforeEach(func() {
		list = dependency.ItemList{
			&dependency.Item{
				Kind: "Pipeline",
				Object: &devops.Pipeline{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "pipeline",
						Namespace: "namespace",
					},
				},
				IntoFunc: func(in, out runtime.Object) (err error) {
					source := in.(*devops.Pipeline)
					target := out.(*devops.Pipeline)
					source.DeepCopyInto(target)
					return
				},
				Error: nil,
			},
		}
		kind = "Pipeline"
	})

	JustBeforeEach(func() {
		result = list.Get(kind)
		err = list.Validate()
		list.GetInto(kind, target)
	})

	Context("Get existing item", func() {
		It("should return item", func() {
			By("Get response")
			Expect(result).ToNot(BeNil())
			Expect(result.Kind).To(Equal("Pipeline"))
			Expect(result.IntoFunc).ToNot(BeNil())
			Expect(result.Error).To(BeNil())

			By("Validate response")
			Expect(err).To(BeNil())

			By("Get Into")
			Expect(target).ToNot(BeNil())
			object := list[0].Object.(*devops.Pipeline)
			Expect(target.ObjectMeta).To(Equal(object.ObjectMeta))
		})
	})

	Context("List is nil", func() {
		BeforeEach(func() {
			list = nil
		})
		It("should return nil", func() {
			By("Get response")
			Expect(result).To(BeNil())

			By("Validate response")
			Expect(err).To(BeNil())
		})
	})

	Context("List has error", func() {
		BeforeEach(func() {
			list = dependency.ItemList{
				&dependency.Item{
					Kind:  "Pipeline",
					Error: fmt.Errorf("list error"),
				},
			}
		})
		It("should return error", func() {
			By("Get response")
			Expect(result).ToNot(BeNil())
			Expect(result.Error).To(Equal(fmt.Errorf("list error")))

			By("Validate response")
			Expect(err).To(Equal(fmt.Errorf("list error")))
		})
	})

	Context("GetInto with error", func() {
		BeforeEach(func() {
			list = dependency.ItemList{
				&dependency.Item{
					Kind: "Pipeline",
					Object: &devops.Pipeline{
						ObjectMeta: metav1.ObjectMeta{
							Name:      "pipeline",
							Namespace: "namespace",
						},
					},
					IntoFunc: func(from, to runtime.Object) error {
						return errors.New("some error")
					},
				},
			}
		})

		It("should return list", func() {
			Expect(result).ToNot(BeNil())
			Expect(err).To(BeNil(), "should not return an error")
			Expect(result.Error).ToNot(BeNil(), "should return an error")
		})
	})
})
