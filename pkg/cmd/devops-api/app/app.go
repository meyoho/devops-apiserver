package app

import (
	"alauda.io/devops-apiserver/pkg/cmd/devops-api/app/options"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/app"
)

const commandDesc = `The application is a WEB server. It provides advanced APIs for the devops frontend application.`

// NewApp creates a new alauda-console app
func NewApp(name string) *app.App {
	opts := options.NewOptions(name)
	application := app.NewApp("DevOps API",
		name,
		app.WithOptions(opts),
		app.WithDescription(commandDesc),
		app.WithRunFunc(run(opts)),
	)
	return application
}

func run(opts *options.Options) app.RunFunc {
	return func(basename string) error {
		srv := server.New(basename)
		err := opts.ApplyToServer(srv)
		if err != nil {
			return err
		}
		srv.Start()
		return nil
	}
}
