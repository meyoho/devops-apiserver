package options

import (
	// importing this package to load all init functions and registry all handlers
	_ "alauda.io/devops-apiserver/pkg/restapi/handler"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server/options"
)

// Options options for alauda-console
type Options struct {
	options.Optioner
}

// NewOptions new options for alauda-console
// currently uses the recommended options but can be changed to add new options as necessary
func NewOptions(name string) *Options {
	return &Options{
		Optioner: options.NewRecommendedOptions().Add(NewBaseOptions()),
	}
}
