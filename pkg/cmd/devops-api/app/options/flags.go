package options

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"strings"

	"alauda.io/devops-apiserver/pkg/util"

	"alauda.io/devops-apiserver/pkg/restapi/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/emicklei/go-restful"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	flagSystemNamespace    = "namespace"
	configSystemNamespace  = "alauda.system.namespace"
	defaultSystemNamespace = "alauda-system"

	defaultCredentialsNamespace = "global-credentials"
	FlagBaseDomain              = "basedomain"
	defaultBaseDomain           = "alauda.io"
)

type BaseOptions struct {
	SystemNamespace      string
	CredentialsNamespace string
	BaseDomain           string
}

func NewBaseOptions() *BaseOptions {
	return &BaseOptions{}
}

// AddFlags adds flags related to debugging for controller manager to the specified FlagSet.
func (o *BaseOptions) AddFlags(fs *pflag.FlagSet) {
	if o == nil {
		return
	}

	fs.String(flagSystemNamespace, defaultSystemNamespace,
		"Used to specify the default namespace (formerly alauda-system).")
	_ = viper.BindPFlag(configSystemNamespace, fs.Lookup(flagSystemNamespace))

	fs.String(FlagBaseDomain, defaultBaseDomain,
		"Used to specify the basedomain (formerly alauda.io).")
	_ = viper.BindPFlag(FlagBaseDomain, fs.Lookup(FlagBaseDomain))

}

// ApplyFlags parsing parameters from the command line or configuration file
// to the options instance.
func (o *BaseOptions) ApplyFlags() []error {
	var errs []error

	o.SystemNamespace = strings.TrimSpace(viper.GetString(configSystemNamespace))
	if o.SystemNamespace == "" {
		o.SystemNamespace = defaultSystemNamespace
	}

	o.CredentialsNamespace = util.GenCredentialsNamespace(o.SystemNamespace)

	o.BaseDomain = strings.TrimSpace(viper.GetString(FlagBaseDomain))
	if o.BaseDomain == "" {
		o.BaseDomain = defaultBaseDomain
	}

	return errs
}

// ApplyToServer apply options to server
func (o *BaseOptions) ApplyToServer(server server.Server) (err error) {
	extraOpts := &context.ExtraOptions{
		SystemNamespace:      o.SystemNamespace,
		CredentialsNamespace: o.CredentialsNamespace,
		AnnotationProvider:   devops.NewAnnotationProvider(o.BaseDomain),
	}

	server.Container().Filter(func(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
		req.Request = req.Request.WithContext(context.WithExtraConfig(req.Request.Context(), extraOpts))

		// disable pretty print to improve performance
		res.PrettyPrint(req.QueryParameter("pretty") == "true")
		chain.ProcessFilter(req, res)
	})
	return nil
}
