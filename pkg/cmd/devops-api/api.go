package main

import (
	"math/rand"
	"os"
	"runtime"
	"time"

	"alauda.io/devops-apiserver/pkg/cmd/devops-api/app"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	if len(os.Getenv("GOMAXPROCS")) == 0 {
		runtime.GOMAXPROCS(runtime.NumCPU())
	}
	app.NewApp("devops-api").Run()
}
