package cmd

import (
	"io"
	"os"

	"github.com/spf13/cobra"
	genericapiserver "k8s.io/apiserver/pkg/server"
	glog "k8s.io/klog"
)

// Devops basic class to hold the root command and other shared variables
type Devops struct {
	RootCmd *cobra.Command
	StdOut  io.Writer
	StdErr  io.Writer
	StopCh  <-chan struct{}
}

var (
	command = &Devops{
		RootCmd: &cobra.Command{
			Use:   "devops-apiserver",
			Short: "devops-apiserver root command doesn't have any function",
			Long:  `use --help flag to get more details`,
		},
		StdErr: os.Stderr,
		StdOut: os.Stdout,
	}
)

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	command.StopCh = genericapiserver.SetupSignalHandler()
	if err := command.RootCmd.Execute(); err != nil {
		glog.Fatal(err)
	}
}
