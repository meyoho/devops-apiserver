package cmd

import (
	"flag"
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/cmd/options"
	"alauda.io/devops-apiserver/pkg/controller"
	devopsgc "alauda.io/devops-apiserver/pkg/controller/devops/gc"
	"alauda.io/devops-apiserver/pkg/controller/manager"
	"alauda.io/devops-apiserver/pkg/util/generic"
	"github.com/spf13/cobra"
	configv1alpha1 "k8s.io/apimachinery/pkg/apis/config/v1alpha1"
	utilerrors "k8s.io/apimachinery/pkg/util/errors"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	glog "k8s.io/klog"
)

const (
	defaultLeaderElect                 = true
	defaultLeaderElectionNamespace     = "alauda-system"
	defaultLeaderElectionID            = "devops-config"
	defaultLeaderElectionLeaseDuration = 60 * time.Second
	defaultLeaderElectionRenewDeadline = 40 * time.Second
	defaultLeaderElectionRetryPeriod   = 15 * time.Second
	defaultSyncPeriod                  = 60 * time.Second
	controllerAgentName                = "devops-controller"
	defaultConcurrentSyncs             = 2
	defaultClientQPS                   = 150
	defaultClientBurst                 = 100
)

const (
	defaultEnableDevOpsTemplateGC = false
)

func init() {
	// Add root as parent
	command.RootCmd.AddCommand(ctrlManagerCmd)

	ctrlManagerOptions = NewControllerManagerOptions()
	ctrlExtraConfig = options.NewExtraConfig()
	ctrlManagerCmd.Flags().AddGoFlagSet(flag.CommandLine)
	flags := ctrlManagerCmd.Flags()
	flags.StringVarP(&ctrlManagerOptions.MasterURL, "master", "", "", "The address of the Kubernetes API server. Overrides any value in kubeconfig. Only required if out-of-cluster.")
	flags.StringVarP(&ctrlManagerOptions.Kubeconfig, "kubeconfig", "", "", "Path to a kubeconfig. Only required if out-of-cluster.")
	flags.DurationVarP(&ctrlManagerOptions.Timeout, "timeout", "t", time.Second*30, "Timeout duration while waiting for listers to fetch cache")
	flags.BoolVar(&ctrlManagerOptions.LeaderElect, "leader-elect", true, ""+
		"If true, cert-manager will perform leader election between instances to ensure no more "+
		"than one instance of cert-manager operates at a time")
	flags.StringVar(&ctrlManagerOptions.LeaderElectionNamespace, "leader-election-namespace", defaultLeaderElectionNamespace, ""+
		"Namespace used to perform leader election. Only used if leader election is enabled")
	flags.StringVar(&ctrlManagerOptions.LeaderElectionID, "leader-election-id", defaultLeaderElectionID, ""+
		"Name of the configmap resource that will store the leader election data.")
	flags.DurationVar(&ctrlManagerOptions.LeaderElectionLeaseDuration, "leader-election-lease-duration", defaultLeaderElectionLeaseDuration, ""+
		"The duration that non-leader candidates will wait after observing a leadership "+
		"renewal until attempting to acquire leadership of a led but unrenewed leader "+
		"slot. This is effectively the maximum duration that a leader can be stopped "+
		"before it is replaced by another candidate. This is only applicable if leader "+
		"election is enabled.")
	flags.DurationVar(&ctrlManagerOptions.LeaderElectionRenewDeadline, "leader-election-renew-deadline", defaultLeaderElectionRenewDeadline, ""+
		"The interval between attempts by the acting master to renew a leadership slot "+
		"before it stops leading. This must be less than or equal to the lease duration. "+
		"This is only applicable if leader election is enabled.")
	flags.DurationVar(&ctrlManagerOptions.LeaderElectionRetryPeriod, "leader-election-retry-period", defaultLeaderElectionRetryPeriod, ""+
		"The duration the clients should wait between attempting acquisition and renewal "+
		"of a leadership. This is only applicable if leader election is enabled.")
	flags.Int32Var(&ctrlManagerOptions.ConcurrentSyncs, "concurrent-syncs", defaultConcurrentSyncs, ""+
		"The number of concurrent syncs for each controller.")
	flags.DurationVar(&ctrlManagerOptions.SyncPeriod, "sync-period", defaultSyncPeriod, ""+
		"The duration cache will be resynced and reconciles re-triggered. "+
		"This affects how consistently the data is been looped. The shorter the period the more load the system will have.")
	flags.Float32Var(&ctrlManagerOptions.ClientConnection.QPS, "kube-api-qps", defaultClientQPS, "QPS to use while talking with kubernetes apiserver.")
	flags.Int32Var(&ctrlManagerOptions.ClientConnection.Burst, "kube-api-burst", defaultClientBurst, "Burst to use while talking with kubernetes apiserver.")
	flags.StringVar(&ctrlManagerOptions.MultiClusterHost, "multi-clusterhost", "https://erebus:32011", "It is the endpoint of the Erebus")
	flags.BoolVar(&devopsgc.DefaultDevOpsGCOptions.EnableTemplateGC, "enable-devops-template-gc", defaultEnableDevOpsTemplateGC, "It is enable devops template gc")

	flags.StringVar(&ctrlManagerOptions.MetricsBindAddress, "--serving-bind-address", "0.0.0.0:8081", "address that the controller should bind to for serving prometheus metrics or profiling")

	ctrlExtraConfig.AddFlags(flags)
}

var (
	ctrlManagerOptions *ControllerManagerOptions
	ctrlExtraConfig    *options.ExtraConfig
	ctrlManagerCmd     = &cobra.Command{
		Use:   "controller-manager",
		Short: "Launch devops controller manager",
		Long:  "Launch devops controller to start reconciling over DevOps resources",
		RunE: func(c *cobra.Command, args []string) error {
			glog.Infof("%s", c.Short)
			if err := ctrlManagerOptions.Validate(args); err != nil {
				return err
			}
			if err := ctrlManagerOptions.Complete(); err != nil {
				return err
			}

			if err := utilerrors.NewAggregate(ctrlExtraConfig.ApplyFlags()); err != nil {
				return err
			}

			return ctrlManagerOptions.Run(command.StopCh)
		},
	}
)

// ControllerManagerOptions options for the controller-manager
type ControllerManagerOptions struct {
	Kubeconfig string
	MasterURL  string
	Timeout    time.Duration

	LeaderElect             bool
	LeaderElectionNamespace string
	LeaderElectionID        string
	// currently not supported by the manager.Manager
	LeaderElectionLeaseDuration time.Duration
	LeaderElectionRenewDeadline time.Duration
	LeaderElectionRetryPeriod   time.Duration

	// Added for our own usage
	ConcurrentSyncs  int32
	ClientConfig     *restclient.Config
	ClientConnection configv1alpha1.ClientConnectionConfiguration
	SyncPeriod       time.Duration

	// MetricsBindAddress is the TCP address that the controller should bind to
	// for serving prometheus metrics
	MetricsBindAddress string

	// It is the endpoint of the multi cluster proxy , eg. alauda erebus
	MultiClusterHost string
	// KubeClient
}

// NewControllerManagerOptions constructor function for the controller manager options
func NewControllerManagerOptions() *ControllerManagerOptions {
	opts := &ControllerManagerOptions{}
	configv1alpha1.RecommendedDefaultClientConnectionConfiguration(&opts.ClientConnection)
	return opts
}

// Validate validates arguments passed to the command and returns an error if any
func (opts *ControllerManagerOptions) Validate(args []string) error {
	errors := []error{}
	return utilerrors.NewAggregate(errors)
}

// Complete completes the configuration initializing shared objects
func (opts *ControllerManagerOptions) Complete() (err error) {
	if opts.ClientConfig, err = clientcmd.BuildConfigFromFlags(opts.MasterURL, opts.Kubeconfig); err != nil {
		err = fmt.Errorf("Error building config from flags: %+v", err)
		return
	}
	opts.ClientConfig.QPS = ctrlManagerOptions.ClientConnection.QPS
	opts.ClientConfig.Burst = int(ctrlManagerOptions.ClientConnection.Burst)

	return
}

// Run starts the controller manager
func (opts *ControllerManagerOptions) Run(stopChan <-chan struct{}) (err error) {
	var (
		mgr manager.Manager
	)

	// Create a new controller-manager with leader election, if set
	glog.V(7).Info("Starting manager...")

	if mgr, err = manager.New(opts.ClientConfig, manager.Options{
		LeaderElection:          opts.LeaderElect,
		LeaderElectionNamespace: opts.LeaderElectionNamespace,
		LeaderElectionID:        opts.LeaderElectionID,
		MaxConcurrentReconciles: int(opts.ConcurrentSyncs),
		SyncPeriod:              &opts.SyncPeriod,
		RoundTripper:            generic.GetDefaultTransport(),
		MultiClusterHost:        opts.MultiClusterHost,
		MetricsBindAddress:      opts.MetricsBindAddress,
	}, manager.ExtraConfig{
		SystemNamespace:      ctrlExtraConfig.SystemNamespace,
		CredentialsNamespace: ctrlExtraConfig.CredentialsNamespace,
		AnnotationProvider:   v1alpha1.NewAnnotationProvider(ctrlExtraConfig.BaseDomain),
	}); err != nil {
		err = fmt.Errorf("Error starting manager: %+s", err.Error())
		return
	}

	// adding to managers scheme
	glog.V(7).Info("Adding Scheme to manager...")
	if err = v1alpha1.AddToScheme(mgr.GetScheme()); err != nil {
		err = fmt.Errorf("Error adding to scheme: %+s", err.Error())
		return
	}

	// Adds all to manager
	glog.V(7).Info("Injecting controllers to manager...")
	if err = controller.AddToManager(mgr); err != nil {
		err = fmt.Errorf("Error injecting to manager: %+s", err.Error())
		return
	}

	// for health check
	go startSimpleHTTPServer()

	// starting controller manager
	err = mgr.Start(stopChan)
	if err != nil {
		err = fmt.Errorf("Error starting controller manager: %+s", err.Error())
		return
	}

	return nil
}
