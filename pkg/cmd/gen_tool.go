package cmd

import (
	"fmt"
	"go/build"
	"go/parser"
	"go/token"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	corev1 "k8s.io/api/core/v1"

	"sort"

	"alauda.io/devops-apiserver/pkg/toolchain"
	"github.com/spf13/cobra"
)

func init() {
	genCommand.AddCommand(genToolChainCommand)

	flags := genToolChainCommand.Flags()
	flags.BoolVarP(&toolChainOpts.PrintOnly, "print", "", false, "Print only.")
	flags.StringVarP(&toolChainOpts.FilePath, "output", "o", "zz_generated.toolchain.go", "File path for generated file.")
	flags.StringVarP(&toolChainOpts.Package, "package", "p", "", "Package to be used for the generated file.")

}

const (
	toolchainTagName = "alauda:toolchain-gen"
)

var genToolChainCommand = &cobra.Command{
	Use:   "tool",
	Short: "Generate toolchain related code",
	RunE: func(c *cobra.Command, args []string) (err error) {
		if err = toolChainOpts.Validate(); err != nil {
			return
		}
		err = generateFile(toolChainOpts)
		return
	},
}

var toolChainOpts toolChainCommandOpts

type toolChainCommandOpts struct {
	PrintOnly bool
	FilePath  string
	Package   string
}

func (t toolChainCommandOpts) Validate() error {
	if t.FilePath == "" {
		return fmt.Errorf("Need an output file")
	}
	if t.Package == "" {
		return fmt.Errorf("Need an package name")
	}
	return nil
}

func generateFile(opts toolChainCommandOpts) error {
	comments, err := getCommentsOnPackage(toolChainOpts.Package, toolchainTagName)
	if len(comments) > 0 {
		rawData := getDataFromComments(comments, toolchainTagName)
		data := getDataFromMap(rawData)
		splitted := strings.Split(opts.Package, "/")
		data.Package = splitted[len(splitted)-1]
		err = renderFile("toolchain", templateFile, opts.FilePath, data)
	}
	return err
}

func getCommentsOnPackage(pkgName, tagName string) (comments []string, err error) {
	// build.New
	cwd, err := os.Getwd()
	if err != nil {
		err = fmt.Errorf("unable to get current directory: %v", err)
		return
	}
	context := build.Default
	pkg, err := context.Import(pkgName, cwd, build.ImportComment)
	if err != nil {
		return
	}
	comments = make([]string, 0, 100)
	if pkg == nil || len(pkg.GoFiles) == 0 {
		return
	}

	fileSet := token.NewFileSet()
	for _, n := range pkg.GoFiles {
		if !strings.HasSuffix(n, ".go") {
			continue
		}
		absPath := filepath.Join(pkg.Dir, n)
		data, err := ioutil.ReadFile(absPath)
		if err != nil {
			fmt.Println("Error reading file", absPath, "err", err)
			continue
		}
		gofile, err := parser.ParseFile(fileSet, absPath, data, parser.DeclarationErrors|parser.ParseComments)
		if err != nil {
			fmt.Println("Error evaluating", absPath, "err", err)
			continue
		}
		if gofile != nil && gofile.Comments != nil {
			for _, c := range gofile.Comments {
				for _, subcomm := range c.List {
					if !strings.Contains(subcomm.Text, tagName) {
						continue
					}
					index := strings.LastIndex(subcomm.Text, tagName)
					trimmed := subcomm.Text[index:]
					comments = append(comments, trimmed)

				}
			}
		}
	}
	sort.Strings(comments)
	return
}

func getDataFromComments(comments []string, tagName string) []map[string]string {
	// transform comments from data
	values := []map[string]string{}
	for _, c := range comments {
		c = strings.TrimPrefix(c, tagName+":")
		tags := strings.Split(c, ",")
		value := map[string]string{}
		for _, t := range tags {
			kv := strings.Split(t, "=")
			if len(kv) == 2 {
				value[kv[0]] = kv[1]
			}
		}
		values = append(values, value)
	}
	return values
}

const (
	classKey           = "class"
	classCategoryValue = "category"
	classSecretValue   = "secret"
	classPanelValue    = "panel"
	classItemValue     = "item"
	categoryKey        = classCategoryValue
	nameKey            = "name"
	indexKey           = "index"
	englishKey         = "en"
	chineseKey         = "zh"
	apiPathKey         = "apipath"
	enabledKey         = "enabled"
	kindKey            = "kind"
	typeKey            = "type"
	apiAccessURIKey    = "api"
	webAccessURIKey    = "web"
	enterpriseKey      = "enterprise"
	publicKey          = "public"
	itemNameKey        = "itemName"
	roleSyncEnabledKey = "roleSyncEnabled"
)

// provide sorting for categories
type categoriesOrdered struct {
	categorys []*toolchain.Category
	indexBy   []int
}

func (c categoriesOrdered) Len() int {
	return len(c.categorys)
}

func (c categoriesOrdered) Swap(i, j int) {
	c.categorys[i], c.categorys[j] = c.categorys[j], c.categorys[i]
	c.indexBy[i], c.indexBy[j] = c.indexBy[j], c.indexBy[i]
}

func (c categoriesOrdered) Less(i, j int) bool {
	return c.indexBy[i] < c.indexBy[j]
}

func initCategoriesOrdered(data []*toolchain.Category, index []int) categoriesOrdered {
	return categoriesOrdered{
		categorys: data,
		indexBy:   index,
	}
}

func getDataFromMap(values []map[string]string) *render {
	categories := []*toolchain.Category{}
	indexByCategories := []int{}
	items := map[string][]*toolchain.Item{}
	// category -> itemName -> secretTypes
	secrets := map[string]map[string][]*toolchain.ItemSecretType{}
	panels := []toolchain.PanelItem{}
	for _, v := range values {
		enabled, _ := strconv.ParseBool(v[enabledKey])
		public, _ := strconv.ParseBool(v[publicKey])
		enterprise, _ := strconv.ParseBool(v[enterpriseKey])
		roleSyncEnabled, _ := strconv.ParseBool(v[roleSyncEnabledKey])
		class := v[classKey]
		switch class {
		case classCategoryValue:
			indexVal, _ := strconv.Atoi(v[indexKey])
			indexByCategories = append(indexByCategories, indexVal)
			categories = append(categories, &toolchain.Category{
				Name: v[nameKey],
				DisplayName: toolchain.DisplayName{
					English: v[englishKey],
					Chinese: v[chineseKey],
				},
				APIPath: v[apiPathKey],
				Enabled: enabled,
			})
		case classItemValue:
			categoryName := v[categoryKey]
			categoryItems, ok := items[categoryName]
			if !ok {
				categoryItems = []*toolchain.Item{}
			}
			categoryItems = append(categoryItems, &toolchain.Item{
				Name: v[nameKey],
				DisplayName: toolchain.DisplayName{
					English: v[englishKey],
					Chinese: v[chineseKey],
				},
				APIPath:         v[apiPathKey],
				Enabled:         enabled,
				Kind:            v[kindKey],
				Type:            v[typeKey],
				Public:          public,
				Enterprise:      enterprise,
				APIAccessURI:    v[apiAccessURIKey],
				WebAccessURI:    v[webAccessURIKey],
				RoleSyncEnabled: roleSyncEnabled,
			})
			items[categoryName] = categoryItems
		case classSecretValue:
			categoryName := v[categoryKey]
			categoryItemNames, ok := secrets[categoryName]
			if !ok {
				categoryItemNames = make(map[string][]*toolchain.ItemSecretType)
			}

			itemNames := strings.Split(v[itemNameKey], "+")
			for _, itemName := range itemNames {
				secretTypes, ok := categoryItemNames[itemName]
				if !ok {
					secretTypes = []*toolchain.ItemSecretType{}
				}
				secretTypes = append(secretTypes, &toolchain.ItemSecretType{
					Type: corev1.SecretType(v[typeKey]),
					Description: toolchain.Description{
						Chinese: v[chineseKey],
						English: v[englishKey],
					},
				})
				categoryItemNames[itemName] = secretTypes
			}
			secrets[categoryName] = categoryItemNames
		case classPanelValue:
			categories := strings.Split(v[categoryKey], ":")
			indexVal, _ := strconv.Atoi(v[indexKey])
			panel := toolchain.PanelItem{
				Name: toolchain.DisplayName{
					English: v[englishKey],
					Chinese: v[chineseKey],
				},
				Index:      indexVal,
				Categories: categories,
			}
			panels = append(panels, panel)
		}
	}

	// sort by index
	sort.Sort(initCategoriesOrdered(categories, indexByCategories))

	// removing not found categories
	notFound := []string{}
	for k := range items {
		found := false
		for _, c := range categories {
			if c.Name == k {
				found = true
			}
		}
		if !found {
			notFound = append(notFound, k)
		}
	}
	for _, not := range notFound {
		delete(items, not)
	}
	return &render{
		Categories:  categories,
		Items:       items,
		Panel:       toolchain.NewPanel().AddPanel(panels...),
		SecretTypes: secrets,
	}
}

type render struct {
	Package     string
	Categories  []*toolchain.Category
	Items       map[string][]*toolchain.Item
	Panel       *toolchain.Panel
	SecretTypes map[string]map[string][]*toolchain.ItemSecretType
}

const (
	templateFile = `// ATTENTION!
// This file was autogenerated by pkg/cmd/gen_tool.go Do not edit it manually!
package {{.Package}}

import (
	"alauda.io/devops-apiserver/pkg/toolchain"
)

// ToolScheme public global variable for schema
var ToolScheme = GetScheme()

// GetScheme get toolchain schema
func GetScheme() *toolchain.Scheme {
	scheme := toolchain.NewScheme()

	// Categories
	{{- range $i, $cat := .Categories }}
	// {{$i}}: {{$cat.Name}}
	{{$cat.Name}} := &toolchain.Category{
		Name: "{{$cat.Name}}",
		DisplayName: toolchain.DisplayName{
			English: "{{$cat.DisplayName.English}}",
			Chinese: "{{$cat.DisplayName.Chinese}}",
		},
		Enabled: {{$cat.Enabled}},
		APIPath: "{{$cat.APIPath}}",
	}
	{{- end }}

	// Items
	{{- range $cat, $items := .Items }}
	// {{$cat}}
	{{- range $i, $item := $items }}
	// {{$i}}: {{$item.Name}}
	scheme.Add({{$cat}}, &toolchain.Item{
		Name: "{{$item.Name}}",
		DisplayName: toolchain.DisplayName{
			English: "{{$item.DisplayName.English}}",
			Chinese: "{{$item.DisplayName.Chinese}}",
		},
		APIPath: "{{$item.APIPath}}",
		Kind: "{{$item.Kind}}",
		Type: "{{$item.Type}}",
		APIAccessURI: "{{$item.APIAccessURI}}",
		WebAccessURI: "{{$item.WebAccessURI}}",
		Enabled: {{$item.Enabled}},
		Enterprise: {{$item.Enterprise}},
		Public: {{$item.Public}},
		RoleSyncEnabled: {{$item.RoleSyncEnabled}},
        SupportedSecretTypes: []toolchain.ItemSecretType {
    {{- range $secret := index $.SecretTypes $cat $item.Name }}
            {
                Type: "{{$secret.Type}}",
				Description: toolchain.Description{
					English: "{{$secret.Description.English}}",
					Chinese: "{{$secret.Description.Chinese}}",
                },
            },
    {{- end }}
        },
	})
	{{- end }}
	{{- end }}

	// Panel
	{{- if .Panel}}
	{{- range $i, $panel := .Panel.Panels }}
	scheme.AddPanel(toolchain.NewPanelItem(
		"{{$panel.Name.English}}", "{{$panel.Name.Chinese}}", {{$panel.Index}},
		{{ range $x, $category := $panel.Categories }} "{{$category}}", {{ end }}
	))
	{{- end }}
	{{- end }}

	return scheme
}
`
)
