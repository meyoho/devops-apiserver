package cmd

import (
	"bytes"
	"io/ioutil"
	"os"
	"text/template"

	"github.com/spf13/cobra"
)

func init() {
	command.RootCmd.AddCommand(genCommand)
}

var genCommand = &cobra.Command{
	Use:   "gen",
	Short: "Generate code command",
}

func renderFile(templateName, templateContent, filePath string, data interface{}) (err error) {
	t := template.New(templateName)
	t, err = t.Parse(templateContent)
	if err != nil {
		return
	}
	bytt := new(bytes.Buffer)
	if err = t.Execute(bytt, data); err != nil {
		return
	}
	err = ioutil.WriteFile(filePath, bytt.Bytes(), os.ModePerm)
	return
}
