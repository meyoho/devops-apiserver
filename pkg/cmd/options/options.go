package options

import (
	"alauda.io/devops-apiserver/pkg/util"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	flagSystemNamespace         = "namespace"
	defaultSystemNamespace      = "alauda-system"
	defaultCredentialsNamespace = "global-credentials"
	FlagBaseDomain              = "basedomain"
	defaultBaseDomain           = "alauda.io"
)

type ExtraConfig struct {
	SystemNamespace      string
	CredentialsNamespace string
	BaseDomain           string
}

func NewExtraConfig() *ExtraConfig {
	return &ExtraConfig{}
}

func (o *ExtraConfig) AddFlags(fs *pflag.FlagSet) {
	if o == nil {
		return
	}

	fs.StringVar(&o.SystemNamespace, flagSystemNamespace, defaultSystemNamespace,
		"Used to specify the default system namespace (formerly alauda-system).")

	fs.StringVar(&o.BaseDomain, FlagBaseDomain, defaultBaseDomain,
		"Used to specify the basedomain (formerly alauda.io).")

	_ = viper.BindPFlag(FlagBaseDomain, fs.Lookup(FlagBaseDomain))
}

func (o *ExtraConfig) ApplyFlags() []error {
	var errs []error
	if o.SystemNamespace == "" {
		o.SystemNamespace = defaultSystemNamespace
	}
	if o.BaseDomain == "" {
		o.SystemNamespace = defaultBaseDomain
	}
	o.CredentialsNamespace = util.GenCredentialsNamespace(o.SystemNamespace)
	return errs
}

// ApplyToServer apply options to server
func (o *ExtraConfig) ApplyToServer(server server.Server) (err error) {
	return nil
}
