/*
Copyright 2016 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cmd

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"

	"flag"
	"fmt"
	"io"
	"net"
	"net/http"
	_ "net/http/pprof"
	"runtime/debug"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/admission/plugin/components"
	"alauda.io/devops-apiserver/pkg/cmd/options"
	"alauda.io/devops-apiserver/pkg/dependency"

	devopsadmission "alauda.io/devops-apiserver/pkg/admission"
	"alauda.io/devops-apiserver/pkg/admission/devopsinitializer"
	"alauda.io/devops-apiserver/pkg/admission/plugin/banproject"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/apiserver"
	clientset "alauda.io/devops-apiserver/pkg/client/clientset/internalversion"
	informers "alauda.io/devops-apiserver/pkg/client/informers/internalversion"
	"github.com/spf13/cobra"
	utilerrors "k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/apiserver/pkg/admission"
	genericapiserver "k8s.io/apiserver/pkg/server"
	genericoptions "k8s.io/apiserver/pkg/server/options"
	"k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
)

func init() {
	// Add root as parent
	command.RootCmd.AddCommand(serverCmd)
	serverExtraConfig = options.NewExtraConfig()
	// needs to do this on init because otherwise cobra will
	// fail saying that the added flags are not valid
	serverOpts = NewDevopsServerOptions(command.StdOut, command.StdErr)
	serverCmd.Flags().AddGoFlagSet(flag.CommandLine)
	flags := serverCmd.Flags()
	flags.StringSliceVar(&serverOpts.RecommendedOptions.Admission.EnablePlugins, "admission-control", serverOpts.RecommendedOptions.Admission.EnablePlugins, ""+
		"admission plugins that should be enabled in addition to default enabled ones ("+
		strings.Join(serverOpts.RecommendedOptions.Admission.RecommendedPluginOrder, ", ")+"). "+
		"Comma-delimited list of admission plugins: "+strings.Join(serverOpts.RecommendedOptions.Admission.Plugins.Registered(), ", ")+". "+
		"The order of plugins in this flag does not matter.")

	serverOpts.RecommendedOptions.AddFlags(flags)

	// serverOpts.Admission.AddFlags(flags)
	serverExtraConfig.AddFlags(flags)
}

const defaultEtcdPathPrefix = "/registry/devops.alauda.io"

// DevopsServerOptions options for the Server
type DevopsServerOptions struct {
	RecommendedOptions *genericoptions.RecommendedOptions
	// Admission          *genericoptions.AdmissionOptions

	SharedInformerFactory informers.SharedInformerFactory
	StdOut                io.Writer
	StdErr                io.Writer
}

// NewDevopsServerOptions constructor for the Server
func NewDevopsServerOptions(out, errOut io.Writer) *DevopsServerOptions {
	o := &DevopsServerOptions{
		RecommendedOptions: genericoptions.NewRecommendedOptions(defaultEtcdPathPrefix, apiserver.Codecs.LegacyCodec(v1alpha1.SchemeGroupVersion), nil),
		// Admission:          genericoptions.NewAdmissionOptions(),

		StdOut: out,
		StdErr: errOut,
	}
	// Adding recommended
	o.RecommendedOptions.Admission.RecommendedPluginOrder = []string{
		banproject.PluginName, components.PluginName,
	}
	// Cleanup automatically added APIServer plugins
	o.RecommendedOptions.Admission.Plugins = admission.NewPlugins()
	// register all plugins
	devopsadmission.RegisterAllPlugins(o.RecommendedOptions.Admission.Plugins)

	return o
}

var (
	serverOpts        *DevopsServerOptions
	serverExtraConfig *options.ExtraConfig
	serverCmd         = &cobra.Command{
		Use:   "server",
		Short: "Launch a devops apiserver",
		Long:  "Launch a devops apiserver",
		RunE: func(c *cobra.Command, args []string) error {

			if err := serverOpts.Complete(); err != nil {
				return err
			}
			if err := serverOpts.Validate(args); err != nil {
				return err
			}

			if err := utilerrors.NewAggregate(serverExtraConfig.ApplyFlags()); err != nil {
				return err
			}

			return serverOpts.RunDevopsServer(command.StopCh)
		},
	}
)

// Validate runs basic validation on arguments
func (o DevopsServerOptions) Validate(args []string) error {
	errors := []error{}
	errors = append(errors, o.RecommendedOptions.Validate()...)
	// errors = append(errors, o.Admission.Validate()...)
	return utilerrors.NewAggregate(errors)
}

// Complete complete any required arguments
func (o *DevopsServerOptions) Complete() error {
	return nil
}

//Config run full drill configuration and return apiserver.Config pointer
func (o *DevopsServerOptions) Config() (*apiserver.Config, error) {
	// register admission plugins
	// banproject.Register(o.RecommendedOptions.Admission.Plugins)
	// verifydependency.Register(o.RecommendedOptions.Admission.Plugins)

	// TODO have a "real" external address
	if err := o.RecommendedOptions.SecureServing.MaybeDefaultWithSelfSignedCerts("localhost", nil, []net.IP{net.ParseIP("127.0.0.1")}); err != nil {
		return nil, fmt.Errorf("error creating self-signed certificates: %v", err)
	}

	glog.Infof("Getting new recommended config...")
	serverConfig := genericapiserver.NewRecommendedConfig(apiserver.Codecs)

	glog.Infof("Adding extra admission initializers...")
	o.RecommendedOptions.ExtraAdmissionInitializers = func(c *genericapiserver.RecommendedConfig) ([]admission.PluginInitializer, error) {
		glog.Infof("Starting extra admission initializers...")
		client, err := clientset.NewForConfig(c.LoopbackClientConfig)
		if err != nil {
			return nil, err
		}
		k8sClient, err := kubernetes.NewForConfig(c.ClientConfig)
		if err != nil {
			glog.Fatalf("Error building kubernetes client: %s", err.Error())
			return nil, err
		}

		informerFactory := informers.NewSharedInformerFactory(client, c.LoopbackClientConfig.Timeout)
		o.SharedInformerFactory = informerFactory

		annotationprovider := devops.NewAnnotationProvider(serverExtraConfig.BaseDomain)
		return []admission.PluginInitializer{
			devopsinitializer.New(informerFactory, serverConfig.SharedInformerFactory, client, k8sClient, dependency.DefaultManager, annotationprovider),
		}, nil
	}

	glog.Infof("Applying recommended options...")
	if err := o.RecommendedOptions.ApplyTo(serverConfig, apiserver.Scheme); err != nil {
		return nil, err
	}

	config := &apiserver.Config{
		GenericConfig: serverConfig,
		ExtraConfig: apiserver.ExtraConfig{
			SystemNamespace:      serverExtraConfig.SystemNamespace,
			CredentialsNamespace: serverExtraConfig.CredentialsNamespace,
			BaseDomain:           serverExtraConfig.BaseDomain,
		},
	}
	return config, nil
}

func healthz(rw http.ResponseWriter, req *http.Request) {
	io.WriteString(rw, "ok")
}

func startSimpleHTTPServer() {
	http.HandleFunc("/healthz", healthz)
	glog.Infoln(http.ListenAndServe("0.0.0.0:8080", nil))
}

// RunDevopsServer run the devops apiserver
func (o *DevopsServerOptions) RunDevopsServer(stopCh <-chan struct{}) error {

	go startSimpleHTTPServer()
	// this will invoke the system to free memory
	go PeriodicFree(30 * time.Second)

	glog.Infof("Getting config...")
	config, err := o.Config()
	if err != nil {
		return err
	}

	glog.Infof("Completing config...")
	server, err := config.Complete().New()
	if err != nil {
		return err
	}

	glog.Infof("Adding post start hook...")
	err = server.GenericAPIServer.AddPostStartHook("start-devops-apiserver-informers", func(context genericapiserver.PostStartHookContext) error {
		// by default it does not start the
		// internal SharedInformerFactory so we
		// need to add this goroutine here to start it
		glog.Infof("Starting post start hook")
		go func() {
			glog.Infof("starting internal SharedInformerFactory")
			o.SharedInformerFactory.Start(context.StopCh)
		}()
		glog.Infof("starting config.GenericConfig.SharedInformerFactory")

		// o.SharedInformerFactory.Start(context.StopCh)
		config.GenericConfig.SharedInformerFactory.Start(context.StopCh)
		return nil
	})
	if err != nil {
		return err
	}

	glog.Infof("Starting server...")
	return server.GenericAPIServer.PrepareRun().Run(stopCh)
}

// PeriodicFree free os memory period
func PeriodicFree(d time.Duration) {
	tick := time.Tick(d)
	for range tick {
		debug.FreeOSMemory()
	}
}
