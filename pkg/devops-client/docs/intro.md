# Guide

## 目标

统一管理Devops工具链的各个工具的客户端的不同版本的客户端代码，并且提供了统一的接口

## 概述

### 目录说明

```bash
artifacts: 存放各个工具的不同版本的swagger文档
docs: 文档
mock: mock测试
pkg: 每个工具都会有一个pkg libaray
templates: 每个工具的适配代码的模板
```

### 自动生成工具介绍

go-swagger

优点：
* go语言实现，运行时轻，可以原生引入，生成速度快
* 模板质量较好，抽象层次更高，更符合Effective go
* 官方提供了丰富toolkit，可以在代码中使用

缺点：
* 非官方维护
* 目前只支持swagger v2
* 只能生成go语言

### Usage
```bash
# 在artifacts目录对应的工具目录下提供swagger文档，约定以文件名作为版本号
# 如生成harbor
go run main.go gen -t harbor
# 自动生成的代码会生成在pkg/harbor/{{version}}下

# 作为library调用时，详细参考examples
client := factory.NewClient("harbor", "v1", opts)
```

## Questions

* 官方提供的swagger文档与实际接口也未必符合，痛点是需要根据官方api文档以及实际调用接口对swagger文档进行维护
* 目前对各个工具的抽象层次还不好，需要重新抽象公共接口