// Package mock is a storage of generated mocks used by the library
// should not host any general logic
// and should only be used in test files
package mock
