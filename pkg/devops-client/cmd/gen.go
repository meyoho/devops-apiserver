package cmd

import (
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/generator"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(genCmd)

	flags := genCmd.Flags()
	flags.StringVarP(&genOpts.Name, "tool", "t", "", "tool name")
}

var genCmd = &cobra.Command{
	Use:   "gen",
	Short: "Generate devops tool client by go-swagger",
	RunE: func(c *cobra.Command, args []string) (err error) {
		if err = genOpts.Validate(); err != nil {
			return
		}
		// do stuff
		err = generator.Generate(genOpts.Name)
		return
	},
}

type genCommandOpts struct {
	// tool name
	// empty for all tools
	Name string
}

var genOpts genCommandOpts

func (genCommandOpts) Validate() error {
	return nil
}
