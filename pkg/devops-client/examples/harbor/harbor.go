package main

import (
	"context"
	"flag"
	"fmt"
	"os"

	"k8s.io/klog"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
)

func main() {
	klog.InitFlags(nil)
	flag.Set("v", "6")
	flag.Parse()

	opts := v1.NewOptions(
		v1.NewBasicConfig("49.4.2.82:8443", "", []string{}),
		v1.NewBasicAuth("admin", "HarborIota123_"),
	)
	client, err := factory.NewClient("harbor", "v1", opts)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	repos, err := client.GetImageRepos(context.Background())
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(repos)
}
