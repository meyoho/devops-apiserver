package {{.Version}}

import (
	"context"
	"errors"
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	v1 "alauda.io/devops-apiserver/pkg/apis/devops"

	"net/http"
	"regexp"
	"strconv"
	"strings"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/generic"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/jira/{{.Version}}/client"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/jira/{{.Version}}/client/operations"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/jira/{{.Version}}/models"
	"github.com/go-logr/logr"
	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	clientv1.NotImplement
	logger     logr.Logger
	client     *client.Jira
	opts       *clientv1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ clientv1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() clientv1.ClientFactory {
	return func(opts *clientv1.Options) clientv1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			client := client.NewHTTPClientWithConfig(nil, config)
			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

// GetProjects implements ProjectService
func (c *Client) GetProjects(ctx context.Context, page, pagesize string) (*v1.ProjectDataList, error) {
	var (
		projectPage     int64
		projectPageSize int64
		err             error
	)

	if page != "" && pagesize != "" {
		projectPage, err = strconv.ParseInt(page, 10, 64)
		projectPageSize, err = strconv.ParseInt(pagesize, 10, 64)
		if err != nil {
			return nil, err
		}
	} else {
		projectPage = 1
		projectPageSize = 10
	}

	start := (projectPage - 1) * projectPageSize
	end := projectPage * projectPageSize

	expand := "description,lead"
	projectsParam := operations.
		NewGetProjectParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithExpand(&expand)
	projectsOK, err := c.client.Operations.GetProject(projectsParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := &v1.ProjectDataList{}

	var count int64 = 0
	for _, project := range projectsOK.Payload {
		if count >= end {
			break
		}
		if start <= count {
			var leadername string
			if project.Lead != nil {
				leadername = project.Lead.Name
			}
			projectData := v1.ProjectData{}
			projectData.Name = project.Name
			projectData.Annotations = map[string]string{
				"self":        project.Self,
				"id":          project.ID,
				"description": project.Description,
				"key":         project.Key,
				"lead":        leadername,
			}
			result.Items = append(result.Items, projectData)
		}
		count++
	}

	return result, nil
}

// CreateProject implements ProjectService
func (c *Client) CreateProject(ctx context.Context, projectName, projectDescription, projectLead, projectKey string) (*v1.ProjectData, error) {
	project := &models.ProjectCreate{
		Name:           projectName,
		Description:    projectDescription,
		Lead:           projectLead,
		Key:            strings.ToUpper(projectKey),
		ProjectTypeKey: "business",
	}
	projectParam := operations.
		NewPostProjectParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithProject(project)
	projectOK, err := c.client.Operations.PostProject(projectParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	projectsuburl := fmt.Sprintf("projects/%s/summary", strings.ToUpper(projectKey))
	projectSelf := fmt.Sprintf("%s/%s", strings.TrimRight(c.opts.BasicConfig.Host, "/"), strings.TrimLeft(projectsuburl, "/"))

	result := &v1.ProjectData{}
	result.Name = projectName
	result.Annotations = map[string]string{
		"self":        projectSelf,
		"id":          fmt.Sprintf("%d", projectOK.Payload.ID),
		"description": projectDescription,
	}

	return result, nil
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(c.httpClient, url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(ctx context.Context) (*v1.HostPortStatus, error) {
	username := ""
	if c.opts.BasicAuth != nil {
		username = c.opts.BasicAuth.Username
	}

	params := operations.
		NewGetUserSearchParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithUsername(&username)
	_, err := c.client.Operations.GetUserSearch(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return &v1.HostPortStatus{StatusCode: 200, Response: "correct username and password"}, nil
}

func (c *Client) SearchForUsers(ctx context.Context, username string) (*devops.ProjectmanagementUser, error) {
	params := operations.
		NewGetUserSearchParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithUsername(&username)
	users, err := c.client.Operations.GetUserSearch(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := &devops.ProjectmanagementUser{}
	for _, user := range users.Payload {
		devopsUser := v1.User{
			Username: user.Name,
			Email:    user.EmailAddress,
			ID:       user.Key,
		}
		result.Items = append(result.Items, devopsUser)
	}
	return result, nil
}

var SearchOptionsMap = map[string]bool{
	"status":    true,
	"priority":  true,
	"issuetype": true,
	"all":       true,
}

// GetIssueOptions get Issues option like status pip
func (c *Client) GetIssueOptions(ctx context.Context, optiontype string) (result *v1.IssueFilterDataList, err error) {

	if SearchOptionsMap[optiontype] {

		result = &v1.IssueFilterDataList{}

		switch optiontype {
		case "all":
			result, err = c.GetAllOptions(ctx, result)
		case "status":
			result, err = c.getStatusOptions(ctx, result)
		case "priority":
			result, err = c.getPriorityOptions(ctx, result)
		case "issuetype":
			result, err = c.getTypeOptions(ctx, result)
		}
		if err != nil {
			return nil, err
		}

		return result, nil
	}
	return result, errors.New(fmt.Sprintf("type %s is not support", optiontype))
}

func (c *Client) GetAllOptions(ctx context.Context, list *v1.IssueFilterDataList) (result *v1.IssueFilterDataList, err error) {

	list, err = c.getStatusOptions(ctx, list)
	if err != nil {
		return nil, err
	}
	list, err = c.getPriorityOptions(ctx, list)
	if err != nil {
		return nil, err
	}
	list, err = c.getTypeOptions(ctx, list)
	if err != nil {
		return nil, err
	}
	return list, err

}

func (c *Client) getStatusOptions(ctx context.Context, list *v1.IssueFilterDataList) (*v1.IssueFilterDataList, error) {

	statusParam := operations.
		NewGetStatusParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)

	statusOK, err := c.client.Operations.GetStatus(statusParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	for _, issuetatus := range statusOK.Payload {
		statusData := v1.IssueFilterData{}
		statusData.Name = issuetatus.Name
		statusData.ID = issuetatus.ID
		list.Status = append(list.Status, statusData)
	}

	return list, nil
}

func (c *Client) getTypeOptions(ctx context.Context, list *v1.IssueFilterDataList) (*v1.IssueFilterDataList, error) {

	typeParam := operations.
		NewGetIssuetypeParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)

	typeOK, err := c.client.Operations.GetIssuetype(typeParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	for _, issuetype := range typeOK.Payload {
		typeData := v1.IssueFilterData{}
		typeData.Name = issuetype.Name
		typeData.ID = issuetype.ID
		list.IssueType = append(list.IssueType, typeData)
	}

	return list, nil
}

func (c *Client) getPriorityOptions(ctx context.Context, list *v1.IssueFilterDataList) (*v1.IssueFilterDataList, error) {

	priorityParam := operations.
		NewGetPriorityParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)

	priorityOK, err := c.client.Operations.GetPriority(priorityParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	for _, priority := range priorityOK.Payload {
		Data := v1.IssueFilterData{}
		Data.Name = priority.Name
		Data.ID = priority.ID
		list.Priority = append(list.Priority, Data)
	}

	return list, nil
}

func (c *Client) GetIssueDetail(ctx context.Context, issuekey string) (*v1.IssueDetail, error) {
	fields := "issuetype,project,summary,creator,priority,comment,issuelinks,assignee,updated,status,created,description"
	issueParam := operations.
		NewGetIssueIssueIDParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithFields(&fields).
		WithIssueID(issuekey)
	Issuedetail, err := c.client.Operations.GetIssueIssueID(issueParam, c.authInfo)

	if err != nil {
		return nil, err
	}
	result := parseIssueDetail(Issuedetail.Payload, true)

	return result, err

}

func parseIssueDetail(Issuedetail *models.IssueDetail, deatil bool) *v1.IssueDetail {
	result := &v1.IssueDetail{}
	result.Key = Issuedetail.Key
	result.Description = Issuedetail.Fields.Description
	result.Created = Issuedetail.Fields.Created
	result.Updated = Issuedetail.Fields.Updated
	result.Summary = Issuedetail.Fields.Summary
	result.Status.Name = Issuedetail.Fields.Status.Name
	result.Status.ID = Issuedetail.Fields.Status.ID
	result.Project.Name = Issuedetail.Fields.Project.Name
	result.Issuetype.Name = Issuedetail.Fields.IssueType.Name
	result.Creator.Username = Issuedetail.Fields.Creator.Name
	result.Priority.Name = Issuedetail.Fields.Priority.Name
	result.Issuetype.Name = Issuedetail.Fields.IssueType.Name
	result.SelfLink = fmt.Sprintf("/browse/%s", Issuedetail.Key)
	if Issuedetail.Fields.Assignee != nil {
		result.Assignee.Username = Issuedetail.Fields.Assignee.Name
	}

	if deatil {
		Comments := []v1.Comment{}
		IssueLinks := []v1.IssueLink{}

		for _, jiracomment := range Issuedetail.Fields.Comment.Comments {
			Comment := v1.Comment{}
			Comment.Time = jiracomment.Updated
			Comment.Author = jiracomment.UpdateAuthor.Name
			Comment.Content = jiracomment.Body
			Comments = append(Comments, Comment)
		}
		result.Comments = Comments
		for _, jiraissuelink := range Issuedetail.Fields.Issuelinks {
			IssueLink := v1.IssueLink{}
			if jiraissuelink.Inwardissue != nil {
				IssueLink.Key = jiraissuelink.Inwardissue.Key
				IssueLink.Summary = jiraissuelink.Inwardissue.Fields.Summary
			} else {
				IssueLink.Key = jiraissuelink.Outwardissue.Key
				IssueLink.Summary = jiraissuelink.Outwardissue.Fields.Summary
			}

			IssueLinks = append(IssueLinks, IssueLink)
		}
		result.IssueLinks = IssueLinks
	}
	return result
}

func (c *Client) GetIssueList(ctx context.Context, listoption v1.ListIssuesOptions) (*v1.IssueDetailList, error) {

	jql, startat, maxresults := GetInfofromListOption(&listoption)

	querybody := models.IssueSearch{
		Jql:        jql,
		StartAt:    int64(startat),
		MaxResults: int64(maxresults),
		Fields: []string{
			"created",
			"priority",
			"status",
			"project",
			"updated",
			"creator",
			"assignee",
			"summary",
			"issuetype",
			"description",
		},
		Expand: []string{},
	}

	issuelistQuery := operations.NewPostSearchParams().WithContext(ctx).WithHTTPClient(c.httpClient).WithIssuesearch(&querybody)
	IssueList, err := c.client.Operations.PostSearch(issuelistQuery, c.authInfo)
	result := &v1.IssueDetailList{}

	if err != nil {
		c.logger.Error(err, "Error occurred when get issuelist")
		if IssueKeyErrorType(err.Error()) {
			err = nil
		}
		return result, err
	}

	result.Total = int(IssueList.Payload.Total)
	//result.MaxResults = IssueList.Payload.Maxresults
	//result.StartAt = IssueList.Payload.Startat
	for _, payload := range IssueList.Payload.Issues {
		resultissue := parseIssueDetail(payload, false)
		result.Items = append(result.Items, *resultissue)
	}

	return result, nil
}

func IssueKeyErrorType(errmessage string) bool {
	ENmatched, err := regexp.MatchString(`.*400.*The issue key.*for field 'issuekey' is invalid`, errmessage)
	if err != nil {
		return false
	}
	CHmatched, err := regexp.MatchString(`.*400.*问题关键字.*在域 “issuekey”中无效`, errmessage)
	if err != nil {
		return false
	}
	return ENmatched || CHmatched
}

func GetInfofromListOption(option *v1.ListIssuesOptions) (jql string, startat, maxresults int) {
	order := fmt.Sprintf("ORDER BY %s %s", option.OrderBy, option.Sort)
	var ProjectKeyInfo string
	if option.Project == "" {
		ProjectKeyInfo = fmt.Sprintf("(project = %s", option.Projects[0])
		for _, projectkey := range option.Projects[1:] {
			projectforjql := fmt.Sprintf(" or Project = %s", projectkey)
			ProjectKeyInfo += projectforjql
		}
		ProjectKeyInfo = fmt.Sprintf("%s)", ProjectKeyInfo)

	} else {
		ProjectKeyInfo = fmt.Sprintf("project = %v", option.Project)
	}

	if option.Type != "" {
		typeforjql := fmt.Sprintf("type = %s", option.Type)
		ProjectKeyInfo = fmt.Sprintf("%s and %s", typeforjql, ProjectKeyInfo)
	}
	if option.Status != "" {
		typeforstatus := fmt.Sprintf("status = %s", option.Status)
		ProjectKeyInfo = fmt.Sprintf("%s and %s", typeforstatus, ProjectKeyInfo)
	}
	if option.IssueKey != "" {
		typeforissuekey := fmt.Sprintf("issuekey = %s", option.IssueKey)
		ProjectKeyInfo = fmt.Sprintf("%s and %s", typeforissuekey, ProjectKeyInfo)
	}
	if option.Priority != "" {
		typeforpriority := fmt.Sprintf("priority = %s", option.Priority)
		ProjectKeyInfo = fmt.Sprintf("%s and %s", typeforpriority, ProjectKeyInfo)
	}
	if option.Summary != "" {
		typeforsummary := fmt.Sprintf("summary ~ '%s'", option.Summary)
		ProjectKeyInfo = fmt.Sprintf("%s and %s", typeforsummary, ProjectKeyInfo)
	}

	ProjectKeyInfo = fmt.Sprintf("%s %s", ProjectKeyInfo, order)

	startat = (option.Page - 1) * option.PageSize
	maxresults = option.PageSize
	return ProjectKeyInfo, startat, maxresults
}
