// Code generated
package {{.Version}}

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"alauda.io/devops-apiserver/pkg/devops-client/pkg/generic"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"
	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/dockerhub/{{.Version}}/client"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/dockerhub/{{.Version}}/client/operations"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/dockerhub/{{.Version}}/models"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/transport"
	"github.com/go-logr/logr"

	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	clientv1.NotImplement
	logger     logr.Logger
	client     *client.Dockerhub
	opts       *clientv1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ clientv1.Interface = &Client{}

var (
	DockerHubPageSize int64 = 100
)

// NewClient new devops tool client
func NewClient() clientv1.ClientFactory {
	return func(opts *clientv1.Options) clientv1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			ct := &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
			ct.refreshToken(context.Background())
			return ct
		}

		return &Client{client: client.Default}
	}
}

func (c *Client) getToken(ctx context.Context) (string, error) {
	if c.opts.BasicAuth == nil {
		return "", errors.New("please provide username and password")
	}

	user := &models.User{
		Password: c.opts.BasicAuth.Password,
		Username: c.opts.BasicAuth.Username,
	}
	params := operations.
		NewPostUsersLoginParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithUser(user)
	result, err := c.client.Operations.PostUsersLogin(params, openapi.PassThroughAuth)
	if err != nil {
		return "", err
	}

	return result.Payload.Token, nil
}

func (c *Client) refreshToken(ctx context.Context) error {
	token, err := c.getToken(ctx)
	if err != nil {
		return err
	}

	c.authInfo = openapi.BearerToken(token)
	return nil
}

func (c *Client) listAllOrgs(ctx context.Context) ([]*models.Organization, error) {
	var (
		page = int64(0)
		orgs []*models.Organization
	)

	for {
		page = page + 1
		params := operations.
			NewGetUserOrgsParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithPage(&page).
			WithPageSize(&DockerHubPageSize)
		result, err := c.client.Operations.GetUserOrgs(params, c.authInfo)
		if err != nil {
			return nil, err
		}

		orgs = append(orgs, result.Payload.Results...)
		if len(result.Payload.Results) < int(DockerHubPageSize) {
			break
		}
	}
	return orgs, nil
}

func (c *Client) listOrgRepos(ctx context.Context, orgName string) ([]*models.Repository, error) {
	var (
		page  = int64(0)
		repos []*models.Repository
	)

	for {
		page = page + 1
		params := operations.
			NewGetRepositoriesOrgNameParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithPageSize(&DockerHubPageSize).
			WithPage(&page).
			WithOrgName(&orgName)
		result, err := c.client.Operations.GetRepositoriesOrgName(params, c.authInfo)
		if err != nil {
			return nil, err
		}

		repos = append(repos, result.Payload.Results...)
		if len(result.Payload.Results) < int(DockerHubPageSize) {
			break
		}
	}

	return repos, nil
}

// GetImageRepos implments ImageRegistryService interface
func (c *Client) GetImageRepos(ctx context.Context) (*v1.ImageRegistryBindingRepositories, error) {
	orgs, err := c.listAllOrgs(ctx)
	if err != nil {
		return nil, err
	}

	var username string
	if c.opts.BasicAuth != nil {
		username = c.opts.BasicAuth.Username
	}
	orgsNameList := make([]string, 0, len(orgs)+1)
	for _, org := range orgs {
		if org.Orgname != username {
			orgsNameList = append(orgsNameList, org.Orgname)
		}
	}
	if len(username) > 0 {
		orgsNameList = append(orgsNameList, username)
	}

	var repoList []*models.Repository
	for _, orgName := range orgsNameList {
		repos, err := c.listOrgRepos(ctx, orgName)
		if err != nil {
			c.logger.Error(err, "Organization list repos failed", "orgName", orgName)
		} else {
			repoList = append(repoList, repos...)
		}
	}

	var imageRepos v1.ImageRegistryBindingRepositories
	for _, repo := range repoList {
		imageRepos.Items = append(imageRepos.Items, fmt.Sprintf("%s/%s", repo.Namespace, repo.Name))
	}
	return &imageRepos, nil
}

func (c *Client) listRepoTags(ctx context.Context, repository string) ([]*models.ImageTag, error) {
	var (
		page = int64(0)
		tags []*models.ImageTag
	)

	for {
		page = page + 1
		params := operations.
			NewGetRepositoriesRepoNameTagsParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithPage(&page).
			WithPageSize(&DockerHubPageSize).
			WithRepoName(&repository)
		result, err := c.client.Operations.GetRepositoriesRepoNameTags(params, c.authInfo)
		if err != nil {
			return nil, err
		}

		tags = append(tags, result.Payload.Results...)
		if len(result.Payload.Results) < int(DockerHubPageSize) {
			break
		}
	}
	return tags, nil
}

// GetImageTags implments ImageRegistryService interface
func (c *Client) GetImageTags(ctx context.Context, repository string) ([]v1.ImageTag, error) {
	tags, err := c.listRepoTags(ctx, repository)
	if err != nil {
		return nil, err
	}

	imageTags := make([]v1.ImageTag, 0, len(tags))
	for _, tag := range tags {
		imageTag := v1.ImageTag{}
		imageTag.Name = tag.Name
		imageTag.Size = generic.ConvertSizeToString(tag.FullSize)
		t, err := time.Parse(time.RFC3339Nano, tag.LastUpdated)
		if err != nil {
			c.logger.Error(err, "convert string to time error", "time", tag.LastUpdated)
		}
		imageTag.CreatedAt = &metav1.Time{Time: t}
		imageTags = append(imageTags, imageTag)
	}
	return imageTags, nil
}

func (c *Client) RepositoryAvailable(ctx context.Context, repositoryName string, lastModifyAt time.Time) (*v1.HostPortStatus, error) {
	var (
		start    = time.Now()
		duration time.Duration
	)
	status := v1.HostPortStatus{
		LastAttempt: &metav1.Time{Time: lastModifyAt},
	}

	params := operations.
		NewGetRepositoriesRepoNameParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithRepoName(&repositoryName)
	_, err := c.client.Operations.GetRepositoriesRepoName(params, c.authInfo)
	if err != nil {
		status.StatusCode = 500
		status.ErrorMessage = err.Error()
	} else {
		status.StatusCode = 200
		status.Response = "available"
	}

	duration = time.Since(start)
	status.Delay = &duration
	return &status, err
}

func (c *Client) GetImageRepoLink(_ context.Context, name string) string {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return fmt.Sprintf("%s/r/%s", url, name)
	}

	return ""
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(c.httpClient, url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(ctx context.Context) (*v1.HostPortStatus, error) {
	err := c.refreshToken(ctx)
	if err != nil {
		return nil, err
	}

	return &v1.HostPortStatus{
		StatusCode:  200,
		LastAttempt: &metav1.Time{Time: time.Now()},
	}, nil
}
