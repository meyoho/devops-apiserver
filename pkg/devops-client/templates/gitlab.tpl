// Code generated
package {{.Version}}

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"

	"github.com/go-logr/logr"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/generic"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/{{.Version}}/client"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/{{.Version}}/client/groups"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/{{.Version}}/client/projects"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/{{.Version}}/client/user"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/{{.Version}}/models"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/transport"
	"github.com/dustin/go-humanize"
	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	clientv1.NotImplement
	logger        logr.Logger
	client        *client.Gitlab
	opts          *clientv1.Options
	authInfo      runtime.ClientAuthInfoWriter
	httpClient    *http.Client
	serviceClient *generic.ServiceClient
}

var _ clientv1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() clientv1.ClientFactory {
	return func(opts *clientv1.Options) clientv1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			ct := &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
			if ct.authInfo == nil {
				// adapter auth
				ct.init()
			}
			return ct
		}

		return &Client{client: client.Default}
	}
}

// init constructs serviceClient for client
func (c *Client) init() {
	if c.opts != nil && c.opts.GenericOpts != nil {
		var (
			codeRepoService *v1.CodeRepoService
			secret          *corev1.Secret
		)

		for _, opt := range c.opts.GenericOpts.Options {
			switch opt.(type) {
			case *v1.CodeRepoService:
				codeRepoService = opt.(*v1.CodeRepoService)
			case *corev1.Secret:
				secret = opt.(*corev1.Secret)
			}
		}

		c.serviceClient = generic.NewServiceClient(codeRepoService, secret)
		// adapter auth info
		if c.serviceClient.IsBasicAuth() {
			c.authInfo = openapi.APIKeyAuth("PRIVATE-TOKEN", "header", c.serviceClient.GetPassword())
		} else {
			c.serviceClient.OAuth2Info.AccessTokenKey = k8s.GitlabAccessTokenKey
			c.serviceClient.OAuth2Info.Scope = k8s.GitlabAccessTokenScope
			c.authInfo = openapi.BearerToken(c.serviceClient.OAuth2Info.AccessToken)
		}
		c.serviceClient.SetClient(c.httpClient)
	}
}

func (c *Client) GetAuthorizeUrl(ctx context.Context, redirectUrl string) string {
	return fmt.Sprintf("%s/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		strings.TrimRight(c.serviceClient.GetHtml(), "/"), c.serviceClient.GetClientID(), redirectUrl, c.serviceClient.GetScope())
}

func (c *Client) getAccessTokenUrl() string {
	return fmt.Sprintf("%s/oauth/token", strings.TrimRight(c.serviceClient.GetHtml(), "/"))
}

type AccessTokenResponseGitlab struct {
	generic.AccessTokenResponse
	Scope string `json:"scope"`

	RefreshToken string `json:"refresh_token"`
	CreatedAt    int64  `json:"created_at"`
}

func (a *AccessTokenResponseGitlab) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken:  a.AccessToken,
		Scope:        a.Scope,
		RefreshToken: a.RefreshToken,
		CreatedAt:    time.Now().Format(time.RFC3339),
	}
}

func (c *Client) AccessToken(ctx context.Context, redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessTokenUrl()
		response       = &AccessTokenResponseGitlab{}
	)

	return c.serviceClient.AccessToken(response, redirectUrl, accessTokenUrl)
}

func (c *Client) listAllRepos(ctx context.Context) ([]*models.Project, error) {
	var (
		page       = int32(0)
		pageSize   = int32(100)
		membership = true
		statistics = true
		repos      = []*models.Project{}
	)

	for {
		page = page + 1
		reposParam := projects.
			NewGetV4ProjectsParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithMembership(&membership).
			WithStatistics(&statistics).
			WithPage(&page).
			WithPerPage(&pageSize)
		reposOK, err := c.client.Projects.GetV4Projects(reposParam, c.authInfo)
		if err != nil {
			return nil, err
		}

		repos = append(repos, reposOK.Payload...)

		if len(reposOK.Payload) < int(pageSize) {
			break
		}
	}

	return repos, nil
}

func (c *Client) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo v1.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if gitlabRepo, ok := remoteRepo.(*models.Project); ok {
		codeRepo = v1.OriginCodeRepository{
			CodeRepoServiceType: v1.CodeRepoServiceTypeGitlab,
			ID:                  strconv.Itoa(int(gitlabRepo.ID)),
			Name:                gitlabRepo.Name,
			FullName:            gitlabRepo.PathWithNamespace,
			Description:         gitlabRepo.Description,
			HTMLURL:             gitlabRepo.WebURL,
			CloneURL:            gitlabRepo.HTTPURLToRepo,
			SSHURL:              gitlabRepo.SSHURLToRepo,
			Language:            "",
			Owner: v1.OwnerInRepository{
				ID:   strconv.Itoa(int(gitlabRepo.Namespace.ID)),
				Name: gitlabRepo.Namespace.Path,
				Type: v1.GetOwnerType(gitlabRepo.Namespace.Kind),
			},

			CreatedAt: &metav1.Time{Time: generic.ConvertStringToTime(gitlabRepo.CreatedAt)},
			// LastActivityAt on gitlab will reflect of pushedAt
			PushedAt:  &metav1.Time{Time: generic.ConvertStringToTime(gitlabRepo.LastActivityAt)},
			UpdatedAt: &metav1.Time{Time: generic.ConvertStringToTime(gitlabRepo.LastActivityAt)},

			Private:      gitlabRepo.VisibilityLevel != "public",
			Size:         getRepoSize(gitlabRepo) * 1000,
			SizeHumanize: humanize.Bytes(uint64(getRepoSize(gitlabRepo))),
		}
	}

	return
}

func getRepoSize(project *models.Project) int64 {
	if project == nil || project.Statistics == nil {
		return 0
	}

	return project.Statistics.RepositorySize
}

// GetRemoteRepos implements CodeRepoService
func (c *Client) GetRemoteRepos(ctx context.Context) (*v1.CodeRepoBindingRepositories, error) {
	repos, err := c.listAllRepos(ctx)
	if err != nil {
		return nil, err
	}

	dictOwners := make(map[string]*v1.CodeRepositoryOwner)
	for _, repo := range repos {
		ownerKey := repo.Namespace.Path

		if owner, ok := dictOwners[ownerKey]; !ok {
			owner = &v1.CodeRepositoryOwner{
				Type:         v1.GetOwnerType(repo.Namespace.Kind),
				ID:           strconv.Itoa(int(repo.Namespace.ID)),
				Name:         repo.Namespace.Path,
				Email:        "",
				DiskUsage:    0,
				Repositories: make([]v1.OriginCodeRepository, 0, 10),
			}

			if repo.Owner != nil {
				owner.HTMLURL = repo.Owner.WebURL
				owner.AvatarURL = repo.Owner.AvatarURL
			}

			dictOwners[ownerKey] = owner
		}

		codeRepo := c.ConvertRemoteRepoToBindingRepo(repo)
		dictOwners[ownerKey].Repositories = append(dictOwners[ownerKey].Repositories, codeRepo)
	}

	result := &v1.CodeRepoBindingRepositories{
		Type:   v1.CodeRepoServiceTypeGitlab,
		Owners: []v1.CodeRepositoryOwner{},
	}

	for _, owner := range dictOwners {
		if len(owner.Repositories) > 0 {
			result.Owners = append(result.Owners, *owner)
		}
	}

	return result, nil
}

// GetBranches implements CodeRepoService
func (c *Client) GetBranches(ctx context.Context, owner, repo, repoFullName string) ([]v1.CodeRepoBranch, error) {
	branchesParam := projects.
		NewGetV4ProjectsIDRepositoryBranchesParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithID(repoFullName)
	branchesOK, err := c.client.Projects.GetV4ProjectsIDRepositoryBranches(branchesParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := make([]v1.CodeRepoBranch, 0)
	for _, branch := range branchesOK.Payload {
		fmt.Println(branch.Commit.ID)
		result = append(result, v1.CodeRepoBranch{
			Name:   branch.Name,
			Commit: branch.Commit.ID,
		})
	}
	return result, nil
}

func gitlabGroupAsProjectData(gitlabGroup models.Group) (*v1.ProjectData, error) {
	data, err := generic.MarshalToMapString(gitlabGroup)
	if err != nil {
		return nil, err
	}

	var projectData = &v1.ProjectData{
		ObjectMeta: metav1.ObjectMeta{
			Name: gitlabGroup.Path,
			Annotations: map[string]string{
				"avatarURL":   gitlabGroup.AvatarURL,
				"webURL":      gitlabGroup.WebURL,
				"description": gitlabGroup.Description,
				"type":        string(v1.OriginCodeRepoRoleTypeOrg),
			},
		},
		Data: data,
	}
	return projectData, nil
}

func gitlabUserAsProjectData(user *models.UserPublic) (*v1.ProjectData, error) {
	var projectData = &v1.ProjectData{
		ObjectMeta: metav1.ObjectMeta{
			Name: user.Username,
			Annotations: map[string]string{
				"avatarURL":   user.AvatarURL,
				"webURL":      "/" + user.Username,
				"description": "",
				"type":        string(v1.OriginCodeRepoOwnerTypeUser),
			},
		},
		Data: map[string]string{
			"username": user.Username,
			"id":       strconv.Itoa(int(user.ID)),
			"name":     user.Name,
			"company":  user.Organization,
			"email":    user.Email,
		},
	}
	return projectData, nil
}

func (c *Client) currentUser(ctx context.Context) (*v1.ProjectData, error) {
	userParams := user.
		NewGetV4UserParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	userOK, err := c.client.User.GetV4User(userParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	return gitlabUserAsProjectData(userOK.Payload)
}

// ListCodeRepoProjects implements CodeRepoService
func (c *Client) ListCodeRepoProjects(ctx context.Context, option v1.ListProjectOptions) (*v1.ProjectDataList, error) {
	groupsParam := groups.
		NewGetV4GroupsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithSearch(&option.Filter)
	groupsOK, err := c.client.Groups.GetV4Groups(groupsParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	items := make([]v1.ProjectData, 0, len(groupsOK.Payload)+1)

	for _, group := range groupsOK.Payload {
		projectData, err := gitlabGroupAsProjectData(*group)
		if err != nil {
			return nil, err
		}

		items = append(items, *projectData)
	}

	userProject, err := c.currentUser(ctx)
	if err != nil {
		return nil, err
	}

	items = append(items, *userProject)

	result := &v1.ProjectDataList{
		Items: items,
	}
	return result, nil
}

// CreateCodeRepoProject implements CodeRepoService
func (c *Client) CreateCodeRepoProject(ctx context.Context, options v1.CreateProjectOptions) (*v1.ProjectData, error) {
	groupName, subGroupName, err := splitGroup(options.Name)
	if err != nil {
		return nil, err
	}

	var group *models.Group

	groupDetail, err := c.getGroup(ctx, groupName)
	if err != nil {
		// assume group not exist
		params := groups.
			NewPostV4GroupsParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithName(groupName).
			WithPath(groupName)
		groupOK, err := c.client.Groups.PostV4Groups(params, c.authInfo)
		if err != nil {
			return nil, err
		}
		group = groupOK.Payload
		if subGroupName == "" {
			return gitlabGroupAsProjectData(*group)
		}
	}

	if groupDetail != nil && subGroupName == "" {
		return nil, fmt.Errorf("group %s is already exist", groupDetail.Name)
	}

	if group != nil && subGroupName != "" {
		// create sub group
		params := groups.
			NewPostV4GroupsParams().
			WithHTTPClient(c.httpClient).
			WithName(subGroupName).
			WithPath(subGroupName).
			WithParentID(&group.ID)
		groupOK, err := c.client.Groups.PostV4Groups(params, c.authInfo)
		if err != nil {
			return nil, err
		}
		group = groupOK.Payload
	}

	return gitlabGroupAsProjectData(*group)
}

func (c *Client) getGroup(ctx context.Context, fullName string) (*models.GroupDetail, error) {
	params := groups.
		NewGetV4GroupsIDParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithID(fullName)
	groupOK, err := c.client.Groups.GetV4GroupsID(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return groupOK.Payload, nil
}

func splitGroup(path string) (groupName, subGroupName string, err error) {
	if path == "" {
		return "", "", fmt.Errorf("error format of path, it cannot be empty")
	}

	segments := strings.Split(path, "/")
	if len(segments) > 2 {
		err := fmt.Errorf("error format of group path '%s', segments should not be more than 2", path)
		return "", "", err
	}

	groupName = segments[0]
	if len(segments) == 2 {
		subGroupName = segments[1]
	}
	return
}

// GetLatestRepoCommit implements CodeRepoService
func (c *Client) GetLatestRepoCommit(ctx context.Context, repoID, owner, repoName, repoFullName string) (commit *v1.RepositoryCommit, status *v1.HostPortStatus) {
	params := projects.
		NewGetV4ProjectsIDRepositoryCommitsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithID(repoFullName)
	payload, err := c.client.Projects.GetV4ProjectsIDRepositoryCommits(params, c.authInfo)
	if err != nil {
		return nil, &v1.HostPortStatus{
			StatusCode: 500,
			Response:   err.Error(),
		}
	}

	if len(payload.Payload) > 0 {
		commitAt, _ := time.Parse(time.RFC3339Nano, payload.Payload[0].CreatedAt)
		commit = &v1.RepositoryCommit{
			CommitID:       payload.Payload[0].ID,
			CommitAt:       &metav1.Time{Time: commitAt},
			CommitterName:  payload.Payload[0].CommitterName,
			CommitterEmail: payload.Payload[0].CommitterEmail,
			CommitMessage:  payload.Payload[0].Message,
		}
	}
	return commit, &v1.HostPortStatus{
		StatusCode:  200,
		LastAttempt: &metav1.Time{Time: time.Now()},
	}
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(c.httpClient, url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(ctx context.Context) (*v1.HostPortStatus, error) {
	params := user.
		NewGetV4UserParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	_, err := c.client.User.GetV4User(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return &v1.HostPortStatus{StatusCode: 200}, nil
}
