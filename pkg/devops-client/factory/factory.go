package factory

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/devops-client/pkg"
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
)

// NewClient returns a devops tool client
func NewClient(tool, version string, options *v1.Options) (v1.Interface, error) {
	factory, ok := pkg.GetRegistry()[strFirstToUpper(tool)]
	if !ok {
		return nil, fmt.Errorf("tool %s not exist", tool)
	}

	var clientFactory v1.ClientFactory

	if factory != nil {
		clientFactory = factory.Version(version)
		if clientFactory == nil {
			return nil, fmt.Errorf("tool %s has no version %s", tool, version)
		}
	}

	return clientFactory(options), nil
}

// convert tool type
// ex: Nexus/nexus -> Nexus
func strFirstToUpper(str string) string {
	if len(str) < 1 {
		return ""
	}
	strArry := []rune(str)
	if strArry[0] >= 97 && strArry[0] <= 122 {
		strArry[0] -= 32
	}
	return string(strArry)
}
