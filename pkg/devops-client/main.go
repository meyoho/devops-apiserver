package main

import (
	"alauda.io/devops-apiserver/pkg/devops-client/cmd"
)

func main() {
	cmd.Execute()
}
