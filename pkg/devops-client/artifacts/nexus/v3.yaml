basePath: /service/rest/
securityDefinitions:
  basicAuth:
    type: basic
security:
  - basicAuth: []
definitions:
  AssetXO:
    properties:
      checksum:
        additionalProperties:
          type: object
        type: object
      downloadUrl:
        type: string
      format:
        type: string
      id:
        type: string
      path:
        type: string
      repository:
        type: string
    type: object
  BlobStoreQuotaResultXO:
    properties:
      blobStoreName:
        type: string
      isViolation:
        type: boolean
      message:
        type: string
    type: object
  ComponentXO:
    properties:
      assets:
        items:
          $ref: '#/definitions/AssetXO'
        type: array
      format:
        type: string
      group:
        type: string
      id:
        type: string
      name:
        type: string
      repository:
        type: string
      version:
        type: string
    type: object
  Page:
    properties:
      continuationToken:
        type: string
      items:
        items:
          type: object
        type: array
    type: object
  PageAssetXO:
    properties:
      continuationToken:
        type: string
      items:
        items:
          $ref: '#/definitions/AssetXO'
        type: array
    type: object
  PageComponentXO:
    properties:
      continuationToken:
        type: string
      items:
        items:
          $ref: '#/definitions/ComponentXO'
        type: array
    type: object
  PageTaskXO:
    properties:
      continuationToken:
        type: string
      items:
        items:
          $ref: '#/definitions/TaskXO'
        type: array
    type: object
  ReadOnlyState:
    properties:
      frozen:
        type: boolean
      summaryReason:
        type: string
      systemInitiated:
        type: boolean
    type: object
  RepositoryXO:
    properties:
      format:
        type: string
      name:
        type: string
      type:
        type: string
      url:
        type: string
    type: object
  Request:
    properties:
      configuration:
        type: boolean
      jmx:
        type: boolean
      limitFileSizes:
        type: boolean
      limitZipSize:
        type: boolean
      log:
        type: boolean
      metrics:
        type: boolean
      security:
        type: boolean
      systemInformation:
        type: boolean
      taskLog:
        type: boolean
      threadDump:
        type: boolean
    type: object
  ScriptResultXO:
    properties:
      name:
        type: string
      result:
        type: string
    type: object
  ScriptXO:
    properties:
      content:
        type: string
      name:
        pattern: ^[a-zA-Z0-9\-]{1}[a-zA-Z0-9_\-\.]*$
        type: string
      type:
        type: string
    type: object
  TaskXO:
    properties:
      currentState:
        type: string
      id:
        type: string
      lastRun:
        format: date-time
        type: string
      lastRunResult:
        type: string
      message:
        type: string
      name:
        type: string
      nextRun:
        format: date-time
        type: string
      type:
        type: string
    type: object
  UploadDefinitionXO:
    properties:
      assetFields:
        items:
          $ref: '#/definitions/UploadFieldDefinitionXO'
        type: array
      componentFields:
        items:
          $ref: '#/definitions/UploadFieldDefinitionXO'
        type: array
      format:
        type: string
      multipleUpload:
        type: boolean
    type: object
  UploadFieldDefinitionXO:
    properties:
      description:
        type: string
      group:
        type: string
      name:
        type: string
      optional:
        type: boolean
      type:
        type: string
    type: object
info:
  title: Nexus Repository Manager REST API
  version: 3.14.0-04
paths:
  /v1/assets:
    get:
      consumes:
      - application/json
      description: ""
      operationId: getAssets
      parameters:
      - description: A token returned by a prior request. If present, the next page
          of results are returned
        in: query
        name: continuationToken
        required: false
        type: string
      - description: Repository from which you would like to retrieve assets.
        in: query
        name: repository
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/PageAssetXO'
        "403":
          description: Insufficient permissions to list assets
        "422":
          description: Parameter 'repository' is required
      summary: List assets
      tags:
      - assets
  /v1/assets/{id}:
    delete:
      consumes:
      - application/json
      description: ""
      operationId: deleteAsset
      parameters:
      - description: Id of the asset to delete
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "204":
          description: Asset was successfully deleted
        "403":
          description: Insufficient permissions to delete asset
        "404":
          description: Asset not found
        "422":
          description: Malformed ID
      summary: Delete a single asset
      tags:
      - assets
    get:
      consumes:
      - application/json
      description: ""
      operationId: getAssetById
      parameters:
      - description: Id of the asset to get
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/AssetXO'
        "403":
          description: Insufficient permissions to get asset
        "404":
          description: Asset not found
        "422":
          description: Malformed ID
      summary: Get a single asset
      tags:
      - assets
  /v1/blobstores/{id}/quota-status:
    get:
      consumes:
      - application/json
      description: ""
      operationId: quotaStatus
      parameters:
      - in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/BlobStoreQuotaResultXO'
      summary: Get quota status for a given blob store
      tags:
      - Blob Store
  /v1/components:
    get:
      consumes:
      - application/json
      description: ""
      operationId: getComponents
      parameters:
      - description: A token returned by a prior request. If present, the next page
          of results are returned
        in: query
        name: continuationToken
        required: false
        type: string
      - description: Repository from which you would like to retrieve components
        in: query
        name: repository
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/PageComponentXO'
        "403":
          description: Insufficient permissions to list components
        "422":
          description: Parameter 'repository' is required
      summary: List components
      tags:
      - components
    post:
      consumes:
      - multipart/form-data
      description: ""
      operationId: uploadComponent
      parameters:
      - description: Name of the repository to which you would like to upload the
          component
        in: query
        name: repository
        required: true
        type: string
      - description: 'rubygems Asset '
        in: formData
        name: rubygems.asset
        required: false
        type: file
      - description: 'nuget Asset '
        in: formData
        name: nuget.asset
        required: false
        type: file
      - description: raw Directory
        in: formData
        name: raw.directory
        required: false
        type: string
      - description: raw Asset 1
        in: formData
        name: raw.asset1
        required: false
        type: file
      - description: raw Asset 1 Filename
        in: formData
        name: raw.asset1.filename
        required: false
        type: string
      - description: raw Asset 2
        in: formData
        name: raw.asset2
        required: false
        type: file
      - description: raw Asset 2 Filename
        in: formData
        name: raw.asset2.filename
        required: false
        type: string
      - description: raw Asset 3
        in: formData
        name: raw.asset3
        required: false
        type: file
      - description: raw Asset 3 Filename
        in: formData
        name: raw.asset3.filename
        required: false
        type: string
      - description: 'npm Asset '
        in: formData
        name: npm.asset
        required: false
        type: file
      - description: 'pypi Asset '
        in: formData
        name: pypi.asset
        required: false
        type: file
      - description: maven2 Group ID
        in: formData
        name: maven2.groupId
        required: false
        type: string
      - description: maven2 Artifact ID
        in: formData
        name: maven2.artifactId
        required: false
        type: string
      - description: maven2 Version
        in: formData
        name: maven2.version
        required: false
        type: string
      - description: maven2 Generate a POM file with these coordinates
        in: formData
        name: maven2.generate-pom
        required: false
        type: boolean
      - description: maven2 Packaging
        in: formData
        name: maven2.packaging
        required: false
        type: string
      - description: maven2 Asset 1
        in: formData
        name: maven2.asset1
        required: false
        type: file
      - description: maven2 Asset 1 Classifier
        in: formData
        name: maven2.asset1.classifier
        required: false
        type: string
      - description: maven2 Asset 1 Extension
        in: formData
        name: maven2.asset1.extension
        required: false
        type: string
      - description: maven2 Asset 2
        in: formData
        name: maven2.asset2
        required: false
        type: file
      - description: maven2 Asset 2 Classifier
        in: formData
        name: maven2.asset2.classifier
        required: false
        type: string
      - description: maven2 Asset 2 Extension
        in: formData
        name: maven2.asset2.extension
        required: false
        type: string
      - description: maven2 Asset 3
        in: formData
        name: maven2.asset3
        required: false
        type: file
      - description: maven2 Asset 3 Classifier
        in: formData
        name: maven2.asset3.classifier
        required: false
        type: string
      - description: maven2 Asset 3 Extension
        in: formData
        name: maven2.asset3.extension
        required: false
        type: string
      produces:
      - application/json
      responses:
        "403":
          description: Insufficient permissions to upload a component
        "422":
          description: Parameter 'repository' is required
      summary: Upload a single component
      tags:
      - components
  /v1/components/{id}:
    delete:
      consumes:
      - application/json
      description: ""
      operationId: deleteComponent
      parameters:
      - description: ID of the component to delete
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "204":
          description: Component was successfully deleted
        "403":
          description: Insufficient permissions to delete component
        "404":
          description: Component not found
        "422":
          description: Malformed ID
      summary: Delete a single component
      tags:
      - components
    get:
      consumes:
      - application/json
      description: ""
      operationId: getComponentById
      parameters:
      - description: ID of the component to retrieve
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/ComponentXO'
        "403":
          description: Insufficient permissions to get component
        "404":
          description: Component not found
        "422":
          description: Malformed ID
      summary: Get a single component
      tags:
      - components
  /v1/formats/{format}/upload-specs:
    get:
      consumes:
      - application/json
      description: ""
      operationId: get_2
      parameters:
      - description: The desired repository format
        in: path
        name: format
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/UploadDefinitionXO'
      summary: Get upload field requirements for the desired format
      tags:
      - formats
  /v1/formats/upload-specs:
    get:
      consumes:
      - application/json
      description: ""
      operationId: get_1
      parameters: []
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            items:
              $ref: '#/definitions/UploadDefinitionXO'
            type: array
      summary: Get upload field requirements for each supported format
      tags:
      - formats
  /v1/read-only:
    get:
      consumes:
      - application/json
      description: ""
      operationId: get
      parameters: []
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/ReadOnlyState'
      summary: Get read-only state
      tags:
      - read-only
  /v1/read-only/force-release:
    post:
      consumes:
      - application/json
      description: 'Forcibly release read-only status, including System initiated
        tasks. Warning: may result in data loss.'
      operationId: forceRelease
      parameters: []
      produces:
      - application/json
      responses:
        "204":
          description: System is no longer read-only
        "403":
          description: Authentication required
        "404":
          description: No change to read-only state
      summary: Forcibly release read-only
      tags:
      - read-only
  /v1/read-only/freeze:
    post:
      consumes:
      - application/json
      description: ""
      operationId: freeze
      parameters: []
      produces:
      - application/json
      responses:
        "204":
          description: System is now read-only
        "403":
          description: Authentication required
        "404":
          description: No change to read-only state
      summary: Enable read-only
      tags:
      - read-only
  /v1/read-only/release:
    post:
      consumes:
      - application/json
      description: Release administrator initiated read-only status. Will not release
        read-only caused by system tasks.
      operationId: release
      parameters: []
      produces:
      - application/json
      responses:
        "204":
          description: System is no longer read-only
        "403":
          description: Authentication required
        "404":
          description: No change to read-only state
      summary: Release read-only
      tags:
      - read-only
  /v1/repositories:
    get:
      consumes:
      - application/json
      description: ""
      operationId: getRepositories
      parameters: []
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            items:
              $ref: '#/definitions/RepositoryXO'
            type: array
      summary: List repositories
      tags:
      - repositories
  /v1/script:
    get:
      consumes:
      - application/json
      description: ""
      operationId: browse
      parameters: []
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            items:
              $ref: '#/definitions/ScriptXO'
            type: array
      summary: List all stored scripts
      tags:
      - script
    post:
      consumes:
      - application/json
      description: ""
      operationId: add
      parameters:
      - in: body
        name: body
        required: false
        schema:
          $ref: '#/definitions/ScriptXO'
      produces:
      - application/json
      responses:
        "204":
          description: Script was added
      summary: Add a new script
      tags:
      - script
  /v1/script/{name}:
    delete:
      consumes:
      - application/json
      description: ""
      operationId: delete
      parameters:
      - in: path
        name: name
        required: true
        type: string
      produces:
      - application/json
      responses:
        "204":
          description: Script was deleted
        "404":
          description: No script with the specified name
      summary: Delete stored script by name
      tags:
      - script
    get:
      consumes:
      - application/json
      description: ""
      operationId: read
      parameters:
      - in: path
        name: name
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/ScriptXO'
        "404":
          description: No script with the specified name
      summary: Read stored script by name
      tags:
      - script
    put:
      consumes:
      - application/json
      description: ""
      operationId: edit
      parameters:
      - in: path
        name: name
        required: true
        type: string
      - in: body
        name: body
        required: false
        schema:
          $ref: '#/definitions/ScriptXO'
      produces:
      - application/json
      responses:
        "204":
          description: Script was updated
        "404":
          description: No script with the specified name
      summary: Update stored script by name
      tags:
      - script
  /v1/script/{name}/run:
    post:
      consumes:
      - text/plain
      description: ""
      operationId: run_1
      parameters:
      - in: path
        name: name
        required: true
        type: string
      - in: body
        name: body
        required: false
        schema:
          type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/ScriptResultXO'
        "404":
          description: No script with the specified name
        "500":
          description: Script execution failed with exception
      summary: Run stored script by name
      tags:
      - script
  /v1/search:
    get:
      consumes:
      - application/json
      description: ""
      operationId: search
      parameters:
      - description: A token returned by a prior request. If present, the next page
          of results are returned
        in: query
        name: continuationToken
        required: false
        type: string
      - description: Query by keyword
        in: query
        name: q
        required: false
        type: string
      - description: Repository name
        in: query
        name: repository
        required: false
        type: string
      - description: Query by format
        in: query
        name: format
        required: false
        type: string
      - description: Component group
        in: query
        name: group
        required: false
        type: string
      - description: Component name
        in: query
        name: name
        required: false
        type: string
      - description: Component version
        in: query
        name: version
        required: false
        type: string
      - description: Specific MD5 hash of component's asset
        in: query
        name: md5
        required: false
        type: string
      - description: Specific SHA-1 hash of component's asset
        in: query
        name: sha1
        required: false
        type: string
      - description: Specific SHA-256 hash of component's asset
        in: query
        name: sha256
        required: false
        type: string
      - description: Specific SHA-512 hash of component's asset
        in: query
        name: sha512
        required: false
        type: string
      - description: Docker image name
        in: query
        name: docker.imageName
        required: false
        type: string
      - description: Docker image tag
        in: query
        name: docker.imageTag
        required: false
        type: string
      - description: Docker layer ID
        in: query
        name: docker.layerId
        required: false
        type: string
      - description: Docker content digest
        in: query
        name: docker.contentDigest
        required: false
        type: string
      - description: Maven groupId
        in: query
        name: maven.groupId
        required: false
        type: string
      - description: Maven artifactId
        in: query
        name: maven.artifactId
        required: false
        type: string
      - description: Maven base version
        in: query
        name: maven.baseVersion
        required: false
        type: string
      - description: Maven extension of component's asset
        in: query
        name: maven.extension
        required: false
        type: string
      - description: Maven classifier of component's asset
        in: query
        name: maven.classifier
        required: false
        type: string
      - description: NPM scope
        in: query
        name: npm.scope
        required: false
        type: string
      - description: Nuget id
        in: query
        name: nuget.id
        required: false
        type: string
      - description: Nuget tags
        in: query
        name: nuget.tags
        required: false
        type: string
      - description: PyPi classifiers
        in: query
        name: pypi.classifiers
        required: false
        type: string
      - description: PyPi description
        in: query
        name: pypi.description
        required: false
        type: string
      - description: PyPi keywords
        in: query
        name: pypi.keywords
        required: false
        type: string
      - description: PyPi summary
        in: query
        name: pypi.summary
        required: false
        type: string
      - description: RubyGems description
        in: query
        name: rubygems.description
        required: false
        type: string
      - description: RubyGems platform
        in: query
        name: rubygems.platform
        required: false
        type: string
      - description: RubyGems summary
        in: query
        name: rubygems.summary
        required: false
        type: string
      - description: Yum architecture
        in: query
        name: yum.architecture
        required: false
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/PageComponentXO'
      summary: Search components
      tags:
      - search
  /v1/search/assets:
    get:
      consumes:
      - application/json
      description: ""
      operationId: searchAssets
      parameters:
      - description: A token returned by a prior request. If present, the next page
          of results are returned
        in: query
        name: continuationToken
        required: false
        type: string
      - description: Query by keyword
        in: query
        name: q
        required: false
        type: string
      - description: Repository name
        in: query
        name: repository
        required: false
        type: string
      - description: Query by format
        in: query
        name: format
        required: false
        type: string
      - description: Component group
        in: query
        name: group
        required: false
        type: string
      - description: Component name
        in: query
        name: name
        required: false
        type: string
      - description: Component version
        in: query
        name: version
        required: false
        type: string
      - description: Specific MD5 hash of component's asset
        in: query
        name: md5
        required: false
        type: string
      - description: Specific SHA-1 hash of component's asset
        in: query
        name: sha1
        required: false
        type: string
      - description: Specific SHA-256 hash of component's asset
        in: query
        name: sha256
        required: false
        type: string
      - description: Specific SHA-512 hash of component's asset
        in: query
        name: sha512
        required: false
        type: string
      - description: Docker image name
        in: query
        name: docker.imageName
        required: false
        type: string
      - description: Docker image tag
        in: query
        name: docker.imageTag
        required: false
        type: string
      - description: Docker layer ID
        in: query
        name: docker.layerId
        required: false
        type: string
      - description: Docker content digest
        in: query
        name: docker.contentDigest
        required: false
        type: string
      - description: Maven groupId
        in: query
        name: maven.groupId
        required: false
        type: string
      - description: Maven artifactId
        in: query
        name: maven.artifactId
        required: false
        type: string
      - description: Maven base version
        in: query
        name: maven.baseVersion
        required: false
        type: string
      - description: Maven extension of component's asset
        in: query
        name: maven.extension
        required: false
        type: string
      - description: Maven classifier of component's asset
        in: query
        name: maven.classifier
        required: false
        type: string
      - description: NPM scope
        in: query
        name: npm.scope
        required: false
        type: string
      - description: Nuget id
        in: query
        name: nuget.id
        required: false
        type: string
      - description: Nuget tags
        in: query
        name: nuget.tags
        required: false
        type: string
      - description: PyPi classifiers
        in: query
        name: pypi.classifiers
        required: false
        type: string
      - description: PyPi description
        in: query
        name: pypi.description
        required: false
        type: string
      - description: PyPi keywords
        in: query
        name: pypi.keywords
        required: false
        type: string
      - description: PyPi summary
        in: query
        name: pypi.summary
        required: false
        type: string
      - description: RubyGems description
        in: query
        name: rubygems.description
        required: false
        type: string
      - description: RubyGems platform
        in: query
        name: rubygems.platform
        required: false
        type: string
      - description: RubyGems summary
        in: query
        name: rubygems.summary
        required: false
        type: string
      - description: Yum architecture
        in: query
        name: yum.architecture
        required: false
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/PageAssetXO'
      summary: Search assets
      tags:
      - search
  /v1/search/assets/download:
    get:
      consumes:
      - application/json
      description: Returns a 302 Found with location header field set to download
        URL. Search must return a single asset to receive download URL.
      operationId: searchAndDownloadAssets
      parameters:
      - description: Query by keyword
        in: query
        name: q
        required: false
        type: string
      - description: Repository name
        in: query
        name: repository
        required: false
        type: string
      - description: Query by format
        in: query
        name: format
        required: false
        type: string
      - description: Component group
        in: query
        name: group
        required: false
        type: string
      - description: Component name
        in: query
        name: name
        required: false
        type: string
      - description: Component version
        in: query
        name: version
        required: false
        type: string
      - description: Specific MD5 hash of component's asset
        in: query
        name: md5
        required: false
        type: string
      - description: Specific SHA-1 hash of component's asset
        in: query
        name: sha1
        required: false
        type: string
      - description: Specific SHA-256 hash of component's asset
        in: query
        name: sha256
        required: false
        type: string
      - description: Specific SHA-512 hash of component's asset
        in: query
        name: sha512
        required: false
        type: string
      - description: Docker image name
        in: query
        name: docker.imageName
        required: false
        type: string
      - description: Docker image tag
        in: query
        name: docker.imageTag
        required: false
        type: string
      - description: Docker layer ID
        in: query
        name: docker.layerId
        required: false
        type: string
      - description: Docker content digest
        in: query
        name: docker.contentDigest
        required: false
        type: string
      - description: Maven groupId
        in: query
        name: maven.groupId
        required: false
        type: string
      - description: Maven artifactId
        in: query
        name: maven.artifactId
        required: false
        type: string
      - description: Maven base version
        in: query
        name: maven.baseVersion
        required: false
        type: string
      - description: Maven extension of component's asset
        in: query
        name: maven.extension
        required: false
        type: string
      - description: Maven classifier of component's asset
        in: query
        name: maven.classifier
        required: false
        type: string
      - description: NPM scope
        in: query
        name: npm.scope
        required: false
        type: string
      - description: Nuget id
        in: query
        name: nuget.id
        required: false
        type: string
      - description: Nuget tags
        in: query
        name: nuget.tags
        required: false
        type: string
      - description: PyPi classifiers
        in: query
        name: pypi.classifiers
        required: false
        type: string
      - description: PyPi description
        in: query
        name: pypi.description
        required: false
        type: string
      - description: PyPi keywords
        in: query
        name: pypi.keywords
        required: false
        type: string
      - description: PyPi summary
        in: query
        name: pypi.summary
        required: false
        type: string
      - description: RubyGems description
        in: query
        name: rubygems.description
        required: false
        type: string
      - description: RubyGems platform
        in: query
        name: rubygems.platform
        required: false
        type: string
      - description: RubyGems summary
        in: query
        name: rubygems.summary
        required: false
        type: string
      - description: Yum architecture
        in: query
        name: yum.architecture
        required: false
        type: string
      produces:
      - application/json
      responses:
        "400":
          description: Search returned multiple assets, please refine search criteria
            to find a single asset
        "404":
          description: Asset search returned no results
      summary: Search and download asset
      tags:
      - search
  /v1/support/supportzip:
    post:
      consumes:
      - application/json
      description: ""
      operationId: supportzip
      parameters:
      - in: body
        name: body
        required: false
        schema:
          $ref: '#/definitions/Request'
      produces:
      - application/octet-stream
      responses:
        default:
          description: successful operation
      summary: Creates and downloads a support zip
      tags:
      - support
  /v1/tasks:
    get:
      consumes:
      - application/json
      description: ""
      operationId: getTasks
      parameters:
      - description: Type of the tasks to get
        in: query
        name: type
        required: false
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/PageTaskXO'
      summary: List tasks
      tags:
      - tasks
  /v1/tasks/{id}:
    get:
      consumes:
      - application/json
      description: ""
      operationId: getTaskById
      parameters:
      - description: Id of the task to get
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: successful operation
          schema:
            $ref: '#/definitions/TaskXO'
        "404":
          description: Task not found
      summary: Get a single task by id
      tags:
      - tasks
  /v1/tasks/{id}/run:
    post:
      consumes:
      - application/json
      description: ""
      operationId: run
      parameters:
      - description: Id of the task to run
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "204":
          description: Task was run
        "404":
          description: Task not found
      summary: Run task
      tags:
      - tasks
  /v1/tasks/{id}/stop:
    post:
      consumes:
      - application/json
      description: ""
      operationId: stop
      parameters:
      - description: Id of the task to stop
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "204":
          description: Task was stopped
        "404":
          description: Task not found
        "409":
          description: Unable to stop task
      summary: Stop task
      tags:
      - tasks
swagger: "2.0"
tags:
- name: read-only
- name: Blob Store
- name: tasks
- name: assets
- name: components
- name: search
- name: formats
- name: repositories
- name: support
- name: script

