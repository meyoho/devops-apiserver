package pkg

import (
	"sync"

	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
)

// Versioner gets a verioned client
type Versioner interface {
	Inject(versionOpt)
	Version(v string) v1.ClientFactory
}

type versionedFactory struct {
	sync.RWMutex
	factory map[string]v1.ClientFactory
}

func (f *versionedFactory) Inject(opt versionOpt) {
	f.Lock()
	f.factory[opt.version] = opt.factory
	if opt.isDefault {
		f.factory[""] = opt.factory
	}
	f.Unlock()
}

func (f *versionedFactory) Version(v string) v1.ClientFactory {
	f.RLock()
	defer f.RUnlock()

	return f.factory[v]
}

var (
	once            sync.Once
	lock            sync.Mutex
	internalFactory map[string]Versioner
)

type versionOpt struct {
	version   string
	factory   v1.ClientFactory
	isDefault bool
}

// GetRegistry gets register map
func GetRegistry() map[string]Versioner {
	return internalFactory
}

// register tool
func register(toolName string, opts ...versionOpt) {
	once.Do(func() {
		internalFactory = make(map[string]Versioner)
	})
	lock.Lock()
	if _, ok := internalFactory[toolName]; !ok {
		internalFactory[toolName] = &versionedFactory{
			factory: make(map[string]v1.ClientFactory),
		}
	}

	for _, opt := range opts {
		internalFactory[toolName].Inject(opt)
	}
	lock.Unlock()
}
