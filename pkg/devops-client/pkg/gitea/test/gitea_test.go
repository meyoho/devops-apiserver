package test

import (
	"context"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	giteav1_9_4 "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitea/v1_9_4"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool Gitea tests", func() {
	var (
		// host
		host    = "https://gitea.test"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options

		transport  = gock.NewTransport()
		httpClient = clientv1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = giteav1_9_4.NewClient()
		opts = clientv1.NewOptions(clientv1.NewBasicConfig("gitea.test", "", []string{}), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetRemoteRepos", func() {
		const (
			repos = `[
    {
        "clone_url": "http://localhost:3000/user1/project1.git",
        "created_at": "2019-10-04T13:19:58Z",
        "default_branch": "master",
        "description": "dododododo",
        "empty": false,
        "fork": false,
        "forks_count": 0,
        "full_name": "user1/project1",
        "html_url": "http://localhost:3000/user1/project1",
        "id": 1,
        "mirror": false,
        "name": "project1",
        "open_issues_count": 1,
        "owner": {
            "avatar_url": "https://secure.gravatar.com/avatar/9eee5d330a8e3fa53527181387d76321?d=identicon",
            "email": "user@qq.com",
            "full_name": "",
            "id": 1,
            "login": "user1",
            "username": "user1"
        },
        "parent": null,
        "permissions": {
            "admin": true,
            "pull": true,
            "push": true
        },
        "private": true,
        "size": 69632,
        "ssh_url": "git@49.4.2.82:user1/project1.git",
        "stars_count": 1,
        "updated_at": "2019-10-15T06:20:15Z",
        "watchers_count": 1,
        "website": ""
    },
    {
        "clone_url": "http://localhost:3000/group1/group1-project1.git",
        "created_at": "2019-10-09T07:14:25Z",
        "default_branch": "master",
        "description": "xxxxxxxxxxhash",
        "empty": false,
        "fork": false,
        "forks_count": 0,
        "full_name": "group1/group1-project1",
        "html_url": "http://localhost:3000/group1/group1-project1",
        "id": 2,
        "mirror": false,
        "name": "group1-project1",
        "open_issues_count": 0,
        "owner": {
            "avatar_url": "http://localhost:3000/avatars/2",
            "email": "",
            "full_name": "",
            "id": 2,
            "login": "group1",
            "username": "group1"
        },
        "parent": null,
        "permissions": {
            "admin": true,
            "pull": true,
            "push": true
        },
        "private": true,
        "size": 0,
        "ssh_url": "git@49.4.2.82:group1/group1-project1.git",
        "stars_count": 0,
        "updated_at": "2019-10-09T07:14:26Z",
        "watchers_count": 2,
        "website": ""
    }
]`
			orgs = `[
    {
        "id": 2,
        "username": "group1",
        "full_name": "",
        "avatar_url": "http://localhost:3000/avatars/2",
        "description": "",
        "website": "",
        "location": ""
    },
    {
        "id": 3,
        "username": "group2",
        "full_name": "",
        "avatar_url": "http://localhost:3000/avatars/3",
        "description": "",
        "website": "",
        "location": ""
    }
]`
		)

		BeforeEach(func() {
			gock.New(host).
				Get("/user/repos").
				Reply(200).
				JSON(repos)
			gock.New(host).
				Get("/user/orgs").
				Reply(200).
				JSON(orgs)
		})

		It("list repos", func() {
			repos, err := client.GetRemoteRepos(context.Background())
			Expect(err).To(BeNil())
			Expect(len(repos.Owners)).To(Equal(2))
			if repos.Owners[0].Name == "user1" {
				Expect(repos.Owners[0].Name).To(Equal("user1"))
				Expect(repos.Owners[0].Type).To(Equal(v1.OriginCodeRepoOwnerType("User")))
				Expect(repos.Owners[1].Name).To(Equal("group1"))
				Expect(repos.Owners[1].Type).To(Equal(v1.OriginCodeRepoOwnerType("Org")))
				Expect(len(repos.Owners[0].Repositories)).To(Equal(1))
				Expect(repos.Owners[0].Repositories[0].Name).To(Equal("project1"))
			} else {
				Expect(repos.Owners[1].Name).To(Equal("user1"))
				Expect(repos.Owners[1].Type).To(Equal(v1.OriginCodeRepoOwnerType("User")))
				Expect(repos.Owners[0].Name).To(Equal("group1"))
				Expect(repos.Owners[0].Type).To(Equal(v1.OriginCodeRepoOwnerType("Org")))
				Expect(len(repos.Owners[0].Repositories)).To(Equal(1))
				Expect(repos.Owners[0].Repositories[0].Name).To(Equal("group1-project1"))
			}
		})
	})

	Context("GetBranches", func() {
		const branches = `[
    {
        "name": "master",
        "commit": {
            "id": "156a53ba13c2d1474df292d201d802e335e79557",
            "message": "Initial commit\n",
            "url": "Not implemented",
            "author": {
                "name": "user1",
                "email": "user@qq.com",
                "username": "user1"
            },
            "committer": {
                "name": "Gogs",
                "email": "gogs@fake.local",
                "username": ""
            },
            "added": null,
            "removed": null,
            "modified": null,
            "timestamp": "2019-10-04T13:19:59Z"
        }
    }
]`
		BeforeEach(func() {
			gock.New(host).
				Get("/repos/user1/project1/branches").
				Reply(200).
				JSON(branches)
		})

		It("get repo branches", func() {
			branches, err := client.GetBranches(context.Background(), "user1", "project1", "")
			Expect(err).To(BeNil())
			Expect(len(branches)).To(Equal(1))
			Expect(branches[0].Name).To(Equal("master"))
			Expect(branches[0].Commit).To(Equal("156a53ba13c2d1474df292d201d802e335e79557"))
		})
	})

	Context("CreateCodeRepoProject", func() {
		const (
			orgs = `{
    "id": 3,
    "username": "group2",
    "full_name": "",
    "avatar_url": "http://localhost:3000/avatars/3",
    "description": "",
    "website": "",
    "location": ""
}`
		)
		BeforeEach(func() {
			gock.New(host).
				Post("/orgs").
				Reply(201).
				JSON(orgs)
		})
		It("create group", func() {
			org, err := client.CreateCodeRepoProject(context.Background(), v1.CreateProjectOptions{Name: "group2"})
			Expect(err).To(BeNil())
			Expect(org.Name).To(Equal("group2"))
		})
	})

	Context("ListCodeRepoProjects", func() {
		const (
			orgs = `[
    {
        "id": 2,
        "username": "group1",
        "full_name": "",
        "avatar_url": "http://localhost:3000/avatars/2",
        "description": "",
        "website": "",
        "location": ""
    },
    {
        "id": 3,
        "username": "group2",
        "full_name": "",
        "avatar_url": "http://localhost:3000/avatars/3",
        "description": "",
        "website": "",
        "location": ""
    }
]`

			user = `{
    "id": 1,
    "username": "user1",
    "login": "user1",
    "full_name": "",
    "email": "user@qq.com",
    "avatar_url": "https://secure.gravatar.com/avatar/9eee5d330a8e3fa53527181387d76321?d=identicon"
}`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/user/orgs").
				Reply(200).
				JSON(orgs)
			gock.New(host).
				Get("/user").
				Reply(200).
				JSON(user)
		})

		It("list groups", func() {
			groups, err := client.ListCodeRepoProjects(context.Background(), v1.ListProjectOptions{})
			Expect(err).To(BeNil())
			Expect(groups.Items).To(HaveLen(3))
			Expect(groups.Items[0].Name).To(Equal("group1"))
			Expect(groups.Items[1].Name).To(Equal("group2"))
			Expect(groups.Items[2].Name).To(Equal("user1"))
		})
	})

})
