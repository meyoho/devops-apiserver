package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v2 "alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2"
)

func init() {
	register(devops.CodeRepoServiceTypeBitbucket.String(), versionOpt{
		version:   "v2",
		factory:   v2.NewClient(),
		isDefault: true,
	})
}
