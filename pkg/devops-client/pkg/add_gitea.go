package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v1_9_4 "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitea/v1_9_4"
)

func init() {
	register(devops.CodeRepoServiceTypeGitea.String(), versionOpt{
		version:   "v1.9.4",
		factory:   v1_9_4.NewClient(),
		isDefault: true,
	})
}
