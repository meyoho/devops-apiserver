package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v3 "alauda.io/devops-apiserver/pkg/devops-client/pkg/nexus/v3"
	v3_ext "alauda.io/devops-apiserver/pkg/devops-client/pkg/nexus/v3_ext"
)

func init() {
	register(devops.ArtifactRegistryManagerTypeNexus.String(), versionOpt{
		version:   "v3",
		factory:   v3.NewClient(),
		isDefault: true,
	})

	register(devops.ArtifactRegistryManagerTypeNexus.String(), versionOpt{
		version:   "v3_ext",
		factory:   v3_ext.NewClient(),
		isDefault: false,
	})
}
