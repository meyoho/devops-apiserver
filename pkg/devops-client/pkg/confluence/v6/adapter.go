package v6

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"
	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/confluence/v6/client"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/confluence/v6/client/operations"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/confluence/v6/models"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/generic"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/transport"
	"github.com/go-logr/logr"
	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	clientv1.NotImplement
	logger     logr.Logger
	client     *client.Confluence
	opts       *clientv1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ clientv1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() clientv1.ClientFactory {
	return func(opts *clientv1.Options) clientv1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

// GetProjects implements ProjectService
func (c *Client) GetProjects(ctx context.Context, page, pagesize string) (*v1.ProjectDataList, error) {
	var (
		projectPage     int64
		projectPageSize int64
		err             error
	)

	if page != "" && pagesize != "" {
		projectPage, err = strconv.ParseInt(page, 10, 32)
		projectPageSize, err = strconv.ParseInt(pagesize, 10, 32)
		if err != nil {
			return nil, err
		}
	} else {
		projectPage = 0
		projectPageSize = 500
	}

	start := projectPage * projectPageSize

	projectsParam := operations.
		NewGetSpaceParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithStart(start).
		WithLimit(projectPageSize)
	projectsOK, err := c.client.Operations.GetSpace(projectsParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := &v1.ProjectDataList{}
	for _, space := range projectsOK.Payload.Results {
		projectData := v1.ProjectData{}
		projectData.Annotations = map[string]string{
			"key":  space.Key,
			"id":   strconv.Itoa(int(space.ID)),
			"type": space.Type,
		}
		projectData.Name = space.Name
		result.Items = append(result.Items, projectData)
	}

	return result, nil
}

// CreateProject implements ProjectService
func (c *Client) CreateProject(ctx context.Context, projectName, projectDescription, projectLead, projectKey string) (*v1.ProjectData, error) {
	space := &models.SpaceCreate{
		Name: projectName,
		Key:  projectKey,
		Description: &models.SpaceCreateDescription{
			Plain: &models.SpaceCreateDescriptionPlain{
				Value: projectDescription,
			},
		},
	}
	projectParam := operations.
		NewPostSpaceParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithSpace(space)
	projectOK, err := c.client.Operations.PostSpace(projectParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	spacesuburl := fmt.Sprintf("display/%s", projectKey)
	projectSelf := fmt.Sprintf("%s/%s", strings.TrimRight(c.opts.BasicConfig.Host, "/"), strings.TrimLeft(spacesuburl, "/"))

	result := &v1.ProjectData{}
	result.Name = projectName
	result.Annotations = map[string]string{
		"self":        projectSelf,
		"id":          fmt.Sprintf("%d", projectOK.Payload.ID),
		"description": projectDescription,
	}

	return result, nil
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(c.httpClient, url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(ctx context.Context) (*v1.HostPortStatus, error) {
	_, err := c.GetProjects(ctx, "1", "10")
	if err != nil {
		return nil, err
	}

	return &v1.HostPortStatus{StatusCode: 200}, nil
}
