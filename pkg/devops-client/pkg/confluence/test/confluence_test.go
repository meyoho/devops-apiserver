package test

import (
	"context"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	v6 "alauda.io/devops-apiserver/pkg/devops-client/pkg/confluence/v6"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool confluence tests", func() {
	var (
		// host
		host    = "http://confluence.test"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options

		transport  = gock.NewTransport()
		httpClient = clientv1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = v6.NewClient()
		opts = clientv1.NewOptions(clientv1.NewBasicConfig("confluence.test", "", []string{}), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		gock.OffAll()
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetProjects", func() {
		const (
			projects = `{
				"results": [
					{
						"id": 98305,
						"key": "ds",
						"name": "Demonstration Space",
						"type": "global",
						"_links": {
							"webui": "/display/ds",
							"self": "http://150.109.47.24:8090/rest/api/space/ds"
						},
						"_expandable": {
							"metadata": "",
							"icon": "",
							"description": "",
							"homepage": "/rest/api/content/65557"
						}
					},
					{
						"id": 98306,
						"key": "SPC",
						"name": "产品开发",
						"type": "global",
						"_links": {
							"webui": "/display/SPC",
							"self": "http://150.109.47.24:8090/rest/api/space/SPC"
						},
						"_expandable": {
							"metadata": "",
							"icon": "",
							"description": "",
							"homepage": "/rest/api/content/65591"
						}
					}
				],
				"start": 0,
				"limit": 25,
				"size": 2,
				"_links": {
					"self": "http://150.109.47.24:8090/rest/api/space",
					"base": "http://150.109.47.24:8090",
					"context": ""
				}
			}`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/space").
				Reply(200).
				JSON(projects)
		})

		It("get projects", func() {
			projects, err := client.GetProjects(context.Background(), "", "")
			Expect(err).To(BeNil())
			Expect(len(projects.Items)).To(Equal(2))
			Expect(projects.Items[1].Name).To(Equal("产品开发"))
		})
	})

	Context("CreateProject", func() {
		const (
			project = `{
				"id": 98307,
				"key": "PKK",
				"name": "project1",
				"description": {
					"plain": {
						"value": "",
						"representation": "plain"
					},
					"_expandable": {
						"view": ""
					}
				},
				"homepage": {
					"id": "65610",
					"type": "page",
					"status": "current",
					"title": "project1 Home",
					"extensions": {
						"position": "none"
					},
					"_links": {
						"webui": "/display/PKK/project1+Home",
						"edit": "/pages/resumedraft.action?draftId=65610",
						"tinyui": "/x/SgAB",
						"self": "http://150.109.47.24:8090/rest/api/content/65610"
					},
					"_expandable": {
						"container": "/rest/api/space/PKK",
						"metadata": "",
						"operations": "",
						"children": "/rest/api/content/65610/child",
						"restrictions": "/rest/api/content/65610/restriction/byOperation",
						"history": "/rest/api/content/65610/history",
						"ancestors": "",
						"body": "",
						"version": "",
						"descendants": "/rest/api/content/65610/descendant",
						"space": "/rest/api/space/PKK"
					}
				},
				"type": "global",
				"_links": {
					"webui": "/display/PKK",
					"collection": "/rest/api/space",
					"base": "http://150.109.47.24:8090",
					"context": "",
					"self": "http://150.109.47.24:8090/rest/api/space/PKK"
				},
				"_expandable": {
					"metadata": "",
					"icon": ""
				}
			}`
		)
		BeforeEach(func() {
			gock.New(host).
				Post("/space").
				Reply(200).
				JSON(project)
		})

		It("create a project", func() {
			project, err := client.CreateProject(context.Background(), "project1", "", "", "PKK")
			Expect(err).To(BeNil())
			Expect(project.Name).To(Equal("project1"))
		})
	})

})
