// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// TestReportSummary test report summary
// swagger:model TestReportSummary
type TestReportSummary struct {

	// class
	Class string `json:"_class,omitempty"`

	// links
	Links *BranchImpllinks `json:"_links,omitempty"`

	// existing failed
	ExistingFailed int64 `json:"existingFailed,omitempty"`

	// failed
	Failed int64 `json:"failed,omitempty"`

	// fixed
	Fixed int64 `json:"fixed,omitempty"`

	// passed
	Passed int64 `json:"passed,omitempty"`

	// regressions
	Regressions int64 `json:"regressions,omitempty"`

	// skipped
	Skipped int64 `json:"skipped,omitempty"`

	// total
	Total int64 `json:"total,omitempty"`
}

// Validate validates this test report summary
func (m *TestReportSummary) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateLinks(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *TestReportSummary) validateLinks(formats strfmt.Registry) error {

	if swag.IsZero(m.Links) { // not required
		return nil
	}

	if m.Links != nil {
		if err := m.Links.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("_links")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *TestReportSummary) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *TestReportSummary) UnmarshalBinary(b []byte) error {
	var res TestReportSummary
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
