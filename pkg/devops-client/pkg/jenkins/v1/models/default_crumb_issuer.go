// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// DefaultCrumbIssuer default crumb issuer
// swagger:model DefaultCrumbIssuer
type DefaultCrumbIssuer struct {

	// class
	Class string `json:"_class,omitempty"`

	// crumb
	Crumb string `json:"crumb,omitempty"`

	// crumb request field
	CrumbRequestField string `json:"crumbRequestField,omitempty"`
}

// Validate validates this default crumb issuer
func (m *DefaultCrumbIssuer) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *DefaultCrumbIssuer) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *DefaultCrumbIssuer) UnmarshalBinary(b []byte) error {
	var res DefaultCrumbIssuer
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
