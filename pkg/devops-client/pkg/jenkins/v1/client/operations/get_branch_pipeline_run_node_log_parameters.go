// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetBranchPipelineRunNodeLogParams creates a new GetBranchPipelineRunNodeLogParams object
// with the default values initialized.
func NewGetBranchPipelineRunNodeLogParams() *GetBranchPipelineRunNodeLogParams {
	var ()
	return &GetBranchPipelineRunNodeLogParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetBranchPipelineRunNodeLogParamsWithTimeout creates a new GetBranchPipelineRunNodeLogParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetBranchPipelineRunNodeLogParamsWithTimeout(timeout time.Duration) *GetBranchPipelineRunNodeLogParams {
	var ()
	return &GetBranchPipelineRunNodeLogParams{

		timeout: timeout,
	}
}

// NewGetBranchPipelineRunNodeLogParamsWithContext creates a new GetBranchPipelineRunNodeLogParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetBranchPipelineRunNodeLogParamsWithContext(ctx context.Context) *GetBranchPipelineRunNodeLogParams {
	var ()
	return &GetBranchPipelineRunNodeLogParams{

		Context: ctx,
	}
}

// NewGetBranchPipelineRunNodeLogParamsWithHTTPClient creates a new GetBranchPipelineRunNodeLogParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetBranchPipelineRunNodeLogParamsWithHTTPClient(client *http.Client) *GetBranchPipelineRunNodeLogParams {
	var ()
	return &GetBranchPipelineRunNodeLogParams{
		HTTPClient: client,
	}
}

/*GetBranchPipelineRunNodeLogParams contains all the parameters to send to the API endpoint
for the get branch pipeline run node log operation typically these are written to a http.Request
*/
type GetBranchPipelineRunNodeLogParams struct {

	/*BranchName
	  Name of the branch

	*/
	BranchName string
	/*Name
	  name of pipeline or pipelineconfig

	*/
	Name string
	/*Namespace
	  namespace of pipeline or pipelineconfig

	*/
	Namespace string
	/*Node
	  Name of the node

	*/
	Node string
	/*Organization
	  Name of the organization

	*/
	Organization string
	/*Run
	  Name of the run

	*/
	Run string
	/*Start
	  Start of log

	*/
	Start *int64

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) WithTimeout(timeout time.Duration) *GetBranchPipelineRunNodeLogParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) WithContext(ctx context.Context) *GetBranchPipelineRunNodeLogParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) WithHTTPClient(client *http.Client) *GetBranchPipelineRunNodeLogParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBranchName adds the branchName to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) WithBranchName(branchName string) *GetBranchPipelineRunNodeLogParams {
	o.SetBranchName(branchName)
	return o
}

// SetBranchName adds the branchName to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) SetBranchName(branchName string) {
	o.BranchName = branchName
}

// WithName adds the name to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) WithName(name string) *GetBranchPipelineRunNodeLogParams {
	o.SetName(name)
	return o
}

// SetName adds the name to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) SetName(name string) {
	o.Name = name
}

// WithNamespace adds the namespace to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) WithNamespace(namespace string) *GetBranchPipelineRunNodeLogParams {
	o.SetNamespace(namespace)
	return o
}

// SetNamespace adds the namespace to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) SetNamespace(namespace string) {
	o.Namespace = namespace
}

// WithNode adds the node to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) WithNode(node string) *GetBranchPipelineRunNodeLogParams {
	o.SetNode(node)
	return o
}

// SetNode adds the node to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) SetNode(node string) {
	o.Node = node
}

// WithOrganization adds the organization to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) WithOrganization(organization string) *GetBranchPipelineRunNodeLogParams {
	o.SetOrganization(organization)
	return o
}

// SetOrganization adds the organization to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) SetOrganization(organization string) {
	o.Organization = organization
}

// WithRun adds the run to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) WithRun(run string) *GetBranchPipelineRunNodeLogParams {
	o.SetRun(run)
	return o
}

// SetRun adds the run to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) SetRun(run string) {
	o.Run = run
}

// WithStart adds the start to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) WithStart(start *int64) *GetBranchPipelineRunNodeLogParams {
	o.SetStart(start)
	return o
}

// SetStart adds the start to the get branch pipeline run node log params
func (o *GetBranchPipelineRunNodeLogParams) SetStart(start *int64) {
	o.Start = start
}

// WriteToRequest writes these params to a swagger request
func (o *GetBranchPipelineRunNodeLogParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param branchName
	if err := r.SetPathParam("branchName", o.BranchName); err != nil {
		return err
	}

	// path param name
	if err := r.SetPathParam("name", o.Name); err != nil {
		return err
	}

	// path param namespace
	if err := r.SetPathParam("namespace", o.Namespace); err != nil {
		return err
	}

	// path param node
	if err := r.SetPathParam("node", o.Node); err != nil {
		return err
	}

	// path param organization
	if err := r.SetPathParam("organization", o.Organization); err != nil {
		return err
	}

	// path param run
	if err := r.SetPathParam("run", o.Run); err != nil {
		return err
	}

	if o.Start != nil {

		// query param start
		var qrStart int64
		if o.Start != nil {
			qrStart = *o.Start
		}
		qStart := swag.FormatInt64(qrStart)
		if qStart != "" {
			if err := r.SetQueryParam("start", qStart); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
