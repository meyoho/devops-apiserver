// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetBranchPipelineRunNodeStepParams creates a new GetBranchPipelineRunNodeStepParams object
// with the default values initialized.
func NewGetBranchPipelineRunNodeStepParams() *GetBranchPipelineRunNodeStepParams {
	var ()
	return &GetBranchPipelineRunNodeStepParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetBranchPipelineRunNodeStepParamsWithTimeout creates a new GetBranchPipelineRunNodeStepParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetBranchPipelineRunNodeStepParamsWithTimeout(timeout time.Duration) *GetBranchPipelineRunNodeStepParams {
	var ()
	return &GetBranchPipelineRunNodeStepParams{

		timeout: timeout,
	}
}

// NewGetBranchPipelineRunNodeStepParamsWithContext creates a new GetBranchPipelineRunNodeStepParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetBranchPipelineRunNodeStepParamsWithContext(ctx context.Context) *GetBranchPipelineRunNodeStepParams {
	var ()
	return &GetBranchPipelineRunNodeStepParams{

		Context: ctx,
	}
}

// NewGetBranchPipelineRunNodeStepParamsWithHTTPClient creates a new GetBranchPipelineRunNodeStepParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetBranchPipelineRunNodeStepParamsWithHTTPClient(client *http.Client) *GetBranchPipelineRunNodeStepParams {
	var ()
	return &GetBranchPipelineRunNodeStepParams{
		HTTPClient: client,
	}
}

/*GetBranchPipelineRunNodeStepParams contains all the parameters to send to the API endpoint
for the get branch pipeline run node step operation typically these are written to a http.Request
*/
type GetBranchPipelineRunNodeStepParams struct {

	/*BranchName
	  Name of the branch

	*/
	BranchName string
	/*Name
	  name of pipeline or pipelineconfig

	*/
	Name string
	/*Namespace
	  namespace of pipeline or pipelineconfig

	*/
	Namespace string
	/*Node
	  Name of the node

	*/
	Node string
	/*Organization
	  Name of the organization

	*/
	Organization string
	/*Run
	  Name of the run

	*/
	Run string
	/*Step
	  Name of the step

	*/
	Step string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) WithTimeout(timeout time.Duration) *GetBranchPipelineRunNodeStepParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) WithContext(ctx context.Context) *GetBranchPipelineRunNodeStepParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) WithHTTPClient(client *http.Client) *GetBranchPipelineRunNodeStepParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBranchName adds the branchName to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) WithBranchName(branchName string) *GetBranchPipelineRunNodeStepParams {
	o.SetBranchName(branchName)
	return o
}

// SetBranchName adds the branchName to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) SetBranchName(branchName string) {
	o.BranchName = branchName
}

// WithName adds the name to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) WithName(name string) *GetBranchPipelineRunNodeStepParams {
	o.SetName(name)
	return o
}

// SetName adds the name to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) SetName(name string) {
	o.Name = name
}

// WithNamespace adds the namespace to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) WithNamespace(namespace string) *GetBranchPipelineRunNodeStepParams {
	o.SetNamespace(namespace)
	return o
}

// SetNamespace adds the namespace to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) SetNamespace(namespace string) {
	o.Namespace = namespace
}

// WithNode adds the node to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) WithNode(node string) *GetBranchPipelineRunNodeStepParams {
	o.SetNode(node)
	return o
}

// SetNode adds the node to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) SetNode(node string) {
	o.Node = node
}

// WithOrganization adds the organization to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) WithOrganization(organization string) *GetBranchPipelineRunNodeStepParams {
	o.SetOrganization(organization)
	return o
}

// SetOrganization adds the organization to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) SetOrganization(organization string) {
	o.Organization = organization
}

// WithRun adds the run to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) WithRun(run string) *GetBranchPipelineRunNodeStepParams {
	o.SetRun(run)
	return o
}

// SetRun adds the run to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) SetRun(run string) {
	o.Run = run
}

// WithStep adds the step to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) WithStep(step string) *GetBranchPipelineRunNodeStepParams {
	o.SetStep(step)
	return o
}

// SetStep adds the step to the get branch pipeline run node step params
func (o *GetBranchPipelineRunNodeStepParams) SetStep(step string) {
	o.Step = step
}

// WriteToRequest writes these params to a swagger request
func (o *GetBranchPipelineRunNodeStepParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param branchName
	if err := r.SetPathParam("branchName", o.BranchName); err != nil {
		return err
	}

	// path param name
	if err := r.SetPathParam("name", o.Name); err != nil {
		return err
	}

	// path param namespace
	if err := r.SetPathParam("namespace", o.Namespace); err != nil {
		return err
	}

	// path param node
	if err := r.SetPathParam("node", o.Node); err != nil {
		return err
	}

	// path param organization
	if err := r.SetPathParam("organization", o.Organization); err != nil {
		return err
	}

	// path param run
	if err := r.SetPathParam("run", o.Run); err != nil {
		return err
	}

	// path param step
	if err := r.SetPathParam("step", o.Step); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
