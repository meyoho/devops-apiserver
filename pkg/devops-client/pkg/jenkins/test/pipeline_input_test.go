package test

import (
	"context"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	jenkinsv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/jenkins/v1"
	gock "gopkg.in/h2non/gock.v1"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Unit test for pipeline input from Jenkins", func() {
	var (
		host    = "https://localhost"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options
		options v1.PipelineInputOptions

		httpClient = clientv1.NewDefaultClient()
	)

	BeforeEach(func() {
		factory = jenkinsv1.NewFactory()
		opts = clientv1.NewOptions(clientv1.NewBasicAuth("admin", "admin"), clientv1.NewClient(httpClient))
		client = factory(opts)
		gock.InterceptClient(httpClient)

		options = v1.PipelineInputOptions{}
	})

	AfterEach(func() {
		gock.Off()
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("Submit pipeline input", func() {
		const crumb = `{"crumb":"fb171d526b9cc9e25afe80b356e12cb7","crumbRequestField":".crumb"}`

		Context("Normal pipeline", func() {
			BeforeEach(func() {
				gock.New(host).
					Post("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/runs/4/nodes/4/steps/4/").
					Reply(200)
				gock.New(host).
					Get("crumbIssuer/api/json").
					Reply(200).
					JSON(crumb)
			})

			It("Get test report summary from a normal pipeline job", func() {
				err := client.SubmitInputStep(context.Background(), "devops", "test", "jenkins", "4", "4", "4", options)
				Expect(err).To(BeNil())
			})
		})

		Context("Multi-branch pipeline", func() {
			BeforeEach(func() {
				gock.New(host).
					Post("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/branches/master/runs/4/nodes/4/steps/4/").
					Reply(200)
				gock.New(host).
					Get("crumbIssuer/api/json").
					Reply(200).
					JSON(crumb)
			})

			It("Get test report summary from a normal pipeline job", func() {
				err := client.SubmitBranchInputStep(context.Background(), "devops", "test", "master", "jenkins", "4", "4", "4", options)
				Expect(err).To(BeNil())
			})
		})
	})
})
