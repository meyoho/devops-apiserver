package test

import (
	"context"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	jenkinsv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/jenkins/v1"
	gock "gopkg.in/h2non/gock.v1"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("DevOps Tool Jenkins tests", func() {
	var (
		host    = "https://localhost"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options

		transport  = gock.NewTransport()
		httpClient = clientv1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = jenkinsv1.NewFactory()
		opts = clientv1.NewOptions(clientv1.NewBasicAuth("admin", "admin"), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetProgressiveLog", func() {
		const progressiveLog = `
			Branch event
			Obtained Jenkinsfile from 261716a5a067466533590c12419ef9dc230582cd
			Running in Durability level: MAX_SURVIVABILITY
			Loading library alauda-cicd@master
			Attempting to resolve master from remote references...
			 > git --version # timeout=10
			using GIT_ASKPASS to set credentials 
			 > git ls-remote -h https://daniel_morinigo@alauda.io/devops-apiserver/pkg/jenkins-shared-library.git # timeout=10
			Found match: refs/heads/master revision c659f8cb927bf03177faaaf69aca3cf5f979a3e4
			using credential bitbucket
 			> git rev-parse --is-inside-work-tree # timeout=10
			Fetching changes from the remote Git repository
			 > git config remote.origin.url https://daniel_morinigo@alauda.io/devops-apiserver/pkg/jenkins-shared-library.git # timeout=10
			Fetching without tags
			Fetching upstream changes from https://daniel_morinigo@alauda.io/devops-apiserver/pkg/jenkins-shared-library.git
 			> git --version # timeout=10
			using GIT_ASKPASS to set credentials 
			 > git fetch --no-tags --progress https://daniel_morinigo@alauda.io/devops-apiserver/pkg/jenkins-shared-library.git +refs/heads/*:refs/remotes/origin/*
			Checking out Revision c659f8cb927bf03177faaaf69aca3cf5f979a3e4 (master)
			 > git config core.sparsecheckout # timeout=10
			 > git checkout -f c659f8cb927bf03177faaaf69aca3cf5f979a3e4
			Commit message: "Merged in fix/add-retry (pull request #30)"
			 > git rev-list --no-walk 2af8bcb641497d46f24fcd1011241ff9dc9d7a51 # timeout=10
			[Bitbucket] Notifying commit build result`

		BeforeEach(func() {
			gock.New(host).
				Get("/job/devops/job/devops-test/2/logText/progressiveText").
				MatchParam("start", "0").
				Reply(200).
				SetHeaders(map[string]string{
					"X-More-Data":  "true",
					"X-Text-Size":  "200",
					"Content-Type": "text/plain",
				}).BodyString(progressiveLog)
			gock.New(host).
				Get("/job/devops/job/devops-test/job/master/2/logText/progressiveText").
				MatchParam("start", "0").
				Reply(200).
				SetHeaders(map[string]string{
					"X-More-Data":  "true",
					"X-Text-Size":  "200",
					"Content-Type": "text/plain",
				}).BodyString(progressiveLog)
		})

		It("get job progressive log", func() {
			log, err := client.GetJobProgressiveLog(context.Background(), "devops", "test", 2, 0)
			Expect(err).To(BeNil())
			Expect(log.Text).To(Equal(progressiveLog))
			Expect(log.HasMore).To(Equal(true))
			Expect(*log.NextStart).To(Equal(int64(200)))
		})

		It("get branch job progressive log", func() {
			log, err := client.GetBranchJobProgressiveLog(context.Background(), "devops", "test", "master", 2, 0)
			Expect(err).To(BeNil())
			Expect(log.Text).To(Equal(progressiveLog))
			Expect(log.HasMore).To(Equal(true))
			Expect(*log.NextStart).To(Equal(int64(200)))
		})
	})
	Context("GetStepLog", func() {
		const stepLog = "library 'alauda-cicd-debug@feat/junit-support'"

		BeforeEach(func() {
			gock.New(host).
				Get("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/runs/1/nodes/1/steps/11/log/").
				MatchParam("start", "0").
				Reply(200).
				SetHeaders(map[string]string{
					"Content-Type": "text/plain",
				}).
				BodyString(stepLog)

			gock.New(host).
				Get("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/branches/master/runs/1/nodes/1/steps/11/log/").
				MatchParam("start", "0").
				Reply(200).
				SetHeaders(map[string]string{
					"Content-Type": "text/plain",
				}).
				BodyString(stepLog)
		})

		It("get job step log", func() {
			log, err := client.GetJobStepLog(context.Background(), "devops", "test", "jenkins", "1", "1", "11", 0)
			Expect(err).To(BeNil())
			Expect(log.Text).To(Equal(stepLog))
			Expect(log.HasMore).To(Equal(false))
			Expect(*log.NextStart).To(Equal(int64(0)))
		})

		It("get branch job step log", func() {
			log, err := client.GetBranchJobStepLog(context.Background(), "devops", "test", "master", "jenkins", "1", "1", "11", 0)
			Expect(err).To(BeNil())
			Expect(log.Text).To(Equal(stepLog))
			Expect(log.HasMore).To(Equal(false))
			Expect(*log.NextStart).To(Equal(int64(0)))
		})
	})

	Context("GetNodeLog", func() {
		const nodeLog = `+ mkdir -p dist
+ cp src/backend/dist/backend dist
+ cp src/backend/images/Dockerfile dist
+ cp src/backend/images/frontendDockerfile dist
`
		BeforeEach(func() {
			gock.New(host).
				Get("blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/runs/1/nodes/1/log/").
				MatchParam("start", "0").
				Reply(200).
				SetHeaders(map[string]string{
					"Content-Type": "text/plain",
				}).
				BodyString(nodeLog)

			gock.New(host).
				Get("blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/branches/master/runs/1/nodes/1/log/").
				MatchParam("start", "0").
				Reply(200).
				SetHeaders(map[string]string{
					"Content-Type": "text/plain",
				}).
				BodyString(nodeLog)
		})

		It("get job node log", func() {
			log, err := client.GetJobNodeLog(context.Background(), "devops", "test", "jenkins", "1", "1", 0)
			Expect(err).To(BeNil())
			Expect(log.Text).To(Equal(nodeLog))
			Expect(log.HasMore).To(Equal(false))
			Expect(*log.NextStart).To(Equal(int64(0)))
		})

		It("get branch job node log", func() {
			log, err := client.GetBranchJobNodeLog(context.Background(), "devops", "test", "master", "jenkins", "1", "1", 0)
			Expect(err).To(BeNil())
			Expect(log.Text).To(Equal(nodeLog))
			Expect(log.HasMore).To(Equal(false))
			Expect(*log.NextStart).To(Equal(int64(0)))
		})
	})

	Context("GetScanLog", func() {
		const scanLog = "scanning"

		BeforeEach(func() {
			gock.New(host).
				Get("job/devops/job/devops-test/indexing/logText/progressiveText").
				MatchParam("start", "0").
				Reply(200).
				SetHeaders(map[string]string{
					"X-More-Data":  "true",
					"X-Text-Size":  "200",
					"Content-Type": "text/plain",
				}).BodyString(scanLog)
		})

		It("get branch job progressive scan log", func() {
			log, err := client.GetBranchProgressiveScanLog(context.Background(), "devops", "test", 0)
			Expect(err).To(BeNil())
			Expect(log.Text).To(Equal(scanLog))
			Expect(log.HasMore).To(Equal(true))
			Expect(*log.NextStart).To(Equal(int64(200)))
		})
	})

	Context("ScanBranchJob", func() {
		const crumb = `{"crumb":"fb171d526b9cc9e25afe80b356e12cb7","crumbRequestField":".crumb"}`

		BeforeEach(func() {
			gock.New(host).
				Post("job/devops/job/devops-test/build").
				Reply(200)
			gock.New(host).
				Get("crumbIssuer/api/json").
				Reply(200).
				JSON(crumb)
		})

		It("trigger branch job scan", func() {
			err := client.ScanMultiBranchJob(context.Background(), "devops", "test")
			Expect(err).To(BeNil())
		})
	})

	Context("GetNodes", func() {
		const nodes = `[ {
  "displayName" : "build",
  "durationInMillis" : 219,
  "edges" : [ {
    "id" : "9"
  } ],
  "id" : "3",
  "result" : "SUCCESS",
  "startTime" : "2016-05-06T15:15:08.719-0700",
  "state" : "FINISHED"
}, {
  "displayName" : "test",
  "durationInMillis" : 158,
  "edges" : [ {
    "id" : "13"
  }, {
    "id" : "14"
  }, {
    "id" : "15"
  } ],
  "id" : "9",
  "result" : "SUCCESS",
  "startTime" : "2016-05-06T15:15:08.938-0700",
  "state" : "FINISHED"
}, {
  "displayName" : "unit",
  "durationInMillis" : 127,
  "edges" : [ {
    "id" : "35"
  } ],
  "id" : "13",
  "result" : "SUCCESS",
  "startTime" : "2016-05-06T15:15:08.942-0700",
  "state" : "FINISHED"
}, {
  "displayName" : "integration",
  "durationInMillis" : 126,
  "edges" : [ {
    "id" : "35"
  } ],
  "id" : "14",
  "result" : "SUCCESS",
  "startTime" : "2016-05-06T15:15:08.944-0700",
  "state" : "FINISHED"
}, {
  "displayName" : "ui",
  "durationInMillis" : 137,
  "edges" : [ {
    "id" : "35"
  } ],
  "id" : "15",
  "result" : "SUCCESS",
  "startTime" : "2016-05-06T15:15:08.945-0700",
  "state" : "FINISHED"
}, {
  "displayName" : "deploy",
  "durationInMillis" : 47,
  "edges" : [ ],
  "id" : "35",
  "result" : "SUCCESS",
  "startTime" : "2016-05-06T15:15:09.096-0700",
  "state" : "FINISHED"
} ]`

		BeforeEach(func() {
			gock.New(host).
				Get("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/runs/1/nodes/").
				Reply(200).
				JSON(nodes)
			gock.New(host).
				Get("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/branches/master/runs/1/nodes/").
				Reply(200).
				JSON(nodes)
		})

		It("get job run nodes", func() {
			nodes, err := client.GetJobNodes(context.Background(), "devops", "test", "jenkins", "1")
			Expect(err).To(BeNil())
			Expect(len(nodes)).To(Equal(6))
			Expect(nodes[0].DisplayName).To(Equal("build"))
		})

		It("get branch job run nodes", func() {
			nodes, err := client.GetBranchJobNodes(context.Background(), "devops", "test", "master", "jenkins", "1")
			Expect(err).To(BeNil())
			Expect(len(nodes)).To(Equal(6))
			Expect(nodes[0].DisplayName).To(Equal("build"))
		})
	})

	Context("GetSteps", func() {
		const steps = `[ {
  "_class" : "io.jenkins.blueocean.rest.model.GenericResource",
  "displayName" : "Shell Script",
  "durationInMillis" : 70,
  "id" : "5",
  "result" : "SUCCESS",
  "startTime" : "2016-06-18T13:28:29.443+0900"
}, {
  "_class" : "io.jenkins.blueocean.rest.model.GenericResource",
  "displayName" : "Print Message",
  "durationInMillis" : 1,
  "id" : "10",
  "result" : "SUCCESS",
  "startTime" : "2016-06-18T13:28:29.545+0900"
}, {
  "_class" : "io.jenkins.blueocean.rest.model.GenericResource",
  "displayName" : "Shell Script",
  "durationInMillis" : 265,
  "id" : "11",
  "input": {
	"id": "D35b5c3d85d6080a01eb1e0984ed523c",
	"message": "Please input your name!",
	"ok": "Confirm",
	"parameters": [{
		"defaultParameterValue": {
			"name": "name",
			"value": "rick"
		},
		"description": "This should not be your real name.",
		"name": "name",
		"type": "StringParameterDefinition"
	}]
  },
  "result" : "SUCCESS",
  "startTime" : "2016-06-18T13:28:29.546+0900"
}, {
  "_class" : "io.jenkins.blueocean.rest.model.GenericResource",
  "displayName" : "Shell Script",
  "durationInMillis" : 279,
  "id" : "12",
  "result" : "SUCCESS",
  "startTime" : "2016-06-18T13:28:29.811+0900"
} ]`

		BeforeEach(func() {
			gock.New(host).
				Get("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/runs/1/nodes/1/steps/").
				Reply(200).
				JSON(steps)
			gock.New(host).
				Get("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/branches/master/runs/1/nodes/1/steps/").
				Reply(200).
				JSON(steps)
		})

		It("get job run nodes", func() {
			steps, err := client.GetJobSteps(context.Background(), "devops", "test", "jenkins", "1", "1")
			Expect(err).To(BeNil())
			Expect(len(steps)).To(Equal(4))
			Expect(steps[0].DisplayName).To(Equal("Shell Script"))
			Expect(len(steps[2].Input.Parameters)).To(Equal(1))
			Expect(steps[2].Input.Parameters[0].DefaultParameterValue.Value).To(Equal("rick"))
		})

		It("get branch job run nodes", func() {
			steps, err := client.GetBranchJobSteps(context.Background(), "devops", "test", "master", "jenkins", "1", "1")
			Expect(err).To(BeNil())
			Expect(len(steps)).To(Equal(4))
			Expect(steps[0].DisplayName).To(Equal("Shell Script"))
		})
	})

	Context("Disable or enable a Jenkins job", func() {
		const crumb = `{"crumb":"fb171d526b9cc9e25afe80b356e12cb7","crumbRequestField":".crumb"}`

		BeforeEach(func() {
			gock.New(host).
				Post("job/devops/job/devops-test/disable").
				Reply(200)
			gock.New(host).
				Post("job/devops/job/devops-test/enable").
				Reply(200)
			gock.New(host).
				Get("crumbIssuer/api/json").
				Reply(200).
				JSON(crumb)
			gock.New(host).
				Get("crumbIssuer/api/json").
				Reply(200).
				JSON(crumb)
		})

		It("disable or a jenkins job", func() {
			err := client.DisableJob(context.Background(), "devops", "test")
			Expect(err).NotTo(HaveOccurred())

			err = client.EnableJob(context.Background(), "devops", "test")
			Expect(err).NotTo(HaveOccurred())
		})
	})
})
