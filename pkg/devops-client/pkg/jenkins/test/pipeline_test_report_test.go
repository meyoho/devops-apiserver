package test

import (
	"context"

	"alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	jenkinsv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/jenkins/v1"
	gock "gopkg.in/h2non/gock.v1"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Unit test for the test report from Jenkins", func() {
	var (
		host    = "https://localhost"
		factory v1.ClientFactory
		client  v1.Interface
		opts    *v1.Options

		httpClient = v1.NewDefaultClient()
	)

	BeforeEach(func() {
		factory = jenkinsv1.NewFactory()
		opts = v1.NewOptions(v1.NewBasicAuth("admin", "admin"), v1.NewClient(httpClient))
		client = factory(opts)
		gock.InterceptClient(httpClient)
	})

	AfterEach(func() {
		gock.Off()
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("Get test report summary", func() {
		summary := getSummary()

		Context("Normal pipeline", func() {
			BeforeEach(func() {
				gock.New(host).
					Get("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/runs/4/blueTestSummary/").
					Reply(200).
					BodyString(summary)
			})

			It("Get test report summary from a normal pipeline job", func() {
				summary, err := client.GetTestReportSummary(context.Background(), "devops", "test", "jenkins", "4")
				Expect(err).To(BeNil())
				Expect(summary).NotTo(BeNil())
				Expect(summary.Passed).To(Equal(int64(2)))
				Expect(summary.Failed).To(Equal(int64(0)))
			})
		})

		Context("Multi-branch pipeline", func() {
			BeforeEach(func() {
				gock.New(host).
					Get("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/branches/master/runs/4/blueTestSummary/").
					Reply(200).
					BodyString(summary)
			})

			It("Get test report summary from a normal pipeline job", func() {
				summary, err := client.GetBranchTestReportSummary(context.Background(), "devops", "test", "master", "jenkins", "4")
				Expect(err).To(BeNil())
				Expect(summary).NotTo(BeNil())
				Expect(summary.Passed).To(Equal(int64(2)))
				Expect(summary.Failed).To(Equal(int64(0)))
			})
		})
	})

	Context("Get test report", func() {
		testReport := getTestReport()

		Context("Normal pipeline", func() {
			BeforeEach(func() {
				gock.New(host).
					Get("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/runs/4/tests/").
					Reply(200).
					BodyString(testReport)
			})

			It("Get test report from a normal pipeline job", func() {
				report, err := client.GetTestReports(context.Background(), "devops", "test", "jenkins", "4", "", "", "", int64(0), int64(100))
				Expect(err).To(BeNil())
				Expect(report).NotTo(BeNil())
				Expect(len(report.Items)).To(Equal(1))
			})
		})

		Context("Multi-branch pipeline", func() {
			BeforeEach(func() {
				gock.New(host).
					Get("/blue/rest/organizations/jenkins/pipelines/devops/pipelines/devops-test/branches/master/runs/4/tests/").
					Reply(200).
					BodyString(testReport)
			})

			It("Get test report from a normal pipeline job", func() {
				report, err := client.GetBranchTestReports(context.Background(), "devops", "test", "master", "jenkins", "4", "", "", "", int64(0), int64(100))
				Expect(err).To(BeNil())
				Expect(report).NotTo(BeNil())
				Expect(len(report.Items)).To(Equal(1))
			})
		})
	})
})

func getSummary() string {
	return `
	{
		"existingFailed": 0,
		"failed": 0,
		"fixed": 0,
		"passed": 2,
		"regressions": 0,
		"skipped": 0,
		"total": 2
		}
	`
}

func getTestReport() string {
	return `[{
"Age": 1,
"Duration": 0.01,
"ErrorDetails": "",
"ErrorStackTrace": "",
"HasStdLog": false,
"ID": "fakeID",
"Name": "fakeName",
"State": "",
"Status": "FAILED"
}]`
}
