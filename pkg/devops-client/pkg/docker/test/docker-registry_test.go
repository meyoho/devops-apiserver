package test

import (
	"context"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	dockerRegistryV2 "alauda.io/devops-apiserver/pkg/devops-client/pkg/docker/v2"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool Docker Registry tests", func() {
	var (
		// host
		host    = "https://docker-registry.test"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options

		transport  = gock.NewTransport()
		httpClient = clientv1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = dockerRegistryV2.NewClient()
		opts = clientv1.NewOptions(clientv1.NewBasicConfig("docker-registry.test", "", []string{}), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetImageRepos", func() {
		const (
			repos = `{
						"repositories": [
							"alaudak8s/devops-apiserver",
							"cyzhang/alpine",
							"iota/dac",
							"iota-test/dac",
							"kube-sample-apiserver",
							"quay/busybox"
						]
					}`
		)

		BeforeEach(func() {
			// register catalog api
			gock.New(host).
				Get("/v2/_catalog").
				Reply(200).
				JSON(repos)
		})

		It("get image repos", func() {
			repos, err := client.GetImageRepos(context.Background())
			Expect(err).To(BeNil())
			Expect(len(repos.Items)).To(Equal(6))
			Expect(repos.Items[0]).To(Equal("alaudak8s/devops-apiserver"))
		})
	})

	Context("GetImageTags", func() {
		const (
			tags = `
						{
							"name": "quay/busybox",
							"tags": [
								"latest"
							]
						}
					`

			manifest = `
							{
							   "schemaVersion": 1,
							   "name": "quay/busybox",
							   "tag": "latest",
							   "architecture": "x86_64",
							   "fsLayers": [
								  {
									 "blobSum": "sha256:184dc3db39b5e19dc39547f43db46ea48cd6cc779e806a3c8a5e5396acd20206"
								  },
								  {
									 "blobSum": "sha256:db80bcab0e8b69656505332fcdff3ef2b9f664a2029d1b2f97224cffcf689afc"
								  },
								  {
									 "blobSum": "sha256:184dc3db39b5e19dc39547f43db46ea48cd6cc779e806a3c8a5e5396acd20206"
								  },
								  {
									 "blobSum": "sha256:25a045fc0afb00076234e8cd19f8f6584856222dc07337362d9bc464966d1239"
								  }
							   ],
							   "history": [
								  {
									 "v1Compatibility": "{\"architecture\":\"x86_64\",\"author\":\"Jérôme Petazzoni \\u003cjerome@docker.com\\u003e\",\"config\":{\"Hostname\":\"28c54617d310\",\"Domainname\":\"\",\"User\":\"\",\"AttachStdin\":false,\"AttachStdout\":false,\"AttachStderr\":false,\"Tty\":false,\"OpenStdin\":false,\"StdinOnce\":false,\"Env\":[\"HOME=/\",\"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\"],\"Cmd\":[\"/bin/sh\",\"-c\",\"/bin/sh\"],\"Image\":\"48e5f45168b97799ad0aafb7e2fef9fac57b5f16f6db7f67ba2000eb947637eb\",\"Volumes\":null,\"WorkingDir\":\"\",\"Entrypoint\":null,\"OnBuild\":null,\"Labels\":null},\"container\":\"a2af951c57be9154cf31a507c619c517757ec24dcbdd6fc41002537256ff6230\",\"container_config\":{\"Hostname\":\"28c54617d310\",\"Domainname\":\"\",\"User\":\"\",\"AttachStdin\":false,\"AttachStdout\":false,\"AttachStderr\":false,\"Tty\":false,\"OpenStdin\":false,\"StdinOnce\":false,\"Env\":[\"HOME=/\",\"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\"],\"Cmd\":[\"/bin/sh\",\"-c\",\"#(nop) CMD [/bin/sh -c /bin/sh]\"],\"Image\":\"48e5f45168b97799ad0aafb7e2fef9fac57b5f16f6db7f67ba2000eb947637eb\",\"Volumes\":null,\"WorkingDir\":\"\",\"Entrypoint\":null,\"OnBuild\":null,\"Labels\":null},\"created\":\"2014-02-03T15:58:08.872585903Z\",\"docker_version\":\"0.6.3\",\"id\":\"c4b16005d0efc22d9a4d38c621db9725e9cc8c34c7c050387aa20bb16c5f81f4\",\"parent\":\"f3853e7a99bdcd73e54ac1475a6afe87cb1c5393c0883e6bc47d977d9b129e03\"}"
								  },
								  {
									 "v1Compatibility": "{\"id\":\"f3853e7a99bdcd73e54ac1475a6afe87cb1c5393c0883e6bc47d977d9b129e03\",\"parent\":\"302cd6d749a26bcda14a7b90305ad4ba3676107d42dfdc81421553cb350d5776\",\"created\":\"2014-02-03T15:58:08.72383042Z\",\"container_config\":{\"Cmd\":[\"/bin/sh -c #(nop) ADD rootfs.tar in /\"]},\"author\":\"Jérôme Petazzoni \\u003cjerome@docker.com\\u003e\"}"
								  },
								  {
									 "v1Compatibility": "{\"id\":\"302cd6d749a26bcda14a7b90305ad4ba3676107d42dfdc81421553cb350d5776\",\"parent\":\"f987f9af12b8c6ffa61c2eef6d8898f3be1e43dbd49420a8263caa0077883611\",\"created\":\"2014-02-03T15:58:08.52236968Z\",\"container_config\":{\"Cmd\":[\"/bin/sh -c #(nop) MAINTAINER Jérôme Petazzoni \\u003cjerome@docker.com\\u003e\"]},\"author\":\"Jérôme Petazzoni \\u003cjerome@docker.com\\u003e\"}"
								  },
								  {
									 "v1Compatibility": "{\"id\":\"f987f9af12b8c6ffa61c2eef6d8898f3be1e43dbd49420a8263caa0077883611\",\"comment\":\"Imported from -\",\"created\":\"2013-06-13T14:03:50.821769-07:00\",\"container_config\":{\"Cmd\":[\"\"]}}"
								  }
							   ],
							   "signatures": [
								  {
									 "header": {
										"jwk": {
										   "crv": "P-256",
										   "kid": "GFIV:KTIB:2CIE:QFSQ:DOGI:2D3N:LEQM:S2L7:NCHW:IZNG:MZWV:QXOZ",
										   "kty": "EC",
										   "x": "lIqP5fMNFiYH73jT5pNqFL6mpF_gbZNwsNULzGYbvZU",
										   "y": "ncTLwUdhhxj-KCtVi_RlyWtDSzz6VH6nLPSWIuyHg8g"
										},
										"alg": "ES256"
									 },
									 "signature": "HXOBmFzv579UWM4rnBVq0zqJb9e-bnEUyhtlbsDPsVyS3rm4wIlg_GchGOa0DxO0EpMV7UyaGDrIVIcMt215Ig",
									 "protected": "eyJmb3JtYXRMZW5ndGgiOjMxNDgsImZvcm1hdFRhaWwiOiJDbjAiLCJ0aW1lIjoiMjAxOS0xMi0yM1QwNjo0Mjo0MloifQ"
								  }
							   ]
							}
						`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/v2/quay/busybox/tags/list").
				Reply(200).
				JSON(tags)

			gock.New(host).
				Get("/v2/quay/busybox/manifests/latest").
				Reply(200).
				JSON(manifest)
		})

		It("get repo tags", func() {
			tags, err := client.GetImageTags(context.Background(), "quay/busybox")
			Expect(err).To(BeNil())
			Expect(len(tags)).To(Equal(1))
			Expect(tags[0].Name).To(Equal("latest"))
		})
	})
})
