package test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestTests(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("docker-registry_test.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "docker-registry", []Reporter{junitReporter})
}
