// Code generated
package v2

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"alauda.io/devops-apiserver/pkg/devops-client/pkg/generic"

	"github.com/go-logr/logr"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"
	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/docker/v2/client"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/docker/v2/client/operations"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/docker/v2/models"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/transport"

	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	clientv1.NotImplement
	logger     logr.Logger
	client     *client.Docker
	opts       *clientv1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ clientv1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() clientv1.ClientFactory {
	return func(opts *clientv1.Options) clientv1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

// refershToken refreshes token
func (c *Client) refreshToken(ctx context.Context) error {
	params := operations.
		NewGetCatalogParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	_, err := c.client.Operations.GetCatalog(params, c.authInfo)
	if err != nil {
		if resp, ok := err.(*operations.GetCatalogUnauthorized); ok {
			authOpts, err := parseAuthenticateString(resp.WwwAuthenticate)
			if err != nil {
				return err
			}

			req, err := http.NewRequest(http.MethodGet, authOpts.Realm, nil)
			if err != nil {
				return err
			}
			query := req.URL.Query()
			query.Add("scope", authOpts.Scope)
			query.Add("service", authOpts.Service)
			req.URL.RawQuery = query.Encode()
			if c.opts.BasicAuth != nil {
				req.SetBasicAuth(c.opts.BasicAuth.Username, c.opts.BasicAuth.Password)
			}
			res, err := c.httpClient.Do(req)
			if err != nil {
				return err
			}
			defer res.Body.Close()
			data, err := ioutil.ReadAll(res.Body)
			if err != nil {
				return err
			}

			var token Token
			if err = json.Unmarshal(data, &token); err != nil {
				return err
			}
			c.authInfo = openapi.BearerToken(token.Token)
		}
	}
	return err
}

type Token struct {
	AccessToken string `json:"access_token"`
	ExpireIn    string `json:"expire_in"`
	IssuedAt    string `json:"issued_at"`
	Token       string `json:"token"`
}

type AuthOpts struct {
	Realm   string
	Scope   string
	Service string
}

func parseAuthenticateString(auth string) (opts AuthOpts, err error) {
	authSplit := strings.SplitN(auth, " ", 2)
	if len(authSplit) != 2 {
		return AuthOpts{}, fmt.Errorf("format of authenticate '%s' is not correct", auth)
	}
	parts := strings.Split(authSplit[1], ",")
	for _, part := range parts {
		valueList := strings.SplitN(part, "=", 2)
		if len(valueList) == 2 {
			value := strings.Trim(valueList[1], "\",")
			switch valueList[0] {
			case "realm":
				opts.Realm = value
			case "scope":
				opts.Scope = value
			case "service":
				opts.Service = value
			}
		}
	}

	if opts.Realm == "" {
		return AuthOpts{}, fmt.Errorf("realm in authenticate '%s' should not be empty", auth)
	}
	return
}

// GetImageRepos implments ImageRegistryService interface
func (c *Client) GetImageRepos(ctx context.Context) (*v1.ImageRegistryBindingRepositories, error) {
	params := operations.
		NewGetCatalogParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	result, err := c.client.Operations.GetCatalog(params, c.authInfo)
	if err != nil {
		if _, ok := err.(*operations.GetCatalogUnauthorized); ok {
			err = c.refreshToken(ctx)
			if err != nil {
				return nil, err
			}

			result, err = c.client.Operations.GetCatalog(params, c.authInfo)
			if err != nil {
				return nil, err
			}
		}
	}

	imageRepos := &v1.ImageRegistryBindingRepositories{}
	for _, repo := range result.Payload.Repositories {
		imageRepos.Items = append(imageRepos.Items, string(repo))
	}

	return imageRepos, nil
}

// GetImageTags implments ImageRegistryService interface
func (c *Client) GetImageTags(ctx context.Context, repository string) ([]v1.ImageTag, error) {
	params := operations.
		NewGetNameTagsListParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithName(repository)
	result, err := c.client.Operations.GetNameTagsList(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	var imageTags []v1.ImageTag
	for _, tag := range result.Payload.Tags {
		tagDetail, err := c.GetImageTagDetail(ctx, repository, string(tag))
		if err != nil {
			c.logger.Error(err, "Get image tag detail error", "image", fmt.Sprintf("%s/%s", repository, tag))
		} else {
			imageTags = append(imageTags, tagDetail)
		}
	}

	return imageTags, nil
}

func (c *Client) GetImageTagDetail(ctx context.Context, repoName, reference string) (v1.ImageTag, error) {
	parmas := operations.
		NewGetNameManifestsReferenceParams().
		WithName(repoName).
		WithReference(reference).
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	result, err := c.client.Operations.GetNameManifestsReference(parmas, c.authInfo)
	if err != nil {
		return v1.ImageTag{}, err
	}

	imageTag := v1.ImageTag{}
	if len(result.Payload.History) > 0 {
		v1Compatibility := result.Payload.History[0].V1Compatibility
		var model models.V1Compatibility
		if err = json.Unmarshal([]byte(v1Compatibility), &model); err != nil {
			return imageTag, err
		}
		time, err := time.Parse(time.RFC3339Nano, model.Created)
		if err != nil {
			return imageTag, err
		}
		imageTag.CreatedAt = &metav1.Time{Time: time}
		imageTag.Name = reference
		imageTag.Digest = strings.TrimPrefix(result.DockerContentDigest, "sha256:")
		return imageTag, nil
	}

	return imageTag, nil
}

func (c *Client) RepositoryAvailable(ctx context.Context, repositoryName string, lastModifyAt time.Time) (*v1.HostPortStatus, error) {
	var (
		start    = time.Now()
		duration time.Duration
	)
	status := v1.HostPortStatus{
		LastAttempt: &metav1.Time{Time: lastModifyAt},
	}
	_, err := c.GetImageTags(ctx, repositoryName)
	if err != nil {
		status.StatusCode = 500
		status.ErrorMessage = err.Error()
	} else {
		status.StatusCode = 200
		status.Response = "available"
	}

	duration = time.Since(start)
	status.Delay = &duration
	return &status, err
}

func (c *Client) GetImageRepoLink(_ context.Context, name string) string {
	return ""
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(c.httpClient, url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(ctx context.Context) (*v1.HostPortStatus, error) {
	_, err := c.GetImageRepos(ctx)
	if err != nil {
		return nil, err
	}

	return &v1.HostPortStatus{
		StatusCode:  200,
		LastAttempt: &metav1.Time{Time: time.Now()},
	}, nil
}
