// Code generated
package v1

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"

	"github.com/go-logr/logr"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/generic"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/harbor/v1/client"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/harbor/v1/client/products"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/harbor/v1/models"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/transport"

	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	clientv1.NotImplement
	logger     logr.Logger
	client     *client.Harbor
	opts       *clientv1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ clientv1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() clientv1.ClientFactory {
	return func(opts *clientv1.Options) clientv1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

func (c *Client) listAllProjects(ctx context.Context) ([]*models.Project, error) {
	var (
		page     = int32(0)
		pageLen  = int32(100)
		projects = []*models.Project{}
	)

	for {
		page = page + 1
		projectsParam := products.
			NewGetProjectsParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithPage(&page).
			WithPageSize(&pageLen)
		projectsOK, err := c.client.Products.GetProjects(projectsParam, c.authInfo)
		if err != nil {
			return nil, err
		}

		projects = append(projects, projectsOK.Payload...)

		if len(projectsOK.Payload) < int(pageLen) {
			break
		}
	}

	return projects, nil
}

func (c *Client) listAllRepos(ctx context.Context, projectID int32) ([]*models.Repository, error) {
	var (
		page    = int32(0)
		pageLen = int32(100)
		repos   = []*models.Repository{}
	)

	for {
		page = page + 1
		reposParams := products.
			NewGetRepositoriesParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithProjectID(projectID).
			WithPage(&page).
			WithPageSize(&pageLen)
		reposOK, err := c.client.Products.GetRepositories(reposParams, c.authInfo)
		if err != nil {
			return nil, err
		}

		repos = append(repos, reposOK.Payload...)

		if len(reposOK.Payload) < int(pageLen) {
			break
		}
	}

	return repos, nil
}

// GetImageRepos implments ImageRegistryService interface
func (c *Client) GetImageRepos(ctx context.Context) (*v1.ImageRegistryBindingRepositories, error) {
	projects, err := c.listAllProjects(ctx)
	if err != nil {
		return nil, err
	}

	result := &v1.ImageRegistryBindingRepositories{}
	for _, project := range projects {
		repos, err := c.listAllRepos(ctx, project.ProjectID)
		if err != nil {
			return nil, err
		}

		for _, repo := range repos {
			result.Items = append(result.Items, repo.Name)
		}
	}

	return result, nil
}

// GetImageTags implments ImageRegistryService interface
func (c *Client) GetImageTags(ctx context.Context, repository string) ([]v1.ImageTag, error) {
	tagParmas := products.
		NewGetRepositoriesRepoNameTagsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithRepoName(repository)
	tags, err := c.client.Products.GetRepositoriesRepoNameTags(tagParmas, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := make([]v1.ImageTag, 0, len(tags.Payload))
	for _, tag := range tags.Payload {
		imageTag := v1.ImageTag{
			Name:   tag.Name,
			Digest: strings.TrimPrefix(tag.Digest, generic.ImageDigestSignature),
			Author: tag.Author,
			Size:   generic.ConvertSizeToString(tag.Size),
		}

		creatdAt, _ := time.Parse(time.RFC3339Nano, tag.Created)
		imageTag.CreatedAt = &metav1.Time{Time: creatdAt}
		imageTag.ScanStatus = v1.ImageTagScanStatusNotScan

		if tag.ScanOverview != nil {
			if tag.ScanOverview.ScanStatus == string(v1.ImageTagScanStatusError) {
				imageTag.ScanStatus = v1.ImageTagScanStatusError
				imageTag.Message = c.GetScanJobLog(tag.ScanOverview.JobID)
			} else {
				imageTag.ScanStatus = v1.ImageTagScanStatus(tag.ScanOverview.ScanStatus)
				imageTag.Level = int(tag.ScanOverview.Severity)
				if tag.ScanOverview.Components != nil {
					for _, s := range tag.ScanOverview.Components.Summary {
						summary := v1.Summary{Severity: int(s.Severity), Count: int(s.Count)}
						imageTag.Summary = append(imageTag.Summary, summary)
					}
				}
			}
		}
		result = append(result, imageTag)
	}

	return result, nil
}

// GetScanJobLog TODO
func (c *Client) GetScanJobLog(jobID int64) string {
	// TDOD
	return ""
}

// TriggerScanImage implments ImageRegistryService interface
func (c *Client) TriggerScanImage(ctx context.Context, repositoryName, tag string) *v1.ImageResult {
	scanParams := products.
		NewPostRepositoriesRepoNameTagsTagScanParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithRepoName(repositoryName).
		WithTag(tag)
	scanOK, err := c.client.Products.PostRepositoriesRepoNameTagsTagScan(scanParams, c.authInfo)

	result := &v1.ImageResult{}
	if err != nil {
		result.StatusCode = http.StatusInternalServerError
		result.Message = err.Error()
		return result
	}

	result.StatusCode = http.StatusOK
	result.Message = scanOK.Error()
	return result
}

// GetVulnerability implments ImageRegistryService interface
func (c *Client) GetVulnerability(ctx context.Context, repositoryName, tag string) (*v1.VulnerabilityList, error) {
	vulParams := products.
		NewGetRepositoriesRepoNameTagsTagVulnerabilityDetailsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithRepoName(repositoryName).
		WithTag(tag)
	vulDetail, err := c.client.Products.GetRepositoriesRepoNameTagsTagVulnerabilityDetails(vulParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := &v1.VulnerabilityList{}
	for _, item := range vulDetail.Payload {
		vulner := v1.Vulnerability{
			ID:          item.ID,
			Severity:    int(item.Severity),
			Package:     item.Package,
			Version:     item.Version,
			Description: item.Description,
		}

		result.Items = append(result.Items, vulner)
	}

	return result, nil
}

// GetImageProjects implments ImageRegistryService interface
func (c *Client) GetImageProjects(ctx context.Context) (*v1.ProjectDataList, error) {
	projects, err := c.listAllProjects(ctx)
	if err != nil {
		return nil, err
	}

	result := &v1.ProjectDataList{}
	for _, project := range projects {
		data, err := generic.MarshalToMapString(project)
		if err != nil {
			return nil, err
		}

		item := v1.ProjectData{
			ObjectMeta: metav1.ObjectMeta{
				Name: project.Name,
				Labels: map[string]string{
					"project_id": strconv.Itoa(int(project.ProjectID)),
				},
			},
			Data: data,
		}
		result.Items = append(result.Items, item)
	}

	return result, nil
}

// CreateImageProject implments ImageRegistryService interface
func (c *Client) CreateImageProject(ctx context.Context, name string) (*v1.ProjectData, error) {
	createParams := products.
		NewPostProjectsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithProject(&models.ProjectReq{ProjectName: name})
	_, err := c.client.Products.PostProjects(createParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := &v1.ProjectData{}
	return result, nil
}

func (c *Client) RepositoryAvailable(ctx context.Context, repositoryName string, lastModifyAt time.Time) (*v1.HostPortStatus, error) {
	var (
		start    = time.Now()
		duration time.Duration
	)
	status := v1.HostPortStatus{
		LastAttempt: &metav1.Time{Time: lastModifyAt},
	}

	params := products.
		NewGetRepositoriesRepoNameTagsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithRepoName(repositoryName)
	_, err := c.client.Products.GetRepositoriesRepoNameTags(params, c.authInfo)
	if err != nil {
		status.StatusCode = 500
		status.Response = err.Error()
	} else {
		status.StatusCode = 200
		status.Response = "available"
	}

	duration = time.Since(start)
	status.Delay = &duration
	return &status, err
}

func (c *Client) GetImageRepoLink(ctx context.Context, name string) string {
	nameList := strings.Split(name, "/")
	if len(nameList) < 2 {
		c.logger.Error(fmt.Errorf("harbor repository name: %s illegal", name), "get image repository link error")
		return ""
	}
	projects, err := c.listAllProjects(ctx)
	if err != nil {
		c.logger.Error(err, "harbor list projects error")
		return ""
	}

	var pid int32
	for _, project := range projects {
		if project.Name == nameList[0] {
			pid = project.ProjectID
			break
		}
	}

	if pid != 0 && c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return fmt.Sprintf("%s/harbor/projects/%d/repositories/%s", url, pid, strings.Replace(name, "/", "%2f", len(nameList)-1))
	}
	return ""
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(c.httpClient, url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(ctx context.Context) (*v1.HostPortStatus, error) {
	params := products.
		NewGetStatisticsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	_, err := c.client.Products.GetStatistics(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return &v1.HostPortStatus{StatusCode: 200}, nil
}
