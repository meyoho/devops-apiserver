package test

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	harborv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/harbor/v1"
	harborv1_8 "alauda.io/devops-apiserver/pkg/devops-client/pkg/harbor/v1_8"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool Harbor tests", func() {
	var (
		// host
		host    = "https://harbor.test"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options

		transport  = gock.NewTransport()
		httpClient = clientv1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = harborv1.NewClient()
		opts = clientv1.NewOptions(clientv1.NewBasicConfig("harbor.test", "", []string{}), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetImageRepos", func() {
		const (
			projects = `[
			{
				"project_id": 3,
				"owner_id": 1,
				"name": "fs-env",
				"creation_time": "2019-01-11T07:01:14Z",
				"update_time": "2019-01-11T07:01:14Z",
				"deleted": false,
				"owner_name": "",
				"togglable": true,
				"current_user_role_id": 1,
				"repo_count": 4,
				"chart_count": 0,
				"metadata": {
					"public": "false"
				}
			}
		]`
			repos = `[
				{
					"id": 27,
					"name": "fs-env/api",
					"project_id": 3,
					"description": "",
					"pull_count": 25,
					"star_count": 0,
					"tags_count": 2,
					"labels": [],
					"creation_time": "2019-01-15T06:46:52.955557Z",
					"update_time": "2019-05-17T14:43:56.556447Z"
				},
				{
					"id": 29,
					"name": "fs-env/aqi-web",
					"project_id": 3,
					"description": "",
					"pull_count": 19,
					"star_count": 0,
					"tags_count": 2,
					"labels": [],
					"creation_time": "2019-01-15T06:47:16.44607Z",
					"update_time": "2019-05-17T15:20:33.209887Z"
				},
				{
					"id": 28,
					"name": "fs-env/web",
					"project_id": 3,
					"description": "",
					"pull_count": 33,
					"star_count": 0,
					"tags_count": 2,
					"labels": [],
					"creation_time": "2019-01-15T06:47:04.937278Z",
					"update_time": "2019-05-17T14:43:12.273434Z"
				},
				{
					"id": 30,
					"name": "fs-env/wq-web",
					"project_id": 3,
					"description": "",
					"pull_count": 17,
					"star_count": 0,
					"tags_count": 2,
					"labels": [],
					"creation_time": "2019-01-15T06:47:27.896822Z",
					"update_time": "2019-05-17T14:43:55.92319Z"
				}
			]`
		)

		BeforeEach(func() {
			// register projects api
			gock.New(host).
				Get("/api/projects").
				Reply(200).
				SetHeader("X-Total-Count", "1").
				JSON(projects)
			// register repos api
			gock.New(host).
				Get("/api/repositories").
				MatchParam("project_id", "3").
				Reply(200).
				SetHeader("X-Total-Count", "4").
				JSON(repos)
		})

		It("list projects", func() {
			res, err := http.Get(fmt.Sprintf("%s/api/projects", host))
			Expect(err).To(BeNil())
			data, err := ioutil.ReadAll(res.Body)
			Expect(err).To(BeNil())
			Expect(string(data)).To(Equal(projects))
		})

		It("list repos", func() {
			res, err := http.Get(fmt.Sprintf("%s/api/repositories?project_id=3", host))
			Expect(err).To(BeNil())
			data, err := ioutil.ReadAll(res.Body)
			Expect(err).To(BeNil())
			Expect(string(data)).To(Equal(repos))
		})

		It("get image repos", func() {
			repos, err := client.GetImageRepos(context.Background())
			Expect(err).To(BeNil())
			Expect(len(repos.Items)).To(Equal(4))
			Expect(repos.Items[0]).To(Equal("fs-env/api"))
		})
	})

	Context("GetImageTags", func() {
		const (
			tags = `[
				{
					"digest": "sha256:c93ee9b83a338f0f8aec2969b1a220d0696e8d85553f892f694157c6e2d4c7ad",
					"name": "0.0.3",
					"size": 30975842,
					"architecture": "amd64",
					"os": "linux",
					"docker_version": "18.06.1-ce",
					"author": "",
					"created": "2019-01-15T07:08:50.821687913Z",
					"config": {
						"labels": null
					},
					"signature": null,
					"labels": []
				},
				{
					"digest": "sha256:cd28dc59dad1f7542c0ab16b4fabc84d5693c75494191a0b07cb804d24729f3b",
					"name": "0.0.4",
					"size": 31158720,
					"architecture": "amd64",
					"os": "linux",
					"docker_version": "18.06.1-ce",
					"author": "",
					"created": "2019-02-14T06:58:30.451911915Z",
					"config": {
						"labels": null
					},
					"signature": null,
					"labels": []
				}
			]`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/api/repositories/fs-env/api/tags").
				Reply(200).
				JSON(tags)
		})

		It("get repo tags", func() {
			tags, err := client.GetImageTags(context.Background(), "fs-env/api")
			Expect(err).To(BeNil())
			Expect(len(tags)).To(Equal(2))
			Expect(tags[0].Name).To(Equal("0.0.3"))
		})
	})

	Context("TriggerScanImage", func() {
		BeforeEach(func() {
			gock.New(host).
				Post("/api/repositories/testproject/busybox/tags/latest/scan").
				Reply(200)
		})

		It("trigger scan image with tag", func() {
			result := client.TriggerScanImage(context.Background(), "testproject/busybox", "latest")
			Expect(result).ToNot(BeNil())
			Expect(result.StatusCode).To(Equal(http.StatusOK))
		})
	})

	Context("GetVulnerability", func() {
		const (
			vulner = `[    
			{
				"id": "CVE-2017-12618",
				"severity": 3,
				"package": "apr-util",
				"version": "1.5.4-1",
				"description": "Apache Portable Runtime Utility (APR-util) 1.6.0 and prior fail to validate the integrity of SDBM database files used by apr_sdbm*() functions, resulting in a possible out of bound read access. A local user with write access to the database can make a program or process using these functions crash, and cause a denial of service.",
				"link": "https://security-tracker.debian.org/tracker/CVE-2017-12618"
			},
			{
				"id": "CVE-2016-9840",
				"severity": 4,
				"package": "zlib",
				"version": "1:1.2.8.dfsg-2",
				"description": "inftrees.c in zlib 1.2.8 might allow context-dependent attackers to have unspecified impact by leveraging improper pointer arithmetic.",
				"link": "https://security-tracker.debian.org/tracker/CVE-2016-9840"
			}]`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/api/repositories/testproject/busybox/tags/latest/vulnerability/details").
				Reply(200).
				JSON(vulner)
		})

		It("get vulnerability", func() {
			vulner, err := client.GetVulnerability(context.Background(), "testproject/busybox", "latest")
			Expect(err).To(BeNil())
			Expect(len(vulner.Items)).To(Equal(2))
			Expect(vulner.Items[0].ID).To(Equal("CVE-2017-12618"))
		})
	})

	Context("GetImageProjects", func() {
		const (
			projects = `[
			{
				"project_id": 3,
				"owner_id": 1,
				"name": "fs-env",
				"creation_time": "2019-01-11T07:01:14Z",
				"update_time": "2019-01-11T07:01:14Z",
				"deleted": false,
				"owner_name": "",
				"togglable": true,
				"current_user_role_id": 1,
				"repo_count": 4,
				"chart_count": 0,
				"metadata": {
					"public": "false"
				}
			}
		]`
		)

		BeforeEach(func() {
			gock.New(host).
				Get("/api/projects").
				Reply(200).
				SetHeader("X-Total-Count", "1").
				JSON(projects)
		})

		It("get image projects", func() {
			data, err := client.GetImageProjects(context.Background())
			Expect(err).To(BeNil())
			Expect(len(data.Items)).To(Equal(1))
			Expect(data.Items[0].Data["Name"]).To(Equal("fs-env"))
		})

	})

	Context("CreateImageProject", func() {
		BeforeEach(func() {
			gock.New(host).
				Post("/api/projects").
				JSON(map[string]string{
					"project_name": "hello",
				}).
				Reply(201)
		})

		It("create a project", func() {
			_, err := client.CreateImageProject(context.Background(), "hello")
			Expect(err).To(BeNil())
		})
	})

	Context("GetHarborConfig", func() {
		BeforeEach(func() {
			factory = harborv1_8.NewClient()
			opts = clientv1.NewOptions(clientv1.NewBasicConfig("harbor.test", "", []string{}), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
			client = factory(opts)

			gock.New(host).
				Get("/api/configurations").
				Reply(200).
				JSON(`{"auth_mode":{"value":"oidc_auth","editable":false},"email_from":{"value":"admin <sample_admin@mydomain.com>","editable":true},"email_host":{"value":"smtp.mydomain.com","editable":true},"email_identity":{"value":"","editable":true},"email_insecure":{"value":false,"editable":true},"email_port":{"value":25,"editable":true},"email_ssl":{"value":false,"editable":true},"email_username":{"value":"","editable":true},"http_authproxy_always_onboard":{"value":false,"editable":true},"http_authproxy_endpoint":{"value":"","editable":true},"http_authproxy_tokenreview_endpoint":{"value":"","editable":true},"http_authproxy_verify_cert":{"value":true,"editable":true},"ldap_base_dn":{"value":"","editable":true},"ldap_filter":{"value":"","editable":true},"ldap_group_admin_dn":{"value":"","editable":true},"ldap_group_attribute_name":{"value":"","editable":true},"ldap_group_base_dn":{"value":"","editable":true},"ldap_group_membership_attribute":{"value":"memberof","editable":true},"ldap_group_search_filter":{"value":"","editable":true},"ldap_group_search_scope":{"value":0,"editable":true},"ldap_scope":{"value":0,"editable":true},"ldap_search_dn":{"value":"","editable":true},"ldap_timeout":{"value":0,"editable":true},"ldap_uid":{"value":"cn","editable":true},"ldap_url":{"value":"","editable":true},"ldap_verify_cert":{"value":false,"editable":true},"oidc_client_id":{"value":"alauda-auth","editable":true},"oidc_endpoint":{"value":"https://acp-hk-build.alauda.cn/dex","editable":true},"oidc_name":{"value":"dex","editable":true},"oidc_scope":{"value":"openid,profile,offline_access,groups,email","editable":true},"oidc_verify_cert":{"value":false,"editable":true},"project_creation_restriction":{"value":"everyone","editable":true},"read_only":{"value":false,"editable":true},"robot_token_duration":{"value":43200,"editable":true},"scan_all_policy":{"value":null,"editable":true},"self_registration":{"value":false,"editable":true},"token_expiration":{"value":30,"editable":true},"uaa_client_id":{"value":"","editable":true},"uaa_client_secret":{"value":"<enc-v1>NK5fRaJKH8FO3YX1Bwd91g==","editable":true},"uaa_endpoint":{"value":"","editable":true},"uaa_verify_cert":{"value":true,"editable":true}}`)
		})

		It("get harbor config", func() {
			cfg, err := client.GetHarborConfig(context.Background())
			Expect(err).To(BeNil())
			Expect(cfg).NotTo(BeNil())
			Expect(cfg.AuthMode).To(Equal("oidc_auth"))
			Expect(cfg.OidcVerifyCert).To(Equal(false))
			Expect(cfg.OidcScope).To(Equal("openid,profile,offline_access,groups,email"))
		})
	})

	Context("UpdateHarborConfig", func() {
		BeforeEach(func() {
			factory = harborv1_8.NewClient()
			opts = clientv1.NewOptions(clientv1.NewBasicConfig("harbor.test", "", []string{}), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
			client = factory(opts)

			gock.New(host).
				Put("/api/configurations").
				Reply(200).
				JSON(`{"auth_mode":"auth_mode","oidc_client_id":"oidc_client_id","oidc_endpoint":"oidc_endpoint","oidc_name":"oidc_name","oidc_scope":"openid,profile,offline_access,groups,email","oidc_verify_cert":false}`)
		})

		It("update harbor config with an invalid json string", func() {
			err := client.UpdateHarborConfig(context.Background(), "")
			Expect(err).To(HaveOccurred())
		})

		It("update harbor config with a valid json string", func() {
			err := client.UpdateHarborConfig(context.Background(), `{"auth_mode":"auth_mode","oidc_client_id":"oidc_client_id","oidc_endpoint":"oidc_endpoint","oidc_name":"oidc_name","oidc_scope":"openid,profile,offline_access,groups,email","oidc_verify_cert":false}`)
			Expect(err).To(BeNil())
		})
	})
})
