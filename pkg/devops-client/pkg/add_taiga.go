package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/taiga/v1"
)

func init() {
	register(devops.ProjectManageTypeTaiga.String(), versionOpt{
		version:   "v1",
		factory:   v1.NewClient(),
		isDefault: true,
	})
}
