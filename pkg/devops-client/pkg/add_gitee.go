package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v5 "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5"
)

func init() {
	register(devops.CodeRepoServiceTypeGitee.String(), versionOpt{
		version:   "v5",
		factory:   v5.NewClient(),
		isDefault: true,
	})
}
