package main

import (
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd"
)

func main() {
	cmd.Execute()
}
