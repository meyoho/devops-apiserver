package nexus

import (
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/common"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/helper"
	"github.com/spf13/cobra"
)

// NewTaskCommand create script command
func NewTaskCommand(commonOpts *common.Options) (cmd *cobra.Command) {
	options := &TaskOptions{
		Options: commonOpts,
	}

	cmd = &cobra.Command{
		Use: "task",
	}

	cmd.PersistentFlags().StringVarP(&options.Name, "name", "n", "",
		"The name of target script.")
	helper.MarkFlagRequired(cmd, "name")

	cmd.AddCommand(NewTaskAddCommand(options))
	return
}

// TaskOptions represents the options for task command
type TaskOptions struct {
	*common.Options

	Name string
	Repo string
	Cron string
}

// GetNexusTaskClient returns the client of nexus task
func (o *TaskOptions) GetNexusTaskClient() (client v1.Interface, err error) {
	client, err = o.GetClient("Nexus", "v3_ext")
	return
}
