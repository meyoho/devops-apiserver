package nexus

import (
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/helper"
	"context"
	"github.com/spf13/cobra"
)

// NewScriptDeleteCommand create script delete command
func NewScriptDeleteCommand(options *ScriptOptions) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "delete",
		Short: "delete script from the target nexus server",
		Run: func(cmd *cobra.Command, args []string) {
			err := options.DeleteScript(cmd, args)
			helper.CheckErr(cmd, err)
		},
	}
	return
}

// DeleteScript delete script from nexus
func (o *ScriptOptions) DeleteScript(cmd *cobra.Command, args []string) (err error) {
	var client v1.Interface
	client, err = o.GetNexusClient()
	if err == nil {
		err = client.DeleteScript(context.Background(), o.Name)
	}
	return
}
