package nexus_test

import (
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd"
	"bytes"
	"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/spf13/cobra"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("command nexus task test", func() {
	var (
		host    = "nexus.test"
		rootCmd *cobra.Command
		buffer  *bytes.Buffer

		transport  = gock.NewTransport()
		httpClient = v1.NewDefaultClient()

		cmdArray []string
		cmdErr   error
	)

	BeforeEach(func() {
		cmd.GetCommonOptions().HTTPClient = httpClient
		cmd.GetCommonOptions().Transport = transport

		rootCmd = cmd.GetRootCmd()
		buffer = new(bytes.Buffer)
		rootCmd.SetOut(buffer)
		rootCmd.SetErr(buffer)

		cmdArray = []string{"nexus", "task",
			"--url", host, "--insecure=true"}
	})

	AfterEach(func() {
		gock.Off()
	})

	JustBeforeEach(func() {
		rootCmd.SetArgs(cmdArray)
		_, cmdErr = rootCmd.ExecuteC()
	})

	Context("test required flags", func() {
		BeforeEach(func() {
			cmdArray = append(cmdArray, []string{
				"add"}...)
		})

		It("lack of name flag", func() {
			Expect(buffer.String()).To(ContainSubstring(`required flag(s) "cron", "repo" not set`))
		})
	})

	Context("normal case, should success", func() {
		var (
			name    string
			repo    string
			cron    string
			payload string
		)

		BeforeEach(func() {
			name = "fake-name"
			repo = "fake-repo"
			cron = "* */10 * * * ?"

			//payload = fmt.Sprintf(`{"action":"coreui_Task","data":[{"cronExpression":"%s","enabled":true,"name":"%s","properties":{"repositoryName":"%s"},"schedule":"advanced","typeId":"repository.maven.publish-dotindex"}],"method":"create","itd":16,"type":"rpc"}`, cron, name, repo)
			payload = fmt.Sprintf(`{"action":"coreui_Task","data":[{"cronExpression":"%s","enabled":true,"name":"%s","properties":{"repositoryName":"%s"},"schedule":"advanced","typeId":"repository.maven.publish-dotindex"}],"method":"create","tid":16,"type":"rpc"}`, cron, name, repo)

			//gock.Observe(gock.DumpRequest)
			gock.New(host).
				Post("/service/extdirect").
				JSON(payload).
				Reply(200)

			cmdArray = append(cmdArray, []string{
				"add", "--name", name, "--repo", repo, "--cron", cron}...)
		})

		It("normal case, should success", func() {
			Expect(cmdErr).To(BeNil())
			Expect(buffer.String()).To(Equal(""))
		})
	})
})
