package nexus

import (
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/helper"
	"context"
	"github.com/spf13/cobra"
)

// NewTaskAddCommand create script add command
func NewTaskAddCommand(options *TaskOptions) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "add",
		Short: "add task to the target nexus server",
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			return options.AddTask(cmd, args)
		},
	}

	cmd.Flags().StringVarP(&options.Repo, "repo", "", "",
		"The name of target repository.")
	cmd.Flags().StringVarP(&options.Cron, "cron", "", "",
		"The cron of the task.")

	helper.MarkFlagsRequired(cmd, "repo", "cron")
	return
}

// AddTask execute the task
func (o *TaskOptions) AddTask(cmd *cobra.Command, args []string) (err error) {
	var client v1.Interface
	client, err = o.GetNexusTaskClient()
	if err == nil {
		err = client.CreateTask(context.Background(), o.Name, o.Repo, o.Cron)
	}
	return
}
