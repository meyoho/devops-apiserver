package nexus

import (
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/helper"
	"context"
	"github.com/spf13/cobra"
)

// NewScriptAddCommand create script add command
func NewScriptAddCommand(options *ScriptOptions) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:    "add",
		Short:  "add script to the target nexus server",
		PreRun: loadContentFromFile(options),
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			return options.AddScript(cmd, args)
		},
	}

	cmd.Flags().StringVarP(&options.Type, "type", "t", "groovy",
		"The type of target script, supported types: groovy.")
	cmd.Flags().StringVarP(&options.Content, "content", "c", "",
		"The content of target script.")
	cmd.Flags().StringVarP(&options.File, "file", "f", "",
		"The file of target script.")

	helper.MarkFlagsRequired(cmd, "type")
	return
}

// AddScript add script to nexus
func (o *ScriptOptions) AddScript(cmd *cobra.Command, args []string) (err error) {
	var client v1.Interface
	client, err = o.GetNexusClient()
	if err == nil {
		err = client.CreateScriptByPath(context.Background(), o.Name, o.Type, o.Content)
	}
	return
}
