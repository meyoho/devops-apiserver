package nexus

import (
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/helper"
	"context"
	"github.com/spf13/cobra"
)

// NewScriptUpdateCommand create script update command
func NewScriptUpdateCommand(options *ScriptOptions) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:    "update",
		Short:  "update the script of nexus server",
		PreRun: loadContentFromFile(options),
		Run: func(cmd *cobra.Command, args []string) {
			err := options.UpdateScript(cmd, args)
			helper.CheckErr(cmd, err)
		},
	}

	cmd.Flags().StringVarP(&options.Type, "type", "t", "groovy",
		"The type of target script, supported types: groovy.")
	cmd.Flags().StringVarP(&options.Content, "content", "c", "",
		"The content of target script.")
	cmd.Flags().StringVarP(&options.File, "file", "f", "",
		"The file of target script.")

	helper.MarkFlagsRequired(cmd, "type", "content")
	return
}

// UpdateScript update script to nexus
func (o *ScriptOptions) UpdateScript(cmd *cobra.Command, args []string) (err error) {
	var client v1.Interface
	client, err = o.GetNexusClient()
	if err == nil {
		err = client.UpdateScriptByPath(context.Background(), o.Name, o.Type, o.Content)
	}
	return
}
