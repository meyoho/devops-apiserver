package nexus

import (
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/helper"
	"context"
	"github.com/spf13/cobra"
)

// RunScriptOptions the options for run a script
type RunScriptOptions struct {
	*ScriptOptions

	JSONParam string
}

// NewScriptRunCommand create script run command
func NewScriptRunCommand(options *ScriptOptions) (cmd *cobra.Command) {
	runOpts := RunScriptOptions{ScriptOptions: options}
	cmd = &cobra.Command{
		Use:   "run",
		Short: "run a script from the nexus server",
		Run: func(cmd *cobra.Command, args []string) {
			err := runOpts.RunScript(cmd, args)
			helper.CheckErr(cmd, err)
		},
	}

	cmd.Flags().StringVarP(&runOpts.JSONParam, "json", "", "",
		"The param of target script with JSON format.")
	return
}

// RunScript run a script
func (o *RunScriptOptions) RunScript(cmd *cobra.Command, args []string) (err error) {
	var client v1.Interface
	client, err = o.GetNexusClient()
	if err == nil {
		err = client.ExecuteScript(context.Background(), o.Name, o.JSONParam)
	}
	return
}
