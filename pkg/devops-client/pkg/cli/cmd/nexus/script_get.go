package nexus

import (
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/helper"
	"context"
	"github.com/spf13/cobra"
)

// NewScriptGetCommand create script list command
func NewScriptGetCommand(options *ScriptOptions) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "get",
		Short: "get a script from the target nexus server",
		Run: func(cmd *cobra.Command, args []string) {
			err := options.GetScript(cmd, args)
			helper.CheckErr(cmd, err)
		},
	}
	return
}

// GetScript get a script from nexus
func (o *ScriptOptions) GetScript(cmd *cobra.Command, args []string) (err error) {
	var client v1.Interface
	var script v1.NexusScript
	client, err = o.GetNexusClient()
	if err == nil {
		if script, err = client.GetScript(context.Background(), o.Name); err == nil {
			cmd.Printf("%s\t%s\n%s\n", script.Name, script.Type, script.Content)
		}
	}
	return
}
