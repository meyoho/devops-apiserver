package nexus

import (
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/helper"
	"context"
	"github.com/spf13/cobra"
)

// NewScriptListCommand create script list command
func NewScriptListCommand(options *ScriptOptions) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "list",
		Short: "list script from the target nexus server",
		Run: func(cmd *cobra.Command, args []string) {
			err := options.ListScripts(cmd, args)
			helper.CheckErr(cmd, err)
		},
	}
	return
}

// ListScripts list script from nexus
func (o *ScriptOptions) ListScripts(cmd *cobra.Command, args []string) (err error) {
	var client v1.Interface
	var scripts []v1.NexusScript
	client, err = o.GetNexusClient()
	if err == nil {
		if scripts, err = client.ListScripts(context.Background()); err == nil {
			for i, script := range scripts {
				cmd.Printf("%d\t%s\t%s\n", i, script.Name, script.Type)
			}
		}
	}
	return
}
