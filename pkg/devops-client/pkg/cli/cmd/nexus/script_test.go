package nexus_test

import (
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	cmd "alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd"
	"bytes"
	"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/spf13/cobra"
	"gopkg.in/h2non/gock.v1"
	"io/ioutil"
	"os"
)

var _ = Describe("nexus command tests", func() {
	var (
		host    = "nexus.test"
		rootCmd *cobra.Command
		buffer  *bytes.Buffer

		transport  = gock.NewTransport()
		httpClient = v1.NewDefaultClient()
	)
	httpClient.Transport = transport

	BeforeEach(func() {
		cmd.GetCommonOptions().HTTPClient = httpClient
		cmd.GetCommonOptions().Transport = transport

		rootCmd = cmd.GetRootCmd()
		buffer = new(bytes.Buffer)
		rootCmd.SetOut(buffer)
		rootCmd.SetErr(buffer)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("nexus script command test", func() {
		var (
			scriptName    = "fake-name"
			scriptType    = "fake-type"
			scriptContent = "fake-content"

			payload = `{"content":"fake-content","name":"fake-name","type":"fake-type"}`
		)

		Context("Add nexus script", func() {
			BeforeEach(func() {
				gock.New(host).
					Post("/service/rest/v1/script").
					JSON(payload).
					Reply(204).
					BodyString(payload)
			})

			It("normal case, should success", func() {
				rootCmd.SetArgs([]string{"nexus", "script", "add",
					"--name", scriptName, "--type", scriptType, "--content", scriptContent,
					"--url", "nexus.test", "--insecure=true"})
				_, err := rootCmd.ExecuteC()

				Expect(err).To(BeNil())
				Expect(buffer.String()).To(Equal(""))
			})

			// we only need to test the file flag for once
			It("add script from a file", func() {
				tmpfile, err := ioutil.TempFile("", "example")
				Expect(err).To(BeNil())
				defer func() {
					err := os.Remove(tmpfile.Name())
					Expect(err).To(BeNil())
				}()

				err = ioutil.WriteFile(tmpfile.Name(), []byte(scriptContent), 0664)
				Expect(err).To(BeNil())

				rootCmd.SetArgs([]string{"nexus", "script", "add",
					"--name", scriptName, "--type", scriptType, "--file", tmpfile.Name(),
					"--url", "nexus.test", "--insecure=true"})
				_, err = rootCmd.ExecuteC()

				Expect(err).To(BeNil())
				Expect(buffer.String()).To(Equal(""))
			})

			// we only need to test the file flag for once
			It("add script from a not exists file", func() {
				rootCmd.SetArgs([]string{"nexus", "script", "add",
					"--name", scriptName, "--type", scriptType, "--file", "fake-file",
					"--url", "nexus.test", "--insecure=true"})
				_, err := rootCmd.ExecuteC()

				Expect(err).To(BeNil())
				Expect(buffer.String()).To(Equal(`error: open fake-file: no such file or directory`))
			})
		})

		Context("Update nexus script", func() {
			BeforeEach(func() {
				gock.New(host).
					Put(fmt.Sprintf("/service/rest/v1/script/%s", scriptName)).
					JSON(payload).
					Reply(204).
					BodyString(payload)
			})

			It("normal case, should success", func() {
				rootCmd.SetArgs([]string{"nexus", "script", "update",
					"--name", scriptName, "--type", scriptType, "--content", scriptContent,
					"--file", "", // TODO put this flag just for pass the test
					"--url", "nexus.test", "--insecure=true"})
				_, err := rootCmd.ExecuteC()

				Expect(err).To(BeNil())
				Expect(buffer.String()).To(Equal(""))
			})
		})

		Context("Run nexus script", func() {
			var (
				jsonParam string
				err       error
			)

			JustBeforeEach(func() {
				gock.New(host).
					Post(fmt.Sprintf("/service/rest/v1/script/%s/run", scriptName)).
					BodyString(jsonParam).
					Reply(200).
					BodyString(payload)
				jsonParam = `{"name":"test-3","versionPolicy":"release","blobStore":""}`

				rootCmd.SetArgs([]string{"nexus", "script", "run",
					"--name", scriptName, "--json", jsonParam,
					"--url", "nexus.test", "--insecure=true"})
				_, err = rootCmd.ExecuteC()
			})

			It("normal case, should success", func() {
				Expect(err).To(BeNil())
				Expect(buffer.String()).To(Equal(""))
			})

			Context("run nexus script with json param", func() {
				BeforeEach(func() {
					jsonParam = `{"name":"test-3","versionPolicy":"release","blobStore":""}`
				})

				It("run nexus script with json param", func() {
					Expect(err).To(BeNil())
					Expect(buffer.String()).To(Equal(""))
				})
			})
		})

		Context("Delete a script", func() {
			BeforeEach(func() {
				gock.New(host).
					Delete(fmt.Sprintf("/service/rest/v1/script/%s", scriptName)).
					Reply(204)
			})

			It("normal case, should success", func() {
				rootCmd.SetArgs([]string{"nexus", "script", "delete",
					"--name", scriptName,
					"--url", "nexus.test", "--insecure=true"})
				_, err := rootCmd.ExecuteC()

				Expect(err).To(BeNil())
				Expect(buffer.String()).To(Equal(""))
			})
		})

		Context("List all scripts", func() {
			BeforeEach(func() {
				gock.New(host).
					Get("/service/rest/v1/script").
					Reply(200).
					BodyString("[" + payload + "]")
			})

			It("normal case, should success", func() {
				rootCmd.SetArgs([]string{"nexus", "script", "list",
					"--url", "nexus.test", "--insecure=true"})
				_, err := rootCmd.ExecuteC()

				Expect(err).To(BeNil())
				Expect(buffer.String()).To(Equal("0\tfake-name\tfake-type\n"))
			})
		})

		Context("Get a script", func() {
			BeforeEach(func() {
				gock.New(host).
					Get(fmt.Sprintf("/service/rest/v1/script/%s", scriptName)).
					Reply(200).BodyString(payload)
			})

			It("normal case, should success", func() {
				rootCmd.SetArgs([]string{"nexus", "script", "get",
					"--name", scriptName,
					"--url", "nexus.test", "--insecure=true"})
				_, err := rootCmd.ExecuteC()

				Expect(err).To(BeNil())
				Expect(buffer.String()).To(Equal("fake-name\tfake-type\nfake-content\n"))
			})
		})
	})
})
