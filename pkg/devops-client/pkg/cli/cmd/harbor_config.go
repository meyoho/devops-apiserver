package cmd

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/common"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/helper"

	"github.com/spf13/cobra"
)

type HarborConfigOpt struct {
	common.Options

	Update bool
	JSON   string
}

var harborConfigOpt HarborConfigOpt
var harborConfigCmd = &cobra.Command{
	Use:   "config",
	Short: "harbor server config",
	Run: func(cmd *cobra.Command, args []string) {
		harborConfigOpt.Run(cmd, args)
	},
}

func init() {
	harborCmd.AddCommand(harborConfigCmd)
	harborConfigCmd.Flags().BoolVarP(&harborConfigOpt.Update, "update", "", false,
		"Whether update configuration of harbor or just print them.")
	harborConfigCmd.Flags().StringVarP(&harborConfigOpt.JSON, "json", "", "",
		"Please provide the json string of harbor configuration when you want to update them.")
}

func (t *HarborConfigOpt) Run(cmd *cobra.Command, args []string) {
	schemes := []string{"http"}
	if !IsInsecure() {
		schemes = append(schemes, "https")
	}

	if harborConfigOpt.Transport == nil {
		harborConfigOpt.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: IsInsecure()},
		}
	}

	opts := v1.NewOptions(
		v1.NewBasicConfig(GetURL(), "", schemes),
		v1.NewBasicAuth(GetUsername(), GetPassword()),
		v1.NewClient(harborConfigOpt.HTTPClient),
		v1.NewTransport(harborConfigOpt.Transport),
	)
	client, err := factory.NewClient("Harbor", "v1.8", opts)
	if err != nil {
		fmt.Println(err)
		//os.Exit(1)
	}
	t.Client = client

	if harborConfigOpt.Update {
		err = t.UpdateHarborConfig(cmd)
	} else {
		err = t.GetHarborConfig(cmd)
	}

	helper.CheckErr(cmd, err)
}

func (t *HarborConfigOpt) GetHarborConfig(cmd *cobra.Command) (err error) {
	var config *v1.Configurations
	if config, err = t.Client.GetHarborConfig(context.Background()); err == nil {
		cmd.Println(config)
	}
	return
}

func (t *HarborConfigOpt) UpdateHarborConfig(cmd *cobra.Command) (err error) {
	err = t.Client.UpdateHarborConfig(context.Background(), t.JSON)
	return
}

func SetHTTPClient(client *http.Client) {
	harborConfigOpt.HTTPClient = client
}

func SetTransport(transport http.RoundTripper) {
	harborConfigOpt.Transport = transport
}
