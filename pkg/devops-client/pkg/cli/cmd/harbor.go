package cmd

import (
	"github.com/spf13/cobra"
)

var harborCmd = &cobra.Command{
	Use:   "harbor",
	Short: "harbor server",
}

func init() {
	rootCmd.AddCommand(harborCmd)
}
