package helper

import "github.com/spf13/cobra"

// MarkFlagRequired marks a field is required
func MarkFlagRequired(cmd *cobra.Command, field string) {
	cmd.MarkFlagRequired(field)
	return
}

// MarkFlagRequired marks several fields are required
func MarkFlagsRequired(cmd *cobra.Command, fields ...string) {
	for _, field := range fields {
		MarkFlagRequired(cmd, field)
	}
	return
}
