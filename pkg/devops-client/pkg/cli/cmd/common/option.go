package common

import (
	"crypto/tls"
	"encoding/base64"
	"net/http"
	"net/url"

	"alauda.io/devops-apiserver/pkg/devops-client/factory"
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
)

// Options hold the common options
type Options struct {
	HTTPClient *http.Client
	Client     v1.Interface
	Transport  http.RoundTripper

	URL      string
	UserName string
	Password string

	Proxy     string
	ProxyAuth string

	Insecure bool
}

// GetSchemes returns the
func (o *Options) GetSchemes() (schemes []string) {
	schemes = []string{"http"}
	if !o.Insecure {
		schemes = append(schemes, "https")
	}
	return
}

// GetClientOptions returns the options of http client
func (o *Options) GetClientOptions() (opts *v1.Options) {
	if o.Transport == nil {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: o.Insecure},
		}

		err := o.SetProxy(tr)
		if err != nil {
			panic(err)
		}

		o.Transport = tr
	}

	options := []v1.Option{
		v1.NewBasicConfig(o.URL, "", o.GetSchemes()),
		v1.NewClient(o.HTTPClient),
		v1.NewTransport(o.Transport)}

	if o.UserName != "" && o.Password != "" {
		options = append(options, v1.NewBasicAuth(o.UserName, o.Password))
	}

	opts = v1.NewOptions(options...)
	return
}

// SetProxy set the proxy for a http
func (o *Options) SetProxy(tr *http.Transport) (err error) {
	if o.Proxy != "" {
		var proxyURL *url.URL
		if proxyURL, err = url.Parse(o.Proxy); err != nil {
			return
		}
		tr.Proxy = http.ProxyURL(proxyURL)
	}

	if o.ProxyAuth != "" {
		basicAuth := "Basic " + base64.StdEncoding.EncodeToString([]byte(o.ProxyAuth))
		tr.ProxyConnectHeader = http.Header{}
		tr.ProxyConnectHeader.Add("Proxy-Authorization", basicAuth)
	}
	return
}

// GetClient returns the client for a specific tool with version
func (o *Options) GetClient(tool, version string) (client v1.Interface, err error) {
	opts := o.GetClientOptions()
	client, err = factory.NewClient(tool, version, opts)
	return
}
