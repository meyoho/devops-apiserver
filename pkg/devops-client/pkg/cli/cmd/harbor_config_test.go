package cmd

import (
	"bytes"

	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("harbor command tests", func() {
	var (
		host       = "https://harbor.test"
		transport  = gock.NewTransport()
		httpClient = v1.NewDefaultClient()
		buffer     *bytes.Buffer
	)
	httpClient.Transport = transport

	BeforeEach(func() {
		SetHTTPClient(httpClient)
		SetTransport(transport)
		buffer = new(bytes.Buffer)
		rootCmd.SetOut(buffer)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetHarborConfig", func() {
		BeforeEach(func() {
			gock.New(host).
				Get("/api/configurations").
				Reply(200).
				JSON(`{"auth_mode":{"value":"oidc_auth","editable":false},"email_from":{"value":"admin <sample_admin@mydomain.com>","editable":true},"email_host":{"value":"smtp.mydomain.com","editable":true},"email_identity":{"value":"","editable":true},"email_insecure":{"value":false,"editable":true},"email_port":{"value":25,"editable":true},"email_ssl":{"value":false,"editable":true},"email_username":{"value":"","editable":true},"http_authproxy_always_onboard":{"value":false,"editable":true},"http_authproxy_endpoint":{"value":"","editable":true},"http_authproxy_tokenreview_endpoint":{"value":"","editable":true},"http_authproxy_verify_cert":{"value":true,"editable":true},"ldap_base_dn":{"value":"","editable":true},"ldap_filter":{"value":"","editable":true},"ldap_group_admin_dn":{"value":"","editable":true},"ldap_group_attribute_name":{"value":"","editable":true},"ldap_group_base_dn":{"value":"","editable":true},"ldap_group_membership_attribute":{"value":"memberof","editable":true},"ldap_group_search_filter":{"value":"","editable":true},"ldap_group_search_scope":{"value":0,"editable":true},"ldap_scope":{"value":0,"editable":true},"ldap_search_dn":{"value":"","editable":true},"ldap_timeout":{"value":0,"editable":true},"ldap_uid":{"value":"cn","editable":true},"ldap_url":{"value":"","editable":true},"ldap_verify_cert":{"value":false,"editable":true},"oidc_client_id":{"value":"alauda-auth","editable":true},"oidc_endpoint":{"value":"https://acp-hk-build.alauda.cn/dex","editable":true},"oidc_name":{"value":"dex","editable":true},"oidc_scope":{"value":"openid,profile,offline_access,groups,email","editable":true},"oidc_verify_cert":{"value":false,"editable":true},"project_creation_restriction":{"value":"everyone","editable":true},"read_only":{"value":false,"editable":true},"robot_token_duration":{"value":43200,"editable":true},"scan_all_policy":{"value":null,"editable":true},"self_registration":{"value":false,"editable":true},"token_expiration":{"value":30,"editable":true},"uaa_client_id":{"value":"","editable":true},"uaa_client_secret":{"value":"<enc-v1>NK5fRaJKH8FO3YX1Bwd91g==","editable":true},"uaa_endpoint":{"value":"","editable":true},"uaa_verify_cert":{"value":true,"editable":true}}`)
		})

		It("get harbor config", func() {
			rootCmd.SetArgs([]string{"harbor", "config",
				"--url", "harbor.test"})
			_, err := rootCmd.ExecuteC()

			Expect(err).To(BeNil())
			Expect(buffer.String()).To(Equal(`&{oidc_auth alauda-auth https://acp-hk-build.alauda.cn/dex dex openid,profile,offline_access,groups,email false}
`))
		})
	})

	Context("UpdateHarborConfig", func() {
		BeforeEach(func() {
			gock.New(host).
				Put("/api/configurations").
				Reply(200).
				JSON(`{"auth_mode":"auth_mode","oidc_client_id":"oidc_client_id","oidc_endpoint":"oidc_endpoint","oidc_name":"oidc_name","oidc_scope":"openid,profile,offline_access,groups,email","oidc_verify_cert":false}`)
		})

		// no ideas how to test this case which with exit code 1
		//It("update harbor config with an invalid json string", func() {
		//	rootCmd.SetArgs([]string{"harbor", "config",
		//		"--url", "harbor.test", "--update"})
		//	_, err := rootCmd.ExecuteC()
		//
		//	Expect(err).To(BeNil())
		//	Expect(buffer.String()).NotTo(Equal(""))
		//})

		It("update harbor config with a valid json string", func() {
			rootCmd.SetArgs([]string{"harbor", "config",
				"--url", "harbor.test", "--update", "--json", `{"auth_mode":"auth_mode","oidc_client_id":"oidc_client_id","oidc_endpoint":"oidc_endpoint","oidc_name":"oidc_name","oidc_scope":"openid,profile,offline_access,groups,email","oidc_verify_cert":false}`})
			_, err := rootCmd.ExecuteC()

			Expect(err).To(BeNil())
			Expect(buffer.String()).To(Equal(""))
		})
	})
})
