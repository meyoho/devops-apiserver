package cmd

import (
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/common"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/cli/cmd/nexus"
	"github.com/spf13/cobra"
	"os"
)

var rootCmd = &cobra.Command{
	Use:   "acp",
	Short: "acp DevOps client",
}

// GetRootCmd only for test purpose
func GetRootCmd() *cobra.Command {
	return rootCmd
}

var commonOptions = common.Options{}

// GetCommonOptions only for test purpose
func GetCommonOptions() *common.Options {
	return &commonOptions
}

// register all commands here
func init() {
	rootCmd.AddCommand(nexus.NewNexusCommand(&commonOptions))

	rootCmd.PersistentFlags().StringVarP(&commonOptions.URL, "url", "", "",
		"The URL of target server")
	rootCmd.PersistentFlags().StringVarP(&commonOptions.UserName, "username", "", "",
		"The username of target server")
	rootCmd.PersistentFlags().StringVarP(&commonOptions.Password, "password", "", "",
		"The password of target server")
	rootCmd.PersistentFlags().BoolVarP(&commonOptions.Insecure, "insecure", "k", false,
		"Skip verify by a insecure connection")

	rootCmd.PersistentFlags().StringVarP(&commonOptions.Proxy, "proxy", "", "",
		"Proxy for the connection")
	rootCmd.PersistentFlags().StringVarP(&commonOptions.ProxyAuth, "proxy-auth", "", "",
		"The auth of proxy for the connection")
}

func GetURL() string {
	return commonOptions.URL
}

func GetUsername() string {
	return commonOptions.UserName
}

func GetPassword() string {
	return commonOptions.Password
}

func IsInsecure() bool {
	return commonOptions.Insecure
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		rootCmd.PrintErrln(err)
		os.Exit(1)
	}
}
