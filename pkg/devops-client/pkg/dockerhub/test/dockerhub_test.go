package test

import (
	"context"
	"time"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	dockerhubV2 "alauda.io/devops-apiserver/pkg/devops-client/pkg/dockerhub/v2"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool DockerHub tests", func() {
	var (
		// host
		host    = "https://dockerhub.test"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options

		transport  = gock.NewTransport()
		httpClient = clientv1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = dockerhubV2.NewClient()
		opts = clientv1.NewOptions(clientv1.NewBasicConfig("dockerhub.test", "", []string{}), clientv1.NewClient(httpClient), clientv1.NewTransport(transport), clientv1.NewBasicAuth("chlins", ""))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetImageRepos", func() {
		const (
			orgs = `{
    "count": 0,
    "next": null,
    "previous": null,
    "results": []
}`
			repos = `{
    "count": 0,
    "next": null,
    "previous": null,
    "results": [
        {
            "user": "chlins",
            "name": "kube-sample-apiserver",
            "namespace": "chlins",
            "repository_type": "image",
            "status": 1,
            "description": "",
            "is_private": false,
            "is_automated": false,
            "can_edit": true,
            "star_count": 0,
            "pull_count": 91,
            "last_updated": "2019-03-16T14:36:16.477927Z",
            "is_migrated": false
        },
        {
            "user": "chlins",
            "name": "nginx",
            "namespace": "chlins",
            "repository_type": "image",
            "status": 1,
            "description": "",
            "is_private": false,
            "is_automated": false,
            "can_edit": true,
            "star_count": 0,
            "pull_count": 21,
            "last_updated": "2019-12-24T06:07:39.774375Z",
            "is_migrated": false
        }
    ]
}`
		)

		BeforeEach(func() {
			gock.New(host).
				Get("/v2/user/orgs").
				Reply(200).
				JSON(orgs)

			gock.New(host).
				Get("/v2/repositories/chlins").
				Reply(200).
				JSON(repos)
		})

		It("get image repos", func() {
			repos, err := client.GetImageRepos(context.Background())
			Expect(err).To(BeNil())
			Expect(len(repos.Items)).To(Equal(2))
			Expect(repos.Items[1]).To(Equal("chlins/nginx"))
		})
	})

	Context("GetImageTags", func() {
		const (
			tags = `{
    "count": 3,
    "next": null,
    "previous": null,
    "results": [
        {
            "name": "1.7.9",
            "full_size": 39942117,
            "images": [
                {
                    "size": 39941893,
                    "digest": "sha256:2204e6cc1bbcc3c5587aaada2c59819599847f1dbe5876dc21eba6764aa795ef",
                    "architecture": "amd64",
                    "os": "linux",
                    "os_version": null,
                    "os_features": "",
                    "variant": null,
                    "features": ""
                }
            ],
            "id": 80730029,
            "repository": 8062298,
            "creator": 5004422,
            "last_updater": 5004422,
            "last_updater_username": "chlins",
            "image_id": null,
            "v2": true,
            "last_updated": "2019-12-24T06:07:39.444763Z"
        },
        {
            "name": "1.7.8",
            "full_size": 39931349,
            "images": [
                {
                    "size": 39931125,
                    "digest": "sha256:e1962791dfe30c2e0e7576bbdca3ac184f7742012dbbf22f5aaeb786074e40ac",
                    "architecture": "amd64",
                    "os": "linux",
                    "os_version": null,
                    "os_features": "",
                    "variant": null,
                    "features": ""
                }
            ],
            "id": 80729944,
            "repository": 8062298,
            "creator": 5004422,
            "last_updater": 5004422,
            "last_updater_username": "chlins",
            "image_id": null,
            "v2": true,
            "last_updated": "2019-12-24T06:07:03.831169Z"
        },
        {
            "name": "1.7",
            "full_size": 39942117,
            "images": [
                {
                    "size": 39941893,
                    "digest": "sha256:2204e6cc1bbcc3c5587aaada2c59819599847f1dbe5876dc21eba6764aa795ef",
                    "architecture": "amd64",
                    "os": "linux",
                    "os_version": null,
                    "os_features": "",
                    "variant": null,
                    "features": ""
                }
            ],
            "id": 77261713,
            "repository": 8062298,
            "creator": 5004422,
            "last_updater": 5004422,
            "last_updater_username": "chlins",
            "image_id": null,
            "v2": true,
            "last_updated": "2019-11-22T08:56:43.366407Z"
        }
    ]
}`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/v2/repositories/chlins/nginx/tags").
				Reply(200).
				JSON(tags)
		})

		It("get repo tags", func() {
			tags, err := client.GetImageTags(context.Background(), "chlins/nginx")
			Expect(err).To(BeNil())
			Expect(len(tags)).To(Equal(3))
			Expect(tags[0].Name).To(Equal("1.7.9"))
		})
	})

	Context("Check Repository Available", func() {
		const (
			repo = `{
    "user": "chlins",
    "name": "nginx",
    "namespace": "chlins",
    "repository_type": "image",
    "status": 1,
    "description": "",
    "is_private": false,
    "is_automated": false,
    "can_edit": true,
    "star_count": 0,
    "pull_count": 21,
    "last_updated": "2019-12-24T06:07:39.774375Z",
    "is_migrated": false,
    "has_starred": false,
    "full_description": null,
    "affiliation": "owner",
    "permissions": {
        "read": true,
        "write": true,
        "admin": true
    }
}`
		)

		BeforeEach(func() {
			gock.New(host).
				Get("/v2/repositories/chlins/nginx").
				Reply(200).
				JSON(repo)
		})

		It("check repository", func() {
			status, err := client.RepositoryAvailable(context.Background(), "chlins/nginx", time.Now())
			Expect(err).To(BeNil())
			Expect(status.StatusCode).To(Equal(200))
		})
	})
})
