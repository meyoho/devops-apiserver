package test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestTests(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("bitbucket_test.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "bitbucket_test", []Reporter{junitReporter})
}
