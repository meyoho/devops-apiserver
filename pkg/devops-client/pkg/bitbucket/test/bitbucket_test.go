package test

import (
	"context"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	v2 "alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool Bitbucket tests", func() {
	var (
		// host
		host    = "https://bitbucket.test"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options

		transport  = gock.NewTransport()
		httpClient = clientv1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = v2.NewClient()
		opts = clientv1.NewOptions(clientv1.NewBasicConfig("bitbucket.test", "", []string{}), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetRemoteRepos", func() {
		const (
			repos = `{
				"pagelen":1,
				"values":[
					{
						"scm":"git",
						"website":"",
						"has_wiki":true,
						"uuid":"{877a102c-e1e0-471a-a72d-578a53e37a4d}",
						"links":{
							"watchers":{
								"href":"https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/watchers"
							},
							"branches":{
								"href":"https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/refs/branches"
							},
							"tags":{
								"href":"https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/refs/tags"
							},
							"commits":{
								"href":"https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/commits"
							},
							"clone":[
								{
									"href":"https://zhangchenyu@alauda.io/devops-apiserver/pkg/fountain.git",
									"name":"https"
								},
								{
									"href":"git@bitbucket.org:mathildetech/fountain.git",
									"name":"ssh"
								}
							],
							"self":{
								"href":"https://api.bitbucket.org/2.0/repositories/mathildetech/fountain"
							},
							"source":{
								"href":"https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/src"
							},
							"html":{
								"href":"https://alauda.io/devops-apiserver/pkg/fountain"
							},
							"avatar":{
								"href":"https://bytebucket.org/ravatar/%7B877a102c-e1e0-471a-a72d-578a53e37a4d%7D?ts=default"
							},
							"hooks":{
								"href":"https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/hooks"
							},
							"forks":{
								"href":"https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/forks"
							},
							"downloads":{
								"href":"https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/downloads"
							},
							"issues":{
								"href":"https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/issues"
							},
							"pullrequests":{
								"href":"https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/pullrequests"
							}
						},
						"fork_policy":"no_public_forks",
						"name":"Fountain",
						"project":{
							"key":"PROJ",
							"type":"project",
							"uuid":"{68a75cf2-80f9-427b-9a45-dcb142a4c1bb}",
							"links":{
								"self":{
									"href":"https://api.bitbucket.org/2.0/teams/mathildetech/projects/PROJ"
								},
								"html":{
									"href":"https://bitbucket.org/account/user/mathildetech/projects/PROJ"
								},
								"avatar":{
									"href":"https://bitbucket.org/account/user/mathildetech/projects/PROJ/avatar/32"
								}
							},
							"name":"Alauda"
						},
						"language":"shell",
						"created_on":"2014-08-14T08:22:40.817310+00:00",
						"mainbranch":{
							"type":"branch",
							"name":"master"
						},
						"full_name":"mathildetech/fountain",
						"has_issues":true,
						"owner":{
							"username":"mathildetech",
							"display_name":"Mathilde Technology Corp",
							"type":"team",
							"uuid":"{946ce0ec-3f05-47b4-a58c-0b098e85d7b9}",
							"links":{
								"self":{
									"href":"https://api.bitbucket.org/2.0/teams/%7B946ce0ec-3f05-47b4-a58c-0b098e85d7b9%7D"
								},
								"html":{
									"href":"https://bitbucket.org/%7B946ce0ec-3f05-47b4-a58c-0b098e85d7b9%7D/"
								},
								"avatar":{
									"href":"https://bitbucket.org/account/mathildetech/avatar/"
								}
							}
						},
						"updated_on":"2016-10-26T06:21:49.711708+00:00",
						"size":4961021,
						"type":"repository",
						"slug":"fountain",
						"is_private":true,
						"description":"Where our heros spawn"
					},
					{
						"scm":"git",
						"website":"",
						"has_wiki":false,
						"name":"pipeline-templates-test",
						"links":{
							"watchers":{
								"href":"https://api.bitbucket.org/2.0/repositories/ynli/pipeline-templates-test/watchers"
							},
							"branches":{
								"href":"https://api.bitbucket.org/2.0/repositories/ynli/pipeline-templates-test/refs/branches"
							},
							"tags":{
								"href":"https://api.bitbucket.org/2.0/repositories/ynli/pipeline-templates-test/refs/tags"
							},
							"commits":{
								"href":"https://api.bitbucket.org/2.0/repositories/ynli/pipeline-templates-test/commits"
							},
							"clone":[
								{
									"href":"https://ynli@bitbucket.org/ynli/pipeline-templates-test.git",
									"name":"https"
								},
								{
									"href":"git@bitbucket.org:ynli/pipeline-templates-test.git",
									"name":"ssh"
								}
							],
							"self":{
								"href":"https://api.bitbucket.org/2.0/repositories/ynli/pipeline-templates-test"
							},
							"source":{
								"href":"https://api.bitbucket.org/2.0/repositories/ynli/pipeline-templates-test/src"
							},
							"html":{
								"href":"https://bitbucket.org/ynli/pipeline-templates-test"
							},
							"avatar":{
								"href":"https://bytebucket.org/ravatar/%7Bde2048a2-e348-45dd-b73a-f3b6ec30c85e%7D?ts=default"
							},
							"hooks":{
								"href":"https://api.bitbucket.org/2.0/repositories/ynli/pipeline-templates-test/hooks"
							},
							"forks":{
								"href":"https://api.bitbucket.org/2.0/repositories/ynli/pipeline-templates-test/forks"
							},
							"downloads":{
								"href":"https://api.bitbucket.org/2.0/repositories/ynli/pipeline-templates-test/downloads"
							},
							"pullrequests":{
								"href":"https://api.bitbucket.org/2.0/repositories/ynli/pipeline-templates-test/pullrequests"
							}
						},
						"fork_policy":"no_public_forks",
						"uuid":"{de2048a2-e348-45dd-b73a-f3b6ec30c85e}",
						"language":"",
						"created_on":"2019-05-10T06:45:58.625346+00:00",
						"mainbranch":{
							"type":"branch",
							"name":"master"
						},
						"full_name":"ynli/pipeline-templates-test",
						"has_issues":false,
						"owner":{
							"display_name":"liyinan",
							"uuid":"{1be053aa-c4a9-4867-b456-cd98f0f5c6a7}",
							"links":{
								"self":{
									"href":"https://api.bitbucket.org/2.0/users/%7B1be053aa-c4a9-4867-b456-cd98f0f5c6a7%7D"
								},
								"html":{
									"href":"https://bitbucket.org/%7B1be053aa-c4a9-4867-b456-cd98f0f5c6a7%7D/"
								},
								"avatar":{
									"href":"https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/5cab0a8fbb353819f2b6168b/67c50fbe-cb04-4d31-b7eb-faaaa17f66a4/128"
								}
							},
							"nickname":"ynli",
							"type":"user",
							"account_id":"5cab0a8fbb353819f2b6168b"
						},
						"updated_on":"2019-06-18T02:02:27.865072+00:00",
						"size":900019,
						"type":"repository",
						"slug":"pipeline-templates-test",
						"is_private":true,
						"description":""
					}
				],
				"next":""
			}`
		)

		BeforeEach(func() {
			gock.New(host).
				Get("/repositories").
				MatchParam("role", "member").
				Reply(200).
				JSON(repos)
		})

		It("list repos", func() {
			repos, err := client.GetRemoteRepos(context.Background())
			Expect(err).To(BeNil())
			Expect(len(repos.Owners)).To(Equal(2))
			for _, v := range repos.Owners {
				Expect(v.Repositories[0].Owner.Name).To(Equal(v.Name))
				switch v.Repositories[0].Owner.Name {
				case "mathildetech":
					Expect(v.Repositories[0].Owner.Type).To(Equal(v1.OriginCodeRepoOwnerType("Org")))
					Expect(v.Repositories[0].Name).To(Equal("Fountain"))
				case "ynli":
					Expect(v.Repositories[0].Name).To(Equal("pipeline-templates-test"))
					Expect(v.Repositories[0].Owner.Type).To(Equal(v1.OriginCodeRepoOwnerType("User")))
				default:
					Expect(v.Repositories[0].Owner.Name).ToNot(HaveLen(0))
				}
			}
		})
	})

	Context("GetBranches", func() {
		const branches = `{
			"pagelen": 10,
			"size": 1,
			"values": [
				{
					"name": "feature/class-volume",
					"links": {
						"commits": {
							"href": "https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/commits/feature/class-volume"
						},
						"self": {
							"href": "https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/refs/branches/feature/class-volume"
						},
						"html": {
							"href": "https://alauda.io/devops-apiserver/pkg/fountain/branch/feature/class-volume"
						}
					},
					"default_merge_strategy": "merge_commit",
					"merge_strategies": [
						"merge_commit",
						"squash",
						"fast_forward"
					],
					"type": "branch",
					"target": {
						"hash": "a8b1173065d3d5d0e237d9b598149969777a6c89",
						"repository": {
							"links": {
								"self": {
									"href": "https://api.bitbucket.org/2.0/repositories/mathildetech/fountain"
								},
								"html": {
									"href": "https://alauda.io/devops-apiserver/pkg/fountain"
								},
								"avatar": {
									"href": "https://bytebucket.org/ravatar/%7B877a102c-e1e0-471a-a72d-578a53e37a4d%7D?ts=default"
								}
							},
							"type": "repository",
							"name": "Fountain",
							"full_name": "mathildetech/fountain",
							"uuid": "{877a102c-e1e0-471a-a72d-578a53e37a4d}"
						},
						"links": {
							"self": {
								"href": "https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/commit/a8b1173065d3d5d0e237d9b598149969777a6c89"
							},
							"comments": {
								"href": "https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/commit/a8b1173065d3d5d0e237d9b598149969777a6c89/comments"
							},
							"patch": {
								"href": "https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/patch/a8b1173065d3d5d0e237d9b598149969777a6c89"
							},
							"html": {
								"href": "https://alauda.io/devops-apiserver/pkg/fountain/commits/a8b1173065d3d5d0e237d9b598149969777a6c89"
							},
							"diff": {
								"href": "https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/diff/a8b1173065d3d5d0e237d9b598149969777a6c89"
							},
							"approve": {
								"href": "https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/commit/a8b1173065d3d5d0e237d9b598149969777a6c89/approve"
							},
							"statuses": {
								"href": "https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/commit/a8b1173065d3d5d0e237d9b598149969777a6c89/statuses"
							}
						},
						"author": {
							"raw": "bin liu <liubin0329@gmail.com>",
							"type": "author",
							"user": {
								"display_name": "bin liu",
								"uuid": "{5ffd35fe-6619-40df-a532-0da2912008de}",
								"links": {
									"self": {
										"href": "https://api.bitbucket.org/2.0/users/%7B5ffd35fe-6619-40df-a532-0da2912008de%7D"
									},
									"html": {
										"href": "https://bitbucket.org/%7B5ffd35fe-6619-40df-a532-0da2912008de%7D/"
									},
									"avatar": {
										"href": "https://avatar-cdn.atlassian.com/557058%3Aec715c5b-481f-4fe8-9af0-c68fdd0491d9?by=id&sg=UIr3CGfbVAbrdpbsK6i1Nc9veGc%3D&d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FBL-4.png"
									}
								},
								"nickname": "liubin0329",
								"type": "user",
								"account_id": "557058:ec715c5b-481f-4fe8-9af0-c68fdd0491d9"
							}
						},
						"parents": [
							{
								"hash": "fe2f24b6eb77dea86ed72ed66a625aa14c5111c1",
								"type": "commit",
								"links": {
									"self": {
										"href": "https://api.bitbucket.org/2.0/repositories/mathildetech/fountain/commit/fe2f24b6eb77dea86ed72ed66a625aa14c5111c1"
									},
									"html": {
										"href": "https://alauda.io/devops-apiserver/pkg/fountain/commits/fe2f24b6eb77dea86ed72ed66a625aa14c5111c1"
									}
								}
							}
						],
						"date": "2016-07-16T03:49:06+00:00",
						"message": "change vars for riki\n",
						"type": "commit"
					}
				}
			],
			"page": 1,
			"next": ""
		}`
		BeforeEach(func() {
			gock.New(host).
				Get("/repositories/mathildetech/Fountain/refs/branches").
				Reply(200).
				JSON(branches)
		})

		It("get repo branches", func() {
			branches, err := client.GetBranches(context.Background(), "mathildetech", "Fountain", "")
			Expect(err).To(BeNil())
			Expect(len(branches)).To(Equal(1))
			Expect(branches[0].Name).To(Equal("feature/class-volume"))
			Expect(branches[0].Commit).To(Equal("a8b1173065d3d5d0e237d9b598149969777a6c89"))
		})
	})

	Context("CreateCodeRepoProject", func() {
		It("create group", func() {
			_, err := client.CreateCodeRepoProject(context.Background(), v1.CreateProjectOptions{})
			Expect(err).ToNot(BeNil())
			Expect(err.Error()).To(Equal("not supported"))
		})
	})

	Context("ListCodeRepoProjects", func() {
		const (
			teams = `{
				"pagelen": 10,
				"values": [
					{
						"username": "mathildetech",
						"display_name": "Mathilde Technology Corp",
						"uuid": "{946ce0ec-3f05-47b4-a58c-0b098e85d7b9}",
						"links": {
							"hooks": {
								"href": "https://api.bitbucket.org/2.0/teams/%7B946ce0ec-3f05-47b4-a58c-0b098e85d7b9%7D/hooks"
							},
							"self": {
								"href": "https://api.bitbucket.org/2.0/teams/%7B946ce0ec-3f05-47b4-a58c-0b098e85d7b9%7D"
							},
							"repositories": {
								"href": "https://api.bitbucket.org/2.0/repositories/%7B946ce0ec-3f05-47b4-a58c-0b098e85d7b9%7D"
							},
							"html": {
								"href": "https://bitbucket.org/%7B946ce0ec-3f05-47b4-a58c-0b098e85d7b9%7D/"
							},
							"avatar": {
								"href": "https://bitbucket.org/account/mathildetech/avatar/"
							},
							"members": {
								"href": "https://api.bitbucket.org/2.0/teams/%7B946ce0ec-3f05-47b4-a58c-0b098e85d7b9%7D/members"
							},
							"projects": {
								"href": "https://api.bitbucket.org/2.0/teams/%7B946ce0ec-3f05-47b4-a58c-0b098e85d7b9%7D/projects/"
							},
							"snippets": {
								"href": "https://api.bitbucket.org/2.0/snippets/%7B946ce0ec-3f05-47b4-a58c-0b098e85d7b9%7D"
							}
						},
						"created_on": "2014-08-11T16:00:03.999549+00:00",
						"type": "team",
						"properties": {},
						"has_2fa_enabled": null
					}
				],
				"page": 1,
				"size": 1
			}`

			user = `{"type":"user","links":{"self":{"href":"https://api.bitbucket.org/2.0/users/chengjingtao"},"html":{"href":"https://bitbucket.org/chengjingtao/"},"avatar":{"href":"https://bitbucket.org/account/chengjingtao/avatar/"},"followers":{"href":"https://api.bitbucket.org/2.0/users/chengjingtao/followers"},"following":{"href":"https://api.bitbucket.org/2.0/users/chengjingtao/following"},"repositories":{"href":"https://api.bitbucket.org/2.0/repositories/chengjingtao"}},"username":"chengjingtao","nickname":"chengjingtao","account_status":"active","display_name":"chengjt","created_on":"2017-06-19T04:41:34.38587Z","uuid":"{93b88a89-bbf8-4527-a875-d8d7f153af88}","account_id":"557058:38c8b1b1-ad26-40e8-9e45-3ce8af3de3c3"}`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/teams").
				MatchParam("role", "member").
				Reply(200).
				JSON(teams)

			gock.New(host).
				Get("/user").
				Reply(200).
				JSON(user)
		})

		It("list groups", func() {
			groups, err := client.ListCodeRepoProjects(context.Background(), v1.ListProjectOptions{})
			Expect(err).To(BeNil())
			Expect(groups.Items).To(HaveLen(2))
			Expect(groups.Items[0].Name).To(Equal("mathildetech"))
		})
	})

})
