// Code generated. DO NOT EDIT.
package v2

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/util/k8s"

	corev1 "k8s.io/api/core/v1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/client/commits"

	"github.com/go-logr/logr"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"
	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/client"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/client/refs"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/client/repositories"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/client/teams"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/client/users"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/models"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/generic"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/transport"
	"github.com/dustin/go-humanize"
	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is v1.tool client
type Client struct {
	clientv1.NotImplement
	logger        logr.Logger
	client        *client.Bitbucket
	opts          *clientv1.Options
	authInfo      runtime.ClientAuthInfoWriter
	httpClient    *http.Client
	serviceClient *generic.ServiceClient
}

var _ clientv1.Interface = &Client{}

// NewClient new v1.tool client
func NewClient() clientv1.ClientFactory {
	return func(opts *clientv1.Options) clientv1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			ct := &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}

			if ct.authInfo == nil {
				// adapter auth
				ct.init()
			}
			return ct
		}

		return &Client{client: client.Default}
	}
}

// init constructs serviceClient for client
func (c *Client) init() {
	if c.opts != nil && c.opts.GenericOpts != nil {
		var (
			codeRepoService *v1.CodeRepoService
			secret          *corev1.Secret
		)

		for _, opt := range c.opts.GenericOpts.Options {
			switch opt.(type) {
			case *v1.CodeRepoService:
				codeRepoService = opt.(*v1.CodeRepoService)
			case *corev1.Secret:
				secret = opt.(*corev1.Secret)
			}
		}

		c.serviceClient = generic.NewServiceClient(codeRepoService, secret)
		// adapter auth info
		if c.serviceClient.IsBasicAuth() {
			c.authInfo = openapi.BasicAuth(c.serviceClient.GetUsername(), c.serviceClient.GetPassword())
		} else {
			c.serviceClient.OAuth2Info.AccessTokenKey = k8s.BitbucketAccessTokenKey
			c.serviceClient.OAuth2Info.Scope = k8s.BitbucketAccessTokenScope
			c.authInfo = openapi.BearerToken(c.serviceClient.OAuth2Info.AccessToken)
		}
		c.serviceClient.SetClient(c.httpClient)
	}
}

func (c *Client) GetAuthorizeUrl(ctx context.Context, redirectUrl string) string {
	return fmt.Sprintf("%s/site/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		c.serviceClient.GetHtml(), c.serviceClient.GetClientID(), redirectUrl, c.serviceClient.GetScope())
}

func (c *Client) getAccessTokenUrl() string {
	return fmt.Sprintf("%s/site/oauth2/access_token", c.serviceClient.GetHtml())
}

type AccessTokenResponseBitbucket struct {
	generic.AccessTokenResponse
	Scopes string `json:"scopes"`

	RefreshToken string `json:"refresh_token"`
}

func (a *AccessTokenResponseBitbucket) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken:  a.AccessToken,
		Scope:        a.Scopes,
		RefreshToken: a.RefreshToken,
		CreatedAt:    time.Now().Format(time.RFC3339),
	}
}

func (c *Client) AccessToken(ctx context.Context, redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessTokenUrl()
		response       = &AccessTokenResponseBitbucket{}
	)

	return c.serviceClient.AccessToken(response, redirectUrl, accessTokenUrl)
}

func (c *Client) GetCloneURL(p *models.Repository) string {
	for _, link := range p.Links.Clone {
		if link.Name == "https" {
			return link.Href.String()
		}
	}
	return ""
}

func (c *Client) GetSSHURL(p *models.Repository) string {
	for _, link := range p.Links.Clone {
		if link.Name == "ssh" {
			return link.Href.String()
		}
	}
	return ""
}

func (c *Client) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo v1.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if bitbucketRepo, ok := remoteRepo.(*models.Repository); ok {
		fullName := strings.Split(bitbucketRepo.FullName, "/")
		username := bitbucketRepo.Owner.Username
		if len(fullName) > 1 && len(username) == 0 {
			username = fullName[0]
		}
		codeRepo = v1.OriginCodeRepository{
			CodeRepoServiceType: v1.CodeRepoServiceTypeBitbucket,
			ID:                  bitbucketRepo.UUID,
			Name:                bitbucketRepo.Name,
			FullName:            bitbucketRepo.FullName,
			Description:         bitbucketRepo.Description,
			HTMLURL:             bitbucketRepo.Links.HTML.Href.String(),
			CloneURL:            c.GetCloneURL(bitbucketRepo),
			SSHURL:              c.GetSSHURL(bitbucketRepo),
			Language:            bitbucketRepo.Language,
			Owner: v1.OwnerInRepository{
				ID:   bitbucketRepo.Owner.UUID,
				Name: username,
				Type: v1.GetOwnerType(bitbucketRepo.Owner.Type),
			},

			CreatedAt: &metav1.Time{Time: time.Time(bitbucketRepo.CreatedOn)},
			// UpdatedOn on bitbuckect will reflect of pushedAt
			PushedAt:  &metav1.Time{Time: time.Time(bitbucketRepo.UpdatedOn)},
			UpdatedAt: &metav1.Time{Time: time.Time(bitbucketRepo.UpdatedOn)},

			Private:      bitbucketRepo.IsPrivate,
			Size:         bitbucketRepo.Size * 1000,
			SizeHumanize: humanize.Bytes(uint64(bitbucketRepo.Size)),
		}
	}

	return
}

// GetRemoteRepos implements CodeRepoService
func (c *Client) GetRemoteRepos(ctx context.Context) (*v1.CodeRepoBindingRepositories, error) {
	var (
		page       = int32(0)
		pageLen    = int32(5000)
		role       = "member"
		modelRepos = []*models.Repository{}
	)

	for {
		page = page + 1
		repoParams := repositories.
			NewGetRepositoriesUsernameParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithRole(&role).
			WithPage(&page).
			WithPagelen(&pageLen)
		repos, err := c.client.Repositories.GetRepositoriesUsername(repoParams, c.authInfo)
		if err != nil {
			return nil, err
		}

		if repos.Payload != nil {
			modelRepos = append(modelRepos, repos.Payload.Values...)

			if repos.Payload.Next == "" {
				break
			}
		}

	}

	result := &v1.CodeRepoBindingRepositories{
		Type:   "bitbucket",
		Owners: []v1.CodeRepositoryOwner{},
	}

	dictOwners := make(map[string]*v1.CodeRepositoryOwner)
	for _, repo := range modelRepos {
		ownerKey := repo.Owner.Username
		fullName := strings.Split(repo.FullName, "/")
		if len(fullName) > 1 && len(ownerKey) == 0 {
			ownerKey = fullName[0]
		}
		if owner, ok := dictOwners[ownerKey]; !ok {
			owner = &v1.CodeRepositoryOwner{
				Type:         v1.GetOwnerType(repo.Owner.Type),
				ID:           repo.Owner.UUID,
				Name:         ownerKey,
				HTMLURL:      repo.Links.HTML.Href.String(),
				AvatarURL:    repo.Links.Avatar.Href.String(),
				Repositories: make([]v1.OriginCodeRepository, 0, 10),
			}
			dictOwners[ownerKey] = owner
		}

		codeRepo := c.ConvertRemoteRepoToBindingRepo(repo)
		dictOwners[ownerKey].Repositories = append(dictOwners[ownerKey].Repositories, codeRepo)
	}

	for _, owner := range dictOwners {
		if len(owner.Repositories) > 0 {
			result.Owners = append(result.Owners, *owner)
		}
	}

	return result, nil
}

// GetBranches implements CodeRepoService
func (c *Client) GetBranches(ctx context.Context, owner, repo, repoFullName string) ([]v1.CodeRepoBranch, error) {
	var (
		page     = int32(0)
		pageLen  = int32(100)
		branches = []*models.Branch{}
	)

	for {
		page = page + 1
		branchParams := refs.
			NewGetRepositoriesUsernameRepoSlugRefsBranchesParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithUsername(owner).
			WithRepoSlug(repo).
			WithPage(&page).
			WithPagelen(&pageLen)
		branchsOK, err := c.client.Refs.GetRepositoriesUsernameRepoSlugRefsBranches(branchParams, c.authInfo)
		if err != nil {
			return nil, err
		}

		if branchsOK.Payload != nil {
			branches = append(branches, branchsOK.Payload.Values...)

			if branchsOK.Payload.Next == "" {
				break
			}
		}

	}

	result := make([]v1.CodeRepoBranch, 0)
	for i := range branches {
		result = append(result, v1.CodeRepoBranch{
			Name:   branches[i].Name,
			Commit: branches[i].Target.Hash,
		})
	}

	return result, nil
}

// CreateCodeRepoProject implements CodeRepoService
func (c *Client) CreateCodeRepoProject(_ context.Context, _ v1.CreateProjectOptions) (*v1.ProjectData, error) {
	return nil, fmt.Errorf("not supported")
}

func bitbucketUserAsProjectData(user *models.User) (*v1.ProjectData, error) {
	var projectData = &v1.ProjectData{
		ObjectMeta: metav1.ObjectMeta{
			Name: user.Username,
			Annotations: map[string]string{
				"avatarURL":   "",
				"webURL":      "/" + user.Username,
				"description": "",
				"type":        string(v1.OriginCodeRepoOwnerTypeUser),
			},
		},

		Data: map[string]string{
			"username":    user.Username,
			"displayname": user.DisplayName,
			"nickname":    user.Nickname,
		},
	}
	return projectData, nil
}

func (c *Client) currentUser(ctx context.Context) (*v1.ProjectData, error) {
	userParams := users.
		NewGetUserParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	userOK, err := c.client.Users.GetUser(userParams, c.authInfo)
	if err != nil {
		return nil, err
	}
	return bitbucketUserAsProjectData(userOK.Payload)
}

// ListCodeRepoProjects implements CodeRepoService
func (c *Client) ListCodeRepoProjects(ctx context.Context, opts v1.ListProjectOptions) (*v1.ProjectDataList, error) {
	var (
		page       = int32(0)
		pageLen    = int32(100)
		role       = "member"
		modelTeams = []*models.Team{}
	)

	for {
		page = page + 1
		teamsParam := teams.
			NewGetTeamsParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithRole(&role).
			WithPage(&page).
			WithPagelen(&pageLen)
		teamsOK, err := c.client.Teams.GetTeams(teamsParam, c.authInfo)
		if err != nil {
			return nil, err
		}

		if teamsOK.Payload != nil {
			modelTeams = append(modelTeams, teamsOK.Payload.Values...)

			if teamsOK.Payload.Next == "" {
				break
			}
		}
	}

	result := &v1.ProjectDataList{
		Items: []v1.ProjectData{},
	}

	for _, team := range modelTeams {
		result.Items = append(result.Items, v1.ProjectData{
			ObjectMeta: metav1.ObjectMeta{
				Name: team.Username,
				Annotations: map[string]string{
					"avatarURL":   team.Links.Avatar.Href.String(),
					"accessPath":  team.Links.HTML.Href.String(),
					"description": team.DisplayName,
					"type":        string(v1.OriginCodeRepoRoleTypeOrg),
				},
			},
		})
	}

	userProject, err := c.currentUser(ctx)
	if err != nil {
		return nil, err
	}

	result.Items = append(result.Items, *userProject)

	return result, nil
}

// GetLatestRepoCommit implements CodeRepoService
func (c *Client) GetLatestRepoCommit(ctx context.Context, repoID, owner, repoName, repoFullName string) (commit *v1.RepositoryCommit, status *v1.HostPortStatus) {
	params := commits.
		NewGetRepositoriesUsernameRepoSlugCommitsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithUsername(owner).
		WithRepoSlug(repoName)
	payload, err := c.client.Commits.GetRepositoriesUsernameRepoSlugCommits(params, c.authInfo)
	if err != nil {
		return nil, &v1.HostPortStatus{
			StatusCode: 500,
			Response:   err.Error(),
		}
	}

	if len(payload.Payload.Values) > 0 {
		commit = &v1.RepositoryCommit{
			CommitID:       payload.Payload.Values[0].Hash,
			CommitAt:       &metav1.Time{Time: time.Time(payload.Payload.Values[0].Date)},
			CommitterName:  payload.Payload.Values[0].Author.User.Username,
			CommitterEmail: payload.Payload.Values[0].Author.Raw,
			CommitMessage:  payload.Payload.Values[0].Message,
		}
	}

	return commit, &v1.HostPortStatus{
		StatusCode:  200,
		LastAttempt: &metav1.Time{Time: time.Now()},
	}
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(c.httpClient, url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(ctx context.Context) (*v1.HostPortStatus, error) {
	params := users.
		NewGetUserParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	_, err := c.client.Users.GetUser(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return &v1.HostPortStatus{StatusCode: 200}, nil
}
