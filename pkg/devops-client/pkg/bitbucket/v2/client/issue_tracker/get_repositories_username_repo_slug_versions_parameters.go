// Code generated by go-swagger; DO NOT EDIT.

package issue_tracker

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetRepositoriesUsernameRepoSlugVersionsParams creates a new GetRepositoriesUsernameRepoSlugVersionsParams object
// with the default values initialized.
func NewGetRepositoriesUsernameRepoSlugVersionsParams() *GetRepositoriesUsernameRepoSlugVersionsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugVersionsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugVersionsParamsWithTimeout creates a new GetRepositoriesUsernameRepoSlugVersionsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetRepositoriesUsernameRepoSlugVersionsParamsWithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugVersionsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugVersionsParams{

		timeout: timeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugVersionsParamsWithContext creates a new GetRepositoriesUsernameRepoSlugVersionsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetRepositoriesUsernameRepoSlugVersionsParamsWithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugVersionsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugVersionsParams{

		Context: ctx,
	}
}

// NewGetRepositoriesUsernameRepoSlugVersionsParamsWithHTTPClient creates a new GetRepositoriesUsernameRepoSlugVersionsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetRepositoriesUsernameRepoSlugVersionsParamsWithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugVersionsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugVersionsParams{
		HTTPClient: client,
	}
}

/*GetRepositoriesUsernameRepoSlugVersionsParams contains all the parameters to send to the API endpoint
for the get repositories username repo slug versions operation typically these are written to a http.Request
*/
type GetRepositoriesUsernameRepoSlugVersionsParams struct {

	/*RepoSlug
	  This can either be the repository slug or the UUID of the repository,
	surrounded by curly-braces, for example: `{repository UUID}`.


	*/
	RepoSlug string
	/*Username
	  This can either be the username or the UUID of the account,
	surrounded by curly-braces, for example: `{account UUID}`. An account
	is either a team or user.


	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repositories username repo slug versions params
func (o *GetRepositoriesUsernameRepoSlugVersionsParams) WithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugVersionsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repositories username repo slug versions params
func (o *GetRepositoriesUsernameRepoSlugVersionsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repositories username repo slug versions params
func (o *GetRepositoriesUsernameRepoSlugVersionsParams) WithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugVersionsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repositories username repo slug versions params
func (o *GetRepositoriesUsernameRepoSlugVersionsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repositories username repo slug versions params
func (o *GetRepositoriesUsernameRepoSlugVersionsParams) WithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugVersionsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repositories username repo slug versions params
func (o *GetRepositoriesUsernameRepoSlugVersionsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithRepoSlug adds the repoSlug to the get repositories username repo slug versions params
func (o *GetRepositoriesUsernameRepoSlugVersionsParams) WithRepoSlug(repoSlug string) *GetRepositoriesUsernameRepoSlugVersionsParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the get repositories username repo slug versions params
func (o *GetRepositoriesUsernameRepoSlugVersionsParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithUsername adds the username to the get repositories username repo slug versions params
func (o *GetRepositoriesUsernameRepoSlugVersionsParams) WithUsername(username string) *GetRepositoriesUsernameRepoSlugVersionsParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get repositories username repo slug versions params
func (o *GetRepositoriesUsernameRepoSlugVersionsParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetRepositoriesUsernameRepoSlugVersionsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
