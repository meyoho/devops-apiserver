// Code generated by go-swagger; DO NOT EDIT.

package issue_tracker

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/models"
)

// GetRepositoriesUsernameRepoSlugVersionsReader is a Reader for the GetRepositoriesUsernameRepoSlugVersions structure.
type GetRepositoriesUsernameRepoSlugVersionsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetRepositoriesUsernameRepoSlugVersionsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetRepositoriesUsernameRepoSlugVersionsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 404:
		result := NewGetRepositoriesUsernameRepoSlugVersionsNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetRepositoriesUsernameRepoSlugVersionsOK creates a GetRepositoriesUsernameRepoSlugVersionsOK with default headers values
func NewGetRepositoriesUsernameRepoSlugVersionsOK() *GetRepositoriesUsernameRepoSlugVersionsOK {
	return &GetRepositoriesUsernameRepoSlugVersionsOK{}
}

/*GetRepositoriesUsernameRepoSlugVersionsOK handles this case with default header values.

The versions that have been defined in the issue tracker.
*/
type GetRepositoriesUsernameRepoSlugVersionsOK struct {
	Payload *models.PaginatedVersions
}

func (o *GetRepositoriesUsernameRepoSlugVersionsOK) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/versions][%d] getRepositoriesUsernameRepoSlugVersionsOK  %+v", 200, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugVersionsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.PaginatedVersions)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetRepositoriesUsernameRepoSlugVersionsNotFound creates a GetRepositoriesUsernameRepoSlugVersionsNotFound with default headers values
func NewGetRepositoriesUsernameRepoSlugVersionsNotFound() *GetRepositoriesUsernameRepoSlugVersionsNotFound {
	return &GetRepositoriesUsernameRepoSlugVersionsNotFound{}
}

/*GetRepositoriesUsernameRepoSlugVersionsNotFound handles this case with default header values.

The specified repository does not exist or does not have the issue tracker enabled.
*/
type GetRepositoriesUsernameRepoSlugVersionsNotFound struct {
	Payload *models.Error
}

func (o *GetRepositoriesUsernameRepoSlugVersionsNotFound) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/versions][%d] getRepositoriesUsernameRepoSlugVersionsNotFound  %+v", 404, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugVersionsNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
