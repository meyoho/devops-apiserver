// Code generated by go-swagger; DO NOT EDIT.

package issue_tracker

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/models"
)

// PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesReader is a Reader for the PostRepositoriesUsernameRepoSlugIssuesIssueIDChanges structure.
type PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 403:
		result := NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesCreated creates a PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesCreated with default headers values
func NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesCreated() *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesCreated {
	return &PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesCreated{}
}

/*PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesCreated handles this case with default header values.

The newly created issue change.
*/
type PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesCreated struct {
	/*The (absolute) URL of the newly created issue change.
	 */
	Location string

	Payload *models.IssueChange
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesCreated) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}/issues/{issue_id}/changes][%d] postRepositoriesUsernameRepoSlugIssuesIssueIdChangesCreated  %+v", 201, o.Payload)
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header Location
	o.Location = response.GetHeader("Location")

	o.Payload = new(models.IssueChange)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesUnauthorized creates a PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesUnauthorized with default headers values
func NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesUnauthorized() *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesUnauthorized {
	return &PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesUnauthorized{}
}

/*PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesUnauthorized handles this case with default header values.

When the request wasn't authenticated.
*/
type PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesUnauthorized struct {
	Payload *models.Error
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesUnauthorized) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}/issues/{issue_id}/changes][%d] postRepositoriesUsernameRepoSlugIssuesIssueIdChangesUnauthorized  %+v", 401, o.Payload)
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesForbidden creates a PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesForbidden with default headers values
func NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesForbidden() *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesForbidden {
	return &PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesForbidden{}
}

/*PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesForbidden handles this case with default header values.

When the authenticated user isn't authorized to modify the issue.
*/
type PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesForbidden struct {
	Payload *models.Error
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesForbidden) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}/issues/{issue_id}/changes][%d] postRepositoriesUsernameRepoSlugIssuesIssueIdChangesForbidden  %+v", 403, o.Payload)
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesNotFound creates a PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesNotFound with default headers values
func NewPostRepositoriesUsernameRepoSlugIssuesIssueIDChangesNotFound() *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesNotFound {
	return &PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesNotFound{}
}

/*PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesNotFound handles this case with default header values.

The specified repository or issue does not exist or does not have the issue tracker enabled.
*/
type PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesNotFound struct {
	Payload *models.Error
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesNotFound) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}/issues/{issue_id}/changes][%d] postRepositoriesUsernameRepoSlugIssuesIssueIdChangesNotFound  %+v", 404, o.Payload)
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDChangesNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
