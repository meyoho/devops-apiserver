// Code generated by go-swagger; DO NOT EDIT.

package issue_tracker

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/models"
)

// PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsReader is a Reader for the PostRepositoriesUsernameRepoSlugIssuesIssueIDComments structure.
type PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 400:
		result := NewPostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsCreated creates a PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsCreated with default headers values
func NewPostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsCreated() *PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsCreated {
	return &PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsCreated{}
}

/*PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsCreated handles this case with default header values.

The newly created comment.
*/
type PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsCreated struct {
	/*The location of the newly issue comment.
	 */
	Location string
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsCreated) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}/issues/{issue_id}/comments][%d] postRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCreated ", 201)
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header Location
	o.Location = response.GetHeader("Location")

	return nil
}

// NewPostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsBadRequest creates a PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsBadRequest with default headers values
func NewPostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsBadRequest() *PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsBadRequest {
	return &PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsBadRequest{}
}

/*PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsBadRequest handles this case with default header values.

If the input was invalid, or if the comment being created is detected as spam
*/
type PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsBadRequest struct {
	Payload *models.Error
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsBadRequest) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}/issues/{issue_id}/comments][%d] postRepositoriesUsernameRepoSlugIssuesIssueIdCommentsBadRequest  %+v", 400, o.Payload)
}

func (o *PostRepositoriesUsernameRepoSlugIssuesIssueIDCommentsBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
