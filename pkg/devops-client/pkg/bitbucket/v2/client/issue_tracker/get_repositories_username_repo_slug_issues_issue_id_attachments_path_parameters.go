// Code generated by go-swagger; DO NOT EDIT.

package issue_tracker

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams creates a new GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams object
// with the default values initialized.
func NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams() *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParamsWithTimeout creates a new GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParamsWithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams{

		timeout: timeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParamsWithContext creates a new GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParamsWithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams{

		Context: ctx,
	}
}

// NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParamsWithHTTPClient creates a new GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParamsWithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams{
		HTTPClient: client,
	}
}

/*GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams contains all the parameters to send to the API endpoint
for the get repositories username repo slug issues issue ID attachments path operation typically these are written to a http.Request
*/
type GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams struct {

	/*IssueID*/
	IssueID string
	/*Path*/
	Path string
	/*RepoSlug*/
	RepoSlug string
	/*Username*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) WithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) WithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) WithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithIssueID adds the issueID to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) WithIssueID(issueID string) *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams {
	o.SetIssueID(issueID)
	return o
}

// SetIssueID adds the issueId to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) SetIssueID(issueID string) {
	o.IssueID = issueID
}

// WithPath adds the path to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) WithPath(path string) *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams {
	o.SetPath(path)
	return o
}

// SetPath adds the path to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) SetPath(path string) {
	o.Path = path
}

// WithRepoSlug adds the repoSlug to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) WithRepoSlug(repoSlug string) *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithUsername adds the username to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) WithUsername(username string) *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get repositories username repo slug issues issue ID attachments path params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param issue_id
	if err := r.SetPathParam("issue_id", o.IssueID); err != nil {
		return err
	}

	// path param path
	if err := r.SetPathParam("path", o.Path); err != nil {
		return err
	}

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
