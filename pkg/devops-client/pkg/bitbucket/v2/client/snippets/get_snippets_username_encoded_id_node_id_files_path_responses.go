// Code generated by go-swagger; DO NOT EDIT.

package snippets

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/models"
)

// GetSnippetsUsernameEncodedIDNodeIDFilesPathReader is a Reader for the GetSnippetsUsernameEncodedIDNodeIDFilesPath structure.
type GetSnippetsUsernameEncodedIDNodeIDFilesPathReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetSnippetsUsernameEncodedIDNodeIDFilesPathReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetSnippetsUsernameEncodedIDNodeIDFilesPathOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewGetSnippetsUsernameEncodedIDNodeIDFilesPathForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewGetSnippetsUsernameEncodedIDNodeIDFilesPathNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetSnippetsUsernameEncodedIDNodeIDFilesPathOK creates a GetSnippetsUsernameEncodedIDNodeIDFilesPathOK with default headers values
func NewGetSnippetsUsernameEncodedIDNodeIDFilesPathOK() *GetSnippetsUsernameEncodedIDNodeIDFilesPathOK {
	return &GetSnippetsUsernameEncodedIDNodeIDFilesPathOK{}
}

/*GetSnippetsUsernameEncodedIDNodeIDFilesPathOK handles this case with default header values.

Returns the contents of the specified file.
*/
type GetSnippetsUsernameEncodedIDNodeIDFilesPathOK struct {
	/*attachment
	 */
	ContentDisposition string
	/*The mime type as derived from the filename
	 */
	ContentType string
}

func (o *GetSnippetsUsernameEncodedIDNodeIDFilesPathOK) Error() string {
	return fmt.Sprintf("[GET /snippets/{username}/{encoded_id}/{node_id}/files/{path}][%d] getSnippetsUsernameEncodedIdNodeIdFilesPathOK ", 200)
}

func (o *GetSnippetsUsernameEncodedIDNodeIDFilesPathOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header Content-Disposition
	o.ContentDisposition = response.GetHeader("Content-Disposition")

	// response header Content-Type
	o.ContentType = response.GetHeader("Content-Type")

	return nil
}

// NewGetSnippetsUsernameEncodedIDNodeIDFilesPathForbidden creates a GetSnippetsUsernameEncodedIDNodeIDFilesPathForbidden with default headers values
func NewGetSnippetsUsernameEncodedIDNodeIDFilesPathForbidden() *GetSnippetsUsernameEncodedIDNodeIDFilesPathForbidden {
	return &GetSnippetsUsernameEncodedIDNodeIDFilesPathForbidden{}
}

/*GetSnippetsUsernameEncodedIDNodeIDFilesPathForbidden handles this case with default header values.

If the authenticated user does not have access to the snippet.
*/
type GetSnippetsUsernameEncodedIDNodeIDFilesPathForbidden struct {
	Payload *models.Error
}

func (o *GetSnippetsUsernameEncodedIDNodeIDFilesPathForbidden) Error() string {
	return fmt.Sprintf("[GET /snippets/{username}/{encoded_id}/{node_id}/files/{path}][%d] getSnippetsUsernameEncodedIdNodeIdFilesPathForbidden  %+v", 403, o.Payload)
}

func (o *GetSnippetsUsernameEncodedIDNodeIDFilesPathForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetSnippetsUsernameEncodedIDNodeIDFilesPathNotFound creates a GetSnippetsUsernameEncodedIDNodeIDFilesPathNotFound with default headers values
func NewGetSnippetsUsernameEncodedIDNodeIDFilesPathNotFound() *GetSnippetsUsernameEncodedIDNodeIDFilesPathNotFound {
	return &GetSnippetsUsernameEncodedIDNodeIDFilesPathNotFound{}
}

/*GetSnippetsUsernameEncodedIDNodeIDFilesPathNotFound handles this case with default header values.

If the file or snippet does not exist.
*/
type GetSnippetsUsernameEncodedIDNodeIDFilesPathNotFound struct {
	Payload *models.Error
}

func (o *GetSnippetsUsernameEncodedIDNodeIDFilesPathNotFound) Error() string {
	return fmt.Sprintf("[GET /snippets/{username}/{encoded_id}/{node_id}/files/{path}][%d] getSnippetsUsernameEncodedIdNodeIdFilesPathNotFound  %+v", 404, o.Payload)
}

func (o *GetSnippetsUsernameEncodedIDNodeIDFilesPathNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
