// Code generated by go-swagger; DO NOT EDIT.

package deploy

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetRepositoriesUsernameRepoSlugDeployKeysParams creates a new GetRepositoriesUsernameRepoSlugDeployKeysParams object
// with the default values initialized.
func NewGetRepositoriesUsernameRepoSlugDeployKeysParams() *GetRepositoriesUsernameRepoSlugDeployKeysParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugDeployKeysParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugDeployKeysParamsWithTimeout creates a new GetRepositoriesUsernameRepoSlugDeployKeysParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetRepositoriesUsernameRepoSlugDeployKeysParamsWithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugDeployKeysParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugDeployKeysParams{

		timeout: timeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugDeployKeysParamsWithContext creates a new GetRepositoriesUsernameRepoSlugDeployKeysParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetRepositoriesUsernameRepoSlugDeployKeysParamsWithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugDeployKeysParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugDeployKeysParams{

		Context: ctx,
	}
}

// NewGetRepositoriesUsernameRepoSlugDeployKeysParamsWithHTTPClient creates a new GetRepositoriesUsernameRepoSlugDeployKeysParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetRepositoriesUsernameRepoSlugDeployKeysParamsWithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugDeployKeysParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugDeployKeysParams{
		HTTPClient: client,
	}
}

/*GetRepositoriesUsernameRepoSlugDeployKeysParams contains all the parameters to send to the API endpoint
for the get repositories username repo slug deploy keys operation typically these are written to a http.Request
*/
type GetRepositoriesUsernameRepoSlugDeployKeysParams struct {

	/*RepoSlug
	  This can either be the repository slug or the UUID of the repository,
	surrounded by curly-braces, for example: `{repository UUID}`.


	*/
	RepoSlug string
	/*Username
	  This can either be the username or the UUID of the account,
	surrounded by curly-braces, for example: `{account UUID}`. An account
	is either a team or user.


	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repositories username repo slug deploy keys params
func (o *GetRepositoriesUsernameRepoSlugDeployKeysParams) WithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugDeployKeysParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repositories username repo slug deploy keys params
func (o *GetRepositoriesUsernameRepoSlugDeployKeysParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repositories username repo slug deploy keys params
func (o *GetRepositoriesUsernameRepoSlugDeployKeysParams) WithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugDeployKeysParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repositories username repo slug deploy keys params
func (o *GetRepositoriesUsernameRepoSlugDeployKeysParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repositories username repo slug deploy keys params
func (o *GetRepositoriesUsernameRepoSlugDeployKeysParams) WithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugDeployKeysParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repositories username repo slug deploy keys params
func (o *GetRepositoriesUsernameRepoSlugDeployKeysParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithRepoSlug adds the repoSlug to the get repositories username repo slug deploy keys params
func (o *GetRepositoriesUsernameRepoSlugDeployKeysParams) WithRepoSlug(repoSlug string) *GetRepositoriesUsernameRepoSlugDeployKeysParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the get repositories username repo slug deploy keys params
func (o *GetRepositoriesUsernameRepoSlugDeployKeysParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithUsername adds the username to the get repositories username repo slug deploy keys params
func (o *GetRepositoriesUsernameRepoSlugDeployKeysParams) WithUsername(username string) *GetRepositoriesUsernameRepoSlugDeployKeysParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get repositories username repo slug deploy keys params
func (o *GetRepositoriesUsernameRepoSlugDeployKeysParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetRepositoriesUsernameRepoSlugDeployKeysParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
