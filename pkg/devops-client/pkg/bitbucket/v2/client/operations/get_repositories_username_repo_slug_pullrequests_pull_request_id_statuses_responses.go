// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/models"
)

// GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesReader is a Reader for the GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatuses structure.
type GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesOK creates a GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesOK with default headers values
func NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesOK() *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesOK {
	return &GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesOK{}
}

/*GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesOK handles this case with default header values.

A paginated list of all commit statuses for this pull request.
*/
type GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesOK struct {
	Payload *models.PaginatedCommitstatuses
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesOK) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/statuses][%d] getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesOK  %+v", 200, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.PaginatedCommitstatuses)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesUnauthorized creates a GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesUnauthorized with default headers values
func NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesUnauthorized() *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesUnauthorized {
	return &GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesUnauthorized{}
}

/*GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesUnauthorized handles this case with default header values.

If the repository is private and the request was not authenticated.
*/
type GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesUnauthorized struct {
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesUnauthorized) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/statuses][%d] getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesUnauthorized ", 401)
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesNotFound creates a GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesNotFound with default headers values
func NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesNotFound() *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesNotFound {
	return &GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesNotFound{}
}

/*GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesNotFound handles this case with default header values.

If the specified repository or pull request does not exist.
*/
type GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesNotFound struct {
	Payload *models.Error
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesNotFound) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/statuses][%d] getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatusesNotFound  %+v", 404, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDStatusesNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
