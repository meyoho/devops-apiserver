// Code generated by go-swagger; DO NOT EDIT.

package refs

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/models"
)

// PostRepositoriesUsernameRepoSlugRefsBranchesReader is a Reader for the PostRepositoriesUsernameRepoSlugRefsBranches structure.
type PostRepositoriesUsernameRepoSlugRefsBranchesReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostRepositoriesUsernameRepoSlugRefsBranchesReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostRepositoriesUsernameRepoSlugRefsBranchesCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewPostRepositoriesUsernameRepoSlugRefsBranchesForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewPostRepositoriesUsernameRepoSlugRefsBranchesNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostRepositoriesUsernameRepoSlugRefsBranchesCreated creates a PostRepositoriesUsernameRepoSlugRefsBranchesCreated with default headers values
func NewPostRepositoriesUsernameRepoSlugRefsBranchesCreated() *PostRepositoriesUsernameRepoSlugRefsBranchesCreated {
	return &PostRepositoriesUsernameRepoSlugRefsBranchesCreated{}
}

/*PostRepositoriesUsernameRepoSlugRefsBranchesCreated handles this case with default header values.

The newly created branch object.
*/
type PostRepositoriesUsernameRepoSlugRefsBranchesCreated struct {
	Payload *models.Branch
}

func (o *PostRepositoriesUsernameRepoSlugRefsBranchesCreated) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}/refs/branches][%d] postRepositoriesUsernameRepoSlugRefsBranchesCreated  %+v", 201, o.Payload)
}

func (o *PostRepositoriesUsernameRepoSlugRefsBranchesCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Branch)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPostRepositoriesUsernameRepoSlugRefsBranchesForbidden creates a PostRepositoriesUsernameRepoSlugRefsBranchesForbidden with default headers values
func NewPostRepositoriesUsernameRepoSlugRefsBranchesForbidden() *PostRepositoriesUsernameRepoSlugRefsBranchesForbidden {
	return &PostRepositoriesUsernameRepoSlugRefsBranchesForbidden{}
}

/*PostRepositoriesUsernameRepoSlugRefsBranchesForbidden handles this case with default header values.

If the repository is private and the authenticated user does not have
access to it.

*/
type PostRepositoriesUsernameRepoSlugRefsBranchesForbidden struct {
	Payload *models.Error
}

func (o *PostRepositoriesUsernameRepoSlugRefsBranchesForbidden) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}/refs/branches][%d] postRepositoriesUsernameRepoSlugRefsBranchesForbidden  %+v", 403, o.Payload)
}

func (o *PostRepositoriesUsernameRepoSlugRefsBranchesForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPostRepositoriesUsernameRepoSlugRefsBranchesNotFound creates a PostRepositoriesUsernameRepoSlugRefsBranchesNotFound with default headers values
func NewPostRepositoriesUsernameRepoSlugRefsBranchesNotFound() *PostRepositoriesUsernameRepoSlugRefsBranchesNotFound {
	return &PostRepositoriesUsernameRepoSlugRefsBranchesNotFound{}
}

/*PostRepositoriesUsernameRepoSlugRefsBranchesNotFound handles this case with default header values.

The specified repository or branch does not exist.
*/
type PostRepositoriesUsernameRepoSlugRefsBranchesNotFound struct {
	Payload *models.Error
}

func (o *PostRepositoriesUsernameRepoSlugRefsBranchesNotFound) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}/refs/branches][%d] postRepositoriesUsernameRepoSlugRefsBranchesNotFound  %+v", 404, o.Payload)
}

func (o *PostRepositoriesUsernameRepoSlugRefsBranchesNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
