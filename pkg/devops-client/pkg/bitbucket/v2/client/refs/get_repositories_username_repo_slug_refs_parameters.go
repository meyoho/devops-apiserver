// Code generated by go-swagger; DO NOT EDIT.

package refs

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetRepositoriesUsernameRepoSlugRefsParams creates a new GetRepositoriesUsernameRepoSlugRefsParams object
// with the default values initialized.
func NewGetRepositoriesUsernameRepoSlugRefsParams() *GetRepositoriesUsernameRepoSlugRefsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugRefsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugRefsParamsWithTimeout creates a new GetRepositoriesUsernameRepoSlugRefsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetRepositoriesUsernameRepoSlugRefsParamsWithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugRefsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugRefsParams{

		timeout: timeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugRefsParamsWithContext creates a new GetRepositoriesUsernameRepoSlugRefsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetRepositoriesUsernameRepoSlugRefsParamsWithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugRefsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugRefsParams{

		Context: ctx,
	}
}

// NewGetRepositoriesUsernameRepoSlugRefsParamsWithHTTPClient creates a new GetRepositoriesUsernameRepoSlugRefsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetRepositoriesUsernameRepoSlugRefsParamsWithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugRefsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugRefsParams{
		HTTPClient: client,
	}
}

/*GetRepositoriesUsernameRepoSlugRefsParams contains all the parameters to send to the API endpoint
for the get repositories username repo slug refs operation typically these are written to a http.Request
*/
type GetRepositoriesUsernameRepoSlugRefsParams struct {

	/*Q

	Query string to narrow down the response as per
	[filtering and sorting](../../../../../../meta/filtering).

	*/
	Q *string
	/*RepoSlug

	This can either be the repository slug or the UUID of the repository,
	surrounded by curly-braces, for example: `{repository UUID}`.


	*/
	RepoSlug string
	/*Sort

	Field by which the results should be sorted as per
	[filtering and sorting](../../../../../../meta/filtering). The `name`
	field is handled specially for refs in that, if specified as the sort field, it
	uses a natural sort order instead of the default lexicographical sort order. For example,
	it will return ['1.1', '1.2', '1.10'] instead of ['1.1', '1.10', '1.2'].

	*/
	Sort *string
	/*Username

	This can either be the username or the UUID of the user,
	surrounded by curly-braces, for example: `{user UUID}`.


	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) WithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugRefsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) WithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugRefsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) WithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugRefsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithQ adds the q to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) WithQ(q *string) *GetRepositoriesUsernameRepoSlugRefsParams {
	o.SetQ(q)
	return o
}

// SetQ adds the q to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) SetQ(q *string) {
	o.Q = q
}

// WithRepoSlug adds the repoSlug to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) WithRepoSlug(repoSlug string) *GetRepositoriesUsernameRepoSlugRefsParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithSort adds the sort to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) WithSort(sort *string) *GetRepositoriesUsernameRepoSlugRefsParams {
	o.SetSort(sort)
	return o
}

// SetSort adds the sort to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) SetSort(sort *string) {
	o.Sort = sort
}

// WithUsername adds the username to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) WithUsername(username string) *GetRepositoriesUsernameRepoSlugRefsParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get repositories username repo slug refs params
func (o *GetRepositoriesUsernameRepoSlugRefsParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetRepositoriesUsernameRepoSlugRefsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Q != nil {

		// query param q
		var qrQ string
		if o.Q != nil {
			qrQ = *o.Q
		}
		qQ := qrQ
		if qQ != "" {
			if err := r.SetQueryParam("q", qQ); err != nil {
				return err
			}
		}

	}

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	if o.Sort != nil {

		// query param sort
		var qrSort string
		if o.Sort != nil {
			qrSort = *o.Sort
		}
		qSort := qrSort
		if qSort != "" {
			if err := r.SetQueryParam("sort", qSort); err != nil {
				return err
			}
		}

	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
