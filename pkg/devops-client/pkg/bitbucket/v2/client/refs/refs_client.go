// Code generated by go-swagger; DO NOT EDIT.

package refs

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new refs API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for refs API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
DeleteRepositoriesUsernameRepoSlugRefsBranchesName Delete a branch in the specified repository.

The main branch is not allowed to be deleted and will return a 400
response.

For Git, the branch name should not include any prefixes (e.g.
refs/heads). For Mercurial, this closes all open heads on the branch,
sets the author of the commit to the authenticated caller, and changes
the date to the datetime of the call.
*/
func (a *Client) DeleteRepositoriesUsernameRepoSlugRefsBranchesName(params *DeleteRepositoriesUsernameRepoSlugRefsBranchesNameParams, authInfo runtime.ClientAuthInfoWriter) (*DeleteRepositoriesUsernameRepoSlugRefsBranchesNameNoContent, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewDeleteRepositoriesUsernameRepoSlugRefsBranchesNameParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "DeleteRepositoriesUsernameRepoSlugRefsBranchesName",
		Method:             "DELETE",
		PathPattern:        "/repositories/{username}/{repo_slug}/refs/branches/{name}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &DeleteRepositoriesUsernameRepoSlugRefsBranchesNameReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*DeleteRepositoriesUsernameRepoSlugRefsBranchesNameNoContent), nil

}

/*
DeleteRepositoriesUsernameRepoSlugRefsTagsName Delete a tag in the specified repository.

For Git, the tag name should not include any prefixes (e.g. refs/tags).
For Mercurial, this adds a commit to the main branch that removes the
specified tag.
*/
func (a *Client) DeleteRepositoriesUsernameRepoSlugRefsTagsName(params *DeleteRepositoriesUsernameRepoSlugRefsTagsNameParams, authInfo runtime.ClientAuthInfoWriter) (*DeleteRepositoriesUsernameRepoSlugRefsTagsNameNoContent, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewDeleteRepositoriesUsernameRepoSlugRefsTagsNameParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "DeleteRepositoriesUsernameRepoSlugRefsTagsName",
		Method:             "DELETE",
		PathPattern:        "/repositories/{username}/{repo_slug}/refs/tags/{name}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &DeleteRepositoriesUsernameRepoSlugRefsTagsNameReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*DeleteRepositoriesUsernameRepoSlugRefsTagsNameNoContent), nil

}

/*
GetRepositoriesUsernameRepoSlugRefs Returns the branches and tags in the repository.

By default, results will be in the order the underlying source control system returns them and identical to
the ordering one sees when running "$ git show-ref". Note that this follows simple
lexical ordering of the ref names.

This can be undesirable as it does apply any natural sorting semantics, meaning for instance that refs are
sorted ["branch1", "branch10", "branch2", "v10", "v11", "v9"] instead of ["branch1", "branch2",
"branch10", "v9", "v10", "v11"].

Sorting can be changed using the ?sort= query parameter. When using ?sort=name to explicitly sort on ref name,
Bitbucket will apply natural sorting and interpret numerical values as numbers instead of strings.
*/
func (a *Client) GetRepositoriesUsernameRepoSlugRefs(params *GetRepositoriesUsernameRepoSlugRefsParams, authInfo runtime.ClientAuthInfoWriter) (*GetRepositoriesUsernameRepoSlugRefsOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetRepositoriesUsernameRepoSlugRefsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "GetRepositoriesUsernameRepoSlugRefs",
		Method:             "GET",
		PathPattern:        "/repositories/{username}/{repo_slug}/refs",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &GetRepositoriesUsernameRepoSlugRefsReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetRepositoriesUsernameRepoSlugRefsOK), nil

}

/*
GetRepositoriesUsernameRepoSlugRefsBranches Returns a list of all open branches within the specified repository.
Results will be in the order the source control manager returns them.

```
$ curl -s https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/refs/branches | jq .
{
  "pagelen": 10,
  "values": [
    {
      "heads": [
        {
          "hash": "f1a0933ce59e809f190602655e22ae6ec107c397",
          "type": "commit",
          "links": {
            "self": {
              "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/f1a0933ce59e809f190602655e22ae6ec107c397"
            },
            "html": {
              "href": "https://bitbucket.org/seanfarley/mercurial/commits/f1a0933ce59e809f190602655e22ae6ec107c397"
            }
          }
        }
      ],
      "type": "named_branch",
      "name": "default",
      "links": {
        "commits": {
          "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commits/default"
        },
        "self": {
          "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/refs/branches/default"
        },
        "html": {
          "href": "https://bitbucket.org/seanfarley/mercurial/branch/default"
        }
      },
      "target": {
        "hash": "f1a0933ce59e809f190602655e22ae6ec107c397",
        "repository": {
          "links": {
            "self": {
              "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial"
            },
            "html": {
              "href": "https://bitbucket.org/seanfarley/mercurial"
            },
            "avatar": {
              "href": "https://bitbucket.org/seanfarley/mercurial/avatar/32/"
            }
          },
          "type": "repository",
          "name": "mercurial",
          "full_name": "seanfarley/mercurial",
          "uuid": "{73dcbd61-e506-4fc1-9ecd-31be2b6d6031}"
        },
        "links": {
          "self": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/f1a0933ce59e809f190602655e22ae6ec107c397"
          },
          "comments": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/f1a0933ce59e809f190602655e22ae6ec107c397/comments"
          },
          "patch": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/patch/f1a0933ce59e809f190602655e22ae6ec107c397"
          },
          "html": {
            "href": "https://bitbucket.org/seanfarley/mercurial/commits/f1a0933ce59e809f190602655e22ae6ec107c397"
          },
          "diff": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/diff/f1a0933ce59e809f190602655e22ae6ec107c397"
          },
          "approve": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/f1a0933ce59e809f190602655e22ae6ec107c397/approve"
          },
          "statuses": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/f1a0933ce59e809f190602655e22ae6ec107c397/statuses"
          }
        },
        "author": {
          "raw": "Martin von Zweigbergk <martinvonz@google.com>",
          "type": "author",
          "user": {
            "username": "martinvonz",
            "nickname": "martinvonz",
            "display_name": "Martin von Zweigbergk",
            "type": "user",
            "uuid": "{fdb0e657-3ade-4fad-a136-95f1ffe4a5ac}",
            "links": {
              "self": {
                "href": "https://api.bitbucket.org/2.0/users/martinvonz"
              },
              "html": {
                "href": "https://bitbucket.org/martinvonz/"
              },
              "avatar": {
                "href": "https://bitbucket.org/account/martinvonz/avatar/32/"
              }
            }
          }
        },
        "parents": [
          {
            "hash": "5523aabb85c30ebc2b8e29aadcaf5e13fa92b375",
            "type": "commit",
            "links": {
              "self": {
                "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/5523aabb85c30ebc2b8e29aadcaf5e13fa92b375"
              },
              "html": {
                "href": "https://bitbucket.org/seanfarley/mercurial/commits/5523aabb85c30ebc2b8e29aadcaf5e13fa92b375"
              }
            }
          }
        ],
        "date": "2018-02-01T18:44:49+00:00",
        "message": "config: replace a for-else by any()",
        "type": "commit"
      }
    },
    {
      "heads": [
        {
          "hash": "1d60ad093792706e1dc7a52b20942593f2c19655",
          "type": "commit",
          "links": {
            "self": {
              "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/1d60ad093792706e1dc7a52b20942593f2c19655"
            },
            "html": {
              "href": "https://bitbucket.org/seanfarley/mercurial/commits/1d60ad093792706e1dc7a52b20942593f2c19655"
            }
          }
        }
      ],
      "type": "named_branch",
      "name": "stable",
      "links": {
        "commits": {
          "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commits/stable"
        },
        "self": {
          "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/refs/branches/stable"
        },
        "html": {
          "href": "https://bitbucket.org/seanfarley/mercurial/branch/stable"
        }
      },
      "target": {
        "hash": "1d60ad093792706e1dc7a52b20942593f2c19655",
        "repository": {
          "links": {
            "self": {
              "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial"
            },
            "html": {
              "href": "https://bitbucket.org/seanfarley/mercurial"
            },
            "avatar": {
              "href": "https://bitbucket.org/seanfarley/mercurial/avatar/32/"
            }
          },
          "type": "repository",
          "name": "mercurial",
          "full_name": "seanfarley/mercurial",
          "uuid": "{73dcbd61-e506-4fc1-9ecd-31be2b6d6031}"
        },
        "links": {
          "self": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/1d60ad093792706e1dc7a52b20942593f2c19655"
          },
          "comments": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/1d60ad093792706e1dc7a52b20942593f2c19655/comments"
          },
          "patch": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/patch/1d60ad093792706e1dc7a52b20942593f2c19655"
          },
          "html": {
            "href": "https://bitbucket.org/seanfarley/mercurial/commits/1d60ad093792706e1dc7a52b20942593f2c19655"
          },
          "diff": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/diff/1d60ad093792706e1dc7a52b20942593f2c19655"
          },
          "approve": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/1d60ad093792706e1dc7a52b20942593f2c19655/approve"
          },
          "statuses": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/1d60ad093792706e1dc7a52b20942593f2c19655/statuses"
          }
        },
        "author": {
          "raw": "Augie Fackler <raf@durin42.com>",
          "type": "author",
          "user": {
            "username": "durin42",
            "nickname": "durin42",
            "display_name": "Augie Fackler",
            "type": "user",
            "uuid": "{e07dc61f-bb05-4218-b43a-d991f26be65a}",
            "links": {
              "self": {
                "href": "https://api.bitbucket.org/2.0/users/durin42"
              },
              "html": {
                "href": "https://bitbucket.org/durin42/"
              },
              "avatar": {
                "href": "https://bitbucket.org/account/durin42/avatar/32/"
              }
            }
          }
        },
        "parents": [
          {
            "hash": "56a0da11bde519d79168e890df4bcf0da62f0a7b",
            "type": "commit",
            "links": {
              "self": {
                "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/56a0da11bde519d79168e890df4bcf0da62f0a7b"
              },
              "html": {
                "href": "https://bitbucket.org/seanfarley/mercurial/commits/56a0da11bde519d79168e890df4bcf0da62f0a7b"
              }
            }
          }
        ],
        "date": "2018-02-01T19:13:41+00:00",
        "message": "Added signature for changeset d334afc585e2",
        "type": "commit"
      }
    }
  ],
  "page": 1,
  "size": 2
}
```

Branches support [filtering and sorting](../../../../../meta/filtering)
that can be used to search for specific branches. For instance, to find
all branches that have "stab" in their name:

```
curl -s https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/refs/branches -G --data-urlencode 'q=name ~ "stab"'
```

By default, results will be in the order the underlying source control system returns them and identical to
the ordering one sees when running "$ hg branches" or "$ git branch --list". Note that this follows simple
lexical ordering of the ref names.

This can be undesirable as it does apply any natural sorting semantics, meaning for instance that tags are
sorted ["v10", "v11", "v9"] instead of ["v9", "v10", "v11"].

Sorting can be changed using the ?q= query parameter. When using ?q=name to explicitly sort on ref name,
Bitbucket will apply natural sorting and interpret numerical values as numbers instead of strings.
*/
func (a *Client) GetRepositoriesUsernameRepoSlugRefsBranches(params *GetRepositoriesUsernameRepoSlugRefsBranchesParams, authInfo runtime.ClientAuthInfoWriter) (*GetRepositoriesUsernameRepoSlugRefsBranchesOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetRepositoriesUsernameRepoSlugRefsBranchesParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "GetRepositoriesUsernameRepoSlugRefsBranches",
		Method:             "GET",
		PathPattern:        "/repositories/{username}/{repo_slug}/refs/branches",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &GetRepositoriesUsernameRepoSlugRefsBranchesReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetRepositoriesUsernameRepoSlugRefsBranchesOK), nil

}

/*
GetRepositoriesUsernameRepoSlugRefsBranchesName Returns a branch object within the specified repository.

```
$ curl -s https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/refs/branches/default | jq .
{
  "heads": [
    {
      "hash": "f1a0933ce59e809f190602655e22ae6ec107c397",
      "type": "commit",
      "links": {
        "self": {
          "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/f1a0933ce59e809f190602655e22ae6ec107c397"
        },
        "html": {
          "href": "https://bitbucket.org/seanfarley/mercurial/commits/f1a0933ce59e809f190602655e22ae6ec107c397"
        }
      }
    }
  ],
  "type": "named_branch",
  "name": "default",
  "links": {
    "commits": {
      "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commits/default"
    },
    "self": {
      "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/refs/branches/default"
    },
    "html": {
      "href": "https://bitbucket.org/seanfarley/mercurial/branch/default"
    }
  },
  "target": {
    "hash": "f1a0933ce59e809f190602655e22ae6ec107c397",
    "repository": {
      "links": {
        "self": {
          "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial"
        },
        "html": {
          "href": "https://bitbucket.org/seanfarley/mercurial"
        },
        "avatar": {
          "href": "https://bitbucket.org/seanfarley/mercurial/avatar/32/"
        }
      },
      "type": "repository",
      "name": "mercurial",
      "full_name": "seanfarley/mercurial",
      "uuid": "{73dcbd61-e506-4fc1-9ecd-31be2b6d6031}"
    },
    "links": {
      "self": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/f1a0933ce59e809f190602655e22ae6ec107c397"
      },
      "comments": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/f1a0933ce59e809f190602655e22ae6ec107c397/comments"
      },
      "patch": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/patch/f1a0933ce59e809f190602655e22ae6ec107c397"
      },
      "html": {
        "href": "https://bitbucket.org/seanfarley/mercurial/commits/f1a0933ce59e809f190602655e22ae6ec107c397"
      },
      "diff": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/diff/f1a0933ce59e809f190602655e22ae6ec107c397"
      },
      "approve": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/f1a0933ce59e809f190602655e22ae6ec107c397/approve"
      },
      "statuses": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/f1a0933ce59e809f190602655e22ae6ec107c397/statuses"
      }
    },
    "author": {
      "raw": "Martin von Zweigbergk <martinvonz@google.com>",
      "type": "author",
      "user": {
        "username": "martinvonz",
        "nickname": "martinvonz",
        "display_name": "Martin von Zweigbergk",
        "type": "user",
        "uuid": "{fdb0e657-3ade-4fad-a136-95f1ffe4a5ac}",
        "links": {
          "self": {
            "href": "https://api.bitbucket.org/2.0/users/martinvonz"
          },
          "html": {
            "href": "https://bitbucket.org/martinvonz/"
          },
          "avatar": {
            "href": "https://bitbucket.org/account/martinvonz/avatar/32/"
          }
        }
      }
    },
    "parents": [
      {
        "hash": "5523aabb85c30ebc2b8e29aadcaf5e13fa92b375",
        "type": "commit",
        "links": {
          "self": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/mercurial/commit/5523aabb85c30ebc2b8e29aadcaf5e13fa92b375"
          },
          "html": {
            "href": "https://bitbucket.org/seanfarley/mercurial/commits/5523aabb85c30ebc2b8e29aadcaf5e13fa92b375"
          }
        }
      }
    ],
    "date": "2018-02-01T18:44:49+00:00",
    "message": "config: replace a for-else by any()",
    "type": "commit"
  }
}
```

This call requires authentication. Private repositories require the
caller to authenticate with an account that has appropriate
authorization.

For Git, the branch name should not include any prefixes (e.g.
refs/heads).

For Mercurial, the response will include an additional field that lists
the open heads.
*/
func (a *Client) GetRepositoriesUsernameRepoSlugRefsBranchesName(params *GetRepositoriesUsernameRepoSlugRefsBranchesNameParams, authInfo runtime.ClientAuthInfoWriter) (*GetRepositoriesUsernameRepoSlugRefsBranchesNameOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetRepositoriesUsernameRepoSlugRefsBranchesNameParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "GetRepositoriesUsernameRepoSlugRefsBranchesName",
		Method:             "GET",
		PathPattern:        "/repositories/{username}/{repo_slug}/refs/branches/{name}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &GetRepositoriesUsernameRepoSlugRefsBranchesNameReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetRepositoriesUsernameRepoSlugRefsBranchesNameOK), nil

}

/*
GetRepositoriesUsernameRepoSlugRefsTags Returns the tags in the repository.

By default, results will be in the order the underlying source control system returns them and identical to
the ordering one sees when running "$ hg tags" or "$ git tag --list". Note that this follows simple
lexical ordering of the ref names.

This can be undesirable as it does apply any natural sorting semantics, meaning for instance that tags are
sorted ["v10", "v11", "v9"] instead of ["v9", "v10", "v11"].

Sorting can be changed using the ?sort= query parameter. When using ?sort=name to explicitly sort on ref name,
Bitbucket will apply natural sorting and interpret numerical values as numbers instead of strings.
*/
func (a *Client) GetRepositoriesUsernameRepoSlugRefsTags(params *GetRepositoriesUsernameRepoSlugRefsTagsParams, authInfo runtime.ClientAuthInfoWriter) (*GetRepositoriesUsernameRepoSlugRefsTagsOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetRepositoriesUsernameRepoSlugRefsTagsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "GetRepositoriesUsernameRepoSlugRefsTags",
		Method:             "GET",
		PathPattern:        "/repositories/{username}/{repo_slug}/refs/tags",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &GetRepositoriesUsernameRepoSlugRefsTagsReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetRepositoriesUsernameRepoSlugRefsTagsOK), nil

}

/*
GetRepositoriesUsernameRepoSlugRefsTagsName Returns the specified tag.

```
$ curl -s https://api.bitbucket.org/2.0/repositories/seanfarley/hg/refs/tags/3.8 -G | jq .
{
  "name": "3.8",
  "links": {
    "commits": {
      "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/hg/commits/3.8"
    },
    "self": {
      "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/hg/refs/tags/3.8"
    },
    "html": {
      "href": "https://bitbucket.org/seanfarley/hg/commits/tag/3.8"
    }
  },
  "tagger": {
    "raw": "Matt Mackall <mpm@selenic.com>",
    "type": "author",
    "user": {
      "username": "mpmselenic",
      "nickname": "mpmselenic",
      "display_name": "Matt Mackall",
      "type": "user",
      "uuid": "{a4934530-db4c-419c-a478-9ab4964c2ee7}",
      "links": {
        "self": {
          "href": "https://api.bitbucket.org/2.0/users/mpmselenic"
        },
        "html": {
          "href": "https://bitbucket.org/mpmselenic/"
        },
        "avatar": {
          "href": "https://bitbucket.org/account/mpmselenic/avatar/32/"
        }
      }
    }
  },
  "date": "2016-05-01T18:52:25+00:00",
  "message": "Added tag 3.8 for changeset f85de28eae32",
  "type": "tag",
  "target": {
    "hash": "f85de28eae32e7d3064b1a1321309071bbaaa069",
    "repository": {
      "links": {
        "self": {
          "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/hg"
        },
        "html": {
          "href": "https://bitbucket.org/seanfarley/hg"
        },
        "avatar": {
          "href": "https://bitbucket.org/seanfarley/hg/avatar/32/"
        }
      },
      "type": "repository",
      "name": "hg",
      "full_name": "seanfarley/hg",
      "uuid": "{c75687fb-e99d-4579-9087-190dbd406d30}"
    },
    "links": {
      "self": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/hg/commit/f85de28eae32e7d3064b1a1321309071bbaaa069"
      },
      "comments": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/hg/commit/f85de28eae32e7d3064b1a1321309071bbaaa069/comments"
      },
      "patch": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/hg/patch/f85de28eae32e7d3064b1a1321309071bbaaa069"
      },
      "html": {
        "href": "https://bitbucket.org/seanfarley/hg/commits/f85de28eae32e7d3064b1a1321309071bbaaa069"
      },
      "diff": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/hg/diff/f85de28eae32e7d3064b1a1321309071bbaaa069"
      },
      "approve": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/hg/commit/f85de28eae32e7d3064b1a1321309071bbaaa069/approve"
      },
      "statuses": {
        "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/hg/commit/f85de28eae32e7d3064b1a1321309071bbaaa069/statuses"
      }
    },
    "author": {
      "raw": "Sean Farley <sean@farley.io>",
      "type": "author",
      "user": {
        "username": "seanfarley",
        "nickname": "seanfarley",
        "display_name": "Sean Farley",
        "type": "user",
        "uuid": "{a295f8a8-5876-4d43-89b5-3ad8c6c3c51d}",
        "links": {
          "self": {
            "href": "https://api.bitbucket.org/2.0/users/seanfarley"
          },
          "html": {
            "href": "https://bitbucket.org/seanfarley/"
          },
          "avatar": {
            "href": "https://bitbucket.org/account/seanfarley/avatar/32/"
          }
        }
      }
    },
    "parents": [
      {
        "hash": "9a98d0e5b07fc60887f9d3d34d9ac7d536f470d2",
        "type": "commit",
        "links": {
          "self": {
            "href": "https://api.bitbucket.org/2.0/repositories/seanfarley/hg/commit/9a98d0e5b07fc60887f9d3d34d9ac7d536f470d2"
          },
          "html": {
            "href": "https://bitbucket.org/seanfarley/hg/commits/9a98d0e5b07fc60887f9d3d34d9ac7d536f470d2"
          }
        }
      }
    ],
    "date": "2016-05-01T04:21:17+00:00",
    "message": "debian: alphabetize build deps",
    "type": "commit"
  }
}
```
*/
func (a *Client) GetRepositoriesUsernameRepoSlugRefsTagsName(params *GetRepositoriesUsernameRepoSlugRefsTagsNameParams, authInfo runtime.ClientAuthInfoWriter) (*GetRepositoriesUsernameRepoSlugRefsTagsNameOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetRepositoriesUsernameRepoSlugRefsTagsNameParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "GetRepositoriesUsernameRepoSlugRefsTagsName",
		Method:             "GET",
		PathPattern:        "/repositories/{username}/{repo_slug}/refs/tags/{name}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &GetRepositoriesUsernameRepoSlugRefsTagsNameReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetRepositoriesUsernameRepoSlugRefsTagsNameOK), nil

}

/*
PostRepositoriesUsernameRepoSlugRefsBranches Creates a new branch in the specified repository.

The payload of the POST should consist of a JSON document that
contains the name of the tag and the target hash.

```
curl https://api.bitbucket.org/2.0/repositories/seanfarley/hg/refs/branches \
-s -u seanfarley -X POST -H "Content-Type: application/json" \
-d '{
    "name" : "smf/create-feature",
    "target" : {
        "hash" : "default",
    }
}'
```

This call requires authentication. Private repositories require the
caller to authenticate with an account that has appropriate
authorization.

For Git, the branch name should not include any prefixes (e.g.
refs/heads). This endpoint does support using short hash prefixes for
the commit hash, but it may return a 400 response if the provided
prefix is ambiguous. Using a full commit hash is the preferred
approach.

For Mercurial, the authenticated user making this call is the author of
the new branch commit and the date is current datetime of the call.
*/
func (a *Client) PostRepositoriesUsernameRepoSlugRefsBranches(params *PostRepositoriesUsernameRepoSlugRefsBranchesParams, authInfo runtime.ClientAuthInfoWriter) (*PostRepositoriesUsernameRepoSlugRefsBranchesCreated, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPostRepositoriesUsernameRepoSlugRefsBranchesParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "PostRepositoriesUsernameRepoSlugRefsBranches",
		Method:             "POST",
		PathPattern:        "/repositories/{username}/{repo_slug}/refs/branches",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &PostRepositoriesUsernameRepoSlugRefsBranchesReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*PostRepositoriesUsernameRepoSlugRefsBranchesCreated), nil

}

/*
PostRepositoriesUsernameRepoSlugRefsTags Creates a new tag in the specified repository.

The payload of the POST should consist of a JSON document that
contains the name of the tag and the target hash.

```
curl https://api.bitbucket.org/2.0/repositories/jdoe/myrepo/refs/tags \
-s -u jdoe -X POST -H "Content-Type: application/json" \
-d '{
    "name" : "new-tag-name",
    "target" : {
        "hash" : "a1b2c3d4e5f6",
    }
}'
```

This endpoint does support using short hash prefixes for the commit
hash, but it may return a 400 response if the provided prefix is
ambiguous. Using a full commit hash is the preferred approach.
*/
func (a *Client) PostRepositoriesUsernameRepoSlugRefsTags(params *PostRepositoriesUsernameRepoSlugRefsTagsParams, authInfo runtime.ClientAuthInfoWriter) (*PostRepositoriesUsernameRepoSlugRefsTagsCreated, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPostRepositoriesUsernameRepoSlugRefsTagsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "PostRepositoriesUsernameRepoSlugRefsTags",
		Method:             "POST",
		PathPattern:        "/repositories/{username}/{repo_slug}/refs/tags",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &PostRepositoriesUsernameRepoSlugRefsTagsReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*PostRepositoriesUsernameRepoSlugRefsTagsCreated), nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
