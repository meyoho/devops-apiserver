// Code generated by go-swagger; DO NOT EDIT.

package refs

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/models"
)

// GetRepositoriesUsernameRepoSlugRefsTagsNameReader is a Reader for the GetRepositoriesUsernameRepoSlugRefsTagsName structure.
type GetRepositoriesUsernameRepoSlugRefsTagsNameReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetRepositoriesUsernameRepoSlugRefsTagsNameReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetRepositoriesUsernameRepoSlugRefsTagsNameOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewGetRepositoriesUsernameRepoSlugRefsTagsNameForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewGetRepositoriesUsernameRepoSlugRefsTagsNameNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetRepositoriesUsernameRepoSlugRefsTagsNameOK creates a GetRepositoriesUsernameRepoSlugRefsTagsNameOK with default headers values
func NewGetRepositoriesUsernameRepoSlugRefsTagsNameOK() *GetRepositoriesUsernameRepoSlugRefsTagsNameOK {
	return &GetRepositoriesUsernameRepoSlugRefsTagsNameOK{}
}

/*GetRepositoriesUsernameRepoSlugRefsTagsNameOK handles this case with default header values.

The tag object.
*/
type GetRepositoriesUsernameRepoSlugRefsTagsNameOK struct {
	Payload *models.Tag
}

func (o *GetRepositoriesUsernameRepoSlugRefsTagsNameOK) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/refs/tags/{name}][%d] getRepositoriesUsernameRepoSlugRefsTagsNameOK  %+v", 200, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugRefsTagsNameOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Tag)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetRepositoriesUsernameRepoSlugRefsTagsNameForbidden creates a GetRepositoriesUsernameRepoSlugRefsTagsNameForbidden with default headers values
func NewGetRepositoriesUsernameRepoSlugRefsTagsNameForbidden() *GetRepositoriesUsernameRepoSlugRefsTagsNameForbidden {
	return &GetRepositoriesUsernameRepoSlugRefsTagsNameForbidden{}
}

/*GetRepositoriesUsernameRepoSlugRefsTagsNameForbidden handles this case with default header values.

If the repository is private and the authenticated user does not have
access to it.

*/
type GetRepositoriesUsernameRepoSlugRefsTagsNameForbidden struct {
	Payload *models.Error
}

func (o *GetRepositoriesUsernameRepoSlugRefsTagsNameForbidden) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/refs/tags/{name}][%d] getRepositoriesUsernameRepoSlugRefsTagsNameForbidden  %+v", 403, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugRefsTagsNameForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetRepositoriesUsernameRepoSlugRefsTagsNameNotFound creates a GetRepositoriesUsernameRepoSlugRefsTagsNameNotFound with default headers values
func NewGetRepositoriesUsernameRepoSlugRefsTagsNameNotFound() *GetRepositoriesUsernameRepoSlugRefsTagsNameNotFound {
	return &GetRepositoriesUsernameRepoSlugRefsTagsNameNotFound{}
}

/*GetRepositoriesUsernameRepoSlugRefsTagsNameNotFound handles this case with default header values.

The specified repository or tag does not exist.
*/
type GetRepositoriesUsernameRepoSlugRefsTagsNameNotFound struct {
	Payload *models.Error
}

func (o *GetRepositoriesUsernameRepoSlugRefsTagsNameNotFound) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/refs/tags/{name}][%d] getRepositoriesUsernameRepoSlugRefsTagsNameNotFound  %+v", 404, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugRefsTagsNameNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
