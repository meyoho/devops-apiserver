// Code generated by go-swagger; DO NOT EDIT.

package addon

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new addon API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for addon API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
DeleteAddon delete addon API
*/
func (a *Client) DeleteAddon(params *DeleteAddonParams, authInfo runtime.ClientAuthInfoWriter) error {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewDeleteAddonParams()
	}

	_, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "DeleteAddon",
		Method:             "DELETE",
		PathPattern:        "/addon",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &DeleteAddonReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return err
	}
	return nil

}

/*
DeleteAddonLinkersLinkerKeyValues delete addon linkers linker key values API
*/
func (a *Client) DeleteAddonLinkersLinkerKeyValues(params *DeleteAddonLinkersLinkerKeyValuesParams, authInfo runtime.ClientAuthInfoWriter) error {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewDeleteAddonLinkersLinkerKeyValuesParams()
	}

	_, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "DeleteAddonLinkersLinkerKeyValues",
		Method:             "DELETE",
		PathPattern:        "/addon/linkers/{linker_key}/values/",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &DeleteAddonLinkersLinkerKeyValuesReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return err
	}
	return nil

}

/*
GetAddonLinkers get addon linkers API
*/
func (a *Client) GetAddonLinkers(params *GetAddonLinkersParams, authInfo runtime.ClientAuthInfoWriter) error {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetAddonLinkersParams()
	}

	_, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "GetAddonLinkers",
		Method:             "GET",
		PathPattern:        "/addon/linkers",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &GetAddonLinkersReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return err
	}
	return nil

}

/*
GetAddonLinkersLinkerKey get addon linkers linker key API
*/
func (a *Client) GetAddonLinkersLinkerKey(params *GetAddonLinkersLinkerKeyParams, authInfo runtime.ClientAuthInfoWriter) error {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetAddonLinkersLinkerKeyParams()
	}

	_, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "GetAddonLinkersLinkerKey",
		Method:             "GET",
		PathPattern:        "/addon/linkers/{linker_key}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &GetAddonLinkersLinkerKeyReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return err
	}
	return nil

}

/*
GetAddonLinkersLinkerKeyValues get addon linkers linker key values API
*/
func (a *Client) GetAddonLinkersLinkerKeyValues(params *GetAddonLinkersLinkerKeyValuesParams, authInfo runtime.ClientAuthInfoWriter) error {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetAddonLinkersLinkerKeyValuesParams()
	}

	_, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "GetAddonLinkersLinkerKeyValues",
		Method:             "GET",
		PathPattern:        "/addon/linkers/{linker_key}/values/",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &GetAddonLinkersLinkerKeyValuesReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return err
	}
	return nil

}

/*
PostAddonLinkersLinkerKeyValues post addon linkers linker key values API
*/
func (a *Client) PostAddonLinkersLinkerKeyValues(params *PostAddonLinkersLinkerKeyValuesParams, authInfo runtime.ClientAuthInfoWriter) error {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPostAddonLinkersLinkerKeyValuesParams()
	}

	_, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "PostAddonLinkersLinkerKeyValues",
		Method:             "POST",
		PathPattern:        "/addon/linkers/{linker_key}/values",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &PostAddonLinkersLinkerKeyValuesReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return err
	}
	return nil

}

/*
PostAddonUsersTargetUserEventsEventKey POST a new custom event.

The data within the event body will be hydrated by Bitbucket. For example, the following event
submission would result in subscribers for the event receiving the full repository object
corresponding to the UUID.

```
$ curl -X POST -H "Content-Type: application/json" -d '{
    "mynumdata": "12345",
    "repository": {
        "type": "repository",
        "uuid": "{be95aa1f-c0b2-47f6-99d1-bf5d3a0f850f}"
}}' https://api.bitbucket.org/2.0/addon/users/myuser/events/com.example.app%3Amyevent
```

Use the optional `fields` property of the custom event Connect
module where the event is defined to add additional
fields to the expanded payload sent to listeners.

For example, the `customEvents` module in the app descriptor for the previous example would look like this:

```
'modules': {
    'customEvents': {
        'com.example.app:myevent': {
            'schema': {
                'properties': {
                    'mynumdata': {'type': 'number'},
                    'repository': {'$ref': '#/definitions/repository'}
                }
            },
            'fields': ['repository.owner']
        }
    }
}
```

By specifying fields as above, the repository owner
will also be sent to subscribers of the event.
*/
func (a *Client) PostAddonUsersTargetUserEventsEventKey(params *PostAddonUsersTargetUserEventsEventKeyParams, authInfo runtime.ClientAuthInfoWriter) (*PostAddonUsersTargetUserEventsEventKeyNoContent, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPostAddonUsersTargetUserEventsEventKeyParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "PostAddonUsersTargetUserEventsEventKey",
		Method:             "POST",
		PathPattern:        "/addon/users/{target_user}/events/{event_key}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &PostAddonUsersTargetUserEventsEventKeyReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*PostAddonUsersTargetUserEventsEventKeyNoContent), nil

}

/*
PutAddon put addon API
*/
func (a *Client) PutAddon(params *PutAddonParams, authInfo runtime.ClientAuthInfoWriter) error {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPutAddonParams()
	}

	_, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "PutAddon",
		Method:             "PUT",
		PathPattern:        "/addon",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &PutAddonReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return err
	}
	return nil

}

/*
PutAddonLinkersLinkerKeyValues put addon linkers linker key values API
*/
func (a *Client) PutAddonLinkersLinkerKeyValues(params *PutAddonLinkersLinkerKeyValuesParams, authInfo runtime.ClientAuthInfoWriter) error {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPutAddonLinkersLinkerKeyValuesParams()
	}

	_, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "PutAddonLinkersLinkerKeyValues",
		Method:             "PUT",
		PathPattern:        "/addon/linkers/{linker_key}/values",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"https"},
		Params:             params,
		Reader:             &PutAddonLinkersLinkerKeyValuesReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return err
	}
	return nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
