// Code generated by go-swagger; DO NOT EDIT.

package pullrequests

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams creates a new PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams object
// with the default values initialized.
func NewPostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams() *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams {
	var ()
	return &PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParamsWithTimeout creates a new PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParamsWithTimeout(timeout time.Duration) *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams {
	var ()
	return &PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams{

		timeout: timeout,
	}
}

// NewPostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParamsWithContext creates a new PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParamsWithContext(ctx context.Context) *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams {
	var ()
	return &PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams{

		Context: ctx,
	}
}

// NewPostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParamsWithHTTPClient creates a new PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParamsWithHTTPClient(client *http.Client) *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams {
	var ()
	return &PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams{
		HTTPClient: client,
	}
}

/*PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams contains all the parameters to send to the API endpoint
for the post repositories username repo slug pullrequests pull request ID approve operation typically these are written to a http.Request
*/
type PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams struct {

	/*PullRequestID*/
	PullRequestID string
	/*RepoSlug*/
	RepoSlug string
	/*Username*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) WithTimeout(timeout time.Duration) *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) WithContext(ctx context.Context) *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) WithHTTPClient(client *http.Client) *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithPullRequestID adds the pullRequestID to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) WithPullRequestID(pullRequestID string) *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams {
	o.SetPullRequestID(pullRequestID)
	return o
}

// SetPullRequestID adds the pullRequestId to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) SetPullRequestID(pullRequestID string) {
	o.PullRequestID = pullRequestID
}

// WithRepoSlug adds the repoSlug to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) WithRepoSlug(repoSlug string) *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithUsername adds the username to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) WithUsername(username string) *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the post repositories username repo slug pullrequests pull request ID approve params
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *PostRepositoriesUsernameRepoSlugPullrequestsPullRequestIDApproveParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param pull_request_id
	if err := r.SetPathParam("pull_request_id", o.PullRequestID); err != nil {
		return err
	}

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
