// Code generated by go-swagger; DO NOT EDIT.

package pullrequests

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/bitbucket/v2/models"
)

// GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDReader is a Reader for the GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentID structure.
type GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDOK creates a GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDOK with default headers values
func NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDOK() *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDOK {
	return &GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDOK{}
}

/*GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDOK handles this case with default header values.

The comment.
*/
type GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDOK struct {
	Payload *models.PullrequestComment
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDOK) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/comments/{comment_id}][%d] getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdOK  %+v", 200, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.PullrequestComment)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDForbidden creates a GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDForbidden with default headers values
func NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDForbidden() *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDForbidden {
	return &GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDForbidden{}
}

/*GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDForbidden handles this case with default header values.

If the authenticated user does not have access to the pull request.
*/
type GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDForbidden struct {
	Payload *models.Error
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDForbidden) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/comments/{comment_id}][%d] getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdForbidden  %+v", 403, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDNotFound creates a GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDNotFound with default headers values
func NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDNotFound() *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDNotFound {
	return &GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDNotFound{}
}

/*GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDNotFound handles this case with default header values.

If the pull request does not exist.
*/
type GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDNotFound struct {
	Payload *models.Error
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDNotFound) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/pullrequests/{pull_request_id}/comments/{comment_id}][%d] getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentIdNotFound  %+v", 404, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
