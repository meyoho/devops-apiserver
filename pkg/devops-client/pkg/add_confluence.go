package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v6 "alauda.io/devops-apiserver/pkg/devops-client/pkg/confluence/v6"
)

func init() {
	register(devops.DocumentManageTypeConfluence.String(), versionOpt{
		version:   "v6",
		factory:   v6.NewClient(),
		isDefault: true,
	})
}
