// Code generated by go-swagger; DO NOT EDIT.

package repository

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gogs/v0_11_86/models"
)

// RepoListStargazersReader is a Reader for the RepoListStargazers structure.
type RepoListStargazersReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *RepoListStargazersReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewRepoListStargazersOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewRepoListStargazersOK creates a RepoListStargazersOK with default headers values
func NewRepoListStargazersOK() *RepoListStargazersOK {
	return &RepoListStargazersOK{}
}

/*RepoListStargazersOK handles this case with default header values.

UserList
*/
type RepoListStargazersOK struct {
	Payload []*models.User
}

func (o *RepoListStargazersOK) Error() string {
	return fmt.Sprintf("[GET /repos/{owner}/{repo}/stargazers][%d] repoListStargazersOK  %+v", 200, o.Payload)
}

func (o *RepoListStargazersOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
