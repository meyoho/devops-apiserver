package v1

import (
	"context"
	"errors"
	"io"
	"time"

	corev1 "k8s.io/api/core/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
)

var (
	// ErrNotImplement defines common error spec
	ErrNotImplement = errors.New("function not implement")
)

// NotImplement implements Interface
type NotImplement struct{}

var _ Interface = &NotImplement{}

// Available implements Interface
func (*NotImplement) Available(_ context.Context) (*devops.HostPortStatus, error) {
	return nil, ErrNotImplement
}

// Authenticate implements Inteface
func (*NotImplement) Authenticate(_ context.Context) (*devops.HostPortStatus, error) {
	return nil, ErrNotImplement
}

// GetRemoteRepos implements Inteface
func (*NotImplement) GetRemoteRepos(_ context.Context) (*devops.CodeRepoBindingRepositories, error) {
	return nil, ErrNotImplement
}

// GetBranches implements Inteface
func (*NotImplement) GetBranches(_ context.Context, owner, repo, repoFullName string) ([]devops.CodeRepoBranch, error) {
	return nil, ErrNotImplement
}

// GetLatestRepoCommit implements Inteface
func (*NotImplement) GetLatestRepoCommit(_ context.Context, repoID, owner, repoName, repoFullName string) (commit *devops.RepositoryCommit, status *devops.HostPortStatus) {
	return nil, nil
}

// CreateCodeRepoProject implements Inteface
func (*NotImplement) CreateCodeRepoProject(_ context.Context, _ devops.CreateProjectOptions) (*devops.ProjectData, error) {
	return nil, ErrNotImplement
}

// ListCodeRepoProjects implements Inteface
func (*NotImplement) ListCodeRepoProjects(_ context.Context, _ devops.ListProjectOptions) (*devops.ProjectDataList, error) {
	return nil, ErrNotImplement
}

// AccessToken implements Inteface
func (*NotImplement) AccessToken(_ context.Context, redirectUrl string) (secret *corev1.Secret, err error) {
	return nil, ErrNotImplement
}

// GetAuthorizeUrl implements Inteface
func (*NotImplement) GetAuthorizeUrl(_ context.Context, redirectUrl string) string {
	return ""
}

// GetImageRepos implements Inteface
func (*NotImplement) GetImageRepos(_ context.Context) (*devops.ImageRegistryBindingRepositories, error) {
	return nil, ErrNotImplement
}

// GetImageTags implements Inteface
func (*NotImplement) GetImageTags(_ context.Context, repository string) ([]devops.ImageTag, error) {
	return nil, ErrNotImplement
}

// TriggerScanImage implements Inteface
func (*NotImplement) TriggerScanImage(_ context.Context, repositoryName, tag string) *devops.ImageResult {
	return nil
}

// GetVulnerability implements Inteface
func (*NotImplement) GetVulnerability(_ context.Context, repositoryName, tag string) (result *devops.VulnerabilityList, err error) {
	return nil, ErrNotImplement
}

// GetImageProjects implements Inteface
func (*NotImplement) GetImageProjects(_ context.Context) (*devops.ProjectDataList, error) {
	return nil, ErrNotImplement
}

// CreateImageProject implements Inteface
func (*NotImplement) CreateImageProject(_ context.Context, name string) (*devops.ProjectData, error) {
	return nil, ErrNotImplement
}

// RepositoryAvailable implements Interface
func (*NotImplement) RepositoryAvailable(_ context.Context, repositoryName string, lastModifyAt time.Time) (*devops.HostPortStatus, error) {
	return nil, ErrNotImplement
}

// GetImageRepoLink implements Interface
func (c *NotImplement) GetImageRepoLink(_ context.Context, name string) string {
	return ""
}

// UpdateHarborConfig implements Interface
func (*NotImplement) UpdateHarborConfig(ctx context.Context, configJSON string) (err error) {
	return ErrNotImplement
}

// GetHarborConfig implements Inteface
func (*NotImplement) GetHarborConfig(ctx context.Context) (config *Configurations, err error) {
	return nil, ErrNotImplement
}

// GetProjects implements Inteface
func (*NotImplement) GetProjects(_ context.Context, page, pagesize string) (*devops.ProjectDataList, error) {
	return nil, ErrNotImplement
}

// CreateProject implements Inteface
func (*NotImplement) CreateProject(_ context.Context, projectName, projectDescription, projectLead, projectKey string) (*devops.ProjectData, error) {
	return nil, ErrNotImplement
}

// GetProjectReport implements Inteface
func (*NotImplement) GetProjectReport(_ context.Context, projectKey string) ([]devops.CodeQualityCondition, error) {
	return nil, ErrNotImplement
}

// GetCorrespondCodeQualityProjects implements Inteface
func (*NotImplement) GetCorrespondCodeQualityProjects(_ context.Context, repositoryList *devops.CodeRepositoryList, binding *devops.CodeQualityBinding) ([]devops.CodeQualityProject, error) {
	return nil, ErrNotImplement
}

// CheckProjectExist implements Inteface
func (*NotImplement) CheckProjectExist(_ context.Context, projectKey string) (bool, error) {
	return false, ErrNotImplement
}

// GetLastAnalysisDate implements Inteface
func (*NotImplement) GetLastAnalysisDate(_ context.Context, projectKey string) (*time.Time, error) {
	return nil, ErrNotImplement
}

// Init implements Inteface
func (*NotImplement) Init(_ context.Context) []error {
	return []error{ErrNotImplement}
}

// ListRegistry implements Inteface
func (*NotImplement) ListRegistry(_ context.Context) (devops.ArtifactRegistryList, error) {
	return devops.ArtifactRegistryList{}, ErrNotImplement
}

// CreateRegistry implements Inteface
func (*NotImplement) CreateRegistry(_ context.Context, artifactType string, v map[string]string) error {
	return ErrNotImplement
}

// ListBlobStore implements Inteface
func (*NotImplement) ListBlobStore(_ context.Context) (devops.BlobStoreOptionList, error) {
	return devops.BlobStoreOptionList{}, ErrNotImplement
}

func (*NotImplement) GetJobProgressiveLog(ctx context.Context, pipelineNamespace, pipelineName string, buildNumber, start int64) (*devops.PipelineLog, error) {
	return &devops.PipelineLog{}, ErrNotImplement
}
func (*NotImplement) GetBranchJobProgressiveLog(ctx context.Context, pipelineNamespace, pipelineName, branchName string, buildNumber, start int64) (*devops.PipelineLog, error) {
	return &devops.PipelineLog{}, ErrNotImplement
}
func (*NotImplement) GetJobStepLog(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId, stepId string, start int64) (*devops.PipelineLog, error) {
	return &devops.PipelineLog{}, ErrNotImplement
}
func (*NotImplement) GetBranchJobStepLog(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, stepId string, start int64) (*devops.PipelineLog, error) {
	return &devops.PipelineLog{}, ErrNotImplement
}
func (*NotImplement) GetJobNodeLog(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId string, start int64) (*devops.PipelineLog, error) {
	return &devops.PipelineLog{}, ErrNotImplement
}
func (*NotImplement) GetBranchJobNodeLog(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string, start int64) (*devops.PipelineLog, error) {
	return &devops.PipelineLog{}, ErrNotImplement
}
func (*NotImplement) GetBranchProgressiveScanLog(ctx context.Context, pipelineNamespace, pipelineName string, start int64) (*devops.PipelineConfigLog, error) {
	return &devops.PipelineConfigLog{}, ErrNotImplement
}
func (*NotImplement) ScanMultiBranchJob(ctx context.Context, pipelineNamespace, pipelineName string) error {
	return ErrNotImplement
}
func (*NotImplement) GetJobNodes(ctx context.Context, pipelineNamespace, pipelineName, organization, runId string) ([]devops.PipelineBlueOceanTask, error) {
	return nil, ErrNotImplement
}
func (*NotImplement) GetBranchJobNodes(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId string) ([]devops.PipelineBlueOceanTask, error) {
	return nil, ErrNotImplement
}
func (*NotImplement) GetJobSteps(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId string) ([]devops.PipelineBlueOceanTask, error) {
	return nil, ErrNotImplement
}
func (*NotImplement) GetBranchJobSteps(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string) ([]devops.PipelineBlueOceanTask, error) {
	return nil, ErrNotImplement
}

func (*NotImplement) GetTestReportSummary(ctx context.Context, pipelineNamespace, pipelineName, organization, runId string) (*devops.PipelineTestReportSummary, error) {
	return nil, ErrNotImplement
}

func (*NotImplement) GetBranchTestReportSummary(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId string) (*devops.PipelineTestReportSummary, error) {
	return nil, ErrNotImplement
}

func (*NotImplement) GetTestReports(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, status, state, stateBang string, start, limit int64) (*devops.PipelineTestReport, error) {
	return nil, ErrNotImplement
}
func (*NotImplement) GetBranchTestReports(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, status, state, stateBang string, start, limit int64) (*devops.PipelineTestReport, error) {
	return nil, ErrNotImplement
}

func (*NotImplement) SubmitInputStep(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId, stepId string, options devops.PipelineInputOptions) error {
	return ErrNotImplement
}
func (*NotImplement) SubmitBranchInputStep(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, stepId string, options devops.PipelineInputOptions) error {
	return ErrNotImplement
}

func (*NotImplement) GetArtifactList(ctx context.Context, pipelineNamespace, pipelineName, organization, run string) (*devops.PipelineArtifactList, error) {
	return nil, ErrNotImplement
}

func (*NotImplement) DownloadArtifact(context context.Context, namespace string, pcname string, runid string, filename string) (body io.ReadCloser, contenttype string, err error) {
	return nil, "", ErrNotImplement
}
func (*NotImplement) DownloadArtifacts(context context.Context, namespace string, pcname string, runid string) (body io.ReadCloser, contenttype string, err error) {
	return nil, "", ErrNotImplement
}

func (*NotImplement) DisableJob(context context.Context, namespace string, pcname string) (err error) {
	return ErrNotImplement
}

func (*NotImplement) EnableJob(context context.Context, namespace string, pcname string) (err error) {
	return ErrNotImplement
}

func (*NotImplement) SearchForUsers(_ context.Context, username string) (*devops.ProjectmanagementUser, error) {
	return nil, ErrNotImplement
}

func (*NotImplement) GetIssueOptions(ctx context.Context, optiontype string) (*devops.IssueFilterDataList, error) {
	return nil, ErrNotImplement
}

func (*NotImplement) GetIssueDetail(ctx context.Context, issuekey string) (*devops.IssueDetail, error) {
	return nil, ErrNotImplement
}

func (*NotImplement) GetIssueList(_ context.Context, listoption devops.ListIssuesOptions) (*devops.IssueDetailList, error) {
	return nil, ErrNotImplement
}

// CreateScriptByPath not implement
func (*NotImplement) CreateScriptByPath(ctx context.Context, name, kind, script string) (err error) {
	return ErrNotImplement
}

// UpdateScriptByPath not implement
func (*NotImplement) UpdateScriptByPath(ctx context.Context, name, kind, script string) (err error) {
	return ErrNotImplement
}

// ExecuteScript not implement
func (*NotImplement) ExecuteScript(ctx context.Context, name, jsonParam string) (err error) {
	return ErrNotImplement
}

// DeleteScript not implement
func (*NotImplement) DeleteScript(ctx context.Context, name string) (err error) {
	return ErrNotImplement
}

// ListScripts not implement
func (*NotImplement) ListScripts(ctx context.Context) ([]NexusScript, error) {
	return nil, ErrNotImplement
}

// GetScript not implement
func (*NotImplement) GetScript(ctx context.Context, name string) (NexusScript, error) {
	return NexusScript{}, ErrNotImplement
}

// GetScript not implement
func (*NotImplement) CreateTask(ctx context.Context, name, repositoryName, cronExpression string) error {
	return ErrNotImplement
}
