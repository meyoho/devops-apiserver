package v1

import (
	"context"
	"io"
	"time"

	corev1 "k8s.io/api/core/v1"

	"alauda.io/devops-apiserver/pkg/apis/devops"
)

// Interface is the main interface for all clients
//go:generate mockgen -destination=../../../mock/interface_mock.go -package=mock alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1 Interface
type Interface interface {
	Initializer
	Checker
	Authorizer
	CodeRepoService
	ImageRegistryService
	ProjectService
	CodeQualityService
	NexusService
	JenkinsService
	ProjectManagementService
	HarborService
}

type Initializer interface {
	Init(context.Context) []error
}

type Checker interface {
	Available(context.Context) (*devops.HostPortStatus, error)
}

type Authorizer interface {
	Authenticate(context.Context) (*devops.HostPortStatus, error)
}

type CodeRepoService interface {
	GetRemoteRepos(context.Context) (*devops.CodeRepoBindingRepositories, error)
	GetBranches(_ context.Context, owner, repo, repoFullName string) ([]devops.CodeRepoBranch, error)
	GetLatestRepoCommit(_ context.Context, repoID, owner, repoName, repoFullName string) (commit *devops.RepositoryCommit, status *devops.HostPortStatus)
	CreateCodeRepoProject(context.Context, devops.CreateProjectOptions) (*devops.ProjectData, error)
	ListCodeRepoProjects(context.Context, devops.ListProjectOptions) (*devops.ProjectDataList, error)
	AccessToken(_ context.Context, redirectUrl string) (secret *corev1.Secret, err error)
	GetAuthorizeUrl(_ context.Context, redirectUrl string) string
}

type ImageRegistryService interface {
	GetImageRepos(context.Context) (*devops.ImageRegistryBindingRepositories, error)
	GetImageTags(_ context.Context, repository string) ([]devops.ImageTag, error)
	TriggerScanImage(_ context.Context, repositoryName, tag string) *devops.ImageResult
	GetVulnerability(_ context.Context, repositoryName, tag string) (result *devops.VulnerabilityList, err error)
	GetImageProjects(_ context.Context) (*devops.ProjectDataList, error)
	CreateImageProject(_ context.Context, name string) (*devops.ProjectData, error)
	RepositoryAvailable(_ context.Context, repositoryName string, lastModifyAt time.Time) (*devops.HostPortStatus, error)
	GetImageRepoLink(_ context.Context, name string) string
}

// HarborService is the interface for interactive with a harbor server
type HarborService interface {
	UpdateHarborConfig(ctx context.Context, configJSON string) (err error)
	GetHarborConfig(ctx context.Context) (config *Configurations, err error)
}

type ProjectService interface {
	GetProjects(_ context.Context, page, pagesize string) (*devops.ProjectDataList, error)
	CreateProject(_ context.Context, projectName, projectDescription, projectLead, projectKey string) (*devops.ProjectData, error)
}

type ProjectManagementService interface {
	SearchForUsers(_ context.Context, username string) (*devops.ProjectmanagementUser, error)
	GetIssueOptions(_ context.Context, optiontype string) (*devops.IssueFilterDataList, error)
	GetIssueDetail(_ context.Context, issuekey string) (*devops.IssueDetail, error)
	GetIssueList(_ context.Context, listoption devops.ListIssuesOptions) (*devops.IssueDetailList, error)
}

type CodeQualityService interface {
	GetProjectReport(_ context.Context, projectKey string) ([]devops.CodeQualityCondition, error)
	GetCorrespondCodeQualityProjects(_ context.Context, repositoryList *devops.CodeRepositoryList, binding *devops.CodeQualityBinding) ([]devops.CodeQualityProject, error)
	CheckProjectExist(_ context.Context, projectKey string) (bool, error)
	GetLastAnalysisDate(_ context.Context, projectKey string) (*time.Time, error)
}

type ArtifactService interface {
	ListRegistry(context.Context) (devops.ArtifactRegistryList, error)
	CreateRegistry(_ context.Context, artifactType string, v map[string]string) error
	ListBlobStore(context.Context) (devops.BlobStoreOptionList, error)
}

// NexusService is the interface for interactive with a Nexus server
type NexusService interface {
	ArtifactService

	CreateScriptByPath(ctx context.Context, name, kind, script string) error
	UpdateScriptByPath(ctx context.Context, name, kind, script string) error
	ExecuteScript(ctx context.Context, name, jsonParam string) error
	DeleteScript(ctx context.Context, name string) error
	ListScripts(ctx context.Context) ([]NexusScript, error)
	GetScript(ctx context.Context, name string) (NexusScript, error)
	CreateTask(ctx context.Context, name, repositoryName, cronExpression string) error
}

type JenkinsService interface {
	GetJobProgressiveLog(ctx context.Context, pipelineNamespace, pipelineName string, buildNumber, start int64) (*devops.PipelineLog, error)
	GetBranchJobProgressiveLog(ctx context.Context, pipelineNamespace, pipelineName, branchName string, buildNumber, start int64) (*devops.PipelineLog, error)
	GetJobStepLog(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId, stepId string, start int64) (*devops.PipelineLog, error)
	GetBranchJobStepLog(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, stepId string, start int64) (*devops.PipelineLog, error)
	GetJobNodeLog(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId string, start int64) (*devops.PipelineLog, error)
	GetBranchJobNodeLog(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string, start int64) (*devops.PipelineLog, error)
	GetBranchProgressiveScanLog(ctx context.Context, pipelineNamespace, pipelineName string, start int64) (*devops.PipelineConfigLog, error)
	ScanMultiBranchJob(ctx context.Context, pipelineNamespace, pipelineName string) error
	GetJobNodes(ctx context.Context, pipelineNamespace, pipelineName, organization, runId string) ([]devops.PipelineBlueOceanTask, error)
	GetBranchJobNodes(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId string) ([]devops.PipelineBlueOceanTask, error)
	GetJobSteps(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId string) ([]devops.PipelineBlueOceanTask, error)
	GetBranchJobSteps(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string) ([]devops.PipelineBlueOceanTask, error)

	GetTestReportSummary(ctx context.Context, pipelineNamespace, pipelineName, organization, runId string) (*devops.PipelineTestReportSummary, error)
	GetBranchTestReportSummary(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId string) (*devops.PipelineTestReportSummary, error)

	GetTestReports(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, status, state, stateBang string, start, limit int64) (*devops.PipelineTestReport, error)
	GetBranchTestReports(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, status, state, stateBang string, start, limit int64) (*devops.PipelineTestReport, error)

	SubmitInputStep(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId, stepId string, options devops.PipelineInputOptions) error
	SubmitBranchInputStep(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, stepId string, options devops.PipelineInputOptions) error
	GetArtifactList(ctx context.Context, pipelineNamespace, pipelineName, organization, run string) (*devops.PipelineArtifactList, error)
	DownloadArtifact(context context.Context, namespace string, pcname string, runid string, filename string) (body io.ReadCloser, contenttype string, err error)
	DownloadArtifacts(context context.Context, namespace string, pcname string, runid string) (body io.ReadCloser, contenttype string, err error)
	DisableJob(context context.Context, namespace string, pcname string) (err error)
	EnableJob(context context.Context, namespace string, pcname string) (err error)
}
