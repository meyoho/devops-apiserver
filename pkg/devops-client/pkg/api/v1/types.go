package v1

type Configurations struct {
	// The auth mode of current system, such as "db_auth", "ldap_auth"
	AuthMode string `json:"auth_mode,omitempty"`

	// Specify the oidc client id.
	OidcClientID string `json:"oidc_client_id,omitempty"`

	// Specify the oidc endpoint.
	OidcEndpoint string `json:"oidc_endpoint,omitempty"`

	// Specify the oidc name.
	OidcName string `json:"oidc_name,omitempty"`

	// Specify the oidc scope.
	OidcScope string `json:"oidc_scope,omitempty"`

	// Specify the oidc verify cert.
	OidcVerifyCert bool `json:"oidc_verify_cert,omitempty"`
}

// NexusScript represents a script which belong to nexus
type NexusScript struct {
	Name    string
	Type    string
	Content string
}
