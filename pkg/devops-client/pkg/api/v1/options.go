package v1

import (
	"crypto/tls"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/go-logr/logr"
	"k8s.io/klog/klogr"
)

// Option injects option
type Option func(*Options)

// Options contains all clients options
type Options struct {
	Logger    logr.Logger
	Transport http.RoundTripper
	Client    *http.Client

	BasicConfig *BasicConfig
	BasicAuth   *BasicAuth
	BearerToken *BearerToken
	APIKey      *APIKey
	// generic type
	GenericOpts *GenericOpts
}

// NewOptions constructor
func NewOptions(opts ...Option) *Options {
	opt := &Options{
		// inject default option
		Logger:    NewDefaultLogger(),
		Transport: NewDefaultTransport(),
	}

	for _, o := range opts {
		o(opt)
	}

	// set client if not inject
	if opt.Client == nil {
		opt.Client = NewDefaultClient()
	}

	// if user inject transport nil
	// rewrite it to default
	if opt.Transport == nil {
		opt.Transport = NewDefaultTransport()
	}
	opt.Client.Transport = opt.Transport

	return opt
}

// NewDefaultLogger ...
func NewDefaultLogger() logr.Logger {
	return klogr.New()
}

// NewDefaultTransport ...
func NewDefaultTransport() http.RoundTripper {
	return &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   10 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
	}
}

// NewDefaultClient ...
func NewDefaultClient() *http.Client {
	return &http.Client{Timeout: 30 * time.Second}
}

// BasicConfig contains BasePath/Scheme/Host
type BasicConfig struct {
	Host     string
	BasePath string
	Schemes  []string
}

// BasicAuth auth by username and password
type BasicAuth struct {
	Username string
	Password string
}

// APIKey auth by api key
type APIKey struct {
	Name  string
	In    string
	Value string
}

// BearerToken auth by token
type BearerToken struct {
	Token string
}

// NewURL is a wrapper for basic config
func NewURL(url string) Option {
	// split url
	var (
		host, scheme string
	)

	urls := strings.Split(url, "://")
	if len(urls) == 2 {
		scheme, host = urls[0], urls[1]
		host = strings.TrimRight(host, "/")
	}
	return NewBasicConfig(host, "", []string{scheme})
}

// NewBasicConfig constructor
func NewBasicConfig(host, basePath string, schemes []string) Option {
	return func(o *Options) {
		o.BasicConfig = &BasicConfig{
			Host:     host,
			BasePath: basePath,
			Schemes:  schemes,
		}
	}
}

// NewBasicAuth constructor
func NewBasicAuth(username, password string) Option {
	return func(o *Options) {
		o.BasicAuth = &BasicAuth{
			Username: username,
			Password: password,
		}
	}
}

// NewBearerToken constructor
func NewBearerToken(token string) Option {
	return func(o *Options) {
		o.BearerToken = &BearerToken{
			Token: token,
		}
	}
}

// NewAPIKey constructor
func NewAPIKey(name, in, value string) Option {
	return func(o *Options) {
		o.APIKey = &APIKey{
			Name:  name,
			In:    in,
			Value: value,
		}
	}
}

// NewLogger injects new logger
func NewLogger(logger logr.Logger) Option {
	return func(o *Options) {
		o.Logger = logger
	}
}

// NewTransport injects new transport
func NewTransport(transport http.RoundTripper) Option {
	return func(o *Options) {
		o.Transport = transport
	}
}

// NewClient injects new client
func NewClient(client *http.Client) Option {
	return func(o *Options) {
		o.Client = client
	}
}

// GenericOpts includes any type option
type GenericOpts struct {
	Options []interface{}
}

func NewGenericOpts(opts ...interface{}) Option {
	return func(o *Options) {
		o.GenericOpts = &GenericOpts{Options: opts}
	}
}
