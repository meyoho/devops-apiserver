// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetTeamsTeamIDMembershipsUsernameParams creates a new GetTeamsTeamIDMembershipsUsernameParams object
// with the default values initialized.
func NewGetTeamsTeamIDMembershipsUsernameParams() *GetTeamsTeamIDMembershipsUsernameParams {
	var ()
	return &GetTeamsTeamIDMembershipsUsernameParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetTeamsTeamIDMembershipsUsernameParamsWithTimeout creates a new GetTeamsTeamIDMembershipsUsernameParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetTeamsTeamIDMembershipsUsernameParamsWithTimeout(timeout time.Duration) *GetTeamsTeamIDMembershipsUsernameParams {
	var ()
	return &GetTeamsTeamIDMembershipsUsernameParams{

		timeout: timeout,
	}
}

// NewGetTeamsTeamIDMembershipsUsernameParamsWithContext creates a new GetTeamsTeamIDMembershipsUsernameParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetTeamsTeamIDMembershipsUsernameParamsWithContext(ctx context.Context) *GetTeamsTeamIDMembershipsUsernameParams {
	var ()
	return &GetTeamsTeamIDMembershipsUsernameParams{

		Context: ctx,
	}
}

// NewGetTeamsTeamIDMembershipsUsernameParamsWithHTTPClient creates a new GetTeamsTeamIDMembershipsUsernameParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetTeamsTeamIDMembershipsUsernameParamsWithHTTPClient(client *http.Client) *GetTeamsTeamIDMembershipsUsernameParams {
	var ()
	return &GetTeamsTeamIDMembershipsUsernameParams{
		HTTPClient: client,
	}
}

/*GetTeamsTeamIDMembershipsUsernameParams contains all the parameters to send to the API endpoint
for the get teams team ID memberships username operation typically these are written to a http.Request
*/
type GetTeamsTeamIDMembershipsUsernameParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*TeamID
	  Id of team.

	*/
	TeamID int64
	/*Username
	  Name of a member.

	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) WithTimeout(timeout time.Duration) *GetTeamsTeamIDMembershipsUsernameParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) WithContext(ctx context.Context) *GetTeamsTeamIDMembershipsUsernameParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) WithHTTPClient(client *http.Client) *GetTeamsTeamIDMembershipsUsernameParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) WithAccept(accept *string) *GetTeamsTeamIDMembershipsUsernameParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithTeamID adds the teamID to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) WithTeamID(teamID int64) *GetTeamsTeamIDMembershipsUsernameParams {
	o.SetTeamID(teamID)
	return o
}

// SetTeamID adds the teamId to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) SetTeamID(teamID int64) {
	o.TeamID = teamID
}

// WithUsername adds the username to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) WithUsername(username string) *GetTeamsTeamIDMembershipsUsernameParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get teams team ID memberships username params
func (o *GetTeamsTeamIDMembershipsUsernameParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetTeamsTeamIDMembershipsUsernameParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	// path param teamId
	if err := r.SetPathParam("teamId", swag.FormatInt64(o.TeamID)); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
