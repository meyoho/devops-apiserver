// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetReposOwnerRepoCommitsParams creates a new GetReposOwnerRepoCommitsParams object
// with the default values initialized.
func NewGetReposOwnerRepoCommitsParams() *GetReposOwnerRepoCommitsParams {
	var ()
	return &GetReposOwnerRepoCommitsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetReposOwnerRepoCommitsParamsWithTimeout creates a new GetReposOwnerRepoCommitsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetReposOwnerRepoCommitsParamsWithTimeout(timeout time.Duration) *GetReposOwnerRepoCommitsParams {
	var ()
	return &GetReposOwnerRepoCommitsParams{

		timeout: timeout,
	}
}

// NewGetReposOwnerRepoCommitsParamsWithContext creates a new GetReposOwnerRepoCommitsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetReposOwnerRepoCommitsParamsWithContext(ctx context.Context) *GetReposOwnerRepoCommitsParams {
	var ()
	return &GetReposOwnerRepoCommitsParams{

		Context: ctx,
	}
}

// NewGetReposOwnerRepoCommitsParamsWithHTTPClient creates a new GetReposOwnerRepoCommitsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetReposOwnerRepoCommitsParamsWithHTTPClient(client *http.Client) *GetReposOwnerRepoCommitsParams {
	var ()
	return &GetReposOwnerRepoCommitsParams{
		HTTPClient: client,
	}
}

/*GetReposOwnerRepoCommitsParams contains all the parameters to send to the API endpoint
for the get repos owner repo commits operation typically these are written to a http.Request
*/
type GetReposOwnerRepoCommitsParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Author
	  GitHub login, name, or email by which to filter by commit author.

	*/
	Author *string
	/*Owner
	  Name of repository owner.

	*/
	Owner string
	/*Path
	  Only commits containing this file path will be returned.

	*/
	Path *string
	/*Repo
	  Name of repository.

	*/
	Repo string
	/*Sha
	  Sha or branch to start listing commits from.

	*/
	Sha *string
	/*Since
	  The time should be passed in as UTC in the ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ.
	Example: "2012-10-09T23:39:01Z".


	*/
	Since *string
	/*Until
	  ISO 8601 Date - Only commits before this date will be returned.

	*/
	Until *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) WithTimeout(timeout time.Duration) *GetReposOwnerRepoCommitsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) WithContext(ctx context.Context) *GetReposOwnerRepoCommitsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) WithHTTPClient(client *http.Client) *GetReposOwnerRepoCommitsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) WithAccept(accept *string) *GetReposOwnerRepoCommitsParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithAuthor adds the author to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) WithAuthor(author *string) *GetReposOwnerRepoCommitsParams {
	o.SetAuthor(author)
	return o
}

// SetAuthor adds the author to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) SetAuthor(author *string) {
	o.Author = author
}

// WithOwner adds the owner to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) WithOwner(owner string) *GetReposOwnerRepoCommitsParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithPath adds the path to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) WithPath(path *string) *GetReposOwnerRepoCommitsParams {
	o.SetPath(path)
	return o
}

// SetPath adds the path to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) SetPath(path *string) {
	o.Path = path
}

// WithRepo adds the repo to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) WithRepo(repo string) *GetReposOwnerRepoCommitsParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) SetRepo(repo string) {
	o.Repo = repo
}

// WithSha adds the sha to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) WithSha(sha *string) *GetReposOwnerRepoCommitsParams {
	o.SetSha(sha)
	return o
}

// SetSha adds the sha to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) SetSha(sha *string) {
	o.Sha = sha
}

// WithSince adds the since to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) WithSince(since *string) *GetReposOwnerRepoCommitsParams {
	o.SetSince(since)
	return o
}

// SetSince adds the since to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) SetSince(since *string) {
	o.Since = since
}

// WithUntil adds the until to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) WithUntil(until *string) *GetReposOwnerRepoCommitsParams {
	o.SetUntil(until)
	return o
}

// SetUntil adds the until to the get repos owner repo commits params
func (o *GetReposOwnerRepoCommitsParams) SetUntil(until *string) {
	o.Until = until
}

// WriteToRequest writes these params to a swagger request
func (o *GetReposOwnerRepoCommitsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	if o.Author != nil {

		// query param author
		var qrAuthor string
		if o.Author != nil {
			qrAuthor = *o.Author
		}
		qAuthor := qrAuthor
		if qAuthor != "" {
			if err := r.SetQueryParam("author", qAuthor); err != nil {
				return err
			}
		}

	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	if o.Path != nil {

		// query param path
		var qrPath string
		if o.Path != nil {
			qrPath = *o.Path
		}
		qPath := qrPath
		if qPath != "" {
			if err := r.SetQueryParam("path", qPath); err != nil {
				return err
			}
		}

	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	if o.Sha != nil {

		// query param sha
		var qrSha string
		if o.Sha != nil {
			qrSha = *o.Sha
		}
		qSha := qrSha
		if qSha != "" {
			if err := r.SetQueryParam("sha", qSha); err != nil {
				return err
			}
		}

	}

	if o.Since != nil {

		// query param since
		var qrSince string
		if o.Since != nil {
			qrSince = *o.Since
		}
		qSince := qrSince
		if qSince != "" {
			if err := r.SetQueryParam("since", qSince); err != nil {
				return err
			}
		}

	}

	if o.Until != nil {

		// query param until
		var qrUntil string
		if o.Until != nil {
			qrUntil = *o.Until
		}
		qUntil := qrUntil
		if qUntil != "" {
			if err := r.SetQueryParam("until", qUntil); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
