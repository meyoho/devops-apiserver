// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/github/v3/models"
)

// NewPutNotificationsParams creates a new PutNotificationsParams object
// with the default values initialized.
func NewPutNotificationsParams() *PutNotificationsParams {
	var ()
	return &PutNotificationsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPutNotificationsParamsWithTimeout creates a new PutNotificationsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPutNotificationsParamsWithTimeout(timeout time.Duration) *PutNotificationsParams {
	var ()
	return &PutNotificationsParams{

		timeout: timeout,
	}
}

// NewPutNotificationsParamsWithContext creates a new PutNotificationsParams object
// with the default values initialized, and the ability to set a context for a request
func NewPutNotificationsParamsWithContext(ctx context.Context) *PutNotificationsParams {
	var ()
	return &PutNotificationsParams{

		Context: ctx,
	}
}

// NewPutNotificationsParamsWithHTTPClient creates a new PutNotificationsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPutNotificationsParamsWithHTTPClient(client *http.Client) *PutNotificationsParams {
	var ()
	return &PutNotificationsParams{
		HTTPClient: client,
	}
}

/*PutNotificationsParams contains all the parameters to send to the API endpoint
for the put notifications operation typically these are written to a http.Request
*/
type PutNotificationsParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Body*/
	Body *models.NotificationMarkRead

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the put notifications params
func (o *PutNotificationsParams) WithTimeout(timeout time.Duration) *PutNotificationsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put notifications params
func (o *PutNotificationsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put notifications params
func (o *PutNotificationsParams) WithContext(ctx context.Context) *PutNotificationsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put notifications params
func (o *PutNotificationsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put notifications params
func (o *PutNotificationsParams) WithHTTPClient(client *http.Client) *PutNotificationsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put notifications params
func (o *PutNotificationsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the put notifications params
func (o *PutNotificationsParams) WithAccept(accept *string) *PutNotificationsParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the put notifications params
func (o *PutNotificationsParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithBody adds the body to the put notifications params
func (o *PutNotificationsParams) WithBody(body *models.NotificationMarkRead) *PutNotificationsParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the put notifications params
func (o *PutNotificationsParams) SetBody(body *models.NotificationMarkRead) {
	o.Body = body
}

// WriteToRequest writes these params to a swagger request
func (o *PutNotificationsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
