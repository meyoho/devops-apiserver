// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/github/v3/models"
)

// NewPostReposOwnerRepoCommitsShaCodeCommentsParams creates a new PostReposOwnerRepoCommitsShaCodeCommentsParams object
// with the default values initialized.
func NewPostReposOwnerRepoCommitsShaCodeCommentsParams() *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	var ()
	return &PostReposOwnerRepoCommitsShaCodeCommentsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostReposOwnerRepoCommitsShaCodeCommentsParamsWithTimeout creates a new PostReposOwnerRepoCommitsShaCodeCommentsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostReposOwnerRepoCommitsShaCodeCommentsParamsWithTimeout(timeout time.Duration) *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	var ()
	return &PostReposOwnerRepoCommitsShaCodeCommentsParams{

		timeout: timeout,
	}
}

// NewPostReposOwnerRepoCommitsShaCodeCommentsParamsWithContext creates a new PostReposOwnerRepoCommitsShaCodeCommentsParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostReposOwnerRepoCommitsShaCodeCommentsParamsWithContext(ctx context.Context) *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	var ()
	return &PostReposOwnerRepoCommitsShaCodeCommentsParams{

		Context: ctx,
	}
}

// NewPostReposOwnerRepoCommitsShaCodeCommentsParamsWithHTTPClient creates a new PostReposOwnerRepoCommitsShaCodeCommentsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostReposOwnerRepoCommitsShaCodeCommentsParamsWithHTTPClient(client *http.Client) *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	var ()
	return &PostReposOwnerRepoCommitsShaCodeCommentsParams{
		HTTPClient: client,
	}
}

/*PostReposOwnerRepoCommitsShaCodeCommentsParams contains all the parameters to send to the API endpoint
for the post repos owner repo commits sha code comments operation typically these are written to a http.Request
*/
type PostReposOwnerRepoCommitsShaCodeCommentsParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Body*/
	Body *models.CommitCommentBody
	/*Owner
	  Name of repository owner.

	*/
	Owner string
	/*Repo
	  Name of repository.

	*/
	Repo string
	/*ShaCode
	  SHA-1 code of the commit.

	*/
	ShaCode string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) WithTimeout(timeout time.Duration) *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) WithContext(ctx context.Context) *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) WithHTTPClient(client *http.Client) *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) WithAccept(accept *string) *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithBody adds the body to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) WithBody(body *models.CommitCommentBody) *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) SetBody(body *models.CommitCommentBody) {
	o.Body = body
}

// WithOwner adds the owner to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) WithOwner(owner string) *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithRepo adds the repo to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) WithRepo(repo string) *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) SetRepo(repo string) {
	o.Repo = repo
}

// WithShaCode adds the shaCode to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) WithShaCode(shaCode string) *PostReposOwnerRepoCommitsShaCodeCommentsParams {
	o.SetShaCode(shaCode)
	return o
}

// SetShaCode adds the shaCode to the post repos owner repo commits sha code comments params
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) SetShaCode(shaCode string) {
	o.ShaCode = shaCode
}

// WriteToRequest writes these params to a swagger request
func (o *PostReposOwnerRepoCommitsShaCodeCommentsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	// path param shaCode
	if err := r.SetPathParam("shaCode", o.ShaCode); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
