// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetUserStarredOwnerRepoParams creates a new GetUserStarredOwnerRepoParams object
// with the default values initialized.
func NewGetUserStarredOwnerRepoParams() *GetUserStarredOwnerRepoParams {
	var ()
	return &GetUserStarredOwnerRepoParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetUserStarredOwnerRepoParamsWithTimeout creates a new GetUserStarredOwnerRepoParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetUserStarredOwnerRepoParamsWithTimeout(timeout time.Duration) *GetUserStarredOwnerRepoParams {
	var ()
	return &GetUserStarredOwnerRepoParams{

		timeout: timeout,
	}
}

// NewGetUserStarredOwnerRepoParamsWithContext creates a new GetUserStarredOwnerRepoParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetUserStarredOwnerRepoParamsWithContext(ctx context.Context) *GetUserStarredOwnerRepoParams {
	var ()
	return &GetUserStarredOwnerRepoParams{

		Context: ctx,
	}
}

// NewGetUserStarredOwnerRepoParamsWithHTTPClient creates a new GetUserStarredOwnerRepoParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetUserStarredOwnerRepoParamsWithHTTPClient(client *http.Client) *GetUserStarredOwnerRepoParams {
	var ()
	return &GetUserStarredOwnerRepoParams{
		HTTPClient: client,
	}
}

/*GetUserStarredOwnerRepoParams contains all the parameters to send to the API endpoint
for the get user starred owner repo operation typically these are written to a http.Request
*/
type GetUserStarredOwnerRepoParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Owner
	  Name of a repository owner.

	*/
	Owner string
	/*Repo
	  Name of a repository.

	*/
	Repo string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) WithTimeout(timeout time.Duration) *GetUserStarredOwnerRepoParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) WithContext(ctx context.Context) *GetUserStarredOwnerRepoParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) WithHTTPClient(client *http.Client) *GetUserStarredOwnerRepoParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) WithAccept(accept *string) *GetUserStarredOwnerRepoParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithOwner adds the owner to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) WithOwner(owner string) *GetUserStarredOwnerRepoParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithRepo adds the repo to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) WithRepo(repo string) *GetUserStarredOwnerRepoParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the get user starred owner repo params
func (o *GetUserStarredOwnerRepoParams) SetRepo(repo string) {
	o.Repo = repo
}

// WriteToRequest writes these params to a swagger request
func (o *GetUserStarredOwnerRepoParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
