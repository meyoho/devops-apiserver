// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/github/v3/models"
)

// NewPutReposOwnerRepoSubscriptionParams creates a new PutReposOwnerRepoSubscriptionParams object
// with the default values initialized.
func NewPutReposOwnerRepoSubscriptionParams() *PutReposOwnerRepoSubscriptionParams {
	var ()
	return &PutReposOwnerRepoSubscriptionParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPutReposOwnerRepoSubscriptionParamsWithTimeout creates a new PutReposOwnerRepoSubscriptionParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPutReposOwnerRepoSubscriptionParamsWithTimeout(timeout time.Duration) *PutReposOwnerRepoSubscriptionParams {
	var ()
	return &PutReposOwnerRepoSubscriptionParams{

		timeout: timeout,
	}
}

// NewPutReposOwnerRepoSubscriptionParamsWithContext creates a new PutReposOwnerRepoSubscriptionParams object
// with the default values initialized, and the ability to set a context for a request
func NewPutReposOwnerRepoSubscriptionParamsWithContext(ctx context.Context) *PutReposOwnerRepoSubscriptionParams {
	var ()
	return &PutReposOwnerRepoSubscriptionParams{

		Context: ctx,
	}
}

// NewPutReposOwnerRepoSubscriptionParamsWithHTTPClient creates a new PutReposOwnerRepoSubscriptionParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPutReposOwnerRepoSubscriptionParamsWithHTTPClient(client *http.Client) *PutReposOwnerRepoSubscriptionParams {
	var ()
	return &PutReposOwnerRepoSubscriptionParams{
		HTTPClient: client,
	}
}

/*PutReposOwnerRepoSubscriptionParams contains all the parameters to send to the API endpoint
for the put repos owner repo subscription operation typically these are written to a http.Request
*/
type PutReposOwnerRepoSubscriptionParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Body*/
	Body *models.SubscriptionBody
	/*Owner
	  Name of repository owner.

	*/
	Owner string
	/*Repo
	  Name of repository.

	*/
	Repo string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) WithTimeout(timeout time.Duration) *PutReposOwnerRepoSubscriptionParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) WithContext(ctx context.Context) *PutReposOwnerRepoSubscriptionParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) WithHTTPClient(client *http.Client) *PutReposOwnerRepoSubscriptionParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) WithAccept(accept *string) *PutReposOwnerRepoSubscriptionParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithBody adds the body to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) WithBody(body *models.SubscriptionBody) *PutReposOwnerRepoSubscriptionParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) SetBody(body *models.SubscriptionBody) {
	o.Body = body
}

// WithOwner adds the owner to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) WithOwner(owner string) *PutReposOwnerRepoSubscriptionParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithRepo adds the repo to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) WithRepo(repo string) *PutReposOwnerRepoSubscriptionParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the put repos owner repo subscription params
func (o *PutReposOwnerRepoSubscriptionParams) SetRepo(repo string) {
	o.Repo = repo
}

// WriteToRequest writes these params to a swagger request
func (o *PutReposOwnerRepoSubscriptionParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
