// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// DeleteReposOwnerRepoReleasesAssetsIDReader is a Reader for the DeleteReposOwnerRepoReleasesAssetsID structure.
type DeleteReposOwnerRepoReleasesAssetsIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteReposOwnerRepoReleasesAssetsIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 204:
		result := NewDeleteReposOwnerRepoReleasesAssetsIDNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewDeleteReposOwnerRepoReleasesAssetsIDForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewDeleteReposOwnerRepoReleasesAssetsIDNoContent creates a DeleteReposOwnerRepoReleasesAssetsIDNoContent with default headers values
func NewDeleteReposOwnerRepoReleasesAssetsIDNoContent() *DeleteReposOwnerRepoReleasesAssetsIDNoContent {
	return &DeleteReposOwnerRepoReleasesAssetsIDNoContent{}
}

/*DeleteReposOwnerRepoReleasesAssetsIDNoContent handles this case with default header values.

No Content
*/
type DeleteReposOwnerRepoReleasesAssetsIDNoContent struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64
}

func (o *DeleteReposOwnerRepoReleasesAssetsIDNoContent) Error() string {
	return fmt.Sprintf("[DELETE /repos/{owner}/{repo}/releases/assets/{id}][%d] deleteReposOwnerRepoReleasesAssetsIdNoContent ", 204)
}

func (o *DeleteReposOwnerRepoReleasesAssetsIDNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	return nil
}

// NewDeleteReposOwnerRepoReleasesAssetsIDForbidden creates a DeleteReposOwnerRepoReleasesAssetsIDForbidden with default headers values
func NewDeleteReposOwnerRepoReleasesAssetsIDForbidden() *DeleteReposOwnerRepoReleasesAssetsIDForbidden {
	return &DeleteReposOwnerRepoReleasesAssetsIDForbidden{}
}

/*DeleteReposOwnerRepoReleasesAssetsIDForbidden handles this case with default header values.

API rate limit exceeded. See http://developer.github.com/v3/#rate-limiting
for details.

*/
type DeleteReposOwnerRepoReleasesAssetsIDForbidden struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64
}

func (o *DeleteReposOwnerRepoReleasesAssetsIDForbidden) Error() string {
	return fmt.Sprintf("[DELETE /repos/{owner}/{repo}/releases/assets/{id}][%d] deleteReposOwnerRepoReleasesAssetsIdForbidden ", 403)
}

func (o *DeleteReposOwnerRepoReleasesAssetsIDForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	return nil
}
