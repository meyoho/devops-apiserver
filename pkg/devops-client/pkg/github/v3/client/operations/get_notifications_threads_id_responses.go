// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/github/v3/models"
)

// GetNotificationsThreadsIDReader is a Reader for the GetNotificationsThreadsID structure.
type GetNotificationsThreadsIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetNotificationsThreadsIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetNotificationsThreadsIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewGetNotificationsThreadsIDForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetNotificationsThreadsIDOK creates a GetNotificationsThreadsIDOK with default headers values
func NewGetNotificationsThreadsIDOK() *GetNotificationsThreadsIDOK {
	return &GetNotificationsThreadsIDOK{}
}

/*GetNotificationsThreadsIDOK handles this case with default header values.

OK
*/
type GetNotificationsThreadsIDOK struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64

	Payload *models.Notifications
}

func (o *GetNotificationsThreadsIDOK) Error() string {
	return fmt.Sprintf("[GET /notifications/threads/{id}][%d] getNotificationsThreadsIdOK  %+v", 200, o.Payload)
}

func (o *GetNotificationsThreadsIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	o.Payload = new(models.Notifications)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetNotificationsThreadsIDForbidden creates a GetNotificationsThreadsIDForbidden with default headers values
func NewGetNotificationsThreadsIDForbidden() *GetNotificationsThreadsIDForbidden {
	return &GetNotificationsThreadsIDForbidden{}
}

/*GetNotificationsThreadsIDForbidden handles this case with default header values.

API rate limit exceeded. See http://developer.github.com/v3/#rate-limiting
for details.

*/
type GetNotificationsThreadsIDForbidden struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64
}

func (o *GetNotificationsThreadsIDForbidden) Error() string {
	return fmt.Sprintf("[GET /notifications/threads/{id}][%d] getNotificationsThreadsIdForbidden ", 403)
}

func (o *GetNotificationsThreadsIDForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	return nil
}
