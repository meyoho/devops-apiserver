// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetUsersUsernameSubscriptionsParams creates a new GetUsersUsernameSubscriptionsParams object
// with the default values initialized.
func NewGetUsersUsernameSubscriptionsParams() *GetUsersUsernameSubscriptionsParams {
	var ()
	return &GetUsersUsernameSubscriptionsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetUsersUsernameSubscriptionsParamsWithTimeout creates a new GetUsersUsernameSubscriptionsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetUsersUsernameSubscriptionsParamsWithTimeout(timeout time.Duration) *GetUsersUsernameSubscriptionsParams {
	var ()
	return &GetUsersUsernameSubscriptionsParams{

		timeout: timeout,
	}
}

// NewGetUsersUsernameSubscriptionsParamsWithContext creates a new GetUsersUsernameSubscriptionsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetUsersUsernameSubscriptionsParamsWithContext(ctx context.Context) *GetUsersUsernameSubscriptionsParams {
	var ()
	return &GetUsersUsernameSubscriptionsParams{

		Context: ctx,
	}
}

// NewGetUsersUsernameSubscriptionsParamsWithHTTPClient creates a new GetUsersUsernameSubscriptionsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetUsersUsernameSubscriptionsParamsWithHTTPClient(client *http.Client) *GetUsersUsernameSubscriptionsParams {
	var ()
	return &GetUsersUsernameSubscriptionsParams{
		HTTPClient: client,
	}
}

/*GetUsersUsernameSubscriptionsParams contains all the parameters to send to the API endpoint
for the get users username subscriptions operation typically these are written to a http.Request
*/
type GetUsersUsernameSubscriptionsParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Username
	  Name of user.

	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get users username subscriptions params
func (o *GetUsersUsernameSubscriptionsParams) WithTimeout(timeout time.Duration) *GetUsersUsernameSubscriptionsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get users username subscriptions params
func (o *GetUsersUsernameSubscriptionsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get users username subscriptions params
func (o *GetUsersUsernameSubscriptionsParams) WithContext(ctx context.Context) *GetUsersUsernameSubscriptionsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get users username subscriptions params
func (o *GetUsersUsernameSubscriptionsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get users username subscriptions params
func (o *GetUsersUsernameSubscriptionsParams) WithHTTPClient(client *http.Client) *GetUsersUsernameSubscriptionsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get users username subscriptions params
func (o *GetUsersUsernameSubscriptionsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get users username subscriptions params
func (o *GetUsersUsernameSubscriptionsParams) WithAccept(accept *string) *GetUsersUsernameSubscriptionsParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get users username subscriptions params
func (o *GetUsersUsernameSubscriptionsParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithUsername adds the username to the get users username subscriptions params
func (o *GetUsersUsernameSubscriptionsParams) WithUsername(username string) *GetUsersUsernameSubscriptionsParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get users username subscriptions params
func (o *GetUsersUsernameSubscriptionsParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetUsersUsernameSubscriptionsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
