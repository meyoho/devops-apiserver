// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetUsersUsernameReceivedEventsParams creates a new GetUsersUsernameReceivedEventsParams object
// with the default values initialized.
func NewGetUsersUsernameReceivedEventsParams() *GetUsersUsernameReceivedEventsParams {
	var ()
	return &GetUsersUsernameReceivedEventsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetUsersUsernameReceivedEventsParamsWithTimeout creates a new GetUsersUsernameReceivedEventsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetUsersUsernameReceivedEventsParamsWithTimeout(timeout time.Duration) *GetUsersUsernameReceivedEventsParams {
	var ()
	return &GetUsersUsernameReceivedEventsParams{

		timeout: timeout,
	}
}

// NewGetUsersUsernameReceivedEventsParamsWithContext creates a new GetUsersUsernameReceivedEventsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetUsersUsernameReceivedEventsParamsWithContext(ctx context.Context) *GetUsersUsernameReceivedEventsParams {
	var ()
	return &GetUsersUsernameReceivedEventsParams{

		Context: ctx,
	}
}

// NewGetUsersUsernameReceivedEventsParamsWithHTTPClient creates a new GetUsersUsernameReceivedEventsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetUsersUsernameReceivedEventsParamsWithHTTPClient(client *http.Client) *GetUsersUsernameReceivedEventsParams {
	var ()
	return &GetUsersUsernameReceivedEventsParams{
		HTTPClient: client,
	}
}

/*GetUsersUsernameReceivedEventsParams contains all the parameters to send to the API endpoint
for the get users username received events operation typically these are written to a http.Request
*/
type GetUsersUsernameReceivedEventsParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Username
	  Name of user.

	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get users username received events params
func (o *GetUsersUsernameReceivedEventsParams) WithTimeout(timeout time.Duration) *GetUsersUsernameReceivedEventsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get users username received events params
func (o *GetUsersUsernameReceivedEventsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get users username received events params
func (o *GetUsersUsernameReceivedEventsParams) WithContext(ctx context.Context) *GetUsersUsernameReceivedEventsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get users username received events params
func (o *GetUsersUsernameReceivedEventsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get users username received events params
func (o *GetUsersUsernameReceivedEventsParams) WithHTTPClient(client *http.Client) *GetUsersUsernameReceivedEventsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get users username received events params
func (o *GetUsersUsernameReceivedEventsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get users username received events params
func (o *GetUsersUsernameReceivedEventsParams) WithAccept(accept *string) *GetUsersUsernameReceivedEventsParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get users username received events params
func (o *GetUsersUsernameReceivedEventsParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithUsername adds the username to the get users username received events params
func (o *GetUsersUsernameReceivedEventsParams) WithUsername(username string) *GetUsersUsernameReceivedEventsParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get users username received events params
func (o *GetUsersUsernameReceivedEventsParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetUsersUsernameReceivedEventsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
