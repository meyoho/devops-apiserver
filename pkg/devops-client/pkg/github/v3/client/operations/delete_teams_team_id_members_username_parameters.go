// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewDeleteTeamsTeamIDMembersUsernameParams creates a new DeleteTeamsTeamIDMembersUsernameParams object
// with the default values initialized.
func NewDeleteTeamsTeamIDMembersUsernameParams() *DeleteTeamsTeamIDMembersUsernameParams {
	var ()
	return &DeleteTeamsTeamIDMembersUsernameParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewDeleteTeamsTeamIDMembersUsernameParamsWithTimeout creates a new DeleteTeamsTeamIDMembersUsernameParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewDeleteTeamsTeamIDMembersUsernameParamsWithTimeout(timeout time.Duration) *DeleteTeamsTeamIDMembersUsernameParams {
	var ()
	return &DeleteTeamsTeamIDMembersUsernameParams{

		timeout: timeout,
	}
}

// NewDeleteTeamsTeamIDMembersUsernameParamsWithContext creates a new DeleteTeamsTeamIDMembersUsernameParams object
// with the default values initialized, and the ability to set a context for a request
func NewDeleteTeamsTeamIDMembersUsernameParamsWithContext(ctx context.Context) *DeleteTeamsTeamIDMembersUsernameParams {
	var ()
	return &DeleteTeamsTeamIDMembersUsernameParams{

		Context: ctx,
	}
}

// NewDeleteTeamsTeamIDMembersUsernameParamsWithHTTPClient creates a new DeleteTeamsTeamIDMembersUsernameParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewDeleteTeamsTeamIDMembersUsernameParamsWithHTTPClient(client *http.Client) *DeleteTeamsTeamIDMembersUsernameParams {
	var ()
	return &DeleteTeamsTeamIDMembersUsernameParams{
		HTTPClient: client,
	}
}

/*DeleteTeamsTeamIDMembersUsernameParams contains all the parameters to send to the API endpoint
for the delete teams team ID members username operation typically these are written to a http.Request
*/
type DeleteTeamsTeamIDMembersUsernameParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*TeamID
	  Id of team.

	*/
	TeamID int64
	/*Username
	  Name of a member.

	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) WithTimeout(timeout time.Duration) *DeleteTeamsTeamIDMembersUsernameParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) WithContext(ctx context.Context) *DeleteTeamsTeamIDMembersUsernameParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) WithHTTPClient(client *http.Client) *DeleteTeamsTeamIDMembersUsernameParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) WithAccept(accept *string) *DeleteTeamsTeamIDMembersUsernameParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithTeamID adds the teamID to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) WithTeamID(teamID int64) *DeleteTeamsTeamIDMembersUsernameParams {
	o.SetTeamID(teamID)
	return o
}

// SetTeamID adds the teamId to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) SetTeamID(teamID int64) {
	o.TeamID = teamID
}

// WithUsername adds the username to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) WithUsername(username string) *DeleteTeamsTeamIDMembersUsernameParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the delete teams team ID members username params
func (o *DeleteTeamsTeamIDMembersUsernameParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteTeamsTeamIDMembersUsernameParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	// path param teamId
	if err := r.SetPathParam("teamId", swag.FormatInt64(o.TeamID)); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
