// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/github/v3/models"
)

// NewDeleteUserEmailsParams creates a new DeleteUserEmailsParams object
// with the default values initialized.
func NewDeleteUserEmailsParams() *DeleteUserEmailsParams {
	var ()
	return &DeleteUserEmailsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewDeleteUserEmailsParamsWithTimeout creates a new DeleteUserEmailsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewDeleteUserEmailsParamsWithTimeout(timeout time.Duration) *DeleteUserEmailsParams {
	var ()
	return &DeleteUserEmailsParams{

		timeout: timeout,
	}
}

// NewDeleteUserEmailsParamsWithContext creates a new DeleteUserEmailsParams object
// with the default values initialized, and the ability to set a context for a request
func NewDeleteUserEmailsParamsWithContext(ctx context.Context) *DeleteUserEmailsParams {
	var ()
	return &DeleteUserEmailsParams{

		Context: ctx,
	}
}

// NewDeleteUserEmailsParamsWithHTTPClient creates a new DeleteUserEmailsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewDeleteUserEmailsParamsWithHTTPClient(client *http.Client) *DeleteUserEmailsParams {
	var ()
	return &DeleteUserEmailsParams{
		HTTPClient: client,
	}
}

/*DeleteUserEmailsParams contains all the parameters to send to the API endpoint
for the delete user emails operation typically these are written to a http.Request
*/
type DeleteUserEmailsParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Body*/
	Body models.UserEmails

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the delete user emails params
func (o *DeleteUserEmailsParams) WithTimeout(timeout time.Duration) *DeleteUserEmailsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete user emails params
func (o *DeleteUserEmailsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the delete user emails params
func (o *DeleteUserEmailsParams) WithContext(ctx context.Context) *DeleteUserEmailsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete user emails params
func (o *DeleteUserEmailsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete user emails params
func (o *DeleteUserEmailsParams) WithHTTPClient(client *http.Client) *DeleteUserEmailsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete user emails params
func (o *DeleteUserEmailsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the delete user emails params
func (o *DeleteUserEmailsParams) WithAccept(accept *string) *DeleteUserEmailsParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the delete user emails params
func (o *DeleteUserEmailsParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithBody adds the body to the delete user emails params
func (o *DeleteUserEmailsParams) WithBody(body models.UserEmails) *DeleteUserEmailsParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the delete user emails params
func (o *DeleteUserEmailsParams) SetBody(body models.UserEmails) {
	o.Body = body
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteUserEmailsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
