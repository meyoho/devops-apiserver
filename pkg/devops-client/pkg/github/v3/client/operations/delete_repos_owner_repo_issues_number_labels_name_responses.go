// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// DeleteReposOwnerRepoIssuesNumberLabelsNameReader is a Reader for the DeleteReposOwnerRepoIssuesNumberLabelsName structure.
type DeleteReposOwnerRepoIssuesNumberLabelsNameReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteReposOwnerRepoIssuesNumberLabelsNameReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 204:
		result := NewDeleteReposOwnerRepoIssuesNumberLabelsNameNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewDeleteReposOwnerRepoIssuesNumberLabelsNameForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewDeleteReposOwnerRepoIssuesNumberLabelsNameNoContent creates a DeleteReposOwnerRepoIssuesNumberLabelsNameNoContent with default headers values
func NewDeleteReposOwnerRepoIssuesNumberLabelsNameNoContent() *DeleteReposOwnerRepoIssuesNumberLabelsNameNoContent {
	return &DeleteReposOwnerRepoIssuesNumberLabelsNameNoContent{}
}

/*DeleteReposOwnerRepoIssuesNumberLabelsNameNoContent handles this case with default header values.

Item removed.
*/
type DeleteReposOwnerRepoIssuesNumberLabelsNameNoContent struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64
}

func (o *DeleteReposOwnerRepoIssuesNumberLabelsNameNoContent) Error() string {
	return fmt.Sprintf("[DELETE /repos/{owner}/{repo}/issues/{number}/labels/{name}][%d] deleteReposOwnerRepoIssuesNumberLabelsNameNoContent ", 204)
}

func (o *DeleteReposOwnerRepoIssuesNumberLabelsNameNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	return nil
}

// NewDeleteReposOwnerRepoIssuesNumberLabelsNameForbidden creates a DeleteReposOwnerRepoIssuesNumberLabelsNameForbidden with default headers values
func NewDeleteReposOwnerRepoIssuesNumberLabelsNameForbidden() *DeleteReposOwnerRepoIssuesNumberLabelsNameForbidden {
	return &DeleteReposOwnerRepoIssuesNumberLabelsNameForbidden{}
}

/*DeleteReposOwnerRepoIssuesNumberLabelsNameForbidden handles this case with default header values.

API rate limit exceeded. See http://developer.github.com/v3/#rate-limiting
for details.

*/
type DeleteReposOwnerRepoIssuesNumberLabelsNameForbidden struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64
}

func (o *DeleteReposOwnerRepoIssuesNumberLabelsNameForbidden) Error() string {
	return fmt.Sprintf("[DELETE /repos/{owner}/{repo}/issues/{number}/labels/{name}][%d] deleteReposOwnerRepoIssuesNumberLabelsNameForbidden ", 403)
}

func (o *DeleteReposOwnerRepoIssuesNumberLabelsNameForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	return nil
}
