// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// Subscription subscription
// swagger:model subscription
type Subscription struct {

	// ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
	CreatedAt string `json:"created_at,omitempty"`

	// ignored
	Ignored bool `json:"ignored,omitempty"`

	// reason
	Reason string `json:"reason,omitempty"`

	// repository url
	RepositoryURL string `json:"repository_url,omitempty"`

	// subscribed
	Subscribed bool `json:"subscribed,omitempty"`

	// thread url
	ThreadURL string `json:"thread_url,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this subscription
func (m *Subscription) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *Subscription) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Subscription) UnmarshalBinary(b []byte) error {
	var res Subscription
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
