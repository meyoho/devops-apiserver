package test

import (
	"context"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	taigav1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/taiga/v1"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool Taiga tests", func() {
	var (
		// host
		host    = "https://taiga.test"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options

		transport  = gock.NewTransport()
		httpClient = clientv1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = taigav1.NewClient()
		opts = clientv1.NewOptions(clientv1.NewBasicConfig("taiga.test", "", []string{}), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		gock.OffAll()
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetProjects", func() {
		const (
			projects = `[
				{
					"id": 327109,
					"name": "test-aaa",
					"slug": "pwn2own-test-aaa",
					"description": "test",
					"created_date": "2019-06-17T03:17:23.088Z",
					"modified_date": "2019-06-17T04:15:10.137Z",
					"owner": {
						"username": "pwn2own",
						"full_name_display": "pwn2own",
						"photo": null,
						"big_photo": null,
						"gravatar_id": "e5bb8b84bc4febf506f64f4e9365cfad",
						"is_active": true,
						"id": 368793
					},
					"members": [
						368793
					],
					"total_milestones": null,
					"total_story_points": null,
					"is_contact_activated": true,
					"is_epics_activated": false,
					"is_backlog_activated": true,
					"is_kanban_activated": false,
					"is_wiki_activated": true,
					"is_issues_activated": true,
					"videoconferences": null,
					"videoconferences_extra_data": null,
					"creation_template": 1,
					"is_private": false,
					"anon_permissions": [
						"view_wiki_pages",
						"view_wiki_links",
						"view_us",
						"view_project",
						"view_epics",
						"view_milestones",
						"view_tasks",
						"view_issues"
					],
					"public_permissions": [
						"view_wiki_pages",
						"view_wiki_links",
						"view_us",
						"view_project",
						"view_epics",
						"view_milestones",
						"view_tasks",
						"view_issues"
					],
					"is_featured": false,
					"is_looking_for_people": false,
					"looking_for_people_note": "",
					"blocked_code": null,
					"totals_updated_datetime": "2019-06-17T04:18:44.031Z",
					"total_fans": 0,
					"total_fans_last_week": 0,
					"total_fans_last_month": 0,
					"total_fans_last_year": 0,
					"total_activity": 2,
					"total_activity_last_week": 2,
					"total_activity_last_month": 2,
					"total_activity_last_year": 2,
					"tags": [],
					"tags_colors": {},
					"default_epic_status": 1496804,
					"default_points": 3943814,
					"default_us_status": 1961782,
					"default_task_status": 1635685,
					"default_priority": 983693,
					"default_severity": 1634252,
					"default_issue_status": 2293306,
					"default_issue_type": 988240,
					"my_permissions": [
						"view_wiki_pages",
						"view_wiki_links",
						"view_us",
						"view_project",
						"view_epics",
						"view_milestones",
						"view_tasks",
						"view_issues"
					],
					"i_am_owner": false,
					"i_am_admin": false,
					"i_am_member": false,
					"notify_level": null,
					"total_closed_milestones": 0,
					"is_watcher": false,
					"total_watchers": 1,
					"logo_small_url": null,
					"logo_big_url": null,
					"is_fan": false,
					"my_homepage": "{}"
				},
				{
					"id": 327107,
					"name": "test-project1",
					"slug": "pwn2own-test-project1",
					"description": "just for test",
					"created_date": "2019-06-17T02:36:07.564Z",
					"modified_date": "2019-06-17T02:36:07.966Z",
					"owner": {
						"username": "pwn2own",
						"full_name_display": "pwn2own",
						"photo": null,
						"big_photo": null,
						"gravatar_id": "e5bb8b84bc4febf506f64f4e9365cfad",
						"is_active": true,
						"id": 368793
					},
					"members": [
						368793
					],
					"total_milestones": null,
					"total_story_points": null,
					"is_contact_activated": true,
					"is_epics_activated": false,
					"is_backlog_activated": true,
					"is_kanban_activated": false,
					"is_wiki_activated": true,
					"is_issues_activated": true,
					"videoconferences": null,
					"videoconferences_extra_data": null,
					"creation_template": 1,
					"is_private": false,
					"anon_permissions": [
						"view_wiki_pages",
						"view_wiki_links",
						"view_us",
						"view_project",
						"view_epics",
						"view_milestones",
						"view_tasks",
						"view_issues"
					],
					"public_permissions": [
						"view_wiki_pages",
						"view_wiki_links",
						"view_us",
						"view_project",
						"view_epics",
						"view_milestones",
						"view_tasks",
						"view_issues"
					],
					"is_featured": false,
					"is_looking_for_people": false,
					"looking_for_people_note": "",
					"blocked_code": null,
					"totals_updated_datetime": "2019-06-17T02:36:08.195Z",
					"total_fans": 0,
					"total_fans_last_week": 0,
					"total_fans_last_month": 0,
					"total_fans_last_year": 0,
					"total_activity": 1,
					"total_activity_last_week": 1,
					"total_activity_last_month": 1,
					"total_activity_last_year": 1,
					"tags": [],
					"tags_colors": {},
					"default_epic_status": 1496794,
					"default_points": 3943790,
					"default_us_status": 1961770,
					"default_task_status": 1635675,
					"default_priority": 983687,
					"default_severity": 1634242,
					"default_issue_status": 2293292,
					"default_issue_type": 988234,
					"my_permissions": [
						"view_wiki_pages",
						"view_wiki_links",
						"view_us",
						"view_project",
						"view_epics",
						"view_milestones",
						"view_tasks",
						"view_issues"
					],
					"i_am_owner": false,
					"i_am_admin": false,
					"i_am_member": false,
					"notify_level": null,
					"total_closed_milestones": 0,
					"is_watcher": false,
					"total_watchers": 1,
					"logo_small_url": null,
					"logo_big_url": null,
					"is_fan": false,
					"my_homepage": "{}"
				}
			]`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/projects").
				Reply(200).
				JSON(projects)
		})

		It("get projects", func() {
			projects, err := client.GetProjects(context.Background(), "", "")
			Expect(err).To(BeNil())
			Expect(len(projects.Items)).To(Equal(2))
			Expect(projects.Items[0].Name).To(Equal("test-aaa"))
		})
	})

	Context("CreateProject", func() {
		const (
			project = `{
				"id": 327635,
				"name": "aaabbb",
				"slug": "pwn2own-aaabbb",
				"description": "xxx",
				"created_date": "2019-06-21T03:48:43.744Z",
				"modified_date": "2019-06-21T03:48:44.177Z",
				"owner": {
					"username": "pwn2own",
					"full_name_display": "pwn2own",
					"photo": null,
					"big_photo": null,
					"gravatar_id": "e5bb8b84bc4febf506f64f4e9365cfad",
					"is_active": true,
					"id": 368793
				},
				"members": [
					{
						"role": 1998528,
						"role_name": "Product Owner",
						"full_name": "pwn2own",
						"full_name_display": "pwn2own",
						"is_active": true,
						"id": 368793,
						"color": "#c2751e",
						"username": "pwn2own",
						"photo": null,
						"gravatar_id": "e5bb8b84bc4febf506f64f4e9365cfad"
					}
				],
				"total_milestones": null,
				"total_story_points": null,
				"is_contact_activated": true,
				"is_epics_activated": false,
				"is_backlog_activated": true,
				"is_kanban_activated": false,
				"is_wiki_activated": true,
				"is_issues_activated": true,
				"videoconferences": null,
				"videoconferences_extra_data": null,
				"creation_template": 1,
				"is_private": true,
				"anon_permissions": [],
				"public_permissions": [],
				"is_featured": false,
				"is_looking_for_people": false,
				"looking_for_people_note": "",
				"blocked_code": null,
				"totals_updated_datetime": "2019-06-21T03:48:43.756Z",
				"total_fans": 0,
				"total_fans_last_week": 0,
				"total_fans_last_month": 0,
				"total_fans_last_year": 0,
				"total_activity": 0,
				"total_activity_last_week": 0,
				"total_activity_last_month": 0,
				"total_activity_last_year": 0,
				"tags": [],
				"tags_colors": {},
				"default_epic_status": 1499441,
				"default_points": 3950171,
				"default_us_status": 1964949,
				"default_task_status": 1638292,
				"default_priority": 985275,
				"default_severity": 1636882,
				"default_issue_status": 2296992,
				"default_issue_type": 989826,
				"my_permissions": [
					"add_wiki_link",
					"view_us",
					"view_project",
					"delete_milestone",
					"view_tasks",
					"comment_issue",
					"comment_task",
					"add_task",
					"view_wiki_pages",
					"comment_wiki_page",
					"delete_wiki_page",
					"view_issues",
					"add_milestone",
					"modify_us",
					"modify_epic",
					"delete_task",
					"add_us",
					"modify_project",
					"delete_us",
					"delete_epic",
					"add_member",
					"add_issue",
					"modify_issue",
					"comment_us",
					"view_epics",
					"delete_wiki_link",
					"add_wiki_page",
					"admin_project_values",
					"comment_epic",
					"modify_wiki_page",
					"view_wiki_links",
					"modify_wiki_link",
					"delete_project",
					"modify_milestone",
					"modify_task",
					"add_epic",
					"view_milestones",
					"delete_issue",
					"admin_roles",
					"remove_member"
				],
				"i_am_owner": true,
				"i_am_admin": true,
				"i_am_member": true,
				"notify_level": 1,
				"total_closed_milestones": 0,
				"is_watcher": true,
				"total_watchers": 1,
				"logo_small_url": null,
				"logo_big_url": null,
				"is_fan": false,
				"my_homepage": false,
				"epic_statuses": [
					{
						"id": 1499441,
						"name": "New",
						"slug": "new",
						"order": 1,
						"is_closed": false,
						"color": "#999999",
						"project_id": 327635
					},
					{
						"id": 1499442,
						"name": "Ready",
						"slug": "ready",
						"order": 2,
						"is_closed": false,
						"color": "#ff8a84",
						"project_id": 327635
					},
					{
						"id": 1499443,
						"name": "In progress",
						"slug": "in-progress",
						"order": 3,
						"is_closed": false,
						"color": "#ff9900",
						"project_id": 327635
					},
					{
						"id": 1499444,
						"name": "Ready for test",
						"slug": "ready-for-test",
						"order": 4,
						"is_closed": false,
						"color": "#fcc000",
						"project_id": 327635
					},
					{
						"id": 1499445,
						"name": "Done",
						"slug": "done",
						"order": 5,
						"is_closed": true,
						"color": "#669900",
						"project_id": 327635
					}
				],
				"us_statuses": [
					{
						"id": 1964949,
						"name": "New",
						"order": 1,
						"is_closed": false,
						"project_id": 327635,
						"color": "#999999",
						"wip_limit": null,
						"slug": "new",
						"is_archived": false
					},
					{
						"id": 1964950,
						"name": "Ready",
						"order": 2,
						"is_closed": false,
						"project_id": 327635,
						"color": "#ff8a84",
						"wip_limit": null,
						"slug": "ready",
						"is_archived": false
					},
					{
						"id": 1964951,
						"name": "In progress",
						"order": 3,
						"is_closed": false,
						"project_id": 327635,
						"color": "#ff9900",
						"wip_limit": null,
						"slug": "in-progress",
						"is_archived": false
					},
					{
						"id": 1964952,
						"name": "Ready for test",
						"order": 4,
						"is_closed": false,
						"project_id": 327635,
						"color": "#fcc000",
						"wip_limit": null,
						"slug": "ready-for-test",
						"is_archived": false
					},
					{
						"id": 1964953,
						"name": "Done",
						"order": 5,
						"is_closed": true,
						"project_id": 327635,
						"color": "#669900",
						"wip_limit": null,
						"slug": "done",
						"is_archived": false
					},
					{
						"id": 1964954,
						"name": "Archived",
						"order": 6,
						"is_closed": true,
						"project_id": 327635,
						"color": "#5c3566",
						"wip_limit": null,
						"slug": "archived",
						"is_archived": true
					}
				],
				"us_duedates": [
					{
						"id": 137090,
						"name": "Default",
						"order": 1,
						"by_default": true,
						"color": "#9dce0a",
						"days_to_due": null,
						"project_id": 327635
					},
					{
						"id": 137091,
						"name": "Due soon",
						"order": 2,
						"by_default": false,
						"color": "#ff9900",
						"days_to_due": 14,
						"project_id": 327635
					},
					{
						"id": 137092,
						"name": "Past due",
						"order": 3,
						"by_default": false,
						"color": "#ff8a84",
						"days_to_due": 0,
						"project_id": 327635
					}
				],
				"points": [
					{
						"id": 3950171,
						"name": "?",
						"order": 1,
						"value": null,
						"project_id": 327635
					},
					{
						"id": 3950172,
						"name": "0",
						"order": 2,
						"value": 0,
						"project_id": 327635
					},
					{
						"id": 3950173,
						"name": "1/2",
						"order": 3,
						"value": 0.5,
						"project_id": 327635
					},
					{
						"id": 3950174,
						"name": "1",
						"order": 4,
						"value": 1,
						"project_id": 327635
					},
					{
						"id": 3950175,
						"name": "2",
						"order": 5,
						"value": 2,
						"project_id": 327635
					},
					{
						"id": 3950176,
						"name": "3",
						"order": 6,
						"value": 3,
						"project_id": 327635
					},
					{
						"id": 3950177,
						"name": "5",
						"order": 7,
						"value": 5,
						"project_id": 327635
					},
					{
						"id": 3950178,
						"name": "8",
						"order": 8,
						"value": 8,
						"project_id": 327635
					},
					{
						"id": 3950179,
						"name": "10",
						"order": 9,
						"value": 10,
						"project_id": 327635
					},
					{
						"id": 3950180,
						"name": "13",
						"order": 10,
						"value": 13,
						"project_id": 327635
					},
					{
						"id": 3950181,
						"name": "20",
						"order": 11,
						"value": 20,
						"project_id": 327635
					},
					{
						"id": 3950182,
						"name": "40",
						"order": 12,
						"value": 40,
						"project_id": 327635
					}
				],
				"task_statuses": [
					{
						"id": 1638292,
						"name": "New",
						"order": 1,
						"is_closed": false,
						"color": "#999999",
						"project_id": 327635,
						"slug": "new"
					},
					{
						"id": 1638293,
						"name": "In progress",
						"order": 2,
						"is_closed": false,
						"color": "#ff9900",
						"project_id": 327635,
						"slug": "in-progress"
					},
					{
						"id": 1638294,
						"name": "Ready for test",
						"order": 3,
						"is_closed": true,
						"color": "#ffcc00",
						"project_id": 327635,
						"slug": "ready-for-test"
					},
					{
						"id": 1638295,
						"name": "Closed",
						"order": 4,
						"is_closed": true,
						"color": "#669900",
						"project_id": 327635,
						"slug": "closed"
					},
					{
						"id": 1638296,
						"name": "Needs Info",
						"order": 5,
						"is_closed": false,
						"color": "#999999",
						"project_id": 327635,
						"slug": "needs-info"
					}
				],
				"task_duedates": [
					{
						"id": 137003,
						"name": "Default",
						"order": 1,
						"by_default": true,
						"color": "#9dce0a",
						"days_to_due": null,
						"project_id": 327635
					},
					{
						"id": 137004,
						"name": "Due soon",
						"order": 2,
						"by_default": false,
						"color": "#ff9900",
						"days_to_due": 14,
						"project_id": 327635
					},
					{
						"id": 137005,
						"name": "Past due",
						"order": 3,
						"by_default": false,
						"color": "#ff8a84",
						"days_to_due": 0,
						"project_id": 327635
					}
				],
				"issue_statuses": [
					{
						"id": 2296992,
						"name": "New",
						"order": 1,
						"is_closed": false,
						"project_id": 327635,
						"color": "#8C2318",
						"slug": "new"
					},
					{
						"id": 2296993,
						"name": "In progress",
						"order": 2,
						"is_closed": false,
						"project_id": 327635,
						"color": "#5E8C6A",
						"slug": "in-progress"
					},
					{
						"id": 2296994,
						"name": "Ready for test",
						"order": 3,
						"is_closed": true,
						"project_id": 327635,
						"color": "#88A65E",
						"slug": "ready-for-test"
					},
					{
						"id": 2296995,
						"name": "Closed",
						"order": 4,
						"is_closed": true,
						"project_id": 327635,
						"color": "#BFB35A",
						"slug": "closed"
					},
					{
						"id": 2296996,
						"name": "Needs Info",
						"order": 5,
						"is_closed": false,
						"project_id": 327635,
						"color": "#89BAB4",
						"slug": "needs-info"
					},
					{
						"id": 2296997,
						"name": "Rejected",
						"order": 6,
						"is_closed": true,
						"project_id": 327635,
						"color": "#CC0000",
						"slug": "rejected"
					},
					{
						"id": 2296998,
						"name": "Postponed",
						"order": 7,
						"is_closed": false,
						"project_id": 327635,
						"color": "#666666",
						"slug": "postponed"
					}
				],
				"issue_types": [
					{
						"id": 989826,
						"name": "Bug",
						"order": 1,
						"project_id": 327635,
						"color": "#89BAB4"
					},
					{
						"id": 989827,
						"name": "Question",
						"order": 2,
						"project_id": 327635,
						"color": "#ba89a8"
					},
					{
						"id": 989828,
						"name": "Enhancement",
						"order": 3,
						"project_id": 327635,
						"color": "#89a8ba"
					}
				],
				"issue_duedates": [
					{
						"id": 136978,
						"name": "Default",
						"order": 1,
						"by_default": true,
						"color": "#9dce0a",
						"days_to_due": null,
						"project_id": 327635
					},
					{
						"id": 136979,
						"name": "Due soon",
						"order": 2,
						"by_default": false,
						"color": "#ff9900",
						"days_to_due": 14,
						"project_id": 327635
					},
					{
						"id": 136980,
						"name": "Past due",
						"order": 3,
						"by_default": false,
						"color": "#ff8a84",
						"days_to_due": 0,
						"project_id": 327635
					}
				],
				"priorities": [
					{
						"id": 985274,
						"name": "Low",
						"order": 1,
						"project_id": 327635,
						"color": "#666666"
					},
					{
						"id": 985275,
						"name": "Normal",
						"order": 3,
						"project_id": 327635,
						"color": "#669933"
					},
					{
						"id": 985276,
						"name": "High",
						"order": 5,
						"project_id": 327635,
						"color": "#CC0000"
					}
				],
				"severities": [
					{
						"id": 1636880,
						"name": "Wishlist",
						"order": 1,
						"project_id": 327635,
						"color": "#666666"
					},
					{
						"id": 1636881,
						"name": "Minor",
						"order": 2,
						"project_id": 327635,
						"color": "#669933"
					},
					{
						"id": 1636882,
						"name": "Normal",
						"order": 3,
						"project_id": 327635,
						"color": "#0000FF"
					},
					{
						"id": 1636883,
						"name": "Important",
						"order": 4,
						"project_id": 327635,
						"color": "#FFA500"
					},
					{
						"id": 1636884,
						"name": "Critical",
						"order": 5,
						"project_id": 327635,
						"color": "#CC0000"
					}
				],
				"epic_custom_attributes": null,
				"userstory_custom_attributes": null,
				"task_custom_attributes": null,
				"issue_custom_attributes": null,
				"roles": [
					{
						"id": 1998524,
						"name": "UX",
						"slug": "ux",
						"order": 10,
						"computable": true,
						"project_id": 327635,
						"permissions": [
							"add_issue",
							"modify_issue",
							"delete_issue",
							"view_issues",
							"add_milestone",
							"modify_milestone",
							"delete_milestone",
							"view_milestones",
							"view_project",
							"add_task",
							"modify_task",
							"delete_task",
							"view_tasks",
							"add_us",
							"modify_us",
							"delete_us",
							"view_us",
							"add_wiki_page",
							"modify_wiki_page",
							"delete_wiki_page",
							"view_wiki_pages",
							"add_wiki_link",
							"delete_wiki_link",
							"view_wiki_links",
							"view_epics",
							"add_epic",
							"modify_epic",
							"delete_epic",
							"comment_epic",
							"comment_us",
							"comment_task",
							"comment_issue",
							"comment_wiki_page"
						]
					},
					{
						"id": 1998525,
						"name": "Design",
						"slug": "design",
						"order": 20,
						"computable": true,
						"project_id": 327635,
						"permissions": [
							"add_issue",
							"modify_issue",
							"delete_issue",
							"view_issues",
							"add_milestone",
							"modify_milestone",
							"delete_milestone",
							"view_milestones",
							"view_project",
							"add_task",
							"modify_task",
							"delete_task",
							"view_tasks",
							"add_us",
							"modify_us",
							"delete_us",
							"view_us",
							"add_wiki_page",
							"modify_wiki_page",
							"delete_wiki_page",
							"view_wiki_pages",
							"add_wiki_link",
							"delete_wiki_link",
							"view_wiki_links",
							"view_epics",
							"add_epic",
							"modify_epic",
							"delete_epic",
							"comment_epic",
							"comment_us",
							"comment_task",
							"comment_issue",
							"comment_wiki_page"
						]
					},
					{
						"id": 1998526,
						"name": "Front",
						"slug": "front",
						"order": 30,
						"computable": true,
						"project_id": 327635,
						"permissions": [
							"add_issue",
							"modify_issue",
							"delete_issue",
							"view_issues",
							"add_milestone",
							"modify_milestone",
							"delete_milestone",
							"view_milestones",
							"view_project",
							"add_task",
							"modify_task",
							"delete_task",
							"view_tasks",
							"add_us",
							"modify_us",
							"delete_us",
							"view_us",
							"add_wiki_page",
							"modify_wiki_page",
							"delete_wiki_page",
							"view_wiki_pages",
							"add_wiki_link",
							"delete_wiki_link",
							"view_wiki_links",
							"view_epics",
							"add_epic",
							"modify_epic",
							"delete_epic",
							"comment_epic",
							"comment_us",
							"comment_task",
							"comment_issue",
							"comment_wiki_page"
						]
					},
					{
						"id": 1998527,
						"name": "Back",
						"slug": "back",
						"order": 40,
						"computable": true,
						"project_id": 327635,
						"permissions": [
							"add_issue",
							"modify_issue",
							"delete_issue",
							"view_issues",
							"add_milestone",
							"modify_milestone",
							"delete_milestone",
							"view_milestones",
							"view_project",
							"add_task",
							"modify_task",
							"delete_task",
							"view_tasks",
							"add_us",
							"modify_us",
							"delete_us",
							"view_us",
							"add_wiki_page",
							"modify_wiki_page",
							"delete_wiki_page",
							"view_wiki_pages",
							"add_wiki_link",
							"delete_wiki_link",
							"view_wiki_links",
							"view_epics",
							"add_epic",
							"modify_epic",
							"delete_epic",
							"comment_epic",
							"comment_us",
							"comment_task",
							"comment_issue",
							"comment_wiki_page"
						]
					},
					{
						"id": 1998528,
						"name": "Product Owner",
						"slug": "product-owner",
						"order": 50,
						"computable": false,
						"project_id": 327635,
						"permissions": [
							"add_issue",
							"modify_issue",
							"delete_issue",
							"view_issues",
							"add_milestone",
							"modify_milestone",
							"delete_milestone",
							"view_milestones",
							"view_project",
							"add_task",
							"modify_task",
							"delete_task",
							"view_tasks",
							"add_us",
							"modify_us",
							"delete_us",
							"view_us",
							"add_wiki_page",
							"modify_wiki_page",
							"delete_wiki_page",
							"view_wiki_pages",
							"add_wiki_link",
							"delete_wiki_link",
							"view_wiki_links",
							"view_epics",
							"add_epic",
							"modify_epic",
							"delete_epic",
							"comment_epic",
							"comment_us",
							"comment_task",
							"comment_issue",
							"comment_wiki_page"
						]
					},
					{
						"id": 1998529,
						"name": "Stakeholder",
						"slug": "stakeholder",
						"order": 60,
						"computable": false,
						"project_id": 327635,
						"permissions": [
							"add_issue",
							"modify_issue",
							"delete_issue",
							"view_issues",
							"view_milestones",
							"view_project",
							"view_tasks",
							"view_us",
							"modify_wiki_page",
							"view_wiki_pages",
							"add_wiki_link",
							"delete_wiki_link",
							"view_wiki_links",
							"view_epics",
							"comment_epic",
							"comment_us",
							"comment_task",
							"comment_issue",
							"comment_wiki_page"
						]
					}
				],
				"total_memberships": 1,
				"is_out_of_owner_limits": false,
				"is_private_extra_info": {
					"can_be_updated": true,
					"reason": null
				},
				"max_memberships": 3,
				"epics_csv_uuid": null,
				"userstories_csv_uuid": null,
				"tasks_csv_uuid": null,
				"issues_csv_uuid": null,
				"transfer_token": null,
				"milestones": []
			}`
		)
		BeforeEach(func() {
			gock.New(host).
				Post("/projects").
				Reply(201).
				JSON(project)
		})

		It("create a project", func() {
			project, err := client.CreateProject(context.Background(), "aaabbb", "xxx", "", "")
			Expect(err).To(BeNil())
			Expect(project.Name).To(Equal("aaabbb"))
		})
	})

})
