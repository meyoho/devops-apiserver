package test

import (
	"context"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"
	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	v7 "alauda.io/devops-apiserver/pkg/devops-client/pkg/sonarqube/v7"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool Sonarqube tests", func() {
	var (
		// host
		host    = "https://sonarqube.test"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options

		transport  = gock.NewTransport()
		httpClient = clientv1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	const (
		component = `{
			"component": {
			  "organization": "my-org-1",
			  "id": "AVIF-FffA3Ax6PH2efPD",
			  "key": "com.sonarsource:java-markdown:src/main/java/com/sonarsource/markdown/impl/Rule.java",
			  "name": "Rule.java",
			  "qualifier": "FIL",
			  "language": "java",
			  "path": "src/main/java/com/sonarsource/markdown/impl/Rule.java",
			  "analysisDate": "2017-03-01T11:39:03+0000",
			  "leakPeriodDate": "2017-01-01T11:39:03+0000",
			  "version": "1.1"
			},
			"ancestors": [
			  {
				"organization": "my-org-1",
				"id": "AVIF-FfgA3Ax6PH2efPF",
				"key": "com.sonarsource:java-markdown:src/main/java/com/sonarsource/markdown/impl",
				"name": "src/main/java/com/sonarsource/markdown/impl",
				"qualifier": "DIR",
				"path": "src/main/java/com/sonarsource/markdown/impl",
				"analysisDate": "2017-03-01T11:39:03+0000",
				"version": "1.1"
			  },
			  {
				"organization": "my-org-1",
				"id": "AVIF98jgA3Ax6PH2efOW",
				"key": "com.sonarsource:java-markdown",
				"name": "Java Markdown",
				"description": "Java Markdown Project",
				"qualifier": "TRK",
				"analysisDate": "2017-03-01T11:39:03+0000",
				"version": "1.1",
				"tags": [
				  "language",
				  "plugin"
				],
				"visibility": "private"
			  }
			]
		  }`

		projectBranches = `{
			"branches": [
			  {
				"name": "feature/bar",
				"isMain": false,
				"type": "LONG",
				"status": {
				  "qualityGateStatus": "OK"
				},
				"analysisDate": "2017-04-01T01:15:42+0000"
			  },
			  {
				"name": "feature/foo",
				"isMain": false,
				"type": "SHORT",
				"mergeBranch": "feature/bar",
				"status": {
				  "qualityGateStatus": "OK",
				  "bugs": 1,
				  "vulnerabilities": 0,
				  "codeSmells": 0
				},
				"analysisDate": "2017-04-03T13:37:00+0000"
			  }
			]
		  }`

		projects = `{
			"paging": {
			  "pageIndex": 1,
			  "pageSize": 100,
			  "total": 2
			},
			"components": [
			  {
				"organization": "my-org-1",
				"id": "project-uuid-1",
				"key": "project-key-1",
				"name": "Project Name 1",
				"qualifier": "TRK",
				"visibility": "public",
				"lastAnalysisDate": "2017-03-01T11:39:03+0300"
			  },
			  {
				"organization": "my-org-1",
				"id": "project-uuid-2",
				"key": "project-key-2",
				"name": "Project Name 1",
				"qualifier": "TRK",
				"visibility": "private",
				"lastAnalysisDate": "2017-03-02T15:21:47+0300"
			  }
			]
		  }`

		measures = `{
			"component": {
			  "key": "MY_PROJECT:ElementImpl.java",
			  "name": "ElementImpl.java",
			  "qualifier": "FIL",
			  "language": "java",
			  "path": "src/main/java/com/sonarsource/markdown/impl/ElementImpl.java",
			  "measures": [
				{
				  "metric": "complexity",
				  "value": "12",
				  "periods": [
					{
					  "index": 1,
					  "value": "2"
					}
				  ]
				},
				{
				  "metric": "new_violations",
				  "periods": [
					{
					  "index": 1,
					  "value": "25"
					}
				  ]
				},
				{
				  "metric": "ncloc",
				  "value": "114",
				  "periods": [
					{
					  "index": 1,
					  "value": "3"
					}
				  ]
				}
			  ]
			},
			"metrics": [
			  {
				"key": "complexity",
				"name": "Complexity",
				"description": "Cyclomatic complexity",
				"domain": "Complexity",
				"type": "INT",
				"higherValuesAreBetter": false,
				"qualitative": false,
				"hidden": false,
				"custom": false
			  },
			  {
				"key": "ncloc",
				"name": "Lines of code",
				"description": "Non Commenting Lines of Code",
				"domain": "Size",
				"type": "INT",
				"higherValuesAreBetter": false,
				"qualitative": false,
				"hidden": false,
				"custom": false
			  },
			  {
				"key": "new_violations",
				"name": "New issues",
				"description": "New Issues",
				"domain": "Issues",
				"type": "INT",
				"higherValuesAreBetter": false,
				"qualitative": true,
				"hidden": false,
				"custom": false
			  }
			],
			"periods": [
			  {
				"index": 1,
				"mode": "previous_version",
				"date": "2016-01-11T10:49:50+0000",
				"parameter": "1.0-SNAPSHOT"
			  }
			]
		  }`

		qualitygates = `{
			"qualityGate": {
			  "id": "23",
			  "name": "My team QG",
			  "default": false
			}
		  }`
	)

	BeforeEach(func() {
		factory = v7.NewClient()
		opts = clientv1.NewOptions(clientv1.NewBasicConfig("sonarqube.test", "", []string{}), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		gock.OffAll()
		Expect(gock.IsDone()).To(Equal(true))
	})

	JustBeforeEach(func() {
		gock.New(host).
			Get("/components/show").
			MatchParam("component", "my-project").
			Reply(200).
			JSON(component)

		gock.New(host).
			Get("/project_branches/list").
			MatchParam("project", "my-project").
			Reply(200).
			JSON(projectBranches)

		gock.New(host).
			Get("/projects/search").
			Reply(200).
			JSON(projects)

		gock.New(host).
			Get("/measures/component").
			Reply(200).
			JSON(measures)

		gock.New(host).
			Get("/qualitygates/get_by_project").
			MatchParam("project", "my_project").
			Reply(200).
			JSON(qualitygates)
	})

	Context("GetProjectReport", func() {
		It("get project report", func() {
			conds, err := client.GetProjectReport(context.Background(), "my-project")
			Expect(err).To(BeNil())
			Expect(len(conds)).ToNot(Equal(0))
		})
	})

	Context("GetCorrespondCodeQualityProjects", func() {
		It("get correspond projects", func() {
			repoList := &v1.CodeRepositoryList{
				Items: []v1.CodeRepository{
					{},
				},
			}
			binding := v1.CodeQualityBinding{}
			_, err := client.GetCorrespondCodeQualityProjects(context.Background(), repoList, &binding)
			Expect(err).To(BeNil())
		})
	})

	Context("CheckProjectExist", func() {
		It("check project exist", func() {
			exist, err := client.CheckProjectExist(context.Background(), "my-project")
			Expect(err).To(BeNil())
			Expect(exist).To(Equal(true))

			exist, err = client.CheckProjectExist(context.Background(), "my-project-1")
			Expect(err).ToNot(BeNil())
		})
	})

	Context("GetLastAnalysisDate", func() {
		It("get last analysis date", func() {
			t, err := client.GetLastAnalysisDate(context.Background(), "my-project")
			Expect(err).To(BeNil())
			Expect(t).ToNot(BeNil())
		})
	})

})
