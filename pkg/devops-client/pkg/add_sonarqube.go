package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v7 "alauda.io/devops-apiserver/pkg/devops-client/pkg/sonarqube/v7"
)

func init() {
	register(devops.CodeQualityToolTypeSonarqube.String(), versionOpt{
		version:   "v7",
		factory:   v7.NewClient(),
		isDefault: true,
	})
}
