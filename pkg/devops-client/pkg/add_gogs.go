package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v0_11_86 "alauda.io/devops-apiserver/pkg/devops-client/pkg/gogs/v0_11_86"
)

func init() {
	register(devops.CodeRepoServiceTypeGogs.String(), versionOpt{
		version:   "v0.11.86",
		factory:   v0_11_86.NewClient(),
		isDefault: true,
	})
}
