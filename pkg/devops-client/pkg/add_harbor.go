package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/harbor/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/harbor/v1_8"
)

func init() {
	register(devops.RegistryTypeHarbor.String(), versionOpt{
		version:   "v1",
		factory:   v1.NewClient(),
		isDefault: false,
	})

	register(devops.RegistryTypeHarbor.String(), versionOpt{
		version:   "v1.8",
		factory:   v1_8.NewClient(),
		isDefault: true,
	})
}
