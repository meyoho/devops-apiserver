// Code generated by go-swagger; DO NOT EDIT.

package internal

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// GetV4InternalDiscoverReader is a Reader for the GetV4InternalDiscover structure.
type GetV4InternalDiscoverReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4InternalDiscoverReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4InternalDiscoverOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4InternalDiscoverOK creates a GetV4InternalDiscoverOK with default headers values
func NewGetV4InternalDiscoverOK() *GetV4InternalDiscoverOK {
	return &GetV4InternalDiscoverOK{}
}

/*GetV4InternalDiscoverOK handles this case with default header values.

get Discover(s)
*/
type GetV4InternalDiscoverOK struct {
}

func (o *GetV4InternalDiscoverOK) Error() string {
	return fmt.Sprintf("[GET /v4/internal/discover][%d] getV4InternalDiscoverOK ", 200)
}

func (o *GetV4InternalDiscoverOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
