// Code generated by go-swagger; DO NOT EDIT.

package sidekiq

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4SidekiqQueueMetricsParams creates a new GetV4SidekiqQueueMetricsParams object
// with the default values initialized.
func NewGetV4SidekiqQueueMetricsParams() *GetV4SidekiqQueueMetricsParams {

	return &GetV4SidekiqQueueMetricsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4SidekiqQueueMetricsParamsWithTimeout creates a new GetV4SidekiqQueueMetricsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4SidekiqQueueMetricsParamsWithTimeout(timeout time.Duration) *GetV4SidekiqQueueMetricsParams {

	return &GetV4SidekiqQueueMetricsParams{

		timeout: timeout,
	}
}

// NewGetV4SidekiqQueueMetricsParamsWithContext creates a new GetV4SidekiqQueueMetricsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4SidekiqQueueMetricsParamsWithContext(ctx context.Context) *GetV4SidekiqQueueMetricsParams {

	return &GetV4SidekiqQueueMetricsParams{

		Context: ctx,
	}
}

// NewGetV4SidekiqQueueMetricsParamsWithHTTPClient creates a new GetV4SidekiqQueueMetricsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4SidekiqQueueMetricsParamsWithHTTPClient(client *http.Client) *GetV4SidekiqQueueMetricsParams {

	return &GetV4SidekiqQueueMetricsParams{
		HTTPClient: client,
	}
}

/*GetV4SidekiqQueueMetricsParams contains all the parameters to send to the API endpoint
for the get v4 sidekiq queue metrics operation typically these are written to a http.Request
*/
type GetV4SidekiqQueueMetricsParams struct {
	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 sidekiq queue metrics params
func (o *GetV4SidekiqQueueMetricsParams) WithTimeout(timeout time.Duration) *GetV4SidekiqQueueMetricsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 sidekiq queue metrics params
func (o *GetV4SidekiqQueueMetricsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 sidekiq queue metrics params
func (o *GetV4SidekiqQueueMetricsParams) WithContext(ctx context.Context) *GetV4SidekiqQueueMetricsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 sidekiq queue metrics params
func (o *GetV4SidekiqQueueMetricsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 sidekiq queue metrics params
func (o *GetV4SidekiqQueueMetricsParams) WithHTTPClient(client *http.Client) *GetV4SidekiqQueueMetricsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 sidekiq queue metrics params
func (o *GetV4SidekiqQueueMetricsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4SidekiqQueueMetricsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
