// Code generated by go-swagger; DO NOT EDIT.

package deploy_keys

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new deploy keys API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for deploy keys API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
GetV4DeployKeys get v4 deploy keys API
*/
func (a *Client) GetV4DeployKeys(params *GetV4DeployKeysParams, authInfo runtime.ClientAuthInfoWriter) (*GetV4DeployKeysOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetV4DeployKeysParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getV4DeployKeys",
		Method:             "GET",
		PathPattern:        "/v4/deploy_keys",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &GetV4DeployKeysReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetV4DeployKeysOK), nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
