// Code generated by go-swagger; DO NOT EDIT.

package groups

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// GetV4GroupsIDMembersUserIDReader is a Reader for the GetV4GroupsIDMembersUserID structure.
type GetV4GroupsIDMembersUserIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4GroupsIDMembersUserIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4GroupsIDMembersUserIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4GroupsIDMembersUserIDOK creates a GetV4GroupsIDMembersUserIDOK with default headers values
func NewGetV4GroupsIDMembersUserIDOK() *GetV4GroupsIDMembersUserIDOK {
	return &GetV4GroupsIDMembersUserIDOK{}
}

/*GetV4GroupsIDMembersUserIDOK handles this case with default header values.

Gets a member of a group or project.
*/
type GetV4GroupsIDMembersUserIDOK struct {
	Payload *models.Member
}

func (o *GetV4GroupsIDMembersUserIDOK) Error() string {
	return fmt.Sprintf("[GET /v4/groups/{id}/members/{user_id}][%d] getV4GroupsIdMembersUserIdOK  %+v", 200, o.Payload)
}

func (o *GetV4GroupsIDMembersUserIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Member)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
