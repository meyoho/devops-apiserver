// Code generated by go-swagger; DO NOT EDIT.

package groups

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewDeleteV4GroupsIDMembersUserIDParams creates a new DeleteV4GroupsIDMembersUserIDParams object
// with the default values initialized.
func NewDeleteV4GroupsIDMembersUserIDParams() *DeleteV4GroupsIDMembersUserIDParams {
	var ()
	return &DeleteV4GroupsIDMembersUserIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewDeleteV4GroupsIDMembersUserIDParamsWithTimeout creates a new DeleteV4GroupsIDMembersUserIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewDeleteV4GroupsIDMembersUserIDParamsWithTimeout(timeout time.Duration) *DeleteV4GroupsIDMembersUserIDParams {
	var ()
	return &DeleteV4GroupsIDMembersUserIDParams{

		timeout: timeout,
	}
}

// NewDeleteV4GroupsIDMembersUserIDParamsWithContext creates a new DeleteV4GroupsIDMembersUserIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewDeleteV4GroupsIDMembersUserIDParamsWithContext(ctx context.Context) *DeleteV4GroupsIDMembersUserIDParams {
	var ()
	return &DeleteV4GroupsIDMembersUserIDParams{

		Context: ctx,
	}
}

// NewDeleteV4GroupsIDMembersUserIDParamsWithHTTPClient creates a new DeleteV4GroupsIDMembersUserIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewDeleteV4GroupsIDMembersUserIDParamsWithHTTPClient(client *http.Client) *DeleteV4GroupsIDMembersUserIDParams {
	var ()
	return &DeleteV4GroupsIDMembersUserIDParams{
		HTTPClient: client,
	}
}

/*DeleteV4GroupsIDMembersUserIDParams contains all the parameters to send to the API endpoint
for the delete v4 groups Id members user Id operation typically these are written to a http.Request
*/
type DeleteV4GroupsIDMembersUserIDParams struct {

	/*ID
	  The group ID

	*/
	ID string
	/*UserID
	  The user ID of the member

	*/
	UserID int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the delete v4 groups Id members user Id params
func (o *DeleteV4GroupsIDMembersUserIDParams) WithTimeout(timeout time.Duration) *DeleteV4GroupsIDMembersUserIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete v4 groups Id members user Id params
func (o *DeleteV4GroupsIDMembersUserIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the delete v4 groups Id members user Id params
func (o *DeleteV4GroupsIDMembersUserIDParams) WithContext(ctx context.Context) *DeleteV4GroupsIDMembersUserIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete v4 groups Id members user Id params
func (o *DeleteV4GroupsIDMembersUserIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete v4 groups Id members user Id params
func (o *DeleteV4GroupsIDMembersUserIDParams) WithHTTPClient(client *http.Client) *DeleteV4GroupsIDMembersUserIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete v4 groups Id members user Id params
func (o *DeleteV4GroupsIDMembersUserIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the delete v4 groups Id members user Id params
func (o *DeleteV4GroupsIDMembersUserIDParams) WithID(id string) *DeleteV4GroupsIDMembersUserIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the delete v4 groups Id members user Id params
func (o *DeleteV4GroupsIDMembersUserIDParams) SetID(id string) {
	o.ID = id
}

// WithUserID adds the userID to the delete v4 groups Id members user Id params
func (o *DeleteV4GroupsIDMembersUserIDParams) WithUserID(userID int32) *DeleteV4GroupsIDMembersUserIDParams {
	o.SetUserID(userID)
	return o
}

// SetUserID adds the userId to the delete v4 groups Id members user Id params
func (o *DeleteV4GroupsIDMembersUserIDParams) SetUserID(userID int32) {
	o.UserID = userID
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteV4GroupsIDMembersUserIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	// path param user_id
	if err := r.SetPathParam("user_id", swag.FormatInt32(o.UserID)); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
