// Code generated by go-swagger; DO NOT EDIT.

package groups

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostV4GroupsParams creates a new PostV4GroupsParams object
// with the default values initialized.
func NewPostV4GroupsParams() *PostV4GroupsParams {
	var ()
	return &PostV4GroupsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostV4GroupsParamsWithTimeout creates a new PostV4GroupsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostV4GroupsParamsWithTimeout(timeout time.Duration) *PostV4GroupsParams {
	var ()
	return &PostV4GroupsParams{

		timeout: timeout,
	}
}

// NewPostV4GroupsParamsWithContext creates a new PostV4GroupsParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostV4GroupsParamsWithContext(ctx context.Context) *PostV4GroupsParams {
	var ()
	return &PostV4GroupsParams{

		Context: ctx,
	}
}

// NewPostV4GroupsParamsWithHTTPClient creates a new PostV4GroupsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostV4GroupsParamsWithHTTPClient(client *http.Client) *PostV4GroupsParams {
	var ()
	return &PostV4GroupsParams{
		HTTPClient: client,
	}
}

/*PostV4GroupsParams contains all the parameters to send to the API endpoint
for the post v4 groups operation typically these are written to a http.Request
*/
type PostV4GroupsParams struct {

	/*Description
	  The description of the group

	*/
	Description *string
	/*LfsEnabled
	  Enable/disable LFS for the projects in this group

	*/
	LfsEnabled *bool
	/*Name
	  The name of the group

	*/
	Name string
	/*ParentID
	  parent id

	*/
	ParentID *int64
	/*Path
	  The path of the group

	*/
	Path string
	/*RequestAccessEnabled
	  Allow users to request member access

	*/
	RequestAccessEnabled *bool
	/*VisibilityLevel
	  The visibility level of the group

	*/
	VisibilityLevel *int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post v4 groups params
func (o *PostV4GroupsParams) WithTimeout(timeout time.Duration) *PostV4GroupsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post v4 groups params
func (o *PostV4GroupsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post v4 groups params
func (o *PostV4GroupsParams) WithContext(ctx context.Context) *PostV4GroupsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post v4 groups params
func (o *PostV4GroupsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post v4 groups params
func (o *PostV4GroupsParams) WithHTTPClient(client *http.Client) *PostV4GroupsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post v4 groups params
func (o *PostV4GroupsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithDescription adds the description to the post v4 groups params
func (o *PostV4GroupsParams) WithDescription(description *string) *PostV4GroupsParams {
	o.SetDescription(description)
	return o
}

// SetDescription adds the description to the post v4 groups params
func (o *PostV4GroupsParams) SetDescription(description *string) {
	o.Description = description
}

// WithLfsEnabled adds the lfsEnabled to the post v4 groups params
func (o *PostV4GroupsParams) WithLfsEnabled(lfsEnabled *bool) *PostV4GroupsParams {
	o.SetLfsEnabled(lfsEnabled)
	return o
}

// SetLfsEnabled adds the lfsEnabled to the post v4 groups params
func (o *PostV4GroupsParams) SetLfsEnabled(lfsEnabled *bool) {
	o.LfsEnabled = lfsEnabled
}

// WithName adds the name to the post v4 groups params
func (o *PostV4GroupsParams) WithName(name string) *PostV4GroupsParams {
	o.SetName(name)
	return o
}

// SetName adds the name to the post v4 groups params
func (o *PostV4GroupsParams) SetName(name string) {
	o.Name = name
}

// WithParentID adds the parentID to the post v4 groups params
func (o *PostV4GroupsParams) WithParentID(parentID *int64) *PostV4GroupsParams {
	o.SetParentID(parentID)
	return o
}

// SetParentID adds the parentId to the post v4 groups params
func (o *PostV4GroupsParams) SetParentID(parentID *int64) {
	o.ParentID = parentID
}

// WithPath adds the path to the post v4 groups params
func (o *PostV4GroupsParams) WithPath(path string) *PostV4GroupsParams {
	o.SetPath(path)
	return o
}

// SetPath adds the path to the post v4 groups params
func (o *PostV4GroupsParams) SetPath(path string) {
	o.Path = path
}

// WithRequestAccessEnabled adds the requestAccessEnabled to the post v4 groups params
func (o *PostV4GroupsParams) WithRequestAccessEnabled(requestAccessEnabled *bool) *PostV4GroupsParams {
	o.SetRequestAccessEnabled(requestAccessEnabled)
	return o
}

// SetRequestAccessEnabled adds the requestAccessEnabled to the post v4 groups params
func (o *PostV4GroupsParams) SetRequestAccessEnabled(requestAccessEnabled *bool) {
	o.RequestAccessEnabled = requestAccessEnabled
}

// WithVisibilityLevel adds the visibilityLevel to the post v4 groups params
func (o *PostV4GroupsParams) WithVisibilityLevel(visibilityLevel *int32) *PostV4GroupsParams {
	o.SetVisibilityLevel(visibilityLevel)
	return o
}

// SetVisibilityLevel adds the visibilityLevel to the post v4 groups params
func (o *PostV4GroupsParams) SetVisibilityLevel(visibilityLevel *int32) {
	o.VisibilityLevel = visibilityLevel
}

// WriteToRequest writes these params to a swagger request
func (o *PostV4GroupsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Description != nil {

		// form param description
		var frDescription string
		if o.Description != nil {
			frDescription = *o.Description
		}
		fDescription := frDescription
		if fDescription != "" {
			if err := r.SetFormParam("description", fDescription); err != nil {
				return err
			}
		}

	}

	if o.LfsEnabled != nil {

		// form param lfs_enabled
		var frLfsEnabled bool
		if o.LfsEnabled != nil {
			frLfsEnabled = *o.LfsEnabled
		}
		fLfsEnabled := swag.FormatBool(frLfsEnabled)
		if fLfsEnabled != "" {
			if err := r.SetFormParam("lfs_enabled", fLfsEnabled); err != nil {
				return err
			}
		}

	}

	// form param name
	frName := o.Name
	fName := frName
	if fName != "" {
		if err := r.SetFormParam("name", fName); err != nil {
			return err
		}
	}

	if o.ParentID != nil {

		// form param parent_id
		var frParentID int64
		if o.ParentID != nil {
			frParentID = *o.ParentID
		}
		fParentID := swag.FormatInt64(frParentID)
		if fParentID != "" {
			if err := r.SetFormParam("parent_id", fParentID); err != nil {
				return err
			}
		}

	}

	// form param path
	frPath := o.Path
	fPath := frPath
	if fPath != "" {
		if err := r.SetFormParam("path", fPath); err != nil {
			return err
		}
	}

	if o.RequestAccessEnabled != nil {

		// form param request_access_enabled
		var frRequestAccessEnabled bool
		if o.RequestAccessEnabled != nil {
			frRequestAccessEnabled = *o.RequestAccessEnabled
		}
		fRequestAccessEnabled := swag.FormatBool(frRequestAccessEnabled)
		if fRequestAccessEnabled != "" {
			if err := r.SetFormParam("request_access_enabled", fRequestAccessEnabled); err != nil {
				return err
			}
		}

	}

	if o.VisibilityLevel != nil {

		// form param visibility_level
		var frVisibilityLevel int32
		if o.VisibilityLevel != nil {
			frVisibilityLevel = *o.VisibilityLevel
		}
		fVisibilityLevel := swag.FormatInt32(frVisibilityLevel)
		if fVisibilityLevel != "" {
			if err := r.SetFormParam("visibility_level", fVisibilityLevel); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
