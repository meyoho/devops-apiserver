// Code generated by go-swagger; DO NOT EDIT.

package snippets

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new snippets API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for snippets API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
DeleteV4SnippetsID removes snippet

This feature was introduced in GitLab 8.15.
*/
func (a *Client) DeleteV4SnippetsID(params *DeleteV4SnippetsIDParams, authInfo runtime.ClientAuthInfoWriter) (*DeleteV4SnippetsIDOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewDeleteV4SnippetsIDParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "deleteV4SnippetsId",
		Method:             "DELETE",
		PathPattern:        "/v4/snippets/{id}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &DeleteV4SnippetsIDReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*DeleteV4SnippetsIDOK), nil

}

/*
GetV4Snippets gets a snippets list for authenticated user

This feature was introduced in GitLab 8.15.
*/
func (a *Client) GetV4Snippets(params *GetV4SnippetsParams, authInfo runtime.ClientAuthInfoWriter) (*GetV4SnippetsOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetV4SnippetsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getV4Snippets",
		Method:             "GET",
		PathPattern:        "/v4/snippets",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &GetV4SnippetsReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetV4SnippetsOK), nil

}

/*
GetV4SnippetsID gets a single snippet

This feature was introduced in GitLab 8.15.
*/
func (a *Client) GetV4SnippetsID(params *GetV4SnippetsIDParams, authInfo runtime.ClientAuthInfoWriter) (*GetV4SnippetsIDOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetV4SnippetsIDParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getV4SnippetsId",
		Method:             "GET",
		PathPattern:        "/v4/snippets/{id}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &GetV4SnippetsIDReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetV4SnippetsIDOK), nil

}

/*
GetV4SnippetsIDRaw gets a raw snippet

This feature was introduced in GitLab 8.15.
*/
func (a *Client) GetV4SnippetsIDRaw(params *GetV4SnippetsIDRawParams, authInfo runtime.ClientAuthInfoWriter) (*GetV4SnippetsIDRawOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetV4SnippetsIDRawParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getV4SnippetsIdRaw",
		Method:             "GET",
		PathPattern:        "/v4/snippets/{id}/raw",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &GetV4SnippetsIDRawReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetV4SnippetsIDRawOK), nil

}

/*
GetV4SnippetsPublic lists all public snippets current user has access to

This feature was introduced in GitLab 8.15.
*/
func (a *Client) GetV4SnippetsPublic(params *GetV4SnippetsPublicParams, authInfo runtime.ClientAuthInfoWriter) (*GetV4SnippetsPublicOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetV4SnippetsPublicParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getV4SnippetsPublic",
		Method:             "GET",
		PathPattern:        "/v4/snippets/public",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &GetV4SnippetsPublicReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetV4SnippetsPublicOK), nil

}

/*
PostV4Snippets creates new snippet

This feature was introduced in GitLab 8.15.
*/
func (a *Client) PostV4Snippets(params *PostV4SnippetsParams, authInfo runtime.ClientAuthInfoWriter) (*PostV4SnippetsCreated, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPostV4SnippetsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "postV4Snippets",
		Method:             "POST",
		PathPattern:        "/v4/snippets",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &PostV4SnippetsReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*PostV4SnippetsCreated), nil

}

/*
PutV4SnippetsID updates an existing snippet

This feature was introduced in GitLab 8.15.
*/
func (a *Client) PutV4SnippetsID(params *PutV4SnippetsIDParams, authInfo runtime.ClientAuthInfoWriter) (*PutV4SnippetsIDOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewPutV4SnippetsIDParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "putV4SnippetsId",
		Method:             "PUT",
		PathPattern:        "/v4/snippets/{id}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &PutV4SnippetsIDReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*PutV4SnippetsIDOK), nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
