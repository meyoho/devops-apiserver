// Code generated by go-swagger; DO NOT EDIT.

package templates

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4TemplatesLicensesNameParams creates a new GetV4TemplatesLicensesNameParams object
// with the default values initialized.
func NewGetV4TemplatesLicensesNameParams() *GetV4TemplatesLicensesNameParams {
	var ()
	return &GetV4TemplatesLicensesNameParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4TemplatesLicensesNameParamsWithTimeout creates a new GetV4TemplatesLicensesNameParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4TemplatesLicensesNameParamsWithTimeout(timeout time.Duration) *GetV4TemplatesLicensesNameParams {
	var ()
	return &GetV4TemplatesLicensesNameParams{

		timeout: timeout,
	}
}

// NewGetV4TemplatesLicensesNameParamsWithContext creates a new GetV4TemplatesLicensesNameParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4TemplatesLicensesNameParamsWithContext(ctx context.Context) *GetV4TemplatesLicensesNameParams {
	var ()
	return &GetV4TemplatesLicensesNameParams{

		Context: ctx,
	}
}

// NewGetV4TemplatesLicensesNameParamsWithHTTPClient creates a new GetV4TemplatesLicensesNameParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4TemplatesLicensesNameParamsWithHTTPClient(client *http.Client) *GetV4TemplatesLicensesNameParams {
	var ()
	return &GetV4TemplatesLicensesNameParams{
		HTTPClient: client,
	}
}

/*GetV4TemplatesLicensesNameParams contains all the parameters to send to the API endpoint
for the get v4 templates licenses name operation typically these are written to a http.Request
*/
type GetV4TemplatesLicensesNameParams struct {

	/*Name
	  The name of the template

	*/
	Name string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 templates licenses name params
func (o *GetV4TemplatesLicensesNameParams) WithTimeout(timeout time.Duration) *GetV4TemplatesLicensesNameParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 templates licenses name params
func (o *GetV4TemplatesLicensesNameParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 templates licenses name params
func (o *GetV4TemplatesLicensesNameParams) WithContext(ctx context.Context) *GetV4TemplatesLicensesNameParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 templates licenses name params
func (o *GetV4TemplatesLicensesNameParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 templates licenses name params
func (o *GetV4TemplatesLicensesNameParams) WithHTTPClient(client *http.Client) *GetV4TemplatesLicensesNameParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 templates licenses name params
func (o *GetV4TemplatesLicensesNameParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithName adds the name to the get v4 templates licenses name params
func (o *GetV4TemplatesLicensesNameParams) WithName(name string) *GetV4TemplatesLicensesNameParams {
	o.SetName(name)
	return o
}

// SetName adds the name to the get v4 templates licenses name params
func (o *GetV4TemplatesLicensesNameParams) SetName(name string) {
	o.Name = name
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4TemplatesLicensesNameParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param name
	if err := r.SetPathParam("name", o.Name); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
