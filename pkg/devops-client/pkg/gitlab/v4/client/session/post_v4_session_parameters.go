// Code generated by go-swagger; DO NOT EDIT.

package session

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostV4SessionParams creates a new PostV4SessionParams object
// with the default values initialized.
func NewPostV4SessionParams() *PostV4SessionParams {
	var ()
	return &PostV4SessionParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostV4SessionParamsWithTimeout creates a new PostV4SessionParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostV4SessionParamsWithTimeout(timeout time.Duration) *PostV4SessionParams {
	var ()
	return &PostV4SessionParams{

		timeout: timeout,
	}
}

// NewPostV4SessionParamsWithContext creates a new PostV4SessionParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostV4SessionParamsWithContext(ctx context.Context) *PostV4SessionParams {
	var ()
	return &PostV4SessionParams{

		Context: ctx,
	}
}

// NewPostV4SessionParamsWithHTTPClient creates a new PostV4SessionParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostV4SessionParamsWithHTTPClient(client *http.Client) *PostV4SessionParams {
	var ()
	return &PostV4SessionParams{
		HTTPClient: client,
	}
}

/*PostV4SessionParams contains all the parameters to send to the API endpoint
for the post v4 session operation typically these are written to a http.Request
*/
type PostV4SessionParams struct {

	/*Email
	  The email of the user

	*/
	Email *string
	/*Login
	  The username

	*/
	Login *string
	/*Password
	  The password of the user

	*/
	Password string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post v4 session params
func (o *PostV4SessionParams) WithTimeout(timeout time.Duration) *PostV4SessionParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post v4 session params
func (o *PostV4SessionParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post v4 session params
func (o *PostV4SessionParams) WithContext(ctx context.Context) *PostV4SessionParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post v4 session params
func (o *PostV4SessionParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post v4 session params
func (o *PostV4SessionParams) WithHTTPClient(client *http.Client) *PostV4SessionParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post v4 session params
func (o *PostV4SessionParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithEmail adds the email to the post v4 session params
func (o *PostV4SessionParams) WithEmail(email *string) *PostV4SessionParams {
	o.SetEmail(email)
	return o
}

// SetEmail adds the email to the post v4 session params
func (o *PostV4SessionParams) SetEmail(email *string) {
	o.Email = email
}

// WithLogin adds the login to the post v4 session params
func (o *PostV4SessionParams) WithLogin(login *string) *PostV4SessionParams {
	o.SetLogin(login)
	return o
}

// SetLogin adds the login to the post v4 session params
func (o *PostV4SessionParams) SetLogin(login *string) {
	o.Login = login
}

// WithPassword adds the password to the post v4 session params
func (o *PostV4SessionParams) WithPassword(password string) *PostV4SessionParams {
	o.SetPassword(password)
	return o
}

// SetPassword adds the password to the post v4 session params
func (o *PostV4SessionParams) SetPassword(password string) {
	o.Password = password
}

// WriteToRequest writes these params to a swagger request
func (o *PostV4SessionParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Email != nil {

		// form param email
		var frEmail string
		if o.Email != nil {
			frEmail = *o.Email
		}
		fEmail := frEmail
		if fEmail != "" {
			if err := r.SetFormParam("email", fEmail); err != nil {
				return err
			}
		}

	}

	if o.Login != nil {

		// form param login
		var frLogin string
		if o.Login != nil {
			frLogin = *o.Login
		}
		fLogin := frLogin
		if fLogin != "" {
			if err := r.SetFormParam("login", fLogin); err != nil {
				return err
			}
		}

	}

	// form param password
	frPassword := o.Password
	fPassword := frPassword
	if fPassword != "" {
		if err := r.SetFormParam("password", fPassword); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
