// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPutV4UsersIDUnblockParams creates a new PutV4UsersIDUnblockParams object
// with the default values initialized.
func NewPutV4UsersIDUnblockParams() *PutV4UsersIDUnblockParams {
	var ()
	return &PutV4UsersIDUnblockParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPutV4UsersIDUnblockParamsWithTimeout creates a new PutV4UsersIDUnblockParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPutV4UsersIDUnblockParamsWithTimeout(timeout time.Duration) *PutV4UsersIDUnblockParams {
	var ()
	return &PutV4UsersIDUnblockParams{

		timeout: timeout,
	}
}

// NewPutV4UsersIDUnblockParamsWithContext creates a new PutV4UsersIDUnblockParams object
// with the default values initialized, and the ability to set a context for a request
func NewPutV4UsersIDUnblockParamsWithContext(ctx context.Context) *PutV4UsersIDUnblockParams {
	var ()
	return &PutV4UsersIDUnblockParams{

		Context: ctx,
	}
}

// NewPutV4UsersIDUnblockParamsWithHTTPClient creates a new PutV4UsersIDUnblockParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPutV4UsersIDUnblockParamsWithHTTPClient(client *http.Client) *PutV4UsersIDUnblockParams {
	var ()
	return &PutV4UsersIDUnblockParams{
		HTTPClient: client,
	}
}

/*PutV4UsersIDUnblockParams contains all the parameters to send to the API endpoint
for the put v4 users Id unblock operation typically these are written to a http.Request
*/
type PutV4UsersIDUnblockParams struct {

	/*ID
	  The ID of the user

	*/
	ID int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the put v4 users Id unblock params
func (o *PutV4UsersIDUnblockParams) WithTimeout(timeout time.Duration) *PutV4UsersIDUnblockParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put v4 users Id unblock params
func (o *PutV4UsersIDUnblockParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put v4 users Id unblock params
func (o *PutV4UsersIDUnblockParams) WithContext(ctx context.Context) *PutV4UsersIDUnblockParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put v4 users Id unblock params
func (o *PutV4UsersIDUnblockParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put v4 users Id unblock params
func (o *PutV4UsersIDUnblockParams) WithHTTPClient(client *http.Client) *PutV4UsersIDUnblockParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put v4 users Id unblock params
func (o *PutV4UsersIDUnblockParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the put v4 users Id unblock params
func (o *PutV4UsersIDUnblockParams) WithID(id int32) *PutV4UsersIDUnblockParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the put v4 users Id unblock params
func (o *PutV4UsersIDUnblockParams) SetID(id int32) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *PutV4UsersIDUnblockParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", swag.FormatInt32(o.ID)); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
