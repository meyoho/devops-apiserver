// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// PutV4UsersIDUnblockReader is a Reader for the PutV4UsersIDUnblock structure.
type PutV4UsersIDUnblockReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PutV4UsersIDUnblockReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPutV4UsersIDUnblockOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPutV4UsersIDUnblockOK creates a PutV4UsersIDUnblockOK with default headers values
func NewPutV4UsersIDUnblockOK() *PutV4UsersIDUnblockOK {
	return &PutV4UsersIDUnblockOK{}
}

/*PutV4UsersIDUnblockOK handles this case with default header values.

Unblock a user. Available only for admins.
*/
type PutV4UsersIDUnblockOK struct {
}

func (o *PutV4UsersIDUnblockOK) Error() string {
	return fmt.Sprintf("[PUT /v4/users/{id}/unblock][%d] putV4UsersIdUnblockOK ", 200)
}

func (o *PutV4UsersIDUnblockOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
