// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostV4ProjectsIDPipelineParams creates a new PostV4ProjectsIDPipelineParams object
// with the default values initialized.
func NewPostV4ProjectsIDPipelineParams() *PostV4ProjectsIDPipelineParams {
	var ()
	return &PostV4ProjectsIDPipelineParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostV4ProjectsIDPipelineParamsWithTimeout creates a new PostV4ProjectsIDPipelineParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostV4ProjectsIDPipelineParamsWithTimeout(timeout time.Duration) *PostV4ProjectsIDPipelineParams {
	var ()
	return &PostV4ProjectsIDPipelineParams{

		timeout: timeout,
	}
}

// NewPostV4ProjectsIDPipelineParamsWithContext creates a new PostV4ProjectsIDPipelineParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostV4ProjectsIDPipelineParamsWithContext(ctx context.Context) *PostV4ProjectsIDPipelineParams {
	var ()
	return &PostV4ProjectsIDPipelineParams{

		Context: ctx,
	}
}

// NewPostV4ProjectsIDPipelineParamsWithHTTPClient creates a new PostV4ProjectsIDPipelineParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostV4ProjectsIDPipelineParamsWithHTTPClient(client *http.Client) *PostV4ProjectsIDPipelineParams {
	var ()
	return &PostV4ProjectsIDPipelineParams{
		HTTPClient: client,
	}
}

/*PostV4ProjectsIDPipelineParams contains all the parameters to send to the API endpoint
for the post v4 projects Id pipeline operation typically these are written to a http.Request
*/
type PostV4ProjectsIDPipelineParams struct {

	/*ID
	  The project ID

	*/
	ID string
	/*Ref
	  Reference

	*/
	Ref string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post v4 projects Id pipeline params
func (o *PostV4ProjectsIDPipelineParams) WithTimeout(timeout time.Duration) *PostV4ProjectsIDPipelineParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post v4 projects Id pipeline params
func (o *PostV4ProjectsIDPipelineParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post v4 projects Id pipeline params
func (o *PostV4ProjectsIDPipelineParams) WithContext(ctx context.Context) *PostV4ProjectsIDPipelineParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post v4 projects Id pipeline params
func (o *PostV4ProjectsIDPipelineParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post v4 projects Id pipeline params
func (o *PostV4ProjectsIDPipelineParams) WithHTTPClient(client *http.Client) *PostV4ProjectsIDPipelineParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post v4 projects Id pipeline params
func (o *PostV4ProjectsIDPipelineParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the post v4 projects Id pipeline params
func (o *PostV4ProjectsIDPipelineParams) WithID(id string) *PostV4ProjectsIDPipelineParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the post v4 projects Id pipeline params
func (o *PostV4ProjectsIDPipelineParams) SetID(id string) {
	o.ID = id
}

// WithRef adds the ref to the post v4 projects Id pipeline params
func (o *PostV4ProjectsIDPipelineParams) WithRef(ref string) *PostV4ProjectsIDPipelineParams {
	o.SetRef(ref)
	return o
}

// SetRef adds the ref to the post v4 projects Id pipeline params
func (o *PostV4ProjectsIDPipelineParams) SetRef(ref string) {
	o.Ref = ref
}

// WriteToRequest writes these params to a swagger request
func (o *PostV4ProjectsIDPipelineParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	// form param ref
	frRef := o.Ref
	fRef := frRef
	if fRef != "" {
		if err := r.SetFormParam("ref", fRef); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
