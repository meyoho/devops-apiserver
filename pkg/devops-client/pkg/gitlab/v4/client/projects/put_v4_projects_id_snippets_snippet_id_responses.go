// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// PutV4ProjectsIDSnippetsSnippetIDReader is a Reader for the PutV4ProjectsIDSnippetsSnippetID structure.
type PutV4ProjectsIDSnippetsSnippetIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PutV4ProjectsIDSnippetsSnippetIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPutV4ProjectsIDSnippetsSnippetIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPutV4ProjectsIDSnippetsSnippetIDOK creates a PutV4ProjectsIDSnippetsSnippetIDOK with default headers values
func NewPutV4ProjectsIDSnippetsSnippetIDOK() *PutV4ProjectsIDSnippetsSnippetIDOK {
	return &PutV4ProjectsIDSnippetsSnippetIDOK{}
}

/*PutV4ProjectsIDSnippetsSnippetIDOK handles this case with default header values.

Update an existing project snippet
*/
type PutV4ProjectsIDSnippetsSnippetIDOK struct {
	Payload *models.ProjectSnippet
}

func (o *PutV4ProjectsIDSnippetsSnippetIDOK) Error() string {
	return fmt.Sprintf("[PUT /v4/projects/{id}/snippets/{snippet_id}][%d] putV4ProjectsIdSnippetsSnippetIdOK  %+v", 200, o.Payload)
}

func (o *PutV4ProjectsIDSnippetsSnippetIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.ProjectSnippet)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
