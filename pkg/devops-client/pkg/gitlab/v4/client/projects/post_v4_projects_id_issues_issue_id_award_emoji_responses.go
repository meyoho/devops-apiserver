// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// PostV4ProjectsIDIssuesIssueIDAwardEmojiReader is a Reader for the PostV4ProjectsIDIssuesIssueIDAwardEmoji structure.
type PostV4ProjectsIDIssuesIssueIDAwardEmojiReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostV4ProjectsIDIssuesIssueIDAwardEmojiReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostV4ProjectsIDIssuesIssueIDAwardEmojiCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostV4ProjectsIDIssuesIssueIDAwardEmojiCreated creates a PostV4ProjectsIDIssuesIssueIDAwardEmojiCreated with default headers values
func NewPostV4ProjectsIDIssuesIssueIDAwardEmojiCreated() *PostV4ProjectsIDIssuesIssueIDAwardEmojiCreated {
	return &PostV4ProjectsIDIssuesIssueIDAwardEmojiCreated{}
}

/*PostV4ProjectsIDIssuesIssueIDAwardEmojiCreated handles this case with default header values.

Award a new Emoji
*/
type PostV4ProjectsIDIssuesIssueIDAwardEmojiCreated struct {
	Payload *models.AwardEmoji
}

func (o *PostV4ProjectsIDIssuesIssueIDAwardEmojiCreated) Error() string {
	return fmt.Sprintf("[POST /v4/projects/{id}/issues/{issue_id}/award_emoji][%d] postV4ProjectsIdIssuesIssueIdAwardEmojiCreated  %+v", 201, o.Payload)
}

func (o *PostV4ProjectsIDIssuesIssueIDAwardEmojiCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.AwardEmoji)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
