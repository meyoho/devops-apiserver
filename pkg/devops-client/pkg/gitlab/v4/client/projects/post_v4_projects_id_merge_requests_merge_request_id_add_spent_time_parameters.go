// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams creates a new PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams object
// with the default values initialized.
func NewPostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams() *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams {
	var ()
	return &PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParamsWithTimeout creates a new PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParamsWithTimeout(timeout time.Duration) *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams {
	var ()
	return &PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams{

		timeout: timeout,
	}
}

// NewPostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParamsWithContext creates a new PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParamsWithContext(ctx context.Context) *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams {
	var ()
	return &PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams{

		Context: ctx,
	}
}

// NewPostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParamsWithHTTPClient creates a new PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParamsWithHTTPClient(client *http.Client) *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams {
	var ()
	return &PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams{
		HTTPClient: client,
	}
}

/*PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams contains all the parameters to send to the API endpoint
for the post v4 projects Id merge requests merge request Id add spent time operation typically these are written to a http.Request
*/
type PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams struct {

	/*Duration
	  The duration to be parsed

	*/
	Duration string
	/*ID
	  The ID of a project

	*/
	ID string
	/*MergeRequestID
	  The ID of a project merge_request

	*/
	MergeRequestID int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) WithTimeout(timeout time.Duration) *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) WithContext(ctx context.Context) *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) WithHTTPClient(client *http.Client) *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithDuration adds the duration to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) WithDuration(duration string) *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams {
	o.SetDuration(duration)
	return o
}

// SetDuration adds the duration to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) SetDuration(duration string) {
	o.Duration = duration
}

// WithID adds the id to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) WithID(id string) *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) SetID(id string) {
	o.ID = id
}

// WithMergeRequestID adds the mergeRequestID to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) WithMergeRequestID(mergeRequestID int32) *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams {
	o.SetMergeRequestID(mergeRequestID)
	return o
}

// SetMergeRequestID adds the mergeRequestId to the post v4 projects Id merge requests merge request Id add spent time params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) SetMergeRequestID(mergeRequestID int32) {
	o.MergeRequestID = mergeRequestID
}

// WriteToRequest writes these params to a swagger request
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAddSpentTimeParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// form param duration
	frDuration := o.Duration
	fDuration := frDuration
	if fDuration != "" {
		if err := r.SetFormParam("duration", fDuration); err != nil {
			return err
		}
	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	// path param merge_request_id
	if err := r.SetPathParam("merge_request_id", swag.FormatInt32(o.MergeRequestID)); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
