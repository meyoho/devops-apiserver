// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// DeleteV4ProjectsIDSnippetsSnippetIDReader is a Reader for the DeleteV4ProjectsIDSnippetsSnippetID structure.
type DeleteV4ProjectsIDSnippetsSnippetIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteV4ProjectsIDSnippetsSnippetIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 204:
		result := NewDeleteV4ProjectsIDSnippetsSnippetIDNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewDeleteV4ProjectsIDSnippetsSnippetIDNoContent creates a DeleteV4ProjectsIDSnippetsSnippetIDNoContent with default headers values
func NewDeleteV4ProjectsIDSnippetsSnippetIDNoContent() *DeleteV4ProjectsIDSnippetsSnippetIDNoContent {
	return &DeleteV4ProjectsIDSnippetsSnippetIDNoContent{}
}

/*DeleteV4ProjectsIDSnippetsSnippetIDNoContent handles this case with default header values.

Delete a project snippet
*/
type DeleteV4ProjectsIDSnippetsSnippetIDNoContent struct {
}

func (o *DeleteV4ProjectsIDSnippetsSnippetIDNoContent) Error() string {
	return fmt.Sprintf("[DELETE /v4/projects/{id}/snippets/{snippet_id}][%d] deleteV4ProjectsIdSnippetsSnippetIdNoContent ", 204)
}

func (o *DeleteV4ProjectsIDSnippetsSnippetIDNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
