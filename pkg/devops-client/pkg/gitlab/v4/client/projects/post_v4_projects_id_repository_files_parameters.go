// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostV4ProjectsIDRepositoryFilesParams creates a new PostV4ProjectsIDRepositoryFilesParams object
// with the default values initialized.
func NewPostV4ProjectsIDRepositoryFilesParams() *PostV4ProjectsIDRepositoryFilesParams {
	var ()
	return &PostV4ProjectsIDRepositoryFilesParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostV4ProjectsIDRepositoryFilesParamsWithTimeout creates a new PostV4ProjectsIDRepositoryFilesParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostV4ProjectsIDRepositoryFilesParamsWithTimeout(timeout time.Duration) *PostV4ProjectsIDRepositoryFilesParams {
	var ()
	return &PostV4ProjectsIDRepositoryFilesParams{

		timeout: timeout,
	}
}

// NewPostV4ProjectsIDRepositoryFilesParamsWithContext creates a new PostV4ProjectsIDRepositoryFilesParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostV4ProjectsIDRepositoryFilesParamsWithContext(ctx context.Context) *PostV4ProjectsIDRepositoryFilesParams {
	var ()
	return &PostV4ProjectsIDRepositoryFilesParams{

		Context: ctx,
	}
}

// NewPostV4ProjectsIDRepositoryFilesParamsWithHTTPClient creates a new PostV4ProjectsIDRepositoryFilesParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostV4ProjectsIDRepositoryFilesParamsWithHTTPClient(client *http.Client) *PostV4ProjectsIDRepositoryFilesParams {
	var ()
	return &PostV4ProjectsIDRepositoryFilesParams{
		HTTPClient: client,
	}
}

/*PostV4ProjectsIDRepositoryFilesParams contains all the parameters to send to the API endpoint
for the post v4 projects Id repository files operation typically these are written to a http.Request
*/
type PostV4ProjectsIDRepositoryFilesParams struct {

	/*AuthorEmail
	  The email of the author

	*/
	AuthorEmail *string
	/*AuthorName
	  The name of the author

	*/
	AuthorName *string
	/*BranchName
	  The name of branch

	*/
	BranchName string
	/*CommitMessage
	  Commit Message

	*/
	CommitMessage string
	/*Content
	  File content

	*/
	Content string
	/*Encoding
	  File encoding

	*/
	Encoding *string
	/*FilePath
	  The path to new file. Ex. lib/class.rb

	*/
	FilePath string
	/*ID
	  The project ID

	*/
	ID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) WithTimeout(timeout time.Duration) *PostV4ProjectsIDRepositoryFilesParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) WithContext(ctx context.Context) *PostV4ProjectsIDRepositoryFilesParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) WithHTTPClient(client *http.Client) *PostV4ProjectsIDRepositoryFilesParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAuthorEmail adds the authorEmail to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) WithAuthorEmail(authorEmail *string) *PostV4ProjectsIDRepositoryFilesParams {
	o.SetAuthorEmail(authorEmail)
	return o
}

// SetAuthorEmail adds the authorEmail to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) SetAuthorEmail(authorEmail *string) {
	o.AuthorEmail = authorEmail
}

// WithAuthorName adds the authorName to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) WithAuthorName(authorName *string) *PostV4ProjectsIDRepositoryFilesParams {
	o.SetAuthorName(authorName)
	return o
}

// SetAuthorName adds the authorName to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) SetAuthorName(authorName *string) {
	o.AuthorName = authorName
}

// WithBranchName adds the branchName to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) WithBranchName(branchName string) *PostV4ProjectsIDRepositoryFilesParams {
	o.SetBranchName(branchName)
	return o
}

// SetBranchName adds the branchName to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) SetBranchName(branchName string) {
	o.BranchName = branchName
}

// WithCommitMessage adds the commitMessage to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) WithCommitMessage(commitMessage string) *PostV4ProjectsIDRepositoryFilesParams {
	o.SetCommitMessage(commitMessage)
	return o
}

// SetCommitMessage adds the commitMessage to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) SetCommitMessage(commitMessage string) {
	o.CommitMessage = commitMessage
}

// WithContent adds the content to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) WithContent(content string) *PostV4ProjectsIDRepositoryFilesParams {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) SetContent(content string) {
	o.Content = content
}

// WithEncoding adds the encoding to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) WithEncoding(encoding *string) *PostV4ProjectsIDRepositoryFilesParams {
	o.SetEncoding(encoding)
	return o
}

// SetEncoding adds the encoding to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) SetEncoding(encoding *string) {
	o.Encoding = encoding
}

// WithFilePath adds the filePath to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) WithFilePath(filePath string) *PostV4ProjectsIDRepositoryFilesParams {
	o.SetFilePath(filePath)
	return o
}

// SetFilePath adds the filePath to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) SetFilePath(filePath string) {
	o.FilePath = filePath
}

// WithID adds the id to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) WithID(id string) *PostV4ProjectsIDRepositoryFilesParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the post v4 projects Id repository files params
func (o *PostV4ProjectsIDRepositoryFilesParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *PostV4ProjectsIDRepositoryFilesParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AuthorEmail != nil {

		// form param author_email
		var frAuthorEmail string
		if o.AuthorEmail != nil {
			frAuthorEmail = *o.AuthorEmail
		}
		fAuthorEmail := frAuthorEmail
		if fAuthorEmail != "" {
			if err := r.SetFormParam("author_email", fAuthorEmail); err != nil {
				return err
			}
		}

	}

	if o.AuthorName != nil {

		// form param author_name
		var frAuthorName string
		if o.AuthorName != nil {
			frAuthorName = *o.AuthorName
		}
		fAuthorName := frAuthorName
		if fAuthorName != "" {
			if err := r.SetFormParam("author_name", fAuthorName); err != nil {
				return err
			}
		}

	}

	// form param branch_name
	frBranchName := o.BranchName
	fBranchName := frBranchName
	if fBranchName != "" {
		if err := r.SetFormParam("branch_name", fBranchName); err != nil {
			return err
		}
	}

	// form param commit_message
	frCommitMessage := o.CommitMessage
	fCommitMessage := frCommitMessage
	if fCommitMessage != "" {
		if err := r.SetFormParam("commit_message", fCommitMessage); err != nil {
			return err
		}
	}

	// form param content
	frContent := o.Content
	fContent := frContent
	if fContent != "" {
		if err := r.SetFormParam("content", fContent); err != nil {
			return err
		}
	}

	if o.Encoding != nil {

		// form param encoding
		var frEncoding string
		if o.Encoding != nil {
			frEncoding = *o.Encoding
		}
		fEncoding := frEncoding
		if fEncoding != "" {
			if err := r.SetFormParam("encoding", fEncoding); err != nil {
				return err
			}
		}

	}

	// form param file_path
	frFilePath := o.FilePath
	fFilePath := frFilePath
	if fFilePath != "" {
		if err := r.SetFormParam("file_path", fFilePath); err != nil {
			return err
		}
	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
