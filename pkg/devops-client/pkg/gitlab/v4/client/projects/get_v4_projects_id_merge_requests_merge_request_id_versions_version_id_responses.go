// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// GetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDReader is a Reader for the GetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionID structure.
type GetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDOK creates a GetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDOK with default headers values
func NewGetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDOK() *GetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDOK {
	return &GetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDOK{}
}

/*GetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDOK handles this case with default header values.

Get a single merge request diff version
*/
type GetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDOK struct {
	Payload *models.MergeRequestDiffFull
}

func (o *GetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDOK) Error() string {
	return fmt.Sprintf("[GET /v4/projects/{id}/merge_requests/{merge_request_id}/versions/{version_id}][%d] getV4ProjectsIdMergeRequestsMergeRequestIdVersionsVersionIdOK  %+v", 200, o.Payload)
}

func (o *GetV4ProjectsIDMergeRequestsMergeRequestIDVersionsVersionIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.MergeRequestDiffFull)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
