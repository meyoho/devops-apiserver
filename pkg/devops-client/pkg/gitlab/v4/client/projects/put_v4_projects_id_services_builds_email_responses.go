// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// PutV4ProjectsIDServicesBuildsEmailReader is a Reader for the PutV4ProjectsIDServicesBuildsEmail structure.
type PutV4ProjectsIDServicesBuildsEmailReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PutV4ProjectsIDServicesBuildsEmailReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPutV4ProjectsIDServicesBuildsEmailOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPutV4ProjectsIDServicesBuildsEmailOK creates a PutV4ProjectsIDServicesBuildsEmailOK with default headers values
func NewPutV4ProjectsIDServicesBuildsEmailOK() *PutV4ProjectsIDServicesBuildsEmailOK {
	return &PutV4ProjectsIDServicesBuildsEmailOK{}
}

/*PutV4ProjectsIDServicesBuildsEmailOK handles this case with default header values.

Set builds-email service for project
*/
type PutV4ProjectsIDServicesBuildsEmailOK struct {
}

func (o *PutV4ProjectsIDServicesBuildsEmailOK) Error() string {
	return fmt.Sprintf("[PUT /v4/projects/{id}/services/builds-email][%d] putV4ProjectsIdServicesBuildsEmailOK ", 200)
}

func (o *PutV4ProjectsIDServicesBuildsEmailOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
