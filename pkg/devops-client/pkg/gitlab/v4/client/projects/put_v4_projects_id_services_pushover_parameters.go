// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPutV4ProjectsIDServicesPushoverParams creates a new PutV4ProjectsIDServicesPushoverParams object
// with the default values initialized.
func NewPutV4ProjectsIDServicesPushoverParams() *PutV4ProjectsIDServicesPushoverParams {
	var ()
	return &PutV4ProjectsIDServicesPushoverParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPutV4ProjectsIDServicesPushoverParamsWithTimeout creates a new PutV4ProjectsIDServicesPushoverParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPutV4ProjectsIDServicesPushoverParamsWithTimeout(timeout time.Duration) *PutV4ProjectsIDServicesPushoverParams {
	var ()
	return &PutV4ProjectsIDServicesPushoverParams{

		timeout: timeout,
	}
}

// NewPutV4ProjectsIDServicesPushoverParamsWithContext creates a new PutV4ProjectsIDServicesPushoverParams object
// with the default values initialized, and the ability to set a context for a request
func NewPutV4ProjectsIDServicesPushoverParamsWithContext(ctx context.Context) *PutV4ProjectsIDServicesPushoverParams {
	var ()
	return &PutV4ProjectsIDServicesPushoverParams{

		Context: ctx,
	}
}

// NewPutV4ProjectsIDServicesPushoverParamsWithHTTPClient creates a new PutV4ProjectsIDServicesPushoverParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPutV4ProjectsIDServicesPushoverParamsWithHTTPClient(client *http.Client) *PutV4ProjectsIDServicesPushoverParams {
	var ()
	return &PutV4ProjectsIDServicesPushoverParams{
		HTTPClient: client,
	}
}

/*PutV4ProjectsIDServicesPushoverParams contains all the parameters to send to the API endpoint
for the put v4 projects Id services pushover operation typically these are written to a http.Request
*/
type PutV4ProjectsIDServicesPushoverParams struct {

	/*APIKey
	  The application key

	*/
	APIKey string
	/*Device
	  Leave blank for all active devices

	*/
	Device string
	/*ID*/
	ID int32
	/*Priority
	  The priority

	*/
	Priority string
	/*PushEvents
	  Event will be triggered by a push to the repository

	*/
	PushEvents *string
	/*Sound
	  The sound of the notification

	*/
	Sound string
	/*UserKey
	  The user key

	*/
	UserKey string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) WithTimeout(timeout time.Duration) *PutV4ProjectsIDServicesPushoverParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) WithContext(ctx context.Context) *PutV4ProjectsIDServicesPushoverParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) WithHTTPClient(client *http.Client) *PutV4ProjectsIDServicesPushoverParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAPIKey adds the aPIKey to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) WithAPIKey(aPIKey string) *PutV4ProjectsIDServicesPushoverParams {
	o.SetAPIKey(aPIKey)
	return o
}

// SetAPIKey adds the apiKey to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) SetAPIKey(aPIKey string) {
	o.APIKey = aPIKey
}

// WithDevice adds the device to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) WithDevice(device string) *PutV4ProjectsIDServicesPushoverParams {
	o.SetDevice(device)
	return o
}

// SetDevice adds the device to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) SetDevice(device string) {
	o.Device = device
}

// WithID adds the id to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) WithID(id int32) *PutV4ProjectsIDServicesPushoverParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) SetID(id int32) {
	o.ID = id
}

// WithPriority adds the priority to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) WithPriority(priority string) *PutV4ProjectsIDServicesPushoverParams {
	o.SetPriority(priority)
	return o
}

// SetPriority adds the priority to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) SetPriority(priority string) {
	o.Priority = priority
}

// WithPushEvents adds the pushEvents to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) WithPushEvents(pushEvents *string) *PutV4ProjectsIDServicesPushoverParams {
	o.SetPushEvents(pushEvents)
	return o
}

// SetPushEvents adds the pushEvents to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) SetPushEvents(pushEvents *string) {
	o.PushEvents = pushEvents
}

// WithSound adds the sound to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) WithSound(sound string) *PutV4ProjectsIDServicesPushoverParams {
	o.SetSound(sound)
	return o
}

// SetSound adds the sound to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) SetSound(sound string) {
	o.Sound = sound
}

// WithUserKey adds the userKey to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) WithUserKey(userKey string) *PutV4ProjectsIDServicesPushoverParams {
	o.SetUserKey(userKey)
	return o
}

// SetUserKey adds the userKey to the put v4 projects Id services pushover params
func (o *PutV4ProjectsIDServicesPushoverParams) SetUserKey(userKey string) {
	o.UserKey = userKey
}

// WriteToRequest writes these params to a swagger request
func (o *PutV4ProjectsIDServicesPushoverParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// form param api_key
	frAPIKey := o.APIKey
	fAPIKey := frAPIKey
	if fAPIKey != "" {
		if err := r.SetFormParam("api_key", fAPIKey); err != nil {
			return err
		}
	}

	// form param device
	frDevice := o.Device
	fDevice := frDevice
	if fDevice != "" {
		if err := r.SetFormParam("device", fDevice); err != nil {
			return err
		}
	}

	// path param id
	if err := r.SetPathParam("id", swag.FormatInt32(o.ID)); err != nil {
		return err
	}

	// form param priority
	frPriority := o.Priority
	fPriority := frPriority
	if fPriority != "" {
		if err := r.SetFormParam("priority", fPriority); err != nil {
			return err
		}
	}

	if o.PushEvents != nil {

		// form param push_events
		var frPushEvents string
		if o.PushEvents != nil {
			frPushEvents = *o.PushEvents
		}
		fPushEvents := frPushEvents
		if fPushEvents != "" {
			if err := r.SetFormParam("push_events", fPushEvents); err != nil {
				return err
			}
		}

	}

	// form param sound
	frSound := o.Sound
	fSound := frSound
	if fSound != "" {
		if err := r.SetFormParam("sound", fSound); err != nil {
			return err
		}
	}

	// form param user_key
	frUserKey := o.UserKey
	fUserKey := frUserKey
	if fUserKey != "" {
		if err := r.SetFormParam("user_key", fUserKey); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
