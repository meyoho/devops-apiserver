// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// GetV4ProjectsIDIssuesReader is a Reader for the GetV4ProjectsIDIssues structure.
type GetV4ProjectsIDIssuesReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4ProjectsIDIssuesReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4ProjectsIDIssuesOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4ProjectsIDIssuesOK creates a GetV4ProjectsIDIssuesOK with default headers values
func NewGetV4ProjectsIDIssuesOK() *GetV4ProjectsIDIssuesOK {
	return &GetV4ProjectsIDIssuesOK{}
}

/*GetV4ProjectsIDIssuesOK handles this case with default header values.

Get a list of project issues
*/
type GetV4ProjectsIDIssuesOK struct {
	Payload *models.Issue
}

func (o *GetV4ProjectsIDIssuesOK) Error() string {
	return fmt.Sprintf("[GET /v4/projects/{id}/issues][%d] getV4ProjectsIdIssuesOK  %+v", 200, o.Payload)
}

func (o *GetV4ProjectsIDIssuesOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Issue)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
