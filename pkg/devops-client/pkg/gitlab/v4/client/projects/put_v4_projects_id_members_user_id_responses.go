// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// PutV4ProjectsIDMembersUserIDReader is a Reader for the PutV4ProjectsIDMembersUserID structure.
type PutV4ProjectsIDMembersUserIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PutV4ProjectsIDMembersUserIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPutV4ProjectsIDMembersUserIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPutV4ProjectsIDMembersUserIDOK creates a PutV4ProjectsIDMembersUserIDOK with default headers values
func NewPutV4ProjectsIDMembersUserIDOK() *PutV4ProjectsIDMembersUserIDOK {
	return &PutV4ProjectsIDMembersUserIDOK{}
}

/*PutV4ProjectsIDMembersUserIDOK handles this case with default header values.

Updates a member of a group or project.
*/
type PutV4ProjectsIDMembersUserIDOK struct {
	Payload *models.Member
}

func (o *PutV4ProjectsIDMembersUserIDOK) Error() string {
	return fmt.Sprintf("[PUT /v4/projects/{id}/members/{user_id}][%d] putV4ProjectsIdMembersUserIdOK  %+v", 200, o.Payload)
}

func (o *PutV4ProjectsIDMembersUserIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Member)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
