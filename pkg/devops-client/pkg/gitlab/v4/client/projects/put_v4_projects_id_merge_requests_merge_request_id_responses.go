// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// PutV4ProjectsIDMergeRequestsMergeRequestIDReader is a Reader for the PutV4ProjectsIDMergeRequestsMergeRequestID structure.
type PutV4ProjectsIDMergeRequestsMergeRequestIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PutV4ProjectsIDMergeRequestsMergeRequestIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPutV4ProjectsIDMergeRequestsMergeRequestIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPutV4ProjectsIDMergeRequestsMergeRequestIDOK creates a PutV4ProjectsIDMergeRequestsMergeRequestIDOK with default headers values
func NewPutV4ProjectsIDMergeRequestsMergeRequestIDOK() *PutV4ProjectsIDMergeRequestsMergeRequestIDOK {
	return &PutV4ProjectsIDMergeRequestsMergeRequestIDOK{}
}

/*PutV4ProjectsIDMergeRequestsMergeRequestIDOK handles this case with default header values.

Update a merge request
*/
type PutV4ProjectsIDMergeRequestsMergeRequestIDOK struct {
	Payload *models.MergeRequest
}

func (o *PutV4ProjectsIDMergeRequestsMergeRequestIDOK) Error() string {
	return fmt.Sprintf("[PUT /v4/projects/{id}/merge_requests/{merge_request_id}][%d] putV4ProjectsIdMergeRequestsMergeRequestIdOK  %+v", 200, o.Payload)
}

func (o *PutV4ProjectsIDMergeRequestsMergeRequestIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.MergeRequest)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
