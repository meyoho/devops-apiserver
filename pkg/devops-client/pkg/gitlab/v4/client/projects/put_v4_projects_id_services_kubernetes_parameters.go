// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPutV4ProjectsIDServicesKubernetesParams creates a new PutV4ProjectsIDServicesKubernetesParams object
// with the default values initialized.
func NewPutV4ProjectsIDServicesKubernetesParams() *PutV4ProjectsIDServicesKubernetesParams {
	var ()
	return &PutV4ProjectsIDServicesKubernetesParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPutV4ProjectsIDServicesKubernetesParamsWithTimeout creates a new PutV4ProjectsIDServicesKubernetesParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPutV4ProjectsIDServicesKubernetesParamsWithTimeout(timeout time.Duration) *PutV4ProjectsIDServicesKubernetesParams {
	var ()
	return &PutV4ProjectsIDServicesKubernetesParams{

		timeout: timeout,
	}
}

// NewPutV4ProjectsIDServicesKubernetesParamsWithContext creates a new PutV4ProjectsIDServicesKubernetesParams object
// with the default values initialized, and the ability to set a context for a request
func NewPutV4ProjectsIDServicesKubernetesParamsWithContext(ctx context.Context) *PutV4ProjectsIDServicesKubernetesParams {
	var ()
	return &PutV4ProjectsIDServicesKubernetesParams{

		Context: ctx,
	}
}

// NewPutV4ProjectsIDServicesKubernetesParamsWithHTTPClient creates a new PutV4ProjectsIDServicesKubernetesParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPutV4ProjectsIDServicesKubernetesParamsWithHTTPClient(client *http.Client) *PutV4ProjectsIDServicesKubernetesParams {
	var ()
	return &PutV4ProjectsIDServicesKubernetesParams{
		HTTPClient: client,
	}
}

/*PutV4ProjectsIDServicesKubernetesParams contains all the parameters to send to the API endpoint
for the put v4 projects Id services kubernetes operation typically these are written to a http.Request
*/
type PutV4ProjectsIDServicesKubernetesParams struct {

	/*APIURL
	  The URL to the Kubernetes cluster API, e.g., https://kubernetes.example.com

	*/
	APIURL string
	/*CaPem
	  A custom certificate authority bundle to verify the Kubernetes cluster with (PEM format)

	*/
	CaPem *string
	/*ID*/
	ID int32
	/*Namespace
	  The Kubernetes namespace to use

	*/
	Namespace string
	/*Token
	  The service token to authenticate against the Kubernetes cluster with

	*/
	Token string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) WithTimeout(timeout time.Duration) *PutV4ProjectsIDServicesKubernetesParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) WithContext(ctx context.Context) *PutV4ProjectsIDServicesKubernetesParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) WithHTTPClient(client *http.Client) *PutV4ProjectsIDServicesKubernetesParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAPIURL adds the aPIURL to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) WithAPIURL(aPIURL string) *PutV4ProjectsIDServicesKubernetesParams {
	o.SetAPIURL(aPIURL)
	return o
}

// SetAPIURL adds the apiUrl to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) SetAPIURL(aPIURL string) {
	o.APIURL = aPIURL
}

// WithCaPem adds the caPem to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) WithCaPem(caPem *string) *PutV4ProjectsIDServicesKubernetesParams {
	o.SetCaPem(caPem)
	return o
}

// SetCaPem adds the caPem to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) SetCaPem(caPem *string) {
	o.CaPem = caPem
}

// WithID adds the id to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) WithID(id int32) *PutV4ProjectsIDServicesKubernetesParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) SetID(id int32) {
	o.ID = id
}

// WithNamespace adds the namespace to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) WithNamespace(namespace string) *PutV4ProjectsIDServicesKubernetesParams {
	o.SetNamespace(namespace)
	return o
}

// SetNamespace adds the namespace to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) SetNamespace(namespace string) {
	o.Namespace = namespace
}

// WithToken adds the token to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) WithToken(token string) *PutV4ProjectsIDServicesKubernetesParams {
	o.SetToken(token)
	return o
}

// SetToken adds the token to the put v4 projects Id services kubernetes params
func (o *PutV4ProjectsIDServicesKubernetesParams) SetToken(token string) {
	o.Token = token
}

// WriteToRequest writes these params to a swagger request
func (o *PutV4ProjectsIDServicesKubernetesParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// form param api_url
	frAPIURL := o.APIURL
	fAPIURL := frAPIURL
	if fAPIURL != "" {
		if err := r.SetFormParam("api_url", fAPIURL); err != nil {
			return err
		}
	}

	if o.CaPem != nil {

		// form param ca_pem
		var frCaPem string
		if o.CaPem != nil {
			frCaPem = *o.CaPem
		}
		fCaPem := frCaPem
		if fCaPem != "" {
			if err := r.SetFormParam("ca_pem", fCaPem); err != nil {
				return err
			}
		}

	}

	// path param id
	if err := r.SetPathParam("id", swag.FormatInt32(o.ID)); err != nil {
		return err
	}

	// form param namespace
	frNamespace := o.Namespace
	fNamespace := frNamespace
	if fNamespace != "" {
		if err := r.SetFormParam("namespace", fNamespace); err != nil {
			return err
		}
	}

	// form param token
	frToken := o.Token
	fToken := frToken
	if fToken != "" {
		if err := r.SetFormParam("token", fToken); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
