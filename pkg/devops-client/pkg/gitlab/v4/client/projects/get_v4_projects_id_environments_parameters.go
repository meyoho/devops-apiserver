// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4ProjectsIDEnvironmentsParams creates a new GetV4ProjectsIDEnvironmentsParams object
// with the default values initialized.
func NewGetV4ProjectsIDEnvironmentsParams() *GetV4ProjectsIDEnvironmentsParams {
	var ()
	return &GetV4ProjectsIDEnvironmentsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4ProjectsIDEnvironmentsParamsWithTimeout creates a new GetV4ProjectsIDEnvironmentsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4ProjectsIDEnvironmentsParamsWithTimeout(timeout time.Duration) *GetV4ProjectsIDEnvironmentsParams {
	var ()
	return &GetV4ProjectsIDEnvironmentsParams{

		timeout: timeout,
	}
}

// NewGetV4ProjectsIDEnvironmentsParamsWithContext creates a new GetV4ProjectsIDEnvironmentsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4ProjectsIDEnvironmentsParamsWithContext(ctx context.Context) *GetV4ProjectsIDEnvironmentsParams {
	var ()
	return &GetV4ProjectsIDEnvironmentsParams{

		Context: ctx,
	}
}

// NewGetV4ProjectsIDEnvironmentsParamsWithHTTPClient creates a new GetV4ProjectsIDEnvironmentsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4ProjectsIDEnvironmentsParamsWithHTTPClient(client *http.Client) *GetV4ProjectsIDEnvironmentsParams {
	var ()
	return &GetV4ProjectsIDEnvironmentsParams{
		HTTPClient: client,
	}
}

/*GetV4ProjectsIDEnvironmentsParams contains all the parameters to send to the API endpoint
for the get v4 projects Id environments operation typically these are written to a http.Request
*/
type GetV4ProjectsIDEnvironmentsParams struct {

	/*ID
	  The project ID

	*/
	ID string
	/*Page
	  Current page number

	*/
	Page *int32
	/*PerPage
	  Number of items per page

	*/
	PerPage *int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) WithTimeout(timeout time.Duration) *GetV4ProjectsIDEnvironmentsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) WithContext(ctx context.Context) *GetV4ProjectsIDEnvironmentsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) WithHTTPClient(client *http.Client) *GetV4ProjectsIDEnvironmentsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) WithID(id string) *GetV4ProjectsIDEnvironmentsParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) SetID(id string) {
	o.ID = id
}

// WithPage adds the page to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) WithPage(page *int32) *GetV4ProjectsIDEnvironmentsParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) SetPage(page *int32) {
	o.Page = page
}

// WithPerPage adds the perPage to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) WithPerPage(perPage *int32) *GetV4ProjectsIDEnvironmentsParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the get v4 projects Id environments params
func (o *GetV4ProjectsIDEnvironmentsParams) SetPerPage(perPage *int32) {
	o.PerPage = perPage
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4ProjectsIDEnvironmentsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int32
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt32(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
