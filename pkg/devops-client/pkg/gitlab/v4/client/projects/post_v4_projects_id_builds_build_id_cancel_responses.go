// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// PostV4ProjectsIDBuildsBuildIDCancelReader is a Reader for the PostV4ProjectsIDBuildsBuildIDCancel structure.
type PostV4ProjectsIDBuildsBuildIDCancelReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostV4ProjectsIDBuildsBuildIDCancelReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostV4ProjectsIDBuildsBuildIDCancelCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostV4ProjectsIDBuildsBuildIDCancelCreated creates a PostV4ProjectsIDBuildsBuildIDCancelCreated with default headers values
func NewPostV4ProjectsIDBuildsBuildIDCancelCreated() *PostV4ProjectsIDBuildsBuildIDCancelCreated {
	return &PostV4ProjectsIDBuildsBuildIDCancelCreated{}
}

/*PostV4ProjectsIDBuildsBuildIDCancelCreated handles this case with default header values.

Cancel a specific build of a project
*/
type PostV4ProjectsIDBuildsBuildIDCancelCreated struct {
	Payload *models.Build
}

func (o *PostV4ProjectsIDBuildsBuildIDCancelCreated) Error() string {
	return fmt.Sprintf("[POST /v4/projects/{id}/builds/{build_id}/cancel][%d] postV4ProjectsIdBuildsBuildIdCancelCreated  %+v", 201, o.Payload)
}

func (o *PostV4ProjectsIDBuildsBuildIDCancelCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Build)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
