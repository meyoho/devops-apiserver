// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// GetV4ProjectsIDRepositoryBranchesReader is a Reader for the GetV4ProjectsIDRepositoryBranches structure.
type GetV4ProjectsIDRepositoryBranchesReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4ProjectsIDRepositoryBranchesReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4ProjectsIDRepositoryBranchesOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4ProjectsIDRepositoryBranchesOK creates a GetV4ProjectsIDRepositoryBranchesOK with default headers values
func NewGetV4ProjectsIDRepositoryBranchesOK() *GetV4ProjectsIDRepositoryBranchesOK {
	return &GetV4ProjectsIDRepositoryBranchesOK{}
}

/*GetV4ProjectsIDRepositoryBranchesOK handles this case with default header values.

Get a project repository branches
*/
type GetV4ProjectsIDRepositoryBranchesOK struct {
	Payload []*models.RepoBranch
}

func (o *GetV4ProjectsIDRepositoryBranchesOK) Error() string {
	return fmt.Sprintf("[GET /v4/projects/{id}/repository/branches][%d] getV4ProjectsIdRepositoryBranchesOK  %+v", 200, o.Payload)
}

func (o *GetV4ProjectsIDRepositoryBranchesOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
