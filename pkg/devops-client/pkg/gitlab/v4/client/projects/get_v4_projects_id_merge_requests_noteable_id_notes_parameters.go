// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4ProjectsIDMergeRequestsNoteableIDNotesParams creates a new GetV4ProjectsIDMergeRequestsNoteableIDNotesParams object
// with the default values initialized.
func NewGetV4ProjectsIDMergeRequestsNoteableIDNotesParams() *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams {
	var ()
	return &GetV4ProjectsIDMergeRequestsNoteableIDNotesParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4ProjectsIDMergeRequestsNoteableIDNotesParamsWithTimeout creates a new GetV4ProjectsIDMergeRequestsNoteableIDNotesParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4ProjectsIDMergeRequestsNoteableIDNotesParamsWithTimeout(timeout time.Duration) *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams {
	var ()
	return &GetV4ProjectsIDMergeRequestsNoteableIDNotesParams{

		timeout: timeout,
	}
}

// NewGetV4ProjectsIDMergeRequestsNoteableIDNotesParamsWithContext creates a new GetV4ProjectsIDMergeRequestsNoteableIDNotesParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4ProjectsIDMergeRequestsNoteableIDNotesParamsWithContext(ctx context.Context) *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams {
	var ()
	return &GetV4ProjectsIDMergeRequestsNoteableIDNotesParams{

		Context: ctx,
	}
}

// NewGetV4ProjectsIDMergeRequestsNoteableIDNotesParamsWithHTTPClient creates a new GetV4ProjectsIDMergeRequestsNoteableIDNotesParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4ProjectsIDMergeRequestsNoteableIDNotesParamsWithHTTPClient(client *http.Client) *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams {
	var ()
	return &GetV4ProjectsIDMergeRequestsNoteableIDNotesParams{
		HTTPClient: client,
	}
}

/*GetV4ProjectsIDMergeRequestsNoteableIDNotesParams contains all the parameters to send to the API endpoint
for the get v4 projects Id merge requests noteable Id notes operation typically these are written to a http.Request
*/
type GetV4ProjectsIDMergeRequestsNoteableIDNotesParams struct {

	/*ID
	  The ID of a project

	*/
	ID string
	/*NoteableID
	  The ID of the noteable

	*/
	NoteableID int32
	/*Page
	  Current page number

	*/
	Page *int32
	/*PerPage
	  Number of items per page

	*/
	PerPage *int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) WithTimeout(timeout time.Duration) *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) WithContext(ctx context.Context) *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) WithHTTPClient(client *http.Client) *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) WithID(id string) *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) SetID(id string) {
	o.ID = id
}

// WithNoteableID adds the noteableID to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) WithNoteableID(noteableID int32) *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams {
	o.SetNoteableID(noteableID)
	return o
}

// SetNoteableID adds the noteableId to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) SetNoteableID(noteableID int32) {
	o.NoteableID = noteableID
}

// WithPage adds the page to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) WithPage(page *int32) *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) SetPage(page *int32) {
	o.Page = page
}

// WithPerPage adds the perPage to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) WithPerPage(perPage *int32) *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the get v4 projects Id merge requests noteable Id notes params
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) SetPerPage(perPage *int32) {
	o.PerPage = perPage
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4ProjectsIDMergeRequestsNoteableIDNotesParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	// path param noteable_id
	if err := r.SetPathParam("noteable_id", swag.FormatInt32(o.NoteableID)); err != nil {
		return err
	}

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int32
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt32(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
