// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPutV4ProjectsIDServicesRedmineParams creates a new PutV4ProjectsIDServicesRedmineParams object
// with the default values initialized.
func NewPutV4ProjectsIDServicesRedmineParams() *PutV4ProjectsIDServicesRedmineParams {
	var ()
	return &PutV4ProjectsIDServicesRedmineParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPutV4ProjectsIDServicesRedmineParamsWithTimeout creates a new PutV4ProjectsIDServicesRedmineParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPutV4ProjectsIDServicesRedmineParamsWithTimeout(timeout time.Duration) *PutV4ProjectsIDServicesRedmineParams {
	var ()
	return &PutV4ProjectsIDServicesRedmineParams{

		timeout: timeout,
	}
}

// NewPutV4ProjectsIDServicesRedmineParamsWithContext creates a new PutV4ProjectsIDServicesRedmineParams object
// with the default values initialized, and the ability to set a context for a request
func NewPutV4ProjectsIDServicesRedmineParamsWithContext(ctx context.Context) *PutV4ProjectsIDServicesRedmineParams {
	var ()
	return &PutV4ProjectsIDServicesRedmineParams{

		Context: ctx,
	}
}

// NewPutV4ProjectsIDServicesRedmineParamsWithHTTPClient creates a new PutV4ProjectsIDServicesRedmineParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPutV4ProjectsIDServicesRedmineParamsWithHTTPClient(client *http.Client) *PutV4ProjectsIDServicesRedmineParams {
	var ()
	return &PutV4ProjectsIDServicesRedmineParams{
		HTTPClient: client,
	}
}

/*PutV4ProjectsIDServicesRedmineParams contains all the parameters to send to the API endpoint
for the put v4 projects Id services redmine operation typically these are written to a http.Request
*/
type PutV4ProjectsIDServicesRedmineParams struct {

	/*Description
	  The description of the tracker

	*/
	Description *string
	/*ID*/
	ID int32
	/*IssuesURL
	  The issues URL

	*/
	IssuesURL string
	/*NewIssueURL
	  The new issue URL

	*/
	NewIssueURL string
	/*ProjectURL
	  The project URL

	*/
	ProjectURL string
	/*PushEvents
	  Event will be triggered by a push to the repository

	*/
	PushEvents *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) WithTimeout(timeout time.Duration) *PutV4ProjectsIDServicesRedmineParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) WithContext(ctx context.Context) *PutV4ProjectsIDServicesRedmineParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) WithHTTPClient(client *http.Client) *PutV4ProjectsIDServicesRedmineParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithDescription adds the description to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) WithDescription(description *string) *PutV4ProjectsIDServicesRedmineParams {
	o.SetDescription(description)
	return o
}

// SetDescription adds the description to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) SetDescription(description *string) {
	o.Description = description
}

// WithID adds the id to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) WithID(id int32) *PutV4ProjectsIDServicesRedmineParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) SetID(id int32) {
	o.ID = id
}

// WithIssuesURL adds the issuesURL to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) WithIssuesURL(issuesURL string) *PutV4ProjectsIDServicesRedmineParams {
	o.SetIssuesURL(issuesURL)
	return o
}

// SetIssuesURL adds the issuesUrl to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) SetIssuesURL(issuesURL string) {
	o.IssuesURL = issuesURL
}

// WithNewIssueURL adds the newIssueURL to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) WithNewIssueURL(newIssueURL string) *PutV4ProjectsIDServicesRedmineParams {
	o.SetNewIssueURL(newIssueURL)
	return o
}

// SetNewIssueURL adds the newIssueUrl to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) SetNewIssueURL(newIssueURL string) {
	o.NewIssueURL = newIssueURL
}

// WithProjectURL adds the projectURL to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) WithProjectURL(projectURL string) *PutV4ProjectsIDServicesRedmineParams {
	o.SetProjectURL(projectURL)
	return o
}

// SetProjectURL adds the projectUrl to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) SetProjectURL(projectURL string) {
	o.ProjectURL = projectURL
}

// WithPushEvents adds the pushEvents to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) WithPushEvents(pushEvents *string) *PutV4ProjectsIDServicesRedmineParams {
	o.SetPushEvents(pushEvents)
	return o
}

// SetPushEvents adds the pushEvents to the put v4 projects Id services redmine params
func (o *PutV4ProjectsIDServicesRedmineParams) SetPushEvents(pushEvents *string) {
	o.PushEvents = pushEvents
}

// WriteToRequest writes these params to a swagger request
func (o *PutV4ProjectsIDServicesRedmineParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Description != nil {

		// form param description
		var frDescription string
		if o.Description != nil {
			frDescription = *o.Description
		}
		fDescription := frDescription
		if fDescription != "" {
			if err := r.SetFormParam("description", fDescription); err != nil {
				return err
			}
		}

	}

	// path param id
	if err := r.SetPathParam("id", swag.FormatInt32(o.ID)); err != nil {
		return err
	}

	// form param issues_url
	frIssuesURL := o.IssuesURL
	fIssuesURL := frIssuesURL
	if fIssuesURL != "" {
		if err := r.SetFormParam("issues_url", fIssuesURL); err != nil {
			return err
		}
	}

	// form param new_issue_url
	frNewIssueURL := o.NewIssueURL
	fNewIssueURL := frNewIssueURL
	if fNewIssueURL != "" {
		if err := r.SetFormParam("new_issue_url", fNewIssueURL); err != nil {
			return err
		}
	}

	// form param project_url
	frProjectURL := o.ProjectURL
	fProjectURL := frProjectURL
	if fProjectURL != "" {
		if err := r.SetFormParam("project_url", fProjectURL); err != nil {
			return err
		}
	}

	if o.PushEvents != nil {

		// form param push_events
		var frPushEvents string
		if o.PushEvents != nil {
			frPushEvents = *o.PushEvents
		}
		fPushEvents := frPushEvents
		if fPushEvents != "" {
			if err := r.SetFormParam("push_events", fPushEvents); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
