// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// DeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDReader is a Reader for the DeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteID structure.
type DeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewDeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewDeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDOK creates a DeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDOK with default headers values
func NewDeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDOK() *DeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDOK {
	return &DeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDOK{}
}

/*DeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDOK handles this case with default header values.

Delete a +noteable+ note
*/
type DeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDOK struct {
	Payload *models.Note
}

func (o *DeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDOK) Error() string {
	return fmt.Sprintf("[DELETE /v4/projects/{id}/merge_requests/{noteable_id}/notes/{note_id}][%d] deleteV4ProjectsIdMergeRequestsNoteableIdNotesNoteIdOK  %+v", 200, o.Payload)
}

func (o *DeleteV4ProjectsIDMergeRequestsNoteableIDNotesNoteIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Note)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
