// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4ProjectsIDRepositoryCommitsShaCommentsParams creates a new GetV4ProjectsIDRepositoryCommitsShaCommentsParams object
// with the default values initialized.
func NewGetV4ProjectsIDRepositoryCommitsShaCommentsParams() *GetV4ProjectsIDRepositoryCommitsShaCommentsParams {
	var ()
	return &GetV4ProjectsIDRepositoryCommitsShaCommentsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4ProjectsIDRepositoryCommitsShaCommentsParamsWithTimeout creates a new GetV4ProjectsIDRepositoryCommitsShaCommentsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4ProjectsIDRepositoryCommitsShaCommentsParamsWithTimeout(timeout time.Duration) *GetV4ProjectsIDRepositoryCommitsShaCommentsParams {
	var ()
	return &GetV4ProjectsIDRepositoryCommitsShaCommentsParams{

		timeout: timeout,
	}
}

// NewGetV4ProjectsIDRepositoryCommitsShaCommentsParamsWithContext creates a new GetV4ProjectsIDRepositoryCommitsShaCommentsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4ProjectsIDRepositoryCommitsShaCommentsParamsWithContext(ctx context.Context) *GetV4ProjectsIDRepositoryCommitsShaCommentsParams {
	var ()
	return &GetV4ProjectsIDRepositoryCommitsShaCommentsParams{

		Context: ctx,
	}
}

// NewGetV4ProjectsIDRepositoryCommitsShaCommentsParamsWithHTTPClient creates a new GetV4ProjectsIDRepositoryCommitsShaCommentsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4ProjectsIDRepositoryCommitsShaCommentsParamsWithHTTPClient(client *http.Client) *GetV4ProjectsIDRepositoryCommitsShaCommentsParams {
	var ()
	return &GetV4ProjectsIDRepositoryCommitsShaCommentsParams{
		HTTPClient: client,
	}
}

/*GetV4ProjectsIDRepositoryCommitsShaCommentsParams contains all the parameters to send to the API endpoint
for the get v4 projects Id repository commits sha comments operation typically these are written to a http.Request
*/
type GetV4ProjectsIDRepositoryCommitsShaCommentsParams struct {

	/*ID
	  The ID of a project

	*/
	ID string
	/*Page
	  Current page number

	*/
	Page *int32
	/*PerPage
	  Number of items per page

	*/
	PerPage *int32
	/*Sha
	  A commit sha, or the name of a branch or tag

	*/
	Sha string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) WithTimeout(timeout time.Duration) *GetV4ProjectsIDRepositoryCommitsShaCommentsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) WithContext(ctx context.Context) *GetV4ProjectsIDRepositoryCommitsShaCommentsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) WithHTTPClient(client *http.Client) *GetV4ProjectsIDRepositoryCommitsShaCommentsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) WithID(id string) *GetV4ProjectsIDRepositoryCommitsShaCommentsParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) SetID(id string) {
	o.ID = id
}

// WithPage adds the page to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) WithPage(page *int32) *GetV4ProjectsIDRepositoryCommitsShaCommentsParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) SetPage(page *int32) {
	o.Page = page
}

// WithPerPage adds the perPage to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) WithPerPage(perPage *int32) *GetV4ProjectsIDRepositoryCommitsShaCommentsParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) SetPerPage(perPage *int32) {
	o.PerPage = perPage
}

// WithSha adds the sha to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) WithSha(sha string) *GetV4ProjectsIDRepositoryCommitsShaCommentsParams {
	o.SetSha(sha)
	return o
}

// SetSha adds the sha to the get v4 projects Id repository commits sha comments params
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) SetSha(sha string) {
	o.Sha = sha
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4ProjectsIDRepositoryCommitsShaCommentsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int32
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt32(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	// path param sha
	if err := r.SetPathParam("sha", o.Sha); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
