// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4ProjectsIDDeploymentsParams creates a new GetV4ProjectsIDDeploymentsParams object
// with the default values initialized.
func NewGetV4ProjectsIDDeploymentsParams() *GetV4ProjectsIDDeploymentsParams {
	var ()
	return &GetV4ProjectsIDDeploymentsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4ProjectsIDDeploymentsParamsWithTimeout creates a new GetV4ProjectsIDDeploymentsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4ProjectsIDDeploymentsParamsWithTimeout(timeout time.Duration) *GetV4ProjectsIDDeploymentsParams {
	var ()
	return &GetV4ProjectsIDDeploymentsParams{

		timeout: timeout,
	}
}

// NewGetV4ProjectsIDDeploymentsParamsWithContext creates a new GetV4ProjectsIDDeploymentsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4ProjectsIDDeploymentsParamsWithContext(ctx context.Context) *GetV4ProjectsIDDeploymentsParams {
	var ()
	return &GetV4ProjectsIDDeploymentsParams{

		Context: ctx,
	}
}

// NewGetV4ProjectsIDDeploymentsParamsWithHTTPClient creates a new GetV4ProjectsIDDeploymentsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4ProjectsIDDeploymentsParamsWithHTTPClient(client *http.Client) *GetV4ProjectsIDDeploymentsParams {
	var ()
	return &GetV4ProjectsIDDeploymentsParams{
		HTTPClient: client,
	}
}

/*GetV4ProjectsIDDeploymentsParams contains all the parameters to send to the API endpoint
for the get v4 projects Id deployments operation typically these are written to a http.Request
*/
type GetV4ProjectsIDDeploymentsParams struct {

	/*ID
	  The project ID

	*/
	ID string
	/*Page
	  Current page number

	*/
	Page *int32
	/*PerPage
	  Number of items per page

	*/
	PerPage *int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) WithTimeout(timeout time.Duration) *GetV4ProjectsIDDeploymentsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) WithContext(ctx context.Context) *GetV4ProjectsIDDeploymentsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) WithHTTPClient(client *http.Client) *GetV4ProjectsIDDeploymentsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) WithID(id string) *GetV4ProjectsIDDeploymentsParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) SetID(id string) {
	o.ID = id
}

// WithPage adds the page to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) WithPage(page *int32) *GetV4ProjectsIDDeploymentsParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) SetPage(page *int32) {
	o.Page = page
}

// WithPerPage adds the perPage to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) WithPerPage(perPage *int32) *GetV4ProjectsIDDeploymentsParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the get v4 projects Id deployments params
func (o *GetV4ProjectsIDDeploymentsParams) SetPerPage(perPage *int32) {
	o.PerPage = perPage
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4ProjectsIDDeploymentsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int32
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt32(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
