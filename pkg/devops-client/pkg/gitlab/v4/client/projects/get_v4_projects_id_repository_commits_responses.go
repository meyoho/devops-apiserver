// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// GetV4ProjectsIDRepositoryCommitsReader is a Reader for the GetV4ProjectsIDRepositoryCommits structure.
type GetV4ProjectsIDRepositoryCommitsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4ProjectsIDRepositoryCommitsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4ProjectsIDRepositoryCommitsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4ProjectsIDRepositoryCommitsOK creates a GetV4ProjectsIDRepositoryCommitsOK with default headers values
func NewGetV4ProjectsIDRepositoryCommitsOK() *GetV4ProjectsIDRepositoryCommitsOK {
	return &GetV4ProjectsIDRepositoryCommitsOK{}
}

/*GetV4ProjectsIDRepositoryCommitsOK handles this case with default header values.

Get a project repository commits
*/
type GetV4ProjectsIDRepositoryCommitsOK struct {
	Payload []*models.RepoCommit
}

func (o *GetV4ProjectsIDRepositoryCommitsOK) Error() string {
	return fmt.Sprintf("[GET /v4/projects/{id}/repository/commits][%d] getV4ProjectsIdRepositoryCommitsOK  %+v", 200, o.Payload)
}

func (o *GetV4ProjectsIDRepositoryCommitsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
