// Code generated by go-swagger; DO NOT EDIT.

package user

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4/models"
)

// GetV4UserEmailsReader is a Reader for the GetV4UserEmails structure.
type GetV4UserEmailsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4UserEmailsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4UserEmailsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4UserEmailsOK creates a GetV4UserEmailsOK with default headers values
func NewGetV4UserEmailsOK() *GetV4UserEmailsOK {
	return &GetV4UserEmailsOK{}
}

/*GetV4UserEmailsOK handles this case with default header values.

Get the currently authenticated user's email addresses
*/
type GetV4UserEmailsOK struct {
	Payload *models.Email
}

func (o *GetV4UserEmailsOK) Error() string {
	return fmt.Sprintf("[GET /v4/user/emails][%d] getV4UserEmailsOK  %+v", 200, o.Payload)
}

func (o *GetV4UserEmailsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Email)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
