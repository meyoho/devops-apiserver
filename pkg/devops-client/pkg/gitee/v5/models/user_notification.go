// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"strconv"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// UserNotification 获取一条通知
// swagger:model UserNotification
type UserNotification struct {

	// 通知发送者
	Actor *UserBasic `json:"actor,omitempty"`

	// content
	Content string `json:"content,omitempty"`

	// html url
	HTMLURL string `json:"html_url,omitempty"`

	// id
	ID int32 `json:"id,omitempty"`

	// mute
	Mute string `json:"mute,omitempty"`

	// 通知次级关联对象
	Namespaces []*UserNotificationNamespace `json:"namespaces"`

	// repository
	Repository *ProjectBasic `json:"repository,omitempty"`

	// 通知直接关联对象
	Subject *UserNotificationSubject `json:"subject,omitempty"`

	// type
	Type string `json:"type,omitempty"`

	// unread
	Unread string `json:"unread,omitempty"`

	// updated at
	UpdatedAt string `json:"updated_at,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this user notification
func (m *UserNotification) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateActor(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateNamespaces(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateRepository(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateSubject(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *UserNotification) validateActor(formats strfmt.Registry) error {

	if swag.IsZero(m.Actor) { // not required
		return nil
	}

	if m.Actor != nil {
		if err := m.Actor.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("actor")
			}
			return err
		}
	}

	return nil
}

func (m *UserNotification) validateNamespaces(formats strfmt.Registry) error {

	if swag.IsZero(m.Namespaces) { // not required
		return nil
	}

	for i := 0; i < len(m.Namespaces); i++ {
		if swag.IsZero(m.Namespaces[i]) { // not required
			continue
		}

		if m.Namespaces[i] != nil {
			if err := m.Namespaces[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("namespaces" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

func (m *UserNotification) validateRepository(formats strfmt.Registry) error {

	if swag.IsZero(m.Repository) { // not required
		return nil
	}

	if m.Repository != nil {
		if err := m.Repository.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("repository")
			}
			return err
		}
	}

	return nil
}

func (m *UserNotification) validateSubject(formats strfmt.Registry) error {

	if swag.IsZero(m.Subject) { // not required
		return nil
	}

	if m.Subject != nil {
		if err := m.Subject.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("subject")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *UserNotification) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *UserNotification) UnmarshalBinary(b []byte) error {
	var res UserNotification
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
