// Code generated
package v5

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"

	"github.com/go-logr/logr"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/generic"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/client"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/client/organizations"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/client/repositories"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/client/users"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/models"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/transport"
	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	clientv1.NotImplement
	logger        logr.Logger
	client        *client.Gitee
	opts          *clientv1.Options
	authInfo      runtime.ClientAuthInfoWriter
	httpClient    *http.Client
	serviceClient *generic.ServiceClient
}

var _ clientv1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() clientv1.ClientFactory {
	return func(opts *clientv1.Options) clientv1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			ct := &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
			if ct.authInfo == nil {
				// adapter auth
				ct.init()
			}
			return ct
		}

		return &Client{client: client.Default}
	}

}

func (c *Client) init() {
	if c.opts != nil && c.opts.GenericOpts != nil {
		var (
			codeRepoService *v1.CodeRepoService
			secret          *corev1.Secret
		)

		for _, opt := range c.opts.GenericOpts.Options {
			switch opt.(type) {
			case *v1.CodeRepoService:
				codeRepoService = opt.(*v1.CodeRepoService)
			case *corev1.Secret:
				secret = opt.(*corev1.Secret)
			}
		}

		c.serviceClient = generic.NewServiceClient(codeRepoService, secret)
		// adapter auth info
		if c.serviceClient.IsBasicAuth() {
			c.authInfo = openapi.APIKeyAuth("access_token", "query", c.serviceClient.GetPassword())
		} else {
			c.serviceClient.OAuth2Info.AccessTokenKey = k8s.GiteeAccessTokenKey
			c.serviceClient.OAuth2Info.Scope = k8s.GiteeAccessTokenScope
			c.authInfo = openapi.APIKeyAuth("access_token", "query", c.serviceClient.OAuth2Info.AccessToken)
		}
		c.serviceClient.SetClient(c.httpClient)
	}
}

func (c *Client) GetAuthorizeUrl(ctx context.Context, redirectUrl string) string {
	return fmt.Sprintf("%s/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
		strings.TrimRight(c.serviceClient.GetHtml(), "/"), c.serviceClient.GetClientID(), redirectUrl, c.serviceClient.GetScope())
}

func (c *Client) getAccessTokenUrl() string {
	return fmt.Sprintf("%s/oauth/token", strings.TrimRight(c.serviceClient.GetHtml(), "/"))
}

type AccessTokenResponseGitee struct {
	generic.AccessTokenResponse
	Scope string `json:"scope"`

	RefreshToken string `json:"refresh_token"`
	CreatedAt    int64  `json:"created_at"`

	IDToken string `json:"id_token,omitempty"` // 'gitee-private' has this field
}

func (a *AccessTokenResponseGitee) ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details {
	return k8s.SecretDataOAuth2Details{
		AccessToken:  a.AccessToken,
		Scope:        a.Scope,
		RefreshToken: a.RefreshToken,
		CreatedAt:    time.Now().Format(time.RFC3339),
	}
}

func (c *Client) AccessToken(ctx context.Context, redirectUrl string) (*corev1.Secret, error) {
	var (
		accessTokenUrl = c.getAccessTokenUrl()
		response       = &AccessTokenResponseGitee{}
	)

	return c.serviceClient.AccessToken(response, redirectUrl, accessTokenUrl)
}

func (c *Client) listAllProjects(ctx context.Context) ([]*models.Project, error) {
	var (
		page     = int32(0)
		pageLen  = int32(5000)
		projects = []*models.Project{}
	)

	for {
		page = page + 1
		repoParams := repositories.
			NewGetV5UserReposParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithPage(&page).
			WithPerPage(&pageLen).
			WithAffiliation([]string{"owner", "collaborator", "organization_member", "enterprise_member", "admin"})
		reposOK, err := c.client.Repositories.GetV5UserRepos(repoParams, c.authInfo)
		if err != nil {
			return nil, err
		}

		projects = append(projects, reposOK.Payload...)

		if len(projects) < int(pageLen) {
			break
		}
	}

	return projects, nil
}

func (c *Client) listAllOrgs(ctx context.Context) ([]*models.Group, error) {
	var (
		page        = int32(0)
		pageLen     = int32(5000)
		admin       = bool(false)
		hasNextPage = true
		orgs        = []*models.Group{}
	)

	for hasNextPage {
		page = page + 1
		orgParam := organizations.
			NewGetV5UserOrgsParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithAdmin(&admin).
			WithPage(&page).
			WithPerPage(&pageLen)
		orgsOK, err := c.client.Organizations.GetV5UserOrgs(orgParam, c.authInfo)
		if err != nil {
			return nil, err
		}

		orgs = append(orgs, orgsOK.Payload...)

		if len(orgsOK.Payload) < int(pageLen) {
			hasNextPage = false
		}
	}

	return orgs, nil
}

func (c *Client) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo v1.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if giteeRepo, ok := remoteRepo.(*models.Project); ok {
		codeRepo = v1.OriginCodeRepository{
			CodeRepoServiceType: v1.CodeRepoServiceTypeGitee,
			ID:                  strconv.Itoa(int(giteeRepo.ID)),
			Name:                giteeRepo.Name,
			FullName:            giteeRepo.FullName,
			Description:         giteeRepo.Description,
			HTMLURL:             giteeRepo.HTMLURL,
			CloneURL:            appendGitSuffix(giteeRepo.HTMLURL),
			SSHURL:              giteeRepo.SSHURL,
			Language:            giteeRepo.Language,
			CreatedAt:           &metav1.Time{Time: generic.ConvertStringToTime(giteeRepo.CreatedAt)},
			PushedAt:            &metav1.Time{Time: generic.ConvertStringToTime(giteeRepo.PushedAt)},
			UpdatedAt:           &metav1.Time{Time: generic.ConvertStringToTime(giteeRepo.UpdatedAt)},
			Private:             giteeRepo.Private,
			Size:                0,
			SizeHumanize:        "0",
		}
	}
	return
}

func appendGitSuffix(url string) string {
	if strings.HasSuffix(url, ".git") {
		return url
	}
	return fmt.Sprintf("%s.git", url)
}

// GetRemoteRepos implements CodeRepoService
func (c *Client) GetRemoteRepos(ctx context.Context) (*v1.CodeRepoBindingRepositories, error) {
	projects, err := c.listAllProjects(ctx)
	if err != nil {
		return nil, err
	}

	orgs, err := c.listAllOrgs(ctx)
	if err != nil {
		return nil, err
	}

	dictOwners := make(map[string]*v1.CodeRepositoryOwner)
	for _, org := range orgs {
		dictOwners[org.Login] = &v1.CodeRepositoryOwner{
			Type:         v1.OriginCodeRepoRoleTypeOrg,
			ID:           strconv.Itoa(int(org.ID)),
			Name:         org.Login,
			HTMLURL:      org.URL,
			AvatarURL:    org.AvatarURL,
			Repositories: make([]v1.OriginCodeRepository, 0, 10),
		}
	}

	for _, project := range projects {
		var ownerKey string
		names := strings.Split(project.FullName, "/")
		if len(names) > 1 {
			ownerKey = names[0]
		} else {
			ownerKey = project.FullName
		}

		if owner, ok := dictOwners[ownerKey]; !ok {
			owner = &v1.CodeRepositoryOwner{
				Type:         v1.GetOwnerType(project.Owner.Type),
				ID:           strconv.Itoa(int(project.Owner.ID)),
				Name:         project.Owner.Login,
				Email:        "",
				HTMLURL:      project.HTMLURL,
				AvatarURL:    project.Owner.AvatarURL,
				DiskUsage:    0,
				Repositories: make([]v1.OriginCodeRepository, 0, 10),
			}
			dictOwners[ownerKey] = owner
		}

		codeRepo := c.ConvertRemoteRepoToBindingRepo(project)
		codeRepo.Owner = v1.OwnerInRepository{
			ID:   dictOwners[ownerKey].ID,
			Name: dictOwners[ownerKey].Name,
			Type: dictOwners[ownerKey].Type,
		}

		dictOwners[ownerKey].Repositories = append(dictOwners[ownerKey].Repositories, codeRepo)
	}

	result := &v1.CodeRepoBindingRepositories{
		Type:   v1.CodeRepoServiceTypeGitee,
		Owners: []v1.CodeRepositoryOwner{},
	}

	for _, owner := range dictOwners {
		if len(owner.Repositories) > 0 {
			result.Owners = append(result.Owners, *owner)
		}
	}

	return result, nil

}

// GetBranches implements CodeRepoService
func (c *Client) GetBranches(ctx context.Context, owner, repo, repoFullName string) ([]v1.CodeRepoBranch, error) {
	fullName := strings.Split(repoFullName, "/")
	if len(fullName) > 1 {
		repo = fullName[len(fullName)-1]
		owner = strings.Join(fullName[:len(fullName)-1], "/")
	}
	branchParam := repositories.
		NewGetV5ReposOwnerRepoBranchesParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithOwner(owner).
		WithRepo(repo)
	branchesOK, err := c.client.Repositories.GetV5ReposOwnerRepoBranches(branchParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := make([]v1.CodeRepoBranch, 0, len(branchesOK.Payload))
	for _, branch := range branchesOK.Payload {
		result = append(result, v1.CodeRepoBranch{
			Name:   branch.Name,
			Commit: branch.Commit.Sha,
		})
	}

	return result, nil
}

func (c *Client) giteeOrgAsProjectData(org models.Group) (*v1.ProjectData, error) {
	data, err := generic.MarshalToMapString(org)
	if err != nil {
		return nil, err
	}

	result := &v1.ProjectData{
		ObjectMeta: metav1.ObjectMeta{
			Name: org.Login,
			Annotations: map[string]string{
				"avatarURL":   org.AvatarURL,
				"accessPath":  org.Login,
				"description": org.Description,
				"type":        string(v1.OriginCodeRepoRoleTypeOrg),
			},
		},
		Data: data,
	}

	return result, nil
}

// CreateCodeRepoProject implements CodeRepoService
func (c *Client) CreateCodeRepoProject(ctx context.Context, opts v1.CreateProjectOptions) (*v1.ProjectData, error) {
	createOrgParam := organizations.
		NewPostV5UsersOrganizationParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithName(opts.Name).
		WithOrg(opts.Name).
		WithDescription(&opts.Name)
	createdOK, err := c.client.Organizations.PostV5UsersOrganization(createOrgParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	return c.giteeOrgAsProjectData(*createdOK.Payload)
}

func giteeUserAsProjectData(user *models.User) (*v1.ProjectData, error) {
	var projectData = &v1.ProjectData{
		ObjectMeta: metav1.ObjectMeta{
			Name: user.Login,
			Annotations: map[string]string{
				"avatarURL":   user.AvatarURL,
				"webURL":      "/" + user.Login,
				"description": "",
				"type":        string(v1.OriginCodeRepoOwnerTypeUser),
			},
		},
		Data: map[string]string{
			"login":   user.Login,
			"name":    user.Name,
			"id":      strconv.Itoa(int(user.ID)),
			"htmlURL": user.HTMLURL,
		},
	}
	return projectData, nil
}

func (c *Client) currentUser(ctx context.Context) (*v1.ProjectData, error) {
	userParams := users.
		NewGetV5UserParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	userOK, err := c.client.Users.GetV5User(userParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	return giteeUserAsProjectData(userOK.Payload)
}

// ListCodeRepoProjects implements CodeRepoService
func (c *Client) ListCodeRepoProjects(ctx context.Context, opts v1.ListProjectOptions) (*v1.ProjectDataList, error) {
	var (
		page    = int32(0)
		pageLen = int32(100)
		orgs    = []*models.Group{}
	)

	for {
		page = page + 1
		orgsParam := organizations.
			NewGetV5UserOrgsParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithPage(&page).
			WithPerPage(&pageLen)
		orgsOK, err := c.client.Organizations.GetV5UserOrgs(orgsParam, c.authInfo)
		if err != nil {
			return nil, err
		}

		orgs = append(orgs, orgsOK.Payload...)

		if len(orgsOK.Payload) < 100 {
			break
		}
	}

	items := make([]v1.ProjectData, 0, len(orgs))
	for _, org := range orgs {
		projectData, err := c.giteeOrgAsProjectData(*org)
		if err != nil {
			return nil, err
		}

		items = append(items, *projectData)
	}

	userProject, err := c.currentUser(ctx)
	if err != nil {
		return nil, err
	}

	items = append(items, *userProject)

	result := &v1.ProjectDataList{
		Items: items,
	}

	return result, nil
}

// GetLatestRepoCommit implements CodeRepoService
func (c *Client) GetLatestRepoCommit(ctx context.Context, repoID, owner, repoName, repoFullName string) (commit *v1.RepositoryCommit, status *v1.HostPortStatus) {
	params := repositories.
		NewGetV5ReposOwnerRepoCommitsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithOwner(owner).
		WithRepo(repoName)
	payload, err := c.client.Repositories.GetV5ReposOwnerRepoCommits(params, c.authInfo)
	if err != nil {
		return nil, &v1.HostPortStatus{
			StatusCode: 500,
			Response:   err.Error(),
		}
	}

	if len(payload.Payload) > 0 {
		commitAt, _ := time.Parse(time.RFC3339Nano, payload.Payload[0].Commit.Commiter.Date)
		commit = &v1.RepositoryCommit{
			CommitID:       payload.Payload[0].Sha,
			CommitAt:       &metav1.Time{Time: commitAt},
			CommitterName:  payload.Payload[0].Commit.Commiter.Name,
			CommitterEmail: payload.Payload[0].Commit.Commiter.Email,
			CommitMessage:  payload.Payload[0].Commit.Message,
		}
	}

	return commit, &v1.HostPortStatus{
		StatusCode:  200,
		LastAttempt: &metav1.Time{Time: time.Now()},
	}
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(c.httpClient, url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(ctx context.Context) (*v1.HostPortStatus, error) {
	params := users.
		NewGetV5UserParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	_, err := c.client.Users.GetV5User(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return &v1.HostPortStatus{StatusCode: 200}, nil
}
