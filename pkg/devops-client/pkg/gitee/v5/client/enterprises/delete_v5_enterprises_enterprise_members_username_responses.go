// Code generated by go-swagger; DO NOT EDIT.

package enterprises

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// DeleteV5EnterprisesEnterpriseMembersUsernameReader is a Reader for the DeleteV5EnterprisesEnterpriseMembersUsername structure.
type DeleteV5EnterprisesEnterpriseMembersUsernameReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteV5EnterprisesEnterpriseMembersUsernameReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 204:
		result := NewDeleteV5EnterprisesEnterpriseMembersUsernameNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewDeleteV5EnterprisesEnterpriseMembersUsernameForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewDeleteV5EnterprisesEnterpriseMembersUsernameNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewDeleteV5EnterprisesEnterpriseMembersUsernameNoContent creates a DeleteV5EnterprisesEnterpriseMembersUsernameNoContent with default headers values
func NewDeleteV5EnterprisesEnterpriseMembersUsernameNoContent() *DeleteV5EnterprisesEnterpriseMembersUsernameNoContent {
	return &DeleteV5EnterprisesEnterpriseMembersUsernameNoContent{}
}

/*DeleteV5EnterprisesEnterpriseMembersUsernameNoContent handles this case with default header values.

执行成功
*/
type DeleteV5EnterprisesEnterpriseMembersUsernameNoContent struct {
}

func (o *DeleteV5EnterprisesEnterpriseMembersUsernameNoContent) Error() string {
	return fmt.Sprintf("[DELETE /v5/enterprises/{enterprise}/members/{username}][%d] deleteV5EnterprisesEnterpriseMembersUsernameNoContent ", 204)
}

func (o *DeleteV5EnterprisesEnterpriseMembersUsernameNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewDeleteV5EnterprisesEnterpriseMembersUsernameForbidden creates a DeleteV5EnterprisesEnterpriseMembersUsernameForbidden with default headers values
func NewDeleteV5EnterprisesEnterpriseMembersUsernameForbidden() *DeleteV5EnterprisesEnterpriseMembersUsernameForbidden {
	return &DeleteV5EnterprisesEnterpriseMembersUsernameForbidden{}
}

/*DeleteV5EnterprisesEnterpriseMembersUsernameForbidden handles this case with default header values.

没有权限
*/
type DeleteV5EnterprisesEnterpriseMembersUsernameForbidden struct {
}

func (o *DeleteV5EnterprisesEnterpriseMembersUsernameForbidden) Error() string {
	return fmt.Sprintf("[DELETE /v5/enterprises/{enterprise}/members/{username}][%d] deleteV5EnterprisesEnterpriseMembersUsernameForbidden ", 403)
}

func (o *DeleteV5EnterprisesEnterpriseMembersUsernameForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewDeleteV5EnterprisesEnterpriseMembersUsernameNotFound creates a DeleteV5EnterprisesEnterpriseMembersUsernameNotFound with default headers values
func NewDeleteV5EnterprisesEnterpriseMembersUsernameNotFound() *DeleteV5EnterprisesEnterpriseMembersUsernameNotFound {
	return &DeleteV5EnterprisesEnterpriseMembersUsernameNotFound{}
}

/*DeleteV5EnterprisesEnterpriseMembersUsernameNotFound handles this case with default header values.

没有相关数据
*/
type DeleteV5EnterprisesEnterpriseMembersUsernameNotFound struct {
}

func (o *DeleteV5EnterprisesEnterpriseMembersUsernameNotFound) Error() string {
	return fmt.Sprintf("[DELETE /v5/enterprises/{enterprise}/members/{username}][%d] deleteV5EnterprisesEnterpriseMembersUsernameNotFound ", 404)
}

func (o *DeleteV5EnterprisesEnterpriseMembersUsernameNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
