// Code generated by go-swagger; DO NOT EDIT.

package enterprises

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/models"
)

// GetV5EnterprisesEnterpriseMembersReader is a Reader for the GetV5EnterprisesEnterpriseMembers structure.
type GetV5EnterprisesEnterpriseMembersReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV5EnterprisesEnterpriseMembersReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV5EnterprisesEnterpriseMembersOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewGetV5EnterprisesEnterpriseMembersForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewGetV5EnterprisesEnterpriseMembersNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV5EnterprisesEnterpriseMembersOK creates a GetV5EnterprisesEnterpriseMembersOK with default headers values
func NewGetV5EnterprisesEnterpriseMembersOK() *GetV5EnterprisesEnterpriseMembersOK {
	return &GetV5EnterprisesEnterpriseMembersOK{}
}

/*GetV5EnterprisesEnterpriseMembersOK handles this case with default header values.

返回格式
*/
type GetV5EnterprisesEnterpriseMembersOK struct {
	Payload []*models.EnterpriseMember
}

func (o *GetV5EnterprisesEnterpriseMembersOK) Error() string {
	return fmt.Sprintf("[GET /v5/enterprises/{enterprise}/members][%d] getV5EnterprisesEnterpriseMembersOK  %+v", 200, o.Payload)
}

func (o *GetV5EnterprisesEnterpriseMembersOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetV5EnterprisesEnterpriseMembersForbidden creates a GetV5EnterprisesEnterpriseMembersForbidden with default headers values
func NewGetV5EnterprisesEnterpriseMembersForbidden() *GetV5EnterprisesEnterpriseMembersForbidden {
	return &GetV5EnterprisesEnterpriseMembersForbidden{}
}

/*GetV5EnterprisesEnterpriseMembersForbidden handles this case with default header values.

没有权限
*/
type GetV5EnterprisesEnterpriseMembersForbidden struct {
}

func (o *GetV5EnterprisesEnterpriseMembersForbidden) Error() string {
	return fmt.Sprintf("[GET /v5/enterprises/{enterprise}/members][%d] getV5EnterprisesEnterpriseMembersForbidden ", 403)
}

func (o *GetV5EnterprisesEnterpriseMembersForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewGetV5EnterprisesEnterpriseMembersNotFound creates a GetV5EnterprisesEnterpriseMembersNotFound with default headers values
func NewGetV5EnterprisesEnterpriseMembersNotFound() *GetV5EnterprisesEnterpriseMembersNotFound {
	return &GetV5EnterprisesEnterpriseMembersNotFound{}
}

/*GetV5EnterprisesEnterpriseMembersNotFound handles this case with default header values.

没有相关数据
*/
type GetV5EnterprisesEnterpriseMembersNotFound struct {
}

func (o *GetV5EnterprisesEnterpriseMembersNotFound) Error() string {
	return fmt.Sprintf("[GET /v5/enterprises/{enterprise}/members][%d] getV5EnterprisesEnterpriseMembersNotFound ", 404)
}

func (o *GetV5EnterprisesEnterpriseMembersNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
