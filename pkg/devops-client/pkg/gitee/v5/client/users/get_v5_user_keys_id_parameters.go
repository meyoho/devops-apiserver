// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV5UserKeysIDParams creates a new GetV5UserKeysIDParams object
// with the default values initialized.
func NewGetV5UserKeysIDParams() *GetV5UserKeysIDParams {
	var ()
	return &GetV5UserKeysIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV5UserKeysIDParamsWithTimeout creates a new GetV5UserKeysIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV5UserKeysIDParamsWithTimeout(timeout time.Duration) *GetV5UserKeysIDParams {
	var ()
	return &GetV5UserKeysIDParams{

		timeout: timeout,
	}
}

// NewGetV5UserKeysIDParamsWithContext creates a new GetV5UserKeysIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV5UserKeysIDParamsWithContext(ctx context.Context) *GetV5UserKeysIDParams {
	var ()
	return &GetV5UserKeysIDParams{

		Context: ctx,
	}
}

// NewGetV5UserKeysIDParamsWithHTTPClient creates a new GetV5UserKeysIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV5UserKeysIDParamsWithHTTPClient(client *http.Client) *GetV5UserKeysIDParams {
	var ()
	return &GetV5UserKeysIDParams{
		HTTPClient: client,
	}
}

/*GetV5UserKeysIDParams contains all the parameters to send to the API endpoint
for the get v5 user keys Id operation typically these are written to a http.Request
*/
type GetV5UserKeysIDParams struct {

	/*AccessToken
	  用户授权码

	*/
	AccessToken *string
	/*ID
	  公钥 ID

	*/
	ID int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v5 user keys Id params
func (o *GetV5UserKeysIDParams) WithTimeout(timeout time.Duration) *GetV5UserKeysIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v5 user keys Id params
func (o *GetV5UserKeysIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v5 user keys Id params
func (o *GetV5UserKeysIDParams) WithContext(ctx context.Context) *GetV5UserKeysIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v5 user keys Id params
func (o *GetV5UserKeysIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v5 user keys Id params
func (o *GetV5UserKeysIDParams) WithHTTPClient(client *http.Client) *GetV5UserKeysIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v5 user keys Id params
func (o *GetV5UserKeysIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccessToken adds the accessToken to the get v5 user keys Id params
func (o *GetV5UserKeysIDParams) WithAccessToken(accessToken *string) *GetV5UserKeysIDParams {
	o.SetAccessToken(accessToken)
	return o
}

// SetAccessToken adds the accessToken to the get v5 user keys Id params
func (o *GetV5UserKeysIDParams) SetAccessToken(accessToken *string) {
	o.AccessToken = accessToken
}

// WithID adds the id to the get v5 user keys Id params
func (o *GetV5UserKeysIDParams) WithID(id int32) *GetV5UserKeysIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get v5 user keys Id params
func (o *GetV5UserKeysIDParams) SetID(id int32) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *GetV5UserKeysIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AccessToken != nil {

		// query param access_token
		var qrAccessToken string
		if o.AccessToken != nil {
			qrAccessToken = *o.AccessToken
		}
		qAccessToken := qrAccessToken
		if qAccessToken != "" {
			if err := r.SetQueryParam("access_token", qAccessToken); err != nil {
				return err
			}
		}

	}

	// path param id
	if err := r.SetPathParam("id", swag.FormatInt32(o.ID)); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
