// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/models"
)

// GetV5UserReader is a Reader for the GetV5User structure.
type GetV5UserReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV5UserReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV5UserOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 404:
		result := NewGetV5UserNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV5UserOK creates a GetV5UserOK with default headers values
func NewGetV5UserOK() *GetV5UserOK {
	return &GetV5UserOK{}
}

/*GetV5UserOK handles this case with default header values.

返回格式
*/
type GetV5UserOK struct {
	Payload *models.User
}

func (o *GetV5UserOK) Error() string {
	return fmt.Sprintf("[GET /v5/user][%d] getV5UserOK  %+v", 200, o.Payload)
}

func (o *GetV5UserOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.User)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetV5UserNotFound creates a GetV5UserNotFound with default headers values
func NewGetV5UserNotFound() *GetV5UserNotFound {
	return &GetV5UserNotFound{}
}

/*GetV5UserNotFound handles this case with default header values.

没有相关数据
*/
type GetV5UserNotFound struct {
}

func (o *GetV5UserNotFound) Error() string {
	return fmt.Sprintf("[GET /v5/user][%d] getV5UserNotFound ", 404)
}

func (o *GetV5UserNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
