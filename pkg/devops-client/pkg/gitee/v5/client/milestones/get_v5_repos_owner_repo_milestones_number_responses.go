// Code generated by go-swagger; DO NOT EDIT.

package milestones

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/models"
)

// GetV5ReposOwnerRepoMilestonesNumberReader is a Reader for the GetV5ReposOwnerRepoMilestonesNumber structure.
type GetV5ReposOwnerRepoMilestonesNumberReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV5ReposOwnerRepoMilestonesNumberReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV5ReposOwnerRepoMilestonesNumberOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV5ReposOwnerRepoMilestonesNumberOK creates a GetV5ReposOwnerRepoMilestonesNumberOK with default headers values
func NewGetV5ReposOwnerRepoMilestonesNumberOK() *GetV5ReposOwnerRepoMilestonesNumberOK {
	return &GetV5ReposOwnerRepoMilestonesNumberOK{}
}

/*GetV5ReposOwnerRepoMilestonesNumberOK handles this case with default header values.

返回格式
*/
type GetV5ReposOwnerRepoMilestonesNumberOK struct {
	Payload *models.Milestone
}

func (o *GetV5ReposOwnerRepoMilestonesNumberOK) Error() string {
	return fmt.Sprintf("[GET /v5/repos/{owner}/{repo}/milestones/{number}][%d] getV5ReposOwnerRepoMilestonesNumberOK  %+v", 200, o.Payload)
}

func (o *GetV5ReposOwnerRepoMilestonesNumberOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Milestone)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
