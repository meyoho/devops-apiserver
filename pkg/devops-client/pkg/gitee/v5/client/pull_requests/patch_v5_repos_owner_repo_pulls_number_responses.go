// Code generated by go-swagger; DO NOT EDIT.

package pull_requests

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/models"
)

// PatchV5ReposOwnerRepoPullsNumberReader is a Reader for the PatchV5ReposOwnerRepoPullsNumber structure.
type PatchV5ReposOwnerRepoPullsNumberReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PatchV5ReposOwnerRepoPullsNumberReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPatchV5ReposOwnerRepoPullsNumberOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPatchV5ReposOwnerRepoPullsNumberOK creates a PatchV5ReposOwnerRepoPullsNumberOK with default headers values
func NewPatchV5ReposOwnerRepoPullsNumberOK() *PatchV5ReposOwnerRepoPullsNumberOK {
	return &PatchV5ReposOwnerRepoPullsNumberOK{}
}

/*PatchV5ReposOwnerRepoPullsNumberOK handles this case with default header values.

返回格式
*/
type PatchV5ReposOwnerRepoPullsNumberOK struct {
	Payload *models.PullRequest
}

func (o *PatchV5ReposOwnerRepoPullsNumberOK) Error() string {
	return fmt.Sprintf("[PATCH /v5/repos/{owner}/{repo}/pulls/{number}][%d] patchV5ReposOwnerRepoPullsNumberOK  %+v", 200, o.Payload)
}

func (o *PatchV5ReposOwnerRepoPullsNumberOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.PullRequest)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
