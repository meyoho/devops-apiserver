// Code generated by go-swagger; DO NOT EDIT.

package pull_requests

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV5ReposOwnerRepoPullsNumberParams creates a new GetV5ReposOwnerRepoPullsNumberParams object
// with the default values initialized.
func NewGetV5ReposOwnerRepoPullsNumberParams() *GetV5ReposOwnerRepoPullsNumberParams {
	var ()
	return &GetV5ReposOwnerRepoPullsNumberParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV5ReposOwnerRepoPullsNumberParamsWithTimeout creates a new GetV5ReposOwnerRepoPullsNumberParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV5ReposOwnerRepoPullsNumberParamsWithTimeout(timeout time.Duration) *GetV5ReposOwnerRepoPullsNumberParams {
	var ()
	return &GetV5ReposOwnerRepoPullsNumberParams{

		timeout: timeout,
	}
}

// NewGetV5ReposOwnerRepoPullsNumberParamsWithContext creates a new GetV5ReposOwnerRepoPullsNumberParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV5ReposOwnerRepoPullsNumberParamsWithContext(ctx context.Context) *GetV5ReposOwnerRepoPullsNumberParams {
	var ()
	return &GetV5ReposOwnerRepoPullsNumberParams{

		Context: ctx,
	}
}

// NewGetV5ReposOwnerRepoPullsNumberParamsWithHTTPClient creates a new GetV5ReposOwnerRepoPullsNumberParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV5ReposOwnerRepoPullsNumberParamsWithHTTPClient(client *http.Client) *GetV5ReposOwnerRepoPullsNumberParams {
	var ()
	return &GetV5ReposOwnerRepoPullsNumberParams{
		HTTPClient: client,
	}
}

/*GetV5ReposOwnerRepoPullsNumberParams contains all the parameters to send to the API endpoint
for the get v5 repos owner repo pulls number operation typically these are written to a http.Request
*/
type GetV5ReposOwnerRepoPullsNumberParams struct {

	/*AccessToken
	  用户授权码

	*/
	AccessToken *string
	/*Number
	  第几个PR，即本仓库PR的序数

	*/
	Number int32
	/*Owner
	  仓库所属空间地址(企业、组织或个人的地址path)

	*/
	Owner string
	/*Repo
	  仓库路径(path)

	*/
	Repo string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) WithTimeout(timeout time.Duration) *GetV5ReposOwnerRepoPullsNumberParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) WithContext(ctx context.Context) *GetV5ReposOwnerRepoPullsNumberParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) WithHTTPClient(client *http.Client) *GetV5ReposOwnerRepoPullsNumberParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccessToken adds the accessToken to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) WithAccessToken(accessToken *string) *GetV5ReposOwnerRepoPullsNumberParams {
	o.SetAccessToken(accessToken)
	return o
}

// SetAccessToken adds the accessToken to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) SetAccessToken(accessToken *string) {
	o.AccessToken = accessToken
}

// WithNumber adds the number to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) WithNumber(number int32) *GetV5ReposOwnerRepoPullsNumberParams {
	o.SetNumber(number)
	return o
}

// SetNumber adds the number to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) SetNumber(number int32) {
	o.Number = number
}

// WithOwner adds the owner to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) WithOwner(owner string) *GetV5ReposOwnerRepoPullsNumberParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithRepo adds the repo to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) WithRepo(repo string) *GetV5ReposOwnerRepoPullsNumberParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the get v5 repos owner repo pulls number params
func (o *GetV5ReposOwnerRepoPullsNumberParams) SetRepo(repo string) {
	o.Repo = repo
}

// WriteToRequest writes these params to a swagger request
func (o *GetV5ReposOwnerRepoPullsNumberParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AccessToken != nil {

		// query param access_token
		var qrAccessToken string
		if o.AccessToken != nil {
			qrAccessToken = *o.AccessToken
		}
		qAccessToken := qrAccessToken
		if qAccessToken != "" {
			if err := r.SetQueryParam("access_token", qAccessToken); err != nil {
				return err
			}
		}

	}

	// path param number
	if err := r.SetPathParam("number", swag.FormatInt32(o.Number)); err != nil {
		return err
	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
