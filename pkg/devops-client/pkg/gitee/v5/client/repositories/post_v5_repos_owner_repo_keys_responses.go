// Code generated by go-swagger; DO NOT EDIT.

package repositories

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/models"
)

// PostV5ReposOwnerRepoKeysReader is a Reader for the PostV5ReposOwnerRepoKeys structure.
type PostV5ReposOwnerRepoKeysReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostV5ReposOwnerRepoKeysReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostV5ReposOwnerRepoKeysCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostV5ReposOwnerRepoKeysCreated creates a PostV5ReposOwnerRepoKeysCreated with default headers values
func NewPostV5ReposOwnerRepoKeysCreated() *PostV5ReposOwnerRepoKeysCreated {
	return &PostV5ReposOwnerRepoKeysCreated{}
}

/*PostV5ReposOwnerRepoKeysCreated handles this case with default header values.

返回格式
*/
type PostV5ReposOwnerRepoKeysCreated struct {
	Payload *models.SSHKey
}

func (o *PostV5ReposOwnerRepoKeysCreated) Error() string {
	return fmt.Sprintf("[POST /v5/repos/{owner}/{repo}/keys][%d] postV5ReposOwnerRepoKeysCreated  %+v", 201, o.Payload)
}

func (o *PostV5ReposOwnerRepoKeysCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.SSHKey)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
