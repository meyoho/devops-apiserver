// Code generated by go-swagger; DO NOT EDIT.

package repositories

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewDeleteV5ReposOwnerRepoReleasesIDParams creates a new DeleteV5ReposOwnerRepoReleasesIDParams object
// with the default values initialized.
func NewDeleteV5ReposOwnerRepoReleasesIDParams() *DeleteV5ReposOwnerRepoReleasesIDParams {
	var ()
	return &DeleteV5ReposOwnerRepoReleasesIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewDeleteV5ReposOwnerRepoReleasesIDParamsWithTimeout creates a new DeleteV5ReposOwnerRepoReleasesIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewDeleteV5ReposOwnerRepoReleasesIDParamsWithTimeout(timeout time.Duration) *DeleteV5ReposOwnerRepoReleasesIDParams {
	var ()
	return &DeleteV5ReposOwnerRepoReleasesIDParams{

		timeout: timeout,
	}
}

// NewDeleteV5ReposOwnerRepoReleasesIDParamsWithContext creates a new DeleteV5ReposOwnerRepoReleasesIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewDeleteV5ReposOwnerRepoReleasesIDParamsWithContext(ctx context.Context) *DeleteV5ReposOwnerRepoReleasesIDParams {
	var ()
	return &DeleteV5ReposOwnerRepoReleasesIDParams{

		Context: ctx,
	}
}

// NewDeleteV5ReposOwnerRepoReleasesIDParamsWithHTTPClient creates a new DeleteV5ReposOwnerRepoReleasesIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewDeleteV5ReposOwnerRepoReleasesIDParamsWithHTTPClient(client *http.Client) *DeleteV5ReposOwnerRepoReleasesIDParams {
	var ()
	return &DeleteV5ReposOwnerRepoReleasesIDParams{
		HTTPClient: client,
	}
}

/*DeleteV5ReposOwnerRepoReleasesIDParams contains all the parameters to send to the API endpoint
for the delete v5 repos owner repo releases Id operation typically these are written to a http.Request
*/
type DeleteV5ReposOwnerRepoReleasesIDParams struct {

	/*AccessToken
	  用户授权码

	*/
	AccessToken *string
	/*ID*/
	ID int32
	/*Owner
	  仓库所属空间地址(企业、组织或个人的地址path)

	*/
	Owner string
	/*Repo
	  仓库路径(path)

	*/
	Repo string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) WithTimeout(timeout time.Duration) *DeleteV5ReposOwnerRepoReleasesIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) WithContext(ctx context.Context) *DeleteV5ReposOwnerRepoReleasesIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) WithHTTPClient(client *http.Client) *DeleteV5ReposOwnerRepoReleasesIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccessToken adds the accessToken to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) WithAccessToken(accessToken *string) *DeleteV5ReposOwnerRepoReleasesIDParams {
	o.SetAccessToken(accessToken)
	return o
}

// SetAccessToken adds the accessToken to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) SetAccessToken(accessToken *string) {
	o.AccessToken = accessToken
}

// WithID adds the id to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) WithID(id int32) *DeleteV5ReposOwnerRepoReleasesIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) SetID(id int32) {
	o.ID = id
}

// WithOwner adds the owner to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) WithOwner(owner string) *DeleteV5ReposOwnerRepoReleasesIDParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithRepo adds the repo to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) WithRepo(repo string) *DeleteV5ReposOwnerRepoReleasesIDParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the delete v5 repos owner repo releases Id params
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) SetRepo(repo string) {
	o.Repo = repo
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteV5ReposOwnerRepoReleasesIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AccessToken != nil {

		// query param access_token
		var qrAccessToken string
		if o.AccessToken != nil {
			qrAccessToken = *o.AccessToken
		}
		qAccessToken := qrAccessToken
		if qAccessToken != "" {
			if err := r.SetQueryParam("access_token", qAccessToken); err != nil {
				return err
			}
		}

	}

	// path param id
	if err := r.SetPathParam("id", swag.FormatInt32(o.ID)); err != nil {
		return err
	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
