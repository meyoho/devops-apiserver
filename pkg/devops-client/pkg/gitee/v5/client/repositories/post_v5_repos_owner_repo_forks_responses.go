// Code generated by go-swagger; DO NOT EDIT.

package repositories

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/models"
)

// PostV5ReposOwnerRepoForksReader is a Reader for the PostV5ReposOwnerRepoForks structure.
type PostV5ReposOwnerRepoForksReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostV5ReposOwnerRepoForksReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostV5ReposOwnerRepoForksCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostV5ReposOwnerRepoForksCreated creates a PostV5ReposOwnerRepoForksCreated with default headers values
func NewPostV5ReposOwnerRepoForksCreated() *PostV5ReposOwnerRepoForksCreated {
	return &PostV5ReposOwnerRepoForksCreated{}
}

/*PostV5ReposOwnerRepoForksCreated handles this case with default header values.

返回格式
*/
type PostV5ReposOwnerRepoForksCreated struct {
	Payload *models.Project
}

func (o *PostV5ReposOwnerRepoForksCreated) Error() string {
	return fmt.Sprintf("[POST /v5/repos/{owner}/{repo}/forks][%d] postV5ReposOwnerRepoForksCreated  %+v", 201, o.Payload)
}

func (o *PostV5ReposOwnerRepoForksCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Project)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
