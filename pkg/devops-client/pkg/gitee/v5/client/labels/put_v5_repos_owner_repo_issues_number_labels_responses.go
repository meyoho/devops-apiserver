// Code generated by go-swagger; DO NOT EDIT.

package labels

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/models"
)

// PutV5ReposOwnerRepoIssuesNumberLabelsReader is a Reader for the PutV5ReposOwnerRepoIssuesNumberLabels structure.
type PutV5ReposOwnerRepoIssuesNumberLabelsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PutV5ReposOwnerRepoIssuesNumberLabelsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPutV5ReposOwnerRepoIssuesNumberLabelsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPutV5ReposOwnerRepoIssuesNumberLabelsOK creates a PutV5ReposOwnerRepoIssuesNumberLabelsOK with default headers values
func NewPutV5ReposOwnerRepoIssuesNumberLabelsOK() *PutV5ReposOwnerRepoIssuesNumberLabelsOK {
	return &PutV5ReposOwnerRepoIssuesNumberLabelsOK{}
}

/*PutV5ReposOwnerRepoIssuesNumberLabelsOK handles this case with default header values.

返回格式
*/
type PutV5ReposOwnerRepoIssuesNumberLabelsOK struct {
	Payload *models.Label
}

func (o *PutV5ReposOwnerRepoIssuesNumberLabelsOK) Error() string {
	return fmt.Sprintf("[PUT /v5/repos/{owner}/{repo}/issues/{number}/labels][%d] putV5ReposOwnerRepoIssuesNumberLabelsOK  %+v", 200, o.Payload)
}

func (o *PutV5ReposOwnerRepoIssuesNumberLabelsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Label)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
