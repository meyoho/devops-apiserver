// Code generated by go-swagger; DO NOT EDIT.

package organizations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/models"
)

// GetV5UserMembershipsOrgsReader is a Reader for the GetV5UserMembershipsOrgs structure.
type GetV5UserMembershipsOrgsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV5UserMembershipsOrgsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV5UserMembershipsOrgsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV5UserMembershipsOrgsOK creates a GetV5UserMembershipsOrgsOK with default headers values
func NewGetV5UserMembershipsOrgsOK() *GetV5UserMembershipsOrgsOK {
	return &GetV5UserMembershipsOrgsOK{}
}

/*GetV5UserMembershipsOrgsOK handles this case with default header values.

返回格式
*/
type GetV5UserMembershipsOrgsOK struct {
	Payload []*models.GroupMember
}

func (o *GetV5UserMembershipsOrgsOK) Error() string {
	return fmt.Sprintf("[GET /v5/user/memberships/orgs][%d] getV5UserMembershipsOrgsOK  %+v", 200, o.Payload)
}

func (o *GetV5UserMembershipsOrgsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
