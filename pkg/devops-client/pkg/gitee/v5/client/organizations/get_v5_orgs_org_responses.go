// Code generated by go-swagger; DO NOT EDIT.

package organizations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/models"
)

// GetV5OrgsOrgReader is a Reader for the GetV5OrgsOrg structure.
type GetV5OrgsOrgReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV5OrgsOrgReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV5OrgsOrgOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewGetV5OrgsOrgForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewGetV5OrgsOrgNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV5OrgsOrgOK creates a GetV5OrgsOrgOK with default headers values
func NewGetV5OrgsOrgOK() *GetV5OrgsOrgOK {
	return &GetV5OrgsOrgOK{}
}

/*GetV5OrgsOrgOK handles this case with default header values.

返回格式
*/
type GetV5OrgsOrgOK struct {
	Payload *models.Group
}

func (o *GetV5OrgsOrgOK) Error() string {
	return fmt.Sprintf("[GET /v5/orgs/{org}][%d] getV5OrgsOrgOK  %+v", 200, o.Payload)
}

func (o *GetV5OrgsOrgOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Group)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetV5OrgsOrgForbidden creates a GetV5OrgsOrgForbidden with default headers values
func NewGetV5OrgsOrgForbidden() *GetV5OrgsOrgForbidden {
	return &GetV5OrgsOrgForbidden{}
}

/*GetV5OrgsOrgForbidden handles this case with default header values.

没有权限
*/
type GetV5OrgsOrgForbidden struct {
}

func (o *GetV5OrgsOrgForbidden) Error() string {
	return fmt.Sprintf("[GET /v5/orgs/{org}][%d] getV5OrgsOrgForbidden ", 403)
}

func (o *GetV5OrgsOrgForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewGetV5OrgsOrgNotFound creates a GetV5OrgsOrgNotFound with default headers values
func NewGetV5OrgsOrgNotFound() *GetV5OrgsOrgNotFound {
	return &GetV5OrgsOrgNotFound{}
}

/*GetV5OrgsOrgNotFound handles this case with default header values.

没有相关数据
*/
type GetV5OrgsOrgNotFound struct {
}

func (o *GetV5OrgsOrgNotFound) Error() string {
	return fmt.Sprintf("[GET /v5/orgs/{org}][%d] getV5OrgsOrgNotFound ", 404)
}

func (o *GetV5OrgsOrgNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
