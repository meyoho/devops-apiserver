// Code generated by go-swagger; DO NOT EDIT.

package gists

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitee/v5/models"
)

// PatchV5GistsIDReader is a Reader for the PatchV5GistsID structure.
type PatchV5GistsIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PatchV5GistsIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPatchV5GistsIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPatchV5GistsIDOK creates a PatchV5GistsIDOK with default headers values
func NewPatchV5GistsIDOK() *PatchV5GistsIDOK {
	return &PatchV5GistsIDOK{}
}

/*PatchV5GistsIDOK handles this case with default header values.

返回格式
*/
type PatchV5GistsIDOK struct {
	Payload *models.CodeForksHistory
}

func (o *PatchV5GistsIDOK) Error() string {
	return fmt.Sprintf("[PATCH /v5/gists/{id}][%d] patchV5GistsIdOK  %+v", 200, o.Payload)
}

func (o *PatchV5GistsIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.CodeForksHistory)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
