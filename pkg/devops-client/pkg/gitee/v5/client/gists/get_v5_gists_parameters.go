// Code generated by go-swagger; DO NOT EDIT.

package gists

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV5GistsParams creates a new GetV5GistsParams object
// with the default values initialized.
func NewGetV5GistsParams() *GetV5GistsParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
	)
	return &GetV5GistsParams{
		Page:    &pageDefault,
		PerPage: &perPageDefault,

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV5GistsParamsWithTimeout creates a new GetV5GistsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV5GistsParamsWithTimeout(timeout time.Duration) *GetV5GistsParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
	)
	return &GetV5GistsParams{
		Page:    &pageDefault,
		PerPage: &perPageDefault,

		timeout: timeout,
	}
}

// NewGetV5GistsParamsWithContext creates a new GetV5GistsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV5GistsParamsWithContext(ctx context.Context) *GetV5GistsParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
	)
	return &GetV5GistsParams{
		Page:    &pageDefault,
		PerPage: &perPageDefault,

		Context: ctx,
	}
}

// NewGetV5GistsParamsWithHTTPClient creates a new GetV5GistsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV5GistsParamsWithHTTPClient(client *http.Client) *GetV5GistsParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
	)
	return &GetV5GistsParams{
		Page:       &pageDefault,
		PerPage:    &perPageDefault,
		HTTPClient: client,
	}
}

/*GetV5GistsParams contains all the parameters to send to the API endpoint
for the get v5 gists operation typically these are written to a http.Request
*/
type GetV5GistsParams struct {

	/*AccessToken
	  用户授权码

	*/
	AccessToken *string
	/*Page
	  当前的页码

	*/
	Page *int32
	/*PerPage
	  每页的数量，最大为 100

	*/
	PerPage *int32
	/*Since
	  起始的更新时间，要求时间格式为 ISO 8601

	*/
	Since *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v5 gists params
func (o *GetV5GistsParams) WithTimeout(timeout time.Duration) *GetV5GistsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v5 gists params
func (o *GetV5GistsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v5 gists params
func (o *GetV5GistsParams) WithContext(ctx context.Context) *GetV5GistsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v5 gists params
func (o *GetV5GistsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v5 gists params
func (o *GetV5GistsParams) WithHTTPClient(client *http.Client) *GetV5GistsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v5 gists params
func (o *GetV5GistsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccessToken adds the accessToken to the get v5 gists params
func (o *GetV5GistsParams) WithAccessToken(accessToken *string) *GetV5GistsParams {
	o.SetAccessToken(accessToken)
	return o
}

// SetAccessToken adds the accessToken to the get v5 gists params
func (o *GetV5GistsParams) SetAccessToken(accessToken *string) {
	o.AccessToken = accessToken
}

// WithPage adds the page to the get v5 gists params
func (o *GetV5GistsParams) WithPage(page *int32) *GetV5GistsParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get v5 gists params
func (o *GetV5GistsParams) SetPage(page *int32) {
	o.Page = page
}

// WithPerPage adds the perPage to the get v5 gists params
func (o *GetV5GistsParams) WithPerPage(perPage *int32) *GetV5GistsParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the get v5 gists params
func (o *GetV5GistsParams) SetPerPage(perPage *int32) {
	o.PerPage = perPage
}

// WithSince adds the since to the get v5 gists params
func (o *GetV5GistsParams) WithSince(since *string) *GetV5GistsParams {
	o.SetSince(since)
	return o
}

// SetSince adds the since to the get v5 gists params
func (o *GetV5GistsParams) SetSince(since *string) {
	o.Since = since
}

// WriteToRequest writes these params to a swagger request
func (o *GetV5GistsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AccessToken != nil {

		// query param access_token
		var qrAccessToken string
		if o.AccessToken != nil {
			qrAccessToken = *o.AccessToken
		}
		qAccessToken := qrAccessToken
		if qAccessToken != "" {
			if err := r.SetQueryParam("access_token", qAccessToken); err != nil {
				return err
			}
		}

	}

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int32
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt32(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	if o.Since != nil {

		// query param since
		var qrSince string
		if o.Since != nil {
			qrSince = *o.Since
		}
		qSince := qrSince
		if qSince != "" {
			if err := r.SetQueryParam("since", qSince); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
