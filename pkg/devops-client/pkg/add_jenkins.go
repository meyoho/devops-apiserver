package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/jenkins/v1"
)

func init() {
	register(devops.TypeJenkins, versionOpt{
		version:   "v1",
		factory:   v1.NewFactory(),
		isDefault: true,
	})
}
