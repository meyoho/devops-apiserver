package generic

import (
	"io/ioutil"
	"net/http"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"
)

// CheckService generic checker
func CheckService(ct *http.Client, url string, header map[string]string) (status *v1.HostPortStatus, err error) {
	var req *http.Request
	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}

	if header == nil {
		header = map[string]string{}
	}

	if len(header) > 0 {
		for key, val := range header {
			req.Header.Add(key, val)
		}
	}

	return ConvertResponseToHostPortStatus(ct.Do(req))
}

func ConvertResponseToHostPortStatus(response *http.Response, err error) (*v1.HostPortStatus, error) {
	var data []byte
	lastAttempt := metav1.Now()
	status := &v1.HostPortStatus{
		StatusCode:  -1,
		LastAttempt: &lastAttempt,
	}

	if err != nil {
		status.ErrorMessage = err.Error()
	}

	if response != nil {
		status.StatusCode = response.StatusCode
		if !response.Close {
			defer response.Body.Close()
			data, err = ioutil.ReadAll(response.Body)
			if err != nil {
				return status, err
			}
			status.Response = string(data)
		}
	}
	return status, err
}
