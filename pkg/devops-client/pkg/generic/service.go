package generic

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/generic"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

type authType int

const (
	basicAuth authType = iota
	oAuthToken
	privateToken
)

type AccessTokenResponse struct {
	TokenType   string `json:"token_type"` // is always 'bearer'
	AccessToken string `json:"access_token"`
}

func (a *AccessTokenResponse) GetAccessToken() string {
	return a.AccessToken
}

type AccessTokenResponseInterface interface {
	ConvertToSecretDataOAuth2Details() k8s.SecretDataOAuth2Details
	GetAccessToken() string
}

type ServiceClient struct {
	AuthType      authType
	Service       *devops.CodeRepoService
	Secret        *corev1.Secret
	BasicAuthInfo *k8s.SecretDataBasicAuth
	OAuth2Info    *k8s.SecretDataOAuth2
	RoundTripper  http.RoundTripper
	Client        *http.Client
}

func NewServiceClient(service *devops.CodeRepoService, secret *corev1.Secret) *ServiceClient {
	client := ServiceClient{
		Service: service,
		Secret:  secret,
	}

	client.AuthType = basicAuth
	client.BasicAuthInfo = k8s.GetDataBasicAuthFromSecret(secret)
	if secret != nil && secret.Type == devops.SecretTypeOAuth2 {
		client.AuthType = oAuthToken
		client.OAuth2Info = k8s.GetDataOAuth2FromSecret(secret)
	}
	return &client
}

func (s *ServiceClient) SetTransport(rt http.RoundTripper) {
	s.RoundTripper = rt
}

func (s *ServiceClient) SetClient(client *http.Client) {
	s.Client = client
}

func (s *ServiceClient) getHTTPClient() *http.Client {
	if s.Client == nil {
		s.Client = generic.NewHTTPClient()
	}
	if s.RoundTripper == nil {
		s.RoundTripper = generic.GetDefaultTransport()
	}
	if s.Client.Transport == nil {
		s.Client.Transport = s.RoundTripper
	}
	return s.Client
}

func (s *ServiceClient) IsBasicAuth() bool {
	return s.AuthType == basicAuth
}

func (s ServiceClient) getServiceType() devops.CodeRepoServiceType {
	return s.Service.Spec.Type
}

func (s *ServiceClient) getHost() string {
	return s.Service.GetHost()
}

func (s *ServiceClient) GetHtml() string {
	return s.Service.GetHtml()
}

func (s *ServiceClient) GetUsername() string {
	if s.BasicAuthInfo == nil {
		return ""
	}
	return s.BasicAuthInfo.Username
}

func (s *ServiceClient) GetPassword() string {
	if s.BasicAuthInfo == nil {
		return ""
	}
	return s.BasicAuthInfo.Password
}

func (s *ServiceClient) GetClientID() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.ClientID
}

func (s *ServiceClient) GetClientSecret() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.ClientSecret
}

func (s *ServiceClient) getCode() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.Code
}

func (s *ServiceClient) getRefreshToken() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.RefreshToken
}

func (s *ServiceClient) getAccessTokenKey() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.AccessTokenKey
}

func (s *ServiceClient) getAccessToken() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.AccessToken
}

func (s *ServiceClient) GetScope() string {
	if s.OAuth2Info == nil {
		return ""
	}
	return s.OAuth2Info.Scope
}

func (c *ServiceClient) getAuthorizePayload(redirectUrl, scope string) map[string]interface{} {
	return map[string]interface{}{
		"client_id":     c.GetClientID(),
		"redirect_uri":  redirectUrl,
		"response_type": "code",
		"scope":         scope,
	}
}

func (c *ServiceClient) getAccessTokenPayload(redirectUrl string) map[string]interface{} {
	return map[string]interface{}{
		"grant_type":    "authorization_code",
		"code":          c.getCode(),
		"client_id":     c.GetClientID(),
		"client_secret": c.GetClientSecret(),
		"redirect_uri":  redirectUrl,
	}
}

func (c *ServiceClient) getRefreshTokenPayload() map[string]interface{} {
	return map[string]interface{}{
		"grant_type":    "refresh_token",
		"refresh_token": c.getRefreshToken(),
		"client_id":     c.GetClientID(),
		"client_secret": c.GetClientSecret(),
	}
}

func (c *ServiceClient) getJsonHeader() map[string]string {
	return map[string]string{
		generic.HeaderKeyAccept:      generic.HeaderValueContentTypeJson,
		generic.HeaderKeyContentType: generic.HeaderValueContentTypeJson,
	}
}

func (c *ServiceClient) getFormHeader() map[string]string {
	return map[string]string{generic.HeaderKeyContentType: generic.HeaderValueContentTypeForm}
}

func (c *ServiceClient) AccessToken(response AccessTokenResponseInterface, redirectUrl, accessTokenUrl string) (*corev1.Secret, error) {
	var (
		secretCopy = c.Secret.DeepCopy()
		err        error
		payload    map[string]interface{}
		key        = fmt.Sprintf("%s/%s", c.Secret.Namespace, c.Secret.Name)
	)

	if c.OAuth2Info == nil {
		return nil, fmt.Errorf("secret is not base_auth, fobid to access token")
	}

	if c.OAuth2Info.HasAccessToken() {
		if c.OAuth2Info.IsExpired() {
			glog.V(5).Infof("<%s> access_token is expired, refresh it", key)
			payload = c.getRefreshTokenPayload()
			err = c.getAccessTokenResponse(response, accessTokenUrl, payload)
		} else {
			glog.V(5).Infof("<%s> access_token is not expired, continue to use it", key)
			return nil, nil
		}
	} else {
		glog.V(5).Infof("<%s> access_token is not exist, generate it", key)
		payload = c.getAccessTokenPayload(redirectUrl)
		err = c.getAccessTokenResponse(response, accessTokenUrl, payload)
	}

	if err != nil {
		return nil, err
	}

	if response != nil {
		c.OAuth2Info.SecretDataOAuth2Details = response.ConvertToSecretDataOAuth2Details()
		secretCopy.StringData = c.OAuth2Info.GetData()
	}

	return secretCopy, nil
}

func (c *ServiceClient) getHeader(lastModifyAt *metav1.Time) (header map[string]string) {
	header = make(map[string]string, 0)
	if lastModifyAt != nil {
		header[k8s.GithubIfModifiedSince] = lastModifyAt.String()
	}

	if c.IsBasicAuth() {
		header["Authorization"] = "Basic " + generic.BasicAuth(c.GetUsername(), c.GetPassword())
	} else {
		header["Authorization"] = "Bearer " + c.getAccessToken()
	}
	return
}

func (c *ServiceClient) getAccessTokenResponse(response AccessTokenResponseInterface, accessTokenUrl string, payload map[string]interface{}) (err error) {
	var (
		req    *http.Request
		header map[string]string
	)

	if c.getServiceType() == devops.CodeRepoServiceTypeBitbucket {
		header = c.getFormHeader()
		if payload != nil {
			values := url.Values{}
			for key, value := range payload {
				if value == nil {
					continue
				}
				values.Add(key, value.(string))
			}
			req, err = http.NewRequest("POST", accessTokenUrl, strings.NewReader(values.Encode()))
		}

	} else {
		header = c.getJsonHeader()
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(payload)
		req, err = http.NewRequest("POST", accessTokenUrl, b)
	}

	glog.V(5).Infof("accessTokenUrl: %s; header: %s; payload: %s; err: %s", accessTokenUrl, header, payload, err)
	if err != nil {
		return
	}

	if header != nil {
		for key, val := range header {
			req.Header.Add(key, val)
		}
	}

	resp, err := c.getHTTPClient().Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return
	}

	if resp.StatusCode >= 400 {
		err = fmt.Errorf("response status is %s when accessing token", resp.Status)
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, response)
	if err != nil {
		return
	}

	if response.GetAccessToken() == "" {
		err = fmt.Errorf("%s", body)
	}

	return
}
