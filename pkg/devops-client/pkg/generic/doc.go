// Package generic this package main function is to store
// all the common function and structs that can be used across clients
// and should not reference any other client package
package generic
