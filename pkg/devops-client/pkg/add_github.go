package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v3 "alauda.io/devops-apiserver/pkg/devops-client/pkg/github/v3"
)

func init() {
	register(devops.CodeRepoServiceTypeGithub.String(), versionOpt{
		version:   "v3",
		factory:   v3.NewClient(),
		isDefault: true,
	})
}
