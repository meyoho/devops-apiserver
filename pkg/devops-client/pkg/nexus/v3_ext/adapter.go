// Code generated
package v3_ext

import (
	"context"
	"net/http"

	"github.com/go-logr/logr"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/nexus/v3_ext/client"
	op "alauda.io/devops-apiserver/pkg/devops-client/pkg/nexus/v3_ext/client/operations"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/nexus/v3_ext/models"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/transport"

	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	clientv1.NotImplement
	logger     logr.Logger
	client     *client.Nexus
	opts       *clientv1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ clientv1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() clientv1.ClientFactory {
	return func(opts *clientv1.Options) clientv1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

// CreateTask will create a task in nexux
func (c *Client) CreateTask(ctx context.Context, name, repositoryName, cronExpression string) (err error) {
	task := &models.Task{
		Action: "coreui_Task",
		Method: "create",
		Type:   "rpc",
		Tid:    16,
		Data: []*models.TaskData{{
			TypeID:     "repository.maven.publish-dotindex",
			Enabled:    true,
			Name:       name,
			AlertEmail: "",
			Schedule:   "advanced",
			Properties: &models.TaskDataProperty{
				RepositoryName: repositoryName,
			},
			CronExpression: cronExpression,
		}},
	}

	params := op.NewExtDirectParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithBody(task)
	_, err = c.client.Operations.ExtDirect(params, c.authInfo)
	return
}
