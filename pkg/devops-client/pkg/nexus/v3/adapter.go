// Code generated
package v3

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"

	"io/ioutil"
	"strings"

	"github.com/go-logr/logr"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/generic"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/nexus/v3/client"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/nexus/v3/client/repositories"
	nexus_script "alauda.io/devops-apiserver/pkg/devops-client/pkg/nexus/v3/client/script"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/nexus/v3/models"
	"alauda.io/devops-apiserver/pkg/devops-client/pkg/transport"

	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	clientv1.NotImplement
	logger     logr.Logger
	client     *client.Nexus
	opts       *clientv1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ clientv1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() clientv1.ClientFactory {
	return func(opts *clientv1.Options) clientv1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

var ScriptMap = map[string](map[string]string){
	"create_Maven2": {"hosted": "alauda_devops_create_maven2_hosted"},
}

var ScriptMapAll = map[string]string{
	"map_roleuser":            "alauda_devops_map_roleuser",
	"get_repository":          "alauda_devops_get_repository",
	"list_blobstore":          "alauda_devops_list_blobstore",
	"add_userrole":            "alauda_devops_add_userrole",
	"remove_userrole":         "alauda_devops_remove_userrole",
	"add_userrolewithactions": "alauda_devops_add_userrole_actions",
}

var (
	nexusFilename              = "nexusclient"
	isFirstTimeInitNexusScript = true
)

// Init implements common inteface
func (c *Client) Init(ctx context.Context) []error {
	var (
		errs = []error{}
	)

	//Create script
	for scriptFormat := range ScriptMap {
		for scriptType := range ScriptMap[scriptFormat] {

			if isFirstTimeInitNexusScript {
				err := c.UpdateScript(ctx, ScriptMap[scriptFormat][scriptType])
				if err != nil {
					errs = append(errs, err)
				}
			}

			err := c.CreateScript(ctx, ScriptMap[scriptFormat][scriptType])
			if err != nil {
				errs = append(errs, err)
			}
		}
	}

	for _, script := range ScriptMapAll {

		if isFirstTimeInitNexusScript {
			err := c.UpdateScript(ctx, script)
			if err != nil {
				errs = append(errs, err)
			}
		}

		err := c.CreateScript(ctx, script)
		if err != nil {
			errs = append(errs, err)
		}
	}

	if isFirstTimeInitNexusScript {
		isFirstTimeInitNexusScript = false
	}

	return errs
}

func GetFileContentAsStringLines(filePath string) (string, error) {
	b, err := ioutil.ReadFile(filePath)
	if err != nil {
		return "", err
	}
	s := string(b)
	s = strings.Replace(s, "\"", "'", -1)

	return s, nil
}

// CreateScript creates a script in nexus by a build-in script file.
// See also CreateScriptByPath.
func (c *Client) CreateScript(ctx context.Context, scriptName string) (err error) {
	methodName := "CreateScript"
	c.logger.V(9).Info("create script", "nexus filename", nexusFilename, "method name", methodName, "script name", scriptName)

	var script string
	if script, err = GetFileContentAsStringLines("./groovy/nexus/" + scriptName + ".groovy"); err == nil {
		err = c.CreateScriptByPath(ctx, scriptName, "groovy", script)
	}
	return
}

// UpdateScript update a script in nexus by a build-in script file.
// See also UpdateScriptByPath
func (c *Client) UpdateScript(ctx context.Context, scriptName string) (err error) {
	methodName := "UpdateScript"

	c.logger.V(9).Info("update script", "nexus filename", nexusFilename, "method name", methodName, "script name", scriptName)

	var script string
	if script, err = GetFileContentAsStringLines("./groovy/nexus/" + scriptName + ".groovy"); err == nil {
		err = c.UpdateScriptByPath(ctx, scriptName, "groovy", script)
	}
	return
}

// ListRegistry implements common interface
func (c *Client) ListRegistry(ctx context.Context) (v1.ArtifactRegistryList, error) {
	//List registry
	result := v1.ArtifactRegistryList{}

	params := repositories.
		NewGetRepositoriesParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	reposOK, err := c.client.Repositories.GetRepositories(params, c.authInfo)
	if err != nil {
		return result, err
	}

	var arl []v1.ArtifactRegistry

	for i := range reposOK.Payload {
		ar := v1.ArtifactRegistry{
			ObjectMeta: metav1.ObjectMeta{
				Name: reposOK.Payload[i].Name,
			},
			Spec: v1.ArtifactRegistrySpec{
				Type:                 reposOK.Payload[i].Format,
				ArtifactRegistryName: reposOK.Payload[i].Name,
				ArtifactRegistryArgs: map[string]string{
					"type": reposOK.Payload[i].Type,
				},
				ToolSpec: v1.ToolSpec{
					HTTP: v1.HostPort{
						Host: reposOK.Payload[i].URL,
					},
				},
			},
		}
		arl = append(arl, ar)
	}

	result.Items = arl
	return result, err
}

func (c *Client) CreateRegistry(ctx context.Context, artifactType string, v map[string]string) error {
	artifactRegistryName := v["artifactRegistryName"]

	ar, err := c.GetRepository(ctx, artifactRegistryName)
	if err != nil {
		return err
	}

	c.logger.V(9).Info("get ar", "ar", ar)

	if ar.Name == "" {
		//Create registry
		body := map[string]string{
			"artifactRegistryName": v["artifactRegistryName"],
			"versionPolicy":        v["versionPolicy"],
			"blobStore":            v["blobStore"],
		}

		bodyJson, err := json.Marshal(body)
		if err != nil {
			return err
		}

		ntype := v["type"]
		scriptName := ScriptMap["create_"+artifactType][ntype]

		if scriptName == "" {
			return errors.New("Not found script named " + scriptName)
		}

		params := nexus_script.
			NewRun1Params().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithName(scriptName).
			WithBody(string(bodyJson))
		_, err = c.client.Script.Run1(params, c.authInfo)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *Client) GetRepository(ctx context.Context, artifactRepositoryName string) (registry v1.ArtifactRegistry, err error) {
	body := map[string]string{"name": artifactRepositoryName}

	bodyJson, err := json.Marshal(body)
	if err != nil {
		return registry, err
	}

	scriptName := ScriptMapAll["get_repository"]

	if scriptName == "" {
		return registry, errors.New("not found scriptName")
	}

	params := nexus_script.
		NewRun1Params().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithName(scriptName).
		WithBody(string(bodyJson))
	scriptOK, err := c.client.Script.Run1(params, c.authInfo)
	if err != nil {
		return v1.ArtifactRegistry{}, err
	}

	repository := models.RepositoryXO{}
	err = json.Unmarshal([]byte(scriptOK.Payload.Result), &repository)
	if err != nil {
		return v1.ArtifactRegistry{}, err
	}

	result := v1.ArtifactRegistry{
		ObjectMeta: metav1.ObjectMeta{
			Name: repository.Name,
		},
		Spec: v1.ArtifactRegistrySpec{
			Type:                 repository.Format,
			ArtifactRegistryName: repository.Name,
			ArtifactRegistryArgs: map[string]string{
				"type": repository.Type,
			},
		},
	}

	return result, err
}

func (c *Client) ListBlobStore(ctx context.Context) (result v1.BlobStoreOptionList, err error) {
	scriptName := ScriptMapAll["list_blobstore"]

	if scriptName == "" {
		return result, err
	}

	params := nexus_script.
		NewRun1Params().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithName(scriptName)
	scriptOK, err := c.client.Script.Run1(params, c.authInfo)
	if err != nil {
		return result, err
	}

	var blobStoreList []struct {
		Name string
		Type string
	}

	err = json.Unmarshal([]byte(scriptOK.Payload.Result), &blobStoreList)
	if err != nil {
		return result, err
	}

	var blobItems []v1.BlobStoreOption
	for _, blob := range blobStoreList {
		blobItems = append(blobItems, v1.BlobStoreOption{
			Type: blob.Type,
			Name: blob.Name,
		})
	}

	result.Items = blobItems
	return result, err
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(c.httpClient, url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(ctx context.Context) (*v1.HostPortStatus, error) {
	params := repositories.
		NewGetRepositoriesParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	_, err := c.client.Repositories.GetRepositories(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return &v1.HostPortStatus{StatusCode: 200}, nil
}

// CreateScriptByPath creates a script in nexus by script's name and kind and content
func (c *Client) CreateScriptByPath(ctx context.Context, name, kind, script string) (err error) {
	body := &models.ScriptXO{
		Name:    name,
		Type:    kind,
		Content: script,
	}

	params := nexus_script.
		NewAddParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithBody(body)
	_, err = c.client.Script.Add(params, c.authInfo)
	return
}

// UpdateScriptByPath update a script in nexus by script's name and kind and content
func (c *Client) UpdateScriptByPath(ctx context.Context, name, kind, script string) (err error) {
	body := &models.ScriptXO{
		Name:    name,
		Type:    kind,
		Content: script,
	}

	params := nexus_script.
		NewEditParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithName(name).
		WithBody(body)
	_, err = c.client.Script.Edit(params, c.authInfo)
	return
}

// ExecuteScript executes the script inside  the Nexus server
func (c *Client) ExecuteScript(ctx context.Context, name, jsonParam string) (err error) {
	params := nexus_script.
		NewRun1Params().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithName(name).
		WithBody(jsonParam)
	_, err = c.client.Script.Run1(params, c.authInfo)
	return
}

// DeleteScript delete a script by name
func (c *Client) DeleteScript(ctx context.Context, name string) (err error) {
	params := nexus_script.
		NewDeleteParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithName(name)
	_, err = c.client.Script.Delete(params, c.authInfo)
	return
}

// ListScripts list all scripts
func (c *Client) ListScripts(ctx context.Context) (scripts []clientv1.NexusScript, err error) {
	params := nexus_script.
		NewBrowseParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	var response *nexus_script.BrowseOK
	if response, err = c.client.Script.Browse(params, c.authInfo); err == nil && len(response.Payload) > 0 {
		scripts = make([]clientv1.NexusScript, len(response.Payload))
		for i, item := range response.Payload {
			scripts[i] = convertToNexusScript(item)
		}
	}
	return
}

// GetScript get a script by name
func (c *Client) GetScript(ctx context.Context, name string) (script clientv1.NexusScript, err error) {
	params := nexus_script.
		NewReadParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithName(name)
	var response *nexus_script.ReadOK
	if response, err = c.client.Script.Read(params, c.authInfo); err == nil {
		script = convertToNexusScript(response.Payload)
	}
	return
}

func convertToNexusScript(model *models.ScriptXO) (script clientv1.NexusScript) {
	script = clientv1.NexusScript{
		Name:    model.Name,
		Type:    model.Type,
		Content: model.Content,
	}
	return
}
