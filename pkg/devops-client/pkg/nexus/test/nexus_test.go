package test

import (
	"context"
	"fmt"

	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	nexusv3 "alauda.io/devops-apiserver/pkg/devops-client/pkg/nexus/v3"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool Nexus tests", func() {
	var (
		host    = "http://localhost"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options

		transport  = gock.NewTransport()
		httpClient = clientv1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = nexusv3.NewClient()
		opts = clientv1.NewOptions(clientv1.NewBasicAuth("admin", "admin"), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("script tests", func() {
		var (
			scriptName    = "fake-name"
			scriptContent = "fake-content"
			scriptKind    = "fake-content"

			payload = `{"content":"fake-content","name":"fake-name","type":"fake-content"}`
		)

		Context("CreateScriptByPath", func() {
			BeforeEach(func() {
				// as a debug way
				//gock.Observe(gock.DumpRequest)
				gock.New(host).
					Post("/service/rest/v1/script").
					BodyString(payload).
					MatchHeaders(map[string]string{
						"Accept":       "application/json",
						"Content-Type": "application/json",
					}).
					Reply(204).
					BodyString(payload)
			})

			It("normal case, should success", func() {
				err := client.CreateScriptByPath(context.Background(), scriptName, scriptKind, scriptContent)
				Expect(err).To(BeNil())
			})
		})

		Context("UpdateScriptByPath", func() {
			BeforeEach(func() {
				gock.New(host).
					Put(fmt.Sprintf("/service/rest/v1/script/%s", scriptName)).
					BodyString(payload).
					MatchHeaders(map[string]string{
						"Accept":       "application/json",
						"Content-Type": "application/json",
					}).
					Reply(204).
					BodyString(payload)
			})

			It("normal case, should success", func() {
				err := client.UpdateScriptByPath(context.Background(), scriptName, scriptKind, scriptContent)
				Expect(err).To(BeNil())
			})
		})

		Context("ExecuteScript", func() {
			var (
				jsonParam = ""
			)
			BeforeEach(func() {
				//gock.Observe(gock.DumpRequest)

				gock.New(host).
					Post(fmt.Sprintf("/service/rest/v1/script/%s/run", scriptName)).
					BodyString(jsonParam).
					MatchHeaders(map[string]string{
						"Content-Type": "text/plain",
					}).
					Reply(200)
			})

			It("normal case, should success", func() {
				err := client.ExecuteScript(context.Background(), scriptName, jsonParam)
				Expect(err).To(BeNil())
			})
		})

		Context("DeleteScript", func() {
			BeforeEach(func() {
				//gock.Observe(gock.DumpRequest)

				gock.New(host).
					Delete(fmt.Sprintf("/service/rest/v1/script/%s", scriptName)).
					Reply(204)
			})

			It("normal case, should success", func() {
				err := client.DeleteScript(context.Background(), scriptName)
				Expect(err).To(BeNil())
			})
		})

		Context("ListScripts", func() {
			BeforeEach(func() {
				gock.New(host).
					Get("/service/rest/v1/script").
					Reply(200).
					BodyString("[" + payload + "]")
			})

			It("normal case, should success", func() {
				scripts, err := client.ListScripts(context.Background())
				Expect(err).To(BeNil())
				Expect(scripts).NotTo(BeNil())
				Expect(len(scripts)).To(Equal(1))
				Expect(scripts[0].Name).To(Equal(scriptName))
				Expect(scripts[0].Content).To(Equal(scriptContent))
				Expect(scripts[0].Type).To(Equal(scriptKind))
			})
		})

		Context("GetScript", func() {
			BeforeEach(func() {
				//gock.Observe(gock.DumpRequest)
				gock.New(host).
					Get(fmt.Sprintf("/service/rest/v1/script/%s", scriptName)).
					Reply(200).BodyString(payload)
			})

			It("normal case, should success", func() {
				nexusScript, err := client.GetScript(context.Background(), scriptName)
				Expect(err).To(BeNil())
				Expect(nexusScript.Name).To(Equal(scriptName))
				Expect(nexusScript.Content).To(Equal(scriptContent))
				Expect(nexusScript.Type).To(Equal(scriptKind))
			})
		})
	})
})

// getScript only test purpose
func getScript(name, content, kind string) string {
	return fmt.Sprintf(`[{
	"name": "%s",
	"content": "%s",
	"type": "%s"
  }]`, name, content, kind)
}
