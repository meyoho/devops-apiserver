package pkg

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	v4 "alauda.io/devops-apiserver/pkg/devops-client/pkg/gitlab/v4"
)

func init() {
	register(devops.CodeRepoServiceTypeGitlab.String(), versionOpt{
		version:   "v4",
		factory:   v4.NewClient(),
		isDefault: true,
	})
}
