// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "alauda.io/devops-apiserver/pkg/devops-client/pkg/jira/v7/models"
)

// NewPostSearchParams creates a new PostSearchParams object
// with the default values initialized.
func NewPostSearchParams() *PostSearchParams {
	var ()
	return &PostSearchParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostSearchParamsWithTimeout creates a new PostSearchParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostSearchParamsWithTimeout(timeout time.Duration) *PostSearchParams {
	var ()
	return &PostSearchParams{

		timeout: timeout,
	}
}

// NewPostSearchParamsWithContext creates a new PostSearchParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostSearchParamsWithContext(ctx context.Context) *PostSearchParams {
	var ()
	return &PostSearchParams{

		Context: ctx,
	}
}

// NewPostSearchParamsWithHTTPClient creates a new PostSearchParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostSearchParamsWithHTTPClient(client *http.Client) *PostSearchParams {
	var ()
	return &PostSearchParams{
		HTTPClient: client,
	}
}

/*PostSearchParams contains all the parameters to send to the API endpoint
for the post search operation typically these are written to a http.Request
*/
type PostSearchParams struct {

	/*Issuesearch*/
	Issuesearch *models.IssueSearch

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post search params
func (o *PostSearchParams) WithTimeout(timeout time.Duration) *PostSearchParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post search params
func (o *PostSearchParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post search params
func (o *PostSearchParams) WithContext(ctx context.Context) *PostSearchParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post search params
func (o *PostSearchParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post search params
func (o *PostSearchParams) WithHTTPClient(client *http.Client) *PostSearchParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post search params
func (o *PostSearchParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithIssuesearch adds the issuesearch to the post search params
func (o *PostSearchParams) WithIssuesearch(issuesearch *models.IssueSearch) *PostSearchParams {
	o.SetIssuesearch(issuesearch)
	return o
}

// SetIssuesearch adds the issuesearch to the post search params
func (o *PostSearchParams) SetIssuesearch(issuesearch *models.IssueSearch) {
	o.Issuesearch = issuesearch
}

// WriteToRequest writes these params to a swagger request
func (o *PostSearchParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Issuesearch != nil {
		if err := r.SetBodyParam(o.Issuesearch); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
