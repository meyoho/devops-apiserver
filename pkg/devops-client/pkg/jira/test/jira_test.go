package test

import (
	"context"

	v1 "alauda.io/devops-apiserver/pkg/apis/devops"
	clientv1 "alauda.io/devops-apiserver/pkg/devops-client/pkg/api/v1"
	v7 "alauda.io/devops-apiserver/pkg/devops-client/pkg/jira/v7"

	"github.com/go-openapi/errors"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool Jira tests", func() {
	var (
		// host
		host    = "http://jira.test"
		factory clientv1.ClientFactory
		client  clientv1.Interface
		opts    *clientv1.Options

		transport  = gock.NewTransport()
		httpClient = clientv1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = v7.NewClient()
		opts = clientv1.NewOptions(clientv1.NewBasicConfig("jira.test", "", []string{}), clientv1.NewClient(httpClient), clientv1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		gock.OffAll()
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetProjects", func() {
		const (
			projects = `[
					{
						"expand": "description,lead,url,projectKeys",
						"self": "http://127.0.0.1:30000/rest/api/2/project/10001",
						"id": "10001",
						"key": "PROJECT16",
						"description": "",
						"lead": {
							"self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
							"key": "JIRAUSER10000",
							"name": "admin",
							"avatarUrls": {
								"48x48": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=48",
								"24x24": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=24",
								"16x16": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=16",
								"32x32": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=32"
							},
							"displayName": "yuzp1996@qq.com",
							"active": true
						},
						"name": "PROJECT16",
						"avatarUrls": {
							"48x48": "http://127.0.0.1:30000/secure/projectavatar?avatarId=10324",
							"24x24": "http://127.0.0.1:30000/secure/projectavatar?size=small&avatarId=10324",
							"16x16": "http://127.0.0.1:30000/secure/projectavatar?size=xsmall&avatarId=10324",
							"32x32": "http://127.0.0.1:30000/secure/projectavatar?size=medium&avatarId=10324"
						},
						"projectTypeKey": "business"
					},
					{
						"expand": "description,lead,url,projectKeys",
						"self": "http://127.0.0.1:30000/rest/api/2/project/10000",
						"id": "10000",
						"key": "TEST",
						"description": "",
						"lead": {
							"self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
							"key": "JIRAUSER10000",
							"name": "admin",
							"avatarUrls": {
								"48x48": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=48",
								"24x24": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=24",
								"16x16": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=16",
								"32x32": "https://www.gravatar.com/avatar/d86278dbb0274afdfa2b81db1e770e5c?d=mm&s=32"
							},
							"displayName": "yuzp1996@qq.com",
							"active": true
						},
						"name": "TEST",
						"avatarUrls": {
							"48x48": "http://127.0.0.1:30000/secure/projectavatar?avatarId=10324",
							"24x24": "http://127.0.0.1:30000/secure/projectavatar?size=small&avatarId=10324",
							"16x16": "http://127.0.0.1:30000/secure/projectavatar?size=xsmall&avatarId=10324",
							"32x32": "http://127.0.0.1:30000/secure/projectavatar?size=medium&avatarId=10324"
						},
						"projectTypeKey": "business"
					}
						]`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/project").
				Reply(200).
				JSON(projects)
		})

		It("get projects", func() {
			projects, err := client.GetProjects(context.Background(), "1", "2")
			Expect(err).To(BeNil())
			Expect(len(projects.Items)).To(Equal(2))
			Expect(projects.Items[0].Name).To(Equal("PROJECT16"))
			Expect(projects.Items[0].Annotations["lead"]).To(Equal("admin"))
			Expect(projects.Items[0].Annotations["key"]).To(Equal("PROJECT16"))
		})
	})

	Context("CreateProject", func() {
		const (
			project = `{
				"self": "http://150.109.47.24:8080/rest/api/2/project/10001",
				"id": 10001,
				"key": "KKK"
			}`
		)
		BeforeEach(func() {
			gock.New(host).
				Post("/project").
				JSON(`{
					"name": "project2",
					"description": "test project",
					"lead": "admin",
					"key": "KKK",
					"projectTypeKey": "business"
				}`).
				Reply(201).
				JSON(project)
		})

		It("create a project", func() {
			project, err := client.CreateProject(context.Background(), "project2", "test project", "admin", "KKK")
			Expect(err).To(BeNil())
			Expect(project.Name).To(Equal("project2"))
		})
	})

	Context("GetIssueStatusOptions", func() {

		const status = `
			[
				{
					"self": "http://10.0.128.67:32002/rest/api/2/status/3",
					"description": "此问题正在被经办人积极处理。",
					"iconUrl": "http://10.0.128.67:32002/images/icons/statuses/inprogress.png",
					"name": "处理中",
					"id": "3",
					"statusCategory": {
						"self": "http://10.0.128.67:32002/rest/api/2/statuscategory/4",
						"id": 4,
						"key": "indeterminate",
						"colorName": "yellow",
						"name": "处理中"
					}
				},
				{
					"self": "http://10.0.128.67:32002/rest/api/2/status/10000",
					"description": "",
					"iconUrl": "http://10.0.128.67:32002/images/icons/status_generic.gif",
					"name": "待办",
					"id": "10000",
					"statusCategory": {
						"self": "http://10.0.128.67:32002/rest/api/2/statuscategory/2",
						"id": 2,
						"key": "new",
						"colorName": "blue-gray",
						"name": "待办"
					}
				},
				{
					"self": "http://10.0.128.67:32002/rest/api/2/status/10001",
					"description": "",
					"iconUrl": "http://10.0.128.67:32002/images/icons/status_generic.gif",
					"name": "完成",
					"id": "10001",
					"statusCategory": {
						"self": "http://10.0.128.67:32002/rest/api/2/statuscategory/3",
						"id": 3,
						"key": "done",
						"colorName": "green",
						"name": "完成"
					}
				}
			]`
		const priority = `
			[
				{
					"self": "http://127.0.0.1:30000/rest/api/2/priority/1",
					"statusColor": "#ff7452",
					"description": "This problem will block progress.",
					"iconUrl": "http://127.0.0.1:30000/images/icons/priorities/highest.svg",
					"name": "Highest",
					"id": "1"
				},
				{
					"self": "http://127.0.0.1:30000/rest/api/2/priority/2",
					"statusColor": "#ff8f73",
					"description": "Serious problem that could block progress.",
					"iconUrl": "http://127.0.0.1:30000/images/icons/priorities/high.svg",
					"name": "High",
					"id": "2"
				},
				{
					"self": "http://127.0.0.1:30000/rest/api/2/priority/3",
					"statusColor": "#ffab00",
					"description": "Has the potential to affect progress.",
					"iconUrl": "http://127.0.0.1:30000/images/icons/priorities/medium.svg",
					"name": "Medium",
					"id": "3"
				},
				{
					"self": "http://127.0.0.1:30000/rest/api/2/priority/4",
					"statusColor": "#0065ff",
					"description": "Minor problem or easily worked around.",
					"iconUrl": "http://127.0.0.1:30000/images/icons/priorities/low.svg",
					"name": "Low",
					"id": "4"
				},
				{
					"self": "http://127.0.0.1:30000/rest/api/2/priority/5",
					"statusColor": "#2684ff",
					"description": "Trivial problem with little or no impact on progress.",
					"iconUrl": "http://127.0.0.1:30000/images/icons/priorities/lowest.svg",
					"name": "Lowest",
						"id": "5"
					}
				]
`
		const issuetype = `
			[
				{
					"self": "http://127.0.0.1:30000/rest/api/2/issuetype/10003",
					"id": "10003",
					"description": "需要完成的任务。",
					"iconUrl": "http://127.0.0.1:30000/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype",
					"name": "任务",
					"subtask": false,
					"avatarId": 10318
				},
				{
					"self": "http://127.0.0.1:30000/rest/api/2/issuetype/10000",
					"id": "10000",
					"description": "问题的子任务",
					"iconUrl": "http://127.0.0.1:30000/images/icons/issuetypes/subtask_alternate.png",
					"name": "子任务",
					"subtask": true
				},
				{
					"self": "http://127.0.0.1:30000/rest/api/2/issuetype/10001",
					"id": "10001",
					"description": "由 Jira Software 创建——请勿编辑或删除。适用于大型用户故事的事务类型， 需加以细分。",
					"iconUrl": "http://127.0.0.1:30000/images/icons/issuetypes/epic.svg",
					"name": "Epic",
					"subtask": false
				},
				{
					"self": "http://127.0.0.1:30000/rest/api/2/issuetype/10002",
					"id": "10002",
					"description": "",
					"iconUrl": "http://127.0.0.1:30000/images/icons/issuetypes/story.svg",
					"name": "故事",
					"subtask": false
				}
			]`

		BeforeEach(func() {
			gock.New(host).
				Get("/status").
				Reply(200).
				JSON(status)
			gock.New(host).
				Get("/priority").
				Reply(200).
				JSON(priority)
			gock.New(host).
				Get("/issuetype").
				Reply(200).
				JSON(issuetype)
		})
		It("get jira issue all option", func() {

			issuestatus, err := client.GetIssueOptions(context.Background(), "all")
			Expect(issuestatus.Priority[0].Name).To(Equal("Highest"))
			Expect(issuestatus.IssueType[0].Name).To(Equal("任务"))
			Expect(issuestatus.Status[0].Name).To(Equal("处理中"))

			Expect(len(issuestatus.Status)).To(Equal(3))
			Expect(len(issuestatus.Priority)).To(Equal(5))
			Expect(len(issuestatus.IssueType)).To(Equal(4))

			Expect(err).To(BeNil())
		})

		It("get jira issue status option", func() {

			issuestatus, err := client.GetIssueOptions(context.Background(), "status")
			Expect(len(issuestatus.Status)).To(Equal(3))
			Expect(len(issuestatus.Priority)).To(Equal(0))
			Expect(len(issuestatus.IssueType)).To(Equal(0))

			Expect(issuestatus.Status[0].Name).To(Equal("处理中"))

			Expect(err).To(BeNil())
		})

		It("get jira issue priority option", func() {

			issuestatus, err := client.GetIssueOptions(context.Background(), "priority")
			Expect(len(issuestatus.Status)).To(Equal(0))
			Expect(len(issuestatus.Priority)).To(Equal(5))
			Expect(len(issuestatus.IssueType)).To(Equal(0))

			Expect(issuestatus.Priority[0].Name).To(Equal("Highest"))

			Expect(err).To(BeNil())
			Expect(err).To(BeNil())
		})

	})

	Context("get issue detail", func() {

		const IssueDetail = `
						{
							"expand": "renderedFields,names,schema,operations,editmeta,changelog,versionedRepresentations",
							"id": "10002",
							"self": "http://127.0.0.1:30000/rest/api/2/issue/10002",
							"key": "TEST-3",
							"fields": {
								"summary": "summary",
								"issuetype": {
									"self": "http://127.0.0.1:30000/rest/api/2/issuetype/10003",
									"id": "10003",
									"description": "需要完成的任务。",
									"iconUrl": "http://127.0.0.1:30000/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype",
									"name": "任务",
									"subtask": false,
									"avatarId": 10318
								},
								"creator": {
									"self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
									"name": "admin",
									"key": "JIRAUSER10000",
									"emailAddress": "yuzp1996@qq.com",
									"avatarUrls": {
										"48x48": "http://127.0.0.1:30000/secure/useravatar?avatarId=10334",
										"24x24": "http://127.0.0.1:30000/secure/useravatar?size=small&avatarId=10334",
										"16x16": "http://127.0.0.1:30000/secure/useravatar?size=xsmall&avatarId=10334",
										"32x32": "http://127.0.0.1:30000/secure/useravatar?size=medium&avatarId=10334"
									},
									"displayName": "yuzp1996@qq.com",
									"active": true,
									"timeZone": "GMT"
								},
								"created": "2019-10-22T05:28:04.017+0000",
								"description": null,
								"project": {
									"self": "http://127.0.0.1:30000/rest/api/2/project/10000",
									"id": "10000",
									"key": "TEST",
									"name": "TEST",
									"projectTypeKey": "business",
									"avatarUrls": {
										"48x48": "http://127.0.0.1:30000/secure/projectavatar?avatarId=10324",
										"24x24": "http://127.0.0.1:30000/secure/projectavatar?size=small&avatarId=10324",
										"16x16": "http://127.0.0.1:30000/secure/projectavatar?size=xsmall&avatarId=10324",
										"32x32": "http://127.0.0.1:30000/secure/projectavatar?size=medium&avatarId=10324"
									}
								},
								"priority": {
									"self": "http://127.0.0.1:30000/rest/api/2/priority/3",
									"iconUrl": "http://127.0.0.1:30000/images/icons/priorities/medium.svg",
									"name": "Medium",
									"id": "3"
								},
								"comment": {
									"comments": [
										{
											"self": "http://127.0.0.1:30000/rest/api/2/issue/10002/comment/10002",
											"id": "10002",
											"author": {
												"self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
												"name": "admin",
												"key": "JIRAUSER10000",
												"emailAddress": "yuzp1996@qq.com",
												"avatarUrls": {
													"48x48": "http://127.0.0.1:30000/secure/useravatar?avatarId=10334",
													"24x24": "http://127.0.0.1:30000/secure/useravatar?size=small&avatarId=10334",
													"16x16": "http://127.0.0.1:30000/secure/useravatar?size=xsmall&avatarId=10334",
													"32x32": "http://127.0.0.1:30000/secure/useravatar?size=medium&avatarId=10334"
												},
												"displayName": "yuzp1996@qq.com",
												"active": true,
												"timeZone": "GMT"
											},
											"body": "comment\r\n\r\n \r\n\r\n ",
											"updateAuthor": {
												"self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
												"name": "admin",
												"key": "JIRAUSER10000",
												"emailAddress": "yuzp1996@qq.com",
												"avatarUrls": {
													"48x48": "http://127.0.0.1:30000/secure/useravatar?avatarId=10334",
													"24x24": "http://127.0.0.1:30000/secure/useravatar?size=small&avatarId=10334",
													"16x16": "http://127.0.0.1:30000/secure/useravatar?size=xsmall&avatarId=10334",
													"32x32": "http://127.0.0.1:30000/secure/useravatar?size=medium&avatarId=10334"
												},
												"displayName": "yuzp1996@qq.com",
												"active": true,
												"timeZone": "GMT"
											},
											"created": "2019-10-22T06:55:48.221+0000",
											"updated": "2019-10-22T06:56:11.284+0000"
										}
									],
									"maxResults": 1,
									"total": 1,
									"startAt": 0
								},
								"issuelinks": [
									{
										"id": "10000",
										"self": "http://127.0.0.1:30000/rest/api/2/issueLink/10000",
										"type": {
											"id": "10000",
											"name": "Blocks",
											"inward": "is blocked by",
											"outward": "blocks",
											"self": "http://127.0.0.1:30000/rest/api/2/issueLinkType/10000"
										},
										"outwardIssue": {
											"id": "10000",
											"key": "TEST-1",
											"self": "http://127.0.0.1:30000/rest/api/2/issue/10000",
											"fields": {
												"summary": "dfddfdfd",
												"status": {
													"self": "http://127.0.0.1:30000/rest/api/2/status/3",
													"description": "此问题正在被经办人积极处理。",
													"iconUrl": "http://127.0.0.1:30000/images/icons/statuses/inprogress.png",
													"name": "处理中",
													"id": "3",
													"statusCategory": {
														"self": "http://127.0.0.1:30000/rest/api/2/statuscategory/4",
														"id": 4,
														"key": "indeterminate",
														"colorName": "yellow",
														"name": "处理中"
													}
												},
												"priority": {
													"self": "http://127.0.0.1:30000/rest/api/2/priority/3",
													"iconUrl": "http://127.0.0.1:30000/images/icons/priorities/medium.svg",
													"name": "Medium",
													"id": "3"
												},
												"issuetype": {
													"self": "http://127.0.0.1:30000/rest/api/2/issuetype/10003",
													"id": "10003",
													"description": "需要完成的任务。",
													"iconUrl": "http://127.0.0.1:30000/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype",
													"name": "任务",
													"subtask": false,
													"avatarId": 10318
												}
											}
										}
									},
									{
										"id": "10002",
										"self": "http://127.0.0.1:30000/rest/api/2/issueLink/10002",
										"type": {
											"id": "10000",
											"name": "Blocks",
											"inward": "is blocked by",
											"outward": "blocks",
											"self": "http://127.0.0.1:30000/rest/api/2/issueLinkType/10000"
										},
										"outwardIssue": {
											"id": "10001",
											"key": "TEST-2",
											"self": "http://127.0.0.1:30000/rest/api/2/issue/10001",
											"fields": {
												"summary": "dfdsfsdfsdf",
												"status": {
													"self": "http://127.0.0.1:30000/rest/api/2/status/10000",
													"description": "",
													"iconUrl": "http://127.0.0.1:30000/images/icons/status_generic.gif",
													"name": "待办",
													"id": "10000",
													"statusCategory": {
														"self": "http://127.0.0.1:30000/rest/api/2/statuscategory/2",
														"id": 2,
														"key": "new",
														"colorName": "blue-gray",
														"name": "待办"
													}
												},
												"priority": {
													"self": "http://127.0.0.1:30000/rest/api/2/priority/3",
													"iconUrl": "http://127.0.0.1:30000/images/icons/priorities/medium.svg",
													"name": "Medium",
													"id": "3"
												},
												"issuetype": {
													"self": "http://127.0.0.1:30000/rest/api/2/issuetype/10003",
													"id": "10003",
													"description": "需要完成的任务。",
													"iconUrl": "http://127.0.0.1:30000/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype",
													"name": "任务",
													"subtask": false,
													"avatarId": 10318
												}
											}
										}
									},
									{
										"id": "10001",
										"self": "http://127.0.0.1:30000/rest/api/2/issueLink/10001",
										"type": {
											"id": "10000",
											"name": "Blocks",
											"inward": "is blocked by",
											"outward": "blocks",
											"self": "http://127.0.0.1:30000/rest/api/2/issueLinkType/10000"
										},
										"inwardIssue": {
											"id": "10003",
											"key": "TES-1",
											"self": "http://127.0.0.1:30000/rest/api/2/issue/10003",
											"fields": {
												"summary": "This is your first task",
												"status": {
													"self": "http://127.0.0.1:30000/rest/api/2/status/10000",
													"description": "",
													"iconUrl": "http://127.0.0.1:30000/images/icons/status_generic.gif",
													"name": "待办",
													"id": "10000",
													"statusCategory": {
														"self": "http://127.0.0.1:30000/rest/api/2/statuscategory/2",
														"id": 2,
														"key": "new",
														"colorName": "blue-gray",
														"name": "待办"
													}
												},
												"priority": {
													"self": "http://127.0.0.1:30000/rest/api/2/priority/3",
													"iconUrl": "http://127.0.0.1:30000/images/icons/priorities/medium.svg",
													"name": "Medium",
													"id": "3"
												},
												"issuetype": {
													"self": "http://127.0.0.1:30000/rest/api/2/issuetype/10003",
													"id": "10003",
													"description": "需要完成的任务。",
													"iconUrl": "http://127.0.0.1:30000/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype",
													"name": "任务",
													"subtask": false,
													"avatarId": 10318
												}
											}
										}
									}
								],
								"assignee": {
									"self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
									"name": "admin",
									"key": "JIRAUSER10000",
									"emailAddress": "yuzp1996@qq.com",
									"avatarUrls": {
										"48x48": "http://127.0.0.1:30000/secure/useravatar?avatarId=10334",
										"24x24": "http://127.0.0.1:30000/secure/useravatar?size=small&avatarId=10334",
										"16x16": "http://127.0.0.1:30000/secure/useravatar?size=xsmall&avatarId=10334",
										"32x32": "http://127.0.0.1:30000/secure/useravatar?size=medium&avatarId=10334"
									},
									"displayName": "yuzp1996@qq.com",
									"active": true,
									"timeZone": "GMT"
								},
								"updated": "2019-10-22T06:56:11.484+0000",
								"status": {
									"self": "http://127.0.0.1:30000/rest/api/2/status/10000",
									"description": "",
									"iconUrl": "http://127.0.0.1:30000/images/icons/status_generic.gif",
									"name": "待办",
									"id": "10000",
									"statusCategory": {
										"self": "http://127.0.0.1:30000/rest/api/2/statuscategory/2",
										"id": 2,
										"key": "new",
										"colorName": "blue-gray",
										"name": "待办"
									}
								}
							}
						}		
`

		BeforeEach(func() {
			gock.New(host).
				Get("/issue").
				Reply(200).
				JSON(IssueDetail)

		})
		It("get the right issue detail", func() {
			issuedetail, err := client.GetIssueDetail(context.Background(), "TEST-3")
			Expect(issuedetail.Summary).To(Equal("summary"))
			Expect(issuedetail.Key).To(Equal("TEST-3"))
			Expect(len(issuedetail.IssueLinks)).To(Equal(3))
			Expect(issuedetail.Assignee.Username).To(Equal("admin"))
			Expect(issuedetail.Comments[0].Author).To(Equal("admin"))
			Expect(err).To(BeNil())

		})
	})

	Context("test get issue list", func() {

		const Issuelist = `{
				"expand": "schema,names",
				"startAt": 0,
				"maxResults": 2,
				"total": 9,
				"issues": [
					{
						"expand": "operations,versionedRepresentations,editmeta,changelog,renderedFields",
						"id": "10004",
						"self": "http://127.0.0.1:30000/rest/api/2/issue/10004",
						"key": "TES-2",
						"fields": {
							"summary": "Workflows and statuses",
							"issuetype": {
								"self": "http://127.0.0.1:30000/rest/api/2/issuetype/10003",
								"id": "10003",
								"description": "需要完成的任务。",
								"iconUrl": "http://127.0.0.1:30000/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype",
								"name": "任务",
								"subtask": false,
								"avatarId": 10318
							},
							"creator": {
								"self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
								"name": "admin",
								"key": "JIRAUSER10000",
								"emailAddress": "yuzp1996@qq.com",
								"avatarUrls": {
									"48x48": "http://127.0.0.1:30000/secure/useravatar?avatarId=10334",
									"24x24": "http://127.0.0.1:30000/secure/useravatar?size=small&avatarId=10334",
									"16x16": "http://127.0.0.1:30000/secure/useravatar?size=xsmall&avatarId=10334",
									"32x32": "http://127.0.0.1:30000/secure/useravatar?size=medium&avatarId=10334"
								},
								"displayName": "yuzp1996@qq.com",
								"active": true,
								"timeZone": "GMT"
							},
							"created": "2019-10-22T05:51:36.000+0000",
							"description": "!Workflow.png|align=left!\nh4. Workflows - What you need to know\n{color:#707070}Workflows define steps to get a task to done. To see the workflow this tasks goes through, click on 'View Workflow' above. {color}\nh4. Status\n{color:#707070} A status represents the “state” of a task at a specific point in your workflow. The current status of your task can be viewed in the 'Details' section above. Once you're ready to move to the next step, click on the appropriate transition button. {color}\n\nNext: [Editing tasks|TES-3]\n{color:#707070}Previous:{color} [This is your first task.|TES-1]\n\n----\n[Learn more about workflows|http://blogs.atlassian.com/2015/11/how-to-set-up-business-workflows-in-jira-core/]",
							"project": {
								"self": "http://127.0.0.1:30000/rest/api/2/project/10001",
								"id": "10001",
								"key": "TES",
								"name": "TESTdddd",
								"projectTypeKey": "business",
								"avatarUrls": {
									"48x48": "http://127.0.0.1:30000/secure/projectavatar?avatarId=10324",
									"24x24": "http://127.0.0.1:30000/secure/projectavatar?size=small&avatarId=10324",
									"16x16": "http://127.0.0.1:30000/secure/projectavatar?size=xsmall&avatarId=10324",
									"32x32": "http://127.0.0.1:30000/secure/projectavatar?size=medium&avatarId=10324"
								}
							},
							"assignee": {
								"self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
								"name": "admin",
								"key": "JIRAUSER10000",
								"emailAddress": "yuzp1996@qq.com",
								"avatarUrls": {
									"48x48": "http://127.0.0.1:30000/secure/useravatar?avatarId=10334",
									"24x24": "http://127.0.0.1:30000/secure/useravatar?size=small&avatarId=10334",
									"16x16": "http://127.0.0.1:30000/secure/useravatar?size=xsmall&avatarId=10334",
									"32x32": "http://127.0.0.1:30000/secure/useravatar?size=medium&avatarId=10334"
								},
								"displayName": "yuzp1996@qq.com",
								"active": true,
								"timeZone": "GMT"
							},
							"priority": {
								"self": "http://127.0.0.1:30000/rest/api/2/priority/3",
								"iconUrl": "http://127.0.0.1:30000/images/icons/priorities/medium.svg",
								"name": "Medium",
								"id": "3"
							},
							"updated": "2019-10-22T05:49:35.000+0000",
							"status": {
								"self": "http://127.0.0.1:30000/rest/api/2/status/10000",
								"description": "",
								"iconUrl": "http://127.0.0.1:30000/images/icons/status_generic.gif",
								"name": "待办",
								"id": "10000",
								"statusCategory": {
									"self": "http://127.0.0.1:30000/rest/api/2/statuscategory/2",
									"id": 2,
									"key": "new",
									"colorName": "blue-gray",
									"name": "待办"
								}
							}
						}
					},
					{
						"expand": "operations,versionedRepresentations,editmeta,changelog,renderedFields",
						"id": "10005",
						"self": "http://127.0.0.1:30000/rest/api/2/issue/10005",
						"key": "TES-3",
						"fields": {
							"summary": "Editing tasks",
							"issuetype": {
								"self": "http://127.0.0.1:30000/rest/api/2/issuetype/10003",
								"id": "10003",
								"description": "需要完成的任务。",
								"iconUrl": "http://127.0.0.1:30000/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype",
								"name": "任务",
								"subtask": false,
								"avatarId": 10318
							},
							"creator": {
								"self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
								"name": "admin",
								"key": "JIRAUSER10000",
								"emailAddress": "yuzp1996@qq.com",
								"avatarUrls": {
									"48x48": "http://127.0.0.1:30000/secure/useravatar?avatarId=10334",
									"24x24": "http://127.0.0.1:30000/secure/useravatar?size=small&avatarId=10334",
									"16x16": "http://127.0.0.1:30000/secure/useravatar?size=xsmall&avatarId=10334",
									"32x32": "http://127.0.0.1:30000/secure/useravatar?size=medium&avatarId=10334"
								},
								"displayName": "yuzp1996@qq.com",
								"active": true,
								"timeZone": "GMT"
							},
							"created": "2019-10-22T05:51:36.000+0000",
							"description": "h4. Editing tasks\n{color:#707070}Hover over the content you want to edit and make the change. Click the checkmark and you're done! You can also edit using keyboard shortcuts or by clicking the 'Edit' button. And don't forget to assign the task to someone. {color}\nh4. Commenting\n{color:#707070}You can add comments to a task below. Comments are a great way to communicate with your team and stay informed. Plus, you can notify specific team members by using @mentions.\n{color}\n!Comment.png!\nNext: [Searching|TES-4]\n{color:#707070}Previous:{color} [Workflows and Statuses|TES-2]\n\n\n----\n[Learn more about editing issues|https://confluence.atlassian.com/display/JIRA/Editing+an+Issue]",
							"project": {
								"self": "http://127.0.0.1:30000/rest/api/2/project/10001",
								"id": "10001",
								"key": "TES",
								"name": "TESTdddd",
								"projectTypeKey": "business",
								"avatarUrls": {
									"48x48": "http://127.0.0.1:30000/secure/projectavatar?avatarId=10324",
									"24x24": "http://127.0.0.1:30000/secure/projectavatar?size=small&avatarId=10324",
									"16x16": "http://127.0.0.1:30000/secure/projectavatar?size=xsmall&avatarId=10324",
									"32x32": "http://127.0.0.1:30000/secure/projectavatar?size=medium&avatarId=10324"
								}
							},
							"assignee": {
								"self": "http://127.0.0.1:30000/rest/api/2/user?username=admin",
								"name": "admin",
								"key": "JIRAUSER10000",
								"emailAddress": "yuzp1996@qq.com",
								"avatarUrls": {
									"48x48": "http://127.0.0.1:30000/secure/useravatar?avatarId=10334",
									"24x24": "http://127.0.0.1:30000/secure/useravatar?size=small&avatarId=10334",
									"16x16": "http://127.0.0.1:30000/secure/useravatar?size=xsmall&avatarId=10334",
									"32x32": "http://127.0.0.1:30000/secure/useravatar?size=medium&avatarId=10334"
								},
								"displayName": "yuzp1996@qq.com",
								"active": true,
								"timeZone": "GMT"
							},
							"priority": {
								"self": "http://127.0.0.1:30000/rest/api/2/priority/3",
								"iconUrl": "http://127.0.0.1:30000/images/icons/priorities/medium.svg",
								"name": "Medium",
								"id": "3"
							},
							"updated": "2019-10-22T05:48:35.000+0000",
							"status": {
								"self": "http://127.0.0.1:30000/rest/api/2/status/10000",
								"description": "",
								"iconUrl": "http://127.0.0.1:30000/images/icons/status_generic.gif",
								"name": "待办",
								"id": "10000",
								"statusCategory": {
									"self": "http://127.0.0.1:30000/rest/api/2/statuscategory/2",
									"id": 2,
									"key": "new",
									"colorName": "blue-gray",
									"name": "待办"
								}
							}
						}
					}
				]
			}
`
		BeforeEach(func() {
			gock.New(host).
				Post("/search").
				Reply(200).
				JSON(Issuelist)

		})
		It("get the right issue detail", func() {
			listopiton := v1.ListIssuesOptions{
				Project: "TEST",
			}
			issuelist, err := client.GetIssueList(context.Background(), listopiton)
			//Expect(issuelist.StartAt).To(Equal(int64(0)))
			Expect(issuelist.Total).To(Equal(9))
			Expect(len(issuelist.Items)).To(Equal(2))
			Expect(issuelist.Items[0].Project.Name).To(Equal("TESTdddd"))
			Expect(issuelist.Items[0].Priority.Name).To(Equal("Medium"))
			Expect(issuelist.Items[0].Creator.Username).To(Equal("admin"))
			Expect(err).To(BeNil())

		})

	})
	Context("get issue list err", func() {
		const ENError = `
				{
					"errorMessages": [
						"A value with ID '21' does not exist for the field 'issuekey'."
					],
					"errors": {}
				}
			`
		const CHError = `
				{
					"errorMessages": [
						"问题关键字“TES”在域 “issuekey”中无效。"
					],
					"errors": {}
				}
			`
		BeforeEach(func() {
			err := errors.New(400, CHError)
			gock.New(host).Post("/search").ReplyError(err).JSON(CHError).Status(400)
		})

		It("can not get the right issue list", func() {
			listopiton := v1.ListIssuesOptions{
				Project:  "TEST",
				IssueKey: "notexsit",
			}
			issuelist, err := client.GetIssueList(context.Background(), listopiton)

			Expect(len(issuelist.Items)).To(Equal(0))

			Expect(err).To(BeNil())

		})

		It("IssueKeyErrorType can distinguish err type", func() {
			enmatched := v7.IssueKeyErrorType("[POST /search][400] postSearchBadRequest  &{ErrorMessages:[The issue key 'sdfdsf' for field 'issuekey' is invalid.] Errors:map[]}")
			Expect(enmatched).To(BeTrue())
			chmatched := v7.IssueKeyErrorType("[POST /search][400] postSearchBadRequest  &{ErrorMessages:[问题关键字“TES”在域 “issuekey”中无效。] Errors:map[]}")
			Expect(chmatched).To(BeTrue())
		})

	})

})
