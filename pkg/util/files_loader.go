package util

import (
	"fmt"
	"os"
	"strings"

	// "github.com/ghodss/yaml"
	"gopkg.in/src-d/go-billy.v4"
	"gopkg.in/src-d/go-billy.v4/memfs"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"gopkg.in/src-d/go-billy.v4/osfs"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
	httptransport "gopkg.in/src-d/go-git.v4/plumbing/transport/http"
	"gopkg.in/src-d/go-git.v4/storage/memory"
	corev1 "k8s.io/api/core/v1"
	glog "k8s.io/klog"
)

type filesLoader interface {
	// Files will return map of path:File
	Files() (bts map[string][]byte, workingVersion string, err error)
}

// ListFiles will list files recursive
// if source is empty will list files in disk path
// if source is not empty will list files from pipeline source
func ListFiles(source *v1alpha1.PipelineSource, diskPath string, secret *corev1.Secret) (bts map[string][]byte, workingVersion string, err error) {
	if source == nil {
		loader := &DiskLoader{
			Path: diskPath,
		}
		return loader.Files()
	}

	loader := &SCMLoader{
		PipelineSource: *source,
		Secret:         secret,
	}
	return loader.Files()
}

type SCMLoader struct {
	v1alpha1.PipelineSource
	Secret *corev1.Secret
}

func (scm *SCMLoader) Files() (bts map[string][]byte, workingVersion string, err error) {
	var loader filesLoader

	if scm.PipelineSource.Git != nil {
		loader = &GitLoader{
			PipelineSourceGit: *scm.PipelineSource.Git,
			Secret:            scm.Secret,
		}
	}

	if loader == nil {
		return nil, "", fmt.Errorf("Not Support %s", scm.PipelineSource.SourceType)
	}

	return loader.Files()
}

type GitLoader struct {
	v1alpha1.PipelineSourceGit
	Secret *corev1.Secret
}

func (gitloader *GitLoader) Files() (bts map[string][]byte, workingVersion string, err error) {
	endPoint, err := transport.NewEndpoint(gitloader.URI)
	if err != nil {
		return nil, "", err
	}

	ref := gitloader.PipelineSourceGit.Ref
	if ref == "" {
		ref = "master"
	}

	cloneOptions, err := gitloader.prepareCloneOptions(endPoint.String(), ref)
	if err != nil {
		return nil, "", err
	}

	fs := memfs.New()
	glog.V(5).Infof("Cloning %s ...", endPoint)
	repo, err := git.Clone(memory.NewStorage(), fs, cloneOptions)
	if err != nil {
		glog.Errorf("Cloned Error %s, error: %s", endPoint, err.Error())
		return nil, "", err
	}

	glog.V(5).Infof("Cloned %s", endPoint)
	files := listFiles(fs, yamlFilter)
	content, err := readFiles(fs, files)

	headRef, err := repo.Head()
	if err != nil {
		glog.Errorf("Get code repository Head error:%s, uri:%s, ref:%s", err.Error(), gitloader.URI, gitloader.PipelineSourceGit.Ref)
	}
	if headRef != nil {
		workingVersion = headRef.Hash().String()
	}

	glog.V(5).Infof("Read files successed from git %s, head:%s, ref:%s", gitloader.URI, workingVersion, gitloader.PipelineSourceGit.Ref)
	return content, workingVersion, err
}

func (gitloader *GitLoader) prepareCloneOptions(endpoint string, ref string) (*git.CloneOptions, error) {
	cloneOptions := &git.CloneOptions{
		URL:           endpoint,
		Depth:         1,
		ReferenceName: plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", ref)),
	}

	user, password, err := getUserPasswd(gitloader.Secret)
	if err != nil {
		return nil, err
	}

	if user == "" || password == "" {
		glog.V(7).Infof("without username and password in %s", endpoint)
	} else {
		cloneOptions.Auth = &httptransport.BasicAuth{
			Username: user,
			Password: password,
		}
	}

	return cloneOptions, nil
}

type DiskLoader struct {
	Path string
}

func (diskloader *DiskLoader) Files() (bts map[string][]byte, workingVersion string, err error) {
	fs := osfs.New(diskloader.Path)
	files := listFiles(fs, yamlFilter)
	bts, err = readFiles(fs, files)
	return
}

func getUserPasswd(secret *corev1.Secret) (user string, passwd string, err error) {

	if secret == nil {
		return
	}

	switch secret.Type {
	case corev1.SecretTypeBasicAuth:
		user = strings.TrimSpace(string(secret.Data[corev1.BasicAuthUsernameKey]))
		passwd = strings.TrimSpace(string(secret.Data[corev1.BasicAuthPasswordKey]))
	case v1alpha1.SecretTypeOAuth2:
		user = strings.TrimSpace(string(secret.Data[v1alpha1.OAuth2AccessTokenKeyKey]))
		passwd = strings.TrimSpace(string(secret.Data[v1alpha1.OAuth2AccessTokenKey]))
	default:
		err = fmt.Errorf("Not support secret type %s, only support %s or %s", secret.Type, corev1.SecretTypeBasicAuth, v1alpha1.SecretTypeOAuth2)
		glog.Errorf(err.Error())
	}

	return
}

func listFiles(fs billy.Filesystem, filter func(os.FileInfo) bool) map[string]os.FileInfo {
	files := map[string]os.FileInfo{}
	findFiles(files, fs, "/", filter)
	return files
}

// findFiles find yaml files from FileSystem
func findFiles(fileMap map[string]os.FileInfo, fs billy.Filesystem, dir string, filter func(os.FileInfo) bool) {
	items, err := fs.ReadDir(dir)
	if err == nil {
		for _, name := range items {
			path := fs.Join(dir, name.Name())

			if name.IsDir() {
				findFiles(fileMap, fs, path, filter)
			} else if filter(name) {
				fileMap[path] = name
			}
		}
	}
}

// yamlFilter filter file that is a yaml file
func yamlFilter(name os.FileInfo) bool {
	return strings.HasSuffix(name.Name(), ".yaml") || strings.HasSuffix(name.Name(), ".yml")
}

func readFiles(fs billy.Filesystem, filesMap map[string]os.FileInfo) (bts map[string][]byte, err error) {
	errs := MultiErrors{}
	bts = make(map[string][]byte, len(filesMap))

	for path, fileInfo := range filesMap {
		file, err := fs.OpenFile(path, os.O_RDONLY, 666)
		if err != nil {
			errs = append(errs, err)
			glog.Errorf("Cannot open file %s, err:%s", path, err.Error())
			continue
		}
		defer file.Close()

		data := make([]byte, fileInfo.Size())
		_, err = file.Read(data)
		if err != nil {
			errs = append(errs, err)
			glog.Errorf("Read file %s error:%s", path, err.Error())
			continue
		}

		bts[path] = data
	}

	if len(errs) == 0 {
		return bts, nil
	}

	return
}
