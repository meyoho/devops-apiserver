package util_test

import (
	"alauda.io/devops-apiserver/pkg/util"
	"github.com/blang/semver"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("DiffObject", func() {

	type caseData struct {
		original []metav1.Object
		wanted   []metav1.Object

		add      []metav1.Object
		update   []metav1.Object
		nochange []metav1.Object
		delete   []metav1.Object
	}

	var caseDatas = map[string]caseData{
		"case-1": caseData{
			original: []metav1.Object{
				&metav1.ObjectMeta{
					Name: "task-update",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-delete",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-nochange",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-nochange-2",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-nochange-3",
					Annotations: map[string]string{
						"version": "0.1.1",
					},
				},
			},
			wanted: []metav1.Object{
				&metav1.ObjectMeta{
					Name: "task-add",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-update",
					Annotations: map[string]string{
						"version": "0.2",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-nochange",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-nochange-2",
					Annotations: map[string]string{
						"version": "0.0.1",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-nochange-3",
					Annotations: map[string]string{
						"version": "0.1.0",
					},
				},
			},
			add: []metav1.Object{
				&metav1.ObjectMeta{
					Name: "task-add",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
			},
			update: []metav1.Object{
				&metav1.ObjectMeta{
					Name: "task-update",
					Annotations: map[string]string{
						"version": "0.2",
					},
				},
			},
			nochange: []metav1.Object{
				&metav1.ObjectMeta{
					Name: "task-nochange",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-nochange-2",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-nochange-3",
					Annotations: map[string]string{
						"version": "0.1.1",
					},
				},
			},
			delete: []metav1.Object{
				&metav1.ObjectMeta{
					Name: "task-delete",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
			},
		},
		"case-2": caseData{
			add: []metav1.Object{
				&metav1.ObjectMeta{
					Name: "task-add",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
			},
			update: []metav1.Object{
				&metav1.ObjectMeta{
					Name: "task-update",
					Annotations: map[string]string{
						"version": "0.2",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-nochange",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-nochange-2",
					Annotations: map[string]string{
						"version": "0.0.1",
					},
				},
				&metav1.ObjectMeta{
					Name: "task-nochange-3",
					Annotations: map[string]string{
						"version": "0.1.0",
					},
				},
			},
			nochange: []metav1.Object{},
			delete: []metav1.Object{
				&metav1.ObjectMeta{
					Name: "task-delete",
					Annotations: map[string]string{
						"version": "0.1",
					},
				},
			},
		},
	}

	It("Should return add and update and nochange and delete resources", func() {
		key := "case-1"

		caseData := caseDatas[key]
		add, update, nochange, delete := util.DiffObject(caseData.original, caseData.wanted, IsNewer)

		assertEqual(add, caseData.add, "Assert Add")
		assertEqual(update, caseData.update, "Assert Update")
		assertEqual(nochange, caseData.nochange, "Assert NoChange")
		assertEqual(delete, caseData.delete, "Assert Delete")
	})

	It("Force Upgrade return add and update and nochange and delete resources", func() {
		key := "case-1"
		targetkey := "case-2"

		sourceCaseData := caseDatas[key]
		targetCaseData := caseDatas[targetkey]
		add, update, nochange, delete := util.DiffObject(sourceCaseData.original, sourceCaseData.wanted, ForceNewer)

		assertEqual(add, targetCaseData.add, "Assert Add")
		assertEqual(update, targetCaseData.update, "Assert Update")
		assertEqual(nochange, targetCaseData.nochange, "Assert NoChange")
		assertEqual(delete, targetCaseData.delete, "Assert Delete")
	})
})

func assertEqual(acutal []metav1.Object, expect []metav1.Object, msg string) {
	Expect(acutal).To(HaveLen(len(expect)), msg)
	for _, item := range acutal {
		Expect(expect).To(ContainElement(item), msg)
	}
}

// IsNewer returns true if the newVer is newer than oldVer
func IsNewer(old metav1.Object, new metav1.Object) bool {
	oldVer := old.GetAnnotations()["version"]
	newVer := new.GetAnnotations()["version"]

	v1, _ := semver.ParseTolerant(oldVer)
	v2, _ := semver.ParseTolerant(newVer)

	return v2.Compare(v1) > 0
}

// ForceNewer returns true if the newVer is newer than oldVer
func ForceNewer(old metav1.Object, new metav1.Object) bool {
	return true
}
