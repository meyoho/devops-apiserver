package util

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	v1 "k8s.io/api/core/v1"
)

var _ = PDescribe("fileloader.Files", func() {
	It("Should load files and return version", func() {
		load := GitLoader{
			PipelineSourceGit: v1alpha1.PipelineSourceGit{
				URI: "https://chengjingtao@bitbucket.org/chengjingtao/pipeline-template-mig.git",
				Ref: "master",
			},
			Secret: &v1.Secret{
				Data: map[string][]byte{
					"password": []byte("YtXCqMUH29Rj5gksSV8R"),
					"username": []byte("chengjingtao"),
				},
				Type: "kubernetes.io/basic-auth",
			},
		}

		files, _, err := load.Files()

		if err != nil {
			Expect(err).NotTo(BeNil())
		}

		for path := range files {
			Expect(path).NotTo(BeEmpty())
		}
	})
})
