package util

import (
	"context"
	"encoding/json"
	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	domain "bitbucket.org/mathildetech/jenkinsfilext/v2/domain"
	jenkinsxt "bitbucket.org/mathildetech/jenkinsfilext/v2/jenkinsfile"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	glog "k8s.io/klog"
	// "github.com/jinzhu/copier"
)

var scheme = runtime.NewScheme()

func init() {
	v1alpha1.AddToScheme(scheme)
	metav1.AddToGroupVersion(scheme, schema.GroupVersion{Version: "v1"})
}

// PreviewJenkinsfile will peview jenkinsfile accordint pipelineTemplate and taskTemplates and argument values
func PreviewJenkinsfile(template devops.PipelineTemplateInterface, taskTemplates []devops.PipelineTaskTemplateInterface, PreviewOptions *devops.JenkinsfilePreviewOptions) (jenkinsfile string, err error) {

	glog.V(7).Infof("Generate Jenkinsfile by template:%s/%s, previewoptions:%#v", template.GetObjectMeta().GetNamespace(), template.GetObjectMeta().GetName(), PreviewOptions)

	// convert argument values
	argumentObjValues := map[string]interface{}{}
	for key, value := range PreviewOptions.Values {
		argumentObjValues[key] = value
	}

	// convert PipelineTemplateSpec
	pipelineTemplateSpec, err := convertPipelineTemplate(template)
	if err != nil {
		return "", err
	}

	// convert task templates
	taskTemplatesRef := make(map[string]domain.TaskTemplateSpec, len(taskTemplates))
	for _, taskTemplate := range taskTemplates {
		taskTemplateSpec, err := convertTaskTemplate(taskTemplate)
		if err != nil {
			return "", err
		}

		taskTemplatesRef[taskTemplate.GetKind()+"/"+taskTemplate.GetObjectMeta().GetName()] = *taskTemplateSpec
	}
	pipelineSpecEnvironments := pipelineTemplateSpec.Environments
	for _, environment := range PreviewOptions.Environments {
		env := jenkinsxt.EnvVar{
			Name:  environment.Name,
			Value: environment.Value,
		}
		pipelineSpecEnvironments = append(pipelineSpecEnvironments, env)
	}

	pipelineTemplateSpec.Environments = pipelineSpecEnvironments
	// preview jenkinsfile
	return pipelineTemplateSpec.Render(taskTemplatesRef, argumentObjValues, nil)
}

func convertPipelineTemplate(in devops.PipelineTemplateInterface) (*domain.PipelineTemplateSpec, error) {

	template := v1alpha1.NewPipelineTemplate(in.GetKind())

	err := scheme.Convert(in, template, context.TODO())
	if err != nil {
		return nil, err
	}

	spec := template.GetPiplineTempateSpec()
	for i := range spec.Stages {
		stage := &spec.Stages[i]
		for j := range stage.Tasks {
			task := &stage.Tasks[j]
			//just compatible with old data, TODO: it will be deleted later
			task.Type = strings.TrimPrefix(task.Type, "public/")
		}
	}

	bts, err := json.Marshal(template.GetPiplineTempateSpec())
	if err != nil {
		glog.Errorf("Marshall PipelineTemplateSpec error:%s, spec:%#v", err.Error(), *template.GetPiplineTempateSpec())
		return nil, err
	}
	pipelineTemplateSpec := &domain.PipelineTemplateSpec{}
	err = json.Unmarshal(bts, pipelineTemplateSpec)
	if err != nil {
		glog.Errorf("Unmarshall PipelineTemplateSpec error:%s, json:%s", err.Error(), string(bts))
		return nil, err
	}

	return pipelineTemplateSpec, nil
}

func convertTaskTemplate(in devops.PipelineTaskTemplateInterface) (*domain.TaskTemplateSpec, error) {
	template := v1alpha1.NewPipelineTaskTemplate(in.GetKind())
	err := scheme.Convert(in, template, context.TODO())
	if err != nil {
		return nil, err
	}

	bts, err := json.Marshal(template.GetPiplineTaskTempateSpec())
	if err != nil {
		glog.Errorf("Marshall PipelineTaskTemplateSpec error:%s, spec:%#v", err.Error(), *template.GetPiplineTaskTempateSpec())
		return nil, err
	}
	taskTemplateSpec := &domain.TaskTemplateSpec{}
	err = json.Unmarshal(bts, taskTemplateSpec)
	if err != nil {
		glog.Errorf("Unmarshall PipelineTaskTemplateSpec error:%s, json:%s", err.Error(), string(bts))
		return nil, err
	}

	return taskTemplateSpec, nil
}

// addDefaultDisplay will add default display to
func addDefaultDisplay(spec *domain.PipelineTemplateSpec) {
	for _, stage := range spec.Stages {
		if stage.Display.ZH_CN == "" && stage.Display.EN == "" {
			stage.Display.ZH_CN = stage.Name
			stage.Display.EN = stage.Name
		}

		for _, task := range stage.Tasks {
			if task.Display.ZH_CN == "" && task.Display.EN == "" {
				task.Display.ZH_CN = task.Name
				task.Display.EN = task.Name
			}
		}
	}
}
