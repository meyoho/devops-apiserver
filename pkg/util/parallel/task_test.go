package parallel_test

import (
	"alauda.io/devops-apiserver/pkg/util"
	"alauda.io/devops-apiserver/pkg/util/parallel"
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

type executeFlag struct {
	executed bool
}

func TestParallelTask(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("parallel.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/parallel", []Reporter{junitReporter})
}

func generateTask(index int, sleep time.Duration, err error, excuted *executeFlag) (task parallel.Task) {
	return func() (interface{}, error) {
		time.Sleep(sleep * time.Second)
		excuted.executed = true
		return fmt.Sprintf("task-%d", index), err
	}
}

var _ = Describe("P().Do().Wait()", func() {

	var (
		t1        parallel.Task
		t1Excuted *executeFlag
		t2        parallel.Task
		t2Excuted *executeFlag
		t3        parallel.Task
		t3Excuted *executeFlag

		ptasks *parallel.ParallelTasks

		res     []interface{}
		errs    error
		elapsed float64
	)

	BeforeEach(func() {
		t1Excuted = &executeFlag{}
		t2Excuted = &executeFlag{}
		t3Excuted = &executeFlag{}

		t1 = generateTask(1, 2, nil, t1Excuted)
		t2 = generateTask(2, 2, nil, t2Excuted)
		t3 = generateTask(3, 3, nil, t3Excuted)
		ptasks = parallel.P("custom case", t1, t2, t3)
	})

	JustBeforeEach(func() {
		begin := time.Now()
		res, errs = ptasks.Do().Wait()
		elapsed = time.Now().Sub(begin).Seconds()
	})

	Context("when none error happend", func() {
		It("should execute task parallel and collect the results", func() {
			Expect(errs).To(BeNil())
			Expect(t1Excuted.executed).To(BeTrue())
			Expect(t2Excuted.executed).To(BeTrue())
			Expect(t3Excuted.executed).To(BeTrue())
			Expect(elapsed < 4 && elapsed > 3).To(BeTrue())
			Expect(len(res)).To(BeEquivalentTo(3))
			Expect(res).To(ContainElement("task-1"))
			Expect(res).To(ContainElement("task-2"))
			Expect(res).To(ContainElement("task-3"))
		})
	})

	Context("when some errors happend", func() {
		errT1 := errors.New("task-1 error")
		errT2 := errors.New("task-2 error")
		BeforeEach(func() {
			t1 = generateTask(1, 2, errT1, t1Excuted)
			t2 = generateTask(2, 2, errT2, t2Excuted)
			ptasks = parallel.P("errors case", t1, t2, t3)
		})

		It("should return all errors happend and execute other task", func() {
			Expect(errs).ToNot(BeNil())
			multiErrs, ok := errs.(*util.MultiErrors)
			Expect(ok).To(BeTrue())
			Expect([]error(*multiErrs)).To(ContainElement(errT1))
			Expect([]error(*multiErrs)).To(ContainElement(errT2))
			Expect(multiErrs.Error()).NotTo(BeEmpty())

			Expect(t1Excuted.executed).To(BeTrue())
			Expect(t2Excuted.executed).To(BeTrue())
			Expect(t3Excuted.executed).To(BeTrue())

			Expect(elapsed < 4 && elapsed > 3).To(BeTrue())

			Expect(len(res)).To(BeEquivalentTo(1))
			Expect(res).To(ContainElement("task-3"))
		})
	})

	Context("when set failfast and errors happend", func() {
		errT1 := errors.New("task-1 error")
		errT2 := errors.New("task-2 error")
		BeforeEach(func() {
			t1 = generateTask(1, 2, errT1, t1Excuted)
			t2 = generateTask(2, 1, errT2, t2Excuted)
			ptasks = parallel.P("failfast case", t1, t2, t3).FailFast()
		})

		It("should fail immediatly and return first error", func() {
			Expect(errs).ToNot(BeNil())
			Expect(errs).To(BeEquivalentTo(errT2))

			Expect(t1Excuted.executed).To(BeFalse())
			Expect(t2Excuted.executed).To(BeTrue())
			Expect(t3Excuted.executed).To(BeFalse())

			Expect(elapsed < 2 && elapsed > 1).To(BeTrue())

			Expect(len(res)).To(BeEquivalentTo(0))
		})
	})

	Context("when canceld by caller", func() {

		It("should return reason of canceld", func() {
			var cancelFunc context.CancelFunc
			var ctx context.Context
			ctx, cancelFunc = context.WithTimeout(context.Background(), 100*time.Millisecond)
			defer cancelFunc()
			ptasksCancelable := parallel.P("canceld case", t1, t2, t3).Context(ctx)
			begin := time.Now()
			res, errs = ptasksCancelable.Do().Wait()
			elapsed = time.Now().Sub(begin).Seconds()

			Expect(errs).ToNot(BeNil())
			Expect(errs).To(BeEquivalentTo(context.DeadlineExceeded))

			fmt.Printf("%v,%v,%v,", t1Excuted.executed, t2Excuted.executed, t3Excuted.executed)
			//up to now, task is not support cancel
			//Expect(t1Excuted.executed).To(BeFalse())
			//Expect(t2Excuted.executed).To(BeFalse())
			//Expect(t3Excuted.executed).To(BeFalse())

			Expect(elapsed < 1 && elapsed > 0.1).To(BeTrue())
			Expect(len(res)).To(BeEquivalentTo(0))
		})
	})

	Context("when many task ", func() {

		flag4 := &executeFlag{}
		flag5 := &executeFlag{}
		flag6 := &executeFlag{}
		flag7 := &executeFlag{}
		flag8 := &executeFlag{}
		flag9 := &executeFlag{}
		flag10 := &executeFlag{}
		BeforeEach(func() {
			t4 := generateTask(4, 2, nil, flag4)
			t5 := generateTask(5, 1, nil, flag5)
			t6 := generateTask(6, 2, nil, flag6)
			t7 := generateTask(7, 1, nil, flag7)
			t8 := generateTask(8, 2, nil, flag8)
			t9 := generateTask(9, 3, nil, flag9)
			t10 := generateTask(10, 3, nil, flag10)
			ptasks = parallel.P("many-tasks", t1, t2, t3, t4, t5, t6).Add(t7).Add(t8).Add(t9).Add(t10).Name("custom case")
		})

		It("should return results of all tasks", func() {
			Expect(errs).To(BeNil())

			Expect(t1Excuted.executed).To(BeTrue())
			Expect(t2Excuted.executed).To(BeTrue())
			Expect(t3Excuted.executed).To(BeTrue())
			Expect(flag4.executed).To(BeTrue())
			Expect(flag5.executed).To(BeTrue())
			Expect(flag6.executed).To(BeTrue())
			Expect(flag7.executed).To(BeTrue())
			Expect(flag8.executed).To(BeTrue())
			Expect(flag9.executed).To(BeTrue())
			Expect(flag10.executed).To(BeTrue())

			Expect(elapsed < 4 && elapsed > 3).To(BeTrue())
			Expect(len(res)).To(BeEquivalentTo(10))

			for i := 1; i <= 10; i++ {
				Expect(res).To(ContainElement(fmt.Sprintf("task-%d", i)))
			}
		})
	})

	Context("when set conccurrent", func() {

		flag4 := &executeFlag{}
		flag5 := &executeFlag{}
		flag6 := &executeFlag{}
		flag7 := &executeFlag{}
		flag8 := &executeFlag{}
		flag9 := &executeFlag{}
		flag10 := &executeFlag{}
		BeforeEach(func() {
			t4 := generateTask(4, 2, nil, flag4)
			t5 := generateTask(5, 1, nil, flag5)
			t6 := generateTask(6, 3, nil, flag6)
			t7 := generateTask(7, 1, nil, flag7)
			t8 := generateTask(8, 2, nil, flag8)
			t9 := generateTask(9, 3, nil, flag9)
			t10 := generateTask(10, 3, nil, flag10)
			ptasks = parallel.P("many-tasks", t1, t2, t3, t4, t5, t6).Add(t7).Add(t8).Add(t9).Add(t10).Name("custom case")
			ptasks.SetConcurrent(5)
		})

		It("should run tasks in conccurrent count", func() {
			Expect(errs).To(BeNil())

			Expect(t1Excuted.executed).To(BeTrue())
			Expect(t2Excuted.executed).To(BeTrue())
			Expect(t3Excuted.executed).To(BeTrue())
			Expect(flag4.executed).To(BeTrue())
			Expect(flag5.executed).To(BeTrue())
			Expect(flag6.executed).To(BeTrue())
			Expect(flag7.executed).To(BeTrue())
			Expect(flag8.executed).To(BeTrue())
			Expect(flag9.executed).To(BeTrue())
			Expect(flag10.executed).To(BeTrue())

			Expect(elapsed < 7 && elapsed > 6).To(BeTrue())
			Expect(len(res)).To(BeEquivalentTo(10))

			for i := 1; i <= 10; i++ {
				Expect(res).To(ContainElement(fmt.Sprintf("task-%d", i)))
			}
		})
	})

})
