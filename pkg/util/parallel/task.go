package parallel

import (
	"alauda.io/devops-apiserver/pkg/util"
	"context"
	glog "k8s.io/klog"
	"sync"
)

type Task func() (interface{}, error)

// ParallelTasks will construct a parallel tasks struct
// you could execute tasks in parallel
// eg.
// result, err :=  P("eg1", f1,f2, f3).Do().Wait()
// result, err :=  P("eg2", f1,f2, f3).Add(f4).FailFast().Do().Wait()
// result, err :=  P("eg3", f1,f2, f3).Context(func()context.Context{
//		ctx, _ := context.WithTimeout(context.Background(), 500*time.Millisecond) // 0.5s will timeout
//		return ctx
// }).Do().Wait()
type ParallelTasks struct {
	name string

	tasks []Task

	// used for collect all errors
	errLock sync.Locker
	errs    []error

	// used for collect all results
	resultsLock sync.Locker
	results     []interface{}

	doneChan  chan struct{}
	doneError error
	doneOnce  sync.Once

	wg  sync.WaitGroup
	ctx context.Context

	// concurrency threshold
	threshold chan struct{}

	Options ParallelOptions
}

type ParallelOptions struct {
	FailFast         bool
	ConcurrencyCount int
}

// P will construct ParallelTasks
// name will be used for log
// you must care about the variable that referenced by Closure
func P(name string, tasks ...Task) *ParallelTasks {
	return &ParallelTasks{
		name:        name,
		tasks:       tasks,
		ctx:         context.Background(),
		resultsLock: &sync.Mutex{},
		results:     []interface{}{},
		errLock:     &sync.Mutex{},
		errs:        []error{},

		doneChan: make(chan struct{}),
	}
}

func (p *ParallelTasks) Name(name string) *ParallelTasks {
	p.name = name
	return p
}

// Add will add more tasks to ParallelTasks
// you should invoke it before invoke Do or Wait
// you must care about the variable that referenced by Closure
//
// eg1.
// pts := P("eg")
// for _, item := range itemArrar {
//  var itemTmp = item
// 	pts.Add(func()(interface{}, error){
// 		fmt.Println(itemTmp)
// 	})
// }
// -----------------------------------------
// eg2.
// func genTask(name string) Task {
//   return func()(interface{},error){
//   	  fmt.Println(name)
//      return nil,nil
//   }
// }

// pts := P("eg")
// for _, item := range itemArrar {
// 	pts.Add(genTask(item))
// }
//
func (p *ParallelTasks) Add(tasks ...Task) *ParallelTasks {
	p.tasks = append(p.tasks, tasks...)
	return p
}

func (p *ParallelTasks) FailFast() *ParallelTasks {
	p.Options.FailFast = true
	return p
}

func (p *ParallelTasks) SetConcurrent(count int) *ParallelTasks {
	p.Options.ConcurrencyCount = count
	return p
}

// Context will set context , up to now , task is not support to cancel
// if you cancel from context, wait will return immediately
func (p *ParallelTasks) Context(ctx context.Context) *ParallelTasks {
	p.ctx = ctx
	return p
}

// waitThreshold will wait one threshold until done
// if done ,it will return false; it will return true untill got the threshold
func (p *ParallelTasks) waitThreshold() bool {
	if p.Options.ConcurrencyCount <= 0 {
		return true
	}

	if p.threshold == nil {
		p.threshold = make(chan struct{}, p.Options.ConcurrencyCount)
	}

	select {
	case p.threshold <- struct{}{}:
		return true
	case <-p.doneChan:
		return false
	}
}

func (p *ParallelTasks) rleaseThreshold() {
	if p.Options.ConcurrencyCount <= 0 {
		return
	}

	<-p.threshold
}

// Do will start to execute all task in parallel
func (p *ParallelTasks) Do() *ParallelTasks {
	if len(p.tasks) == 0 {
		return p
	}

	go func() {
		select {
		case <-p.ctx.Done():
			p.Cancel(p.ctx.Err())
		}
	}()

	for i, task := range p.tasks {
		if !p.waitThreshold() {
			return p
		}

		p.wg.Add(1)
		go func(index int, t Task) {
			defer func() {
				p.wg.Done()
				p.rleaseThreshold()
				glog.V(9).Infof("[ParallelTasks %s] task-%d: completed \n", p.name, index+1)
			}()

			result, err := t()
			if err != nil {
				glog.Errorf("[ParallelTasks %s] task-%d: result: %#v, err: %s \n", p.name, index, result, err.Error())
			} else {
				glog.V(9).Infof("[ParallelTasks %s] task-%d: result: %#v, \n", p.name, index, result)
			}

			// error is not nil, we should save error
			if err != nil {
				p.errLock.Lock()
				defer p.errLock.Unlock()
				p.errs = append(p.errs, err)
				if p.Options.FailFast {
					glog.V(9).Infof("[ParallelTasks %s] fail fast, will cancel, reason: %#v \n", p.name, err)
					p.Cancel(err)
				}
				return
			}

			// error is nil, we should save result
			p.resultsLock.Lock()
			p.results = append(p.results, result)
			p.resultsLock.Unlock()

		}(i, task)
	}

	return p
}

func (p *ParallelTasks) Cancel(cancelReason error) {
	p.done(cancelReason)
}

func (p *ParallelTasks) done(reason error) *ParallelTasks {
	p.doneOnce.Do(func() {
		p.doneError = reason
		p.doneChan <- struct{}{}
		close(p.doneChan)
	})
	return p
}

// Wait will wait all task executed, if set fail fast , it will return immediatly if any task returns errors
// up to now , task is not support to cancel
// you should invoke Do() before invoke Wait()
// the result of task will be saved in []interface{}
// if you set failfast and one errors happend, it will return one error
// if you not set failfase and any errors happend, it will return []error as MultiErrors
func (p *ParallelTasks) Wait() ([]interface{}, error) {

	if len(p.tasks) == 0 {
		return nil, nil
	}

	go func() {
		p.wg.Wait()
		p.done(nil)
	}()

	glog.V(9).Infof("[ParallelTasks %s] waiting done.", p.name)
	<-p.doneChan
	glog.V(9).Infof("[ParallelTasks %s] waited done.", p.name)
	if p.doneError != nil {
		return p.results, p.doneError
	}

	if len(p.errs) > 0 {
		errors := util.MultiErrors(p.errs)
		return p.results, &errors
	}

	return p.results, nil
}
