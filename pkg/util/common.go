package util

import (
	"fmt"
	"os"

	"strings"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	listers "alauda.io/devops-apiserver/pkg/client/listers/devops/v1alpha1"
	"github.com/ghodss/yaml"
	"gopkg.in/src-d/go-billy.v4"
	"gopkg.in/src-d/go-billy.v4/memfs"
	"gopkg.in/src-d/go-billy.v4/osfs"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
	httptransport "gopkg.in/src-d/go-git.v4/plumbing/transport/http"
	"gopkg.in/src-d/go-git.v4/storage/memory"
	glog "k8s.io/klog"
)

const (
	defaultSystemNamespace      = "alauda-system"
	defaultCredentialsNamespace = "global-credentials"
)

// PipelineSourceMerge merger multi-source into one
func PipelineSourceMerge(source *v1alpha1.PipelineSource,
	codeRepoLister listers.CodeRepositoryLister, codeRepoBindingLister listers.CodeRepoBindingLister, namespace string) (err error) {
	codeRepositoryRef := source.CodeRepository
	if codeRepositoryRef == nil {
		return nil
	}

	name := codeRepositoryRef.Name
	branch := codeRepositoryRef.Ref

	// fetch codeRepository by ref-name
	codeRepository, err := codeRepoLister.CodeRepositories(namespace).Get(name)
	if err != nil {
		return err
	}

	// we use git finally
	if source.Git == nil {
		source.Git = &v1alpha1.PipelineSourceGit{}
	}

	codeBindingName := codeRepository.Spec.CodeRepoBinding.Name
	codeBinding, err := codeRepoBindingLister.CodeRepoBindings(namespace).Get(codeBindingName)
	if err != nil {
		return err
	}

	source.Git.Ref = branch
	source.Git.URI = codeRepository.Spec.Repository.CloneURL
	source.Secret = &codeBinding.Spec.Account.Secret

	return nil
}

// ReadFromGit will read content from git source into FileSystem
func ReadFromGit(uri string, ref string, user string, password string) (fs billy.Filesystem, err error) {
	endPoint, err := transport.NewEndpoint(uri)
	cloneOptions := &git.CloneOptions{
		URL:           endPoint.String(),
		ReferenceName: plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", ref)),
	}

	if user == "" || password == "" {
		glog.V(7).Infof("without username and password in %s", endPoint)
	} else {
		cloneOptions.Auth = &httptransport.BasicAuth{
			Username: user,
			Password: password,
		}
	}

	fs = memfs.New()
	_, err = git.Clone(memory.NewStorage(), fs, cloneOptions)

	return
}

func GetObjectFromFs(fs billy.Filesystem, handle func([]byte, string) error) (errs []error) {
	fileMap := map[string]os.FileInfo{}

	// TODO we should sort the map, handle task template first
	findYamlFiles(fileMap, fs, "/")
	glog.V(7).Infof("find yaml files: %v", fileMap)
	for path, name := range fileMap {
		file, err := fs.OpenFile(path, os.O_RDWR, 666)
		if err != nil {
			glog.V(7).Infof("can't open file %s - %v", path, err)
			errs = append(errs, err)
			continue
		}
		defer file.Close()

		data := make([]byte, name.Size())
		_, err = file.Read(data)
		if err != nil {
			glog.V(7).Infof("can't read file %s - %v", path, err)
			errs = append(errs, err)
			continue
		}

		data, err = yaml.YAMLToJSON(data)
		if err != nil {
			glog.V(7).Infof("can't convert yaml to json, file %s - %v", path, err)
			errs = append(errs, err)
			continue
		}

		glog.V(7).Infof("json - %v", (string)(data))

		err = handle(data, path)
		if err != nil {
			errs = append(errs)
		}
	}
	return
}

func GetObjectFromOS(path string, handle func([]byte, string) error) (errs []error) {
	fs := osfs.New(path)
	errs = GetObjectFromFs(fs, handle)

	return
}

// GetObjectFrom get objects from the url resources
func GetObjectFrom(url string, ref string, user string, password string, handle func([]byte, string) error) (errs []error) {
	fs, err := ReadFromGit(url, ref, user, password)
	if err != nil {
		errs = append(errs, err)
	} else {
		errs = append(errs, GetObjectFromFs(fs, handle)...)
	}

	return
}

// findYamlFiles find yaml files from FileSystem
func findYamlFiles(fileMap map[string]os.FileInfo, fs billy.Filesystem, dir string) {
	items, err := fs.ReadDir(dir)
	if err == nil {
		for _, name := range items {
			path := fs.Join(dir, name.Name())

			if name.IsDir() {
				findYamlFiles(fileMap, fs, path)
			} else if isYamlFile(name) {
				fileMap[path] = name
			}
		}
	}
}

// isYamlFile check whether target file is a yaml file
func isYamlFile(name os.FileInfo) bool {
	return strings.HasSuffix(name.Name(), ".yaml") || strings.HasSuffix(name.Name(), ".yml")
}

// RemoveDuplicateStrings removes
func RemoveDuplicateStrings(data []string) []string {
	index := map[string]struct{}{}
	for i := 0; i < len(data); i++ {
		if _, ok := index[data[i]]; ok {
			data = append(data[:i], data[i+1:]...)
			i--
		} else {
			index[data[i]] = struct{}{}
		}
	}
	return data
}

// GenCredentialsNamespace Use SystemNamespace generation CredentialsNamespace
func GenCredentialsNamespace(target string) string {
	if target == "" || target == defaultSystemNamespace {
		return defaultCredentialsNamespace
	}
	return fmt.Sprintf("%s-%s", target, defaultCredentialsNamespace)
}
