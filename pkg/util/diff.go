package util

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	glog "k8s.io/klog"
)

// DiffObject
// - will be added when in wanted, but not in original
// - will be update when in original and wanted, but resource has not equal(has changed), items will be all from wanted !
// - will be nochanged when in original and wanted , but resource has not changed,  items will be all from original !
// - will be removed when in original, but not in wanted, items will be all from original !
func DiffObject(original []metav1.Object, wanted []metav1.Object, changeFunc func(ori, wan metav1.Object) bool) (
	add []metav1.Object,
	update []metav1.Object,
	nochange []metav1.Object,
	remove []metav1.Object,
) {

	debugPrint("Original", original)
	debugPrint("Wanted", wanted)

	for _, item := range original {
		findInWanted := ExistsInObjects(wanted, item)

		if findInWanted == nil {
			remove = append(remove, item)
			continue
		}

		changed := changeFunc(item, findInWanted)
		if changed {
			// we should append findInWanted when need update
			update = append(update, findInWanted)
		} else {
			// we should append origin item when nochange
			nochange = append(nochange, item)
		}
	}

	for _, item := range wanted {
		findInOrigin := ExistsInObjects(original, item)
		if findInOrigin == nil {
			add = append(add, item)
		}
	}

	debugPrint("Add", add)
	debugPrint("Update", update)
	debugPrint("NoChange", nochange)
	debugPrint("Delete", remove)

	return
}

func debugPrint(name string, objs []metav1.Object) {
	if len(objs) == 0 {
		return
	}

	for _, obj := range objs {
		glog.V(8).Infof("[Diff] %s -  %s/%s", name, obj.GetNamespace(), obj.GetName())
	}
}

func ExistsInObjects(items []metav1.Object, target metav1.Object) (find metav1.Object) {
	for _, item := range items {

		// the same resource found
		if item.GetName() == target.GetName() && item.GetNamespace() == target.GetNamespace() {
			// if !equalFunc(ori, want) {
			// 	changed = true
			// }
			return item
		}
	}

	return nil
}
