package util_test

import (
	"alauda.io/devops-apiserver/pkg/util"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("RemoveDuplicateStrings", func() {
	It("should remove duplicated strings", func() {
		Expect(util.RemoveDuplicateStrings([]string{"a", "b", "a", "c", "b", "c"})).To(Equal([]string{"a", "b", "c"}))

		Expect(util.RemoveDuplicateStrings([]string{"a", "b", "c"})).To(Equal([]string{"a", "b", "c"}))
	})
})
