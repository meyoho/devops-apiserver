package util

import (
	"fmt"
	"strings"
)

type MultiErrors []error

func (err *MultiErrors) Error() string {
	var s strings.Builder
	for i, item := range *err {
		s.WriteString(fmt.Sprintf("Error %d: %s\n", i, item.Error()))
	}
	return s.String()
}
