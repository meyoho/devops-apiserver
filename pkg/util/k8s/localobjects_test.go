package k8s_test

import (
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"github.com/stretchr/testify/assert"
	"k8s.io/api/core/v1"
	"testing"
)

func TestIsPresent(t *testing.T) {
	type caseData struct {
		desc     string
		source   []v1.LocalObjectReference
		name     string
		expected bool
	}

	var table = []caseData{
		{
			desc:     "when source is nil, it should return false",
			source:   nil,
			name:     "secret1",
			expected: false,
		},
		{
			desc:     "when source is empty, it should return false",
			source:   []v1.LocalObjectReference{},
			name:     "secret1",
			expected: false,
		},
		{
			desc:     "when name is not exist, it should return false",
			source:   []v1.LocalObjectReference{{Name: "secret-1"}, {Name: "secret-2"}},
			name:     "secret1",
			expected: false,
		},
		{
			desc:     "when name is not exist, it should return false",
			source:   []v1.LocalObjectReference{{Name: "secret-1"}, {Name: "secret-2"}},
			name:     "",
			expected: false,
		},
		{
			desc:     "when name is exist, it should return true",
			source:   []v1.LocalObjectReference{{Name: "secret1"}, {Name: "secret2"}},
			name:     "secret1",
			expected: true,
		},
	}

	for _, testcase := range table {
		actual := k8s.LocalObjects(testcase.source).IsPresent(testcase.name)
		assert.Equal(t, testcase.expected, actual, testcase.desc)
	}
}

func TestSub(t *testing.T) {
	type caseData struct {
		desc    string
		source  []v1.LocalObjectReference
		target  []v1.LocalObjectReference
		result  []v1.LocalObjectReference
		deleted []v1.LocalObjectReference
	}

	var table = []caseData{
		{
			desc:    "when source is nil, it should return nil",
			source:  nil,
			target:  []v1.LocalObjectReference{},
			result:  nil,
			deleted: []v1.LocalObjectReference{},
		},
		{
			desc:    "when target is nil, it should return source",
			source:  []v1.LocalObjectReference{{Name: "s"}},
			target:  nil,
			result:  []v1.LocalObjectReference{{Name: "s"}},
			deleted: []v1.LocalObjectReference{},
		},
		{
			desc:    "when source and target is not nil, it should works well 1",
			source:  []v1.LocalObjectReference{{Name: "s1"}},
			target:  []v1.LocalObjectReference{{Name: "s2"}},
			result:  []v1.LocalObjectReference{{Name: "s1"}},
			deleted: []v1.LocalObjectReference{},
		},
		{
			desc:    "when source and target is not nil, it should works well 2",
			source:  []v1.LocalObjectReference{{Name: "s1"}, {Name: "s2"}},
			target:  []v1.LocalObjectReference{{Name: "s2"}},
			result:  []v1.LocalObjectReference{{Name: "s1"}},
			deleted: []v1.LocalObjectReference{{Name: "s2"}},
		},
		{
			desc:    "when source and target is not nil, it should works well 3",
			source:  []v1.LocalObjectReference{{Name: "s1"}},
			target:  []v1.LocalObjectReference{{Name: "s1"}, {Name: "s2"}},
			result:  []v1.LocalObjectReference{},
			deleted: []v1.LocalObjectReference{{Name: "s1"}},
		},
	}

	for _, testcase := range table {
		actual, deleted := k8s.LocalObjects(testcase.source).Sub(testcase.target)
		assert.Equal(t, testcase.result, actual.Array(), testcase.desc)
		assert.Equal(t, testcase.deleted, deleted.Array(), testcase.desc)
	}
}

func TestString(t *testing.T) {
	var data1 = []v1.LocalObjectReference{}
	assert.Equal(t, "", k8s.LocalObjects(data1).String())

	data1 = []v1.LocalObjectReference{{Name: "s1"}}
	assert.Equal(t, "s1", k8s.LocalObjects(data1).String())

	data1 = []v1.LocalObjectReference{{Name: "s1"}, {Name: "s2"}}
	assert.Equal(t, "s1,s2", k8s.LocalObjects(data1).String())
}
