package k8s_test

import (
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGenerateDockerCfgSecretData(t *testing.T) {
	host := "test.harbor.li"
	username := "admin"
	password := "123456"

	dockerConf := k8s.NewDockerConfigration(host, username, password)
	assert.Equal(t, 1, len(dockerConf.Auths))
	auth := dockerConf.GetAuthByHost(host)
	assert.Equal(t, username, auth.Username)
	assert.Equal(t, password, auth.Password)
	data := k8s.EncodeDockerConf(dockerConf)
	assert.NotEmpty(t, data)
}
