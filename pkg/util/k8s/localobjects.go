package k8s

import (
	"k8s.io/api/core/v1"
	"strings"
)

type LocalObjects []v1.LocalObjectReference

func (array LocalObjects) IsPresent(name string) bool {
	if len(array) == 0 {
		return false
	}

	for _, obj := range array {
		if obj.Name == name {
			return true
		}
	}
	return false
}

func (array LocalObjects) Array() []v1.LocalObjectReference {
	return []v1.LocalObjectReference(array)
}

// Sub will substract targetArray from LocalObjects array
// [ {name: "1"}, {name:"2"} ] .Sub( [ {name: "1"} ] ) will be [ {name:"2"} ]
func (array LocalObjects) Sub(targetArray []v1.LocalObjectReference) (result LocalObjects, deletedObjects LocalObjects) {
	if len(array) == 0 || len(targetArray) == 0 {
		return array, []v1.LocalObjectReference{}
	}

	target := LocalObjects(targetArray)

	deletedObjects = []v1.LocalObjectReference{}
	result = []v1.LocalObjectReference{}

	for _, item := range array {
		if target.IsPresent(item.Name) {
			deletedObjects = append(deletedObjects, item)
			continue
		}

		result = append(result, item)
	}

	return result, deletedObjects
}

func (array LocalObjects) String() string {
	if len(array) == 0 {
		return ""
	}

	builder := strings.Builder{}
	for _, item := range array {
		builder.WriteString(item.Name + ",")
	}
	return strings.TrimSuffix(builder.String(), ",")
}
