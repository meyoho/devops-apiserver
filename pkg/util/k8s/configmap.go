package k8s

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubeClientset "k8s.io/client-go/kubernetes"
	glog "k8s.io/klog"
)

const (
	defaultSystemNamespace = "alauda-system"
)

func getDefaultDevopsConfigmap() *corev1.ConfigMap {
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      v1alpha1.SettingsConfigMapName,
			Namespace: defaultSystemNamespace,
		},
		TypeMeta: metav1.TypeMeta{
			Kind:       v1alpha1.ConfigMapKindName,
			APIVersion: v1alpha1.ConfigMapAPIVersion,
		},
		Data: map[string]string{
			v1alpha1.SettingsKeyGithubCreated: "false",
		},
	}
}

func GetDevopsConfigmap(c kubeClientset.Interface) *corev1.ConfigMap {
	devopsConfigMap, err := c.CoreV1().ConfigMaps(defaultSystemNamespace).Get(v1alpha1.SettingsConfigMapName, v1alpha1.GetOptions())
	if err != nil && errors.IsNotFound(err) {
		glog.Infof("Configmap %s/%s does not exist, will create...", defaultSystemNamespace, v1alpha1.SettingsConfigMapName)
		devopsConfigMap = getDefaultDevopsConfigmap()
		devopsConfigMap, err = c.CoreV1().ConfigMaps(defaultSystemNamespace).Create(devopsConfigMap)
		if err != nil {
			glog.Errorf("Error creating Configmap %s/%s configuration configmap: %v", defaultSystemNamespace, v1alpha1.SettingsConfigMapName, err)
		}
	}
	if devopsConfigMap == nil {
		devopsConfigMap = getDefaultDevopsConfigmap()
	}
	if devopsConfigMap.Data == nil {
		devopsConfigMap.Data = make(map[string]string)
	}
	return devopsConfigMap
}
