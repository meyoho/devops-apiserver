package k8s_test

import (
	"alauda.io/devops-apiserver/pkg/util/generic"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestSecretDataBasicAuth(t *testing.T) {

	type Table struct {
		name     string
		input    k8s.SecretDataBasicAuth
		expected map[string]string
	}

	tests := []Table{
		{
			name:     "empty SecretDataBasicAuth",
			input:    k8s.SecretDataBasicAuth{},
			expected: map[string]string{"username": "", "password": ""},
		},
		{
			name: "not empty SecretDataBasicAuth",
			input: k8s.SecretDataBasicAuth{
				Username: "aaaa",
				Password: "bbbb",
			},
			expected: map[string]string{"username": "aaaa", "password": "bbbb"},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.expected, test.input.GetData())
		})
	}
}

func TestSecretDataOAuth2(t *testing.T) {

	type Table struct {
		name              string
		input             k8s.SecretDataOAuth2
		expectedGetData   map[string]string
		expectedIsExpired bool
	}

	tests := []Table{
		{
			name: "access_token is expired",
			input: k8s.SecretDataOAuth2{
				ClientID:       "aaa",
				ClientSecret:   "bbb",
				Code:           "ccc",
				AccessTokenKey: "ddd",
				SecretDataOAuth2Details: k8s.SecretDataOAuth2Details{
					AccessToken:  "eee",
					Scope:        "fff",
					RefreshToken: "ggg",
					CreatedAt:    "2010-08-05 15:59:54",
				},
			},
			expectedGetData: map[string]string{
				"clientID":       "aaa",
				"clientSecret":   "bbb",
				"code":           "ccc",
				"accessTokenKey": "ddd",
				"accessToken":    "eee",
				"scope":          "fff",
				"refreshToken":   "ggg",
				"createdAt":      "2010-08-05 15:59:54",
			},
			expectedIsExpired: true,
		},
		{
			name: "1. access_token is not expired",
			input: k8s.SecretDataOAuth2{
				ClientID:       "aaa",
				ClientSecret:   "bbb",
				Code:           "ccc",
				AccessTokenKey: "ddd",
				SecretDataOAuth2Details: k8s.SecretDataOAuth2Details{
					AccessToken:  "eee",
					Scope:        "fff",
					RefreshToken: "ggg",
					CreatedAt:    generic.ConvertTimeToStr(time.Now()),
				},
			},
			expectedGetData: map[string]string{
				"clientID":       "aaa",
				"clientSecret":   "bbb",
				"code":           "ccc",
				"accessTokenKey": "ddd",
				"accessToken":    "eee",
				"scope":          "fff",
				"refreshToken":   "ggg",
				"createdAt":      generic.ConvertTimeToStr(time.Now()),
			},
			expectedIsExpired: false,
		},
		{
			name:              "2. access_token is not expired",
			input:             k8s.SecretDataOAuth2{},
			expectedGetData:   map[string]string{"accessTokenKey": "", "clientID": "", "clientSecret": "", "scope": "", "refreshToken": "", "createdAt": "", "code": "", "accessToken": ""},
			expectedIsExpired: false,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.expectedGetData, test.input.GetData())

			assert.Equal(t, test.expectedIsExpired, test.input.IsExpired())
		})
	}
}
