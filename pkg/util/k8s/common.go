package k8s

import (
	"encoding/json"
	"fmt"
	"strings"

	"alauda.io/devops-apiserver/pkg/restapi/api"
	"alauda.io/devops-apiserver/pkg/toolchain"

	"alauda.io/devops-apiserver/pkg/apis/devops"

	"github.com/mitchellh/mapstructure"
	glog "k8s.io/klog"
	"k8s.io/klog/klogr"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	kubeClientset "k8s.io/client-go/kubernetes"
	corev1listers "k8s.io/client-go/listers/core/v1"
)

// Do not import packages outside

const (
	defaultConfigMapNamespace = "alauda-system"
	alaudaio                  = "alauda.io/"
)

func GetToolChains(c kubeClientset.Interface) (elements []*toolchain.Category, err error) {
	elements = []*toolchain.Category{}
	devopsConfigMap := GetDevopsConfigmap(c)

	domainMap := make(map[string]interface{})
	if domain, ok := devopsConfigMap.Data["_domain"]; ok && domain != "" {
		domainByte := []byte(domain)
		json.Unmarshal(domainByte, &domainMap)
	}

	if toolChains, ok := domainMap[v1alpha1.SettingsKeyToolChains]; ok && toolChains != nil {
		err = mapstructure.Decode(toolChains, &elements)
		if err != nil {
			glog.Errorf("decode the tool chain, err: %v", err)
			return
		}
	}
	return
}

func GetUsernameAndPasswordFromSecret(secretLister corev1listers.SecretLister, secretName, usernameKey, apiTokenKey, namespace string) (username, password string, err error) {
	if secretName == "" || apiTokenKey == "" || usernameKey == "" {
		return
	}

	secret, err1 := secretLister.Secrets(namespace).Get(secretName)
	if err1 != nil {
		err = err1
		return
	}
	if secret != nil && secret.Data != nil {
		username = strings.TrimSpace(string(secret.Data[usernameKey]))
		password = strings.TrimSpace(string(secret.Data[apiTokenKey]))
	}
	return
}

func GetValueInSecret(secret *corev1.Secret, key string) string {
	if secret == nil || secret.Data == nil || len(secret.Data) == 0 {
		return ""
	}

	if value, ok := secret.Data[key]; ok {
		return strings.TrimSpace(string(value))
	}
	return ""
}

func GetOwnerReferences(metadata metav1.ObjectMeta, kind string) (owners []*metav1.OwnerReference) {
	if metadata.OwnerReferences == nil || len(metadata.OwnerReferences) == 0 {
		return
	}

	owners = make([]*metav1.OwnerReference, 0)
	for _, ownerReference := range metadata.OwnerReferences {
		ownerReferenceCopy := ownerReference.DeepCopy()
		if ownerReference.Kind == kind {
			owners = append(owners, ownerReferenceCopy)
		}
	}
	return
}

func GetOwnerReference(metadata metav1.ObjectMeta, kind, name string) *metav1.OwnerReference {
	if metadata.OwnerReferences == nil || len(metadata.OwnerReferences) == 0 {
		return nil
	}

	for _, ownerReference := range metadata.OwnerReferences {
		if ownerReference.Kind == kind && ownerReference.Name == name {
			return &ownerReference
		}
	}
	return nil
}

// AddOwnerReference adds an owner reference if not foudn in the slice
func AddOwnerReference(ownerRefs []metav1.OwnerReference, owner metav1.OwnerReference) []metav1.OwnerReference {
	if ownerRefs == nil {
		ownerRefs = make([]metav1.OwnerReference, 0, 1)
	}
	found := false
	for _, o := range ownerRefs {
		if o.Kind == owner.Kind && o.Name == owner.Name && o.UID == owner.UID {
			found = true
			break
		}
	}
	if !found {
		ownerRefs = append(ownerRefs, owner)
	}
	return ownerRefs
}

func GetDataBasicAuthFromSecret(secret *corev1.Secret) *SecretDataBasicAuth {
	return &SecretDataBasicAuth{
		Username: GetValueInSecret(secret, corev1.BasicAuthUsernameKey),
		Password: GetValueInSecret(secret, corev1.BasicAuthPasswordKey),
	}
}

func GetDataOAuth2FromSecret(secret *corev1.Secret) *SecretDataOAuth2 {
	return &SecretDataOAuth2{
		ClientID:       GetValueInSecret(secret, devops.OAuth2ClientIDKey),
		ClientSecret:   GetValueInSecret(secret, devops.OAuth2ClientSecretKey),
		Code:           GetValueInSecret(secret, devops.OAuth2CodeKey),
		AccessTokenKey: GetValueInSecret(secret, devops.OAuth2AccessTokenKeyKey),
		SecretDataOAuth2Details: SecretDataOAuth2Details{
			AccessToken:  GetValueInSecret(secret, devops.OAuth2AccessTokenKey),
			Scope:        GetValueInSecret(secret, devops.OAuth2ScopeKey),
			RefreshToken: GetValueInSecret(secret, devops.OAuth2RefreshTokenKey),
			CreatedAt:    GetValueInSecret(secret, devops.OAuth2CreatedAtKey),
		},
	}
}

// GetOAuthRedirectURL fetches and returns a oAuth2 redurect url
// Uses the target secret to investigate the origin of secret
// in case it was created in ACE product it will return alauda-system/devops-config configmap
// ace related data, otherwise it will fetch alauda-system/auth-config configmap and use
// ACP url to generate
func GetOAuthRedirectURL(c corev1listers.ConfigMapLister, secret *corev1.Secret, provider devops.AnnotationProvider) string {
	return fmt.Sprintf("%s?is_secret_validate=true", getDevopsRedirectURL(c, secret, provider))
}

// GetACPPlatformAddress get platformAddress
func GetACPPlatformAddress(client kubernetes.Interface, systemNamespace string) (url string, err error) {
	var ok bool
	configMapName := ConfigMapNameGlobalConfigmap
	configMapKey := ConfigMapKeyDevOpsHost
	log := klogr.New().WithValues("configmap", fmt.Sprintf("%s/%s", systemNamespace, configMapName))
	configMap, err := client.CoreV1().ConfigMaps(systemNamespace).Get(configMapName, api.GetOptionsInCache)
	if err != nil || configMap == nil || configMap.Data == nil {
		log.Error(err, "Error when getting or configmap data is nil")
		return
	}
	url, ok = configMap.Data[configMapKey]
	if !ok {
		log.Info("Configmap \"%s/%s\" does not have key \"%s\"", systemNamespace, configMapName, configMapKey)
	}
	return
}

func getDevopsRedirectURL(c corev1listers.ConfigMapLister, secret *corev1.Secret, provider devops.AnnotationProvider) (redirectURL string) {
	if v, ok := secret.Data[devops.OAuth2RedirectURLKey]; ok {
		return string(v)
	}
	// this is used specially for development
	redirectURL = DefaultDevopsRedirectUrl
	// the used redirect URL is defined by the secret's annotations
	// if the secret was created by ACE it should use
	annotations := map[string]string{}
	if secret != nil {
		annotations = secret.GetAnnotations()
	}
	product := "ACP"
	if annotations != nil && annotations[provider.AnnotationsKeyProduct()] != "" {
		product = annotations[provider.AnnotationsKeyProduct()]
	}
	configMapName := ConfigMapNameAuthConfig
	configMapKey := ConfigMapKeyCustomRedirectURI
	var processFunc func(string) (string, error)
	switch product {
	// for ACE product just need to fetch the key directly
	case v1alpha1.AnnotationsSecretProductACE:
		configMapName = v1alpha1.SettingsConfigMapName
		configMapKey = v1alpha1.SettingsKeyACEEndpoint
		processFunc = nil

	// ACP or if not defined should process the key as a json object and get the value from it
	default:
		processFunc = func(value string) (url string, err error) {
			var customRedirectURI ConfigMapDataCustomRedirectURI
			err = json.Unmarshal([]byte(value), &customRedirectURI)
			if err != nil {
				return
			}
			// support acp1.x to acp2.x
			// configmap struct is changed
			if strings.TrimSpace(customRedirectURI.Devops) == "" {
				url = customRedirectURI.ConsoleDevops
			} else {
				url = customRedirectURI.Devops
			}
			return
		}
	}
	// Get the configmap from alauda-system namespace, and get the defined key
	// if a processing function is defined it should process
	// in the end if the value is not empty we should set as the redirect url
	configMap, err := c.ConfigMaps(defaultConfigMapNamespace).Get(configMapName)
	if err != nil || configMap == nil || configMap.Data == nil {
		glog.Errorf("Error when getting or processing configmap \"%s/%s\", err: %v", defaultConfigMapNamespace, configMapName, err)
		return
	}
	value, ok := configMap.Data[configMapKey]
	if !ok {
		glog.Errorf("Configmap \"%s/%s\" does not have key \"%s\".", defaultConfigMapNamespace, configMapName, configMapKey)
		return
	}
	if processFunc != nil {
		value, err = processFunc(value)
		if err != nil {
			glog.Errorf("Error processing Configmap \"%s/%s\" key \"%s\". err: %v", defaultConfigMapNamespace, configMapName, configMapKey, err)
		}
	}
	if value != "" {
		redirectURL = value
	}
	return
}

func CompatiableBaseDomain(object metav1.Object) (metav1.Object, bool) {

	var updateannotation, updatelabel bool
	sourceanno := object.GetAnnotations()
	updateannotation, targetanno := GetTargetAnnotation(sourceanno)
	if updateannotation {
		object.SetAnnotations(targetanno)
	}

	sourcelable := object.GetLabels()
	updatelabel, targetlabel := GetTargetAnnotation(sourcelable)
	if updatelabel {
		object.SetLabels(targetlabel)
	}
	if updatelabel || updateannotation {
		return object, true
	}

	return object, false
}

func GetTargetAnnotation(source map[string]string) (bool, map[string]string) {
	var needupdate bool
	target := map[string]string{}
	for k, v := range source {
		if containalauda(k) {
			target[deletealauda(k)] = v
			needupdate = true
		} else {
			target[k] = v
		}
	}
	return needupdate, target
}

func containalauda(data string) bool {
	return strings.HasPrefix(data, alaudaio)
}
func deletealauda(data string) string {
	return strings.TrimPrefix(data, alaudaio)
}

// HasMultibranchLabel return true if it's a multi-branch pipeline
func HasMultibranchLabel(labels map[string]string) bool {
	kind, ok := labels[devops.LabelPipelineKind]
	return ok && kind == devops.LabelPipelineKindMultiBranch
}
