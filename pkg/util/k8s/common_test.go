package k8s_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops"
	"alauda.io/devops-apiserver/pkg/util/k8s"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
)

func TestGetOwnerReferences(t *testing.T) {
	type Table struct {
		name         string
		metadata     metav1.ObjectMeta
		resourceKind string
		output       []*metav1.OwnerReference
	}

	tests := []Table{
		Table{
			name:         "get empty ownerRefenence",
			metadata:     metav1.ObjectMeta{OwnerReferences: []metav1.OwnerReference{}},
			resourceKind: devops.TypeCodeRepoService,
			output:       []*metav1.OwnerReference{},
		},

		Table{
			name: "get service ownerRefenence",
			metadata: metav1.ObjectMeta{OwnerReferences: []metav1.OwnerReference{
				metav1.OwnerReference{Name: "name1", Kind: devops.TypeCodeRepoService},
				metav1.OwnerReference{Name: "name2", Kind: devops.TypeCodeRepoBinding},
				metav1.OwnerReference{Name: "name3", Kind: devops.TypeCodeRepoBinding},
			}},
			resourceKind: devops.TypeCodeRepoService,
			output:       []*metav1.OwnerReference{{Name: "name1", Kind: devops.TypeCodeRepoService}},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, len(test.output), len(k8s.GetOwnerReferences(test.metadata, test.resourceKind)))
		})
	}
}

func TestGetOwnerReference(t *testing.T) {
	type Table struct {
		name         string
		metadata     metav1.ObjectMeta
		resourceKind string
		resourceName string
		output       *metav1.OwnerReference
	}

	tests := []Table{
		Table{
			name:         "get empty ownerRefenence",
			metadata:     metav1.ObjectMeta{OwnerReferences: []metav1.OwnerReference{}},
			resourceKind: devops.TypeCodeRepoService,
			output:       nil,
		},

		Table{
			name: "get service ownerRefenence",
			metadata: metav1.ObjectMeta{OwnerReferences: []metav1.OwnerReference{
				metav1.OwnerReference{Name: "name1", Kind: devops.TypeCodeRepoService},
				metav1.OwnerReference{Name: "name2", Kind: devops.TypeCodeRepoBinding},
				metav1.OwnerReference{Name: "name3", Kind: devops.TypeCodeRepoBinding},
			}},
			resourceKind: devops.TypeCodeRepoService,
			resourceName: "name1",
			output:       &metav1.OwnerReference{Name: "name1", Kind: devops.TypeCodeRepoService},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.output, k8s.GetOwnerReference(test.metadata, test.resourceKind, test.resourceName))
		})
	}
}

func TestHasMultibranchLabel(t *testing.T) {
	testCases := []struct {
		Name     string
		Labels   map[string]string
		Expected bool
	}{
		{
			Name: "contain the expected label",
			Labels: map[string]string{
				devops.LabelPipelineKind: devops.LabelPipelineKindMultiBranch,
			},
			Expected: true,
		}, {
			Name:     "same key, but different value",
			Labels:   map[string]string{devops.LabelPipelineKind: "fake"},
			Expected: false,
		}, {
			Name:     "not contain the expected label, labels are not empty",
			Labels:   map[string]string{"": ""},
			Expected: false,
		}, {
			Name:     "labels are empty",
			Labels:   nil,
			Expected: false,
		},
	}

	for _, tc := range testCases {
		result := k8s.HasMultibranchLabel(tc.Labels)
		assert.Equal(t, tc.Expected, result, tc.Name)
	}
}
