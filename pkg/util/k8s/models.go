package k8s

import (
	"time"

	"alauda.io/devops-apiserver/pkg/util/generic"
)

type SecretDataBasicAuth struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (s *SecretDataBasicAuth) GetData() (data map[string]string) {
	if s == nil {
		return
	}

	data = map[string]string{
		"username": s.Username,
		"password": s.Password,
	}
	return
}

// SecretDataOAuth2Details is the data we can get from access_token response
type SecretDataOAuth2Details struct {
	AccessToken  string `json:"accessToken"`
	Scope        string `json:"scope"`
	RefreshToken string `json:"refreshToken"`
	CreatedAt    string `json:"createdAt"`
	ExpiresIn    int64  `json:"expiresIn"`
}

type SecretDataOAuth2 struct {
	ClientID       string `json:"clientID"`
	ClientSecret   string `json:"clientSecret"`
	Code           string `json:"code"`
	AccessTokenKey string `json:"accessTokenKey"`
	SecretDataOAuth2Details
}

func (s *SecretDataOAuth2) HasEnoughParamsToGetAccessToken() bool {
	return s.ClientID != "" && s.ClientSecret != "" && s.Code != ""
}

func (s *SecretDataOAuth2) HasAccessToken() bool {
	return s.AccessToken != ""
}

func (s *SecretDataOAuth2) IsExpired() bool {
	// github has no RefreshToken
	if s.RefreshToken == "" || s.AccessToken == "" {
		return false
	}
	// default expires time is 7200s
	expiresIn := int64(7200)
	if s.ExpiresIn > 0 {
		expiresIn = s.ExpiresIn
	}

	createdAt := generic.ConvertStrToTime(s.CreatedAt)
	// 7200 is the minimum time
	return createdAt.Add(time.Duration(expiresIn) * time.Second).Before(time.Now())
}

func (s *SecretDataOAuth2) GetData() (data map[string]string) {
	if s == nil {
		return
	}

	data = map[string]string{
		"clientID":       s.ClientID,
		"clientSecret":   s.ClientSecret,
		"code":           s.Code,
		"accessTokenKey": s.AccessTokenKey,
		"accessToken":    s.AccessToken,
		"scope":          s.Scope,
		"refreshToken":   s.RefreshToken,
		"createdAt":      s.CreatedAt,
	}
	return
}

type ConfigMapDataCustomRedirectURI struct {
	ConsoleDevops string `json:"console-devops"`
	Devops        string `json:"devops"`
	Dashboard     string `json:"dashboard"`
}
