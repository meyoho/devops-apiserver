package label

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// LabelSelectorIsNotEmpty checks label is empty
func LabelSelectorIsNotEmpty(labelSelector *metav1.LabelSelector) bool {
	return len(labelSelector.MatchLabels) > 0 || len(labelSelector.MatchExpressions) > 0
}
