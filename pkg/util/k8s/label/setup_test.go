package label

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestLabelUtil(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("label.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/util/k8s/label", []Reporter{junitReporter})
}
