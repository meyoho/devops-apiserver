package label_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	labelutil "alauda.io/devops-apiserver/pkg/util/k8s/label"
)

var _ = Describe("k8s util label test", func() {
	var (
		result bool
		label  *metav1.LabelSelector
	)

	ShouldBeTrue := func() {
		Expect(result).To(BeTrue())
	}
	ShouldBeFalse := func() {
		Expect(result).To(BeFalse())
	}

	BeforeEach(func() {
		result = false
	})

	Context("labelSelector is not empty", func() {
		BeforeEach(func() {
			label = &metav1.LabelSelector{
				MatchLabels:      map[string]string{"foo": "bar"},
				MatchExpressions: []metav1.LabelSelectorRequirement{},
			}
		})
		JustBeforeEach(func() {
			result = labelutil.LabelSelectorIsNotEmpty(label)
		})
		It("should return true", ShouldBeTrue)
	})

	Context("should return false", func() {
		BeforeEach(func() {
			label = &metav1.LabelSelector{
				MatchLabels:      map[string]string{},
				MatchExpressions: []metav1.LabelSelectorRequirement{},
			}
		})
		JustBeforeEach(func() {
			result = labelutil.LabelSelectorIsNotEmpty(label)
		})
		It("should return false", ShouldBeFalse)
	})
})
