package k8s

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"k8s.io/api/core/v1"
	glog "k8s.io/klog"
)

type DockerSecret struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
	Auth     string `json:"auth"`
}

type DockerConfigration struct {
	Auths map[string]DockerSecret `json:"auths"`
}

func NewDockerConfigration(host, username, password string) DockerConfigration {
	dockerConf := DockerConfigration{}
	dockerSecret := DockerSecret{
		Username: username,
		Password: password,
		Email:    "my@email.com",
		Auth:     base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", username, password))),
	}
	dockerConf.AppendAuth(host, &dockerSecret)
	return dockerConf
}

func (d *DockerConfigration) AppendAuth(host string, secretInfo *DockerSecret) {
	if d.Auths == nil {
		d.Auths = make(map[string]DockerSecret, 0)
	}
	if secretInfo != nil {
		d.Auths[host] = *secretInfo
	}
}

func (d *DockerConfigration) GetAuthByHost(host string) *DockerSecret {
	if value, ok := d.Auths[host]; ok {
		return &value
	}
	return nil
}

func EncodeDockerConf(dockerConf DockerConfigration) map[string]string {
	stringData := make(map[string]string, 0)
	src, err := json.Marshal(dockerConf)
	if err != nil {
		glog.Errorf("marshal dockerConf; err: %s", err)
	}

	stringData[v1.DockerConfigJsonKey] = string(src)
	return stringData
}

func DecodeDockerConf(secret *v1.Secret) (DockerConfigration, error) {
	var (
		dockerConf DockerConfigration
		err        error
	)
	if value, ok := secret.Data[v1.DockerConfigJsonKey]; ok {

		err = json.Unmarshal(value, &dockerConf)
		if err != nil {
			glog.Errorf("unmarshal data %#v in secret; err: %v", value, err)
			return dockerConf, err
		}
	}
	return dockerConf, err
}
