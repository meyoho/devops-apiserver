package k8s

const (
	ConfigMapNameAuthConfig       = "auth-config"
	ConfigMapKeyCustomRedirectURI = "custom_redirect_uri"

	// ConfigMapNameGlobalConfigmap and ConfigMapKeyDevOpsHost are from https://bitbucket.org/mathildetech/charts/src/master/dex/templates/configmap.yaml
	ConfigMapNameGlobalConfigmap = "global-configmap"
	ConfigMapKeyDevOpsHost       = "devops_host"

	ConfigMapNameDevOpsDefaultViewTemplate = "devops-pipeline-view"

	DefaultDevopsRedirectUrl = "http://localhost:4200"

	GithubAccessTokenKey    = "x-access-token"
	GitlabAccessTokenKey    = "oauth2"
	BitbucketAccessTokenKey = "x-token-auth"
	GiteeAccessTokenKey     = "oauth2"

	GithubAccessTokenScope    = "user,repo"
	GitlabAccessTokenScope    = "api"
	BitbucketAccessTokenScope = "account team repository"
	GiteeAccessTokenScope     = "user_info groups projects"

	GithubIfModifiedSince = "If-Modified-Since"
	GitlabPrivateTokenKey = "PRIVATE-TOKEN"

	GiteaAccessTokenKey = "oauth2"
	PerPage             = 100
)
