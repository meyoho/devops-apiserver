package util_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestUtil(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("util.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/util", []Reporter{junitReporter})
}
