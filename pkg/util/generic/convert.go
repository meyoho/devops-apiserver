package generic

import (
	"encoding/json"
	"fmt"
	"github.com/mitchellh/mapstructure"
	glog "k8s.io/klog"
	"log"
	"net/url"
	"reflect"
	"strings"
)

func ConvertToMapString(obj interface{}) (result interface{}) {
	result = map[string]interface{}{}
	data, err := json.Marshal(obj)
	if err != nil {
		log.Println("marshal error", err)
		return nil
	}
	err = json.Unmarshal(data, &result)
	if err != nil {
		log.Println("unmarshal error", err)
	}
	return
}

// Bool is a helper routine that allocates a new bool value
// to store v and returns a pointer to it.
func Bool(v bool) *bool { return &v }

// Int is a helper routine that allocates a new int value
// to store v and returns a pointer to it.
func Int(v int) *int { return &v }

// Int64 is a helper routine that allocates a new int64 value
// to store v and returns a pointer to it.
func Int64(v int64) *int64 { return &v }

// String is a helper routine that allocates a new string value
// to store v and returns a pointer to it.
func String(v string) *string { return &v }

func GetPrettyString(result interface{}) string {
	js, err := json.MarshalIndent(&result, "", "\t")
	if err != nil {
		glog.Error(err)
	}
	return string(js)
}

func PrintJson(result interface{}) {
	js := GetPrettyString(result)
	glog.V(5).Infoln(string(js))
}

func GetHostInUrl(endpoint string) (host string) {
	host = endpoint
	if strings.HasPrefix(endpoint, "http://") || strings.HasPrefix(endpoint, "https://") {
		u, err := url.Parse(endpoint)
		if err != nil {
			glog.Errorf("GetHostInUrl is error: %s", err)
			return
		}
		host = u.Host
	}
	return
}

func MarshalToMapString(data interface{}) (map[string]string, error) {
	mapData := &map[string]interface{}{}
	err := mapstructure.Decode(data, mapData)
	if err != nil {
		glog.Errorf("Decode data %#v to MapData error:%#v", data, err)
		return nil, err
	}

	result := map[string]string{}
	for key, value := range *mapData {
		str, err := marshalToString(value)
		if err != nil {
			glog.Errorf("Marshal %#v to string error:%#v", value, err)
			return nil, err
		}
		result[key] = str
	}

	return result, nil
}

func marshalToString(simpleObj interface{}) (string, error) {
	if simpleObj == nil {
		return "", nil
	}

	var originStringKind = map[reflect.Kind]string{
		reflect.Bool: "", reflect.Int: "", reflect.Int8: "", reflect.Int16: "", reflect.Int32: "", reflect.Int64: "",
		reflect.Uint: "", reflect.Uint8: "", reflect.Uint16: "", reflect.Uint32: "", reflect.Uint64: "", reflect.Uintptr: "",
		reflect.Float32: "", reflect.Float64: "", reflect.String: "",
	}

	t := reflect.TypeOf(simpleObj)
	if _, ok := originStringKind[t.Kind()]; ok {
		return fmt.Sprint(simpleObj), nil
	}

	bts, err := json.Marshal(simpleObj)
	if err != nil {
		return "", err
	}
	return string(bts), nil
}
