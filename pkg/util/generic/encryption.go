package generic

import (
	"encoding/base64"
	"strings"
)

var coder = base64.NewEncoding(string([]byte("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/")))

var hashFunctionHeader = "0"
var hashFunctionFooter = "1"

func Base64Encode(str string) string {
	var src []byte = []byte(hashFunctionHeader + str + hashFunctionFooter)
	return string([]byte(coder.EncodeToString(src)))
}

func Base64Decode(str string) (string, error) {
	var src = []byte(str)
	by, err := coder.DecodeString(string(src))
	return strings.Replace(strings.Replace(string(by), hashFunctionHeader, "", -1), hashFunctionFooter, "", -1), err
}
