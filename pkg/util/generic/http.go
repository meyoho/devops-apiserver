package generic

import (
	"crypto/tls"
	"encoding/base64"
	"net"
	"net/http"
	"strings"
	"time"
)

const (
	HeaderKeyAccept            = "Accept"
	HeaderKeyContentType       = "Content-Type"
	HeaderValueContentTypeJson = "application/json"
	HeaderValueContentTypeForm = "application/x-www-form-urlencoded"
)

// AddFormHeader add the form header to request object
func AddFormHeader(request *http.Request) {
	request.Header.Add(HeaderKeyContentType, HeaderValueContentTypeForm)
}

func GetSchemaAndHost(url string) (string, string) {
	var scheme string
	schemeSplit := strings.Split(url, "//")
	if len(schemeSplit) > 0 {
		scheme = schemeSplit[0]
		scheme = strings.Replace(scheme, ":", "", -1)
	}
	if len(schemeSplit) > 1 {
		url = schemeSplit[1]
	}
	return scheme, url
}

// getRealHostAndPath will return real host and path.
// for example, if we call this method will host www.example.com/path-example
// it will return "www.example.com" and "path-example"
func GetRealHostAndPath(host string) (realHost, path string) {
	if !strings.Contains(host, "/") {
		return host, ""
	}

	hostSplit := strings.SplitN(host, "/", 2)
	return hostSplit[0], hostSplit[1]
}

func CombinePaths(pathInTheLeft, pathInTheRight string) string {
	pathInTheLeft = strings.Trim(pathInTheLeft, "/ ")
	if pathInTheLeft == "" {
		return pathInTheRight
	}
	pathInTheRight = strings.TrimLeft(pathInTheRight, "/")

	return "/" + pathInTheLeft + "/" + pathInTheRight
}

func BasicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

// GetDefaultTransport returns a Default transport
// that can request self-signed certificates
func GetDefaultTransport() http.RoundTripper {
	return &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   10 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
	}
}

// ClientOption includes http client configuration
type ClientOption struct {
	transport http.RoundTripper
	timeout   time.Duration
	// TODO
	// more client options
}

// Apply injects options to http client
func (co *ClientOption) Apply(client *http.Client) {
	client.Transport = co.transport
	client.Timeout = co.timeout
}

// ClientOptionFunc apply client option
type ClientOptionFunc func(*ClientOption)

// WithTransport apply transport
func WithTransport(transport http.RoundTripper) ClientOptionFunc {
	return func(co *ClientOption) {
		co.transport = transport
	}
}

// WithTimeout apply timeout
func WithTimeout(timeout time.Duration) ClientOptionFunc {
	return func(co *ClientOption) {
		co.timeout = timeout
	}
}

// NewHTTPClient returns a http client with options
func NewHTTPClient(opts ...ClientOptionFunc) *http.Client {
	co := &ClientOption{
		// default field
		transport: GetDefaultTransport(),
		timeout:   30 * time.Second,
	}

	for _, fn := range opts {
		fn(co)
	}

	client := &http.Client{}
	co.Apply(client)

	return client
}
