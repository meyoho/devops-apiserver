package generic_test

import (
	. "github.com/onsi/ginkgo"

	. "github.com/onsi/gomega"

	. "alauda.io/devops-apiserver/pkg/util/generic"
)

var _ = Describe("Generic", func() {
	It("test the encrypy AES", func() {
		data := "YWRtaW46MTE4ZDNmYjY4NDFlNmQzZjU2MDI4OTRiZTVlZTliMGYyNw=="
		x1 := Base64Encode(data)
		x2, _ := Base64Decode(x1)
		Expect(x2).To(Equal(data))
	})
})
