import groovy.json.JsonOutput
import org.sonatype.nexus.blobstore.api.BlobStore

List<Map<String, String>> bl = []

blobStore.blobStoreManager.browse().each {
    BlobStore bs ->
        Map<String, String> m = new HashMap<String, String>()
        m.put("name", bs.blobStoreConfiguration.name)
        m.put("type", bs.blobStoreConfiguration.type)
        bl.add(m)
}

return JsonOutput.toJson(bl)

