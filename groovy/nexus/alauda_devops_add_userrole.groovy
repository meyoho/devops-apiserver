import groovy.json.JsonSlurper
import org.sonatype.nexus.security.privilege.Privilege
import org.sonatype.nexus.security.role.RoleIdentifier


class RoleUserMap {

    List<String> users

    String registry

    String project

    String roleType

    String artifactType
}

RoleUserMap roleUserMap = new JsonSlurper().parseText(args)

List<String> users = roleUserMap.users

String registry = roleUserMap.registry

String roleType = roleUserMap.roleType

String artifactType = roleUserMap.artifactType

String privilegePrefix = "nx-repository"

String project = roleUserMap.project

String role = project + "-" + registry + "-" + roleType

def allRoles = security.getSecuritySystem().listRoles()
def roleName = null
for (int i = 0; i < allRoles.size(); i++) {

    def r = allRoles[i]
    if (r.roleId.equals(role)) {
        roleName = r.roleId
        break
    }
}

if (roleName == null) {
    def allPrivileges = security.getSecuritySystem().listPrivileges()

    def privilegeIds = new ArrayList<String>()

    String roleTypeP = roleType

    allPrivileges.each {
        Privilege p ->
            if (checkRoleType(p.name, privilegePrefix, artifactType, registry, roleType)) {
                privilegeIds.add(p.id)
            }
    }

    security.addRole(role, role, role, privilegeIds, new ArrayList<String>())
}

users.each {
    String user ->
        def userRolesIds = security.getSecuritySystem().getUser(user).roles

        List<String> userRoles = new ArrayList<String>()

        userRolesIds.each {
            RoleIdentifier ri ->
                userRoles.add(ri.roleId)
        }

        userRoles.add(role)

        security.setUserRoles(user, userRoles)
}

boolean checkRoleType(String pName, String privilegePrefix, String artifactType, String registry, String rType) {
    boolean result = false

    switch (rType) {
        case "admin":
            if (pName.indexOf("-" + artifactType + "-" + registry) >= 0) {
                result = true
            }
            break
        case "developer":
            if (pName.indexOf(privilegePrefix + "-view" + "-" + artifactType + "-" + registry) >= 0) {
                result = true
            }
            break
        default: result = false
    }
    return result
}

//{"project":"a6","registry":"test-ar-3","users":["test-ar-3-admin"],"roleType":"developer","artifactType":"maven2"}