import groovy.json.JsonSlurper
import org.sonatype.nexus.security.privilege.Privilege
import org.sonatype.nexus.security.role.RoleIdentifier


class RoleUserMap {

    List<String> users

    String registry

    String project

    String roleType
}

RoleUserMap roleUserMap = new JsonSlurper().parseText(args)

List<String> users = roleUserMap.users

String registry = roleUserMap.registry

String roleType = roleUserMap.roleType

String privilegePrefix = "nx-repository"

String project = roleUserMap.project

String role = project + "-" + registry + "-" + roleType

users.each {
    String user ->
        def userRolesIds = security.getSecuritySystem().getUser(user).roles

        List<String> userRoles = new ArrayList<String>()

        userRolesIds.each {
            RoleIdentifier ri ->
                if (!ri.roleId.equals(role)){
                    userRoles.add(ri.roleId)
                }

        }

        security.setUserRoles(user, userRoles)
}

//{"project":"a6","registry":"test-ar-3","users":["test-ar-3-admin"],"roleType":"developer"}