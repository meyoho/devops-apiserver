import groovy.json.JsonOutput
import org.sonatype.nexus.repository.Repository

def body = new groovy.json.JsonSlurper().parseText(args)
String name = body.name

Repository repo = repository.repositoryManager.get(name)

Map<String, String> result = new HashMap<String, String>()

if (repo != null) {
    result.put("name", repo.name)
    result.put("type", repo.type.toString())
    result.put("format", repo.format.toString())
    result.put("url", repo.url)
    result.put("config", JsonOutput.toJson(repo.configuration.attributes))
}

return JsonOutput.toJson(result)