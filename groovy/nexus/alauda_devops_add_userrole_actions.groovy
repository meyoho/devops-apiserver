import groovy.json.JsonSlurper
import org.sonatype.nexus.security.privilege.Privilege
import org.sonatype.nexus.security.role.RoleIdentifier


class RoleUserMap {

    List<String> users

    Map<String, List<String>> rolePrivilegesMap

    String registry

    String project

    String artifactType

    String roleType
}


RoleUserMap roleUserMap = new JsonSlurper().parseText(args)

List<String> users = roleUserMap.users

String registry = roleUserMap.registry

String artifactType = roleUserMap.artifactType

String privilegePrefix = "nx-repository"

String project = roleUserMap.project

String roleType = roleUserMap.roleType

StringBuffer role = new StringBuffer()
role = role.append(project).append("-").append(registry).append("-").append(roleType)

Map<String, List<String>> rolePrivilegesMap = roleUserMap.rolePrivilegesMap

def allRoles = security.getSecuritySystem().listRoles()
def roleName = null
for (int i = 0; i < allRoles.size(); i++) {

    def r = allRoles[i]
    if (r.roleId.equals(role.toString())) {
        roleName = r.roleId
        break
    }
}


if (roleName == null) {

    List<String> privilegeIds = new ArrayList<>()

    Iterator<Map.Entry<String, List<String>>> iterator = rolePrivilegesMap.iterator()

    while (iterator.hasNext()) {
        Map.Entry<String, List<String>> entry = (Map.Entry<String, List<String>>) iterator.next()
        String privilegesType = entry.getKey()
        List<String> actions = entry.getValue()

        privilegeIds.addAll(getPrivileges(privilegesType, actions, privilegePrefix, artifactType, registry))

    }

    security.addRole(role.toString(), role.toString(), role.toString(), privilegeIds, new ArrayList<String>())

}

users.each {
    String user ->
        def userRolesIds = security.getSecuritySystem().getUser(user).roles

        List<String> userRoles = new ArrayList<String>()

        userRolesIds.each {
            RoleIdentifier ri ->
                userRoles.add(ri.roleId)
        }

        userRoles.add(role.toString())

        security.setUserRoles(user, userRoles)
}

List<String> getPrivileges(String privilegesType, List<String> actions, String privilegePrefix, String artifactType, String registry) {
    Set<Privilege> allPrivileges = security.getSecuritySystem().listPrivileges()

    List<String> privilegeIds = new ArrayList<String>()

    allPrivileges.each {
        Privilege p ->

            if (checkPrivilege(p.name, privilegePrefix, artifactType, registry, privilegesType, actions)) {

                privilegeIds.add(p.id)

            }
    }

    return privilegeIds


}

boolean checkPrivilege(String pName, String privilegePrefix, String artifactType, String registry, String privilegesType, List<String> actions) {
    boolean result = false

    switch (privilegesType) {
        case "admin":
            if (pName.indexOf(privilegePrefix + "-admin" + "-" + artifactType + "-" + registry) >= 0) {
                result = true
            }
            break
        case "view":
            if (pName.indexOf(privilegePrefix + "-view" + "-" + artifactType + "-" + registry) >= 0) {
                result = true
            }
            break
        default: result = false
    }

    String[] pStrList = pName.split("-")
    if (!actions.contains(pStrList[pStrList.length - 1])) {
        result = false
    }

    return result
}

//{"project":"a6","registry":"test-ar-3","users":["test-ar-3-admin"],"roleType":"developer","artifactType":"maven2","rolePrivilegesMap":{"developer":["remove"],"admin":["add","read"]}}
