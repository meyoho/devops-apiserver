import org.sonatype.nexus.blobstore.api.BlobStoreManager
import org.sonatype.nexus.repository.storage.WritePolicy
import org.sonatype.nexus.repository.maven.VersionPolicy
import org.sonatype.nexus.repository.maven.LayoutPolicy
import groovy.json.JsonSlurper

//{"name":"repositoryname","versionPolicy":"release"}
def createObj = new JsonSlurper().parseText(args)
String name = createObj.artifactRegistryName

if (!repository.repositoryManager.exists(name)) {
    String blobStore = createObj.blobStore
    def versionPolicy
    switch (createObj.versionPolicy) {
        case "release": versionPolicy = VersionPolicy.RELEASE; break
        case "RELEASE": versionPolicy = VersionPolicy.RELEASE; break
        case "Release": versionPolicy = VersionPolicy.RELEASE; break
        case "snapshot": versionPolicy = VersionPolicy.SNAPSHOT; break
        case "SNAPSHOT": versionPolicy = VersionPolicy.SNAPSHOT; break
        case "Snapshot": versionPolicy = VersionPolicy.SNAPSHOT; break
        case "mixed": versionPolicy = VersionPolicy.MIXED; break
        case "MIXED": versionPolicy = VersionPolicy.MIXED; break
        case "Mixed": versionPolicy = VersionPolicy.MIXED; break
        default: versionPolicy = VersionPolicy.SNAPSHOT
    }
    repository.createMavenHosted(name, blobStore, true, versionPolicy,
            WritePolicy.ALLOW_ONCE, LayoutPolicy.STRICT)
}