package test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"github.com/ghodss/yaml"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	MasterURLKey = "master_url"
)

// Image registry useful methods for testing

// CreateImageRegistry creates a ImageRegistry in APIServer and returns
func CreateImageRegistry(devopsClient versioned.Interface, name, registryType, host string) (imageRegistry *v1alpha1.ImageRegistry, err error) {
	// first check if an existing harbor with the same host address exists, if not we should create
	imageRegistryList, err := devopsClient.DevopsV1alpha1().ImageRegistries().List(metav1.ListOptions{ResourceVersion: "0"})
	if imageRegistryList != nil && len(imageRegistryList.Items) > 0 {
		for _, item := range imageRegistryList.Items {
			if item.Spec.HTTP.Host == host {
				imageRegistry = &item
				break
			}
		}
	}
	// already found one, just return
	if imageRegistry != nil && string(imageRegistry.Spec.Type) == registryType {
		err = nil
		return
	}
	// we need to create a new one
	var content []byte
	if content, err = LoadFile("../../data/imageregistry.yaml", map[string]string{
		"{metadata.name}":  name,
		"{spec.type}":      registryType,
		"{spec.http.host}": host,
	}); err != nil {
		return
	}
	imageRegistry = new(v1alpha1.ImageRegistry)
	if err = yaml.Unmarshal(content, imageRegistry); err != nil {
		return
	}
	imageRegistry, err = devopsClient.DevopsV1alpha1().ImageRegistries().Create(imageRegistry)
	return
}

type ImageRegistryBindingOpts struct {
	Name            string
	Namespace       string
	RegistryName    string
	RegistryType    string
	SecretName      string
	SecretNamespace string
	Repositories    []string
}

// CreateImageRegistryBinding creates a ImageRegistryBinding in APIServer and returns
func CreateImageRegistryBinding(devopsClient versioned.Interface, opts ImageRegistryBindingOpts) (imageRegistryBinding *v1alpha1.ImageRegistryBinding, err error) {
	imageRegistryBinding, err = getImageRegistryBinding(opts)
	if err != nil {
		return
	}

	imageRegistryBinding.Spec.RepoInfo.Repositories = opts.Repositories
	imageRegistryBinding, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(opts.Namespace).Create(imageRegistryBinding)
	return
}

// UpdateImageRegistryBinding update a ImageRegistryBinding in APIServer and returns
func UpdateImageRegistryBinding(devopsClient versioned.Interface, opts ImageRegistryBindingOpts) (imageRegistryBinding *v1alpha1.ImageRegistryBinding, err error) {
	imageRegistryBinding, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(opts.Namespace).Get(opts.Name, metav1.GetOptions{})
	if err != nil {
		return
	}
	imageRegistryBinding = imageRegistryBinding.DeepCopy()
	imageRegistryBinding.Spec.RepoInfo.Repositories = opts.Repositories
	imageRegistryBinding, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(opts.Namespace).Update(imageRegistryBinding)
	if err != nil && errors.IsConflict(err) {
		imageRegistryBinding, err = UpdateImageRegistryBinding(devopsClient, opts)
	}
	return
}

func getImageRegistryBinding(opts ImageRegistryBindingOpts) (imageRegistryBinding *v1alpha1.ImageRegistryBinding, err error) {
	var content []byte
	if content, err = LoadFile("../../data/imageregistrybinding.yaml", map[string]string{
		"{metadata.name}":         opts.Name,
		"{metadata.namespace}":    opts.Namespace,
		"{imageregistry.name}":    opts.RegistryName,
		"{metadata.labels.type}":  opts.RegistryType,
		"{spec.secret.name}":      opts.SecretName,
		"{spec.secret.namespace}": opts.SecretNamespace,
	}); err != nil {
		return
	}
	imageRegistryBinding = new(v1alpha1.ImageRegistryBinding)
	if err = yaml.Unmarshal(content, imageRegistryBinding); err != nil {
		return
	}
	return
}

func DeleteImageRegistryBinding(devopsClient versioned.Interface, registryBinding *v1alpha1.ImageRegistryBinding) (err error) {
	if devopsClient == nil || registryBinding == nil {
		return
	}
	err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(registryBinding.Namespace).Delete(registryBinding.Name, &metav1.DeleteOptions{})
	return
}
