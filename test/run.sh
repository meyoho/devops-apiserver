#!/bin/sh

set -o pipefail
# set -o nounset

if [ "$@" ]; then
    echo "evaluation arguments $@ ..."
    eval "$($@)"
else
    echo "no arguments..."
fi
    

PLUGIN_NAME="${PLUGIN_NAME:-devops-api}"
RESULTS_DIR="${RESULTS_DIR:-/tmp/results}"
DEBUG="${DEBUG:-false}"
VERBOSE="${VERBOSE:-false}"
KEEP_GOING="${KEEP_GOING:-true}"
TEST_FOCUS="${TEST_FOCUS:-}"
export CGO_ENABLED="${CGO_ENABLED:-0}"
GENERATE_REPORT="${GENERATE_REPORT:-true}"
CONTEXT="${CONTEXT:-.}"
SOURCE_CODE="${SOURCE_CODE:-false}"

mkdir -p $RESULTS_DIR
echo "====== Processing flags  ======"
COMMAND="ginkgo -r --progress "
if [ "${DEBUG}" == "true" ] || [ "${VERBOSE}" == "true" ] ; then
    echo "==== DEBUG ${DEBUG}"
    COMMAND="${COMMAND} -v"
    echo "==== Environment variables ===="
    env
    echo "==============================="
fi
if [ "${KEEP_GOING}" == "true" ]; then
    echo "==== KEEP_GOING ${KEEP_GOING}"
    COMMAND="${COMMAND} -keepGoing"
fi
if [ "${TEST_FOCUS}" != "" ]; then
    echo "==== TEST_FOCUS ${TEST_FOCUS}"
    COMMAND="${COMMAND} -focus=${TEST_FOCUS}"
fi
if [ "${CONTEXT}" != "" ]; then
    echo "==== CONTEXT ${CONTEXT}"
    COMMAND="${COMMAND} ${CONTEXT}"
fi
echo "==============================="
echo "will run test command ${COMMAND}"
echo "==============================="

if [ "${SOURCE_CODE}" != "true" ]; then
    for f in $(find "${CONTEXT}" -name "*.test");
    do
        eval "$COMMAND -p ${f}"
    done
else
    echo "should run ${COMMAND}"
    eval "$COMMAND"
fi
# run tests
# find "${CONTEXT}" -name "*.test" -exec $COMMAND -p {} \;
# eval "$COMMAND"
CODE="${?:-0}"

if [ "$GENERATE_REPORT" == "true" ]; then
    echo "====== Generating reports ======"
    cp *.xml ${RESULTS_DIR}
    
    tar -czf ${RESULTS_DIR}/${PLUGIN_NAME}.tar.gz *.xml 

    
    echo -n "${RESULTS_DIR}/${PLUGIN_NAME}.tar.gz" >"${RESULTS_DIR}/done"
    echo "==============================="
fi

exit $CODE