package test

import (
	"fmt"
	"strconv"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	corev1 "k8s.io/api/core/v1"
)

var (
	env               = New()
	TimeoutDuration   = time.Duration(time.Minute * 10)
	QuickCheckTimeout = time.Duration(time.Second * 5)
)

const (
	TestTimeoutKey  = "test_timeout"
	CheckTimeoutKey = "check_timeout"

	IsPublicKey = "ISPUBLIC"

	// Gitlab Data
	GitlabIsReadyKey    = "GITLAB_ISREADY"
	GitlabNameKey       = "GITLAB_NAME"
	GitlabUrlKey        = "GITLAB_URL"
	GitlabUserKey       = "GITLAB_USER"
	GitlabTokenKey      = "GITLAB_TOKEN"
	GitlabRepositoryKey = "gitlab_repository"

	// Harbor Data
	HarborIsReadyKey = "HARBOR_ISREADY"
	HarborNameKey    = "HARBOR_NAME"
	HarborHttpKey    = "HARBOR_HTTP"
	HarborHostKey    = "HARBOR_HOST"
	HarborUserKey    = "HARBOR_USER"
	HarborPwdKey     = "HARBOR_PWD"
	HarborProjectKey = "HARBOR_PROJECT"
	HarborRepoKey    = "HARBOR_REPO"
	HarborRepoTagKey = "HARBOR_TAG"

	// Jenkins Data
	JenkinsIsReadyKey = "JENKINS_ISREADY"
	JenkinsNameKey    = "JENKINS_NAME"
	JenkinsHostKey    = "JENKINS_HOST"
	JenkinsUserKey    = "JENKINS_USER"
	JenkinsPwdKey     = "JENKINS_PWD"

	// PipelineConfig Data
	CodeGitlabGoPrivateKey   = "GITLAB_GOPRIVATE"
	CodeGitlabGoPublicKey    = "GITLAB_GOPUBLIC"
	CodeGitlabJavaPrivateKey = "GITLAB_JAVAPRIVATE"
	CodeGitlabJavaPublicKey  = "GITLAB_JAVAPUBLIC"

	CodeGithubJenkinsfilePrivateKey = "GITHUB_YML_PRIVATE"
	CodeGithubJenkinsfilePublicKey  = "GITHUB_YML_PUBLIC"

	DeploymentImage = "deployment_image"
)

func SetDefaults() {
	env.SetDefault(TestTimeoutKey, TimeoutDuration.String())
	env.SetDefault(CheckTimeoutKey, QuickCheckTimeout.String())
	env.SetDefault(IsPublicKey, "true")
	env.SetDefault(JenkinsIsReadyKey, "false")
	env.SetDefault(GitlabIsReadyKey, "true")
	env.SetDefault(HarborIsReadyKey, "true")

	env.SetDefault(GitlabNameKey, "gitlab-enterprise")
	env.SetDefault(GitlabUrlKey, "http://62.234.104.184:31101")
	env.SetDefault(GitlabUserKey, "root")
	env.SetDefault(GitlabTokenKey, "aT3x7EeSkHeHZ_NSTS4V")
	env.SetDefault(GitlabRepositoryKey, "root")

	env.SetDefault(JenkinsNameKey, "jenkins")
	env.SetDefault(JenkinsHostKey, "http://62.234.104.184:32001")
	env.SetDefault(JenkinsUserKey, "admin")
	env.SetDefault(JenkinsPwdKey, "1163cd92a2494f30ad1f352e3724999375")

	env.SetDefault(HarborNameKey, "harbor-registry")
	env.SetDefault(HarborHttpKey, "https://")
	env.SetDefault(HarborHostKey, "harbor.devsparrow.host")
	env.SetDefault(HarborUserKey, "admin")
	env.SetDefault(HarborPwdKey, "Harbor12345")
	env.SetDefault(HarborProjectKey, "auto-e2erepo")
	env.SetDefault(HarborRepoKey, "hello-go")
	env.SetDefault(HarborRepoTagKey, "e2elatest")

	env.SetDefault(CodeGitlabGoPrivateKey, "go-test-private")
	env.SetDefault(CodeGitlabGoPublicKey, "go-test-public")
	env.SetDefault(CodeGitlabJavaPrivateKey, "java-test-private")
	env.SetDefault(CodeGitlabJavaPublicKey, "java-test-public")

	env.SetDefault(CodeGithubJenkinsfilePrivateKey, "jenkinsfile-private")
	env.SetDefault(CodeGithubJenkinsfilePublicKey, "jenkinsfile-public")

	env.SetDefault(DeploymentImage, "index.alauda.cn/alaudaorg/qaimages:helloworld")
}

func GetValue(key string) string {
	return env.GetValue(key)
}

func WithDefault(val, defaultVal string) string {
	if val == "" {
		val = defaultVal
	}
	return val
}

func GetBoolean(key string) bool {
	val := GetValue(key)
	res, _ := strconv.ParseBool(val)
	return res
}

func GetIterationDuration() time.Duration {
	return GetDuration(CheckTimeoutKey, QuickCheckTimeout)
}

func GetDuration(key string, defaultTimeout time.Duration) time.Duration {
	return env.GetDuration(key, defaultTimeout)
}

func GetGitlabBindingData() (name string) {
	name = GenName("e2egitlabbind-")
	return
}

func GetGitlabData() (name, host string) {
	name = GenName("e2egitlab-")
	host = env.GetValue(GitlabUrlKey)
	return
}

func GetGitlabCredentials() (name, secretType string, data map[string]string) {
	name = GenName("e2egitlabsecret-")
	secretType = string(corev1.SecretTypeBasicAuth)
	data = map[string]string{
		"username": env.GetValue(GitlabUserKey),
		"password": env.GetValue(GitlabTokenKey),
	}
	return
}

func GetJenkinsData() (name, host string) {
	name = GenName("e2ejenkins-")
	host = env.GetValue(JenkinsHostKey)
	return
}

func GetJenkinsBindingData() (name string) {
	name = GenName("e2ejenkinsbind-")
	return
}
func GetJenkinsCredentials() (name, secretType string, data map[string]string) {
	name = GenName("e2ejenkinssecret-")
	secretType = string(corev1.SecretTypeBasicAuth)
	data = map[string]string{
		"username": env.GetValue(JenkinsUserKey),
		"password": env.GetValue(JenkinsPwdKey),
	}
	return
}

func GetHarborData() (name, host string) {
	name = GenName("e2eharbor-")
	host = env.GetValue(HarborHttpKey) + env.GetValue(HarborHostKey)
	return
}

func GetHarborCredentials() (name, secretType string, data map[string]string) {
	name = GenName("e2eharborsecret-")
	secretType = string(corev1.SecretTypeBasicAuth)
	data = map[string]string{
		"username": env.GetValue(HarborUserKey),
		"password": env.GetValue(HarborPwdKey),
	}
	return
}
func GetHarborBindingData() (name string) {
	name = GenName("e2eharborbind-")
	return
}

// Get CodeRepo Url
func GetCodeUrl(codeProject string) (codeUrl string) {
	codeUrl = fmt.Sprintf("%s/%s/%s", env.GetValue(GitlabUrlKey), env.GetValue(GitlabRepositoryKey), codeProject)
	return
}

// Get Repo Credential
func GetCredentialId(secret *corev1.Secret) (credentialId string) {
	credentialId = fmt.Sprintf("%s-%s", secret.Namespace, secret.Name)
	return
}
func GetImageCredentialID(imageRegistryBinding *v1alpha1.ImageRegistryBinding) (credentialId string) {
	namespace := imageRegistryBinding.Namespace
	credentialId = namespace + "-dockercfg--" + namespace + "--" + imageRegistryBinding.Name
	return
}

// Get ImageRepo Path
func GetImagePath() (imagePath string) {
	imagePath = fmt.Sprintf("%s/%s/%s", env.GetValue(HarborHostKey), env.GetValue(HarborProjectKey), env.GetValue(HarborRepoKey))
	return
}

func GetPipelineConfig_InputCodeRepo(sourceType, codeProject string, secret *corev1.Secret) (coderepo string) {
	codeUrl := GetCodeUrl(codeProject)
	codeCredentialId := GetCredentialId(secret)
	coderepo = `{"url":"` + codeUrl + `","credentialId":"` + codeCredentialId + `","sourceType":"` + sourceType + `","kind":"input"}`
	return
}
func GetPipelineConfig_SelectCodeRepo(sourceType, codeProject, serviceName string, secret *corev1.Secret) (coderepo string) {
	codeUrl := GetCodeUrl(codeProject)
	codeCredentialId := GetCredentialId(secret)
	codeRepoName := serviceName + "-" + env.GetValue(GitlabUserKey) + "-" + env.GetValue(CodeGitlabGoPrivateKey)
	coderepo = `{"url":"` + codeUrl + `","credentialId":"` + codeCredentialId + `","kind":"select","bindingRepositoryName":"` + codeRepoName + `","sourceType":"` + sourceType + `"}`
	return
}
func GetPipelineConfig_Image(kind string, imageRegistryBinding *v1alpha1.ImageRegistryBinding) (imagerepo string) {
	namespace := imageRegistryBinding.Namespace
	imagePath := GetImagePath()
	imageCredentialId := GetImageCredentialID(imageRegistryBinding)
	tag := env.GetValue(HarborRepoTagKey)
	imagerepo = `{"credentialId":"` + imageCredentialId + `","repositoryPath":"` + imagePath + `","type":"` + kind + `","tag":"` + tag + `","secretNamespace":"` + namespace + `"}`
	return
}
