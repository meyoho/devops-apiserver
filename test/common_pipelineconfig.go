package test

import (
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func GetClusterTemplateForPipelineConfig(devopsClient versioned.Interface, name string, values map[string]string) (template *v1alpha1.PipelineConfigTemplate) {
	var (
		err             error
		clusterTemplate *v1alpha1.ClusterPipelineTemplate
		tasks           []v1alpha1.ClusterPipelineTaskTemplate
	)

	clusterTemplate, err = GetClusterPipelineTemplate(devopsClient, name)
	Expect(err).To(BeNil())

	tasks, err = ListClusterPipelineTaskTemplates(devopsClient)
	Expect(err).To(BeNil())

	template = ConvertClusterPipelineTemplate(clusterTemplate, tasks)
	if len(values) > 0 {
		SetValuesToTemplate(template, values)
	}
	template.Kind = v1alpha1.TypeClusterPipelineTemplate
	return
}

// first: Get ClusterPipelineTemplate by name
func GetClusterPipelineTemplate(devopsClient versioned.Interface, name string) (clusterTemplate *v1alpha1.ClusterPipelineTemplate, err error) {
	clusterTemplate, err = devopsClient.DevopsV1alpha1().ClusterPipelineTemplates().Get(name, metav1.GetOptions{ResourceVersion: "0"})
	return
}

// second: Get a list of ClusterPipelineTaskTemplate
func ListClusterPipelineTaskTemplates(devopsClient versioned.Interface) (tasks []v1alpha1.ClusterPipelineTaskTemplate, err error) {
	var list *v1alpha1.ClusterPipelineTaskTemplateList

	list, err = devopsClient.DevopsV1alpha1().ClusterPipelineTaskTemplates().List(metav1.ListOptions{ResourceVersion: "0"})
	if err != nil {
		return
	}

	tasks = make([]v1alpha1.ClusterPipelineTaskTemplate, 0, len(list.Items))
	for _, t := range list.Items {
		tasks = append(tasks, t)
	}
	return
}

func GetPipelineTemplate(devopsClient versioned.Interface, name, namespace string) (template *v1alpha1.PipelineTemplate, err error) {
	// TODO:
	return
}

func ConvertClusterPipelineTemplate(clusterTemplate *v1alpha1.ClusterPipelineTemplate, clusterTasks []v1alpha1.ClusterPipelineTaskTemplate) (template *v1alpha1.PipelineConfigTemplate) {
	template = &v1alpha1.PipelineConfigTemplate{
		TypeMeta:   clusterTemplate.TypeMeta,
		ObjectMeta: clusterTemplate.ObjectMeta,
		Labels:     clusterTemplate.ObjectMeta.Labels,
		Spec: v1alpha1.PipelineConfigTemplateSpec{
			Engine:     clusterTemplate.Spec.Engine,
			WithSCM:    clusterTemplate.Spec.WithSCM,
			Agent:      clusterTemplate.Spec.Agent,
			Stages:     ConvertPipelineStages(clusterTemplate.Spec.Stages, clusterTasks),
			Parameters: clusterTemplate.Spec.Parameters,
			Arguments:  clusterTemplate.Spec.Arguments,
			Dependencies: &v1alpha1.PipelineDependency{
				Plugins: []v1alpha1.JenkinsPlugin{},
			},
			Environments: clusterTemplate.Spec.Environments,
		},
	}
	for _, task := range clusterTasks {
		if task.Spec.Dependencies != nil && len(task.Spec.Dependencies.Plugins) > 0 {
			template.Spec.Dependencies.Plugins = append(template.Spec.Dependencies.Plugins, task.Spec.Dependencies.Plugins...)
		}
	}
	return
}

func ConvertPipelineStages(stages []v1alpha1.PipelineStage, clusterTasks []v1alpha1.ClusterPipelineTaskTemplate) (result []v1alpha1.PipelineStageInstance) {
	result = make([]v1alpha1.PipelineStageInstance, 0, len(stages))
	for _, st := range stages {
		stage := v1alpha1.PipelineStageInstance{
			Name:  st.Name,
			Tasks: ConvertPipelineStageTasks(st.Tasks, clusterTasks),
		}
		result = append(result, stage)
	}
	return
}

func ConvertPipelineStageTasks(tasks []v1alpha1.PipelineTemplateTask, clusterTasks []v1alpha1.ClusterPipelineTaskTemplate) (result []v1alpha1.PipelineTemplateTaskInstance) {
	result = make([]v1alpha1.PipelineTemplateTaskInstance, 0, len(tasks))
	for _, t := range tasks {
		for _, c := range clusterTasks {
			if c.Name != t.Name {
				continue
			}
			instance := v1alpha1.PipelineTemplateTaskInstance{
				ObjectMeta: c.ObjectMeta,
				TypeMeta:   c.TypeMeta,
				Spec: v1alpha1.PipelineTemplateTaskInstanceSpec{
					Engine:       c.Spec.Engine,
					Agent:        t.Agent,
					Type:         t.Type,
					Body:         c.Spec.Body,
					Options:      t.Options,
					Approve:      t.Approve,
					Environments: t.Environments,
					Exports:      c.Spec.Exports,
					Arguments:    make([]v1alpha1.PipelineTemplateArgument, 0, len(c.Spec.Arguments)),
					// Relation: make([]v1alpha1.PipelineTaskArgumentAction, 0, len(c.Spec.Arguments)),
					Relation: t.Relation,
				},
			}
			// array := []v1alpha1.PipelineTemplateArgument{}
			for _, arg := range c.Spec.Arguments {
				instArg := v1alpha1.PipelineTemplateArgument{
					Name:       arg.Name,
					Schema:     arg.Schema,
					Display:    arg.Display,
					Required:   arg.Required,
					Default:    arg.Default,
					Validation: arg.Validation,
					Relation:   arg.Relation,
				}
				if instArg.Display.Description.En == "" {
					instArg.Display.Description.En = "en"
				}
				if instArg.Display.Description.Zh == "" {
					instArg.Display.Description.Zh = "zh"
				}
				instance.Spec.Arguments = append(instance.Spec.Arguments, instArg)
			}
			result = append(result, instance)
		}
	}
	return
}

type PipelineTriggerOpts struct {
	TriggerType v1alpha1.PipelineTriggerType
	Time        string
}

func GetPipelineTriggers(devopsClient versioned.Interface, opts ...PipelineTriggerOpts) (result []v1alpha1.PipelineTrigger) {

	result = make([]v1alpha1.PipelineTrigger, 0, len(opts))

	for _, op := range opts {
		trig := v1alpha1.PipelineTrigger{Type: op.TriggerType}
		switch trig.Type {
		case v1alpha1.PipelineTriggerTypeCron:
			trig.Cron = &v1alpha1.PipelineTriggerCron{
				Enabled: true,
				Rule:    op.Time,
			}
		case v1alpha1.PipelineTriggerTypeCodeChange:
			trig.CodeChange = &v1alpha1.PipelineTriggerCodeChange{
				Enabled:       true,
				PeriodicCheck: op.Time,
			}
		default:
			continue
		}
		result = append(result, trig)
	}
	return
}

func SetValuesToTemplate(template *v1alpha1.PipelineConfigTemplate, values map[string]string) {
	for a, args := range template.Spec.Arguments {
		for i, item := range args.Items {
			currentValue, ok := values[item.Name]
			if ok {
				item.Value = currentValue
				By(fmt.Sprintf("Arg %s Will use value %s", item.Name, currentValue))
			} else {
				item.Value = item.Default
				By(fmt.Sprintf("Arg %s Will use default %s", item.Name, item.Default))
			}
			args.Items[i] = item
		}
		template.Spec.Arguments[a] = args
	}
}

func ExecuteAndTestPipelineConfig(devopsClient versioned.Interface, k8sClient kubernetes.Interface, config *v1alpha1.PipelineConfig, expectedStatus v1alpha1.PipelinePhase) (pipeline *v1alpha1.Pipeline, err error) {
	By("Waiting pipelineconfig to be ready...")
	namespace := config.Namespace
	name := config.Name
	Eventually(func() (err error) {
		config, err = devopsClient.DevopsV1alpha1().PipelineConfigs(namespace).Get(name, metav1.GetOptions{ResourceVersion: "0"})
		if errors.IsNotFound(err) {
			Fail(fmt.Sprintf("PipelineConfig \"%s/%s\" not found: %#v", namespace, name, err))
			return nil
		}
		if config.Status.Phase != v1alpha1.PipelineConfigPhaseReady {
			err = fmt.Errorf("PipelineConfig \"%s\" is \"%v\"", config.Name, config.Status.Phase)
		} else {
			err = nil
		}
		return err
	}, time.Second*15).Should(Succeed())

	By("Triggering pipeline...")
	pipe := ConvertPipelineFrom(config.DeepCopy())
	pipeline, err = devopsClient.DevopsV1alpha1().Pipelines(config.Namespace).Create(pipe)
	Expect(err).To(BeNil())
	Expect(pipeline).ToNot(BeNil())
	By("Waiting pipeline to finish")
	name = pipeline.Name
	Eventually(func() (err error) {
		result, err := devopsClient.DevopsV1alpha1().Pipelines(namespace).Get(name, metav1.GetOptions{ResourceVersion: "0"})
		if err != nil && errors.IsNotFound(err) {
			Fail(fmt.Sprintf("Pipeline \"%s/%s\" not found: %#v", namespace, name, err))
			return nil
		}
		if err == nil && pipeline != nil {
			if !result.Status.Phase.IsFinalPhase() {
				err = fmt.Errorf("Pipeline status still \"%v\"", result.Status)
			} else {
				if result.Status.Phase != expectedStatus {
					Fail(fmt.Sprintf("Pipeline \"%s/%s\" phase \"%v\" is different than expected \"%v\"", namespace, name, result.Status.Phase, expectedStatus))
				}
				return nil
			}
		}
		return
	}, env.GetDuration(TestTimeoutKey, TimeoutDuration)).Should(Succeed())
	return
}

func ConvertPipelineFrom(config *v1alpha1.PipelineConfig) (pipeline *v1alpha1.Pipeline) {
	objectMeta := config.ObjectMeta.DeepCopy()
	objectMeta.ResourceVersion = ""
	objectMeta.Generation = 0
	objectMeta.SelfLink = ""
	objectMeta.UID = ""
	objectMeta.Name = "somepipelinename"
	pipeline = &v1alpha1.Pipeline{
		ObjectMeta: *objectMeta,
		Spec: v1alpha1.PipelineSpec{
			JenkinsBinding: config.Spec.JenkinsBinding,
			PipelineConfig: v1alpha1.LocalObjectReference{
				Name: config.Name,
			},
			Cause: v1alpha1.PipelineCause{
				Type:    v1alpha1.PipelineCauseTypeManual,
				Message: "Triggered using Alauda DevOps Console",
			},
			RunPolicy:  config.Spec.RunPolicy,
			Triggers:   config.Spec.Triggers,
			Strategy:   config.Spec.Strategy,
			Hooks:      config.Spec.Hooks,
			Source:     config.Spec.Source,
			Parameters: config.Spec.Parameters,
		},
	}
	return
}
