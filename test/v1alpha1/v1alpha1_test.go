package v1alpha1_test

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"sync"
	"testing"
	"time"

	clientset "alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	devopsinformers "alauda.io/devops-apiserver/pkg/client/informers/externalversions"
	"github.com/fatih/color"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

func encode(str string) (encStr string) {
	encStr = base64.StdEncoding.EncodeToString([]byte(str))
	return
}

var (
	environment = "private"

	githubAccountName    = "melody-jn"
	githubSecretUserName = encode("jnshi@alauda.io")                          //"am5zaGlAYWxhdWRhLmlv"
	githubSecretPassWord = encode("8c213e645326a918b2af989f06728655f0971e29") //"OGMyMTNlNjQ1MzI2YTkxOGIyYWY5ODlmMDY3Mjg2NTVmMDk3MWUyOQ=="
	githubOrgName        = "alauda-devops"

	gitlabAccountName    = "alaudabot"
	gitlabSecretUserName = encode("alaudabot@alauda.io")  //"YWxhdWRhYm90QGFsYXVkYS5pbw=="
	gitlabSecretPassWord = encode("zyZNE9Njx1gAdVop6EqB") //"enlaTkU5Tmp4MWdBZFZvcDZFcUI="
	gitlabOrgName        = "alauda-test"

	giteeAccountName    = "sniperyen1"
	giteeSecretUserName = encode("sniperyen1")                       //"c25pcGVyeWVuMQ=="
	giteeSecretPassWord = encode("242dcc10e395470ae7c08a74cf7355c9") //"MjQyZGNjMTBlMzk1NDcwYWU3YzA4YTc0Y2Y3MzU1Yzk="
	giteeOrgName        = "my-devops"

	bitbucketAccountName    = "jingnanshi"
	bitbucketSecretUserName = encode("jnshi@alauda.io") //"am5zaGlAYWxhdWRhLmlv"
	bitbucketSecretPassWord = encode("1021nanjing.?")   //"MTAyMW5hbmppbmcuPw=="
	bitbucketOrgName        = "mathildetech"

	templateAccountName    = "melody-jn"
	templateSecretUserName = encode("jnshi@alauda.io")                          //"am5zaGlAYWxhdWRhLmlv"
	templateSecretPassWord = encode("8c213e645326a918b2af989f06728655f0971e29") //"OGMyMTNlNjQ1MzI2YTkxOGIyYWY5ODlmMDY3Mjg2NTVmMDk3MWUyOQ=="

	httpImageRegistryHost = "http://123.206.63.18:3001"
	httpSecretUserName    = encode("admin")    //"YWRtaW4="
	httpSecretPassWord    = encode("password") //"cGFzc3dvcmQ="

	authImageRegistryHost = "http://123.206.63.18:3000"
	authSecretUserName    = encode("admin")    //"YWRtaW4="
	authSecretPassWord    = encode("password") //"cGFzc3dvcmQ="

	harborImageRegistryHost = "http://www.myalauda.com:31413"
	harborSecretUserName    = encode("admin")       //"YWRtaW4="
	harborSecretPassWord    = encode("Harbor12345") //"SGFyYm9yMTIzNDU="

)

var (
	toDelete            = []string{}
	yellow              = color.New(color.FgYellow).Add(color.Bold)
	blue                = color.New(color.FgBlue).Add(color.Bold)
	white               = color.New(color.FgWhite)
	red                 = color.New(color.FgRed)
	stoutBuf            bytes.Buffer
	devopsClient        clientset.Interface
	k8sClient           kubernetes.Interface
	kubeInformers       kubeinformers.SharedInformerFactory
	devopsInformers     devopsinformers.SharedInformerFactory
	stopChan            chan struct{}
	testCaseErrorBuffer map[string]*bytes.Buffer
	failedTests         map[string]bool
	initErrorBuffer     sync.Once
	initFailedTests     sync.Once
)

func Start() {
	if kubeInformers != nil {
		go kubeInformers.Start(stopChan)
	}
	if devopsInformers != nil {
		go devopsInformers.Start(stopChan)
	}
	testCaseErrorBuffer = map[string]*bytes.Buffer{}
	failedTests = map[string]bool{}

}

func addDelete(file string) {
	toDelete = append(toDelete, file)
}

func makeFile(fromFile string, data map[string]string) (toFile string) {
	timestamp := fmt.Sprint(time.Now().UnixNano())
	if _, ok := data["{{TIME}}"]; !ok {
		data["{{TIME}}"] = timestamp
	}

	b, _ := ioutil.ReadFile(fromFile)
	content := string(b)
	for k, v := range data {
		content = strings.Replace(content, k, v, -1)
	}
	s := strings.Split(fromFile, "/")
	fileName := s[len(s)-1]
	_, err := os.Stat("data")
	if os.IsNotExist(err) {
		os.Mkdir("data", os.ModePerm)
	}
	toFile = "data/" + fileName
	d1 := []byte(content)
	ioutil.WriteFile(toFile, d1, 0644)
	toDelete = append(toDelete, toFile)
	return toFile
}

func TestV1Alpha1(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("../v1alpha1.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "DevopsV1alpha1", []Reporter{junitReporter})
	close(stopChan)
}

func kubectlApply(fileName string, verbose ...bool) (output []byte, err error) {
	printH(line34, "kubectl apply ", fileName)
	output, err = commandP(
		"kubectl",
		"apply",
		"-f",
		fileName,
		"--validate=false",
	)
	if len(verbose) > 0 && verbose[0] && err != nil {
		// printR("kubectl apply err:", err)
	}
	return
}

func command(name string, args ...string) ([]byte, error) {
	var sterrBuf bytes.Buffer
	defer func() {
		if sterrBuf.Len() > 0 {
			//red.Println(mainLine)
			//red.Println(errorTitle, strings.TrimSpace(sterrBuf.String()))
			//red.Println(mainLine)
			addCommandBuffer(sterrBuf)
		}
	}()

	cmd := exec.Command(name, args...)
	cmd.Stderr = &sterrBuf
	return cmd.Output()
}

func commandP(name string, args ...string) (out []byte, err error) {
	out, err = command(name, args...)
	// printR("out:", trim(out))
	if err != nil {
		// printR("err:", err)
	}
	return
}

func addCommandBuffer(errBuff bytes.Buffer) {
	initErrorBuffer.Do(func() {
		if testCaseErrorBuffer == nil {
			testCaseErrorBuffer = make(map[string]*bytes.Buffer)
		}
	})
	if existingBuffer, ok := testCaseErrorBuffer[CurrentGinkgoTestDescription().FullTestText]; ok {
		existingBuffer.WriteString("\n")
		existingBuffer.Write(errBuff.Bytes())
	} else {
		testCaseErrorBuffer[CurrentGinkgoTestDescription().FullTestText] = bytes.NewBuffer(errBuff.Bytes())
	}
}

var _ = AfterEach(func() {
	initFailedTests.Do(func() {
		if failedTests == nil {
			failedTests = make(map[string]bool)
		}
	})
	failedTests[CurrentGinkgoTestDescription().FullTestText] = CurrentGinkgoTestDescription().Failed
})

func printBufferedErrors() {
	if len(testCaseErrorBuffer) == 0 {
		return
	}
	red.Println("")
	for key, failed := range failedTests {
		// red.Println("test", key, failed)
		if errBuffer, found := testCaseErrorBuffer[key]; found && failed {
			red.Println(line34, "test case errors: ", key)
			red.Println(strings.TrimSpace(errBuffer.String()))
			red.Println(line)
		}
	}
}

const (
	header = iota
	mainHeader
	result
	footer

	line       = `----------------------------------------------`
	line34     = `--------`
	mainLine   = `===================================================`
	errorTitle = " - Command returned error:"
)

func printH(content ...interface{}) {
	printTest(header, content...)
}
func printM(content ...interface{}) {
	printTest(mainHeader, content...)
}

func printF(content ...interface{}) {
	printTest(footer, content...)
}

func printR(content ...interface{}) {
	printTest(result, content...)
}

func trim(data []byte) string {
	return strings.TrimSpace(string(data))
}

func printTest(printType int, content ...interface{}) {
	//Do nothing now
	return
}

// DescribeF small set of cases
func DescribeF(title string, test func()) bool {
	return Describe(title, func() {
		// printH(line34, "Starting:", title)
		test()
	})
}

// XDescribeF Pending tests
func XDescribeF(title string, test func()) bool {
	return XDescribe(title, func() {
		// printH(line34, "Pending:", title)
		test()
	})
}

func SDescribeF(title string, test func()) bool {
	if environment == "private" {
		return XDescribe(title, func() {
			// printH(line34, "Pending:", title)
			test()
		})
	} else {
		return Describe(title, func() {
			// printH(line34, "Starting:", title)
			test()
		})
	}
}

// DescribeM
// this is a major describe function to group a big set of cases
func DescribeM(title string, test func()) bool {
	return Describe(title, func() {
		defer GinkgoRecover()
		printM("\tStarting:", title)
		test()
		// printM("\tEnding:", title)
	})
}

var _ = AfterSuite(func() {
	printM("End!")
	var (
		err    error
		output []byte
	)
	for _, de := range toDelete {
		// printF("\ndeleting file:", de)
		output, err = exec.Command("kubectl", "delete", "-f", de).Output()
		if output != nil || err != nil {
			//
		}
		// GinkgoWriter.Write(output)
		// printF("output", string(output), "err", err)
		os.Remove(de)
	}
	os.Remove("data")
	printBufferedErrors()
	// Expect(err).NotTo(HaveOccurred())

})

func init() {

	stopChan = make(chan struct{})
	var (
		clientConfig   *restclient.Config
		kubeConfigPath = ""
		err            error
		home           = os.Getenv("HOME")
		// kube           = os.Getenv("KUBE")
	)
	if _, err = os.Stat(fmt.Sprintf("%s/.kube/config", home)); err == nil {
		kubeConfigPath = fmt.Sprintf("%s/.kube/config", home)
		printH("Will use kube config: ", kubeConfigPath)
	}

	if clientConfig, err = clientcmd.BuildConfigFromFlags("", kubeConfigPath); err != nil {
		printR("Error building config:", err.Error())
	}
	if k8sClient, err = kubernetes.NewForConfig(clientConfig); err != nil {
		printR("Error building kubernetes client:", err.Error())
	}
	if devopsClient, err = clientset.NewForConfig(clientConfig); err != nil {
		printR("Error building devops client:", err.Error())
	}
	kubeInformers = kubeinformers.NewSharedInformerFactory(k8sClient, time.Second*30)
	devopsInformers = devopsinformers.NewSharedInformerFactory(devopsClient, time.Second*30)

}

func RetryExecution(f func() error) (err error) {
	Eventually(f, time.Second*5).Should(Succeed())
	return nil
}
