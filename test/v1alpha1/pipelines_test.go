package v1alpha1_test

import (
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = DescribeM("PipelineConfig", func() {
	var (
		pipelineconfig     *v1alpha1.PipelineConfig
		pipelineconfiglist *v1alpha1.PipelineConfigList
		pipeline           *v1alpha1.Pipeline
		pipelinelist       *v1alpha1.PipelineList
		// event                                                                                 *corev1.EventList
		// project                                                                               *v1alpha1.Project
		// jenkins                                                                               *v1alpha1.Jenkins
		// jenkinsbinding                                                                        *v1alpha1.JenkinsBinding
		// jbsecret, gitsecret                                                                   *corev1.Secret
		// namespace                                                                             *corev1.Namespace
		name, pipelinename, triggertype                                                       string
		file, dependencies, pipelinefile                                                      string
		pipelineconfigData                                                                    map[string]string
		timestamp                                                                             int64
		projectName, jenkinsName, jenkinsBindingSecretName, jenkinsbindingName, gitSecretName string
		err, projecterr, nserr, jenkinserr, jenkinsbindingerr, secreterr, gitsecreterr        error
		// eventErr                                                                              error
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		name = "e2epipelineconfigTEST" + fmt.Sprintf("%v", timestamp)
		pipelinename = name + "-1"
		projectName = "nsforpipelineconfig" + fmt.Sprintf("%v", timestamp)
		jenkinsName = "jenkinsforpipelineconfig" + fmt.Sprintf("%v", timestamp)
		jenkinsBindingSecretName = "jbsecretforpipelineconfig" + fmt.Sprintf("%v", timestamp)
		jenkinsbindingName = "jenkinsbindingforpipelineconfig" + fmt.Sprintf("%v", timestamp)
		gitSecretName = "gitsecretforpipelineconfig" + fmt.Sprintf("%v", timestamp)
	})
	JustBeforeEach(func() {
		//create dependencies
		dependencies = "pipelines/pipelineconfig-dependencies.yaml"
		pipelineconfigData = map[string]string{
			"namespaceName":            projectName,
			"projectName":              projectName,
			"jenkinsName":              jenkinsName,
			"jenkinsBindingSecretName": jenkinsBindingSecretName,
			"jenkinsbindingName":       jenkinsbindingName,
			"gitSecretName":            gitSecretName,
			"pipelineConfigName":       name,
			"pipelineName":             pipelinename,
		}
		dependencies = makeFile(dependencies, pipelineconfigData)
		kubectlApply(dependencies, true)

		file = "pipelines/pipelineconfig.yaml"
		file = makeFile(file, pipelineconfigData)
		kubectlApply(file, true)

		pipelinefile = "pipelines/pipeline.yaml"
		pipelinefile = makeFile(pipelinefile, pipelineconfigData)
		kubectlApply(pipelinefile, true)

	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", pipelinefile)
		commandP("kubectl", "delete", "-f", file)
		commandP("kubectl", "delete", "-f", dependencies)
	})
	DescribeF("create pipelineconfig", func() {
		JustBeforeEach(func() {
			_, nserr = k8sClient.CoreV1().Namespaces().Get(projectName, v1.GetOptions{})
			_, jenkinserr = devopsClient.DevopsV1alpha1().Jenkinses().Get(jenkinsName, v1.GetOptions{})
			_, secreterr = k8sClient.CoreV1().Secrets(projectName).Get(jenkinsBindingSecretName, v1.GetOptions{})
			_, jenkinsbindingerr = devopsClient.DevopsV1alpha1().JenkinsBindings(projectName).Get(jenkinsbindingName, v1.GetOptions{})
			_, gitsecreterr = k8sClient.CoreV1().Secrets(projectName).Get(gitSecretName, v1.GetOptions{})
			pipelineconfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(projectName).Get(name, v1.GetOptions{})
			time.Sleep(time.Second * 10)
			// event, _ = k8sClient.CoreV1().Events(projectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(projecterr).To(BeNil())
			Expect(nserr).To(BeNil())
			Expect(jenkinserr).To(BeNil())
			Expect(secreterr).To(BeNil())
			Expect(gitsecreterr).To(BeNil())
			Expect(jenkinsbindingerr).To(BeNil())
			Expect(err).To(BeNil())
			triggertype = string(pipelineconfig.Spec.Triggers[0].Type)
			Expect(triggertype).To(Equal("codeChange"))
			phase := string(pipelineconfig.Status.Phase)
			Expect(phase).To(Or(Equal("Creating"), Equal("Syncing")))
			// Expect(event.String()).To(ContainSubstring("PipelineConfigCreated"))
		})
	})

	DescribeF("list pipelineconfig", func() {
		JustBeforeEach(func() {
			pipelineconfiglist, err = devopsClient.DevopsV1alpha1().PipelineConfigs(projectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(pipelineconfiglist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range pipelineconfiglist.Items {
				if p.Name == name {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})

	DescribeF("update pipelineconfig", func() {
		JustBeforeEach(func() {
			RetryExecution(func() error {
				pipelineconfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(projectName).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				pipelineconfig = pipelineconfig.DeepCopy()
				pipelineconfig.Spec.Triggers = []v1alpha1.PipelineTrigger{
					v1alpha1.PipelineTrigger{
						Type: "cron",
						Cron: &v1alpha1.PipelineTriggerCron{
							Enabled: true,
							Rule:    "* * * * *",
						},
					},
				}
				pipelineconfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(projectName).Update(pipelineconfig)
				return err
			})
			time.Sleep(time.Second * 10)
			// event, eventErr = k8sClient.CoreV1().Events(projectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			triggertype = string(pipelineconfig.Spec.Triggers[0].Type)
			Expect(triggertype).To(Equal("cron"))
			// Expect(eventErr).To(BeNil())
			// Expect(event.String()).To(ContainSubstring("PipelineConfigUpdated"))
		})
	})

	// Pipeline naming convertion is:
	// <PipelineConfig name>-<sequential number>
	DescribeF("create pipeline", func() {
		JustBeforeEach(func() {
			pipeline, err = devopsClient.DevopsV1alpha1().Pipelines(projectName).Get(pipelinename, v1.GetOptions{})
			time.Sleep(time.Second * 10)
			// event, _ = k8sClient.CoreV1().Events(projectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			phase := string(pipeline.Status.Phase)
			Expect(phase).To(Equal("Pending"))
			// Expect(event.String()).To(ContainSubstring("PipelineStarted"))
		})
	})

	DescribeF("list pipeline", func() {
		JustBeforeEach(func() {
			pipelinelist, err = devopsClient.DevopsV1alpha1().Pipelines(projectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(pipelinelist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range pipelinelist.Items {
				if p.Name == pipelinename {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})

	DescribeF("delete pipeline", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().Pipelines(projectName).Delete(pipelinename, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})

	DescribeF("get pipelinelog", func() {
		JustBeforeEach(func() {
			pipelinelogoption := v1alpha1.PipelineLogOptions{}
			_, err = devopsClient.DevopsV1alpha1().Pipelines(projectName).GetLogs(pipelinename, &pipelinelogoption)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})

	DescribeF("get pipelinetask", func() {
		JustBeforeEach(func() {
			pipelinetaskoption := v1alpha1.PipelineTaskOptions{}
			_, err = devopsClient.DevopsV1alpha1().Pipelines(projectName).GetTasks(pipelinename, &pipelinetaskoption)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})

	DescribeF("delete pipelineconfig", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().PipelineConfigs(projectName).Delete(name, nil)
			time.Sleep(time.Second * 10)
			// event, eventErr = k8sClient.CoreV1().Events(projectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			// Expect(eventErr).To(BeNil())
			// Expect(len(event.Items)).ToNot(Equal(0))
			// for _, ev := range event.Items {
			// 	if ev.
			// }
			// Expect(event.String()).To(ContainSubstring("PipelineConfigDeleted"))
		})
	})
})
