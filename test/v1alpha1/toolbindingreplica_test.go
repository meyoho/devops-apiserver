package v1alpha1_test

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	devopsv1alpha1 "alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = DescribeM("ToolBindingReplica的API API Test", func() {

	var (
		PROJECT_NS, GROUP3_NS, GROUP1_NS, GROUP2_NS, GROUP4_NS, GROUP5_NS, GROUP6_NS, GROUP7_NS                            string
		toolBindingReplicaFile, toolDependencyFile, dependencyFile, file_group, file_error, file_namespace, name, toolName string
		timestamp                                                                                                          int64
		err                                                                                                                error
		output                                                                                                             []byte
		toolbindingreplicaconfigData                                                                                       map[string]string
		coderepobinding                                                                                                    *devopsv1alpha1.CodeRepoBinding
		toolbindingreplica                                                                                                 *devopsv1alpha1.ToolBindingReplica
		toolbindingreplicaList                                                                                             *devopsv1alpha1.ToolBindingReplicaList
	)

	PContext("Valid binding is successful and update delete modify", func() {

		BeforeEach(func() {
			timestamp = time.Now().UnixNano()
			name = "e2etoolbindingreplica-" + fmt.Sprintf("%v", timestamp)
			toolName = "any-tool"
			PROJECT_NS = "alaudatest" + fmt.Sprintf("%v", timestamp)
			GROUP1_NS = "alaudatest-dev" + fmt.Sprintf("%v", timestamp)
			GROUP2_NS = "alaudatest-ops" + fmt.Sprintf("%v", timestamp)
			GROUP3_NS = "alaudatest-test" + fmt.Sprintf("%v", timestamp)
		})

		JustBeforeEach(func() {
			dependencyFile = "toolbindingreplica/toolbindingreplica.dependency.yaml"
			toolbindingreplicaconfigData = map[string]string{
				"{{PROJECT_NS}}": PROJECT_NS,
				"{{GROUP1_NS}}":  GROUP1_NS,
				"{{GROUP2_NS}}":  GROUP2_NS,
				"{{TOOL_NAME}}":  toolName,
				"{{NAME}}":       name,
				"{{TIME}}":       fmt.Sprint(timestamp),
			}

			dependencyFile = makeFile(dependencyFile, toolbindingreplicaconfigData)
			kubectlApply(dependencyFile, true)
			Eventually(eventuallyNamespaceReadyFunc(PROJECT_NS, GROUP1_NS, GROUP2_NS), time.Second*5).Should(Succeed())
		})

		AfterEach(func() {
			commandP("kubectl", "delete", "-f", toolBindingReplicaFile)
			commandP("kubectl", "delete", "-f", toolDependencyFile)
			commandP("kubectl", "delete", "-f", dependencyFile)
		})

		// we cannot depend alauda confluence online, so Pending it, you can execute on local development
		PContext("When tool kind is DocumentManagement", func() {
			JustBeforeEach(func() {
				toolDependencyFile = "toolbindingreplica/toolbindingreplica.confluence-dependency.yaml"
				toolDependencyFile = makeFile(toolDependencyFile, toolbindingreplicaconfigData)
				kubectlApply(toolDependencyFile, true)

				toolBindingReplicaFile = "toolbindingreplica/toolbindingreplica.confluence.yaml"
				toolBindingReplicaFile = makeFile(toolBindingReplicaFile, toolbindingreplicaconfigData)
				kubectlApply(toolBindingReplicaFile, true)
			})

			It("Get ToolBindingReplica successfully", func() {
				Eventually(eventuallyFunc(PROJECT_NS, name, string(devopsv1alpha1.ServiceStatusPhaseReady)), time.Second*20).Should(Succeed())
				toolbindingreplica, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(len(toolbindingreplica.Status.Conditions)).To(Equal(4))

				documentManageBinding, err := devopsClient.DevopsV1alpha1().DocumentManagementBindings(GROUP1_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(documentManageBinding.Name).To(Equal(name))
				Expect(documentManageBinding.Namespace).To(Equal(GROUP1_NS))
				Expect(len(documentManageBinding.Spec.DocumentManagementSpaceRefs)).To(Equal(2))
				Expect(documentManageBinding.Spec.DocumentManagementSpaceRefs).To(ContainElement(devopsv1alpha1.DocumentManagementSpaceRef{Name: "alaudakb"}))
				Expect(documentManageBinding.Spec.DocumentManagementSpaceRefs).To(ContainElement(devopsv1alpha1.DocumentManagementSpaceRef{Name: "development"}))
				Expect(documentManageBinding.Spec.DocumentManagement.Name).To(Equal(toolName))

				documentManageBinding, err = devopsClient.DevopsV1alpha1().DocumentManagementBindings(GROUP2_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(documentManageBinding.Name).To(Equal(name))
				Expect(documentManageBinding.Namespace).To(Equal(GROUP2_NS))
				Expect(len(documentManageBinding.Spec.DocumentManagementSpaceRefs)).To(Equal(1))
				Expect(documentManageBinding.Spec.DocumentManagementSpaceRefs).To(ContainElement(devopsv1alpha1.DocumentManagementSpaceRef{Name: "alaudakb"}))
				Expect(documentManageBinding.Spec.DocumentManagement.Name).To(Equal(toolName))
			})
		})

		// we cannot depend alauda sonarqube online, so Pending it, you can execute on local development
		PContext("When tool kind is CodeQualityTool", func() {
			JustBeforeEach(func() {
				toolDependencyFile = "toolbindingreplica/toolbindingreplica.sonarqube-dependency.yaml"
				toolDependencyFile = makeFile(toolDependencyFile, toolbindingreplicaconfigData)
				kubectlApply(toolDependencyFile, true)

				toolBindingReplicaFile = "toolbindingreplica/toolbindingreplica.sonarqube.yaml"
				toolBindingReplicaFile = makeFile(toolBindingReplicaFile, toolbindingreplicaconfigData)
				kubectlApply(toolBindingReplicaFile, true)
			})

			It("Get ToolBindingReplica successfully", func() {
				Eventually(eventuallyFunc(PROJECT_NS, name, string(devopsv1alpha1.ServiceStatusPhaseReady)), time.Second*20).Should(Succeed())
				toolbindingreplica, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(len(toolbindingreplica.Status.Conditions)).To(Equal(4))

				codeQualityToolBinding, err := devopsClient.DevopsV1alpha1().CodeQualityBindings(GROUP1_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(codeQualityToolBinding.Name).To(Equal(name))
				Expect(codeQualityToolBinding.Namespace).To(Equal(GROUP1_NS))

				Expect(codeQualityToolBinding.Spec.CodeQualityTool.Name).To(Equal(toolName))

				codeQualityToolBinding, err = devopsClient.DevopsV1alpha1().CodeQualityBindings(GROUP2_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(codeQualityToolBinding.Name).To(Equal(name))
				Expect(codeQualityToolBinding.Namespace).To(Equal(GROUP2_NS))
				Expect(codeQualityToolBinding.Spec.CodeQualityTool.Name).To(Equal(toolName))
			})
		})

		PContext("When tool kind is CodeRepoService", func() {
			JustBeforeEach(func() {
				toolDependencyFile = "toolbindingreplica/toolbindingreplica.github-dependency.yaml"
				toolDependencyFile = makeFile(toolDependencyFile, toolbindingreplicaconfigData)
				kubectlApply(toolDependencyFile, true)

				toolBindingReplicaFile = "toolbindingreplica/toolbindingreplica.github.yaml"
				toolBindingReplicaFile = makeFile(toolBindingReplicaFile, toolbindingreplicaconfigData)
				kubectlApply(toolBindingReplicaFile, true)

			})

			It("Get ToolBindingReplica successfully", func() {

				Eventually(eventuallyFunc(PROJECT_NS, name, string(devopsv1alpha1.ServiceStatusPhaseReady)), time.Second*20).Should(Succeed())
				toolbindingreplica, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(toolbindingreplica.Name).To(Equal(name))
				Expect(toolbindingreplica.Annotations[devopsv1alpha1.AnnotationsSecretType]).To(Equal(string(corev1.SecretTypeBasicAuth)))
				Expect(len(toolbindingreplica.Status.Conditions)).To(Equal(4))
				Expect(
					toolbindingreplica.Status.Conditions[2].Namespace == GROUP1_NS || toolbindingreplica.Status.Conditions[2].Namespace == GROUP2_NS).To(BeTrue())
				Expect(toolbindingreplica.Status.Conditions[2].Name).To(Equal(name))
				Expect(toolbindingreplica.Status.Conditions[3].Namespace == GROUP1_NS || toolbindingreplica.Status.Conditions[3].Namespace == GROUP2_NS).To(BeTrue())
				Expect(toolbindingreplica.Status.Conditions[3].Name).To(Equal(name))

				coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP1_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(coderepobinding.Name).To(Equal(name))
				Expect(coderepobinding.Namespace).To(Equal(GROUP1_NS))
				Expect(len(coderepobinding.Spec.Account.Owners)).To(Equal(2))

				coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP2_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(coderepobinding.Name).To(Equal(name))
				Expect(coderepobinding.Namespace).To(Equal(GROUP2_NS))
				Expect(len(coderepobinding.Spec.Account.Owners)).To(Equal(1))
				Expect(coderepobinding.Spec.Account.Owners[0].Name).To(Equal("chengjingtao"))
			})

			It("Update ToolBindingReplica alauda.io/description successfully", func() {

				RetryExecution(func() error {
					updateToolBindingReplica, err := devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Get(name, v1.GetOptions{})
					updateToolBindingReplica = updateToolBindingReplica.DeepCopy()
					updateToolBindingReplica.Annotations["alauda.io/description"] = "update"
					_, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Update(updateToolBindingReplica)
					return err
				})

				toolbindingreplica, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(toolbindingreplica.Annotations["alauda.io/description"]).To(Equal("update"))
			})

			It("Update ToolBindingReplica successfully when add one group", func() {

				file_namespace = "toolbindingreplica/namespace.yaml"

				//增加组之前，需要先创建一个namespace
				file_namespace = makeFile(file_namespace, map[string]string{
					"{{GROUP3_NS}}":  GROUP3_NS,
					"{{PROJECT_NS}}": PROJECT_NS,
				})
				kubectlApply(file_namespace)

				RetryExecution(func() error {
					group := devopsv1alpha1.CodeRepoBindingTemplate{
						Selector: devopsv1alpha1.BindingReplicaNamespaceSelector{
							NamespacesRef: []string{GROUP3_NS},
						},
						Spec: devopsv1alpha1.CodeRepoBindingSpec{
							CodeRepoService: devopsv1alpha1.LocalObjectReference{
								Name: "",
							},
							Account: devopsv1alpha1.CodeRepoBindingAccount{
								Owners: []devopsv1alpha1.CodeRepositoryOwnerSync{
									devopsv1alpha1.CodeRepositoryOwnerSync{Type: "test"},
								},
							},
						},
					}
					updateToolBindingReplica, err := devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Get(name, v1.GetOptions{})
					updateToolBindingReplica = updateToolBindingReplica.DeepCopy()

					//group1加入原有的组
					updateToolBindingReplica.Spec.CodeRepoService.Template.Bindings = append(
						updateToolBindingReplica.Spec.CodeRepoService.Template.Bindings, group,
					)
					_, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Update(updateToolBindingReplica)
					return err
				})
				Eventually(eventuallyFunc(PROJECT_NS, name, string(devopsv1alpha1.ServiceStatusPhaseReady)), time.Second*15).Should(Succeed())

				bindingReplica, err := devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Get(name, v1.GetOptions{})

				Expect(err).To(BeNil())
				Expect(bindingReplica.Name).To(Equal(name))

				codeRepoBinding, err := devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP1_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(codeRepoBinding.Name).To(Equal(name))
				Expect(codeRepoBinding.Namespace).To(Equal(GROUP1_NS))

				codeRepoBinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP2_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(codeRepoBinding.Name).To(Equal(name))
				Expect(codeRepoBinding.Namespace).To(Equal(GROUP2_NS))

				codeRepoBinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP3_NS).Get(name, v1.GetOptions{})

				Expect(err).To(BeNil())
				Expect(codeRepoBinding.Name).To(Equal(name))
				Expect(codeRepoBinding.Namespace).To(Equal(GROUP3_NS))
				Expect(len(codeRepoBinding.Spec.Account.Owners)).To(Equal(2))

				commandP("kubectl", "delete", "ns", GROUP3_NS)
			})

			It("Update ToolBindingReplica successfully when delete one group", func() {
				RetryExecution(func() error {
					//获取绑定的组
					outputDetail, _ := command("kubectl", "get", "toolbindingreplica", name, "-o", "json", "-n", PROJECT_NS)
					tbrObj := devopsv1alpha1.ToolBindingReplica{}
					err := json.Unmarshal(outputDetail, &tbrObj)
					Expect(err).To(BeNil())
					group := tbrObj.Spec.CodeRepoService.Template.Bindings
					//只使用group[0]
					group1 := group[0]
					toolbindingreplica, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Get(name, v1.GetOptions{})
					toolbindingreplica = toolbindingreplica.DeepCopy()
					toolbindingreplica.Spec.CodeRepoService.Template.Bindings = []devopsv1alpha1.CodeRepoBindingTemplate{group1}
					_, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Update(toolbindingreplica)
					return err
				})
				Eventually(eventuallyFunc(PROJECT_NS, name, string(devopsv1alpha1.ServiceStatusPhaseReady)), time.Second*15).Should(Succeed())

				toolbindingreplica, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(toolbindingreplica.Name).To(Equal(name))

				coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP1_NS).Get(name, v1.GetOptions{})
				Expect(err).To(BeNil())
				Expect(coderepobinding.Spec.Account.Owners[0].Name).To(Equal("jtcheng-org"))

				coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP2_NS).Get(name, v1.GetOptions{})
				Expect(err).ToNot(BeNil())
			})

			It("List toolBindingReplica should contain "+name, func() {
				toolbindingreplicaList, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).List(v1.ListOptions{})
				Expect(err).To(BeNil())
				Expect(toolbindingreplicaList).ToNot(BeNil())
				Expect(toolbindingreplicaList.Items).ToNot(BeEmpty())
				found := false
				for _, p := range toolbindingreplicaList.Items {
					if p.Name == name {
						found = true
						break
					}
				}
				Expect(found).To(BeTrue())
			})

			It("Delete toolBindingReplica successfully", func() {

				err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Delete(name, nil)
				Expect(err).To(BeNil())
				timer := time.NewTimer(time.Second * 30)
				for {
					select {
					case <-timer.C:
						Fail("timeout")
						return
					default:
						_, err = k8sClient.CoreV1().Namespaces().Get(name, v1.GetOptions{})
						if err != nil {
							namespacelist, _ := k8sClient.CoreV1().Namespaces().List(v1.ListOptions{})
							if !strings.Contains(namespacelist.String(), name) {
								Succeed()
								return
							}
						}
						time.Sleep(time.Second * 1)
					}
				}
			})
		})

	})

	PContext("Binding fail when incorrect secret", func() {
		BeforeEach(func() {
			timestamp = time.Now().UnixNano()
			name = "e2etoolbindingreplica-" + fmt.Sprintf("%v", timestamp)
			toolName = "github"
			PROJECT_NS = "alaudatest" + fmt.Sprintf("%v", timestamp)
			GROUP1_NS = "alaudatest-dev" + fmt.Sprintf("%v", timestamp)
			GROUP2_NS = "alaudatest-ops" + fmt.Sprintf("%v", timestamp)

		})
		JustBeforeEach(func() {
			dependencyFile = "toolbindingreplica/toolbindingreplica-error.github-dependency.yaml"
			file_error = "toolbindingreplica/toolbindingreplica-error.github.yaml"
			toolbindingreplicaconfigData = map[string]string{
				"{{PROJECT_NS}}": PROJECT_NS,
				"{{GROUP1_NS}}":  GROUP1_NS,
				"{{GROUP2_NS}}":  GROUP2_NS,
				"{{TOOL_NAME}}":  toolName,
				"{{NAME}}":       name,
				"{{TIME}}":       fmt.Sprint(timestamp),
			}
			dependencyFile = makeFile(dependencyFile, toolbindingreplicaconfigData)
			kubectlApply(dependencyFile, true)
			Eventually(eventuallyNamespaceReadyFunc(PROJECT_NS, GROUP1_NS, GROUP2_NS), time.Second*5).Should(Succeed())
			file_error = makeFile(file_error, toolbindingreplicaconfigData)
			kubectlApply(file_error, true)
		})

		AfterEach(func() {
			commandP("kubectl", "delete", "-f", file_error)
			commandP("kubectl", "delete", "-f", dependencyFile)
		})
		It("Binding fail when incorrect secret", func() {
			output, err = kubectlApply(file_error, true)
			Expect(err).ToNot(BeNil())
			Expect(trim(output)).ToNot(BeNil())
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP1_NS).Get(name, v1.GetOptions{})

			Expect(err).ToNot(BeNil())
			Expect(coderepobinding.Name).To(Equal(``))

			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP2_NS).Get(name, v1.GetOptions{})
			Expect(err).ToNot(BeNil())
			Expect(coderepobinding.Name).To(Equal(``))
		})
	})

	PContext("Bind multiple groups successfully", func() {
		BeforeEach(func() {
			timestamp = time.Now().UnixNano()
			name = "e2etoolbindingreplica-" + fmt.Sprintf("%v", timestamp)
			PROJECT_NS = "alaudatest" + fmt.Sprintf("%v", timestamp)
			GROUP1_NS = "alaudatest-dev" + fmt.Sprintf("%v", timestamp)
			GROUP2_NS = "alaudatest-ops" + fmt.Sprintf("%v", timestamp)
			GROUP3_NS = "alaudatest-test" + fmt.Sprintf("%v", timestamp)
			GROUP4_NS = "alaudatest-sale" + fmt.Sprintf("%v", timestamp)
			GROUP5_NS = "alaudatest-bank" + fmt.Sprintf("%v", timestamp)
			GROUP6_NS = "alaudatest-mark" + fmt.Sprintf("%v", timestamp)
			GROUP7_NS = "alaudatest-liyi" + fmt.Sprintf("%v", timestamp)
			toolName = "github"
		})
		JustBeforeEach(func() {
			dependencyFile = "toolbindingreplica/toolbindingreplica-multigroup.github-dependency.yaml"
			file_group = "toolbindingreplica/toolbindingreplica-multigroup.github.yaml"
			toolbindingreplicaconfigData = map[string]string{
				"{{PROJECT_NS}}": PROJECT_NS,
				"{{GROUP1_NS}}":  GROUP1_NS,
				"{{GROUP2_NS}}":  GROUP2_NS,
				"{{GROUP3_NS}}":  GROUP3_NS,
				"{{GROUP4_NS}}":  GROUP4_NS,
				"{{GROUP5_NS}}":  GROUP5_NS,
				"{{GROUP6_NS}}":  GROUP6_NS,
				"{{GROUP7_NS}}":  GROUP7_NS,
				"{{TOOL_NAME}}":  toolName,
				"{{NAME}}":       name,
				"{{TIME}}":       fmt.Sprint(timestamp),
			}
			dependencyFile = makeFile(dependencyFile, toolbindingreplicaconfigData)
			kubectlApply(dependencyFile, true)
			Eventually(eventuallyNamespaceReadyFunc(PROJECT_NS, GROUP1_NS, GROUP2_NS, GROUP3_NS, GROUP4_NS, GROUP5_NS, GROUP6_NS, GROUP7_NS), time.Second*5).Should(Succeed())
			Eventually(func() error {
				secretName := "chengjingtao-github-" + fmt.Sprintf("%v", timestamp)
				secret, errSecret := k8sClient.CoreV1().Secrets(PROJECT_NS).Get(secretName, v1.GetOptions{})
				if errSecret != nil {
					return errSecret
				}
				if secret == nil {
					return fmt.Errorf("secret %s is not created ready", secretName)
				}
				return nil
			}, time.Second*5)
			file_group = makeFile(file_group, toolbindingreplicaconfigData)
			kubectlApply(file_group, true)
			Eventually(eventuallyFunc(PROJECT_NS, name, string(devopsv1alpha1.ServiceStatusPhaseReady)), time.Second*10).Should(Succeed())
		})

		AfterEach(func() {
			commandP("kubectl", "delete", "-f", file_group)
			commandP("kubectl", "delete", "-f", dependencyFile)
		})
		It("Bind multiple groups successfully, random check", func() {
			toolbindingreplica, err = devopsClient.DevopsV1alpha1().ToolBindingReplicas(PROJECT_NS).Get(name, v1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(toolbindingreplica.Name).To(Equal(name))

			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP1_NS).Get(name, v1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(coderepobinding.Name).To(Equal(name))
			Expect(coderepobinding.Namespace).To(Equal(GROUP1_NS))

			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP3_NS).Get(name, v1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(coderepobinding.Name).To(Equal(name))
			Expect(coderepobinding.Namespace).To(Equal(GROUP3_NS))

			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(GROUP7_NS).Get(name, v1.GetOptions{})
			Expect(err).To(BeNil())
			Expect(coderepobinding.Name).To(Equal(name))
			Expect(coderepobinding.Namespace).To(Equal(GROUP7_NS))

		})
	})

})

//在timeoutSeconds时间内，获取ToolBindingReplica的状态
func eventuallyFunc(namespace, name, phase string) func() error {
	return func() error {
		toolbindingreplica, err := devopsClient.DevopsV1alpha1().ToolBindingReplicas(namespace).Get(name, v1.GetOptions{})
		if err != nil || toolbindingreplica.Status.Phase.Is(devopsv1alpha1.ServiceStatusPhaseReady) {
			return err
		}
		Expect(toolbindingreplica).ToNot(BeNil())
		if string(toolbindingreplica.Status.Phase) != phase {
			err = fmt.Errorf("Not in status yet: %s, now is %v , \n status.Conditions: %#v \ntoolbindingreplica is %#v ", phase, toolbindingreplica.Status.Phase, toolbindingreplica.Status.Conditions, toolbindingreplica)
		}
		return err
	}
}

func eventuallyNamespaceReadyFunc(names ...string) func() error {
	return func() error {
		for _, name := range names {
			namespace, err := k8sClient.CoreV1().Namespaces().Get(name, v1.GetOptions{})
			if err != nil {
				return err
			}
			if namespace == nil {
				return fmt.Errorf("Namespace %s is not created ready", name)
			}
		}

		return nil
	}
}
