package v1alpha1_test

import (
	"fmt"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func getImageRepositoryList(ProjectName string, ImageRegistryName string) (repositorylist *v1alpha1.ImageRepositoryList, err error, found bool) {
	RegisterFailHandler(Fail)
	timer := time.NewTimer(time.Minute * 3)
	for {
		repositorylist, err = devopsClient.DevopsV1alpha1().ImageRepositories(ProjectName).List(v1.ListOptions{})
		println("getImageRepositoryList size: ", len(repositorylist.Items))
		select {
		case <-timer.C:
			found = false
			return
		default:
			for _, p := range repositorylist.Items {
				if strings.Contains(p.Name, ImageRegistryName) {
					found = true
					return
				}
			}
			time.Sleep(time.Second * 10)
		}
	}
}

func getImageRegistryNameByHost(host string) (name string, found bool) {
	imageregistrylist, _ := devopsClient.DevopsV1alpha1().ImageRegistries().List(v1.ListOptions{})
	found = false
	name = ""
	for _, p := range imageregistrylist.Items {
		if string(p.Spec.HTTP.Host) == host {
			found = true
			name = p.Name
			break
		}
	}
	return
}

var _ = DescribeM("ImageRegistry docker", func() {
	var (
		imageregistry                              *v1alpha1.ImageRegistry
		imageregistrylist                          *v1alpha1.ImageRegistryList
		ImageRegistryName, ImageRegistryHost, file string
		ImageRegistryData                          map[string]string
		timestamp                                  int64
		err                                        error
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		ImageRegistryName = "e2eimageregistrydocker" + fmt.Sprintf("%v", timestamp)
		ImageRegistryHost = "http://index-staging.alauda.cn" + fmt.Sprintf("%v", timestamp)
		ImageRegistryData = map[string]string{
			"ImageRegistryName": ImageRegistryName,
			"ImageRegistryHost": ImageRegistryHost,
		}
		file = "registry/imageregistry.yaml"
		file = makeFile(file, ImageRegistryData)
		kubectlApply(file, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", file)
	})
	DescribeF("get ImageRegistryList", func() {
		JustBeforeEach(func() {
			imageregistrylist, err = devopsClient.DevopsV1alpha1().ImageRegistries().List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(imageregistrylist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range imageregistrylist.Items {
				if p.Name == ImageRegistryName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("update ImageRegistry", func() {
		JustBeforeEach(func() {
			imageregistry, err = devopsClient.DevopsV1alpha1().ImageRegistries().Get(ImageRegistryName, v1.GetOptions{})
			imageregistry = imageregistry.DeepCopy()
			imageregistry.Spec.HTTP.Host = "http://update.com"
			imageregistry, err = devopsClient.DevopsV1alpha1().ImageRegistries().Update(imageregistry)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(string(imageregistry.Spec.HTTP.Host)).To(Equal("http://update.com"))
		})
	})
	DescribeF("get ImageRegistry detail", func() {
		JustBeforeEach(func() {
			imageregistry, err = devopsClient.DevopsV1alpha1().ImageRegistries().Get(ImageRegistryName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(string(imageregistry.Spec.Type)).To(ContainSubstring("Docker"))
		})
	})
	DescribeF("delete  ImageRegistry ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().ImageRegistries().Delete(ImageRegistryName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("ImageRegistryBinding http", func() {
	var (
		imageregistrybinding                                                        *v1alpha1.ImageRegistryBinding
		imageregistrybindinglist                                                    *v1alpha1.ImageRegistryBindingList
		bindingrepo                                                                 *v1alpha1.ImageRegistryBindingRepositories
		repositorylist                                                              *v1alpha1.ImageRepositoryList
		ImageRegistryName, ImageRegistryHost, ImageRegistryBindingName, bindingfile string
		ProjectName, SecretName, SecretUserName, SecretPassWord                     string
		ImageRegistryBindingData                                                    map[string]string
		timestamp                                                                   int64
		err                                                                         error
		found                                                                       bool
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		ImageRegistryName = "e2eimageregistryhttp" + fmt.Sprintf("%v", timestamp)
		ImageRegistryHost = httpImageRegistryHost + fmt.Sprintf("%v", timestamp)
		ImageRegistryBindingName = "e2eimageregistrybindinghttp" + fmt.Sprintf("%v", timestamp)
		ProjectName = "e2eprojectforimageregistrybindinghttp" + fmt.Sprintf("%v", timestamp)
		SecretName = "e2esecretforimageregistrybindinghttp" + fmt.Sprintf("%v", timestamp)
		SecretUserName = httpSecretUserName
		SecretPassWord = httpSecretUserName
	})
	JustBeforeEach(func() {
		ImageRegistryBindingData = map[string]string{
			"ImageRegistryName":        ImageRegistryName,
			"ImageRegistryHost":        ImageRegistryHost,
			"ImageRegistryBindingName": ImageRegistryBindingName,
			"ProjectName":              ProjectName,
			"SecretName":               SecretName,
			"SecretUserName":           SecretUserName,
			"SecretPassWord":           SecretPassWord,
		}
		bindingfile = "registry/imageregistrybinding-http.yaml"
		bindingfile = makeFile(bindingfile, ImageRegistryBindingData)
		kubectlApply(bindingfile, true)

	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", bindingfile)
	})

	DescribeF("get ImageRegistryBindingList", func() {
		JustBeforeEach(func() {
			imageregistrybindinglist, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(imageregistrybindinglist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range imageregistrybindinglist.Items {
				if p.Name == ImageRegistryBindingName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("update ImageRegistryBinding", func() {
		JustBeforeEach(func() {
			imageregistrybinding, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).Get(ImageRegistryBindingName, v1.GetOptions{})
			imageregistrybinding = imageregistrybinding.DeepCopy()
			imageregistrybinding.Spec.RepoInfo.Repositories = []string{"alaudaorg", "alauda"}
			imageregistrybinding, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).Update(imageregistrybinding)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(imageregistrybinding.Spec.RepoInfo.Repositories)).To(Equal(2))
		})
	})
	DescribeF("get ImageRegistryBinding detail", func() {
		JustBeforeEach(func() {
			imageregistrybinding, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).Get(ImageRegistryBindingName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(imageregistrybinding.Spec.ImageRegistry.Name).To(Equal(ImageRegistryName))
			Expect(imageregistrybinding.Spec.Secret.Name).To(ContainSubstring(SecretName))
		})
	})
	SDescribeF("get ImageRegistryBinding imagerepos", func() {
		BeforeEach(func() {
			ImageRegistryHost = httpImageRegistryHost
			name, found := getImageRegistryNameByHost(ImageRegistryHost)
			if found == true {
				ImageRegistryName = name
			}
		})
		JustBeforeEach(func() {
			bindingrepo, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).GetImageRepos(ImageRegistryBindingName)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(bindingrepo.Items)).To(BeNumerically(">", 0))
		})
	})
	SDescribeF("get imagerepository list", func() {
		BeforeEach(func() {
			ImageRegistryHost = httpImageRegistryHost
			name, found := getImageRegistryNameByHost(ImageRegistryHost)
			if found == true {
				ImageRegistryName = name
			}
		})
		JustBeforeEach(func() {
			repositorylist, err, found = getImageRepositoryList(ProjectName, ImageRegistryName)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(repositorylist.Items).NotTo(BeEmpty())
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("delete  ImageRegistryBinding ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).Delete(ImageRegistryBindingName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("ImageRegistryBinding auth", func() {
	var (
		repositorylist                                                                      *v1alpha1.ImageRepositoryList
		imagerepository                                                                     *v1alpha1.ImageRepository
		ImageRegistryName, ImageRegistryHost, ImageRegistryBindingName, imagerepositoryName string
		ProjectName, SecretName, SecretUserName, SecretPassWord, bindingfile                string
		ImageRegistryBindingData                                                            map[string]string
		timestamp                                                                           int64
		err                                                                                 error
		found                                                                               bool
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		ImageRegistryName = "e2eimageregistryauth" + fmt.Sprintf("%v", timestamp)
		ImageRegistryHost = authImageRegistryHost + fmt.Sprintf("%v", timestamp)
		ImageRegistryBindingName = "e2eimageregistrybindingauth" + fmt.Sprintf("%v", timestamp)
		ProjectName = "e2eprojectforimageregistrybindingauth" + fmt.Sprintf("%v", timestamp)
		SecretName = "e2esecretforimageregistrybindingauth" + fmt.Sprintf("%v", timestamp)
		SecretUserName = authSecretUserName
		SecretPassWord = authSecretPassWord
		imagerepositoryName = ImageRegistryName + "-alauda-darchrow"
	})
	JustBeforeEach(func() {
		ImageRegistryBindingData = map[string]string{
			"ImageRegistryName":        ImageRegistryName,
			"ImageRegistryHost":        ImageRegistryHost,
			"ImageRegistryBindingName": ImageRegistryBindingName,
			"ProjectName":              ProjectName,
			"SecretName":               SecretName,
			"SecretUserName":           SecretUserName,
			"SecretPassWord":           SecretPassWord,
		}
		bindingfile = "registry/imageregistrybinding-auth.yaml"
		bindingfile = makeFile(bindingfile, ImageRegistryBindingData)
		kubectlApply(bindingfile, true)
		repositorylist, err, found = getImageRepositoryList(ProjectName, ImageRegistryName)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", bindingfile)
	})

	SDescribeF("get imagerepository list", func() {
		BeforeEach(func() {
			ImageRegistryHost = authImageRegistryHost
			name, found := getImageRegistryNameByHost(ImageRegistryHost)
			if found == true {
				ImageRegistryName = name
			}
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(repositorylist.Items).NotTo(BeEmpty())
			Expect(found).To(BeTrue())
		})
	})
	SDescribeF("get imagerepository detail", func() {
		BeforeEach(func() {
			ImageRegistryHost = authImageRegistryHost
			name, found := getImageRegistryNameByHost(ImageRegistryHost)
			if found == true {
				ImageRegistryName = name
			}
		})
		JustBeforeEach(func() {
			imagerepository, err = devopsClient.DevopsV1alpha1().ImageRepositories(ProjectName).Get(imagerepositoryName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(imagerepository.Spec.ImageRegistryBinding.Name).To(Equal(ImageRegistryBindingName))
		})
	})

	SDescribeF("get imagerepository tags", func() {
		BeforeEach(func() {
			ImageRegistryHost = authImageRegistryHost
			name, found := getImageRegistryNameByHost(ImageRegistryHost)
			if found == true {
				ImageRegistryName = name
			}
		})
		JustBeforeEach(func() {
			imagerepository, err = devopsClient.DevopsV1alpha1().ImageRepositories(ProjectName).Get(imagerepositoryName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(imagerepository.Status.Tags)).To(BeNumerically(">", 0))
		})
	})

	SDescribeF("delete imagerepository ", func() {
		BeforeEach(func() {
			ImageRegistryHost = authImageRegistryHost
			name, found := getImageRegistryNameByHost(ImageRegistryHost)
			if found == true {
				ImageRegistryName = name
			}
		})
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().ImageRepositories(ProjectName).Delete(imagerepositoryName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("ImageRegistry harbor", func() {
	var (
		imageregistry                              *v1alpha1.ImageRegistry
		imageregistrylist                          *v1alpha1.ImageRegistryList
		ImageRegistryName, ImageRegistryHost, file string
		ImageRegistryData                          map[string]string
		timestamp                                  int64
		err                                        error
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		ImageRegistryName = "e2eimageregistryharbor" + fmt.Sprintf("%v", timestamp)
		ImageRegistryHost = "http://www.myalauda.com:31413" + fmt.Sprintf("%v", timestamp)
		ImageRegistryData = map[string]string{
			"ImageRegistryName": ImageRegistryName,
			"ImageRegistryHost": ImageRegistryHost,
		}
		file = "registry/imageregistry-harbor.yaml"
		file = makeFile(file, ImageRegistryData)
		kubectlApply(file, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", file)
	})
	DescribeF("get ImageRegistryList", func() {
		JustBeforeEach(func() {
			imageregistrylist, err = devopsClient.DevopsV1alpha1().ImageRegistries().List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(imageregistrylist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range imageregistrylist.Items {
				if p.Name == ImageRegistryName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("update ImageRegistry", func() {
		JustBeforeEach(func() {
			imageregistry, err = devopsClient.DevopsV1alpha1().ImageRegistries().Get(ImageRegistryName, v1.GetOptions{})
			imageregistry = imageregistry.DeepCopy()
			imageregistry.Spec.HTTP.Host = "http://update.com"
			imageregistry, err = devopsClient.DevopsV1alpha1().ImageRegistries().Update(imageregistry)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(string(imageregistry.Spec.HTTP.Host)).To(Equal("http://update.com"))
		})
	})
	DescribeF("get ImageRegistry detail", func() {
		JustBeforeEach(func() {
			imageregistry, err = devopsClient.DevopsV1alpha1().ImageRegistries().Get(ImageRegistryName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(string(imageregistry.Spec.Type)).To(ContainSubstring("Harbor"))
		})
	})
	DescribeF("delete  ImageRegistry ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().ImageRegistries().Delete(ImageRegistryName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("ImageRegistryBinding harbor", func() {
	var (
		imageregistrybinding                                                         *v1alpha1.ImageRegistryBinding
		imageregistrybindinglist                                                     *v1alpha1.ImageRegistryBindingList
		imagerepository                                                              *v1alpha1.ImageRepository
		bindingrepo                                                                  *v1alpha1.ImageRegistryBindingRepositories
		repositorylist                                                               *v1alpha1.ImageRepositoryList
		ImageRegistryName, ImageRegistryHost, ImageRegistryBindingName, bindingfile  string
		ProjectName, SecretName, SecretUserName, SecretPassWord, imagerepositoryName string
		ImageRegistryBindingData                                                     map[string]string
		timestamp                                                                    int64
		err                                                                          error
		found                                                                        bool
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		ImageRegistryName = "e2eimageregistryharbor" + fmt.Sprintf("%v", timestamp)
		ImageRegistryHost = harborImageRegistryHost + fmt.Sprintf("%v", timestamp)
		ImageRegistryBindingName = "e2eimageregistrybindingharbor" + fmt.Sprintf("%v", timestamp)
		ProjectName = "e2eprojectforimageregistrybindingharbor" + fmt.Sprintf("%v", timestamp)
		SecretName = "e2esecretforimageregistrybindingharbor" + fmt.Sprintf("%v", timestamp)
		SecretUserName = harborSecretUserName
		SecretPassWord = harborSecretPassWord
		imagerepositoryName = ImageRegistryName + "-alaudaorg-darchrow"
	})
	JustBeforeEach(func() {
		ImageRegistryBindingData = map[string]string{
			"ImageRegistryName":        ImageRegistryName,
			"ImageRegistryHost":        ImageRegistryHost,
			"ImageRegistryBindingName": ImageRegistryBindingName,
			"ProjectName":              ProjectName,
			"SecretName":               SecretName,
			"SecretUserName":           SecretUserName,
			"SecretPassWord":           SecretPassWord,
		}
		bindingfile = "registry/imageregistrybinding-harbor.yaml"
		bindingfile = makeFile(bindingfile, ImageRegistryBindingData)
		kubectlApply(bindingfile, true)

	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", bindingfile)
	})

	DescribeF("get ImageRegistryBindingList", func() {
		JustBeforeEach(func() {
			imageregistrybindinglist, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(imageregistrybindinglist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range imageregistrybindinglist.Items {
				if p.Name == ImageRegistryBindingName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("update ImageRegistryBinding", func() {
		JustBeforeEach(func() {
			imageregistrybinding, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).Get(ImageRegistryBindingName, v1.GetOptions{})
			imageregistrybinding = imageregistrybinding.DeepCopy()
			imageregistrybinding.Spec.RepoInfo.Repositories = []string{"alaudaorg", "library"}
			imageregistrybinding, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).Update(imageregistrybinding)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(imageregistrybinding.Spec.RepoInfo.Repositories)).To(Equal(2))
		})
	})
	DescribeF("get ImageRegistryBinding detail", func() {
		JustBeforeEach(func() {
			imageregistrybinding, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).Get(ImageRegistryBindingName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(imageregistrybinding.Spec.ImageRegistry.Name).To(Equal(ImageRegistryName))
			Expect(imageregistrybinding.Spec.Secret.Name).To(Equal(SecretName))
		})
	})

	DescribeF("delete  ImageRegistryBinding ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).Delete(ImageRegistryBindingName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})

	SDescribeF("get ImageRegistryBinding imagerepos", func() {
		BeforeEach(func() {
			ImageRegistryHost = harborImageRegistryHost
			name, found := getImageRegistryNameByHost(ImageRegistryHost)
			if found == true {
				ImageRegistryName = name
			}
		})
		JustBeforeEach(func() {
			bindingrepo, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(ProjectName).GetImageRepos(ImageRegistryBindingName)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(bindingrepo.Items)).To(BeNumerically(">", 0))
		})
	})

	SDescribeF("get ImageRepositories list", func() {
		BeforeEach(func() {
			ImageRegistryHost = harborImageRegistryHost
			name, found := getImageRegistryNameByHost(ImageRegistryHost)
			if found == true {
				ImageRegistryName = name
			}
		})
		JustBeforeEach(func() {
			repositorylist, err, found = getImageRepositoryList(ProjectName, ImageRegistryName)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(repositorylist.Items).NotTo(BeEmpty())
			Expect(found).To(BeTrue())
		})
	})

	SDescribeF("get ImageRepositories detail", func() {
		BeforeEach(func() {
			ImageRegistryHost = harborImageRegistryHost
			name, found := getImageRegistryNameByHost(ImageRegistryHost)
			if found == true {
				ImageRegistryName = name
			}
		})
		JustBeforeEach(func() {
			getImageRepositoryList(ProjectName, ImageRegistryName)
			imagerepository, err = devopsClient.DevopsV1alpha1().ImageRepositories(ProjectName).Get(imagerepositoryName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(imagerepository.Spec.ImageRegistryBinding.Name).To(Equal(ImageRegistryBindingName))
		})
	})

	SDescribeF("get ImageRepositories tags", func() {
		BeforeEach(func() {
			ImageRegistryHost = harborImageRegistryHost
			name, found := getImageRegistryNameByHost(ImageRegistryHost)
			if found == true {
				ImageRegistryName = name
			}
		})
		JustBeforeEach(func() {
			getImageRepositoryList(ProjectName, ImageRegistryName)
			imagerepository, err = devopsClient.DevopsV1alpha1().ImageRepositories(ProjectName).Get(imagerepositoryName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(imagerepository.Status.Tags)).To(BeNumerically(">", 0))
		})
	})

	SDescribeF("delete ImageRepositories ", func() {
		BeforeEach(func() {
			ImageRegistryHost = harborImageRegistryHost
			name, found := getImageRegistryNameByHost(ImageRegistryHost)
			if found == true {
				ImageRegistryName = name
			}
		})
		JustBeforeEach(func() {
			getImageRepositoryList(ProjectName, ImageRegistryName)
			err = devopsClient.DevopsV1alpha1().ImageRepositories(ProjectName).Delete(imagerepositoryName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})
