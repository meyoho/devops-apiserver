package v1alpha1_test

import (
	"encoding/json"
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// var jenkinsLister listers.JenkinsLister
// var name string

func getJenkinsStatus(jenkinsName string, expectStatus string) (result string) {
	RegisterFailHandler(Fail)
	timer := time.NewTimer(time.Second * 60)
	for {
		select {
		case <-timer.C:
			result = "timeout"
			return result
		default:
			outputDetail, _ := commandP("kubectl", "get", "jenkins", jenkinsName, "-o", "json")
			jenkinsObj := v1alpha1.Jenkins{}
			err := json.Unmarshal(outputDetail, &jenkinsObj)
			Expect(err).To(BeNil())
			status := string(jenkinsObj.Status.Phase)
			if status == expectStatus {
				result = expectStatus
				return result
			}
			time.Sleep(time.Second * 1)
		}
	}
}

func getJenkinsbindingStatus(jenkinsbindingName string, namespaceName string, expectStatus string) (result string) {
	RegisterFailHandler(Fail)
	timer := time.NewTimer(time.Second * 60)
	for {
		select {
		case <-timer.C:
			result = "timeout"
			return result
		default:
			_, err := commandP("kubectl", "get", "jenkins")
			outputDetail, _ := commandP("kubectl", "get", "jenkinsbinding", jenkinsbindingName, "-n", namespaceName, "-o", "json")
			jenkinsbindingObj := v1alpha1.JenkinsBinding{}
			err = json.Unmarshal(outputDetail, &jenkinsbindingObj)
			Expect(err).To(BeNil())
			status := string(jenkinsbindingObj.Status.Phase)
			if status == expectStatus {
				result = expectStatus
				return result
			}
			time.Sleep(time.Second * 1)
		}
	}
}

// var _ = BeforeSuite(func() {
// 	printH("BeforeSuite")
// 	if jenkinsLister == nil {
// 		jenkinsInformer := devopsInformers.Devops().V1alpha1().Jenkinses()
// 		jenkinsLister = jenkinsInformer.Lister()
// 		// printH("Will wait for sync....")
// 		// go func() {

// 		// 	for {
// 		// 		time.Sleep(time.Second)
// 		// 		synced := jenkinsInformer.Informer().HasSynced()
// 		// 		printR("Synced?", synced)
// 		// 		if synced {
// 		// 			break
// 		// 		}

// 		// 	}
// 		// }()
// 		Start()

// 		if ok := cache.WaitForCacheSync(stopChan, jenkinsInformer.Informer().HasSynced); !ok {
// 			printR("Jenkins Informer sync error")
// 		}
// 		// printH("Ready!")
// 	}
// 	// if name != "" {
// 	// 	if _, err := jenkinsLister.Get(name); err == nil {
// 	// 		devopsClient.DevopsV1alpha1().Jenkinses().Delete(name, nil)
// 	// 	}
// 	// }

// })

// var _ = AfterSuite(func() {
// 	printH("AfterSuite")
// })

// var _ = BeforeSuite(func() {
// printH("devopsInformers = ", devopsInformers)

// 	jenkinsLister = jenkinsInformer.Lister()
// 	Start()
// 	if ok := cache.WaitForCacheSync(stopChan, jenkinsInformer.Informer().HasSynced); !ok {
// 		printR("Jenkins Informer sync error")
// 	}
// })

// var _ = DescribeM("Jenkins2", func() {
// 	BeforeSuite(func() {

// 	})
// })

var _ = DescribeM("Jenkins", func() {
	var (
		// jenkinsInformer informers.JenkinsInformer
		// jenkinsLister   listers.JenkinsLister
		jenkins *v1alpha1.Jenkins
		err     error
		name    string
		host    string
		file    string
		// labelSelector labels.Selector
	)
	// BeforeEach(func() {
	// 	if jenkinsLister != nil {
	// 		return
	// 	}
	// 	jenkinsInformer = devopsInformers.Devops().V1alpha1().Jenkinses()
	// 	jenkinsLister = jenkinsInformer.Lister()
	// 	Start()
	// 	if ok := cache.WaitForCacheSync(stopChan, jenkinsInformer.Informer().HasSynced); !ok {
	// 		printR("Jenkins Informer sync error")
	// 	}

	// })

	BeforeEach(func() {
		name = "e2eTestJenkins" + fmt.Sprintf("%v", time.Now().UnixNano())
		file = "jenkins/test-jenkins.yaml"
		host = "http://127.0.0.1"
		// labelSelector = labels.Everything()
	})

	JustBeforeEach(func() {
		file = makeFile(file, map[string]string{
			"jenkinsName": name,
			"jenkinsHost": host,
		})
		kubectlApply(file, true)
	})

	AfterEach(func() {
		command("kubectl", "delete", "-f", file)
		// delErr := devopsClient.DevopsV1alpha1().Jenkinses().Delete(name, nil)
		// if delErr != nil {
		// 	printH("afterEach delete err : ", delErr)
		// }
	})

	DescribeF("Create jenkins", func() {

		JustBeforeEach(func() {
			jenkins, err = devopsClient.DevopsV1alpha1().Jenkinses().Get(name, v1.GetOptions{})
		})

		It("should not error", func() {
			Expect(err).To(BeNil())
		})
		It("should create a jenkins", func() {
			Expect(jenkins).ToNot(BeNil())
		})
	})

	DescribeF("List Jenkins", func() {
		var jenkinsList *v1alpha1.JenkinsList
		JustBeforeEach(func() {
			jenkinsList, err = devopsClient.DevopsV1alpha1().Jenkinses().List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
		It("should contain "+name, func() {
			Expect(jenkinsList).ToNot(BeNil())
			Expect(jenkinsList.Items).ToNot(BeEmpty())
			found := false
			for _, j := range jenkinsList.Items {
				if j.Name == name {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})

	})

	DescribeF("Retrieve jenkins", func() {

		JustBeforeEach(func() {
			jenkins, err = devopsClient.DevopsV1alpha1().Jenkinses().Get(name, v1.GetOptions{})
		})
		It("should retrieve jenkins with status error", func() {
			Expect(err).To(BeNil())
			Expect(jenkins).ToNot(BeNil())
			Expect(jenkins.Status.Phase).ToNot(BeEmpty())
			Expect(jenkins.Spec.HTTP.Host).ToNot(BeEmpty())
		})
	})

	DescribeF("Update jenkins", func() {

		JustBeforeEach(func() {

			// controller will update the data quite quickly so we need to try a few times
			RetryExecution(func() error {
				jenkins, err = devopsClient.DevopsV1alpha1().Jenkinses().Get(name, v1.GetOptions{})
				jenkins = jenkins.DeepCopy()
				jenkins.Spec.HTTP.Host = "http://10.0.0.1"
				jenkins, err = devopsClient.DevopsV1alpha1().Jenkinses().Update(jenkins)
				return err
			})
		})

		It("Should update", func() {
			Expect(jenkins).ToNot(BeNil())
			Expect(err).To(BeNil())
		})
	})

	DescribeF("Delete jenkins", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().Jenkinses().Delete(name, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("Jenkinsbinding", func() {

	var (
		jenkins                                                                 *v1alpha1.Jenkins
		jenkinsBinding                                                          *v1alpha1.JenkinsBinding
		secret                                                                  *corev1.Secret
		namespace                                                               *corev1.Namespace
		jenkinsErr, jenkinsBindingErr, secretErr, namespaceErr, projectErr      error
		jenkinsBindingName, namespaceName, secretName, jenkinsName, projectName string
		dependenciesFile, jenkinsBindingFile                                    string
	)

	BeforeEach(func() {
		namespaceName = "e2etestproject" + fmt.Sprintf("%v", time.Now().UnixNano())
		projectName = "e2etestproject" + fmt.Sprintf("%v", time.Now().UnixNano())
		secretName = "e2etestsecretforjenkinsbinding"
		jenkinsBindingName = "e2eTestJenkinsbinding"
		jenkinsName = "e2eTestJenkins" + fmt.Sprintf("%v", time.Now().UnixNano())
		dependenciesFile = "jenkins/test-jenkinsbinding-dependencies.yaml"
		jenkinsBindingFile = "jenkins/test-jenkinsbinding.yaml"
	})

	JustBeforeEach(func() {
		data := map[string]string{
			"namespaceName":      namespaceName,
			"projectName":        projectName,
			"secretName":         secretName,
			"jenkinsName":        jenkinsName,
			"jenkinsbindingName": jenkinsBindingName,
		}
		dependenciesFile = makeFile(dependenciesFile, data)
		kubectlApply(dependenciesFile, true)

		time.Sleep(time.Second)
		jenkinsBindingFile = makeFile(jenkinsBindingFile, data)
		kubectlApply(jenkinsBindingFile, true)

	})

	AfterEach(func() {
		command("kubectl", "delete", "-f", jenkinsBindingFile)
		command("kubectl", "delete", "-f", dependenciesFile)
	})

	DescribeF("Create jenkinsbinding", func() {
		JustBeforeEach(func() {
			jenkins, jenkinsErr = devopsClient.DevopsV1alpha1().Jenkinses().Get(jenkinsName, v1.GetOptions{})
			namespace, namespaceErr = k8sClient.CoreV1().Namespaces().Get(namespaceName, v1.GetOptions{})
			secret, secretErr = k8sClient.CoreV1().Secrets(namespaceName).Get(secretName, v1.GetOptions{})
			jenkinsBinding, jenkinsBindingErr = devopsClient.DevopsV1alpha1().JenkinsBindings(namespaceName).Get(jenkinsBindingName, v1.GetOptions{})

		})
		It("should create all resources", func() {
			Expect(jenkinsErr).To(BeNil())
			Expect(projectErr).To(BeNil())
			Expect(namespaceErr).To(BeNil())
			Expect(secretErr).To(BeNil())
			Expect(jenkinsBindingErr).To(BeNil())

			Expect(jenkins).ToNot(BeNil())
			Expect(namespace).ToNot(BeNil())
			Expect(secret).ToNot(BeNil())
			Expect(jenkinsBinding).ToNot(BeNil())
		})
	})

	DescribeF("List jenkinsbinding", func() {
		var jenkinsBindingList *v1alpha1.JenkinsBindingList
		JustBeforeEach(func() {
			jenkinsBindingList, jenkinsBindingErr = devopsClient.DevopsV1alpha1().JenkinsBindings(namespaceName).List(v1.ListOptions{})
		})
		It("should list jenkins binding", func() {
			Expect(jenkinsBindingErr).To(BeNil())
			Expect(jenkinsBindingList).ToNot(BeNil())
			Expect(jenkinsBindingList.Items).ToNot(BeNil())
			found := false
			for _, j := range jenkinsBindingList.Items {
				if j.Name == jenkinsBindingName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})

	DescribeF("update jenkinsbinding", func() {
		JustBeforeEach(func() {
			jenkinsBinding, jenkinsBindingErr = devopsClient.DevopsV1alpha1().JenkinsBindings(namespaceName).Get(jenkinsBindingName, v1.GetOptions{})
			jenkinsBinding = jenkinsBinding.DeepCopy()
			jenkinsBinding.Spec.Jenkins.Name = "not-existing-jenkins"
			_, jenkinsBindingErr = devopsClient.DevopsV1alpha1().JenkinsBindings(namespaceName).Update(jenkinsBinding)
			jenkinsBinding, _ = devopsClient.DevopsV1alpha1().JenkinsBindings(namespaceName).Get(jenkinsBindingName, v1.GetOptions{})
		})
		It("detail should not update", func() {
			Expect(jenkinsBinding).ToNot(BeNil())
			Expect(jenkinsBindingErr).ToNot(BeNil())
			Expect(jenkinsBinding.Spec.Jenkins.Name).To(Equal(jenkinsName))
		})
	})

	DescribeF("Delete jenkinsbinding", func() {
		JustBeforeEach(func() {
			jenkinsBindingErr = devopsClient.DevopsV1alpha1().JenkinsBindings(namespaceName).Delete(jenkinsBindingName, nil)
		})
		It("should not error", func() {
			Expect(jenkinsBindingErr).To(BeNil())
		})
	})

})
