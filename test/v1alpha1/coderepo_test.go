package v1alpha1_test

import (
	"fmt"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func getCodeRepositoryList(ProjectName string, CodeRepoServiceName string) (coderepositorylist *v1alpha1.CodeRepositoryList, err error, found bool) {
	RegisterFailHandler(Fail)
	timer := time.NewTimer(time.Minute * 5)
	for {
		coderepositorylist, err = devopsClient.DevopsV1alpha1().CodeRepositories(ProjectName).List(v1.ListOptions{})
		println("getCodeRepositoryList size: ", len(coderepositorylist.Items))
		select {
		case <-timer.C:
			found = false
			return
		default:
			for _, p := range coderepositorylist.Items {
				if strings.Contains(p.Name, CodeRepoServiceName) {
					found = true
					return
				}
			}
			time.Sleep(time.Second * 10)
		}
	}
}

func getCodeRepoServiceNameByHost(host string) (name string, found bool) {
	codereposervicelist, _ := devopsClient.DevopsV1alpha1().CodeRepoServices().List(v1.ListOptions{})
	found = false
	name = ""
	for _, p := range codereposervicelist.Items {
		if string(p.Spec.HTTP.Host) == host {
			found = true
			name = p.Name
			break
		}
	}
	return
}

var _ = DescribeM("CodeRepoService github", func() {
	var (
		codereposervice                                       *v1alpha1.CodeRepoService
		codereposervicelist                                   *v1alpha1.CodeRepoServiceList
		CodeRepoServiceName, CodeRepoServiceHost, servicefile string
		codeRepoData                                          map[string]string
		timestamp                                             int64
		err                                                   error
		SecretName, SecretUserName, SecretPassWord            string
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		CodeRepoServiceName = "e2ecodereposervicegithub" + fmt.Sprintf("%v", timestamp)
		CodeRepoServiceHost = "https://api.github.com" + fmt.Sprintf("%v", timestamp)
		SecretName = "e2esecretforcoderepogithub" + fmt.Sprintf("%v", timestamp)
		SecretUserName = githubSecretUserName
		SecretPassWord = githubSecretPassWord
		codeRepoData = map[string]string{
			"CodeRepoServiceName": CodeRepoServiceName,
			"CodeRepoServiceHost": CodeRepoServiceHost,
			"SecretName":          SecretName,
			"SecretUserName":      SecretUserName,
			"SecretPassWord":      SecretPassWord,
		}

		servicefile = "coderepo/codereposervice.github.yaml"
		servicefile = makeFile(servicefile, codeRepoData)
		kubectlApply(servicefile, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", servicefile)
	})
	DescribeF("get CodeRepoServiceList", func() {
		JustBeforeEach(func() {
			codereposervicelist, err = devopsClient.DevopsV1alpha1().CodeRepoServices().List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(codereposervicelist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range codereposervicelist.Items {
				if p.Name == CodeRepoServiceName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get CodeRepoService detail", func() {
		JustBeforeEach(func() {
			codereposervice, err = devopsClient.DevopsV1alpha1().CodeRepoServices().Get(CodeRepoServiceName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(string(codereposervice.Spec.Type)).To(ContainSubstring("Github"))
			Expect(codereposervice.Spec.Secret.Name).To(Equal(SecretName))
		})
	})
	DescribeF("delete  CodeRepoService ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().CodeRepoServices().Delete(CodeRepoServiceName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("CodeRepoBinding github", func() {
	var (
		coderepobinding                                                                         *v1alpha1.CodeRepoBinding
		coderepobindinglist                                                                     *v1alpha1.CodeRepoBindingList
		coderepositorylist                                                                      *v1alpha1.CodeRepositoryList
		coderepobindingrepo                                                                     *v1alpha1.CodeRepoBindingRepositories
		CodeRepoServiceName, CodeRepoServiceHost, CodeRepoBindingName, servicefile, bindingfile string
		ProjectName, SecretName, SecretUserName, SecretPassWord, AccountName, OrgName           string
		codeRepoData                                                                            map[string]string
		timestamp                                                                               int64
		err                                                                                     error
		found                                                                                   bool
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		CodeRepoServiceName = "e2ecodereposerviceforgithub" + fmt.Sprintf("%v", timestamp)
		CodeRepoServiceHost = "https://api.github.com" + fmt.Sprintf("%v", timestamp)
		CodeRepoBindingName = "e2ecoderepobindingforgithub" + fmt.Sprintf("%v", timestamp)
		ProjectName = "e2eprojectforcoderepogithub" + fmt.Sprintf("%v", timestamp)
		SecretName = "e2esecretforcoderepogithub" + fmt.Sprintf("%v", timestamp)
		SecretUserName = githubSecretUserName
		SecretPassWord = githubSecretPassWord
		// "8c213e645326a918b2af989f06728655f0971e29"
		AccountName = githubAccountName
		OrgName = githubOrgName
	})
	JustBeforeEach(func() {
		codeRepoData = map[string]string{
			"CodeRepoServiceName": CodeRepoServiceName,
			"CodeRepoServiceHost": CodeRepoServiceHost,
			"CodeRepoBindingName": CodeRepoBindingName,
			"ProjectName":         ProjectName,
			"SecretName":          SecretName,
			"SecretUserName":      SecretUserName,
			"SecretPassWord":      SecretPassWord,
			"AccountName":         AccountName,
		}
		servicefile = "coderepo/codereposervice.github.yaml"
		servicefile = makeFile(servicefile, codeRepoData)
		kubectlApply(servicefile, true)
		bindingfile = "coderepo/coderepobinding.github.yaml"
		bindingfile = makeFile(bindingfile, codeRepoData)
		kubectlApply(bindingfile, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", bindingfile)
		commandP("kubectl", "delete", "-f", servicefile)
	})

	SDescribeF("get CodeRepositoryList", func() {
		BeforeEach(func() {
			CodeRepoServiceHost = "https://api.github.com"
			name, found := getCodeRepoServiceNameByHost(CodeRepoServiceHost)
			if found == true {
				CodeRepoServiceName = name
			}
		})
		JustBeforeEach(func() {
			coderepositorylist, err, found = getCodeRepositoryList(ProjectName, CodeRepoServiceName)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepositorylist.Items).NotTo(BeEmpty())
			Expect(found).To(BeTrue())
		})
	})

	DescribeF("get CodeRepoBindingList", func() {
		JustBeforeEach(func() {
			coderepobindinglist, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobindinglist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range coderepobindinglist.Items {
				if p.Name == CodeRepoBindingName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get CodeRepoBinding detail", func() {
		JustBeforeEach(func() {
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Get(CodeRepoBindingName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobinding.Spec.Account.Owners).NotTo(BeEmpty())
		})
	})

	SDescribeF("get remote-repositories", func() {
		BeforeEach(func() {
			CodeRepoServiceHost = "https://api.github.com"
			name, found := getCodeRepoServiceNameByHost(CodeRepoServiceHost)
			if found == true {
				CodeRepoServiceName = name
			}
		})
		JustBeforeEach(func() {
			coderepobindingrepo, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).GetRemoteRepositories(CodeRepoBindingName, &v1alpha1.CodeRepoBindingRepositoryOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobindingrepo).NotTo(BeNil())
			Expect(string(coderepobindingrepo.Type)).To(Equal("Github"))
			Expect(len(coderepobindingrepo.Owners)).ToNot(Equal(0))
		})
	})

	DescribeF("update CodeRepoBinding repos", func() {
		JustBeforeEach(func() {
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Get(CodeRepoBindingName, v1.GetOptions{})
			coderepobinding = coderepobinding.DeepCopy()
			coderepobinding.SetAnnotations(map[string]string{
				"alauda.io/description": "update",
			})
			coderepobinding.Spec.Account.Owners = []v1alpha1.CodeRepositoryOwnerSync{
				v1alpha1.CodeRepositoryOwnerSync{
					Type:         "Org",
					Name:         OrgName,
					All:          true,
					Repositories: []string{},
				},
			}
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Update(coderepobinding)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(string(coderepobinding.Spec.Account.Owners[0].Type)).To(Equal("Org"))
			Expect(coderepobinding.GetAnnotations()["alauda.io/description"]).To(Equal("update"))
		})
	})
	DescribeF("delete  CodeRepoBinding ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Delete(CodeRepoBindingName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("CodeRepoService gitlab", func() {
	var (
		codereposervice                                       *v1alpha1.CodeRepoService
		codereposervicelist                                   *v1alpha1.CodeRepoServiceList
		CodeRepoServiceName, CodeRepoServiceHost, servicefile string
		codeRepoData                                          map[string]string
		timestamp                                             int64
		err                                                   error
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		CodeRepoServiceName = "e2ecodereposervicegitlab" + fmt.Sprintf("%v", timestamp)
		CodeRepoServiceHost = "https://gitlab.com" + fmt.Sprintf("%v", timestamp)
		codeRepoData = map[string]string{
			"CodeRepoServiceName": CodeRepoServiceName,
			"CodeRepoServiceHost": CodeRepoServiceHost,
		}
		servicefile = "coderepo/codereposervice.gitlab.yaml"
		servicefile = makeFile(servicefile, codeRepoData)
		kubectlApply(servicefile, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", servicefile)
	})
	DescribeF("get CodeRepoServiceList", func() {
		JustBeforeEach(func() {
			codereposervicelist, err = devopsClient.DevopsV1alpha1().CodeRepoServices().List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(codereposervicelist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range codereposervicelist.Items {
				if p.Name == CodeRepoServiceName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get CodeRepoService detail", func() {
		JustBeforeEach(func() {
			codereposervice, err = devopsClient.DevopsV1alpha1().CodeRepoServices().Get(CodeRepoServiceName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(string(codereposervice.Spec.Type)).To(ContainSubstring("Gitlab"))
		})
	})
	DescribeF("delete  CodeRepoService ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().CodeRepoServices().Delete(CodeRepoServiceName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("CodeRepoBinding gitlab", func() {
	var (
		coderepobinding                                                                         *v1alpha1.CodeRepoBinding
		coderepobindinglist                                                                     *v1alpha1.CodeRepoBindingList
		coderepositorylist                                                                      *v1alpha1.CodeRepositoryList
		coderepobindingrepo                                                                     *v1alpha1.CodeRepoBindingRepositories
		CodeRepoServiceName, CodeRepoServiceHost, CodeRepoBindingName, servicefile, bindingfile string
		ProjectName, SecretName, SecretUserName, SecretPassWord, AccountName, OrgName           string
		codeRepoData                                                                            map[string]string
		timestamp                                                                               int64
		err                                                                                     error
		found                                                                                   bool
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		CodeRepoServiceName = "e2ecodereposerviceforgitlab" + fmt.Sprintf("%v", timestamp)
		CodeRepoServiceHost = "https://gitlab.com" + fmt.Sprintf("%v", timestamp)
		CodeRepoBindingName = "e2ecoderepobindingforgitlab" + fmt.Sprintf("%v", timestamp)
		ProjectName = "e2eprojectforcoderepogitlab" + fmt.Sprintf("%v", timestamp)
		SecretName = "e2esecretforcoderepogitlab" + fmt.Sprintf("%v", timestamp)
		SecretUserName = gitlabSecretUserName
		SecretPassWord = gitlabSecretPassWord
		AccountName = gitlabAccountName
		OrgName = gitlabOrgName
	})
	JustBeforeEach(func() {
		codeRepoData = map[string]string{
			"CodeRepoServiceName": CodeRepoServiceName,
			"CodeRepoServiceHost": CodeRepoServiceHost,
			"CodeRepoBindingName": CodeRepoBindingName,
			"ProjectName":         ProjectName,
			"SecretName":          SecretName,
			"SecretUserName":      SecretUserName,
			"SecretPassWord":      SecretPassWord,
			"AccountName":         AccountName,
			"OrgName":             OrgName,
		}
		servicefile = "coderepo/codereposervice.gitlab.yaml"
		servicefile = makeFile(servicefile, codeRepoData)
		kubectlApply(servicefile, true)
		bindingfile = "coderepo/coderepobinding.gitlab.yaml"
		bindingfile = makeFile(bindingfile, codeRepoData)
		kubectlApply(bindingfile, true)

	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", bindingfile)
		commandP("kubectl", "delete", "-f", servicefile)
	})

	SDescribeF("get CodeRepositoryList", func() {
		BeforeEach(func() {
			CodeRepoServiceHost = "https://gitlab.com"
			name, found := getCodeRepoServiceNameByHost(CodeRepoServiceHost)
			if found == true {
				CodeRepoServiceName = name
			}
		})
		JustBeforeEach(func() {
			coderepositorylist, err, found = getCodeRepositoryList(ProjectName, CodeRepoServiceName)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepositorylist.Items).NotTo(BeEmpty())
			Expect(found).To(BeTrue())
		})
	})

	DescribeF("get CodeRepoBindingList", func() {
		JustBeforeEach(func() {
			coderepobindinglist, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobindinglist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range coderepobindinglist.Items {
				if p.Name == CodeRepoBindingName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get CodeRepoBinding detail", func() {
		JustBeforeEach(func() {
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Get(CodeRepoBindingName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobinding.Spec.Account.Owners).NotTo(BeEmpty())
		})
	})

	SDescribeF("get remote-repositories", func() {
		BeforeEach(func() {
			CodeRepoServiceHost = "https://gitlab.com"
			name, found := getCodeRepoServiceNameByHost(CodeRepoServiceHost)
			if found == true {
				CodeRepoServiceName = name
			}
		})
		JustBeforeEach(func() {
			coderepobindingrepo, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).GetRemoteRepositories(CodeRepoBindingName, &v1alpha1.CodeRepoBindingRepositoryOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobindingrepo).NotTo(BeNil())
			Expect(string(coderepobindingrepo.Type)).To(Equal("Gitlab"))
			Expect(len(coderepobindingrepo.Owners)).ToNot(Equal(0))
		})
	})

	DescribeF("update CodeRepoBinding repos", func() {
		JustBeforeEach(func() {
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Get(CodeRepoBindingName, v1.GetOptions{})
			coderepobinding = coderepobinding.DeepCopy()
			coderepobinding.SetAnnotations(map[string]string{
				"alauda.io/description": "update",
			})
			coderepobinding.Spec.Account.Owners = []v1alpha1.CodeRepositoryOwnerSync{
				v1alpha1.CodeRepositoryOwnerSync{
					Type:         "Org",
					Name:         OrgName,
					All:          true,
					Repositories: []string{},
				},
			}
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Update(coderepobinding)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(string(coderepobinding.Spec.Account.Owners[0].Type)).To(Equal("Org"))
			Expect(coderepobinding.GetAnnotations()["alauda.io/description"]).To(Equal("update"))
		})
	})
	DescribeF("delete  CodeRepoBinding ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Delete(CodeRepoBindingName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("CodeRepoService gitee", func() {
	var (
		codereposervice                                       *v1alpha1.CodeRepoService
		codereposervicelist                                   *v1alpha1.CodeRepoServiceList
		CodeRepoServiceName, CodeRepoServiceHost, servicefile string
		codeRepoData                                          map[string]string
		timestamp                                             int64
		err                                                   error
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		CodeRepoServiceName = "e2ecodereposervicegitee" + fmt.Sprintf("%v", timestamp)
		CodeRepoServiceHost = "https://gitee.com" + fmt.Sprintf("%v", timestamp)
		codeRepoData = map[string]string{
			"CodeRepoServiceName": CodeRepoServiceName,
			"CodeRepoServiceHost": CodeRepoServiceHost,
		}
		servicefile = "coderepo/codereposervice.gitee.yaml"
		servicefile = makeFile(servicefile, codeRepoData)
		kubectlApply(servicefile, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", servicefile)
	})
	DescribeF("get CodeRepoServiceList", func() {
		JustBeforeEach(func() {
			codereposervicelist, err = devopsClient.DevopsV1alpha1().CodeRepoServices().List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(codereposervicelist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range codereposervicelist.Items {
				if p.Name == CodeRepoServiceName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get CodeRepoService detail", func() {
		JustBeforeEach(func() {
			codereposervice, err = devopsClient.DevopsV1alpha1().CodeRepoServices().Get(CodeRepoServiceName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(string(codereposervice.Spec.Type)).To(ContainSubstring("Gitee"))
		})
	})
	DescribeF("delete  CodeRepoService ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().CodeRepoServices().Delete(CodeRepoServiceName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("CodeRepoBinding gitee", func() {
	var (
		coderepobinding                                                                         *v1alpha1.CodeRepoBinding
		coderepobindinglist                                                                     *v1alpha1.CodeRepoBindingList
		coderepositorylist                                                                      *v1alpha1.CodeRepositoryList
		coderepobindingrepo                                                                     *v1alpha1.CodeRepoBindingRepositories
		CodeRepoServiceName, CodeRepoServiceHost, CodeRepoBindingName, servicefile, bindingfile string
		ProjectName, SecretName, SecretUserName, SecretPassWord, AccountName, OrgName, UserName string
		codeRepoData                                                                            map[string]string
		timestamp                                                                               int64
		err                                                                                     error
		found                                                                                   bool
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		CodeRepoServiceName = "e2ecodereposerviceforgitee" + fmt.Sprintf("%v", timestamp)
		CodeRepoServiceHost = "https://gitee.com" + fmt.Sprintf("%v", timestamp)
		CodeRepoBindingName = "e2ecoderepobindingforgitee" + fmt.Sprintf("%v", timestamp)
		ProjectName = "e2eprojectforcoderepogitee" + fmt.Sprintf("%v", timestamp)
		SecretName = "e2esecretforcoderepogitee" + fmt.Sprintf("%v", timestamp)
		SecretUserName = giteeSecretUserName
		SecretPassWord = giteeSecretPassWord
		AccountName = giteeAccountName
		UserName = AccountName
		OrgName = giteeOrgName
	})
	JustBeforeEach(func() {
		codeRepoData = map[string]string{
			"CodeRepoServiceName": CodeRepoServiceName,
			"CodeRepoServiceHost": CodeRepoServiceHost,
			"CodeRepoBindingName": CodeRepoBindingName,
			"ProjectName":         ProjectName,
			"SecretName":          SecretName,
			"SecretUserName":      SecretUserName,
			"SecretPassWord":      SecretPassWord,
			"AccountName":         AccountName,
			"OrgName":             OrgName,
		}
		servicefile = "coderepo/codereposervice.gitee.yaml"
		servicefile = makeFile(servicefile, codeRepoData)
		kubectlApply(servicefile, true)
		bindingfile = "coderepo/coderepobinding.gitee.yaml"
		bindingfile = makeFile(bindingfile, codeRepoData)
		kubectlApply(bindingfile, true)

	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", bindingfile)
		commandP("kubectl", "delete", "-f", servicefile)
	})

	SDescribeF("get CodeRepositoryList", func() {
		BeforeEach(func() {
			CodeRepoServiceHost = "https://gitee.com"
			name, found := getCodeRepoServiceNameByHost(CodeRepoServiceHost)
			if found == true {
				CodeRepoServiceName = name
			}
		})
		JustBeforeEach(func() {
			coderepositorylist, err, found = getCodeRepositoryList(ProjectName, CodeRepoServiceName)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepositorylist.Items).NotTo(BeEmpty())
			Expect(found).To(BeTrue())
		})
	})

	DescribeF("get CodeRepoBindingList", func() {
		JustBeforeEach(func() {
			coderepobindinglist, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobindinglist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range coderepobindinglist.Items {
				if p.Name == CodeRepoBindingName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get CodeRepoBinding detail", func() {
		JustBeforeEach(func() {
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Get(CodeRepoBindingName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobinding.Spec.Account.Owners).NotTo(BeEmpty())
		})
	})

	SDescribeF("get remote-repositories", func() {
		BeforeEach(func() {
			CodeRepoServiceHost = "https://gitee.com"
			name, found := getCodeRepoServiceNameByHost(CodeRepoServiceHost)
			if found == true {
				CodeRepoServiceName = name
			}
		})
		JustBeforeEach(func() {
			coderepobindingrepo, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).GetRemoteRepositories(CodeRepoBindingName, &v1alpha1.CodeRepoBindingRepositoryOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobindingrepo).NotTo(BeNil())
			Expect(string(coderepobindingrepo.Type)).To(Equal("Gitee"))
			Expect(len(coderepobindingrepo.Owners)).ToNot(Equal(0))
		})
	})

	DescribeF("update CodeRepoBinding repos", func() {
		JustBeforeEach(func() {
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Get(CodeRepoBindingName, v1.GetOptions{})
			coderepobinding = coderepobinding.DeepCopy()
			coderepobinding.SetAnnotations(map[string]string{
				"alauda.io/description": "update",
			})
			coderepobinding.Spec.Account.Owners = []v1alpha1.CodeRepositoryOwnerSync{
				v1alpha1.CodeRepositoryOwnerSync{
					Type:         "User",
					Name:         UserName,
					All:          true,
					Repositories: []string{},
				},
			}
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Update(coderepobinding)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(string(coderepobinding.Spec.Account.Owners[0].Type)).To(Equal("User"))
			Expect(coderepobinding.GetAnnotations()["alauda.io/description"]).To(Equal("update"))
		})
	})
	DescribeF("delete  CodeRepoBinding ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Delete(CodeRepoBindingName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("CodeRepoService bitbucket", func() {
	var (
		codereposervice                                       *v1alpha1.CodeRepoService
		codereposervicelist                                   *v1alpha1.CodeRepoServiceList
		CodeRepoServiceName, CodeRepoServiceHost, servicefile string
		codeRepoData                                          map[string]string
		timestamp                                             int64
		err                                                   error
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		CodeRepoServiceName = "e2ecodereposervicebitbucket" + fmt.Sprintf("%v", timestamp)
		CodeRepoServiceHost = "https://api.bitbucket.org" + fmt.Sprintf("%v", timestamp)
		codeRepoData = map[string]string{
			"CodeRepoServiceName": CodeRepoServiceName,
			"CodeRepoServiceHost": CodeRepoServiceHost,
		}
		servicefile = "coderepo/codereposervice.bitbucket.yaml"
		servicefile = makeFile(servicefile, codeRepoData)
		kubectlApply(servicefile, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", servicefile)
	})
	DescribeF("get CodeRepoServiceList", func() {
		JustBeforeEach(func() {
			codereposervicelist, err = devopsClient.DevopsV1alpha1().CodeRepoServices().List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(codereposervicelist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range codereposervicelist.Items {
				if p.Name == CodeRepoServiceName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get CodeRepoService detail", func() {
		JustBeforeEach(func() {
			codereposervice, err = devopsClient.DevopsV1alpha1().CodeRepoServices().Get(CodeRepoServiceName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(string(codereposervice.Spec.Type)).To(ContainSubstring("Bitbucket"))
		})
	})
	DescribeF("delete  CodeRepoService ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().CodeRepoServices().Delete(CodeRepoServiceName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("CodeRepoBinding bitbucket", func() {
	var (
		coderepobinding                                                                         *v1alpha1.CodeRepoBinding
		coderepobindinglist                                                                     *v1alpha1.CodeRepoBindingList
		coderepositorylist                                                                      *v1alpha1.CodeRepositoryList
		coderepobindingrepo                                                                     *v1alpha1.CodeRepoBindingRepositories
		CodeRepoServiceName, CodeRepoServiceHost, CodeRepoBindingName, servicefile, bindingfile string
		ProjectName, SecretName, SecretUserName, SecretPassWord, AccountName                    string
		codeRepoData                                                                            map[string]string
		timestamp                                                                               int64
		err                                                                                     error
		found                                                                                   bool
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		CodeRepoServiceName = "e2ecodereposerviceforbitbucket" + fmt.Sprintf("%v", timestamp)
		CodeRepoServiceHost = "https://api.bitbucket.org" + fmt.Sprintf("%v", timestamp)
		CodeRepoBindingName = "e2ecoderepobindingforbitbucket" + fmt.Sprintf("%v", timestamp)
		ProjectName = "e2eprojectforcoderepobitbucket" + fmt.Sprintf("%v", timestamp)
		SecretName = "e2esecretforcoderepobitbucket" + fmt.Sprintf("%v", timestamp)
		SecretUserName = bitbucketSecretUserName
		SecretPassWord = bitbucketSecretPassWord
		AccountName = bitbucketAccountName
	})
	JustBeforeEach(func() {
		codeRepoData = map[string]string{
			"CodeRepoServiceName": CodeRepoServiceName,
			"CodeRepoServiceHost": CodeRepoServiceHost,
			"CodeRepoBindingName": CodeRepoBindingName,
			"ProjectName":         ProjectName,
			"SecretName":          SecretName,
			"SecretUserName":      SecretUserName,
			"SecretPassWord":      SecretPassWord,
			"AccountName":         AccountName,
		}
		servicefile = "coderepo/codereposervice.bitbucket.yaml"
		servicefile = makeFile(servicefile, codeRepoData)
		kubectlApply(servicefile, true)
		bindingfile = "coderepo/coderepobinding.bitbucket.yaml"
		bindingfile = makeFile(bindingfile, codeRepoData)
		kubectlApply(bindingfile, true)

	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", bindingfile)
		commandP("kubectl", "delete", "-f", servicefile)
	})

	SDescribeF("get CodeRepositoryList", func() {
		BeforeEach(func() {
			CodeRepoServiceHost = "https://api.bitbucket.org"
			name, found := getCodeRepoServiceNameByHost(CodeRepoServiceHost)
			if found == true {
				CodeRepoServiceName = name
			}
		})
		JustBeforeEach(func() {
			coderepositorylist, err, found = getCodeRepositoryList(ProjectName, CodeRepoServiceName)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepositorylist.Items).NotTo(BeEmpty())
			Expect(found).To(BeTrue())
		})
	})

	DescribeF("get CodeRepoBindingList", func() {
		JustBeforeEach(func() {
			coderepobindinglist, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).List(v1.ListOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobindinglist.Items).NotTo(BeEmpty())
			found := false
			for _, p := range coderepobindinglist.Items {
				if p.Name == CodeRepoBindingName {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get CodeRepoBinding detail", func() {
		JustBeforeEach(func() {
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Get(CodeRepoBindingName, v1.GetOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobinding.Spec.Account.Owners).NotTo(BeEmpty())
		})
	})

	SDescribeF("get remote-repositories", func() {
		BeforeEach(func() {
			CodeRepoServiceHost = "https://api.bitbucket.org"
			name, found := getCodeRepoServiceNameByHost(CodeRepoServiceHost)
			if found == true {
				CodeRepoServiceName = name
			}
		})
		JustBeforeEach(func() {
			coderepobindingrepo, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).GetRemoteRepositories(CodeRepoBindingName, &v1alpha1.CodeRepoBindingRepositoryOptions{})
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobindingrepo).NotTo(BeNil())
			Expect(string(coderepobindingrepo.Type)).To(Equal("Bitbucket"))
			Expect(len(coderepobindingrepo.Owners)).ToNot(Equal(0))
		})
	})

	DescribeF("update CodeRepoBinding repos", func() {
		JustBeforeEach(func() {
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Get(CodeRepoBindingName, v1.GetOptions{})
			coderepobinding = coderepobinding.DeepCopy()
			coderepobinding.SetAnnotations(map[string]string{
				"alauda.io/description": "update",
			})
			coderepobinding.Spec.Account.Owners = []v1alpha1.CodeRepositoryOwnerSync{
				v1alpha1.CodeRepositoryOwnerSync{
					Type:         "User",
					Name:         AccountName,
					All:          true,
					Repositories: []string{},
				},
			}
			coderepobinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Update(coderepobinding)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(coderepobinding.GetAnnotations()["alauda.io/description"]).To(Equal("update"))
			Expect(string(coderepobinding.Spec.Account.Owners[0].Type)).To(Equal("User"))
		})
	})
	DescribeF("delete  CodeRepoBinding ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().CodeRepoBindings(ProjectName).Delete(CodeRepoBindingName, nil)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})
