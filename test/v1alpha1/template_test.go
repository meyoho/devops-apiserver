package v1alpha1_test

import (
	"fmt"
	"strings"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = DescribeM("PipelineTemplateSync", func() {
	var (
		pipelineTemplateSync                                                                                           *v1alpha1.PipelineTemplateSync
		pipelineTemplateSyncList                                                                                       *v1alpha1.PipelineTemplateSyncList
		ProjectName, SecretName, SecretUserName, SecretPassWord, AccountName, CodeRepoServiceName, CodeRepoBindingName string
		PipelineTemplateSyncName, file, dependfile                                                                     string
		templateSyncData                                                                                               map[string]string
		timestamp                                                                                                      int64
		err                                                                                                            error
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		ProjectName = "e2eprojectfortemplatesync" + fmt.Sprintf("%v", timestamp)
		SecretName = "e2esecretfortemplatesync" + fmt.Sprintf("%v", timestamp)
		SecretUserName = templateSecretUserName
		SecretPassWord = templateSecretPassWord
		// "8c213e645326a918b2af989f06728655f0971e29"
		AccountName = templateAccountName
		CodeRepoServiceName = "e2ecodeservicefortemplatesync" + fmt.Sprintf("%v", timestamp)
		CodeRepoBindingName = "e2ecodebindingfortemplatesync" + fmt.Sprintf("%v", timestamp)
		PipelineTemplateSyncName = "e2etemplatesync" + fmt.Sprintf("%v", timestamp)
		templateSyncData = map[string]string{
			"ProjectName":              ProjectName,
			"SecretName":               SecretName,
			"SecretUserName":           SecretUserName,
			"SecretPassWord":           SecretPassWord,
			"AccountName":              AccountName,
			"CodeRepoServiceName":      CodeRepoServiceName,
			"CodeRepoBindingName":      CodeRepoBindingName,
			"PipelineTemplateSyncName": PipelineTemplateSyncName,
		}
		dependfile = "template/pipelinetemplatesync-dependencies.yaml"
		dependfile = makeFile(dependfile, templateSyncData)
		kubectlApply(dependfile, true)
		file = "template/pipelinetemplatesync.yaml"
		file = makeFile(file, templateSyncData)
		kubectlApply(file, true)

	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", file)
		commandP("kubectl", "delete", "-f", dependfile)
	})

	DescribeF("get pipelinetemplateSyncList", func() {
		JustBeforeEach(func() {
			pipelineTemplateSyncList, err = devopsClient.DevopsV1alpha1().PipelineTemplateSyncs(ProjectName).List(v1alpha1.ListOptions())
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(pipelineTemplateSyncList.Items).NotTo(BeEmpty())
			found := false
			for _, p := range pipelineTemplateSyncList.Items {
				if strings.Contains(p.Name, PipelineTemplateSyncName) {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get pipelinetemplateSync", func() {
		JustBeforeEach(func() {
			pipelineTemplateSync, err = devopsClient.DevopsV1alpha1().PipelineTemplateSyncs(ProjectName).Get(PipelineTemplateSyncName, v1alpha1.GetOptions())
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(pipelineTemplateSync.GetName()).To(Equal(PipelineTemplateSyncName))
		})
	})
	DescribeF("trigger pipelinetemplateSync", func() {
		JustBeforeEach(func() {
			pipelineTemplateSync, err = devopsClient.DevopsV1alpha1().PipelineTemplateSyncs(ProjectName).Get(PipelineTemplateSyncName, v1alpha1.GetOptions())
			pipelineTemplateSync = pipelineTemplateSync.DeepCopy()
			pipelineTemplateSync.Status = &v1alpha1.PipelineTemplateSyncStatus{}
			pipelineTemplateSync.Status.Phase = "Pending"
			pipelineTemplateSync, err = devopsClient.DevopsV1alpha1().PipelineTemplateSyncs(ProjectName).Update(pipelineTemplateSync)
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(pipelineTemplateSync.GetName()).To(Equal(PipelineTemplateSyncName))
			Expect(string(pipelineTemplateSync.Status.Phase)).To(Equal("Pending"))
		})
	})
	DescribeF("delete pipelinetemplateSync", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().PipelineTemplateSyncs(ProjectName).Delete(PipelineTemplateSyncName, nil)
			time.Sleep(time.Second * 1)
			pipelineTemplateSyncList, _ = devopsClient.DevopsV1alpha1().PipelineTemplateSyncs(ProjectName).List(v1alpha1.ListOptions())
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			found := true
			for _, p := range pipelineTemplateSyncList.Items {
				if strings.Contains(p.Name, PipelineTemplateSyncName) {
					found = false
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
})

var _ = DescribeM("PipelineTaskTemplate", func() {
	var (
		pipelineTaskTemplate                        *v1alpha1.PipelineTaskTemplate
		pipelineTaskTemplateList                    *v1alpha1.PipelineTaskTemplateList
		PipelineTaskTemplateName, ProjectName, file string
		templateData                                map[string]string
		timestamp                                   int64
		err                                         error
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		ProjectName = "e2eprojectforpipelinetasktemplate" + fmt.Sprintf("%v", timestamp)
		PipelineTaskTemplateName = "e2epipelinetasktemplate" + fmt.Sprintf("%v", timestamp)
		templateData = map[string]string{
			"ProjectName":              ProjectName,
			"PipelineTaskTemplateName": PipelineTaskTemplateName,
		}
		file = "template/pipelinetasktemplate.yaml"
		file = makeFile(file, templateData)
		kubectlApply(file, true)

	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", file)
	})

	DescribeF("get PipelineTaskTemplateList", func() {
		JustBeforeEach(func() {
			pipelineTaskTemplateList, err = devopsClient.DevopsV1alpha1().PipelineTaskTemplates(ProjectName).List(v1alpha1.ListOptions())
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(pipelineTaskTemplateList.Items).NotTo(BeEmpty())
			found := false
			for _, p := range pipelineTaskTemplateList.Items {
				if strings.Contains(p.Name, PipelineTaskTemplateName) {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get PipelineTaskTemplate", func() {
		JustBeforeEach(func() {
			pipelineTaskTemplate, err = devopsClient.DevopsV1alpha1().PipelineTaskTemplates(ProjectName).Get(PipelineTaskTemplateName, v1alpha1.GetOptions())
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(pipelineTaskTemplate.GetName()).To(Equal(PipelineTaskTemplateName))
		})
	})
	DescribeF("delete PipelineTaskTemplate", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().PipelineTaskTemplates(ProjectName).Delete(PipelineTaskTemplateName, nil)
			time.Sleep(time.Second * 1)
			pipelineTaskTemplateList, _ = devopsClient.DevopsV1alpha1().PipelineTaskTemplates(ProjectName).List(v1alpha1.ListOptions())
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			found := true
			for _, p := range pipelineTaskTemplateList.Items {
				if strings.Contains(p.Name, PipelineTaskTemplateName) {
					found = false
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
})

var _ = DescribeM("PipelineTemplate", func() {
	var (
		pipelineTemplate                                                               *v1alpha1.PipelineTemplate
		pipelineTemplateList                                                           *v1alpha1.PipelineTemplateList
		ProjectName, jenkinsName, secretGitName, secretJenkinsName, JenkinsBindingName string
		PipelineTemplateName, dependfile, file                                         string
		templateData                                                                   map[string]string
		timestamp                                                                      int64
		err                                                                            error
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		ProjectName = "e2eprojectforpipelinetemplate" + fmt.Sprintf("%v", timestamp)
		jenkinsName = "e2ejenkinsforpipelinetemplate" + fmt.Sprintf("%v", timestamp)
		secretGitName = "e2esecretgitforpipelinetemplate" + fmt.Sprintf("%v", timestamp)
		secretJenkinsName = "e2esecretjenkinsforpipelinetemplate" + fmt.Sprintf("%v", timestamp)
		JenkinsBindingName = "e2ejenkinsbindingforpipelinetemplate" + fmt.Sprintf("%v", timestamp)
		PipelineTemplateName = "e2epipelinetemplate" + fmt.Sprintf("%v", timestamp)
		templateData = map[string]string{
			"ProjectName":          ProjectName,
			"jenkinsName":          jenkinsName,
			"secretGitName":        secretGitName,
			"secretJenkinsName":    secretJenkinsName,
			"JenkinsBindingName":   JenkinsBindingName,
			"PipelineTemplateName": PipelineTemplateName,
		}
		dependfile = "template/pipelinetemplate-dependencies.yaml"
		dependfile = makeFile(dependfile, templateData)
		kubectlApply(dependfile, true)
		file = "template/pipelinetemplate.yaml"
		file = makeFile(file, templateData)
		kubectlApply(file, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", file)
		commandP("kubectl", "delete", "-f", dependfile)
	})

	DescribeF("get PipelineTemplateList", func() {
		JustBeforeEach(func() {
			pipelineTemplateList, err = devopsClient.DevopsV1alpha1().PipelineTemplates(ProjectName).List(v1alpha1.ListOptions())
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(pipelineTemplateList.Items).NotTo(BeEmpty())
			found := false
			for _, p := range pipelineTemplateList.Items {
				if strings.Contains(p.Name, PipelineTemplateName) {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get PipelineTemplate", func() {
		JustBeforeEach(func() {
			pipelineTemplate, err = devopsClient.DevopsV1alpha1().PipelineTemplates(ProjectName).Get(PipelineTemplateName, v1alpha1.GetOptions())
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(pipelineTemplate.GetName()).To(Equal(PipelineTemplateName))
		})
	})
	DescribeF("delete PipelineTemplate", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().PipelineTemplates(ProjectName).Delete(PipelineTemplateName, nil)
			time.Sleep(time.Second * 1)
			pipelineTemplateList, _ = devopsClient.DevopsV1alpha1().PipelineTemplates(ProjectName).List(v1alpha1.ListOptions())
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			found := true
			for _, p := range pipelineTemplateList.Items {
				if strings.Contains(p.Name, PipelineTemplateName) {
					found = false
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})

})

var _ = DescribeM("pipelineconfig use pipelinetemplate", func() {
	var (
		pipeconfig                                                                                         *v1alpha1.PipelineConfig
		ProjectName, jenkinsName, secretJenkinsName, secretGitName, JenkinsBindingName, PipelineConfigName string
		PipelineTemplateName, dependfile, file, pipelineconfigfile                                         string
		templateData                                                                                       map[string]string
		timestamp                                                                                          int64
		err                                                                                                error
	)
	BeforeEach(func() {
		timestamp = time.Now().UnixNano()
		ProjectName = "e2eprojectforpipelinetemplate" + fmt.Sprintf("%v", timestamp)
		jenkinsName = "e2ejenkinsforpipelinetemplate" + fmt.Sprintf("%v", timestamp)
		secretGitName = "e2esecretgitforpipelinetemplate" + fmt.Sprintf("%v", timestamp)
		secretJenkinsName = "e2esecretjenkinsforpipelinetemplate" + fmt.Sprintf("%v", timestamp)
		JenkinsBindingName = "e2ejenkinsbindingforpipelinetemplate" + fmt.Sprintf("%v", timestamp)
		PipelineConfigName = "e2epipelineconfigforpipelinetemplate" + fmt.Sprintf("%v", timestamp)
		PipelineTemplateName = "e2epipelinetemplate" + fmt.Sprintf("%v", timestamp)
		templateData = map[string]string{
			"ProjectName":          ProjectName,
			"jenkinsName":          jenkinsName,
			"secretGitName":        secretGitName,
			"secretJenkinsName":    secretJenkinsName,
			"JenkinsBindingName":   JenkinsBindingName,
			"PipelineTemplateName": PipelineTemplateName,
			"PipelineConfigName":   PipelineConfigName,
		}
		dependfile = "template/pipelinetemplate-dependencies.yaml"
		dependfile = makeFile(dependfile, templateData)
		kubectlApply(dependfile, true)
		file = "template/pipelinetemplate.yaml"
		file = makeFile(file, templateData)
		kubectlApply(file, true)
		pipelineconfigfile = "template/pipelinetemplate-pipelineconfig.yaml"
		pipelineconfigfile = makeFile(pipelineconfigfile, templateData)
		kubectlApply(pipelineconfigfile, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", pipelineconfigfile)
		commandP("kubectl", "delete", "-f", file)
		commandP("kubectl", "delete", "-f", dependfile)
	})
	DescribeF("get PipelineConfig", func() {
		JustBeforeEach(func() {
			pipeconfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(ProjectName).Get(PipelineConfigName, v1alpha1.GetOptions())
		})
		It("should not error", func() {
			Expect(err).To(BeNil())
			Expect(pipeconfig.GetName()).To(Equal(PipelineConfigName))
			Expect(pipeconfig.Spec.Strategy.Template.GetName()).To(Equal(PipelineTemplateName))
		})
	})
})
