package v1alpha1_test

import (
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = DescribeM("jira", func() {
	var (
		jira                                         *v1alpha1.ProjectManagement
		jiralist                                     *v1alpha1.ProjectManagementList
		jirabinding                                  *v1alpha1.ProjectManagementBinding
		jirabindinglist                              *v1alpha1.ProjectManagementBindingList
		err                                          error
		jiraname, jirahost, bindingname, projectname string
		file_jira                                    string
		jiraData                                     map[string]string
	)
	BeforeEach(func() {
		jiraname = "e2etestjiraname" + fmt.Sprintf("%v", time.Now().UnixNano())
		jirahost = "http://jira.alaudatech.com/" + fmt.Sprintf("%v", time.Now().UnixNano())
		bindingname = "e2etestjirabindingname" + fmt.Sprintf("%v", time.Now().UnixNano())
		projectname = "e2etestprojectforjirabinding" + fmt.Sprintf("%v", time.Now().UnixNano())
		file_jira = "qianqian/jira.yaml"
		// file_binding = "qianqian/jira-binding.yaml"
		jiraData = map[string]string{
			"JiraName":        jiraname,
			"JiraHost":        jirahost,
			"JiraBindingName": bindingname,
			"ProjectName":     projectname,
		}
		file_jira = makeFile(file_jira, jiraData)
		kubectlApply(file_jira, true)
		// file_binding = makeFile(file_binding, jiraData)
		// kubectlApply(file_binding, true)
	})
	AfterEach(func() {
		// commandP("kubectl", "delete", "-f", file_binding)
		commandP("kubectl", "delete", "-f", file_jira)
	})
	DescribeF("get jira list", func() {
		JustBeforeEach(func() {
			jiralist, err = devopsClient.DevopsV1alpha1().ProjectManagements().List(v1.ListOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(jiralist.Items)).NotTo(BeZero())
			found := false
			for _, j := range jiralist.Items {
				if j.Name == jiraname {
					found = true
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get jira detail", func() {
		JustBeforeEach(func() {
			jira, err = devopsClient.DevopsV1alpha1().ProjectManagements().Get(jiraname, v1.GetOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(jira.Name).To(Equal(jiraname))
		})
	})
	DescribeF("delete jira", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().ProjectManagements().Delete(jiraname, nil)
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
		})
	})
	XDescribeF("get jirabinding list", func() {
		JustBeforeEach(func() {
			jirabindinglist, err = devopsClient.DevopsV1alpha1().ProjectManagementBindings(projectname).List(v1.ListOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(jirabindinglist.Items)).NotTo(BeZero())
			found := false
			for _, b := range jirabindinglist.Items {
				if b.Name == bindingname {
					found = true
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	XDescribeF("get jirabinding detail", func() {
		JustBeforeEach(func() {
			jirabinding, err = devopsClient.DevopsV1alpha1().ProjectManagementBindings(projectname).Get(bindingname, v1.GetOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(jirabinding.Name).To(Equal(bindingname))
		})
	})
	XDescribeF("delete jirabinding", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().ProjectManagementBindings(projectname).Delete(bindingname, nil)
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("taiga", func() {
	var (
		taiga                                          *v1alpha1.ProjectManagement
		taigalist                                      *v1alpha1.ProjectManagementList
		taigabinding                                   *v1alpha1.ProjectManagementBinding
		taigabindinglist                               *v1alpha1.ProjectManagementBindingList
		err                                            error
		taiganame, taigahost, bindingname, projectname string
		file_taiga                                     string
		taigaData                                      map[string]string
	)
	BeforeEach(func() {
		taiganame = "e2etesttaiganame" + fmt.Sprintf("%v", time.Now().UnixNano())
		taigahost = "http://taiga.com/" + fmt.Sprintf("%v", time.Now().UnixNano())
		bindingname = "e2etesttaigabindingname" + fmt.Sprintf("%v", time.Now().UnixNano())
		projectname = "e2etestprojectfortaigabinding" + fmt.Sprintf("%v", time.Now().UnixNano())
		file_taiga = "qianqian/taiga.yaml"
		// file_binding = "qianqian/taiga-binding.yaml"
		taigaData = map[string]string{
			"TaigaName":        taiganame,
			"TaigaHost":        taigahost,
			"TaigaBindingName": bindingname,
			"ProjectName":      projectname,
		}
		file_taiga = makeFile(file_taiga, taigaData)
		kubectlApply(file_taiga, true)
		// file_binding = makeFile(file_binding, taigaData)
		// kubectlApply(file_binding, true)
	})
	AfterEach(func() {
		// commandP("kubectl", "delete", "-f", file_binding)
		commandP("kubectl", "delete", "-f", file_taiga)
	})
	DescribeF("get taiga list", func() {
		JustBeforeEach(func() {
			taigalist, err = devopsClient.DevopsV1alpha1().ProjectManagements().List(v1.ListOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(taigalist.Items)).NotTo(BeZero())
			found := false
			for _, j := range taigalist.Items {
				if j.Name == taiganame {
					found = true
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get taiga detail", func() {
		JustBeforeEach(func() {
			taiga, err = devopsClient.DevopsV1alpha1().ProjectManagements().Get(taiganame, v1.GetOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(taiga.Name).To(Equal(taiganame))
		})
	})
	DescribeF("delete taiga", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().ProjectManagements().Delete(taiganame, nil)
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
		})
	})
	XDescribeF("get taigabinding list", func() {
		JustBeforeEach(func() {
			taigabindinglist, err = devopsClient.DevopsV1alpha1().ProjectManagementBindings(projectname).List(v1.ListOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(taigabindinglist.Items)).NotTo(BeZero())
			found := false
			for _, b := range taigabindinglist.Items {
				if b.Name == bindingname {
					found = true
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	XDescribeF("get taigabinding detail", func() {
		JustBeforeEach(func() {
			taigabinding, err = devopsClient.DevopsV1alpha1().ProjectManagementBindings(projectname).Get(bindingname, v1.GetOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(taigabinding.Name).To(Equal(bindingname))
		})
	})
	XDescribeF("delete taigabinding", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().ProjectManagementBindings(projectname).Delete(bindingname, nil)
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("redwoodHQ", func() {
	var (
		redwoodHQ                                              *v1alpha1.TestTool
		redwoodHQlist                                          *v1alpha1.TestToolList
		redwoodHQbinding                                       *v1alpha1.TestToolBinding
		redwoodHQbindinglist                                   *v1alpha1.TestToolBindingList
		err                                                    error
		redwoodHQname, redwoodHQhost, bindingname, projectname string
		file_redwoodHQ                                         string
		redwoodHQData                                          map[string]string
	)
	BeforeEach(func() {
		redwoodHQname = "e2etestredwoodhqname" + fmt.Sprintf("%v", time.Now().UnixNano())
		redwoodHQhost = "http://redwoodhq.com/" + fmt.Sprintf("%v", time.Now().UnixNano())
		bindingname = "e2etestredwoodhqbindingname" + fmt.Sprintf("%v", time.Now().UnixNano())
		projectname = "e2etestprojectforredwoodhqbinding" + fmt.Sprintf("%v", time.Now().UnixNano())
		file_redwoodHQ = "qianqian/redwoodhq.yaml"
		// file_binding = "qianqian/redwoodhq-binding.yaml"
		redwoodHQData = map[string]string{
			"RedwoodHQName":        redwoodHQname,
			"RedwoodHQHost":        redwoodHQhost,
			"RedwoodHQBindingName": bindingname,
			"ProjectName":          projectname,
		}
		file_redwoodHQ = makeFile(file_redwoodHQ, redwoodHQData)
		kubectlApply(file_redwoodHQ, true)
		// file_binding = makeFile(file_binding, redwoodHQData)
		// kubectlApply(file_binding, true)
	})
	AfterEach(func() {
		// commandP("kubectl", "delete", "-f", file_binding)
		commandP("kubectl", "delete", "-f", file_redwoodHQ)
	})
	DescribeF("get redwoodHQ list", func() {
		JustBeforeEach(func() {
			redwoodHQlist, err = devopsClient.DevopsV1alpha1().TestTools().List(v1.ListOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(redwoodHQlist.Items)).NotTo(BeZero())
			found := false
			for _, j := range redwoodHQlist.Items {
				if j.Name == redwoodHQname {
					found = true
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get redwoodHQ detail", func() {
		JustBeforeEach(func() {
			redwoodHQ, err = devopsClient.DevopsV1alpha1().TestTools().Get(redwoodHQname, v1.GetOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(redwoodHQ.Name).To(Equal(redwoodHQname))
		})
	})
	DescribeF("delete redwoodHQ", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().TestTools().Delete(redwoodHQname, nil)
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
		})
	})
	XDescribeF("get redwoodHQbinding list", func() {
		JustBeforeEach(func() {
			redwoodHQbindinglist, err = devopsClient.DevopsV1alpha1().TestToolBindings(projectname).List(v1.ListOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(redwoodHQbindinglist.Items)).NotTo(BeZero())
			found := false
			for _, b := range redwoodHQbindinglist.Items {
				if b.Name == bindingname {
					found = true
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	XDescribeF("get redwoodHQbinding detail", func() {
		JustBeforeEach(func() {
			redwoodHQbinding, err = devopsClient.DevopsV1alpha1().TestToolBindings(projectname).Get(bindingname, v1.GetOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(redwoodHQbinding.Name).To(Equal(bindingname))
		})
	})
	XDescribeF("delete redwoodHQbinding", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().TestToolBindings(projectname).Delete(bindingname, nil)
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})

var _ = DescribeM("sonarqube", func() {
	var (
		sonarqube                                    *v1alpha1.CodeQualityTool
		sonarqubelist                                *v1alpha1.CodeQualityToolList
		err                                          error
		sonarqubename, sonarqubehost, file_sonarqube string
		sonarqubeData                                map[string]string
	)
	BeforeEach(func() {
		sonarqubename = "e2etestsonarqubename" + fmt.Sprintf("%v", time.Now().UnixNano())
		sonarqubehost = "http://sonarcloud.io/" + fmt.Sprintf("%v", time.Now().UnixNano())
		file_sonarqube = "qianqian/sonarqube.yaml"
		sonarqubeData = map[string]string{
			"SonarQubeName": sonarqubename,
			"SonarQubeHost": sonarqubehost,
		}
		file_sonarqube = makeFile(file_sonarqube, sonarqubeData)
		kubectlApply(file_sonarqube, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", file_sonarqube)
	})
	DescribeF("get sonarqube list", func() {
		JustBeforeEach(func() {
			sonarqubelist, err = devopsClient.DevopsV1alpha1().CodeQualityTools().List(v1.ListOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(len(sonarqubelist.Items)).NotTo(BeZero())
			found := false
			for _, j := range sonarqubelist.Items {
				if j.Name == sonarqubename {
					found = true
				}
			}
			Expect(found).To(BeTrue())
		})
	})
	DescribeF("get sonarqube detail", func() {
		JustBeforeEach(func() {
			sonarqube, err = devopsClient.DevopsV1alpha1().CodeQualityTools().Get(sonarqubename, v1.GetOptions{})
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(sonarqube.Name).To(Equal(sonarqubename))
			Expect(string(sonarqube.Spec.Type)).To(Equal("Sonarqube"))
		})
	})
	DescribeF("delete sonarqube", func() {
		JustBeforeEach(func() {
			err = devopsClient.DevopsV1alpha1().CodeQualityTools().Delete(sonarqubename, nil)
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
		})
	})
})
