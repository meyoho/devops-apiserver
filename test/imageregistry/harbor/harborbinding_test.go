package harbor

import (
	"fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	testcommon "alauda.io/devops-apiserver/test"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("Harbor.ImageRegistryBinding GetRemoteRepositories", func() {
	var (
		opts                 testcommon.ImageRegistryBindingOpts
		remoteImageRepos     *v1alpha1.ImageRegistryBindingRepositories
		imageRegistryBinding *v1alpha1.ImageRegistryBinding
		imageScanResult      *v1alpha1.ImageResult
		err                  error
	)

	BeforeEach(func() {
		opts = testcommon.ImageRegistryBindingOpts{
			Name:            testcommon.GenName("e2eharborbind-"),
			Namespace:       namespace.Name,
			RegistryName:    imageRegistry.Name,
			RegistryType:    string(v1alpha1.RegistryTypeHarbor),
			SecretName:      correctSecretGlobal.Name,
			SecretNamespace: correctSecretGlobal.Namespace,
			Repositories:    nil,
		}
	})

	JustBeforeEach(func() {
		// Create Binding use global secret
		imageRegistryBinding, err = testcommon.CreateImageRegistryBinding(devopsClient, opts)
		Expect(err).To(BeNil())
		Expect(imageRegistryBinding).ToNot(BeNil())
		Expect(imageRegistryBinding.Name).ToNot(Equal(""))
	})

	AfterEach(func() {
		// delete binding
		testcommon.DeleteImageRegistryBinding(devopsClient, imageRegistryBinding)
	})

	Context("Working harbor, correct globalsecret", func() {
		It("check binding repository is working", func() {
			// Fetch remote repositories
			remoteImageRepos, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(opts.Namespace).GetImageRepos(imageRegistryBinding.Name)
			Expect(err).To(BeNil())
			Expect(remoteImageRepos).ToNot(BeNil())
			Expect(remoteImageRepos.Items).ToNot(BeNil())
			Expect(remoteImageRepos.Items).To(ContainElement(ContainSubstring(testcommon.GetValue(testcommon.HarborProjectKey))))
		})
		It("Wait harborsecret sync to namespace", func() {
			By("When ImageBinding succeed, Check whether globalsecret sync SecretTypeDockerConfigJson to namespace")
			var (
				secret         *corev1.Secret
				syncSecretName = imageRegistryBinding.GetDockerCfgSecretName()
				syncSecretType = string(corev1.SecretTypeDockerConfigJson)
			)
			Eventually(func() (err error) {
				secret, err = k8sClient.CoreV1().Secrets(imageRegistryBinding.Namespace).Get(syncSecretName, metav1.GetOptions{})
				if err != nil {
					return
				}
				if secret != nil && string(secret.Type) != syncSecretType {
					Expect(string(secret.Type)).Should(Equal(syncSecretType))
				}
				if err == nil && secret == nil {
					err = fmt.Errorf("Auto sync  has failed %s", syncSecretName)
					return
				}
				return nil
			}, testcommon.GetDuration(testcommon.TestTimeoutKey, testcommon.TimeoutDuration), time.Second).Should(Succeed())
		})
	})

	Context("Working harbor, correct privatesecret", func() {
		BeforeEach(func() {
			// use private secret Create Binding
			opts.SecretName = correctSecretPrivate.Name
			opts.SecretNamespace = correctSecretPrivate.Namespace
		})
		It("Wait privatesecret sync to namespace", func() {
			By("When ImageBinding succeed, Check whether privatesecret sync SecretTypeDockerConfigJson to namespace")
			var (
				secret         *corev1.Secret
				syncSecretName = imageRegistryBinding.GetDockerCfgSecretName()
				syncSecretType = string(corev1.SecretTypeDockerConfigJson)
			)
			Eventually(func() (err error) {
				secret, err = k8sClient.CoreV1().Secrets(imageRegistryBinding.Namespace).Get(syncSecretName, metav1.GetOptions{})
				if err != nil {
					return
				}
				if secret != nil && string(secret.Type) != syncSecretType {
					Expect(string(secret.Type)).Should(Equal(syncSecretType))
					return
				}
				if err == nil && secret == nil {
					err = fmt.Errorf("Auto sync SecretTypeDockerConfigJson has failed %s", syncSecretName)
					return
				}
				return nil
			}, testcommon.GetDuration(testcommon.TestTimeoutKey, testcommon.TimeoutDuration), time.Second).Should(Succeed())
		})
	})

	Context("Update ImageRegistryBinding with repositories", func() {
		JustBeforeEach(func() {
			Expect(remoteImageRepos.Items).ToNot(BeEmpty())
			// update image registry binding ,set repositories
			opts.Repositories = remoteImageRepos.Items
			imageRegistryBinding, err = testcommon.UpdateImageRegistryBinding(devopsClient, opts)
		})
		It("assign repositories correct", func() {
			By("Updated successfully!")
			Expect(err).To(BeNil())
			if len(remoteImageRepos.Items) == 0 {
				return
			}

			By("Image repositories are synced")
			var repositoryList *v1alpha1.ImageRepositoryList
			Eventually(func() (err error) {
				repositoryList, err = devopsClient.DevopsV1alpha1().ImageRepositories(opts.Namespace).List(metav1.ListOptions{ResourceVersion: "0"})
				if err == nil && len(repositoryList.Items) == 0 {
					err = fmt.Errorf("No image repositories synced yet!")
				}
				if len(repositoryList.Items) > 0 {
					err = nil
				}
				return
			}, testcommon.GetDuration(testcommon.TestTimeoutKey, testcommon.TimeoutDuration), time.Second).Should(Succeed())

			By("Getting image repository and tags")
			// find image repository
			imageRepositoryName := fmt.Sprintf("%s-%s-%s", opts.RegistryName, testcommon.GetValue(testcommon.HarborProjectKey), testcommon.GetValue(testcommon.HarborRepoKey))
			var imageRepository *v1alpha1.ImageRepository
			for _, repo := range repositoryList.Items {
				if imageRepositoryName == repo.Name {
					imageRepository = &repo
					break
				}
			}
			Expect(imageRepository).ToNot(BeNil())

			var tags []v1alpha1.ImageTag
			By("Waiting tag to sync")
			Eventually(func() (err error) {
				imageRepository, err = devopsClient.DevopsV1alpha1().ImageRepositories(opts.Namespace).Get(imageRepositoryName, metav1.GetOptions{})
				if err != nil {
					return
				}
				if imageRepository == nil || len(imageRepository.Status.Tags) == 0 {
					err = fmt.Errorf("ImageRepository \"%s\" has no tag yet", imageRepositoryName)
					return
				}
				tags = imageRepository.Status.Tags
				return nil
			}, testcommon.GetDuration(testcommon.TestTimeoutKey, testcommon.TimeoutDuration), time.Second).Should(Succeed())

			By("Trigger Image Scan")
			tagName := tags[0].Name
			imageScanOptions := &v1alpha1.ImageScanOptions{
				Tag: tagName,
			}
			imageScanResult, err = devopsClient.DevopsV1alpha1().ImageRepositories(opts.Namespace).ScanImage(imageRepositoryName, imageScanOptions)
			Expect(imageScanResult).ToNot(BeNil())

			By("Waiting Image to scan and get results")
			Eventually(func() (err error) {
				imageRepository, err = devopsClient.DevopsV1alpha1().ImageRepositories(opts.Namespace).Get(imageRepositoryName, metav1.GetOptions{})
				if err != nil && imageRepository != nil {
					tag := v1alpha1.ImageTag{}
					for _, t := range imageRepository.Status.Tags {
						if t.Name == tagName {
							tag = t
						}
					}
					if tag.ScanStatus == v1alpha1.ImageTagScanStatusFinished {
						return
					}
				}
				return
			}, testcommon.GetDuration(testcommon.TestTimeoutKey, testcommon.TimeoutDuration), time.Second).Should(Succeed())
		})
	})
})

var _ = Describe("Harbor.ImageRegistry Authorize", func() {
	//devopsClient.DevopsV1alpha1().ImageRegistries().Authorize(harbor.Name, )
	var (
		opts         testcommon.ImageRegistryBindingOpts
		authResponse *v1alpha1.CodeRepoServiceAuthorizeResponse
		authErr      error
	)

	BeforeEach(func() {
		opts = testcommon.ImageRegistryBindingOpts{
			Name:            testcommon.GenName("e2eharborbinderror-"),
			Namespace:       namespace.Name,
			RegistryName:    imageRegistry.Name,
			RegistryType:    string(v1alpha1.RegistryTypeHarbor),
			SecretName:      incorrectSecret.Name,
			SecretNamespace: incorrectSecret.Namespace,
			Repositories:    nil,
		}
	})

	JustBeforeEach(func() {
		// Create Binding use global secret
		authResponse, authErr = devopsClient.DevopsV1alpha1().ImageRegistries().Authorize(opts.RegistryName, &v1alpha1.CodeRepoServiceAuthorizeOptions{
			SecretName: opts.SecretName,
			Namespace:  opts.SecretNamespace,
		})
	})

	AfterEach(func() {
	})

	Context("Working harbor, correct secret", func() {
		BeforeEach(func() {
			opts.SecretName = correctSecretPrivate.Name
			opts.SecretNamespace = correctSecretPrivate.Namespace
		})
		It("should not get gitlab remote repositories", func() {
			Expect(authErr).To(BeNil())
			Expect(authResponse).ToNot(BeNil())
		})
	})

	Context("Working harbor, incorrect secret", func() {
		It("should return error on Authorize", func() {
			Expect(authErr).ToNot(BeNil())
		})
	})

	Context("unworking harbor, correct secret", func() {
		BeforeEach(func() {
			opts.RegistryName = unworkingImageRegistry.Name
		})
		It("should return error on Authorize", func() {
			Expect(authErr).ToNot(BeNil())
		})
	})
})
