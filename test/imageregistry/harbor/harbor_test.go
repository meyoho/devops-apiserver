package harbor

import (
	"testing"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	testcommon "alauda.io/devops-apiserver/test"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

var (
	devopsClient                versioned.Interface
	k8sClient                   kubernetes.Interface
	imageRegistry               *v1alpha1.ImageRegistry
	unworkingImageRegistry      *v1alpha1.ImageRegistry
	namespace                   *corev1.Namespace
	correctSecretGlobal         *corev1.Secret
	correctSecretPrivate        *corev1.Secret
	incorrectSecret             *corev1.Secret
	err                         error
	timeoutDuration             = time.Duration(time.Minute * 10)
	defaultCredentialsNamespace = "global-credentials"
)

func TestHarbor(t *testing.T) {
	testcommon.SetDefaults()
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("../../imageregistry-harbor.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "Harbor", []Reporter{junitReporter})
}

// Create harbor integration
var _ = BeforeSuite(func() {
	devopsClient, k8sClient = testcommon.MustClients("", "")

	namespace = testcommon.MustNamespace(devopsClient, k8sClient)

	By("Integrate working Harbor client and correct secret under the global for harbor")
	imageRegistry, correctSecretGlobal = testcommon.MustHarborAndSecret(devopsClient, k8sClient)

	By("Create correct secret under the namespace for harbor")
	secretName1, secretType1, secretData1 := testcommon.GetHarborCredentials()
	correctSecretPrivate, err = testcommon.CreateSecret(k8sClient, secretName1, namespace.Name, secretType1, secretData1)
	Expect(err).To(BeNil())
	Expect(correctSecretPrivate).ToNot(BeNil())

	By("Integrate unworking Harbor client")
	name, host := testcommon.GetHarborData()
	host = testcommon.GenName("https://sparrow") + ".host"
	unworkingImageRegistry, err = testcommon.CreateImageRegistry(devopsClient, name, "Harbor", host)
	Expect(err).To(BeNil())
	Expect(unworkingImageRegistry).ToNot(BeNil())

	By("Create incorrect secret for harbor")
	secretName, secretType, secretData := testcommon.GetHarborCredentials()
	secretData["username"] = "wronguser"
	secretData["password"] = "wronpassword"
	incorrectSecret, err = testcommon.CreateSecret(k8sClient, secretName, defaultCredentialsNamespace, secretType, secretData)
	Expect(err).To(BeNil())
	Expect(incorrectSecret).ToNot(BeNil())
})

// Delete harbor integration
var _ = AfterSuite(func() {
	if devopsClient == nil {
		return
	}
	By("Destruct Harbor")
	if unworkingImageRegistry != nil {
		err = devopsClient.DevopsV1alpha1().ImageRegistries().Delete(unworkingImageRegistry.Name, &metav1.DeleteOptions{})
		if err != nil {

		}
	}
	if correctSecretGlobal != nil {
		err = k8sClient.CoreV1().Secrets(correctSecretGlobal.Namespace).Delete(correctSecretGlobal.Name, &metav1.DeleteOptions{})
		if err != nil {
		}
	}
	if incorrectSecret != nil {
		err = k8sClient.CoreV1().Secrets(incorrectSecret.Namespace).Delete(incorrectSecret.Name, &metav1.DeleteOptions{})
		if err != nil {
		}
	}
	if namespace != nil {
		err = k8sClient.CoreV1().Namespaces().Delete(namespace.Name, &metav1.DeleteOptions{})
		if err != nil {
		}
	}
})
