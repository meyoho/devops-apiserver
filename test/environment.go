package test

import (
	"os"
	"time"
)

// Environment helper to fetch environment related data
type Environment struct {
	defaults map[string]string
}

// New returns an environment object
func New() Environment {
	return Environment{
		defaults: make(map[string]string),
	}
}

func (env *Environment) SetDefault(key, value string) {
	env.defaults[key] = value
}

func (env *Environment) GetValue(key string) (value string) {
	value = os.Getenv(key)
	if value == "" {
		value = env.defaults[key]
	}
	return
}

func (env *Environment) GetDuration(key string, defaultValue time.Duration) (value time.Duration) {
	stringValue := env.GetValue(key)
	var err error
	value, err = time.ParseDuration(stringValue)
	if err != nil {
		value = defaultValue
	}
	return
}
