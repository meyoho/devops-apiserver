package test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
)

const (
	defaultCredentialsNamespace = "global-credentials"
)

// MustClients must init devops and k8s clients
func MustClients(master, kubeconfig string) (devopsClient versioned.Interface, k8sClient kubernetes.Interface) {
	var err error
	By("Init DevOps client")
	devopsClient, err = GetDevOpsClient(master, kubeconfig)
	Expect(err).To(BeNil())
	Expect(devopsClient).ToNot(BeNil())

	By("Init K8s client")
	k8sClient, err = GetKubernetesClient(master, kubeconfig)
	Expect(err).To(BeNil())
	Expect(k8sClient).ToNot(BeNil())
	return
}

// MustNamespaceAndSecret must init namespace
func MustNamespace(devopsClient versioned.Interface, k8sClient kubernetes.Interface) (namespace *corev1.Namespace) {
	var err error
	By("Create Namespace")
	namespaceName := GetNamespaceData()
	namespace, err = CreateNamespace(k8sClient, namespaceName)
	Expect(err).To(BeNil())
	Expect(namespace).ToNot(BeNil())
	return
}

func MustHarborAndSecret(devopsClient versioned.Interface, k8sClient kubernetes.Interface) (imageRegistry *v1alpha1.ImageRegistry, correctSecret *corev1.Secret) {
	var err error
	By("Integrate Harbor client")
	name, host := GetHarborData()
	name = GetValue(HarborNameKey)
	imageRegistry, err = CreateImageRegistry(devopsClient, name, "Harbor", host)
	Expect(err).To(BeNil())
	Expect(imageRegistry).ToNot(BeNil())

	By("Create correct secret for harbor")
	secretName, secretType, secretData := GetHarborCredentials()
	secretNamespace := defaultCredentialsNamespace
	correctSecret, err = CreateSecret(k8sClient, secretName, secretNamespace, secretType, secretData)
	Expect(err).To(BeNil())
	Expect(correctSecret).ToNot(BeNil())
	return
}

func MustGitlabAndSecret(devopsClient versioned.Interface, k8sClient kubernetes.Interface) (coderepoService *v1alpha1.CodeRepoService, correctSecret *corev1.Secret) {
	var err error
	By("Integrate Gitlab client")
	name, host := GetGitlabData()
	name = GetValue(GitlabNameKey)
	coderepoService, err = CreateCodeRepoService(devopsClient, name, "Gitlab", host)
	Expect(err).To(BeNil())
	Expect(coderepoService).ToNot(BeNil())

	By("Create correct secret for gitlab")
	secretName, secretType, secretData := GetGitlabCredentials()
	secretNamespace := defaultCredentialsNamespace
	correctSecret, err = CreateSecret(k8sClient, secretName, secretNamespace, secretType, secretData)
	Expect(err).To(BeNil())
	Expect(correctSecret).ToNot(BeNil())
	return
}

func MustJenkinsAndSecret(devopsClient versioned.Interface, k8sClient kubernetes.Interface) (jenkins *v1alpha1.Jenkins, correctSecret *corev1.Secret, created bool) {
	var err error
	By("Integrate working Jenkins client")
	name, host := GetJenkinsData()
	name = WithDefault(GetValue(JenkinsNameKey), name)
	jenkins, err, created = CreateJenkins(devopsClient, name, host)
	Expect(err).To(BeNil())
	Expect(jenkins).ToNot(BeNil())

	By("Create correct secret for Jenkins")
	secretName, secretType, secretData := GetJenkinsCredentials()
	secretNamespace := defaultCredentialsNamespace
	correctSecret, err = CreateSecret(k8sClient, secretName, secretNamespace, secretType, secretData)
	Expect(err).To(BeNil())
	Expect(correctSecret).ToNot(BeNil())
	return
}
