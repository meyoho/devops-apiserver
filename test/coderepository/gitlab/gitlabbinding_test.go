package gitlab

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	testcommon "alauda.io/devops-apiserver/test"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("Gitlab.CoderepoServiceBinding GetRemoteRepositories", func() {
	var (
		opts            testcommon.CodeRepoBindingOpts
		remoteCodeRepos *v1alpha1.CodeRepoBindingRepositories
		coderepoBinding *v1alpha1.CodeRepoBinding
		err             error
	)

	BeforeEach(func() {
		opts = testcommon.CodeRepoBindingOpts{
			Name:            testcommon.GenName("e2egitlabbind-"),
			Namespace:       namespace.Name,
			CoderepoName:    coderepoService.Name,
			SecretName:      correctSecret.Name,
			SecretNamespace: correctSecret.Namespace,
			AccountName:     "root",
			Repositories:    nil,
			All:             false,
		}
	})

	JustBeforeEach(func() {
		// Create Gitlab Binding
		coderepoBinding, err = testcommon.CreateCodeRepoBinding(devopsClient, opts)
		Expect(err).To(BeNil())
		Expect(coderepoBinding).ToNot(BeNil())
		Expect(coderepoBinding.Name).ToNot(Equal(""))

		// Fetch remote repositories
		remoteCodeRepos, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(opts.Namespace).GetRemoteRepositories(coderepoBinding.Name, &v1alpha1.CodeRepoBindingRepositoryOptions{})
	})

	AfterEach(func() {
		// delete binding
		testcommon.DeleteCodeRepoBinding(devopsClient, coderepoBinding)
	})

	Context("Working gitlab, correct globalsecret", func() {
		It("check gitlab binding repository is working", func() {
			Expect(err).To(BeNil())
			Expect(remoteCodeRepos).ToNot(BeNil())
			Expect(remoteCodeRepos.Owners[0]).ToNot(BeNil())
			Expect(remoteCodeRepos.Owners[0].Name).To(ContainSubstring(env.GetValue(testcommon.GitlabRepositoryKey)))
		})
	})

	Context("Working gitlab, correct privatesecret in namespace2", func() {
		BeforeEach(func() {
			opts.Namespace = namespace2.Name
			opts.SecretName = correctSecretPrivate.Name
			opts.SecretNamespace = correctSecretPrivate.Namespace
		})
		It("check gitlab binding repository is working", func() {
			Expect(err).To(BeNil())
			Expect(remoteCodeRepos).ToNot(BeNil())
			Expect(remoteCodeRepos.Owners[0]).ToNot(BeNil())
			Expect(remoteCodeRepos.Owners[0].Name).To(ContainSubstring(env.GetValue(testcommon.GitlabRepositoryKey)))
		})
	})

	//TODO：优化数据判断
	Context("Update coderepoBinding with repositories", func() {
		BeforeEach(func() {
			opts.All = true
		})
		JustBeforeEach(func() {
			// update image registry binding ,set repositories
			coderepoBinding, err = testcommon.UpdateCodeRepoBinding(devopsClient, opts)
		})
		It("assign repositories correct", func() {
			By("Updated successfully!")
			Expect(err).To(BeNil())
			codeOwner := v1alpha1.CodeRepositoryOwner{}
			for _, owner := range remoteCodeRepos.Owners {
				if owner.Name == env.GetValue(testcommon.GitlabRepositoryKey) {
					codeOwner = owner
				}
			}
			if len(codeOwner.Repositories) == 0 {
				return
			}

			By("Gitlab repositories are synced")
			var repositoryList *v1alpha1.CodeRepositoryList
			Eventually(func() (err error) {
				repositoryList, err = devopsClient.DevopsV1alpha1().CodeRepositories(opts.Namespace).List(metav1.ListOptions{ResourceVersion: "0"})
				if err == nil && len(repositoryList.Items) == 0 {
					err = fmt.Errorf("No gitlab repositories synced yet!")
				}
				if len(repositoryList.Items) > 0 {
					err = nil
				}
				return
			}, env.GetDuration(testcommon.TestTimeoutKey, testcommon.TimeoutDuration)).Should(Succeed())
		})
	})
})

var _ = Describe("Gitlab.CodeRepoService Authorize", func() {
	var (
		opts         testcommon.CodeRepoBindingOpts
		authResponse *v1alpha1.CodeRepoServiceAuthorizeResponse
		authErr      error
	)

	BeforeEach(func() {
		opts = testcommon.CodeRepoBindingOpts{
			Name:            testcommon.GenName("e2egitlabbinderror-"),
			Namespace:       namespace.Name,
			CoderepoName:    coderepoService.Name,
			SecretName:      incorrectSecret.Name,
			SecretNamespace: incorrectSecret.Namespace,
			AccountName:     "root",
			Repositories:    nil,
			All:             false,
		}
	})

	JustBeforeEach(func() {
		authResponse, authErr = devopsClient.DevopsV1alpha1().CodeRepoServices().Authorize(opts.CoderepoName, &v1alpha1.CodeRepoServiceAuthorizeOptions{
			SecretName: opts.SecretName,
			Namespace:  opts.SecretNamespace,
		})
	})

	AfterEach(func() {
	})

	Context("Working gitlab, correct secret", func() {
		BeforeEach(func() {
			opts.SecretName = correctSecret.Name
			opts.SecretNamespace = correctSecret.Namespace
		})
		It("should not get gitlab remote repositories", func() {
			Expect(authErr).To(BeNil())
			Expect(authResponse).ToNot(BeNil())
		})
	})

	Context("Working gitlab, incorrect secret", func() {
		It("should return error on Authorize", func() {
			Expect(authErr).ToNot(BeNil())
		})
	})

	Context("unworking gitlab, correct secret", func() {
		BeforeEach(func() {
			opts.CoderepoName = unworkingCoderepoService.Name
		})
		It("should return error on Authorize", func() {
			Expect(authErr).ToNot(BeNil())
		})
	})
})
