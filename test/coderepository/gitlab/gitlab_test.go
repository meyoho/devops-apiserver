package gitlab

import (
	"testing"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	testcommon "alauda.io/devops-apiserver/test"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

var (
	devopsClient                versioned.Interface
	k8sClient                   kubernetes.Interface
	coderepoService             *v1alpha1.CodeRepoService
	unworkingCoderepoService    *v1alpha1.CodeRepoService
	namespace                   *corev1.Namespace
	namespace2                  *corev1.Namespace
	correctSecret               *corev1.Secret
	correctSecretPrivate        *corev1.Secret
	incorrectSecret             *corev1.Secret
	err                         error
	env                         = testcommon.New()
	timeoutDuration             = time.Duration(time.Minute * 10)
	defaultCredentialsNamespace = "global-credentials"
)

func TestGitlab(t *testing.T) {
	testcommon.SetDefaults()
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("../../codereposervice-gitlab.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "Gitlab", []Reporter{junitReporter})
}

// Create Gitlab Integration
var _ = BeforeSuite(func() {
	devopsClient, k8sClient = testcommon.MustClients("", "")

	namespace = testcommon.MustNamespace(devopsClient, k8sClient)
	namespace2 = testcommon.MustNamespace(devopsClient, k8sClient)

	By("Integrate working Gitlab client and correct secret under the global for gitlab")
	coderepoService, correctSecret = testcommon.MustGitlabAndSecret(devopsClient, k8sClient)

	By("Create correct secret under the namespace2 for gitlab")
	secretName1, secretType1, secretData1 := testcommon.GetGitlabCredentials()
	correctSecretPrivate, err = testcommon.CreateSecret(k8sClient, secretName1, namespace2.Name, secretType1, secretData1)
	Expect(err).To(BeNil())
	Expect(correctSecretPrivate).ToNot(BeNil())

	By("Integrate unworking Gitlab client")
	name, host := testcommon.GetGitlabData()
	host = testcommon.GenName("http://gitlab") + ".com"
	unworkingCoderepoService, err = testcommon.CreateCodeRepoService(devopsClient, name, "Gitlab", host)
	Expect(err).To(BeNil())
	Expect(unworkingCoderepoService).ToNot(BeNil())

	By("Create incorrect secret for gitlab")
	secretName, secretType, secretData := testcommon.GetGitlabCredentials()
	secretData["username"] = "wronguser"
	secretData["password"] = "wronpassword"
	incorrectSecret, err = testcommon.CreateSecret(k8sClient, secretName, defaultCredentialsNamespace, secretType, secretData)
	Expect(err).To(BeNil())
	Expect(incorrectSecret).ToNot(BeNil())

})

// Delete Gitlab integration
var _ = AfterSuite(func() {
	if devopsClient == nil {
		return
	}
	By("Destruct Gitlab")
	if unworkingCoderepoService != nil {
		err = devopsClient.DevopsV1alpha1().CodeRepoServices().Delete(unworkingCoderepoService.Name, &metav1.DeleteOptions{})
		if err != nil {

		}
	}
	if correctSecret != nil {
		err = k8sClient.CoreV1().Secrets(correctSecret.Namespace).Delete(correctSecret.Name, &metav1.DeleteOptions{})
		if err != nil {
		}
	}
	if incorrectSecret != nil {
		err = k8sClient.CoreV1().Secrets(incorrectSecret.Namespace).Delete(incorrectSecret.Name, &metav1.DeleteOptions{})
		if err != nil {
		}
	}
	if namespace != nil {
		err = k8sClient.CoreV1().Namespaces().Delete(namespace.Name, &metav1.DeleteOptions{})
		if err != nil {
		}
	}
	if namespace2 != nil {
		err = k8sClient.CoreV1().Namespaces().Delete(namespace2.Name, &metav1.DeleteOptions{})
		if err != nil {
		}
	}
})
