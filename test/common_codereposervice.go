package test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"github.com/ghodss/yaml"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// CreateCodeRepoService creates a CodeRepoService in APIServer and returns
func CreateCodeRepoService(devopsClient versioned.Interface, name, coderepoType, host string) (codeRepoService *v1alpha1.CodeRepoService, err error) {
	// first check if an existing codereposervice with the same host address exists, if not we should create
	coderepoServiceList, err := devopsClient.DevopsV1alpha1().CodeRepoServices().List(metav1.ListOptions{ResourceVersion: "0"})
	if coderepoServiceList != nil && len(coderepoServiceList.Items) > 0 {
		for _, item := range coderepoServiceList.Items {
			if item.Spec.HTTP.Host == host {
				codeRepoService = &item
				break
			}
		}
	}
	//already found one, just return
	if codeRepoService != nil && string(codeRepoService.Spec.Type) == coderepoType {
		err = nil
		return
	}

	// we need to create a new one
	var content []byte
	if content, err = LoadFile("../../data/codereposervice.yaml", map[string]string{
		"{metadata.name}":  name,
		"{spec.type}":      coderepoType,
		"{spec.http.host}": host,
	}); err != nil {
		return
	}
	codeRepoService = new(v1alpha1.CodeRepoService)
	if err = yaml.Unmarshal(content, codeRepoService); err != nil {
		return
	}
	codeRepoService, err = devopsClient.DevopsV1alpha1().CodeRepoServices().Create(codeRepoService)
	return
}

type CodeRepoBindingOpts struct {
	Name            string
	Namespace       string
	CoderepoName    string
	SecretName      string
	SecretNamespace string
	AccountName     string
	Repositories    []string
	All             bool
}

// CreateCodeRepoBinding creates a CodeRepoServiceBinding in APIServer and returns
func CreateCodeRepoBinding(devopsClient versioned.Interface, opts CodeRepoBindingOpts) (codeRepoBinding *v1alpha1.CodeRepoBinding, err error) {
	codeRepoBinding, err = getCodeRepoBinding(opts)
	if err != nil {
		return
	}
	// codeRepoBinding.Spec.Account.Owners.Repositories = repositories
	codeRepoBinding.Spec.Account.Owners[0].All = opts.All
	codeRepoBinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(opts.Namespace).Create(codeRepoBinding)
	return
}

// UpdateCodeRepoBinding update a CodeRepoBinding in APIServer and returns
func UpdateCodeRepoBinding(devopsClient versioned.Interface, opts CodeRepoBindingOpts) (codeRepoBinding *v1alpha1.CodeRepoBinding, err error) {
	data, err := getCodeRepoBinding(opts)
	if err != nil {
		return
	}
	codeRepoBinding, err = getLatestAndReplaceSpec(devopsClient, opts, data)
	if err != nil {
		return
	}
	// codeRepoBinding.Spec.Account.Owners.Repositories = repositories
	codeRepoBinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(opts.Namespace).Update(codeRepoBinding)
	if err != nil && errors.IsConflict(err) {
		codeRepoBinding, err = UpdateCodeRepoBinding(devopsClient, opts)
	}
	return
}

func getLatestAndReplaceSpec(devopsClient versioned.Interface, opts CodeRepoBindingOpts, data *v1alpha1.CodeRepoBinding) (codeRepoBinding *v1alpha1.CodeRepoBinding, err error) {
	codeRepoBinding, err = devopsClient.DevopsV1alpha1().CodeRepoBindings(opts.Namespace).Get(opts.Name, metav1.GetOptions{})
	if err != nil {
		return
	}
	codeRepoBinding = codeRepoBinding.DeepCopy()
	codeRepoBinding.Spec = data.Spec
	return
}

func getCodeRepoBinding(opts CodeRepoBindingOpts) (codeRepoBinding *v1alpha1.CodeRepoBinding, err error) {
	var content []byte
	if content, err = LoadFile("../../data/coderepobinding.yaml", map[string]string{
		"{metadata.name}":                 opts.Name,
		"{metadata.namespace}":            opts.Namespace,
		"{codereposervice.name}":          opts.CoderepoName,
		"{spec.account.secret.name}":      opts.SecretName,
		"{spec.account.secret.namespace}": opts.SecretNamespace,
		"{account.name}":                  opts.AccountName,
	}); err != nil {
		return
	}
	codeRepoBinding = new(v1alpha1.CodeRepoBinding)
	if err = yaml.Unmarshal(content, codeRepoBinding); err != nil {
		return
	}
	return
}

func DeleteCodeRepoBinding(devopsClient versioned.Interface, coderepoBinding *v1alpha1.CodeRepoBinding) (err error) {
	if devopsClient == nil || coderepoBinding == nil {
		return
	}
	err = devopsClient.DevopsV1alpha1().CodeRepoBindings(coderepoBinding.Namespace).Delete(coderepoBinding.Name, &metav1.DeleteOptions{})
	return
}
