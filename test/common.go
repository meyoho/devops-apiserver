package test

import (
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"bytes"
	"fmt"
	"io/ioutil"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/storage/names"
	"k8s.io/client-go/kubernetes"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"os"
	"strings"
	"text/template"
)

func GetNamespaceData() (name string) {
	name = GenName("e2enamespace-")
	return
}

func GetDevOpsClient(masterURL, kubeConfig string) (client versioned.Interface, err error) {
	config, configErr := getConfig(masterURL, kubeConfig)
	if configErr != nil {
		err = configErr
		return
	}
	return versioned.NewForConfig(config)
}

func GetKubernetesClient(masterURL, kubeConfig string) (client kubernetes.Interface, err error) {
	config, configErr := getConfig(masterURL, kubeConfig)
	if configErr != nil {
		err = configErr
		return
	}
	return kubernetes.NewForConfig(config)
}

func getConfig(masterURL, kubeConfig string) (config *restclient.Config, err error) {
	home := os.Getenv("HOME")
	if kubeConfig == "" && home != "" {
		kubeConfigPath := fmt.Sprintf("%s/.kube/config", home)
		if _, err = os.Stat(kubeConfigPath); err == nil {
			kubeConfig = kubeConfigPath
		}
	}
	return clientcmd.BuildConfigFromFlags(masterURL, kubeConfig)
}

// LoadFile loads a file from the simples and replace content using a key/value map
func LoadFile(path string, replaces map[string]string) (content []byte, err error) {
	if content, err = ioutil.ReadFile(path); err != nil {
		return
	}
	contentStr := string(content)
	if len(replaces) > 0 {
		for key, value := range replaces {
			contentStr = strings.Replace(contentStr, key, value, -1)
		}
	}
	content = []byte(contentStr)
	return
}

// LoadTemplate loads a template from the file system and renders using the data parameter
func LoadTemplate(path string, data interface{}) (content []byte, err error) {
	if content, err = ioutil.ReadFile(path); err != nil {
		return
	}
	defer func() {
		if err != nil {
			content = nil
		}
	}()
	var (
		temp *template.Template
		buff *bytes.Buffer
	)
	if temp, err = template.New(path).Parse(string(content)); err != nil {
		return
	}
	buff = new(bytes.Buffer)
	if err = temp.Execute(buff, data); err != nil {
		return
	}
	content = buff.Bytes()
	return
}

// CreateSecret creates a secret
func CreateSecret(k8sClient kubernetes.Interface, name, namespace, secretType string, dataString map[string]string) (secret *corev1.Secret, err error) {
	secret, err = k8sClient.CoreV1().Secrets(namespace).Get(name, metav1.GetOptions{})
	if err != nil && errors.IsNotFound(err) {
		secret = &corev1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      name,
				Namespace: namespace,
			},
			Type:       corev1.SecretType(secretType),
			StringData: dataString,
		}
		secret, err = k8sClient.CoreV1().Secrets(namespace).Create(secret)
	} else if secret != nil && string(secret.Type) == secretType {
		secret = secret.DeepCopy()
		secret.StringData = dataString
		secret, err = k8sClient.CoreV1().Secrets(namespace).Update(secret)
	}
	return
}

func GetSecretList(k8sClient kubernetes.Interface, namespace string) (secretList *corev1.SecretList, err error) {
	secretList, err = k8sClient.CoreV1().Secrets(namespace).List(metav1.ListOptions{ResourceVersion: "0"})
	return
}

// CreateNamespace creates a namespace
func CreateNamespace(k8sClient kubernetes.Interface, name string) (namespace *corev1.Namespace, err error) {
	namespace, err = k8sClient.CoreV1().Namespaces().Get(name, metav1.GetOptions{})
	if err != nil && errors.IsNotFound(err) {
		namespace = &corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
		}
		namespace, err = k8sClient.CoreV1().Namespaces().Create(namespace)
	}
	return
}

func GenName(prefix string) (name string) {
	return names.SimpleNameGenerator.GenerateName(prefix)
}

type DeploymentOpts struct {
	Name      string
	Namespace string
	Container string
	Image     string
}

func CreateDeployment(client kubernetes.Interface, opts DeploymentOpts) (app *appsv1.Deployment, err error) {
	labels := map[string]string{
		"app": opts.Name,
	}
	count := int32(1)
	app = &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      opts.Name,
			Namespace: opts.Namespace,
			Labels:    labels,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &count,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						corev1.Container{
							Name:            opts.Container,
							Image:           opts.Image,
							ImagePullPolicy: corev1.PullAlways,
						},
					},
				},
			},
			Strategy:             appsv1.DeploymentStrategy{},
			RevisionHistoryLimit: &count,
		},
	}
	app, err = client.AppsV1().Deployments(opts.Namespace).Create(app)
	return
}

func GetDeployment(client kubernetes.Interface, name, namespace string) (app *appsv1.Deployment, err error) {
	app, err = client.AppsV1().Deployments(namespace).Get(name, metav1.GetOptions{ResourceVersion: "0"})
	return
}

func AddMap(target, source map[string]string) map[string]string {
	for k, v := range source {
		target[k] = v
	}
	return target
}
