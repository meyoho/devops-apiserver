package test

import (
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	"github.com/ghodss/yaml"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// CreateJenkins creates a Jenkins in APIServer and returns
func CreateJenkins(devopsClient versioned.Interface, name, host string) (jenkins *v1alpha1.Jenkins, err error, created bool) {
	// first check if an existing Jenkins with the save host address exists, if not we should create
	By("Look for jenkins with host \"" + host + "\"")
	jenkinsList, err := devopsClient.DevopsV1alpha1().Jenkinses().List(metav1.ListOptions{ResourceVersion: "0"})
	if jenkinsList != nil && len(jenkinsList.Items) > 0 {
		for _, item := range jenkinsList.Items {
			if item.Spec.HTTP.Host == host {
				jenkins = &item
				break
			}
		}
	}
	// already found one, just return
	if jenkins != nil {
		By("Using existing jenkins " + jenkins.Name)
		err = nil
		return
	}
	// we need to ceate a new one
	var content []byte
	if content, err = LoadFile("../../data/jenkins.yaml", map[string]string{
		"{metadata.name}":  name,
		"{spec.http.host}": host,
	}); err != nil {
		return
	}

	jenkins = new(v1alpha1.Jenkins)
	if err = yaml.Unmarshal(content, jenkins); err != nil {
		return
	}
	jenkins, err = devopsClient.DevopsV1alpha1().Jenkinses().Create(jenkins)
	created = true
	return
}

type JenkinsBindingOpts struct {
	Name            string
	Namespace       string
	JenkinsName     string
	SecretName      string
	SecretNamespace string
}

func CreateJenkinsBinding(devopsClient versioned.Interface, opts JenkinsBindingOpts, timeout time.Duration) (jenkinsBinding *v1alpha1.JenkinsBinding, err error) {
	jenkinsBinding, err = getJenkinsBinding(opts)
	if err != nil {
		return
	}
	var jenkinsReturn *v1alpha1.JenkinsBinding
	Eventually(func() error {
		jenkinsReturn, err = devopsClient.DevopsV1alpha1().JenkinsBindings(opts.Namespace).Create(jenkinsBinding)
		return err
	}, timeout).Should(Succeed())
	jenkinsBinding = jenkinsReturn
	return
}

func getJenkinsBinding(opts JenkinsBindingOpts) (jenkinsBinding *v1alpha1.JenkinsBinding, err error) {
	var content []byte
	if content, err = LoadFile("../../data/jenkinsbinding.yaml", map[string]string{
		"{metadata.name}":                 opts.Name,
		"{metadata.namespace}":            opts.Namespace,
		"{spec.jenkins.name}":             opts.JenkinsName,
		"{spec.account.secret.name}":      opts.SecretName,
		"{spec.account.secret.namespace}": opts.SecretNamespace,
	}); err != nil {
		return
	}
	jenkinsBinding = new(v1alpha1.JenkinsBinding)
	if err = yaml.Unmarshal(content, jenkinsBinding); err != nil {
		return
	}
	return
}

type PipelineConfigOpts struct {
	Name               string
	Namespace          string
	JenkinsBindingName string
	RunPolicy          v1alpha1.PipelineRunPolicy
	Jenkinsfile        string
	JenkinsfilePath    string
	GitURL             string
	GitBranch          string
	SecretName         string
	CodeRepositoryName string
	SourceType         string
	Triggers           []v1alpha1.PipelineTrigger

	Template *v1alpha1.PipelineConfigTemplate
}

func CreatePipelineConfig(devopsClient versioned.Interface, opts PipelineConfigOpts) (pipelineConfig *v1alpha1.PipelineConfig, err error) {
	pipelineConfig, err = getPipelineConfig(opts)
	if err != nil {
		return
	}
	if opts.Template != nil {
		if opts.Template.Spec.Environments == nil {
			opts.Template.Spec.Environments = []v1alpha1.PipelineEnvironment{}
		}
		opts.Template.Spec.Environments = append(opts.Template.Spec.Environments, v1alpha1.PipelineEnvironment{
			Name: "ALAUDA_PROJECT", Value: opts.Namespace,
		})
		pipelineConfig.Spec.Strategy.Template = opts.Template
	}

	pipelineConfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(opts.Namespace).Create(pipelineConfig)
	return
}
func UpdatePipelineConfig(devopsClient versioned.Interface, opts PipelineConfigOpts) (pipelineConfig *v1alpha1.PipelineConfig, err error) {
	pipelineConfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(opts.Namespace).Get(opts.Name, metav1.GetOptions{})
	if err != nil {
		return
	}
	pipelineConfig = pipelineConfig.DeepCopy()
	pipelineConfig.Spec.Triggers = opts.Triggers
	pipelineConfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(opts.Namespace).Update(pipelineConfig)
	return
}

func getPipelineConfig(opts PipelineConfigOpts) (pipelineConfig *v1alpha1.PipelineConfig, err error) {
	var content []byte
	if content, err = LoadTemplate("../../data/pipelineconfig-jenkinsfile.yaml", opts); err != nil {
		return
	}
	pipelineConfig = new(v1alpha1.PipelineConfig)
	if err = yaml.Unmarshal(content, pipelineConfig); err != nil {
		return
	}

	return
}

func DeletePipelineConfig(devopsClient versioned.Interface, config *v1alpha1.PipelineConfig) (err error) {
	if devopsClient == nil || config == nil {
		return
	}
	err = devopsClient.DevopsV1alpha1().PipelineConfigs(config.Namespace).Delete(config.Name, &metav1.DeleteOptions{})
	return
}
