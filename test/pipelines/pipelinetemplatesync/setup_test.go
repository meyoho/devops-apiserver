package pipelinetemplatesync_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	testcommon "alauda.io/devops-apiserver/test"
	testdenpend "alauda.io/devops-apiserver/test/denpendency"
	"alauda.io/devops-apiserver/test/utils"
	"context"
	"flag"
	"fmt"
	"github.com/ghodss/yaml"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"io/ioutil"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/klog"
	"os"
	"testing"
	"time"

	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/client-go/kubernetes"
	//ospath "path"
	//osfilepath "path/filepath"
)

func TestPipelineTemplateSync(t *testing.T) {
	flag.Parse()
	klog.InitFlags(flag.CommandLine)
	testcommon.SetDefaults()
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("../../pipelines-templatesync.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "PipelineTemplateSync", []Reporter{junitReporter})
}

type caseIdentity string

func (c caseIdentity) String() string {
	return string(c)
}

const (
	// indentity of PipelineTemplaets, will use this indentity as folder name and set kind of resource to PipelineTemplates
	CasePipelineTemplates        caseIdentity = "pipeline-templates"
	CaseClusterPipelineTemplates caseIdentity = "cluster-pipeline-templates"
)

var Options = options{

	RepoUserName: "jtcheng",
	RepoPassword: "T6FFXyRxy7NNsGVWRzVf",

	PipelineTemplateRepo:        "https://jtcheng:T6FFXyRxy7NNsGVWRzVf@gitlab.alauda.cn/intergration-testing/pipeline-templates",
	ClusterPipelineTemplateRepo: "https://jtcheng:T6FFXyRxy7NNsGVWRzVf@gitlab.alauda.cn/intergration-testing/cluster-pipeline-templates",

	Namespace: testcommon.GetNamespaceData(),
}

var Denpendency = testdenpend.Denpendency{
	K8sResource: &testdenpend.K8sResource{
		Ref: &testdenpend.K8sResourceRef{
			Name: Options.Namespace,
			Kind: "namespace",
		},
	},
}

type options struct {
	DevopsClient versioned.Interface
	K8sClient    kubernetes.Interface

	RepoUserName string
	RepoPassword string
	// 项目级别模板仓库
	PipelineTemplateRepo string
	// 平台级别模板仓库
	ClusterPipelineTemplateRepo string
	// namespace that will create PipelineTemplateSync into
	Namespace string
}

// BeforeSuite will import all pipelinetemplates into git repo
var _ = BeforeSuite(func() {

	Options.DevopsClient, Options.K8sClient = testcommon.MustClients("", "")
	Denpendency.K8sResource.Set(Options.K8sClient)

	err := Denpendency.Init()
	Expect(err).Should(BeNil())

	removeGitRepoTemplatesData(CasePipelineTemplates)
	removeGitRepoTemplatesData(CaseClusterPipelineTemplates)
	initGitRepoTemplatesData(CasePipelineTemplates)
	initGitRepoTemplatesData(CaseClusterPipelineTemplates)
})

// AfterSuite will reset git repo to firstly commit
var _ = AfterSuite(func() {
	//resetGitRepoTemplatesData(CasePipelineTemplates)
	//resetGitRepoTemplatesData(CaseClusterPipelineTemplates)
	removeGitRepoTemplatesData(CasePipelineTemplates)
	removeGitRepoTemplatesData(CaseClusterPipelineTemplates)
	checkoutGitRepoTemplatesData(CasePipelineTemplates)
	checkoutGitRepoTemplatesData(CaseClusterPipelineTemplates)
	deleteE2EClusterCustomerTemplate(Options.DevopsClient)
	deleteTemplateSync(CaseClusterPipelineTemplates, Options.DevopsClient)

	err := Denpendency.Destroy()
	Expect(err).Should(BeNil())
})

func runCmd(workIngDir string, name string, args ...string) (stdout, stderr string, err error) {
	ctx := context.TODO()
	stdout, stderr, err = utils.RunCmd(ctx, workIngDir, name, args...)
	if err != nil {
		klog.Errorf("stdout: %s, stderr: %s", stdout, stderr)
	}
	Expect(err).To(BeNil())
	return stdout, stderr, err
}

func removeGitRepoTemplatesData(caseName caseIdentity) {
	path := "./data/" + caseName.String() + "/.git"
	err := os.RemoveAll(path)
	if err != nil {
		klog.Errorf("remove %s error:%s", path, err.Error())
	}
}

func initGitRepoTemplatesData(caseName caseIdentity) {

	repoAddress := getRepoAddressByName(caseName)

	runCmd("./data/"+caseName.String(), "git", "init", ".")

	runCmd("./data/"+caseName.String(), "git", "remote", "add", "origin", repoAddress)

	runCmd("./data/"+caseName.String(), "git", "config", "user.email", "e2e.user.email")
	runCmd("./data/"+caseName.String(), "git", "config", "user.name", "e2e.user.name")

	runCmd("./data/"+caseName.String(), "git", "add", ".")

	runCmd("./data/"+caseName.String(), "git", "commit", "-m", "init templates")

	runCmd("./data/"+caseName.String(), "git", "push", "-f", "origin", "master")
}

func resetGitRepoTemplatesData(caseName caseIdentity) {

	stdout, _, _ := runCmd("./data/"+caseName.String(), "git", "rev-list", "--max-parents=0", "HEAD")
	Expect(stdout).ToNot(BeEmpty())

	firstCommitID := stdout
	runCmd("./data/"+caseName.String(), "git", "reset", "--hard", firstCommitID)

	runCmd("./data/"+caseName.String(), "git", "push", "-f", "origin", "master")
}

func checkoutGitRepoTemplatesData(caseName caseIdentity) {
	_, _, _ = runCmd("./data/"+caseName.String(), "git", "checkout", ".")
}

func getRepoAddressByName(name caseIdentity) string {
	Expect([]string{
		"pipeline-templates", "cluster-pipeline-templates",
	}).To(ContainElement(name.String()))

	repoAddress := Options.PipelineTemplateRepo
	if name == "cluster-pipeline-templates" {
		repoAddress = Options.ClusterPipelineTemplateRepo
	}
	return repoAddress
}

func getTemplateSyncKindByName(name caseIdentity) string {
	Expect([]string{
		"pipeline-templates", "cluster-pipeline-templates",
	}).To(ContainElement(name.String()))

	if name == "pipeline-templates" {
		return v1alpha1.TypePipelineTemplateSync
	}
	return v1alpha1.TypeClusterPipelineTemplateSync
}

func buildTemplateSync(kind string, repoAddress string) v1alpha1.PipelineTemplateSyncInterface {
	var typeMeta = metav1.TypeMeta{APIVersion: v1alpha1.APIVersionV1Alpha1, Kind: kind}
	var objectMeta = metav1.ObjectMeta{}
	var spec = v1alpha1.PipelineTemplateSyncSpec{
		Source: v1alpha1.PipelineSource{
			SourceType: v1alpha1.PipelineSourceTypeGit,
			Git: &v1alpha1.PipelineSourceGit{
				URI: repoAddress,
				Ref: "master",
			},
		},
	}
	var status = v1alpha1.PipelineTemplateSyncStatus{Phase: v1alpha1.PipelineTemplateSyncPhasePending}

	if kind == v1alpha1.TypeClusterPipelineTemplateSync {
		objectMeta.Name = "ClusterTemplateSync"
		var sync = v1alpha1.ClusterPipelineTemplateSync{
			TypeMeta:   typeMeta,
			ObjectMeta: objectMeta,
			Spec:       spec,
			Status:     &status,
		}
		return &sync
	}

	objectMeta.Namespace = Options.Namespace
	objectMeta.Name = "TemplateSync"
	var sync = v1alpha1.PipelineTemplateSync{
		TypeMeta:   typeMeta,
		ObjectMeta: objectMeta,
		Spec:       spec,
		Status:     &status,
	}
	return &sync
}

// applyTemplateSync name should be pipeline-templates or cluster-pipeline-templates
func applyTemplateSync(caseName caseIdentity, devopsClient versioned.Interface) v1alpha1.PipelineTemplateSyncInterface {
	repoAddress := getRepoAddressByName(caseName)
	syncKind := getTemplateSyncKindByName(caseName)

	templateSyncInter := buildTemplateSync(syncKind, repoAddress)
	namespace := templateSyncInter.GetNamespace()

	if caseName == CasePipelineTemplates {
		templateSync := templateSyncInter.(*v1alpha1.PipelineTemplateSync)
		syncCreated, err := devopsClient.DevopsV1alpha1().PipelineTemplateSyncs(namespace).Get(templateSync.Name, metav1.GetOptions{})
		if k8serrors.IsNotFound(err) {
			sync, err := devopsClient.DevopsV1alpha1().PipelineTemplateSyncs(namespace).Create(templateSync)
			Expect(err).Should(BeNil())
			return sync
		}

		Expect(err).Should(BeNil())

		copy := syncCreated.DeepCopy()
		copy.Status.Phase = v1alpha1.PipelineTemplateSyncPhasePending
		sync, err := devopsClient.DevopsV1alpha1().PipelineTemplateSyncs(namespace).Update(copy)
		Expect(err).Should(BeNil())
		return sync
	}

	templateSync := templateSyncInter.(*v1alpha1.ClusterPipelineTemplateSync)
	syncCreated, err := devopsClient.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Get(templateSync.Name, metav1.GetOptions{})
	if k8serrors.IsNotFound(err) {
		sync, err := devopsClient.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Create(templateSync)
		Expect(err).Should(BeNil())
		return sync
	}

	Expect(err).Should(BeNil())

	copy := syncCreated.DeepCopy()
	copy.Status.Phase = v1alpha1.PipelineTemplateSyncPhasePending
	sync, err := devopsClient.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Update(copy)
	Expect(err).Should(BeNil())
	return sync
}

// deleteTemplateSync name should be pipeline-templates or cluster-pipeline-templates
func deleteTemplateSync(caseName caseIdentity, devopsClient versioned.Interface) {
	repoAddress := getRepoAddressByName(caseName)
	syncKind := getTemplateSyncKindByName(caseName)

	if caseName == CasePipelineTemplates {
		templateSyncInter := buildTemplateSync(syncKind, repoAddress)
		namespace := templateSyncInter.GetNamespace()
		templateSync := templateSyncInter.(*v1alpha1.PipelineTemplateSync)
		err := devopsClient.DevopsV1alpha1().PipelineTemplateSyncs(namespace).Delete(templateSync.Name, &metav1.DeleteOptions{})
		Expect(err).Should(BeNil())
		return
	}

	templateSyncInter := buildTemplateSync(syncKind, repoAddress)
	templateSync := templateSyncInter.(*v1alpha1.ClusterPipelineTemplateSync)
	err := devopsClient.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Delete(templateSync.Name, &metav1.DeleteOptions{})
	Expect(err).Should(BeNil())
}

// waitUntilTemplateSyncReady will wait until TemplateSync status change to ready
func waitUntilTemplateSyncReady(caseName caseIdentity, devopsClient versioned.Interface) {
	syncInter := buildTemplateSync(getTemplateSyncKindByName(caseName), getRepoAddressByName(caseName))
	namespace := syncInter.GetNamespace()

	Eventually(func() error {

		if caseName == CasePipelineTemplates {
			sync, err := devopsClient.DevopsV1alpha1().PipelineTemplateSyncs(namespace).Get(syncInter.GetName(), metav1.GetOptions{})
			if err != nil {
				return err
			}
			if sync.Status == nil {
				return fmt.Errorf("status is not ready, sync: %#v", sync)
			}
			if sync.Status.Phase == v1alpha1.StatusReady {
				return nil
			}
			//klog.Infof("PipelineTemplateSync %s/%s status.phase is %s", sync.GetNamespace(), sync.GetName(), sync.Status.Phase)
			return fmt.Errorf("status is not ready, sync: %#v", sync)
		}

		sync, err := devopsClient.DevopsV1alpha1().ClusterPipelineTemplateSyncs().Get(syncInter.GetName(), metav1.GetOptions{})
		if err != nil {
			return err
		}
		if sync.Status == nil {
			return fmt.Errorf("status is not ready, sync: %#v", sync)
		}
		if sync.Status.Phase == v1alpha1.PipelineTemplateSyncPhaseError {
			klog.Errorf("Sync status:%#v", sync.Status)
		}
		Expect(sync.Status.Phase).ShouldNot(BeEquivalentTo(v1alpha1.PipelineTemplateSyncPhaseError))
		if sync.Status.Phase == v1alpha1.StatusReady {
			return nil
		}
		//klog.Infof("ClusterPipelineTemplateSync %s status.phase is %s", sync.GetName(), sync.Status.Phase)
		return fmt.Errorf("status is not ready, sync: %#v", sync)

	}, 3*time.Minute).Should(Succeed())
}

func expectContains(conditions []v1alpha1.PipelineTemplateSyncCondition, syncStatus string, templateName string, kind string, version string) {
	for _, item := range conditions {
		if item.Name == templateName {

			msg := fmt.Sprintf("Should contains %s %s/%s:%s in conditions: %#v", syncStatus, kind, templateName, version, conditions)
			Expect(item.Status).Should(BeEquivalentTo(syncStatus), msg)
			Expect(item.Type).Should(BeEquivalentTo(kind), msg)
			Expect(item.Version).Should(BeEquivalentTo(version), msg)
			return
		}
	}
	Fail(fmt.Sprintf("Should contains %s %s/%s:%s in conditions: %#v", syncStatus, kind, templateName, version, conditions))
}

func editVersionAndGitPush(caseName caseIdentity, relativePath string, version string, editF func(kind string, content []byte) interface{}) {
	path := "./data/" + caseName.String() + "/" + relativePath
	dir, err := os.Getwd()
	Expect(err).Should(BeNil())
	klog.Infof("PWD is %s", dir)

	byts, err := ioutil.ReadFile(path)
	Expect(err).Should(BeNil())

	data := &map[string]interface{}{}
	err = yaml.Unmarshal(byts, data)
	Expect(err).Should(BeNil())
	kind := fmt.Sprint((*data)["kind"])

	modifiedObj := editF(kind, byts)

	byts, err = yaml.Marshal(modifiedObj)
	Expect(err).Should(BeNil())

	err = ioutil.WriteFile(path, byts, 064)
	Expect(err).Should(BeNil())

	// GIT Commit
	runCmd("./data/"+caseName.String(), "git",
		"commit", "-a", "-m", fmt.Sprintf("change %s version to %s", relativePath, version))

	// Git PUSH
	runCmd("./data/"+caseName.String(), "git", "push", "-f", "origin", "master")
}

func updateOneTaskTemplate(caseName caseIdentity, filename string, version string) {

	editVersionAndGitPush(caseName, "tasks/"+filename, version, func(kind string, content []byte) interface{} {
		template := v1alpha1.NewPipelineTaskTemplate(kind)
		err := yaml.Unmarshal(content, template)
		Expect(err).Should(BeNil())

		ano := template.GetAnnotations()
		ano["version"] = version
		template.SetAnnotations(ano)
		return template
	})

}

func updateOneTemplate(caseName caseIdentity, filename string, version string) {
	editVersionAndGitPush(caseName, "templates/"+filename, version, func(kind string, content []byte) interface{} {
		template := v1alpha1.NewPipelineTemplate(kind)
		err := yaml.Unmarshal(content, template)
		Expect(err).Should(BeNil())

		ano := template.GetAnnotations()
		ano["version"] = version
		template.SetAnnotations(ano)
		return template
	})
}

func deleteOneFileAndGitPush(caseName caseIdentity, relativePath string) {
	path := "./data/" + caseName.String() + "/" + relativePath

	err := os.Remove(path)
	Expect(err).Should(BeNil())

	// GIT rm
	ctx := context.TODO()
	stdout, stderr, err := utils.RunCmd(ctx, "./data/"+caseName.String(), "git",
		"rm", relativePath)
	if err != nil {
		klog.Errorf("stdout: %s, stderr: %s", stdout, stderr)
	}
	Expect(err).Should(BeNil())

	// GIT Commit
	ctx = context.TODO()
	stdout, stderr, err = utils.RunCmd(ctx, "./data/"+caseName.String(), "git",
		"commit", "-a", "-m", fmt.Sprintf("rm template %s", relativePath))
	if err != nil {
		klog.Errorf("stdout: %s, stderr: %s", stdout, stderr)
	}
	Expect(err).Should(BeNil())

	// Git PUSH
	ctx = context.TODO()
	stdout, stderr, err = utils.RunCmd(ctx, "./data/"+caseName.String(), "git", "push", "-f", "origin", "master")
	if err != nil {
		klog.Errorf("stdout: %s, stderr: %s", stdout, stderr)
	}
	Expect(err).Should(BeNil())
}

func deleteOneTaskTemplate(caseName caseIdentity, filename string) {
	deleteOneFileAndGitPush(caseName, "tasks/"+filename)
}
func deleteOneTemplate(caseName caseIdentity, filename string) {
	deleteOneFileAndGitPush(caseName, "templates/"+filename)
}

func deleteE2EClusterCustomerTemplate(devopsClient versioned.Interface) {
	labelSelector := metav1.LabelSelector{MatchLabels: map[string]string{
		"e2etest": "true",
	}}
	selector, err := metav1.LabelSelectorAsSelector(&labelSelector)
	Expect(err).Should(BeNil())
	err = devopsClient.DevopsV1alpha1().ClusterPipelineTemplates().DeleteCollection(
		&metav1.DeleteOptions{},
		metav1.ListOptions{LabelSelector: selector.String()},
	)
	Expect(err).Should(BeNil())

	err = devopsClient.DevopsV1alpha1().ClusterPipelineTaskTemplates().DeleteCollection(
		&metav1.DeleteOptions{},
		metav1.ListOptions{LabelSelector: selector.String()},
	)
	Expect(err).Should(BeNil())
}

func expectTemplateExists(devopsClient versioned.Interface, kind, namespace, templateName string) {
	template, err := versioned.NewExpansion(devopsClient).DevopsV1alpha1Expansion().PipelineTemplateInterface().Get(kind, namespace, templateName, metav1.GetOptions{ResourceVersion: "0"})
	Expect(err).Should(BeNil())
	Expect(template).ShouldNot(BeNil())
}
func expectTemplateNotExists(devopsClient versioned.Interface, kind, namespace, templateName string) {
	_, err := versioned.NewExpansion(devopsClient).DevopsV1alpha1Expansion().PipelineTemplateInterface().Get(kind, namespace, templateName, metav1.GetOptions{ResourceVersion: "0"})
	msg := fmt.Sprintf("%s %s/%s should not exists", kind, namespace, templateName)
	Expect(err).ShouldNot(BeNil(), msg)
	Expect(k8serrors.IsNotFound(err)).Should(BeTrue(), msg)
}
func expectTaskTemplateExists(devopsClient versioned.Interface, kind, namespace, templateName string) {
	taskTemplate, err := versioned.NewExpansion(devopsClient).DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Get(kind, namespace, templateName, metav1.GetOptions{ResourceVersion: "0"})

	Expect(err).Should(BeNil())
	Expect(taskTemplate).ShouldNot(BeNil())
}

func expectTaskTemplateNotExists(devopsClient versioned.Interface, kind, namespace, templateName string) {
	_, err := versioned.NewExpansion(devopsClient).DevopsV1alpha1Expansion().PipelineTaskTemplateInterface().Get(kind, namespace, templateName, metav1.GetOptions{ResourceVersion: "0"})
	msg := fmt.Sprintf("%s %s/%s should not exists, err: %#v", kind, namespace, templateName, err)
	Expect(err).ShouldNot(BeNil(), msg)
	Expect(k8serrors.IsNotFound(err)).Should(BeTrue(), msg)
}
