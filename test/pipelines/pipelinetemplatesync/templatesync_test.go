package pipelinetemplatesync_test

import (
	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("PipelineTemplateSync.Sync", func() {

	var (
		caseName  = CasePipelineTemplates
		syncInter v1alpha1.PipelineTemplateSyncInterface
	)

	BeforeEach(func() {
		syncInter = applyTemplateSync(caseName, Options.DevopsClient)
		waitUntilTemplateSyncReady(caseName, Options.DevopsClient)
	})

	AfterEach(func() {
	})

	Context("Sync from git repository firstly", func() {
		It("Should sync all PipelineTemplates", func() {
			sync, err := Options.DevopsClient.DevopsV1alpha1().PipelineTemplateSyncs(Options.Namespace).Get(syncInter.GetName(), metav1.GetOptions{})
			Expect(err).Should(BeNil())
			//klog.Infof("Sync status is %#v", *sync.Status)
			expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSuccess), "alauda-all-in-one", v1alpha1.TypePipelineTaskTemplate, "1.8.12")
			expectTaskTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTaskTemplate, Options.Namespace, "alauda-all-in-one")
			expectTaskTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTaskTemplate, Options.Namespace, "alauda-all-in-one.1.8.12")

			expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSuccess), "running", v1alpha1.TypePipelineTaskTemplate, "0.1.1")
			expectTaskTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTaskTemplate, Options.Namespace, "running")
			expectTaskTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTaskTemplate, Options.Namespace, "running.0.1.1")

			expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSuccess), "AlaudaAllInOne", v1alpha1.TypePipelineTemplate, "1.8.12")
			expectTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTemplate, Options.Namespace, "AlaudaAllInOne")
			expectTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTemplate, Options.Namespace, "AlaudaAllInOne.1.8.12")

			expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSuccess), "GeneralBuild", v1alpha1.TypePipelineTemplate, "0.1.3")
			expectTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTemplate, Options.Namespace, "GeneralBuild")
			expectTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTemplate, Options.Namespace, "GeneralBuild.0.1.3")

		})
	})

	var _ = Describe("PipelineTemplateSync.ReSync", func() {

		Context("Update one task template version in git repository and ReSync", func() {
			BeforeEach(func() {
				updateOneTaskTemplate(CasePipelineTemplates, "task-all-in-one.yaml", "2.0.0")
				syncInter = applyTemplateSync(caseName, Options.DevopsClient)
				waitUntilTemplateSyncReady(caseName, Options.DevopsClient)
			})
			It("Should upgrade one template", func() {
				sync, err := Options.DevopsClient.DevopsV1alpha1().PipelineTemplateSyncs(Options.Namespace).Get(syncInter.GetName(), metav1.GetOptions{})
				Expect(err).Should(BeNil())
				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSuccess), "alauda-all-in-one", v1alpha1.TypePipelineTaskTemplate, "2.0.0")
				expectTaskTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTaskTemplate, Options.Namespace, "alauda-all-in-one")
				expectTaskTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTaskTemplate, Options.Namespace, "alauda-all-in-one.2.0.0")

				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "running", v1alpha1.TypePipelineTaskTemplate, "0.1.1")
				expectTaskTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTaskTemplate, Options.Namespace, "running")
				expectTaskTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTaskTemplate, Options.Namespace, "running.0.1.1")

				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "AlaudaAllInOne", v1alpha1.TypePipelineTemplate, "1.8.12")
				expectTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTemplate, Options.Namespace, "AlaudaAllInOne")
				expectTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTemplate, Options.Namespace, "AlaudaAllInOne.1.8.12")

				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "GeneralBuild", v1alpha1.TypePipelineTemplate, "0.1.3")
				expectTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTemplate, Options.Namespace, "GeneralBuild")
				expectTemplateExists(Options.DevopsClient, v1alpha1.TypePipelineTemplate, Options.Namespace, "GeneralBuild.0.1.3")

			})
		})

		Context("Update one template version in git repository and ReSync", func() {
			BeforeEach(func() {
				updateOneTemplate(caseName, "template-all-in-one.yaml", "2.0.0")
				syncInter = applyTemplateSync(caseName, Options.DevopsClient)
				waitUntilTemplateSyncReady(caseName, Options.DevopsClient)
			})
			It("Should upgrade one template", func() {
				sync, err := Options.DevopsClient.DevopsV1alpha1().PipelineTemplateSyncs(Options.Namespace).Get(syncInter.GetName(), metav1.GetOptions{})
				Expect(err).Should(BeNil())
				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "alauda-all-in-one", v1alpha1.TypePipelineTaskTemplate, "2.0.0")
				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "running", v1alpha1.TypePipelineTaskTemplate, "0.1.1")

				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSuccess), "AlaudaAllInOne", v1alpha1.TypePipelineTemplate, "2.0.0")
				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "GeneralBuild", v1alpha1.TypePipelineTemplate, "0.1.3")
			})
		})

		Context("Do nothing and ReSync", func() {
			BeforeEach(func() {
				syncInter = applyTemplateSync(caseName, Options.DevopsClient)
				waitUntilTemplateSyncReady(caseName, Options.DevopsClient)
			})
			It("Should skip all templates", func() {
				sync, err := Options.DevopsClient.DevopsV1alpha1().PipelineTemplateSyncs(Options.Namespace).Get(syncInter.GetName(), metav1.GetOptions{})
				Expect(err).Should(BeNil())
				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "alauda-all-in-one", v1alpha1.TypePipelineTaskTemplate, "2.0.0")
				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "running", v1alpha1.TypePipelineTaskTemplate, "0.1.1")

				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "AlaudaAllInOne", v1alpha1.TypePipelineTemplate, "2.0.0")
				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "GeneralBuild", v1alpha1.TypePipelineTemplate, "0.1.3")
			})
		})

		Context("Delete one template in git repository and ReSync", func() {
			BeforeEach(func() {
				deleteOneTemplate(caseName, "template-all-in-one.yaml")
				syncInter = applyTemplateSync(caseName, Options.DevopsClient)
				waitUntilTemplateSyncReady(caseName, Options.DevopsClient)
			})
			It("Should delete one template", func() {
				sync, err := Options.DevopsClient.DevopsV1alpha1().PipelineTemplateSyncs(Options.Namespace).Get(syncInter.GetName(), metav1.GetOptions{})
				Expect(err).Should(BeNil())

				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "alauda-all-in-one", v1alpha1.TypePipelineTaskTemplate, "2.0.0")
				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "running", v1alpha1.TypePipelineTaskTemplate, "0.1.1")

				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusDeleted), "AlaudaAllInOne", v1alpha1.TypePipelineTemplate, "2.0.0")
				expectTemplateNotExists(Options.DevopsClient, v1alpha1.TypePipelineTemplate, Options.Namespace, "AlaudaAllInOne")
				expectTemplateNotExists(Options.DevopsClient, v1alpha1.TypePipelineTemplate, Options.Namespace, "AlaudaAllInOne.2.0.0")

				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "GeneralBuild", v1alpha1.TypePipelineTemplate, "0.1.3")
			})
		})

		Context("Delete one task template in git repository and ReSync", func() {
			BeforeEach(func() {
				deleteOneTaskTemplate(caseName, "task-all-in-one.yaml")
				syncInter = applyTemplateSync(caseName, Options.DevopsClient)
				waitUntilTemplateSyncReady(caseName, Options.DevopsClient)
			})
			It("Should delete one template", func() {
				sync, err := Options.DevopsClient.DevopsV1alpha1().PipelineTemplateSyncs(Options.Namespace).Get(syncInter.GetName(), metav1.GetOptions{})
				Expect(err).Should(BeNil())

				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusDeleted), "alauda-all-in-one", v1alpha1.TypePipelineTaskTemplate, "2.0.0")
				expectTaskTemplateNotExists(Options.DevopsClient, v1alpha1.TypePipelineTaskTemplate, Options.Namespace, "alauda-all-in-one")
				expectTaskTemplateNotExists(Options.DevopsClient, v1alpha1.TypePipelineTaskTemplate, Options.Namespace, "alauda-all-in-one.2.0.0")

				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "running", v1alpha1.TypePipelineTaskTemplate, "0.1.1")
				expectContains(sync.Status.Conditions, string(v1alpha1.SyncStatusSkip), "GeneralBuild", v1alpha1.TypePipelineTemplate, "0.1.3")
			})
		})

	})

})
