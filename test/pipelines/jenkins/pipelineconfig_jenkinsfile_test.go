package jenkins

import (
	"fmt"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	testcommon "alauda.io/devops-apiserver/test"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("Jenkins.Jenkinsfile.CodeRepository", func() {
	var (
		opts           testcommon.PipelineConfigOpts
		pipelineConfig *v1alpha1.PipelineConfig
		pipelineList   *v1alpha1.PipelineList
		// pipeline *v1alpha1.Pipeline
		err error
	)

	BeforeEach(func() {
		opts = testcommon.PipelineConfigOpts{
			Name:               testcommon.GenName("e2eyaml-"),
			Namespace:          namespace.Name,
			JenkinsBindingName: jenkinsBinding.Name,
			RunPolicy:          v1alpha1.PipelinePolicySerial,
		}
	})

	JustBeforeEach(func() {
		pipelineConfig, err = testcommon.CreatePipelineConfig(devopsClient, opts)
		Expect(err).To(BeNil())
		Expect(pipelineConfig).ToNot(BeNil())

		By("Get PipelineConfig by name")
		pipelineConfig, err = GetPipelineConfig(devopsClient, opts.Namespace, pipelineConfig.Name)
		Expect(err).To(BeNil())
		Expect(pipelineConfig).ToNot(BeNil())
	})
	AfterEach(func() {
		// delete pipelineconfig and pipeline
		testcommon.DeletePipelineConfig(devopsClient, pipelineConfig)
	})

	Context("Input CodeRepoService", func() {
		BeforeEach(func() {
			opts.JenkinsfilePath = "Jenkinsfile"
			opts.GitURL = testcommon.GetCodeUrl(testcommon.GetValue(testcommon.CodeGithubJenkinsfilePrivateKey))
			opts.GitBranch = "master"
			opts.SourceType = "GIT"
			// opts.SecretName = gitlabSecret.Name
		})
		It("Use the input CodeRepo Url to create pipelineconfig", func() {

			By("Check Jenkinsfile after pipelineconfig update")
			Expect(pipelineConfig.Spec.Source.Git.URI).To(ContainSubstring(opts.GitURL))
			Expect(pipelineConfig.Spec.Source.Git.Ref).To(ContainSubstring("master"))

			By("When pipelineconfig is created successfully, get pipeline is empty")
			pipelineList, err = devopsClient.DevopsV1alpha1().Pipelines(opts.Namespace).List(metav1.ListOptions{ResourceVersion: "0"})
			Expect(err).To(BeNil())
			Expect(pipelineList).ToNot(BeNil())
			Expect(pipelineList.Items).To(BeEmpty())
		})
	})

	Context("Use Binding CodeRepoService", func() {
		BeforeEach(func() {
			codeRepositoryName := fmt.Sprintf("%s-%s-%s", gitlabService.Name, testcommon.GetValue(testcommon.GitlabRepositoryKey), testcommon.GetValue(testcommon.CodeGithubJenkinsfilePrivateKey))
			opts.GitBranch = "master"
			opts.JenkinsfilePath = "Jenkinsfile"
			opts.CodeRepositoryName = codeRepositoryName
			opts.SourceType = "GIT"
		})
		It("Select the binding CodeRepo to create pipelineconfig", func() {

			By("Check Jenkinsfile after pipelineconfig update")
			Expect(pipelineConfig.Spec.Source.Git.URI).To(ContainSubstring(opts.GitURL + ".git"))
			Expect(pipelineConfig.Spec.Source.Git.Ref).To(ContainSubstring("master"))
		})
	})

})
