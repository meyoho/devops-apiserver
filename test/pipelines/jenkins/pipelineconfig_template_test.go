package jenkins

import (
	// "fmt"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	testcommon "alauda.io/devops-apiserver/test"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func GetPipelineConfig(client versioned.Interface, namespace, name string) (pipelineConfig *v1alpha1.PipelineConfig, err error) {
	Eventually(func() error {
		pipelineConfig, err = client.DevopsV1alpha1().PipelineConfigs(namespace).Get(name, v1alpha1.GetOptions())
		return err
	}, testcommon.GetIterationDuration()).Should(Succeed())
	return
}

var _ = Describe("Jenkins.PipelineTemplate.ClusterPipelineTemplate", func() {
	var (
		opts           testcommon.PipelineConfigOpts
		pipelineConfig *v1alpha1.PipelineConfig
		// pipelineList *v1alpha1.PipelineList
		pipeline                         *v1alpha1.Pipeline
		err                              error
		name, templateName, sunarBinding string
		values                           map[string]string
	)

	BeforeEach(func() {
		name = testcommon.GenName("e2egobuild-")
		templateName = "GoLangBuilder"
		sunarBinding = `{"namespace":"` + namespace.Name + `","name":""}`
		values = map[string]string{
			"PlatformCodeRepository":     testcommon.GetPipelineConfig_InputCodeRepo("GIT", testcommon.GetValue(testcommon.CodeGitlabGoPrivateKey), gitlabSecret),
			"Branch":                     "*/master",
			"RelativeDirectory":          "src",
			"buildCommand":               "go build",
			"UseSonarQube":               "false",
			"CodeQualityBinding":         sunarBinding,
			"EnableBranchAnalysis":       "false",
			"AnalysisParameters":         "sonar.sources=.",
			"FailedIfNotPassQualityGate": "false",
			"imageRepository":            testcommon.GetPipelineConfig_Image("input", harborBinding),
			"context":                    ".",
			"buildArguments":             "",
			"dockerfile":                 "Dockerfile",
			"retry":                      "3",
		}
	})
	AfterEach(func() {
		// delete pipelineconfig and pipeline
		testcommon.DeletePipelineConfig(devopsClient, pipelineConfig)
	})

	JustBeforeEach(func() {
		template := testcommon.GetClusterTemplateForPipelineConfig(devopsClient, templateName, values)
		opts = testcommon.PipelineConfigOpts{
			Name:               name,
			Namespace:          namespace.Name,
			JenkinsBindingName: jenkinsBinding.Name,
			RunPolicy:          v1alpha1.PipelinePolicySerial,
			Template:           template,
		}

		pipelineConfig, err = testcommon.CreatePipelineConfig(devopsClient, opts)
		Expect(err).To(BeNil())
		Expect(pipelineConfig).ToNot(BeNil())

		pipelineConfig, err = GetPipelineConfig(devopsClient, opts.Namespace, pipelineConfig.Name)
		Expect(err).To(BeNil())
		Expect(pipelineConfig).ToNot(BeNil())
	})

	Context("Use GoLangBuilder Create PipelineConfig", func() {
		It("GoLangBuilder PipelineConfig", func() {
			By("Get GoLangBuilder PipelineConfig by name")
			// pipelineConfig, err = GetPipelineConfig(opts.Namespace, pipelineConfig.Name)
			// pipelineConfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(opts.Namespace).Get(pipelineConfig.Name, metav1.GetOptions{ResourceVersion: "0"})
			// Expect(err).To(BeNil())
			// Expect(pipelineConfig).ToNot(BeNil())

			if testcommon.GetBoolean(testcommon.JenkinsIsReadyKey) {
				By("Manual execute pipeline")
				By("Waiting secret to sync...")
				time.Sleep(2 * time.Minute)
				pipeline, err = testcommon.ExecuteAndTestPipelineConfig(devopsClient, k8sClient, pipelineConfig, v1alpha1.PipelinePhaseComplete)
				Expect(err).To(BeNil())
				Expect(pipeline).ToNot(BeNil())
			}

			By("Update PipelineConfig")
			triggers := testcommon.GetPipelineTriggers(devopsClient,
				testcommon.PipelineTriggerOpts{TriggerType: v1alpha1.PipelineTriggerTypeCron, Time: "H/2 * * * *"},
			)
			opts.Triggers = triggers
			pipelineConfig, err = testcommon.UpdatePipelineConfig(devopsClient, opts)
			Expect(err).To(BeNil())

			By("Check Jenkinsfile after pipelineconfig update")
			pipelineConfig, err = GetPipelineConfig(devopsClient, opts.Namespace, pipelineConfig.Name)
			// pipelineConfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(opts.Namespace).Get(pipelineConfig.Name, metav1.GetOptions{ResourceVersion: "0"})
			Expect(err).To(BeNil())
			Expect(pipelineConfig).ToNot(BeNil())
			codeUrl := testcommon.GetCodeUrl(testcommon.GetValue(testcommon.CodeGitlabGoPrivateKey))
			imagePath := testcommon.GetImagePath()
			imageCredentialID := testcommon.GetImageCredentialID(harborBinding)
			Expect(pipelineConfig.Spec.Strategy.Jenkins.Jenkinsfile).To(ContainSubstring(`env.CODE_REPO = "` + codeUrl + `"`))
			Expect(pipelineConfig.Spec.Strategy.Jenkins.Jenkinsfile).To(ContainSubstring(`def repositoryAddr = '` + imagePath + `'`))
			Expect(pipelineConfig.Spec.Strategy.Jenkins.Jenkinsfile).To(ContainSubstring(`credentialId = "` + imageCredentialID + `"`))
		})
	})

	// select coderepo and select imagerepo
	Context("Use GoLangAndDeployService Create PipelineConfig", func() {
		BeforeEach(func() {
			name = testcommon.GenName("e2egodeploy-")
			templateName = "GoLangAndDeployService"
			values = testcommon.AddMap(values, map[string]string{
				"PlatformCodeRepository": testcommon.GetPipelineConfig_SelectCodeRepo("GIT", testcommon.GetValue(testcommon.CodeGitlabGoPublicKey), gitlabService.Name, gitlabSecret),
				"imageRepository":        testcommon.GetPipelineConfig_Image("select", harborBinding),
				"serviceName":            app.Name + ":deployment:" + app.Name,
				"containerName":          app.Spec.Template.Spec.Containers[0].Name,
			})
		})
		It("GoLangAndDeployService PipelineConfig", func() {
			By("Get GoLangAndDeployService PipelineConfig by name")
			// Eventually(func() error {
			// 	pipelineConfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(opts.Namespace).Get(pipelineConfig.Name, metav1.GetOptions{ResourceVersion: "0"})
			// 	return err
			// }, testcommon.GetIterationDuration()).Should(Succeed())
			// Expect(err).To(BeNil())
			// Expect(pipelineConfig).ToNot(BeNil())

			if testcommon.GetBoolean(testcommon.JenkinsIsReadyKey) {
				By("Manual execute pipeline")
				pipeline, err = testcommon.ExecuteAndTestPipelineConfig(devopsClient, k8sClient, pipelineConfig, v1alpha1.PipelinePhaseComplete)
				Expect(err).To(BeNil())
				Expect(pipeline).ToNot(BeNil())
			}
		})
	})

	Context("Use JavaBuilder Create PipelineConfig", func() {
		BeforeEach(func() {
			name = testcommon.GenName("e2ejavabuild-")
			templateName = "JavaBuilder"
			values = testcommon.AddMap(values, map[string]string{
				"PlatformCodeRepository": testcommon.GetPipelineConfig_InputCodeRepo("GIT", testcommon.GetValue(testcommon.CodeGitlabJavaPrivateKey), gitlabSecret),
				"buildCommand":           "mvn clean package",
				"RelativeDirectory":      ".",
				"AnalysisParameters":     "sonar.sources=./src\nsonar.java.binaries=./target/classes",
			})
		})
		It("JavaBuilder PipelineConfig", func() {
			By("Get JavaBuilder PipelineConfig by name")
			// Eventually(func() error {
			// 	pipelineConfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(opts.Namespace).Get(pipelineConfig.Name, metav1.GetOptions{ResourceVersion: "0"})
			// 	return err
			// }, testcommon.GetIterationDuration()).Should(Succeed())

			// Expect(err).To(BeNil())
			// Expect(pipelineConfig).ToNot(BeNil())

			if testcommon.GetBoolean(testcommon.JenkinsIsReadyKey) {
				By("Manual execute pipeline")
				pipeline, err = testcommon.ExecuteAndTestPipelineConfig(devopsClient, k8sClient, pipelineConfig, v1alpha1.PipelinePhaseComplete)
				Expect(err).To(BeNil())
				Expect(pipeline).ToNot(BeNil())
			}
		})
	})

	// select coderepo and select imagerepo
	Context("Use JavaBuildAndDeployService Create PipelineConfig", func() {
		BeforeEach(func() {
			name = testcommon.GenName("e2ejavadeploy-")
			templateName = "JavaBuildAndDeployService"
			values = testcommon.AddMap(values, map[string]string{
				"PlatformCodeRepository": testcommon.GetPipelineConfig_SelectCodeRepo("GIT", testcommon.GetValue(testcommon.CodeGitlabJavaPublicKey), gitlabService.Name, gitlabSecret),
				"buildCommand":           "mvn clean package",
				"imageRepository":        testcommon.GetPipelineConfig_Image("select", harborBinding),
				"serviceName":            app.Name + ":deployment:" + app.Name,
				"containerName":          app.Spec.Template.Spec.Containers[0].Name,
				"RelativeDirectory":      ".",
				"AnalysisParameters":     "sonar.sources=./src\nsonar.java.binaries=./target/classes",
			})
		})
		It("JavaBuildAndDeployService PipelineConfig", func() {
			By("Get JavaBuildAndDeployService PipelineConfig by name")
			// Eventually(func() error {
			// 	pipelineConfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(opts.Namespace).Get(pipelineConfig.Name, metav1.GetOptions{ResourceVersion: "0"})
			// 	return err
			// }, testcommon.GetIterationDuration()).Should(Succeed())
			// Expect(err).To(BeNil())
			// Expect(pipelineConfig).ToNot(BeNil())

			if testcommon.GetBoolean(testcommon.JenkinsIsReadyKey) {
				By("Manual execute pipeline")
				pipeline, err = testcommon.ExecuteAndTestPipelineConfig(devopsClient, k8sClient, pipelineConfig, v1alpha1.PipelinePhaseComplete)
				Expect(err).To(BeNil())
				Expect(pipeline).ToNot(BeNil())
			}
		})
	})

	Context("Use alaudaSyncImage Create PipelineConfig", func() {
		BeforeEach(func() {
			name = testcommon.GenName("e2esyncimage-")
			templateName = "alaudaSyncImage"
			imagePath := testcommon.GetImagePath()
			imageCredentialID := testcommon.GetImageCredentialID(harborBinding)
			values = map[string]string{
				"sourceImageRepository": imagePath,
				"sourceImageTag":        "latest",
				"sourceCredentialsId":   imageCredentialID,
				"targetImageRepository": imagePath,
				"targetImageTag":        "synclatest",
				"targetCredentialsId":   imageCredentialID,
			}
		})
		It("alaudaSyncImage PipelineConfig", func() {
			By("Get alaudaSyncImage PipelineConfig by name")
			// Eventually(func() error {
			// 	pipelineConfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(opts.Namespace).Get(pipelineConfig.Name, metav1.GetOptions{ResourceVersion: "0"})
			// 	return err
			// }, testcommon.GetIterationDuration()).Should(Succeed())
			// Expect(err).To(BeNil())
			// Expect(pipelineConfig).ToNot(BeNil())

			if testcommon.GetBoolean(testcommon.JenkinsIsReadyKey) {
				By("Manual execute pipeline")
				pipeline, err = testcommon.ExecuteAndTestPipelineConfig(devopsClient, k8sClient, pipelineConfig, v1alpha1.PipelinePhaseComplete)
				Expect(err).To(BeNil())
				Expect(pipeline).ToNot(BeNil())
			}
		})
	})

	Context("Use alaudaDeployService Create PipelineConfig", func() {
		BeforeEach(func() {
			name = testcommon.GenName("e2edeploy-")
			templateName = "alaudaDeployService"
			values = map[string]string{
				"serviceName":           app.Name + ":deployment:" + app.Name,
				"containerName":         app.Spec.Template.Spec.Containers[0].Name,
				"imageRepositoryDeploy": testcommon.GetPipelineConfig_Image("select", harborBinding),
			}
		})
		It("alaudaDeployService PipelineConfig", func() {
			By("Get alaudaDeployService PipelineConfig by name")
			// Eventually(func() error {
			// 	pipelineConfig, err = devopsClient.DevopsV1alpha1().PipelineConfigs(opts.Namespace).Get(pipelineConfig.Name, metav1.GetOptions{ResourceVersion: "0"})
			// 	return error
			// }, testcommon.GetIterationDuration()).Should(Succeed())
			// Expect(err).To(BeNil())
			// Expect(pipelineConfig).ToNot(BeNil())

			if testcommon.GetBoolean(testcommon.JenkinsIsReadyKey) {
				By("Manual execute pipeline")
				pipeline, err = testcommon.ExecuteAndTestPipelineConfig(devopsClient, k8sClient, pipelineConfig, v1alpha1.PipelinePhaseComplete)
				Expect(err).To(BeNil())
				Expect(pipeline).ToNot(BeNil())
			}
		})
	})
})
