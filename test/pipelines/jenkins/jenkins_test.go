package jenkins

import (
	"fmt"
	"testing"
	"time"

	"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/devops-apiserver/pkg/client/clientset/versioned"
	testcommon "alauda.io/devops-apiserver/test"
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

var (
	devopsClient    versioned.Interface
	k8sClient       kubernetes.Interface
	workingJenkins  *v1alpha1.Jenkins
	namespace       *corev1.Namespace
	jenkinsSecret   *corev1.Secret
	jenkinsBinding  *v1alpha1.JenkinsBinding
	gitlabBinding   *v1alpha1.CodeRepoBinding
	err             error
	createdJenkins  bool
	env             = testcommon.New()
	timeoutDuration = time.Duration(time.Minute * 10)

	gitlabService *v1alpha1.CodeRepoService
	gitlabSecret  *corev1.Secret

	harborRegistry      *v1alpha1.ImageRegistry
	harborBinding       *v1alpha1.ImageRegistryBinding
	harborSecret        *corev1.Secret
	remoteImageRepos    *v1alpha1.ImageRegistryBindingRepositories
	harborBindingAssign *v1alpha1.ImageRegistryBinding

	app *appsv1.Deployment
)

func TestJenkins(t *testing.T) {
	testcommon.SetDefaults()
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("../../pipelines-jenkins.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "Jenkins", []Reporter{junitReporter})
}

//Create Jenkins integration
var _ = BeforeSuite(func() {
	devopsClient, k8sClient = testcommon.MustClients("", "")

	namespace = testcommon.MustNamespace(devopsClient, k8sClient)

	workingJenkins, jenkinsSecret, createdJenkins = testcommon.MustJenkinsAndSecret(devopsClient, k8sClient)

	gitlabService, gitlabSecret = testcommon.MustGitlabAndSecret(devopsClient, k8sClient)

	harborRegistry, harborSecret = testcommon.MustHarborAndSecret(devopsClient, k8sClient)

	By("Create JenkinsBinding")
	bindingName := testcommon.GetJenkinsBindingData()
	jenkinsBinding, err = testcommon.CreateJenkinsBinding(devopsClient, testcommon.JenkinsBindingOpts{
		Name:            bindingName,
		Namespace:       namespace.Name,
		JenkinsName:     workingJenkins.Name,
		SecretName:      jenkinsSecret.Name,
		SecretNamespace: jenkinsSecret.Namespace,
	}, time.Minute*1)
	Expect(err).To(BeNil())
	Expect(jenkinsBinding).ToNot(BeNil())

	By("Create GitlabBinding")
	bindingName = testcommon.GetGitlabBindingData()
	gitlabBinding, err = testcommon.CreateCodeRepoBinding(devopsClient, testcommon.CodeRepoBindingOpts{
		Name:            bindingName,
		Namespace:       namespace.Name,
		CoderepoName:    gitlabService.Name,
		SecretName:      gitlabSecret.Name,
		SecretNamespace: gitlabSecret.Namespace,
		AccountName:     "root",
		Repositories:    nil,
		All:             true,
	})
	Expect(err).To(BeNil())
	Expect(jenkinsBinding).ToNot(BeNil())

	By("Wait Gitlab repositories synced")
	var repositoryList *v1alpha1.CodeRepositoryList
	codeRepositoryName := fmt.Sprintf("%s-%s-%s", gitlabService.Name, testcommon.GetValue(testcommon.GitlabRepositoryKey), testcommon.GetValue(testcommon.CodeGithubJenkinsfilePrivateKey))
	Eventually(func() (err error) {
		repositoryList, err = devopsClient.DevopsV1alpha1().CodeRepositories(namespace.Name).List(metav1.ListOptions{ResourceVersion: "0"})
		if err == nil && len(repositoryList.Items) == 0 {
			err = fmt.Errorf("No code repositories synced yet!")
		}
		if len(repositoryList.Items) > 0 {
			var codeRepository *v1alpha1.CodeRepository
			for _, repo := range repositoryList.Items {
				if codeRepositoryName == repo.Name {
					codeRepository = &repo
					err = nil
					break
				}
			}
			if codeRepository == nil {
				err = fmt.Errorf("codeRepository \"%s\" has no synced yet", codeRepositoryName)
			}
		}
		return
	}, env.GetDuration(testcommon.TestTimeoutKey, testcommon.TimeoutDuration)).Should(Succeed())

	By("Create HarborBinding")
	bindingName = testcommon.GetHarborBindingData()
	harborBinding, err = testcommon.CreateImageRegistryBinding(devopsClient, testcommon.ImageRegistryBindingOpts{
		Name:            bindingName,
		Namespace:       namespace.Name,
		RegistryName:    harborRegistry.Name,
		RegistryType:    string(v1alpha1.RegistryTypeHarbor),
		SecretName:      harborSecret.Name,
		SecretNamespace: harborSecret.Namespace,
		Repositories:    nil,
	})
	Expect(err).To(BeNil())
	Expect(jenkinsBinding).ToNot(BeNil())

	By("Fetch remote repositories")
	Eventually(func() (err error) {
		remoteImageRepos, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(harborBinding.Namespace).GetImageRepos(harborBinding.Name)
		if err != nil {
			return
		}
		if remoteImageRepos == nil || remoteImageRepos.Items == nil {
			err = fmt.Errorf("remoteImageRepos has no fetch yet")
			return
		}
		Expect(remoteImageRepos.Items).To(ContainElement(ContainSubstring(testcommon.GetValue(testcommon.HarborProjectKey))))
		return nil
	}, testcommon.GetDuration(testcommon.TestTimeoutKey, testcommon.TimeoutDuration), time.Second).Should(Succeed())

	// time.Sleep(2 * time.Second)
	// remoteImageRepos, err = devopsClient.DevopsV1alpha1().ImageRegistryBindings(harborBinding.Namespace).GetImageRepos(harborBinding.Name)
	// Expect(err).To(BeNil())
	// Expect(remoteImageRepos).ToNot(BeNil())
	// Expect(remoteImageRepos.Items).ToNot(BeEmpty())
	// Expect(remoteImageRepos.Items).To(ContainElement(ContainSubstring(testcommon.GetValue(testcommon.HarborProjectKey))))

	By("update image registry binding ,set repositories")
	harborBindingAssign, err = testcommon.UpdateImageRegistryBinding(devopsClient, testcommon.ImageRegistryBindingOpts{
		Name:            harborBinding.Name,
		Namespace:       harborBinding.Namespace,
		RegistryName:    harborRegistry.Name,
		RegistryType:    string(v1alpha1.RegistryTypeHarbor),
		SecretName:      harborSecret.Name,
		SecretNamespace: harborSecret.Namespace,
		Repositories:    remoteImageRepos.Items,
	})

	By("Wait Image repositories synced")
	var imageRepositoryList *v1alpha1.ImageRepositoryList
	imageRepositoryName := fmt.Sprintf("%s-%s-%s", harborRegistry.Name, testcommon.GetValue(testcommon.HarborProjectKey), testcommon.GetValue(testcommon.HarborRepoKey))
	Eventually(func() (err error) {
		imageRepositoryList, err = devopsClient.DevopsV1alpha1().ImageRepositories(harborBinding.Namespace).List(metav1.ListOptions{ResourceVersion: "0"})
		if err == nil && len(imageRepositoryList.Items) == 0 {
			err = fmt.Errorf("No image repositories synced yet!")
		} else if len(imageRepositoryList.Items) > 0 {
			var imageRepository *v1alpha1.ImageRepository
			for _, repo := range imageRepositoryList.Items {
				if imageRepositoryName == repo.Name {
					imageRepository = &repo
					err = nil
					break
				}
			}
			if imageRepository == nil {
				err = fmt.Errorf("imageRepository \"%s\" has no synced yet", imageRepositoryName)
			}
		}
		return
	}, testcommon.GetDuration(testcommon.TestTimeoutKey, testcommon.TimeoutDuration)).Should(Succeed())

	By("Create Deployment")
	app, err = testcommon.CreateDeployment(k8sClient, testcommon.DeploymentOpts{
		Name:      testcommon.GenName("e2eapp-"),
		Namespace: namespace.Name,
		Container: testcommon.GenName("e2ehello-"),
		Image:     testcommon.GetValue(testcommon.DeploymentImage),
	})
	Expect(err).To(BeNil())
	Expect(app).ToNot(BeNil())
})

var _ = AfterSuite(func() {
	if devopsClient == nil {
		return
	}
	By("Destruct Namespace")
	if namespace != nil {
		err = k8sClient.CoreV1().Namespaces().Delete(namespace.Name, &metav1.DeleteOptions{})
		if err != nil {
		}
	}

	if createdJenkins && workingJenkins != nil {
		By("Destruct Jenkins")
		err = devopsClient.DevopsV1alpha1().Jenkinses().Delete(workingJenkins.Name, nil)
		if err != nil {
		}
	}

	By("Destruct Jenkins Secret")
	if jenkinsSecret != nil {
		err = k8sClient.CoreV1().Secrets(jenkinsSecret.Namespace).Delete(jenkinsSecret.Name, &metav1.DeleteOptions{})
		if err != nil {
		}
	}

	By("Destruct Gitlab Secret")
	if gitlabSecret != nil {
		err = k8sClient.CoreV1().Secrets(gitlabSecret.Namespace).Delete(gitlabSecret.Name, &metav1.DeleteOptions{})
		if err != nil {
		}
	}

	By("Destruct Harbor Secret")
	if harborSecret != nil {
		err = k8sClient.CoreV1().Secrets(harborSecret.Namespace).Delete(harborSecret.Name, &metav1.DeleteOptions{})
		if err != nil {
		}
	}
})
