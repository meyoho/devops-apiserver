package denpendency

import (
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog"
)

var _ Interface = &K8sResource{}

type K8sResource struct {
	Ref *K8sResourceRef

	denpend k8sresourceDenpend
}

func (res *K8sResource) Init() error {
	Expect(res.Ref.Kind).To(BeEquivalentTo("namespace"))
	klog.Infof("Init: [K8s Resource] %s/%s", res.Ref.Kind, res.Ref.Name)

	_, err := res.denpend.client.CoreV1().Namespaces().Create(&corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: res.Ref.Name,
		},
	})
	return err
}

func (res *K8sResource) Destroy() error {
	Expect(res.Ref.Kind).To(BeEquivalentTo("namespace"))
	klog.Infof("Destroy: [K8s Resource] %s/%s", res.Ref.Kind, res.Ref.Name)

	err := res.denpend.client.CoreV1().Namespaces().Delete(res.Ref.Name, &metav1.DeleteOptions{})
	return err
}

func (res *K8sResource) Set(client kubernetes.Interface) {
	res.denpend.client = client
}

type K8sResourceRef struct {
	Kind string
	Name string
}

type k8sresourceDenpend struct {
	client kubernetes.Interface
}
