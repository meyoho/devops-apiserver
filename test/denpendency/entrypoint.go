package denpendency

import ()

type Interface interface {
	Init() error
	Destroy() error
}

type Denpendency struct {
	*K8sResource
}

func (denpend *Denpendency) Init() error {
	if denpend.K8sResource != nil {
		return denpend.K8sResource.Init()
	}
	return nil
}

func (denpend *Denpendency) Destroy() error {
	if denpend.K8sResource != nil {
		return denpend.K8sResource.Destroy()
	}
	return nil
}
