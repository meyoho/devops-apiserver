package utils

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"k8s.io/klog"
	"os/exec"
	"strings"
)

func RunCmd(ctx context.Context, workdir, name string, args ...string) (stdout string, stderr string, err error) {
	cmdStr := name + " " + strings.Join(args, " ")
	klog.Infof("executing \"%s\" in \"%s\" \n", cmdStr, workdir)

	cmd := exec.CommandContext(ctx, name, args...)
	cmd.Env = []string{
		"GIT_TERMINAL_PROMPT=0",
	}
	cmd.Dir = workdir
	stdoutBf := bytes.NewBufferString("")
	stderrBf := bytes.NewBufferString("")
	cmd.Stdout = stdoutBf
	cmd.Stderr = stderrBf
	err = cmd.Run()

	stdoutBts, _err := ioutil.ReadAll(stdoutBf)
	if _err != nil {
		klog.Errorf("read com commd %s stdout error %s", cmdStr, _err)
	}
	stderrBts, _err := ioutil.ReadAll(stderrBf)
	if _err != nil {
		klog.Errorf("read com commd %s stdout error %s", cmdStr, _err)
	}

	if ctx.Err() != nil {
		if err == nil {
			err = ctx.Err()
		} else {
			err = fmt.Errorf("err: %s, contextErr: %s", err, ctx.Err().Error())
		}
	}

	return string(stdoutBts), string(stderrBts), err
}
