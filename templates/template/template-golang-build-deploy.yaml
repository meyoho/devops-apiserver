apiVersion: devops.alauda.io/v1alpha1
kind: ClusterPipelineTemplate
metadata:
  name: GoLangAndDeployService
  namespace: default
  annotations:
    displayName.zh-CN: "Golang 构建并部署应用"
    displayName.en: "Golang Build & Deploy Application"
    description.zh-CN: "克隆代码 -> Golang 构建 -> 代码扫描 -> Docker 构建 -> 部署应用"
    description.en: "Clone -> Golang Build -> Code Scan -> Docker Build -> Deploy Application"
    version: "2.10.1"
    style.icon: golang,docker,sonarqube,kubernetes
  labels:
    category: Build
    lang: "Golang"
spec:
  engine: graph
  withSCM: true
  agent:
    label: golang
    labelMatcher: golang.*
  options:
    raw: "buildDiscarder(logRotator(numToKeepStr: '200'))"
  stages:
    - name: Clone
      tasks:
        - name: clone
          kind: ClusterPipelineTaskTemplate
          type: public/clone
    - name: "Golang Build"
      tasks:
        - name: golang
          kind: ClusterPipelineTaskTemplate
          type: public/golang
    - name: "Code Scan"
      tasks:
        - name: sonar
          kind: ClusterPipelineTaskTemplate
          type: public/sonar
          relation:
            - action: show
              when:
                name: UseSonarQube
                value: true
    - name: "Docker Build"
      tasks:
        - name: build-docker
          kind: ClusterPipelineTaskTemplate
          type: public/build-docker
    - name: "DeployService"
      tasks:
        - name: deployService
          kind: ClusterPipelineTaskTemplate
          type: public/deployService
  post:
    always:
      - name: notification
        kind: ClusterPipelineTaskTemplate
        relation:
          - action: show
            when:
              name: UseNotification
              value: true
  arguments:
    - displayName:
        zh-CN: "代码检出"
        en: "Clone"
      items:
        - name: "PlatformCodeRepository"
          schema:
            type: alauda.io/coderepositorymix
          required: true
          value: ""
          binding:
            - clone.args.PlatformCodeRepository
          display:
            type: alauda.io/coderepositorymix
            name:
              zh-CN: "代码仓库"
              en: Code Repository
            description:
              zh-CN: "选择已为项目分配的代码仓库"
              en: ""
        - name: "Branch"
          schema:
            type: string
          required: false
          value: ""
          binding:
            - clone.args.Branch
          display:
            type: alauda.io/codebranch
            advanced: false
            name:
              zh-CN: "分支"
              en: "Branch"
            description:
              zh-CN: "检出代码仓库中的分支"
              en: "The code repository branch that you want to check out."
            related: PlatformCodeRepository
        - name: "RelativeDirectory"
          schema:
            type: string
          required: false
          value: ""
          binding:
            - clone.args.RelativeDirectory
          default: "src"
          display:
            type: string
            advanced: true
            name:
              zh-CN: "相对目录"
              en: Relative Directory
            description:
              zh-CN: "在 Golang 程序为 GOPATH的子目录，例如 src/github.com/alauda/alauda。指定检出 Git 仓库的本地目录(相对于 workspace 根目录)。若为空，将使用 workspace 根目录"
              en: "Used as the path in GOPATH, e.g src/github.com/alauda/alauda. Specify a local directory (relative to the workspace root) where the Git repository will be checked out. If left empty, the workspace root itself will be used"
    - displayName:
        zh-CN: "Golang 构建"
        en: "Golang Build"
      items:
        - name: "buildCommand"
          schema:
            type: string
          binding:
            - golang.args.buildCommand
          display:
            type: code
            name:
              zh-CN: "构建命令"
              en: "Build Command"
            description:
              zh-CN: "自定义更详细的构建命令。默认为：go build"
              en: ""
          required: false
          value: ""
          default: 'go build'
    - displayName:
        zh-CN: "代码扫描"
        en: "Code Scanning"
      items:
        - name: "UseSonarQube"
          schema:
            type: boolean
          display:
            type: boolean
            name:
              zh-CN: "开启代码扫描"
              en: "Use code analysis"
            description:
              zh-CN: "开启后，进行代码扫描"
              en: "Whether to use Sonarqube for code scan"
          required: false
          value: "false"
        - name: "CodeQualityBinding"
          schema:
            type: alauda.io/toolbinding
          binding:
            - sonar.args.CodeQualityBinding
          required: true
          value: ""
          display:
            type: alauda.io/toolbinding
            args:
              bindingKind: codequalitytool
              bindingToolType: Sonarqube
            name:
              zh-CN: "SonarQube"
              en: "SonarQube"
            description:
              zh-CN: "选择要使用的 SonarQube"
              en: "Select a SonarQube"
          relation:
            - action: show
              when:
                name: UseSonarQube
                value: true
        - name: "EnableBranchAnalysis"
          schema:
            type: boolean
          required: false
          binding:
            - sonar.args.EnableBranchAnalysis
          value: "false"
          display:
            type: boolean
            advanced: true
            name:
              zh-CN: "区分代码分支"
              en: "Enable branch analysis"
            description:
              zh-CN: "扫描主分支以外的分支时，若想查看这些分支的扫描结果，需要在 Sonarqube 支持多代码分支且要至少扫描过一次主分支的前提下，开启此开关才可扫描成功。"
              en: "When scanning branches other than the main branch while keeping separate results, the Sonarqube instance needs to support branch analysis and turn on this feature"
          relation:
            - action: show
              when:
                name: UseSonarQube
                value: true
        - name: "AnalysisParameters"
          schema:
            type: string
          required: false
          binding:
            - sonar.args.AnalysisParameters
          value: |
            sonar.sources=.
            sonar.sourceEncoding=UTF-8
          display:
            type: stringMultiline
            advanced: false
            name:
              zh-CN: "代码扫描参数"
              en: "Analysis Parameters"
            description:
              zh-CN: "为 Sonar Scanner 设置分析参数。当目录中存在 sonar-project.properties 文件时，将会忽略这个参数。详细参数见文档: https://docs.sonarqube.org/latest/analysis/analysis-parameters 和 https://docs.sonarqube.org/display/PLUG/SonarGo"
              en: "Set analysis parameters for Sonar Scanner. When 'sonar-project.properties' file exists in the code repository, this argument will be omitted. See https://docs.sonarqube.org/latest/analysis/analysis-parameters and https://docs.sonarqube.org/display/PLUG/SonarGo"
          relation:
            - action: show
              when:
                name: UseSonarQube
                value: true
        - name: "FailedIfNotPassQualityGate"
          schema:
            type: boolean
          required: false
          binding:
            - sonar.args.FailedIfNotPassQualityGate
          value: "false"
          display:
            type: boolean
            advanced: true
            name:
              zh-CN: "质量阈未通过终止流水线"
              en: "Fail pipeline when quality gate fails"
            description:
              zh-CN: "开启后，代码扫描结果为失败时，流水线执行状态变为失败，终止流水线"
              en: "Fails pipeline execution when quality gate fails"
          relation:
            - action: show
              when:
                name: UseSonarQube
                value: true
    - displayName:
        zh-CN: "Docker 构建"
        en: "Docker Build"
      items:
        - name: "imageRepository"
          schema:
            type: alauda.io/dockerimagerepositorymix
          binding:
            - build-docker.args.imageRepository
            - deployService.args.imageRepositoryDeploy
          required: true
          value: ""
          display:
            type: alauda.io/dockerimagerepositorymix
            name:
              zh-CN: "镜像仓库"
              en: Repository
            description:
              zh-CN: "选择已为项目分配的镜像仓库或者输入地址,更新应用时将使用第一个tag进行应用的更新"
              en: ""
        - name: "context"
          schema:
            type: string
          binding:
            - build-docker.args.context
          display:
            type: string
            advanced: true
            name:
              zh-CN: "构建上下文"
              en: "Build Context"
            description:
              zh-CN: "构建过程可以引用上下文中的任何文件。例如，构建中可以使用 COPY 命令在上下文中引用文件"
              en: "The build process can refer to any of the files in the context. For example, your build can use a COPY instruction to reference a file in the context"
          required: true
          value: ""
          default: '.'
        - name: "buildArguments"
          schema:
            type: string
          binding:
            - build-docker.args.buildArguments
          display:
            type: string
            advanced: true
            name:
              zh-CN: "构建参数"
              en: "Build Arguments"
            description:
              zh-CN: "自定义docker build Options，如 --add-host，多个 Options 用空格隔开；可参考 https://docs.docker.com/engine/reference/commandline/build/"
              en: ""
          required: false
          value: ""
          default: ''
        - name: "dockerfile"
          schema:
            type: string
          binding:
            - build-docker.args.dockerfile
          display:
            type: string
            advanced: true
            name:
              zh-CN: "Dockerfile"
              en: "Dockerfile"
            description:
              zh-CN: "Dockerfile 在代码仓库中的绝对路径"
              en: ""
          required: true
          value: ""
          default: 'Dockerfile'
        - name: "retry"
          schema:
            type: string
          binding:
            - build-docker.args.retry
          display:
            type: string
            advanced: true
            name:
              zh-CN: "重试次数"
              en: "Retry Times"
            description:
              zh-CN: "生成镜像时的失败重试次数"
              en: ""
          required: false
          value: ""
          default: '3'
    - displayName:
        zh-CN: "部署应用"
        en: "DeployService"
      items:
        - name: createApp
          schema:
            type: boolean
          binding:
          - deployService.args.createApp
          display:
            type: boolean
            name:
              zh-CN: "创建应用"
              en: "create application"
            description:
              zh-CN: "关闭为更新已有应用；如开启，请将YAML、JSON资源文件放置在所选代码仓库中，应用将在下面选择的集群和命名空间下创建，创建后首次执行流水线为创建应用、后续为更新应用"
              en: "Close to update existing applications. Such as open, please place the YAML, JSON resource files in the selected code warehouse, applications will be created under the selected cluster and namespace, created for the first time to perform assembly line for creating applications, subsequent to update application"
          default: "false"
          value: ""
        - name: clusterName
          schema:
            type: string
          binding:
          - deployService.args.clusterName
          display:
            type: alauda.io/clustername
            name:
              zh-CN: "集群"
              en: "cluster"
            description:
              zh-CN: "应用所在的集群"
              en: "cluster"
          required: true
          value: ""
        - name: namespace
          schema:
            type: string
          binding:
          - deployService.args.namespace
          display:
            type: alauda.io/namespace
            name:
              zh-CN: "命名空间"
              en: "namespace"
            description:
              zh-CN: "应用所在的命名空间"
              en: "namespace"
            related: clusterName
          required: true
          value: ""
        - name: serviceName
          schema:
            type: string
          binding:
            - deployService.args.serviceName
          display:
            type: alauda.io/servicenamemix
            name:
              zh-CN: "应用"
              en: "application"
            description:
              zh-CN: "应用名称、组件类型、组件名称"
              en: "application"
            related: namespace
          relation:
            - action: hidden
              when:
                name: createApp
                value: true
          required: true
          value: ""
        - name: containerName
          schema:
            type: string
          binding:
            - deployService.args.containerName
          display:
            type: alauda.io/containername
            name:
              zh-CN: "容器"
              en: "container"
            description:
              zh-CN: "容器实例名称"
              en: "container"
            related: serviceName
          required: true
          value: ""
          relation:
            - action: hidden
              when:
                name: createApp
                value: true
        - name: deployConfigFolder
          schema:
            type: string
          binding:
            - deployService.args.deployConfigFolder
          display:
            type: string
            name:
              zh-CN: "部署配置目录"
              en: "Deploy Configuration Directory"
            description:
              zh-CN: "1.所选代码仓库中YAML、JSON文件的路径，如'a/b/c'，为避免出错，请尽可能保证所选文件夹下仅包含YAML、JSON的Kubernetes资源文件; 2.创建应用仅支持单应用创建，如YAML、JSON文件中存在多个应用的定义，则会创建失败"
              en: "1.The path to the YAML and JSON files in the selected code repository, such as 'a/b/c', as much as possible to avoid errors, please ensure that the selected folder contains only the Kubernetes YAML, JSON resource files; 2.Creating a reference that only supports single-app creation, such as a definition of multiple apps in YAML, JSON files, will fail to be created"
          required: true
          default: ""
          value: ""
          relation:
            - action: show
              when:
                name: createApp
                value: true
        - name: container
          schema:
            type: string
          binding:
            - deployService.args.container
          display:
            type: string
            name:
              zh-CN: "容器"
              en: "container"
            description:
              zh-CN: "构建出的镜像运行的容器名"
              en: "container name"
          required: true
          value: ""
          relation:
            - action: show
              when:
                name: createApp
                value: true
        - name: "rollback"
          schema:
            type: boolean
          required: false
          binding:
          - deployService.args.rollback
          display:
            type: boolean
            name:
              zh-CN: "失败后回滚"
              en: "rollbackOnFailure"
            description:
              zh-CN: "更新应用后，如应用不能正常运行，则回滚到更新前的版本"
              en: "After updating application, such as application cannot run normally, roll back to the previous of the updated version"
          relation:
            - action: hidden
              when:
                name: createApp
                value: true
        - name: "command"
          schema:
            type: string
          required: false
          binding:
          - deployService.args.command
          value: ""
          display:
            type: string
            advanced: true
            name:
              zh-CN: "启动命令"
              en: "command"
            description:
              zh-CN: "即 command，相当于 Dockerfile 中的 ENTRYPOINT 命令。如果没有指定启动命令，将使用容器镜像中的 ENTRYPOINT"
              en: "command"
          relation:
            - action: hidden
              when:
                name: createApp
                value: true
        - name: "args"
          schema:
            type: string
          required: false
          binding:
          - deployService.args.args
          display:
            type: string
            advanced: true
            name:
              zh-CN: "参数"
              en: "args"
            description:
              zh-CN: "即 args，相当于 Dockerfile 中的 CMD，负责提供启动命令的命令参数。如果没有指定参数，将使用容器镜像中的 CMD"
              en: "args"
          relation:
            - action: hidden
              when:
                name: createApp
                value: true
        - name: timeout
          schema:
            type: string
          binding:
            - deployService.args.timeout
          display:
            type: string
            advanced: true
            name:
              zh-CN: "超时时间（秒）"
              en: "Timeout (s)"
            description:
              zh-CN: "当前任务的执行时间若超过超时时间，则会中止任务，流水线变为失败状态"
              en: ""
          required: true
          default: '300'
          value: ""
    - displayName:
        zh-CN: "通知"
        en: "Notification"
      items:
        - name: "UseNotification"
          schema:
            type: boolean
          display:
            type: boolean
            name:
              zh-CN: "开启通知"
              en: "Use Notification"
            description:
              zh-CN: "开启后，可选择已创建好的通知，当流水线执行完成后，会根据通知设置发送通知。"
              en: "After opening, you can choose the created notification. When the pipeline is completed, the notification will be sent according to the notification settings."
          required: false
          value: "false"
        - name: "PlatformNotification"
          schema:
            type: alauda.io/notificationmix
          required: true
          binding:
            - notification.args.PlatformNotification
          value: ""
          display:
            type: alauda.io/notificationmix
            name:
              zh-CN: "通知"
              en: Notification
            description:
              zh-CN: "选择已创建好的通知（由管理员创建），将根据选择对流水线进行通知。"
              en: "Select the created notification (created by the administrator), and the pipeline will be notified according to the selection."
          relation:
            - action: show
              when:
                name: UseNotification
                value: true
  supportedTriggers:
  - type: cron
  - type: codeChange