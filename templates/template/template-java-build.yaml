apiVersion: devops.alauda.io/v1alpha1
kind: ClusterPipelineTemplate
metadata:
  name: JavaBuilder
  namespace: default
  annotations:
    displayName.zh-CN: "Java 构建"
    displayName.en: "Java Build"
    description.zh-CN: "克隆代码 -> Java 构建 -> 代码扫描 -> Docker 构建"
    description.en: "Clone -> Java Build -> Code Scan -> Docker Build"
    version: "2.10.1"
    style.icon: java,maven,docker,sonarqube
  labels:
    category: Build
    lang: "Java"
spec:
  engine: graph
  withSCM: true
  agent:
    label: java
    labelMatcher: java.*|maven.*
  options:
    raw: "buildDiscarder(logRotator(numToKeepStr: '200'))"
  stages:
    - name: Clone
      tasks:
        - name: clone
          kind: ClusterPipelineTaskTemplate
          type: public/clone
    - name: "Java Build"
      tasks:
        - name: maven
          kind: ClusterPipelineTaskTemplate
          type: public/maven
    - name: "Code Scan"
      tasks:
        - name: sonar
          kind: ClusterPipelineTaskTemplate
          type: public/sonar
          relation:
            - action: show
              when:
                name: UseSonarQube
                value: true
    - name: "Docker Build"
      tasks:
        - name: build-docker
          kind: ClusterPipelineTaskTemplate
          type: public/build-docker
  post:
    always:
      - name: notification
        kind: ClusterPipelineTaskTemplate
        relation:
          - action: show
            when:
              name: UseNotification
              value: true
  arguments:
    - displayName:
        zh-CN: "代码检出"
        en: "Clone"
      items:
        - name: "PlatformCodeRepository"
          schema:
            type: alauda.io/coderepositorymix
          required: true
          value: ""
          binding:
            - clone.args.PlatformCodeRepository
          display:
            type: alauda.io/coderepositorymix
            name:
              zh-CN: "代码仓库"
              en: RepositoryPath
            description:
              zh-CN: "选择已为项目分配的代码仓库"
              en: ""
        - name: "Branch"
          schema:
            type: string
          required: false
          value: ""
          binding:
            - clone.args.Branch
          display:
            type: alauda.io/codebranch
            advanced: false
            name:
              zh-CN: "分支"
              en: "Branch"
            description:
              zh-CN: "检出代码仓库中的分支"
              en: "The code repository branch that you want to check out."
            related: PlatformCodeRepository
        - name: "RelativeDirectory"
          schema:
            type: string
          required: false
          value: ""
          binding:
            - clone.args.RelativeDirectory
          default: "."
          display:
            type: string
            advanced: true
            name:
              zh-CN: "相对目录"
              en: RelativeDirectory
            description:
              zh-CN: "指定检出 Git 仓库的本地目录(相对于 workspace 根目录)。若为空，将使用 workspace 根目录"
              en: "Specify a local directory (relative to the workspace root) where the Git repository will be checked out. If left empty, the workspace root itself will be used"
    - displayName:
        zh-CN: "Maven 构建"
        en: "Maven Build"
      items:
        - name: "buildCommand"
          schema:
            type: string
          binding:
            - maven.args.buildCommand
          display:
            type: code
            advanced: false
            name:
              zh-CN: "构建命令"
              en: "Build Command"
            description:
              zh-CN: "自定义更详细的构建命令。默认为：mvn clean package"
              en: ""
          required: false
          default: 'mvn clean package'
          value: ""
        - name: "showTestResult"
          schema:
            type: boolean
          required: false
          value: "false"
          binding:
            - maven.args.showTestResult
          display:
            type: boolean
            advanced: false
            name:
              zh-CN: "展示 JUnit 测试报告"
              en: "Show JUnit test report"
            description:
              zh-CN: "开启后，将在流水线执行记录详情页展示 JUnit 测试报告"
              en: ""
        - name: "testResultLocation"
          schema:
            type: string
          required: true
          value: ""
          binding:
            - maven.args.testResultLocation
          default: "**/target/surefire-reports/**/*.xml"
          display:
            type: string
            advanced: false
            name:
              zh-CN: "测试结果位置"
              en: testResultLocation
            description:
              zh-CN: "例如，**/target/surefire-reports/**/*.xml"
              en: ""
          relation:
            - action: show
              when:
                name: showTestResult
                value: true
    - displayName:
        zh-CN: "代码扫描"
        en: "Code Scanning"
      items:
        - name: "UseSonarQube"
          schema:
            type: boolean
          display:
            type: boolean
            name:
              zh-CN: "开启代码扫描"
              en: "Use code analysis"
            description:
              zh-CN: "开启后，进行代码扫描"
              en: "Whether to use Sonarqube for code scan"
          required: false
          value: "false"
        - name: "CodeQualityBinding"
          schema:
            type: alauda.io/toolbinding
          binding:
            - sonar.args.CodeQualityBinding
          required: true
          value: ""
          display:
            type: alauda.io/toolbinding
            args:
              bindingKind: codequalitytool
              bindingToolType: Sonarqube
            name:
              zh-CN: "SonarQube"
              en: "SonarQube"
            description:
              zh-CN: "选择要使用的 SonarQube"
              en: "Select a SonarQube binding"
          relation:
            - action: show
              when:
                name: UseSonarQube
                value: true
        - name: "EnableBranchAnalysis"
          schema:
            type: boolean
          required: false
          binding:
            - sonar.args.EnableBranchAnalysis
          value: "false"
          display:
            type: boolean
            advanced: true
            name:
              zh-CN: "区分代码分支"
              en: "Enable branch analysis"
            description:
              zh-CN: "扫描主分支以外的分支时，若想查看这些分支的扫描结果，需要在 Sonarqube 支持多代码分支且要至少扫描过一次主分支的前提下，开启此开关才可扫描成功。"
              en: "When scanning branches other than the main branch while keeping separate results, the Sonarqube instance needs to support branch analysis and turn on this feature"
          relation:
            - action: show
              when:
                name: UseSonarQube
                value: true
        - name: "AnalysisParameters"
          schema:
            type: string
          required: false
          binding:
            - sonar.args.AnalysisParameters
          value: |
            sonar.sources=./src
            sonar.java.binaries=./target/classes
            sonar.sourceEncoding=UTF-8
          display:
            type: stringMultiline
            advanced: false
            name:
              zh-CN: "代码扫描参数"
              en: "Analysis Parameters"
            description:
              zh-CN: "为 Sonar Scanner 设置分析参数。当目录中存在 sonar-project.properties 文件时，将会忽略这个参数。详细参数见文档: https://docs.sonarqube.org/latest/analysis/analysis-parameters 和 https://docs.sonarqube.org/display/PLUG/Java+Plugin+and+Bytecode"
              en: "Set analysis parameters for Sonar Scanner. When 'sonar-project.properties' existed in project, this argument will be omitted. see https://docs.sonarqube.org/latest/analysis/analysis-parameters and https://docs.sonarqube.org/display/PLUG/Java+Plugin+and+Bytecode for more details"
          relation:
            - action: show
              when:
                name: UseSonarQube
                value: true
        - name: "FailedIfNotPassQualityGate"
          schema:
            type: boolean
          required: false
          binding:
            - sonar.args.FailedIfNotPassQualityGate
          value: "false"
          display:
            type: boolean
            advanced: true
            name:
              zh-CN: "质量阈未通过终止流水线"
              en: "Fail pipeline when quality gate fails"
            description:
              zh-CN: "开启后，代码扫描结果为失败时，流水线执行状态变为失败，终止流水线"
              en: "Fails pipeline execution when quality gate fails"
          relation:
            - action: show
              when:
                name: UseSonarQube
                value: true
    - displayName:
        zh-CN: "Docker 构建"
        en: "Docker Build"
      items:
        - name: "imageRepository"
          schema:
            type: alauda.io/dockerimagerepositorymix
          binding:
            - build-docker.args.imageRepository
          required: true
          value: ""
          display:
            type: alauda.io/dockerimagerepositorymix
            name:
              zh-CN: "镜像仓库"
              en: Repository
            description:
              zh-CN: "选择已为项目分配的镜像仓库或者输入地址"
              en: ""
        - name: "dockerfile"
          schema:
            type: string
          binding:
            - build-docker.args.dockerfile
          display:
            type: string
            advanced: true
            name:
              zh-CN: "Dockerfile"
              en: "Dockerfile"
            description:
              zh-CN: "Dockerfile 在代码仓库中的绝对路径"
              en: ""
          required: true
          default: 'Dockerfile'
          value: ""
        - name: "context"
          schema:
            type: string
          binding:
            - build-docker.args.context
          display:
            type: string
            advanced: true
            name:
              zh-CN: "构建上下文"
              en: "Build Context"
            description:
              zh-CN: "构建过程可以引用上下文中的任何文件。例如，构建中可以使用 COPY 命令在上下文中引用文件"
              en: "The build process can refer to any of the files in the context. For example, your build can use a COPY instruction to reference a file in the context"
          required: true
          default: '.'
          value: ""
        - name: "buildArguments"
          schema:
            type: string
          binding:
            - build-docker.args.buildArguments
          display:
            type: string
            advanced: true
            name:
              zh-CN: "构建参数"
              en: "Build Arguments"
            description:
              zh-CN: "自定义docker build Options，如 --add-host，多个 Options 用空格隔开；可参考 https://docs.docker.com/engine/reference/commandline/build/"
              en: ""
          required: false
          default: ''
          value: ""
        - name: "retry"
          schema:
            type: string
          binding:
            - build-docker.args.retry
          display:
            type: string
            advanced: true
            name:
              zh-CN: "重试次数"
              en: "Retry Times"
            description:
              zh-CN: "生成镜像时的失败重试次数"
              en: ""
          required: false
          default: '3'
          value: ""
    - displayName:
        zh-CN: "通知"
        en: "Notification"
      items:
        - name: "UseNotification"
          schema:
            type: boolean
          display:
            type: boolean
            name:
              zh-CN: "开启通知"
              en: "Use Notification"
            description:
              zh-CN: "开启后，可选择已创建好的通知，当流水线执行完成后，会根据通知设置发送通知。"
              en: "After opening, you can choose the created notification. When the pipeline is completed, the notification will be sent according to the notification settings."
          required: false
          value: "false"
        - name: "PlatformNotification"
          schema:
            type: alauda.io/notificationmix
          required: true
          binding:
            - notification.args.PlatformNotification
          value: ""
          display:
            type: alauda.io/notificationmix
            name:
              zh-CN: "通知"
              en: Notification
            description:
              zh-CN: "选择已创建好的通知（由管理员创建），将根据选择对流水线进行通知。"
              en: "Select the created notification (created by the administrator), and the pipeline will be notified according to the selection."
          relation:
            - action: show
              when:
                name: UseNotification
                value: true
  supportedTriggers:
  - type: cron
  - type: codeChange
