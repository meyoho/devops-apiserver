apiVersion: devops.alauda.io/v1alpha1
kind: ClusterPipelineTaskTemplate
metadata:
  name: clone
  namespace: default
  annotations:
    displayName.zh-CN: "Git Clone"
    displayName.en: "Git Clone"
    description.zh-CN: "Git Clone"
    description.en: "Git Clone"
    version: "2.10.0"
    style.icon: ""
  labels:
    category: CI
spec:
  engine: gotpl
  body: |+
    script {
      def codeRepository = "{{.PlatformCodeRepository.url}}"
      def credential_id = ""
      env.CODE_REPO = codeRepository
      {{ if .PlatformCodeRepository.bindingRepositoryName -}}
      try {
        alaudaDevops.withCluster(){
          alaudaDevops.withProject(alaudaContext.getNamespace()){
            def coderepo = alaudaDevops.selector("coderepository", "{{.PlatformCodeRepository.bindingRepositoryName}}").object()
            if(coderepo) {
              credential_id = coderepo.metadata.annotations.secretNamespace + '-' + coderepo.metadata.annotations.secretName
            }
          }
        }
      } catch(err) {
        echo err.getMessage()
      }
      {{- end }}
      if ( credential_id == "") {
        credential_id = "{{.PlatformCodeRepository.credentialId}}"
      }

      env.CREDENTIAL_ID = credential_id
      def relativeDirectory = "{{.RelativeDirectory}}"
      env.RELATIVE_DIRECTORY = relativeDirectory

      {{if ne .PlatformCodeRepository.sourceType "SVN" -}}

      def branchFromInput = "{{.Branch}}"

      if("".equals(branchFromInput)){
        branchFromInput = "master"
      }

      env.BRANCH = branchFromInput

      def scmVars = checkout([
        $class: 'GitSCM',
        branches: [[name: branchFromInput]],
        extensions: [[
          $class: 'SubmoduleOption',
          recursiveSubmodules: true,
          parentCredentials: true,
          reference: '',
        ],[
          $class: 'RelativeTargetDirectory',
          relativeTargetDir: relativeDirectory
        ]],
        userRemoteConfigs: [[
          credentialsId: credential_id,
          url: codeRepository
        ]]
      ])

      env.GIT_COMMIT = scmVars.GIT_COMMIT
      env.GIT_BRANCH = scmVars.GIT_BRANCH
      env.GIT_BRANCH_AS_TAG = scmVars.GIT_BRANCH.replaceFirst("origin/","").replaceAll("/","-")
      alaudaPipeline.appendInfo(STAGE_NAME, [commit_id: scmVars.GIT_COMMIT, branch: scmVars.GIT_BRANCH, repo_url: codeRepository as String], '_Clone')
      {{- end}}

      {{if eq .PlatformCodeRepository.sourceType "SVN" -}}
      {{if eq .RelativeDirectory "" -}}
        env.RELATIVE_DIRECTORY = "."
        relativeDirectory = "."
      {{- end}}
      def scmVars = checkout([
          $class: "SubversionSCM",
          additionalCredentials: [],
          excludedCommitMessages: "",
          excludedRegions: "",
          excludedRevprop: "",
          excludedUsers: "",
          filterChangelog: false,
          ignoreDirPropChanges: false,
          includedRegions: "",
          locations: [[
            credentialsId: credential_id, depthOption: "infinity", ignoreExternalsOption: true, local: relativeDirectory, remote: codeRepository
          ]],
          quietOperation: true, workspaceUpdater: [$class: "UpdateUpdater"]
        ])
        SVN_REVISION = scmVars.SVN_REVISION
        env.SVN_REVISION = SVN_REVISION
        env.CODE_COMMIT = SVN_REVISION
      {{- end}}
      env.TIMESTAMP = new Date().format("yyyyMMddHHmmss")
    }
  exports:
    - name: GIT_COMMIT
      description:
        zh-CN: "Git Commit ID"
        en: "Git Commit ID"
    - name: GIT_BRANCH
      description:
        zh-CN: "Git 分支名称"
        en: "Git Branch Name"
    - name: RELATIVE_DIRECTORY
      description:
        zh-CN: "相对目录"
        en: "RelativeDirectory"
    - name: GIT_BRANCH_AS_TAG
      description:
        zh-CN: "可以作为 Tag 的 Git Branch Name"
        en: "Git Branch Without Origin"
    - name: TIMESTAMP
      description:
        zh-CN: "时间戳: yyyyMMddHHmmss"
        en: "The time stamp: yyyyMMddHHmmss"
  arguments:
    - name: "PlatformCodeRepository"
      schema:
        type: alauda.io/coderepositorymix
      required: true
      display:
        type: alauda.io/coderepositorymix
        name:
          zh-CN: "代码仓库"
          en: RepositoryPath
        description:
          zh-CN: "选择已为项目分配的代码仓库"
          en: "Select a code repository for the project."
    - name: "Branch"
      schema:
        type: string
      required: false
      display:
        type: alauda.io/codebranch
        name:
          zh-CN: "分支"
          en: Branch
        description:
          zh-CN: "检出代码仓库中的分支"
          en: "The code repository branch that you want to check out. "
        related: PlatformCodeRepository
    - name: "RelativeDirectory"
      schema:
        type: string
      required: false
      default: "."
      display:
        type: string
        name:
          zh-CN: "相对目录"
          en: RelativeDirectory
        description:
          zh-CN: "指定签出 Git 仓库的本地目录(相对于 workspace 根目录)。若为空，将使用 workspace 根目录"
          en: "Specify a local directory (relative to the workspace root) where the Git repository will be checked out. If left empty, the workspace root itself will be used"
  view:
    markdown: |-
      {{ if eq $item.type "_Clone"}}
      ## {{$item.name}}
      
      | Name | Value |
      | :--- | :---- |
      | commitID | {{$item.value.commit_id}} |
      | 分支 | {{$item.value.branch}} |
      | 代码仓库 | [{{$item.value.repo_url}}]({{$item.value.repo_url}}) |
      
      {{ end}}