FROM golang:1.12-alpine3.9

ENV TERM=xterm \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    LC_CTYPE=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8

RUN apk add --no-cache --update \
    build-base curl bash git tar zip unzip ca-certificates docker upx \
    wget rsync

ENV OPENAPI_GEN_VERSION=release-1.10
ENV KUBERNETES_VERSION=release-1.13
ENV CODEGEN_VERSION=release-1.13
ENV GOCLIENT_VERSION=release-10.0
ENV GO111MODULE=on

RUN set -ex && go get -v github.com/tools/godep

RUN set -ex \
    && go get -v github.com/tools/godep \
    && mkdir -p ${GOPATH}/src/k8s.io \
    && cd ${GOPATH}/src/k8s.io \
    && git clone --progress --depth=1 --branch ${OPENAPI_GEN_VERSION} https://github.com/kubernetes/kube-openapi \
    && cd kube-openapi && go mod init && go install k8s.io/kube-openapi/example/openapi-gen \
    && cd ${GOPATH}/src/k8s.io \
    && git clone --progress --depth=1 --branch ${CODEGEN_VERSION} https://github.com/kubernetes/code-generator \
    && git clone --progress --depth=1 --branch ${KUBERNETES_VERSION} https://github.com/kubernetes/apimachinery \
    && git clone --progress --depth=1 --branch ${KUBERNETES_VERSION} https://github.com/kubernetes/api \
    && git clone --progress --depth=1 --branch ${KUBERNETES_VERSION} https://github.com/kubernetes/apiserver \
    && git clone --progress --depth=1 --branch ${GOCLIENT_VERSION} https://github.com/kubernetes/client-go \
    && git clone --progress --depth=1 --branch ${KUBERNETES_VERSION} https://github.com/kubernetes/apiextensions-apiserver

ARG commit_id=dev
ARG app_version=dev
ENV COMMIT_ID=${commit_id}
ENV APP_VERSION=${app_version}