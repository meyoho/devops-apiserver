#!/bin/bash

reply=$(curl -s -o /dev/null -w %{http_code} https://localhost/healthz --insecure);
if [ "$reply" -lt 200 -o "$reply" -ge 400 ]; then exit 1; fi;