#!/bin/bash

# Copyright 2017 The Kubernetes Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -o errexit
set -o nounset
set -o pipefail
GO111MODULE=off

SCRIPT_ROOT=$(dirname ${BASH_SOURCE})/..
CODEGEN_PKG=${CODEGEN_PKG:-$(cd ${SCRIPT_ROOT}; ls -d -1 ./vendor/k8s.io/code-generator 2>/dev/null || echo ../../k8s.io/code-generator)}


# generate the code with:
# --output-base    because this script should also be able to run inside the vendor dir of
#                  k8s.io/kubernetes. The output-base is needed for the generators to output into the vendor dir
#                  instead of the $GOPATH directly. For normal projects this can be dropped.
${CODEGEN_PKG}/generate-internal-groups.sh all \
  alauda.io/devops-apiserver/pkg/client alauda.io/devops-apiserver/pkg/apis alauda.io/devops-apiserver/pkg/apis \
  devops:v1alpha1 \
  --output-base "$(dirname ${BASH_SOURCE})/../../.." \
  --go-header-file ${SCRIPT_ROOT}/hack/custom-boilerplate.go.txt

if ! type "openapi-gen" > /dev/null; then
  echo "Installing openapi-gen..."
  go install ${CODEGEN_PKG}/cmd/openapi-gen
fi

echo "Generating OpenAPI..."
${GOPATH}/bin/openapi-gen \
  --input-dirs alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1,k8s.io/apimachinery/pkg/apis/meta/v1,k8s.io/apimachinery/pkg/apis/meta/v1beta1,k8s.io/apimachinery/pkg/runtime,k8s.io/apimachinery/pkg/version \
  --output-package alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1 \
  --output-base ${GOPATH}/src/ --go-header-file ./hack/custom-boilerplate.go.txt

# openapi-gen will import v1alpha1 again , so delete it
N=`cat ./pkg/apis/devops/v1alpha1/openapi_generated.go | grep -n "v1alpha1 \"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1\"" | awk -F ':' '{print $1}'`
sed -i -e "${N}d" ./pkg/apis/devops/v1alpha1/openapi_generated.go
rm ./pkg/apis/devops/v1alpha1/openapi_generated.go-e || true