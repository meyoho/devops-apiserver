docker run --name gen-client --rm -w /go/src/alauda.io/devops-apiserver \
  -v ${PWD}:/go/src/alauda.io/devops-apiserver \
  -v ${GOPATH}/pkg/mod:/go/pkg/mod \
  -e GO111MODULE=off \
  -e GOPROXY=https://athens.acp.alauda.cn \
  index.alauda.cn/alaudak8s/devops-apiserver-tool:v2.2 ./hack/update-codegen.ori.sh

echo "Generating ToolChain scheme and Role Sync scheme..."
GO111MODULE=on go generate ./pkg/apis/...