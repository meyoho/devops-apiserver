#!/bin/bash

set -o pipefail

NAMESPACE="${1:-alauda-system}"
CONFIGMAP="${2:-jenkins-auto}"
EXPORT_FILE="${3:-false}"

kubectl get configmap -n $NAMESPACE $CONFIGMAP -o yaml > tmp.cm.yaml
if [ "$?" == "0" ]; then
    yq r tmp.cm.yaml data > tmp.cm.data.yaml
    while read LINE
    do
        IFS=":"
        for i in ${LINE[@]}; do
            IFS=""
            if [ "${EXPORT_FILE}" == "false" ]; then
                echo "export ${i}=$(echo \"$(yq r tmp.cm.data.yaml $i)\")"
                # export "${i}"=$(echo \"$(yq r tmp.cm.data.yaml $i)\")
            else
                yq w -i $EXPORT_FILE data.$i "$(echo \"$(yq r tmp.cm.data.yaml $i)\")"
            fi
            break
        done 
    done < tmp.cm.data.yaml
    rm tmp.cm.data.yaml
else
    echo "nevermind"
fi
env | grep JENKINS
rm tmp.cm.yaml