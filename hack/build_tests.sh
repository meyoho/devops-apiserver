#!/bin/sh
CGO_ENABLED=0 ginkgo build test/...

mkdir -p testbin || true
cp -rf test/* testbin/

find ./testbin -name "*.go" -exec rm {} \;
find ./testbin -name "*.xml" -exec rm {} \;
