package v1

import (
	"bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bitbucket.org/mathildetech/devops-client/pkg/generic"
	"bitbucket.org/mathildetech/devops-client/pkg/jenkins/v1/client"
	"bitbucket.org/mathildetech/devops-client/pkg/jenkins/v1/client/operations"
	"bitbucket.org/mathildetech/devops-client/pkg/jenkins/v1/models"
	"bitbucket.org/mathildetech/devops-client/pkg/transport"
	"context"
	"errors"
	"fmt"
	"github.com/go-logr/logr"
	"github.com/go-openapi/runtime"
	"io"
	"net/http"
	"strconv"

	openapi "github.com/go-openapi/runtime/client"
)

type Client struct {
	v1.NotImplement
	logger     logr.Logger
	client     *client.Jenkins
	opts       *v1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ v1.Interface = &Client{}

func NewFactory() v1.ClientFactory {
	return func(opts *v1.Options) v1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		return generic.CheckService(c.opts.BasicConfig.Host, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(ctx context.Context) (*v1.HostPortStatus, error) {
	return nil, nil
}

func (c *Client) GetJobProgressiveLog(ctx context.Context, pipelineNamespace, pipelineName string, buildNumber, start int64) (*v1.PipelineLog, error) {
	progressiveTextParams := operations.
		NewGetJobProgressiveTextParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithBuildNumber(buildNumber).
		WithStart(&start)

	response, err := c.client.Operations.GetJobProgressiveText(progressiveTextParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	textSize, err := strconv.ParseInt(response.XTextSize, 10, 64)
	if err != nil {
		textSize = 0
	}

	return &v1.PipelineLog{
		Text:      response.Payload,
		HasMore:   response.XMoreData,
		NextStart: &textSize,
	}, nil
}

func (c *Client) GetBranchJobProgressiveLog(ctx context.Context, pipelineNamespace, pipelineName, branchName string, buildNumber, start int64) (*v1.PipelineLog, error) {
	progressiveTextParams := operations.
		NewGetBranchJobProgressiveTextParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithBranchName(branchName).
		WithBuildNumber(buildNumber).
		WithStart(&start)

	response, err := c.client.Operations.GetBranchJobProgressiveText(progressiveTextParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	textSize, err := strconv.ParseInt(response.XTextSize, 10, 64)
	if err != nil {
		textSize = 0
	}

	return &v1.PipelineLog{
		Text:      response.Payload,
		HasMore:   response.XMoreData,
		NextStart: &textSize,
	}, nil
}

func (c *Client) GetJobStepLog(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId, stepId string, start int64) (*v1.PipelineLog, error) {
	stepLogParams := operations.
		NewGetPipelineRunNodeStepLogParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithOrganization(organization).
		WithRun(runId).
		WithNode(nodeId).
		WithStep(stepId).
		WithStart(&start)

	response, err := c.client.Operations.GetPipelineRunNodeStepLog(stepLogParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	textSize, err := strconv.ParseInt(response.XTextSize, 10, 64)
	if err != nil {
		textSize = 0
	}

	return &v1.PipelineLog{
		Text:      response.Payload,
		HasMore:   response.XMoreData,
		NextStart: &textSize,
	}, nil
}

func (c *Client) GetArtifactList(ctx context.Context, pipelineNamespace, pipelineName, organization, run string) (models.ArtifactList, error) {
	artifactListParams := operations.
		NewGetPipelineArtifactsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithOrganization(organization).
		WithRun(run)

	response, err := c.client.Operations.GetPipelineArtifacts(artifactListParams, c.authInfo)
	if err != nil {
		return nil, err
	}
	return response.Payload, err
}
func (c *Client) DownloadArtifacts(ctx context.Context, namespace, pcname, runid string) (body io.ReadCloser, contenttype string, err error) {
	var path string
	scheme := c.opts.BasicConfig.Schemes[0]
	host := getschemeshost(scheme,c.opts.BasicConfig.Host)
	path = fmt.Sprintf("%s/job/%s/job/%s-%s/%s/artifact/*zip*/archive.zip", host, namespace, namespace, pcname, runid)
	body,contenttype,err = c.download(path)
	return body, contenttype, nil
}

func (c *Client) DownloadArtifact(context context.Context, namespace string, pcname string, runid string, filename string)(body io.ReadCloser, contenttype string, err error){
	var path string
	scheme := c.opts.BasicConfig.Schemes[0]
	host := getschemeshost(scheme,c.opts.BasicConfig.Host)
	path = fmt.Sprintf("%s/job/%s/job/%s-%s/%s/artifact/%s", host, namespace, namespace, pcname, runid, filename)

	if filename == "pipeline.log" {
		path = fmt.Sprintf("%s/blue/rest/organizations/%s/pipelines/%s/pipelines/%s-%s/runs/%s/log/?start=0&download=true", c.opts.BasicConfig.BasePath, "jenkins", namespace, namespace, pcname, runid)
	}
	body,contenttype,err = c.download(path)
	return body, contenttype, nil
}

func getschemeshost(scheme,host string)string{
	return fmt.Sprintf("%s://%s",scheme,host)
}

func (c *Client)download(path string)(body io.ReadCloser, contenttype string, err error){

	request, err := http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		return nil, "", err
	}
	request.Header = map[string][]string{}
	request.Header.Set("Authorization", "Basic "+c.opts.BearerToken.Token)
	resp, err := c.httpClient.Do(request)
	if err != nil {
		return nil, "", err
	}
	contenttype = resp.Header.Get("Content-Type")
	return resp.Body, contenttype, nil
}

func (c *Client) GetBranchJobStepLog(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, stepId string, start int64) (*v1.PipelineLog, error) {
	stepLogParams := operations.
		NewGetBranchPipelineRunNodeStepLogParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithBranchName(branchName).
		WithOrganization(organization).
		WithRun(runId).
		WithNode(nodeId).
		WithStep(stepId).
		WithStart(&start)

	response, err := c.client.Operations.GetBranchPipelineRunNodeStepLog(stepLogParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	textSize, err := strconv.ParseInt(response.XTextSize, 10, 64)
	if err != nil {
		textSize = 0
	}

	return &v1.PipelineLog{
		Text:      response.Payload,
		HasMore:   response.XMoreData,
		NextStart: &textSize,
	}, nil
}

func (c *Client) GetJobNodeLog(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId string, start int64) (*v1.PipelineLog, error) {
	nodeLogParams := operations.
		NewGetPipelineRunNodeLogParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithOrganization(organization).
		WithRun(runId).
		WithNode(nodeId).
		WithStart(&start)

	response, err := c.client.Operations.GetPipelineRunNodeLog(nodeLogParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	textSize, err := strconv.ParseInt(response.XTextSize, 10, 64)
	if err != nil {
		textSize = 0
	}

	return &v1.PipelineLog{
		Text:      response.Payload,
		HasMore:   response.XMoreData,
		NextStart: &textSize,
	}, nil
}

func (c *Client) GetBranchJobNodeLog(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string, start int64) (*v1.PipelineLog, error) {
	nodeLogParams := operations.
		NewGetBranchPipelineRunNodeLogParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithBranchName(branchName).
		WithOrganization(organization).
		WithRun(runId).
		WithNode(nodeId).
		WithStart(&start)

	response, err := c.client.Operations.GetBranchPipelineRunNodeLog(nodeLogParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	textSize, err := strconv.ParseInt(response.XTextSize, 10, 64)
	if err != nil {
		textSize = 0
	}

	return &v1.PipelineLog{
		Text:      response.Payload,
		HasMore:   response.XMoreData,
		NextStart: &textSize,
	}, nil
}

func (c *Client) GetBranchProgressiveScanLog(ctx context.Context, pipelineNamespace, pipelineName string, start int64) (*v1.PipelineLog, error) {
	scanLogParams := operations.
		NewGetBranchJobProgressiveScanLogParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithStart(&start)

	response, err := c.client.Operations.GetBranchJobProgressiveScanLog(scanLogParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	textSize, err := strconv.ParseInt(response.XTextSize, 10, 64)
	if err != nil {
		textSize = 0
	}

	return &v1.PipelineLog{
		Text:      response.Payload,
		HasMore:   response.XMoreData,
		NextStart: &textSize,
	}, nil
}

func (c *Client) ScanMultiBranchJob(ctx context.Context, pipelineNamespace, pipelineName string) error {
	crumbIssuer, err := c.getJenkinsCrumb(ctx)
	if err != nil {
		return err
	}

	scanLogParams := operations.
		NewScanMulitiBranchJobParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithJenkinsCrumb(&crumbIssuer.Crumb)

	_, err = c.client.Operations.ScanMulitiBranchJob(scanLogParams, c.authInfo)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) getJenkinsCrumb(ctx context.Context) (*models.DefaultCrumbIssuer, error) {
	crumbParams := operations.
		NewGetCrumbeParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)

	response, err := c.client.Operations.GetCrumbe(crumbParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	return response.Payload, nil
}

func (c *Client) GetJobNodes(ctx context.Context, pipelineNamespace, pipelineName, organization, runId string) ([]v1.PipelineBlueOceanTask, error) {
	jobNodesParams := operations.
		NewGetPipelineRunNodesParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithOrganization(organization).
		WithRun(runId)

	response, err := c.client.Operations.GetPipelineRunNodes(jobNodesParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	var tasks []v1.PipelineBlueOceanTask
	for _, node := range response.Payload {

		task := c.convertNodeToTask(node)

		tasks = append(tasks, task)
	}

	return tasks, nil
}

func (c *Client) GetBranchJobNodes(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId string) ([]v1.PipelineBlueOceanTask, error) {
	jobNodesParams := operations.
		NewGetBranchPipelineRunNodesParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithBranchName(branchName).
		WithOrganization(organization).
		WithRun(runId)

	response, err := c.client.Operations.GetBranchPipelineRunNodes(jobNodesParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	var tasks []v1.PipelineBlueOceanTask
	for _, node := range response.Payload {

		task := c.convertNodeToTask(node)
		tasks = append(tasks, task)
	}

	return tasks, nil
}

func (c *Client) convertNodeToTask(node *models.PipelineRunNode) v1.PipelineBlueOceanTask {
	task := v1.PipelineBlueOceanTask{
		PipelineBlueOceanRef: v1.PipelineBlueOceanRef{
			ID:   node.ID,
			Type: node.Type,
		},
		DisplayName:      node.DisplayName,
		DurationInMillis: node.DurationInMillis,
		Result:           node.Result,
		State:            node.State,
		StartTime:        node.StartTime,
	}

	for _, action := range node.Actions {
		task.Actions = append(task.Actions, v1.PipelineBlueOceanRef{
			URLName: action.URLName,
		})
	}

	for _, edge := range node.Edges {
		edge := v1.PipelineBlueOceanRef{
			ID:   edge.ID,
			Type: edge.Type,
		}
		task.Edges = append(task.Edges, edge)
	}
	return task
}

func (c *Client) GetJobSteps(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId string) ([]v1.PipelineBlueOceanTask, error) {
	jobStepsParams := operations.
		NewGetPipelineRunNodeStepsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithOrganization(organization).
		WithRun(runId).
		WithNode(nodeId)

	response, err := c.client.Operations.GetPipelineRunNodeSteps(jobStepsParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	var tasks []v1.PipelineBlueOceanTask
	for _, step := range response.Payload {

		task := c.convertStepToTask(step)
		tasks = append(tasks, task)
	}

	return tasks, nil
}

func (c *Client) GetBranchJobSteps(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string) ([]v1.PipelineBlueOceanTask, error) {
	jobStepsParams := operations.
		NewGetBranchPipelineRunNodeStepsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithBranchName(branchName).
		WithOrganization(organization).
		WithRun(runId).
		WithNode(nodeId)

	response, err := c.client.Operations.GetBranchPipelineRunNodeSteps(jobStepsParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	var tasks []v1.PipelineBlueOceanTask
	for _, step := range response.Payload {

		task := c.convertStepToTask(step)
		tasks = append(tasks, task)
	}

	return tasks, nil
}

func (c *Client) GetBranchTestReportSummary(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId string) (*v1.TestReportSummary, error) {
	testReportSummaryParams := operations.
		NewGetBranchPipelineTestSummaryParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithOrganization(organization).
		WithBranchName(branchName).
		WithRun(runId)

	response, err := c.client.Operations.GetBranchPipelineTestSummary(testReportSummaryParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	payload := response.Payload
	testReportSummary := &v1.TestReportSummary{
		ExistingFailed: payload.ExistingFailed,
		Failed:         payload.Failed,
		Fixed:          payload.Fixed,
		Passed:         payload.Passed,
		Regressions:    payload.Regressions,
		Skipped:        payload.Skipped,
		Total:          payload.Total,
	}

	return testReportSummary, nil
}

func (c *Client) GetTestReportSummary(ctx context.Context, pipelineNamespace, pipelineName, organization, runId string) (*v1.TestReportSummary, error) {
	testReportSummaryParams := operations.
		NewGetPipelineTestSummaryParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithOrganization(organization).
		WithRun(runId)

	response, err := c.client.Operations.GetPipelineTestSummary(testReportSummaryParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	payload := response.Payload
	testReportSummary := &v1.TestReportSummary{
		ExistingFailed: payload.ExistingFailed,
		Failed:         payload.Failed,
		Fixed:          payload.Fixed,
		Passed:         payload.Passed,
		Regressions:    payload.Regressions,
		Skipped:        payload.Skipped,
		Total:          payload.Total,
	}

	return testReportSummary, nil
}

func (c *Client) GetBranchTestReports(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, status, state, stateBang string, start, limit int64) (*v1.PipelineTestReport, error) {
	testReportsParams := operations.
		NewGetBranchPipelineTestReportsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithOrganization(organization).
		WithBranchName(branchName).
		WithStatus(&status).
		WithState(&state).
		WithStateBang(&stateBang).
		WithStart(start).
		WithLimit(limit).
		WithRun(runId)

	response, err := c.client.Operations.GetBranchPipelineTestReports(testReportsParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	items := make([]v1.PipelineTestReportItem, 0)
	for _, item := range response.Payload {
		items = append(items, v1.PipelineTestReportItem{
			Age:             item.Age,
			Duration:        item.Duration,
			ErrorDetails:    item.ErrorDetails,
			ErrorStackTrace: item.ErrorStackTrace,
			HasStdLog:       item.HasStdLog,
			ID:              item.ID,
			Name:            item.Name,
			State:           item.State,
			Status:          item.Status,
		})
	}

	return &v1.PipelineTestReport{
		Items: items,
	}, nil
}

func (c *Client) GetTestReports(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, status, state, stateBang string, start, limit int64) (*v1.PipelineTestReport, error) {
	testReportsParams := operations.
		NewGetPipelineTestReportsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithOrganization(organization).
		WithStatus(&status).
		WithState(&state).
		WithStateBang(&stateBang).
		WithStart(start).
		WithLimit(limit).
		WithRun(runId)

	response, err := c.client.Operations.GetPipelineTestReports(testReportsParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	items := make([]v1.PipelineTestReportItem, 0)
	for _, item := range response.Payload {
		items = append(items, v1.PipelineTestReportItem{
			Age:             item.Age,
			Duration:        item.Duration,
			ErrorDetails:    item.ErrorDetails,
			ErrorStackTrace: item.ErrorStackTrace,
			HasStdLog:       item.HasStdLog,
			ID:              item.ID,
			Name:            item.Name,
			State:           item.State,
			Status:          item.Status,
		})
	}

	return &v1.PipelineTestReport{
		Items: items,
	}, nil
}

func (c *Client) convertStepToTask(step *models.PipelineStepImpl) v1.PipelineBlueOceanTask {
	task := v1.PipelineBlueOceanTask{
		PipelineBlueOceanRef: v1.PipelineBlueOceanRef{
			ID:   step.ID,
			Type: step.Type,
		},
		DisplayName:        step.DisplayName,
		DisplayDescription: step.DisplayDescription,
		DurationInMillis:   step.DurationInMillis,
		Result:             step.Result,
		State:              step.State,
		StartTime:          step.StartTime,
	}

	if step.Links != nil {
		if step.Links.Self != nil {
			task.Href = step.Links.Self.Href
		}

		if step.Links.Actions != nil {
			task.Actions = append(task.Actions, v1.PipelineBlueOceanRef{
				Href: step.Links.Actions.Href,
			})
		}
	}

	for _, action := range step.Actions {
		task.Actions = append(task.Actions, v1.PipelineBlueOceanRef{
			URLName: action.URLName,
		})
	}

	if step.Input != nil {
		task.Input = &v1.PipelineBlueOceanInput{
			PipelineBlueOceanRef: v1.PipelineBlueOceanRef{
				ID: step.Input.ID,
			},
			Message:   step.Input.Message,
			OK:        step.Input.Ok,
			Submitter: step.Input.Submitter,
		}

		for _, param := range step.Input.Parameters {
			inputParams := v1.PipelineBlueOceanParameter{
				PipelineBlueOceanRef: v1.PipelineBlueOceanRef{
					Name:        param.Name,
					Description: param.Description,
					Type:        param.Type,
				},
				DefaultParameterValue: v1.PipelineBlueOceanRef{
					Name: param.DefaultParameterValue.Name,
				},
			}
			switch param.Type {
			case "StringParameterDefinition":
				if value, ok := param.DefaultParameterValue.Value.(string); ok {
					inputParams.DefaultParameterValue.Value = v1.ComposeValue{
						Value: value,
					}
				}
			case "BooleanParameterDefinition":
				if value, ok := param.DefaultParameterValue.Value.(bool); ok {
					inputParams.DefaultParameterValue.Value = v1.ComposeValue{
						Value: strconv.FormatBool(value),
					}
				}
			}
			task.Input.Parameters = append(task.Input.Parameters, inputParams)
		}
	}

	return task
}

func (c *Client) SubmitBranchInputStep(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, stepId string, options v1.PipelineInputOptions) error {
	crumbIssuer, err := c.getJenkinsCrumb(ctx)
	if err != nil {
		return err
	}

	inputParams := make([](*models.ParameterValue), 0)
	for _, param := range options.Parameters {
		inputParams = append(inputParams, &models.ParameterValue{
			Name:  param.Name,
			Value: param.Value,
		})
	}

	payload := &models.InputPayload{
		Abort:      !options.Approve,
		ID:         options.InputID,
		Parameters: inputParams,
	}

	scanLogParams := operations.
		NewSubmitBranchInputStepParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithOrganization(organization).
		WithStep(stepId).
		WithBranchName(branchName).
		WithPayload(payload).
		WithRun(runId).
		WithNode(nodeId).
		WithJenkinsCrumb(&crumbIssuer.Crumb)

	_, err = c.client.Operations.SubmitBranchInputStep(scanLogParams, c.authInfo)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) SubmitInputStep(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId, stepId string, options v1.PipelineInputOptions) error {
	crumbIssuer, err := c.getJenkinsCrumb(ctx)
	if err != nil {
		return err
	}

	inputParams := make([](*models.ParameterValue), 0)
	for _, param := range options.Parameters {
		inputParams = append(inputParams, &models.ParameterValue{
			Name:  param.Name,
			Value: param.Value,
		})
	}

	payload := &models.InputPayload{
		Abort:      !options.Approve,
		ID:         options.InputID,
		Parameters: inputParams,
	}

	scanLogParams := operations.
		NewSubmitInputStepParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithNamespace(pipelineNamespace).
		WithName(pipelineName).
		WithOrganization(organization).
		WithStep(stepId).
		WithPayload(payload).
		WithRun(runId).
		WithNode(nodeId).
		WithJenkinsCrumb(&crumbIssuer.Crumb)

	_, err = c.client.Operations.SubmitInputStep(scanLogParams, c.authInfo)
	if err != nil {
		return err
	}

	return nil
}
