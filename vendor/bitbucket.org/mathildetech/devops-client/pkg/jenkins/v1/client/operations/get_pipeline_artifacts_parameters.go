// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetPipelineArtifactsParams creates a new GetPipelineArtifactsParams object
// with the default values initialized.
func NewGetPipelineArtifactsParams() *GetPipelineArtifactsParams {
	var ()
	return &GetPipelineArtifactsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetPipelineArtifactsParamsWithTimeout creates a new GetPipelineArtifactsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetPipelineArtifactsParamsWithTimeout(timeout time.Duration) *GetPipelineArtifactsParams {
	var ()
	return &GetPipelineArtifactsParams{

		timeout: timeout,
	}
}

// NewGetPipelineArtifactsParamsWithContext creates a new GetPipelineArtifactsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetPipelineArtifactsParamsWithContext(ctx context.Context) *GetPipelineArtifactsParams {
	var ()
	return &GetPipelineArtifactsParams{

		Context: ctx,
	}
}

// NewGetPipelineArtifactsParamsWithHTTPClient creates a new GetPipelineArtifactsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetPipelineArtifactsParamsWithHTTPClient(client *http.Client) *GetPipelineArtifactsParams {
	var ()
	return &GetPipelineArtifactsParams{
		HTTPClient: client,
	}
}

/*GetPipelineArtifactsParams contains all the parameters to send to the API endpoint
for the get pipeline artifacts operation typically these are written to a http.Request
*/
type GetPipelineArtifactsParams struct {

	/*Name
	  name of pipeline or pipelineconfig

	*/
	Name string
	/*Namespace
	  namespace of pipeline or pipelineconfig

	*/
	Namespace string
	/*Organization
	  name of the organization

	*/
	Organization string
	/*Run
	  name of the run

	*/
	Run string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) WithTimeout(timeout time.Duration) *GetPipelineArtifactsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) WithContext(ctx context.Context) *GetPipelineArtifactsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) WithHTTPClient(client *http.Client) *GetPipelineArtifactsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithName adds the name to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) WithName(name string) *GetPipelineArtifactsParams {
	o.SetName(name)
	return o
}

// SetName adds the name to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) SetName(name string) {
	o.Name = name
}

// WithNamespace adds the namespace to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) WithNamespace(namespace string) *GetPipelineArtifactsParams {
	o.SetNamespace(namespace)
	return o
}

// SetNamespace adds the namespace to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) SetNamespace(namespace string) {
	o.Namespace = namespace
}

// WithOrganization adds the organization to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) WithOrganization(organization string) *GetPipelineArtifactsParams {
	o.SetOrganization(organization)
	return o
}

// SetOrganization adds the organization to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) SetOrganization(organization string) {
	o.Organization = organization
}

// WithRun adds the run to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) WithRun(run string) *GetPipelineArtifactsParams {
	o.SetRun(run)
	return o
}

// SetRun adds the run to the get pipeline artifacts params
func (o *GetPipelineArtifactsParams) SetRun(run string) {
	o.Run = run
}

// WriteToRequest writes these params to a swagger request
func (o *GetPipelineArtifactsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param name
	if err := r.SetPathParam("name", o.Name); err != nil {
		return err
	}

	// path param namespace
	if err := r.SetPathParam("namespace", o.Namespace); err != nil {
		return err
	}

	// path param organization
	if err := r.SetPathParam("organization", o.Organization); err != nil {
		return err
	}

	// path param run
	if err := r.SetPathParam("run", o.Run); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
