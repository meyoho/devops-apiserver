// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/jenkins/v1/models"
)

// GetPipelineBranchReader is a Reader for the GetPipelineBranch structure.
type GetPipelineBranchReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetPipelineBranchReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetPipelineBranchOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewGetPipelineBranchUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 403:
		result := NewGetPipelineBranchForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetPipelineBranchOK creates a GetPipelineBranchOK with default headers values
func NewGetPipelineBranchOK() *GetPipelineBranchOK {
	return &GetPipelineBranchOK{}
}

/*GetPipelineBranchOK handles this case with default header values.

Successfully retrieved branch details
*/
type GetPipelineBranchOK struct {
	Payload *models.BranchImpl
}

func (o *GetPipelineBranchOK) Error() string {
	return fmt.Sprintf("[GET /blue/rest/organizations/{organization}/pipelines/{namespace}/pipelines/{namespace}-{name}/branches/{branchName}/][%d] getPipelineBranchOK  %+v", 200, o.Payload)
}

func (o *GetPipelineBranchOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.BranchImpl)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetPipelineBranchUnauthorized creates a GetPipelineBranchUnauthorized with default headers values
func NewGetPipelineBranchUnauthorized() *GetPipelineBranchUnauthorized {
	return &GetPipelineBranchUnauthorized{}
}

/*GetPipelineBranchUnauthorized handles this case with default header values.

Authentication failed - incorrect username and/or password
*/
type GetPipelineBranchUnauthorized struct {
}

func (o *GetPipelineBranchUnauthorized) Error() string {
	return fmt.Sprintf("[GET /blue/rest/organizations/{organization}/pipelines/{namespace}/pipelines/{namespace}-{name}/branches/{branchName}/][%d] getPipelineBranchUnauthorized ", 401)
}

func (o *GetPipelineBranchUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewGetPipelineBranchForbidden creates a GetPipelineBranchForbidden with default headers values
func NewGetPipelineBranchForbidden() *GetPipelineBranchForbidden {
	return &GetPipelineBranchForbidden{}
}

/*GetPipelineBranchForbidden handles this case with default header values.

Jenkins requires authentication - please set username and password
*/
type GetPipelineBranchForbidden struct {
}

func (o *GetPipelineBranchForbidden) Error() string {
	return fmt.Sprintf("[GET /blue/rest/organizations/{organization}/pipelines/{namespace}/pipelines/{namespace}-{name}/branches/{branchName}/][%d] getPipelineBranchForbidden ", 403)
}

func (o *GetPipelineBranchForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
