// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4/models"
)

// GetV4ProjectsIDTriggersReader is a Reader for the GetV4ProjectsIDTriggers structure.
type GetV4ProjectsIDTriggersReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4ProjectsIDTriggersReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4ProjectsIDTriggersOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4ProjectsIDTriggersOK creates a GetV4ProjectsIDTriggersOK with default headers values
func NewGetV4ProjectsIDTriggersOK() *GetV4ProjectsIDTriggersOK {
	return &GetV4ProjectsIDTriggersOK{}
}

/*GetV4ProjectsIDTriggersOK handles this case with default header values.

Get triggers list
*/
type GetV4ProjectsIDTriggersOK struct {
	Payload *models.Trigger
}

func (o *GetV4ProjectsIDTriggersOK) Error() string {
	return fmt.Sprintf("[GET /v4/projects/{id}/triggers][%d] getV4ProjectsIdTriggersOK  %+v", 200, o.Payload)
}

func (o *GetV4ProjectsIDTriggersOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Trigger)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
