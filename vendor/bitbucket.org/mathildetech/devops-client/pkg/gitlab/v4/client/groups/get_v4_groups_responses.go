// Code generated by go-swagger; DO NOT EDIT.

package groups

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4/models"
)

// GetV4GroupsReader is a Reader for the GetV4Groups structure.
type GetV4GroupsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4GroupsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4GroupsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4GroupsOK creates a GetV4GroupsOK with default headers values
func NewGetV4GroupsOK() *GetV4GroupsOK {
	return &GetV4GroupsOK{}
}

/*GetV4GroupsOK handles this case with default header values.

Get a groups list
*/
type GetV4GroupsOK struct {
	Payload []*models.Group
}

func (o *GetV4GroupsOK) Error() string {
	return fmt.Sprintf("[GET /v4/groups][%d] getV4GroupsOK  %+v", 200, o.Payload)
}

func (o *GetV4GroupsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
