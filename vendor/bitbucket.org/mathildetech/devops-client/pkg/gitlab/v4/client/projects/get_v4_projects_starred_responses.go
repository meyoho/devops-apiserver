// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4/models"
)

// GetV4ProjectsStarredReader is a Reader for the GetV4ProjectsStarred structure.
type GetV4ProjectsStarredReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4ProjectsStarredReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4ProjectsStarredOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4ProjectsStarredOK creates a GetV4ProjectsStarredOK with default headers values
func NewGetV4ProjectsStarredOK() *GetV4ProjectsStarredOK {
	return &GetV4ProjectsStarredOK{}
}

/*GetV4ProjectsStarredOK handles this case with default header values.

Gets starred project for the authenticated user
*/
type GetV4ProjectsStarredOK struct {
	Payload *models.BasicProjectDetails
}

func (o *GetV4ProjectsStarredOK) Error() string {
	return fmt.Sprintf("[GET /v4/projects/starred][%d] getV4ProjectsStarredOK  %+v", 200, o.Payload)
}

func (o *GetV4ProjectsStarredOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.BasicProjectDetails)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
