// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4ProjectsParams creates a new GetV4ProjectsParams object
// with the default values initialized.
func NewGetV4ProjectsParams() *GetV4ProjectsParams {
	var (
		orderByDefault = string("created_at")
		sortDefault    = string("desc")
	)
	return &GetV4ProjectsParams{
		OrderBy: &orderByDefault,
		Sort:    &sortDefault,

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4ProjectsParamsWithTimeout creates a new GetV4ProjectsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4ProjectsParamsWithTimeout(timeout time.Duration) *GetV4ProjectsParams {
	var (
		orderByDefault = string("created_at")
		sortDefault    = string("desc")
	)
	return &GetV4ProjectsParams{
		OrderBy: &orderByDefault,
		Sort:    &sortDefault,

		timeout: timeout,
	}
}

// NewGetV4ProjectsParamsWithContext creates a new GetV4ProjectsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4ProjectsParamsWithContext(ctx context.Context) *GetV4ProjectsParams {
	var (
		orderByDefault = string("created_at")
		sortDefault    = string("desc")
	)
	return &GetV4ProjectsParams{
		OrderBy: &orderByDefault,
		Sort:    &sortDefault,

		Context: ctx,
	}
}

// NewGetV4ProjectsParamsWithHTTPClient creates a new GetV4ProjectsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4ProjectsParamsWithHTTPClient(client *http.Client) *GetV4ProjectsParams {
	var (
		orderByDefault = string("created_at")
		sortDefault    = string("desc")
	)
	return &GetV4ProjectsParams{
		OrderBy:    &orderByDefault,
		Sort:       &sortDefault,
		HTTPClient: client,
	}
}

/*GetV4ProjectsParams contains all the parameters to send to the API endpoint
for the get v4 projects operation typically these are written to a http.Request
*/
type GetV4ProjectsParams struct {

	/*Archived
	  Limit by archived status

	*/
	Archived *bool
	/*Membership
	  Limit by projects that the current user is a member of

	*/
	Membership *bool
	/*OrderBy
	  Return projects ordered by field

	*/
	OrderBy *string
	/*Page
	  Current page number

	*/
	Page *int32
	/*PerPage
	  Number of items per page

	*/
	PerPage *int32
	/*Search
	  Return list of authorized projects matching the search criteria

	*/
	Search *string
	/*Simple
	  Return only the ID, URL, name, and path of each project

	*/
	Simple *bool
	/*Sort
	  Return projects sorted in ascending and descending order

	*/
	Sort *string
	/*Statistics
	  Include project statistics

	*/
	Statistics *bool
	/*Visibility
	  Limit by visibility

	*/
	Visibility *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 projects params
func (o *GetV4ProjectsParams) WithTimeout(timeout time.Duration) *GetV4ProjectsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 projects params
func (o *GetV4ProjectsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 projects params
func (o *GetV4ProjectsParams) WithContext(ctx context.Context) *GetV4ProjectsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 projects params
func (o *GetV4ProjectsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 projects params
func (o *GetV4ProjectsParams) WithHTTPClient(client *http.Client) *GetV4ProjectsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 projects params
func (o *GetV4ProjectsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithArchived adds the archived to the get v4 projects params
func (o *GetV4ProjectsParams) WithArchived(archived *bool) *GetV4ProjectsParams {
	o.SetArchived(archived)
	return o
}

// SetArchived adds the archived to the get v4 projects params
func (o *GetV4ProjectsParams) SetArchived(archived *bool) {
	o.Archived = archived
}

// WithMembership adds the membership to the get v4 projects params
func (o *GetV4ProjectsParams) WithMembership(membership *bool) *GetV4ProjectsParams {
	o.SetMembership(membership)
	return o
}

// SetMembership adds the membership to the get v4 projects params
func (o *GetV4ProjectsParams) SetMembership(membership *bool) {
	o.Membership = membership
}

// WithOrderBy adds the orderBy to the get v4 projects params
func (o *GetV4ProjectsParams) WithOrderBy(orderBy *string) *GetV4ProjectsParams {
	o.SetOrderBy(orderBy)
	return o
}

// SetOrderBy adds the orderBy to the get v4 projects params
func (o *GetV4ProjectsParams) SetOrderBy(orderBy *string) {
	o.OrderBy = orderBy
}

// WithPage adds the page to the get v4 projects params
func (o *GetV4ProjectsParams) WithPage(page *int32) *GetV4ProjectsParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get v4 projects params
func (o *GetV4ProjectsParams) SetPage(page *int32) {
	o.Page = page
}

// WithPerPage adds the perPage to the get v4 projects params
func (o *GetV4ProjectsParams) WithPerPage(perPage *int32) *GetV4ProjectsParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the get v4 projects params
func (o *GetV4ProjectsParams) SetPerPage(perPage *int32) {
	o.PerPage = perPage
}

// WithSearch adds the search to the get v4 projects params
func (o *GetV4ProjectsParams) WithSearch(search *string) *GetV4ProjectsParams {
	o.SetSearch(search)
	return o
}

// SetSearch adds the search to the get v4 projects params
func (o *GetV4ProjectsParams) SetSearch(search *string) {
	o.Search = search
}

// WithSimple adds the simple to the get v4 projects params
func (o *GetV4ProjectsParams) WithSimple(simple *bool) *GetV4ProjectsParams {
	o.SetSimple(simple)
	return o
}

// SetSimple adds the simple to the get v4 projects params
func (o *GetV4ProjectsParams) SetSimple(simple *bool) {
	o.Simple = simple
}

// WithSort adds the sort to the get v4 projects params
func (o *GetV4ProjectsParams) WithSort(sort *string) *GetV4ProjectsParams {
	o.SetSort(sort)
	return o
}

// SetSort adds the sort to the get v4 projects params
func (o *GetV4ProjectsParams) SetSort(sort *string) {
	o.Sort = sort
}

// WithStatistics adds the statistics to the get v4 projects params
func (o *GetV4ProjectsParams) WithStatistics(statistics *bool) *GetV4ProjectsParams {
	o.SetStatistics(statistics)
	return o
}

// SetStatistics adds the statistics to the get v4 projects params
func (o *GetV4ProjectsParams) SetStatistics(statistics *bool) {
	o.Statistics = statistics
}

// WithVisibility adds the visibility to the get v4 projects params
func (o *GetV4ProjectsParams) WithVisibility(visibility *string) *GetV4ProjectsParams {
	o.SetVisibility(visibility)
	return o
}

// SetVisibility adds the visibility to the get v4 projects params
func (o *GetV4ProjectsParams) SetVisibility(visibility *string) {
	o.Visibility = visibility
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4ProjectsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Archived != nil {

		// query param archived
		var qrArchived bool
		if o.Archived != nil {
			qrArchived = *o.Archived
		}
		qArchived := swag.FormatBool(qrArchived)
		if qArchived != "" {
			if err := r.SetQueryParam("archived", qArchived); err != nil {
				return err
			}
		}

	}

	if o.Membership != nil {

		// query param membership
		var qrMembership bool
		if o.Membership != nil {
			qrMembership = *o.Membership
		}
		qMembership := swag.FormatBool(qrMembership)
		if qMembership != "" {
			if err := r.SetQueryParam("membership", qMembership); err != nil {
				return err
			}
		}

	}

	if o.OrderBy != nil {

		// query param order_by
		var qrOrderBy string
		if o.OrderBy != nil {
			qrOrderBy = *o.OrderBy
		}
		qOrderBy := qrOrderBy
		if qOrderBy != "" {
			if err := r.SetQueryParam("order_by", qOrderBy); err != nil {
				return err
			}
		}

	}

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int32
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt32(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	if o.Search != nil {

		// query param search
		var qrSearch string
		if o.Search != nil {
			qrSearch = *o.Search
		}
		qSearch := qrSearch
		if qSearch != "" {
			if err := r.SetQueryParam("search", qSearch); err != nil {
				return err
			}
		}

	}

	if o.Simple != nil {

		// query param simple
		var qrSimple bool
		if o.Simple != nil {
			qrSimple = *o.Simple
		}
		qSimple := swag.FormatBool(qrSimple)
		if qSimple != "" {
			if err := r.SetQueryParam("simple", qSimple); err != nil {
				return err
			}
		}

	}

	if o.Sort != nil {

		// query param sort
		var qrSort string
		if o.Sort != nil {
			qrSort = *o.Sort
		}
		qSort := qrSort
		if qSort != "" {
			if err := r.SetQueryParam("sort", qSort); err != nil {
				return err
			}
		}

	}

	if o.Statistics != nil {

		// query param statistics
		var qrStatistics bool
		if o.Statistics != nil {
			qrStatistics = *o.Statistics
		}
		qStatistics := swag.FormatBool(qrStatistics)
		if qStatistics != "" {
			if err := r.SetQueryParam("statistics", qStatistics); err != nil {
				return err
			}
		}

	}

	if o.Visibility != nil {

		// query param visibility
		var qrVisibility string
		if o.Visibility != nil {
			qrVisibility = *o.Visibility
		}
		qVisibility := qrVisibility
		if qVisibility != "" {
			if err := r.SetQueryParam("visibility", qVisibility); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
