// Code generated by go-swagger; DO NOT EDIT.

package groups

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// DeleteV4GroupsIDAccessRequestsUserIDReader is a Reader for the DeleteV4GroupsIDAccessRequestsUserID structure.
type DeleteV4GroupsIDAccessRequestsUserIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteV4GroupsIDAccessRequestsUserIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 204:
		result := NewDeleteV4GroupsIDAccessRequestsUserIDNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewDeleteV4GroupsIDAccessRequestsUserIDNoContent creates a DeleteV4GroupsIDAccessRequestsUserIDNoContent with default headers values
func NewDeleteV4GroupsIDAccessRequestsUserIDNoContent() *DeleteV4GroupsIDAccessRequestsUserIDNoContent {
	return &DeleteV4GroupsIDAccessRequestsUserIDNoContent{}
}

/*DeleteV4GroupsIDAccessRequestsUserIDNoContent handles this case with default header values.

Denies an access request for the given user.
*/
type DeleteV4GroupsIDAccessRequestsUserIDNoContent struct {
}

func (o *DeleteV4GroupsIDAccessRequestsUserIDNoContent) Error() string {
	return fmt.Sprintf("[DELETE /v4/groups/{id}/access_requests/{user_id}][%d] deleteV4GroupsIdAccessRequestsUserIdNoContent ", 204)
}

func (o *DeleteV4GroupsIDAccessRequestsUserIDNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
