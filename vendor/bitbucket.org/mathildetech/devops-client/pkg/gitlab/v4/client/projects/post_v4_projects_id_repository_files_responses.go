// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// PostV4ProjectsIDRepositoryFilesReader is a Reader for the PostV4ProjectsIDRepositoryFiles structure.
type PostV4ProjectsIDRepositoryFilesReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostV4ProjectsIDRepositoryFilesReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostV4ProjectsIDRepositoryFilesCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostV4ProjectsIDRepositoryFilesCreated creates a PostV4ProjectsIDRepositoryFilesCreated with default headers values
func NewPostV4ProjectsIDRepositoryFilesCreated() *PostV4ProjectsIDRepositoryFilesCreated {
	return &PostV4ProjectsIDRepositoryFilesCreated{}
}

/*PostV4ProjectsIDRepositoryFilesCreated handles this case with default header values.

Create new file in repository
*/
type PostV4ProjectsIDRepositoryFilesCreated struct {
}

func (o *PostV4ProjectsIDRepositoryFilesCreated) Error() string {
	return fmt.Sprintf("[POST /v4/projects/{id}/repository/files][%d] postV4ProjectsIdRepositoryFilesCreated ", 201)
}

func (o *PostV4ProjectsIDRepositoryFilesCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
