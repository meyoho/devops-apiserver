// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4ProjectsIDEventsParams creates a new GetV4ProjectsIDEventsParams object
// with the default values initialized.
func NewGetV4ProjectsIDEventsParams() *GetV4ProjectsIDEventsParams {
	var ()
	return &GetV4ProjectsIDEventsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4ProjectsIDEventsParamsWithTimeout creates a new GetV4ProjectsIDEventsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4ProjectsIDEventsParamsWithTimeout(timeout time.Duration) *GetV4ProjectsIDEventsParams {
	var ()
	return &GetV4ProjectsIDEventsParams{

		timeout: timeout,
	}
}

// NewGetV4ProjectsIDEventsParamsWithContext creates a new GetV4ProjectsIDEventsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4ProjectsIDEventsParamsWithContext(ctx context.Context) *GetV4ProjectsIDEventsParams {
	var ()
	return &GetV4ProjectsIDEventsParams{

		Context: ctx,
	}
}

// NewGetV4ProjectsIDEventsParamsWithHTTPClient creates a new GetV4ProjectsIDEventsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4ProjectsIDEventsParamsWithHTTPClient(client *http.Client) *GetV4ProjectsIDEventsParams {
	var ()
	return &GetV4ProjectsIDEventsParams{
		HTTPClient: client,
	}
}

/*GetV4ProjectsIDEventsParams contains all the parameters to send to the API endpoint
for the get v4 projects Id events operation typically these are written to a http.Request
*/
type GetV4ProjectsIDEventsParams struct {

	/*ID
	  The ID of a project

	*/
	ID string
	/*Page
	  Current page number

	*/
	Page *int32
	/*PerPage
	  Number of items per page

	*/
	PerPage *int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) WithTimeout(timeout time.Duration) *GetV4ProjectsIDEventsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) WithContext(ctx context.Context) *GetV4ProjectsIDEventsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) WithHTTPClient(client *http.Client) *GetV4ProjectsIDEventsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) WithID(id string) *GetV4ProjectsIDEventsParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) SetID(id string) {
	o.ID = id
}

// WithPage adds the page to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) WithPage(page *int32) *GetV4ProjectsIDEventsParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) SetPage(page *int32) {
	o.Page = page
}

// WithPerPage adds the perPage to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) WithPerPage(perPage *int32) *GetV4ProjectsIDEventsParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the get v4 projects Id events params
func (o *GetV4ProjectsIDEventsParams) SetPerPage(perPage *int32) {
	o.PerPage = perPage
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4ProjectsIDEventsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int32
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt32(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
