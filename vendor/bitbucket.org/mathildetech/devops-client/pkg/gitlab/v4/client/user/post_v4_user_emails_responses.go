// Code generated by go-swagger; DO NOT EDIT.

package user

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4/models"
)

// PostV4UserEmailsReader is a Reader for the PostV4UserEmails structure.
type PostV4UserEmailsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostV4UserEmailsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostV4UserEmailsCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostV4UserEmailsCreated creates a PostV4UserEmailsCreated with default headers values
func NewPostV4UserEmailsCreated() *PostV4UserEmailsCreated {
	return &PostV4UserEmailsCreated{}
}

/*PostV4UserEmailsCreated handles this case with default header values.

Add new email address to the currently authenticated user
*/
type PostV4UserEmailsCreated struct {
	Payload *models.Email
}

func (o *PostV4UserEmailsCreated) Error() string {
	return fmt.Sprintf("[POST /v4/user/emails][%d] postV4UserEmailsCreated  %+v", 201, o.Payload)
}

func (o *PostV4UserEmailsCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Email)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
