// Code generated by go-swagger; DO NOT EDIT.

package sidekiq

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// GetV4SidekiqProcessMetricsReader is a Reader for the GetV4SidekiqProcessMetrics structure.
type GetV4SidekiqProcessMetricsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4SidekiqProcessMetricsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4SidekiqProcessMetricsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4SidekiqProcessMetricsOK creates a GetV4SidekiqProcessMetricsOK with default headers values
func NewGetV4SidekiqProcessMetricsOK() *GetV4SidekiqProcessMetricsOK {
	return &GetV4SidekiqProcessMetricsOK{}
}

/*GetV4SidekiqProcessMetricsOK handles this case with default header values.

Get the Sidekiq process metrics
*/
type GetV4SidekiqProcessMetricsOK struct {
}

func (o *GetV4SidekiqProcessMetricsOK) Error() string {
	return fmt.Sprintf("[GET /v4/sidekiq/process_metrics][%d] getV4SidekiqProcessMetricsOK ", 200)
}

func (o *GetV4SidekiqProcessMetricsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
