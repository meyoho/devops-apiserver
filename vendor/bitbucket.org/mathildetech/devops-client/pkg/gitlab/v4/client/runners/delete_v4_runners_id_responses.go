// Code generated by go-swagger; DO NOT EDIT.

package runners

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4/models"
)

// DeleteV4RunnersIDReader is a Reader for the DeleteV4RunnersID structure.
type DeleteV4RunnersIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteV4RunnersIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewDeleteV4RunnersIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewDeleteV4RunnersIDOK creates a DeleteV4RunnersIDOK with default headers values
func NewDeleteV4RunnersIDOK() *DeleteV4RunnersIDOK {
	return &DeleteV4RunnersIDOK{}
}

/*DeleteV4RunnersIDOK handles this case with default header values.

Remove a runner
*/
type DeleteV4RunnersIDOK struct {
	Payload *models.Runner
}

func (o *DeleteV4RunnersIDOK) Error() string {
	return fmt.Sprintf("[DELETE /v4/runners/{id}][%d] deleteV4RunnersIdOK  %+v", 200, o.Payload)
}

func (o *DeleteV4RunnersIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Runner)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
