// Code generated by go-swagger; DO NOT EDIT.

package groups

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPutV4GroupsIDParams creates a new PutV4GroupsIDParams object
// with the default values initialized.
func NewPutV4GroupsIDParams() *PutV4GroupsIDParams {
	var ()
	return &PutV4GroupsIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPutV4GroupsIDParamsWithTimeout creates a new PutV4GroupsIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPutV4GroupsIDParamsWithTimeout(timeout time.Duration) *PutV4GroupsIDParams {
	var ()
	return &PutV4GroupsIDParams{

		timeout: timeout,
	}
}

// NewPutV4GroupsIDParamsWithContext creates a new PutV4GroupsIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewPutV4GroupsIDParamsWithContext(ctx context.Context) *PutV4GroupsIDParams {
	var ()
	return &PutV4GroupsIDParams{

		Context: ctx,
	}
}

// NewPutV4GroupsIDParamsWithHTTPClient creates a new PutV4GroupsIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPutV4GroupsIDParamsWithHTTPClient(client *http.Client) *PutV4GroupsIDParams {
	var ()
	return &PutV4GroupsIDParams{
		HTTPClient: client,
	}
}

/*PutV4GroupsIDParams contains all the parameters to send to the API endpoint
for the put v4 groups Id operation typically these are written to a http.Request
*/
type PutV4GroupsIDParams struct {

	/*Description
	  The description of the group

	*/
	Description *string
	/*ID
	  The ID of a group

	*/
	ID string
	/*LfsEnabled
	  Enable/disable LFS for the projects in this group

	*/
	LfsEnabled *bool
	/*Name
	  The name of the group

	*/
	Name *string
	/*Path
	  The path of the group

	*/
	Path *string
	/*RequestAccessEnabled
	  Allow users to request member access

	*/
	RequestAccessEnabled *bool
	/*VisibilityLevel
	  The visibility level of the group

	*/
	VisibilityLevel *int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the put v4 groups Id params
func (o *PutV4GroupsIDParams) WithTimeout(timeout time.Duration) *PutV4GroupsIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put v4 groups Id params
func (o *PutV4GroupsIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put v4 groups Id params
func (o *PutV4GroupsIDParams) WithContext(ctx context.Context) *PutV4GroupsIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put v4 groups Id params
func (o *PutV4GroupsIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put v4 groups Id params
func (o *PutV4GroupsIDParams) WithHTTPClient(client *http.Client) *PutV4GroupsIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put v4 groups Id params
func (o *PutV4GroupsIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithDescription adds the description to the put v4 groups Id params
func (o *PutV4GroupsIDParams) WithDescription(description *string) *PutV4GroupsIDParams {
	o.SetDescription(description)
	return o
}

// SetDescription adds the description to the put v4 groups Id params
func (o *PutV4GroupsIDParams) SetDescription(description *string) {
	o.Description = description
}

// WithID adds the id to the put v4 groups Id params
func (o *PutV4GroupsIDParams) WithID(id string) *PutV4GroupsIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the put v4 groups Id params
func (o *PutV4GroupsIDParams) SetID(id string) {
	o.ID = id
}

// WithLfsEnabled adds the lfsEnabled to the put v4 groups Id params
func (o *PutV4GroupsIDParams) WithLfsEnabled(lfsEnabled *bool) *PutV4GroupsIDParams {
	o.SetLfsEnabled(lfsEnabled)
	return o
}

// SetLfsEnabled adds the lfsEnabled to the put v4 groups Id params
func (o *PutV4GroupsIDParams) SetLfsEnabled(lfsEnabled *bool) {
	o.LfsEnabled = lfsEnabled
}

// WithName adds the name to the put v4 groups Id params
func (o *PutV4GroupsIDParams) WithName(name *string) *PutV4GroupsIDParams {
	o.SetName(name)
	return o
}

// SetName adds the name to the put v4 groups Id params
func (o *PutV4GroupsIDParams) SetName(name *string) {
	o.Name = name
}

// WithPath adds the path to the put v4 groups Id params
func (o *PutV4GroupsIDParams) WithPath(path *string) *PutV4GroupsIDParams {
	o.SetPath(path)
	return o
}

// SetPath adds the path to the put v4 groups Id params
func (o *PutV4GroupsIDParams) SetPath(path *string) {
	o.Path = path
}

// WithRequestAccessEnabled adds the requestAccessEnabled to the put v4 groups Id params
func (o *PutV4GroupsIDParams) WithRequestAccessEnabled(requestAccessEnabled *bool) *PutV4GroupsIDParams {
	o.SetRequestAccessEnabled(requestAccessEnabled)
	return o
}

// SetRequestAccessEnabled adds the requestAccessEnabled to the put v4 groups Id params
func (o *PutV4GroupsIDParams) SetRequestAccessEnabled(requestAccessEnabled *bool) {
	o.RequestAccessEnabled = requestAccessEnabled
}

// WithVisibilityLevel adds the visibilityLevel to the put v4 groups Id params
func (o *PutV4GroupsIDParams) WithVisibilityLevel(visibilityLevel *int32) *PutV4GroupsIDParams {
	o.SetVisibilityLevel(visibilityLevel)
	return o
}

// SetVisibilityLevel adds the visibilityLevel to the put v4 groups Id params
func (o *PutV4GroupsIDParams) SetVisibilityLevel(visibilityLevel *int32) {
	o.VisibilityLevel = visibilityLevel
}

// WriteToRequest writes these params to a swagger request
func (o *PutV4GroupsIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Description != nil {

		// form param description
		var frDescription string
		if o.Description != nil {
			frDescription = *o.Description
		}
		fDescription := frDescription
		if fDescription != "" {
			if err := r.SetFormParam("description", fDescription); err != nil {
				return err
			}
		}

	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if o.LfsEnabled != nil {

		// form param lfs_enabled
		var frLfsEnabled bool
		if o.LfsEnabled != nil {
			frLfsEnabled = *o.LfsEnabled
		}
		fLfsEnabled := swag.FormatBool(frLfsEnabled)
		if fLfsEnabled != "" {
			if err := r.SetFormParam("lfs_enabled", fLfsEnabled); err != nil {
				return err
			}
		}

	}

	if o.Name != nil {

		// form param name
		var frName string
		if o.Name != nil {
			frName = *o.Name
		}
		fName := frName
		if fName != "" {
			if err := r.SetFormParam("name", fName); err != nil {
				return err
			}
		}

	}

	if o.Path != nil {

		// form param path
		var frPath string
		if o.Path != nil {
			frPath = *o.Path
		}
		fPath := frPath
		if fPath != "" {
			if err := r.SetFormParam("path", fPath); err != nil {
				return err
			}
		}

	}

	if o.RequestAccessEnabled != nil {

		// form param request_access_enabled
		var frRequestAccessEnabled bool
		if o.RequestAccessEnabled != nil {
			frRequestAccessEnabled = *o.RequestAccessEnabled
		}
		fRequestAccessEnabled := swag.FormatBool(frRequestAccessEnabled)
		if fRequestAccessEnabled != "" {
			if err := r.SetFormParam("request_access_enabled", fRequestAccessEnabled); err != nil {
				return err
			}
		}

	}

	if o.VisibilityLevel != nil {

		// form param visibility_level
		var frVisibilityLevel int32
		if o.VisibilityLevel != nil {
			frVisibilityLevel = *o.VisibilityLevel
		}
		fVisibilityLevel := swag.FormatInt32(frVisibilityLevel)
		if fVisibilityLevel != "" {
			if err := r.SetFormParam("visibility_level", fVisibilityLevel); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
