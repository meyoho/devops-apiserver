// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4ProjectsIDDeploymentsDeploymentIDParams creates a new GetV4ProjectsIDDeploymentsDeploymentIDParams object
// with the default values initialized.
func NewGetV4ProjectsIDDeploymentsDeploymentIDParams() *GetV4ProjectsIDDeploymentsDeploymentIDParams {
	var ()
	return &GetV4ProjectsIDDeploymentsDeploymentIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4ProjectsIDDeploymentsDeploymentIDParamsWithTimeout creates a new GetV4ProjectsIDDeploymentsDeploymentIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4ProjectsIDDeploymentsDeploymentIDParamsWithTimeout(timeout time.Duration) *GetV4ProjectsIDDeploymentsDeploymentIDParams {
	var ()
	return &GetV4ProjectsIDDeploymentsDeploymentIDParams{

		timeout: timeout,
	}
}

// NewGetV4ProjectsIDDeploymentsDeploymentIDParamsWithContext creates a new GetV4ProjectsIDDeploymentsDeploymentIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4ProjectsIDDeploymentsDeploymentIDParamsWithContext(ctx context.Context) *GetV4ProjectsIDDeploymentsDeploymentIDParams {
	var ()
	return &GetV4ProjectsIDDeploymentsDeploymentIDParams{

		Context: ctx,
	}
}

// NewGetV4ProjectsIDDeploymentsDeploymentIDParamsWithHTTPClient creates a new GetV4ProjectsIDDeploymentsDeploymentIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4ProjectsIDDeploymentsDeploymentIDParamsWithHTTPClient(client *http.Client) *GetV4ProjectsIDDeploymentsDeploymentIDParams {
	var ()
	return &GetV4ProjectsIDDeploymentsDeploymentIDParams{
		HTTPClient: client,
	}
}

/*GetV4ProjectsIDDeploymentsDeploymentIDParams contains all the parameters to send to the API endpoint
for the get v4 projects Id deployments deployment Id operation typically these are written to a http.Request
*/
type GetV4ProjectsIDDeploymentsDeploymentIDParams struct {

	/*DeploymentID
	  The deployment ID

	*/
	DeploymentID int32
	/*ID
	  The project ID

	*/
	ID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 projects Id deployments deployment Id params
func (o *GetV4ProjectsIDDeploymentsDeploymentIDParams) WithTimeout(timeout time.Duration) *GetV4ProjectsIDDeploymentsDeploymentIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 projects Id deployments deployment Id params
func (o *GetV4ProjectsIDDeploymentsDeploymentIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 projects Id deployments deployment Id params
func (o *GetV4ProjectsIDDeploymentsDeploymentIDParams) WithContext(ctx context.Context) *GetV4ProjectsIDDeploymentsDeploymentIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 projects Id deployments deployment Id params
func (o *GetV4ProjectsIDDeploymentsDeploymentIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 projects Id deployments deployment Id params
func (o *GetV4ProjectsIDDeploymentsDeploymentIDParams) WithHTTPClient(client *http.Client) *GetV4ProjectsIDDeploymentsDeploymentIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 projects Id deployments deployment Id params
func (o *GetV4ProjectsIDDeploymentsDeploymentIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithDeploymentID adds the deploymentID to the get v4 projects Id deployments deployment Id params
func (o *GetV4ProjectsIDDeploymentsDeploymentIDParams) WithDeploymentID(deploymentID int32) *GetV4ProjectsIDDeploymentsDeploymentIDParams {
	o.SetDeploymentID(deploymentID)
	return o
}

// SetDeploymentID adds the deploymentId to the get v4 projects Id deployments deployment Id params
func (o *GetV4ProjectsIDDeploymentsDeploymentIDParams) SetDeploymentID(deploymentID int32) {
	o.DeploymentID = deploymentID
}

// WithID adds the id to the get v4 projects Id deployments deployment Id params
func (o *GetV4ProjectsIDDeploymentsDeploymentIDParams) WithID(id string) *GetV4ProjectsIDDeploymentsDeploymentIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get v4 projects Id deployments deployment Id params
func (o *GetV4ProjectsIDDeploymentsDeploymentIDParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4ProjectsIDDeploymentsDeploymentIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param deployment_id
	if err := r.SetPathParam("deployment_id", swag.FormatInt32(o.DeploymentID)); err != nil {
		return err
	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
