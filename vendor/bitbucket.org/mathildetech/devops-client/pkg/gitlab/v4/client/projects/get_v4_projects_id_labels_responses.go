// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4/models"
)

// GetV4ProjectsIDLabelsReader is a Reader for the GetV4ProjectsIDLabels structure.
type GetV4ProjectsIDLabelsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4ProjectsIDLabelsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4ProjectsIDLabelsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4ProjectsIDLabelsOK creates a GetV4ProjectsIDLabelsOK with default headers values
func NewGetV4ProjectsIDLabelsOK() *GetV4ProjectsIDLabelsOK {
	return &GetV4ProjectsIDLabelsOK{}
}

/*GetV4ProjectsIDLabelsOK handles this case with default header values.

Get all labels of the project
*/
type GetV4ProjectsIDLabelsOK struct {
	Payload *models.Label
}

func (o *GetV4ProjectsIDLabelsOK) Error() string {
	return fmt.Sprintf("[GET /v4/projects/{id}/labels][%d] getV4ProjectsIdLabelsOK  %+v", 200, o.Payload)
}

func (o *GetV4ProjectsIDLabelsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Label)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
