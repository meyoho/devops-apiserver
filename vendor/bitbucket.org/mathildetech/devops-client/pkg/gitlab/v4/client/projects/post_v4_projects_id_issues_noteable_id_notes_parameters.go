// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostV4ProjectsIDIssuesNoteableIDNotesParams creates a new PostV4ProjectsIDIssuesNoteableIDNotesParams object
// with the default values initialized.
func NewPostV4ProjectsIDIssuesNoteableIDNotesParams() *PostV4ProjectsIDIssuesNoteableIDNotesParams {
	var ()
	return &PostV4ProjectsIDIssuesNoteableIDNotesParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostV4ProjectsIDIssuesNoteableIDNotesParamsWithTimeout creates a new PostV4ProjectsIDIssuesNoteableIDNotesParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostV4ProjectsIDIssuesNoteableIDNotesParamsWithTimeout(timeout time.Duration) *PostV4ProjectsIDIssuesNoteableIDNotesParams {
	var ()
	return &PostV4ProjectsIDIssuesNoteableIDNotesParams{

		timeout: timeout,
	}
}

// NewPostV4ProjectsIDIssuesNoteableIDNotesParamsWithContext creates a new PostV4ProjectsIDIssuesNoteableIDNotesParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostV4ProjectsIDIssuesNoteableIDNotesParamsWithContext(ctx context.Context) *PostV4ProjectsIDIssuesNoteableIDNotesParams {
	var ()
	return &PostV4ProjectsIDIssuesNoteableIDNotesParams{

		Context: ctx,
	}
}

// NewPostV4ProjectsIDIssuesNoteableIDNotesParamsWithHTTPClient creates a new PostV4ProjectsIDIssuesNoteableIDNotesParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostV4ProjectsIDIssuesNoteableIDNotesParamsWithHTTPClient(client *http.Client) *PostV4ProjectsIDIssuesNoteableIDNotesParams {
	var ()
	return &PostV4ProjectsIDIssuesNoteableIDNotesParams{
		HTTPClient: client,
	}
}

/*PostV4ProjectsIDIssuesNoteableIDNotesParams contains all the parameters to send to the API endpoint
for the post v4 projects Id issues noteable Id notes operation typically these are written to a http.Request
*/
type PostV4ProjectsIDIssuesNoteableIDNotesParams struct {

	/*Body
	  The content of a note

	*/
	Body string
	/*CreatedAt
	  The creation date of the note

	*/
	CreatedAt *string
	/*ID
	  The ID of a project

	*/
	ID string
	/*NoteableID
	  The ID of the noteable

	*/
	NoteableID int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) WithTimeout(timeout time.Duration) *PostV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) WithContext(ctx context.Context) *PostV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) WithHTTPClient(client *http.Client) *PostV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) WithBody(body string) *PostV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) SetBody(body string) {
	o.Body = body
}

// WithCreatedAt adds the createdAt to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) WithCreatedAt(createdAt *string) *PostV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetCreatedAt(createdAt)
	return o
}

// SetCreatedAt adds the createdAt to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) SetCreatedAt(createdAt *string) {
	o.CreatedAt = createdAt
}

// WithID adds the id to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) WithID(id string) *PostV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) SetID(id string) {
	o.ID = id
}

// WithNoteableID adds the noteableID to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) WithNoteableID(noteableID int32) *PostV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetNoteableID(noteableID)
	return o
}

// SetNoteableID adds the noteableId to the post v4 projects Id issues noteable Id notes params
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) SetNoteableID(noteableID int32) {
	o.NoteableID = noteableID
}

// WriteToRequest writes these params to a swagger request
func (o *PostV4ProjectsIDIssuesNoteableIDNotesParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// form param body
	frBody := o.Body
	fBody := frBody
	if fBody != "" {
		if err := r.SetFormParam("body", fBody); err != nil {
			return err
		}
	}

	if o.CreatedAt != nil {

		// form param created_at
		var frCreatedAt string
		if o.CreatedAt != nil {
			frCreatedAt = *o.CreatedAt
		}
		fCreatedAt := frCreatedAt
		if fCreatedAt != "" {
			if err := r.SetFormParam("created_at", fCreatedAt); err != nil {
				return err
			}
		}

	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	// path param noteable_id
	if err := r.SetPathParam("noteable_id", swag.FormatInt32(o.NoteableID)); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
