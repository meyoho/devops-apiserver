// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostV4ProjectsIDPipelinesPipelineIDRetryParams creates a new PostV4ProjectsIDPipelinesPipelineIDRetryParams object
// with the default values initialized.
func NewPostV4ProjectsIDPipelinesPipelineIDRetryParams() *PostV4ProjectsIDPipelinesPipelineIDRetryParams {
	var ()
	return &PostV4ProjectsIDPipelinesPipelineIDRetryParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostV4ProjectsIDPipelinesPipelineIDRetryParamsWithTimeout creates a new PostV4ProjectsIDPipelinesPipelineIDRetryParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostV4ProjectsIDPipelinesPipelineIDRetryParamsWithTimeout(timeout time.Duration) *PostV4ProjectsIDPipelinesPipelineIDRetryParams {
	var ()
	return &PostV4ProjectsIDPipelinesPipelineIDRetryParams{

		timeout: timeout,
	}
}

// NewPostV4ProjectsIDPipelinesPipelineIDRetryParamsWithContext creates a new PostV4ProjectsIDPipelinesPipelineIDRetryParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostV4ProjectsIDPipelinesPipelineIDRetryParamsWithContext(ctx context.Context) *PostV4ProjectsIDPipelinesPipelineIDRetryParams {
	var ()
	return &PostV4ProjectsIDPipelinesPipelineIDRetryParams{

		Context: ctx,
	}
}

// NewPostV4ProjectsIDPipelinesPipelineIDRetryParamsWithHTTPClient creates a new PostV4ProjectsIDPipelinesPipelineIDRetryParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostV4ProjectsIDPipelinesPipelineIDRetryParamsWithHTTPClient(client *http.Client) *PostV4ProjectsIDPipelinesPipelineIDRetryParams {
	var ()
	return &PostV4ProjectsIDPipelinesPipelineIDRetryParams{
		HTTPClient: client,
	}
}

/*PostV4ProjectsIDPipelinesPipelineIDRetryParams contains all the parameters to send to the API endpoint
for the post v4 projects Id pipelines pipeline Id retry operation typically these are written to a http.Request
*/
type PostV4ProjectsIDPipelinesPipelineIDRetryParams struct {

	/*ID
	  The project ID

	*/
	ID string
	/*PipelineID
	  The pipeline ID

	*/
	PipelineID int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post v4 projects Id pipelines pipeline Id retry params
func (o *PostV4ProjectsIDPipelinesPipelineIDRetryParams) WithTimeout(timeout time.Duration) *PostV4ProjectsIDPipelinesPipelineIDRetryParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post v4 projects Id pipelines pipeline Id retry params
func (o *PostV4ProjectsIDPipelinesPipelineIDRetryParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post v4 projects Id pipelines pipeline Id retry params
func (o *PostV4ProjectsIDPipelinesPipelineIDRetryParams) WithContext(ctx context.Context) *PostV4ProjectsIDPipelinesPipelineIDRetryParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post v4 projects Id pipelines pipeline Id retry params
func (o *PostV4ProjectsIDPipelinesPipelineIDRetryParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post v4 projects Id pipelines pipeline Id retry params
func (o *PostV4ProjectsIDPipelinesPipelineIDRetryParams) WithHTTPClient(client *http.Client) *PostV4ProjectsIDPipelinesPipelineIDRetryParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post v4 projects Id pipelines pipeline Id retry params
func (o *PostV4ProjectsIDPipelinesPipelineIDRetryParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the post v4 projects Id pipelines pipeline Id retry params
func (o *PostV4ProjectsIDPipelinesPipelineIDRetryParams) WithID(id string) *PostV4ProjectsIDPipelinesPipelineIDRetryParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the post v4 projects Id pipelines pipeline Id retry params
func (o *PostV4ProjectsIDPipelinesPipelineIDRetryParams) SetID(id string) {
	o.ID = id
}

// WithPipelineID adds the pipelineID to the post v4 projects Id pipelines pipeline Id retry params
func (o *PostV4ProjectsIDPipelinesPipelineIDRetryParams) WithPipelineID(pipelineID int32) *PostV4ProjectsIDPipelinesPipelineIDRetryParams {
	o.SetPipelineID(pipelineID)
	return o
}

// SetPipelineID adds the pipelineId to the post v4 projects Id pipelines pipeline Id retry params
func (o *PostV4ProjectsIDPipelinesPipelineIDRetryParams) SetPipelineID(pipelineID int32) {
	o.PipelineID = pipelineID
}

// WriteToRequest writes these params to a swagger request
func (o *PostV4ProjectsIDPipelinesPipelineIDRetryParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	// path param pipeline_id
	if err := r.SetPathParam("pipeline_id", swag.FormatInt32(o.PipelineID)); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
