// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4/models"
)

// GetV4ProjectsIDEnvironmentsReader is a Reader for the GetV4ProjectsIDEnvironments structure.
type GetV4ProjectsIDEnvironmentsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV4ProjectsIDEnvironmentsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV4ProjectsIDEnvironmentsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV4ProjectsIDEnvironmentsOK creates a GetV4ProjectsIDEnvironmentsOK with default headers values
func NewGetV4ProjectsIDEnvironmentsOK() *GetV4ProjectsIDEnvironmentsOK {
	return &GetV4ProjectsIDEnvironmentsOK{}
}

/*GetV4ProjectsIDEnvironmentsOK handles this case with default header values.

Get all environments of the project
*/
type GetV4ProjectsIDEnvironmentsOK struct {
	Payload *models.Environment
}

func (o *GetV4ProjectsIDEnvironmentsOK) Error() string {
	return fmt.Sprintf("[GET /v4/projects/{id}/environments][%d] getV4ProjectsIdEnvironmentsOK  %+v", 200, o.Payload)
}

func (o *GetV4ProjectsIDEnvironmentsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Environment)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
