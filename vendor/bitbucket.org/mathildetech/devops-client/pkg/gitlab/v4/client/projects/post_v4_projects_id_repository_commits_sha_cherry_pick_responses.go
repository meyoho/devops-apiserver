// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4/models"
)

// PostV4ProjectsIDRepositoryCommitsShaCherryPickReader is a Reader for the PostV4ProjectsIDRepositoryCommitsShaCherryPick structure.
type PostV4ProjectsIDRepositoryCommitsShaCherryPickReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostV4ProjectsIDRepositoryCommitsShaCherryPickReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostV4ProjectsIDRepositoryCommitsShaCherryPickCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostV4ProjectsIDRepositoryCommitsShaCherryPickCreated creates a PostV4ProjectsIDRepositoryCommitsShaCherryPickCreated with default headers values
func NewPostV4ProjectsIDRepositoryCommitsShaCherryPickCreated() *PostV4ProjectsIDRepositoryCommitsShaCherryPickCreated {
	return &PostV4ProjectsIDRepositoryCommitsShaCherryPickCreated{}
}

/*PostV4ProjectsIDRepositoryCommitsShaCherryPickCreated handles this case with default header values.

Cherry pick commit into a branch
*/
type PostV4ProjectsIDRepositoryCommitsShaCherryPickCreated struct {
	Payload *models.RepoCommit
}

func (o *PostV4ProjectsIDRepositoryCommitsShaCherryPickCreated) Error() string {
	return fmt.Sprintf("[POST /v4/projects/{id}/repository/commits/{sha}/cherry_pick][%d] postV4ProjectsIdRepositoryCommitsShaCherryPickCreated  %+v", 201, o.Payload)
}

func (o *PostV4ProjectsIDRepositoryCommitsShaCherryPickCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.RepoCommit)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
