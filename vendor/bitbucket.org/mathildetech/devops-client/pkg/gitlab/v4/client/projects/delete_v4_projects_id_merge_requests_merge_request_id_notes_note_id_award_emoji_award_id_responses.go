// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4/models"
)

// DeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDReader is a Reader for the DeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardID structure.
type DeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewDeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewDeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDOK creates a DeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDOK with default headers values
func NewDeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDOK() *DeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDOK {
	return &DeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDOK{}
}

/*DeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDOK handles this case with default header values.

Delete a +awardables+ award emoji
*/
type DeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDOK struct {
	Payload *models.AwardEmoji
}

func (o *DeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDOK) Error() string {
	return fmt.Sprintf("[DELETE /v4/projects/{id}/merge_requests/{merge_request_id}/notes/{note_id}/award_emoji/{award_id}][%d] deleteV4ProjectsIdMergeRequestsMergeRequestIdNotesNoteIdAwardEmojiAwardIdOK  %+v", 200, o.Payload)
}

func (o *DeleteV4ProjectsIDMergeRequestsMergeRequestIDNotesNoteIDAwardEmojiAwardIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.AwardEmoji)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
