// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4/models"
)

// PostV4ProjectsIDBuildsBuildIDEraseReader is a Reader for the PostV4ProjectsIDBuildsBuildIDErase structure.
type PostV4ProjectsIDBuildsBuildIDEraseReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostV4ProjectsIDBuildsBuildIDEraseReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostV4ProjectsIDBuildsBuildIDEraseCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostV4ProjectsIDBuildsBuildIDEraseCreated creates a PostV4ProjectsIDBuildsBuildIDEraseCreated with default headers values
func NewPostV4ProjectsIDBuildsBuildIDEraseCreated() *PostV4ProjectsIDBuildsBuildIDEraseCreated {
	return &PostV4ProjectsIDBuildsBuildIDEraseCreated{}
}

/*PostV4ProjectsIDBuildsBuildIDEraseCreated handles this case with default header values.

Erase build (remove artifacts and build trace)
*/
type PostV4ProjectsIDBuildsBuildIDEraseCreated struct {
	Payload *models.Build
}

func (o *PostV4ProjectsIDBuildsBuildIDEraseCreated) Error() string {
	return fmt.Sprintf("[POST /v4/projects/{id}/builds/{build_id}/erase][%d] postV4ProjectsIdBuildsBuildIdEraseCreated  %+v", 201, o.Payload)
}

func (o *PostV4ProjectsIDBuildsBuildIDEraseCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Build)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
