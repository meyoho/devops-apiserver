// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams creates a new GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams object
// with the default values initialized.
func NewGetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams() *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams {
	var ()
	return &GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParamsWithTimeout creates a new GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParamsWithTimeout(timeout time.Duration) *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams {
	var ()
	return &GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams{

		timeout: timeout,
	}
}

// NewGetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParamsWithContext creates a new GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParamsWithContext(ctx context.Context) *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams {
	var ()
	return &GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams{

		Context: ctx,
	}
}

// NewGetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParamsWithHTTPClient creates a new GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParamsWithHTTPClient(client *http.Client) *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams {
	var ()
	return &GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams{
		HTTPClient: client,
	}
}

/*GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams contains all the parameters to send to the API endpoint
for the get v4 projects Id snippets noteable Id notes note Id operation typically these are written to a http.Request
*/
type GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams struct {

	/*ID
	  The ID of a project

	*/
	ID string
	/*NoteID
	  The ID of a note

	*/
	NoteID int32
	/*NoteableID
	  The ID of the noteable

	*/
	NoteableID int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) WithTimeout(timeout time.Duration) *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) WithContext(ctx context.Context) *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) WithHTTPClient(client *http.Client) *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) WithID(id string) *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) SetID(id string) {
	o.ID = id
}

// WithNoteID adds the noteID to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) WithNoteID(noteID int32) *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams {
	o.SetNoteID(noteID)
	return o
}

// SetNoteID adds the noteId to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) SetNoteID(noteID int32) {
	o.NoteID = noteID
}

// WithNoteableID adds the noteableID to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) WithNoteableID(noteableID int32) *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams {
	o.SetNoteableID(noteableID)
	return o
}

// SetNoteableID adds the noteableId to the get v4 projects Id snippets noteable Id notes note Id params
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) SetNoteableID(noteableID int32) {
	o.NoteableID = noteableID
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4ProjectsIDSnippetsNoteableIDNotesNoteIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	// path param note_id
	if err := r.SetPathParam("note_id", swag.FormatInt32(o.NoteID)); err != nil {
		return err
	}

	// path param noteable_id
	if err := r.SetPathParam("noteable_id", swag.FormatInt32(o.NoteableID)); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
