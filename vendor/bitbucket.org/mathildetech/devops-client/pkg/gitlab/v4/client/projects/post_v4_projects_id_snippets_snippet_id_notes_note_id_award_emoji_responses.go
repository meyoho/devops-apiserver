// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4/models"
)

// PostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiReader is a Reader for the PostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmoji structure.
type PostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiCreated creates a PostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiCreated with default headers values
func NewPostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiCreated() *PostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiCreated {
	return &PostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiCreated{}
}

/*PostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiCreated handles this case with default header values.

Award a new Emoji
*/
type PostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiCreated struct {
	Payload *models.AwardEmoji
}

func (o *PostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiCreated) Error() string {
	return fmt.Sprintf("[POST /v4/projects/{id}/snippets/{snippet_id}/notes/{note_id}/award_emoji][%d] postV4ProjectsIdSnippetsSnippetIdNotesNoteIdAwardEmojiCreated  %+v", 201, o.Payload)
}

func (o *PostV4ProjectsIDSnippetsSnippetIDNotesNoteIDAwardEmojiCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.AwardEmoji)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
