// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4ProjectsIDRepositoryCommitsShaBuildsParams creates a new GetV4ProjectsIDRepositoryCommitsShaBuildsParams object
// with the default values initialized.
func NewGetV4ProjectsIDRepositoryCommitsShaBuildsParams() *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	var ()
	return &GetV4ProjectsIDRepositoryCommitsShaBuildsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4ProjectsIDRepositoryCommitsShaBuildsParamsWithTimeout creates a new GetV4ProjectsIDRepositoryCommitsShaBuildsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4ProjectsIDRepositoryCommitsShaBuildsParamsWithTimeout(timeout time.Duration) *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	var ()
	return &GetV4ProjectsIDRepositoryCommitsShaBuildsParams{

		timeout: timeout,
	}
}

// NewGetV4ProjectsIDRepositoryCommitsShaBuildsParamsWithContext creates a new GetV4ProjectsIDRepositoryCommitsShaBuildsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4ProjectsIDRepositoryCommitsShaBuildsParamsWithContext(ctx context.Context) *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	var ()
	return &GetV4ProjectsIDRepositoryCommitsShaBuildsParams{

		Context: ctx,
	}
}

// NewGetV4ProjectsIDRepositoryCommitsShaBuildsParamsWithHTTPClient creates a new GetV4ProjectsIDRepositoryCommitsShaBuildsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4ProjectsIDRepositoryCommitsShaBuildsParamsWithHTTPClient(client *http.Client) *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	var ()
	return &GetV4ProjectsIDRepositoryCommitsShaBuildsParams{
		HTTPClient: client,
	}
}

/*GetV4ProjectsIDRepositoryCommitsShaBuildsParams contains all the parameters to send to the API endpoint
for the get v4 projects Id repository commits sha builds operation typically these are written to a http.Request
*/
type GetV4ProjectsIDRepositoryCommitsShaBuildsParams struct {

	/*ID
	  The ID of a project

	*/
	ID string
	/*Page
	  Current page number

	*/
	Page *int32
	/*PerPage
	  Number of items per page

	*/
	PerPage *int32
	/*Scope
	  The scope of builds to show

	*/
	Scope *string
	/*Sha
	  The SHA id of a commit

	*/
	Sha string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) WithTimeout(timeout time.Duration) *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) WithContext(ctx context.Context) *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) WithHTTPClient(client *http.Client) *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) WithID(id string) *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) SetID(id string) {
	o.ID = id
}

// WithPage adds the page to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) WithPage(page *int32) *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) SetPage(page *int32) {
	o.Page = page
}

// WithPerPage adds the perPage to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) WithPerPage(perPage *int32) *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) SetPerPage(perPage *int32) {
	o.PerPage = perPage
}

// WithScope adds the scope to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) WithScope(scope *string) *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	o.SetScope(scope)
	return o
}

// SetScope adds the scope to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) SetScope(scope *string) {
	o.Scope = scope
}

// WithSha adds the sha to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) WithSha(sha string) *GetV4ProjectsIDRepositoryCommitsShaBuildsParams {
	o.SetSha(sha)
	return o
}

// SetSha adds the sha to the get v4 projects Id repository commits sha builds params
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) SetSha(sha string) {
	o.Sha = sha
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4ProjectsIDRepositoryCommitsShaBuildsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int32
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt32(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	if o.Scope != nil {

		// query param scope
		var qrScope string
		if o.Scope != nil {
			qrScope = *o.Scope
		}
		qScope := qrScope
		if qScope != "" {
			if err := r.SetQueryParam("scope", qScope); err != nil {
				return err
			}
		}

	}

	// path param sha
	if err := r.SetPathParam("sha", o.Sha); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
