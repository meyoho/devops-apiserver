// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// Compare Compare two branches, tags, or commits
// swagger:model Compare
type Compare struct {

	// commit
	Commit *RepoCommit `json:"commit,omitempty"`

	// commits
	Commits *RepoCommit `json:"commits,omitempty"`

	// compare same ref
	CompareSameRef string `json:"compare_same_ref,omitempty"`

	// compare timeout
	CompareTimeout string `json:"compare_timeout,omitempty"`

	// diffs
	Diffs *RepoDiff `json:"diffs,omitempty"`
}

// Validate validates this compare
func (m *Compare) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateCommit(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateCommits(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateDiffs(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *Compare) validateCommit(formats strfmt.Registry) error {

	if swag.IsZero(m.Commit) { // not required
		return nil
	}

	if m.Commit != nil {
		if err := m.Commit.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("commit")
			}
			return err
		}
	}

	return nil
}

func (m *Compare) validateCommits(formats strfmt.Registry) error {

	if swag.IsZero(m.Commits) { // not required
		return nil
	}

	if m.Commits != nil {
		if err := m.Commits.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("commits")
			}
			return err
		}
	}

	return nil
}

func (m *Compare) validateDiffs(formats strfmt.Registry) error {

	if swag.IsZero(m.Diffs) { // not required
		return nil
	}

	if m.Diffs != nil {
		if err := m.Diffs.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("diffs")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *Compare) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Compare) UnmarshalBinary(b []byte) error {
	var res Compare
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
