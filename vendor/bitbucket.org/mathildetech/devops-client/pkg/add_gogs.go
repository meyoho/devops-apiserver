package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v0_11_86 "bitbucket.org/mathildetech/devops-client/pkg/gogs/v0_11_86"
)

func init() {
	register(devopsv1.TypeGogs, versionOpt{
		version:   "v0.11.86",
		factory:   v0_11_86.NewClient(),
		isDefault: true,
	})
}
