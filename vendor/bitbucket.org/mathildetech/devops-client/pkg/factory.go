package pkg

import (
	"fmt"
	"sync"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
)

// Versioner gets a verioned client
type Versioner interface {
	Inject(versionOpt)
	Version(v string) v1.ClientFactory
}

type versionedFactory struct {
	sync.RWMutex
	factory map[string]v1.ClientFactory
}

func (f *versionedFactory) Inject(opt versionOpt) {
	f.Lock()
	f.factory[opt.version] = opt.factory
	if opt.isDefault {
		f.factory[""] = opt.factory
	}
	f.Unlock()
}

func (f *versionedFactory) Version(v string) v1.ClientFactory {
	f.RLock()
	defer f.RUnlock()

	return f.factory[v]
}

var (
	once            sync.Once
	lock            sync.Mutex
	internalFactory map[string]Versioner
)

type versionOpt struct {
	version   string
	factory   v1.ClientFactory
	isDefault bool
}

// register tool
func register(toolName string, opts ...versionOpt) {
	once.Do(func() {
		internalFactory = make(map[string]Versioner)
	})
	lock.Lock()
	if _, ok := internalFactory[toolName]; !ok {
		internalFactory[toolName] = &versionedFactory{
			factory: make(map[string]v1.ClientFactory),
		}
	}

	for _, opt := range opts {
		internalFactory[toolName].Inject(opt)
	}
	lock.Unlock()
}

// NewClient returns a devops tool client
func NewClient(tool, version string, options *v1.Options) (v1.Interface, error) {
	factory, ok := internalFactory[tool]
	if !ok {
		return nil, fmt.Errorf("tool %s not exist", tool)
	}

	var clientFactory v1.ClientFactory

	if factory != nil {
		clientFactory = factory.Version(version)
		if clientFactory == nil {
			return nil, fmt.Errorf("tool %s has no version %s", tool, version)
		}
	}

	return clientFactory(options), nil
}
