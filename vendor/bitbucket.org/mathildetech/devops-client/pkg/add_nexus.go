package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v3 "bitbucket.org/mathildetech/devops-client/pkg/nexus/v3"
	v3_ext "bitbucket.org/mathildetech/devops-client/pkg/nexus/v3_ext"
)

func init() {
	register(devopsv1.TypeNexus, versionOpt{
		version:   "v3",
		factory:   v3.NewClient(),
		isDefault: true,
	})

	register(devopsv1.TypeNexus, versionOpt{
		version:   "v3_ext",
		factory:   v3_ext.NewClient(),
		isDefault: true,
	})
}
