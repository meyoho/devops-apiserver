package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v4 "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4"
)

func init() {
	register(devopsv1.TypeGitlab, versionOpt{
		version:   "v4",
		factory:   v4.NewClient(),
		isDefault: true,
	})
}
