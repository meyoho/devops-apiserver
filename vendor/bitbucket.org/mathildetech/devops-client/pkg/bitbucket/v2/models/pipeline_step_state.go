// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// PipelineStepState pipeline step state
// swagger:model pipeline_step_state
type PipelineStepState struct {
	Object

	PipelineStepStateAllOf1
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (m *PipelineStepState) UnmarshalJSON(raw []byte) error {
	// AO0
	var aO0 Object
	if err := swag.ReadJSON(raw, &aO0); err != nil {
		return err
	}
	m.Object = aO0

	// AO1
	var aO1 PipelineStepStateAllOf1
	if err := swag.ReadJSON(raw, &aO1); err != nil {
		return err
	}
	m.PipelineStepStateAllOf1 = aO1

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (m PipelineStepState) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 2)

	aO0, err := swag.WriteJSON(m.Object)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, aO0)

	aO1, err := swag.WriteJSON(m.PipelineStepStateAllOf1)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, aO1)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this pipeline step state
func (m *PipelineStepState) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *PipelineStepState) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *PipelineStepState) UnmarshalBinary(b []byte) error {
	var res PipelineStepState
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// PipelineStepStateAllOf1 The representation of the progress state of a pipeline step.
// swagger:model PipelineStepStateAllOf1
type PipelineStepStateAllOf1 interface{}
