// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"strconv"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// PipelineStep pipeline step
// swagger:model pipeline_step
type PipelineStep struct {
	Object

	// The timestamp when the step execution was completed. This is not set if the step is still in progress.
	// Format: date-time
	CompletedOn strfmt.DateTime `json:"completed_on,omitempty"`

	// The Docker image used as the build container for the step.
	Image *PipelineImage `json:"image,omitempty"`

	// The list of build commands. These commands are executed in the build container.
	ScriptCommands []*PipelineCommand `json:"script_commands"`

	// The list of commands that are executed as part of the setup phase of the build. These commands are executed outside the build container.
	SetupCommands []*PipelineCommand `json:"setup_commands"`

	// The timestamp when the step execution was started. This is not set when the step hasn't executed yet.
	// Format: date-time
	StartedOn strfmt.DateTime `json:"started_on,omitempty"`

	// The current state of the step
	State *PipelineStepState `json:"state,omitempty"`

	// The UUID identifying the step.
	UUID string `json:"uuid,omitempty"`

	// a o1 additional properties
	AO1AdditionalProperties map[string]interface{} `json:"-"`
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (m *PipelineStep) UnmarshalJSON(raw []byte) error {
	// AO0
	var aO0 Object
	if err := swag.ReadJSON(raw, &aO0); err != nil {
		return err
	}
	m.Object = aO0

	// AO1
	var dataAO1 struct {
		CompletedOn strfmt.DateTime `json:"completed_on,omitempty"`

		Image *PipelineImage `json:"image,omitempty"`

		ScriptCommands []*PipelineCommand `json:"script_commands"`

		SetupCommands []*PipelineCommand `json:"setup_commands"`

		StartedOn strfmt.DateTime `json:"started_on,omitempty"`

		State *PipelineStepState `json:"state,omitempty"`

		UUID string `json:"uuid,omitempty"`

		AO1AdditionalProperties map[string]interface{} `json:"-"`
	}
	if err := swag.ReadJSON(raw, &dataAO1); err != nil {
		return err
	}

	m.CompletedOn = dataAO1.CompletedOn

	m.Image = dataAO1.Image

	m.ScriptCommands = dataAO1.ScriptCommands

	m.SetupCommands = dataAO1.SetupCommands

	m.StartedOn = dataAO1.StartedOn

	m.State = dataAO1.State

	m.UUID = dataAO1.UUID

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (m PipelineStep) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 2)

	aO0, err := swag.WriteJSON(m.Object)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, aO0)

	var dataAO1 struct {
		CompletedOn strfmt.DateTime `json:"completed_on,omitempty"`

		Image *PipelineImage `json:"image,omitempty"`

		ScriptCommands []*PipelineCommand `json:"script_commands"`

		SetupCommands []*PipelineCommand `json:"setup_commands"`

		StartedOn strfmt.DateTime `json:"started_on,omitempty"`

		State *PipelineStepState `json:"state,omitempty"`

		UUID string `json:"uuid,omitempty"`

		AO1AdditionalProperties map[string]interface{} `json:"-"`
	}

	dataAO1.CompletedOn = m.CompletedOn

	dataAO1.Image = m.Image

	dataAO1.ScriptCommands = m.ScriptCommands

	dataAO1.SetupCommands = m.SetupCommands

	dataAO1.StartedOn = m.StartedOn

	dataAO1.State = m.State

	dataAO1.UUID = m.UUID

	jsonDataAO1, errAO1 := swag.WriteJSON(dataAO1)
	if errAO1 != nil {
		return nil, errAO1
	}
	_parts = append(_parts, jsonDataAO1)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this pipeline step
func (m *PipelineStep) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with Object

	if err := m.validateCompletedOn(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateImage(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateScriptCommands(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateSetupCommands(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateStartedOn(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateState(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *PipelineStep) validateCompletedOn(formats strfmt.Registry) error {

	if swag.IsZero(m.CompletedOn) { // not required
		return nil
	}

	if err := validate.FormatOf("completed_on", "body", "date-time", m.CompletedOn.String(), formats); err != nil {
		return err
	}

	return nil
}

func (m *PipelineStep) validateImage(formats strfmt.Registry) error {

	if swag.IsZero(m.Image) { // not required
		return nil
	}

	if m.Image != nil {
		if err := m.Image.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("image")
			}
			return err
		}
	}

	return nil
}

func (m *PipelineStep) validateScriptCommands(formats strfmt.Registry) error {

	if swag.IsZero(m.ScriptCommands) { // not required
		return nil
	}

	for i := 0; i < len(m.ScriptCommands); i++ {
		if swag.IsZero(m.ScriptCommands[i]) { // not required
			continue
		}

		if m.ScriptCommands[i] != nil {
			if err := m.ScriptCommands[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("script_commands" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

func (m *PipelineStep) validateSetupCommands(formats strfmt.Registry) error {

	if swag.IsZero(m.SetupCommands) { // not required
		return nil
	}

	for i := 0; i < len(m.SetupCommands); i++ {
		if swag.IsZero(m.SetupCommands[i]) { // not required
			continue
		}

		if m.SetupCommands[i] != nil {
			if err := m.SetupCommands[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("setup_commands" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

func (m *PipelineStep) validateStartedOn(formats strfmt.Registry) error {

	if swag.IsZero(m.StartedOn) { // not required
		return nil
	}

	if err := validate.FormatOf("started_on", "body", "date-time", m.StartedOn.String(), formats); err != nil {
		return err
	}

	return nil
}

func (m *PipelineStep) validateState(formats strfmt.Registry) error {

	if swag.IsZero(m.State) { // not required
		return nil
	}

	if m.State != nil {
		if err := m.State.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("state")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *PipelineStep) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *PipelineStep) UnmarshalBinary(b []byte) error {
	var res PipelineStep
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
