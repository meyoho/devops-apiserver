// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetRepositoriesUsernameRepoSlugSrcNodePathParams creates a new GetRepositoriesUsernameRepoSlugSrcNodePathParams object
// with the default values initialized.
func NewGetRepositoriesUsernameRepoSlugSrcNodePathParams() *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugSrcNodePathParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugSrcNodePathParamsWithTimeout creates a new GetRepositoriesUsernameRepoSlugSrcNodePathParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetRepositoriesUsernameRepoSlugSrcNodePathParamsWithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugSrcNodePathParams{

		timeout: timeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugSrcNodePathParamsWithContext creates a new GetRepositoriesUsernameRepoSlugSrcNodePathParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetRepositoriesUsernameRepoSlugSrcNodePathParamsWithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugSrcNodePathParams{

		Context: ctx,
	}
}

// NewGetRepositoriesUsernameRepoSlugSrcNodePathParamsWithHTTPClient creates a new GetRepositoriesUsernameRepoSlugSrcNodePathParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetRepositoriesUsernameRepoSlugSrcNodePathParamsWithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugSrcNodePathParams{
		HTTPClient: client,
	}
}

/*GetRepositoriesUsernameRepoSlugSrcNodePathParams contains all the parameters to send to the API endpoint
for the get repositories username repo slug src node path operation typically these are written to a http.Request
*/
type GetRepositoriesUsernameRepoSlugSrcNodePathParams struct {

	/*Format
	  If 'meta' is provided, returns the (json) meta data for the contents of the file.  If 'rendered' is provided, returns the contents of a non-binary file in HTML-formatted rendered markup. Since Git and Mercurial do not generally track what text encoding scheme is used, this endpoint attempts to detect the most appropriate character encoding. While usually correct, determining the character encoding can be ambiguous which in exceptional cases can lead to misinterpretation of the characters. As such, the raw element in the response object should not be treated as equivalent to the file's actual contents.

	*/
	Format *string
	/*MaxDepth
	  If provided, returns the contents of the repository and its subdirectories recursively until the specified max_depth of nested directories. When omitted, this defaults to 1.

	*/
	MaxDepth *int64
	/*Node*/
	Node string
	/*Path*/
	Path string
	/*Q
	  Optional filter expression as per [filtering and sorting](../../../../../../meta/filtering).

	*/
	Q *string
	/*RepoSlug*/
	RepoSlug string
	/*Sort
	  Optional sorting parameter as per [filtering and sorting](../../../../../../meta/filtering#query-sort).

	*/
	Sort *string
	/*Username*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithFormat adds the format to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WithFormat(format *string) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	o.SetFormat(format)
	return o
}

// SetFormat adds the format to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) SetFormat(format *string) {
	o.Format = format
}

// WithMaxDepth adds the maxDepth to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WithMaxDepth(maxDepth *int64) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	o.SetMaxDepth(maxDepth)
	return o
}

// SetMaxDepth adds the maxDepth to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) SetMaxDepth(maxDepth *int64) {
	o.MaxDepth = maxDepth
}

// WithNode adds the node to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WithNode(node string) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	o.SetNode(node)
	return o
}

// SetNode adds the node to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) SetNode(node string) {
	o.Node = node
}

// WithPath adds the path to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WithPath(path string) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	o.SetPath(path)
	return o
}

// SetPath adds the path to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) SetPath(path string) {
	o.Path = path
}

// WithQ adds the q to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WithQ(q *string) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	o.SetQ(q)
	return o
}

// SetQ adds the q to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) SetQ(q *string) {
	o.Q = q
}

// WithRepoSlug adds the repoSlug to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WithRepoSlug(repoSlug string) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithSort adds the sort to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WithSort(sort *string) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	o.SetSort(sort)
	return o
}

// SetSort adds the sort to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) SetSort(sort *string) {
	o.Sort = sort
}

// WithUsername adds the username to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WithUsername(username string) *GetRepositoriesUsernameRepoSlugSrcNodePathParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get repositories username repo slug src node path params
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetRepositoriesUsernameRepoSlugSrcNodePathParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Format != nil {

		// query param format
		var qrFormat string
		if o.Format != nil {
			qrFormat = *o.Format
		}
		qFormat := qrFormat
		if qFormat != "" {
			if err := r.SetQueryParam("format", qFormat); err != nil {
				return err
			}
		}

	}

	if o.MaxDepth != nil {

		// query param max_depth
		var qrMaxDepth int64
		if o.MaxDepth != nil {
			qrMaxDepth = *o.MaxDepth
		}
		qMaxDepth := swag.FormatInt64(qrMaxDepth)
		if qMaxDepth != "" {
			if err := r.SetQueryParam("max_depth", qMaxDepth); err != nil {
				return err
			}
		}

	}

	// path param node
	if err := r.SetPathParam("node", o.Node); err != nil {
		return err
	}

	// path param path
	if err := r.SetPathParam("path", o.Path); err != nil {
		return err
	}

	if o.Q != nil {

		// query param q
		var qrQ string
		if o.Q != nil {
			qrQ = *o.Q
		}
		qQ := qrQ
		if qQ != "" {
			if err := r.SetQueryParam("q", qQ); err != nil {
				return err
			}
		}

	}

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	if o.Sort != nil {

		// query param sort
		var qrSort string
		if o.Sort != nil {
			qrSort = *o.Sort
		}
		qSort := qrSort
		if qSort != "" {
			if err := r.SetQueryParam("sort", qSort); err != nil {
				return err
			}
		}

	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
