// Code generated by go-swagger; DO NOT EDIT.

package issue_tracker

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/bitbucket/v2/models"
)

// NewPostRepositoriesUsernameRepoSlugIssuesParams creates a new PostRepositoriesUsernameRepoSlugIssuesParams object
// with the default values initialized.
func NewPostRepositoriesUsernameRepoSlugIssuesParams() *PostRepositoriesUsernameRepoSlugIssuesParams {
	var ()
	return &PostRepositoriesUsernameRepoSlugIssuesParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostRepositoriesUsernameRepoSlugIssuesParamsWithTimeout creates a new PostRepositoriesUsernameRepoSlugIssuesParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostRepositoriesUsernameRepoSlugIssuesParamsWithTimeout(timeout time.Duration) *PostRepositoriesUsernameRepoSlugIssuesParams {
	var ()
	return &PostRepositoriesUsernameRepoSlugIssuesParams{

		timeout: timeout,
	}
}

// NewPostRepositoriesUsernameRepoSlugIssuesParamsWithContext creates a new PostRepositoriesUsernameRepoSlugIssuesParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostRepositoriesUsernameRepoSlugIssuesParamsWithContext(ctx context.Context) *PostRepositoriesUsernameRepoSlugIssuesParams {
	var ()
	return &PostRepositoriesUsernameRepoSlugIssuesParams{

		Context: ctx,
	}
}

// NewPostRepositoriesUsernameRepoSlugIssuesParamsWithHTTPClient creates a new PostRepositoriesUsernameRepoSlugIssuesParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostRepositoriesUsernameRepoSlugIssuesParamsWithHTTPClient(client *http.Client) *PostRepositoriesUsernameRepoSlugIssuesParams {
	var ()
	return &PostRepositoriesUsernameRepoSlugIssuesParams{
		HTTPClient: client,
	}
}

/*PostRepositoriesUsernameRepoSlugIssuesParams contains all the parameters to send to the API endpoint
for the post repositories username repo slug issues operation typically these are written to a http.Request
*/
type PostRepositoriesUsernameRepoSlugIssuesParams struct {

	/*Body
	  The new issue. The only required element is `title`. All other elements can be omitted from the body.

	*/
	Body *models.Issue
	/*RepoSlug
	  This can either be the repository slug or the UUID of the repository,
	surrounded by curly-braces, for example: `{repository UUID}`.


	*/
	RepoSlug string
	/*Username
	  This can either be the username or the UUID of the account,
	surrounded by curly-braces, for example: `{account UUID}`. An account
	is either a team or user.


	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) WithTimeout(timeout time.Duration) *PostRepositoriesUsernameRepoSlugIssuesParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) WithContext(ctx context.Context) *PostRepositoriesUsernameRepoSlugIssuesParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) WithHTTPClient(client *http.Client) *PostRepositoriesUsernameRepoSlugIssuesParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) WithBody(body *models.Issue) *PostRepositoriesUsernameRepoSlugIssuesParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) SetBody(body *models.Issue) {
	o.Body = body
}

// WithRepoSlug adds the repoSlug to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) WithRepoSlug(repoSlug string) *PostRepositoriesUsernameRepoSlugIssuesParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithUsername adds the username to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) WithUsername(username string) *PostRepositoriesUsernameRepoSlugIssuesParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the post repositories username repo slug issues params
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *PostRepositoriesUsernameRepoSlugIssuesParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
