// Code generated by go-swagger; DO NOT EDIT.

package refs

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetRepositoriesUsernameRepoSlugRefsBranchesParams creates a new GetRepositoriesUsernameRepoSlugRefsBranchesParams object
// with the default values initialized.
func NewGetRepositoriesUsernameRepoSlugRefsBranchesParams() *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugRefsBranchesParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugRefsBranchesParamsWithTimeout creates a new GetRepositoriesUsernameRepoSlugRefsBranchesParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetRepositoriesUsernameRepoSlugRefsBranchesParamsWithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugRefsBranchesParams{

		timeout: timeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugRefsBranchesParamsWithContext creates a new GetRepositoriesUsernameRepoSlugRefsBranchesParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetRepositoriesUsernameRepoSlugRefsBranchesParamsWithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugRefsBranchesParams{

		Context: ctx,
	}
}

// NewGetRepositoriesUsernameRepoSlugRefsBranchesParamsWithHTTPClient creates a new GetRepositoriesUsernameRepoSlugRefsBranchesParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetRepositoriesUsernameRepoSlugRefsBranchesParamsWithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugRefsBranchesParams{
		HTTPClient: client,
	}
}

/*GetRepositoriesUsernameRepoSlugRefsBranchesParams contains all the parameters to send to the API endpoint
for the get repositories username repo slug refs branches operation typically these are written to a http.Request
*/
type GetRepositoriesUsernameRepoSlugRefsBranchesParams struct {

	/*Page*/
	Page *int32
	/*Pagelen*/
	Pagelen *int32
	/*Q

	Query string to narrow down the response as per
	[filtering and sorting](../../../../../../meta/filtering).

	*/
	Q *string
	/*RepoSlug

	This can either be the repository slug or the UUID of the repository,
	surrounded by curly-braces, for example: `{repository UUID}`.


	*/
	RepoSlug string
	/*Sort

	Field by which the results should be sorted as per
	[filtering and sorting](../../../../../../meta/filtering). The `name`
	field is handled specially for branches in that, if specified as the sort field, it
	uses a natural sort order instead of the default lexicographical sort order. For example,
	it will return ['branch1', 'branch2', 'branch10'] instead of ['branch1', 'branch10', 'branch2'].

	*/
	Sort *string
	/*Username

	This can either be the username or the UUID of the user,
	surrounded by curly-braces, for example: `{user UUID}`.


	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) WithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) WithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) WithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithPage adds the page to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) WithPage(page *int32) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) SetPage(page *int32) {
	o.Page = page
}

// WithPagelen adds the pagelen to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) WithPagelen(pagelen *int32) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	o.SetPagelen(pagelen)
	return o
}

// SetPagelen adds the pagelen to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) SetPagelen(pagelen *int32) {
	o.Pagelen = pagelen
}

// WithQ adds the q to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) WithQ(q *string) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	o.SetQ(q)
	return o
}

// SetQ adds the q to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) SetQ(q *string) {
	o.Q = q
}

// WithRepoSlug adds the repoSlug to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) WithRepoSlug(repoSlug string) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithSort adds the sort to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) WithSort(sort *string) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	o.SetSort(sort)
	return o
}

// SetSort adds the sort to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) SetSort(sort *string) {
	o.Sort = sort
}

// WithUsername adds the username to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) WithUsername(username string) *GetRepositoriesUsernameRepoSlugRefsBranchesParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get repositories username repo slug refs branches params
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetRepositoriesUsernameRepoSlugRefsBranchesParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.Pagelen != nil {

		// query param pagelen
		var qrPagelen int32
		if o.Pagelen != nil {
			qrPagelen = *o.Pagelen
		}
		qPagelen := swag.FormatInt32(qrPagelen)
		if qPagelen != "" {
			if err := r.SetQueryParam("pagelen", qPagelen); err != nil {
				return err
			}
		}

	}

	if o.Q != nil {

		// query param q
		var qrQ string
		if o.Q != nil {
			qrQ = *o.Q
		}
		qQ := qrQ
		if qQ != "" {
			if err := r.SetQueryParam("q", qQ); err != nil {
				return err
			}
		}

	}

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	if o.Sort != nil {

		// query param sort
		var qrSort string
		if o.Sort != nil {
			qrSort = *o.Sort
		}
		qSort := qrSort
		if qSort != "" {
			if err := r.SetQueryParam("sort", qSort); err != nil {
				return err
			}
		}

	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
