// Code generated by go-swagger; DO NOT EDIT.

package pullrequests

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/bitbucket/v2/models"
)

// NewPutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams creates a new PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams object
// with the default values initialized.
func NewPutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams() *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams {
	var ()
	return &PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParamsWithTimeout creates a new PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParamsWithTimeout(timeout time.Duration) *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams {
	var ()
	return &PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams{

		timeout: timeout,
	}
}

// NewPutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParamsWithContext creates a new PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewPutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParamsWithContext(ctx context.Context) *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams {
	var ()
	return &PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams{

		Context: ctx,
	}
}

// NewPutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParamsWithHTTPClient creates a new PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParamsWithHTTPClient(client *http.Client) *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams {
	var ()
	return &PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams{
		HTTPClient: client,
	}
}

/*PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams contains all the parameters to send to the API endpoint
for the put repositories username repo slug pullrequests pull request ID operation typically these are written to a http.Request
*/
type PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams struct {

	/*Body
	  The pull request that is to be updated.

	*/
	Body *models.Pullrequest
	/*PullRequestID
	  The id of the pull request.

	*/
	PullRequestID int64
	/*RepoSlug
	  This can either be the repository slug or the UUID of the repository,
	surrounded by curly-braces, for example: `{repository UUID}`.


	*/
	RepoSlug string
	/*Username
	  This can either be the username or the UUID of the account,
	surrounded by curly-braces, for example: `{account UUID}`. An account
	is either a team or user.


	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) WithTimeout(timeout time.Duration) *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) WithContext(ctx context.Context) *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) WithHTTPClient(client *http.Client) *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) WithBody(body *models.Pullrequest) *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) SetBody(body *models.Pullrequest) {
	o.Body = body
}

// WithPullRequestID adds the pullRequestID to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) WithPullRequestID(pullRequestID int64) *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams {
	o.SetPullRequestID(pullRequestID)
	return o
}

// SetPullRequestID adds the pullRequestId to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) SetPullRequestID(pullRequestID int64) {
	o.PullRequestID = pullRequestID
}

// WithRepoSlug adds the repoSlug to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) WithRepoSlug(repoSlug string) *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithUsername adds the username to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) WithUsername(username string) *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the put repositories username repo slug pullrequests pull request ID params
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *PutRepositoriesUsernameRepoSlugPullrequestsPullRequestIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	// path param pull_request_id
	if err := r.SetPathParam("pull_request_id", swag.FormatInt64(o.PullRequestID)); err != nil {
		return err
	}

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
