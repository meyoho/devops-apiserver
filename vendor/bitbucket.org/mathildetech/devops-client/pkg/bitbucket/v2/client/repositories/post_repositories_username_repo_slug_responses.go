// Code generated by go-swagger; DO NOT EDIT.

package repositories

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/bitbucket/v2/models"
)

// PostRepositoriesUsernameRepoSlugReader is a Reader for the PostRepositoriesUsernameRepoSlug structure.
type PostRepositoriesUsernameRepoSlugReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostRepositoriesUsernameRepoSlugReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPostRepositoriesUsernameRepoSlugOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 400:
		result := NewPostRepositoriesUsernameRepoSlugBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 401:
		result := NewPostRepositoriesUsernameRepoSlugUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostRepositoriesUsernameRepoSlugOK creates a PostRepositoriesUsernameRepoSlugOK with default headers values
func NewPostRepositoriesUsernameRepoSlugOK() *PostRepositoriesUsernameRepoSlugOK {
	return &PostRepositoriesUsernameRepoSlugOK{}
}

/*PostRepositoriesUsernameRepoSlugOK handles this case with default header values.

The newly created repository.
*/
type PostRepositoriesUsernameRepoSlugOK struct {
	Payload *models.Repository
}

func (o *PostRepositoriesUsernameRepoSlugOK) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}][%d] postRepositoriesUsernameRepoSlugOK  %+v", 200, o.Payload)
}

func (o *PostRepositoriesUsernameRepoSlugOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Repository)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPostRepositoriesUsernameRepoSlugBadRequest creates a PostRepositoriesUsernameRepoSlugBadRequest with default headers values
func NewPostRepositoriesUsernameRepoSlugBadRequest() *PostRepositoriesUsernameRepoSlugBadRequest {
	return &PostRepositoriesUsernameRepoSlugBadRequest{}
}

/*PostRepositoriesUsernameRepoSlugBadRequest handles this case with default header values.

If the input document was invalid, or if the caller lacks the privilege to create repositories under the targeted account.
*/
type PostRepositoriesUsernameRepoSlugBadRequest struct {
	Payload *models.Error
}

func (o *PostRepositoriesUsernameRepoSlugBadRequest) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}][%d] postRepositoriesUsernameRepoSlugBadRequest  %+v", 400, o.Payload)
}

func (o *PostRepositoriesUsernameRepoSlugBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPostRepositoriesUsernameRepoSlugUnauthorized creates a PostRepositoriesUsernameRepoSlugUnauthorized with default headers values
func NewPostRepositoriesUsernameRepoSlugUnauthorized() *PostRepositoriesUsernameRepoSlugUnauthorized {
	return &PostRepositoriesUsernameRepoSlugUnauthorized{}
}

/*PostRepositoriesUsernameRepoSlugUnauthorized handles this case with default header values.

If the request was not authenticated.
*/
type PostRepositoriesUsernameRepoSlugUnauthorized struct {
	Payload *models.Error
}

func (o *PostRepositoriesUsernameRepoSlugUnauthorized) Error() string {
	return fmt.Sprintf("[POST /repositories/{username}/{repo_slug}][%d] postRepositoriesUsernameRepoSlugUnauthorized  %+v", 401, o.Payload)
}

func (o *PostRepositoriesUsernameRepoSlugUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
