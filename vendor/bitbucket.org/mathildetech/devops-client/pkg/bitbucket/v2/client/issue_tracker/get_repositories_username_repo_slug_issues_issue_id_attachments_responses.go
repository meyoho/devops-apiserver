// Code generated by go-swagger; DO NOT EDIT.

package issue_tracker

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/bitbucket/v2/models"
)

// GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsReader is a Reader for the GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachments structure.
type GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsOK creates a GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsOK with default headers values
func NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsOK() *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsOK {
	return &GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsOK{}
}

/*GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsOK handles this case with default header values.

A paginated list of all attachments for this issue.
*/
type GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsOK struct {
	Payload *models.PaginatedIssueAttachments
}

func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsOK) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments][%d] getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsOK  %+v", 200, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.PaginatedIssueAttachments)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsUnauthorized creates a GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsUnauthorized with default headers values
func NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsUnauthorized() *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsUnauthorized {
	return &GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsUnauthorized{}
}

/*GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsUnauthorized handles this case with default header values.

If the issue tracker is private and the request was not authenticated.
*/
type GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsUnauthorized struct {
}

func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsUnauthorized) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments][%d] getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsUnauthorized ", 401)
}

func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsNotFound creates a GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsNotFound with default headers values
func NewGetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsNotFound() *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsNotFound {
	return &GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsNotFound{}
}

/*GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsNotFound handles this case with default header values.

The specified repository or issue does not exist or does not have the issue tracker enabled.
*/
type GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsNotFound struct {
	Payload *models.Error
}

func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsNotFound) Error() string {
	return fmt.Sprintf("[GET /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments][%d] getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsNotFound  %+v", 404, o.Payload)
}

func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
