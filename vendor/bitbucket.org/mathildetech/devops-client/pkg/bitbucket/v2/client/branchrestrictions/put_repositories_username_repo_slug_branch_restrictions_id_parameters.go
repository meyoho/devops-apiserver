// Code generated by go-swagger; DO NOT EDIT.

package branchrestrictions

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/bitbucket/v2/models"
)

// NewPutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams creates a new PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams object
// with the default values initialized.
func NewPutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams() *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams {
	var ()
	return &PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPutRepositoriesUsernameRepoSlugBranchRestrictionsIDParamsWithTimeout creates a new PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPutRepositoriesUsernameRepoSlugBranchRestrictionsIDParamsWithTimeout(timeout time.Duration) *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams {
	var ()
	return &PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams{

		timeout: timeout,
	}
}

// NewPutRepositoriesUsernameRepoSlugBranchRestrictionsIDParamsWithContext creates a new PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewPutRepositoriesUsernameRepoSlugBranchRestrictionsIDParamsWithContext(ctx context.Context) *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams {
	var ()
	return &PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams{

		Context: ctx,
	}
}

// NewPutRepositoriesUsernameRepoSlugBranchRestrictionsIDParamsWithHTTPClient creates a new PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPutRepositoriesUsernameRepoSlugBranchRestrictionsIDParamsWithHTTPClient(client *http.Client) *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams {
	var ()
	return &PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams{
		HTTPClient: client,
	}
}

/*PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams contains all the parameters to send to the API endpoint
for the put repositories username repo slug branch restrictions ID operation typically these are written to a http.Request
*/
type PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams struct {

	/*Body
	  The new version of the existing rule

	*/
	Body *models.Branchrestriction
	/*ID
	  The restriction rule's id

	*/
	ID string
	/*RepoSlug
	  This can either be the repository slug or the UUID of the repository,
	surrounded by curly-braces, for example: `{repository UUID}`.


	*/
	RepoSlug string
	/*Username
	  This can either be the username or the UUID of the account,
	surrounded by curly-braces, for example: `{account UUID}`. An account
	is either a team or user.


	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) WithTimeout(timeout time.Duration) *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) WithContext(ctx context.Context) *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) WithHTTPClient(client *http.Client) *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) WithBody(body *models.Branchrestriction) *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) SetBody(body *models.Branchrestriction) {
	o.Body = body
}

// WithID adds the id to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) WithID(id string) *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) SetID(id string) {
	o.ID = id
}

// WithRepoSlug adds the repoSlug to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) WithRepoSlug(repoSlug string) *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithUsername adds the username to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) WithUsername(username string) *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the put repositories username repo slug branch restrictions ID params
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *PutRepositoriesUsernameRepoSlugBranchRestrictionsIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
