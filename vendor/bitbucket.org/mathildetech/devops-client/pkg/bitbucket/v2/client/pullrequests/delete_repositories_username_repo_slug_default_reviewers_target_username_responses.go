// Code generated by go-swagger; DO NOT EDIT.

package pullrequests

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/bitbucket/v2/models"
)

// DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameReader is a Reader for the DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername structure.
type DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {

	result := NewDeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDefault(response.Code())
	if err := result.readResponse(response, consumer, o.formats); err != nil {
		return nil, err
	}
	if response.Code()/100 == 2 {
		return result, nil
	}
	return nil, result

}

// NewDeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDefault creates a DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDefault with default headers values
func NewDeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDefault(code int) *DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDefault {
	return &DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDefault{
		_statusCode: code,
	}
}

/*DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDefault handles this case with default header values.

Unexpected error.
*/
type DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDefault struct {
	_statusCode int

	Payload *models.Error
}

// Code gets the status code for the delete repositories username repo slug default reviewers target username default response
func (o *DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDefault) Code() int {
	return o._statusCode
}

func (o *DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDefault) Error() string {
	return fmt.Sprintf("[DELETE /repositories/{username}/{repo_slug}/default-reviewers/{target_username}][%d] DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername default  %+v", o._statusCode, o.Payload)
}

func (o *DeleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsernameDefault) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
