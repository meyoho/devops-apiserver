// Code generated by go-swagger; DO NOT EDIT.

package issue_tracker

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/bitbucket/v2/models"
)

// DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathReader is a Reader for the DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPath structure.
type DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 204:
		result := NewDeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 401:
		result := NewDeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewDeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewDeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNoContent creates a DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNoContent with default headers values
func NewDeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNoContent() *DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNoContent {
	return &DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNoContent{}
}

/*DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNoContent handles this case with default header values.

Indicates that the deletion was successful
*/
type DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNoContent struct {
}

func (o *DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNoContent) Error() string {
	return fmt.Sprintf("[DELETE /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments/{path}][%d] deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathNoContent ", 204)
}

func (o *DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewDeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathUnauthorized creates a DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathUnauthorized with default headers values
func NewDeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathUnauthorized() *DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathUnauthorized {
	return &DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathUnauthorized{}
}

/*DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathUnauthorized handles this case with default header values.

If the issue tracker is private and the request was not authenticated.
*/
type DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathUnauthorized struct {
}

func (o *DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathUnauthorized) Error() string {
	return fmt.Sprintf("[DELETE /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments/{path}][%d] deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathUnauthorized ", 401)
}

func (o *DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewDeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNotFound creates a DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNotFound with default headers values
func NewDeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNotFound() *DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNotFound {
	return &DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNotFound{}
}

/*DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNotFound handles this case with default header values.

The specified repository or issue does not exist or does not have the issue tracker enabled.
*/
type DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNotFound struct {
	Payload *models.Error
}

func (o *DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNotFound) Error() string {
	return fmt.Sprintf("[DELETE /repositories/{username}/{repo_slug}/issues/{issue_id}/attachments/{path}][%d] deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPathNotFound  %+v", 404, o.Payload)
}

func (o *DeleteRepositoriesUsernameRepoSlugIssuesIssueIDAttachmentsPathNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
