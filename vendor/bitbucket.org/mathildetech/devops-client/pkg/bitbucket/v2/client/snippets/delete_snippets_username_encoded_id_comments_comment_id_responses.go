// Code generated by go-swagger; DO NOT EDIT.

package snippets

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/bitbucket/v2/models"
)

// DeleteSnippetsUsernameEncodedIDCommentsCommentIDReader is a Reader for the DeleteSnippetsUsernameEncodedIDCommentsCommentID structure.
type DeleteSnippetsUsernameEncodedIDCommentsCommentIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteSnippetsUsernameEncodedIDCommentsCommentIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 204:
		result := NewDeleteSnippetsUsernameEncodedIDCommentsCommentIDNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewDeleteSnippetsUsernameEncodedIDCommentsCommentIDForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 404:
		result := NewDeleteSnippetsUsernameEncodedIDCommentsCommentIDNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewDeleteSnippetsUsernameEncodedIDCommentsCommentIDNoContent creates a DeleteSnippetsUsernameEncodedIDCommentsCommentIDNoContent with default headers values
func NewDeleteSnippetsUsernameEncodedIDCommentsCommentIDNoContent() *DeleteSnippetsUsernameEncodedIDCommentsCommentIDNoContent {
	return &DeleteSnippetsUsernameEncodedIDCommentsCommentIDNoContent{}
}

/*DeleteSnippetsUsernameEncodedIDCommentsCommentIDNoContent handles this case with default header values.

Indicates the comment was deleted successfully.
*/
type DeleteSnippetsUsernameEncodedIDCommentsCommentIDNoContent struct {
}

func (o *DeleteSnippetsUsernameEncodedIDCommentsCommentIDNoContent) Error() string {
	return fmt.Sprintf("[DELETE /snippets/{username}/{encoded_id}/comments/{comment_id}][%d] deleteSnippetsUsernameEncodedIdCommentsCommentIdNoContent ", 204)
}

func (o *DeleteSnippetsUsernameEncodedIDCommentsCommentIDNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewDeleteSnippetsUsernameEncodedIDCommentsCommentIDForbidden creates a DeleteSnippetsUsernameEncodedIDCommentsCommentIDForbidden with default headers values
func NewDeleteSnippetsUsernameEncodedIDCommentsCommentIDForbidden() *DeleteSnippetsUsernameEncodedIDCommentsCommentIDForbidden {
	return &DeleteSnippetsUsernameEncodedIDCommentsCommentIDForbidden{}
}

/*DeleteSnippetsUsernameEncodedIDCommentsCommentIDForbidden handles this case with default header values.

If the authenticated user is not the author of the comment.
*/
type DeleteSnippetsUsernameEncodedIDCommentsCommentIDForbidden struct {
	Payload *models.Error
}

func (o *DeleteSnippetsUsernameEncodedIDCommentsCommentIDForbidden) Error() string {
	return fmt.Sprintf("[DELETE /snippets/{username}/{encoded_id}/comments/{comment_id}][%d] deleteSnippetsUsernameEncodedIdCommentsCommentIdForbidden  %+v", 403, o.Payload)
}

func (o *DeleteSnippetsUsernameEncodedIDCommentsCommentIDForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewDeleteSnippetsUsernameEncodedIDCommentsCommentIDNotFound creates a DeleteSnippetsUsernameEncodedIDCommentsCommentIDNotFound with default headers values
func NewDeleteSnippetsUsernameEncodedIDCommentsCommentIDNotFound() *DeleteSnippetsUsernameEncodedIDCommentsCommentIDNotFound {
	return &DeleteSnippetsUsernameEncodedIDCommentsCommentIDNotFound{}
}

/*DeleteSnippetsUsernameEncodedIDCommentsCommentIDNotFound handles this case with default header values.

If the comment or the snippet does not exist.
*/
type DeleteSnippetsUsernameEncodedIDCommentsCommentIDNotFound struct {
	Payload *models.Error
}

func (o *DeleteSnippetsUsernameEncodedIDCommentsCommentIDNotFound) Error() string {
	return fmt.Sprintf("[DELETE /snippets/{username}/{encoded_id}/comments/{comment_id}][%d] deleteSnippetsUsernameEncodedIdCommentsCommentIdNotFound  %+v", 404, o.Payload)
}

func (o *DeleteSnippetsUsernameEncodedIDCommentsCommentIDNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Error)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
