package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v1 "bitbucket.org/mathildetech/devops-client/pkg/taiga/v1"
)

func init() {
	register(devopsv1.TypeTaiga, versionOpt{
		version:   "v1",
		factory:   v1.NewClient(),
		isDefault: true,
	})
}
