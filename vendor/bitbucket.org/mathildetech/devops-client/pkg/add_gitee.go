package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v5 "bitbucket.org/mathildetech/devops-client/pkg/gitee/v5"
)

func init() {
	register(devopsv1.TypeGitee, versionOpt{
		version:   "v5",
		factory:   v5.NewClient(),
		isDefault: true,
	})
}
