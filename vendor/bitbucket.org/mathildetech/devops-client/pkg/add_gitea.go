package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v1_9_4 "bitbucket.org/mathildetech/devops-client/pkg/gitea/v1_9_4"
)

func init() {
	register(devopsv1.TypeGitea, versionOpt{
		version:   "v1.9.4",
		factory:   v1_9_4.NewClient(),
		isDefault: true,
	})
}
