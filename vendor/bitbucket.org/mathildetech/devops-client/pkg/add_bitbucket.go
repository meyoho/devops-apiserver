package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v2 "bitbucket.org/mathildetech/devops-client/pkg/bitbucket/v2"
)

func init() {
	register(devopsv1.TypeBitbucket, versionOpt{
		version:   "v2",
		factory:   v2.NewClient(),
		isDefault: true,
	})
}
