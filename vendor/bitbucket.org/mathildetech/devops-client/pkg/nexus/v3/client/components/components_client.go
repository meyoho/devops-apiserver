// Code generated by go-swagger; DO NOT EDIT.

package components

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new components API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for components API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
DeleteComponent deletes a single component
*/
func (a *Client) DeleteComponent(params *DeleteComponentParams, authInfo runtime.ClientAuthInfoWriter) (*DeleteComponentNoContent, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewDeleteComponentParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "deleteComponent",
		Method:             "DELETE",
		PathPattern:        "/v1/components/{id}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &DeleteComponentReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*DeleteComponentNoContent), nil

}

/*
GetComponentByID gets a single component
*/
func (a *Client) GetComponentByID(params *GetComponentByIDParams, authInfo runtime.ClientAuthInfoWriter) (*GetComponentByIDOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetComponentByIDParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getComponentById",
		Method:             "GET",
		PathPattern:        "/v1/components/{id}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetComponentByIDReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetComponentByIDOK), nil

}

/*
GetComponents lists components
*/
func (a *Client) GetComponents(params *GetComponentsParams, authInfo runtime.ClientAuthInfoWriter) (*GetComponentsOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetComponentsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getComponents",
		Method:             "GET",
		PathPattern:        "/v1/components",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &GetComponentsReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetComponentsOK), nil

}

/*
UploadComponent uploads a single component
*/
func (a *Client) UploadComponent(params *UploadComponentParams, authInfo runtime.ClientAuthInfoWriter) error {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewUploadComponentParams()
	}

	_, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "uploadComponent",
		Method:             "POST",
		PathPattern:        "/v1/components",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"multipart/form-data"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &UploadComponentReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return err
	}
	return nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
