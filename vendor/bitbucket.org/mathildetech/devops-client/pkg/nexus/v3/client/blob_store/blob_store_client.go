// Code generated by go-swagger; DO NOT EDIT.

package blob_store

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new blob store API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for blob store API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
QuotaStatus gets quota status for a given blob store
*/
func (a *Client) QuotaStatus(params *QuotaStatusParams, authInfo runtime.ClientAuthInfoWriter) (*QuotaStatusOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewQuotaStatusParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "quotaStatus",
		Method:             "GET",
		PathPattern:        "/v1/blobstores/{id}/quota-status",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http"},
		Params:             params,
		Reader:             &QuotaStatusReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*QuotaStatusOK), nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
