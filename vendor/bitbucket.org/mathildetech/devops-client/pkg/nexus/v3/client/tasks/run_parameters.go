// Code generated by go-swagger; DO NOT EDIT.

package tasks

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewRunParams creates a new RunParams object
// with the default values initialized.
func NewRunParams() *RunParams {
	var ()
	return &RunParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewRunParamsWithTimeout creates a new RunParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewRunParamsWithTimeout(timeout time.Duration) *RunParams {
	var ()
	return &RunParams{

		timeout: timeout,
	}
}

// NewRunParamsWithContext creates a new RunParams object
// with the default values initialized, and the ability to set a context for a request
func NewRunParamsWithContext(ctx context.Context) *RunParams {
	var ()
	return &RunParams{

		Context: ctx,
	}
}

// NewRunParamsWithHTTPClient creates a new RunParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewRunParamsWithHTTPClient(client *http.Client) *RunParams {
	var ()
	return &RunParams{
		HTTPClient: client,
	}
}

/*RunParams contains all the parameters to send to the API endpoint
for the run operation typically these are written to a http.Request
*/
type RunParams struct {

	/*ID
	  Id of the task to run

	*/
	ID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the run params
func (o *RunParams) WithTimeout(timeout time.Duration) *RunParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the run params
func (o *RunParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the run params
func (o *RunParams) WithContext(ctx context.Context) *RunParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the run params
func (o *RunParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the run params
func (o *RunParams) WithHTTPClient(client *http.Client) *RunParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the run params
func (o *RunParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the run params
func (o *RunParams) WithID(id string) *RunParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the run params
func (o *RunParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *RunParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
