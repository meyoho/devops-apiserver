package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v3 "bitbucket.org/mathildetech/devops-client/pkg/github/v3"
)

func init() {
	register(devopsv1.TypeGithub, versionOpt{
		version:   "v3",
		factory:   v3.NewClient(),
		isDefault: true,
	})
}
