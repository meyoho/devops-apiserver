// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"
)

// Emojis emojis
// swagger:model emojis
type Emojis map[string]string

// Validate validates this emojis
func (m Emojis) Validate(formats strfmt.Registry) error {
	return nil
}
