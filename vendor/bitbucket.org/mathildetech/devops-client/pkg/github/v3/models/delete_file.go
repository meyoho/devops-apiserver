// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// DeleteFile delete file
// swagger:model deleteFile
type DeleteFile struct {

	// commit
	Commit *DeleteFileCommit `json:"commit,omitempty"`

	// content
	Content string `json:"content,omitempty"`
}

// Validate validates this delete file
func (m *DeleteFile) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateCommit(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *DeleteFile) validateCommit(formats strfmt.Registry) error {

	if swag.IsZero(m.Commit) { // not required
		return nil
	}

	if m.Commit != nil {
		if err := m.Commit.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("commit")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *DeleteFile) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *DeleteFile) UnmarshalBinary(b []byte) error {
	var res DeleteFile
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// DeleteFileCommit delete file commit
// swagger:model DeleteFileCommit
type DeleteFileCommit struct {

	// author
	Author *DeleteFileCommitAuthor `json:"author,omitempty"`

	// committer
	Committer *DeleteFileCommitCommitter `json:"committer,omitempty"`

	// html url
	HTMLURL string `json:"html_url,omitempty"`

	// message
	Message string `json:"message,omitempty"`

	// parents
	Parents *DeleteFileCommitParents `json:"parents,omitempty"`

	// sha
	Sha string `json:"sha,omitempty"`

	// tree
	Tree *DeleteFileCommitTree `json:"tree,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this delete file commit
func (m *DeleteFileCommit) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateAuthor(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateCommitter(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateParents(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateTree(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *DeleteFileCommit) validateAuthor(formats strfmt.Registry) error {

	if swag.IsZero(m.Author) { // not required
		return nil
	}

	if m.Author != nil {
		if err := m.Author.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("commit" + "." + "author")
			}
			return err
		}
	}

	return nil
}

func (m *DeleteFileCommit) validateCommitter(formats strfmt.Registry) error {

	if swag.IsZero(m.Committer) { // not required
		return nil
	}

	if m.Committer != nil {
		if err := m.Committer.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("commit" + "." + "committer")
			}
			return err
		}
	}

	return nil
}

func (m *DeleteFileCommit) validateParents(formats strfmt.Registry) error {

	if swag.IsZero(m.Parents) { // not required
		return nil
	}

	if m.Parents != nil {
		if err := m.Parents.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("commit" + "." + "parents")
			}
			return err
		}
	}

	return nil
}

func (m *DeleteFileCommit) validateTree(formats strfmt.Registry) error {

	if swag.IsZero(m.Tree) { // not required
		return nil
	}

	if m.Tree != nil {
		if err := m.Tree.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("commit" + "." + "tree")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *DeleteFileCommit) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *DeleteFileCommit) UnmarshalBinary(b []byte) error {
	var res DeleteFileCommit
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// DeleteFileCommitAuthor delete file commit author
// swagger:model DeleteFileCommitAuthor
type DeleteFileCommitAuthor struct {

	// date
	Date string `json:"date,omitempty"`

	// email
	Email string `json:"email,omitempty"`

	// name
	Name string `json:"name,omitempty"`
}

// Validate validates this delete file commit author
func (m *DeleteFileCommitAuthor) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *DeleteFileCommitAuthor) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *DeleteFileCommitAuthor) UnmarshalBinary(b []byte) error {
	var res DeleteFileCommitAuthor
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// DeleteFileCommitCommitter delete file commit committer
// swagger:model DeleteFileCommitCommitter
type DeleteFileCommitCommitter struct {

	// date
	Date string `json:"date,omitempty"`

	// email
	Email string `json:"email,omitempty"`

	// name
	Name string `json:"name,omitempty"`
}

// Validate validates this delete file commit committer
func (m *DeleteFileCommitCommitter) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *DeleteFileCommitCommitter) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *DeleteFileCommitCommitter) UnmarshalBinary(b []byte) error {
	var res DeleteFileCommitCommitter
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// DeleteFileCommitParents delete file commit parents
// swagger:model DeleteFileCommitParents
type DeleteFileCommitParents struct {

	// html url
	HTMLURL string `json:"html_url,omitempty"`

	// sha
	Sha string `json:"sha,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this delete file commit parents
func (m *DeleteFileCommitParents) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *DeleteFileCommitParents) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *DeleteFileCommitParents) UnmarshalBinary(b []byte) error {
	var res DeleteFileCommitParents
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// DeleteFileCommitTree delete file commit tree
// swagger:model DeleteFileCommitTree
type DeleteFileCommitTree struct {

	// sha
	Sha string `json:"sha,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this delete file commit tree
func (m *DeleteFileCommitTree) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *DeleteFileCommitTree) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *DeleteFileCommitTree) UnmarshalBinary(b []byte) error {
	var res DeleteFileCommitTree
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
