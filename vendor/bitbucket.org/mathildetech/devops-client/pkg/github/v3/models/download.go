// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// Download download
// swagger:model download
type Download struct {

	// content type
	ContentType string `json:"content_type,omitempty"`

	// description
	Description string `json:"description,omitempty"`

	// download count
	DownloadCount int64 `json:"download_count,omitempty"`

	// html url
	HTMLURL string `json:"html_url,omitempty"`

	// id
	ID int64 `json:"id,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// size
	Size int64 `json:"size,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this download
func (m *Download) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *Download) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Download) UnmarshalBinary(b []byte) error {
	var res Download
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
