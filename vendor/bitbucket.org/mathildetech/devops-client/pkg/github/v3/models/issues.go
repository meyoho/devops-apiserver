// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"strconv"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// Issues issues
// swagger:model issues
type Issues []*IssuesItems0

// Validate validates this issues
func (m Issues) Validate(formats strfmt.Registry) error {
	var res []error

	for i := 0; i < len(m); i++ {
		if swag.IsZero(m[i]) { // not required
			continue
		}

		if m[i] != nil {
			if err := m[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName(strconv.Itoa(i))
				}
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// IssuesItems0 issues items0
// swagger:model IssuesItems0
type IssuesItems0 struct {

	// assignee
	Assignee *User `json:"assignee,omitempty"`

	// body
	Body string `json:"body,omitempty"`

	// ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
	ClosedAt string `json:"closed_at,omitempty"`

	// comments
	Comments int64 `json:"comments,omitempty"`

	// ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
	CreatedAt string `json:"created_at,omitempty"`

	// html url
	HTMLURL string `json:"html_url,omitempty"`

	// labels
	Labels []*IssuesItems0LabelsItems0 `json:"labels"`

	// milestone
	Milestone *IssuesItems0Milestone `json:"milestone,omitempty"`

	// number
	Number int64 `json:"number,omitempty"`

	// pull request
	PullRequest *IssuesItems0PullRequest `json:"pull_request,omitempty"`

	// state
	// Enum: [open closed]
	State interface{} `json:"state,omitempty"`

	// title
	Title string `json:"title,omitempty"`

	// ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
	UpdatedAt string `json:"updated_at,omitempty"`

	// url
	URL string `json:"url,omitempty"`

	// user
	User *User `json:"user,omitempty"`
}

// Validate validates this issues items0
func (m *IssuesItems0) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateAssignee(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateLabels(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateMilestone(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validatePullRequest(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateUser(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *IssuesItems0) validateAssignee(formats strfmt.Registry) error {

	if swag.IsZero(m.Assignee) { // not required
		return nil
	}

	if m.Assignee != nil {
		if err := m.Assignee.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("assignee")
			}
			return err
		}
	}

	return nil
}

func (m *IssuesItems0) validateLabels(formats strfmt.Registry) error {

	if swag.IsZero(m.Labels) { // not required
		return nil
	}

	for i := 0; i < len(m.Labels); i++ {
		if swag.IsZero(m.Labels[i]) { // not required
			continue
		}

		if m.Labels[i] != nil {
			if err := m.Labels[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("labels" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

func (m *IssuesItems0) validateMilestone(formats strfmt.Registry) error {

	if swag.IsZero(m.Milestone) { // not required
		return nil
	}

	if m.Milestone != nil {
		if err := m.Milestone.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("milestone")
			}
			return err
		}
	}

	return nil
}

func (m *IssuesItems0) validatePullRequest(formats strfmt.Registry) error {

	if swag.IsZero(m.PullRequest) { // not required
		return nil
	}

	if m.PullRequest != nil {
		if err := m.PullRequest.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("pull_request")
			}
			return err
		}
	}

	return nil
}

func (m *IssuesItems0) validateUser(formats strfmt.Registry) error {

	if swag.IsZero(m.User) { // not required
		return nil
	}

	if m.User != nil {
		if err := m.User.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("user")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *IssuesItems0) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *IssuesItems0) UnmarshalBinary(b []byte) error {
	var res IssuesItems0
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// IssuesItems0LabelsItems0 issues items0 labels items0
// swagger:model IssuesItems0LabelsItems0
type IssuesItems0LabelsItems0 struct {

	// color
	Color string `json:"color,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this issues items0 labels items0
func (m *IssuesItems0LabelsItems0) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *IssuesItems0LabelsItems0) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *IssuesItems0LabelsItems0) UnmarshalBinary(b []byte) error {
	var res IssuesItems0LabelsItems0
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// IssuesItems0Milestone issues items0 milestone
// swagger:model IssuesItems0Milestone
type IssuesItems0Milestone struct {

	// closed issues
	ClosedIssues int64 `json:"closed_issues,omitempty"`

	// ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
	CreatedAt string `json:"created_at,omitempty"`

	// creator
	Creator *User `json:"creator,omitempty"`

	// description
	Description string `json:"description,omitempty"`

	// ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
	DueOn string `json:"due_on,omitempty"`

	// number
	Number int64 `json:"number,omitempty"`

	// open issues
	OpenIssues int64 `json:"open_issues,omitempty"`

	// state
	// Enum: [open closed]
	State interface{} `json:"state,omitempty"`

	// title
	Title string `json:"title,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this issues items0 milestone
func (m *IssuesItems0Milestone) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateCreator(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *IssuesItems0Milestone) validateCreator(formats strfmt.Registry) error {

	if swag.IsZero(m.Creator) { // not required
		return nil
	}

	if m.Creator != nil {
		if err := m.Creator.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("milestone" + "." + "creator")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *IssuesItems0Milestone) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *IssuesItems0Milestone) UnmarshalBinary(b []byte) error {
	var res IssuesItems0Milestone
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// IssuesItems0PullRequest issues items0 pull request
// swagger:model IssuesItems0PullRequest
type IssuesItems0PullRequest struct {

	// diff url
	DiffURL string `json:"diff_url,omitempty"`

	// html url
	HTMLURL string `json:"html_url,omitempty"`

	// patch url
	PatchURL string `json:"patch_url,omitempty"`
}

// Validate validates this issues items0 pull request
func (m *IssuesItems0PullRequest) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *IssuesItems0PullRequest) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *IssuesItems0PullRequest) UnmarshalBinary(b []byte) error {
	var res IssuesItems0PullRequest
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
