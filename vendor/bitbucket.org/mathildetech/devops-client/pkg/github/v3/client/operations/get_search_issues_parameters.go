// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetSearchIssuesParams creates a new GetSearchIssuesParams object
// with the default values initialized.
func NewGetSearchIssuesParams() *GetSearchIssuesParams {
	var (
		orderDefault = string("desc")
	)
	return &GetSearchIssuesParams{
		Order: &orderDefault,

		timeout: cr.DefaultTimeout,
	}
}

// NewGetSearchIssuesParamsWithTimeout creates a new GetSearchIssuesParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetSearchIssuesParamsWithTimeout(timeout time.Duration) *GetSearchIssuesParams {
	var (
		orderDefault = string("desc")
	)
	return &GetSearchIssuesParams{
		Order: &orderDefault,

		timeout: timeout,
	}
}

// NewGetSearchIssuesParamsWithContext creates a new GetSearchIssuesParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetSearchIssuesParamsWithContext(ctx context.Context) *GetSearchIssuesParams {
	var (
		orderDefault = string("desc")
	)
	return &GetSearchIssuesParams{
		Order: &orderDefault,

		Context: ctx,
	}
}

// NewGetSearchIssuesParamsWithHTTPClient creates a new GetSearchIssuesParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetSearchIssuesParamsWithHTTPClient(client *http.Client) *GetSearchIssuesParams {
	var (
		orderDefault = string("desc")
	)
	return &GetSearchIssuesParams{
		Order:      &orderDefault,
		HTTPClient: client,
	}
}

/*GetSearchIssuesParams contains all the parameters to send to the API endpoint
for the get search issues operation typically these are written to a http.Request
*/
type GetSearchIssuesParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Order
	  The sort field. if sort param is provided. Can be either asc or desc.

	*/
	Order *string
	/*Q
	  The q search term can also contain any combination of the supported issue search qualifiers:

	*/
	Q string
	/*Sort
	  The sort field. Can be comments, created, or updated. Default: results are sorted by best match.

	*/
	Sort *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get search issues params
func (o *GetSearchIssuesParams) WithTimeout(timeout time.Duration) *GetSearchIssuesParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get search issues params
func (o *GetSearchIssuesParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get search issues params
func (o *GetSearchIssuesParams) WithContext(ctx context.Context) *GetSearchIssuesParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get search issues params
func (o *GetSearchIssuesParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get search issues params
func (o *GetSearchIssuesParams) WithHTTPClient(client *http.Client) *GetSearchIssuesParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get search issues params
func (o *GetSearchIssuesParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get search issues params
func (o *GetSearchIssuesParams) WithAccept(accept *string) *GetSearchIssuesParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get search issues params
func (o *GetSearchIssuesParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithOrder adds the order to the get search issues params
func (o *GetSearchIssuesParams) WithOrder(order *string) *GetSearchIssuesParams {
	o.SetOrder(order)
	return o
}

// SetOrder adds the order to the get search issues params
func (o *GetSearchIssuesParams) SetOrder(order *string) {
	o.Order = order
}

// WithQ adds the q to the get search issues params
func (o *GetSearchIssuesParams) WithQ(q string) *GetSearchIssuesParams {
	o.SetQ(q)
	return o
}

// SetQ adds the q to the get search issues params
func (o *GetSearchIssuesParams) SetQ(q string) {
	o.Q = q
}

// WithSort adds the sort to the get search issues params
func (o *GetSearchIssuesParams) WithSort(sort *string) *GetSearchIssuesParams {
	o.SetSort(sort)
	return o
}

// SetSort adds the sort to the get search issues params
func (o *GetSearchIssuesParams) SetSort(sort *string) {
	o.Sort = sort
}

// WriteToRequest writes these params to a swagger request
func (o *GetSearchIssuesParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	if o.Order != nil {

		// query param order
		var qrOrder string
		if o.Order != nil {
			qrOrder = *o.Order
		}
		qOrder := qrOrder
		if qOrder != "" {
			if err := r.SetQueryParam("order", qOrder); err != nil {
				return err
			}
		}

	}

	// query param q
	qrQ := o.Q
	qQ := qrQ
	if qQ != "" {
		if err := r.SetQueryParam("q", qQ); err != nil {
			return err
		}
	}

	if o.Sort != nil {

		// query param sort
		var qrSort string
		if o.Sort != nil {
			qrSort = *o.Sort
		}
		qSort := qrSort
		if qSort != "" {
			if err := r.SetQueryParam("sort", qSort); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
