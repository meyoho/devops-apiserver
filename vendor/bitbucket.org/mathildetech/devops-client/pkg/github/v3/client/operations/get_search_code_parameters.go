// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetSearchCodeParams creates a new GetSearchCodeParams object
// with the default values initialized.
func NewGetSearchCodeParams() *GetSearchCodeParams {
	var (
		orderDefault = string("desc")
	)
	return &GetSearchCodeParams{
		Order: &orderDefault,

		timeout: cr.DefaultTimeout,
	}
}

// NewGetSearchCodeParamsWithTimeout creates a new GetSearchCodeParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetSearchCodeParamsWithTimeout(timeout time.Duration) *GetSearchCodeParams {
	var (
		orderDefault = string("desc")
	)
	return &GetSearchCodeParams{
		Order: &orderDefault,

		timeout: timeout,
	}
}

// NewGetSearchCodeParamsWithContext creates a new GetSearchCodeParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetSearchCodeParamsWithContext(ctx context.Context) *GetSearchCodeParams {
	var (
		orderDefault = string("desc")
	)
	return &GetSearchCodeParams{
		Order: &orderDefault,

		Context: ctx,
	}
}

// NewGetSearchCodeParamsWithHTTPClient creates a new GetSearchCodeParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetSearchCodeParamsWithHTTPClient(client *http.Client) *GetSearchCodeParams {
	var (
		orderDefault = string("desc")
	)
	return &GetSearchCodeParams{
		Order:      &orderDefault,
		HTTPClient: client,
	}
}

/*GetSearchCodeParams contains all the parameters to send to the API endpoint
for the get search code operation typically these are written to a http.Request
*/
type GetSearchCodeParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Order
	  The sort field. if sort param is provided. Can be either asc or desc.

	*/
	Order *string
	/*Q
	  The search terms. This can be any combination of the supported code
	search parameters:
	'Search In' Qualifies which fields are searched. With this qualifier
	you can restrict the search to just the file contents, the file path,
	or both.
	'Languages' Searches code based on the language it's written in.
	'Forks' Filters repositories based on the number of forks, and/or
	whether code from forked repositories should be included in the results
	at all.
	'Size' Finds files that match a certain size (in bytes).
	'Path' Specifies the path that the resulting file must be at.
	'Extension' Matches files with a certain extension.
	'Users' or 'Repositories' Limits searches to a specific user or repository.


	*/
	Q string
	/*Sort
	  Can only be 'indexed', which indicates how recently a file has been indexed
	by the GitHub search infrastructure. If not provided, results are sorted
	by best match.


	*/
	Sort *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get search code params
func (o *GetSearchCodeParams) WithTimeout(timeout time.Duration) *GetSearchCodeParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get search code params
func (o *GetSearchCodeParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get search code params
func (o *GetSearchCodeParams) WithContext(ctx context.Context) *GetSearchCodeParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get search code params
func (o *GetSearchCodeParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get search code params
func (o *GetSearchCodeParams) WithHTTPClient(client *http.Client) *GetSearchCodeParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get search code params
func (o *GetSearchCodeParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get search code params
func (o *GetSearchCodeParams) WithAccept(accept *string) *GetSearchCodeParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get search code params
func (o *GetSearchCodeParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithOrder adds the order to the get search code params
func (o *GetSearchCodeParams) WithOrder(order *string) *GetSearchCodeParams {
	o.SetOrder(order)
	return o
}

// SetOrder adds the order to the get search code params
func (o *GetSearchCodeParams) SetOrder(order *string) {
	o.Order = order
}

// WithQ adds the q to the get search code params
func (o *GetSearchCodeParams) WithQ(q string) *GetSearchCodeParams {
	o.SetQ(q)
	return o
}

// SetQ adds the q to the get search code params
func (o *GetSearchCodeParams) SetQ(q string) {
	o.Q = q
}

// WithSort adds the sort to the get search code params
func (o *GetSearchCodeParams) WithSort(sort *string) *GetSearchCodeParams {
	o.SetSort(sort)
	return o
}

// SetSort adds the sort to the get search code params
func (o *GetSearchCodeParams) SetSort(sort *string) {
	o.Sort = sort
}

// WriteToRequest writes these params to a swagger request
func (o *GetSearchCodeParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	if o.Order != nil {

		// query param order
		var qrOrder string
		if o.Order != nil {
			qrOrder = *o.Order
		}
		qOrder := qrOrder
		if qOrder != "" {
			if err := r.SetQueryParam("order", qOrder); err != nil {
				return err
			}
		}

	}

	// query param q
	qrQ := o.Q
	qQ := qrQ
	if qQ != "" {
		if err := r.SetQueryParam("q", qQ); err != nil {
			return err
		}
	}

	if o.Sort != nil {

		// query param sort
		var qrSort string
		if o.Sort != nil {
			qrSort = *o.Sort
		}
		qSort := qrSort
		if qSort != "" {
			if err := r.SetQueryParam("sort", qSort); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
