// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetUsersUsernameEventsOrgsOrgParams creates a new GetUsersUsernameEventsOrgsOrgParams object
// with the default values initialized.
func NewGetUsersUsernameEventsOrgsOrgParams() *GetUsersUsernameEventsOrgsOrgParams {
	var ()
	return &GetUsersUsernameEventsOrgsOrgParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetUsersUsernameEventsOrgsOrgParamsWithTimeout creates a new GetUsersUsernameEventsOrgsOrgParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetUsersUsernameEventsOrgsOrgParamsWithTimeout(timeout time.Duration) *GetUsersUsernameEventsOrgsOrgParams {
	var ()
	return &GetUsersUsernameEventsOrgsOrgParams{

		timeout: timeout,
	}
}

// NewGetUsersUsernameEventsOrgsOrgParamsWithContext creates a new GetUsersUsernameEventsOrgsOrgParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetUsersUsernameEventsOrgsOrgParamsWithContext(ctx context.Context) *GetUsersUsernameEventsOrgsOrgParams {
	var ()
	return &GetUsersUsernameEventsOrgsOrgParams{

		Context: ctx,
	}
}

// NewGetUsersUsernameEventsOrgsOrgParamsWithHTTPClient creates a new GetUsersUsernameEventsOrgsOrgParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetUsersUsernameEventsOrgsOrgParamsWithHTTPClient(client *http.Client) *GetUsersUsernameEventsOrgsOrgParams {
	var ()
	return &GetUsersUsernameEventsOrgsOrgParams{
		HTTPClient: client,
	}
}

/*GetUsersUsernameEventsOrgsOrgParams contains all the parameters to send to the API endpoint
for the get users username events orgs org operation typically these are written to a http.Request
*/
type GetUsersUsernameEventsOrgsOrgParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Org*/
	Org string
	/*Username
	  Name of user.

	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) WithTimeout(timeout time.Duration) *GetUsersUsernameEventsOrgsOrgParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) WithContext(ctx context.Context) *GetUsersUsernameEventsOrgsOrgParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) WithHTTPClient(client *http.Client) *GetUsersUsernameEventsOrgsOrgParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) WithAccept(accept *string) *GetUsersUsernameEventsOrgsOrgParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithOrg adds the org to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) WithOrg(org string) *GetUsersUsernameEventsOrgsOrgParams {
	o.SetOrg(org)
	return o
}

// SetOrg adds the org to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) SetOrg(org string) {
	o.Org = org
}

// WithUsername adds the username to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) WithUsername(username string) *GetUsersUsernameEventsOrgsOrgParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get users username events orgs org params
func (o *GetUsersUsernameEventsOrgsOrgParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetUsersUsernameEventsOrgsOrgParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	// path param org
	if err := r.SetPathParam("org", o.Org); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
