// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/github/v3/models"
)

// PatchReposOwnerRepoIssuesNumberReader is a Reader for the PatchReposOwnerRepoIssuesNumber structure.
type PatchReposOwnerRepoIssuesNumberReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PatchReposOwnerRepoIssuesNumberReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPatchReposOwnerRepoIssuesNumberOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewPatchReposOwnerRepoIssuesNumberForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPatchReposOwnerRepoIssuesNumberOK creates a PatchReposOwnerRepoIssuesNumberOK with default headers values
func NewPatchReposOwnerRepoIssuesNumberOK() *PatchReposOwnerRepoIssuesNumberOK {
	return &PatchReposOwnerRepoIssuesNumberOK{}
}

/*PatchReposOwnerRepoIssuesNumberOK handles this case with default header values.

OK
*/
type PatchReposOwnerRepoIssuesNumberOK struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64

	Payload *models.Issue
}

func (o *PatchReposOwnerRepoIssuesNumberOK) Error() string {
	return fmt.Sprintf("[PATCH /repos/{owner}/{repo}/issues/{number}][%d] patchReposOwnerRepoIssuesNumberOK  %+v", 200, o.Payload)
}

func (o *PatchReposOwnerRepoIssuesNumberOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	o.Payload = new(models.Issue)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPatchReposOwnerRepoIssuesNumberForbidden creates a PatchReposOwnerRepoIssuesNumberForbidden with default headers values
func NewPatchReposOwnerRepoIssuesNumberForbidden() *PatchReposOwnerRepoIssuesNumberForbidden {
	return &PatchReposOwnerRepoIssuesNumberForbidden{}
}

/*PatchReposOwnerRepoIssuesNumberForbidden handles this case with default header values.

API rate limit exceeded. See http://developer.github.com/v3/#rate-limiting
for details.

*/
type PatchReposOwnerRepoIssuesNumberForbidden struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64
}

func (o *PatchReposOwnerRepoIssuesNumberForbidden) Error() string {
	return fmt.Sprintf("[PATCH /repos/{owner}/{repo}/issues/{number}][%d] patchReposOwnerRepoIssuesNumberForbidden ", 403)
}

func (o *PatchReposOwnerRepoIssuesNumberForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	return nil
}
