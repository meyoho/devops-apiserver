// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/github/v3/models"
)

// PutReposOwnerRepoPullsNumberMergeReader is a Reader for the PutReposOwnerRepoPullsNumberMerge structure.
type PutReposOwnerRepoPullsNumberMergeReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PutReposOwnerRepoPullsNumberMergeReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPutReposOwnerRepoPullsNumberMergeOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewPutReposOwnerRepoPullsNumberMergeForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	case 405:
		result := NewPutReposOwnerRepoPullsNumberMergeMethodNotAllowed()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPutReposOwnerRepoPullsNumberMergeOK creates a PutReposOwnerRepoPullsNumberMergeOK with default headers values
func NewPutReposOwnerRepoPullsNumberMergeOK() *PutReposOwnerRepoPullsNumberMergeOK {
	return &PutReposOwnerRepoPullsNumberMergeOK{}
}

/*PutReposOwnerRepoPullsNumberMergeOK handles this case with default header values.

Response if merge was successful.
*/
type PutReposOwnerRepoPullsNumberMergeOK struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64

	Payload *models.Merge
}

func (o *PutReposOwnerRepoPullsNumberMergeOK) Error() string {
	return fmt.Sprintf("[PUT /repos/{owner}/{repo}/pulls/{number}/merge][%d] putReposOwnerRepoPullsNumberMergeOK  %+v", 200, o.Payload)
}

func (o *PutReposOwnerRepoPullsNumberMergeOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	o.Payload = new(models.Merge)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPutReposOwnerRepoPullsNumberMergeForbidden creates a PutReposOwnerRepoPullsNumberMergeForbidden with default headers values
func NewPutReposOwnerRepoPullsNumberMergeForbidden() *PutReposOwnerRepoPullsNumberMergeForbidden {
	return &PutReposOwnerRepoPullsNumberMergeForbidden{}
}

/*PutReposOwnerRepoPullsNumberMergeForbidden handles this case with default header values.

API rate limit exceeded. See http://developer.github.com/v3/#rate-limiting
for details.

*/
type PutReposOwnerRepoPullsNumberMergeForbidden struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64
}

func (o *PutReposOwnerRepoPullsNumberMergeForbidden) Error() string {
	return fmt.Sprintf("[PUT /repos/{owner}/{repo}/pulls/{number}/merge][%d] putReposOwnerRepoPullsNumberMergeForbidden ", 403)
}

func (o *PutReposOwnerRepoPullsNumberMergeForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	return nil
}

// NewPutReposOwnerRepoPullsNumberMergeMethodNotAllowed creates a PutReposOwnerRepoPullsNumberMergeMethodNotAllowed with default headers values
func NewPutReposOwnerRepoPullsNumberMergeMethodNotAllowed() *PutReposOwnerRepoPullsNumberMergeMethodNotAllowed {
	return &PutReposOwnerRepoPullsNumberMergeMethodNotAllowed{}
}

/*PutReposOwnerRepoPullsNumberMergeMethodNotAllowed handles this case with default header values.

Response if merge cannot be performed.
*/
type PutReposOwnerRepoPullsNumberMergeMethodNotAllowed struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64

	Payload *models.Merge
}

func (o *PutReposOwnerRepoPullsNumberMergeMethodNotAllowed) Error() string {
	return fmt.Sprintf("[PUT /repos/{owner}/{repo}/pulls/{number}/merge][%d] putReposOwnerRepoPullsNumberMergeMethodNotAllowed  %+v", 405, o.Payload)
}

func (o *PutReposOwnerRepoPullsNumberMergeMethodNotAllowed) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	o.Payload = new(models.Merge)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
