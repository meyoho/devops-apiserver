// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewDeleteReposOwnerRepoGitRefsRefParams creates a new DeleteReposOwnerRepoGitRefsRefParams object
// with the default values initialized.
func NewDeleteReposOwnerRepoGitRefsRefParams() *DeleteReposOwnerRepoGitRefsRefParams {
	var ()
	return &DeleteReposOwnerRepoGitRefsRefParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewDeleteReposOwnerRepoGitRefsRefParamsWithTimeout creates a new DeleteReposOwnerRepoGitRefsRefParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewDeleteReposOwnerRepoGitRefsRefParamsWithTimeout(timeout time.Duration) *DeleteReposOwnerRepoGitRefsRefParams {
	var ()
	return &DeleteReposOwnerRepoGitRefsRefParams{

		timeout: timeout,
	}
}

// NewDeleteReposOwnerRepoGitRefsRefParamsWithContext creates a new DeleteReposOwnerRepoGitRefsRefParams object
// with the default values initialized, and the ability to set a context for a request
func NewDeleteReposOwnerRepoGitRefsRefParamsWithContext(ctx context.Context) *DeleteReposOwnerRepoGitRefsRefParams {
	var ()
	return &DeleteReposOwnerRepoGitRefsRefParams{

		Context: ctx,
	}
}

// NewDeleteReposOwnerRepoGitRefsRefParamsWithHTTPClient creates a new DeleteReposOwnerRepoGitRefsRefParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewDeleteReposOwnerRepoGitRefsRefParamsWithHTTPClient(client *http.Client) *DeleteReposOwnerRepoGitRefsRefParams {
	var ()
	return &DeleteReposOwnerRepoGitRefsRefParams{
		HTTPClient: client,
	}
}

/*DeleteReposOwnerRepoGitRefsRefParams contains all the parameters to send to the API endpoint
for the delete repos owner repo git refs ref operation typically these are written to a http.Request
*/
type DeleteReposOwnerRepoGitRefsRefParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Owner
	  Name of repository owner.

	*/
	Owner string
	/*Ref*/
	Ref string
	/*Repo
	  Name of repository.

	*/
	Repo string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) WithTimeout(timeout time.Duration) *DeleteReposOwnerRepoGitRefsRefParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) WithContext(ctx context.Context) *DeleteReposOwnerRepoGitRefsRefParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) WithHTTPClient(client *http.Client) *DeleteReposOwnerRepoGitRefsRefParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) WithAccept(accept *string) *DeleteReposOwnerRepoGitRefsRefParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithOwner adds the owner to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) WithOwner(owner string) *DeleteReposOwnerRepoGitRefsRefParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithRef adds the ref to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) WithRef(ref string) *DeleteReposOwnerRepoGitRefsRefParams {
	o.SetRef(ref)
	return o
}

// SetRef adds the ref to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) SetRef(ref string) {
	o.Ref = ref
}

// WithRepo adds the repo to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) WithRepo(repo string) *DeleteReposOwnerRepoGitRefsRefParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the delete repos owner repo git refs ref params
func (o *DeleteReposOwnerRepoGitRefsRefParams) SetRepo(repo string) {
	o.Repo = repo
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteReposOwnerRepoGitRefsRefParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	// path param ref
	if err := r.SetPathParam("ref", o.Ref); err != nil {
		return err
	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
