// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetUserSubscriptionsParams creates a new GetUserSubscriptionsParams object
// with the default values initialized.
func NewGetUserSubscriptionsParams() *GetUserSubscriptionsParams {
	var ()
	return &GetUserSubscriptionsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetUserSubscriptionsParamsWithTimeout creates a new GetUserSubscriptionsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetUserSubscriptionsParamsWithTimeout(timeout time.Duration) *GetUserSubscriptionsParams {
	var ()
	return &GetUserSubscriptionsParams{

		timeout: timeout,
	}
}

// NewGetUserSubscriptionsParamsWithContext creates a new GetUserSubscriptionsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetUserSubscriptionsParamsWithContext(ctx context.Context) *GetUserSubscriptionsParams {
	var ()
	return &GetUserSubscriptionsParams{

		Context: ctx,
	}
}

// NewGetUserSubscriptionsParamsWithHTTPClient creates a new GetUserSubscriptionsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetUserSubscriptionsParamsWithHTTPClient(client *http.Client) *GetUserSubscriptionsParams {
	var ()
	return &GetUserSubscriptionsParams{
		HTTPClient: client,
	}
}

/*GetUserSubscriptionsParams contains all the parameters to send to the API endpoint
for the get user subscriptions operation typically these are written to a http.Request
*/
type GetUserSubscriptionsParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get user subscriptions params
func (o *GetUserSubscriptionsParams) WithTimeout(timeout time.Duration) *GetUserSubscriptionsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get user subscriptions params
func (o *GetUserSubscriptionsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get user subscriptions params
func (o *GetUserSubscriptionsParams) WithContext(ctx context.Context) *GetUserSubscriptionsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get user subscriptions params
func (o *GetUserSubscriptionsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get user subscriptions params
func (o *GetUserSubscriptionsParams) WithHTTPClient(client *http.Client) *GetUserSubscriptionsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get user subscriptions params
func (o *GetUserSubscriptionsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get user subscriptions params
func (o *GetUserSubscriptionsParams) WithAccept(accept *string) *GetUserSubscriptionsParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get user subscriptions params
func (o *GetUserSubscriptionsParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WriteToRequest writes these params to a swagger request
func (o *GetUserSubscriptionsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
