// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/github/v3/models"
)

// PostGistsIDCommentsReader is a Reader for the PostGistsIDComments structure.
type PostGistsIDCommentsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PostGistsIDCommentsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 201:
		result := NewPostGistsIDCommentsCreated()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 403:
		result := NewPostGistsIDCommentsForbidden()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPostGistsIDCommentsCreated creates a PostGistsIDCommentsCreated with default headers values
func NewPostGistsIDCommentsCreated() *PostGistsIDCommentsCreated {
	return &PostGistsIDCommentsCreated{}
}

/*PostGistsIDCommentsCreated handles this case with default header values.

Created
*/
type PostGistsIDCommentsCreated struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64

	Payload *models.Comment
}

func (o *PostGistsIDCommentsCreated) Error() string {
	return fmt.Sprintf("[POST /gists/{id}/comments][%d] postGistsIdCommentsCreated  %+v", 201, o.Payload)
}

func (o *PostGistsIDCommentsCreated) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	o.Payload = new(models.Comment)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewPostGistsIDCommentsForbidden creates a PostGistsIDCommentsForbidden with default headers values
func NewPostGistsIDCommentsForbidden() *PostGistsIDCommentsForbidden {
	return &PostGistsIDCommentsForbidden{}
}

/*PostGistsIDCommentsForbidden handles this case with default header values.

API rate limit exceeded. See http://developer.github.com/v3/#rate-limiting
for details.

*/
type PostGistsIDCommentsForbidden struct {
	/*You can check the current version of media type in responses.

	 */
	XGitHubMediaType string

	XGitHubRequestID string

	XRateLimitLimit int64

	XRateLimitRemaining int64

	XRateLimitReset int64
}

func (o *PostGistsIDCommentsForbidden) Error() string {
	return fmt.Sprintf("[POST /gists/{id}/comments][%d] postGistsIdCommentsForbidden ", 403)
}

func (o *PostGistsIDCommentsForbidden) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response header X-GitHub-Media-Type
	o.XGitHubMediaType = response.GetHeader("X-GitHub-Media-Type")

	// response header X-GitHub-Request-Id
	o.XGitHubRequestID = response.GetHeader("X-GitHub-Request-Id")

	// response header X-RateLimit-Limit
	xRateLimitLimit, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Limit"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Limit", "header", "int64", response.GetHeader("X-RateLimit-Limit"))
	}
	o.XRateLimitLimit = xRateLimitLimit

	// response header X-RateLimit-Remaining
	xRateLimitRemaining, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Remaining"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Remaining", "header", "int64", response.GetHeader("X-RateLimit-Remaining"))
	}
	o.XRateLimitRemaining = xRateLimitRemaining

	// response header X-RateLimit-Reset
	xRateLimitReset, err := swag.ConvertInt64(response.GetHeader("X-RateLimit-Reset"))
	if err != nil {
		return errors.InvalidType("X-RateLimit-Reset", "header", "int64", response.GetHeader("X-RateLimit-Reset"))
	}
	o.XRateLimitReset = xRateLimitReset

	return nil
}
