// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostMarkdownRawParams creates a new PostMarkdownRawParams object
// with the default values initialized.
func NewPostMarkdownRawParams() *PostMarkdownRawParams {
	var ()
	return &PostMarkdownRawParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostMarkdownRawParamsWithTimeout creates a new PostMarkdownRawParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostMarkdownRawParamsWithTimeout(timeout time.Duration) *PostMarkdownRawParams {
	var ()
	return &PostMarkdownRawParams{

		timeout: timeout,
	}
}

// NewPostMarkdownRawParamsWithContext creates a new PostMarkdownRawParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostMarkdownRawParamsWithContext(ctx context.Context) *PostMarkdownRawParams {
	var ()
	return &PostMarkdownRawParams{

		Context: ctx,
	}
}

// NewPostMarkdownRawParamsWithHTTPClient creates a new PostMarkdownRawParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostMarkdownRawParamsWithHTTPClient(client *http.Client) *PostMarkdownRawParams {
	var ()
	return &PostMarkdownRawParams{
		HTTPClient: client,
	}
}

/*PostMarkdownRawParams contains all the parameters to send to the API endpoint
for the post markdown raw operation typically these are written to a http.Request
*/
type PostMarkdownRawParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post markdown raw params
func (o *PostMarkdownRawParams) WithTimeout(timeout time.Duration) *PostMarkdownRawParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post markdown raw params
func (o *PostMarkdownRawParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post markdown raw params
func (o *PostMarkdownRawParams) WithContext(ctx context.Context) *PostMarkdownRawParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post markdown raw params
func (o *PostMarkdownRawParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post markdown raw params
func (o *PostMarkdownRawParams) WithHTTPClient(client *http.Client) *PostMarkdownRawParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post markdown raw params
func (o *PostMarkdownRawParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the post markdown raw params
func (o *PostMarkdownRawParams) WithAccept(accept *string) *PostMarkdownRawParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the post markdown raw params
func (o *PostMarkdownRawParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WriteToRequest writes these params to a swagger request
func (o *PostMarkdownRawParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
