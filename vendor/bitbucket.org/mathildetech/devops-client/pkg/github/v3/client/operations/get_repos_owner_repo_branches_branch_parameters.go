// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetReposOwnerRepoBranchesBranchParams creates a new GetReposOwnerRepoBranchesBranchParams object
// with the default values initialized.
func NewGetReposOwnerRepoBranchesBranchParams() *GetReposOwnerRepoBranchesBranchParams {
	var ()
	return &GetReposOwnerRepoBranchesBranchParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetReposOwnerRepoBranchesBranchParamsWithTimeout creates a new GetReposOwnerRepoBranchesBranchParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetReposOwnerRepoBranchesBranchParamsWithTimeout(timeout time.Duration) *GetReposOwnerRepoBranchesBranchParams {
	var ()
	return &GetReposOwnerRepoBranchesBranchParams{

		timeout: timeout,
	}
}

// NewGetReposOwnerRepoBranchesBranchParamsWithContext creates a new GetReposOwnerRepoBranchesBranchParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetReposOwnerRepoBranchesBranchParamsWithContext(ctx context.Context) *GetReposOwnerRepoBranchesBranchParams {
	var ()
	return &GetReposOwnerRepoBranchesBranchParams{

		Context: ctx,
	}
}

// NewGetReposOwnerRepoBranchesBranchParamsWithHTTPClient creates a new GetReposOwnerRepoBranchesBranchParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetReposOwnerRepoBranchesBranchParamsWithHTTPClient(client *http.Client) *GetReposOwnerRepoBranchesBranchParams {
	var ()
	return &GetReposOwnerRepoBranchesBranchParams{
		HTTPClient: client,
	}
}

/*GetReposOwnerRepoBranchesBranchParams contains all the parameters to send to the API endpoint
for the get repos owner repo branches branch operation typically these are written to a http.Request
*/
type GetReposOwnerRepoBranchesBranchParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Branch
	  Name of the branch.

	*/
	Branch string
	/*Owner
	  Name of repository owner.

	*/
	Owner string
	/*Repo
	  Name of repository.

	*/
	Repo string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) WithTimeout(timeout time.Duration) *GetReposOwnerRepoBranchesBranchParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) WithContext(ctx context.Context) *GetReposOwnerRepoBranchesBranchParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) WithHTTPClient(client *http.Client) *GetReposOwnerRepoBranchesBranchParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) WithAccept(accept *string) *GetReposOwnerRepoBranchesBranchParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithBranch adds the branch to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) WithBranch(branch string) *GetReposOwnerRepoBranchesBranchParams {
	o.SetBranch(branch)
	return o
}

// SetBranch adds the branch to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) SetBranch(branch string) {
	o.Branch = branch
}

// WithOwner adds the owner to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) WithOwner(owner string) *GetReposOwnerRepoBranchesBranchParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithRepo adds the repo to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) WithRepo(repo string) *GetReposOwnerRepoBranchesBranchParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the get repos owner repo branches branch params
func (o *GetReposOwnerRepoBranchesBranchParams) SetRepo(repo string) {
	o.Repo = repo
}

// WriteToRequest writes these params to a swagger request
func (o *GetReposOwnerRepoBranchesBranchParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	// path param branch
	if err := r.SetPathParam("branch", o.Branch); err != nil {
		return err
	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
