// Code generated
package v3

import (
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/go-logr/logr"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bitbucket.org/mathildetech/devops-client/pkg/generic"
	"bitbucket.org/mathildetech/devops-client/pkg/github/v3/client"
	"bitbucket.org/mathildetech/devops-client/pkg/github/v3/client/operations"
	"bitbucket.org/mathildetech/devops-client/pkg/github/v3/models"
	"bitbucket.org/mathildetech/devops-client/pkg/transport"
	"github.com/dustin/go-humanize"
	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	v1.NotImplement
	logger     logr.Logger
	client     *client.Github
	opts       *v1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ v1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() v1.ClientFactory {
	return func(opts *v1.Options) v1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

func (c *Client) listAllRepos(ctx context.Context) ([]*models.Repo, error) {
	var (
		page     = int32(0)
		pageSize = int32(100)
		repos    = []*models.Repo{}
	)

	for {
		page = page + 1
		reposParam := operations.
			NewGetUserReposParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithPage(&page).
			WithPerPage(&pageSize)
		reposOK, err := c.client.Operations.GetUserRepos(reposParam, c.authInfo)
		if err != nil {
			return nil, err
		}

		repos = append(repos, reposOK.Payload...)

		if len(reposOK.Payload) < int(pageSize) {
			break
		}
	}

	return repos, nil
}

func (c *Client) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}) (codeRepo v1.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if githubRepo, ok := remoteRepo.(*models.Repo); ok {
		codeRepo = v1.OriginCodeRepository{
			CodeRepoServiceType: v1.CodeRepoServiceTypeGithub,
			ID:                  strconv.FormatInt(githubRepo.ID, 10),
			Name:                githubRepo.Name,
			FullName:            githubRepo.FullName,
			Description:         githubRepo.Description,
			HTMLURL:             githubRepo.HTMLURL,
			CloneURL:            githubRepo.CloneURL,
			SSHURL:              githubRepo.SSHURL,
			Language:            githubRepo.Language,
			Owner: v1.OwnerInRepository{
				ID:   strconv.FormatInt(githubRepo.Owner.ID, 10),
				Name: githubRepo.Owner.Login,
				Type: v1.GetOwnerType(githubRepo.Owner.Type.(string)),
			},

			CreatedAt: generic.ConvertStringToTime(githubRepo.CreatedAt),
			PushedAt:  generic.ConvertStringToTime(githubRepo.PushedAt),
			UpdatedAt: generic.ConvertStringToTime(githubRepo.UpdatedAt),

			Private:      githubRepo.Private,
			Size:         int64(githubRepo.Size * 1000),
			SizeHumanize: humanize.Bytes(uint64(githubRepo.Size * 1000)),
		}
	}

	return
}

// GetRemoteRepos implements CodeRepoService
func (c *Client) GetRemoteRepos(ctx context.Context) (*v1.CodeRepoBindingRepositories, error) {
	repos, err := c.listAllRepos(ctx)
	if err != nil {
		return nil, err
	}

	dictOwners := make(map[string]*v1.CodeRepositoryOwner)
	for _, repo := range repos {
		ownerKey := repo.Owner.Login

		if owner, ok := dictOwners[ownerKey]; !ok {
			owner = &v1.CodeRepositoryOwner{
				Type:         v1.GetOwnerType(repo.Owner.Type.(string)),
				ID:           strconv.FormatInt(repo.Owner.ID, 10),
				Name:         repo.Owner.Login,
				Email:        repo.Owner.Email,
				HTMLURL:      repo.Owner.HTMLURL,
				AvatarURL:    repo.Owner.AvatarURL,
				DiskUsage:    int(repo.Owner.DiskUsage),
				Repositories: make([]v1.OriginCodeRepository, 0, 10),
			}
			dictOwners[ownerKey] = owner
		}
		codeRepo := c.ConvertRemoteRepoToBindingRepo(repo)
		dictOwners[ownerKey].Repositories = append(dictOwners[ownerKey].Repositories, codeRepo)
	}

	result := &v1.CodeRepoBindingRepositories{
		Type:   v1.CodeRepoServiceTypeGithub,
		Owners: []v1.CodeRepositoryOwner{},
	}

	for _, owner := range dictOwners {
		if len(owner.Repositories) > 0 {
			result.Owners = append(result.Owners, *owner)
		}
	}

	return result, nil

}

// GetBranches implements CodeRepoService
func (c *Client) GetBranches(ctx context.Context, owner, repo, repoFullName string) ([]v1.CodeRepoBranch, error) {
	branchesParam := operations.
		NewGetReposOwnerRepoBranchesParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithOwner(owner).
		WithRepo(repo)
	branchesOK, err := c.client.Operations.GetReposOwnerRepoBranches(branchesParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := make([]v1.CodeRepoBranch, 0, len(branchesOK.Payload))
	for _, branch := range branchesOK.Payload {
		result = append(result, v1.CodeRepoBranch{
			Name:   branch.Name,
			Commit: branch.Commit.Sha,
		})
	}

	return result, nil
}

// CreateCodeRepoProject implements CodeRepoService
func (c *Client) CreateCodeRepoProject(_ context.Context, _ v1.CreateProjectOptions) (*v1.ProjectData, error) {
	return nil, errors.New("not supported")
}

func githubOrgAsProjectData(org models.Organization) (*v1.ProjectData, error) {
	data, err := generic.MarshalToMapString(org)
	if err != nil {
		return nil, err
	}

	projectData := &v1.ProjectData{
		Name: org.Login,
		Annotations: map[string]string{
			"avatarURL":   org.AvaterURL,
			"accessPath":  "/" + org.Login,
			"description": org.Description,
			"type":        v1.OriginCodeRepoRoleTypeOrg,
		},
		Data: data,
	}

	return projectData, nil
}

func githubUserAsProjectData(user *models.User) (*v1.ProjectData, error) {
	var projectData = &v1.ProjectData{
		Name: user.Login,
		Annotations: map[string]string{
			"avatarURL":   user.AvatarURL,
			"webURL":      "/" + user.Login,
			"description": "",
			"type":        string(v1.OriginCodeRepoOwnerTypeUser),
		},
		Data: map[string]string{
			"login":   user.Login,
			"id":      strconv.Itoa(int(user.ID)),
			"name":    user.Name,
			"company": user.Company,
			"type":    user.Type,
			"email":   user.Email,
		},
	}
	return projectData, nil
}

func (c *Client) currentUser(ctx context.Context) (*v1.ProjectData, error) {
	userParams := operations.
		NewGetUserParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	userOK, err := c.client.Operations.GetUser(userParams, c.authInfo)
	if err != nil {
		return nil, err
	}
	return githubUserAsProjectData(userOK.Payload)
}

// ListCodeRepoProjects implements CodeRepoService
func (c *Client) ListCodeRepoProjects(ctx context.Context, opts v1.ListProjectOptions) (*v1.ProjectDataList, error) {
	orgsParam := operations.
		NewGetUserOrgsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	orgsOK, err := c.client.Operations.GetUserOrgs(orgsParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	items := make([]v1.ProjectData, 0, len(orgsOK.Payload))
	for _, org := range orgsOK.Payload {
		projectData, err := githubOrgAsProjectData(*org)
		if err != nil {
			return nil, err
		}

		items = append(items, *projectData)
	}

	userProject, err := c.currentUser(ctx)
	if err != nil {
		return nil, err
	}

	items = append(items, *userProject)

	result := &v1.ProjectDataList{
		Items: items,
	}

	return result, nil
}

// GetLatestRepoCommit implements CodeRepoService
func (c *Client) GetLatestRepoCommit(ctx context.Context, repoID, owner, repoName, repoFullName string) (commit *v1.RepositoryCommit, status *v1.HostPortStatus) {
	params := operations.
		NewGetReposOwnerRepoCommitsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithOwner(owner).
		WithRepo(repoName)
	payload, err := c.client.Operations.GetReposOwnerRepoCommits(params, c.authInfo)
	if err != nil {
		return nil, &v1.HostPortStatus{
			StatusCode: 500,
			Response:   err.Error(),
		}
	}

	if len(payload.Payload) > 0 {
		commitAt, _ := time.Parse(time.RFC3339Nano, payload.Payload[0].Commit.Committer.Date)
		commit = &v1.RepositoryCommit{
			CommitID:       payload.Payload[0].Sha,
			CommitAt:       commitAt,
			CommitterName:  payload.Payload[0].Commit.Committer.Name,
			CommitterEmail: payload.Payload[0].Commit.Committer.Email,
			CommitMessage:  payload.Payload[0].Commit.Message,
		}
	}
	return commit, &v1.HostPortStatus{
		StatusCode:  200,
		LastAttempt: time.Now(),
	}
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		return generic.CheckService(c.opts.BasicConfig.Host, nil)
	}
	return nil, errors.New("host config error")
}
