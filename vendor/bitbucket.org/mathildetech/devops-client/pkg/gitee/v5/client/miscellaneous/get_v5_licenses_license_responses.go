// Code generated by go-swagger; DO NOT EDIT.

package miscellaneous

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// GetV5LicensesLicenseReader is a Reader for the GetV5LicensesLicense structure.
type GetV5LicensesLicenseReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV5LicensesLicenseReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV5LicensesLicenseOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV5LicensesLicenseOK creates a GetV5LicensesLicenseOK with default headers values
func NewGetV5LicensesLicenseOK() *GetV5LicensesLicenseOK {
	return &GetV5LicensesLicenseOK{}
}

/*GetV5LicensesLicenseOK handles this case with default header values.

获取一个开源许可协议
*/
type GetV5LicensesLicenseOK struct {
}

func (o *GetV5LicensesLicenseOK) Error() string {
	return fmt.Sprintf("[GET /v5/licenses/{license}][%d] getV5LicensesLicenseOK ", 200)
}

func (o *GetV5LicensesLicenseOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
