// Code generated by go-swagger; DO NOT EDIT.

package gists

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostV5GistsGistIDCommentsParams creates a new PostV5GistsGistIDCommentsParams object
// with the default values initialized.
func NewPostV5GistsGistIDCommentsParams() *PostV5GistsGistIDCommentsParams {
	var ()
	return &PostV5GistsGistIDCommentsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostV5GistsGistIDCommentsParamsWithTimeout creates a new PostV5GistsGistIDCommentsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostV5GistsGistIDCommentsParamsWithTimeout(timeout time.Duration) *PostV5GistsGistIDCommentsParams {
	var ()
	return &PostV5GistsGistIDCommentsParams{

		timeout: timeout,
	}
}

// NewPostV5GistsGistIDCommentsParamsWithContext creates a new PostV5GistsGistIDCommentsParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostV5GistsGistIDCommentsParamsWithContext(ctx context.Context) *PostV5GistsGistIDCommentsParams {
	var ()
	return &PostV5GistsGistIDCommentsParams{

		Context: ctx,
	}
}

// NewPostV5GistsGistIDCommentsParamsWithHTTPClient creates a new PostV5GistsGistIDCommentsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostV5GistsGistIDCommentsParamsWithHTTPClient(client *http.Client) *PostV5GistsGistIDCommentsParams {
	var ()
	return &PostV5GistsGistIDCommentsParams{
		HTTPClient: client,
	}
}

/*PostV5GistsGistIDCommentsParams contains all the parameters to send to the API endpoint
for the post v5 gists gist Id comments operation typically these are written to a http.Request
*/
type PostV5GistsGistIDCommentsParams struct {

	/*AccessToken
	  用户授权码

	*/
	AccessToken *string
	/*Body
	  评论内容

	*/
	Body string
	/*GistID
	  代码片段的ID

	*/
	GistID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) WithTimeout(timeout time.Duration) *PostV5GistsGistIDCommentsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) WithContext(ctx context.Context) *PostV5GistsGistIDCommentsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) WithHTTPClient(client *http.Client) *PostV5GistsGistIDCommentsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccessToken adds the accessToken to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) WithAccessToken(accessToken *string) *PostV5GistsGistIDCommentsParams {
	o.SetAccessToken(accessToken)
	return o
}

// SetAccessToken adds the accessToken to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) SetAccessToken(accessToken *string) {
	o.AccessToken = accessToken
}

// WithBody adds the body to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) WithBody(body string) *PostV5GistsGistIDCommentsParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) SetBody(body string) {
	o.Body = body
}

// WithGistID adds the gistID to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) WithGistID(gistID string) *PostV5GistsGistIDCommentsParams {
	o.SetGistID(gistID)
	return o
}

// SetGistID adds the gistId to the post v5 gists gist Id comments params
func (o *PostV5GistsGistIDCommentsParams) SetGistID(gistID string) {
	o.GistID = gistID
}

// WriteToRequest writes these params to a swagger request
func (o *PostV5GistsGistIDCommentsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AccessToken != nil {

		// form param access_token
		var frAccessToken string
		if o.AccessToken != nil {
			frAccessToken = *o.AccessToken
		}
		fAccessToken := frAccessToken
		if fAccessToken != "" {
			if err := r.SetFormParam("access_token", fAccessToken); err != nil {
				return err
			}
		}

	}

	// form param body
	frBody := o.Body
	fBody := frBody
	if fBody != "" {
		if err := r.SetFormParam("body", fBody); err != nil {
			return err
		}
	}

	// path param gist_id
	if err := r.SetPathParam("gist_id", o.GistID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
