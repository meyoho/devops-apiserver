// Code generated by go-swagger; DO NOT EDIT.

package issues

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitee/v5/models"
)

// GetV5ReposOwnerRepoIssuesReader is a Reader for the GetV5ReposOwnerRepoIssues structure.
type GetV5ReposOwnerRepoIssuesReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV5ReposOwnerRepoIssuesReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV5ReposOwnerRepoIssuesOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV5ReposOwnerRepoIssuesOK creates a GetV5ReposOwnerRepoIssuesOK with default headers values
func NewGetV5ReposOwnerRepoIssuesOK() *GetV5ReposOwnerRepoIssuesOK {
	return &GetV5ReposOwnerRepoIssuesOK{}
}

/*GetV5ReposOwnerRepoIssuesOK handles this case with default header values.

返回格式
*/
type GetV5ReposOwnerRepoIssuesOK struct {
	Payload []*models.Issue
}

func (o *GetV5ReposOwnerRepoIssuesOK) Error() string {
	return fmt.Sprintf("[GET /v5/repos/{owner}/{repo}/issues][%d] getV5ReposOwnerRepoIssuesOK  %+v", 200, o.Payload)
}

func (o *GetV5ReposOwnerRepoIssuesOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
