// Code generated by go-swagger; DO NOT EDIT.

package issues

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitee/v5/models"
)

// PatchV5ReposOwnerRepoIssuesCommentsIDReader is a Reader for the PatchV5ReposOwnerRepoIssuesCommentsID structure.
type PatchV5ReposOwnerRepoIssuesCommentsIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *PatchV5ReposOwnerRepoIssuesCommentsIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewPatchV5ReposOwnerRepoIssuesCommentsIDOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewPatchV5ReposOwnerRepoIssuesCommentsIDOK creates a PatchV5ReposOwnerRepoIssuesCommentsIDOK with default headers values
func NewPatchV5ReposOwnerRepoIssuesCommentsIDOK() *PatchV5ReposOwnerRepoIssuesCommentsIDOK {
	return &PatchV5ReposOwnerRepoIssuesCommentsIDOK{}
}

/*PatchV5ReposOwnerRepoIssuesCommentsIDOK handles this case with default header values.

返回格式
*/
type PatchV5ReposOwnerRepoIssuesCommentsIDOK struct {
	Payload *models.Note
}

func (o *PatchV5ReposOwnerRepoIssuesCommentsIDOK) Error() string {
	return fmt.Sprintf("[PATCH /v5/repos/{owner}/{repo}/issues/comments/{id}][%d] patchV5ReposOwnerRepoIssuesCommentsIdOK  %+v", 200, o.Payload)
}

func (o *PatchV5ReposOwnerRepoIssuesCommentsIDOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.Note)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
