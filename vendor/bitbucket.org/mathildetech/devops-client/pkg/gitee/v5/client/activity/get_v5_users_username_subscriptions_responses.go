// Code generated by go-swagger; DO NOT EDIT.

package activity

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"

	models "bitbucket.org/mathildetech/devops-client/pkg/gitee/v5/models"
)

// GetV5UsersUsernameSubscriptionsReader is a Reader for the GetV5UsersUsernameSubscriptions structure.
type GetV5UsersUsernameSubscriptionsReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetV5UsersUsernameSubscriptionsReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 200:
		result := NewGetV5UsersUsernameSubscriptionsOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	case 404:
		result := NewGetV5UsersUsernameSubscriptionsNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewGetV5UsersUsernameSubscriptionsOK creates a GetV5UsersUsernameSubscriptionsOK with default headers values
func NewGetV5UsersUsernameSubscriptionsOK() *GetV5UsersUsernameSubscriptionsOK {
	return &GetV5UsersUsernameSubscriptionsOK{}
}

/*GetV5UsersUsernameSubscriptionsOK handles this case with default header values.

返回格式
*/
type GetV5UsersUsernameSubscriptionsOK struct {
	Payload []*models.Project
}

func (o *GetV5UsersUsernameSubscriptionsOK) Error() string {
	return fmt.Sprintf("[GET /v5/users/{username}/subscriptions][%d] getV5UsersUsernameSubscriptionsOK  %+v", 200, o.Payload)
}

func (o *GetV5UsersUsernameSubscriptionsOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetV5UsersUsernameSubscriptionsNotFound creates a GetV5UsersUsernameSubscriptionsNotFound with default headers values
func NewGetV5UsersUsernameSubscriptionsNotFound() *GetV5UsersUsernameSubscriptionsNotFound {
	return &GetV5UsersUsernameSubscriptionsNotFound{}
}

/*GetV5UsersUsernameSubscriptionsNotFound handles this case with default header values.

没有相关数据
*/
type GetV5UsersUsernameSubscriptionsNotFound struct {
}

func (o *GetV5UsersUsernameSubscriptionsNotFound) Error() string {
	return fmt.Sprintf("[GET /v5/users/{username}/subscriptions][%d] getV5UsersUsernameSubscriptionsNotFound ", 404)
}

func (o *GetV5UsersUsernameSubscriptionsNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
