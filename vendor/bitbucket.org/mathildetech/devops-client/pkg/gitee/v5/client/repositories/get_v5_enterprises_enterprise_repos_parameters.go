// Code generated by go-swagger; DO NOT EDIT.

package repositories

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV5EnterprisesEnterpriseReposParams creates a new GetV5EnterprisesEnterpriseReposParams object
// with the default values initialized.
func NewGetV5EnterprisesEnterpriseReposParams() *GetV5EnterprisesEnterpriseReposParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
		typeVarDefault = string("all")
	)
	return &GetV5EnterprisesEnterpriseReposParams{
		Page:    &pageDefault,
		PerPage: &perPageDefault,
		Type:    &typeVarDefault,

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV5EnterprisesEnterpriseReposParamsWithTimeout creates a new GetV5EnterprisesEnterpriseReposParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV5EnterprisesEnterpriseReposParamsWithTimeout(timeout time.Duration) *GetV5EnterprisesEnterpriseReposParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
		typeVarDefault = string("all")
	)
	return &GetV5EnterprisesEnterpriseReposParams{
		Page:    &pageDefault,
		PerPage: &perPageDefault,
		Type:    &typeVarDefault,

		timeout: timeout,
	}
}

// NewGetV5EnterprisesEnterpriseReposParamsWithContext creates a new GetV5EnterprisesEnterpriseReposParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV5EnterprisesEnterpriseReposParamsWithContext(ctx context.Context) *GetV5EnterprisesEnterpriseReposParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
		typeDefault    = string("all")
	)
	return &GetV5EnterprisesEnterpriseReposParams{
		Page:    &pageDefault,
		PerPage: &perPageDefault,
		Type:    &typeDefault,

		Context: ctx,
	}
}

// NewGetV5EnterprisesEnterpriseReposParamsWithHTTPClient creates a new GetV5EnterprisesEnterpriseReposParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV5EnterprisesEnterpriseReposParamsWithHTTPClient(client *http.Client) *GetV5EnterprisesEnterpriseReposParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
		typeDefault    = string("all")
	)
	return &GetV5EnterprisesEnterpriseReposParams{
		Page:       &pageDefault,
		PerPage:    &perPageDefault,
		Type:       &typeDefault,
		HTTPClient: client,
	}
}

/*GetV5EnterprisesEnterpriseReposParams contains all the parameters to send to the API endpoint
for the get v5 enterprises enterprise repos operation typically these are written to a http.Request
*/
type GetV5EnterprisesEnterpriseReposParams struct {

	/*AccessToken
	  用户授权码

	*/
	AccessToken *string
	/*Direct
	  只获取直属仓库，默认: false

	*/
	Direct *bool
	/*Enterprise
	  企业的路径(path/login)

	*/
	Enterprise string
	/*Page
	  当前的页码

	*/
	Page *int32
	/*PerPage
	  每页的数量，最大为 100

	*/
	PerPage *int32
	/*Type
	  筛选仓库的类型，可以是 all, public, internal, private。默认: all

	*/
	Type *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) WithTimeout(timeout time.Duration) *GetV5EnterprisesEnterpriseReposParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) WithContext(ctx context.Context) *GetV5EnterprisesEnterpriseReposParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) WithHTTPClient(client *http.Client) *GetV5EnterprisesEnterpriseReposParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccessToken adds the accessToken to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) WithAccessToken(accessToken *string) *GetV5EnterprisesEnterpriseReposParams {
	o.SetAccessToken(accessToken)
	return o
}

// SetAccessToken adds the accessToken to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) SetAccessToken(accessToken *string) {
	o.AccessToken = accessToken
}

// WithDirect adds the direct to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) WithDirect(direct *bool) *GetV5EnterprisesEnterpriseReposParams {
	o.SetDirect(direct)
	return o
}

// SetDirect adds the direct to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) SetDirect(direct *bool) {
	o.Direct = direct
}

// WithEnterprise adds the enterprise to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) WithEnterprise(enterprise string) *GetV5EnterprisesEnterpriseReposParams {
	o.SetEnterprise(enterprise)
	return o
}

// SetEnterprise adds the enterprise to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) SetEnterprise(enterprise string) {
	o.Enterprise = enterprise
}

// WithPage adds the page to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) WithPage(page *int32) *GetV5EnterprisesEnterpriseReposParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) SetPage(page *int32) {
	o.Page = page
}

// WithPerPage adds the perPage to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) WithPerPage(perPage *int32) *GetV5EnterprisesEnterpriseReposParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) SetPerPage(perPage *int32) {
	o.PerPage = perPage
}

// WithType adds the typeVar to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) WithType(typeVar *string) *GetV5EnterprisesEnterpriseReposParams {
	o.SetType(typeVar)
	return o
}

// SetType adds the type to the get v5 enterprises enterprise repos params
func (o *GetV5EnterprisesEnterpriseReposParams) SetType(typeVar *string) {
	o.Type = typeVar
}

// WriteToRequest writes these params to a swagger request
func (o *GetV5EnterprisesEnterpriseReposParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AccessToken != nil {

		// query param access_token
		var qrAccessToken string
		if o.AccessToken != nil {
			qrAccessToken = *o.AccessToken
		}
		qAccessToken := qrAccessToken
		if qAccessToken != "" {
			if err := r.SetQueryParam("access_token", qAccessToken); err != nil {
				return err
			}
		}

	}

	if o.Direct != nil {

		// query param direct
		var qrDirect bool
		if o.Direct != nil {
			qrDirect = *o.Direct
		}
		qDirect := swag.FormatBool(qrDirect)
		if qDirect != "" {
			if err := r.SetQueryParam("direct", qDirect); err != nil {
				return err
			}
		}

	}

	// path param enterprise
	if err := r.SetPathParam("enterprise", o.Enterprise); err != nil {
		return err
	}

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int32
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt32(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	if o.Type != nil {

		// query param type
		var qrType string
		if o.Type != nil {
			qrType = *o.Type
		}
		qType := qrType
		if qType != "" {
			if err := r.SetQueryParam("type", qType); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
