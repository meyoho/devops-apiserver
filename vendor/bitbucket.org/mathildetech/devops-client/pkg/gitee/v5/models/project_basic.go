// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// ProjectBasic project basic
// swagger:model ProjectBasic
type ProjectBasic struct {

	// description
	Description string `json:"description,omitempty"`

	// fork
	Fork string `json:"fork,omitempty"`

	// full name
	FullName string `json:"full_name,omitempty"`

	// html url
	HTMLURL string `json:"html_url,omitempty"`

	// human name
	HumanName string `json:"human_name,omitempty"`

	// id
	ID int32 `json:"id,omitempty"`

	// internal
	Internal string `json:"internal,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// namespace
	Namespace interface{} `json:"namespace,omitempty"`

	// owner
	Owner string `json:"owner,omitempty"`

	// path
	Path string `json:"path,omitempty"`

	// private
	Private string `json:"private,omitempty"`

	// public
	Public string `json:"public,omitempty"`

	// ssh url
	SSHURL string `json:"ssh_url,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this project basic
func (m *ProjectBasic) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *ProjectBasic) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ProjectBasic) UnmarshalBinary(b []byte) error {
	var res ProjectBasic
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
