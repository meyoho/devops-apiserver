package v1

import (
	"encoding/json"
	"strings"
	"time"
)

const (
	TypeHarbor     = "harbor"
	TypeGithub     = "github"
	TypeBitbucket  = "bitbucket"
	TypeGitee      = "gitee"
	TypeGitlab     = "gitlab"
	TypeConfluence = "confluence"
	TypeJira       = "jira"
	TypeTaiga      = "taiga"
	TypeSonarQube  = "sonarqube"
	TypeNexus      = "nexus"
	TypeJenkins    = "jenkins"
	TypeGogs       = "gogs"
	TypeGitea      = "gitea"

	TypeCodeQualityReport = "CodeQualityReport"
	CodeQualityTime       = "2006-01-02T15:04:05+0000"

	// SonarQube Metric Names
	SonarQubeBugs               = "bugs"
	SonarQubeVulnerabilities    = "vulnerabilities"
	SonarQubeCodeSmells         = "codeSmells"
	SonarQubeDuplication        = "duplications"
	SonarQubeCoverage           = "coverage"
	SonarQubeLanguages          = "languages"
	SonarQubeNewBugs            = "newBugs"
	SonarQubeNewCodeSmells      = "newCodeSmells"
	SonarQubeNewDuplication     = "newDuplications"
	SonarQubeNewVulnerabilities = "newVulnerabilities"
	SonarQubeNewCoverage        = "newCoverage"
)

const (
	CodeRepoServiceTypeGithub string = "Github"

	CodeRepoServiceTypeGitlab string = "Gitlab"

	CodeRepoServiceTypeGitee string = "Gitee"

	CodeRepoServiceTypeBitbucket string = "Bitbucket"

	CodeRepoServiceTypeGogs string = "Gogs"

	CodeRepoServiceTypeGitea string = "Gitea"
)

// HostPortStatus types
type HostPortStatus struct {
	// StatusCode is the status code of http response
	// +optional
	StatusCode int
	// Response is the response of the http request.
	// +optional
	Response string
	// Version is the version of the http request.
	// +optional
	Version string
	// Delay means the http request will attempt later
	// +optional
	Delay time.Duration
	// Last time we probed the http request.
	// +optional
	LastAttempt time.Time
	// Error Message of http request
	// +optional
	ErrorMessage string
}

type RepositoryCommit struct {
	// CommitID defines the commit id
	// +optional
	CommitID string
	// CommitMessage defines the commit id
	// +optional
	CommitMessage string
	// CommitAt defines the commit time
	// +optional
	CommitAt time.Time
	// CommitterName defines the committer's name
	// +optional
	CommitterName string
	// CommitterEmail defines the committer's email
	// +optional
	CommitterEmail string
}

// CodeRepoBindingRepositories used to retrieve logs from a pipeline
type CodeRepoBindingRepositories struct {
	// CodeRepoServiceType defines the service type.
	Type string
	// Owners a list of owner can be fetched from a binding.
	// +optional
	Owners []CodeRepositoryOwner
}

// CodeRepositoryOwner defines the repository owner.
type CodeRepositoryOwner struct {
	// Type defines the owner type.
	Type string
	// ID defines the id.
	ID string
	// Name defines the name.
	Name string
	// Email defines the email.
	// +optional
	Email string
	// HTMLURL defines the htmlURL.
	// +optional
	HTMLURL string
	// AvatarURL defines the avatarURL.
	// +optional
	AvatarURL string
	// DiskUsage defines the diskUsage.
	// +optional
	DiskUsage int
	// Repositories defines the repositories in this owner.
	// +optional
	Repositories []OriginCodeRepository
}

type OriginCodeRepository struct {
	// CodeRepoServiceType defines the service type.
	CodeRepoServiceType string
	// ID defines the id.
	ID string
	// Name defines the name.
	Name string
	// FullName defines the full name
	// +optional
	FullName string
	// Description defines the description.
	// +optional
	Description string
	// HTMLURL defines the html url.
	// +optional
	HTMLURL string
	// CloneURL defines the clone url which begins with "https://" or "http://".
	CloneURL string
	// SSHURL defines ssh clone url which begins with "git@".
	SSHURL string
	// Language defines the language.
	// +optional
	Language string
	// Owner defines the owner.
	Owner OwnerInRepository
	// CreatedAt defines the created time.
	CreatedAt time.Time
	// PushedAt defines the pushed time.
	// +optional
	PushedAt time.Time
	// UpdatedAt defines the updated time.
	// +optional
	UpdatedAt time.Time
	// Private defines the visibility of the repository.
	// +optional
	Private bool
	// Size defines the size of the repository used.
	// +optional
	Size int64
	// Size defines the size of the repository used which is humanize.
	// +optional
	SizeHumanize string
	// Data defines reserved fields for the repository
	// +optional
	Data map[string]string
}

// OwnerInRepository defines the owner in repository which includes fewer properties.
type OwnerInRepository struct {
	// OriginCodeRepoOwnerType defines the type of the owner
	// +optional
	Type string
	// ID defines the id of the owner
	// +optional
	ID string
	// Name defines the name of the owner
	// +optional
	Name string
}

type CodeRepoBranch struct {
	// Name defines the name in branch
	Name string
	// Commit defines the record was last commit by this branch
	Commit string
}

// CreateProjectOptions work for create projectmanagement project
type CreateProjectOptions struct {
	// +optional
	Name     string
	IsRemote bool
	Data     map[string]string
}

// ProjectData return projectmanagement project Info
type ProjectData struct {
	Name        string
	ProjectID   int
	Annotations map[string]string
	Data        map[string]string
}

// ProjectDataList  fetch project infor
type ProjectDataList struct {
	Items []ProjectData
}

//IssueOptionsList represent the all issue options

type IssueOptionsList struct {
	Status   []IssueOptions
	Priority []IssueOptions
	Type     []IssueOptions
}

// IssueStatus represent the issue's status
type IssueOptions struct {
	Name string
	ID   string
}

//IssueDetail represent the issue detail
type IssueDetail struct {
	//link of the issue
	SelfLink string
	//key of the issue
	Key string
	//summay of the issue
	Summary string
	//description of the issue
	Description string
	//comments of the issue
	Comments []Comment
	//issuelinks of the issue
	IssueLinks []IssueLink
	//priority of the issue
	Priority IssuePriority
	//issuetype of the issue
	Issuetype IssueType
	//status of the issue
	Status IssueStatus
	//project which this issue belong to
	Project ProjectData
	//created time
	Created string
	//updated time
	Updated string
	//creator name
	Creator User
	//assigneer name
	Assignee User
}

//IssueType represent the type of Issue
type IssueType struct {
	//Issue type name
	Name string
}

//IssueStatus represent the status of Issue
type IssueStatus struct {
	//Issue Status name
	Name string
	//Issue Status ID
	ID string
}

//IssuePriority represent the priority of Issue
type IssuePriority struct {
	//Issue Priority Name
	Name string
}

// Comment Info in Issue
type Comment struct {
	//Comment author name
	Author string
	//Created Time
	Time string
	//Content of the comment
	Content string
}

//IssueLink represent the issue link info
type IssueLink struct {
	SelfLink string
	Summary  string
	Key      string
}

//User represent the user info in Jira
type User struct {
	Username string
	Email    string
	ID       string
}

// IssueList reprsent issue list
type IssueList struct {
	Issues     []IssueDetail
	StartAt    int64
	MaxResults int64
	Total      int64
}

//ListIssuesOptions repersent the option to use when list Issue
type ListIssuesOptions struct {
	//Page defines the page num
	Page int
	//PageSize defines the pagesize num
	PageSize int
	//Project defines the Project name
	//+optional
	Project string
	//Type defines the issue type
	//+optional
	Type string
	//Priority defines the priority of the issue list
	//+optional
	Priority string
	//Status defines the status of the issue list
	//+optional
	Status string
	//Summary defines the summary of the issue list
	//+optional
	Summary string
	//Issuekey defines the issue key of a issue
	//+optional
	IssueKey string
	//OrderBy defines the order
	OrderBy string
	//Sort chould be ASC or DESC
	Sort string
	// Projects that can be use
	Projects []string
}

// ListProjectOptions list projects
type ListProjectOptions struct {
	Page     string
	PageSize string
	Filter   string
	IsRemote bool
}

// ImageRegistryBindingRepositories used to retrieve repository from registry
type ImageRegistryBindingRepositories struct {
	Items []string
}

type ImageTagScanStatus string

const (
	ImageTagScanStatusNotScan   ImageTagScanStatus = "notScan"
	ImageTagScanStatusPending   ImageTagScanStatus = "pending"
	ImageTagScanStatusAnalyzing ImageTagScanStatus = "analyzing"
	ImageTagScanStatusFinished  ImageTagScanStatus = "finished"
	ImageTagScanStatusError     ImageTagScanStatus = "error"
)

// ImageTag defines the image tag attributes
type ImageTag struct {
	// Name defines the tag name
	// +optional
	Name string
	// Digest defines the tag digest
	// +optional
	Digest string
	// Author defines the tag author
	// Support for Harbor
	// +optional
	Author string
	// CreatedAt defines the tag created_at
	// +optional
	CreatedAt time.Time
	// UpdatedAt defines the completion time of Harbor scan tag
	// Support for Harbor
	// +optional
	UpdatedAt  time.Time
	Size       string
	Level      int
	ScanStatus ImageTagScanStatus
	Message    string
	Summary    []Summary
}

// Summary defines the Severity Count
type Summary struct {
	Severity int
	Count    int
}

type ImageResult struct {
	StatusCode int
	Message    string
}

type VulnerabilityList struct {
	Items []Vulnerability
}

type Vulnerability struct {
	ID          string
	Severity    int
	Package     string
	Version     string
	Description string
	Link        string
}

// CodeQualityCondition presents CodeQualityProject analyze info
type CodeQualityCondition struct {
	BindingCondition
	// Branch defines analyze code branch, default is master
	Branch string
	// IsMain defines whether the branch is the main branch
	IsMain bool
	// QualityGate defines project use which quality gate
	QualityGate string
	// Public defines project visible
	Visibility string
	// Metrics define a series of metrics of this project
	Metrics map[string]CodeQualityAnalyzeMetric
}

// BindingCondition defines the resource associated with the binding.
// The binding controller will check the status of the resource periodic and change it's status.
// The resource can be found by "name"+"type"+"binding's namespace"
type BindingCondition struct {
	// Name defines the name.
	// +optional
	Name string
	// namespace defines the name.
	// +optional
	Namespace string
	// Type defines the type.
	// +optional
	Type string
	// Last time we probed the condition.
	// +optional
	LastAttempt time.Time
	// Unique, one-word, CamelCase reason for the condition's last transition.
	// +optional
	Reason string
	// Human-readable message indicating details about last transition.
	// +optional
	Message string
	// Status defines the status.
	// +optional
	Status string
	// Owner defins who own current condition
	// +optional
	Owner string
}

// CodeQualityAnalyzeMetric present CodeQualityProject analyze result
type CodeQualityAnalyzeMetric struct {
	// Name defines the name of this metric
	Name string
	// Value defines the value of this metric
	Value string
	// Level defines the level of the value
	// +optional
	Level string
}

// CodeQualityBinding is the binding referenced to CodeQuality
type CodeQualityBinding struct {
	Name, Namespace string
	Spec            CodeQualityBindingSpec
}

type CodeQualityTool struct {
	Name string
}

// CodeQualityBindingSpec represents CodeQualityBinding specs
type CodeQualityBindingSpec struct {
	// CodeQualityTool defines the CodeQualityTool in spec
	CodeQualityTool CodeQualityTool
	// CodeRepository defines the CodeRepository in spec
	// +optional
	CodeRepository CodeRepositoryInfo
	// Pattern defines sync project model is prefix of name
	// +optional
	Pattern CodeRepositoryPattern
}

// CodeRepositoryInfo defines sync project model is devops binding codeRepository
type CodeRepositoryInfo struct {
	// +optional
	All bool
}

// CodeRepositoryPattern defines sync project model is prefix of name
type CodeRepositoryPattern struct {
	// Prefix is a list of sync project model's prefix of name
	// +optional
	Prefix []string
}

// CodeQualityProject save CodeQualityTool Project info
type CodeQualityProject struct {
	// Specification of the desired behavior of the CodeQualityProject.
	// +optional
	Name, Namespace string
	Spec            CodeQualityProjectSpec
}

type LocalObjectReference struct {
	Name string
}

// CodeQualityProject presents CodeQualityProject's spec
type CodeQualityProjectSpec struct {
	// CodeQualityTool defines the CodeQualityTool in spec
	CodeQualityTool LocalObjectReference
	// CodeQualityBinding defines the CodeQualityBinding in spec
	CodeQualityBinding LocalObjectReference
	// CodeRepository defines the CodeRepository in spec
	CodeRepository LocalObjectReference
	// Project defines CodeQualityProject info
	Project CodeQualityProjectInfo
}

// CodeQualityProjectInfo presents CodeQualityProject info
type CodeQualityProjectInfo struct {
	// ProjectKey defines key in CodeQualityProjectInfo
	ProjectKey string
	// ProjectName defines display name in CodeQualityProjectInfo
	ProjectName string
	// CodeAddress defines code address in CodeQualityProjectInfo
	CodeAddress string
	// LastAnalysis defines the last analysis date of this project
	LastAnalysis time.Time
}

// CodeRepositoryList is a list of CodeRepository objects.
type CodeRepositoryList struct {
	// Items list of CodeRepository objects.
	Items []CodeRepository
}

// CodeRepository struct holds a reference to a specific CodeRepository configuration
// and some user data for access
type CodeRepository struct {
	Name string
	// Specification of the desired behavior of the CodeRepository.
	// +optional
	Spec CodeRepositorySpec
}

// CodeRepositorySpec defines CodeRepository's specs
type CodeRepositorySpec struct {
	// CodeRepoBinding defines the codeRepoBinding
	CodeRepoBinding LocalObjectReference
	// Repository defines the repository
	Repository OriginCodeRepository
}

// ArtifactRegistryList Get ArtifactRegistry list
type ArtifactRegistryList struct {
	Items []ArtifactRegistry
}

type ArtifactRegistry struct {
	Name string
	Spec ArtifactRegistrySpec
}

type ArtifactRegistrySpec struct {
	ArtifactRegistryArgs map[string]string
	ArtifactRegistryName string
	Type                 string
	Public               bool
	URL                  string
}

type BlobStoreOptionList struct {
	Items []BlobStoreOption
}

type BlobStoreOption struct {
	Name string

	Type string
}

// PipelineLog used to retrieve logs from a pipeline
type PipelineLog struct {
	// True means has more log behind。
	// +optional
	HasMore bool
	// NextStart is next start number to fetch new log.
	// +optional
	NextStart *int64
	// Text is the context of the log.
	// +optional
	Text string
}

// PipelineBlueOceanTask a task from BlueOcean API
type PipelineBlueOceanTask struct {
	// extends PipelineBlueOceanRef
	PipelineBlueOceanRef
	// DisplayDescription description for step/stage
	DisplayDescription string
	// DisplayName is a display name for step/stage
	// +optional
	DisplayName string
	// Duration in milliseconds
	// +optional
	DurationInMillis int64
	// Input describes a input for Jenkins step
	// +optional
	Input *PipelineBlueOceanInput

	// Result describes a result for a stage/step in Jenkins
	Result string
	// Stage describe the current state of the stage/step in Jenkins
	State string
	// StartTime the starting time for the stage/step
	// +optional
	StartTime string

	// Edges edges for a specific stage
	// +optional
	Edges []PipelineBlueOceanRef
	// Actions
	// +optional
	Actions []PipelineBlueOceanRef
}

// PipelineBlueOceanRef reference of a class/resource
type PipelineBlueOceanRef struct {
	// Href reference url for resource
	// +optional
	Href string
	// ID unique identifier for step/stage
	// +optional
	ID string
	// Type describes the resource type
	// +optional
	Type string
	// URLName describes a url name for the resource
	// +optional
	URLName string

	// Description description for reference
	// +optional
	Description string
	// Name name for reference
	// +optional
	Name string

	// Value for reference
	// +optional
	Value ComposeValue
}

// ComposeValue represent a compose value
type ComposeValue struct {
	Value string
}

// UnmarshalJSON convert multi-type into string
func (f *ComposeValue) UnmarshalJSON(data []byte) error {
	originStr := strings.Trim(string(data), `"`)
	switch str := strings.ToLower(originStr); str {
	case "true":
		f.Value = "true"
	case "false":
		f.Value = "false"
	default:
		f.Value = originStr
	}
	return nil
}

// MarshalJSON marshal the compose value
func (f ComposeValue) MarshalJSON() ([]byte, error) {
	return json.Marshal(f.Value)
}

func (f ComposeValue) String() string {
	return f.Value
}

// PipelineBlueOceanInput describes a Jenkins input for a step
type PipelineBlueOceanInput struct {
	// extends PipelineBlueOceanRef
	PipelineBlueOceanRef
	// Message describes the message for the input
	Message string
	// OK describes which option is used for successful submit
	OK string

	// Parameters parameters for input
	// +optional
	Parameters []PipelineBlueOceanParameter
	// Submitter list of usernames or user ids that can approve
	// +optional
	Submitter string
}

// PipelineBlueOceanParameter one step parameter for Jenkins step
type PipelineBlueOceanParameter struct {
	PipelineBlueOceanRef
	// DefaultParameterValue type and default value for parameter
	// +optional
	DefaultParameterValue PipelineBlueOceanRef
}

// PipelineTestReport represents the test report from Pipeline
type PipelineTestReport struct {
	// Items for the test report
	// +optional
	Items []PipelineTestReportItem
}

// PipelineInputOptions options for pipeline input request
type PipelineInputOptions struct {
	// Approve whether approve this
	Approve bool
	// InputID is the id for input dsl step from Jenkinsfile
	InputID string
	// PlatformApprover for who approve or reject this
	// +optional
	PlatformApprover string
	// Parameters is the parameters of the pipeline input request
	// +optional
	Parameters []PipelineParameter
}

// PipelineParameter specifies a parameter for a pipeline
type PipelineParameter struct {
	// Name is the name of the parameter.
	// +optional
	Name string
	// Type is the type of the parameter.
	// +optional
	Type PipelineParameterType
	// Value is the value of the parameter.
	// +optional
	Value string
	// Description is the description of the parameter.
	// +optional
	Description string
}

// PipelineParameterType type of parameter for pipeline
type PipelineParameterType string

const (
	// PipelineParameterTypeString parameter type string
	PipelineParameterTypeString PipelineParameterType = "string"
	// PipelineParameterTypeBoolean parameter type boolean
	PipelineParameterTypeBoolean PipelineParameterType = "boolean"
)

// PipelineTestReportItem test report item
type PipelineTestReportItem struct {
	Age int64
	// Duration the time of test
	Duration float64
	// ErrorDetails error details of test
	ErrorDetails string
	// ErrorStackTrace if the status is erro then error stack trace of test
	ErrorStackTrace string
	// HasStdLog indicate whether has standard log outpupt
	HasStdLog bool
	// ID id for the test report item
	ID string
	// Name is the name of test report item
	Name  string
	State string
	// Status indicates the status of report item
	Status string
}

type TestReportSummary struct {
	// existing failed
	ExistingFailed int64 `json:"existingFailed,omitempty"`
	// failed
	Failed int64 `json:"failed,omitempty"`
	// fixed
	Fixed int64 `json:"fixed,omitempty"`
	// passed
	Passed int64 `json:"passed,omitempty"`
	// regressions
	Regressions int64 `json:"regressions,omitempty"`
	// skipped
	Skipped int64 `json:"skipped,omitempty"`
	// total
	Total int64 `json:"total,omitempty"`
}

const (
	GithubOrgKey    = "Organization"
	GitlabOrgKey    = "group"
	BitbucketOrgKey = "team"
	// gitee was not defined here because of no org key in response.
)

const (
	// OriginCodeRepoOwnerTypeUser defines the owner type of user
	OriginCodeRepoOwnerTypeUser = "User"
	// OriginCodeRepoRoleTypeOrg defines the owner type of org
	OriginCodeRepoRoleTypeOrg = "Org"
)

// GetOwnerType unify the owner type from different code repository service.
func GetOwnerType(strOwnerType string) string {
	switch strOwnerType {
	case GithubOrgKey, GitlabOrgKey, BitbucketOrgKey:
		return OriginCodeRepoRoleTypeOrg
	default:
		return OriginCodeRepoOwnerTypeUser
	}
}

type Configurations struct {
	// The auth mode of current system, such as "db_auth", "ldap_auth"
	AuthMode string `json:"auth_mode,omitempty"`

	// Specify the oidc client id.
	OidcClientID string `json:"oidc_client_id,omitempty"`

	// Specify the oidc endpoint.
	OidcEndpoint string `json:"oidc_endpoint,omitempty"`

	// Specify the oidc name.
	OidcName string `json:"oidc_name,omitempty"`

	// Specify the oidc scope.
	OidcScope string `json:"oidc_scope,omitempty"`

	// Specify the oidc verify cert.
	OidcVerifyCert bool `json:"oidc_verify_cert,omitempty"`
}

// NexusScript represents a script which belong to nexus
type NexusScript struct {
	Name    string
	Type    string
	Content string
}
