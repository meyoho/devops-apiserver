package v1

import (
	"bitbucket.org/mathildetech/devops-client/pkg/jenkins/v1/models"
	"context"
	"io"
	"time"
)

// Interface is the main interface for all clients
//go:generate mockgen -destination=../../../mock/interface_mock.go -package=mock bitbucket.org/mathildetech/devops-client/pkg/api/v1 Interface
type Interface interface {
	Initializer
	Checker
	Authorizer
	CodeRepoService
	ImageRegistryService
	ProjectService
	CodeQualityService
	NexusService
	JenkinsService
	ProjectManagementService
	HarborService
}

type Initializer interface {
	Init(context.Context) []error
}

type Checker interface {
	Available(context.Context) (*HostPortStatus, error)
}

type Authorizer interface {
	Authenticate(context.Context) (*HostPortStatus, error)
}

type CodeRepoService interface {
	GetRemoteRepos(context.Context) (*CodeRepoBindingRepositories, error)
	GetBranches(_ context.Context, owner, repo, repoFullName string) ([]CodeRepoBranch, error)
	GetLatestRepoCommit(_ context.Context, repoID, owner, repoName, repoFullName string) (commit *RepositoryCommit, status *HostPortStatus)
	CreateCodeRepoProject(context.Context, CreateProjectOptions) (*ProjectData, error)
	ListCodeRepoProjects(context.Context, ListProjectOptions) (*ProjectDataList, error)
}

type ImageRegistryService interface {
	GetImageRepos(context.Context) (*ImageRegistryBindingRepositories, error)
	GetImageTags(_ context.Context, repository string) ([]ImageTag, error)
	TriggerScanImage(_ context.Context, repositoryName, tag string) *ImageResult
	GetVulnerability(_ context.Context, repositoryName, tag string) (result *VulnerabilityList, err error)
	GetImageProjects(_ context.Context) (*ProjectDataList, error)
	CreateImageProject(_ context.Context, name string) (*ProjectData, error)
}

// HarborService is the interface for interactive with a harbor server
type HarborService interface {
	UpdateHarborConfig(ctx context.Context, configJSON string) (err error)
	GetHarborConfig(ctx context.Context) (config *Configurations, err error)
}

type ProjectService interface {
	GetProjects(_ context.Context, page, pagesize string) (*ProjectDataList, error)
	CreateProject(_ context.Context, projectName, projectDescription, projectLead, projectKey string) (*ProjectData, error)
}

type ProjectManagementService interface {
	GetIssueOptions(_ context.Context, optiontype string) (*IssueOptionsList, error)
	GetIssueDetail(_ context.Context, issuekey string) (*IssueDetail, error)
	GetIssueList(_ context.Context, listoption ListIssuesOptions) (*IssueList, error)
}

type CodeQualityService interface {
	GetProjectReport(_ context.Context, projectKey string) ([]CodeQualityCondition, error)
	GetCorrespondCodeQualityProjects(_ context.Context, repositoryList *CodeRepositoryList, binding *CodeQualityBinding) ([]CodeQualityProject, error)
	CheckProjectExist(_ context.Context, projectKey string) (bool, error)
	GetLastAnalysisDate(_ context.Context, projectKey string) (*time.Time, error)
}

type ArtifactService interface {
	ListRegistry(context.Context) (ArtifactRegistryList, error)
	CreateRegistry(_ context.Context, artifactType string, v map[string]string) error
	ListBlobStore(context.Context) (BlobStoreOptionList, error)
}

// NexusService is the interface for interactive with a Nexus server
type NexusService interface {
	ArtifactService

	CreateScriptByPath(ctx context.Context, name, kind, script string) error
	UpdateScriptByPath(ctx context.Context, name, kind, script string) error
	ExecuteScript(ctx context.Context, name, jsonParam string) error
	DeleteScript(ctx context.Context, name string) error
	ListScripts(ctx context.Context) ([]NexusScript, error)
	GetScript(ctx context.Context, name string) (NexusScript, error)

	CreateTask(ctx context.Context, name, repositoryName, cronExpression string) error
}

type JenkinsService interface {
	GetJobProgressiveLog(ctx context.Context, pipelineNamespace, pipelineName string, buildNumber, start int64) (*PipelineLog, error)
	GetBranchJobProgressiveLog(ctx context.Context, pipelineNamespace, pipelineName, branchName string, buildNumber, start int64) (*PipelineLog, error)
	GetJobStepLog(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId, stepId string, start int64) (*PipelineLog, error)
	GetBranchJobStepLog(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, stepId string, start int64) (*PipelineLog, error)
	GetJobNodeLog(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId string, start int64) (*PipelineLog, error)
	GetBranchJobNodeLog(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string, start int64) (*PipelineLog, error)
	GetBranchProgressiveScanLog(ctx context.Context, pipelineNamespace, pipelineName string, start int64) (*PipelineLog, error)
	ScanMultiBranchJob(ctx context.Context, pipelineNamespace, pipelineName string) error
	GetJobNodes(ctx context.Context, pipelineNamespace, pipelineName, organization, runId string) ([]PipelineBlueOceanTask, error)
	GetBranchJobNodes(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId string) ([]PipelineBlueOceanTask, error)
	GetJobSteps(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId string) ([]PipelineBlueOceanTask, error)
	GetBranchJobSteps(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId string) ([]PipelineBlueOceanTask, error)

	GetTestReportSummary(ctx context.Context, pipelineNamespace, pipelineName, organization, runId string) (*TestReportSummary, error)
	GetBranchTestReportSummary(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId string) (*TestReportSummary, error)

	GetTestReports(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, status, state, stateBang string, start, limit int64) (*PipelineTestReport, error)
	GetBranchTestReports(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, status, state, stateBang string, start, limit int64) (*PipelineTestReport, error)

	SubmitInputStep(ctx context.Context, pipelineNamespace, pipelineName, organization, runId, nodeId, stepId string, options PipelineInputOptions) error
	SubmitBranchInputStep(ctx context.Context, pipelineNamespace, pipelineName, branchName, organization, runId, nodeId, stepId string, options PipelineInputOptions) error
	GetArtifactList(ctx context.Context, pipelineNamespace, pipelineName, organization, run string) (models.ArtifactList, error)
	DownloadArtifact(context context.Context, namespace string, pcname string, runid string, filename string)(body io.ReadCloser, contenttype string, err error)
	DownloadArtifacts(context context.Context, namespace string, pcname string, runid string)(body io.ReadCloser, contenttype string, err error)
}
