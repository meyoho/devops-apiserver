package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v6 "bitbucket.org/mathildetech/devops-client/pkg/confluence/v6"
)

func init() {
	register(devopsv1.TypeConfluence, versionOpt{
		version:   "v6",
		factory:   v6.NewClient(),
		isDefault: true,
	})
}
