package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v1 "bitbucket.org/mathildetech/devops-client/pkg/harbor/v1"
	"bitbucket.org/mathildetech/devops-client/pkg/harbor/v1_8"
)

func init() {
	register(devopsv1.TypeHarbor, versionOpt{
		version:   "v1",
		factory:   v1.NewClient(),
		isDefault: false,
	})

	register(devopsv1.TypeHarbor, versionOpt{
		version:   "v1.8",
		factory:   v1_8.NewClient(),
		isDefault: true,
	})
}
