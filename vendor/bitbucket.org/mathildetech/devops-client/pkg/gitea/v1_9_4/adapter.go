// Code generated
package v1_9_4

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/go-logr/logr"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bitbucket.org/mathildetech/devops-client/pkg/generic"
	"bitbucket.org/mathildetech/devops-client/pkg/gitea/v1_9_4/client"
	"bitbucket.org/mathildetech/devops-client/pkg/gitea/v1_9_4/client/organization"
	"bitbucket.org/mathildetech/devops-client/pkg/gitea/v1_9_4/client/repository"
	"bitbucket.org/mathildetech/devops-client/pkg/gitea/v1_9_4/client/user"
	"bitbucket.org/mathildetech/devops-client/pkg/gitea/v1_9_4/models"
	"bitbucket.org/mathildetech/devops-client/pkg/transport"
	"github.com/dustin/go-humanize"
	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	v1.NotImplement
	logger     logr.Logger
	client     *client.Gitea
	opts       *v1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ v1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() v1.ClientFactory {
	return func(opts *v1.Options) v1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

func (c *Client) listAllRepos(ctx context.Context) ([]*models.Repository, error) {
	params := user.
		NewUserCurrentListReposParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	res, err := c.client.User.UserCurrentListRepos(params, c.authInfo)
	if err != nil {
		return nil, err
	}
	return res.Payload, nil
}

func (c *Client) listUserOrgs(ctx context.Context) ([]*models.Organization, error) {
	params := organization.
		NewOrgListCurrentUserOrgsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	res, err := c.client.Organization.OrgListCurrentUserOrgs(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return res.Payload, nil
}

func (c *Client) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}, orgs map[string]struct{}) (codeRepo v1.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if giteaRepo, ok := remoteRepo.(*models.Repository); ok {
		codeRepo = v1.OriginCodeRepository{
			CodeRepoServiceType: v1.CodeRepoServiceTypeGitea,
			ID:                  strconv.FormatInt(giteaRepo.ID, 10),
			Name:                giteaRepo.Name,
			FullName:            giteaRepo.FullName,
			Description:         giteaRepo.Description,
			HTMLURL:             giteaRepo.HTMLURL,
			CloneURL:            giteaRepo.CloneURL,
			SSHURL:              giteaRepo.SSHURL,
			Language:            "",
			Owner: v1.OwnerInRepository{
				ID:   strconv.FormatInt(giteaRepo.Owner.ID, 10),
				Name: giteaRepo.Owner.UserName,
				Type: c.getUserType(orgs, giteaRepo.Owner.UserName),
			},

			CreatedAt: time.Time(giteaRepo.Created),
			PushedAt:  time.Time(giteaRepo.Updated),
			UpdatedAt: time.Time(giteaRepo.Updated),

			Private:      giteaRepo.Private,
			Size:         giteaRepo.Size * 1000,
			SizeHumanize: humanize.Bytes(uint64(giteaRepo.Size * 1000)),
		}
	}

	return
}

func (c *Client) getUserType(orgs map[string]struct{}, name string) string {
	if _, ok := orgs[name]; ok {
		return v1.OriginCodeRepoRoleTypeOrg
	}
	return v1.OriginCodeRepoOwnerTypeUser
}

// GetRemoteRepos implements CodeRepoService
func (c *Client) GetRemoteRepos(ctx context.Context) (*v1.CodeRepoBindingRepositories, error) {
	repos, err := c.listAllRepos(ctx)
	if err != nil {
		return nil, err
	}

	orgs, err := c.listUserOrgs(ctx)
	if err != nil {
		return nil, err
	}

	dictOrgs := make(map[string]struct{}, len(orgs))
	for _, org := range orgs {
		dictOrgs[org.UserName] = struct{}{}
	}

	dictOwners := make(map[string]*v1.CodeRepositoryOwner)
	for _, repo := range repos {
		ownerKey := repo.Owner.UserName

		if owner, ok := dictOwners[ownerKey]; !ok {
			owner = &v1.CodeRepositoryOwner{
				Type:         c.getUserType(dictOrgs, ownerKey),
				ID:           strconv.FormatInt(repo.Owner.ID, 10),
				Name:         repo.Owner.UserName,
				Email:        repo.Owner.Email.String(),
				HTMLURL:      "",
				AvatarURL:    repo.Owner.AvatarURL,
				DiskUsage:    0,
				Repositories: make([]v1.OriginCodeRepository, 0, 10),
			}
			dictOwners[ownerKey] = owner
		}
		codeRepo := c.ConvertRemoteRepoToBindingRepo(repo, dictOrgs)
		dictOwners[ownerKey].Repositories = append(dictOwners[ownerKey].Repositories, codeRepo)
	}

	result := &v1.CodeRepoBindingRepositories{
		Type:   v1.CodeRepoServiceTypeGitea,
		Owners: []v1.CodeRepositoryOwner{},
	}

	for _, owner := range dictOwners {
		if len(owner.Repositories) > 0 {
			result.Owners = append(result.Owners, *owner)
		}
	}

	return result, nil

}

// GetBranches implements CodeRepoService
func (c *Client) GetBranches(ctx context.Context, owner, repo, repoFullName string) ([]v1.CodeRepoBranch, error) {
	params := repository.
		NewRepoListBranchesParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithOwner(owner).
		WithRepo(repo)
	res, err := c.client.Repository.RepoListBranches(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := make([]v1.CodeRepoBranch, 0, len(res.Payload))
	for _, branch := range res.Payload {
		result = append(result, v1.CodeRepoBranch{
			Name:   branch.Name,
			Commit: branch.Commit.ID,
		})
	}

	return result, nil
}

// CreateCodeRepoProject implements CodeRepoService
func (c *Client) CreateCodeRepoProject(ctx context.Context, opts v1.CreateProjectOptions) (*v1.ProjectData, error) {
	params := organization.
		NewOrgCreateParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithOrganization(&models.CreateOrgOption{
			UserName: &opts.Name,
		})
	res, err := c.client.Organization.OrgCreate(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return giteaOrgAsProjectData(*res.Payload)
}

func giteaOrgAsProjectData(org models.Organization) (*v1.ProjectData, error) {
	data, err := generic.MarshalToMapString(org)
	if err != nil {
		return nil, err
	}

	projectData := &v1.ProjectData{
		Name: org.UserName,
		Annotations: map[string]string{
			"avatarURL":   org.AvatarURL,
			"accessPath":  "/" + org.UserName,
			"description": org.Description,
			"type":        v1.OriginCodeRepoRoleTypeOrg,
		},
		Data: data,
	}

	return projectData, nil
}

func giteaUserAsProjectData(user *models.User) (*v1.ProjectData, error) {
	var projectData = &v1.ProjectData{
		Name: user.UserName,
		Annotations: map[string]string{
			"avatarURL":   user.AvatarURL,
			"webURL":      "/" + user.UserName,
			"description": "",
			"type":        string(v1.OriginCodeRepoOwnerTypeUser),
		},
		Data: map[string]string{
			"login":   user.UserName,
			"id":      strconv.Itoa(int(user.ID)),
			"name":    user.FullName,
			"company": "",
			"type":    "",
			"email":   user.Email.String(),
		},
	}
	return projectData, nil
}

func (c *Client) currentUser(ctx context.Context) (*v1.ProjectData, error) {
	params := user.
		NewUserGetCurrentParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	res, err := c.client.User.UserGetCurrent(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return giteaUserAsProjectData(res.Payload)
}

// ListCodeRepoProjects implements CodeRepoService
func (c *Client) ListCodeRepoProjects(ctx context.Context, opts v1.ListProjectOptions) (*v1.ProjectDataList, error) {
	params := organization.
		NewOrgListCurrentUserOrgsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	res, err := c.client.Organization.OrgListCurrentUserOrgs(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	items := make([]v1.ProjectData, 0, len(res.Payload))
	for _, org := range res.Payload {
		projectData, err := giteaOrgAsProjectData(*org)
		if err != nil {
			return nil, err
		}

		items = append(items, *projectData)
	}

	userProject, err := c.currentUser(ctx)
	if err != nil {
		return nil, err
	}

	items = append(items, *userProject)

	result := &v1.ProjectDataList{
		Items: items,
	}

	return result, nil
}

// GetLatestRepoCommit implements CodeRepoService
func (c *Client) GetLatestRepoCommit(ctx context.Context, repoID, owner, repoName, repoFullName string) (commit *v1.RepositoryCommit, status *v1.HostPortStatus) {
	parmas := repository.
		NewRepoGetAllCommitsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithOwner(owner).
		WithRepo(repoName)
	res, err := c.client.Repository.RepoGetAllCommits(parmas, c.authInfo)
	if err != nil {
		return nil, &v1.HostPortStatus{
			StatusCode: 500,
			Response:   err.Error(),
		}
	}

	if len(res.Payload) > 0 {
		commitAt, _ := time.Parse(time.RFC3339Nano, res.Payload[0].Commit.Committer.Date)
		commit = &v1.RepositoryCommit{
			CommitID:       res.Payload[0].SHA,
			CommitAt:       commitAt,
			CommitterName:  res.Payload[0].Commit.Committer.Name,
			CommitterEmail: res.Payload[0].Commit.Committer.Email.String(),
			CommitMessage:  res.Payload[0].Commit.Message,
		}
	}

	return commit, &v1.HostPortStatus{
		StatusCode:  200,
		LastAttempt: time.Now(),
	}
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(_ context.Context) (*v1.HostPortStatus, error) {
	var url string
	var header map[string]string

	if c.opts.BasicConfig != nil {
		url = c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host + "/api/v1/user"
	}

	if c.opts.BasicAuth != nil {
		username, password := c.opts.BasicAuth.Username, c.opts.BasicAuth.Password
		encoded := base64.StdEncoding.EncodeToString([]byte(username + ":" + password))
		header = map[string]string{
			"Authorization": fmt.Sprintf("Basic %s", encoded),
		}
	} else if c.opts.APIKey != nil {
		header = map[string]string{
			c.opts.APIKey.Name: c.opts.APIKey.Value,
		}

	}

	return generic.CheckService(url, header)
}