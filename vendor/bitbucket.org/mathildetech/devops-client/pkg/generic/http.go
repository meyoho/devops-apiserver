package generic

import (
	"io/ioutil"
	"net/http"
	"time"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
)

// CheckService generic checker
func CheckService(url string, header map[string]string) (status *v1.HostPortStatus, err error) {
	var (
		resp        *http.Response
		req         *http.Request
		start       = time.Now()
		duration    time.Duration
		lastAttempt = start
	)
	status = &v1.HostPortStatus{
		StatusCode:  200,
		LastAttempt: lastAttempt,
	}

	defer func() {
		if err != nil {
			status.StatusCode = 500
			status.Response = err.Error()
		} else if resp != nil && !resp.Close {
			status.StatusCode = resp.StatusCode
			data, _ := ioutil.ReadAll(resp.Body)
			status.Response = string(data)
			resp.Body.Close()
		}

		duration = time.Since(start)
		status.Delay = duration
	}()

	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}

	if header != nil && len(header) > 0 {
		for key, val := range header {
			req.Header.Add(key, val)
		}
	}

	// TODO customize transport
	client := &http.Client{
		Transport: v1.NewDefaultTransport(),
		Timeout:   30 * time.Second,
	}
	resp, err = client.Do(req)
	return
}
