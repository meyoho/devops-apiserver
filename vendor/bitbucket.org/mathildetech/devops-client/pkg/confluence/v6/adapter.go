package v6

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-logr/logr"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bitbucket.org/mathildetech/devops-client/pkg/confluence/v6/client"
	"bitbucket.org/mathildetech/devops-client/pkg/confluence/v6/client/operations"
	"bitbucket.org/mathildetech/devops-client/pkg/confluence/v6/models"
	"bitbucket.org/mathildetech/devops-client/pkg/generic"
	"bitbucket.org/mathildetech/devops-client/pkg/transport"
	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	v1.NotImplement
	logger     logr.Logger
	client     *client.Confluence
	opts       *v1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ v1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() v1.ClientFactory {
	return func(opts *v1.Options) v1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

// GetProjects implements ProjectService
func (c *Client) GetProjects(ctx context.Context, page, pagesize string) (*v1.ProjectDataList, error) {
	var (
		projectPage     int64
		projectPageSize int64
		err             error
	)

	if page != "" && pagesize != "" {
		projectPage, err = strconv.ParseInt(page, 10, 32)
		projectPageSize, err = strconv.ParseInt(pagesize, 10, 32)
		if err != nil {
			return nil, err
		}
	} else {
		projectPage = 0
		projectPageSize = 500
	}

	start := projectPage * projectPageSize

	projectsParam := operations.
		NewGetSpaceParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithStart(start).
		WithLimit(projectPageSize)
	projectsOK, err := c.client.Operations.GetSpace(projectsParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := &v1.ProjectDataList{}
	for _, space := range projectsOK.Payload.Results {
		projectData := v1.ProjectData{}
		projectData.Annotations = map[string]string{
			"key":  space.Key,
			"id":   strconv.Itoa(int(space.ID)),
			"type": space.Type,
		}
		projectData.Name = space.Name
		result.Items = append(result.Items, projectData)
	}

	return result, nil
}

// CreateProject implements ProjectService
func (c *Client) CreateProject(ctx context.Context, projectName, projectDescription, projectLead, projectKey string) (*v1.ProjectData, error) {
	space := &models.SpaceCreate{
		Name: projectName,
		Key:  projectKey,
		Description: &models.SpaceCreateDescription{
			Plain: &models.SpaceCreateDescriptionPlain{
				Value: projectDescription,
			},
		},
	}
	projectParam := operations.
		NewPostSpaceParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithSpace(space)
	projectOK, err := c.client.Operations.PostSpace(projectParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	spacesuburl := fmt.Sprintf("display/%s", projectKey)
	projectSelf := fmt.Sprintf("%s/%s", strings.TrimRight(c.opts.BasicConfig.Host, "/"), strings.TrimLeft(spacesuburl, "/"))

	result := &v1.ProjectData{}
	result.Name = projectName
	result.Annotations = map[string]string{
		"self":        projectSelf,
		"id":          fmt.Sprintf("%d", projectOK.Payload.ID),
		"description": projectDescription,
	}

	return result, nil
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		return generic.CheckService(c.opts.BasicConfig.Host, nil)
	}
	return nil, errors.New("host config error")
}
