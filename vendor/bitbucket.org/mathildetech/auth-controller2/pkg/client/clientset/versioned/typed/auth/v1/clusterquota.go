/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// Code generated by client-gen. DO NOT EDIT.

package v1

import (
	v1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	scheme "bitbucket.org/mathildetech/auth-controller2/pkg/client/clientset/versioned/scheme"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// ClusterQuotasGetter has a method to return a ClusterQuotaInterface.
// A group's client should implement this interface.
type ClusterQuotasGetter interface {
	ClusterQuotas() ClusterQuotaInterface
}

// ClusterQuotaInterface has methods to work with ClusterQuota resources.
type ClusterQuotaInterface interface {
	Create(*v1.ClusterQuota) (*v1.ClusterQuota, error)
	Update(*v1.ClusterQuota) (*v1.ClusterQuota, error)
	UpdateStatus(*v1.ClusterQuota) (*v1.ClusterQuota, error)
	Delete(name string, options *metav1.DeleteOptions) error
	DeleteCollection(options *metav1.DeleteOptions, listOptions metav1.ListOptions) error
	Get(name string, options metav1.GetOptions) (*v1.ClusterQuota, error)
	List(opts metav1.ListOptions) (*v1.ClusterQuotaList, error)
	Watch(opts metav1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1.ClusterQuota, err error)
	ClusterQuotaExpansion
}

// clusterQuotas implements ClusterQuotaInterface
type clusterQuotas struct {
	client rest.Interface
}

// newClusterQuotas returns a ClusterQuotas
func newClusterQuotas(c *AuthV1Client) *clusterQuotas {
	return &clusterQuotas{
		client: c.RESTClient(),
	}
}

// Get takes name of the clusterQuota, and returns the corresponding clusterQuota object, and an error if there is any.
func (c *clusterQuotas) Get(name string, options metav1.GetOptions) (result *v1.ClusterQuota, err error) {
	result = &v1.ClusterQuota{}
	err = c.client.Get().
		Resource("clusterquotas").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of ClusterQuotas that match those selectors.
func (c *clusterQuotas) List(opts metav1.ListOptions) (result *v1.ClusterQuotaList, err error) {
	result = &v1.ClusterQuotaList{}
	err = c.client.Get().
		Resource("clusterquotas").
		VersionedParams(&opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested clusterQuotas.
func (c *clusterQuotas) Watch(opts metav1.ListOptions) (watch.Interface, error) {
	opts.Watch = true
	return c.client.Get().
		Resource("clusterquotas").
		VersionedParams(&opts, scheme.ParameterCodec).
		Watch()
}

// Create takes the representation of a clusterQuota and creates it.  Returns the server's representation of the clusterQuota, and an error, if there is any.
func (c *clusterQuotas) Create(clusterQuota *v1.ClusterQuota) (result *v1.ClusterQuota, err error) {
	result = &v1.ClusterQuota{}
	err = c.client.Post().
		Resource("clusterquotas").
		Body(clusterQuota).
		Do().
		Into(result)
	return
}

// Update takes the representation of a clusterQuota and updates it. Returns the server's representation of the clusterQuota, and an error, if there is any.
func (c *clusterQuotas) Update(clusterQuota *v1.ClusterQuota) (result *v1.ClusterQuota, err error) {
	result = &v1.ClusterQuota{}
	err = c.client.Put().
		Resource("clusterquotas").
		Name(clusterQuota.Name).
		Body(clusterQuota).
		Do().
		Into(result)
	return
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().

func (c *clusterQuotas) UpdateStatus(clusterQuota *v1.ClusterQuota) (result *v1.ClusterQuota, err error) {
	result = &v1.ClusterQuota{}
	err = c.client.Put().
		Resource("clusterquotas").
		Name(clusterQuota.Name).
		SubResource("status").
		Body(clusterQuota).
		Do().
		Into(result)
	return
}

// Delete takes name of the clusterQuota and deletes it. Returns an error if one occurs.
func (c *clusterQuotas) Delete(name string, options *metav1.DeleteOptions) error {
	return c.client.Delete().
		Resource("clusterquotas").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *clusterQuotas) DeleteCollection(options *metav1.DeleteOptions, listOptions metav1.ListOptions) error {
	return c.client.Delete().
		Resource("clusterquotas").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched clusterQuota.
func (c *clusterQuotas) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1.ClusterQuota, err error) {
	result = &v1.ClusterQuota{}
	err = c.client.Patch(pt).
		Resource("clusterquotas").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}
