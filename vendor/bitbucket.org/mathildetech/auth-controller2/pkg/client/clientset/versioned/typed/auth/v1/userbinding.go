/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// Code generated by client-gen. DO NOT EDIT.

package v1

import (
	v1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	scheme "bitbucket.org/mathildetech/auth-controller2/pkg/client/clientset/versioned/scheme"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// UserBindingsGetter has a method to return a UserBindingInterface.
// A group's client should implement this interface.
type UserBindingsGetter interface {
	UserBindings(namespace string) UserBindingInterface
}

// UserBindingInterface has methods to work with UserBinding resources.
type UserBindingInterface interface {
	Create(*v1.UserBinding) (*v1.UserBinding, error)
	Update(*v1.UserBinding) (*v1.UserBinding, error)
	UpdateStatus(*v1.UserBinding) (*v1.UserBinding, error)
	Delete(name string, options *metav1.DeleteOptions) error
	DeleteCollection(options *metav1.DeleteOptions, listOptions metav1.ListOptions) error
	Get(name string, options metav1.GetOptions) (*v1.UserBinding, error)
	List(opts metav1.ListOptions) (*v1.UserBindingList, error)
	Watch(opts metav1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1.UserBinding, err error)
	UserBindingExpansion
}

// userBindings implements UserBindingInterface
type userBindings struct {
	client rest.Interface
	ns     string
}

// newUserBindings returns a UserBindings
func newUserBindings(c *AuthV1Client, namespace string) *userBindings {
	return &userBindings{
		client: c.RESTClient(),
		ns:     namespace,
	}
}

// Get takes name of the userBinding, and returns the corresponding userBinding object, and an error if there is any.
func (c *userBindings) Get(name string, options metav1.GetOptions) (result *v1.UserBinding, err error) {
	result = &v1.UserBinding{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("userbindings").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of UserBindings that match those selectors.
func (c *userBindings) List(opts metav1.ListOptions) (result *v1.UserBindingList, err error) {
	result = &v1.UserBindingList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("userbindings").
		VersionedParams(&opts, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested userBindings.
func (c *userBindings) Watch(opts metav1.ListOptions) (watch.Interface, error) {
	opts.Watch = true
	return c.client.Get().
		Namespace(c.ns).
		Resource("userbindings").
		VersionedParams(&opts, scheme.ParameterCodec).
		Watch()
}

// Create takes the representation of a userBinding and creates it.  Returns the server's representation of the userBinding, and an error, if there is any.
func (c *userBindings) Create(userBinding *v1.UserBinding) (result *v1.UserBinding, err error) {
	result = &v1.UserBinding{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("userbindings").
		Body(userBinding).
		Do().
		Into(result)
	return
}

// Update takes the representation of a userBinding and updates it. Returns the server's representation of the userBinding, and an error, if there is any.
func (c *userBindings) Update(userBinding *v1.UserBinding) (result *v1.UserBinding, err error) {
	result = &v1.UserBinding{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("userbindings").
		Name(userBinding.Name).
		Body(userBinding).
		Do().
		Into(result)
	return
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().

func (c *userBindings) UpdateStatus(userBinding *v1.UserBinding) (result *v1.UserBinding, err error) {
	result = &v1.UserBinding{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("userbindings").
		Name(userBinding.Name).
		SubResource("status").
		Body(userBinding).
		Do().
		Into(result)
	return
}

// Delete takes name of the userBinding and deletes it. Returns an error if one occurs.
func (c *userBindings) Delete(name string, options *metav1.DeleteOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("userbindings").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *userBindings) DeleteCollection(options *metav1.DeleteOptions, listOptions metav1.ListOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("userbindings").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched userBinding.
func (c *userBindings) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1.UserBinding, err error) {
	result = &v1.UserBinding{}
	err = c.client.Patch(pt).
		Namespace(c.ns).
		Resource("userbindings").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}
