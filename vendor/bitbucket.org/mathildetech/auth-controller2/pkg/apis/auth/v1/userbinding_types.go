/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"context"
	"fmt"
	"strings"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	UserBindingKind = "UserBinding"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// UserBindingSpec defines the desired state of UserBinding
type UserBindingSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// UserBindingStatus defines the observed state of UserBinding
type UserBindingStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// UserBinding is the Schema for the userbindings API
// +k8s:openapi-gen=true
type UserBinding struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   UserBindingSpec   `json:"spec,omitempty"`
	Status UserBindingStatus `json:"status,omitempty"`
}

func (u *UserBinding) RoleLevel() string {
	if level, ok := u.Labels[RoleLevelLabelKey]; ok {
		return level
	}
	return ""
}

func (u *UserBinding) RoleName() string {
	if name, ok := u.Labels[RoleNameLabelKey]; ok {
		return name
	}
	return ""
}

func (u *UserBinding) UserEmail() string {
	if email, ok := u.Annotations[UserEmailAnnotationKey]; ok {
		return email
	}
	return ""
}

func (u *UserBinding) NamespaceCluster() string {
	if cluster, ok := u.Labels[ClusterLabelKey]; ok {
		return cluster
	}
	return ""
}

func (u *UserBinding) SetAnnotationUserEmail(email string) {

	if len(u.GetAnnotations()) == 0 {
		u.Annotations = make(map[string]string, 0)
	}
	u.Annotations[UserEmailAnnotationKey] = email
}

func (u *UserBinding) IsUserEmailExists() bool {
	if len(u.UserEmail()) == 0 {
		return false
	}
	return true
}

func (u *UserBinding) UserEmailName() string {
	return u.Labels[UserEmailLabelKey]
}

func (u *UserBinding) CurrentCluster() string {
	if cluster, ok := u.Annotations[CurrentClusterAnnotationKey]; ok {
		return cluster
	}
	return ""
}

func (u *UserBinding) IsCurrentClusterExists() bool {
	if len(u.CurrentCluster()) == 0 {
		return false
	}
	return true
}

func (u *UserBinding) SetCurrentCluster(cluster string) {

	if len(u.GetAnnotations()) == 0 {
		u.Annotations = make(map[string]string, 0)
	}
	u.Annotations[CurrentClusterAnnotationKey] = cluster
}

func (u *UserBinding) ProjectName() string {
	if project, ok := u.Labels[ProjectLabelKey]; ok {
		return project
	}
	return ""
}

func (u *UserBinding) NamespaceName() string {
	if ns, ok := u.Labels[NamespaceLabelKey]; ok {
		return ns
	}
	return ""
}

func (u *UserBinding) Validate() error {
	if level, ok := u.Labels[RoleLevelLabelKey]; level == "" || !ok {
		return fmt.Errorf("role level label is invalid")
	}

	if name, ok := u.Labels[RoleNameLabelKey]; name == "" || !ok {
		return fmt.Errorf("role name label is invalid")
	}

	if email, ok := u.Labels[UserEmailLabelKey]; email == "" || !ok {
		return fmt.Errorf("user email label is invalid")
	}

	switch u.RoleLevel() {
	case RoleLevelPlatform:
		if u.ProjectName() != "" || u.NamespaceName() != "" || u.NamespaceCluster() != "" {
			return fmt.Errorf("platform level parameters are invalid")
		}
	case RoleLevelProject:
		if u.ProjectName() == "" || u.NamespaceName() != "" || u.NamespaceCluster() != "" {
			return fmt.Errorf("project level parameters are invalid")
		}
	case RoleLevelNamespace:
		if u.ProjectName() == "" || u.NamespaceName() == "" || u.NamespaceCluster() == "" {
			return fmt.Errorf("namespace level parameters are invalid")
		}
	default:
		return fmt.Errorf("role level is invalid")
	}
	return nil
}

func (u *UserBinding) Update(client client.Client) error {
	return client.Update(context.TODO(), u)
}

func (u *UserBinding) ObjForCreate() *UserBinding {
	return &UserBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:        u.Name,
			Labels:      u.Labels,
			Annotations: u.Annotations,
		},
		Spec: u.Spec,
	}
}

func (u *UserBinding) ParseUserFromEmail() string {
	arr := strings.Split(u.UserEmailName(), ".")
	return arr[0]
}

func (u *UserBinding) InstanceForBusinessCluster() UserBinding {
	annotations := u.GetAnnotations()
	if len(annotations) == 0 {
		annotations = make(map[string]string, 0)
	}
	annotations[CurrentClusterAnnotationKey] = ClusterBusiness
	return UserBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:            u.Name,
			Labels:          u.GetLabels(),
			Annotations:     annotations,
			ResourceVersion: "",
		},
		Spec: u.Spec,
	}
}

func (u *UserBinding) IsNeedSync() bool {
	if currentCluster, ok := u.Annotations[CurrentClusterAnnotationKey]; ok && currentCluster == ClusterGlobal {
		return true
	}
	return false
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// UserBindingList contains a list of UserBinding
type UserBindingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []UserBinding `json:"items"`
}

func init() {
	SchemeBuilder.Register(&UserBinding{}, &UserBindingList{})
}
