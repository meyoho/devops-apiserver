// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dataselect

import (
	"strings"
	"time"
)

// ----------------------- Standard Comparable Types ------------------------
// These types specify how given value should be compared
// They all implement ComparableValueInterface
// You can convert basic types to these types to support auto sorting etc.
// If you cant find your type compare here you will have to implement it yourself :)

// StdComparableInt must be equal ints
type StdComparableInt int

// Compare compares with  the given value
func (v StdComparableInt) Compare(otherV ComparableValue) int {
	other := otherV.(StdComparableInt)
	return intsCompare(int(v), int(other))
}

// Contains returns if value is contained
func (v StdComparableInt) Contains(otherV ComparableValue) bool {
	return v.Compare(otherV) == 0
}

// StdEqualString strings that must be exactly the same
type StdEqualString string

// Compare compares with  the given value
func (v StdEqualString) Compare(otherV ComparableValue) int {
	other := otherV.(StdComparableString)
	return strings.Compare(string(v), string(other))
}

// Contains returns if value is contained
func (v StdEqualString) Contains(otherV ComparableValue) bool {
	return v.Compare(otherV) == 0
}

// StdComparableString only equal decoded UTF-8 values will return true
type StdComparableString string

// Compare compares with  the given value
func (v StdComparableString) Compare(otherV ComparableValue) int {
	other := otherV.(StdComparableString)
	return strings.Compare(string(v), string(other))
}

// Contains returns if value is contained
func (v StdComparableString) Contains(otherV ComparableValue) bool {
	other := otherV.(StdComparableString)
	return strings.EqualFold(string(v), string(other))
}

// StdLowerComparableString constructor for StdComparableString returning a lower case string
func StdLowerComparableString(val string) StdComparableString {
	return StdComparableString(strings.ToLower(val))
}

// StdCaseInSensitiveComparableString case insensitive wrapper of StdComparableString
type StdCaseInSensitiveComparableString string

// Compare compares with  the given value
func (v StdCaseInSensitiveComparableString) Compare(otherV ComparableValue) int {
	other := otherV.(StdCaseInSensitiveComparableString)
	return strings.Compare(strings.ToLower(string(v)), strings.ToLower(string(other)))
}

// Contains returns if value is contained
func (v StdCaseInSensitiveComparableString) Contains(otherV ComparableValue) bool {
	other := otherV.(StdComparableString)
	return strings.Contains(strings.ToLower(string(v)), strings.ToLower(string(other)))
}

// StdComparableRFC3339Timestamp takes RFC3339 Timestamp strings and compares them as TIMES. In case of time parsing error compares values as strings.
type StdComparableRFC3339Timestamp string

// Compare compares with  the given value
func (v StdComparableRFC3339Timestamp) Compare(otherV ComparableValue) int {
	other := otherV.(StdComparableRFC3339Timestamp)
	// try to compare as timestamp (earlier = smaller)
	selfTime, err1 := time.Parse(time.RFC3339, string(v))
	otherTime, err2 := time.Parse(time.RFC3339, string(other))

	if err1 != nil || err2 != nil {
		// in case of timestamp parsing failure just compare as strings
		return strings.Compare(string(v), string(other))
	}
	return ints64Compare(selfTime.Unix(), otherTime.Unix())
}

// Contains returns if value is contained
func (v StdComparableRFC3339Timestamp) Contains(otherV ComparableValue) bool {
	return v.Compare(otherV) == 0
}

// StdComparableTime time.Time implementation for ComperableValue
type StdComparableTime time.Time

// Compare compares with  the given value
func (v StdComparableTime) Compare(otherV ComparableValue) int {
	other := otherV.(StdComparableTime)
	return ints64Compare(time.Time(v).Unix(), time.Time(other).Unix())
}

// Contains returns if value is contained
func (v StdComparableTime) Contains(otherV ComparableValue) bool {
	return v.Compare(otherV) == 0
}

// StdExactString exact comparisson of strings using == operator for ComparableValue
type StdExactString string

// Compare compares with  the given value
func (v StdExactString) Compare(otherV ComparableValue) int {
	other := otherV.(StdExactString)
	return strings.Compare(string(v), string(other))
}

// Contains returns if value is contained
func (v StdExactString) Contains(otherV ComparableValue) bool {
	other := otherV.(StdComparableString)
	return string(v) == string(other)
}

// Int comparison functions. Similar to strings.Compare.
func intsCompare(a, b int) int {
	if a > b {
		return 1
	} else if a == b {
		return 0
	}
	return -1
}

func ints64Compare(a, b int64) int {
	if a > b {
		return 1
	} else if a == b {
		return 0
	}
	return -1
}

// StdComparableLabel label implementation of ComparableValue.
// supports multiple values split by comma ","
type StdComparableLabel string

// Compare compares with  the given value
func (v StdComparableLabel) Compare(otherV ComparableValue) int {
	other := otherV.(StdComparableLabel)
	return strings.Compare(string(v), string(other))
}

// Contains returns if value is contained
func (v StdComparableLabel) Contains(otherV ComparableValue) bool {
	other := string(otherV.(StdComparableString))
	split := strings.Split(string(v), ",")
	if len(split) == 0 {
		return false
	}
	for _, s := range split {
		if s == other {
			return true
		}
	}
	return false
}

// StdComparableStringIn comparable string in.
// Supports multiple values using ":" as a separator
// if any of the values is equal returns true
type StdComparableStringIn string

// Compare compares with  the given value
func (v StdComparableStringIn) Compare(otherV ComparableValue) int {
	other := otherV.(StdComparableStringIn)
	return strings.Compare(string(v), string(other))
}

// Contains returns if value is contained
func (v StdComparableStringIn) Contains(otherV ComparableValue) bool {
	cur := string(v)
	other := otherV.(StdComparableString)
	split := strings.Split(string(other), ":")
	if len(split) == 0 {
		return true
	}
	for _, s := range split {
		if s == cur {
			return true
		}
	}
	return false
}

// StdComparableContainsString if one string contains the other
type StdComparableContainsString string

// Compare compares with  the given value
func (v StdComparableContainsString) Compare(otherV ComparableValue) int {
	other := otherV.(StdComparableContainsString)
	return strings.Compare(string(v), string(other))
}

// Contains returns if value is contained
func (v StdComparableContainsString) Contains(otherV ComparableValue) bool {
	other := otherV.(StdComparableString)
	return strings.Contains(strings.ToLower(string(v)), strings.ToLower(string(other)))
}
