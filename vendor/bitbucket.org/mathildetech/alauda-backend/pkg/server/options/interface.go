package options

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/spf13/pflag"
)

// Optioner interface for all options
type Optioner interface {
	AddFlags(*pflag.FlagSet)
	ApplyFlags() []error
	ApplyToServer(server.Server) error
}
