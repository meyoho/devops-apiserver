package options

import (
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/emicklei/go-restful"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	flagEnableMetrics = "metrics"
)

const (
	configEnableMetrics = "monitoring.metrics"
)

// MetricsOptions holds the Debugging options.
type MetricsOptions struct {
	EnableMetrics bool
}

// NewMetricsOptions creates the default MetricsOptions object.
func NewMetricsOptions() *MetricsOptions {
	return &MetricsOptions{
		EnableMetrics: true,
	}
}

// AddFlags adds flags related to debugging for controller manager to the specified FlagSet.
func (o *MetricsOptions) AddFlags(fs *pflag.FlagSet) {
	if o == nil {
		return
	}

	fs.Bool(flagEnableMetrics, o.EnableMetrics,
		"Enable metrics for prometheus web interface host:port/metrics")
	_ = viper.BindPFlag(configEnableMetrics, fs.Lookup(flagEnableMetrics))
}

// ApplyFlags parsing parameters from the command line or configuration file
// to the options instance.
func (o *MetricsOptions) ApplyFlags() []error {
	var errs []error

	o.EnableMetrics = viper.GetBool(configEnableMetrics)

	return errs
}

// ApplyToServer apply options to server
func (o *MetricsOptions) ApplyToServer(server server.Server) (err error) {
	if o == nil || !o.EnableMetrics {
		return
	}
	for _, metric := range metrics {
		prometheus.MustRegister(metric)
	}

	server.Container().Handle("/metrics/", http.HandlerFunc(redirectTo("/metrics")))
	server.Container().Handle("/metrics", prometheus.Handler())
	server.Container().Filter(o.Filter)
	return
}

// Filter middleware for metrics
func (o *MetricsOptions) Filter(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	start := time.Now()
	chain.ProcessFilter(req, res)
	elapsed := time.Now().Sub(start)
	elapsedMicroseconds := float64(elapsed / time.Microsecond)
	userAgent := cleanUserAgent(req.Request.UserAgent())
	contentType := res.ResponseWriter.Header().Get("content-type")
	path := strings.TrimSuffix(req.Request.URL.Path, "/")

	// request count
	requestCounter.WithLabelValues(req.Request.Method, path, userAgent, contentType, codeToString(res.StatusCode())).Inc()
	// request latency
	requestLatencies.WithLabelValues(req.Request.Method, path).Observe(elapsedMicroseconds)
	requestLatenciesSummary.WithLabelValues(req.Request.Method, path).Observe(elapsedMicroseconds)
	// response body from GET requests
	if req.Request.Method == http.MethodGet {
		responseSizes.WithLabelValues(req.Request.Method, path).Observe(float64(res.ContentLength()))
	}
}

// resettableCollector is the interface implemented by prometheus.MetricVec
// that can be used by Prometheus to collect metrics and reset their values.
type resettableCollector interface {
	prometheus.Collector
	Reset()
}

var (
	// from k8s.io/apiserver/pkg/endpoints/metrics/metrics.go
	requestCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "apiserver_request_count",
			Help: "Counter of apiserver requests broken out for each verb, API resource, client, and HTTP response contentType and code.",
		},
		[]string{"verb", "path", "client", "contentType", "code"},
	)
	longRunningRequestGauge = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "apiserver_longrunning_gauge",
			Help: "Gauge of all active long-running apiserver requests broken out by verb, API resource, and scope. Not all requests are tracked this way.",
		},
		[]string{"verb", "path"},
	)
	requestLatencies = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "apiserver_request_latencies",
			Help: "Response latency distribution in microseconds for each verb, resource and subresource.",
			// Use buckets ranging from 125 ms to 8 seconds.
			Buckets: prometheus.ExponentialBuckets(125000, 2.0, 7),
		},
		[]string{"verb", "path"},
	)
	requestLatenciesSummary = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "apiserver_request_latencies_summary",
			Help: "Response latency summary in microseconds for each verb, resource and subresource.",
			// Make the sliding window of 5h.
			// TODO: The value for this should be based on our SLI definition (medium term).
			MaxAge: 5 * time.Hour,
		},
		[]string{"verb", "path"},
	)
	responseSizes = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "apiserver_response_sizes",
			Help: "Response size distribution in bytes for each verb, resource, subresource and scope (namespace/cluster).",
			// Use buckets ranging from 1000 bytes (1KB) to 10^9 bytes (1GB).
			Buckets: prometheus.ExponentialBuckets(1000, 10.0, 7),
		},
		[]string{"verb", "path"},
	)
	kubectlExeRegexp = regexp.MustCompile(`^.*((?i:kubectl\.exe))`)

	metrics = []resettableCollector{
		requestCounter,
		longRunningRequestGauge,
		requestLatencies,
		requestLatenciesSummary,
		responseSizes,
	}
)

// from k8s.io/apiserver/pkg/endpoints/metrics/metrics.go
func cleanUserAgent(ua string) string {
	// We collapse all "web browser"-type user agents into one "browser" to reduce metric cardinality.
	if strings.HasPrefix(ua, "Mozilla/") {
		return "Browser"
	}
	// If an old "kubectl.exe" has passed us its full path, we discard the path portion.
	ua = kubectlExeRegexp.ReplaceAllString(ua, "$1")
	return ua
}

// Small optimization over Itoa
// from k8s.io/apiserver/pkg/endpoints/metrics/metrics.go
func codeToString(s int) string {
	switch s {
	case 100:
		return "100"
	case 101:
		return "101"

	case 200:
		return "200"
	case 201:
		return "201"
	case 202:
		return "202"
	case 203:
		return "203"
	case 204:
		return "204"
	case 205:
		return "205"
	case 206:
		return "206"

	case 300:
		return "300"
	case 301:
		return "301"
	case 302:
		return "302"
	case 304:
		return "304"
	case 305:
		return "305"
	case 307:
		return "307"

	case 400:
		return "400"
	case 401:
		return "401"
	case 402:
		return "402"
	case 403:
		return "403"
	case 404:
		return "404"
	case 405:
		return "405"
	case 406:
		return "406"
	case 407:
		return "407"
	case 408:
		return "408"
	case 409:
		return "409"
	case 410:
		return "410"
	case 411:
		return "411"
	case 412:
		return "412"
	case 413:
		return "413"
	case 414:
		return "414"
	case 415:
		return "415"
	case 416:
		return "416"
	case 417:
		return "417"
	case 418:
		return "418"

	case 500:
		return "500"
	case 501:
		return "501"
	case 502:
		return "502"
	case 503:
		return "503"
	case 504:
		return "504"
	case 505:
		return "505"

	case 428:
		return "428"
	case 429:
		return "429"
	case 431:
		return "431"
	case 511:
		return "511"

	default:
		return strconv.Itoa(s)
	}
}
