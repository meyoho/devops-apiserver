package domain

import (
	"fmt"

	"bitbucket.org/mathildetech/jenkinsfilext/v2/domain/arguments"
	"bitbucket.org/mathildetech/jenkinsfilext/v2/domain/common"
)

// BuildTemplate will build PipelineTemplateSpec by simple template spec
// taskRefs is map of < taskTemplateKind/taskTemplateName, PipelineTaskTemplate >
func BuildTemplate(spec PipelineTemplateSpec, taskRefs map[string]PipelineTaskTemplate) (PipelineTemplateSpec, error) {

	for _, stage := range spec.Stages {
		for _, task := range stage.Tasks {
			templateIdentity := task.Kind + "/" + task.Name

			if _, ok := taskRefs[templateIdentity]; !ok {
				return spec, fmt.Errorf("not contains task template %s", templateIdentity)
			}

			taskTemplate := taskRefs[templateIdentity]
			section := buildArgSection(task.GetID(), taskTemplate)
			spec.Arguments = append(spec.Arguments, section)
		}
	}

	return spec, nil
}

func buildArgSection(taskIdentity string, taskTemplate PipelineTaskTemplate) arguments.ArgSection {
	section := arguments.ArgSection{
		DisplayName: common.MulitLangValue{
			ZH_CN: taskTemplate.Annotations[AnnotationDisplayNameCN],
			EN:    taskTemplate.Annotations[AnnotationDisplayNameEN],
		},
		Items: make([]arguments.ArgItem, 0, len(taskTemplate.Spec.Arguments)),
	}

	for _, arg := range taskTemplate.Spec.Arguments {
		arg.Binding = []string{
			fmt.Sprintf("%s.args.%s", taskIdentity, arg.Name),
		}
		argName := BuildArgName(taskIdentity, arg.Name)
		arg.Name = argName
		section.Items = append(section.Items, arg)
	}
	return section
}

func BuildArgName(taskIdentity string, argName string) string {
	return fmt.Sprintf("%s__%s", taskIdentity, argName)
}
