package domain

import (
	"fmt"

	"bitbucket.org/mathildetech/jenkinsfilext/v2/domain/common"
)

func ValidateTriggers(triggers interface{}) error {
	if triggers == nil {
		return nil
	}

	_, isMap := triggers.(map[string]interface{})

	if isMap {
		return nil
	}

	_, isString := triggers.(string)
	if isString {
		return nil
	}
	return common.NewTemplateDefinitionError(fmt.Sprintf("triggers should be map[string]interface{} but %T", triggers), nil)
}
