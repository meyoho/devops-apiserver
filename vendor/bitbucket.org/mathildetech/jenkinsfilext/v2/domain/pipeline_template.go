package domain

import (
	"fmt"
	"strings"

	"bitbucket.org/mathildetech/jenkinsfilext/v2/domain/arguments"
	"bitbucket.org/mathildetech/jenkinsfilext/v2/domain/common"
	"github.com/mitchellh/mapstructure"

	"encoding/json"

	"bitbucket.org/mathildetech/jenkinsfilext/v2/goutils"
	"bitbucket.org/mathildetech/jenkinsfilext/v2/jenkinsfile"
)

type PipelineTemplateSpec struct {
	Engine   string      `json:"engine"`
	Triggers interface{} `json:"triggers,omitempty" mapstructure:"triggers" yaml:"triggers,omitempty"`
	WithSCM  bool        `json:"withSCM"  mapstructure:"withSCM" yaml:"withSCM"`
	// Agent can accepts three formats
	//  1. string  eg. agent: "java"
	//  2. jenkinsfile.Agent  eg. agent: { "label":"java", "raw": "docker{what ever}" } , if set label and raw at same time, we will use raw
	//  3. map[string]interface{} that could parse to jenkinsfile.Agent
	Agent        interface{}           `json:"agent,omitempty" mapstructure:"agent" yaml:"agent" yaml:"agent,omitempty"`
	Stages       []*Stage              `json:"stages"`
	Post         map[string][]*Task    `json:"post,omitempty" yaml:"post,omitempty"`
	ConstValues  *ConstValues          `json:"values,omitempty"  mapstructure:"values" yaml:"values,omitempty"`
	Options      *jenkinsfile.Options  `json:"options,omitempty" yaml:"options,omitempty"`
	Arguments    arguments.ArgSections `json:"arguments,omitempty" yaml:"arguments,omitempty"`
	Environments []jenkinsfile.EnvVar  `json:"environments,omitempty" yaml:"environments,omitempty"`
}

// SCMInfo is clone info
//
// Deprecated: move this field to arguments value
type SCMInfo struct {
	Type           scmType
	RepositoryPath string
	CredentialsID  string
	Branch         string
}

type scmType string

// SCMTypeEnum enum of SCMType
var SCMTypeEnum = struct {
	GIT scmType
	SVN scmType
}{
	GIT: "GIT",
	SVN: "SVN",
}

type ConstValues struct {
	Tasks map[string]*TaskConstValue `json:"tasks"  yaml:"tasks,omitempty"`
}

type TaskConstValue struct {
	Args    map[string]interface{} `json:"args" yaml:"args,omitempty"`
	Options *jenkinsfile.Options   `json:"options" yaml:"options,omitempty"`
	Approve *jenkinsfile.Approve   `json:"approve" yaml:"approve,omitempty"`
}

type Stage struct {
	Name       string                `json:"name"`
	Display    common.MulitLangValue `json:"display,omitempty"`
	Conditions *jenkinsfile.When     `json:"conditions" yaml:"conditions,omitempty"`
	Tasks      []*Task               `json:"tasks"`
}

func (s *Stage) GetDisplayName() string {
	if s.Display.EN != "" {
		return s.Display.EN
	}

	if s.Display.ZH_CN != "" {
		return s.Display.ZH_CN
	}

	return ""
}

func (s *Stage) validateDefinition() error {
	if strings.TrimSpace(s.Name) == "" {
		return common.NewTemplateDefinitionError("stage.name should not be empty", nil)
	}

	if s.Tasks == nil || len(s.Tasks) == 0 {
		return common.NewTemplateDefinitionError(fmt.Sprintf("stage `%s`'s tasks should be one at least", s.Name), nil)
	}
	return nil
}

type Task struct {
	// Kind kind of takstemplate
	Kind string `json:"kind"`
	// Name  name of tasktemplate reference
	Name string `json:"name"`
	// ID identity of current task, it should be string
	ID string `json:"id,omitempty"`
	// Display display of current task
	Display common.MulitLangValue `json:"display,omitempty"`
	Agent   interface{}           `json:"agent,omitempty" yaml:"agent,omitempty"`
	// Type Deprecated: it will replaced by kind and name TODO: should be deleted after migrated
	Type         string               `json:"type"`
	Options      *jenkinsfile.Options `json:"options,omitempty" yaml:"options,omitempty"`
	Conditions   *jenkinsfile.When    `json:"conditions,omitempty" yaml:"conditions,omitempty"`
	Approve      *jenkinsfile.Approve `json:"approve,omitempty" yaml:"approve,omitempty"`
	Environments []jenkinsfile.EnvVar `json:"environments,omitempty" yaml:"environments,omitempty"`
	Relation     *common.Relation     `json:"relation,omitempty" yaml:"relation,omitempty"`

	taskTemplateSpec      *TaskTemplateSpec      `json:"-" yaml:"-"`
	taskTemplateArgValues map[string]interface{} `json:"-" yaml:"-"`
	meaningfull           bool                   `json:"-" yaml:"-"`
}

func (task *Task) GetID() string {

	if task.ID != "" {
		return task.ID
	}
	// Genernally, task.id will be same as task.name (task template name without version suffix)
	//
	// In feature of acp 2.0, I have make a mistake to use display name as binding's name
	// when creating pipelinetemplate, we did not have a appropriate field to save id at that time.
	// so we should return displayName first but not task.name
	// task.name is trustless， it is the task template's name that may be having special version
	// we should delete this logic after all data migrated by template sync
	if task.GetDisplayName() != "" {
		return task.GetDisplayName()
	}

	return task.Name
}

func (task *Task) GetDisplayName() string {
	if task.Display.EN != "" {
		return task.Display.EN
	}

	if task.Display.ZH_CN != "" {
		return task.Display.ZH_CN
	}

	if task.ID != "" {
		return task.ID
	}

	return task.Name
}

func (task *Task) IsMeaningful(argumentsValues map[string]interface{}) bool {
	meaningful := task.Relation.IsMathcShowAction(argumentsValues)
	// fmt.Printf("task `%s` meaningful = %t \n", task.Name, meaningful)
	return meaningful
}

func (t *Task) validateDefinition() error {
	errs := common.Errors{}
	if t.Name == "" {
		errs = append(errs, common.NewTemplateDefinitionError("ref task name shoule not be empty", nil))
	}

	if err := ValidateAgent(t.Agent); err != nil {
		errs = append(errs, err)
	}

	err := validateResourceName(t.Name)
	if err != nil {
		errs = append(errs, err)
	}

	if t.GetDisplayName() == "" {
		errs = append(errs, common.NewTemplateDefinitionError(fmt.Sprintf("ref task %s display should not be empty", t.Name), nil))
	}

	if len(errs) == 0 {
		return nil
	}

	return errs
}

func unboxToInt(value interface{}) (int, error) {
	v, ok := value.(int)
	if !ok {
		return 0, fmt.Errorf("%v is not int", value)
	}
	return v, nil
}

func (t *Task) applyConstValue(constValues *TaskConstValue) {
	if constValues == nil {
		return
	}

	if constValues.Options != nil {
		if t.Options == nil {
			t.Options = &jenkinsfile.Options{}
		}
		t.Options.Timeout = constValues.Options.Timeout
	}

	if constValues.Approve != nil {
		if t.Approve == nil {
			t.Approve = &jenkinsfile.Approve{}
		}
		t.Approve.Timeout = constValues.Approve.Timeout
	}

	if constValues.Args != nil && len(constValues.Args) != 0 {
		for key, value := range constValues.Args {
			if t.taskTemplateArgValues == nil {
				t.taskTemplateArgValues = map[string]interface{}{}
			}
			t.taskTemplateArgValues[key] = value
		}
	}
}

func (t *Task) assignTemplateArgValues(templateArgValues map[string]interface{}) {
	t.taskTemplateArgValues = goutils.MergeMap(templateArgValues, t.taskTemplateArgValues)
}

func (t *Task) assignSystemArgValue(systemArg interface{}) {
	if systemArg != nil {
		t.taskTemplateArgValues = goutils.MergeMap(t.taskTemplateArgValues, map[string]interface{}{
			SystemArgKey: systemArg,
		})
	}
}

func (t *Task) assignArgValues(argValues map[string]interface{}) error {
	for path, value := range argValues {
		err := t.assignArgValueByPath(path, value)
		if err != nil {
			return err
		}
	}
	return nil
}

func (t *Task) assignArgValueByPath(path string, value interface{}) error {
	switch path {
	case "options.timeout":
		v, err := unboxToInt(value)
		if err != nil {
			return common.NewValidateError(fmt.Sprintf("%s's value %v should be int, but got %T", path, value, value), nil)
		}
		t.Options.Timeout = v
	case "approve.timeout":
		v, err := unboxToInt(value)
		if err != nil {
			return err
		}
		t.Options.Timeout = v
	}
	return nil
}

func (t *Task) toJenkinsfileStage() (*jenkinsfile.Stage, error) {
	taskScriptBody, err := t.taskTemplateSpec.Render(t.taskTemplateArgValues)

	if err != nil {
		return nil, err
	}

	var agent = t.Agent
	if agent == nil {
		agent = t.taskTemplateSpec.Agent
	} // if set agent in pipeline template , just use it. if not set in pipeline template but set in task template, use that.

	jenkinsStage := &jenkinsfile.Stage{
		Name:         t.GetDisplayName(),
		Agent:        agent,
		Options:      t.Options,
		When:         t.Conditions,
		Approve:      t.Approve,
		Environments: t.Environments,
		Steps: &jenkinsfile.Steps{
			ScriptsContent: taskScriptBody,
		},
	}

	return jenkinsStage, err
}

// ValidateDefinition validate template define
func (spec *PipelineTemplateSpec) ValidateDefinition() error {
	errs := common.Errors{}

	err := ValidateTriggers(spec.Triggers)
	if err != nil {
		errs = append(errs, err)
	}

	err = ValidateAgent(spec.Agent)
	if err != nil {
		errs = append(errs, err)
	}

	err = spec.validateStagesDefinition()
	if err != nil {
		errs = append(errs, err)
	}

	err = spec.validateTasksDefinition()
	if err != nil {
		errs = append(errs, err)
	}

	for _, argItem := range spec.Arguments.AllArgItems() {
		err = argItem.ValidateDefinition()
		if err != nil {
			errs = append(errs, err)
		}
	}

	if len(errs) > 0 {
		return errs
	}
	return nil
}

func (spec *PipelineTemplateSpec) validateStagesDefinition() error {
	errs := common.Errors{}

	if spec.Stages == nil || len(spec.Stages) == 0 {
		return common.NewTemplateDefinitionError(fmt.Sprint("stages should be one at least"), nil)
	}

	for _, stage := range spec.Stages {
		err := stage.validateDefinition()
		if err != nil {
			errs = append(errs, err)
		}
	}

	if len(errs) == 0 {
		return nil
	}
	return errs
}

func (spec *PipelineTemplateSpec) validateTasksDefinition() error {
	errs := common.Errors{}

	taskIDMap := map[string]interface{}{}

	tasks := spec.allTasks()
	if tasks == nil || len(tasks) == 0 {
		return common.NewTemplateDefinitionError(fmt.Sprint("tasks should be one at least"), nil)
	}

	for _, task := range tasks {
		if _, ok := taskIDMap[task.GetID()]; ok {
			errs = append(errs, common.NewTemplateDefinitionError(fmt.Sprintf("task id :%s should be unique, same as field of `name` when `id` is empty", task.GetID()), nil))
		} else {
			taskIDMap[task.GetID()] = struct{}{}
		}
		err := task.validateDefinition()
		if err != nil {
			errs = append(errs, err)
		}
	}

	if len(errs) == 0 {
		return nil
	}
	return errs
}

//ValidateValue validate values
func (spec *PipelineTemplateSpec) ValidateValue(argumentsValues map[string]interface{}) error {
	argItems := spec.Arguments.AllArgItems()
	argItemsMap := make(map[string]arguments.ArgItem, len(argItems))
	for _, argItem := range argItems {
		argItemsMap[argItem.Name] = argItem
	}

	errs := common.Errors{}
	for argName, value := range argumentsValues {
		if argItem, ok := argItemsMap[argName]; ok {
			if !argItem.IsMeaningful(argumentsValues) {
				fmt.Printf("arg `%s` is not meaningful , skip validate value \n", argItem.Name)
				continue
			}

			err := argItem.ValidateValue(value)
			if err != nil {
				errs = append(errs, err)
			}
		}
	}

	if len(errs) == 0 {
		return nil
	}
	return errs
}

//Render redner PipelineTemplateSpec to jenkinsfile content
// taskTemplatesRef: map< identityOfTemplate, TaskTemplateSpec >
// the identityOfTemplate format is taskTemplateKind/taskTemplateName , eg. ClusterPipelineTaskTemplate/clone
func (spec *PipelineTemplateSpec) Render(taskTemplatesRef map[string]TaskTemplateSpec, argumentsValues map[string]interface{}, scm *SCMInfo) (string, error) {
	return spec.getRenderEngine()(taskTemplatesRef, argumentsValues, scm)
}

type pipelineTemplateRenderEngine func(taskTemplatesRef map[string]TaskTemplateSpec, argumentsValues map[string]interface{}, scm *SCMInfo) (string, error)

func (spec *PipelineTemplateSpec) getRenderEngine() pipelineTemplateRenderEngine {
	switch spec.Engine {

	default: //default is graph
		{
			return func(taskTemplatesRef map[string]TaskTemplateSpec, argumentsValues map[string]interface{}, scm *SCMInfo) (string, error) {
				return spec.graphRender(taskTemplatesRef, argumentsValues, scm)
			}
		}
	}
}

func (spec *PipelineTemplateSpec) graphRender(taskTemplatesRef map[string]TaskTemplateSpec, argumentsValues map[string]interface{}, scm *SCMInfo) (string, error) {

	err := spec.ValidateDefinition()
	if err != nil {
		return "", err
	}

	// merge default values to argumentsValue
	defaultValues := spec.getDefaultValues()
	argumentsValues = goutils.MergeMap(defaultValues, argumentsValues)

	err = spec.ValidateValue(argumentsValues)
	if err != nil {
		return "", err
	}

	//scm is fixex information
	// it will not be used in latest version
	// if spec.WithSCM {
	// 	argumentsValues[CloneTaskTemplateArgName] = scm
	// 	spec.addSCMArg()
	// }

	// append task template spec reference
	err = spec.appendTaskTemplateSpecRef(taskTemplatesRef)
	if err != nil {
		return "", err
	}

	// apply const values
	spec.applyConstValues()

	// apply pipeline values , like agent
	err = spec.applyPipelineValues(argumentsValues) //
	if err != nil {
		return "", err
	}

	// assign value to all tasks arguments
	spec.assignValuesToEachTask(argumentsValues)

	// mark the task that meaningful
	spec.markMeaningfulTask(argumentsValues)

	pipeline, err := spec.parseToJenkinsfilePipeline()
	if err != nil {
		fmt.Printf("parse to jenkinsfile pipeline error:%#v", err)
		return "", err
	}

	return pipeline.Render()
}

func (spec *PipelineTemplateSpec) addSCMArg() {
	if spec.Arguments == nil || len(spec.Arguments) == 0 {
		spec.Arguments = arguments.ArgSections{
			arguments.ArgSection{
				Items: []arguments.ArgItem{},
			},
		}
	}

	scmArgItem := arguments.ArgItem{
		Name: "SCM",
		Schema: &arguments.ArgItemSchema{
			Type: "object",
		},
		Binding: []string{
			"Clone.args.SCM",
		},
		Required: true,
	}

	spec.Arguments[0].Items = append(spec.Arguments[0].Items, scmArgItem)
}

// applyPipelineValues will set values like `agent` to pipeline that passed from client user in arguments
func (spec *PipelineTemplateSpec) applyPipelineValues(argumentsValues map[string]interface{}) error {
	pipelineArgsValues, err := getPipelineArgValues(argumentsValues)
	if err != nil {
		return err
	}
	if pipelineArgsValues == nil {
		return nil
	}
	if pipelineArgsValues.Agent != nil {
		spec.Agent = pipelineArgsValues.Agent
	}
	if pipelineArgsValues.Options != nil {
		spec.Options = pipelineArgsValues.Options
	}
	return nil
}

func (spec *PipelineTemplateSpec) getDefaultValues() (defaultValues map[string]interface{}) {
	defaultValues = map[string]interface{}{}
	allArgItems := spec.Arguments.AllArgItems()
	for _, argItem := range allArgItems {
		if argItem.Default != nil {
			defaultValues[argItem.Name] = argItem.Default
		}
	}

	return
}

func (spec *PipelineTemplateSpec) applyConstValues() {
	if spec.ConstValues == nil {
		return
	}

	if spec.ConstValues.Tasks == nil || len(spec.ConstValues.Tasks) == 0 {
		return
	}

	allTasks := spec.allTasks()
	for _, t := range allTasks {
		t.applyConstValue(spec.ConstValues.Tasks[t.GetID()])
	}
}

func (spec *PipelineTemplateSpec) parseToJenkinsfilePipeline() (*jenkinsfile.Pipeline, error) {
	jenkinsfileStages, err := spec.getJenkinsfileStages()
	if err != nil {
		fmt.Printf("parse task template script body error :%v\n", err)
		return nil, err
	}
	jenkinsfilePost, err := spec.getJenkinsfilePost()
	if err != nil {
		fmt.Printf("parse task template script body in post error :%v\n", err)
		return nil, err
	}

	return &jenkinsfile.Pipeline{
		Triggers:     spec.Triggers,
		Options:      spec.Options,
		Agent:        spec.Agent,
		Environments: spec.Environments,
		Stages:       jenkinsfileStages,
		Post:         jenkinsfilePost,
	}, nil
}

func (spec *PipelineTemplateSpec) getJenkinsfileStages() ([]*jenkinsfile.Stage, error) {
	errs := common.Errors{}

	jenkinsStages := []*jenkinsfile.Stage{}
	for _, stage := range spec.Stages {
		if len(stage.Tasks) == 1 {
			if stage.Tasks[0].meaningfull == false {
				fmt.Printf("task %s is not meaningful, will skip to render it\n", stage.Tasks[0].Name)
				continue
			}

			jenkinsStage, err := stage.Tasks[0].toJenkinsfileStage()
			if err != nil {
				fmt.Printf("render task %s script body error:%#v", stage.Tasks[0].Name, err)
				errs = append(errs, err)
				continue
			}
			jenkinsStages = append(jenkinsStages, jenkinsStage)
		}

		if len(stage.Tasks) > 1 {
			jenkinsStage := &jenkinsfile.Stage{
				Name:   stage.GetDisplayName(),
				When:   stage.Conditions,
				Stages: []*jenkinsfile.Stage{},
			}

			if jenkinsStage.Name == "" {
				jenkinsStage.Name = stage.Name
			}

			for _, parallelTask := range stage.Tasks {
				if parallelTask.meaningfull == false {
					fmt.Printf("task %s is not meaningful, will skip to render it\n", parallelTask.Name)
					continue
				}

				pStage, err := parallelTask.toJenkinsfileStage()
				if err != nil {
					fmt.Printf("render task %s script body error:%#v", parallelTask.Name, err)
					errs = append(errs, err)
					continue
				}
				jenkinsStage.Stages = append(jenkinsStage.Stages, pStage)
			}

			jenkinsStages = append(jenkinsStages, jenkinsStage)
		}
	}

	if len(errs) > 0 {
		return jenkinsStages, errs
	}

	return jenkinsStages, nil
}

func (spec *PipelineTemplateSpec) getJenkinsfilePost() ([]*jenkinsfile.PostCondition, error) {
	errs := common.Errors{}

	jenkinsPost := []*jenkinsfile.PostCondition{}
	for name, tasks := range spec.Post {
		var scripts string
		var hasTask = false
		for _, task := range tasks {
			if task.meaningfull == false {
				fmt.Printf("task %s is not meaningful, will skip to render it\n", task.GetID())
				continue
			}
			hasTask = true
			taskScriptBody, err := task.taskTemplateSpec.Render(task.taskTemplateArgValues)
			if err != nil {
				fmt.Printf("render task %s script body error:%#v", task.Name, err)
				errs = append(errs, err)
				continue
			}
			scripts += taskScriptBody
		}

		if hasTask {
			postCondition := &jenkinsfile.PostCondition{
				Name:    name,
				Scripts: scripts,
			}
			jenkinsPost = append(jenkinsPost, postCondition)
		}
	}

	// add cleanWorkspace if post is empty
	if spec.Post == nil {
		postCondition := &jenkinsfile.PostCondition{
			Name:    jenkinsfile.POST_ALWAYS,
			Scripts: CleanWorkspaceScript,
		}
		jenkinsPost = append(jenkinsPost, postCondition)
	}

	if len(errs) > 0 {
		return jenkinsPost, errs
	}

	return jenkinsPost, nil
}

var CleanWorkspaceScript = `
			script{
				echo "clean up workspace"
				deleteDir()
			}
`

type taskValues struct {
	// task  template 中定义的参数的值
	templateArgValues map[string]interface{}
	// pipeline 中引用 task时，的参数的值，例如options.timeout 等
	argValues map[string]interface{}
}

var SystemArgKey = "_system_"
var PipelineArgKey = "_pipeline_"

// 我们将 arguments的参数分为两类
// 一类是用户在用户界面 通过动态表单 填入的参数，这些参数，都是 模板中定义的参数
// 另外一类，是跟alauda system 相关的参数，例如，project name, namespace, 甚至 当前的 cluster  name 等参数
// 这些参数为 system 参数， 放在 key=_system_ 的键值对中。
// 将来的扩展，还可以 多一种 _jenkins_的参数， 例如 是否支持并行构建，保存的历史记录个数等等。 这些参数 不需要通过动态表单生成
// 他们的个数和含义是在平台本身就已经限定好的。不由用户在创建模板时定义。
func getSystemArgumentsValues(argumentsValues map[string]interface{}) (interface{}, error) {
	v, ok := argumentsValues[SystemArgKey]
	if !ok {
		return nil, nil
	}

	if jsonStr, ok := v.(string); ok {
		// it should be json
		values := map[string]interface{}{}
		err := json.Unmarshal([]byte(jsonStr), &values)
		if err != nil {
			fmt.Printf("Unmarshall to map[string]interface{} error:%s, string is: %s", err.Error(), jsonStr)
			return values, err
		}

		return values, nil
	}

	return v, nil
}

func getPipelineArgValues(argumentsValues map[string]interface{}) (*PipelineTemplateSpec, error) {
	v, ok := argumentsValues[PipelineArgKey]
	if !ok {
		return nil, nil
	}

	pipelineValues := &PipelineTemplateSpec{}

	if jsonStr, ok := v.(string); ok {
		// it should be json
		err := json.Unmarshal([]byte(jsonStr), pipelineValues)
		if err != nil {
			fmt.Printf("Unmarshall to PipelineTemplateSpec error:%s, string is: %s", err.Error(), jsonStr)
			return nil, err
		}

		return pipelineValues, nil
	}

	err := mapstructure.Decode(v, pipelineValues)
	if err != nil {
		return nil, err
	}
	return pipelineValues, nil
}

func (spec *PipelineTemplateSpec) assignValuesToEachTask(argumentsValues map[string]interface{}) error {

	var allArgItems = []arguments.ArgItem{}
	for _, argSection := range spec.Arguments {
		allArgItems = append(allArgItems, argSection.Items...)
	}

	var tasksValuesMap = map[string]taskValues{}

	for _, argItem := range allArgItems {
		for _, binding := range argItem.Binding {
			segments := strings.Split(binding, ".")

			if len(segments) <= 1 {
				return common.NewTemplateDefinitionError(fmt.Sprintf("Pipeline template error, %s's Binding format:%s error", argItem.Name, binding), nil)
			}

			// segments is allowed start with task.GetID()
			var taskID = segments[0]
			if _, ok := tasksValuesMap[taskID]; !ok {
				tasksValuesMap[taskID] = taskValues{
					templateArgValues: map[string]interface{}{},
					argValues:         map[string]interface{}{},
				}
			}

			scope := segments[1]
			switch scope {
			case "args":
				{
					fieldName := segments[2]
					//debug it
					//fmt.Printf("task name :%s , fieldName:%s value:%v\n", taskName, fieldName, argumentsValues[argItem.Name])
					tasksValuesMap[taskID].templateArgValues[fieldName] = argumentsValues[argItem.Name]
				}
			default:
				{
					fieldPath := strings.Join(segments[1:], ".")
					tasksValuesMap[taskID].argValues[fieldPath] = argumentsValues[argItem.Name]
				}
			}
		}
	}

	systemValue, err := getSystemArgumentsValues(argumentsValues)
	if err != nil {
		return err
	}

	// assignValues
	for _, stage := range spec.Stages {
		for _, task := range stage.Tasks {
			taskID := task.GetID()

			task.assignTemplateArgValues(tasksValuesMap[taskID].templateArgValues)
			task.assignSystemArgValue(systemValue)
			task.assignArgValues(tasksValuesMap[taskID].argValues)
		}
	}

	for _, tasks := range spec.Post {
		for _, task := range tasks {
			taskID := task.GetID()
			task.assignTemplateArgValues(tasksValuesMap[taskID].templateArgValues)
			task.assignSystemArgValue(systemValue)
			task.assignArgValues(tasksValuesMap[taskID].argValues)
		}
	}

	return nil
}

// func (spec *PipelineTemplateSpec) findTask(taskName string) *Task {
// 	for _, stage := range spec.Stages {
// 		for _, task := range stage.Tasks {
// 			if task.Name == taskName {
// 				return task
// 			}
// 		}
// 	}

// 	return nil
// }

func (spec *PipelineTemplateSpec) allTasks() []*Task {
	tasks := []*Task{}

	for _, stage := range spec.Stages {
		for _, task := range stage.Tasks {
			tasks = append(tasks, task)
		}
	}

	for _, post := range spec.Post {
		for _, task := range post {
			tasks = append(tasks, task)
		}
	}

	return tasks
}

func (spec *PipelineTemplateSpec) appendTaskTemplateSpecRef(taskTemlateRefs map[string]TaskTemplateSpec) error {

	errs := common.Errors{}
	for _, stage := range spec.Stages {
		for _, task := range stage.Tasks {
			templateIdentity := task.Kind + "/" + task.Name

			if _, ok := taskTemlateRefs[templateIdentity]; !ok {
				errs = append(errs, common.NewValidateError(fmt.Sprintf("require definition of task template named:%s", task.Name), nil))
				continue
			}
			taskTemplateSpec := taskTemlateRefs[templateIdentity]
			task.taskTemplateSpec = &taskTemplateSpec
		}
	}

	for _, tasks := range spec.Post {
		for _, task := range tasks {
			templateIdentity := task.Kind + "/" + task.Name

			if _, ok := taskTemlateRefs[templateIdentity]; !ok {
				errs = append(errs, common.NewValidateError(fmt.Sprintf("require definition of task template named:%s", task.Name), nil))
				continue
			}
			taskTemplateSpec := taskTemlateRefs[templateIdentity]
			task.taskTemplateSpec = &taskTemplateSpec
		}
	}

	if len(errs) == 0 {
		return nil
	}
	return errs
}

func (spec *PipelineTemplateSpec) markMeaningfulTask(argumentsValues map[string]interface{}) {

	for _, stage := range spec.Stages {
		for _, task := range stage.Tasks {
			if task.IsMeaningful(argumentsValues) {
				task.meaningfull = true
			} else {
				task.meaningfull = false
			}
		}
	}

	for _, tasks := range spec.Post {
		for _, task := range tasks {
			if task.IsMeaningful(argumentsValues) {
				task.meaningfull = true
			} else {
				task.meaningfull = false
			}
		}
	}
}
