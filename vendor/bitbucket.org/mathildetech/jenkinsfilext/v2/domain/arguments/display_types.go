package arguments

// DisplayString 单行文本框
const DisplayString = "string" // +displayType

// DisplayCode 代码行
// 适用于代码的多行文本
const DisplayCode = "code" // +displayType

// DisplayStringMultiLine 多行文本
// 普通的多行文本框
const DisplayStringMultiLine = "stringMultiline" // +displayType

// DisplayCodeBranch 代码仓库分支选择
// 用来根据选择的代码仓库，下拉出对应的代码分支，供用户选择
//
// 例如：
//
//  ```yaml
//	name: "Branch"
//	schema:
//		type: string
//	required: true
//	display:
//		type: alauda.io/codebranch
//		name:
//			zh-CN: "分支"
//			en: Branch
//		description:
//			zh-CN: "检出代码仓库中的分支"
//			en: "The code repository branch that you want to check out. "
//		# 需要 related 描述对应的代码仓库
//		related: PlatformCodeRepository
//  ```
//
// 提交值的格式如下：
const DisplayCodeBranch = "alauda.io/codebranch" // +displayType

// DisplayClusterName 集群名称
// 用于显示当前项目下的集群下拉框
const DisplayClusterName = "alauda.io/clustername" // +displayType

// DisplayNamespace 集群namespace
// 用于显示选定集群的namespace下拉列表
const DisplayNamespace = "alauda.io/namespace" // +displayType

// DisplayJenkinsCredentials 凭据
// 当前项目下的DevOps 凭据
const DisplayJenkinsCredentials = "alauda.io/jenkinscredentials" // +displayType

// DisplayServiceNameMix 应用名称
// 用于显示选定的namespace下的应用名称下拉框
const DisplayServiceNameMix = "alauda.io/servicenamemix" // +displayType

// DisplayContainerName 容器名称
// 用于显示指定应用下的容器名称下拉框
const DisplayContainerName = "alauda.io/containername" // +displayType

// DisplayBoolean 两相开关
// 两相开关
const DisplayBoolean = "boolean" // +displayType

// DisplayCodeRepositoryMix 代码仓库
// 用于选择一个具体的代码仓库
const DisplayCodeRepositoryMix = "alauda.io/coderepositorymix" // +displayType

// DisplayImageRepositoryMix 镜像选择（deprecated）
// 用于镜像选择的控件，支持多个tag的填写，通常用于 push 镜像时的镜像选择。
// 支持选择，输入，新建
const DisplayImageRepositoryMix = "alauda.io/imagerepositorymix" // +displayType

// DisplayDockerImageRepositoryMix 镜像选择（push）
// 用于镜像选择的控件，支持多个tag的填写，通常用于 push 镜像时的镜像选择。
// 支持选择，输入，新建
const DisplayDockerImageRepositoryMix = "alauda.io/dockerimagerepositorymix" // +displayType

// DisplayDockerImageRepositoryPullMix 镜像选择（pull）
// 用于镜像选择的控件，选择单个tag, 通常用于pull 镜像时的镜像选择。
const DisplayDockerImageRepositoryPullMix = "alauda.io/dockerimagerepositorypullmix" // +displayType

// DisplayToolBinding 工具绑定下拉框
// 用于选择某种工具绑定类型的资源，例如：代码扫描
// 支持
//	- display.args.bindingKind: 绑定的资源的Kind
//	- display.args.bindingToolType: 绑定的资源的工具的Type
//
// 例子：
//
// ```yaml
//	name: "CodeQualityBinding"
//	schema:
//		type: alauda.io/toolbinding
//	required: true
//	value: ""
//	display:
//		type: alauda.io/toolbinding
//		args:
//			bindingKind: codequalitytool
//			bindingToolType: Sonarqube
//		name:
//			zh-CN: "SonarQube 实例"
//			en: "SonarQube instance"
//		description:
//			zh-CN: "选择要使用的 SonarQube 实例"
//			en: "Select a SonarQube instance"
//	relation:
//		- action: show
//			when:
//				name: UseSonarQube
//				value: true
// ```
// 提交值的格式如下：
const DisplayToolBinding = "alauda.io/toolbinding" // +displayType
