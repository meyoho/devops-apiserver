package arguments

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/mathildetech/jenkinsfilext/v2/domain/common"
)

type ArgSections []ArgSection

func (argSections ArgSections) AllArgItems() []ArgItem {
	allArgItems := []ArgItem{}
	for _, section := range argSections {
		allArgItems = append(allArgItems, section.Items...)
	}

	return allArgItems
}

type ArgSection struct {
	DisplayName common.MulitLangValue `json:"displayName" mapstructure:"displayName" yaml:"displayName"`
	Items       []ArgItem             `json:"items"`
}

type ArgItem struct {
	Name string `json:"name"`
	// Binding format could be {task name}.args.{argument name} or {task display name}.args.{argument name} or {task name}.{xxx}
	Binding  []string       `json:"binding,omitempty"`
	Schema   *ArgItemSchema `json:"schema"`
	Required bool           `json:"required"`
	Default  interface{}    `json:"default,omitempty"`
	// Value is just compatiable for devops apiserver
	Value       interface{}        `json:"value,omitempty"`
	Validation  *ArgItemValidation `json:"validation,omitempty"`
	DisplayInfo *ArgDisplayInfo    `json:"display" mapstructure:"display" yaml:"display"`
	Relation    *common.Relation   `json:"relation,omitempty" mapstructure:"relation" yaml:"relation,omitempty"`
}
type ArgDisplayInfo struct {
	Type        string                 `json:"type"`
	Advanced    bool                   `json:"advanced"`
	Name        common.MulitLangValue  `json:"name"`
	Args        map[string]interface{} `json:"args,omitempty"`
	Description common.MulitLangValue  `json:"description,omitempty"`
	Related     string                 `json:"related,omitempty"`
}

func (arg *ArgItem) GetImplementor() IArgItem {
	implementorNew, _ := ArgItemImplementors[ArgValueType(arg.Schema.Type)]
	implementor := implementorNew(*arg)
	return implementor
}

func (arg *ArgItem) ValidateDefinition() error {
	//TODO
	if strings.TrimSpace(arg.Name) == "" {
		return common.NewTemplateDefinitionError("name should not be empty", nil)
	}

	if arg.Schema == nil {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.schema is required", arg.Name), nil)
	}
	if _, ok := ArgItemImplementors[ArgValueType(arg.Schema.Type)]; !ok {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.schema.type=%s is not support now", arg.Name, arg.Schema.Type), nil)
	}

	if arg.DisplayInfo == nil {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.display is required", arg.Name), nil)
	}
	if arg.DisplayInfo.Type == "" {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.display.type is required", arg.Name), nil)
	}
	if arg.DisplayInfo.Name.ZH_CN == "" {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.display.Name.zh-CN is required", arg.Name), nil)
	}
	if arg.DisplayInfo.Name.EN == "" {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.display.Name.en is required", arg.Name), nil)
	}

	implementor := arg.GetImplementor()
	return implementor.ValidateDefinition()
}

func (arg *ArgItem) ValidateValue(value interface{}) error {

	// required
	if arg.Required && value == nil {
		return common.NewValidateError(fmt.Sprintf("%s is required", arg.Name), nil)
	}

	if value == nil && arg.Required == false {
		return nil
	}

	implementor := arg.GetImplementor()
	return implementor.ValidateValue(value)
}

func (arg *ArgItem) IsMeaningful(argumentsValues map[string]interface{}) bool {
	meaningful := arg.Relation.IsMathcShowAction(argumentsValues)
	// fmt.Printf("arg `%s` meaningful = %t \n", arg.Name, meaningful)
	return meaningful
}

// GetValue: get value from provider value , you should ValidateValue at first.
func (arg *ArgItem) GetValue(value interface{}) interface{} {
	return arg.GetImplementor().GetValue(value)
}

type ArgItemValidation struct {
	Pattern   string `json:"pattern"`
	MaxLength int    `json:"maxLength"`
}

type ArgItemSchema struct {
	Type  string             `json:"type,omitempty"`
	Items *ArgItemSchemaItem `json:"items,omitempty"`
}

type ArgItemSchemaItem struct {
	Type string `json:"type"`
}

type IArgItem interface {
	ValidateDefinition() error
	ValidateValue(value interface{}) error
	GetValue(value interface{}) interface{}

	SupportDisplayTypes() []string
}

var ArgItemImplementors = map[ArgValueType]func(data ArgItem) IArgItem{}

func init() {
	ArgItemImplementors[ArgString] = func(data ArgItem) IArgItem {
		item := ArgItemString(data)
		return &item
	}

	ArgItemImplementors[ArgBoolean] = func(data ArgItem) IArgItem {
		item := ArgItemBoolean(data)
		return &item
	}

	ArgItemImplementors[ArgObject] = func(data ArgItem) IArgItem {
		item := ArgItemObject(data)
		return &item
	}

	ArgItemImplementors[ArgInt] = func(data ArgItem) IArgItem {
		item := ArgItemInt(data)
		return &item
	}

	ArgItemImplementors[ArgImageRepositoryAlaudaIO] = func(data ArgItem) IArgItem {
		item := ArgItemImageRepositoryMix(data)
		return &item
	}

	ArgItemImplementors[ArgK8SEnvAlaudaIO] = func(data ArgItem) IArgItem {
		item := ArgItemK8sEnv(data)
		return &item
	}

	ArgItemImplementors[ArgNewK8sContainerMix] = func(data ArgItem) IArgItem {
		item := ArgItemContainerMix(data)
		return &item
	}

	ArgItemImplementors[ArgV1NewK8sContainerMix] = func(data ArgItem) IArgItem {
		item := ArgItemV1ContainerMix(data)
		return &item
	}

	ArgItemImplementors[ArgArray] = func(data ArgItem) IArgItem {
		item := ArgItemArray(data)
		return &item
	}

	ArgItemImplementors[ArgCodeRepositoryMix] = func(data ArgItem) IArgItem {
		item := ArgItemCodeRepositoryMix(data)
		return &item
	}

	ArgItemImplementors[ArgToolBinding] = func(data ArgItem) IArgItem {
		item := ArgItemToolBinding(data)
		return &item
	}

	ArgItemImplementors[ArgDockerImageRepositoryMix] = func(data ArgItem) IArgItem {
		item := ArgItemDockerImageRepositoryMix(data)
		return &item
	}

	ArgItemImplementors[ArgDockerImageRepositoryPullMix] = func(data ArgItem) IArgItem {
		item := ArgItemDockerImageRepositoryPullMix(data)
		return &item
	}

}

func isStringLiteralBoolValue(value interface{}) bool {
	if strValue, ok := value.(string); ok {
		_, err := strconv.ParseBool(strValue)
		if err == nil {
			return true
		}
	}
	return false
}
