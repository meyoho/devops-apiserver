package arguments

import (
	"bitbucket.org/mathildetech/jenkinsfilext/v2/domain/common"
	"fmt"
)

type ArgItemImageRepositoryMix ArgItem

func (*ArgItemImageRepositoryMix) SupportDisplayTypes() []string {
	return []string{
		DisplayImageRepositoryMix,
	}
}

func (arg *ArgItemImageRepositoryMix) ValidateDefinition() error {
	if arg.DisplayInfo.Type != string(ArgImageRepositoryAlaudaIO) {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.display.type should be %s", arg.Name, ArgImageRepositoryAlaudaIO), nil)
	}

	return nil
}

func (arg *ArgItemImageRepositoryMix) ValidateValue(_value interface{}) error {
	value := arg.GetValue(_value)
	if value == nil {
		return nil
	}
	v := map[string]interface{}{}
	ok := true

	if v, ok = value.(map[string]interface{}); !ok {
		return common.NewValidateError(fmt.Sprintf("%s's value %v is invalid , but got type %T", arg.Name, value, value), nil)
	}

	var requiredFields = []string{
		"registry",
		"repository",
	}

	for _, field := range requiredFields {
		var fieldValue interface{}

		if fieldValue, ok = v[field]; !ok {
			return common.NewValidateError(fmt.Sprintf("%s is required for argument %s, but get value %v", field, arg.Name, v), nil)
		}
		if _, ok = fieldValue.(string); !ok {
			return common.NewValidateError(fmt.Sprintf("argument %s.%s's value %v is invalid format, it should be string, but got type %T",
				arg.Name, field, fieldValue, fieldValue), nil)
		}
	}

	return nil
}

func (arg *ArgItemImageRepositoryMix) GetValue(value interface{}) interface{} {
	return value
}
