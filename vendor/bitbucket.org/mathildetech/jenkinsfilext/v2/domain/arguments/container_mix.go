package arguments

import (
	"bitbucket.org/mathildetech/jenkinsfilext/v2/domain/common"
	"fmt"
)

type ArgItemV1ContainerMix ArgItem

func (*ArgItemV1ContainerMix) SupportDisplayTypes() []string {
	return []string{}
}

func (arg *ArgItemV1ContainerMix) ValidateDefinition() error {
	if arg.DisplayInfo.Type != string(ArgV1NewK8sContainerMix) {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.display.type should be %s", arg.Name, ArgV1NewK8sContainerMix), nil)
	}

	return nil
}

func (arg *ArgItemV1ContainerMix) ValidateValue(_value interface{}) error {
	value := arg.GetValue(_value)
	if value == nil {
		return nil
	}
	v := map[string]interface{}{}
	ok := true

	if v, ok = value.(map[string]interface{}); !ok {
		return common.NewValidateError(fmt.Sprintf("argument %s(%s)'s value %v is invalid format, got type %T", arg.Schema.Type, arg.Name, value, value), nil)
	}

	var requiredFields = []string{
		"clusterName",
		"namespace",
		"applicationName",
		"componentName",
		"componentType",
		"containerName",
	}

	for _, field := range requiredFields {
		var fieldValue interface{}
		if fieldValue, ok = v[field]; !ok {
			return common.NewValidateError(fmt.Sprintf("%s is required for argument %s, but get value %v", field, arg.Name, v), nil)
		}

		if _, ok = fieldValue.(string); !ok {
			return common.NewValidateError(fmt.Sprintf("argument %s.%s's value %v is invalid format, it should be string, but got type %T",
				arg.Name, field, fieldValue, fieldValue), nil)
		}
	}

	return nil
}

func (arg *ArgItemV1ContainerMix) GetValue(value interface{}) interface{} {
	return value
}

type ArgItemContainerMix ArgItem

func (*ArgItemContainerMix) SupportDisplayTypes() []string {
	return []string{}
}

func (arg *ArgItemContainerMix) ValidateDefinition() error {
	if arg.DisplayInfo.Type != string(ArgNewK8sContainerMix) {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.display.type should be %s", arg.Name, ArgNewK8sContainerMix), nil)
	}

	return nil
}

func (arg *ArgItemContainerMix) ValidateValue(_value interface{}) error {
	value := arg.GetValue(_value)
	if value == nil {
		return nil
	}
	v := map[string]interface{}{}
	ok := true

	if v, ok = value.(map[string]interface{}); !ok {
		return common.NewValidateError(fmt.Sprintf("argument %s(%s)'s value %v is invalid format, got type %T", arg.Schema.Type, arg.Name, value, value), nil)
	}

	var requiredFields = []string{
		"clusterName",
		"serviceName",
		"containerName",
		"namespace",
	}

	for _, field := range requiredFields {
		var fieldValue interface{}
		if fieldValue, ok = v[field]; !ok {
			return common.NewValidateError(fmt.Sprintf("%s is required for argument %s, but get value %v", field, arg.Name, v), nil)
		}

		if _, ok = fieldValue.(string); !ok {
			return common.NewValidateError(fmt.Sprintf("argument %s.%s's value %v is invalid format, it should be string, but got type %T",
				arg.Name, field, fieldValue, fieldValue), nil)
		}
	}

	return nil
}

func (arg *ArgItemContainerMix) GetValue(value interface{}) interface{} {
	return value
}
