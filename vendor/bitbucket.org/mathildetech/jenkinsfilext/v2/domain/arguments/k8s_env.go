package arguments

import (
	"bitbucket.org/mathildetech/jenkinsfilext/v2/domain/common"
	"fmt"
)

type ArgItemK8sEnv ArgItem

func (*ArgItemK8sEnv) SupportDisplayTypes() []string {
	return []string{}
}

func (arg *ArgItemK8sEnv) ValidateDefinition() error {
	if arg.DisplayInfo.Type != string(ArgK8SEnvAlaudaIO) {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.display.type should be %s", arg.Name, ArgK8SEnvAlaudaIO), nil)
	}

	return nil
}

func (arg *ArgItemK8sEnv) ValidateValue(_value interface{}) error {
	value := arg.GetValue(_value)
	if value == nil {
		return nil
	}
	v := []interface{}{}
	ok := true

	if v, ok = value.([]interface{}); !ok {
		return common.NewValidateError(fmt.Sprintf("argument %s(%s)'s value %v is invalid format, but got type %T",
			arg.Schema.Type, arg.Name, value, value), nil)
	}

	for _, item := range v {
		itemMap, ok := item.(map[string]interface{})
		if !ok {
			return common.NewValidateError(fmt.Sprintf("argument %s(%s)'s value %v is invalid format, it should be map in array, but got type %T",
				arg.Schema.Type, arg.Name, value, value), nil)
		}

		name, nameExist := itemMap["name"]
		if !nameExist {
			return common.NewValidateError(fmt.Sprintf("[].name is required for argument %s, but get value %v", arg.Name, v), nil)
		}

		if name == "" {
			return common.NewValidateError(fmt.Sprintf("[].name shoule not be empty for argument %s", arg.Name), nil)
		}

		_, withValue := itemMap["value"]
		_, withValueFrom := itemMap["valueFrom"]
		// 不能都为true
		if withValue && withValueFrom {
			return common.NewValidateError(fmt.Sprintf("argument %s's item value `%v` is invalid format", arg.Name, item), nil)
		}
		//不能都为false
		if withValue == false && withValueFrom == false {
			return common.NewValidateError(fmt.Sprintf("argument %s's item value `%v` is invalid format", arg.Name, item), nil)
		}

		if withValue {
			if itemMap["value"] == "" {
				return common.NewValidateError(fmt.Sprintf("[].value shoule not be empty for argument %s", arg.Name), nil)
			}
		} else {
			valueFrom, ok := itemMap["valueFrom"].(map[string]interface{})
			if !ok {
				return common.NewValidateError(fmt.Sprintf("argument %s's item value `%v` is invalid format, should contains valueFrom", arg.Name, item), nil)
			}
			ref, ok := valueFrom["configMapKeyRef"]
			if !ok {
				return common.NewValidateError(fmt.Sprintf("argument %s's item value `%v` is invalid format, should contains configMapKeyRef", arg.Name, item), nil)
			}
			refValue, ok := ref.(map[string]interface{})
			if !ok {
				return common.NewValidateError(fmt.Sprintf("argument %s's item value `%v` is invalid format, configMapKeyRef should be map", arg.Name, item), nil)
			}
			if fmt.Sprint(refValue["key"]) == "" || fmt.Sprint(refValue["name"]) == "" {
				return common.NewValidateError(fmt.Sprintf("argument %s's item value `%v` is invalid format, should contains configMapKeyRef", arg.Name, item), nil)
			}
		}

	}

	return nil
}

func (arg *ArgItemK8sEnv) GetValue(value interface{}) interface{} {
	return value
}
