package arguments

type ArgValueType string

// ArgArray
//
// is only support in backend, not support in any frontend,
// it is an object array in backend
const ArgArray ArgValueType = "array"

// ArgString
//
// is support in ace jenkins and alauda devops，
// ace jenkins displayTypes:
// 	shellscripts
// 	string
// 	alauda.io/jenkinscredentials
// 	alauda.io/imagetag
// 	alauda.io/integration
// 	alauda.io/sonarqube/qualitygates
// 	alauda.io/sonarqube/lang
// alauda devops displayTypes:
//  string
//	code
//	stringMultiline
//	alauda.io/clustername
//	alauda.io/k8snamespace
//	alauda.io/applicationName
//	alauda.io/componentName
//	alauda.io/componentClass
//	alauda.io/containerName
const ArgString ArgValueType = "string"

// ArgBoolean
//
// is support in ace jenkins and alauda devops,
// displayType: boolean
//
// post value example:
//  "key": false
const ArgBoolean ArgValueType = "boolean"

// ArgObject
//
// is only support in ace jenkins,
//
// displayType could be anything
// post value example:
//  "key": {"xx":"xx", "xx":"xx"}
// the value could be json string or json object
const ArgObject ArgValueType = "object"

// ArgInt
//
// displayType: int,
// example of post value "key": 1
const ArgInt ArgValueType = "int"

// ArgImageRepositoryAlaudaIO
//
// only support in ace jenkins frontend,
// displayType: alauda.io/imagerepositorymix,
// post value example:
//   "key": { "registry": "index.alauda.cn", "repository":"demo/hello" }
const ArgImageRepositoryAlaudaIO ArgValueType = "alauda.io/imagerepositorymix"

// ArgK8SEnvAlaudaIO
//
// deprecated now
const ArgK8SEnvAlaudaIO ArgValueType = "alauda.io/k8senv"

// ArgNewK8sContainerMix
//
// deprecated now.
// is support in ace jenkins frontend
// displayType: alauda.io/newk8scontainermix
// post value example:
//  "key": { "clusterName": "", "containerName":"", "namespace":"", "serviceName":"" }
const ArgNewK8sContainerMix ArgValueType = "alauda.io/newk8scontainermix"

// ArgV1NewK8sContainerMix
//
// is support in ace jenkins frontend
// displayType: alauda.io/v1newk8scontainermix
// post value example:
//  "key": { "clusterName": "", "namespace":"", "applicationName":"", "componentName":""， "componentType":"", "containerName":""}
const ArgV1NewK8sContainerMix ArgValueType = "alauda.io/v1newk8scontainermix"

// ArgCodeRepositoryMix
//
// is support in alauda devops
// displayType: alauda.io/coderepositorymix
// post value example:
//  "key": {"url":"", "kind":"", "credentialId": ""}
const ArgCodeRepositoryMix ArgValueType = "alauda.io/coderepositorymix"

// ArgToolBinding
//
// is support in alauda devops
// displayType: alauda.io/toolbinding
// post value example:
//  "key": {"name":"name of toolbinding", "namespace":"namespace of toolbinding"}
const ArgToolBinding ArgValueType = "alauda.io/toolbinding"

// ArgDockerImageRepositoryMix
//
// is support in alauda devops
// displayType: alauda.io/dockerimagerepositorymix
// post value example:
//   "key": "{"credentialId":"jtcheng-secret-create","repositoryPath":"aaaaa","type":"input","tag":"latest,first,second","secretNamespace":"jtcheng","secretName":"secret-create"}"
// use for push
//
const ArgDockerImageRepositoryMix ArgValueType = "alauda.io/dockerimagerepositorymix"

// ArgDockerImageRepositoryPullMix
//
// is support in alauda devops
// displayType: alauda.io/dockerimagerepositorypullmix
// post value example:
//   "key": "{"credentialId":"jtcheng-secret-create","repositoryPath":"aaaaa","type":"input","tag":"latest","secretNamespace":"jtcheng","secretName":"secret-create"}"
// use for pull
//
const ArgDockerImageRepositoryPullMix ArgValueType = "alauda.io/dockerimagerepositorypullmix"
