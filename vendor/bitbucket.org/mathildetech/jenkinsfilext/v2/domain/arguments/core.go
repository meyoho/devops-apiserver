package arguments

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/mathildetech/jenkinsfilext/v2/domain/common"
)

type ArgItemString ArgItem

const (
	DisplayInfoIntegration string = "alauda.io/integration"
)

func (*ArgItemString) SupportDisplayTypes() []string {
	return []string{
		DisplayString,
		DisplayStringMultiLine,
		DisplayCodeBranch,
		DisplayCode,
		DisplayClusterName,
		DisplayNamespace,
		DisplayJenkinsCredentials,
		DisplayServiceNameMix,
		DisplayContainerName,
	}
}

func (stringArg *ArgItemString) ValidateDefinition() error {
	if stringArg.DisplayInfo.Type == DisplayInfoIntegration {
		// need key types
		if _, ok := stringArg.DisplayInfo.Args["types"]; !ok {
			return common.NewTemplateDefinitionError(fmt.Sprintf("%s.display.args require key \"types\"", stringArg.Name), nil)
		}
	}
	return nil
}

func (stringArg *ArgItemString) ValidateValue(value interface{}) error {
	var (
		v  = ""
		ok = true
	)

	// value type validate
	if v, ok = value.(string); !ok {
		return common.NewValidateError(fmt.Sprintf("%s should be string", stringArg.Name), map[string]interface{}{
			"RecievedValue": value,
			"RecievedType":  fmt.Sprintf("%T", value),
		})
	}

	// value validate
	if stringArg.Validation != nil {
		if stringArg.Validation.MaxLength > 0 {
			if len(v) > stringArg.Validation.MaxLength {
				return common.NewValidateError(fmt.Sprintf("%s is to long, length should be less than %d",
					stringArg.Name, stringArg.Validation.MaxLength), map[string]interface{}{
					"RecievedValue": value,
					"MaxLength":     stringArg.Validation.MaxLength,
				})
			}
		}

		if stringArg.Validation.Pattern != "" {
			if matched, _ := regexp.MatchString(stringArg.Validation.Pattern, v); !matched {
				return common.NewValidateError(fmt.Sprintf("%s's value %v is not match the pattern %s",
					stringArg.Name, v, stringArg.Validation.Pattern), map[string]interface{}{
					"RecievedValue": value,
					"Pattern":       stringArg.Validation.Pattern,
				})
			}
		}
	}

	return nil
}

func (stringArg *ArgItemString) GetValue(value interface{}) interface{} {
	if value == nil {
		if stringArg.Default == nil {
			return ""
		}
		return stringArg.Default
	}
	return strings.TrimSpace(fmt.Sprint(value))
}

type ArgItemBoolean ArgItem

func (*ArgItemBoolean) SupportDisplayTypes() []string {
	return []string{
		DisplayBoolean,
	}
}

func (arg *ArgItemBoolean) ValidateDefinition() error {
	return nil
}

func (arg *ArgItemBoolean) ValidateValue(value interface{}) error {
	var (
		ok = true
	)

	// value type validate
	if _, ok = value.(bool); !ok && !isStringLiteralBoolValue(value) {
		return common.NewValidateError(fmt.Sprintf("%s should be boolean", arg.Name), map[string]interface{}{
			"RecievedValue": value,
			"RecievedType":  fmt.Sprintf("%T", value),
		})
	}

	return nil
}

func (arg *ArgItemBoolean) GetValue(value interface{}) interface{} {
	if value == nil {
		if arg.Default == nil {
			return false
		}
		// we should make sure the type of arg.Default
		// so , just pass arg.Default to value and try to parse to boolean
		value = arg.Default
	}

	if isStringLiteralBoolValue(value) {
		parsedValue, _ := strconv.ParseBool(value.(string))
		return parsedValue
	}
	return value
}

// ArgItemObject
// when value is string, it must be json string as a json object
type ArgItemObject ArgItem

func (*ArgItemObject) SupportDisplayTypes() []string {
	return []string{}
}

func (arg *ArgItemObject) ValidateDefinition() error {
	return nil
}

func (arg *ArgItemObject) ValidateValue(value interface{}) error {
	// when value is string, it must be json string as a json object
	if textVal, ok := value.(string); ok {
		result := map[string]interface{}{}
		err := json.Unmarshal(([]byte)(textVal), &result)
		return err
	}

	return nil
}

func (arg *ArgItemObject) GetValue(value interface{}) (result interface{}) {
	if value == nil {
		if arg.Default == nil {
			return struct{}{}
		}
		return arg.Default
	}

	if textVal, ok := value.(string); ok {
		json.Unmarshal(([]byte)(textVal), &result)
		return
	}

	return value
}

// ArgItemInt allow user to input int like value (int or string)
type ArgItemInt ArgItem

func (*ArgItemInt) SupportDisplayTypes() []string {
	return []string{}
}

func (arg *ArgItemInt) ValidateDefinition() error {
	return nil
}

// ValidateValue allow intOrString that could parse to int
func (arg *ArgItemInt) ValidateValue(value interface{}) error {
	if str, ok := value.(string); ok {
		_, err := strconv.Atoi(str)
		return err
	}

	return nil
}

func (arg *ArgItemInt) GetValue(value interface{}) (result interface{}) {
	if value == nil {
		if arg.Default == nil {
			return 0
		}
		result = arg.Default
	} else {
		result = value
	}

	if _, ok := result.(int); ok {
		return result
	}

	if _, ok := result.(string); ok {
		v, err := strconv.Atoi(fmt.Sprint(result))
		if err != nil {
			fmt.Printf("parse %#v to int error:%s\n", value, err.Error())
			return 0
		}
		return v
	}

	return value
}

type ArgItemArray ArgItem

func (*ArgItemArray) SupportDisplayTypes() []string {
	return []string{}
}

func (arg *ArgItemArray) ValidateDefinition() error {
	if arg.Schema.Items == nil {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.schema.items should not be nil", arg.Name), nil)
	}

	if _, ok := ArgItemImplementors[ArgValueType(arg.Schema.Items.Type)]; !ok {
		return common.NewTemplateDefinitionError(fmt.Sprintf("%s.schema.items.type=%s is not support now", arg.Name, arg.Schema.Type), nil)
	}

	itemImplementorNew, _ := ArgItemImplementors[ArgValueType(arg.Schema.Items.Type)]
	itemImplementor := itemImplementorNew(ArgItem(*arg))
	return itemImplementor.ValidateDefinition()
}

func (arg *ArgItemArray) ValidateValue(value interface{}) error {

	items, ok := value.([]interface{})
	if !ok {
		return common.NewValidateError(fmt.Sprintf("argument %s‘s value is invalid format ,it should be an array, but got type %T", arg.Name, value), nil)
	}

	itemImplementorNew, _ := ArgItemImplementors[ArgValueType(arg.Schema.Items.Type)]
	itemImplementor := itemImplementorNew(ArgItem(*arg))
	errs := common.Errors{}
	for _, item := range items {
		err := itemImplementor.ValidateValue(item)
		if err != nil {
			errs = append(errs, err)
		}
	}

	if len(errs) == 0 {
		return nil
	}

	return errs
}

func (arg *ArgItemArray) GetValue(value interface{}) (result interface{}) {
	if textVal, ok := value.(string); ok {
		json.Unmarshal(([]byte)(textVal), &result)
		return
	}

	return value
}
