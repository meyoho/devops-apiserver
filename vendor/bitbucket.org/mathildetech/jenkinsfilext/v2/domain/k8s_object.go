package domain

import (
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strings"

	"bitbucket.org/mathildetech/jenkinsfilext/v2/domain/common"
	"github.com/pkg/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"github.com/ghodss/yaml"
)

// K8SObject will replace struct of kubernete
type K8SObject struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`
	Status            map[string]interface{} `json:"status,omitempy"`

	res k8sResourceInterface
}

var k8sObjects = map[string]func() k8sResourceInterface{}

func register(kind string, f func() k8sResourceInterface) {
	k8sObjects[kind] = f
}

func get(kind string) (k8sResourceInterface, error) {
	newObj, ok := k8sObjects[kind]
	if !ok {
		return nil, errors.New(fmt.Sprintf("Not Support Kind:%s", kind))
	}
	return newObj(), nil
}

func (kube *K8SObject) GetTypeMeta() metav1.TypeMeta {
	return kube.TypeMeta
}

func (kube *K8SObject) LoadFromYaml(yamls string) error {
	err := yaml.Unmarshal([]byte(yamls), kube)
	if err != nil {
		return err
	}

	objF, ok := k8sObjects[kube.Kind]
	if !ok {
		return common.NewTemplateDefinitionError(fmt.Sprintf("kind %s is not support now", kube.Kind), nil)
	}

	obj := objF()
	err = yaml.Unmarshal([]byte(yamls), obj)
	if err != nil {
		return err
	}
	kube.res = obj
	return nil
}

func (kube *K8SObject) LoadFromFile(path string) error {
	_, err := os.Stat(path)
	if err != nil {
		return err
	}

	byts, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	if strings.HasSuffix(path, ".yaml") || strings.HasSuffix(path, ".yml") {
		return kube.LoadFromYaml(string(byts))
	}

	return common.Error{
		Message: "only support .yaml or .yml",
	}
}

func (kube *K8SObject) Resource() k8sResourceInterface {
	return kube.res
}

// ValidateMetadata validate K8SObject objectmeta
func (metadata *K8SObject) ValidateObjectMeta() error {
	if strings.TrimSpace(metadata.Name) == "" {
		return common.NewTemplateDefinitionError("metadata.name should be required", nil)
	}

	var nameRegx = "^[a-zA-Z]([-a-zA-Z0-9]*[a-zA-Z0-9])?$"
	var validName = regexp.MustCompile(nameRegx)
	if !validName.MatchString(metadata.Name) {
		return fmt.Errorf("name should match ^[a-zA-Z]([-a-zA-Z0-9]*[a-zA-Z0-9])?$")
	}

	var requiredAnnotations = []string{
		AnnotationDisplayNameCN,
		AnnotationDisplayNameEN,
		AnnotationVersion,
	}
	errs := common.Errors{}
	for _, name := range requiredAnnotations {
		if strings.TrimSpace(metadata.Annotations[name]) == "" {
			errs = append(errs, common.NewTemplateDefinitionError(fmt.Sprintf("metadata.annotations.[%s] is required", name), nil))
		}
	}

	if len(errs) == 0 {
		return nil
	}

	return errs
}

func (typemeta *K8SObject) ValidateTypetMeta() error {
	support := false
	for _, version := range SupportVersions {
		if typemeta.APIVersion == version {
			support = true
			break
		}
	}

	if !support {
		return common.NewTemplateDefinitionError(fmt.Sprintf("apiVersion %s is not support now", typemeta.APIVersion), nil)
	}
	return nil
}

func (kube *K8SObject) ValidateDefinition() error {

	err := kube.ValidateTypetMeta()
	if err != nil {
		return err
	}

	err = kube.ValidateObjectMeta()
	if err != nil {
		return common.NewTemplateDefinitionError(err.Error(), nil)
	}

	return kube.Resource().ValidateDefinition()
}

func init() {
	register(string(KuberneteKindPipelineTemplate), func() k8sResourceInterface {
		return &PipelineTemplate{}
	})
	register(string(KuberneteKindPipelineTaskTemplate), func() k8sResourceInterface {
		return &PipelineTaskTemplate{}
	})

	register(string(KuberneteKindClusterPipelineTemplate), func() k8sResourceInterface {
		return &PipelineTemplate{}
	})
	register(string(KuberneteKindClusterPipelineTaskTemplate), func() k8sResourceInterface {
		return &PipelineTaskTemplate{}
	})
}

type k8sResourceInterface interface {
	metav1.Object
	GetTypeMeta() metav1.TypeMeta
	ValidateDefinition() error
}

type PipelineTemplateInterface interface {
	k8sResourceInterface
	GetSpec() *PipelineTemplateSpec
}

type TaskTemplateInterface interface {
	k8sResourceInterface
	GetSpec() *TaskTemplateSpec
}

type PipelineTemplate struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`
	Spec              PipelineTemplateSpec `json:"spec"`
}

func (t *PipelineTemplate) GetTypeMeta() metav1.TypeMeta {
	return t.TypeMeta
}

func (t *PipelineTemplate) GetSpec() *PipelineTemplateSpec {
	return &t.Spec
}
func (t *PipelineTemplate) ValidateDefinition() error {
	return t.Spec.ValidateDefinition()
}

type PipelineTaskTemplate struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`
	Spec              TaskTemplateSpec `json:"spec"`
}

func (t *PipelineTaskTemplate) GetSpec() *TaskTemplateSpec {
	return &t.Spec
}

func (t *PipelineTaskTemplate) GetTypeMeta() metav1.TypeMeta {
	return t.TypeMeta
}

func (t *PipelineTaskTemplate) ValidateDefinition() error {
	return t.Spec.ValidateDefinition()
}
