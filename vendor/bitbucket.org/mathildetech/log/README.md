Alauda Kubernetes Engine Log Library
=====================================
Log is an interface connecting multiple logging interfaces through one generic API.



# Licensing
Licensed under the Apache License, Version 2.0. See [LICENSE](LICENSE) for the full license text.