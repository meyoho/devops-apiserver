module bitbucket.org/mathildetech/log

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/hashicorp/go-hclog v0.8.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.3.1
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
	golang.org/x/sys v0.0.0-20190402054613-e4093980e83e // indirect
)
