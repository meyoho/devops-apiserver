/*
 * Copyright 2019 THL A29 Limited, a Tencent company.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package log

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	logger *zap.Logger
)

// InitLogger initializes logger the way we want for tke.
func InitLogger(opts *Options) {
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:        "time",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "msg",
		StacktraceKey:  "stack",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     timeEncoder,
		EncodeDuration: milliSecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
	// when output to local path, with color is forbidden
	if !opts.DisableColor {
		encoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	}
	loggerConfig := &zap.Config{
		Level:             zap.NewAtomicLevelAt(opts.Level),
		Development:       false,
		DisableCaller:     !opts.EnableCaller,
		DisableStacktrace: false,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding:         opts.Format.String(),
		EncoderConfig:    encoderConfig,
		OutputPaths:      opts.OutputPaths,
		ErrorOutputPaths: opts.ErrorOutputPaths,
	}

	var err error
	logger, err = loggerConfig.Build(zap.AddStacktrace(zapcore.PanicLevel), zap.AddCallerSkip(1))
	if err != nil {
		panic(err)
	}
}

// FlushLogger calls the underlying Core's Sync method, flushing any buffered
// log entries. Applications should take care to call Sync before exiting.
func FlushLogger() {
	if logger != nil {
		_ = logger.Sync()
	}
}

// CheckLevel return if logging a message at the specified level is enabled.
func CheckLevel(level Level) bool {
	if logger == nil {
		return false
	}
	checkEntry := logger.Check(level, "")
	return checkEntry != nil
}

// CheckIntLevel return if logging a message at the specified level is enabled.
func CheckIntLevel(level int32) bool {
	var lvl zapcore.Level
	if level < 5 {
		lvl = zapcore.InfoLevel
	} else {
		lvl = zapcore.DebugLevel
	}
	checkEntry := logger.Check(lvl, "")
	return checkEntry != nil
}

// ZapLogger returns zap logger instance.
func ZapLogger() *zap.Logger {
	return logger
}

// Debug method output debug level log.
func Debug(msg string, fields ...Field) {
	if logger != nil {
		logger.Debug(msg, fields...)
	}
}

// Debugf method output debug level log.
func Debugf(format string, v ...interface{}) {
	Debug(fmt.Sprintf(format, v...))
}

// Info method output info level log.
func Info(msg string, fields ...Field) {
	if logger != nil {
		logger.Info(msg, fields...)
	}
}

// Infof method output info level log.
func Infof(format string, v ...interface{}) {
	Info(fmt.Sprintf(format, v...))
}

// Warn method output warning level log.
func Warn(msg string, fields ...Field) {
	if logger != nil {
		logger.Warn(msg, fields...)
	}
}

// Warnf method output warning level log.
func Warnf(format string, v ...interface{}) {
	Warn(fmt.Sprintf(format, v...))
}

// Error method output error level log.
func Error(msg string, fields ...Field) {
	if logger != nil {
		logger.Error(msg, fields...)
	}
}

// Errorf method output error level log.
func Errorf(format string, v ...interface{}) {
	Error(fmt.Sprintf(format, v...))
}

// Panic method output panic level log and shutdown application.
func Panic(msg string, fields ...Field) {
	if logger != nil {
		logger.Panic(msg, fields...)
	}
}

// Panicf method output panic level log and shutdown application.
func Panicf(format string, v ...interface{}) {
	Panic(fmt.Sprintf(format, v...))
}

// Fatal method output fatal level log.
func Fatal(msg string, fields ...Field) {
	if logger != nil {
		logger.Fatal(msg, fields...)
	}
}

// Fatalf method output fatal level log.
func Fatalf(format string, v ...interface{}) {
	Fatal(fmt.Sprintf(format, v...))
}
