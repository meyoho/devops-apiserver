package app

import (
	"sync"

	"strings"
	"encoding/json"
	"github.com/appscode/jsonpatch"
	"alauda.io/app-core/crd/v1beta1"
	"github.com/Jeffail/gabs"
	dc "github.com/alauda/cyborg/pkg/client"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
)

type ApplicationClient struct {
	config rest.Config
	dc     dc.KubeClient

	// Cluster is cluster name or id, should be unique among clusters
	Cluster string

	// BaseDomain decide all the label/annotations'key format
	// eg: <prefix>.<base_domain>/<key>:<value>. Default to `alauda.io`
	BaseDomain string
}

// NewClient generate a new application client
// Cluster: Cluster name, used to support multi Cluster. If set to "", equals to "default"
// BaseDomain: base domain, If set to "", equals to "alauda.io"
func NewClient(config *rest.Config, cluster string, baseDomain string) (*ApplicationClient, error) {
	config.APIPath = "apis"

	if cluster == "" {
		cluster = "default"
	}

	if baseDomain == "" {
		baseDomain = DefaultBaseDomain
	}

	kc, err := dc.NewKubeClient(config, cluster)
	if err != nil {
		return nil, err
	}

	c := ApplicationClient{
		config: *config,
		dc:     *kc,

		Cluster:    cluster,
		BaseDomain: baseDomain,
	}

	return &c, nil
}

// getClient gen client by resource use it's namespace and apiVersion, kind
func (ac *ApplicationClient) getClient(resource *unstructured.Unstructured) (dynamic.ResourceInterface, error) {
	i, err := ac.getClientByGVK(resource.GroupVersionKind())
	if err != nil {
		return nil, err
	}
	return i.Namespace(resource.GetNamespace()), nil
}

func (ac *ApplicationClient) getClientByGVK(gvk schema.GroupVersionKind) (dynamic.NamespaceableResourceInterface, error) {
	return ac.dc.ClientForGVK(gvk)
}

func (ac *ApplicationClient) getAppClient() (dynamic.NamespaceableResourceInterface, error) {
	return ac.getClientByGVK(getAppGVK())
}

func (ac *ApplicationClient) createAppCrd(info *ApplicationInfo, resources *[]unstructured.Unstructured) (*Application, error) {
	crd := genAppCrd(info, resources, ac.BaseDomain)
	app, _ := appCrdToUnstructured(crd)

	application := Application{
		appCrd:      crd,
		appResource: app,
	}

	result, err := ac.createResource(app)
	if err == nil {
		application.appResource = result
	}
	return &application, err
}

func (ac *ApplicationClient) createApplicationParallel(app *Application, result *Result, resources *[]unstructured.Unstructured) {

	res := make(chan *unstructured.Unstructured, len(*resources))
	results := make(chan *ResourceResult, len(*resources))
	eventsError := make(chan error, len(*resources))

	var wg sync.WaitGroup

	for _, item := range *resources {
		wg.Add(1)
		go func(item unstructured.Unstructured) {
			defer wg.Done()

			resource := &item
			app.updateResourceLabels(resource)
			resp, err := ac.createResource(resource)
			if err == nil {
				res <- resp
			}
			eventsError <- ac.sendCreateEvent(app, resource, err)
			results <- &ResourceResult{
				GroupVersionKind: resource.GroupVersionKind(),
				Name:             resource.GetName(),
				Action:           ActionCreate,
				Error:            err,
			}
		}(item)
	}

	go func() {
		wg.Wait()
		close(results)
		close(res)
		close(eventsError)
	}()

	for item := range results {
		result.append(item)
	}
	for err := range eventsError {
		result.addEventError(err)
	}
	for item := range res {
		app.addResource(item)
	}

}

// force to update the resource namespace
func (ac *ApplicationClient) fulfillResourcesNamespace(namespace string, resources *[]unstructured.Unstructured) {
	for _, item := range *resources {
		ac.fulfillResourceNamespace(namespace, &item)
	}
}

// force to update the resource namespace
func (ac *ApplicationClient) fulfillResourceNamespace(namespace string, resource *unstructured.Unstructured) {
	resource.SetNamespace(namespace)
}

// CreateApplication create application and sub-resources
// app crd will always be created first
// TODO:
// 3. set resource namespace
func (ac *ApplicationClient) CreateApplication(info *ApplicationInfo, resources *[]unstructured.Unstructured, parallel bool) (*Application, *Result) {
	var result Result

	app, err := ac.createAppCrd(info, resources)
	result.addCreateResult(app.appResource, err)
	if err != nil {
		return app, &result
	}
	ac.fulfillResourcesNamespace(info.Namespace, resources)
	if parallel {
		ac.createApplicationParallel(app, &result, resources)
	} else {
		for _, item := range *resources {
			resource := &item
			app.updateResourceLabels(resource)

			resp, err := ac.createResource(resource)
			result.addCreateResult(resource, err)
			result.addEventError(ac.sendCreateEvent(app, resource, err))
			if err == nil {
				app.addResource(resp)
			}
		}
	}

	phase := v1beta1.Succeeded
	if result.IsError() {
		phase = v1beta1.Failed
	}
	appRes, err := ac.updateAppPhase(app.appResource, phase)
	if err != nil {
		result.addUpdateResult(app.appResource, err)
	} else {
		app.appResource = appRes
	}

	return app, &result
}

func (ac *ApplicationClient) UpdateApplicationDisplayName(namespace, name, displayName string) (*Application, error) {
	app, res := ac.GetApplication(namespace, name)
	if res.IsError() {
		return app, res.CombineError()
	}
	key := getDisplayNameKey(ac.BaseDomain)
	result, err := ac.updateAppAnnotation(app.appResource, key, displayName)
	if err != nil {
		return app, err
	}
	app.appResource = result
	return app, nil

}

// UpdateApplication update application
// If resources contains app crd, use it and update it
// If resources doest not contain app crd, use existing one
func (ac *ApplicationClient) UpdateApplication(namespace, name string, resources *[]unstructured.Unstructured, opts ...ApplicationUpdateOptions) (*Application, *Result) {
	app, res := ac.GetApplication(namespace, name)
	if res.IsError() {
		return app, res
	}
	ac.fulfillResourcesNamespace(namespace, resources)
	var result Result
	var createList []unstructured.Unstructured
	var deleteList []unstructured.Unstructured
	var newList []unstructured.Unstructured
	var patchlist map[*unstructured.Unstructured]*unstructured.Unstructured
	var appNewRes unstructured.Unstructured
	var appOldRes unstructured.Unstructured

	patchlist = make(map[*unstructured.Unstructured]*unstructured.Unstructured)
	old := app.Resources

	// resources is new resource
	for _, _item := range *resources {
		item := _item
		if gvkCompare(item.GroupVersionKind(), getAppGVK()) {
			appNewRes = item
			continue
		} else {
			newList = append(newList, item)
		}

		if exist,oldresource := findResourceInList(&item, &old);exist {
			patchlist[oldresource] = &item
		} else {
			createList = append(createList, item)
		}
	}

	for _, item := range old {
		if !isResourceInList(&item, &newList) {
			deleteList = append(deleteList, item)
		}
	}

	if appNewRes.Object == nil {
		appNewRes = *app.appResource
	}

	appOldRes = *app.appResource

	app.Resources = nil

	for _, item := range deleteList {
		err := ac.deleteResource(&item)
		result.addDeleteResult(&item, err)
		ac.sendDeleteEvent(app, &item, err)
	}

	for _, item := range createList {
		app.updateResourceLabels(&item)
		resResult, err := ac.createResource(&item)
		result.addCreateResult(&item, err)
		ac.sendCreateEvent(app, &item, err)
		if err == nil {
			app.addResource(resResult)
		}
	}

	for old, new := range patchlist {

		app.updateResourceLabels(new)
		var (
			resResult *unstructured.Unstructured
			err       error
		)
		oldJson, err := json.Marshal(old)
		if err != nil {
			result.addApp(name, ActionUpdate, err)
			return app, &result
		}
		newJson, err := json.Marshal(new)
		if err != nil {
			result.addApp(name, ActionUpdate, err)
			return app, &result
		}
		patchOperations, err := jsonpatch.CreatePatch(oldJson, newJson)
		if err != nil {
			result.addApp(name, ActionUpdate, err)
			return app, &result
		}

		patch, err := json.Marshal(patchOperations)
		if err != nil {
			result.addApp(name, ActionUpdate, err)
			return app, &result
		}
		resResult, err = ac.patchResource(new,patch,types.JSONPatchType)
		result.addUpdateResult(new, err)
		ac.sendUpdateEvent(app, new, err)
		if err == nil {
			app.addResource(resResult)
		}
	}

	gvks := getCompGroupKinds(&newList)

	oldapplicaiton, err := unstructuredToAppCrd(&appOldRes)
	if err != nil {
		result.addApp(name, ActionUpdate, err)
		return app, &result
	}
	oldbody, err := appCrdToUnstructured(oldapplicaiton)

	crd, err := unstructuredToAppCrd(&appNewRes)
	if err != nil {
		result.addApp(name, ActionUpdate, err)
		return app, &result
	}
	crd.Spec.ComponentGroupKinds = gvks
	body, err := appCrdToUnstructured(crd)
	if err != nil {
		result.addApp(name, ActionUpdate, err)
		return app, &result
	}
	body.SetUID(types.UID(""))


	oldJson, err := json.Marshal(oldbody)
	if err != nil {
		result.addApp(name, ActionUpdate, err)
		return app, &result
	}
	newJson, err := json.Marshal(body)
	if err != nil {
		result.addApp(name, ActionUpdate, err)
		return app, &result
	}
	patchOperations, err := jsonpatch.CreatePatch(oldJson, newJson)
	if err != nil {
		result.addApp(name, ActionUpdate, err)
		return app, &result
	}
	patch, err := json.Marshal(patchOperations)
	resResult, err := ac.patchResource(body,patch,types.JSONPatchType)

	result.addUpdateResult(body, err)
	ac.sendUpdateEvent(app, body, err)
	if err != nil {
		app.appResource = resResult
	}

	return app, &result
}

// GetApplication retrieve application and sub-resources
func (ac *ApplicationClient) GetApplication(namespace, name string) (*Application, *Result) {
	var result Result
	appRes, err := ac.getAppResource(namespace, name)
	result.addApp(name, ActionGet, err)
	if err != nil {
		return nil, &result
	}

	app, err := unstructuredToAppCrd(appRes)
	if err != nil {
		result.addApp(name, ActionGet, err)
		return nil, &result
	}

	var subResources ListObjects
	kinds := len(app.Spec.ComponentGroupKinds)
	listObjects := make(chan *ListObject, kinds)
	results := make(chan *ResourceResult, kinds)

	var wg sync.WaitGroup

	for _, ck := range app.Spec.ComponentGroupKinds {
		wg.Add(1)
		go func(ck metav1.GroupKind) {
			defer wg.Done()

			group := ck.Group
			if group == "core" {
				group = ""
			}

			gvk, err := ac.getGVKByGroupKind(group, ck.Kind)
			rr := ResourceResult{
				Action:           ActionList,
				GroupVersionKind: *gvk,
			}

			if err != nil {
				rr.Error = err
				results <- &rr
				return
			}

			lo, err := ac.listResource(*gvk, namespace, metav1.ListOptions{
				LabelSelector: getAppLabelSelector(app),
			})
			rr.Error = err
			results <- &rr
			if err == nil {
				listObjects <- lo
			}
		}(ck)
	}

	go func() {
		wg.Wait()
		close(results)
		close(listObjects)
	}()

	for item := range listObjects {
		subResources.add(item)
	}

	for item := range results {
		result.append(item)
	}

	application := Application{
		Resources:   *subResources.mergeAll(),
		appCrd:      app,
		appResource: appRes,
	}

	return &application, &result
}

// ListApplications list application in namespace(empty means all namespaces)
// Note: list app in parallel
func (ac *ApplicationClient) ListApplications(namespace string) (*[]Application, []error) {
	object, err := ac.listResource(getAppGVK(), namespace, metav1.ListOptions{})
	if err != nil {
		return nil, []error{err}
	}

	num := len(object.Items)
	if num == 0 {
		return &[]Application{}, nil
	}

	apps := make(chan *Application, num)
	results := make(chan *Result, num)

	var wg sync.WaitGroup

	for _, item := range object.Items {
		wg.Add(1)
		go func(item unstructured.Unstructured) {
			defer wg.Done()
			app, result := ac.GetApplication(item.GetNamespace(), item.GetName())
			if result.IsError() {
				results <- result
			} else {
				apps <- app
			}
		}(item)
	}

	go func() {
		wg.Wait()
		close(results)
		close(apps)
	}()

	var applicationList []Application
	var errList []error
	for item := range results {
		errList = append(errList, item.Errors()...)
	}

	for app := range apps {
		applicationList = append(applicationList, *app)
	}

	return &applicationList, errList
}

// DeleteApplication delete application by delete the specified resources
// Note:
// 1. items should not contain the Application resource itself, only sub-resources is allowed
// 2. if items is none, all the sub-resources of app will be deleted
// 3. if items contains part of the sub-resources, these resources will not be deleted
// 4. app crd will always be deleted
func (ac *ApplicationClient) DeleteApplication(namespace, name string, items []GVKName) *Result {
	app, res := ac.GetApplication(namespace, name)
	if res.IsError() {
		return res
	}

	var result Result

	var deletes []unstructured.Unstructured
	var remains []unstructured.Unstructured

	for _, item := range app.Resources {
		if isMatchSearch(&item, items) {
			remains = append(remains, item)
		} else {
			deletes = append(deletes, item)
		}
	}

	for _, item := range deletes {
		err := ac.deleteResource(&item)
		result.addDeleteResult(&item, err)
	}

	if len(remains) != 0 {
		for _, item := range remains {
			_, err := ac.patchResource(&item, getRemoveAppLabelsData(&item, app.appCrd.Spec.Selector.MatchLabels), types.StrategicMergePatchType)
			result.addUpdateResult(&item, err)
		}
	}

	err := ac.deleteResource(app.appResource)
	result.addDeleteResult(app.appResource, err)

	return &result

}

// FindApplicationName try to find application from resource, this method will not check if the application exist
// or not. If BaseDomain = "", use default BaseDomain
func (ac *ApplicationClient) FindApplicationName(baseDomain string, resource *unstructured.Unstructured) string {
	if resource.GetLabels() == nil {
		return ""
	}
	v := resource.GetLabels()[getAppNameKey(baseDomain)]
	names := strings.Split(v, ".")
	if len(names) != 2 {
		return ""
	}
	return names[0]
}

// FindApplication find application related to this resource if possible
// If BaseDomain ==0 , use default BaseDomain
func (ac *ApplicationClient) FindApplication(baseDomain string, resource *unstructured.Unstructured) (*Application, error) {
	if resource.GetLabels() == nil {
		return nil, ErrNotBelongToApplication
	}

	v := resource.GetLabels()[getAppNameKey(baseDomain)]
	names := strings.Split(v, ".")
	if len(names) != 2 {
		return nil, ErrNotBelongToApplication
	}
	app, result := ac.GetApplication(names[1], names[0])
	return app, result.CombineError()

}

// UpdateApplicationResource update a resource in application
func (ac *ApplicationClient) UpdateApplicationResource(namespace, name string, resource *unstructured.Unstructured) (*unstructured.Unstructured, error) {
	app, res := ac.GetApplication(namespace, name)
	if res.IsError() {
		return nil, res.CombineError()
	}
	ac.fulfillResourceNamespace(namespace, resource)
	updateResourceLabels(resource, app.appCrd.Spec.Selector.MatchLabels)
	return ac.updateResource(resource)

}

func (ac *ApplicationClient) UpdateApplicationResources(namespace, name string, resources *[]unstructured.Unstructured) (*Application, *Result) {
	app, res := ac.GetApplication(namespace, name)
	if res.IsError() {
		return app, res
	}
	ac.fulfillResourcesNamespace(namespace, resources)

	var result Result
	for _, item := range *resources {
		updateResourceLabels(&item, app.appCrd.Spec.Selector.MatchLabels)

		_, err := ac.updateResource(&item)
		result.addUpdateResult(&item, err)
		ac.sendUpdateEvent(app, &item, err)
		if err != nil {
			return app, &result
		}
	}
	return ac.GetApplication(namespace, name)
}

// DeleteApplicationResource delete a resource from application
func (ac *ApplicationClient) DeleteApplicationResource(namespace, name string, gn GVKName) (*Application, *Result) {
	return ac.DeleteApplicationResources(namespace, name, []GVKName{gn})
}

func (ac *ApplicationClient) RemoveApplicationResource(namespace, name string, resource *unstructured.Unstructured) (*Application, *Result) {
	return ac.RemoveApplicationResources(namespace, name, &[]unstructured.Unstructured{*resource})
}

// RemoveApplicationResources remove resources from application, these resources will still exist
func (ac *ApplicationClient) RemoveApplicationResources(namespace, name string, resources *[]unstructured.Unstructured) (*Application, *Result) {
	app, res := ac.GetApplication(namespace, name)
	if res.IsError() {
		return app, res
	}

	var result Result
	var gns []GVKName

	for _, item := range *resources {
		_, err := ac.patchResource(&item, getRemoveAppLabelsData(&item, app.appCrd.Spec.Selector.MatchLabels), types.StrategicMergePatchType)
		result.addUpdateResult(&item, err)
		if err != nil {
			return app, &result
		}
		gns = append(gns, UnstructToGVKName(&item))
	}

	var newSubs []unstructured.Unstructured
	for _, item := range app.Resources {
		if !isMatchSearch(&item, gns) {
			newSubs = append(newSubs, item)
		}
	}

	return ac.updateApplicationGKS(app, &newSubs, &result)
}

// updateApplicationGKS update the application's GK info based on the new subs(sub resources)
func (ac *ApplicationClient) updateApplicationGKS(app *Application, subs *[]unstructured.Unstructured, result *Result) (*Application, *Result) {
	newAppResource, err := ac.updateAppGKS(app.appResource, subs)
	result.addUpdateResult(app.appResource, err)
	ac.sendUpdateEvent(app, app.appResource, err)
	if err == nil {
		app.appResource = newAppResource
		app.Resources = *subs
	} else {
		// If update failed, setting app to Failed phase.
		newAppResource, err := ac.updateAppPhase(app.appResource, v1beta1.Failed)
		if err == nil {
			app.appResource = newAppResource
		} else {
			result.addUpdateResult(app.appResource, err)
		}
	}
	return app, result
}

// DeleteApplicationResources delete list of resources from application
// return at first error
func (ac *ApplicationClient) DeleteApplicationResources(namespace, name string, gns []GVKName) (*Application, *Result) {
	app, res := ac.GetApplication(namespace, name)
	if res.IsError() {
		return app, res
	}

	var result Result
	ns := app.GetAppCrd().GetNamespace()

	for _, item := range gns {
		resource := GVKNameToUnstruct(item, ns)
		err := ac.deleteResource(resource)
		result.addDeleteResult(resource, err)
		if err != nil {
			return app, &result
		}
	}

	var newSubs []unstructured.Unstructured
	for _, item := range app.Resources {
		if !isMatchSearch(&item, gns) {
			newSubs = append(newSubs, item)
		}
	}
	return ac.updateApplicationGKS(app, &newSubs, &result)
}

func (ac *ApplicationClient) AddApplicationResource(namespace, name string, resource *unstructured.Unstructured) (*Application, *Result) {
	return ac.AddApplicationResources(namespace, name, &[]unstructured.Unstructured{*resource})
}

// ImportApplicationResource import one existing resource to application
// return the updated application and operation result
func (ac *ApplicationClient) ImportApplicationResource(namespace, name string, resource *unstructured.Unstructured) (*Application, *Result) {
	return ac.ImportApplicationResources(namespace, name, &[]unstructured.Unstructured{*resource})
}

// ImportApplicationResources import existing resources to application
// return the updated application and operation result
func (ac *ApplicationClient) ImportApplicationResources(namespace, name string, resources *[]unstructured.Unstructured) (*Application, *Result) {
	app, res := ac.GetApplication(namespace, name)
	if res.IsError() {
		return app, res
	}
	ac.fulfillResourcesNamespace(namespace, resources)

	data := getAddAppLabelsData(app.appCrd.Spec.Selector.MatchLabels)

	var result Result
	for _, item := range *resources {
		newRes, err := ac.patchResource(&item, data, types.StrategicMergePatchType)
		result.addUpdateResult(&item, err)
		ac.sendUpdateEvent(app, &item, err)
		if err != nil {
			return app, &result
		} else {
			app.addResource(newRes)
		}
	}
	return ac.updateApplicationGKS(app, &app.Resources, &result)
}

// AddApplicationResources add resources to application
// return at first error
func (ac *ApplicationClient) AddApplicationResources(namespace, name string, resources *[]unstructured.Unstructured) (*Application, *Result) {
	app, res := ac.GetApplication(namespace, name)
	if res.IsError() {
		return app, res
	}
	ac.fulfillResourcesNamespace(namespace, resources)

	var result Result

	for _, item := range *resources {
		updateResourceLabels(&item, app.appCrd.Spec.Selector.MatchLabels)

		newRes, err := ac.createResource(&item)
		result.addCreateResult(&item, err)
		ac.sendCreateEvent(app, &item, err)
		if err == nil {
			app.addResource(newRes)
		} else {
			return app, &result
		}
	}

	return ac.updateApplicationGKS(app, &app.Resources, &result)

}

func (ac *ApplicationClient) updateAppAnnotation(app *unstructured.Unstructured, key, value string) (*unstructured.Unstructured, error) {
	data := gabs.New()
	if _, err := data.Set(value, "metadata", "annotations", key); err != nil {
		return nil, err
	}
	return ac.patchResource(app, data.Bytes(), types.MergePatchType)

}

func (ac *ApplicationClient) updateAppPhase(app *unstructured.Unstructured, phase string) (*unstructured.Unstructured, error) {
	data := gabs.New()
	data.SetP(phase, "spec.assemblyPhase")
	return ac.patchResource(app, data.Bytes(), types.MergePatchType)
}

func (ac *ApplicationClient) updateAppGKS(app *unstructured.Unstructured, resources *[]unstructured.Unstructured) (*unstructured.Unstructured, error) {
	gk := getCompGroupKinds(resources)
	j := gabs.New()
	j.SetP(gk, "spec.componentKinds")

	return ac.patchResource(app, j.Bytes(), types.MergePatchType)
}
