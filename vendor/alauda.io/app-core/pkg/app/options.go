package app

// ApplicationUpdateOptions options when updating an app
type ApplicationUpdateOptions struct {
	// UpdateConflictMaxRetry when updating an app's resource
	// this option will add a maximum retry number
	// if an update action result in resourceVersion conflict is from server
	UpdateConflictMaxRetry int
}
