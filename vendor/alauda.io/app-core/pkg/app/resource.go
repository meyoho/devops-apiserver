package app

import (
	"errors"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
)

var (
	ErrNotBelongToApplication = errors.New("resource not belong to an application")
)

func (ac *ApplicationClient) getGVKByGroupKind(group, kind string) (*schema.GroupVersionKind, error) {
	version, err := ac.dc.GetVersionByGroup(group)
	if err != nil {
		return nil, err
	}
	return &schema.GroupVersionKind{
		Group:   group,
		Kind:    kind,
		Version: version,
	}, nil

}

func (ac *ApplicationClient) deleteResource(resource *unstructured.Unstructured) error {
	client, err := ac.getClient(resource)
	if err != nil {
		return err
	}

	return client.Delete(resource.GetName(), &metav1.DeleteOptions{})

}

func (ac *ApplicationClient) createResource(resource *unstructured.Unstructured) (*unstructured.Unstructured, error) {
	client, err := ac.getClient(resource)
	if err != nil {
		return nil, err
	}

	return client.Create(resource, metav1.CreateOptions{})
}

func (ac *ApplicationClient) updateResource(resource *unstructured.Unstructured) (*unstructured.Unstructured, error) {
	client, err := ac.getClient(resource)
	if err != nil {
		return nil, err
	}

	currentVersion, err := client.Get(resource.GetName(), metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	resource.SetResourceVersion(currentVersion.GetResourceVersion())

	return client.Update(resource, metav1.UpdateOptions{})
}

func (ac *ApplicationClient) patchResource(resource *unstructured.Unstructured, body []byte, jt types.PatchType) (*unstructured.Unstructured, error) {
	client, err := ac.getClient(resource)
	if err != nil {
		return nil, err
	}

	return client.Patch(resource.GetName(), jt, body, metav1.UpdateOptions{})

}

func (ac *ApplicationClient) getAppResource(namespace, name string) (*unstructured.Unstructured, error) {
	client, err := ac.getAppClient()
	if err != nil {
		return nil, err
	}

	c := client.Namespace(namespace)

	return c.Get(name, metav1.GetOptions{})
}

func (ac *ApplicationClient) listResource(gvk schema.GroupVersionKind, namespace string, options metav1.ListOptions) (*ListObject, error) {
	client, err := ac.getClientByGVK(gvk)
	if err != nil {
		return nil, err
	}

	object, err := client.Namespace(namespace).List(options)
	if err != nil {
		return nil, err
	}
	return parseListObject(object)
}

type GroupVersionKindName interface {
	GroupVersionKind() schema.GroupVersionKind
	GetName() string
}

func UnstructToGVKName(resource *unstructured.Unstructured) GVKName {
	return NewGVKName(resource.GroupVersionKind(), resource.GetName())
}

func GVKNameToUnstruct(gn GVKName, namespace string) *unstructured.Unstructured {
	var res unstructured.Unstructured
	res.SetName(gn.Name)
	res.SetGroupVersionKind(gn.GroupVersionKind)
	res.SetNamespace(namespace)
	return &res
}
