package app

import (
	"alauda.io/app-core/crd/v1beta1"
	"encoding/json"
	"fmt"

	"github.com/Jeffail/gabs"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

const (
	DefaultBaseDomain     = "alauda.io"
	ApplicationKind       = "Application"
	ApplicationAPIVersion = "app.k8s.io/v1beta1"
)

// ApplicationInfo contains the info used to create the Application Crd.
type ApplicationInfo struct {
	Name string

	Namespace string

	// DisplayName of this application, will store it in annotations
	// format: app.<base_domain>/display-name:<value>
	DisplayName string

	// Extra can contains various info  about this Application, it will converted to
	// annotations in format like: extra.<base_domain>/<key>:<value>
	Extra map[string]string
}

type Application struct {
	appResource *unstructured.Unstructured
	appCrd      *v1beta1.Application
	Resources   []unstructured.Unstructured `json:"resources"`
}

// If BaseDomain == "" ,use default BaseDomain `alauda.io`
func (app *Application) GetDisplayName(baseDomain string) string {
	if app.appResource != nil && app.appResource.GetAnnotations() != nil {
		return app.appResource.GetAnnotations()[getDisplayNameKey(baseDomain)]
	}
	return ""
}

// GetAppCrd get Application crd for this Application
func (app *Application) GetAppCrd() *v1beta1.Application {
	if app.appResource != nil {
		crd, _ := unstructuredToAppCrd(app.appResource)
		return crd
	}

	if app.appCrd != nil {
		return app.appCrd
	}
	return nil
}

func (app *Application) GetAllResources() *[]unstructured.Unstructured {
	items := app.Resources
	if app.appResource != nil {
		items = append(items, *app.appResource)
	}
	return &items
}

func (app *Application) String() string {
	if app.appResource == nil {
		return ""
	}
	var sub string
	if app.Resources != nil {
		for _, item := range app.Resources {
			sub += fmt.Sprintf("%s/%s,", item.GetKind(), item.GetName())
		}
	}
	return fmt.Sprintf("Application: %s/%s, SubResources: %s", app.appResource.GetNamespace(), app.appResource.GetName(), sub)
}

func (app *Application) addResource(resource *unstructured.Unstructured) {
	if resource == nil {
		return
	}
	if app.Resources == nil {
		app.Resources = []unstructured.Unstructured{}
	}
	if !isResourceInList(resource, &app.Resources) {
		app.Resources = append(app.Resources, *resource)
	}
}

func appCrdToUnstructured(app *v1beta1.Application) (*unstructured.Unstructured, error) {
	data, err := json.Marshal(app)
	if err != nil {
		return nil, err
	}
	var to unstructured.Unstructured
	err = to.UnmarshalJSON(data)
	return &to, err
}

func unstructuredToAppCrd(resource *unstructured.Unstructured) (*v1beta1.Application, error) {
	data, err := resource.MarshalJSON()
	if err != nil {
		return nil, err
	}
	var app v1beta1.Application
	err = json.Unmarshal(data, &app)
	return &app, err

}

func (app *Application) updateResourceLabels(resource *unstructured.Unstructured) {
	updateResourceLabels(resource, app.appCrd.Spec.Selector.MatchLabels)
}

func updateResourceLabels(resource *unstructured.Unstructured, new map[string]string) {
	labels := resource.GetLabels()
	if labels == nil {
		labels = map[string]string{}
	}
	for k, v := range new {
		labels[k] = v
	}
	resource.SetLabels(labels)
}

func getAddAppLabelsData(appLabels map[string]string) []byte {
	j := gabs.New()
	j.SetP(appLabels, "metadata.labels")
	return j.Bytes()
}

func getRemoveAppLabelsData(resource *unstructured.Unstructured, appLabels map[string]string) []byte {
	var newLabels map[string]interface{}
	labels := resource.GetLabels()

	if labels != nil {
		newLabels = make(map[string]interface{}, len(labels))
		for k, v := range labels {
			if _, ok := appLabels[k]; ok {
				newLabels[k] = nil
			} else {
				newLabels[k] = v
			}
		}
	}

	if newLabels != nil && len(newLabels) == 0 {
		newLabels = nil
	}

	j := gabs.New()
	j.SetP(newLabels, "metadata.labels")

	return j.Bytes()

}

func getCompGroupKinds(resources *[]unstructured.Unstructured) []metav1.GroupKind {
	var result []metav1.GroupKind
	exist := map[string]bool{}

	for _, resource := range *resources {
		if !exist[resource.GetKind()] {
			result = append(result, metav1.GroupKind{
				Kind:  resource.GetKind(),
				Group: resource.GroupVersionKind().Group,
			})
			exist[resource.GetKind()] = true
		}
	}
	return result

}

func getDisplayNameKey(baseDomain string) string {
	if baseDomain == "" {
		baseDomain = DefaultBaseDomain
	}
	return fmt.Sprintf("app.%s/display-name", baseDomain)
}

func getAppNameKey(baseDomain string) string {
	if baseDomain == "" {
		baseDomain = DefaultBaseDomain
	}
	return fmt.Sprintf("app.%s/name", baseDomain)
}

func genAppCrd(info *ApplicationInfo, resources *[]unstructured.Unstructured, baseDomain string) *v1beta1.Application {
	var app v1beta1.Application
	app.Kind = "Application"
	app.APIVersion = "app.k8s.io/v1beta1"
	app.Name = info.Name
	app.Namespace = info.Namespace

	key := getAppNameKey(baseDomain)
	value := fmt.Sprintf("%s.%s", app.Name, app.Namespace)
	selector := metav1.LabelSelector{MatchLabels: map[string]string{key: value}}
	app.Spec.Selector = &selector

	if resources != nil {
		app.Spec.ComponentGroupKinds = getCompGroupKinds(resources)
	}

	annotations := map[string]string{}
	if info.DisplayName != "" {
		annotations[getDisplayNameKey(baseDomain)] = info.DisplayName
	}
	if len(info.Extra) != 0 {
		for k, v := range info.Extra {
			key := fmt.Sprintf("extra.%s/%s", baseDomain, k)
			annotations[key] = v
		}
	}
	app.SetAnnotations(annotations)

	app.Spec.AssemblyPhase = v1beta1.Pending
	return &app
}

func getAppGVK() schema.GroupVersionKind {
	return schema.GroupVersionKind{
		Group:   "app.k8s.io",
		Version: "v1beta1",
		Kind:    ApplicationKind,
	}
}
