package app

import (
	"alauda.io/app-core/crd/v1beta1"
	"encoding/json"
	"errors"
	"fmt"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"strings"
)

type GVKName struct {
	schema.GroupVersionKind
	Name string
}

func NewGVKName(gvk schema.GroupVersionKind, name string) GVKName {
	return GVKName{
		GroupVersionKind: gvk,
		Name:             name,
	}
}

func gvkCompare(l schema.GroupVersionKind, r schema.GroupVersionKind) bool {
	if l.Group == r.Group && l.Kind == r.Kind && l.Version == r.Version {
		return true
	}
	return false
}

func isResourceInList(resource *unstructured.Unstructured, resources *[]unstructured.Unstructured) bool {
	for _, item := range *resources {
		if item.GetKind() == resource.GetKind() && resource.GetName() == item.GetName() {
			return true
		}
	}
	return false
}

func findResourceInList(resource *unstructured.Unstructured, resources *[]unstructured.Unstructured)(bool, *unstructured.Unstructured ) {
	for _, item := range *resources {
		if item.GetKind() == resource.GetKind() && resource.GetName() == item.GetName() {
			return true, &item
		}
	}
	return false, nil
}


func isMatchSearch(resource *unstructured.Unstructured, types []GVKName) bool {
	for _, item := range types {
		if gvkCompare(resource.GroupVersionKind(), item.GroupVersionKind) && resource.GetName() == item.Name {
			return true
		}
	}
	return false
}

type Result struct {
	Items []ResourceResult `json:"items"`

	eventErrors []error
}

type ResourceResult struct {
	schema.GroupVersionKind
	Name string `json:"name"`
	// Create/Add/Remove/Delete/Update
	Action string `json:"action"`
	Error  error  `json:"error"`
}

func (rr ResourceResult) MarshalJSON() ([]byte, error) {
	type alias struct {
		Group   string `json:"group"`
		Version string `json:"version"`
		Kind    string `json:"kind"`
		Name    string `json:"name"`
		Action  string `json:"action"`
		Error   error  `json:"error"`
	}
	var a = alias{
		Group:   rr.Group,
		Version: rr.Version,
		Kind:    rr.Kind,
		Name:    rr.Name,
		Action:  rr.Action,
		Error:   rr.Error,
	}
	return json.Marshal(a)
}

func (rr *ResourceResult) IsError() bool {
	return rr.Error != nil
}

const (
	ActionCreate = "Create"
	ActionUpdate = "Update"
	ActionGet    = "Get"
	ActionList   = "List"
	ActionDelete = "Delete"
)

func (result *Result) IsError() bool {
	return len(result.Errors()) != 0
}

func (result *Result) FailedResources() []ResourceResult {
	var rr []ResourceResult
	for _, item := range result.Items {
		if item.IsError() {
			rr = append(rr, item)
		}
	}
	return rr
}

func (result *Result) SuccessResources() []ResourceResult {
	var rr []ResourceResult
	for _, item := range result.Items {
		if !item.IsError() {
			rr = append(rr, item)
		}
	}
	return rr
}

func (result *Result) addEventError(err error) {
	if result.eventErrors == nil {
		result.eventErrors = []error{}
	}
	if err != nil {
		result.eventErrors = append(result.eventErrors, err)
	}
}

func (result *Result) append(resourceResult *ResourceResult) {
	if result.Items == nil {
		result.Items = []ResourceResult{}
	}
	result.Items = append(result.Items, *resourceResult)
}

// CombineError will return the result errors
// 1. If no errors, return nil
// 2. If only one error, return it directly, k8s error check methods will working perfectly
// 3. multiple errors found, contact the error strings
func (result *Result) CombineError() error {
	errList := result.Errors()
	if len(errList) == 0 {
		return nil
	} else {
		if len(errList) == 1 {
			return errList[0]
		}

		var errStr string
		for _, item := range errList {
			errStr += item.Error() + ","
		}
		return errors.New(errStr)
	}
}

func (result *Result) Errors() []error {
	var errors []error
	if len(result.Items) == 0 {
		return errors
	}
	for _, item := range result.Items {
		if item.Error != nil {
			errors = append(errors, item.Error)
		}
	}
	return errors
}

func (result *Result) addListResult(gvk *schema.GroupVersionKind, err error) {
	result.append(&ResourceResult{
		GroupVersionKind: *gvk,
		Action:           ActionList,
		Error:            err,
	})
}

func (result *Result) addDeleteResult(resource *unstructured.Unstructured, err error) {
	result.append(genResourceResult(resource, ActionDelete, err))
}

func (result *Result) addUpdateResult(resource *unstructured.Unstructured, err error) {
	result.append(genResourceResult(resource, ActionUpdate, err))
}

func (result *Result) addCreateResult(resource *unstructured.Unstructured, err error) {
	result.append(genResourceResult(resource, ActionCreate, err))
}

func (result *Result) addApp(name, action string, err error) {
	result.append(&ResourceResult{
		GroupVersionKind: getAppGVK(),
		Name:             name,
		Action:           action,
		Error:            err,
	})
}

func genResourceResult(resource *unstructured.Unstructured, action string, err error) *ResourceResult {
	return &ResourceResult{
		GroupVersionKind: resource.GroupVersionKind(),
		Name:             resource.GetName(),
		Action:           action,
		Error:            err,
	}
}

func getAppLabelSelector(app *v1beta1.Application) string {
	selector := app.Spec.Selector.MatchLabels

	result := ""
	for k, v := range selector {
		result = fmt.Sprintf("%s=%s,", k, v)
	}

	return strings.TrimSuffix(result, ",")
}

type ListObject struct {
	Kind       string                      `json:"kind"`
	ApiVersion string                      `json:"apiVersion"`
	Items      []unstructured.Unstructured `json:"items"`
}

func parseListObject(object runtime.Object) (*ListObject, error) {
	data, err := json.Marshal(object)
	if err != nil {
		return nil, err
	}
	var lo ListObject
	err = json.Unmarshal(data, &lo)
	return &lo, err
}

type ListObjects struct {
	Objects []ListObject
}

func (l *ListObjects) add(o *ListObject) {
	if l.Objects == nil {
		l.Objects = []ListObject{}
	}
	l.Objects = append(l.Objects, *o)
}

func (l *ListObjects) mergeAll() *[]unstructured.Unstructured {
	var result []unstructured.Unstructured
	if l.Objects != nil {
		for _, item := range l.Objects {
			result = append(result, item.Items...)
		}
	}

	return &result
}
