package app

import (
	"encoding/json"
	"fmt"
	"time"

	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/rand"
)

type Event struct {
	Type    string
	Reason  string
	Message string

	Name      string
	Namespace string
	UID       types.UID
}

func ToUnstruct(object interface{}) (*unstructured.Unstructured, error) {
	var obj unstructured.Unstructured
	data, err := json.Marshal(object)
	if err != nil {
		return &obj, err
	}
	if err := json.Unmarshal(data, &obj); err != nil {
		return nil, err
	} else {
		return &obj, nil
	}
}

func genApplicationEvent(ev *Event) *v1.Event {
	event := v1.Event{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Event",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      ev.Name + "-" + rand.String(5),
			Namespace: ev.Namespace,
		},
		InvolvedObject: v1.ObjectReference{
			Kind:       ApplicationKind,
			Name:       ev.Name,
			Namespace:  ev.Namespace,
			UID:        ev.UID,
			APIVersion: ApplicationAPIVersion,
		},
		Message: ev.Message,
		Reason:  ev.Reason,
		Source: v1.EventSource{
			Component: "app-core",
		},
		Count:          1,
		Type:           ev.Type,
		FirstTimestamp: metav1.Time{Time: time.Now().UTC()},
		LastTimestamp:  metav1.Time{Time: time.Now().UTC()},
	}
	return &event
}

func (ac *ApplicationClient) sendEvent(
	app *Application,
	resource *unstructured.Unstructured,
	eventType, reason, message string) error {
	ev := Event{
		Name:      app.appResource.GetName(),
		Namespace: app.appResource.GetNamespace(),
		UID:       app.appResource.GetUID(),
		Type:      eventType,
		Reason:    reason,
		Message:   message,
	}
	event := genApplicationEvent(&ev)
	data, err := ToUnstruct(event)
	if err != nil {
		return err
	}
	c, err := ac.dc.DynamicClientForResource("events", "")
	if err != nil {
		return err
	}
	_, err = c.Namespace(ev.Namespace).Create(data, metav1.CreateOptions{})
	return err

}

func (ac *ApplicationClient) sendCreateEvent(app *Application, resource *unstructured.Unstructured, err error) error {
	reason := "Created"
	message := fmt.Sprintf("Create %s %s done.", resource.GetKind(), resource.GetName())
	eventType := "Normal"
	if err != nil {
		eventType = "Warning"
		reason = "FailedCreate"
		message = fmt.Sprintf("Create %s %s failed: %s", resource.GetKind(), resource.GetName(), err.Error())
	}
	return ac.sendEvent(app, resource, eventType, reason, message)
}

func (ac *ApplicationClient) sendUpdateEvent(app *Application, resource *unstructured.Unstructured, err error) error {
	reason := "Updated"
	message := fmt.Sprintf("Update %s %s done.", resource.GetKind(), resource.GetName())
	eventType := "Normal"
	if err != nil {
		eventType = "Warning"
		reason = "FailedUpdate"
		message = fmt.Sprintf("Update %s %s failed: %s", resource.GetKind(), resource.GetName(), err.Error())
	}
	return ac.sendEvent(app, resource, eventType, reason, message)
}

func (ac *ApplicationClient) sendDeleteEvent(app *Application, resource *unstructured.Unstructured, err error) error {
	reason := "Deleted"
	message := fmt.Sprintf("Delete %s %s done.", resource.GetKind(), resource.GetName())
	eventType := "Normal"
	if err != nil {
		eventType = "Warning"
		reason = "FailedDelete"
		message = fmt.Sprintf("Delete %s %s failed: %s", resource.GetKind(), resource.GetName(), err.Error())
	}
	return ac.sendEvent(app, resource, eventType, reason, message)
}
