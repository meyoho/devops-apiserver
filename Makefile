.PHONY: init build-base build unitest test
PROJ?=lauda.io/devops-apiserver
PWD=$(shell pwd)
CODEGENERATOR=${GOPATH}/src/k8s.io/code-generator
K8S_ROOT=${GOPATH}/src/k8s.io
APIMACHINERY=${GOPATH}/src/k8s.io/apimachinery
K8SCODE=${GOPATH}/src/k8s.io/kubernetes
TAG=dev-$(shell cat .version)-$(shell git config --get user.email | sed -e "s/@/-/")-$(shell git rev-parse --short HEAD)
IMAGE=index.alauda.cn/alaudak8s/devops-apiserver
TEM_IMAGE=index.alauda.cn/alaudak8s/pipeline-templates
API_IMAGE=index.alauda.cn/alaudak8s/devops-api
image=$(IMAGE)
tag=$(TAG)
PACKAGES = $(shell go list ./... | grep -v 'client' | grep -v 'mock' | grep -v vendor | grep -v 'test')
SET1=$(foreach pkg,$(shell ls ./pkg/ | grep -v client | grep -v mock), $(shell echo './pkg/'$(pkg)'/...'))
BASE_TOKEN=$(shell echo $(TAG))-int-dev
INT_TOKEN=$(shell echo $(BASE_TOKEN) | sed 's|\.|-|g')
PROTOC=$(shell which protoc)
NAMESPACE=alauda-system
CONFIGMAP=jenkins-auto
KUBERNETES_VERSION=release-1.13
CODEGEN_OLD_VERSION=kubernetes-1.9.9
CODEGEN_NEW_VERSION=release-1.13
GOCLIENT_VERSION=release-10.0
export GO111MODULE=on
export GOPROXY=https://athens.acp.alauda.cn

test: fmt vet
	GOPROXY=https://athens.acp.alauda.cn GO111MODULE=on go test -cover -v ./pkg/... -json -coverprofile=coverage-all.out -covermode=count > test.json

test-all: fmt vet
	GOPROXY=https://athens.acp.alauda.cn GO111MODULE=on go test -v ./pkg/...

vet:
	GOPROXY=https://athens.acp.alauda.cn GO111MODULE=on go vet ./...

fmt:
	go fmt ./pkg/...

init: init-k8scode init-dependencies 
	
init-dependencies: init-protoc
	go get -u github.com/alauda/gitversion
	go get -u github.com/tools/godep
	go get github.com/golang/mock/gomock
	go install github.com/golang/mock/mockgen
	go get -u github.com/onsi/ginkgo

init-protoc:
	echo $(PROTOC)
	cd ${K8S_ROOT}/code-generator/cmd/go-to-protobuf && go install
	cd ${K8S_ROOT}/code-generator/cmd/go-to-protobuf/protoc-gen-gogo && go install
	go get -u github.com/gogo/protobuf/protoc-gen-gofast

init-k8scode:
	GO111MODULE=off go get -u k8s.io/kubernetes || true
	cd ${K8SCODE} && git checkout ${KUBERNETES_VERSION}
	GO111MODULE=off go get -u k8s.io/kube-openapi || true
	GO111MODULE=off go get -u k8s.io/code-generator || true
	cd ${K8S_ROOT}/code-generator && git checkout ${CODEGEN_OLD_VERSION} && cd cmd/openapi-gen && go install
	cd ${K8S_ROOT}/code-generator && git checkout ${CODEGEN_NEW_VERSION}
	GO111MODULE=off go get -u k8s.io/apimachinery || true
	cd ${K8S_ROOT}/apimachinery && git checkout ${KUBERNETES_VERSION}
	GO111MODULE=off go get -u k8s.io/api || true
	cd ${K8S_ROOT}/api && git checkout ${KUBERNETES_VERSION}
	GO111MODULE=off go get -u k8s.io/apiserver || true
	cd ${K8S_ROOT}/apiserver && git checkout ${KUBERNETES_VERSION}
	GO111MODULE=off go get -u k8s.io/client-go || true
	cd ${K8S_ROOT}/client-go && git checkout ${GOCLIENT_VERSION}
	GO111MODULE=off go get -u k8s.io/apiextensions-apiserver || true
	cd ${K8S_ROOT}/apiextensions-apiserver && git checkout ${KUBERNETES_VERSION}

clean-k8scode:
	rm -rf ${K8SCODE} 
	rm -rf ${K8S_ROOT}/code-generator 
	rm -rf ${K8S_ROOT}/apimachinery
	rm -rf ${K8S_ROOT}/api 
	rm -rf ${K8S_ROOT}/apiserver
	rm -rf ${K8S_ROOT}/client-go
	rm -rf ${K8S_ROOT}/apiextensions-apiserver
	rm -rf ${K8S_ROOT}/kube-openapi

build:
	GOPROXY=https://athens.acp.alauda.cn GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -ldflags "-w -s" -a -installsuffix cgo -o ${PWD}/bin/devops-apiserver

upx:
	upx ${PWD}/bin/devops-apiserver

build-api:
	GOPROXY=https://athens.acp.alauda.cn GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -ldflags "-w -s" -a -installsuffix cgo -o ${PWD}/apibin/devops-api ./pkg/cmd/devops-api

build-template-image:
	mkdir -p ./templatebin/templates
	cp -r templates ./templatebin/templatedir
	cp artifacts/images/Dockerfile.template ./templatebin/Dockerfile
	docker build -t ${TEM_IMAGE}:${TAG} -f ./templatebin/Dockerfile ./templatebin
	rm -rf ./templatebin



k8s-init:
	kubectl create -f artifacts/deploy/ns.yaml
	kubectl create -f artifacts/deploy/sa.yaml
	kubectl create -f artifacts/deploy/auth-delegator.yaml
	kubectl create -f artifacts/deploy/auth-reader.yaml
	kubectl create -f artifacts/deploy/secret.yaml
	kubectl create -f artifacts/deploy/etcd-pod.yaml
	kubectl create -f artifacts/deploy/deploy.1.yaml
	kubectl create -f artifacts/deploy/service.yaml
	kubectl create -f artifacts/deploy/apiservice.yaml
	$(MAKE) run

k8s-drop:
	kubectl delete -f artifacts/deploy/apiservice.yaml
	kubectl delete -f artifacts/deploy/sa.yaml
	kubectl delete -f artifacts/deploy/auth-delegator.yaml
	kubectl delete -f artifacts/deploy/auth-reader.yaml
	kubectl delete -f artifacts/deploy/secret.yaml
	kubectl delete -f artifacts/deploy/etcd-pod.yaml
	kubectl delete -f artifacts/deploy/deploy.1.yaml
	kubectl delete -f artifacts/deploy/service.yaml
	kubectl delete -f artifacts/deploy/ns.yaml


build-image: build upx
	cp artifacts/images/Dockerfile ./bin
	cp -r groovy ./bin
	cp -r toolx ./bin
	docker build -t ${IMAGE}:${TAG} -f ./bin/Dockerfile ./bin
	rm -rf ./bin

build-api-image: build-api
	upx ${PWD}/apibin/devops-api
	cp artifacts/images/Dockerfile.api ./apibin/Dockerfile
	docker build -t ${API_IMAGE}:${TAG} -f ./apibin/Dockerfile ./apibin
	rm -rf ./apibin

build-tool-image:
	docker build -t ${IMAGE}-tool:${TAG} -f artifacts/images/Dockerfile.tool ./

push-image: build-image
	docker push ${IMAGE}:${TAG}

push-api-image: build-api-image
	docker push ${API_IMAGE}:${TAG}

push-template-image: build-template-image
	docker push ${TEM_IMAGE}:${TAG}

run: push-image
	$(MAKE) image=${IMAGE} tag=${TAG} update

run-sep: push-image
	$(MAKE) image=${IMAGE} tag=${TAG} update-sep

update:
	# kubectl set image deploy/devops-apiserver -n alauda-system server=$(image):$(tag) controller=$(image):$(tag)
	kubectl set image deploy/devops-apiserver -n alauda-system server=$(image):$(tag)
	kubectl set image deploy/devops-controller -n alauda-system controller=$(image):$(tag)
	make restart-sep

update-sep:
	kubectl set image deploy/devops-apiserver -n alauda-system server=$(image):$(tag)
	kubectl set image deploy/devops-controller -n alauda-system controller=$(image):$(tag)
	make restart-sep

controller-m: 
	go run main.go controller-manager --kubeconfig ~/.kube/config --logtostderr -v 5

clean:
	rm -rf ./bin

.PHONY: gen-client
gen-client:
	./hack/update-codegen.sh

.PHONY: build-in-docker
build-in-docker:
	docker run --rm -v ${PWD}:/go/src/alauda.io/devops-apiserver -w /go/src/alauda.io/devops-apiserver index.alauda.cn/library/golang:alpine go install

.PHONY: cover
cover: collect-cover-data test-cover-html

collect-cover-data:
	echo "mode: count" > coverage-all.out
	@$(foreach pkg,$(PACKAGES),\
		go test -v -coverprofile=coverage.out -covermode=count $(pkg);\
		if [ -f coverage.out ]; then\
			tail -n +2 coverage.out >> coverage-all.out;\
		fi;)

test-cover-html:
	go tool cover -html=coverage-all.out -o coverage.html

test-cover-func:
	go tool cover -func=coverage-all.out

open-cover-html:
	open coverage.html

check: check-test check-coverage

check-full: check-lint check-test check-coverage

check-lint:
	gometalinter.v2 --checkstyle --exclude='(.)*/zz_(.)*go' --exclude='(.)*_test(.)go' --exclude='(.)*/generated(.)*go' --deadline 10m $(SET1)  > report.xml || true

check-coverage:
	gocov test $(PACKAGES) | gocov-xml > coverage.xml

check-test:
	go test -v $(PACKAGES) | go-junit-report > test.xml

set:
	echo $(PROTOC)

build-test:
	rm -rf testbin || true
	docker run --rm -v ${PWD}:/go/src/alauda.io/devops-apiserver -w /go/src/alauda.io/devops-apiserver --entrypoint=/go/src/alauda.io/devops-apiserver/hack/build_tests.sh index.alauda.cn/alaudak8s/devops-apiserver-tests-base:latest

build-test-image: build-test
	cp artifacts/images/Dockerfile.test ./testbin/Dockerfile
	docker build -t index.alauda.cn/alaudak8s/devops-apiserver-tests:$(INT_TOKEN) -f testbin/Dockerfile testbin
	docker push index.alauda.cn/alaudak8s/devops-apiserver-tests:$(INT_TOKEN)

integration: int-cleanup build-test-image 
	echo "needs a alauda-system/jenkins-auto configmap to store auto-test configuration, all keys can be fetched from test/common_data.go"
	cp test/integration-tests.yaml tmp.int.yaml
	cp test/configmap.yaml tmp.configmap.yaml
	hack/gen_configmap.sh $(NAMESPACE) $(CONFIGMAP) tmp.configmap.yaml
	sed -i '' "s|%TOKEN%|$(INT_TOKEN)|g" tmp.int.yaml
	sed -i '' "s|%TOKEN%|$(INT_TOKEN)|g" tmp.configmap.yaml
	sed -i '' "s|%IMAGE%|index.alauda.cn/alaudak8s/devops-apiserver-tests:$(INT_TOKEN)|g" tmp.int.yaml
	kubectl create -f tmp.int.yaml
	kubectl create -f tmp.configmap.yaml

local-integration:
	export EXPORT_VARS=true GENERATE_REPORT=false SOURCE_CODE=true CONTEXT=test; test/run.sh ./hack/gen_configmap.sh

int-cleanup:
	kubectl delete -f tmp.int.yaml || true
	curl -X DELETE -H 'Authorization: Token '$(INT_TOKEN) http://sonobuoy-alaudak8s.myalauda.cn/status/
	rm tmp.*.yaml || true

int-status:
	curl -H 'Authorization: Token '$(INT_TOKEN) http://sonobuoy-alaudak8s.myalauda.cn/status/ | python -m json.tool

int-pods:
	kubectl get pods -n sonobuoy$(INT_TOKEN)

int-log:
	kubectl logs -f -n sonobuoy$(INT_TOKEN) -c devops-api $(shell kubectl -n sonobuoy$(INT_TOKEN) get pods -o=custom-columns=NAME:.metadata.name | grep sonobuoy-devops-api-job-)

gen-protobuf:
	./hack/generate-proto.sh

install-vendor:
	cd ${K8S_ROOT}/kubernetes && godep restore

gen-mock:
	mockgen -destination ./pkg/mock/client-go/informers/factory.go -package informers k8s.io/client-go/informers SharedInformerFactory
	mockgen -destination ./pkg/mock/client-go/workqueue/workqueue.go -package workqueue k8s.io/client-go/util/workqueue RateLimitingInterface,RateLimiter,DelayingInterface
	mockgen -destination ./pkg/mock/apiserver/registry/storage.go -package registry k8s.io/apiserver/pkg/registry/rest StandardStorage
	mockgen -destination ./pkg/mock/apimachinery/meta/object.go -package meta k8s.io/apimachinery/pkg/apis/meta/v1 Object
	mockgen -destination ./pkg/mock/apimachinery/runtime/runtime.go -package meta k8s.io/apimachinery/pkg/runtime Object
	mockgen -destination ./pkg/mock/printers/handler.go -package printers k8s.io/kubernetes/pkg/printers PrintHandler
	mockgen -destination ./pkg/mock/mhttp/roundtripper.go -package mhttp net/http RoundTripper
	mockgen -destination ./pkg/mock/dependency/interface.go -package dependency alauda.io/devops-apiserver/pkg/dependency Manager,Resolver
	mockgen -destination ./pkg/mock/devops/informers/factory.go -package informers alauda.io/devops-apiserver/pkg/client/informers/externalversions SharedInformerFactory
	mockgen -destination ./pkg/mock/devops/registry/url_builder.go -package registry alauda.io/devops-apiserver/pkg/registry/devops/pipeline URLBuilder
	mockgen -destination ./pkg/mock/devops/controller/manager.go -package controller alauda.io/devops-apiserver/pkg/controller/manager Manager
	mockgen -destination ./pkg/mock/devops/controller/projectmanagementbinding.go -package controller alauda.io/devops-apiserver/pkg/client/clientset/versioned/typed/devops/v1alpha1 ProjectManagementBindingExpansion
	mockgen -destination ./pkg/mock/devops/controller/projectmanagement.go -package controller alauda.io/devops-apiserver/pkg/client/clientset/versioned/typed/devops/v1alpha1 ProjectManagementExpansion
	mockgen -destination ./pkg/mock/devops/controller/projectmanagement.go -package controller alauda.io/devops-apiserver/pkg/client/clientset/versioned/typed/devops/v1alpha1 ProjectManagementExpansion
	mockgen -destination ./pkg/mock/download/download.go -package download alauda.io/devops-apiserver/pkg/registry/generic/download Downloader
	mockgen -destination ./pkg/mock/devops/controller/queuehandler.go -package controller alauda.io/devops-apiserver/pkg/controller/handler QueueEventHandler
	mockgen -destination ./pkg/mock/devops/controller/templatesync.go -package controller alauda.io/devops-apiserver/pkg/controller/devops/pipelinetemplatesync SyncerInterface
	mockgen -destination ./pkg/mock/devops/controller/codequalityprojectsync.go -package controller alauda.io/devops-apiserver/pkg/controller/devops/codequalitybinding CodeQualityProjectSyncer
	mockgen -destination ./pkg/mock/sigs.k8s.io/controller/manager.go -package controller sigs.k8s.io/controller-runtime/pkg/manager Manager
	mockgen -destination ./pkg/mock/sigs.k8s.io/controller/handler.go -package controller sigs.k8s.io/controller-runtime/pkg/handler EventHandler
	mockgen -destination ./pkg/mock/sigs.k8s.io/controller/reconcile.go -package controller sigs.k8s.io/controller-runtime/pkg/reconcile Reconciler
	mockgen -destination ./pkg/mock/sigs.k8s.io/controller/controller.go -package controller sigs.k8s.io/controller-runtime/pkg/controller Controller
	mockgen -destination ./pkg/mock/ace/interface.go -package ace alauda.io/devops-apiserver/pkg/client/thirdparty/ace Interface

	mockgen -destination ./pkg/mock/thirdparty/external/alauda.go -package external alauda.io/devops-apiserver/pkg/client/thirdparty/devops/externalversions AlaudaClientFactory,Interface
	find ./pkg/mock/ -type f -print0 | xargs -0 sed -i '' "s|alauda.io/devops-apiserver/vendor/||g" 
	find ./pkg/mock/ -type f -print0 | xargs -0 sed -i '' "s|k8s.io/kubernetes/vendor/||g" 
	find ./pkg/mock/ -type f -print0 | xargs -0 sed -i '' "s|sigs.k8s.io/controller-runtime/vendor/||g" 
	# mockgen -destination ./pkg/mock/apiserver/registry/generic.go -package registry k8s.io/apiserver/pkg/registry/generic RESTOptionsGetter
	
restart:
	kubectl scale deploy -n alauda-system devops-apiserver --replicas=0
	kubectl scale deploy -n alauda-system devops-apiserver --replicas=1

server-logs:
	kubectl logs -f deploy/devops-apiserver -c server -n alauda-system

controller-logs:
	kubectl logs -f deploy/devops-apiserver -c controller -n alauda-system
	
restart-sep:
	kubectl scale deploy -n alauda-system devops-apiserver --replicas=0
	kubectl scale deploy -n alauda-system devops-apiserver --replicas=1
	kubectl scale deploy -n alauda-system devops-controller --replicas=0
	kubectl scale deploy -n alauda-system devops-controller --replicas=1
	
startapi:
	GOPROXY=https://athens.acp.alauda.cn GO111MODULE=on go run pkg/cmd/devops-api/api.go --kubeconfig=${HOME}/.kube/config --basedomain=alauda.io

buildapiserver:
	CGO_ENABLED=0 GOOS=linux go build -ldflags '-w -s' -a -installsuffix cgo -o bin/devops-apiserver
	cp artifacts/images/Dockerfile ./bin
	cp -r groovy ./bin
	cp -r toolx ./bin
	ls -lha bin
	upx bin/devops-apiserver
	cd bin
	docker build -t index.alauda.cn/alaudak8s/devops-apiserver:$(tag) ./bin
	docker push index.alauda.cn/alaudak8s/devops-apiserver:$(tag)

buildapi:
	CGO_ENABLED=0 GOOS=linux go build -ldflags '-w -s' -a -installsuffix cgo -o apibin/devops-api ./pkg/cmd/devops-api
	cp artifacts/images/Dockerfile.api ./apibin/Dockerfile
	ls -lha apibin
	cd apibin
	docker build -t index.alauda.cn/alaudak8s/devops-api:$(tag) ./apibin
	docker push index.alauda.cn/alaudak8s/devops-api:$(tag)

# usage: make cleanbuild tag=whateveryouwant
cleanbuild: buildapiserver buildapi
	rm -rf ./bin && rm -rf ./apibin
