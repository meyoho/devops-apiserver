# Devops-apiserver

API server and controller for devops.alauda.io k8s resources

### Pre-requisites
Before starting make sure of the following:

1. this repo is available at `$GOPATH/src/alauda.io/devops-apiserver`
2. kubectl is installed and configured to use minikube or docker's kubernetes
3. your kubernetes cluster is up and running
4. manually docker login to index.alauda.cn using alaudak8s' account (or change the Makefile and `artifacts/deploy/secret.yaml` and `artifacts/deploy/deploy.yaml` accordingly)
5. you have upx installed `brew install upx`

### Initial setup

We need to add a few resources to the cluster before starting, including the deployment of our API server  
You can see all the necessary files and tweek as necessary in the `artifacts/deploy` folder  

```
make k8s-init
```

Check the logs for any errors and fix them as necessary

### Up and running

After initial setup, you can build your own image and deploy to the cluster with one command:

```
make run
```

This command will:
1. Build the application locally and upx it
2. Build the docker image locally and push it to the index.alauda.cn registry (unless you change the Makefile)
3. Update the image reference in the deployment
4. Down scale and upscale the deployment again forcing an update

If everything runs smoothly you should see the pods using

```
kubectl get pods -n alauda-system
```

### Debugging

Debugging tools will not work specially while deploying on a cluster, so we rely on logs for debugging

replace `<container>` with `server` or `controller`

```
kubectl logs -f -n alauda-system deploy/devops-apiserver -c <container>
```

As for development and `minikube` we need to use the `etcd` container in the deployment, and unless the data is stored outside it will, mostlikely loose all the data as soon as it is re-deployed.

### Code generation

Never change the code inside `pkg/client` manually. We can generate this code automatically with the code-generator provided by kubernetes team.

You can install all the necessary tools for development using

```
make init
```

Make sure to read this amazing guide https://blog.openshift.com/kubernetes-deep-dive-code-generation-customresources/

All those comments with special tags are really important

Once you changed any of the `types.go` file inside `apis/` file you should generate the client folder files again running:

```
make gen-client
```

### Dependencies

We relly on the soon-to-be official dependency management [dep](https://golang.github.io/dep/)

```
go get -u github.com/golang/dep/cmd/dep
```

If any external dependencies were added just make sure to run `dep ensure` before commiting

To make sure all dependencies are there please run

```
make build-in-docker
```

### Contribution

[研发流程与规范](http://confluence.alaudatech.com/pages/viewpage.action?pageId=26313295)

Read more details about current project at [doc directory](doc).
