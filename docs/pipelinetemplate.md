+++
title = "自定义流水线模板文档"
description = ""
weight = 2
alwaysopen = false
level = "D"
+++

# 流水线模板

# 自定义
如果官方提供的模板无法满足流水线配置的实际需要，可以按照下面给出的步骤完成流水线模板的自定义。

## 背景知识
在动手编写模板之前，首先需要对模板涉及到的技术有所有了解。

模板采用 `YAML` 的格式，并采用 `Go Template` 进行解析和渲染（在使用变量的过程中，就需要遵守 `Go Template` 的规范）。
而重要的流水线逻辑部分，则必须要能够熟悉 `Jenkins` 的流水线 `Jenkinsfile` 语法，熟悉常用的流水线步骤（step）的使用。

而对于具体的业务场景，我们同样需要相关的技术。例如：我们想要编写一个构建 `Maven` 项目，并推送到 `Docker` 镜像仓库中的话，
就需要对 `Java`、`Maven`、`Docker` 等技术有一定程度的掌握。

读者应该对 `Kubernetes` 的基本概念有一定的了解，以及掌握命令行工具 `kubectl` 的基本使用。

### 参考链接

* https://yaml.org/
* https://golang.org/pkg/text/template/
* https://github.com/kubernetes/kubectl
* https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/

### Jenkins 流水线相关资料

* http://alauda.study.163.com/ 所在目录为：内部技术分享》DevOps 主题培训
* http://confluence.alaudatech.com/pages/viewpage.action?pageId=36870465
* https://jenkins.io/doc/pipeline/tour/hello-world/
* https://jenkins.io/doc/book/pipeline/syntax/
* https://jenkins.io/doc/pipeline/steps/

## 基本原理
流水线模板的基本原理，就是把 `Jenkinsfile` 中业务相关的、不确定的部分使用 `Go Template` 的语法写成变量的形式，
每个阶段（stage）逻辑会放置到各自单独的任务模板文件 `task-xxx.yaml` 中，而包含多个阶段（stage）的流水线会放置到
模板文件 `template-xxx.yaml` 中。一个流水线模板文件会引用到多个任务模板文件。最后，在由我们的平台通过模板创建出流水线后，
模板渲染引擎会负责把上述的多个 `YAML` 文件生成出一个 `Jenkinsfile` 文件。

流水线模板渲染引擎 https://bitbucket.org/mathildetech/jenkinsfilext

## 示例
这里以构建 `Java` 程序为例说明如何编写模板：

### 流水线任务模板
```yaml
apiVersion: devops.alauda.io/v1alpha1
kind: PipelineTaskTemplate
metadata:
  name: maven
  annotations:
    displayName.zh-CN: Maven构建
    displayName.en: Maven Build
    description.zh-CN: Maven构建
    description.en: Maven Build
    readme.zh-CN: Maven构建
    readme.en: Maven Build
    version: "2.0.0"
    style.icon: ""
  labels:
    category: CI
spec:
  engine: gotpl
  body: |+
    script {
      container('java'){
        sh """{{.buildCommand}}"""
      }
    }
  arguments:
    - name: "buildCommand"
      schema:
        type: string
      display:
        type: code
        name:
          zh-CN: "构建命令"
          en: "Build Command"
        description:
          zh-CN: "自定义更详细的构建命令。默认为：mvn clean package"
          en: ""
      required: true
      default: 'mvn clean package'
```

### 流水线模板
```yaml
apiVersion: devops.alauda.io/v1alpha1
kind: ClusterPipelineTemplate
metadata:
  name: JavaBuilder
  namespace: default
  annotations:
    displayName.zh-CN: "Java 构建"
    displayName.en: "Java Build"
    description.zh-CN: "克隆代码 -> Java 构建 -> 代码扫描 -> Docker 构建"
    description.en: "Clone -> Java Build -> Code Scan -> Docker Build"
    version: "2.0.1"
    style.icon: java,maven,docker,sonarqube
  labels:
    category: Build
spec:
  engine: graph
  withSCM: true
  agent:
    label: java
    labelMatcher: java.*
  options:
    raw: "buildDiscarder(logRotator(numToKeepStr: '200'))"
  stages:
    - name: Clone
      tasks:
        - name: clone
          kind: ClusterPipelineTaskTemplate
          type: public/clone
    - name: "Java Build"
      tasks:
        - name: maven
          kind: ClusterPipelineTaskTemplate
          type: public/maven
    - name: "Code Scan"
      tasks:
        - name: sonar
          kind: ClusterPipelineTaskTemplate
          type: public/sonar
          relation:
            - action: show
              when:
                name: UseSonarQube
                value: true
    - name: "Docker Build"
      tasks:
        - name: build-docker
          kind: ClusterPipelineTaskTemplate
          type: public/build-docker
  arguments:
    - displayName:
        zh-CN: "代码检出"
        en: "Clone"
      items:
        - name: "PlatformCodeRepository"
          schema:
            type: alauda.io/coderepositorymix
          required: true
          value: ""
          binding:
            - clone.args.PlatformCodeRepository
            - sonar.args.PlatformCodeRepository
          display:
            type: alauda.io/coderepositorymix
            name:
              zh-CN: "代码仓库"
              en: RepositoryPath
            description:
              zh-CN: "选择已为项目分配的代码仓库"
              en: ""
...........................................
```

## 更多例子
你可以参考[实际项目中使用到的模板](https://bitbucket.org/mathildetech/private-pipeline-templates/src/master/contri/)。
另外，也可以参考官方模板，包括：[ACP](src/test/resources/acp)、[ACE](src/test/resources/ace)或者[公共模板](src/test/resources/public)。

## CRD
|Kind|说明|
|-----|----|
|`PipelineTaskTemplate`|自定义流水线任务模板|
|`PipelineTemplate`|自定义流水线模板|
|`ClusterPipelineTaskTemplate`|官方流水线任务模板|
|`ClusterPipelineTemplate`|官方流水线模板|

### 字段说明
> 文中 使用[] 表示当前对象是一个数组， 例如， stages[].name 表示 stages 是一个数组， 其中数组中的对象，有一个 name 属性  
> 文中使用 ”“ 表示当前内容是一个整体，表示一个key. 例如  `metadata.annotations."version"` 用来表示， metadata 对象 有个 annotations 数组， 其中的元素，含有一个key 为 version的元素
  对应的 yaml 为 
  ```
  metadata:
    annotations:
      version: 1.0
  ```
> 文中使用 ? 来表示 ， 可以是任意值，或者给定的值中的任意值。
  例如，`spec.post.?[].kind` 表示， spec 有一个 post属性，post 下可以使用值为 always, success 等， 本身是一个数组
  对应的yaml为
  ```
  spec:
    post:
      success:
        - kind: ClusterPipelineTaskTemplate
          name: Notify
      always:
        - kind: ClusterPipelineTaskTemplate
          name: Notify
      failure:
        - kind: ClusterPipelineTaskTemplate
          name: Notify
  ```

|字段|说明|
|-----|----|
|`metadata.annotations."version"`|平台以版本号是否变更为依据来判断模板是否有变化，在导入模板时，如果版本号没有变化的话，平台将会忽略|
|`metadata.annotations."style.icon"`|图标配置，平台的模板列表页面会根据这个来显示响应的图标，例如：nodejs,docker,sonarqube,kubernetes|
|`metadata.labels.category`|模板的分类，包含：Build、SyncImage、DeployService等|
|`spec.body`|流水线中某个阶段的逻辑代码，部分不确定的代码会采用 `Go Template` 的语法来写成变量|
|`spec.agent.label`|这里对应 `Jenkinsfile` 中的 `agent`，当值为 `java` 时，效果如下：agent{label: 'java'}|
|`spec.agent.labelMatcher`|筛选推荐用于这个模版的 label 的正则表达式|
|`spec.agent.raw`|支持任意类型的 `agent`，例如："{label: 'java'}"，效果如下：agent{label: 'java'}, 具体可以参考https://jenkins.io/doc/book/pipeline/syntax/#agent|
|`spec.options.raw`|流水线的配置，与 Jenkinsfile 中的 `options` 相对应|
|`spec.stages`|流水线的阶段定义，如 Jenkinsfile 中的 `stage` 相对应|
|`spec.stages[].name`|阶段名称|
|`spec.stages[].name.tasks`|阶段中包含的任务列表|
|`spec.stages[].name.tasks[].name`|任务模板的名称|
|`spec.stages[].name.tasks[].id`|表示当前任务的唯一标识符号，可选，如果为空，则默认和name 一致， 当同一个流水线中，出现两个相同的 task时，可以用来进行区分。 binding 时，需要使用该值为prefix|
|`spec.stages[].name.tasks[].kind`|任务类型，可选值包括：`ClusterPipelineTaskTemplate`、`PipelineTaskTemplate`， 如果引用的是平台级别的模板，则需要使用`ClusterPipelineTaskTemplate`， 如果是项目级别的模板则使用`PipelineTaskTemplate`|
|`spec.post`| 表示pipeline 结束之后运行的逻辑，通常可以用来实现通知等逻辑|
|`spec.post.?`| 可自定义post condition, 支持 always, aborted, failure, success 等，详细可参考 https://jenkins.io/doc/book/pipeline/syntax/#post|
|`spec.post.?[].kind`| 任务类型，可选值包括：`ClusterPipelineTaskTemplate`、`PipelineTaskTemplate`， 如果引用的是平台级别的模板，则需要使用`ClusterPipelineTaskTemplate`， 如果是项目级别的模板则使用`PipelineTaskTemplate`|
|`spec.post.?[].name`| 任务模板的名称|
|`spec.post.?[].agent`|表示当前任务执行的agent，同`spec.agent`|
|`spec.post.?[].relation`|表示当前任务的联动情况，详细可参见 表单联动的说明|
|`spec.parameters`|定义流水线参数化执行的参数列表，生成的脚本参考[这里](https://jenkins.io/zh/doc/book/pipeline/syntax/#参数)|
|`spec.parameters[].name`|参数名称|
|`spec.parameters[].type`|参数化的类型，支持的类型包括：`string`、`boolean`|
|`spec.parameters[].value`|参数默认值|
|`spec.parameters[].description`|参数描述|
|`spec.arguments`|参数部分，与上面 `body` 中的参数对应，前端页面部分会据此渲染动态表单让用户来填写或者选择|
|`spec.arguments[].displayName`|阶段描述，支持中、英文格式|
|`spec.arguments[]."displayName.zh-CN"`|阶段描述中文字段|
|`spec.arguments[]."displayName.en"`|阶段描述英文字段|
|`spec.arguments[].items`|阶段中的参数列表|
|`spec.arguments[].items[].name`|参数名称|
|`spec.arguments[].items[].schema.type`|参数类型（后端）|
|`spec.arguments[].items[].binding`|字符串数组类型，参数与任务模板中的参数绑定，例如：<task.id>.args.<paramName>|
|`spec.arguments[].items[].display`|前端展示使用|
|`spec.arguments[].items[].display.type`|参数类型（前端）|
|`spec.arguments[].items[].display.args`|前端展示使用的额外信息，具体参见动态表单各个控件的说明|
|`spec.arguments[].items[].require`|是否为必要的参数，`true` 为必须要填写的|
|`spec.arguments[].items[].default`|默认值，如果schema.type是boolean, 那么default 不能为空，需要是"true"或者"false"|
|`spec.arguments[].items[].relation`|表单联动配置，可以配置多个|
|`spec.arguments[].items[].relation[].action`|联动的动作，值包括：`show`、`hidden`|
|`spec.arguments[].items[].relation[].when`|联动的动作条件|
|`spec.arguments[].items[].relation[].when.name`|联动的字段名称|
|`spec.arguments[].items[].relation[].when.value`|联动的字段值|
|`spec.arguments[].items[].relation[].when.all`|联动的动作条件, 是一个数组, 表示这些条件需要同时满足|
|`spec.arguments[].items[].relation[].when.all[].name`|某一个联动条件的字段名称|
|`spec.arguments[].items[].relation[].when.all[].value`|某一个联动条件的字段值|
|`spec.arguments[].items[].relation[].when.any`|联动的动作条件, 是一个数组, 表示这些条件有任何一个满足即可. when.all 和 when.any 只能同时设置其中一个字段|
|`spec.arguments[].items[].relation[].when.any[].name`|某一个联动条件的字段名称|
|`spec.arguments[].items[].relation[].when.any[].value`|某一个联动条件的字段值|

### 动态表单

|类型|说明|
|---|---|
|string|字符串格式|
|alauda.io/codebranch|代码分支，字符串格式|
|alauda.io/clustername|集群名称，字符串格式|
|alauda.io/containerName|容器名称，字符串格式|
|alauda.io/k8snamespace|命名空间，字符串格式|
|alauda.io/applicationName|应用名称，字符串格式|
|alauda.io/componentName|组件名称，字符串格式|
|alauda.io/componentClass|组件类型，例如：deployment等，字符串格式|
|alauda.io/dockerimagerepositorypullmix|镜像仓库，包括的字段有：credentialId,secretNamespace,repositoryPath,tag|
|alauda.io/coderepositorymix|代码仓库，包括的字段有：url,credentialId,sourceType|
|alauda.io/jenkinscredentials|devops凭据，用来下拉显示当前项目下的devops凭据, 详细参见下文 [alauda.io/jenkinscredentials](#alauda.io/jenkinscredentials)|

#### alauda.io/jenkinscredentials
支持的 display.args:
- `type`: 可选, 可以用来过滤显示的 k8s 的 secret type , 可选值例如 kubernetes.io/dockerconfigjson 或 kubernetes.io/service-account-token
例如：
```
display:
  type: alauda.io/jenkinscredentials
  args:
    type: kubernetes.io/dockerconfigjson
```

完整的使用例子:
```
name: "sourceCredentialsId"
schema:
  type: string
binding:
  - alaudaSyncImage.args.sourceCredentialsId
display:
  type: alauda.io/jenkinscredentials
  args:
    type: kubernetes.io/dockerconfigjson
  name:
    zh-CN: "源镜像凭据"
    en: "Source Credentials"
  description:
    zh-CN: "拉取镜像时，使用的凭据"
    en: "Source Credentials"
required: false
```

表单的字段类型，大致可以分为：简单类型、符合类型。简单类型以字符串类型
为代表，只有单一的值。符合类型，通常是 `key-value` 的形式，可能会
包含多个值。

### 表单联动

**单个字段的联动控制**

当我们的模板在用户填写表单时，需要有联动的需求，就可以利用下面的功能来实现。

```yaml
spec:
  arguments:
    - displayName:
      items:
      - name: "CodeQualityBinding"
        relation:
          - action: show
            when:
              name: UseSonarQube
              value: true
          - action: hidden
            when:
              name: UseSonarQube
              value: false
```

上面配置的含义为：当 `UseSonarQube` 的值为 `true` 时，会显示 `CodeQualityBinding`。

**多个字段的联动控制**

除了上边的支持单个开关的控制， 我们还支持多个字段的控制。例如，两个字段同时为True，则当前某个字段显示。或者两个字段有任意一个满足条件，则显示。

例如, 如下yaml 表示对于 `ApplicationName` 这个字段，当 同时满足两个条件 `OpenDeploy=true` && `UseAppYaml=false`时，进行展示。
如果要设置或的关系，可以设置 `when.any` 字段即可, 详细可参考字段的定义说明。

```yaml
spec:
  arguments:
    - displayName:
      items:
      - name: "ApplicationName"
        relation:
        - action: show
          when:
            all:
            - name: OpenDeploy
              value: true
            - name: UseAppYaml
              value: false
```

### 模板中的Label

模板中含有特定的Label可供页面上过滤使用。  
例如 `category`, `lang`。

对于 `category`, 目前系统中的内置流水线模板的category为

- Build
- DeployService
- SyncImage

对于 `lang`, 目前系统中内置的流水线模板的lang为
- Golang
- Java
- Python
- Nodejs

这些 Label 会在 创建流水线的时候，显示出来。

### 插件依赖
流水线的脚本会用到很多的插件，如果涉及到一些不常用的插件，为了避免在
模板的使用过程中出现一些由于缺少插件而不容易检查到的问题。你可以在任务
模板中增加插件依赖的配置，我们平台的 [Jenkins 插件](https://github.com/alauda/alauda-devops-sync-plugin)
会在同步过程中做依赖检查，并会在缺少依赖是记录相关的信息。

参考的配置如下：
```yaml
spec:
  dependencies:
    plugins:
      - name: alauda-pipeline-plugin
        version: "1.3"
```

### 全局变量
由我们的插件提供的全局环境变量，具体的文档可以在 Jenkins 的界面中查看，地址为：`http://localhost:8080/jenkins/pipeline-syntax/globals#alaudaDevops`

## 如何测试
当模板编写完成后，可以通过 `kubectl` 将资源导入 `k8s` 后即可进行后续的测试。执行命令如下：

`kubectl apply -f template-xxx.yaml`

需要注意的是，template 模板中会引用 task 的模板，他们是有引用关系的，所以首先导入 `task-xxx.yaml`，然后再导入 `template-xxx.yaml`。如果没有报错的话，就说明导入成功了。

## troubleshooting
待完善。

## FAQ

- 如何获取产品携带的官方模板的源代码？
模板的源代码就在当前 仓库的 ./src/test/resources/ 中。
但是由于不同版本的产品，对应的版本不同，可以直接从 部署的 devops-controller的 pod 的 /templates 目录，获取对应的模板。该内容即为当前部署版本对应的模板版本。

- 官方模板， 非官方模板， 平台级别的模板， 项目级别的模板 四个概念有什么区别和联系？
`官方模板`：是产品在初始化时， 默认安装的，目前都是  ClusterPipelineTemplate 或 ClusterPipelineTaskTemplate。 资源上含有  source=official 的 label.
`非官方模板`：用户自己定义，自己导入的模板，即为非官方模板。 资源上含有  source=customer 的 label
`平台级别的模板`： ClusterPipelineTemplate 和  ClusterPipelineTaskTemplate 是 平台级别的模板， 在平台和各个项目都能看到。
`项目级别的模板`： PipelineTemplate 和  PipelineTaskTemplate 都是项目级别的模板， 只有当前项目能看到。

  换个角度理解：
    - `是否官方`： 取决于 资源上 source label 的 值， source=official 或者 customer . 
    - `是否平台级别`： 取决于模板是从项目导入的，还是平台导入的（目前产品不支持用户从平台导入）
另外： 只要是从项目下导入的模板，Kind 都会变成 PipelineTemplate 或者 PipelineTaskTemplate, 会自动增加source=customer (不要模板中写source)




## 限制
待完善。
