# subResources

Expose the kubernetes token by the following command:
`
export token=$(kubectl -n cpaas-system get secret | grep devops-apiserver-token | awk '{print $1}' | \
xargs kubectl -n cpaas-system get secret -o jsonpath={.data.token} | base64 --d)
`

## toggle

Check if it's ready by the following command:
```
curl -s -k  https://localhost:6443/apis/devops.alauda.io/v1alpha1 --header "Authorization: Bearer ${token}" | grep pipelineconfigs/toggle
```

Enable a PipelineConfig by the following command:
```shell script
curl -s -k -H "Content-Type:application/json" https://localhost:6443/apis/devops.alauda.io/v1alpha1/namespaces/zxj/pipelineconfigs/param/toggle -X POST \
--data '{"enable": true}' --header "Authorization: Bearer ${token}"
```

Disable a PipelineConfig by the following command:
```shell script
curl -s -k -H "Content-Type:application/json" https://localhost:6443/apis/devops.alauda.io/v1alpha1/namespaces/zxj/pipelineconfigs/multi-pipelien-param-copy/toggle -X POST \
--data '{"enable": false}' --header "Authorization: Bearer ${token}"
```
