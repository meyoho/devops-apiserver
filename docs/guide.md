+++
title = "开发入门指导"
description = ""
weight = 2
alwaysopen = false
level = "D"
+++

# 开发笔记

[TOC]

## 概述

`devops-apiserver` 包含了两个子命令: `server` 和 `controller`，它们有完全不同的职责，所以是把它们部署在一个 `deploy` 中两个不同的 `container` 中。具体配置可见 `artifacts/deploy/deploy.yaml` 文件。

这个项目的目的主要是对 `k8s` 的功能进行扩展，添加自定义的资源类型，可以像操作原生资源一样，对这些新资源进行增删改查等操作。

添加自定义资源有两种方式:

1. 将定义资源格式的 `crd` 模板，导入到 `k8s` 中，可参考 `catalog-controller` 中 `chart` 和 `release` 的实现方式
2. 在代码中定义资源的 `struct`，然后启动 `server` 时候注册到 `k8s` 中。

本项目是用第二种方式来添加自定义资源，`struct` 的定义可以参考 `pkg/apis/devops/types.go` 中的代码。具体的定义规范参见下文。

## server

`server` 实际上 `k8s` 中 `apiservice` 的扩展，可以在里面添加自定义的资源类型，目前在它里面添加的资源类型有:

* Project
* Jenkins
* JenkinsBinding
* PipelineConfig
* Pipeline
* CodeRepoService
* CodeRepoBinding
* CodeRepository

### `struct` 的定义

添加自定义资源类型，大致需要如下操作：

1. 在 `pkg/apis/devops/types.go` 中添加资源类定义
2. 在 `pkg/apis/devops/v1alpha1/types.go` 中添加资源类定义
3. 生成 `client`
4. 注册新添加的类型

#### 资源的定义

如果资源属于 `cluster` 级别，在类的上面添加三行注释:

``` golang
// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoService struct holds a reference to a specific CodeRepoService configuration
// and some user data for access
type CodeRepoService struct {
    ...
}
```

如果资源属于 `namespace` 级别，在类的上面添加如下两行注释:

``` golang
// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoBinding struct holds a reference to a specific CodeRepoBinding configuration
// and some user data for access
type CodeRepoBinding struct {
    ....
}
```

每种资源还需要定义一个列表类，格式如下所示：

``` golang
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CodeRepoServiceList is a list of CodeRepoService objects.
type CodeRepoServiceList struct {
    metav1.TypeMeta
    metav1.ListMeta

    // Items is a list of CodeRepoService
    Items []CodeRepoService
}
```

定义好上面的类或方法后，需要把它们拷贝到 `pkg/apis/devops/v1alpha1/types.go` 中，并给每个字段添加 `json` 字段定义，以及添加相应的备注。如下所示：

``` golang
// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Project struct to hold the project data
type Project struct {
    metav1.TypeMeta `json:",inline"`
    // Standard object's metadata.
    // More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
    // +optional
    metav1.ObjectMeta `json:"metadata,omitempty"`

    // Specification of the desired behavior of the Project.
    // +optional
    Spec ProjectSpec `json:"spec,omitempty"`
    // Most recently observed status of the Project.
    // Populated by the system.
    // Read-only.
    // +optional
    Status ProjectStatus `json:"status,omitempty"`
}
```

_注：如果某个字段没有加 `+options`，表示这个资源在通过 `kubectl` 操作时是必须的。_

在 `types.go` 中修改了任何的类结构，在使用前，都需要先执行 `make gen-client` 来生成对应的 `client`，生成后的代码目录为 `pkg/client` 中的 `clientset`、`informers`、`listers`，这三个目录中内容不要手动去做任何修改。那些以 `zz_generated.` 开头的文件，也是自动生成的，同样不要做任何修改。

除了 `types.go` 中定义的类或方法外，在 `pkg/apis/devops/` 中定义的其它对象，也都需要在 `pkg/apis/devops/v1alpha1/` 中定义一份，例如 `constants.go`、`register.go` 等。这样做主要是为了版本的管理，`v1alpha1` 中包含了这个版本所定义的所有类、方法以及常量等定义，如果后面升级到了新版本 `v1beta1`, 只用新建一个 `v1beta1` 目录，里面去定义新的操作，原来 `v1alpha1` 的资源操作不会受任何影响。

### 资源的注册

定义好资源的 `struct` 后，需要把它注册到系统中，才可以被 `k8s` 认可。

* 在 `pkg/apis/devops/register.go` 中的 `addKnownTypes` 方法中添加自定义的类型
* 在 `pkg/apis/devops/v1alpha1/register.go` 中的 `addKnownTypes` 方法中添加自定义的类型
* 在 `pkg/apis/devops/install/install.go` 中的 `Install` 方法中设置相关的属性
    ``` golang
    // Install registers the API group and adds types to a scheme
    func Install(groupFactoryRegistry announced.APIGroupFactoryRegistry, registry *registered.APIRegistrationManager, scheme *runtime.Scheme) {
        if err := announced.NewGroupMetaFactory(
            &announced.GroupMetaFactoryArgs{
                GroupName: devops.GroupName,
                RootScopedKinds: sets.NewString(  // 如果新添加的资源是 `cluster` 级别，添加到这里
                    "Project",
                    "ProjectList",
                    "Jenkins",
                    "JenkinsList",
                    "CodeRepoService",
                    "CodeRepoServiceList",
                ),
                IgnoredKinds: sets.NewString(  // 忽略的资源类型
                    "PipelineLogOptions",
                    "PipelineLog",
                ),
                VersionPreferenceOrder:     []string{v1alpha1.SchemeGroupVersion.Version}, // 外部类型列表
                AddInternalObjectsToScheme: devops.AddToScheme,  // 内部类型
            },
            announced.VersionToSchemeFunc{  // 外部类型与 scheme 的关联关系
                v1alpha1.SchemeGroupVersion.Version: v1alpha1.AddToScheme,
            },
        ).Announce(groupFactoryRegistry).RegisterAndEnable(registry, scheme); err != nil {
            panic(err)
        }
    }
    ```
* 在 `pkg/registry/devops/资源名/strategy.go` 中添加资源的 `Strategy`, 可以参考之前的实现, 下面是一些常用策略:
    ``` golang
    // 一些基础判断
    NamespaceScoped // 是否是 `namespace` 级别资源
    AllowCreateOnUpdate // 是否允许以 `put` 方法来创建，默认为 `false`
    AllowUnconditionalUpdate  // 是否允许无条件更新，不管 'version' 是否变化， 默认为 `false`

    // 创建用到的策略，执行顺序从上到下
    PrepareForCreate // 创建前的准备工作：例如将状态置为 `creating` 操作，添加一些基础的字段
    Validate // 验证创建时的字段是否 ok 等操作，这里不能对资源进行修改

    // 更新用到的策略，执行顺序从上到下
    PrepareForUpdate // 更新前的准备工作：检查资源的某些字段是否有变化，如果变化，清空之前的状态，这样 `controller` 才会去再次检查这个资源
    ValidateUpdate // 验证更新时的字段是否 ok 等操作，这里不能对资源进行修改

    // 删除用到的策略
    ...

    // 执行完前面创建或更新策略后，验证成功的前提下，对数据格式进行格式化操作，经常做类型检查操作，或者就保留空方法
    Canonicalize
    ```
* 在 `pkg/registry/devops/资源名/etcd.go` 中配置资源的 `RESTStorage`，如下代码：
    ``` golang
    // NewREST returns a RESTStorage object that will work against API services.
    func NewREST(scheme *runtime.Scheme, optsGetter generic.RESTOptionsGetter) (*registry.REST, error) {
        strategy := NewStrategy(scheme)

        store := &genericregistry.Store{
            NewFunc:                  func() runtime.Object { return &devops.CodeRepoBinding{} },  // 创建操作返回的对象
            NewListFunc:              func() runtime.Object { return &devops.CodeRepoBindingList{} },  // list 操作返回的对象
            PredicateFunc:            MatchCodeRepoBinding,
            DefaultQualifiedResource: devops.Resource("coderepobindings"),  // 资源在 url 中的名称（复数）

            // 增删改策略，写的了一起，到同目录的 `strategy.go` 中查看
            CreateStrategy: strategy,  
            UpdateStrategy: strategy,
            DeleteStrategy: strategy,
        }
        options := &generic.StoreOptions{RESTOptions: optsGetter, AttrFunc: GetAttrs}
        if err := store.CompleteWithOptions(options); err != nil {
            return nil, err
        }
        return &registry.REST{
            Store: store,
            Short: []string{"crb"},  // 资源类型的缩略名，可以有多个
        }, nil
    }
    ```
* 在 `pkg/apiserver/apiserver.go` 中添加 `url` 与 `RESTStorage` 之间的映射， 参考如下代码
    ``` golang
    // adding RepoService REST API
    codeRepoServiceStore := devopsregistry.RESTInPeace(codereposervicestorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
    v1alpha1storage["codereposervices"] = codeRepoServiceStore
    codeRepoBindingStore := devopsregistry.RESTInPeace(coderepobindingstorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
    v1alpha1storage["coderepobindings"] = codeRepoBindingStore
    v1alpha1storage["coderepobindings/repositories"] = coderepobindingstoragerest.NewRepositoryREST(codeRepoServiceStore, codeRepoBindingStore, secretLister)
    v1alpha1storage["coderepositories"] = devopsregistry.RESTInPeace(coderepositorystorage.NewREST(Scheme, c.GenericConfig.RESTOptionsGetter))
    ```

完成上面的操作，资源的注册操作就完成了，但有两个地方需要注意：

* `Stratage` 中的 `validate` 操作，具体逻辑不会写到 `stratage.go` 中，这样不利于复用，代码实际写在 `pkg/apis/devops/validation/` 中
* `PrepareForUpdate` 中关于 `controller` 的详细描述，见下一章节

## controller

完成了前面 `server` 的操作，已经可以通过 `api` 或 `kubectl` 对资源进行简单的增删改查操作，但一些更精细化的操作，例如：

* 与其它平台进行资源的同步操作
* 一个资源变化影响其它资源
* 对资源的操作触发自定义的 `webhook`
* 定时检查资源的状态

放到 `server` 中则不太合适， `server` 应该只是用来提供简单的 `web` 服务，以及对 `payload` 做简单的校验，不应该太重。
这时候需要一个 `worker` 来进行这些后台的操作，这个 `worker` 就是 `controller`.

一个 `controller` 下面可以包含多个子 `controller`, 但不必给每个资源类型添加一个 `controller`，给一类资源添加一个 `controller` 则可。

以代码仓库为例，这个功能包含了三种资源类型 `CodeRepoService`、`CodeRepoBinding`、`CodeRepository`， 它们的 `controller` 定义在 `pkg/controller/devops/coderepo/controller.go` 中，具体逻辑见代码中的注释。

## 添加插件

`pkg/plugin` 下面可以添加自定义的插件，具体写法见这个目录下的具体代码，在启动 `server` 时候动态的添加进去：

``` golang
...
    spec:
      serviceAccountName: devops-apiserver
      containers:
      - name: server
        image: index.alauda.cn/alaudak8s/devops-apiserver:v0.2.10
        imagePullPolicy: Always
        command:
        - "/devops-apiserver"
        - "server"
        - "--etcd-servers=http://etcd-service:2379"
        - "--logtostderr"
        - "--v=5"
        - "--admission-control=BanProject,VerifyDependency"
...
```

现在有两个插件:

* `BanProject`
  * 管理 `project` 与 `namespace` 之间的管理，必须一对一
  * 管理 `namespace` 的黑名单，例如 `kube-system`, `default`, `alauda-system` 这三个不允许用户操作
* `VerifyDependency`
  * 管理资源之间的依赖关系，例如创建 `CodeRepoBinding`，需要确保其关联的 `CodeRepoService` 在 `k8s` 中是存在的，不存在则不允许创建
  * 保存到 `etcd` 前自动添加一些额外数据（非用户传递过来的），例如触发流水线，自动添加流水线的 `number`

## 第三方服务的添加

如果需要调用第三方服务，来完成一些公共的操作，例如检查服务状态、获取仓库列表等操作，这些操作在 `server` 和 `controller` 中都会用到，这些操作可以抽象到 `pkg/client/thirdparty` 中，这个目录下有两个子目录 `externalversions` 和 `internalversion`, 这两个目录中代码是一样的，不同的是：

* 前者引用  `pkg/apis/devops/v1alpha1/types.go` 中的类，后者引用 `pkg/apis/devops/types.go` 中的类
* 前者是给 `controler` 或第三方用的，后者是给 `server` 用的

## 子资源

k8s 子资源（sub-resource）是对默认的 CRUD 以外的功能扩展。

在对应的类型上，添加类似如下的注解后，可以通过命令来生成对应的客户端代码：`./hack/update-codegen.sh`

`// +genclient:method=Toggle,verb=create,subresource=toggle,input=PipelineConfigToggleOptions,result=PipelineConfigToggleResult`

## questions

1. 现在两个 `types.go` 中有相同的类，使用时如何区分呢？

    * 以版本号命名的目录，都是给 `apiservice` 外部使用的，例如 `controller`、`diablo` 等其他组件使用的
    * `devops` 目录下除版本号命名的目录外，包含 `type.go`, `constrants.go` 等文件，才是给 `apiservice` 使用的。