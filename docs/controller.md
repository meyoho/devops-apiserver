+++
title = "Controller Manager Proposal"
description = ""
weight = 2
alwaysopen = false
level = "D"
+++

# Controller Manager Proposal

## Challenges/Problems

1. Each controller has a lot of bootstrap and workqueue code. It leads to complex development time, hard to implement controller and unit testing
2. No default pattern is used in each controller leading to lots of unsustainable code

## Proposal

This proposal offers the solution in three different steps that are described bellow:
1. Controller Manager
2. Controller
3. Reconciler

As observed in Kubernetes source code, it's own controllers still have the same problem:

1. Each one have to maintain its own queue logic:
2. Number of workers and queuing logic is all the same for all: [replica ctrl](https://github.com/kubernetes/kubernetes/blob/master/pkg/controller/replicaset/replica_set.go#L177) [serviceaccount ctrl](https://github.com/kubernetes/kubernetes/blob/master/pkg/controller/serviceaccount/serviceaccounts_controller.go#L111)
3. 



Another good reference for controller stack is used in [Kubebuilder](https://github.com/kubernetes-sigs/kubebuilder/) that uses a common framework [controller-runtime](https://github.com/kubernetes-sigs/controller-runtime) and [controller-tools](https://github.com/kubernetes-sigs/controller-tools) . We could use it directly but there are a few issues:

1. 








### Controller Manager