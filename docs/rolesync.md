+++
title = ""
description = ""
weight = 2
alwaysopen = false
level = "D"
+++

# Role Sync guide

[文档](http://confluence.alaudatech.com/pages/viewpage.action?pageId=36864613)

[TOC]

## Scheme 
`pkg/role.Scheme`：权限同步机制使用一个公用的 `scheme` 来解决不同的平台的权限或角色mapping，以及角色之间的优先级。

`pkg/role.Platform`：不同的工具需要定义权限同步的角色mapping以及同步方式。目前权限同步的机制支持如下三种权限模式：

1. rbac (一个用户在一个租户中可以拥有多个角色)
2. rbac-mutex (一个用户在一个租户中只能拥有一个角色)
3. custom (平台共享的角色提供一个自定义的权限map)

场景：ACP 和 ACE 产品都使用 RBAC 模式。Gitlab/Github在不同的组织下用户只能拥有一个角色

`pkg/role.Priority`：为了方便计算用户的角色以及权限，权限同步机制使用一个中间角色列表以及提供不同的角色的优先级 

## Controller
不同的服务的资源（例如 `CodeRepoService`, `Jenkins`, 等）需要添加一个新的独立的controller来开始处理权限同步



### 前提

1. 使用新的controller. 请见 [优化controller方案](http://confluence.alaudatech.com/pages/viewpage.action?pageId=35688172)

2. controller本身不直接访问工具，而是通过 Roles Subresource API 来访问（见如下 Roles Subresource API)



### 实现

1. 添加服务资源controller（Jenkins，CodeRepoService，ImageRegistry，等）
2. 使用 `pkg/controller/reconciler.RoleSyncReconciler` 。需要提供如下函数：

#### GetToolData

```
//  GetToolData should return a set of data defined by ToolData regarding the tool in question
GetToolData func(reconcile.Request) (data ToolData, err error)
```

此函数需要获取服务具体的实例，所有实例相关的binding，和namespace～工具租户关系



**参数：**Request信息

**返回结果：** ToolData 和 error

```
// ToolData data type returned by the tool for role syncing
type ToolData struct {
	// ToolInstance is the instace of the the DevOps Tool or integrated service. e.g CodeRepoService Gitlab, etc.
	ToolInstance runtime.Object
	// Bindings collections of bindings for the instance. Used only by clients of RoleSyncReconciler
	Bindings []runtime.Object

	// NamespaceProjectsMap a map of namespace ~ tool projects.
	// For instance: a specific namespace "dev" already binds gitlab's "dev", "dev-project" groups.
	// In this case the map should be "dev": ["dev", "dev-project"]
	NamespaceProjectsMap map[string][]string

	// ToolType denotes the kind of tool, e.g "gitlab", "harbor" to be used when calculating permissions
	ToolType string
}

```



#### GetRoleMapping

```
// GetRoleMapping function to return RoleMapping information based on the projects fetched from the platform
GetRoleMapping func(data ToolData, opts *v1alpha1.RoleMappingListOptions) (roleMapping *v1alpha1.RoleMapping, err error)
```

此函数需要使用如下 Roles Subresource API来获取具体实例不同租户的用户～角色列表。Reconciler需要使用这些信息来计算权限

**参数：**ToolData， RoleMappingListOptions

**返回结果：**RoleMapping 和 error



#### ApplyRoleMapping

```
// ApplyRoleMapping function to apply/send a request to the API Server in order to update the role mapping
ApplyRoleMapping func(data ToolData, roleMapping *v1alpha1.RoleMapping) (result *v1alpha1.RoleMapping, err error)
```

此函数需要使用如下 Role Subresource API 来设置计算出来的权限设置

**参数：**ToolData， RoleMapping (计算结果)

**返回结果：**RoleMapping （从Roles Subresource API） 和 error

## Roles Subresource API
不同的服务的资源（例如 `CodeRepoService`, `Jenkins`, 等）需要添加两个subresource API：
POST /roles
GET /roles

为了方便大家实现 `/roles` subresource API 使用 `pkg/registry/generic.RolesREST` 来完成大体架子。每个工具服务实例需要实现如下函数：

### GetRolesMappingFunc

```
// GetRolesMappingFunc function to get a specific tool role mapping provided a dependency getter, context, instance name, and a list of options
GetRolesMappingFunc func(DependencyGetter, context.Context, string, *devops.RoleMappingListOptions) (*devops.RoleMapping, error)
```

此函数主要是通过 DependencyGetter 来获取具体服务的实例，context是请求上下文，name string是具体实例名称，RoleMappingListOptions 是服务中租户用户角色参数。主要需要返回的结果是options指定的租户列表中所有的用户～角色信息（请参考 RoleMapping [设计文档](http://confluence.alaudatech.com/pages/viewpage.action?pageId=36864613#space-menu-link-content) ）



### ApplyRolesMappingFunc

```
// ApplyRolesMappingFunc function to apply a role mapping to a specific tool provided a dependency getter, context, instance name and the target role mapping
ApplyRolesMappingFunc func(DependencyGetter, context.Context, string, *devops.RoleMapping) (*devops.RoleMapping, error)
```



此函数主要是通过 DependencyGetter 来获取具体服务的实例，context是请求上下文，name string是具体实例名称，RoleMapping 是用户～角色操作列表（请参考 RoleMapping [设计文档](http://confluence.alaudatech.com/pages/viewpage.action?pageId=36864613#space-menu-link-content) ）
实现时需要根据 RoleMapping 信息在不同的租户中执行对应的操作



## 自动生成 Role Sync Scheme: Types comments
为了自动生产 Role Sync Scheme 不同的平台需要添加自己的权限信息

主要字段：

| key        | values                            | description                                                  |
| ---------- | --------------------------------- | ------------------------------------------------------------ |
| `class`    | `platform` or `priority`          | 两种信息。不同的平台应该使用 `platform`. `priority` 使用处在定义角色优先级（全局） |
| `name`     | 字符串                            | 名称                                                         |
| `sync`     | `rbac` `rbac-mutext` or `custom`  | (class=platform) 如上提到了不同的权限同步方式                |
| `enabled`  | `true` or `false`                 | (class=platform) 平台是否开启使用                            |
| `roles`    | <role mapping>                    | (class=platform) 角色mapping（如下有描述）                   |
| `source`   | `true` or `false` (默认 `false` ) | (class=platform) 是否权限角色source                          |
| `priority` | <数字>                            | (class=priority) 优先级                                      |



### rbac

使用RBAC时主要操作列表包含 `add` 或 `remove` 角色操作

full example

`// +alauda:rolesync-gen:class=platform,name=acp,sync=rbac,enabled=true,roles=project_admin:alauda_project_admin;project_auditor:alauda_project_auditor;namespace_admin:alauda_namespace_admin;namespace_developer:alauda_namespace_developer;namespace_auditor:alauda_namespace_auditor;space_admin:alauda_space_admin;space_developer:alauda_space_developer;space_auditor:alauda_space_auditor`

在 `sync=rbac` 时，对应的角色mapping使用如下格式来定义：

`roles=<公共角色名称·>:<平台对应的角色名称>;<其他角色名称>:<平台对应角色>`

Example: `roles=project_admin:alauda_project_admin;project_auditor:alauda_project_auditor`

另外，rbac 还支持自定义角色的权限，可以通过custom 字段来实现。 具体可参考 下文中关于 custom 类型的描述。

### rbac-mutex

使用 RBAC Mutex时主要操作列表包含 `add`，`update`  或 `remove` 角色操作

full example

`// +alauda:rolesync-gen:class=platform,name=gitlab,sync=rbac-mutex,enabled=true,roles=project_admin:Owner;project_auditor:Guester;namespace_admin:Master;namespace_developer:Developer;namespace_auditor:Reporter;space_admin:Master;space_developer:Developer;space_auditor:Reporter`

在 `sync=rbac-mutex` 时，对应的角色mapping使用如下格式来定义：

`roles=<公共角色名称·>:<平台对应的角色名称>;<其他角色名称>:<平台对应角色>`

Example: `roles=project_admin:alauda_project_admin;project_auditor:alauda_project_auditor`

### custom

这种角色同步方式完全由具体工具的实现来处理最终设置的方式。因为无法判断用户当前的权限所有的操作是 `update` 操作来提供最终权限，并且由于具体实现来判断以及处理

full example：

`// +alauda:rolesync-gen:class=platform,name=jenkins,sync=custom,enabled=true,roles=project_admin[read:true;write:true;admin:true;delete:true]project_auditor[read:true;write:false;admin:false;delete:false]namespace_admin[read:true;write:true;admin:false;delete:true]namespace_developer[read:true;write:true;admin:false;delete:false]namespace_auditor[read:true;write:true;admin:false;delete:false]space_admin[read:true;write:true;admin:false;delete:false]space_developer[read:true;write:true;admin:false;delete:false]space_auditor[read:true;write:true;admin:false;delete:false]`

在`sync=custom` 时对应角色mapping稍微复杂：

`custom=<公共角色名称>[<key1>:<value1>;<key2>:<value2>]<其他角色名称[<key>:<value>]`

example: `custom=project_admin[read:true;write:true;admin:true;delete:true]project_auditor[read:true;write:false;admin:false;delete:false]`

