+++
title = "镜像仓库凭据多集群同步方案"
description = ""
weight = 2
alwaysopen = false
level = "D"
+++

# 镜像仓库凭据多集群同步方案（对接标准产品）

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->
[TOC]
相关文档：方案： [镜像仓库凭据多集群同步](http://confluence.alaudatech.com/pages/viewpage.action?pageId=36866625)

## 目的
标准产品 Project 会对接多集群，所以需要 支持 镜像仓库的凭据能够同步到相应的关联的多集群中。
## 方案
基本和 方案： [镜像仓库凭据多集群同步](http://confluence.alaudatech.com/pages/viewpage.action?pageId=36866625) 一致

## 依赖
- auth 的 Project CRD
- aladua proxy（erebus） 组件  
- auth, devops组件在Global部署, 可在当前集群，查询到 project信息。


### 获取 project 的 所有 namespace

- Projects 查询  
  API 为 GET /apis/auth.alauda.io/v1/projects/{name},   
  由于 Controller 和 Auth 都部署在Gobal , 所以， 当前的Project信息，可以直接从当前集群中获取。从Projects 信息中，可以获取相关的Cluster.

- namespace 查询
  根据 label selector alauda.io/project={project} ， 遍历 获得的clusters，调用多集群的API, 查询namespace

### 多集群的API调用
- Controller 启动参数中增加 `--multi-clusterhost`
  根据 cluster registry api endpoint  以及 要操作的目标 cluster 名称 初始化 cluster 的 client

```dot
 digraph graphname {
     devops_controller -> alauda_proxy -> cluster_1;
     alauda_proxy -> cluster_2
     alauda_proxy -> cluster_3
 }
```