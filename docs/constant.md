+++
title = "代码中的constant的添加和使用"
description = ""
weight = 2
alwaysopen = false
level = "D"
+++

## 如何优雅的增加、使用constant

### 目的
目前我们在代码中大量的使用到了constant,这些constant很多都是作为某个资源的annotation或者label的。目前为了提高用户体验，我们在这些constant上提供了可定制的basedomain的服务。

这样如果我们需要增加一个constant的时候，就需要考虑如何使得constant能够实现定制化的需求。

### 解决方式
为了解决上述问题，我们在 pkg/apis/devops.annotationprovider.go 中实现了 AnnotationProvider。该结构体会获取当前的basedomain,并且通过实现的各种方法将 basedomain 加入到 constant 中。

### 如何使用

#### 添加
如果需要添加某个常量，需要在 pkg/apis/devops.annotationprovider.go 实现对应的函数，示例如下
```
func (p AnnotationProvider) AnnotationsKeyDisplayNameZh() string {
	return p.formated("displayName.zh-CN")
}
```

调用的时候，只需要直接调用 AnnotationProvider.AnnotationsKeyDisplayNameZh() 即可

#### 使用
devops-apiserver使用示例

```
    // 初始化  devops-apiserver/pkg/apiserver/apiserver.go
    provider := devops.NewAnnotationProvider(c.ExtraConfig.BaseDomain)
	
    // 调用  devops-apiserver/pkg/registry/devops/pipeline/rest/common.go
    _, isMultiBranchPipeline = pipeline.Annotations[provider.AnnotationsJenkinsMultiBranchName()]


```
