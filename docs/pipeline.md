+++
title = "流水线相关说明"
description = ""
weight = 2
alwaysopen = false
level = "D"
+++

# pipeline
`pipeline` 为流水线执行记录对应的资源类型。

通过下面的命令可以获取单个 pipeline 的数据
```
curl -s -k  https://localhost:6443/apis/devops.alauda.io/v1alpha1/namespaces/zxj/pipelines/input-request-3-2 --header "Authorization: Bearer `cat token`"
```

# subResources

## input

通过下面的命令可以测试该子资源是否注册成功

```
curl -s -k  https://localhost:6443/apis/devops.alauda.io/v1alpha1 --header "Authorization: Bearer `cat token`" | grep pipelines/input
```

通过下面的命令可以测试该子资源。请注意，下面的命令是在 master 节点上直接执行的。例子中，流水线的名称为 `input`，所在命名空间为 `zxj`。

你需要根据实际情况修改 `inputID` 和 `parameters`。`inputID` 可以在 `input` 步骤中指定，下面的例子中是自动生成的。生成的 `inputID` 可以从 Jenkins 页面表单中拿到，例如：访问 `https://129.28.182.197/jenkins/job/zxj/job/zxj-input/18/input/` 后找到对应的 `form` 表单的 `name` 字段即可。

```
curl -s -k -H "Content-Type:application/json" https://localhost:6443/apis/devops.alauda.io/v1alpha1/namespaces/zxj/pipelines/input-27/input -X POST --data '{"approve": true, "inputID": "3aa22b42367cdb540a0cf69e34ac7489", "parameters": [{"name": "test-1-str", "value": "true"}]}' --header "Authorization: Bearer `kubectl -n alauda-system get secret | grep devops-apiserver-token | awk '{print $1}' | xargs kubectl -n alauda-system get secret -o jsonpath={.data.token} | base64 --d`"
```

## tasks

```
curl -s -k -H "Content-Type:application/json" https://localhost:6443/apis/devops.alauda.io/v1alpha1/namespaces/zxj/pipelines/input-27/tasks?stage=17 --header "Authorization: Bearer `kubectl -n alauda-system get secret | grep devops-apiserver-token | awk '{print $1}' | xargs kubectl -n alauda-system get secret -o jsonpath={.data.token} | base64 --d`"
```

## testReports

首先通过下面的命令来确认子资源是否注册成功
```
curl -s -k  https://localhost:6443/apis/devops.alauda.io/v1alpha1 --header "Authorization: Bearer `kubectl -n alauda-system get secret | grep devops-apiserver-token | awk '{print $1}' | xargs kubectl -n alauda-system get secret -o jsonpath={.data.token} | base64 --d`" | grep pipelines/testreports
```

然后，创建一个执行过 `JUnit` 单元测试的流水线。

```
curl -s -k "https://localhost:6443/apis/devops.alauda.io/v1alpha1/namespaces/zxj/pipelines/junit-5/testreports?start=0&limit=100"  --header "Authorization: Bearer `kubectl -n alauda-system get secret | grep devops-apiserver-token | awk '{print $1}' | xargs kubectl -n alauda-system get secret -o jsonpath={.data.token} | base64 --d`"
```
