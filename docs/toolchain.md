+++
title = "如何集成新的工具链"
description = ""
weight = 2
alwaysopen = false
level = "D"
+++

# 工具链

[TOC]

## 概述

工具链是 `devops` 平台上的一个功能，里面包含了一系列的工具，这些工具分不同的类别，例如 `代码仓库`、`持续集成`、`制品仓库` 等，每一个类别下
包含一个或多个工具。这些工具的默认配置信息会在 `devops` 初次部署时，添加到 `devops-config` 中。

当开发了新的功能，需要加入到工具链中时，需要遵循如下的操作步骤：

## 操作步骤 (New)

（自动生成代码模式）

1. 确认新添加的功能属于哪一分类，例如添加了集成 `github` 的功能，此功能属于工具链中的 `代码仓库` 类别
2. 如果加了分类需要在对应的`types` 文件添加 `category` 的注释，例如：
  `// +alauda:toolchain-gen:class=category,name=codeQualityTool,en=Code Quality,zh=代码检查,enabled=true `
3. 如果要在分类添加新的type可以添加 item 的注释，例如：
  `// +alauda:toolchain-gen:class=item,category=codeQualityTool,name=sonarqube,en=SonarQube,zh=SonarQube,apipath=codequalitytools,enabled=true,kind=CodeQualityTool,type=Sonarqube`
4. 如果要给 type 添加所支持的 secret 类型的注释，例如：
`// +alauda:toolchain-gen:class=secret,category=codeQualityTool,itemNames=sonarqube,type=kubernetes.io/basic-auth,zh=用户名和密码均为登录时的用户名和密码,en=username and password used to login`
5. ~~执行 make gen-client~~
使用pkg/cmd/gen_tool.go，简易用法`go run main.go gen tool -p ./pkg/apis/devops/v1alpha1 -o ./pkg/apis/devops/v1alpha1/zz_generated.toolchain.go && go fmt ./pkg/apis/devops/v1alpha1/zz_generated.toolchain.go`
6. 执行后代码会生成到 `zz_generated.toolchain.go` 文件里

参数描述：

`// +alauda:toolchain-gen:` 是toolchain-gen 的前缀

category和item 共享的参数

| 参数    | 可选值           | 描述                                                         |
| ------- | ---------------- | ------------------------------------------------------------ |
| class   | item 或 category | 如果是大分类，比如 CI，代码仓库， 制品仓库需要用 class=category<br />如果是分类内的某一个type，例如 Github, Dockerhub, 需要用 class=item |
| name    |                  | 资源名称，这使用为默认添加的名称                             |
| en      |                  | 英文显示名称                                                 |
| zh      |                  | 中文显示名称                                                 |
| enabled | false 或 true    | 是否给用户展示，如果不体现就默认为false                      |

category特殊参数

| 参数    | 可选值           | 描述                                                          |
| ------- | ---------------- | ------------------------------------------------------------|
| index    |                  | 用来支持toolchains的排序                                     |

item特殊参数



| 参数       | 可选值        | 描述                                                        |
| ---------- | ------------- | ----------------------------------------------------------- |
| category   |               | category的名称，必须跟 class=category,name=<名称> 一致      |
| apipath    |               | 资源类型的 API 路径，必须跟 pkg/apiserver/apiserver.go 一致 |
| kind       |               | 资源类型，例如 CodeRepoService                              |
| type       |               | 资源类型中的 Type, 例如 Github, Gitee 等                    |
| public     | true 或 false | 是否公网的服务                                              |
| enterprise | true 或 false | 是否企业版                                                  |
| api        |               | public为true时，需要定义一个API地址                         |
| web        |               | public为true时，需要定义一个页面访问地址                    |
|roleSyncEnabled | true 或 false| 是否支持权限同步                                       |

secret的参数

| 参数       | 可选值        | 描述                                                        |
| ---------- | ------------- | ----------------------------------------------------------- |
| category   |               | category 的名称，必须跟 class=category,name=<名称> 一致         |
| itemNames   |               | 支持这个 secret 类型的 item 名字，多个名字用 + 号分割                              |
| type       |               | Secret 的类型，例如 kubernetes.io/basic-auth                 |
| en         |               | 英文 secret 使用说明                                  |
| zh         |               | 中文 secret 使用说明 

## 操作步骤 (OLD - DEPRECATED)

1. 确认新添加的功能属于哪一分类，例如添加了集成 `github` 的功能，此功能属于工具链中的 `代码仓库` 类别
2. 打开 `pkg/apis/devops/v1alpha1/toolchain.go`，找到对应的方法，添加默认配置到对应的方法中:

    ``` sh
    getDefaultCodeRepositoryItems: 添加`代码仓库`的默认配置
    getDefaultContinuousIntegrationItems: 添加`持续集成`的默认配置
    getDefaultArtifactRepositoryItems: 添加`制品仓库`的默认配置
    getDefaultTestToolItems: 添加`测试工具`的默认配置
    getDefaultProjectManageItems: 添加`项目管理`的默认配置
    ```
3. 添加默认配置的相关代码如下：
    ``` sh
    // append github
    items = append(items, &ToolChainElementItem{
        Kind:    ResourceKindCodeRepoService,  // 资源的类型，例如 CodeRepoService
        Type:    CodeRepoServiceTypeGithub.String(),  // 资源的子类型，例如 Github，如果没有则为空
        Public:  true,  // 是否是公共的 (公共的工具， host 不可编辑)
        Enabled: true,  // 是否可用 (如果为false，页面上不显示)
        Name:    GithubName,  // 集成时候的默认名字
        DisplayName: DisplayName{
            EN: GithubDisplayNameEN,  // 英文显示名
            ZH: GithubDisplayNameCN,  // 中文显示名
        },
        Host: GithubHost,  // api 地址
        HTML: GithubHTML,  // 网页访问地址
        SupportedSecretTypes: []ToolChainItemSecretType{  // 支持的 Secret 类型
        			{
        				Type: GithubSecretTypeBasicAuth,
        				Description: I18nDescription{
        					ZH: GithubSecretTypeBasicAuthDescriptionCN, // 中文 Secret 使用说明
        					EN: GithubSecretTypeBasicAuthDescriptionEN, // 英文 Secret 使用说明
        				},
        			},
        			{
        				Type: GithubSecretTypeOAuth2,
        				Description: I18nDescription{
        					ZH: GithubSecretTypeOAuth2DescriptionCN,
        					EN: GithubSecretTypeOAuth2DescriptionEN,
        				},
        			},
        		},
    })
    ```
4. 添加完成，验证工具是否添加成功：
    * `make run` 后， 执行 `kubectl -n alauda-system get cm devops-config -o yaml`， 查看 `data --> _domain --> toolChains` 是否有集成的工具信息
    * 到工具链页面，点击 `集成` 按钮，检查弹出的页面是否有新添加的工具，如果有，说明新功能已成功集成的工具链中

## 其它

* 上面步骤只是添加了工具的默认配置信息，但用户可以直接修改 `devops-config` 中的默认配置信息，但重启 `deploy` 时，不应该覆盖用户的更改，只会添加我们新增添的工具。（如果没有新增，则跳过）
* 添加新功能到工具链后，需要跟前端保持沟通，告诉集成新功能所需的api，可以参考之前集成代码仓库和`jenkins`的集成实现。