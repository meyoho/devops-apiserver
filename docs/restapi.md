+++
title = "devops-api开发说明"
description = ""
weight = 2
alwaysopen = false
level = "D"
+++

# RESTful API

作用：为了更容易的迁移diablo backend中的API到devops-apiserver。

启动方式：`make startapi`

可以看一下其它启动参数： `GOPROXY=https://athens.acp.alauda.cn GO111MODULE=on go run pkg/cmd/devops-api/api.go --help`

启动后可以访问 http://localhost:8080/swagger-ui/#/ 来查看所有的API

## 包结构

主要内容为以下三部分

 - context: 操作 context.Context 内容（导入或获取对象）
 - decorator: go-restful 的 middleware (filter) 和request/response helper函数 
 - handler: 具体API的包 如果有对api的修改或者增删  主要需要在这个部分中进行修改

### Handler

这部分需要分两部：

 - `add_builders.go` 添加具体handler的constructor函数
 - 按不同的资源区分不同的包（例如 jenkins）


一个具体的handler里面区分三部分：

 - Constructor function
 - Handler
 - Processor


#### Constructor

主要作用：

 - 初始化 handler and processor
 - 注册当前资源的API路径和API结构
 - 初始化需要的middleware等


#### Handler

主要作用：

声明所有的API的handler和helper相关的函数，包含一个Processor interface的对象

不同的API的handler中主要作用是：
 - 从context获取所有需要的对象
 - 调度processor对应的函数
 - 基于processor的结果写response


#### Processor

内部处理资源操作，包括过滤，排序，分页

可以参考 `jenkins`的例子

##### 过滤，排序和分页：

目前可以继续用DataCell 的概念来提供过滤和排序。alauda-backend默认提供一个可以使用metadata上的数据的实现：


```
const (
	objectName              = "name"
	objectNamespace         = "namespace"
	objectLabel             = "labels"
	objectAnnotation        = "annotations"
	objectCreationTimestamp = "creationTimestamp"
)

// GetProperty returns a comparablevalue for metav1.Object datacell
func (o ObjectDataCell) GetProperty(name PropertyName) ComparableValue {
	if o.Object == nil {
		return nil
	}
	switch name {
	case objectName:
		return StdComparableContainsString(o.GetName())
	case objectNamespace:
		return StdComparableString(o.GetNamespace())
	case objectCreationTimestamp:
		return StdComparableTime(o.GetCreationTimestamp().Time)
	case objectLabel:
		if len(o.GetLabels()) > 0 {
			return GetComparableLabelFromMap(o.GetLabels())
		}
	case objectAnnotation:
		if len(o.GetAnnotations()) > 0 {
			return GetComparableLabelFromMap(o.GetAnnotations())
		}
	}
	return nil
}
```

为了使用可以这样写：

```
// filter using standard filters
itemCells := dataselect.ToObjectCellSlice(data.Items)
itemCells, _ = dataselect.GenericDataSelectWithFilter(itemCells, query)
result := dataselect.FromCellToObjectSlice(itemCells)
// ConvertToJenkinsSlice 函数需要自己把数据从[]metav1.Object到目标类型的slice
data.Items = ConvertToJenkinsSlice(result)

/////
func ConvertToJenkinsSlice(filtered []metav1.Object) (items []v1alpha1.Jenkins) {
	items = make([]v1alpha1.Jenkins, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.Jenkins); ok {
			items = append(items, *cm)
		}
	}
	return
}
```

##### 资源过滤时的filter 语法说明

filterBy的语法是： 
```
<propertyName>,<propertyValue>, <propertyName>,<propertyValue> 
```

相同的 propertyName  是与的关系。

对于流水线模板的筛选：
PropertyName 为 label的时候， property value 的语法为：<labelKey>:<labelValue>
即 labels,<labelKey>:<labelValue>, labels,<labelKey>:<labelValue>

PropertyName 为 category的时候， property value 的语法为：<category value1>:<category value2>
即 category,<category value1>:<category value2>
value1 和 value2 是与的关系。

例如，需要过滤最新的流水线模板，并且是官方的模板，语言为Java， 类型为Build的模板时：
```
 filterBy=category,Build,labels,lang:Java,labels,latest:true,labels,source:official
```