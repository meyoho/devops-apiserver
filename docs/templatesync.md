+++
title = "模板同步逻辑说明"
description = ""
weight = 2
alwaysopen = false
level = "D"
+++

## 模板介绍

目前模板按照 模板的资源Scope 划分为 Cluster 级别的资源和 Namespace 级别的资源， 由资源的Kind 来标识。
按照来源， 划分为  official 和 customer 两种类型， 会在模板的 label  source=customer 或者 source=official 中标识。
在业务定义上， 官方模板， 一定是 Cluster 级别的，客户的自定义模板，有可能是  Cluster级别的，也有可能是namespace 级别的。

使用图表来表示

| \ | `source=official` | `source=customer` |
|-----|----|---|
| Cluster| 官方模板|平台级别自定义模板|
| Namespaced| 无 |项目级别的自定义模板|

目前，产品上并没有将 `自定义的平台级别的模板` 产品化，也就是说，没有相应的入口。

在进行模板导入时：

- 如果是从 磁盘导入的 或者 namespace = global-credentials ， 则我们认为是 Cluster 级别的模板。
- 如果是从磁盘导入的， 则认为是 official 模板， 其他是 customer 模板。

使用图表来表示：
| \ | `source=official`  磁盘导入 | `source=customer`  非磁盘导入 | 
|-----|----|---|---|
| **Cluster** <br> 磁盘导入 or (导入命名空间为global-credentials ) | 官方模板| 平台级别自定义模板 |
| **Namespaced** <br> 导入命名空间不为global-credentials| 无 |项目级别的自定义模板|

## 模板同步逻辑

### 概念
现在模板按照名称区分，分为以下两种

* 冗余版本: 带有版本号的模板
 
* latest版本: 不带有版本号的模板


图中红线为latest版本  绿线为冗余版本

![differenttypetempalte](difftemplate.png)

###策略
目前的模板同步分为两个策略

* VersionUpgrade(高版本覆盖低版本，只有引入模板的版本高于集群中模板的版本，才会发生模板的更新)

* Force(强制同步，无论版本高低，都会进行模板的更新）

现在默认用户自定义模板同步使用 VersionUpgrade 策略，官方模板使用 Force 策略，文档也会按照当前这种策略来编写

### 同步流程

两个策略只有在对待 update 模板的时候有区别，其他地方没有区别

#### Force:

1. 找出集群中包含label createdmethod = templatesync 和 source=official 的 clusterpipelinetemplate模板

2. 与模板文件中的模板进行对比(对比名称和版本号)，最终将模板分成以下四种
    * add: 某一名称的模板，在集群中不存在，但是在仓库文件中存在
    * remove: 某一名称的模板，在集群中存在，但是在官方模板仓库中不存在，则该模板和其对应的冗余版本都会被删除
    * update: 仓库中所有的模板，都会进行更新
    * nochange: 无
    * ignore: 冗余版本的模板，并且模板文件中存在与冗余模板同名的模板
    
3. 对不同类型的模板进行对应操作
    * add: 增加该模板的latest版本，并且增加冗余版本
    * remove: 删除该模板
    * update: 根据仓库模板文件对集群中的模板进行更新,包括冗余版本和latest版本。如果存在版本号相同的冗余版本模板，则直接对冗余版本进行更新，如果冗余版本不存在，则会创建对应的冗余版本版本，最后会更新对应的latest模板
    * nochange: 无
    * ignore：无


####VersionUpgrade

1. 在特定命名空间下，找出中包含label createdmethod = templatesync 和 source=custome 的 pipelinetemplate 模板

2. 与模板文件中的模板进行对比(对比名称和版本号)，最终将模板分成以下四种
    * add: 某一名称的模板，在集群中不存在，但是在仓库文件中存在
    * remove: 某一名称的模板，在集群中存在，但是在用户导入的仓库中不存在，则该模板和其对应的冗余版本都会被删除
    * update: 仓库中模板版本号增加了的模板
    * nochange: 版本号没有增加的模板
    * ignore: 冗余版本的模板，并且模板文件中存在与冗余模板同名的模板
    
3. 对不同类型的模板进行对应操作
    * add: 增加该模板的latest版本，并且增加冗余版本
    * remove: 删除该模板
    * update: 根据仓库模板文件对集群中的模板进行更新,创建新的冗余版本模板，并且更新对应的latest版本模板
    * nochange: 无
    * ignore: 无